﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3497;
#include "t1648.h"

 void m19429 (t3497 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19430 (t3497 * __this, t1648  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19431 (t3497 * __this, t1648  p0, t1648  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
