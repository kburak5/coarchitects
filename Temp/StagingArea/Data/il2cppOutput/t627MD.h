﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t627;
struct t29;
struct t20;
struct t3067;
struct t136;
struct t42;
#include "t3068.h"

#include "t2229MD.h"
#define m2906(__this, method) (void)m11064_gshared((t2229 *)__this, method)
#define m16873(__this, method) (bool)m11065_gshared((t2229 *)__this, method)
#define m16874(__this, method) (t29 *)m11066_gshared((t2229 *)__this, method)
#define m16875(__this, p0, p1, method) (void)m11067_gshared((t2229 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m16876(__this, method) (t29*)m11068_gshared((t2229 *)__this, method)
#define m16877(__this, method) (t29 *)m11069_gshared((t2229 *)__this, method)
#define m16878(__this, method) (t42 *)m11070_gshared((t2229 *)__this, method)
#define m2909(__this, method) (t42 *)m11071_gshared((t2229 *)__this, method)
#define m2907(__this, p0, method) (void)m11072_gshared((t2229 *)__this, (t29 *)p0, method)
#define m2911(__this, method) (int32_t)m11073_gshared((t2229 *)__this, method)
 t3068  m16879 (t627 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
