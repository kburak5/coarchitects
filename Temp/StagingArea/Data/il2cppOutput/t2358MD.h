﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2358;
struct t29;
struct t20;
#include "t725.h"

 void m12151 (t2358 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12152 (t2358 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12153 (t2358 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12154 (t2358 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12155 (t2358 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
