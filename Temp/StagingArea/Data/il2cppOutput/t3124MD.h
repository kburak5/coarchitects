﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3124;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m17257_gshared (t3124 * __this, t29 * p0, t35 p1, MethodInfo* method);
#define m17257(__this, p0, p1, method) (void)m17257_gshared((t3124 *)__this, (t29 *)p0, (t35)p1, method)
 void m17258_gshared (t3124 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m17258(__this, p0, p1, method) (void)m17258_gshared((t3124 *)__this, (t29 *)p0, (t29 *)p1, method)
 t29 * m17259_gshared (t3124 * __this, t29 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method);
#define m17259(__this, p0, p1, p2, p3, method) (t29 *)m17259_gshared((t3124 *)__this, (t29 *)p0, (t29 *)p1, (t67 *)p2, (t29 *)p3, method)
 void m17260_gshared (t3124 * __this, t29 * p0, MethodInfo* method);
#define m17260(__this, p0, method) (void)m17260_gshared((t3124 *)__this, (t29 *)p0, method)
