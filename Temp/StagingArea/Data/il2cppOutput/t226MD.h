﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t226;
struct t29;
struct t139;
struct t2639;
struct t20;
struct t136;
struct t2640;
struct t2641;
struct t2642;
struct t2638;
struct t2643;
struct t2644;
#include "t2645.h"

#include "t294MD.h"
#define m1874(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m14178(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m14179(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m14180(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m14181(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m14182(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m14183(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m14184(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m14185(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m14186(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m14187(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m14188(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m14189(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m14190(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m14191(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m14192(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m14193(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m14194(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m1880(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m14195(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m14196(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m14197(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m14198(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m14199(__this, method) (t2642 *)m10583_gshared((t294 *)__this, method)
#define m14200(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m14201(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m14202(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m14203(__this, p0, method) (t139 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m14204(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m14205(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2645  m14206 (t226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m14207(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m14208(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m14209(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m14210(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m14211(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m1881(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m14212(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m14213(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m14214(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m14215(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m14216(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m14217(__this, method) (t2638*)m10619_gshared((t294 *)__this, method)
#define m14218(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m14219(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m14220(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1888(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1886(__this, p0, method) (t139 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m14221(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
