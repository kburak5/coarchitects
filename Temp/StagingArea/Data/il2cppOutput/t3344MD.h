﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3344;
struct t29;
struct t20;
#include "t1301.h"

 void m18592 (t3344 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18593 (t3344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18594 (t3344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18595 (t3344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18596 (t3344 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
