﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t566;
struct t557;
struct t7;
struct t29;
struct t556;
struct t565;
struct t42;
struct t316;
struct t537;
#include "t554.h"

 void m2812 (t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1324 (t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1325 (t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m2813 (t566 * __this, t565 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m2814 (t566 * __this, t7* p0, t29 * p1, int32_t p2, t42 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2815 (t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2816 (t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2817 (t566 * __this, t556 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2818 (t566 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2819 (t566 * __this, t316* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1323 (t566 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m2820 (t29 * __this, t29 * p0, t7* p1, t537* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
