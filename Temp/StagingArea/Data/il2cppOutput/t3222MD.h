﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3222;
struct t29;
struct t20;
#include "t790.h"

 void m17910 (t3222 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17911 (t3222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17912 (t3222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17913 (t3222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17914 (t3222 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
