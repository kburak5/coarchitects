﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3252;
struct t29;
struct t20;
#include "t1024.h"

 void m18056 (t3252 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18057 (t3252 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18058 (t3252 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18059 (t3252 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m18060 (t3252 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
