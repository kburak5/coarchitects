﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1436;
struct t1425;
struct t633;
struct t781;
#include "t35.h"

 void m8788 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1425 * m8789 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1436 * m8790 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1436 * m8791 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8792 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t633 * m8793 (t1436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8794 (t1436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8795 (t1436 * __this, t633 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t633 * m8796 (t1436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8797 (t1436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8798 (t1436 * __this, t633 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t633 * m8799 (t1436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t633 * m8800 (t1436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8801 (t1436 * __this, t35 p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8802 (t1436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8803 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8804 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8805 (t1436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8806 (t1436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
