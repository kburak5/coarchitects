﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t292;
struct t7;
struct t733;
struct t29;
struct t200;
struct t316;
struct t1094;
#include "t735.h"

 void m8656 (t292 * __this, t7* p0, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8657 (t292 * __this, t7* p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1311 (t292 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2923 (t292 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1431 (t292 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3992 (t292 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8658 (t292 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8659 (t292 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8660 (t292 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8661 (t292 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4184 (t292 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4285 (t292 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m8662 (t292 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8663 (t292 * __this, int32_t p0, uint16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1315 (t292 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8664 (t292 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m8665 (t292 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m8666 (t292 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m8667 (t292 * __this, t7* p0, t7* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m2927 (t292 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m4077 (t292 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m4013 (t292 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m3993 (t292 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m1761 (t292 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m8668 (t292 * __this, uint16_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m8669 (t292 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m8670 (t292 * __this, t7* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m1314 (t292 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m1313 (t292 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m5105 (t292 * __this, t7* p0, t316* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m8671 (t292 * __this, t29 * p0, t7* p1, t316* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m4012 (t292 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m4105 (t292 * __this, t7* p0, t29 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m4108 (t292 * __this, t7* p0, t29 * p1, t29 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m8672 (t292 * __this, int32_t p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m8673 (t292 * __this, int32_t p0, uint16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t292 * m8674 (t292 * __this, int32_t p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8675 (t292 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
