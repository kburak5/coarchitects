﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t522;
struct t522_marshaled;

void t522_marshal(const t522& unmarshaled, t522_marshaled& marshaled);
void t522_marshal_back(const t522_marshaled& marshaled, t522& unmarshaled);
void t522_marshal_cleanup(t522_marshaled& marshaled);
