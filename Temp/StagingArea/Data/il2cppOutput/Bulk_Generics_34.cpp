﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t6465_TI;

#include "t44.h"
#include "t40.h"
#include "t21.h"
#include "mscorlib_ArrayTypes.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>
extern MethodInfo m33471_MI;
static PropertyInfo t6465____Count_PropertyInfo = 
{
	&t6465_TI, "Count", &m33471_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33472_MI;
static PropertyInfo t6465____IsReadOnly_PropertyInfo = 
{
	&t6465_TI, "IsReadOnly", &m33472_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6465_PIs[] =
{
	&t6465____Count_PropertyInfo,
	&t6465____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33471_GM;
MethodInfo m33471_MI = 
{
	"get_Count", NULL, &t6465_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33471_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33472_GM;
MethodInfo m33472_MI = 
{
	"get_IsReadOnly", NULL, &t6465_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33472_GM};
extern Il2CppType t2044_0_0_0;
extern Il2CppType t2044_0_0_0;
static ParameterInfo t6465_m33473_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2044_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33473_GM;
MethodInfo m33473_MI = 
{
	"Add", NULL, &t6465_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6465_m33473_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33473_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33474_GM;
MethodInfo m33474_MI = 
{
	"Clear", NULL, &t6465_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33474_GM};
extern Il2CppType t2044_0_0_0;
static ParameterInfo t6465_m33475_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2044_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33475_GM;
MethodInfo m33475_MI = 
{
	"Contains", NULL, &t6465_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6465_m33475_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33475_GM};
extern Il2CppType t3680_0_0_0;
extern Il2CppType t3680_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6465_m33476_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3680_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33476_GM;
MethodInfo m33476_MI = 
{
	"CopyTo", NULL, &t6465_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6465_m33476_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33476_GM};
extern Il2CppType t2044_0_0_0;
static ParameterInfo t6465_m33477_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2044_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33477_GM;
MethodInfo m33477_MI = 
{
	"Remove", NULL, &t6465_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6465_m33477_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33477_GM};
static MethodInfo* t6465_MIs[] =
{
	&m33471_MI,
	&m33472_MI,
	&m33473_MI,
	&m33474_MI,
	&m33475_MI,
	&m33476_MI,
	&m33477_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t6467_TI;
static TypeInfo* t6465_ITIs[] = 
{
	&t603_TI,
	&t6467_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6465_0_0_0;
extern Il2CppType t6465_1_0_0;
struct t6465;
extern Il2CppGenericClass t6465_GC;
TypeInfo t6465_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6465_MIs, t6465_PIs, NULL, NULL, NULL, NULL, NULL, &t6465_TI, t6465_ITIs, NULL, &EmptyCustomAttributesCache, &t6465_TI, &t6465_0_0_0, &t6465_1_0_0, NULL, &t6465_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>
extern Il2CppType t4947_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33478_GM;
MethodInfo m33478_MI = 
{
	"GetEnumerator", NULL, &t6467_TI, &t4947_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33478_GM};
static MethodInfo* t6467_MIs[] =
{
	&m33478_MI,
	NULL
};
static TypeInfo* t6467_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6467_0_0_0;
extern Il2CppType t6467_1_0_0;
struct t6467;
extern Il2CppGenericClass t6467_GC;
TypeInfo t6467_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6467_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6467_TI, t6467_ITIs, NULL, &EmptyCustomAttributesCache, &t6467_TI, &t6467_0_0_0, &t6467_1_0_0, NULL, &t6467_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4947_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>
extern MethodInfo m33479_MI;
static PropertyInfo t4947____Current_PropertyInfo = 
{
	&t4947_TI, "Current", &m33479_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4947_PIs[] =
{
	&t4947____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2044_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33479_GM;
MethodInfo m33479_MI = 
{
	"get_Current", NULL, &t4947_TI, &t2044_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33479_GM};
static MethodInfo* t4947_MIs[] =
{
	&m33479_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4947_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4947_0_0_0;
extern Il2CppType t4947_1_0_0;
struct t4947;
extern Il2CppGenericClass t4947_GC;
TypeInfo t4947_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4947_MIs, t4947_PIs, NULL, NULL, NULL, NULL, NULL, &t4947_TI, t4947_ITIs, NULL, &EmptyCustomAttributesCache, &t4947_TI, &t4947_0_0_0, &t4947_1_0_0, NULL, &t4947_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3426.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3426_TI;
#include "t3426MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
extern TypeInfo t2044_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m19004_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m25633_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m25633(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>
extern Il2CppType t20_0_0_1;
FieldInfo t3426_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3426_TI, offsetof(t3426, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3426_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3426_TI, offsetof(t3426, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3426_FIs[] =
{
	&t3426_f0_FieldInfo,
	&t3426_f1_FieldInfo,
	NULL
};
extern MethodInfo m19001_MI;
static PropertyInfo t3426____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3426_TI, "System.Collections.IEnumerator.Current", &m19001_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3426____Current_PropertyInfo = 
{
	&t3426_TI, "Current", &m19004_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3426_PIs[] =
{
	&t3426____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3426____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3426_m19000_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19000_GM;
MethodInfo m19000_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3426_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3426_m19000_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19000_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19001_GM;
MethodInfo m19001_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3426_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19001_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19002_GM;
MethodInfo m19002_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3426_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19002_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19003_GM;
MethodInfo m19003_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3426_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19003_GM};
extern Il2CppType t2044_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19004_GM;
MethodInfo m19004_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3426_TI, &t2044_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19004_GM};
static MethodInfo* t3426_MIs[] =
{
	&m19000_MI,
	&m19001_MI,
	&m19002_MI,
	&m19003_MI,
	&m19004_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m19003_MI;
extern MethodInfo m19002_MI;
static MethodInfo* t3426_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19001_MI,
	&m19003_MI,
	&m19002_MI,
	&m19004_MI,
};
static TypeInfo* t3426_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4947_TI,
};
static Il2CppInterfaceOffsetPair t3426_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4947_TI, 7},
};
extern TypeInfo t2044_TI;
static Il2CppRGCTXData t3426_RGCTXData[3] = 
{
	&m19004_MI/* Method Usage */,
	&t2044_TI/* Class Usage */,
	&m25633_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3426_0_0_0;
extern Il2CppType t3426_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3426_GC;
extern TypeInfo t20_TI;
TypeInfo t3426_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3426_MIs, t3426_PIs, t3426_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3426_TI, t3426_ITIs, t3426_VT, &EmptyCustomAttributesCache, &t3426_TI, &t3426_0_0_0, &t3426_1_0_0, t3426_IOs, &t3426_GC, NULL, NULL, NULL, t3426_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3426)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6466_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeClientContextSink>
extern MethodInfo m33480_MI;
extern MethodInfo m33481_MI;
static PropertyInfo t6466____Item_PropertyInfo = 
{
	&t6466_TI, "Item", &m33480_MI, &m33481_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6466_PIs[] =
{
	&t6466____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2044_0_0_0;
static ParameterInfo t6466_m33482_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2044_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33482_GM;
MethodInfo m33482_MI = 
{
	"IndexOf", NULL, &t6466_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6466_m33482_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33482_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2044_0_0_0;
static ParameterInfo t6466_m33483_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2044_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33483_GM;
MethodInfo m33483_MI = 
{
	"Insert", NULL, &t6466_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6466_m33483_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33483_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6466_m33484_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33484_GM;
MethodInfo m33484_MI = 
{
	"RemoveAt", NULL, &t6466_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6466_m33484_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33484_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6466_m33480_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2044_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33480_GM;
MethodInfo m33480_MI = 
{
	"get_Item", NULL, &t6466_TI, &t2044_0_0_0, RuntimeInvoker_t29_t44, t6466_m33480_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33480_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2044_0_0_0;
static ParameterInfo t6466_m33481_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2044_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33481_GM;
MethodInfo m33481_MI = 
{
	"set_Item", NULL, &t6466_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6466_m33481_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33481_GM};
static MethodInfo* t6466_MIs[] =
{
	&m33482_MI,
	&m33483_MI,
	&m33484_MI,
	&m33480_MI,
	&m33481_MI,
	NULL
};
static TypeInfo* t6466_ITIs[] = 
{
	&t603_TI,
	&t6465_TI,
	&t6467_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6466_0_0_0;
extern Il2CppType t6466_1_0_0;
struct t6466;
extern Il2CppGenericClass t6466_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6466_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6466_MIs, t6466_PIs, NULL, NULL, NULL, NULL, NULL, &t6466_TI, t6466_ITIs, NULL, &t1908__CustomAttributeCache, &t6466_TI, &t6466_0_0_0, &t6466_1_0_0, NULL, &t6466_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6468_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>
extern MethodInfo m33485_MI;
static PropertyInfo t6468____Count_PropertyInfo = 
{
	&t6468_TI, "Count", &m33485_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33486_MI;
static PropertyInfo t6468____IsReadOnly_PropertyInfo = 
{
	&t6468_TI, "IsReadOnly", &m33486_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6468_PIs[] =
{
	&t6468____Count_PropertyInfo,
	&t6468____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33485_GM;
MethodInfo m33485_MI = 
{
	"get_Count", NULL, &t6468_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33485_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33486_GM;
MethodInfo m33486_MI = 
{
	"get_IsReadOnly", NULL, &t6468_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33486_GM};
extern Il2CppType t2045_0_0_0;
extern Il2CppType t2045_0_0_0;
static ParameterInfo t6468_m33487_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2045_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33487_GM;
MethodInfo m33487_MI = 
{
	"Add", NULL, &t6468_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6468_m33487_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33487_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33488_GM;
MethodInfo m33488_MI = 
{
	"Clear", NULL, &t6468_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33488_GM};
extern Il2CppType t2045_0_0_0;
static ParameterInfo t6468_m33489_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2045_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33489_GM;
MethodInfo m33489_MI = 
{
	"Contains", NULL, &t6468_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6468_m33489_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33489_GM};
extern Il2CppType t3681_0_0_0;
extern Il2CppType t3681_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6468_m33490_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3681_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33490_GM;
MethodInfo m33490_MI = 
{
	"CopyTo", NULL, &t6468_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6468_m33490_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33490_GM};
extern Il2CppType t2045_0_0_0;
static ParameterInfo t6468_m33491_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2045_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33491_GM;
MethodInfo m33491_MI = 
{
	"Remove", NULL, &t6468_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6468_m33491_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33491_GM};
static MethodInfo* t6468_MIs[] =
{
	&m33485_MI,
	&m33486_MI,
	&m33487_MI,
	&m33488_MI,
	&m33489_MI,
	&m33490_MI,
	&m33491_MI,
	NULL
};
extern TypeInfo t6470_TI;
static TypeInfo* t6468_ITIs[] = 
{
	&t603_TI,
	&t6470_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6468_0_0_0;
extern Il2CppType t6468_1_0_0;
struct t6468;
extern Il2CppGenericClass t6468_GC;
TypeInfo t6468_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6468_MIs, t6468_PIs, NULL, NULL, NULL, NULL, NULL, &t6468_TI, t6468_ITIs, NULL, &EmptyCustomAttributesCache, &t6468_TI, &t6468_0_0_0, &t6468_1_0_0, NULL, &t6468_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>
extern Il2CppType t4949_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33492_GM;
MethodInfo m33492_MI = 
{
	"GetEnumerator", NULL, &t6470_TI, &t4949_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33492_GM};
static MethodInfo* t6470_MIs[] =
{
	&m33492_MI,
	NULL
};
static TypeInfo* t6470_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6470_0_0_0;
extern Il2CppType t6470_1_0_0;
struct t6470;
extern Il2CppGenericClass t6470_GC;
TypeInfo t6470_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6470_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6470_TI, t6470_ITIs, NULL, &EmptyCustomAttributesCache, &t6470_TI, &t6470_0_0_0, &t6470_1_0_0, NULL, &t6470_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4949_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>
extern MethodInfo m33493_MI;
static PropertyInfo t4949____Current_PropertyInfo = 
{
	&t4949_TI, "Current", &m33493_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4949_PIs[] =
{
	&t4949____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2045_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33493_GM;
MethodInfo m33493_MI = 
{
	"get_Current", NULL, &t4949_TI, &t2045_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33493_GM};
static MethodInfo* t4949_MIs[] =
{
	&m33493_MI,
	NULL
};
static TypeInfo* t4949_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4949_0_0_0;
extern Il2CppType t4949_1_0_0;
struct t4949;
extern Il2CppGenericClass t4949_GC;
TypeInfo t4949_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4949_MIs, t4949_PIs, NULL, NULL, NULL, NULL, NULL, &t4949_TI, t4949_ITIs, NULL, &EmptyCustomAttributesCache, &t4949_TI, &t4949_0_0_0, &t4949_1_0_0, NULL, &t4949_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3427.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3427_TI;
#include "t3427MD.h"

extern TypeInfo t2045_TI;
extern MethodInfo m19009_MI;
extern MethodInfo m25644_MI;
struct t20;
#define m25644(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>
extern Il2CppType t20_0_0_1;
FieldInfo t3427_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3427_TI, offsetof(t3427, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3427_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3427_TI, offsetof(t3427, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3427_FIs[] =
{
	&t3427_f0_FieldInfo,
	&t3427_f1_FieldInfo,
	NULL
};
extern MethodInfo m19006_MI;
static PropertyInfo t3427____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3427_TI, "System.Collections.IEnumerator.Current", &m19006_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3427____Current_PropertyInfo = 
{
	&t3427_TI, "Current", &m19009_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3427_PIs[] =
{
	&t3427____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3427____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3427_m19005_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19005_GM;
MethodInfo m19005_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3427_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3427_m19005_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19005_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19006_GM;
MethodInfo m19006_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3427_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19006_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19007_GM;
MethodInfo m19007_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3427_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19007_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19008_GM;
MethodInfo m19008_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3427_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19008_GM};
extern Il2CppType t2045_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19009_GM;
MethodInfo m19009_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3427_TI, &t2045_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19009_GM};
static MethodInfo* t3427_MIs[] =
{
	&m19005_MI,
	&m19006_MI,
	&m19007_MI,
	&m19008_MI,
	&m19009_MI,
	NULL
};
extern MethodInfo m19008_MI;
extern MethodInfo m19007_MI;
static MethodInfo* t3427_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19006_MI,
	&m19008_MI,
	&m19007_MI,
	&m19009_MI,
};
static TypeInfo* t3427_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4949_TI,
};
static Il2CppInterfaceOffsetPair t3427_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4949_TI, 7},
};
extern TypeInfo t2045_TI;
static Il2CppRGCTXData t3427_RGCTXData[3] = 
{
	&m19009_MI/* Method Usage */,
	&t2045_TI/* Class Usage */,
	&m25644_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3427_0_0_0;
extern Il2CppType t3427_1_0_0;
extern Il2CppGenericClass t3427_GC;
TypeInfo t3427_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3427_MIs, t3427_PIs, t3427_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3427_TI, t3427_ITIs, t3427_VT, &EmptyCustomAttributesCache, &t3427_TI, &t3427_0_0_0, &t3427_1_0_0, t3427_IOs, &t3427_GC, NULL, NULL, NULL, t3427_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3427)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6469_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContributeServerContextSink>
extern MethodInfo m33494_MI;
extern MethodInfo m33495_MI;
static PropertyInfo t6469____Item_PropertyInfo = 
{
	&t6469_TI, "Item", &m33494_MI, &m33495_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6469_PIs[] =
{
	&t6469____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2045_0_0_0;
static ParameterInfo t6469_m33496_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2045_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33496_GM;
MethodInfo m33496_MI = 
{
	"IndexOf", NULL, &t6469_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6469_m33496_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33496_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2045_0_0_0;
static ParameterInfo t6469_m33497_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2045_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33497_GM;
MethodInfo m33497_MI = 
{
	"Insert", NULL, &t6469_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6469_m33497_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33497_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6469_m33498_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33498_GM;
MethodInfo m33498_MI = 
{
	"RemoveAt", NULL, &t6469_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6469_m33498_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33498_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6469_m33494_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2045_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33494_GM;
MethodInfo m33494_MI = 
{
	"get_Item", NULL, &t6469_TI, &t2045_0_0_0, RuntimeInvoker_t29_t44, t6469_m33494_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33494_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2045_0_0_0;
static ParameterInfo t6469_m33495_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2045_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33495_GM;
MethodInfo m33495_MI = 
{
	"set_Item", NULL, &t6469_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6469_m33495_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33495_GM};
static MethodInfo* t6469_MIs[] =
{
	&m33496_MI,
	&m33497_MI,
	&m33498_MI,
	&m33494_MI,
	&m33495_MI,
	NULL
};
static TypeInfo* t6469_ITIs[] = 
{
	&t603_TI,
	&t6468_TI,
	&t6470_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6469_0_0_0;
extern Il2CppType t6469_1_0_0;
struct t6469;
extern Il2CppGenericClass t6469_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6469_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6469_MIs, t6469_PIs, NULL, NULL, NULL, NULL, NULL, &t6469_TI, t6469_ITIs, NULL, &t1908__CustomAttributeCache, &t6469_TI, &t6469_0_0_0, &t6469_1_0_0, NULL, &t6469_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4951_TI;

#include "t1437.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>
extern MethodInfo m33499_MI;
static PropertyInfo t4951____Current_PropertyInfo = 
{
	&t4951_TI, "Current", &m33499_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4951_PIs[] =
{
	&t4951____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1437_0_0_0;
extern void* RuntimeInvoker_t1437 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33499_GM;
MethodInfo m33499_MI = 
{
	"get_Current", NULL, &t4951_TI, &t1437_0_0_0, RuntimeInvoker_t1437, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33499_GM};
static MethodInfo* t4951_MIs[] =
{
	&m33499_MI,
	NULL
};
static TypeInfo* t4951_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4951_0_0_0;
extern Il2CppType t4951_1_0_0;
struct t4951;
extern Il2CppGenericClass t4951_GC;
TypeInfo t4951_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4951_MIs, t4951_PIs, NULL, NULL, NULL, NULL, NULL, &t4951_TI, t4951_ITIs, NULL, &EmptyCustomAttributesCache, &t4951_TI, &t4951_0_0_0, &t4951_1_0_0, NULL, &t4951_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3428.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3428_TI;
#include "t3428MD.h"

extern TypeInfo t1437_TI;
extern MethodInfo m19014_MI;
extern MethodInfo m25655_MI;
struct t20;
 uint8_t m25655 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19010_MI;
 void m19010 (t3428 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19011_MI;
 t29 * m19011 (t3428 * __this, MethodInfo* method){
	{
		uint8_t L_0 = m19014(__this, &m19014_MI);
		uint8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1437_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19012_MI;
 void m19012 (t3428 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19013_MI;
 bool m19013 (t3428 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint8_t m19014 (t3428 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint8_t L_8 = m25655(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25655_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.ArgInfoType>
extern Il2CppType t20_0_0_1;
FieldInfo t3428_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3428_TI, offsetof(t3428, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3428_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3428_TI, offsetof(t3428, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3428_FIs[] =
{
	&t3428_f0_FieldInfo,
	&t3428_f1_FieldInfo,
	NULL
};
static PropertyInfo t3428____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3428_TI, "System.Collections.IEnumerator.Current", &m19011_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3428____Current_PropertyInfo = 
{
	&t3428_TI, "Current", &m19014_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3428_PIs[] =
{
	&t3428____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3428____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3428_m19010_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19010_GM;
MethodInfo m19010_MI = 
{
	".ctor", (methodPointerType)&m19010, &t3428_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3428_m19010_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19010_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19011_GM;
MethodInfo m19011_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19011, &t3428_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19011_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19012_GM;
MethodInfo m19012_MI = 
{
	"Dispose", (methodPointerType)&m19012, &t3428_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19012_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19013_GM;
MethodInfo m19013_MI = 
{
	"MoveNext", (methodPointerType)&m19013, &t3428_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19013_GM};
extern Il2CppType t1437_0_0_0;
extern void* RuntimeInvoker_t1437 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19014_GM;
MethodInfo m19014_MI = 
{
	"get_Current", (methodPointerType)&m19014, &t3428_TI, &t1437_0_0_0, RuntimeInvoker_t1437, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19014_GM};
static MethodInfo* t3428_MIs[] =
{
	&m19010_MI,
	&m19011_MI,
	&m19012_MI,
	&m19013_MI,
	&m19014_MI,
	NULL
};
static MethodInfo* t3428_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19011_MI,
	&m19013_MI,
	&m19012_MI,
	&m19014_MI,
};
static TypeInfo* t3428_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4951_TI,
};
static Il2CppInterfaceOffsetPair t3428_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4951_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3428_0_0_0;
extern Il2CppType t3428_1_0_0;
extern Il2CppGenericClass t3428_GC;
TypeInfo t3428_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3428_MIs, t3428_PIs, t3428_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3428_TI, t3428_ITIs, t3428_VT, &EmptyCustomAttributesCache, &t3428_TI, &t3428_0_0_0, &t3428_1_0_0, t3428_IOs, &t3428_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3428)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6471_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.ArgInfoType>
extern MethodInfo m33500_MI;
static PropertyInfo t6471____Count_PropertyInfo = 
{
	&t6471_TI, "Count", &m33500_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33501_MI;
static PropertyInfo t6471____IsReadOnly_PropertyInfo = 
{
	&t6471_TI, "IsReadOnly", &m33501_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6471_PIs[] =
{
	&t6471____Count_PropertyInfo,
	&t6471____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33500_GM;
MethodInfo m33500_MI = 
{
	"get_Count", NULL, &t6471_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33500_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33501_GM;
MethodInfo m33501_MI = 
{
	"get_IsReadOnly", NULL, &t6471_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33501_GM};
extern Il2CppType t1437_0_0_0;
extern Il2CppType t1437_0_0_0;
static ParameterInfo t6471_m33502_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33502_GM;
MethodInfo m33502_MI = 
{
	"Add", NULL, &t6471_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t6471_m33502_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33502_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33503_GM;
MethodInfo m33503_MI = 
{
	"Clear", NULL, &t6471_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33503_GM};
extern Il2CppType t1437_0_0_0;
static ParameterInfo t6471_m33504_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33504_GM;
MethodInfo m33504_MI = 
{
	"Contains", NULL, &t6471_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6471_m33504_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33504_GM};
extern Il2CppType t3682_0_0_0;
extern Il2CppType t3682_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6471_m33505_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3682_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33505_GM;
MethodInfo m33505_MI = 
{
	"CopyTo", NULL, &t6471_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6471_m33505_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33505_GM};
extern Il2CppType t1437_0_0_0;
static ParameterInfo t6471_m33506_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33506_GM;
MethodInfo m33506_MI = 
{
	"Remove", NULL, &t6471_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6471_m33506_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33506_GM};
static MethodInfo* t6471_MIs[] =
{
	&m33500_MI,
	&m33501_MI,
	&m33502_MI,
	&m33503_MI,
	&m33504_MI,
	&m33505_MI,
	&m33506_MI,
	NULL
};
extern TypeInfo t6473_TI;
static TypeInfo* t6471_ITIs[] = 
{
	&t603_TI,
	&t6473_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6471_0_0_0;
extern Il2CppType t6471_1_0_0;
struct t6471;
extern Il2CppGenericClass t6471_GC;
TypeInfo t6471_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6471_MIs, t6471_PIs, NULL, NULL, NULL, NULL, NULL, &t6471_TI, t6471_ITIs, NULL, &EmptyCustomAttributesCache, &t6471_TI, &t6471_0_0_0, &t6471_1_0_0, NULL, &t6471_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Messaging.ArgInfoType>
extern Il2CppType t4951_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33507_GM;
MethodInfo m33507_MI = 
{
	"GetEnumerator", NULL, &t6473_TI, &t4951_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33507_GM};
static MethodInfo* t6473_MIs[] =
{
	&m33507_MI,
	NULL
};
static TypeInfo* t6473_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6473_0_0_0;
extern Il2CppType t6473_1_0_0;
struct t6473;
extern Il2CppGenericClass t6473_GC;
TypeInfo t6473_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6473_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6473_TI, t6473_ITIs, NULL, &EmptyCustomAttributesCache, &t6473_TI, &t6473_0_0_0, &t6473_1_0_0, NULL, &t6473_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6472_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.ArgInfoType>
extern MethodInfo m33508_MI;
extern MethodInfo m33509_MI;
static PropertyInfo t6472____Item_PropertyInfo = 
{
	&t6472_TI, "Item", &m33508_MI, &m33509_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6472_PIs[] =
{
	&t6472____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1437_0_0_0;
static ParameterInfo t6472_m33510_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33510_GM;
MethodInfo m33510_MI = 
{
	"IndexOf", NULL, &t6472_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t6472_m33510_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33510_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1437_0_0_0;
static ParameterInfo t6472_m33511_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33511_GM;
MethodInfo m33511_MI = 
{
	"Insert", NULL, &t6472_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6472_m33511_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33511_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6472_m33512_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33512_GM;
MethodInfo m33512_MI = 
{
	"RemoveAt", NULL, &t6472_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6472_m33512_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33512_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6472_m33508_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1437_0_0_0;
extern void* RuntimeInvoker_t1437_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33508_GM;
MethodInfo m33508_MI = 
{
	"get_Item", NULL, &t6472_TI, &t1437_0_0_0, RuntimeInvoker_t1437_t44, t6472_m33508_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33508_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1437_0_0_0;
static ParameterInfo t6472_m33509_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33509_GM;
MethodInfo m33509_MI = 
{
	"set_Item", NULL, &t6472_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6472_m33509_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33509_GM};
static MethodInfo* t6472_MIs[] =
{
	&m33510_MI,
	&m33511_MI,
	&m33512_MI,
	&m33508_MI,
	&m33509_MI,
	NULL
};
static TypeInfo* t6472_ITIs[] = 
{
	&t603_TI,
	&t6471_TI,
	&t6473_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6472_0_0_0;
extern Il2CppType t6472_1_0_0;
struct t6472;
extern Il2CppGenericClass t6472_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6472_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6472_MIs, t6472_PIs, NULL, NULL, NULL, NULL, NULL, &t6472_TI, t6472_ITIs, NULL, &t1908__CustomAttributeCache, &t6472_TI, &t6472_0_0_0, &t6472_1_0_0, NULL, &t6472_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4953_TI;

#include "t1448.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Messaging.Header>
extern MethodInfo m33513_MI;
static PropertyInfo t4953____Current_PropertyInfo = 
{
	&t4953_TI, "Current", &m33513_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4953_PIs[] =
{
	&t4953____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1448_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33513_GM;
MethodInfo m33513_MI = 
{
	"get_Current", NULL, &t4953_TI, &t1448_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33513_GM};
static MethodInfo* t4953_MIs[] =
{
	&m33513_MI,
	NULL
};
static TypeInfo* t4953_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4953_0_0_0;
extern Il2CppType t4953_1_0_0;
struct t4953;
extern Il2CppGenericClass t4953_GC;
TypeInfo t4953_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4953_MIs, t4953_PIs, NULL, NULL, NULL, NULL, NULL, &t4953_TI, t4953_ITIs, NULL, &EmptyCustomAttributesCache, &t4953_TI, &t4953_0_0_0, &t4953_1_0_0, NULL, &t4953_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3429.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3429_TI;
#include "t3429MD.h"

extern TypeInfo t1448_TI;
extern MethodInfo m19019_MI;
extern MethodInfo m25666_MI;
struct t20;
#define m25666(__this, p0, method) (t1448 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Messaging.Header>
extern Il2CppType t20_0_0_1;
FieldInfo t3429_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3429_TI, offsetof(t3429, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3429_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3429_TI, offsetof(t3429, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3429_FIs[] =
{
	&t3429_f0_FieldInfo,
	&t3429_f1_FieldInfo,
	NULL
};
extern MethodInfo m19016_MI;
static PropertyInfo t3429____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3429_TI, "System.Collections.IEnumerator.Current", &m19016_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3429____Current_PropertyInfo = 
{
	&t3429_TI, "Current", &m19019_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3429_PIs[] =
{
	&t3429____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3429____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3429_m19015_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19015_GM;
MethodInfo m19015_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3429_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3429_m19015_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19015_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19016_GM;
MethodInfo m19016_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3429_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19016_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19017_GM;
MethodInfo m19017_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3429_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19017_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19018_GM;
MethodInfo m19018_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3429_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19018_GM};
extern Il2CppType t1448_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19019_GM;
MethodInfo m19019_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3429_TI, &t1448_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19019_GM};
static MethodInfo* t3429_MIs[] =
{
	&m19015_MI,
	&m19016_MI,
	&m19017_MI,
	&m19018_MI,
	&m19019_MI,
	NULL
};
extern MethodInfo m19018_MI;
extern MethodInfo m19017_MI;
static MethodInfo* t3429_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19016_MI,
	&m19018_MI,
	&m19017_MI,
	&m19019_MI,
};
static TypeInfo* t3429_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4953_TI,
};
static Il2CppInterfaceOffsetPair t3429_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4953_TI, 7},
};
extern TypeInfo t1448_TI;
static Il2CppRGCTXData t3429_RGCTXData[3] = 
{
	&m19019_MI/* Method Usage */,
	&t1448_TI/* Class Usage */,
	&m25666_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3429_0_0_0;
extern Il2CppType t3429_1_0_0;
extern Il2CppGenericClass t3429_GC;
TypeInfo t3429_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3429_MIs, t3429_PIs, t3429_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3429_TI, t3429_ITIs, t3429_VT, &EmptyCustomAttributesCache, &t3429_TI, &t3429_0_0_0, &t3429_1_0_0, t3429_IOs, &t3429_GC, NULL, NULL, NULL, t3429_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3429)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6474_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Messaging.Header>
extern MethodInfo m33514_MI;
static PropertyInfo t6474____Count_PropertyInfo = 
{
	&t6474_TI, "Count", &m33514_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33515_MI;
static PropertyInfo t6474____IsReadOnly_PropertyInfo = 
{
	&t6474_TI, "IsReadOnly", &m33515_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6474_PIs[] =
{
	&t6474____Count_PropertyInfo,
	&t6474____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33514_GM;
MethodInfo m33514_MI = 
{
	"get_Count", NULL, &t6474_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33514_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33515_GM;
MethodInfo m33515_MI = 
{
	"get_IsReadOnly", NULL, &t6474_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33515_GM};
extern Il2CppType t1448_0_0_0;
extern Il2CppType t1448_0_0_0;
static ParameterInfo t6474_m33516_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1448_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33516_GM;
MethodInfo m33516_MI = 
{
	"Add", NULL, &t6474_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6474_m33516_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33516_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33517_GM;
MethodInfo m33517_MI = 
{
	"Clear", NULL, &t6474_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33517_GM};
extern Il2CppType t1448_0_0_0;
static ParameterInfo t6474_m33518_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1448_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33518_GM;
MethodInfo m33518_MI = 
{
	"Contains", NULL, &t6474_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6474_m33518_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33518_GM};
extern Il2CppType t1451_0_0_0;
extern Il2CppType t1451_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6474_m33519_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1451_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33519_GM;
MethodInfo m33519_MI = 
{
	"CopyTo", NULL, &t6474_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6474_m33519_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33519_GM};
extern Il2CppType t1448_0_0_0;
static ParameterInfo t6474_m33520_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1448_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33520_GM;
MethodInfo m33520_MI = 
{
	"Remove", NULL, &t6474_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6474_m33520_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33520_GM};
static MethodInfo* t6474_MIs[] =
{
	&m33514_MI,
	&m33515_MI,
	&m33516_MI,
	&m33517_MI,
	&m33518_MI,
	&m33519_MI,
	&m33520_MI,
	NULL
};
extern TypeInfo t6476_TI;
static TypeInfo* t6474_ITIs[] = 
{
	&t603_TI,
	&t6476_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6474_0_0_0;
extern Il2CppType t6474_1_0_0;
struct t6474;
extern Il2CppGenericClass t6474_GC;
TypeInfo t6474_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6474_MIs, t6474_PIs, NULL, NULL, NULL, NULL, NULL, &t6474_TI, t6474_ITIs, NULL, &EmptyCustomAttributesCache, &t6474_TI, &t6474_0_0_0, &t6474_1_0_0, NULL, &t6474_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Messaging.Header>
extern Il2CppType t4953_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33521_GM;
MethodInfo m33521_MI = 
{
	"GetEnumerator", NULL, &t6476_TI, &t4953_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33521_GM};
static MethodInfo* t6476_MIs[] =
{
	&m33521_MI,
	NULL
};
static TypeInfo* t6476_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6476_0_0_0;
extern Il2CppType t6476_1_0_0;
struct t6476;
extern Il2CppGenericClass t6476_GC;
TypeInfo t6476_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6476_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6476_TI, t6476_ITIs, NULL, &EmptyCustomAttributesCache, &t6476_TI, &t6476_0_0_0, &t6476_1_0_0, NULL, &t6476_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6475_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Messaging.Header>
extern MethodInfo m33522_MI;
extern MethodInfo m33523_MI;
static PropertyInfo t6475____Item_PropertyInfo = 
{
	&t6475_TI, "Item", &m33522_MI, &m33523_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6475_PIs[] =
{
	&t6475____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1448_0_0_0;
static ParameterInfo t6475_m33524_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1448_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33524_GM;
MethodInfo m33524_MI = 
{
	"IndexOf", NULL, &t6475_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6475_m33524_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33524_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1448_0_0_0;
static ParameterInfo t6475_m33525_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1448_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33525_GM;
MethodInfo m33525_MI = 
{
	"Insert", NULL, &t6475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6475_m33525_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33525_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6475_m33526_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33526_GM;
MethodInfo m33526_MI = 
{
	"RemoveAt", NULL, &t6475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6475_m33526_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33526_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6475_m33522_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1448_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33522_GM;
MethodInfo m33522_MI = 
{
	"get_Item", NULL, &t6475_TI, &t1448_0_0_0, RuntimeInvoker_t29_t44, t6475_m33522_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33522_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1448_0_0_0;
static ParameterInfo t6475_m33523_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1448_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33523_GM;
MethodInfo m33523_MI = 
{
	"set_Item", NULL, &t6475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6475_m33523_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33523_GM};
static MethodInfo* t6475_MIs[] =
{
	&m33524_MI,
	&m33525_MI,
	&m33526_MI,
	&m33522_MI,
	&m33523_MI,
	NULL
};
static TypeInfo* t6475_ITIs[] = 
{
	&t603_TI,
	&t6474_TI,
	&t6476_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6475_0_0_0;
extern Il2CppType t6475_1_0_0;
struct t6475;
extern Il2CppGenericClass t6475_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6475_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6475_MIs, t6475_PIs, NULL, NULL, NULL, NULL, NULL, &t6475_TI, t6475_ITIs, NULL, &t1908__CustomAttributeCache, &t6475_TI, &t6475_0_0_0, &t6475_1_0_0, NULL, &t6475_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4955_TI;

#include "t1464.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>
extern MethodInfo m33527_MI;
static PropertyInfo t4955____Current_PropertyInfo = 
{
	&t4955_TI, "Current", &m33527_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4955_PIs[] =
{
	&t4955____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1464_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33527_GM;
MethodInfo m33527_MI = 
{
	"get_Current", NULL, &t4955_TI, &t1464_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33527_GM};
static MethodInfo* t4955_MIs[] =
{
	&m33527_MI,
	NULL
};
static TypeInfo* t4955_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4955_0_0_0;
extern Il2CppType t4955_1_0_0;
struct t4955;
extern Il2CppGenericClass t4955_GC;
TypeInfo t4955_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4955_MIs, t4955_PIs, NULL, NULL, NULL, NULL, NULL, &t4955_TI, t4955_ITIs, NULL, &EmptyCustomAttributesCache, &t4955_TI, &t4955_0_0_0, &t4955_1_0_0, NULL, &t4955_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3430.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3430_TI;
#include "t3430MD.h"

extern TypeInfo t1464_TI;
extern MethodInfo m19024_MI;
extern MethodInfo m25677_MI;
struct t20;
#define m25677(__this, p0, method) (t1464 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Proxies.ProxyAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3430_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3430_TI, offsetof(t3430, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3430_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3430_TI, offsetof(t3430, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3430_FIs[] =
{
	&t3430_f0_FieldInfo,
	&t3430_f1_FieldInfo,
	NULL
};
extern MethodInfo m19021_MI;
static PropertyInfo t3430____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3430_TI, "System.Collections.IEnumerator.Current", &m19021_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3430____Current_PropertyInfo = 
{
	&t3430_TI, "Current", &m19024_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3430_PIs[] =
{
	&t3430____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3430____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3430_m19020_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19020_GM;
MethodInfo m19020_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3430_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3430_m19020_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19020_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19021_GM;
MethodInfo m19021_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3430_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19021_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19022_GM;
MethodInfo m19022_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3430_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19022_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19023_GM;
MethodInfo m19023_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3430_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19023_GM};
extern Il2CppType t1464_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19024_GM;
MethodInfo m19024_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3430_TI, &t1464_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19024_GM};
static MethodInfo* t3430_MIs[] =
{
	&m19020_MI,
	&m19021_MI,
	&m19022_MI,
	&m19023_MI,
	&m19024_MI,
	NULL
};
extern MethodInfo m19023_MI;
extern MethodInfo m19022_MI;
static MethodInfo* t3430_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19021_MI,
	&m19023_MI,
	&m19022_MI,
	&m19024_MI,
};
static TypeInfo* t3430_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4955_TI,
};
static Il2CppInterfaceOffsetPair t3430_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4955_TI, 7},
};
extern TypeInfo t1464_TI;
static Il2CppRGCTXData t3430_RGCTXData[3] = 
{
	&m19024_MI/* Method Usage */,
	&t1464_TI/* Class Usage */,
	&m25677_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3430_0_0_0;
extern Il2CppType t3430_1_0_0;
extern Il2CppGenericClass t3430_GC;
TypeInfo t3430_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3430_MIs, t3430_PIs, t3430_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3430_TI, t3430_ITIs, t3430_VT, &EmptyCustomAttributesCache, &t3430_TI, &t3430_0_0_0, &t3430_1_0_0, t3430_IOs, &t3430_GC, NULL, NULL, NULL, t3430_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3430)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6477_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Proxies.ProxyAttribute>
extern MethodInfo m33528_MI;
static PropertyInfo t6477____Count_PropertyInfo = 
{
	&t6477_TI, "Count", &m33528_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33529_MI;
static PropertyInfo t6477____IsReadOnly_PropertyInfo = 
{
	&t6477_TI, "IsReadOnly", &m33529_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6477_PIs[] =
{
	&t6477____Count_PropertyInfo,
	&t6477____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33528_GM;
MethodInfo m33528_MI = 
{
	"get_Count", NULL, &t6477_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33528_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33529_GM;
MethodInfo m33529_MI = 
{
	"get_IsReadOnly", NULL, &t6477_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33529_GM};
extern Il2CppType t1464_0_0_0;
extern Il2CppType t1464_0_0_0;
static ParameterInfo t6477_m33530_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1464_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33530_GM;
MethodInfo m33530_MI = 
{
	"Add", NULL, &t6477_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6477_m33530_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33530_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33531_GM;
MethodInfo m33531_MI = 
{
	"Clear", NULL, &t6477_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33531_GM};
extern Il2CppType t1464_0_0_0;
static ParameterInfo t6477_m33532_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1464_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33532_GM;
MethodInfo m33532_MI = 
{
	"Contains", NULL, &t6477_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6477_m33532_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33532_GM};
extern Il2CppType t3683_0_0_0;
extern Il2CppType t3683_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6477_m33533_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3683_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33533_GM;
MethodInfo m33533_MI = 
{
	"CopyTo", NULL, &t6477_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6477_m33533_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33533_GM};
extern Il2CppType t1464_0_0_0;
static ParameterInfo t6477_m33534_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1464_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33534_GM;
MethodInfo m33534_MI = 
{
	"Remove", NULL, &t6477_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6477_m33534_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33534_GM};
static MethodInfo* t6477_MIs[] =
{
	&m33528_MI,
	&m33529_MI,
	&m33530_MI,
	&m33531_MI,
	&m33532_MI,
	&m33533_MI,
	&m33534_MI,
	NULL
};
extern TypeInfo t6479_TI;
static TypeInfo* t6477_ITIs[] = 
{
	&t603_TI,
	&t6479_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6477_0_0_0;
extern Il2CppType t6477_1_0_0;
struct t6477;
extern Il2CppGenericClass t6477_GC;
TypeInfo t6477_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6477_MIs, t6477_PIs, NULL, NULL, NULL, NULL, NULL, &t6477_TI, t6477_ITIs, NULL, &EmptyCustomAttributesCache, &t6477_TI, &t6477_0_0_0, &t6477_1_0_0, NULL, &t6477_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Proxies.ProxyAttribute>
extern Il2CppType t4955_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33535_GM;
MethodInfo m33535_MI = 
{
	"GetEnumerator", NULL, &t6479_TI, &t4955_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33535_GM};
static MethodInfo* t6479_MIs[] =
{
	&m33535_MI,
	NULL
};
static TypeInfo* t6479_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6479_0_0_0;
extern Il2CppType t6479_1_0_0;
struct t6479;
extern Il2CppGenericClass t6479_GC;
TypeInfo t6479_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6479_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6479_TI, t6479_ITIs, NULL, &EmptyCustomAttributesCache, &t6479_TI, &t6479_0_0_0, &t6479_1_0_0, NULL, &t6479_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6478_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Proxies.ProxyAttribute>
extern MethodInfo m33536_MI;
extern MethodInfo m33537_MI;
static PropertyInfo t6478____Item_PropertyInfo = 
{
	&t6478_TI, "Item", &m33536_MI, &m33537_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6478_PIs[] =
{
	&t6478____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1464_0_0_0;
static ParameterInfo t6478_m33538_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1464_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33538_GM;
MethodInfo m33538_MI = 
{
	"IndexOf", NULL, &t6478_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6478_m33538_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33538_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1464_0_0_0;
static ParameterInfo t6478_m33539_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1464_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33539_GM;
MethodInfo m33539_MI = 
{
	"Insert", NULL, &t6478_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6478_m33539_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33539_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6478_m33540_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33540_GM;
MethodInfo m33540_MI = 
{
	"RemoveAt", NULL, &t6478_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6478_m33540_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33540_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6478_m33536_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1464_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33536_GM;
MethodInfo m33536_MI = 
{
	"get_Item", NULL, &t6478_TI, &t1464_0_0_0, RuntimeInvoker_t29_t44, t6478_m33536_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33536_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1464_0_0_0;
static ParameterInfo t6478_m33537_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1464_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33537_GM;
MethodInfo m33537_MI = 
{
	"set_Item", NULL, &t6478_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6478_m33537_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33537_GM};
static MethodInfo* t6478_MIs[] =
{
	&m33538_MI,
	&m33539_MI,
	&m33540_MI,
	&m33536_MI,
	&m33537_MI,
	NULL
};
static TypeInfo* t6478_ITIs[] = 
{
	&t603_TI,
	&t6477_TI,
	&t6479_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6478_0_0_0;
extern Il2CppType t6478_1_0_0;
struct t6478;
extern Il2CppGenericClass t6478_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6478_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6478_MIs, t6478_PIs, NULL, NULL, NULL, NULL, NULL, &t6478_TI, t6478_ITIs, NULL, &t1908__CustomAttributeCache, &t6478_TI, &t6478_0_0_0, &t6478_1_0_0, NULL, &t6478_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4957_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>
extern MethodInfo m33541_MI;
static PropertyInfo t4957____Current_PropertyInfo = 
{
	&t4957_TI, "Current", &m33541_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4957_PIs[] =
{
	&t4957____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2051_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33541_GM;
MethodInfo m33541_MI = 
{
	"get_Current", NULL, &t4957_TI, &t2051_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33541_GM};
static MethodInfo* t4957_MIs[] =
{
	&m33541_MI,
	NULL
};
static TypeInfo* t4957_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4957_0_0_0;
extern Il2CppType t4957_1_0_0;
struct t4957;
extern Il2CppGenericClass t4957_GC;
TypeInfo t4957_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4957_MIs, t4957_PIs, NULL, NULL, NULL, NULL, NULL, &t4957_TI, t4957_ITIs, NULL, &EmptyCustomAttributesCache, &t4957_TI, &t4957_0_0_0, &t4957_1_0_0, NULL, &t4957_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3431.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3431_TI;
#include "t3431MD.h"

extern TypeInfo t2051_TI;
extern MethodInfo m19029_MI;
extern MethodInfo m25688_MI;
struct t20;
#define m25688(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t3431_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3431_TI, offsetof(t3431, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3431_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3431_TI, offsetof(t3431, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3431_FIs[] =
{
	&t3431_f0_FieldInfo,
	&t3431_f1_FieldInfo,
	NULL
};
extern MethodInfo m19026_MI;
static PropertyInfo t3431____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3431_TI, "System.Collections.IEnumerator.Current", &m19026_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3431____Current_PropertyInfo = 
{
	&t3431_TI, "Current", &m19029_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3431_PIs[] =
{
	&t3431____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3431____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3431_m19025_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19025_GM;
MethodInfo m19025_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3431_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3431_m19025_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19025_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19026_GM;
MethodInfo m19026_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3431_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19026_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19027_GM;
MethodInfo m19027_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3431_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19027_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19028_GM;
MethodInfo m19028_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3431_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19028_GM};
extern Il2CppType t2051_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19029_GM;
MethodInfo m19029_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3431_TI, &t2051_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19029_GM};
static MethodInfo* t3431_MIs[] =
{
	&m19025_MI,
	&m19026_MI,
	&m19027_MI,
	&m19028_MI,
	&m19029_MI,
	NULL
};
extern MethodInfo m19028_MI;
extern MethodInfo m19027_MI;
static MethodInfo* t3431_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19026_MI,
	&m19028_MI,
	&m19027_MI,
	&m19029_MI,
};
static TypeInfo* t3431_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4957_TI,
};
static Il2CppInterfaceOffsetPair t3431_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4957_TI, 7},
};
extern TypeInfo t2051_TI;
static Il2CppRGCTXData t3431_RGCTXData[3] = 
{
	&m19029_MI/* Method Usage */,
	&t2051_TI/* Class Usage */,
	&m25688_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3431_0_0_0;
extern Il2CppType t3431_1_0_0;
extern Il2CppGenericClass t3431_GC;
TypeInfo t3431_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3431_MIs, t3431_PIs, t3431_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3431_TI, t3431_ITIs, t3431_VT, &EmptyCustomAttributesCache, &t3431_TI, &t3431_0_0_0, &t3431_1_0_0, t3431_IOs, &t3431_GC, NULL, NULL, NULL, t3431_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3431)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6480_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Services.ITrackingHandler>
extern MethodInfo m33542_MI;
static PropertyInfo t6480____Count_PropertyInfo = 
{
	&t6480_TI, "Count", &m33542_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33543_MI;
static PropertyInfo t6480____IsReadOnly_PropertyInfo = 
{
	&t6480_TI, "IsReadOnly", &m33543_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6480_PIs[] =
{
	&t6480____Count_PropertyInfo,
	&t6480____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33542_GM;
MethodInfo m33542_MI = 
{
	"get_Count", NULL, &t6480_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33542_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33543_GM;
MethodInfo m33543_MI = 
{
	"get_IsReadOnly", NULL, &t6480_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33543_GM};
extern Il2CppType t2051_0_0_0;
extern Il2CppType t2051_0_0_0;
static ParameterInfo t6480_m33544_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2051_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33544_GM;
MethodInfo m33544_MI = 
{
	"Add", NULL, &t6480_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6480_m33544_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33544_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33545_GM;
MethodInfo m33545_MI = 
{
	"Clear", NULL, &t6480_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33545_GM};
extern Il2CppType t2051_0_0_0;
static ParameterInfo t6480_m33546_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2051_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33546_GM;
MethodInfo m33546_MI = 
{
	"Contains", NULL, &t6480_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6480_m33546_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33546_GM};
extern Il2CppType t2052_0_0_0;
extern Il2CppType t2052_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6480_m33547_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2052_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33547_GM;
MethodInfo m33547_MI = 
{
	"CopyTo", NULL, &t6480_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6480_m33547_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33547_GM};
extern Il2CppType t2051_0_0_0;
static ParameterInfo t6480_m33548_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2051_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33548_GM;
MethodInfo m33548_MI = 
{
	"Remove", NULL, &t6480_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6480_m33548_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33548_GM};
static MethodInfo* t6480_MIs[] =
{
	&m33542_MI,
	&m33543_MI,
	&m33544_MI,
	&m33545_MI,
	&m33546_MI,
	&m33547_MI,
	&m33548_MI,
	NULL
};
extern TypeInfo t6482_TI;
static TypeInfo* t6480_ITIs[] = 
{
	&t603_TI,
	&t6482_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6480_0_0_0;
extern Il2CppType t6480_1_0_0;
struct t6480;
extern Il2CppGenericClass t6480_GC;
TypeInfo t6480_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6480_MIs, t6480_PIs, NULL, NULL, NULL, NULL, NULL, &t6480_TI, t6480_ITIs, NULL, &EmptyCustomAttributesCache, &t6480_TI, &t6480_0_0_0, &t6480_1_0_0, NULL, &t6480_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Services.ITrackingHandler>
extern Il2CppType t4957_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33549_GM;
MethodInfo m33549_MI = 
{
	"GetEnumerator", NULL, &t6482_TI, &t4957_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33549_GM};
static MethodInfo* t6482_MIs[] =
{
	&m33549_MI,
	NULL
};
static TypeInfo* t6482_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6482_0_0_0;
extern Il2CppType t6482_1_0_0;
struct t6482;
extern Il2CppGenericClass t6482_GC;
TypeInfo t6482_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6482_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6482_TI, t6482_ITIs, NULL, &EmptyCustomAttributesCache, &t6482_TI, &t6482_0_0_0, &t6482_1_0_0, NULL, &t6482_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6481_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Services.ITrackingHandler>
extern MethodInfo m33550_MI;
extern MethodInfo m33551_MI;
static PropertyInfo t6481____Item_PropertyInfo = 
{
	&t6481_TI, "Item", &m33550_MI, &m33551_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6481_PIs[] =
{
	&t6481____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2051_0_0_0;
static ParameterInfo t6481_m33552_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2051_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33552_GM;
MethodInfo m33552_MI = 
{
	"IndexOf", NULL, &t6481_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6481_m33552_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33552_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2051_0_0_0;
static ParameterInfo t6481_m33553_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2051_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33553_GM;
MethodInfo m33553_MI = 
{
	"Insert", NULL, &t6481_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6481_m33553_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33553_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6481_m33554_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33554_GM;
MethodInfo m33554_MI = 
{
	"RemoveAt", NULL, &t6481_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6481_m33554_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33554_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6481_m33550_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2051_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33550_GM;
MethodInfo m33550_MI = 
{
	"get_Item", NULL, &t6481_TI, &t2051_0_0_0, RuntimeInvoker_t29_t44, t6481_m33550_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33550_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2051_0_0_0;
static ParameterInfo t6481_m33551_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2051_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33551_GM;
MethodInfo m33551_MI = 
{
	"set_Item", NULL, &t6481_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6481_m33551_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33551_GM};
static MethodInfo* t6481_MIs[] =
{
	&m33552_MI,
	&m33553_MI,
	&m33554_MI,
	&m33550_MI,
	&m33551_MI,
	NULL
};
static TypeInfo* t6481_ITIs[] = 
{
	&t603_TI,
	&t6480_TI,
	&t6482_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6481_0_0_0;
extern Il2CppType t6481_1_0_0;
struct t6481;
extern Il2CppGenericClass t6481_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6481_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6481_MIs, t6481_PIs, NULL, NULL, NULL, NULL, NULL, &t6481_TI, t6481_ITIs, NULL, &t1908__CustomAttributeCache, &t6481_TI, &t6481_0_0_0, &t6481_1_0_0, NULL, &t6481_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4959_TI;

#include "t1484.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>
extern MethodInfo m33555_MI;
static PropertyInfo t4959____Current_PropertyInfo = 
{
	&t4959_TI, "Current", &m33555_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4959_PIs[] =
{
	&t4959____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1484_0_0_0;
extern void* RuntimeInvoker_t1484 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33555_GM;
MethodInfo m33555_MI = 
{
	"get_Current", NULL, &t4959_TI, &t1484_0_0_0, RuntimeInvoker_t1484, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33555_GM};
static MethodInfo* t4959_MIs[] =
{
	&m33555_MI,
	NULL
};
static TypeInfo* t4959_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4959_0_0_0;
extern Il2CppType t4959_1_0_0;
struct t4959;
extern Il2CppGenericClass t4959_GC;
TypeInfo t4959_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4959_MIs, t4959_PIs, NULL, NULL, NULL, NULL, NULL, &t4959_TI, t4959_ITIs, NULL, &EmptyCustomAttributesCache, &t4959_TI, &t4959_0_0_0, &t4959_1_0_0, NULL, &t4959_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3432.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3432_TI;
#include "t3432MD.h"

extern TypeInfo t1484_TI;
extern MethodInfo m19034_MI;
extern MethodInfo m25699_MI;
struct t20;
 int32_t m25699 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19030_MI;
 void m19030 (t3432 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19031_MI;
 t29 * m19031 (t3432 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19034(__this, &m19034_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1484_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19032_MI;
 void m19032 (t3432 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19033_MI;
 bool m19033 (t3432 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19034 (t3432 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25699(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25699_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.WellKnownObjectMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3432_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3432_TI, offsetof(t3432, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3432_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3432_TI, offsetof(t3432, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3432_FIs[] =
{
	&t3432_f0_FieldInfo,
	&t3432_f1_FieldInfo,
	NULL
};
static PropertyInfo t3432____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3432_TI, "System.Collections.IEnumerator.Current", &m19031_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3432____Current_PropertyInfo = 
{
	&t3432_TI, "Current", &m19034_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3432_PIs[] =
{
	&t3432____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3432____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3432_m19030_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19030_GM;
MethodInfo m19030_MI = 
{
	".ctor", (methodPointerType)&m19030, &t3432_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3432_m19030_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19030_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19031_GM;
MethodInfo m19031_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19031, &t3432_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19031_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19032_GM;
MethodInfo m19032_MI = 
{
	"Dispose", (methodPointerType)&m19032, &t3432_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19032_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19033_GM;
MethodInfo m19033_MI = 
{
	"MoveNext", (methodPointerType)&m19033, &t3432_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19033_GM};
extern Il2CppType t1484_0_0_0;
extern void* RuntimeInvoker_t1484 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19034_GM;
MethodInfo m19034_MI = 
{
	"get_Current", (methodPointerType)&m19034, &t3432_TI, &t1484_0_0_0, RuntimeInvoker_t1484, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19034_GM};
static MethodInfo* t3432_MIs[] =
{
	&m19030_MI,
	&m19031_MI,
	&m19032_MI,
	&m19033_MI,
	&m19034_MI,
	NULL
};
static MethodInfo* t3432_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19031_MI,
	&m19033_MI,
	&m19032_MI,
	&m19034_MI,
};
static TypeInfo* t3432_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4959_TI,
};
static Il2CppInterfaceOffsetPair t3432_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4959_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3432_0_0_0;
extern Il2CppType t3432_1_0_0;
extern Il2CppGenericClass t3432_GC;
TypeInfo t3432_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3432_MIs, t3432_PIs, t3432_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3432_TI, t3432_ITIs, t3432_VT, &EmptyCustomAttributesCache, &t3432_TI, &t3432_0_0_0, &t3432_1_0_0, t3432_IOs, &t3432_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3432)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6483_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.WellKnownObjectMode>
extern MethodInfo m33556_MI;
static PropertyInfo t6483____Count_PropertyInfo = 
{
	&t6483_TI, "Count", &m33556_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33557_MI;
static PropertyInfo t6483____IsReadOnly_PropertyInfo = 
{
	&t6483_TI, "IsReadOnly", &m33557_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6483_PIs[] =
{
	&t6483____Count_PropertyInfo,
	&t6483____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33556_GM;
MethodInfo m33556_MI = 
{
	"get_Count", NULL, &t6483_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33556_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33557_GM;
MethodInfo m33557_MI = 
{
	"get_IsReadOnly", NULL, &t6483_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33557_GM};
extern Il2CppType t1484_0_0_0;
extern Il2CppType t1484_0_0_0;
static ParameterInfo t6483_m33558_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33558_GM;
MethodInfo m33558_MI = 
{
	"Add", NULL, &t6483_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6483_m33558_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33558_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33559_GM;
MethodInfo m33559_MI = 
{
	"Clear", NULL, &t6483_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33559_GM};
extern Il2CppType t1484_0_0_0;
static ParameterInfo t6483_m33560_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33560_GM;
MethodInfo m33560_MI = 
{
	"Contains", NULL, &t6483_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6483_m33560_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33560_GM};
extern Il2CppType t3684_0_0_0;
extern Il2CppType t3684_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6483_m33561_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3684_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33561_GM;
MethodInfo m33561_MI = 
{
	"CopyTo", NULL, &t6483_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6483_m33561_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33561_GM};
extern Il2CppType t1484_0_0_0;
static ParameterInfo t6483_m33562_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33562_GM;
MethodInfo m33562_MI = 
{
	"Remove", NULL, &t6483_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6483_m33562_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33562_GM};
static MethodInfo* t6483_MIs[] =
{
	&m33556_MI,
	&m33557_MI,
	&m33558_MI,
	&m33559_MI,
	&m33560_MI,
	&m33561_MI,
	&m33562_MI,
	NULL
};
extern TypeInfo t6485_TI;
static TypeInfo* t6483_ITIs[] = 
{
	&t603_TI,
	&t6485_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6483_0_0_0;
extern Il2CppType t6483_1_0_0;
struct t6483;
extern Il2CppGenericClass t6483_GC;
TypeInfo t6483_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6483_MIs, t6483_PIs, NULL, NULL, NULL, NULL, NULL, &t6483_TI, t6483_ITIs, NULL, &EmptyCustomAttributesCache, &t6483_TI, &t6483_0_0_0, &t6483_1_0_0, NULL, &t6483_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.WellKnownObjectMode>
extern Il2CppType t4959_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33563_GM;
MethodInfo m33563_MI = 
{
	"GetEnumerator", NULL, &t6485_TI, &t4959_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33563_GM};
static MethodInfo* t6485_MIs[] =
{
	&m33563_MI,
	NULL
};
static TypeInfo* t6485_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6485_0_0_0;
extern Il2CppType t6485_1_0_0;
struct t6485;
extern Il2CppGenericClass t6485_GC;
TypeInfo t6485_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6485_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6485_TI, t6485_ITIs, NULL, &EmptyCustomAttributesCache, &t6485_TI, &t6485_0_0_0, &t6485_1_0_0, NULL, &t6485_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6484_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.WellKnownObjectMode>
extern MethodInfo m33564_MI;
extern MethodInfo m33565_MI;
static PropertyInfo t6484____Item_PropertyInfo = 
{
	&t6484_TI, "Item", &m33564_MI, &m33565_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6484_PIs[] =
{
	&t6484____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1484_0_0_0;
static ParameterInfo t6484_m33566_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33566_GM;
MethodInfo m33566_MI = 
{
	"IndexOf", NULL, &t6484_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6484_m33566_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33566_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1484_0_0_0;
static ParameterInfo t6484_m33567_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33567_GM;
MethodInfo m33567_MI = 
{
	"Insert", NULL, &t6484_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6484_m33567_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33567_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6484_m33568_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33568_GM;
MethodInfo m33568_MI = 
{
	"RemoveAt", NULL, &t6484_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6484_m33568_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33568_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6484_m33564_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1484_0_0_0;
extern void* RuntimeInvoker_t1484_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33564_GM;
MethodInfo m33564_MI = 
{
	"get_Item", NULL, &t6484_TI, &t1484_0_0_0, RuntimeInvoker_t1484_t44, t6484_m33564_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33564_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1484_0_0_0;
static ParameterInfo t6484_m33565_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33565_GM;
MethodInfo m33565_MI = 
{
	"set_Item", NULL, &t6484_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6484_m33565_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33565_GM};
static MethodInfo* t6484_MIs[] =
{
	&m33566_MI,
	&m33567_MI,
	&m33568_MI,
	&m33564_MI,
	&m33565_MI,
	NULL
};
static TypeInfo* t6484_ITIs[] = 
{
	&t603_TI,
	&t6483_TI,
	&t6485_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6484_0_0_0;
extern Il2CppType t6484_1_0_0;
struct t6484;
extern Il2CppGenericClass t6484_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6484_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6484_MIs, t6484_PIs, NULL, NULL, NULL, NULL, NULL, &t6484_TI, t6484_ITIs, NULL, &t1908__CustomAttributeCache, &t6484_TI, &t6484_0_0_0, &t6484_1_0_0, NULL, &t6484_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4961_TI;

#include "t1490.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>
extern MethodInfo m33569_MI;
static PropertyInfo t4961____Current_PropertyInfo = 
{
	&t4961_TI, "Current", &m33569_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4961_PIs[] =
{
	&t4961____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1490_0_0_0;
extern void* RuntimeInvoker_t1490 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33569_GM;
MethodInfo m33569_MI = 
{
	"get_Current", NULL, &t4961_TI, &t1490_0_0_0, RuntimeInvoker_t1490, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33569_GM};
static MethodInfo* t4961_MIs[] =
{
	&m33569_MI,
	NULL
};
static TypeInfo* t4961_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4961_0_0_0;
extern Il2CppType t4961_1_0_0;
struct t4961;
extern Il2CppGenericClass t4961_GC;
TypeInfo t4961_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4961_MIs, t4961_PIs, NULL, NULL, NULL, NULL, NULL, &t4961_TI, t4961_ITIs, NULL, &EmptyCustomAttributesCache, &t4961_TI, &t4961_0_0_0, &t4961_1_0_0, NULL, &t4961_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3433.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3433_TI;
#include "t3433MD.h"

extern TypeInfo t1490_TI;
extern MethodInfo m19039_MI;
extern MethodInfo m25710_MI;
struct t20;
 uint8_t m25710 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19035_MI;
 void m19035 (t3433 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19036_MI;
 t29 * m19036 (t3433 * __this, MethodInfo* method){
	{
		uint8_t L_0 = m19039(__this, &m19039_MI);
		uint8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1490_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19037_MI;
 void m19037 (t3433 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19038_MI;
 bool m19038 (t3433 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint8_t m19039 (t3433 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint8_t L_8 = m25710(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25710_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>
extern Il2CppType t20_0_0_1;
FieldInfo t3433_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3433_TI, offsetof(t3433, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3433_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3433_TI, offsetof(t3433, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3433_FIs[] =
{
	&t3433_f0_FieldInfo,
	&t3433_f1_FieldInfo,
	NULL
};
static PropertyInfo t3433____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3433_TI, "System.Collections.IEnumerator.Current", &m19036_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3433____Current_PropertyInfo = 
{
	&t3433_TI, "Current", &m19039_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3433_PIs[] =
{
	&t3433____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3433____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3433_m19035_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19035_GM;
MethodInfo m19035_MI = 
{
	".ctor", (methodPointerType)&m19035, &t3433_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3433_m19035_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19035_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19036_GM;
MethodInfo m19036_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19036, &t3433_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19036_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19037_GM;
MethodInfo m19037_MI = 
{
	"Dispose", (methodPointerType)&m19037, &t3433_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19037_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19038_GM;
MethodInfo m19038_MI = 
{
	"MoveNext", (methodPointerType)&m19038, &t3433_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19038_GM};
extern Il2CppType t1490_0_0_0;
extern void* RuntimeInvoker_t1490 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19039_GM;
MethodInfo m19039_MI = 
{
	"get_Current", (methodPointerType)&m19039, &t3433_TI, &t1490_0_0_0, RuntimeInvoker_t1490, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19039_GM};
static MethodInfo* t3433_MIs[] =
{
	&m19035_MI,
	&m19036_MI,
	&m19037_MI,
	&m19038_MI,
	&m19039_MI,
	NULL
};
static MethodInfo* t3433_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19036_MI,
	&m19038_MI,
	&m19037_MI,
	&m19039_MI,
};
static TypeInfo* t3433_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4961_TI,
};
static Il2CppInterfaceOffsetPair t3433_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4961_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3433_0_0_0;
extern Il2CppType t3433_1_0_0;
extern Il2CppGenericClass t3433_GC;
TypeInfo t3433_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3433_MIs, t3433_PIs, t3433_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3433_TI, t3433_ITIs, t3433_VT, &EmptyCustomAttributesCache, &t3433_TI, &t3433_0_0_0, &t3433_1_0_0, t3433_IOs, &t3433_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3433)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6486_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>
extern MethodInfo m33570_MI;
static PropertyInfo t6486____Count_PropertyInfo = 
{
	&t6486_TI, "Count", &m33570_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33571_MI;
static PropertyInfo t6486____IsReadOnly_PropertyInfo = 
{
	&t6486_TI, "IsReadOnly", &m33571_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6486_PIs[] =
{
	&t6486____Count_PropertyInfo,
	&t6486____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33570_GM;
MethodInfo m33570_MI = 
{
	"get_Count", NULL, &t6486_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33570_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33571_GM;
MethodInfo m33571_MI = 
{
	"get_IsReadOnly", NULL, &t6486_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33571_GM};
extern Il2CppType t1490_0_0_0;
extern Il2CppType t1490_0_0_0;
static ParameterInfo t6486_m33572_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1490_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33572_GM;
MethodInfo m33572_MI = 
{
	"Add", NULL, &t6486_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t6486_m33572_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33572_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33573_GM;
MethodInfo m33573_MI = 
{
	"Clear", NULL, &t6486_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33573_GM};
extern Il2CppType t1490_0_0_0;
static ParameterInfo t6486_m33574_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1490_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33574_GM;
MethodInfo m33574_MI = 
{
	"Contains", NULL, &t6486_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6486_m33574_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33574_GM};
extern Il2CppType t3685_0_0_0;
extern Il2CppType t3685_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6486_m33575_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3685_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33575_GM;
MethodInfo m33575_MI = 
{
	"CopyTo", NULL, &t6486_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6486_m33575_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33575_GM};
extern Il2CppType t1490_0_0_0;
static ParameterInfo t6486_m33576_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1490_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33576_GM;
MethodInfo m33576_MI = 
{
	"Remove", NULL, &t6486_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6486_m33576_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33576_GM};
static MethodInfo* t6486_MIs[] =
{
	&m33570_MI,
	&m33571_MI,
	&m33572_MI,
	&m33573_MI,
	&m33574_MI,
	&m33575_MI,
	&m33576_MI,
	NULL
};
extern TypeInfo t6488_TI;
static TypeInfo* t6486_ITIs[] = 
{
	&t603_TI,
	&t6488_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6486_0_0_0;
extern Il2CppType t6486_1_0_0;
struct t6486;
extern Il2CppGenericClass t6486_GC;
TypeInfo t6486_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6486_MIs, t6486_PIs, NULL, NULL, NULL, NULL, NULL, &t6486_TI, t6486_ITIs, NULL, &EmptyCustomAttributesCache, &t6486_TI, &t6486_0_0_0, &t6486_1_0_0, NULL, &t6486_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>
extern Il2CppType t4961_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33577_GM;
MethodInfo m33577_MI = 
{
	"GetEnumerator", NULL, &t6488_TI, &t4961_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33577_GM};
static MethodInfo* t6488_MIs[] =
{
	&m33577_MI,
	NULL
};
static TypeInfo* t6488_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6488_0_0_0;
extern Il2CppType t6488_1_0_0;
struct t6488;
extern Il2CppGenericClass t6488_GC;
TypeInfo t6488_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6488_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6488_TI, t6488_ITIs, NULL, &EmptyCustomAttributesCache, &t6488_TI, &t6488_0_0_0, &t6488_1_0_0, NULL, &t6488_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6487_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.BinaryElement>
extern MethodInfo m33578_MI;
extern MethodInfo m33579_MI;
static PropertyInfo t6487____Item_PropertyInfo = 
{
	&t6487_TI, "Item", &m33578_MI, &m33579_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6487_PIs[] =
{
	&t6487____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1490_0_0_0;
static ParameterInfo t6487_m33580_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1490_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33580_GM;
MethodInfo m33580_MI = 
{
	"IndexOf", NULL, &t6487_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t6487_m33580_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33580_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1490_0_0_0;
static ParameterInfo t6487_m33581_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1490_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33581_GM;
MethodInfo m33581_MI = 
{
	"Insert", NULL, &t6487_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6487_m33581_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33581_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6487_m33582_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33582_GM;
MethodInfo m33582_MI = 
{
	"RemoveAt", NULL, &t6487_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6487_m33582_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33582_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6487_m33578_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1490_0_0_0;
extern void* RuntimeInvoker_t1490_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33578_GM;
MethodInfo m33578_MI = 
{
	"get_Item", NULL, &t6487_TI, &t1490_0_0_0, RuntimeInvoker_t1490_t44, t6487_m33578_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33578_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1490_0_0_0;
static ParameterInfo t6487_m33579_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1490_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33579_GM;
MethodInfo m33579_MI = 
{
	"set_Item", NULL, &t6487_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6487_m33579_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33579_GM};
static MethodInfo* t6487_MIs[] =
{
	&m33580_MI,
	&m33581_MI,
	&m33582_MI,
	&m33578_MI,
	&m33579_MI,
	NULL
};
static TypeInfo* t6487_ITIs[] = 
{
	&t603_TI,
	&t6486_TI,
	&t6488_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6487_0_0_0;
extern Il2CppType t6487_1_0_0;
struct t6487;
extern Il2CppGenericClass t6487_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6487_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6487_MIs, t6487_PIs, NULL, NULL, NULL, NULL, NULL, &t6487_TI, t6487_ITIs, NULL, &t1908__CustomAttributeCache, &t6487_TI, &t6487_0_0_0, &t6487_1_0_0, NULL, &t6487_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4963_TI;

#include "t1491.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
extern MethodInfo m33583_MI;
static PropertyInfo t4963____Current_PropertyInfo = 
{
	&t4963_TI, "Current", &m33583_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4963_PIs[] =
{
	&t4963____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1491_0_0_0;
extern void* RuntimeInvoker_t1491 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33583_GM;
MethodInfo m33583_MI = 
{
	"get_Current", NULL, &t4963_TI, &t1491_0_0_0, RuntimeInvoker_t1491, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33583_GM};
static MethodInfo* t4963_MIs[] =
{
	&m33583_MI,
	NULL
};
static TypeInfo* t4963_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4963_0_0_0;
extern Il2CppType t4963_1_0_0;
struct t4963;
extern Il2CppGenericClass t4963_GC;
TypeInfo t4963_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4963_MIs, t4963_PIs, NULL, NULL, NULL, NULL, NULL, &t4963_TI, t4963_ITIs, NULL, &EmptyCustomAttributesCache, &t4963_TI, &t4963_0_0_0, &t4963_1_0_0, NULL, &t4963_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3434.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3434_TI;
#include "t3434MD.h"

extern TypeInfo t1491_TI;
extern MethodInfo m19044_MI;
extern MethodInfo m25721_MI;
struct t20;
 uint8_t m25721 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19040_MI;
 void m19040 (t3434 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19041_MI;
 t29 * m19041 (t3434 * __this, MethodInfo* method){
	{
		uint8_t L_0 = m19044(__this, &m19044_MI);
		uint8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1491_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19042_MI;
 void m19042 (t3434 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19043_MI;
 bool m19043 (t3434 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint8_t m19044 (t3434 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint8_t L_8 = m25721(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25721_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
extern Il2CppType t20_0_0_1;
FieldInfo t3434_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3434_TI, offsetof(t3434, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3434_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3434_TI, offsetof(t3434, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3434_FIs[] =
{
	&t3434_f0_FieldInfo,
	&t3434_f1_FieldInfo,
	NULL
};
static PropertyInfo t3434____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3434_TI, "System.Collections.IEnumerator.Current", &m19041_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3434____Current_PropertyInfo = 
{
	&t3434_TI, "Current", &m19044_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3434_PIs[] =
{
	&t3434____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3434____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3434_m19040_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19040_GM;
MethodInfo m19040_MI = 
{
	".ctor", (methodPointerType)&m19040, &t3434_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3434_m19040_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19040_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19041_GM;
MethodInfo m19041_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19041, &t3434_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19041_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19042_GM;
MethodInfo m19042_MI = 
{
	"Dispose", (methodPointerType)&m19042, &t3434_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19042_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19043_GM;
MethodInfo m19043_MI = 
{
	"MoveNext", (methodPointerType)&m19043, &t3434_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19043_GM};
extern Il2CppType t1491_0_0_0;
extern void* RuntimeInvoker_t1491 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19044_GM;
MethodInfo m19044_MI = 
{
	"get_Current", (methodPointerType)&m19044, &t3434_TI, &t1491_0_0_0, RuntimeInvoker_t1491, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19044_GM};
static MethodInfo* t3434_MIs[] =
{
	&m19040_MI,
	&m19041_MI,
	&m19042_MI,
	&m19043_MI,
	&m19044_MI,
	NULL
};
static MethodInfo* t3434_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19041_MI,
	&m19043_MI,
	&m19042_MI,
	&m19044_MI,
};
static TypeInfo* t3434_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4963_TI,
};
static Il2CppInterfaceOffsetPair t3434_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4963_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3434_0_0_0;
extern Il2CppType t3434_1_0_0;
extern Il2CppGenericClass t3434_GC;
TypeInfo t3434_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3434_MIs, t3434_PIs, t3434_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3434_TI, t3434_ITIs, t3434_VT, &EmptyCustomAttributesCache, &t3434_TI, &t3434_0_0_0, &t3434_1_0_0, t3434_IOs, &t3434_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3434)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6489_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
extern MethodInfo m33584_MI;
static PropertyInfo t6489____Count_PropertyInfo = 
{
	&t6489_TI, "Count", &m33584_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33585_MI;
static PropertyInfo t6489____IsReadOnly_PropertyInfo = 
{
	&t6489_TI, "IsReadOnly", &m33585_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6489_PIs[] =
{
	&t6489____Count_PropertyInfo,
	&t6489____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33584_GM;
MethodInfo m33584_MI = 
{
	"get_Count", NULL, &t6489_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33584_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33585_GM;
MethodInfo m33585_MI = 
{
	"get_IsReadOnly", NULL, &t6489_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33585_GM};
extern Il2CppType t1491_0_0_0;
extern Il2CppType t1491_0_0_0;
static ParameterInfo t6489_m33586_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33586_GM;
MethodInfo m33586_MI = 
{
	"Add", NULL, &t6489_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t6489_m33586_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33586_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33587_GM;
MethodInfo m33587_MI = 
{
	"Clear", NULL, &t6489_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33587_GM};
extern Il2CppType t1491_0_0_0;
static ParameterInfo t6489_m33588_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33588_GM;
MethodInfo m33588_MI = 
{
	"Contains", NULL, &t6489_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6489_m33588_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33588_GM};
extern Il2CppType t2056_0_0_0;
extern Il2CppType t2056_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6489_m33589_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2056_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33589_GM;
MethodInfo m33589_MI = 
{
	"CopyTo", NULL, &t6489_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6489_m33589_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33589_GM};
extern Il2CppType t1491_0_0_0;
static ParameterInfo t6489_m33590_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33590_GM;
MethodInfo m33590_MI = 
{
	"Remove", NULL, &t6489_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6489_m33590_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33590_GM};
static MethodInfo* t6489_MIs[] =
{
	&m33584_MI,
	&m33585_MI,
	&m33586_MI,
	&m33587_MI,
	&m33588_MI,
	&m33589_MI,
	&m33590_MI,
	NULL
};
extern TypeInfo t6491_TI;
static TypeInfo* t6489_ITIs[] = 
{
	&t603_TI,
	&t6491_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6489_0_0_0;
extern Il2CppType t6489_1_0_0;
struct t6489;
extern Il2CppGenericClass t6489_GC;
TypeInfo t6489_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6489_MIs, t6489_PIs, NULL, NULL, NULL, NULL, NULL, &t6489_TI, t6489_ITIs, NULL, &EmptyCustomAttributesCache, &t6489_TI, &t6489_0_0_0, &t6489_1_0_0, NULL, &t6489_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
extern Il2CppType t4963_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33591_GM;
MethodInfo m33591_MI = 
{
	"GetEnumerator", NULL, &t6491_TI, &t4963_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33591_GM};
static MethodInfo* t6491_MIs[] =
{
	&m33591_MI,
	NULL
};
static TypeInfo* t6491_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6491_0_0_0;
extern Il2CppType t6491_1_0_0;
struct t6491;
extern Il2CppGenericClass t6491_GC;
TypeInfo t6491_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6491_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6491_TI, t6491_ITIs, NULL, &EmptyCustomAttributesCache, &t6491_TI, &t6491_0_0_0, &t6491_1_0_0, NULL, &t6491_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6490_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
extern MethodInfo m33592_MI;
extern MethodInfo m33593_MI;
static PropertyInfo t6490____Item_PropertyInfo = 
{
	&t6490_TI, "Item", &m33592_MI, &m33593_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6490_PIs[] =
{
	&t6490____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1491_0_0_0;
static ParameterInfo t6490_m33594_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33594_GM;
MethodInfo m33594_MI = 
{
	"IndexOf", NULL, &t6490_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t6490_m33594_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33594_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1491_0_0_0;
static ParameterInfo t6490_m33595_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33595_GM;
MethodInfo m33595_MI = 
{
	"Insert", NULL, &t6490_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6490_m33595_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33595_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6490_m33596_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33596_GM;
MethodInfo m33596_MI = 
{
	"RemoveAt", NULL, &t6490_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6490_m33596_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33596_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6490_m33592_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1491_0_0_0;
extern void* RuntimeInvoker_t1491_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33592_GM;
MethodInfo m33592_MI = 
{
	"get_Item", NULL, &t6490_TI, &t1491_0_0_0, RuntimeInvoker_t1491_t44, t6490_m33592_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33592_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1491_0_0_0;
static ParameterInfo t6490_m33593_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33593_GM;
MethodInfo m33593_MI = 
{
	"set_Item", NULL, &t6490_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6490_m33593_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33593_GM};
static MethodInfo* t6490_MIs[] =
{
	&m33594_MI,
	&m33595_MI,
	&m33596_MI,
	&m33592_MI,
	&m33593_MI,
	NULL
};
static TypeInfo* t6490_ITIs[] = 
{
	&t603_TI,
	&t6489_TI,
	&t6491_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6490_0_0_0;
extern Il2CppType t6490_1_0_0;
struct t6490;
extern Il2CppGenericClass t6490_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6490_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6490_MIs, t6490_PIs, NULL, NULL, NULL, NULL, NULL, &t6490_TI, t6490_ITIs, NULL, &t1908__CustomAttributeCache, &t6490_TI, &t6490_0_0_0, &t6490_1_0_0, NULL, &t6490_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4965_TI;

#include "t1492.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>
extern MethodInfo m33597_MI;
static PropertyInfo t4965____Current_PropertyInfo = 
{
	&t4965_TI, "Current", &m33597_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4965_PIs[] =
{
	&t4965____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1492_0_0_0;
extern void* RuntimeInvoker_t1492 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33597_GM;
MethodInfo m33597_MI = 
{
	"get_Current", NULL, &t4965_TI, &t1492_0_0_0, RuntimeInvoker_t1492, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33597_GM};
static MethodInfo* t4965_MIs[] =
{
	&m33597_MI,
	NULL
};
static TypeInfo* t4965_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4965_0_0_0;
extern Il2CppType t4965_1_0_0;
struct t4965;
extern Il2CppGenericClass t4965_GC;
TypeInfo t4965_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4965_MIs, t4965_PIs, NULL, NULL, NULL, NULL, NULL, &t4965_TI, t4965_ITIs, NULL, &EmptyCustomAttributesCache, &t4965_TI, &t4965_0_0_0, &t4965_1_0_0, NULL, &t4965_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3435.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3435_TI;
#include "t3435MD.h"

extern TypeInfo t1492_TI;
extern MethodInfo m19049_MI;
extern MethodInfo m25732_MI;
struct t20;
 int32_t m25732 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19045_MI;
 void m19045 (t3435 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19046_MI;
 t29 * m19046 (t3435 * __this, MethodInfo* method){
	{
		int32_t L_0 = m19049(__this, &m19049_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1492_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19047_MI;
 void m19047 (t3435 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19048_MI;
 bool m19048 (t3435 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m19049 (t3435 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25732(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25732_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3435_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3435_TI, offsetof(t3435, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3435_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3435_TI, offsetof(t3435, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3435_FIs[] =
{
	&t3435_f0_FieldInfo,
	&t3435_f1_FieldInfo,
	NULL
};
static PropertyInfo t3435____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3435_TI, "System.Collections.IEnumerator.Current", &m19046_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3435____Current_PropertyInfo = 
{
	&t3435_TI, "Current", &m19049_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3435_PIs[] =
{
	&t3435____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3435____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3435_m19045_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19045_GM;
MethodInfo m19045_MI = 
{
	".ctor", (methodPointerType)&m19045, &t3435_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3435_m19045_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19045_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19046_GM;
MethodInfo m19046_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19046, &t3435_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19046_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19047_GM;
MethodInfo m19047_MI = 
{
	"Dispose", (methodPointerType)&m19047, &t3435_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19047_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19048_GM;
MethodInfo m19048_MI = 
{
	"MoveNext", (methodPointerType)&m19048, &t3435_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19048_GM};
extern Il2CppType t1492_0_0_0;
extern void* RuntimeInvoker_t1492 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19049_GM;
MethodInfo m19049_MI = 
{
	"get_Current", (methodPointerType)&m19049, &t3435_TI, &t1492_0_0_0, RuntimeInvoker_t1492, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19049_GM};
static MethodInfo* t3435_MIs[] =
{
	&m19045_MI,
	&m19046_MI,
	&m19047_MI,
	&m19048_MI,
	&m19049_MI,
	NULL
};
static MethodInfo* t3435_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19046_MI,
	&m19048_MI,
	&m19047_MI,
	&m19049_MI,
};
static TypeInfo* t3435_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4965_TI,
};
static Il2CppInterfaceOffsetPair t3435_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4965_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3435_0_0_0;
extern Il2CppType t3435_1_0_0;
extern Il2CppGenericClass t3435_GC;
TypeInfo t3435_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3435_MIs, t3435_PIs, t3435_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3435_TI, t3435_ITIs, t3435_VT, &EmptyCustomAttributesCache, &t3435_TI, &t3435_0_0_0, &t3435_1_0_0, t3435_IOs, &t3435_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3435)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6492_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>
extern MethodInfo m33598_MI;
static PropertyInfo t6492____Count_PropertyInfo = 
{
	&t6492_TI, "Count", &m33598_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33599_MI;
static PropertyInfo t6492____IsReadOnly_PropertyInfo = 
{
	&t6492_TI, "IsReadOnly", &m33599_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6492_PIs[] =
{
	&t6492____Count_PropertyInfo,
	&t6492____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33598_GM;
MethodInfo m33598_MI = 
{
	"get_Count", NULL, &t6492_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33598_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33599_GM;
MethodInfo m33599_MI = 
{
	"get_IsReadOnly", NULL, &t6492_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33599_GM};
extern Il2CppType t1492_0_0_0;
extern Il2CppType t1492_0_0_0;
static ParameterInfo t6492_m33600_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1492_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33600_GM;
MethodInfo m33600_MI = 
{
	"Add", NULL, &t6492_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6492_m33600_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33600_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33601_GM;
MethodInfo m33601_MI = 
{
	"Clear", NULL, &t6492_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33601_GM};
extern Il2CppType t1492_0_0_0;
static ParameterInfo t6492_m33602_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1492_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33602_GM;
MethodInfo m33602_MI = 
{
	"Contains", NULL, &t6492_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6492_m33602_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33602_GM};
extern Il2CppType t3686_0_0_0;
extern Il2CppType t3686_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6492_m33603_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3686_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33603_GM;
MethodInfo m33603_MI = 
{
	"CopyTo", NULL, &t6492_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6492_m33603_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33603_GM};
extern Il2CppType t1492_0_0_0;
static ParameterInfo t6492_m33604_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1492_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33604_GM;
MethodInfo m33604_MI = 
{
	"Remove", NULL, &t6492_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6492_m33604_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33604_GM};
static MethodInfo* t6492_MIs[] =
{
	&m33598_MI,
	&m33599_MI,
	&m33600_MI,
	&m33601_MI,
	&m33602_MI,
	&m33603_MI,
	&m33604_MI,
	NULL
};
extern TypeInfo t6494_TI;
static TypeInfo* t6492_ITIs[] = 
{
	&t603_TI,
	&t6494_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6492_0_0_0;
extern Il2CppType t6492_1_0_0;
struct t6492;
extern Il2CppGenericClass t6492_GC;
TypeInfo t6492_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6492_MIs, t6492_PIs, NULL, NULL, NULL, NULL, NULL, &t6492_TI, t6492_ITIs, NULL, &EmptyCustomAttributesCache, &t6492_TI, &t6492_0_0_0, &t6492_1_0_0, NULL, &t6492_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>
extern Il2CppType t4965_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33605_GM;
MethodInfo m33605_MI = 
{
	"GetEnumerator", NULL, &t6494_TI, &t4965_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33605_GM};
static MethodInfo* t6494_MIs[] =
{
	&m33605_MI,
	NULL
};
static TypeInfo* t6494_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6494_0_0_0;
extern Il2CppType t6494_1_0_0;
struct t6494;
extern Il2CppGenericClass t6494_GC;
TypeInfo t6494_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6494_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6494_TI, t6494_ITIs, NULL, &EmptyCustomAttributesCache, &t6494_TI, &t6494_0_0_0, &t6494_1_0_0, NULL, &t6494_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6493_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.MethodFlags>
extern MethodInfo m33606_MI;
extern MethodInfo m33607_MI;
static PropertyInfo t6493____Item_PropertyInfo = 
{
	&t6493_TI, "Item", &m33606_MI, &m33607_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6493_PIs[] =
{
	&t6493____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1492_0_0_0;
static ParameterInfo t6493_m33608_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1492_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33608_GM;
MethodInfo m33608_MI = 
{
	"IndexOf", NULL, &t6493_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6493_m33608_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33608_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1492_0_0_0;
static ParameterInfo t6493_m33609_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1492_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33609_GM;
MethodInfo m33609_MI = 
{
	"Insert", NULL, &t6493_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6493_m33609_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33609_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6493_m33610_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33610_GM;
MethodInfo m33610_MI = 
{
	"RemoveAt", NULL, &t6493_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6493_m33610_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33610_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6493_m33606_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1492_0_0_0;
extern void* RuntimeInvoker_t1492_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33606_GM;
MethodInfo m33606_MI = 
{
	"get_Item", NULL, &t6493_TI, &t1492_0_0_0, RuntimeInvoker_t1492_t44, t6493_m33606_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33606_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1492_0_0_0;
static ParameterInfo t6493_m33607_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1492_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33607_GM;
MethodInfo m33607_MI = 
{
	"set_Item", NULL, &t6493_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6493_m33607_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33607_GM};
static MethodInfo* t6493_MIs[] =
{
	&m33608_MI,
	&m33609_MI,
	&m33610_MI,
	&m33606_MI,
	&m33607_MI,
	NULL
};
static TypeInfo* t6493_ITIs[] = 
{
	&t603_TI,
	&t6492_TI,
	&t6494_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6493_0_0_0;
extern Il2CppType t6493_1_0_0;
struct t6493;
extern Il2CppGenericClass t6493_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6493_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6493_MIs, t6493_PIs, NULL, NULL, NULL, NULL, NULL, &t6493_TI, t6493_ITIs, NULL, &t1908__CustomAttributeCache, &t6493_TI, &t6493_0_0_0, &t6493_1_0_0, NULL, &t6493_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4967_TI;

#include "t1493.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>
extern MethodInfo m33611_MI;
static PropertyInfo t4967____Current_PropertyInfo = 
{
	&t4967_TI, "Current", &m33611_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4967_PIs[] =
{
	&t4967____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1493_0_0_0;
extern void* RuntimeInvoker_t1493 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33611_GM;
MethodInfo m33611_MI = 
{
	"get_Current", NULL, &t4967_TI, &t1493_0_0_0, RuntimeInvoker_t1493, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33611_GM};
static MethodInfo* t4967_MIs[] =
{
	&m33611_MI,
	NULL
};
static TypeInfo* t4967_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4967_0_0_0;
extern Il2CppType t4967_1_0_0;
struct t4967;
extern Il2CppGenericClass t4967_GC;
TypeInfo t4967_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4967_MIs, t4967_PIs, NULL, NULL, NULL, NULL, NULL, &t4967_TI, t4967_ITIs, NULL, &EmptyCustomAttributesCache, &t4967_TI, &t4967_0_0_0, &t4967_1_0_0, NULL, &t4967_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3436.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3436_TI;
#include "t3436MD.h"

extern TypeInfo t1493_TI;
extern MethodInfo m19054_MI;
extern MethodInfo m25743_MI;
struct t20;
 uint8_t m25743 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19050_MI;
 void m19050 (t3436 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19051_MI;
 t29 * m19051 (t3436 * __this, MethodInfo* method){
	{
		uint8_t L_0 = m19054(__this, &m19054_MI);
		uint8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1493_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19052_MI;
 void m19052 (t3436 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19053_MI;
 bool m19053 (t3436 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint8_t m19054 (t3436 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint8_t L_8 = m25743(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25743_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>
extern Il2CppType t20_0_0_1;
FieldInfo t3436_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3436_TI, offsetof(t3436, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3436_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3436_TI, offsetof(t3436, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3436_FIs[] =
{
	&t3436_f0_FieldInfo,
	&t3436_f1_FieldInfo,
	NULL
};
static PropertyInfo t3436____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3436_TI, "System.Collections.IEnumerator.Current", &m19051_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3436____Current_PropertyInfo = 
{
	&t3436_TI, "Current", &m19054_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3436_PIs[] =
{
	&t3436____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3436____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3436_m19050_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19050_GM;
MethodInfo m19050_MI = 
{
	".ctor", (methodPointerType)&m19050, &t3436_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3436_m19050_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19050_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19051_GM;
MethodInfo m19051_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19051, &t3436_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19051_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19052_GM;
MethodInfo m19052_MI = 
{
	"Dispose", (methodPointerType)&m19052, &t3436_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19052_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19053_GM;
MethodInfo m19053_MI = 
{
	"MoveNext", (methodPointerType)&m19053, &t3436_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19053_GM};
extern Il2CppType t1493_0_0_0;
extern void* RuntimeInvoker_t1493 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19054_GM;
MethodInfo m19054_MI = 
{
	"get_Current", (methodPointerType)&m19054, &t3436_TI, &t1493_0_0_0, RuntimeInvoker_t1493, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19054_GM};
static MethodInfo* t3436_MIs[] =
{
	&m19050_MI,
	&m19051_MI,
	&m19052_MI,
	&m19053_MI,
	&m19054_MI,
	NULL
};
static MethodInfo* t3436_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19051_MI,
	&m19053_MI,
	&m19052_MI,
	&m19054_MI,
};
static TypeInfo* t3436_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4967_TI,
};
static Il2CppInterfaceOffsetPair t3436_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4967_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3436_0_0_0;
extern Il2CppType t3436_1_0_0;
extern Il2CppGenericClass t3436_GC;
TypeInfo t3436_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3436_MIs, t3436_PIs, t3436_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3436_TI, t3436_ITIs, t3436_VT, &EmptyCustomAttributesCache, &t3436_TI, &t3436_0_0_0, &t3436_1_0_0, t3436_IOs, &t3436_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3436)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6495_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>
extern MethodInfo m33612_MI;
static PropertyInfo t6495____Count_PropertyInfo = 
{
	&t6495_TI, "Count", &m33612_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33613_MI;
static PropertyInfo t6495____IsReadOnly_PropertyInfo = 
{
	&t6495_TI, "IsReadOnly", &m33613_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6495_PIs[] =
{
	&t6495____Count_PropertyInfo,
	&t6495____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33612_GM;
MethodInfo m33612_MI = 
{
	"get_Count", NULL, &t6495_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33612_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33613_GM;
MethodInfo m33613_MI = 
{
	"get_IsReadOnly", NULL, &t6495_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33613_GM};
extern Il2CppType t1493_0_0_0;
extern Il2CppType t1493_0_0_0;
static ParameterInfo t6495_m33614_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1493_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33614_GM;
MethodInfo m33614_MI = 
{
	"Add", NULL, &t6495_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t6495_m33614_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33614_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33615_GM;
MethodInfo m33615_MI = 
{
	"Clear", NULL, &t6495_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33615_GM};
extern Il2CppType t1493_0_0_0;
static ParameterInfo t6495_m33616_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1493_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33616_GM;
MethodInfo m33616_MI = 
{
	"Contains", NULL, &t6495_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6495_m33616_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33616_GM};
extern Il2CppType t3687_0_0_0;
extern Il2CppType t3687_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6495_m33617_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3687_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33617_GM;
MethodInfo m33617_MI = 
{
	"CopyTo", NULL, &t6495_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6495_m33617_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33617_GM};
extern Il2CppType t1493_0_0_0;
static ParameterInfo t6495_m33618_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1493_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33618_GM;
MethodInfo m33618_MI = 
{
	"Remove", NULL, &t6495_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t6495_m33618_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33618_GM};
static MethodInfo* t6495_MIs[] =
{
	&m33612_MI,
	&m33613_MI,
	&m33614_MI,
	&m33615_MI,
	&m33616_MI,
	&m33617_MI,
	&m33618_MI,
	NULL
};
extern TypeInfo t6497_TI;
static TypeInfo* t6495_ITIs[] = 
{
	&t603_TI,
	&t6497_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6495_0_0_0;
extern Il2CppType t6495_1_0_0;
struct t6495;
extern Il2CppGenericClass t6495_GC;
TypeInfo t6495_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6495_MIs, t6495_PIs, NULL, NULL, NULL, NULL, NULL, &t6495_TI, t6495_ITIs, NULL, &EmptyCustomAttributesCache, &t6495_TI, &t6495_0_0_0, &t6495_1_0_0, NULL, &t6495_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>
extern Il2CppType t4967_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33619_GM;
MethodInfo m33619_MI = 
{
	"GetEnumerator", NULL, &t6497_TI, &t4967_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33619_GM};
static MethodInfo* t6497_MIs[] =
{
	&m33619_MI,
	NULL
};
static TypeInfo* t6497_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6497_0_0_0;
extern Il2CppType t6497_1_0_0;
struct t6497;
extern Il2CppGenericClass t6497_GC;
TypeInfo t6497_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6497_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6497_TI, t6497_ITIs, NULL, &EmptyCustomAttributesCache, &t6497_TI, &t6497_0_0_0, &t6497_1_0_0, NULL, &t6497_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6496_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>
extern MethodInfo m33620_MI;
extern MethodInfo m33621_MI;
static PropertyInfo t6496____Item_PropertyInfo = 
{
	&t6496_TI, "Item", &m33620_MI, &m33621_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6496_PIs[] =
{
	&t6496____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1493_0_0_0;
static ParameterInfo t6496_m33622_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1493_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33622_GM;
MethodInfo m33622_MI = 
{
	"IndexOf", NULL, &t6496_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t6496_m33622_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33622_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1493_0_0_0;
static ParameterInfo t6496_m33623_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1493_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33623_GM;
MethodInfo m33623_MI = 
{
	"Insert", NULL, &t6496_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6496_m33623_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33623_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6496_m33624_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33624_GM;
MethodInfo m33624_MI = 
{
	"RemoveAt", NULL, &t6496_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6496_m33624_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33624_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6496_m33620_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1493_0_0_0;
extern void* RuntimeInvoker_t1493_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33620_GM;
MethodInfo m33620_MI = 
{
	"get_Item", NULL, &t6496_TI, &t1493_0_0_0, RuntimeInvoker_t1493_t44, t6496_m33620_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33620_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1493_0_0_0;
static ParameterInfo t6496_m33621_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1493_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33621_GM;
MethodInfo m33621_MI = 
{
	"set_Item", NULL, &t6496_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t6496_m33621_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33621_GM};
static MethodInfo* t6496_MIs[] =
{
	&m33622_MI,
	&m33623_MI,
	&m33624_MI,
	&m33620_MI,
	&m33621_MI,
	NULL
};
static TypeInfo* t6496_ITIs[] = 
{
	&t603_TI,
	&t6495_TI,
	&t6497_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6496_0_0_0;
extern Il2CppType t6496_1_0_0;
struct t6496;
extern Il2CppGenericClass t6496_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6496_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6496_MIs, t6496_PIs, NULL, NULL, NULL, NULL, NULL, &t6496_TI, t6496_ITIs, NULL, &t1908__CustomAttributeCache, &t6496_TI, &t6496_0_0_0, &t6496_1_0_0, NULL, &t6496_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4968_TI;

#include "t465.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.DateTime>
extern MethodInfo m33625_MI;
static PropertyInfo t4968____Current_PropertyInfo = 
{
	&t4968_TI, "Current", &m33625_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4968_PIs[] =
{
	&t4968____Current_PropertyInfo,
	NULL
};
extern Il2CppType t465_0_0_0;
extern void* RuntimeInvoker_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33625_GM;
MethodInfo m33625_MI = 
{
	"get_Current", NULL, &t4968_TI, &t465_0_0_0, RuntimeInvoker_t465, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33625_GM};
static MethodInfo* t4968_MIs[] =
{
	&m33625_MI,
	NULL
};
static TypeInfo* t4968_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4968_0_0_0;
extern Il2CppType t4968_1_0_0;
struct t4968;
extern Il2CppGenericClass t4968_GC;
TypeInfo t4968_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4968_MIs, t4968_PIs, NULL, NULL, NULL, NULL, NULL, &t4968_TI, t4968_ITIs, NULL, &EmptyCustomAttributesCache, &t4968_TI, &t4968_0_0_0, &t4968_1_0_0, NULL, &t4968_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3437.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3437_TI;
#include "t3437MD.h"

extern TypeInfo t465_TI;
extern MethodInfo m19059_MI;
extern MethodInfo m25754_MI;
struct t20;
 t465  m25754 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19055_MI;
 void m19055 (t3437 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19056_MI;
 t29 * m19056 (t3437 * __this, MethodInfo* method){
	{
		t465  L_0 = m19059(__this, &m19059_MI);
		t465  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t465_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19057_MI;
 void m19057 (t3437 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19058_MI;
 bool m19058 (t3437 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t465  m19059 (t3437 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t465  L_8 = m25754(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25754_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.DateTime>
extern Il2CppType t20_0_0_1;
FieldInfo t3437_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3437_TI, offsetof(t3437, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3437_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3437_TI, offsetof(t3437, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3437_FIs[] =
{
	&t3437_f0_FieldInfo,
	&t3437_f1_FieldInfo,
	NULL
};
static PropertyInfo t3437____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3437_TI, "System.Collections.IEnumerator.Current", &m19056_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3437____Current_PropertyInfo = 
{
	&t3437_TI, "Current", &m19059_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3437_PIs[] =
{
	&t3437____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3437____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3437_m19055_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19055_GM;
MethodInfo m19055_MI = 
{
	".ctor", (methodPointerType)&m19055, &t3437_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3437_m19055_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19055_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19056_GM;
MethodInfo m19056_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19056, &t3437_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19056_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19057_GM;
MethodInfo m19057_MI = 
{
	"Dispose", (methodPointerType)&m19057, &t3437_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19057_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19058_GM;
MethodInfo m19058_MI = 
{
	"MoveNext", (methodPointerType)&m19058, &t3437_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19058_GM};
extern Il2CppType t465_0_0_0;
extern void* RuntimeInvoker_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19059_GM;
MethodInfo m19059_MI = 
{
	"get_Current", (methodPointerType)&m19059, &t3437_TI, &t465_0_0_0, RuntimeInvoker_t465, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19059_GM};
static MethodInfo* t3437_MIs[] =
{
	&m19055_MI,
	&m19056_MI,
	&m19057_MI,
	&m19058_MI,
	&m19059_MI,
	NULL
};
static MethodInfo* t3437_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19056_MI,
	&m19058_MI,
	&m19057_MI,
	&m19059_MI,
};
static TypeInfo* t3437_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4968_TI,
};
static Il2CppInterfaceOffsetPair t3437_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4968_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3437_0_0_0;
extern Il2CppType t3437_1_0_0;
extern Il2CppGenericClass t3437_GC;
TypeInfo t3437_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3437_MIs, t3437_PIs, t3437_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3437_TI, t3437_ITIs, t3437_VT, &EmptyCustomAttributesCache, &t3437_TI, &t3437_0_0_0, &t3437_1_0_0, t3437_IOs, &t3437_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3437)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6498_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.DateTime>
extern MethodInfo m33626_MI;
static PropertyInfo t6498____Count_PropertyInfo = 
{
	&t6498_TI, "Count", &m33626_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33627_MI;
static PropertyInfo t6498____IsReadOnly_PropertyInfo = 
{
	&t6498_TI, "IsReadOnly", &m33627_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6498_PIs[] =
{
	&t6498____Count_PropertyInfo,
	&t6498____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33626_GM;
MethodInfo m33626_MI = 
{
	"get_Count", NULL, &t6498_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33626_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33627_GM;
MethodInfo m33627_MI = 
{
	"get_IsReadOnly", NULL, &t6498_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33627_GM};
extern Il2CppType t465_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t6498_m33628_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33628_GM;
MethodInfo m33628_MI = 
{
	"Add", NULL, &t6498_TI, &t21_0_0_0, RuntimeInvoker_t21_t465, t6498_m33628_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33628_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33629_GM;
MethodInfo m33629_MI = 
{
	"Clear", NULL, &t6498_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33629_GM};
extern Il2CppType t465_0_0_0;
static ParameterInfo t6498_m33630_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33630_GM;
MethodInfo m33630_MI = 
{
	"Contains", NULL, &t6498_TI, &t40_0_0_0, RuntimeInvoker_t40_t465, t6498_m33630_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33630_GM};
extern Il2CppType t2053_0_0_0;
extern Il2CppType t2053_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6498_m33631_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2053_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33631_GM;
MethodInfo m33631_MI = 
{
	"CopyTo", NULL, &t6498_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6498_m33631_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33631_GM};
extern Il2CppType t465_0_0_0;
static ParameterInfo t6498_m33632_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33632_GM;
MethodInfo m33632_MI = 
{
	"Remove", NULL, &t6498_TI, &t40_0_0_0, RuntimeInvoker_t40_t465, t6498_m33632_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33632_GM};
static MethodInfo* t6498_MIs[] =
{
	&m33626_MI,
	&m33627_MI,
	&m33628_MI,
	&m33629_MI,
	&m33630_MI,
	&m33631_MI,
	&m33632_MI,
	NULL
};
extern TypeInfo t6500_TI;
static TypeInfo* t6498_ITIs[] = 
{
	&t603_TI,
	&t6500_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6498_0_0_0;
extern Il2CppType t6498_1_0_0;
struct t6498;
extern Il2CppGenericClass t6498_GC;
TypeInfo t6498_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6498_MIs, t6498_PIs, NULL, NULL, NULL, NULL, NULL, &t6498_TI, t6498_ITIs, NULL, &EmptyCustomAttributesCache, &t6498_TI, &t6498_0_0_0, &t6498_1_0_0, NULL, &t6498_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.DateTime>
extern Il2CppType t4968_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33633_GM;
MethodInfo m33633_MI = 
{
	"GetEnumerator", NULL, &t6500_TI, &t4968_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33633_GM};
static MethodInfo* t6500_MIs[] =
{
	&m33633_MI,
	NULL
};
static TypeInfo* t6500_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6500_0_0_0;
extern Il2CppType t6500_1_0_0;
struct t6500;
extern Il2CppGenericClass t6500_GC;
TypeInfo t6500_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6500_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6500_TI, t6500_ITIs, NULL, &EmptyCustomAttributesCache, &t6500_TI, &t6500_0_0_0, &t6500_1_0_0, NULL, &t6500_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6499_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.DateTime>
extern MethodInfo m33634_MI;
extern MethodInfo m33635_MI;
static PropertyInfo t6499____Item_PropertyInfo = 
{
	&t6499_TI, "Item", &m33634_MI, &m33635_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6499_PIs[] =
{
	&t6499____Item_PropertyInfo,
	NULL
};
extern Il2CppType t465_0_0_0;
static ParameterInfo t6499_m33636_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33636_GM;
MethodInfo m33636_MI = 
{
	"IndexOf", NULL, &t6499_TI, &t44_0_0_0, RuntimeInvoker_t44_t465, t6499_m33636_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33636_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t6499_m33637_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33637_GM;
MethodInfo m33637_MI = 
{
	"Insert", NULL, &t6499_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t465, t6499_m33637_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33637_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6499_m33638_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33638_GM;
MethodInfo m33638_MI = 
{
	"RemoveAt", NULL, &t6499_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6499_m33638_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33638_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6499_m33634_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t465_0_0_0;
extern void* RuntimeInvoker_t465_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33634_GM;
MethodInfo m33634_MI = 
{
	"get_Item", NULL, &t6499_TI, &t465_0_0_0, RuntimeInvoker_t465_t44, t6499_m33634_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33634_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t6499_m33635_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33635_GM;
MethodInfo m33635_MI = 
{
	"set_Item", NULL, &t6499_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t465, t6499_m33635_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33635_GM};
static MethodInfo* t6499_MIs[] =
{
	&m33636_MI,
	&m33637_MI,
	&m33638_MI,
	&m33634_MI,
	&m33635_MI,
	NULL
};
static TypeInfo* t6499_ITIs[] = 
{
	&t603_TI,
	&t6498_TI,
	&t6500_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6499_0_0_0;
extern Il2CppType t6499_1_0_0;
struct t6499;
extern Il2CppGenericClass t6499_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6499_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6499_MIs, t6499_PIs, NULL, NULL, NULL, NULL, NULL, &t6499_TI, t6499_ITIs, NULL, &t1908__CustomAttributeCache, &t6499_TI, &t6499_0_0_0, &t6499_1_0_0, NULL, &t6499_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6501_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.DateTime>>
extern MethodInfo m33639_MI;
static PropertyInfo t6501____Count_PropertyInfo = 
{
	&t6501_TI, "Count", &m33639_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33640_MI;
static PropertyInfo t6501____IsReadOnly_PropertyInfo = 
{
	&t6501_TI, "IsReadOnly", &m33640_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6501_PIs[] =
{
	&t6501____Count_PropertyInfo,
	&t6501____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33639_GM;
MethodInfo m33639_MI = 
{
	"get_Count", NULL, &t6501_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33639_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33640_GM;
MethodInfo m33640_MI = 
{
	"get_IsReadOnly", NULL, &t6501_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33640_GM};
extern Il2CppType t2074_0_0_0;
extern Il2CppType t2074_0_0_0;
static ParameterInfo t6501_m33641_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2074_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33641_GM;
MethodInfo m33641_MI = 
{
	"Add", NULL, &t6501_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6501_m33641_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33641_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33642_GM;
MethodInfo m33642_MI = 
{
	"Clear", NULL, &t6501_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33642_GM};
extern Il2CppType t2074_0_0_0;
static ParameterInfo t6501_m33643_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2074_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33643_GM;
MethodInfo m33643_MI = 
{
	"Contains", NULL, &t6501_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6501_m33643_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33643_GM};
extern Il2CppType t3688_0_0_0;
extern Il2CppType t3688_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6501_m33644_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3688_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33644_GM;
MethodInfo m33644_MI = 
{
	"CopyTo", NULL, &t6501_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6501_m33644_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33644_GM};
extern Il2CppType t2074_0_0_0;
static ParameterInfo t6501_m33645_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2074_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33645_GM;
MethodInfo m33645_MI = 
{
	"Remove", NULL, &t6501_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6501_m33645_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33645_GM};
static MethodInfo* t6501_MIs[] =
{
	&m33639_MI,
	&m33640_MI,
	&m33641_MI,
	&m33642_MI,
	&m33643_MI,
	&m33644_MI,
	&m33645_MI,
	NULL
};
extern TypeInfo t6503_TI;
static TypeInfo* t6501_ITIs[] = 
{
	&t603_TI,
	&t6503_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6501_0_0_0;
extern Il2CppType t6501_1_0_0;
struct t6501;
extern Il2CppGenericClass t6501_GC;
TypeInfo t6501_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6501_MIs, t6501_PIs, NULL, NULL, NULL, NULL, NULL, &t6501_TI, t6501_ITIs, NULL, &EmptyCustomAttributesCache, &t6501_TI, &t6501_0_0_0, &t6501_1_0_0, NULL, &t6501_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.DateTime>>
extern Il2CppType t4970_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33646_GM;
MethodInfo m33646_MI = 
{
	"GetEnumerator", NULL, &t6503_TI, &t4970_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33646_GM};
static MethodInfo* t6503_MIs[] =
{
	&m33646_MI,
	NULL
};
static TypeInfo* t6503_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6503_0_0_0;
extern Il2CppType t6503_1_0_0;
struct t6503;
extern Il2CppGenericClass t6503_GC;
TypeInfo t6503_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6503_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6503_TI, t6503_ITIs, NULL, &EmptyCustomAttributesCache, &t6503_TI, &t6503_0_0_0, &t6503_1_0_0, NULL, &t6503_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4970_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.DateTime>>
extern MethodInfo m33647_MI;
static PropertyInfo t4970____Current_PropertyInfo = 
{
	&t4970_TI, "Current", &m33647_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4970_PIs[] =
{
	&t4970____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2074_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33647_GM;
MethodInfo m33647_MI = 
{
	"get_Current", NULL, &t4970_TI, &t2074_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33647_GM};
static MethodInfo* t4970_MIs[] =
{
	&m33647_MI,
	NULL
};
static TypeInfo* t4970_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4970_0_0_0;
extern Il2CppType t4970_1_0_0;
struct t4970;
extern Il2CppGenericClass t4970_GC;
TypeInfo t4970_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4970_MIs, t4970_PIs, NULL, NULL, NULL, NULL, NULL, &t4970_TI, t4970_ITIs, NULL, &EmptyCustomAttributesCache, &t4970_TI, &t4970_0_0_0, &t4970_1_0_0, NULL, &t4970_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2074_TI;



// Metadata Definition System.IComparable`1<System.DateTime>
extern Il2CppType t465_0_0_0;
static ParameterInfo t2074_m33648_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33648_GM;
MethodInfo m33648_MI = 
{
	"CompareTo", NULL, &t2074_TI, &t44_0_0_0, RuntimeInvoker_t44_t465, t2074_m33648_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33648_GM};
static MethodInfo* t2074_MIs[] =
{
	&m33648_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2074_1_0_0;
struct t2074;
extern Il2CppGenericClass t2074_GC;
TypeInfo t2074_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t2074_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2074_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2074_TI, &t2074_0_0_0, &t2074_1_0_0, NULL, &t2074_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3438.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3438_TI;
#include "t3438MD.h"

extern MethodInfo m19064_MI;
extern MethodInfo m25765_MI;
struct t20;
#define m25765(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.DateTime>>
extern Il2CppType t20_0_0_1;
FieldInfo t3438_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3438_TI, offsetof(t3438, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3438_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3438_TI, offsetof(t3438, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3438_FIs[] =
{
	&t3438_f0_FieldInfo,
	&t3438_f1_FieldInfo,
	NULL
};
extern MethodInfo m19061_MI;
static PropertyInfo t3438____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3438_TI, "System.Collections.IEnumerator.Current", &m19061_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3438____Current_PropertyInfo = 
{
	&t3438_TI, "Current", &m19064_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3438_PIs[] =
{
	&t3438____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3438____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3438_m19060_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19060_GM;
MethodInfo m19060_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3438_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3438_m19060_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19060_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19061_GM;
MethodInfo m19061_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3438_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19061_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19062_GM;
MethodInfo m19062_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3438_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19062_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19063_GM;
MethodInfo m19063_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3438_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19063_GM};
extern Il2CppType t2074_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19064_GM;
MethodInfo m19064_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3438_TI, &t2074_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19064_GM};
static MethodInfo* t3438_MIs[] =
{
	&m19060_MI,
	&m19061_MI,
	&m19062_MI,
	&m19063_MI,
	&m19064_MI,
	NULL
};
extern MethodInfo m19063_MI;
extern MethodInfo m19062_MI;
static MethodInfo* t3438_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19061_MI,
	&m19063_MI,
	&m19062_MI,
	&m19064_MI,
};
static TypeInfo* t3438_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4970_TI,
};
static Il2CppInterfaceOffsetPair t3438_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4970_TI, 7},
};
extern TypeInfo t2074_TI;
static Il2CppRGCTXData t3438_RGCTXData[3] = 
{
	&m19064_MI/* Method Usage */,
	&t2074_TI/* Class Usage */,
	&m25765_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3438_0_0_0;
extern Il2CppType t3438_1_0_0;
extern Il2CppGenericClass t3438_GC;
TypeInfo t3438_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3438_MIs, t3438_PIs, t3438_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3438_TI, t3438_ITIs, t3438_VT, &EmptyCustomAttributesCache, &t3438_TI, &t3438_0_0_0, &t3438_1_0_0, t3438_IOs, &t3438_GC, NULL, NULL, NULL, t3438_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3438)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6502_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.DateTime>>
extern MethodInfo m33649_MI;
extern MethodInfo m33650_MI;
static PropertyInfo t6502____Item_PropertyInfo = 
{
	&t6502_TI, "Item", &m33649_MI, &m33650_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6502_PIs[] =
{
	&t6502____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2074_0_0_0;
static ParameterInfo t6502_m33651_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2074_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33651_GM;
MethodInfo m33651_MI = 
{
	"IndexOf", NULL, &t6502_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6502_m33651_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33651_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2074_0_0_0;
static ParameterInfo t6502_m33652_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2074_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33652_GM;
MethodInfo m33652_MI = 
{
	"Insert", NULL, &t6502_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6502_m33652_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33652_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6502_m33653_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33653_GM;
MethodInfo m33653_MI = 
{
	"RemoveAt", NULL, &t6502_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6502_m33653_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33653_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6502_m33649_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2074_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33649_GM;
MethodInfo m33649_MI = 
{
	"get_Item", NULL, &t6502_TI, &t2074_0_0_0, RuntimeInvoker_t29_t44, t6502_m33649_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33649_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2074_0_0_0;
static ParameterInfo t6502_m33650_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2074_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33650_GM;
MethodInfo m33650_MI = 
{
	"set_Item", NULL, &t6502_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6502_m33650_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33650_GM};
static MethodInfo* t6502_MIs[] =
{
	&m33651_MI,
	&m33652_MI,
	&m33653_MI,
	&m33649_MI,
	&m33650_MI,
	NULL
};
static TypeInfo* t6502_ITIs[] = 
{
	&t603_TI,
	&t6501_TI,
	&t6503_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6502_0_0_0;
extern Il2CppType t6502_1_0_0;
struct t6502;
extern Il2CppGenericClass t6502_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6502_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6502_MIs, t6502_PIs, NULL, NULL, NULL, NULL, NULL, &t6502_TI, t6502_ITIs, NULL, &t1908__CustomAttributeCache, &t6502_TI, &t6502_0_0_0, &t6502_1_0_0, NULL, &t6502_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6504_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.DateTime>>
extern MethodInfo m33654_MI;
static PropertyInfo t6504____Count_PropertyInfo = 
{
	&t6504_TI, "Count", &m33654_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33655_MI;
static PropertyInfo t6504____IsReadOnly_PropertyInfo = 
{
	&t6504_TI, "IsReadOnly", &m33655_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6504_PIs[] =
{
	&t6504____Count_PropertyInfo,
	&t6504____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33654_GM;
MethodInfo m33654_MI = 
{
	"get_Count", NULL, &t6504_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33654_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33655_GM;
MethodInfo m33655_MI = 
{
	"get_IsReadOnly", NULL, &t6504_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33655_GM};
extern Il2CppType t2075_0_0_0;
extern Il2CppType t2075_0_0_0;
static ParameterInfo t6504_m33656_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2075_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33656_GM;
MethodInfo m33656_MI = 
{
	"Add", NULL, &t6504_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6504_m33656_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33656_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33657_GM;
MethodInfo m33657_MI = 
{
	"Clear", NULL, &t6504_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33657_GM};
extern Il2CppType t2075_0_0_0;
static ParameterInfo t6504_m33658_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2075_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33658_GM;
MethodInfo m33658_MI = 
{
	"Contains", NULL, &t6504_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6504_m33658_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33658_GM};
extern Il2CppType t3689_0_0_0;
extern Il2CppType t3689_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6504_m33659_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3689_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33659_GM;
MethodInfo m33659_MI = 
{
	"CopyTo", NULL, &t6504_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6504_m33659_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33659_GM};
extern Il2CppType t2075_0_0_0;
static ParameterInfo t6504_m33660_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2075_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33660_GM;
MethodInfo m33660_MI = 
{
	"Remove", NULL, &t6504_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6504_m33660_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33660_GM};
static MethodInfo* t6504_MIs[] =
{
	&m33654_MI,
	&m33655_MI,
	&m33656_MI,
	&m33657_MI,
	&m33658_MI,
	&m33659_MI,
	&m33660_MI,
	NULL
};
extern TypeInfo t6506_TI;
static TypeInfo* t6504_ITIs[] = 
{
	&t603_TI,
	&t6506_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6504_0_0_0;
extern Il2CppType t6504_1_0_0;
struct t6504;
extern Il2CppGenericClass t6504_GC;
TypeInfo t6504_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6504_MIs, t6504_PIs, NULL, NULL, NULL, NULL, NULL, &t6504_TI, t6504_ITIs, NULL, &EmptyCustomAttributesCache, &t6504_TI, &t6504_0_0_0, &t6504_1_0_0, NULL, &t6504_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.DateTime>>
extern Il2CppType t4972_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33661_GM;
MethodInfo m33661_MI = 
{
	"GetEnumerator", NULL, &t6506_TI, &t4972_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33661_GM};
static MethodInfo* t6506_MIs[] =
{
	&m33661_MI,
	NULL
};
static TypeInfo* t6506_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6506_0_0_0;
extern Il2CppType t6506_1_0_0;
struct t6506;
extern Il2CppGenericClass t6506_GC;
TypeInfo t6506_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6506_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6506_TI, t6506_ITIs, NULL, &EmptyCustomAttributesCache, &t6506_TI, &t6506_0_0_0, &t6506_1_0_0, NULL, &t6506_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4972_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.DateTime>>
extern MethodInfo m33662_MI;
static PropertyInfo t4972____Current_PropertyInfo = 
{
	&t4972_TI, "Current", &m33662_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4972_PIs[] =
{
	&t4972____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2075_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33662_GM;
MethodInfo m33662_MI = 
{
	"get_Current", NULL, &t4972_TI, &t2075_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33662_GM};
static MethodInfo* t4972_MIs[] =
{
	&m33662_MI,
	NULL
};
static TypeInfo* t4972_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4972_0_0_0;
extern Il2CppType t4972_1_0_0;
struct t4972;
extern Il2CppGenericClass t4972_GC;
TypeInfo t4972_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4972_MIs, t4972_PIs, NULL, NULL, NULL, NULL, NULL, &t4972_TI, t4972_ITIs, NULL, &EmptyCustomAttributesCache, &t4972_TI, &t4972_0_0_0, &t4972_1_0_0, NULL, &t4972_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2075_TI;



// Metadata Definition System.IEquatable`1<System.DateTime>
extern Il2CppType t465_0_0_0;
static ParameterInfo t2075_m33663_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t465 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33663_GM;
MethodInfo m33663_MI = 
{
	"Equals", NULL, &t2075_TI, &t40_0_0_0, RuntimeInvoker_t40_t465, t2075_m33663_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33663_GM};
static MethodInfo* t2075_MIs[] =
{
	&m33663_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2075_1_0_0;
struct t2075;
extern Il2CppGenericClass t2075_GC;
TypeInfo t2075_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t2075_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2075_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2075_TI, &t2075_0_0_0, &t2075_1_0_0, NULL, &t2075_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3439.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3439_TI;
#include "t3439MD.h"

extern MethodInfo m19069_MI;
extern MethodInfo m25776_MI;
struct t20;
#define m25776(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.DateTime>>
extern Il2CppType t20_0_0_1;
FieldInfo t3439_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3439_TI, offsetof(t3439, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3439_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3439_TI, offsetof(t3439, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3439_FIs[] =
{
	&t3439_f0_FieldInfo,
	&t3439_f1_FieldInfo,
	NULL
};
extern MethodInfo m19066_MI;
static PropertyInfo t3439____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3439_TI, "System.Collections.IEnumerator.Current", &m19066_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3439____Current_PropertyInfo = 
{
	&t3439_TI, "Current", &m19069_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3439_PIs[] =
{
	&t3439____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3439____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3439_m19065_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19065_GM;
MethodInfo m19065_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3439_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3439_m19065_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19065_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19066_GM;
MethodInfo m19066_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3439_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19066_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19067_GM;
MethodInfo m19067_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3439_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19067_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19068_GM;
MethodInfo m19068_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3439_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19068_GM};
extern Il2CppType t2075_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19069_GM;
MethodInfo m19069_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3439_TI, &t2075_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19069_GM};
static MethodInfo* t3439_MIs[] =
{
	&m19065_MI,
	&m19066_MI,
	&m19067_MI,
	&m19068_MI,
	&m19069_MI,
	NULL
};
extern MethodInfo m19068_MI;
extern MethodInfo m19067_MI;
static MethodInfo* t3439_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19066_MI,
	&m19068_MI,
	&m19067_MI,
	&m19069_MI,
};
static TypeInfo* t3439_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4972_TI,
};
static Il2CppInterfaceOffsetPair t3439_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4972_TI, 7},
};
extern TypeInfo t2075_TI;
static Il2CppRGCTXData t3439_RGCTXData[3] = 
{
	&m19069_MI/* Method Usage */,
	&t2075_TI/* Class Usage */,
	&m25776_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3439_0_0_0;
extern Il2CppType t3439_1_0_0;
extern Il2CppGenericClass t3439_GC;
TypeInfo t3439_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3439_MIs, t3439_PIs, t3439_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3439_TI, t3439_ITIs, t3439_VT, &EmptyCustomAttributesCache, &t3439_TI, &t3439_0_0_0, &t3439_1_0_0, t3439_IOs, &t3439_GC, NULL, NULL, NULL, t3439_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3439)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6505_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.DateTime>>
extern MethodInfo m33664_MI;
extern MethodInfo m33665_MI;
static PropertyInfo t6505____Item_PropertyInfo = 
{
	&t6505_TI, "Item", &m33664_MI, &m33665_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6505_PIs[] =
{
	&t6505____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2075_0_0_0;
static ParameterInfo t6505_m33666_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2075_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33666_GM;
MethodInfo m33666_MI = 
{
	"IndexOf", NULL, &t6505_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6505_m33666_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33666_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2075_0_0_0;
static ParameterInfo t6505_m33667_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2075_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33667_GM;
MethodInfo m33667_MI = 
{
	"Insert", NULL, &t6505_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6505_m33667_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33667_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6505_m33668_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33668_GM;
MethodInfo m33668_MI = 
{
	"RemoveAt", NULL, &t6505_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6505_m33668_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33668_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6505_m33664_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2075_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33664_GM;
MethodInfo m33664_MI = 
{
	"get_Item", NULL, &t6505_TI, &t2075_0_0_0, RuntimeInvoker_t29_t44, t6505_m33664_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33664_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2075_0_0_0;
static ParameterInfo t6505_m33665_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2075_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33665_GM;
MethodInfo m33665_MI = 
{
	"set_Item", NULL, &t6505_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6505_m33665_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33665_GM};
static MethodInfo* t6505_MIs[] =
{
	&m33666_MI,
	&m33667_MI,
	&m33668_MI,
	&m33664_MI,
	&m33665_MI,
	NULL
};
static TypeInfo* t6505_ITIs[] = 
{
	&t603_TI,
	&t6504_TI,
	&t6506_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6505_0_0_0;
extern Il2CppType t6505_1_0_0;
struct t6505;
extern Il2CppGenericClass t6505_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6505_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6505_MIs, t6505_PIs, NULL, NULL, NULL, NULL, NULL, &t6505_TI, t6505_ITIs, NULL, &t1908__CustomAttributeCache, &t6505_TI, &t6505_0_0_0, &t6505_1_0_0, NULL, &t6505_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4973_TI;

#include "t1126.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Decimal>
extern MethodInfo m33669_MI;
static PropertyInfo t4973____Current_PropertyInfo = 
{
	&t4973_TI, "Current", &m33669_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4973_PIs[] =
{
	&t4973____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1126_0_0_0;
extern void* RuntimeInvoker_t1126 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33669_GM;
MethodInfo m33669_MI = 
{
	"get_Current", NULL, &t4973_TI, &t1126_0_0_0, RuntimeInvoker_t1126, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33669_GM};
static MethodInfo* t4973_MIs[] =
{
	&m33669_MI,
	NULL
};
static TypeInfo* t4973_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4973_0_0_0;
extern Il2CppType t4973_1_0_0;
struct t4973;
extern Il2CppGenericClass t4973_GC;
TypeInfo t4973_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4973_MIs, t4973_PIs, NULL, NULL, NULL, NULL, NULL, &t4973_TI, t4973_ITIs, NULL, &EmptyCustomAttributesCache, &t4973_TI, &t4973_0_0_0, &t4973_1_0_0, NULL, &t4973_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3440.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3440_TI;
#include "t3440MD.h"

extern TypeInfo t1126_TI;
extern MethodInfo m19074_MI;
extern MethodInfo m25787_MI;
struct t20;
 t1126  m25787 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19070_MI;
 void m19070 (t3440 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19071_MI;
 t29 * m19071 (t3440 * __this, MethodInfo* method){
	{
		t1126  L_0 = m19074(__this, &m19074_MI);
		t1126  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1126_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19072_MI;
 void m19072 (t3440 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19073_MI;
 bool m19073 (t3440 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t1126  m19074 (t3440 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t1126  L_8 = m25787(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25787_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Decimal>
extern Il2CppType t20_0_0_1;
FieldInfo t3440_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3440_TI, offsetof(t3440, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3440_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3440_TI, offsetof(t3440, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3440_FIs[] =
{
	&t3440_f0_FieldInfo,
	&t3440_f1_FieldInfo,
	NULL
};
static PropertyInfo t3440____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3440_TI, "System.Collections.IEnumerator.Current", &m19071_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3440____Current_PropertyInfo = 
{
	&t3440_TI, "Current", &m19074_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3440_PIs[] =
{
	&t3440____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3440____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3440_m19070_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19070_GM;
MethodInfo m19070_MI = 
{
	".ctor", (methodPointerType)&m19070, &t3440_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3440_m19070_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19070_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19071_GM;
MethodInfo m19071_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19071, &t3440_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19071_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19072_GM;
MethodInfo m19072_MI = 
{
	"Dispose", (methodPointerType)&m19072, &t3440_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19072_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19073_GM;
MethodInfo m19073_MI = 
{
	"MoveNext", (methodPointerType)&m19073, &t3440_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19073_GM};
extern Il2CppType t1126_0_0_0;
extern void* RuntimeInvoker_t1126 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19074_GM;
MethodInfo m19074_MI = 
{
	"get_Current", (methodPointerType)&m19074, &t3440_TI, &t1126_0_0_0, RuntimeInvoker_t1126, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19074_GM};
static MethodInfo* t3440_MIs[] =
{
	&m19070_MI,
	&m19071_MI,
	&m19072_MI,
	&m19073_MI,
	&m19074_MI,
	NULL
};
static MethodInfo* t3440_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19071_MI,
	&m19073_MI,
	&m19072_MI,
	&m19074_MI,
};
static TypeInfo* t3440_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4973_TI,
};
static Il2CppInterfaceOffsetPair t3440_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4973_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3440_0_0_0;
extern Il2CppType t3440_1_0_0;
extern Il2CppGenericClass t3440_GC;
TypeInfo t3440_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3440_MIs, t3440_PIs, t3440_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3440_TI, t3440_ITIs, t3440_VT, &EmptyCustomAttributesCache, &t3440_TI, &t3440_0_0_0, &t3440_1_0_0, t3440_IOs, &t3440_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3440)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6507_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Decimal>
extern MethodInfo m33670_MI;
static PropertyInfo t6507____Count_PropertyInfo = 
{
	&t6507_TI, "Count", &m33670_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33671_MI;
static PropertyInfo t6507____IsReadOnly_PropertyInfo = 
{
	&t6507_TI, "IsReadOnly", &m33671_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6507_PIs[] =
{
	&t6507____Count_PropertyInfo,
	&t6507____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33670_GM;
MethodInfo m33670_MI = 
{
	"get_Count", NULL, &t6507_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33670_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33671_GM;
MethodInfo m33671_MI = 
{
	"get_IsReadOnly", NULL, &t6507_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33671_GM};
extern Il2CppType t1126_0_0_0;
extern Il2CppType t1126_0_0_0;
static ParameterInfo t6507_m33672_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t1126 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33672_GM;
MethodInfo m33672_MI = 
{
	"Add", NULL, &t6507_TI, &t21_0_0_0, RuntimeInvoker_t21_t1126, t6507_m33672_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33672_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33673_GM;
MethodInfo m33673_MI = 
{
	"Clear", NULL, &t6507_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33673_GM};
extern Il2CppType t1126_0_0_0;
static ParameterInfo t6507_m33674_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1126 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33674_GM;
MethodInfo m33674_MI = 
{
	"Contains", NULL, &t6507_TI, &t40_0_0_0, RuntimeInvoker_t40_t1126, t6507_m33674_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33674_GM};
extern Il2CppType t2054_0_0_0;
extern Il2CppType t2054_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6507_m33675_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2054_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33675_GM;
MethodInfo m33675_MI = 
{
	"CopyTo", NULL, &t6507_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6507_m33675_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33675_GM};
extern Il2CppType t1126_0_0_0;
static ParameterInfo t6507_m33676_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t1126 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33676_GM;
MethodInfo m33676_MI = 
{
	"Remove", NULL, &t6507_TI, &t40_0_0_0, RuntimeInvoker_t40_t1126, t6507_m33676_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33676_GM};
static MethodInfo* t6507_MIs[] =
{
	&m33670_MI,
	&m33671_MI,
	&m33672_MI,
	&m33673_MI,
	&m33674_MI,
	&m33675_MI,
	&m33676_MI,
	NULL
};
extern TypeInfo t6509_TI;
static TypeInfo* t6507_ITIs[] = 
{
	&t603_TI,
	&t6509_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6507_0_0_0;
extern Il2CppType t6507_1_0_0;
struct t6507;
extern Il2CppGenericClass t6507_GC;
TypeInfo t6507_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6507_MIs, t6507_PIs, NULL, NULL, NULL, NULL, NULL, &t6507_TI, t6507_ITIs, NULL, &EmptyCustomAttributesCache, &t6507_TI, &t6507_0_0_0, &t6507_1_0_0, NULL, &t6507_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Decimal>
extern Il2CppType t4973_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33677_GM;
MethodInfo m33677_MI = 
{
	"GetEnumerator", NULL, &t6509_TI, &t4973_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33677_GM};
static MethodInfo* t6509_MIs[] =
{
	&m33677_MI,
	NULL
};
static TypeInfo* t6509_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6509_0_0_0;
extern Il2CppType t6509_1_0_0;
struct t6509;
extern Il2CppGenericClass t6509_GC;
TypeInfo t6509_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6509_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6509_TI, t6509_ITIs, NULL, &EmptyCustomAttributesCache, &t6509_TI, &t6509_0_0_0, &t6509_1_0_0, NULL, &t6509_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6508_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Decimal>
extern MethodInfo m33678_MI;
extern MethodInfo m33679_MI;
static PropertyInfo t6508____Item_PropertyInfo = 
{
	&t6508_TI, "Item", &m33678_MI, &m33679_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6508_PIs[] =
{
	&t6508____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1126_0_0_0;
static ParameterInfo t6508_m33680_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t1126 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33680_GM;
MethodInfo m33680_MI = 
{
	"IndexOf", NULL, &t6508_TI, &t44_0_0_0, RuntimeInvoker_t44_t1126, t6508_m33680_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33680_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1126_0_0_0;
static ParameterInfo t6508_m33681_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t1126 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33681_GM;
MethodInfo m33681_MI = 
{
	"Insert", NULL, &t6508_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1126, t6508_m33681_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33681_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6508_m33682_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33682_GM;
MethodInfo m33682_MI = 
{
	"RemoveAt", NULL, &t6508_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6508_m33682_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33682_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6508_m33678_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1126_0_0_0;
extern void* RuntimeInvoker_t1126_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33678_GM;
MethodInfo m33678_MI = 
{
	"get_Item", NULL, &t6508_TI, &t1126_0_0_0, RuntimeInvoker_t1126_t44, t6508_m33678_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33678_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1126_0_0_0;
static ParameterInfo t6508_m33679_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t1126 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33679_GM;
MethodInfo m33679_MI = 
{
	"set_Item", NULL, &t6508_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1126, t6508_m33679_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33679_GM};
static MethodInfo* t6508_MIs[] =
{
	&m33680_MI,
	&m33681_MI,
	&m33682_MI,
	&m33678_MI,
	&m33679_MI,
	NULL
};
static TypeInfo* t6508_ITIs[] = 
{
	&t603_TI,
	&t6507_TI,
	&t6509_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6508_0_0_0;
extern Il2CppType t6508_1_0_0;
struct t6508;
extern Il2CppGenericClass t6508_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6508_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6508_MIs, t6508_PIs, NULL, NULL, NULL, NULL, NULL, &t6508_TI, t6508_ITIs, NULL, &t1908__CustomAttributeCache, &t6508_TI, &t6508_0_0_0, &t6508_1_0_0, NULL, &t6508_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6510_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Decimal>>
extern MethodInfo m33683_MI;
static PropertyInfo t6510____Count_PropertyInfo = 
{
	&t6510_TI, "Count", &m33683_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33684_MI;
static PropertyInfo t6510____IsReadOnly_PropertyInfo = 
{
	&t6510_TI, "IsReadOnly", &m33684_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6510_PIs[] =
{
	&t6510____Count_PropertyInfo,
	&t6510____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33683_GM;
MethodInfo m33683_MI = 
{
	"get_Count", NULL, &t6510_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33683_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33684_GM;
MethodInfo m33684_MI = 
{
	"get_IsReadOnly", NULL, &t6510_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33684_GM};
extern Il2CppType t1757_0_0_0;
extern Il2CppType t1757_0_0_0;
static ParameterInfo t6510_m33685_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1757_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33685_GM;
MethodInfo m33685_MI = 
{
	"Add", NULL, &t6510_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6510_m33685_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33685_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33686_GM;
MethodInfo m33686_MI = 
{
	"Clear", NULL, &t6510_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33686_GM};
extern Il2CppType t1757_0_0_0;
static ParameterInfo t6510_m33687_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1757_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33687_GM;
MethodInfo m33687_MI = 
{
	"Contains", NULL, &t6510_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6510_m33687_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33687_GM};
extern Il2CppType t3690_0_0_0;
extern Il2CppType t3690_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6510_m33688_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3690_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33688_GM;
MethodInfo m33688_MI = 
{
	"CopyTo", NULL, &t6510_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6510_m33688_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33688_GM};
extern Il2CppType t1757_0_0_0;
static ParameterInfo t6510_m33689_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1757_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33689_GM;
MethodInfo m33689_MI = 
{
	"Remove", NULL, &t6510_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6510_m33689_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33689_GM};
static MethodInfo* t6510_MIs[] =
{
	&m33683_MI,
	&m33684_MI,
	&m33685_MI,
	&m33686_MI,
	&m33687_MI,
	&m33688_MI,
	&m33689_MI,
	NULL
};
extern TypeInfo t6512_TI;
static TypeInfo* t6510_ITIs[] = 
{
	&t603_TI,
	&t6512_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6510_0_0_0;
extern Il2CppType t6510_1_0_0;
struct t6510;
extern Il2CppGenericClass t6510_GC;
TypeInfo t6510_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6510_MIs, t6510_PIs, NULL, NULL, NULL, NULL, NULL, &t6510_TI, t6510_ITIs, NULL, &EmptyCustomAttributesCache, &t6510_TI, &t6510_0_0_0, &t6510_1_0_0, NULL, &t6510_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Decimal>>
extern Il2CppType t4975_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33690_GM;
MethodInfo m33690_MI = 
{
	"GetEnumerator", NULL, &t6512_TI, &t4975_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33690_GM};
static MethodInfo* t6512_MIs[] =
{
	&m33690_MI,
	NULL
};
static TypeInfo* t6512_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6512_0_0_0;
extern Il2CppType t6512_1_0_0;
struct t6512;
extern Il2CppGenericClass t6512_GC;
TypeInfo t6512_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6512_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6512_TI, t6512_ITIs, NULL, &EmptyCustomAttributesCache, &t6512_TI, &t6512_0_0_0, &t6512_1_0_0, NULL, &t6512_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4975_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Decimal>>
extern MethodInfo m33691_MI;
static PropertyInfo t4975____Current_PropertyInfo = 
{
	&t4975_TI, "Current", &m33691_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4975_PIs[] =
{
	&t4975____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1757_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33691_GM;
MethodInfo m33691_MI = 
{
	"get_Current", NULL, &t4975_TI, &t1757_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33691_GM};
static MethodInfo* t4975_MIs[] =
{
	&m33691_MI,
	NULL
};
static TypeInfo* t4975_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4975_0_0_0;
extern Il2CppType t4975_1_0_0;
struct t4975;
extern Il2CppGenericClass t4975_GC;
TypeInfo t4975_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4975_MIs, t4975_PIs, NULL, NULL, NULL, NULL, NULL, &t4975_TI, t4975_ITIs, NULL, &EmptyCustomAttributesCache, &t4975_TI, &t4975_0_0_0, &t4975_1_0_0, NULL, &t4975_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3441.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3441_TI;
#include "t3441MD.h"

extern TypeInfo t1757_TI;
extern MethodInfo m19079_MI;
extern MethodInfo m25798_MI;
struct t20;
#define m25798(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Decimal>>
extern Il2CppType t20_0_0_1;
FieldInfo t3441_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3441_TI, offsetof(t3441, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3441_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3441_TI, offsetof(t3441, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3441_FIs[] =
{
	&t3441_f0_FieldInfo,
	&t3441_f1_FieldInfo,
	NULL
};
extern MethodInfo m19076_MI;
static PropertyInfo t3441____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3441_TI, "System.Collections.IEnumerator.Current", &m19076_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3441____Current_PropertyInfo = 
{
	&t3441_TI, "Current", &m19079_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3441_PIs[] =
{
	&t3441____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3441____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3441_m19075_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19075_GM;
MethodInfo m19075_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3441_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3441_m19075_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19075_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19076_GM;
MethodInfo m19076_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3441_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19076_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19077_GM;
MethodInfo m19077_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3441_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19077_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19078_GM;
MethodInfo m19078_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3441_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19078_GM};
extern Il2CppType t1757_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19079_GM;
MethodInfo m19079_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3441_TI, &t1757_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19079_GM};
static MethodInfo* t3441_MIs[] =
{
	&m19075_MI,
	&m19076_MI,
	&m19077_MI,
	&m19078_MI,
	&m19079_MI,
	NULL
};
extern MethodInfo m19078_MI;
extern MethodInfo m19077_MI;
static MethodInfo* t3441_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19076_MI,
	&m19078_MI,
	&m19077_MI,
	&m19079_MI,
};
static TypeInfo* t3441_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4975_TI,
};
static Il2CppInterfaceOffsetPair t3441_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4975_TI, 7},
};
extern TypeInfo t1757_TI;
static Il2CppRGCTXData t3441_RGCTXData[3] = 
{
	&m19079_MI/* Method Usage */,
	&t1757_TI/* Class Usage */,
	&m25798_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3441_0_0_0;
extern Il2CppType t3441_1_0_0;
extern Il2CppGenericClass t3441_GC;
TypeInfo t3441_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3441_MIs, t3441_PIs, t3441_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3441_TI, t3441_ITIs, t3441_VT, &EmptyCustomAttributesCache, &t3441_TI, &t3441_0_0_0, &t3441_1_0_0, t3441_IOs, &t3441_GC, NULL, NULL, NULL, t3441_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3441)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6511_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Decimal>>
extern MethodInfo m33692_MI;
extern MethodInfo m33693_MI;
static PropertyInfo t6511____Item_PropertyInfo = 
{
	&t6511_TI, "Item", &m33692_MI, &m33693_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6511_PIs[] =
{
	&t6511____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1757_0_0_0;
static ParameterInfo t6511_m33694_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1757_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33694_GM;
MethodInfo m33694_MI = 
{
	"IndexOf", NULL, &t6511_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6511_m33694_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33694_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1757_0_0_0;
static ParameterInfo t6511_m33695_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1757_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33695_GM;
MethodInfo m33695_MI = 
{
	"Insert", NULL, &t6511_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6511_m33695_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33695_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6511_m33696_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33696_GM;
MethodInfo m33696_MI = 
{
	"RemoveAt", NULL, &t6511_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6511_m33696_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33696_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6511_m33692_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1757_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33692_GM;
MethodInfo m33692_MI = 
{
	"get_Item", NULL, &t6511_TI, &t1757_0_0_0, RuntimeInvoker_t29_t44, t6511_m33692_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33692_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1757_0_0_0;
static ParameterInfo t6511_m33693_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1757_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33693_GM;
MethodInfo m33693_MI = 
{
	"set_Item", NULL, &t6511_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6511_m33693_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33693_GM};
static MethodInfo* t6511_MIs[] =
{
	&m33694_MI,
	&m33695_MI,
	&m33696_MI,
	&m33692_MI,
	&m33693_MI,
	NULL
};
static TypeInfo* t6511_ITIs[] = 
{
	&t603_TI,
	&t6510_TI,
	&t6512_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6511_0_0_0;
extern Il2CppType t6511_1_0_0;
struct t6511;
extern Il2CppGenericClass t6511_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6511_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6511_MIs, t6511_PIs, NULL, NULL, NULL, NULL, NULL, &t6511_TI, t6511_ITIs, NULL, &t1908__CustomAttributeCache, &t6511_TI, &t6511_0_0_0, &t6511_1_0_0, NULL, &t6511_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6513_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Decimal>>
extern MethodInfo m33697_MI;
static PropertyInfo t6513____Count_PropertyInfo = 
{
	&t6513_TI, "Count", &m33697_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33698_MI;
static PropertyInfo t6513____IsReadOnly_PropertyInfo = 
{
	&t6513_TI, "IsReadOnly", &m33698_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6513_PIs[] =
{
	&t6513____Count_PropertyInfo,
	&t6513____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33697_GM;
MethodInfo m33697_MI = 
{
	"get_Count", NULL, &t6513_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33697_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33698_GM;
MethodInfo m33698_MI = 
{
	"get_IsReadOnly", NULL, &t6513_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33698_GM};
extern Il2CppType t1758_0_0_0;
extern Il2CppType t1758_0_0_0;
static ParameterInfo t6513_m33699_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1758_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33699_GM;
MethodInfo m33699_MI = 
{
	"Add", NULL, &t6513_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6513_m33699_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33699_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33700_GM;
MethodInfo m33700_MI = 
{
	"Clear", NULL, &t6513_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33700_GM};
extern Il2CppType t1758_0_0_0;
static ParameterInfo t6513_m33701_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1758_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33701_GM;
MethodInfo m33701_MI = 
{
	"Contains", NULL, &t6513_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6513_m33701_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33701_GM};
extern Il2CppType t3691_0_0_0;
extern Il2CppType t3691_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6513_m33702_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3691_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33702_GM;
MethodInfo m33702_MI = 
{
	"CopyTo", NULL, &t6513_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6513_m33702_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33702_GM};
extern Il2CppType t1758_0_0_0;
static ParameterInfo t6513_m33703_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1758_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33703_GM;
MethodInfo m33703_MI = 
{
	"Remove", NULL, &t6513_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6513_m33703_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33703_GM};
static MethodInfo* t6513_MIs[] =
{
	&m33697_MI,
	&m33698_MI,
	&m33699_MI,
	&m33700_MI,
	&m33701_MI,
	&m33702_MI,
	&m33703_MI,
	NULL
};
extern TypeInfo t6515_TI;
static TypeInfo* t6513_ITIs[] = 
{
	&t603_TI,
	&t6515_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6513_0_0_0;
extern Il2CppType t6513_1_0_0;
struct t6513;
extern Il2CppGenericClass t6513_GC;
TypeInfo t6513_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6513_MIs, t6513_PIs, NULL, NULL, NULL, NULL, NULL, &t6513_TI, t6513_ITIs, NULL, &EmptyCustomAttributesCache, &t6513_TI, &t6513_0_0_0, &t6513_1_0_0, NULL, &t6513_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Decimal>>
extern Il2CppType t4977_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33704_GM;
MethodInfo m33704_MI = 
{
	"GetEnumerator", NULL, &t6515_TI, &t4977_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33704_GM};
static MethodInfo* t6515_MIs[] =
{
	&m33704_MI,
	NULL
};
static TypeInfo* t6515_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6515_0_0_0;
extern Il2CppType t6515_1_0_0;
struct t6515;
extern Il2CppGenericClass t6515_GC;
TypeInfo t6515_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6515_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6515_TI, t6515_ITIs, NULL, &EmptyCustomAttributesCache, &t6515_TI, &t6515_0_0_0, &t6515_1_0_0, NULL, &t6515_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4977_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Decimal>>
extern MethodInfo m33705_MI;
static PropertyInfo t4977____Current_PropertyInfo = 
{
	&t4977_TI, "Current", &m33705_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4977_PIs[] =
{
	&t4977____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1758_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33705_GM;
MethodInfo m33705_MI = 
{
	"get_Current", NULL, &t4977_TI, &t1758_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33705_GM};
static MethodInfo* t4977_MIs[] =
{
	&m33705_MI,
	NULL
};
static TypeInfo* t4977_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4977_0_0_0;
extern Il2CppType t4977_1_0_0;
struct t4977;
extern Il2CppGenericClass t4977_GC;
TypeInfo t4977_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4977_MIs, t4977_PIs, NULL, NULL, NULL, NULL, NULL, &t4977_TI, t4977_ITIs, NULL, &EmptyCustomAttributesCache, &t4977_TI, &t4977_0_0_0, &t4977_1_0_0, NULL, &t4977_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3442.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3442_TI;
#include "t3442MD.h"

extern TypeInfo t1758_TI;
extern MethodInfo m19084_MI;
extern MethodInfo m25809_MI;
struct t20;
#define m25809(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Decimal>>
extern Il2CppType t20_0_0_1;
FieldInfo t3442_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3442_TI, offsetof(t3442, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3442_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3442_TI, offsetof(t3442, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3442_FIs[] =
{
	&t3442_f0_FieldInfo,
	&t3442_f1_FieldInfo,
	NULL
};
extern MethodInfo m19081_MI;
static PropertyInfo t3442____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3442_TI, "System.Collections.IEnumerator.Current", &m19081_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3442____Current_PropertyInfo = 
{
	&t3442_TI, "Current", &m19084_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3442_PIs[] =
{
	&t3442____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3442____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3442_m19080_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19080_GM;
MethodInfo m19080_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3442_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3442_m19080_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19080_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19081_GM;
MethodInfo m19081_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3442_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19081_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19082_GM;
MethodInfo m19082_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3442_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19082_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19083_GM;
MethodInfo m19083_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3442_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19083_GM};
extern Il2CppType t1758_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19084_GM;
MethodInfo m19084_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3442_TI, &t1758_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19084_GM};
static MethodInfo* t3442_MIs[] =
{
	&m19080_MI,
	&m19081_MI,
	&m19082_MI,
	&m19083_MI,
	&m19084_MI,
	NULL
};
extern MethodInfo m19083_MI;
extern MethodInfo m19082_MI;
static MethodInfo* t3442_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19081_MI,
	&m19083_MI,
	&m19082_MI,
	&m19084_MI,
};
static TypeInfo* t3442_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4977_TI,
};
static Il2CppInterfaceOffsetPair t3442_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4977_TI, 7},
};
extern TypeInfo t1758_TI;
static Il2CppRGCTXData t3442_RGCTXData[3] = 
{
	&m19084_MI/* Method Usage */,
	&t1758_TI/* Class Usage */,
	&m25809_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3442_0_0_0;
extern Il2CppType t3442_1_0_0;
extern Il2CppGenericClass t3442_GC;
TypeInfo t3442_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3442_MIs, t3442_PIs, t3442_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3442_TI, t3442_ITIs, t3442_VT, &EmptyCustomAttributesCache, &t3442_TI, &t3442_0_0_0, &t3442_1_0_0, t3442_IOs, &t3442_GC, NULL, NULL, NULL, t3442_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3442)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6514_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Decimal>>
extern MethodInfo m33706_MI;
extern MethodInfo m33707_MI;
static PropertyInfo t6514____Item_PropertyInfo = 
{
	&t6514_TI, "Item", &m33706_MI, &m33707_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6514_PIs[] =
{
	&t6514____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1758_0_0_0;
static ParameterInfo t6514_m33708_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1758_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33708_GM;
MethodInfo m33708_MI = 
{
	"IndexOf", NULL, &t6514_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6514_m33708_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33708_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1758_0_0_0;
static ParameterInfo t6514_m33709_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1758_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33709_GM;
MethodInfo m33709_MI = 
{
	"Insert", NULL, &t6514_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6514_m33709_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33709_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6514_m33710_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33710_GM;
MethodInfo m33710_MI = 
{
	"RemoveAt", NULL, &t6514_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6514_m33710_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33710_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6514_m33706_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1758_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33706_GM;
MethodInfo m33706_MI = 
{
	"get_Item", NULL, &t6514_TI, &t1758_0_0_0, RuntimeInvoker_t29_t44, t6514_m33706_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33706_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1758_0_0_0;
static ParameterInfo t6514_m33707_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1758_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33707_GM;
MethodInfo m33707_MI = 
{
	"set_Item", NULL, &t6514_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6514_m33707_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33707_GM};
static MethodInfo* t6514_MIs[] =
{
	&m33708_MI,
	&m33709_MI,
	&m33710_MI,
	&m33706_MI,
	&m33707_MI,
	NULL
};
static TypeInfo* t6514_ITIs[] = 
{
	&t603_TI,
	&t6513_TI,
	&t6515_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6514_0_0_0;
extern Il2CppType t6514_1_0_0;
struct t6514;
extern Il2CppGenericClass t6514_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6514_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6514_MIs, t6514_PIs, NULL, NULL, NULL, NULL, NULL, &t6514_TI, t6514_ITIs, NULL, &t1908__CustomAttributeCache, &t6514_TI, &t6514_0_0_0, &t6514_1_0_0, NULL, &t6514_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4979_TI;

#include "t816.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.TimeSpan>
extern MethodInfo m33711_MI;
static PropertyInfo t4979____Current_PropertyInfo = 
{
	&t4979_TI, "Current", &m33711_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4979_PIs[] =
{
	&t4979____Current_PropertyInfo,
	NULL
};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33711_GM;
MethodInfo m33711_MI = 
{
	"get_Current", NULL, &t4979_TI, &t816_0_0_0, RuntimeInvoker_t816, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33711_GM};
static MethodInfo* t4979_MIs[] =
{
	&m33711_MI,
	NULL
};
static TypeInfo* t4979_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4979_0_0_0;
extern Il2CppType t4979_1_0_0;
struct t4979;
extern Il2CppGenericClass t4979_GC;
TypeInfo t4979_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4979_MIs, t4979_PIs, NULL, NULL, NULL, NULL, NULL, &t4979_TI, t4979_ITIs, NULL, &EmptyCustomAttributesCache, &t4979_TI, &t4979_0_0_0, &t4979_1_0_0, NULL, &t4979_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3443.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3443_TI;
#include "t3443MD.h"

extern TypeInfo t816_TI;
extern MethodInfo m19089_MI;
extern MethodInfo m25820_MI;
struct t20;
 t816  m25820 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m19085_MI;
 void m19085 (t3443 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19086_MI;
 t29 * m19086 (t3443 * __this, MethodInfo* method){
	{
		t816  L_0 = m19089(__this, &m19089_MI);
		t816  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t816_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m19087_MI;
 void m19087 (t3443 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m19088_MI;
 bool m19088 (t3443 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t816  m19089 (t3443 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t816  L_8 = m25820(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25820_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.TimeSpan>
extern Il2CppType t20_0_0_1;
FieldInfo t3443_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3443_TI, offsetof(t3443, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3443_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3443_TI, offsetof(t3443, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3443_FIs[] =
{
	&t3443_f0_FieldInfo,
	&t3443_f1_FieldInfo,
	NULL
};
static PropertyInfo t3443____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3443_TI, "System.Collections.IEnumerator.Current", &m19086_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3443____Current_PropertyInfo = 
{
	&t3443_TI, "Current", &m19089_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3443_PIs[] =
{
	&t3443____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3443____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3443_m19085_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19085_GM;
MethodInfo m19085_MI = 
{
	".ctor", (methodPointerType)&m19085, &t3443_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3443_m19085_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19085_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19086_GM;
MethodInfo m19086_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m19086, &t3443_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19086_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19087_GM;
MethodInfo m19087_MI = 
{
	"Dispose", (methodPointerType)&m19087, &t3443_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19087_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19088_GM;
MethodInfo m19088_MI = 
{
	"MoveNext", (methodPointerType)&m19088, &t3443_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19088_GM};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19089_GM;
MethodInfo m19089_MI = 
{
	"get_Current", (methodPointerType)&m19089, &t3443_TI, &t816_0_0_0, RuntimeInvoker_t816, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19089_GM};
static MethodInfo* t3443_MIs[] =
{
	&m19085_MI,
	&m19086_MI,
	&m19087_MI,
	&m19088_MI,
	&m19089_MI,
	NULL
};
static MethodInfo* t3443_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19086_MI,
	&m19088_MI,
	&m19087_MI,
	&m19089_MI,
};
static TypeInfo* t3443_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4979_TI,
};
static Il2CppInterfaceOffsetPair t3443_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4979_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3443_0_0_0;
extern Il2CppType t3443_1_0_0;
extern Il2CppGenericClass t3443_GC;
TypeInfo t3443_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3443_MIs, t3443_PIs, t3443_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3443_TI, t3443_ITIs, t3443_VT, &EmptyCustomAttributesCache, &t3443_TI, &t3443_0_0_0, &t3443_1_0_0, t3443_IOs, &t3443_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3443)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6516_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.TimeSpan>
extern MethodInfo m33712_MI;
static PropertyInfo t6516____Count_PropertyInfo = 
{
	&t6516_TI, "Count", &m33712_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33713_MI;
static PropertyInfo t6516____IsReadOnly_PropertyInfo = 
{
	&t6516_TI, "IsReadOnly", &m33713_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6516_PIs[] =
{
	&t6516____Count_PropertyInfo,
	&t6516____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33712_GM;
MethodInfo m33712_MI = 
{
	"get_Count", NULL, &t6516_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33712_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33713_GM;
MethodInfo m33713_MI = 
{
	"get_IsReadOnly", NULL, &t6516_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33713_GM};
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t6516_m33714_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33714_GM;
MethodInfo m33714_MI = 
{
	"Add", NULL, &t6516_TI, &t21_0_0_0, RuntimeInvoker_t21_t816, t6516_m33714_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33714_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33715_GM;
MethodInfo m33715_MI = 
{
	"Clear", NULL, &t6516_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33715_GM};
extern Il2CppType t816_0_0_0;
static ParameterInfo t6516_m33716_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33716_GM;
MethodInfo m33716_MI = 
{
	"Contains", NULL, &t6516_TI, &t40_0_0_0, RuntimeInvoker_t40_t816, t6516_m33716_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33716_GM};
extern Il2CppType t2055_0_0_0;
extern Il2CppType t2055_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6516_m33717_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2055_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33717_GM;
MethodInfo m33717_MI = 
{
	"CopyTo", NULL, &t6516_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6516_m33717_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33717_GM};
extern Il2CppType t816_0_0_0;
static ParameterInfo t6516_m33718_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33718_GM;
MethodInfo m33718_MI = 
{
	"Remove", NULL, &t6516_TI, &t40_0_0_0, RuntimeInvoker_t40_t816, t6516_m33718_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33718_GM};
static MethodInfo* t6516_MIs[] =
{
	&m33712_MI,
	&m33713_MI,
	&m33714_MI,
	&m33715_MI,
	&m33716_MI,
	&m33717_MI,
	&m33718_MI,
	NULL
};
extern TypeInfo t6518_TI;
static TypeInfo* t6516_ITIs[] = 
{
	&t603_TI,
	&t6518_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6516_0_0_0;
extern Il2CppType t6516_1_0_0;
struct t6516;
extern Il2CppGenericClass t6516_GC;
TypeInfo t6516_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6516_MIs, t6516_PIs, NULL, NULL, NULL, NULL, NULL, &t6516_TI, t6516_ITIs, NULL, &EmptyCustomAttributesCache, &t6516_TI, &t6516_0_0_0, &t6516_1_0_0, NULL, &t6516_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.TimeSpan>
extern Il2CppType t4979_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33719_GM;
MethodInfo m33719_MI = 
{
	"GetEnumerator", NULL, &t6518_TI, &t4979_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33719_GM};
static MethodInfo* t6518_MIs[] =
{
	&m33719_MI,
	NULL
};
static TypeInfo* t6518_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6518_0_0_0;
extern Il2CppType t6518_1_0_0;
struct t6518;
extern Il2CppGenericClass t6518_GC;
TypeInfo t6518_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6518_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6518_TI, t6518_ITIs, NULL, &EmptyCustomAttributesCache, &t6518_TI, &t6518_0_0_0, &t6518_1_0_0, NULL, &t6518_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6517_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.TimeSpan>
extern MethodInfo m33720_MI;
extern MethodInfo m33721_MI;
static PropertyInfo t6517____Item_PropertyInfo = 
{
	&t6517_TI, "Item", &m33720_MI, &m33721_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6517_PIs[] =
{
	&t6517____Item_PropertyInfo,
	NULL
};
extern Il2CppType t816_0_0_0;
static ParameterInfo t6517_m33722_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33722_GM;
MethodInfo m33722_MI = 
{
	"IndexOf", NULL, &t6517_TI, &t44_0_0_0, RuntimeInvoker_t44_t816, t6517_m33722_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33722_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t6517_m33723_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33723_GM;
MethodInfo m33723_MI = 
{
	"Insert", NULL, &t6517_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t816, t6517_m33723_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33723_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6517_m33724_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33724_GM;
MethodInfo m33724_MI = 
{
	"RemoveAt", NULL, &t6517_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6517_m33724_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33724_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6517_m33720_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t816_0_0_0;
extern void* RuntimeInvoker_t816_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33720_GM;
MethodInfo m33720_MI = 
{
	"get_Item", NULL, &t6517_TI, &t816_0_0_0, RuntimeInvoker_t816_t44, t6517_m33720_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33720_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t6517_m33721_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33721_GM;
MethodInfo m33721_MI = 
{
	"set_Item", NULL, &t6517_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t816, t6517_m33721_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33721_GM};
static MethodInfo* t6517_MIs[] =
{
	&m33722_MI,
	&m33723_MI,
	&m33724_MI,
	&m33720_MI,
	&m33721_MI,
	NULL
};
static TypeInfo* t6517_ITIs[] = 
{
	&t603_TI,
	&t6516_TI,
	&t6518_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6517_0_0_0;
extern Il2CppType t6517_1_0_0;
struct t6517;
extern Il2CppGenericClass t6517_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6517_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6517_MIs, t6517_PIs, NULL, NULL, NULL, NULL, NULL, &t6517_TI, t6517_ITIs, NULL, &t1908__CustomAttributeCache, &t6517_TI, &t6517_0_0_0, &t6517_1_0_0, NULL, &t6517_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6519_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.TimeSpan>>
extern MethodInfo m33725_MI;
static PropertyInfo t6519____Count_PropertyInfo = 
{
	&t6519_TI, "Count", &m33725_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33726_MI;
static PropertyInfo t6519____IsReadOnly_PropertyInfo = 
{
	&t6519_TI, "IsReadOnly", &m33726_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6519_PIs[] =
{
	&t6519____Count_PropertyInfo,
	&t6519____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33725_GM;
MethodInfo m33725_MI = 
{
	"get_Count", NULL, &t6519_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33725_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33726_GM;
MethodInfo m33726_MI = 
{
	"get_IsReadOnly", NULL, &t6519_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33726_GM};
extern Il2CppType t2099_0_0_0;
extern Il2CppType t2099_0_0_0;
static ParameterInfo t6519_m33727_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2099_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33727_GM;
MethodInfo m33727_MI = 
{
	"Add", NULL, &t6519_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6519_m33727_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33727_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33728_GM;
MethodInfo m33728_MI = 
{
	"Clear", NULL, &t6519_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33728_GM};
extern Il2CppType t2099_0_0_0;
static ParameterInfo t6519_m33729_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2099_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33729_GM;
MethodInfo m33729_MI = 
{
	"Contains", NULL, &t6519_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6519_m33729_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33729_GM};
extern Il2CppType t3692_0_0_0;
extern Il2CppType t3692_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6519_m33730_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3692_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33730_GM;
MethodInfo m33730_MI = 
{
	"CopyTo", NULL, &t6519_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6519_m33730_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33730_GM};
extern Il2CppType t2099_0_0_0;
static ParameterInfo t6519_m33731_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2099_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33731_GM;
MethodInfo m33731_MI = 
{
	"Remove", NULL, &t6519_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6519_m33731_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33731_GM};
static MethodInfo* t6519_MIs[] =
{
	&m33725_MI,
	&m33726_MI,
	&m33727_MI,
	&m33728_MI,
	&m33729_MI,
	&m33730_MI,
	&m33731_MI,
	NULL
};
extern TypeInfo t6521_TI;
static TypeInfo* t6519_ITIs[] = 
{
	&t603_TI,
	&t6521_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6519_0_0_0;
extern Il2CppType t6519_1_0_0;
struct t6519;
extern Il2CppGenericClass t6519_GC;
TypeInfo t6519_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6519_MIs, t6519_PIs, NULL, NULL, NULL, NULL, NULL, &t6519_TI, t6519_ITIs, NULL, &EmptyCustomAttributesCache, &t6519_TI, &t6519_0_0_0, &t6519_1_0_0, NULL, &t6519_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.TimeSpan>>
extern Il2CppType t4981_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33732_GM;
MethodInfo m33732_MI = 
{
	"GetEnumerator", NULL, &t6521_TI, &t4981_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33732_GM};
static MethodInfo* t6521_MIs[] =
{
	&m33732_MI,
	NULL
};
static TypeInfo* t6521_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6521_0_0_0;
extern Il2CppType t6521_1_0_0;
struct t6521;
extern Il2CppGenericClass t6521_GC;
TypeInfo t6521_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6521_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6521_TI, t6521_ITIs, NULL, &EmptyCustomAttributesCache, &t6521_TI, &t6521_0_0_0, &t6521_1_0_0, NULL, &t6521_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4981_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.TimeSpan>>
extern MethodInfo m33733_MI;
static PropertyInfo t4981____Current_PropertyInfo = 
{
	&t4981_TI, "Current", &m33733_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4981_PIs[] =
{
	&t4981____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2099_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33733_GM;
MethodInfo m33733_MI = 
{
	"get_Current", NULL, &t4981_TI, &t2099_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33733_GM};
static MethodInfo* t4981_MIs[] =
{
	&m33733_MI,
	NULL
};
static TypeInfo* t4981_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4981_0_0_0;
extern Il2CppType t4981_1_0_0;
struct t4981;
extern Il2CppGenericClass t4981_GC;
TypeInfo t4981_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4981_MIs, t4981_PIs, NULL, NULL, NULL, NULL, NULL, &t4981_TI, t4981_ITIs, NULL, &EmptyCustomAttributesCache, &t4981_TI, &t4981_0_0_0, &t4981_1_0_0, NULL, &t4981_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2099_TI;



// Metadata Definition System.IComparable`1<System.TimeSpan>
extern Il2CppType t816_0_0_0;
static ParameterInfo t2099_m33734_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t816 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33734_GM;
MethodInfo m33734_MI = 
{
	"CompareTo", NULL, &t2099_TI, &t44_0_0_0, RuntimeInvoker_t44_t816, t2099_m33734_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33734_GM};
static MethodInfo* t2099_MIs[] =
{
	&m33734_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2099_1_0_0;
struct t2099;
extern Il2CppGenericClass t2099_GC;
TypeInfo t2099_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t2099_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2099_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2099_TI, &t2099_0_0_0, &t2099_1_0_0, NULL, &t2099_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3444.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3444_TI;
#include "t3444MD.h"

extern MethodInfo m19094_MI;
extern MethodInfo m25831_MI;
struct t20;
#define m25831(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.TimeSpan>>
extern Il2CppType t20_0_0_1;
FieldInfo t3444_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3444_TI, offsetof(t3444, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3444_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3444_TI, offsetof(t3444, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3444_FIs[] =
{
	&t3444_f0_FieldInfo,
	&t3444_f1_FieldInfo,
	NULL
};
extern MethodInfo m19091_MI;
static PropertyInfo t3444____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3444_TI, "System.Collections.IEnumerator.Current", &m19091_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3444____Current_PropertyInfo = 
{
	&t3444_TI, "Current", &m19094_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3444_PIs[] =
{
	&t3444____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3444____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3444_m19090_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19090_GM;
MethodInfo m19090_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3444_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3444_m19090_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m19090_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19091_GM;
MethodInfo m19091_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3444_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19091_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19092_GM;
MethodInfo m19092_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3444_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19092_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19093_GM;
MethodInfo m19093_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3444_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19093_GM};
extern Il2CppType t2099_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m19094_GM;
MethodInfo m19094_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3444_TI, &t2099_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m19094_GM};
static MethodInfo* t3444_MIs[] =
{
	&m19090_MI,
	&m19091_MI,
	&m19092_MI,
	&m19093_MI,
	&m19094_MI,
	NULL
};
extern MethodInfo m19093_MI;
extern MethodInfo m19092_MI;
static MethodInfo* t3444_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m19091_MI,
	&m19093_MI,
	&m19092_MI,
	&m19094_MI,
};
static TypeInfo* t3444_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4981_TI,
};
static Il2CppInterfaceOffsetPair t3444_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4981_TI, 7},
};
extern TypeInfo t2099_TI;
static Il2CppRGCTXData t3444_RGCTXData[3] = 
{
	&m19094_MI/* Method Usage */,
	&t2099_TI/* Class Usage */,
	&m25831_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3444_0_0_0;
extern Il2CppType t3444_1_0_0;
extern Il2CppGenericClass t3444_GC;
TypeInfo t3444_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3444_MIs, t3444_PIs, t3444_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3444_TI, t3444_ITIs, t3444_VT, &EmptyCustomAttributesCache, &t3444_TI, &t3444_0_0_0, &t3444_1_0_0, t3444_IOs, &t3444_GC, NULL, NULL, NULL, t3444_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3444)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6520_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.TimeSpan>>
extern MethodInfo m33735_MI;
extern MethodInfo m33736_MI;
static PropertyInfo t6520____Item_PropertyInfo = 
{
	&t6520_TI, "Item", &m33735_MI, &m33736_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6520_PIs[] =
{
	&t6520____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2099_0_0_0;
static ParameterInfo t6520_m33737_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2099_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33737_GM;
MethodInfo m33737_MI = 
{
	"IndexOf", NULL, &t6520_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6520_m33737_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33737_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2099_0_0_0;
static ParameterInfo t6520_m33738_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2099_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33738_GM;
MethodInfo m33738_MI = 
{
	"Insert", NULL, &t6520_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6520_m33738_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33738_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6520_m33739_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33739_GM;
MethodInfo m33739_MI = 
{
	"RemoveAt", NULL, &t6520_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6520_m33739_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33739_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6520_m33735_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2099_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33735_GM;
MethodInfo m33735_MI = 
{
	"get_Item", NULL, &t6520_TI, &t2099_0_0_0, RuntimeInvoker_t29_t44, t6520_m33735_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33735_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2099_0_0_0;
static ParameterInfo t6520_m33736_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2099_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33736_GM;
MethodInfo m33736_MI = 
{
	"set_Item", NULL, &t6520_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6520_m33736_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33736_GM};
static MethodInfo* t6520_MIs[] =
{
	&m33737_MI,
	&m33738_MI,
	&m33739_MI,
	&m33735_MI,
	&m33736_MI,
	NULL
};
static TypeInfo* t6520_ITIs[] = 
{
	&t603_TI,
	&t6519_TI,
	&t6521_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6520_0_0_0;
extern Il2CppType t6520_1_0_0;
struct t6520;
extern Il2CppGenericClass t6520_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6520_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6520_MIs, t6520_PIs, NULL, NULL, NULL, NULL, NULL, &t6520_TI, t6520_ITIs, NULL, &t1908__CustomAttributeCache, &t6520_TI, &t6520_0_0_0, &t6520_1_0_0, NULL, &t6520_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6522_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.TimeSpan>>
extern MethodInfo m33740_MI;
static PropertyInfo t6522____Count_PropertyInfo = 
{
	&t6522_TI, "Count", &m33740_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33741_MI;
static PropertyInfo t6522____IsReadOnly_PropertyInfo = 
{
	&t6522_TI, "IsReadOnly", &m33741_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6522_PIs[] =
{
	&t6522____Count_PropertyInfo,
	&t6522____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33740_GM;
MethodInfo m33740_MI = 
{
	"get_Count", NULL, &t6522_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33740_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33741_GM;
MethodInfo m33741_MI = 
{
	"get_IsReadOnly", NULL, &t6522_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33741_GM};
extern Il2CppType t2100_0_0_0;
extern Il2CppType t2100_0_0_0;
static ParameterInfo t6522_m33742_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2100_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33742_GM;
MethodInfo m33742_MI = 
{
	"Add", NULL, &t6522_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6522_m33742_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33742_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33743_GM;
MethodInfo m33743_MI = 
{
	"Clear", NULL, &t6522_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33743_GM};
extern Il2CppType t2100_0_0_0;
static ParameterInfo t6522_m33744_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2100_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33744_GM;
MethodInfo m33744_MI = 
{
	"Contains", NULL, &t6522_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6522_m33744_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33744_GM};
extern Il2CppType t3693_0_0_0;
extern Il2CppType t3693_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6522_m33745_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3693_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33745_GM;
MethodInfo m33745_MI = 
{
	"CopyTo", NULL, &t6522_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6522_m33745_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33745_GM};
extern Il2CppType t2100_0_0_0;
static ParameterInfo t6522_m33746_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2100_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33746_GM;
MethodInfo m33746_MI = 
{
	"Remove", NULL, &t6522_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6522_m33746_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33746_GM};
static MethodInfo* t6522_MIs[] =
{
	&m33740_MI,
	&m33741_MI,
	&m33742_MI,
	&m33743_MI,
	&m33744_MI,
	&m33745_MI,
	&m33746_MI,
	NULL
};
extern TypeInfo t6524_TI;
static TypeInfo* t6522_ITIs[] = 
{
	&t603_TI,
	&t6524_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6522_0_0_0;
extern Il2CppType t6522_1_0_0;
struct t6522;
extern Il2CppGenericClass t6522_GC;
TypeInfo t6522_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6522_MIs, t6522_PIs, NULL, NULL, NULL, NULL, NULL, &t6522_TI, t6522_ITIs, NULL, &EmptyCustomAttributesCache, &t6522_TI, &t6522_0_0_0, &t6522_1_0_0, NULL, &t6522_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.TimeSpan>>
extern Il2CppType t4983_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33747_GM;
MethodInfo m33747_MI = 
{
	"GetEnumerator", NULL, &t6524_TI, &t4983_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33747_GM};
static MethodInfo* t6524_MIs[] =
{
	&m33747_MI,
	NULL
};
static TypeInfo* t6524_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6524_0_0_0;
extern Il2CppType t6524_1_0_0;
struct t6524;
extern Il2CppGenericClass t6524_GC;
TypeInfo t6524_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6524_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6524_TI, t6524_ITIs, NULL, &EmptyCustomAttributesCache, &t6524_TI, &t6524_0_0_0, &t6524_1_0_0, NULL, &t6524_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
