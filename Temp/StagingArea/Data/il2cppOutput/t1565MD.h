﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1565;
struct t29;
struct t136;

 void m8472 (t1565 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8473 (t1565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8474 (t1565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
