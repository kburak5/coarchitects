﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t35;
struct t733;
struct t29;
struct t7;
#include "t21.h"
#include "t735.h"
#include "t35.h"

 void m2887 (t35* __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5807 (t35* __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5808 (t35* __this, void* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5809 (t35* __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5810 (t35* __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5811 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5812 (t35* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5813 (t35* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5814 (t35* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5815 (t35* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5816 (t35* __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2949 (t29 * __this, t35 p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2888 (t29 * __this, t35 p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m5817 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m5818 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m5819 (t29 * __this, void* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2948 (t29 * __this, t35 p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void* m5820 (t29 * __this, t35 p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
