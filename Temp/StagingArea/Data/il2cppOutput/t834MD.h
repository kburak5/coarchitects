﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t834;
struct t833;
struct t29;
struct t20;
struct t136;

 void m3523 (t834 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3524 (t834 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3525 (t834 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t833 * m3526 (t834 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3527 (t834 * __this, t833 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3528 (t834 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3529 (t834 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3530 (t834 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
