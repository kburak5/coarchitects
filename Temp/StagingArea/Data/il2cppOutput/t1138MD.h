﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1138;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m5890 (t1138 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5891 (t1138 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5892 (t1138 * __this, int32_t p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5893 (t1138 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
