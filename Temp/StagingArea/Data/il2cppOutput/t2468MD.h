﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2468;
struct t29;
struct t20;
#include "t2465.h"

 void m12923 (t2468 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12924 (t2468 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12925 (t2468 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12926 (t2468 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2465  m12927 (t2468 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
