﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t956;
struct t7;
struct t733;
struct t295;
#include "t735.h"

 void m9516 (t956 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4202 (t956 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9517 (t956 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9518 (t956 * __this, t7* p0, t295 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
