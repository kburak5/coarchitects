﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t4907_TI;

#include "t1398.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>
extern MethodInfo m33191_MI;
static PropertyInfo t4907____Current_PropertyInfo = 
{
	&t4907_TI, "Current", &m33191_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4907_PIs[] =
{
	&t4907____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1398_0_0_0;
extern void* RuntimeInvoker_t1398 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33191_GM;
MethodInfo m33191_MI = 
{
	"get_Current", NULL, &t4907_TI, &t1398_0_0_0, RuntimeInvoker_t1398, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33191_GM};
static MethodInfo* t4907_MIs[] =
{
	&m33191_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4907_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4907_0_0_0;
extern Il2CppType t4907_1_0_0;
struct t4907;
extern Il2CppGenericClass t4907_GC;
TypeInfo t4907_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4907_MIs, t4907_PIs, NULL, NULL, NULL, NULL, NULL, &t4907_TI, t4907_ITIs, NULL, &EmptyCustomAttributesCache, &t4907_TI, &t4907_0_0_0, &t4907_1_0_0, NULL, &t4907_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3406.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3406_TI;
#include "t3406MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t40.h"
extern TypeInfo t1398_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m18904_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m25413_MI;
struct t20;
#include "t915.h"
 int32_t m25413 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18900_MI;
 void m18900 (t3406 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18901_MI;
 t29 * m18901 (t3406 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18904(__this, &m18904_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1398_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18902_MI;
 void m18902 (t3406 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18903_MI;
 bool m18903 (t3406 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18904 (t3406 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25413(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25413_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Consistency>
extern Il2CppType t20_0_0_1;
FieldInfo t3406_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3406_TI, offsetof(t3406, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3406_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3406_TI, offsetof(t3406, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3406_FIs[] =
{
	&t3406_f0_FieldInfo,
	&t3406_f1_FieldInfo,
	NULL
};
static PropertyInfo t3406____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3406_TI, "System.Collections.IEnumerator.Current", &m18901_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3406____Current_PropertyInfo = 
{
	&t3406_TI, "Current", &m18904_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3406_PIs[] =
{
	&t3406____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3406____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3406_m18900_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18900_GM;
MethodInfo m18900_MI = 
{
	".ctor", (methodPointerType)&m18900, &t3406_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3406_m18900_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18900_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18901_GM;
MethodInfo m18901_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18901, &t3406_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18901_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18902_GM;
MethodInfo m18902_MI = 
{
	"Dispose", (methodPointerType)&m18902, &t3406_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18902_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18903_GM;
MethodInfo m18903_MI = 
{
	"MoveNext", (methodPointerType)&m18903, &t3406_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18903_GM};
extern Il2CppType t1398_0_0_0;
extern void* RuntimeInvoker_t1398 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18904_GM;
MethodInfo m18904_MI = 
{
	"get_Current", (methodPointerType)&m18904, &t3406_TI, &t1398_0_0_0, RuntimeInvoker_t1398, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18904_GM};
static MethodInfo* t3406_MIs[] =
{
	&m18900_MI,
	&m18901_MI,
	&m18902_MI,
	&m18903_MI,
	&m18904_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t3406_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18901_MI,
	&m18903_MI,
	&m18902_MI,
	&m18904_MI,
};
static TypeInfo* t3406_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4907_TI,
};
static Il2CppInterfaceOffsetPair t3406_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4907_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3406_0_0_0;
extern Il2CppType t3406_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3406_GC;
extern TypeInfo t20_TI;
TypeInfo t3406_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3406_MIs, t3406_PIs, t3406_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3406_TI, t3406_ITIs, t3406_VT, &EmptyCustomAttributesCache, &t3406_TI, &t3406_0_0_0, &t3406_1_0_0, t3406_IOs, &t3406_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3406)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6405_TI;

#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Consistency>
extern MethodInfo m33192_MI;
static PropertyInfo t6405____Count_PropertyInfo = 
{
	&t6405_TI, "Count", &m33192_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33193_MI;
static PropertyInfo t6405____IsReadOnly_PropertyInfo = 
{
	&t6405_TI, "IsReadOnly", &m33193_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6405_PIs[] =
{
	&t6405____Count_PropertyInfo,
	&t6405____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33192_GM;
MethodInfo m33192_MI = 
{
	"get_Count", NULL, &t6405_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33192_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33193_GM;
MethodInfo m33193_MI = 
{
	"get_IsReadOnly", NULL, &t6405_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33193_GM};
extern Il2CppType t1398_0_0_0;
extern Il2CppType t1398_0_0_0;
static ParameterInfo t6405_m33194_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1398_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33194_GM;
MethodInfo m33194_MI = 
{
	"Add", NULL, &t6405_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6405_m33194_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33194_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33195_GM;
MethodInfo m33195_MI = 
{
	"Clear", NULL, &t6405_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33195_GM};
extern Il2CppType t1398_0_0_0;
static ParameterInfo t6405_m33196_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1398_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33196_GM;
MethodInfo m33196_MI = 
{
	"Contains", NULL, &t6405_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6405_m33196_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33196_GM};
extern Il2CppType t3661_0_0_0;
extern Il2CppType t3661_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6405_m33197_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3661_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33197_GM;
MethodInfo m33197_MI = 
{
	"CopyTo", NULL, &t6405_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6405_m33197_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33197_GM};
extern Il2CppType t1398_0_0_0;
static ParameterInfo t6405_m33198_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1398_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33198_GM;
MethodInfo m33198_MI = 
{
	"Remove", NULL, &t6405_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6405_m33198_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33198_GM};
static MethodInfo* t6405_MIs[] =
{
	&m33192_MI,
	&m33193_MI,
	&m33194_MI,
	&m33195_MI,
	&m33196_MI,
	&m33197_MI,
	&m33198_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t6407_TI;
static TypeInfo* t6405_ITIs[] = 
{
	&t603_TI,
	&t6407_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6405_0_0_0;
extern Il2CppType t6405_1_0_0;
struct t6405;
extern Il2CppGenericClass t6405_GC;
TypeInfo t6405_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6405_MIs, t6405_PIs, NULL, NULL, NULL, NULL, NULL, &t6405_TI, t6405_ITIs, NULL, &EmptyCustomAttributesCache, &t6405_TI, &t6405_0_0_0, &t6405_1_0_0, NULL, &t6405_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.ConstrainedExecution.Consistency>
extern Il2CppType t4907_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33199_GM;
MethodInfo m33199_MI = 
{
	"GetEnumerator", NULL, &t6407_TI, &t4907_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33199_GM};
static MethodInfo* t6407_MIs[] =
{
	&m33199_MI,
	NULL
};
static TypeInfo* t6407_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6407_0_0_0;
extern Il2CppType t6407_1_0_0;
struct t6407;
extern Il2CppGenericClass t6407_GC;
TypeInfo t6407_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6407_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6407_TI, t6407_ITIs, NULL, &EmptyCustomAttributesCache, &t6407_TI, &t6407_0_0_0, &t6407_1_0_0, NULL, &t6407_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6406_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Consistency>
extern MethodInfo m33200_MI;
extern MethodInfo m33201_MI;
static PropertyInfo t6406____Item_PropertyInfo = 
{
	&t6406_TI, "Item", &m33200_MI, &m33201_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6406_PIs[] =
{
	&t6406____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1398_0_0_0;
static ParameterInfo t6406_m33202_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1398_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33202_GM;
MethodInfo m33202_MI = 
{
	"IndexOf", NULL, &t6406_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6406_m33202_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33202_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1398_0_0_0;
static ParameterInfo t6406_m33203_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1398_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33203_GM;
MethodInfo m33203_MI = 
{
	"Insert", NULL, &t6406_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6406_m33203_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33203_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6406_m33204_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33204_GM;
MethodInfo m33204_MI = 
{
	"RemoveAt", NULL, &t6406_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6406_m33204_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33204_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6406_m33200_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1398_0_0_0;
extern void* RuntimeInvoker_t1398_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33200_GM;
MethodInfo m33200_MI = 
{
	"get_Item", NULL, &t6406_TI, &t1398_0_0_0, RuntimeInvoker_t1398_t44, t6406_m33200_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33200_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1398_0_0_0;
static ParameterInfo t6406_m33201_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1398_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33201_GM;
MethodInfo m33201_MI = 
{
	"set_Item", NULL, &t6406_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6406_m33201_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33201_GM};
static MethodInfo* t6406_MIs[] =
{
	&m33202_MI,
	&m33203_MI,
	&m33204_MI,
	&m33200_MI,
	&m33201_MI,
	NULL
};
static TypeInfo* t6406_ITIs[] = 
{
	&t603_TI,
	&t6405_TI,
	&t6407_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6406_0_0_0;
extern Il2CppType t6406_1_0_0;
struct t6406;
extern Il2CppGenericClass t6406_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6406_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6406_MIs, t6406_PIs, NULL, NULL, NULL, NULL, NULL, &t6406_TI, t6406_ITIs, NULL, &t1908__CustomAttributeCache, &t6406_TI, &t6406_0_0_0, &t6406_1_0_0, NULL, &t6406_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4909_TI;

#include "t1400.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>
extern MethodInfo m33205_MI;
static PropertyInfo t4909____Current_PropertyInfo = 
{
	&t4909_TI, "Current", &m33205_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4909_PIs[] =
{
	&t4909____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1400_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33205_GM;
MethodInfo m33205_MI = 
{
	"get_Current", NULL, &t4909_TI, &t1400_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33205_GM};
static MethodInfo* t4909_MIs[] =
{
	&m33205_MI,
	NULL
};
static TypeInfo* t4909_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4909_0_0_0;
extern Il2CppType t4909_1_0_0;
struct t4909;
extern Il2CppGenericClass t4909_GC;
TypeInfo t4909_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4909_MIs, t4909_PIs, NULL, NULL, NULL, NULL, NULL, &t4909_TI, t4909_ITIs, NULL, &EmptyCustomAttributesCache, &t4909_TI, &t4909_0_0_0, &t4909_1_0_0, NULL, &t4909_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3407.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3407_TI;
#include "t3407MD.h"

extern TypeInfo t1400_TI;
extern MethodInfo m18909_MI;
extern MethodInfo m25424_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m25424(__this, p0, method) (t1400 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3407_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3407_TI, offsetof(t3407, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3407_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3407_TI, offsetof(t3407, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3407_FIs[] =
{
	&t3407_f0_FieldInfo,
	&t3407_f1_FieldInfo,
	NULL
};
extern MethodInfo m18906_MI;
static PropertyInfo t3407____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3407_TI, "System.Collections.IEnumerator.Current", &m18906_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3407____Current_PropertyInfo = 
{
	&t3407_TI, "Current", &m18909_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3407_PIs[] =
{
	&t3407____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3407____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3407_m18905_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18905_GM;
MethodInfo m18905_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3407_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3407_m18905_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18905_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18906_GM;
MethodInfo m18906_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3407_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18906_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18907_GM;
MethodInfo m18907_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3407_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18907_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18908_GM;
MethodInfo m18908_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3407_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18908_GM};
extern Il2CppType t1400_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18909_GM;
MethodInfo m18909_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3407_TI, &t1400_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18909_GM};
static MethodInfo* t3407_MIs[] =
{
	&m18905_MI,
	&m18906_MI,
	&m18907_MI,
	&m18908_MI,
	&m18909_MI,
	NULL
};
extern MethodInfo m18908_MI;
extern MethodInfo m18907_MI;
static MethodInfo* t3407_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18906_MI,
	&m18908_MI,
	&m18907_MI,
	&m18909_MI,
};
static TypeInfo* t3407_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4909_TI,
};
static Il2CppInterfaceOffsetPair t3407_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4909_TI, 7},
};
extern TypeInfo t1400_TI;
static Il2CppRGCTXData t3407_RGCTXData[3] = 
{
	&m18909_MI/* Method Usage */,
	&t1400_TI/* Class Usage */,
	&m25424_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3407_0_0_0;
extern Il2CppType t3407_1_0_0;
extern Il2CppGenericClass t3407_GC;
TypeInfo t3407_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3407_MIs, t3407_PIs, t3407_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3407_TI, t3407_ITIs, t3407_VT, &EmptyCustomAttributesCache, &t3407_TI, &t3407_0_0_0, &t3407_1_0_0, t3407_IOs, &t3407_GC, NULL, NULL, NULL, t3407_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3407)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6408_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>
extern MethodInfo m33206_MI;
static PropertyInfo t6408____Count_PropertyInfo = 
{
	&t6408_TI, "Count", &m33206_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33207_MI;
static PropertyInfo t6408____IsReadOnly_PropertyInfo = 
{
	&t6408_TI, "IsReadOnly", &m33207_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6408_PIs[] =
{
	&t6408____Count_PropertyInfo,
	&t6408____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33206_GM;
MethodInfo m33206_MI = 
{
	"get_Count", NULL, &t6408_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33206_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33207_GM;
MethodInfo m33207_MI = 
{
	"get_IsReadOnly", NULL, &t6408_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33207_GM};
extern Il2CppType t1400_0_0_0;
extern Il2CppType t1400_0_0_0;
static ParameterInfo t6408_m33208_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1400_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33208_GM;
MethodInfo m33208_MI = 
{
	"Add", NULL, &t6408_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6408_m33208_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33208_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33209_GM;
MethodInfo m33209_MI = 
{
	"Clear", NULL, &t6408_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33209_GM};
extern Il2CppType t1400_0_0_0;
static ParameterInfo t6408_m33210_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1400_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33210_GM;
MethodInfo m33210_MI = 
{
	"Contains", NULL, &t6408_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6408_m33210_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33210_GM};
extern Il2CppType t3662_0_0_0;
extern Il2CppType t3662_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6408_m33211_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3662_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33211_GM;
MethodInfo m33211_MI = 
{
	"CopyTo", NULL, &t6408_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6408_m33211_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33211_GM};
extern Il2CppType t1400_0_0_0;
static ParameterInfo t6408_m33212_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1400_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33212_GM;
MethodInfo m33212_MI = 
{
	"Remove", NULL, &t6408_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6408_m33212_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33212_GM};
static MethodInfo* t6408_MIs[] =
{
	&m33206_MI,
	&m33207_MI,
	&m33208_MI,
	&m33209_MI,
	&m33210_MI,
	&m33211_MI,
	&m33212_MI,
	NULL
};
extern TypeInfo t6410_TI;
static TypeInfo* t6408_ITIs[] = 
{
	&t603_TI,
	&t6410_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6408_0_0_0;
extern Il2CppType t6408_1_0_0;
struct t6408;
extern Il2CppGenericClass t6408_GC;
TypeInfo t6408_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6408_MIs, t6408_PIs, NULL, NULL, NULL, NULL, NULL, &t6408_TI, t6408_ITIs, NULL, &EmptyCustomAttributesCache, &t6408_TI, &t6408_0_0_0, &t6408_1_0_0, NULL, &t6408_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>
extern Il2CppType t4909_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33213_GM;
MethodInfo m33213_MI = 
{
	"GetEnumerator", NULL, &t6410_TI, &t4909_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33213_GM};
static MethodInfo* t6410_MIs[] =
{
	&m33213_MI,
	NULL
};
static TypeInfo* t6410_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6410_0_0_0;
extern Il2CppType t6410_1_0_0;
struct t6410;
extern Il2CppGenericClass t6410_GC;
TypeInfo t6410_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6410_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6410_TI, t6410_ITIs, NULL, &EmptyCustomAttributesCache, &t6410_TI, &t6410_0_0_0, &t6410_1_0_0, NULL, &t6410_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6409_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.ReliabilityContractAttribute>
extern MethodInfo m33214_MI;
extern MethodInfo m33215_MI;
static PropertyInfo t6409____Item_PropertyInfo = 
{
	&t6409_TI, "Item", &m33214_MI, &m33215_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6409_PIs[] =
{
	&t6409____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1400_0_0_0;
static ParameterInfo t6409_m33216_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1400_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33216_GM;
MethodInfo m33216_MI = 
{
	"IndexOf", NULL, &t6409_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6409_m33216_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33216_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1400_0_0_0;
static ParameterInfo t6409_m33217_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1400_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33217_GM;
MethodInfo m33217_MI = 
{
	"Insert", NULL, &t6409_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6409_m33217_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33217_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6409_m33218_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33218_GM;
MethodInfo m33218_MI = 
{
	"RemoveAt", NULL, &t6409_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6409_m33218_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33218_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6409_m33214_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1400_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33214_GM;
MethodInfo m33214_MI = 
{
	"get_Item", NULL, &t6409_TI, &t1400_0_0_0, RuntimeInvoker_t29_t44, t6409_m33214_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33214_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1400_0_0_0;
static ParameterInfo t6409_m33215_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1400_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33215_GM;
MethodInfo m33215_MI = 
{
	"set_Item", NULL, &t6409_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6409_m33215_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33215_GM};
static MethodInfo* t6409_MIs[] =
{
	&m33216_MI,
	&m33217_MI,
	&m33218_MI,
	&m33214_MI,
	&m33215_MI,
	NULL
};
static TypeInfo* t6409_ITIs[] = 
{
	&t603_TI,
	&t6408_TI,
	&t6410_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6409_0_0_0;
extern Il2CppType t6409_1_0_0;
struct t6409;
extern Il2CppGenericClass t6409_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6409_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6409_MIs, t6409_PIs, NULL, NULL, NULL, NULL, NULL, &t6409_TI, t6409_ITIs, NULL, &t1908__CustomAttributeCache, &t6409_TI, &t6409_0_0_0, &t6409_1_0_0, NULL, &t6409_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4911_TI;

#include "t1153.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.CallingConvention>
extern MethodInfo m33219_MI;
static PropertyInfo t4911____Current_PropertyInfo = 
{
	&t4911_TI, "Current", &m33219_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4911_PIs[] =
{
	&t4911____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1153_0_0_0;
extern void* RuntimeInvoker_t1153 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33219_GM;
MethodInfo m33219_MI = 
{
	"get_Current", NULL, &t4911_TI, &t1153_0_0_0, RuntimeInvoker_t1153, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33219_GM};
static MethodInfo* t4911_MIs[] =
{
	&m33219_MI,
	NULL
};
static TypeInfo* t4911_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4911_0_0_0;
extern Il2CppType t4911_1_0_0;
struct t4911;
extern Il2CppGenericClass t4911_GC;
TypeInfo t4911_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4911_MIs, t4911_PIs, NULL, NULL, NULL, NULL, NULL, &t4911_TI, t4911_ITIs, NULL, &EmptyCustomAttributesCache, &t4911_TI, &t4911_0_0_0, &t4911_1_0_0, NULL, &t4911_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3408.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3408_TI;
#include "t3408MD.h"

extern TypeInfo t1153_TI;
extern MethodInfo m18914_MI;
extern MethodInfo m25435_MI;
struct t20;
 int32_t m25435 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18910_MI;
 void m18910 (t3408 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18911_MI;
 t29 * m18911 (t3408 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18914(__this, &m18914_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1153_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18912_MI;
 void m18912 (t3408 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18913_MI;
 bool m18913 (t3408 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18914 (t3408 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25435(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25435_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CallingConvention>
extern Il2CppType t20_0_0_1;
FieldInfo t3408_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3408_TI, offsetof(t3408, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3408_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3408_TI, offsetof(t3408, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3408_FIs[] =
{
	&t3408_f0_FieldInfo,
	&t3408_f1_FieldInfo,
	NULL
};
static PropertyInfo t3408____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3408_TI, "System.Collections.IEnumerator.Current", &m18911_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3408____Current_PropertyInfo = 
{
	&t3408_TI, "Current", &m18914_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3408_PIs[] =
{
	&t3408____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3408____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3408_m18910_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18910_GM;
MethodInfo m18910_MI = 
{
	".ctor", (methodPointerType)&m18910, &t3408_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3408_m18910_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18910_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18911_GM;
MethodInfo m18911_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18911, &t3408_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18911_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18912_GM;
MethodInfo m18912_MI = 
{
	"Dispose", (methodPointerType)&m18912, &t3408_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18912_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18913_GM;
MethodInfo m18913_MI = 
{
	"MoveNext", (methodPointerType)&m18913, &t3408_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18913_GM};
extern Il2CppType t1153_0_0_0;
extern void* RuntimeInvoker_t1153 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18914_GM;
MethodInfo m18914_MI = 
{
	"get_Current", (methodPointerType)&m18914, &t3408_TI, &t1153_0_0_0, RuntimeInvoker_t1153, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18914_GM};
static MethodInfo* t3408_MIs[] =
{
	&m18910_MI,
	&m18911_MI,
	&m18912_MI,
	&m18913_MI,
	&m18914_MI,
	NULL
};
static MethodInfo* t3408_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18911_MI,
	&m18913_MI,
	&m18912_MI,
	&m18914_MI,
};
static TypeInfo* t3408_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4911_TI,
};
static Il2CppInterfaceOffsetPair t3408_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4911_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3408_0_0_0;
extern Il2CppType t3408_1_0_0;
extern Il2CppGenericClass t3408_GC;
TypeInfo t3408_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3408_MIs, t3408_PIs, t3408_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3408_TI, t3408_ITIs, t3408_VT, &EmptyCustomAttributesCache, &t3408_TI, &t3408_0_0_0, &t3408_1_0_0, t3408_IOs, &t3408_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3408)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6411_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CallingConvention>
extern MethodInfo m33220_MI;
static PropertyInfo t6411____Count_PropertyInfo = 
{
	&t6411_TI, "Count", &m33220_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33221_MI;
static PropertyInfo t6411____IsReadOnly_PropertyInfo = 
{
	&t6411_TI, "IsReadOnly", &m33221_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6411_PIs[] =
{
	&t6411____Count_PropertyInfo,
	&t6411____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33220_GM;
MethodInfo m33220_MI = 
{
	"get_Count", NULL, &t6411_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33220_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33221_GM;
MethodInfo m33221_MI = 
{
	"get_IsReadOnly", NULL, &t6411_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33221_GM};
extern Il2CppType t1153_0_0_0;
extern Il2CppType t1153_0_0_0;
static ParameterInfo t6411_m33222_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1153_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33222_GM;
MethodInfo m33222_MI = 
{
	"Add", NULL, &t6411_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6411_m33222_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33222_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33223_GM;
MethodInfo m33223_MI = 
{
	"Clear", NULL, &t6411_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33223_GM};
extern Il2CppType t1153_0_0_0;
static ParameterInfo t6411_m33224_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1153_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33224_GM;
MethodInfo m33224_MI = 
{
	"Contains", NULL, &t6411_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6411_m33224_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33224_GM};
extern Il2CppType t3663_0_0_0;
extern Il2CppType t3663_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6411_m33225_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3663_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33225_GM;
MethodInfo m33225_MI = 
{
	"CopyTo", NULL, &t6411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6411_m33225_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33225_GM};
extern Il2CppType t1153_0_0_0;
static ParameterInfo t6411_m33226_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1153_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33226_GM;
MethodInfo m33226_MI = 
{
	"Remove", NULL, &t6411_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6411_m33226_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33226_GM};
static MethodInfo* t6411_MIs[] =
{
	&m33220_MI,
	&m33221_MI,
	&m33222_MI,
	&m33223_MI,
	&m33224_MI,
	&m33225_MI,
	&m33226_MI,
	NULL
};
extern TypeInfo t6413_TI;
static TypeInfo* t6411_ITIs[] = 
{
	&t603_TI,
	&t6413_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6411_0_0_0;
extern Il2CppType t6411_1_0_0;
struct t6411;
extern Il2CppGenericClass t6411_GC;
TypeInfo t6411_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6411_MIs, t6411_PIs, NULL, NULL, NULL, NULL, NULL, &t6411_TI, t6411_ITIs, NULL, &EmptyCustomAttributesCache, &t6411_TI, &t6411_0_0_0, &t6411_1_0_0, NULL, &t6411_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.CallingConvention>
extern Il2CppType t4911_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33227_GM;
MethodInfo m33227_MI = 
{
	"GetEnumerator", NULL, &t6413_TI, &t4911_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33227_GM};
static MethodInfo* t6413_MIs[] =
{
	&m33227_MI,
	NULL
};
static TypeInfo* t6413_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6413_0_0_0;
extern Il2CppType t6413_1_0_0;
struct t6413;
extern Il2CppGenericClass t6413_GC;
TypeInfo t6413_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6413_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6413_TI, t6413_ITIs, NULL, &EmptyCustomAttributesCache, &t6413_TI, &t6413_0_0_0, &t6413_1_0_0, NULL, &t6413_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6412_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.CallingConvention>
extern MethodInfo m33228_MI;
extern MethodInfo m33229_MI;
static PropertyInfo t6412____Item_PropertyInfo = 
{
	&t6412_TI, "Item", &m33228_MI, &m33229_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6412_PIs[] =
{
	&t6412____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1153_0_0_0;
static ParameterInfo t6412_m33230_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1153_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33230_GM;
MethodInfo m33230_MI = 
{
	"IndexOf", NULL, &t6412_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6412_m33230_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33230_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1153_0_0_0;
static ParameterInfo t6412_m33231_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1153_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33231_GM;
MethodInfo m33231_MI = 
{
	"Insert", NULL, &t6412_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6412_m33231_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33231_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6412_m33232_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33232_GM;
MethodInfo m33232_MI = 
{
	"RemoveAt", NULL, &t6412_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6412_m33232_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33232_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6412_m33228_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1153_0_0_0;
extern void* RuntimeInvoker_t1153_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33228_GM;
MethodInfo m33228_MI = 
{
	"get_Item", NULL, &t6412_TI, &t1153_0_0_0, RuntimeInvoker_t1153_t44, t6412_m33228_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33228_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1153_0_0_0;
static ParameterInfo t6412_m33229_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1153_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33229_GM;
MethodInfo m33229_MI = 
{
	"set_Item", NULL, &t6412_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6412_m33229_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33229_GM};
static MethodInfo* t6412_MIs[] =
{
	&m33230_MI,
	&m33231_MI,
	&m33232_MI,
	&m33228_MI,
	&m33229_MI,
	NULL
};
static TypeInfo* t6412_ITIs[] = 
{
	&t603_TI,
	&t6411_TI,
	&t6413_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6412_0_0_0;
extern Il2CppType t6412_1_0_0;
struct t6412;
extern Il2CppGenericClass t6412_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6412_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6412_MIs, t6412_PIs, NULL, NULL, NULL, NULL, NULL, &t6412_TI, t6412_ITIs, NULL, &t1908__CustomAttributeCache, &t6412_TI, &t6412_0_0_0, &t6412_1_0_0, NULL, &t6412_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4913_TI;

#include "t1154.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.CharSet>
extern MethodInfo m33233_MI;
static PropertyInfo t4913____Current_PropertyInfo = 
{
	&t4913_TI, "Current", &m33233_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4913_PIs[] =
{
	&t4913____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1154_0_0_0;
extern void* RuntimeInvoker_t1154 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33233_GM;
MethodInfo m33233_MI = 
{
	"get_Current", NULL, &t4913_TI, &t1154_0_0_0, RuntimeInvoker_t1154, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33233_GM};
static MethodInfo* t4913_MIs[] =
{
	&m33233_MI,
	NULL
};
static TypeInfo* t4913_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4913_0_0_0;
extern Il2CppType t4913_1_0_0;
struct t4913;
extern Il2CppGenericClass t4913_GC;
TypeInfo t4913_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4913_MIs, t4913_PIs, NULL, NULL, NULL, NULL, NULL, &t4913_TI, t4913_ITIs, NULL, &EmptyCustomAttributesCache, &t4913_TI, &t4913_0_0_0, &t4913_1_0_0, NULL, &t4913_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3409.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3409_TI;
#include "t3409MD.h"

extern TypeInfo t1154_TI;
extern MethodInfo m18919_MI;
extern MethodInfo m25446_MI;
struct t20;
 int32_t m25446 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18915_MI;
 void m18915 (t3409 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18916_MI;
 t29 * m18916 (t3409 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18919(__this, &m18919_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1154_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18917_MI;
 void m18917 (t3409 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18918_MI;
 bool m18918 (t3409 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18919 (t3409 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25446(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25446_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.CharSet>
extern Il2CppType t20_0_0_1;
FieldInfo t3409_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3409_TI, offsetof(t3409, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3409_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3409_TI, offsetof(t3409, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3409_FIs[] =
{
	&t3409_f0_FieldInfo,
	&t3409_f1_FieldInfo,
	NULL
};
static PropertyInfo t3409____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3409_TI, "System.Collections.IEnumerator.Current", &m18916_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3409____Current_PropertyInfo = 
{
	&t3409_TI, "Current", &m18919_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3409_PIs[] =
{
	&t3409____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3409____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3409_m18915_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18915_GM;
MethodInfo m18915_MI = 
{
	".ctor", (methodPointerType)&m18915, &t3409_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3409_m18915_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18915_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18916_GM;
MethodInfo m18916_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18916, &t3409_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18916_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18917_GM;
MethodInfo m18917_MI = 
{
	"Dispose", (methodPointerType)&m18917, &t3409_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18917_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18918_GM;
MethodInfo m18918_MI = 
{
	"MoveNext", (methodPointerType)&m18918, &t3409_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18918_GM};
extern Il2CppType t1154_0_0_0;
extern void* RuntimeInvoker_t1154 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18919_GM;
MethodInfo m18919_MI = 
{
	"get_Current", (methodPointerType)&m18919, &t3409_TI, &t1154_0_0_0, RuntimeInvoker_t1154, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18919_GM};
static MethodInfo* t3409_MIs[] =
{
	&m18915_MI,
	&m18916_MI,
	&m18917_MI,
	&m18918_MI,
	&m18919_MI,
	NULL
};
static MethodInfo* t3409_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18916_MI,
	&m18918_MI,
	&m18917_MI,
	&m18919_MI,
};
static TypeInfo* t3409_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4913_TI,
};
static Il2CppInterfaceOffsetPair t3409_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4913_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3409_0_0_0;
extern Il2CppType t3409_1_0_0;
extern Il2CppGenericClass t3409_GC;
TypeInfo t3409_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3409_MIs, t3409_PIs, t3409_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3409_TI, t3409_ITIs, t3409_VT, &EmptyCustomAttributesCache, &t3409_TI, &t3409_0_0_0, &t3409_1_0_0, t3409_IOs, &t3409_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3409)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6414_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.CharSet>
extern MethodInfo m33234_MI;
static PropertyInfo t6414____Count_PropertyInfo = 
{
	&t6414_TI, "Count", &m33234_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33235_MI;
static PropertyInfo t6414____IsReadOnly_PropertyInfo = 
{
	&t6414_TI, "IsReadOnly", &m33235_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6414_PIs[] =
{
	&t6414____Count_PropertyInfo,
	&t6414____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33234_GM;
MethodInfo m33234_MI = 
{
	"get_Count", NULL, &t6414_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33234_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33235_GM;
MethodInfo m33235_MI = 
{
	"get_IsReadOnly", NULL, &t6414_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33235_GM};
extern Il2CppType t1154_0_0_0;
extern Il2CppType t1154_0_0_0;
static ParameterInfo t6414_m33236_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1154_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33236_GM;
MethodInfo m33236_MI = 
{
	"Add", NULL, &t6414_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6414_m33236_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33236_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33237_GM;
MethodInfo m33237_MI = 
{
	"Clear", NULL, &t6414_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33237_GM};
extern Il2CppType t1154_0_0_0;
static ParameterInfo t6414_m33238_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1154_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33238_GM;
MethodInfo m33238_MI = 
{
	"Contains", NULL, &t6414_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6414_m33238_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33238_GM};
extern Il2CppType t3664_0_0_0;
extern Il2CppType t3664_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6414_m33239_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3664_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33239_GM;
MethodInfo m33239_MI = 
{
	"CopyTo", NULL, &t6414_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6414_m33239_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33239_GM};
extern Il2CppType t1154_0_0_0;
static ParameterInfo t6414_m33240_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1154_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33240_GM;
MethodInfo m33240_MI = 
{
	"Remove", NULL, &t6414_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6414_m33240_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33240_GM};
static MethodInfo* t6414_MIs[] =
{
	&m33234_MI,
	&m33235_MI,
	&m33236_MI,
	&m33237_MI,
	&m33238_MI,
	&m33239_MI,
	&m33240_MI,
	NULL
};
extern TypeInfo t6416_TI;
static TypeInfo* t6414_ITIs[] = 
{
	&t603_TI,
	&t6416_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6414_0_0_0;
extern Il2CppType t6414_1_0_0;
struct t6414;
extern Il2CppGenericClass t6414_GC;
TypeInfo t6414_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6414_MIs, t6414_PIs, NULL, NULL, NULL, NULL, NULL, &t6414_TI, t6414_ITIs, NULL, &EmptyCustomAttributesCache, &t6414_TI, &t6414_0_0_0, &t6414_1_0_0, NULL, &t6414_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.CharSet>
extern Il2CppType t4913_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33241_GM;
MethodInfo m33241_MI = 
{
	"GetEnumerator", NULL, &t6416_TI, &t4913_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33241_GM};
static MethodInfo* t6416_MIs[] =
{
	&m33241_MI,
	NULL
};
static TypeInfo* t6416_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6416_0_0_0;
extern Il2CppType t6416_1_0_0;
struct t6416;
extern Il2CppGenericClass t6416_GC;
TypeInfo t6416_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6416_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6416_TI, t6416_ITIs, NULL, &EmptyCustomAttributesCache, &t6416_TI, &t6416_0_0_0, &t6416_1_0_0, NULL, &t6416_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6415_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.CharSet>
extern MethodInfo m33242_MI;
extern MethodInfo m33243_MI;
static PropertyInfo t6415____Item_PropertyInfo = 
{
	&t6415_TI, "Item", &m33242_MI, &m33243_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6415_PIs[] =
{
	&t6415____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1154_0_0_0;
static ParameterInfo t6415_m33244_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1154_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33244_GM;
MethodInfo m33244_MI = 
{
	"IndexOf", NULL, &t6415_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6415_m33244_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33244_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1154_0_0_0;
static ParameterInfo t6415_m33245_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1154_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33245_GM;
MethodInfo m33245_MI = 
{
	"Insert", NULL, &t6415_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6415_m33245_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33245_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6415_m33246_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33246_GM;
MethodInfo m33246_MI = 
{
	"RemoveAt", NULL, &t6415_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6415_m33246_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33246_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6415_m33242_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1154_0_0_0;
extern void* RuntimeInvoker_t1154_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33242_GM;
MethodInfo m33242_MI = 
{
	"get_Item", NULL, &t6415_TI, &t1154_0_0_0, RuntimeInvoker_t1154_t44, t6415_m33242_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33242_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1154_0_0_0;
static ParameterInfo t6415_m33243_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1154_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33243_GM;
MethodInfo m33243_MI = 
{
	"set_Item", NULL, &t6415_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6415_m33243_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33243_GM};
static MethodInfo* t6415_MIs[] =
{
	&m33244_MI,
	&m33245_MI,
	&m33246_MI,
	&m33242_MI,
	&m33243_MI,
	NULL
};
static TypeInfo* t6415_ITIs[] = 
{
	&t603_TI,
	&t6414_TI,
	&t6416_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6415_0_0_0;
extern Il2CppType t6415_1_0_0;
struct t6415;
extern Il2CppGenericClass t6415_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6415_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6415_MIs, t6415_PIs, NULL, NULL, NULL, NULL, NULL, &t6415_TI, t6415_ITIs, NULL, &t1908__CustomAttributeCache, &t6415_TI, &t6415_0_0_0, &t6415_1_0_0, NULL, &t6415_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4915_TI;

#include "t1402.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>
extern MethodInfo m33247_MI;
static PropertyInfo t4915____Current_PropertyInfo = 
{
	&t4915_TI, "Current", &m33247_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4915_PIs[] =
{
	&t4915____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1402_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33247_GM;
MethodInfo m33247_MI = 
{
	"get_Current", NULL, &t4915_TI, &t1402_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33247_GM};
static MethodInfo* t4915_MIs[] =
{
	&m33247_MI,
	NULL
};
static TypeInfo* t4915_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4915_0_0_0;
extern Il2CppType t4915_1_0_0;
struct t4915;
extern Il2CppGenericClass t4915_GC;
TypeInfo t4915_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4915_MIs, t4915_PIs, NULL, NULL, NULL, NULL, NULL, &t4915_TI, t4915_ITIs, NULL, &EmptyCustomAttributesCache, &t4915_TI, &t4915_0_0_0, &t4915_1_0_0, NULL, &t4915_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3410.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3410_TI;
#include "t3410MD.h"

extern TypeInfo t1402_TI;
extern MethodInfo m18924_MI;
extern MethodInfo m25457_MI;
struct t20;
#define m25457(__this, p0, method) (t1402 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3410_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3410_TI, offsetof(t3410, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3410_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3410_TI, offsetof(t3410, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3410_FIs[] =
{
	&t3410_f0_FieldInfo,
	&t3410_f1_FieldInfo,
	NULL
};
extern MethodInfo m18921_MI;
static PropertyInfo t3410____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3410_TI, "System.Collections.IEnumerator.Current", &m18921_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3410____Current_PropertyInfo = 
{
	&t3410_TI, "Current", &m18924_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3410_PIs[] =
{
	&t3410____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3410____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3410_m18920_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18920_GM;
MethodInfo m18920_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3410_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3410_m18920_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18920_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18921_GM;
MethodInfo m18921_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3410_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18921_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18922_GM;
MethodInfo m18922_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3410_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18922_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18923_GM;
MethodInfo m18923_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3410_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18923_GM};
extern Il2CppType t1402_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18924_GM;
MethodInfo m18924_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3410_TI, &t1402_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18924_GM};
static MethodInfo* t3410_MIs[] =
{
	&m18920_MI,
	&m18921_MI,
	&m18922_MI,
	&m18923_MI,
	&m18924_MI,
	NULL
};
extern MethodInfo m18923_MI;
extern MethodInfo m18922_MI;
static MethodInfo* t3410_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18921_MI,
	&m18923_MI,
	&m18922_MI,
	&m18924_MI,
};
static TypeInfo* t3410_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4915_TI,
};
static Il2CppInterfaceOffsetPair t3410_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4915_TI, 7},
};
extern TypeInfo t1402_TI;
static Il2CppRGCTXData t3410_RGCTXData[3] = 
{
	&m18924_MI/* Method Usage */,
	&t1402_TI/* Class Usage */,
	&m25457_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3410_0_0_0;
extern Il2CppType t3410_1_0_0;
extern Il2CppGenericClass t3410_GC;
TypeInfo t3410_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3410_MIs, t3410_PIs, t3410_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3410_TI, t3410_ITIs, t3410_VT, &EmptyCustomAttributesCache, &t3410_TI, &t3410_0_0_0, &t3410_1_0_0, t3410_IOs, &t3410_GC, NULL, NULL, NULL, t3410_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3410)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6417_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceAttribute>
extern MethodInfo m33248_MI;
static PropertyInfo t6417____Count_PropertyInfo = 
{
	&t6417_TI, "Count", &m33248_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33249_MI;
static PropertyInfo t6417____IsReadOnly_PropertyInfo = 
{
	&t6417_TI, "IsReadOnly", &m33249_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6417_PIs[] =
{
	&t6417____Count_PropertyInfo,
	&t6417____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33248_GM;
MethodInfo m33248_MI = 
{
	"get_Count", NULL, &t6417_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33248_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33249_GM;
MethodInfo m33249_MI = 
{
	"get_IsReadOnly", NULL, &t6417_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33249_GM};
extern Il2CppType t1402_0_0_0;
extern Il2CppType t1402_0_0_0;
static ParameterInfo t6417_m33250_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1402_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33250_GM;
MethodInfo m33250_MI = 
{
	"Add", NULL, &t6417_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6417_m33250_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33250_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33251_GM;
MethodInfo m33251_MI = 
{
	"Clear", NULL, &t6417_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33251_GM};
extern Il2CppType t1402_0_0_0;
static ParameterInfo t6417_m33252_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1402_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33252_GM;
MethodInfo m33252_MI = 
{
	"Contains", NULL, &t6417_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6417_m33252_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33252_GM};
extern Il2CppType t3665_0_0_0;
extern Il2CppType t3665_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6417_m33253_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3665_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33253_GM;
MethodInfo m33253_MI = 
{
	"CopyTo", NULL, &t6417_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6417_m33253_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33253_GM};
extern Il2CppType t1402_0_0_0;
static ParameterInfo t6417_m33254_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1402_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33254_GM;
MethodInfo m33254_MI = 
{
	"Remove", NULL, &t6417_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6417_m33254_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33254_GM};
static MethodInfo* t6417_MIs[] =
{
	&m33248_MI,
	&m33249_MI,
	&m33250_MI,
	&m33251_MI,
	&m33252_MI,
	&m33253_MI,
	&m33254_MI,
	NULL
};
extern TypeInfo t6419_TI;
static TypeInfo* t6417_ITIs[] = 
{
	&t603_TI,
	&t6419_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6417_0_0_0;
extern Il2CppType t6417_1_0_0;
struct t6417;
extern Il2CppGenericClass t6417_GC;
TypeInfo t6417_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6417_MIs, t6417_PIs, NULL, NULL, NULL, NULL, NULL, &t6417_TI, t6417_ITIs, NULL, &EmptyCustomAttributesCache, &t6417_TI, &t6417_0_0_0, &t6417_1_0_0, NULL, &t6417_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ClassInterfaceAttribute>
extern Il2CppType t4915_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33255_GM;
MethodInfo m33255_MI = 
{
	"GetEnumerator", NULL, &t6419_TI, &t4915_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33255_GM};
static MethodInfo* t6419_MIs[] =
{
	&m33255_MI,
	NULL
};
static TypeInfo* t6419_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6419_0_0_0;
extern Il2CppType t6419_1_0_0;
struct t6419;
extern Il2CppGenericClass t6419_GC;
TypeInfo t6419_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6419_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6419_TI, t6419_ITIs, NULL, &EmptyCustomAttributesCache, &t6419_TI, &t6419_0_0_0, &t6419_1_0_0, NULL, &t6419_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6418_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceAttribute>
extern MethodInfo m33256_MI;
extern MethodInfo m33257_MI;
static PropertyInfo t6418____Item_PropertyInfo = 
{
	&t6418_TI, "Item", &m33256_MI, &m33257_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6418_PIs[] =
{
	&t6418____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1402_0_0_0;
static ParameterInfo t6418_m33258_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1402_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33258_GM;
MethodInfo m33258_MI = 
{
	"IndexOf", NULL, &t6418_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6418_m33258_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33258_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1402_0_0_0;
static ParameterInfo t6418_m33259_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1402_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33259_GM;
MethodInfo m33259_MI = 
{
	"Insert", NULL, &t6418_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6418_m33259_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33259_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6418_m33260_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33260_GM;
MethodInfo m33260_MI = 
{
	"RemoveAt", NULL, &t6418_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6418_m33260_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33260_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6418_m33256_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1402_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33256_GM;
MethodInfo m33256_MI = 
{
	"get_Item", NULL, &t6418_TI, &t1402_0_0_0, RuntimeInvoker_t29_t44, t6418_m33256_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33256_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1402_0_0_0;
static ParameterInfo t6418_m33257_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1402_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33257_GM;
MethodInfo m33257_MI = 
{
	"set_Item", NULL, &t6418_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6418_m33257_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33257_GM};
static MethodInfo* t6418_MIs[] =
{
	&m33258_MI,
	&m33259_MI,
	&m33260_MI,
	&m33256_MI,
	&m33257_MI,
	NULL
};
static TypeInfo* t6418_ITIs[] = 
{
	&t603_TI,
	&t6417_TI,
	&t6419_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6418_0_0_0;
extern Il2CppType t6418_1_0_0;
struct t6418;
extern Il2CppGenericClass t6418_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6418_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6418_MIs, t6418_PIs, NULL, NULL, NULL, NULL, NULL, &t6418_TI, t6418_ITIs, NULL, &t1908__CustomAttributeCache, &t6418_TI, &t6418_0_0_0, &t6418_1_0_0, NULL, &t6418_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4917_TI;

#include "t1403.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>
extern MethodInfo m33261_MI;
static PropertyInfo t4917____Current_PropertyInfo = 
{
	&t4917_TI, "Current", &m33261_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4917_PIs[] =
{
	&t4917____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1403_0_0_0;
extern void* RuntimeInvoker_t1403 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33261_GM;
MethodInfo m33261_MI = 
{
	"get_Current", NULL, &t4917_TI, &t1403_0_0_0, RuntimeInvoker_t1403, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33261_GM};
static MethodInfo* t4917_MIs[] =
{
	&m33261_MI,
	NULL
};
static TypeInfo* t4917_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4917_0_0_0;
extern Il2CppType t4917_1_0_0;
struct t4917;
extern Il2CppGenericClass t4917_GC;
TypeInfo t4917_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4917_MIs, t4917_PIs, NULL, NULL, NULL, NULL, NULL, &t4917_TI, t4917_ITIs, NULL, &EmptyCustomAttributesCache, &t4917_TI, &t4917_0_0_0, &t4917_1_0_0, NULL, &t4917_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3411.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3411_TI;
#include "t3411MD.h"

extern TypeInfo t1403_TI;
extern MethodInfo m18929_MI;
extern MethodInfo m25468_MI;
struct t20;
 int32_t m25468 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18925_MI;
 void m18925 (t3411 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18926_MI;
 t29 * m18926 (t3411 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18929(__this, &m18929_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1403_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18927_MI;
 void m18927 (t3411 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18928_MI;
 bool m18928 (t3411 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18929 (t3411 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25468(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25468_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ClassInterfaceType>
extern Il2CppType t20_0_0_1;
FieldInfo t3411_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3411_TI, offsetof(t3411, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3411_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3411_TI, offsetof(t3411, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3411_FIs[] =
{
	&t3411_f0_FieldInfo,
	&t3411_f1_FieldInfo,
	NULL
};
static PropertyInfo t3411____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3411_TI, "System.Collections.IEnumerator.Current", &m18926_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3411____Current_PropertyInfo = 
{
	&t3411_TI, "Current", &m18929_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3411_PIs[] =
{
	&t3411____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3411____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3411_m18925_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18925_GM;
MethodInfo m18925_MI = 
{
	".ctor", (methodPointerType)&m18925, &t3411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3411_m18925_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18925_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18926_GM;
MethodInfo m18926_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18926, &t3411_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18926_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18927_GM;
MethodInfo m18927_MI = 
{
	"Dispose", (methodPointerType)&m18927, &t3411_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18927_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18928_GM;
MethodInfo m18928_MI = 
{
	"MoveNext", (methodPointerType)&m18928, &t3411_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18928_GM};
extern Il2CppType t1403_0_0_0;
extern void* RuntimeInvoker_t1403 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18929_GM;
MethodInfo m18929_MI = 
{
	"get_Current", (methodPointerType)&m18929, &t3411_TI, &t1403_0_0_0, RuntimeInvoker_t1403, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18929_GM};
static MethodInfo* t3411_MIs[] =
{
	&m18925_MI,
	&m18926_MI,
	&m18927_MI,
	&m18928_MI,
	&m18929_MI,
	NULL
};
static MethodInfo* t3411_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18926_MI,
	&m18928_MI,
	&m18927_MI,
	&m18929_MI,
};
static TypeInfo* t3411_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4917_TI,
};
static Il2CppInterfaceOffsetPair t3411_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4917_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3411_0_0_0;
extern Il2CppType t3411_1_0_0;
extern Il2CppGenericClass t3411_GC;
TypeInfo t3411_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3411_MIs, t3411_PIs, t3411_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3411_TI, t3411_ITIs, t3411_VT, &EmptyCustomAttributesCache, &t3411_TI, &t3411_0_0_0, &t3411_1_0_0, t3411_IOs, &t3411_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3411)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6420_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ClassInterfaceType>
extern MethodInfo m33262_MI;
static PropertyInfo t6420____Count_PropertyInfo = 
{
	&t6420_TI, "Count", &m33262_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33263_MI;
static PropertyInfo t6420____IsReadOnly_PropertyInfo = 
{
	&t6420_TI, "IsReadOnly", &m33263_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6420_PIs[] =
{
	&t6420____Count_PropertyInfo,
	&t6420____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33262_GM;
MethodInfo m33262_MI = 
{
	"get_Count", NULL, &t6420_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33262_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33263_GM;
MethodInfo m33263_MI = 
{
	"get_IsReadOnly", NULL, &t6420_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33263_GM};
extern Il2CppType t1403_0_0_0;
extern Il2CppType t1403_0_0_0;
static ParameterInfo t6420_m33264_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1403_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33264_GM;
MethodInfo m33264_MI = 
{
	"Add", NULL, &t6420_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6420_m33264_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33264_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33265_GM;
MethodInfo m33265_MI = 
{
	"Clear", NULL, &t6420_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33265_GM};
extern Il2CppType t1403_0_0_0;
static ParameterInfo t6420_m33266_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1403_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33266_GM;
MethodInfo m33266_MI = 
{
	"Contains", NULL, &t6420_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6420_m33266_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33266_GM};
extern Il2CppType t3666_0_0_0;
extern Il2CppType t3666_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6420_m33267_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3666_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33267_GM;
MethodInfo m33267_MI = 
{
	"CopyTo", NULL, &t6420_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6420_m33267_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33267_GM};
extern Il2CppType t1403_0_0_0;
static ParameterInfo t6420_m33268_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1403_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33268_GM;
MethodInfo m33268_MI = 
{
	"Remove", NULL, &t6420_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6420_m33268_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33268_GM};
static MethodInfo* t6420_MIs[] =
{
	&m33262_MI,
	&m33263_MI,
	&m33264_MI,
	&m33265_MI,
	&m33266_MI,
	&m33267_MI,
	&m33268_MI,
	NULL
};
extern TypeInfo t6422_TI;
static TypeInfo* t6420_ITIs[] = 
{
	&t603_TI,
	&t6422_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6420_0_0_0;
extern Il2CppType t6420_1_0_0;
struct t6420;
extern Il2CppGenericClass t6420_GC;
TypeInfo t6420_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6420_MIs, t6420_PIs, NULL, NULL, NULL, NULL, NULL, &t6420_TI, t6420_ITIs, NULL, &EmptyCustomAttributesCache, &t6420_TI, &t6420_0_0_0, &t6420_1_0_0, NULL, &t6420_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ClassInterfaceType>
extern Il2CppType t4917_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33269_GM;
MethodInfo m33269_MI = 
{
	"GetEnumerator", NULL, &t6422_TI, &t4917_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33269_GM};
static MethodInfo* t6422_MIs[] =
{
	&m33269_MI,
	NULL
};
static TypeInfo* t6422_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6422_0_0_0;
extern Il2CppType t6422_1_0_0;
struct t6422;
extern Il2CppGenericClass t6422_GC;
TypeInfo t6422_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6422_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6422_TI, t6422_ITIs, NULL, &EmptyCustomAttributesCache, &t6422_TI, &t6422_0_0_0, &t6422_1_0_0, NULL, &t6422_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6421_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.ClassInterfaceType>
extern MethodInfo m33270_MI;
extern MethodInfo m33271_MI;
static PropertyInfo t6421____Item_PropertyInfo = 
{
	&t6421_TI, "Item", &m33270_MI, &m33271_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6421_PIs[] =
{
	&t6421____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1403_0_0_0;
static ParameterInfo t6421_m33272_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1403_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33272_GM;
MethodInfo m33272_MI = 
{
	"IndexOf", NULL, &t6421_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6421_m33272_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33272_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1403_0_0_0;
static ParameterInfo t6421_m33273_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1403_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33273_GM;
MethodInfo m33273_MI = 
{
	"Insert", NULL, &t6421_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6421_m33273_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33273_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6421_m33274_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33274_GM;
MethodInfo m33274_MI = 
{
	"RemoveAt", NULL, &t6421_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6421_m33274_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33274_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6421_m33270_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1403_0_0_0;
extern void* RuntimeInvoker_t1403_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33270_GM;
MethodInfo m33270_MI = 
{
	"get_Item", NULL, &t6421_TI, &t1403_0_0_0, RuntimeInvoker_t1403_t44, t6421_m33270_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33270_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1403_0_0_0;
static ParameterInfo t6421_m33271_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1403_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33271_GM;
MethodInfo m33271_MI = 
{
	"set_Item", NULL, &t6421_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6421_m33271_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33271_GM};
static MethodInfo* t6421_MIs[] =
{
	&m33272_MI,
	&m33273_MI,
	&m33274_MI,
	&m33270_MI,
	&m33271_MI,
	NULL
};
static TypeInfo* t6421_ITIs[] = 
{
	&t603_TI,
	&t6420_TI,
	&t6422_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6421_0_0_0;
extern Il2CppType t6421_1_0_0;
struct t6421;
extern Il2CppGenericClass t6421_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6421_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6421_MIs, t6421_PIs, NULL, NULL, NULL, NULL, NULL, &t6421_TI, t6421_ITIs, NULL, &t1908__CustomAttributeCache, &t6421_TI, &t6421_0_0_0, &t6421_1_0_0, NULL, &t6421_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4919_TI;

#include "t1404.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>
extern MethodInfo m33275_MI;
static PropertyInfo t4919____Current_PropertyInfo = 
{
	&t4919_TI, "Current", &m33275_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4919_PIs[] =
{
	&t4919____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1404_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33275_GM;
MethodInfo m33275_MI = 
{
	"get_Current", NULL, &t4919_TI, &t1404_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33275_GM};
static MethodInfo* t4919_MIs[] =
{
	&m33275_MI,
	NULL
};
static TypeInfo* t4919_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4919_0_0_0;
extern Il2CppType t4919_1_0_0;
struct t4919;
extern Il2CppGenericClass t4919_GC;
TypeInfo t4919_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4919_MIs, t4919_PIs, NULL, NULL, NULL, NULL, NULL, &t4919_TI, t4919_ITIs, NULL, &EmptyCustomAttributesCache, &t4919_TI, &t4919_0_0_0, &t4919_1_0_0, NULL, &t4919_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3412.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3412_TI;
#include "t3412MD.h"

extern TypeInfo t1404_TI;
extern MethodInfo m18934_MI;
extern MethodInfo m25479_MI;
struct t20;
#define m25479(__this, p0, method) (t1404 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3412_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3412_TI, offsetof(t3412, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3412_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3412_TI, offsetof(t3412, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3412_FIs[] =
{
	&t3412_f0_FieldInfo,
	&t3412_f1_FieldInfo,
	NULL
};
extern MethodInfo m18931_MI;
static PropertyInfo t3412____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3412_TI, "System.Collections.IEnumerator.Current", &m18931_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3412____Current_PropertyInfo = 
{
	&t3412_TI, "Current", &m18934_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3412_PIs[] =
{
	&t3412____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3412____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3412_m18930_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18930_GM;
MethodInfo m18930_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3412_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3412_m18930_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18930_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18931_GM;
MethodInfo m18931_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3412_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18931_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18932_GM;
MethodInfo m18932_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3412_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18932_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18933_GM;
MethodInfo m18933_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3412_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18933_GM};
extern Il2CppType t1404_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18934_GM;
MethodInfo m18934_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3412_TI, &t1404_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18934_GM};
static MethodInfo* t3412_MIs[] =
{
	&m18930_MI,
	&m18931_MI,
	&m18932_MI,
	&m18933_MI,
	&m18934_MI,
	NULL
};
extern MethodInfo m18933_MI;
extern MethodInfo m18932_MI;
static MethodInfo* t3412_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18931_MI,
	&m18933_MI,
	&m18932_MI,
	&m18934_MI,
};
static TypeInfo* t3412_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4919_TI,
};
static Il2CppInterfaceOffsetPair t3412_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4919_TI, 7},
};
extern TypeInfo t1404_TI;
static Il2CppRGCTXData t3412_RGCTXData[3] = 
{
	&m18934_MI/* Method Usage */,
	&t1404_TI/* Class Usage */,
	&m25479_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3412_0_0_0;
extern Il2CppType t3412_1_0_0;
extern Il2CppGenericClass t3412_GC;
TypeInfo t3412_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3412_MIs, t3412_PIs, t3412_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3412_TI, t3412_ITIs, t3412_VT, &EmptyCustomAttributesCache, &t3412_TI, &t3412_0_0_0, &t3412_1_0_0, t3412_IOs, &t3412_GC, NULL, NULL, NULL, t3412_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3412)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6423_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>
extern MethodInfo m33276_MI;
static PropertyInfo t6423____Count_PropertyInfo = 
{
	&t6423_TI, "Count", &m33276_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33277_MI;
static PropertyInfo t6423____IsReadOnly_PropertyInfo = 
{
	&t6423_TI, "IsReadOnly", &m33277_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6423_PIs[] =
{
	&t6423____Count_PropertyInfo,
	&t6423____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33276_GM;
MethodInfo m33276_MI = 
{
	"get_Count", NULL, &t6423_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33276_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33277_GM;
MethodInfo m33277_MI = 
{
	"get_IsReadOnly", NULL, &t6423_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33277_GM};
extern Il2CppType t1404_0_0_0;
extern Il2CppType t1404_0_0_0;
static ParameterInfo t6423_m33278_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1404_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33278_GM;
MethodInfo m33278_MI = 
{
	"Add", NULL, &t6423_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6423_m33278_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33278_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33279_GM;
MethodInfo m33279_MI = 
{
	"Clear", NULL, &t6423_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33279_GM};
extern Il2CppType t1404_0_0_0;
static ParameterInfo t6423_m33280_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1404_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33280_GM;
MethodInfo m33280_MI = 
{
	"Contains", NULL, &t6423_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6423_m33280_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33280_GM};
extern Il2CppType t3667_0_0_0;
extern Il2CppType t3667_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6423_m33281_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3667_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33281_GM;
MethodInfo m33281_MI = 
{
	"CopyTo", NULL, &t6423_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6423_m33281_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33281_GM};
extern Il2CppType t1404_0_0_0;
static ParameterInfo t6423_m33282_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1404_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33282_GM;
MethodInfo m33282_MI = 
{
	"Remove", NULL, &t6423_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6423_m33282_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33282_GM};
static MethodInfo* t6423_MIs[] =
{
	&m33276_MI,
	&m33277_MI,
	&m33278_MI,
	&m33279_MI,
	&m33280_MI,
	&m33281_MI,
	&m33282_MI,
	NULL
};
extern TypeInfo t6425_TI;
static TypeInfo* t6423_ITIs[] = 
{
	&t603_TI,
	&t6425_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6423_0_0_0;
extern Il2CppType t6423_1_0_0;
struct t6423;
extern Il2CppGenericClass t6423_GC;
TypeInfo t6423_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6423_MIs, t6423_PIs, NULL, NULL, NULL, NULL, NULL, &t6423_TI, t6423_ITIs, NULL, &EmptyCustomAttributesCache, &t6423_TI, &t6423_0_0_0, &t6423_1_0_0, NULL, &t6423_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>
extern Il2CppType t4919_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33283_GM;
MethodInfo m33283_MI = 
{
	"GetEnumerator", NULL, &t6425_TI, &t4919_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33283_GM};
static MethodInfo* t6425_MIs[] =
{
	&m33283_MI,
	NULL
};
static TypeInfo* t6425_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6425_0_0_0;
extern Il2CppType t6425_1_0_0;
struct t6425;
extern Il2CppGenericClass t6425_GC;
TypeInfo t6425_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6425_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6425_TI, t6425_ITIs, NULL, &EmptyCustomAttributesCache, &t6425_TI, &t6425_0_0_0, &t6425_1_0_0, NULL, &t6425_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6424_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>
extern MethodInfo m33284_MI;
extern MethodInfo m33285_MI;
static PropertyInfo t6424____Item_PropertyInfo = 
{
	&t6424_TI, "Item", &m33284_MI, &m33285_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6424_PIs[] =
{
	&t6424____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1404_0_0_0;
static ParameterInfo t6424_m33286_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1404_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33286_GM;
MethodInfo m33286_MI = 
{
	"IndexOf", NULL, &t6424_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6424_m33286_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33286_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1404_0_0_0;
static ParameterInfo t6424_m33287_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1404_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33287_GM;
MethodInfo m33287_MI = 
{
	"Insert", NULL, &t6424_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6424_m33287_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33287_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6424_m33288_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33288_GM;
MethodInfo m33288_MI = 
{
	"RemoveAt", NULL, &t6424_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6424_m33288_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33288_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6424_m33284_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1404_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33284_GM;
MethodInfo m33284_MI = 
{
	"get_Item", NULL, &t6424_TI, &t1404_0_0_0, RuntimeInvoker_t29_t44, t6424_m33284_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33284_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1404_0_0_0;
static ParameterInfo t6424_m33285_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1404_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33285_GM;
MethodInfo m33285_MI = 
{
	"set_Item", NULL, &t6424_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6424_m33285_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33285_GM};
static MethodInfo* t6424_MIs[] =
{
	&m33286_MI,
	&m33287_MI,
	&m33288_MI,
	&m33284_MI,
	&m33285_MI,
	NULL
};
static TypeInfo* t6424_ITIs[] = 
{
	&t603_TI,
	&t6423_TI,
	&t6425_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6424_0_0_0;
extern Il2CppType t6424_1_0_0;
struct t6424;
extern Il2CppGenericClass t6424_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6424_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6424_MIs, t6424_PIs, NULL, NULL, NULL, NULL, NULL, &t6424_TI, t6424_ITIs, NULL, &t1908__CustomAttributeCache, &t6424_TI, &t6424_0_0_0, &t6424_1_0_0, NULL, &t6424_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4921_TI;

#include "t1405.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>
extern MethodInfo m33289_MI;
static PropertyInfo t4921____Current_PropertyInfo = 
{
	&t4921_TI, "Current", &m33289_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4921_PIs[] =
{
	&t4921____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1405_0_0_0;
extern void* RuntimeInvoker_t1405 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33289_GM;
MethodInfo m33289_MI = 
{
	"get_Current", NULL, &t4921_TI, &t1405_0_0_0, RuntimeInvoker_t1405, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33289_GM};
static MethodInfo* t4921_MIs[] =
{
	&m33289_MI,
	NULL
};
static TypeInfo* t4921_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4921_0_0_0;
extern Il2CppType t4921_1_0_0;
struct t4921;
extern Il2CppGenericClass t4921_GC;
TypeInfo t4921_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4921_MIs, t4921_PIs, NULL, NULL, NULL, NULL, NULL, &t4921_TI, t4921_ITIs, NULL, &EmptyCustomAttributesCache, &t4921_TI, &t4921_0_0_0, &t4921_1_0_0, NULL, &t4921_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3413.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3413_TI;
#include "t3413MD.h"

extern TypeInfo t1405_TI;
extern MethodInfo m18939_MI;
extern MethodInfo m25490_MI;
struct t20;
 int32_t m25490 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18935_MI;
 void m18935 (t3413 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18936_MI;
 t29 * m18936 (t3413 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18939(__this, &m18939_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1405_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18937_MI;
 void m18937 (t3413 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18938_MI;
 bool m18938 (t3413 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18939 (t3413 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25490(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25490_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComInterfaceType>
extern Il2CppType t20_0_0_1;
FieldInfo t3413_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3413_TI, offsetof(t3413, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3413_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3413_TI, offsetof(t3413, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3413_FIs[] =
{
	&t3413_f0_FieldInfo,
	&t3413_f1_FieldInfo,
	NULL
};
static PropertyInfo t3413____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3413_TI, "System.Collections.IEnumerator.Current", &m18936_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3413____Current_PropertyInfo = 
{
	&t3413_TI, "Current", &m18939_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3413_PIs[] =
{
	&t3413____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3413____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3413_m18935_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18935_GM;
MethodInfo m18935_MI = 
{
	".ctor", (methodPointerType)&m18935, &t3413_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3413_m18935_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18935_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18936_GM;
MethodInfo m18936_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18936, &t3413_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18936_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18937_GM;
MethodInfo m18937_MI = 
{
	"Dispose", (methodPointerType)&m18937, &t3413_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18937_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18938_GM;
MethodInfo m18938_MI = 
{
	"MoveNext", (methodPointerType)&m18938, &t3413_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18938_GM};
extern Il2CppType t1405_0_0_0;
extern void* RuntimeInvoker_t1405 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18939_GM;
MethodInfo m18939_MI = 
{
	"get_Current", (methodPointerType)&m18939, &t3413_TI, &t1405_0_0_0, RuntimeInvoker_t1405, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18939_GM};
static MethodInfo* t3413_MIs[] =
{
	&m18935_MI,
	&m18936_MI,
	&m18937_MI,
	&m18938_MI,
	&m18939_MI,
	NULL
};
static MethodInfo* t3413_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18936_MI,
	&m18938_MI,
	&m18937_MI,
	&m18939_MI,
};
static TypeInfo* t3413_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4921_TI,
};
static Il2CppInterfaceOffsetPair t3413_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4921_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3413_0_0_0;
extern Il2CppType t3413_1_0_0;
extern Il2CppGenericClass t3413_GC;
TypeInfo t3413_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3413_MIs, t3413_PIs, t3413_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3413_TI, t3413_ITIs, t3413_VT, &EmptyCustomAttributesCache, &t3413_TI, &t3413_0_0_0, &t3413_1_0_0, t3413_IOs, &t3413_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3413)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6426_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComInterfaceType>
extern MethodInfo m33290_MI;
static PropertyInfo t6426____Count_PropertyInfo = 
{
	&t6426_TI, "Count", &m33290_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33291_MI;
static PropertyInfo t6426____IsReadOnly_PropertyInfo = 
{
	&t6426_TI, "IsReadOnly", &m33291_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6426_PIs[] =
{
	&t6426____Count_PropertyInfo,
	&t6426____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33290_GM;
MethodInfo m33290_MI = 
{
	"get_Count", NULL, &t6426_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33290_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33291_GM;
MethodInfo m33291_MI = 
{
	"get_IsReadOnly", NULL, &t6426_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33291_GM};
extern Il2CppType t1405_0_0_0;
extern Il2CppType t1405_0_0_0;
static ParameterInfo t6426_m33292_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33292_GM;
MethodInfo m33292_MI = 
{
	"Add", NULL, &t6426_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6426_m33292_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33292_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33293_GM;
MethodInfo m33293_MI = 
{
	"Clear", NULL, &t6426_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33293_GM};
extern Il2CppType t1405_0_0_0;
static ParameterInfo t6426_m33294_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33294_GM;
MethodInfo m33294_MI = 
{
	"Contains", NULL, &t6426_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6426_m33294_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33294_GM};
extern Il2CppType t3668_0_0_0;
extern Il2CppType t3668_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6426_m33295_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3668_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33295_GM;
MethodInfo m33295_MI = 
{
	"CopyTo", NULL, &t6426_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6426_m33295_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33295_GM};
extern Il2CppType t1405_0_0_0;
static ParameterInfo t6426_m33296_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33296_GM;
MethodInfo m33296_MI = 
{
	"Remove", NULL, &t6426_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6426_m33296_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33296_GM};
static MethodInfo* t6426_MIs[] =
{
	&m33290_MI,
	&m33291_MI,
	&m33292_MI,
	&m33293_MI,
	&m33294_MI,
	&m33295_MI,
	&m33296_MI,
	NULL
};
extern TypeInfo t6428_TI;
static TypeInfo* t6426_ITIs[] = 
{
	&t603_TI,
	&t6428_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6426_0_0_0;
extern Il2CppType t6426_1_0_0;
struct t6426;
extern Il2CppGenericClass t6426_GC;
TypeInfo t6426_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6426_MIs, t6426_PIs, NULL, NULL, NULL, NULL, NULL, &t6426_TI, t6426_ITIs, NULL, &EmptyCustomAttributesCache, &t6426_TI, &t6426_0_0_0, &t6426_1_0_0, NULL, &t6426_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.ComInterfaceType>
extern Il2CppType t4921_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33297_GM;
MethodInfo m33297_MI = 
{
	"GetEnumerator", NULL, &t6428_TI, &t4921_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33297_GM};
static MethodInfo* t6428_MIs[] =
{
	&m33297_MI,
	NULL
};
static TypeInfo* t6428_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6428_0_0_0;
extern Il2CppType t6428_1_0_0;
struct t6428;
extern Il2CppGenericClass t6428_GC;
TypeInfo t6428_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6428_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6428_TI, t6428_ITIs, NULL, &EmptyCustomAttributesCache, &t6428_TI, &t6428_0_0_0, &t6428_1_0_0, NULL, &t6428_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6427_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.ComInterfaceType>
extern MethodInfo m33298_MI;
extern MethodInfo m33299_MI;
static PropertyInfo t6427____Item_PropertyInfo = 
{
	&t6427_TI, "Item", &m33298_MI, &m33299_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6427_PIs[] =
{
	&t6427____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1405_0_0_0;
static ParameterInfo t6427_m33300_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33300_GM;
MethodInfo m33300_MI = 
{
	"IndexOf", NULL, &t6427_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6427_m33300_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33300_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1405_0_0_0;
static ParameterInfo t6427_m33301_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33301_GM;
MethodInfo m33301_MI = 
{
	"Insert", NULL, &t6427_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6427_m33301_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33301_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6427_m33302_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33302_GM;
MethodInfo m33302_MI = 
{
	"RemoveAt", NULL, &t6427_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6427_m33302_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33302_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6427_m33298_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1405_0_0_0;
extern void* RuntimeInvoker_t1405_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33298_GM;
MethodInfo m33298_MI = 
{
	"get_Item", NULL, &t6427_TI, &t1405_0_0_0, RuntimeInvoker_t1405_t44, t6427_m33298_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33298_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1405_0_0_0;
static ParameterInfo t6427_m33299_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33299_GM;
MethodInfo m33299_MI = 
{
	"set_Item", NULL, &t6427_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6427_m33299_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33299_GM};
static MethodInfo* t6427_MIs[] =
{
	&m33300_MI,
	&m33301_MI,
	&m33302_MI,
	&m33298_MI,
	&m33299_MI,
	NULL
};
static TypeInfo* t6427_ITIs[] = 
{
	&t603_TI,
	&t6426_TI,
	&t6428_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6427_0_0_0;
extern Il2CppType t6427_1_0_0;
struct t6427;
extern Il2CppGenericClass t6427_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6427_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6427_MIs, t6427_PIs, NULL, NULL, NULL, NULL, NULL, &t6427_TI, t6427_ITIs, NULL, &t1908__CustomAttributeCache, &t6427_TI, &t6427_0_0_0, &t6427_1_0_0, NULL, &t6427_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4923_TI;

#include "t1406.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>
extern MethodInfo m33303_MI;
static PropertyInfo t4923____Current_PropertyInfo = 
{
	&t4923_TI, "Current", &m33303_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4923_PIs[] =
{
	&t4923____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1406_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33303_GM;
MethodInfo m33303_MI = 
{
	"get_Current", NULL, &t4923_TI, &t1406_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33303_GM};
static MethodInfo* t4923_MIs[] =
{
	&m33303_MI,
	NULL
};
static TypeInfo* t4923_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4923_0_0_0;
extern Il2CppType t4923_1_0_0;
struct t4923;
extern Il2CppGenericClass t4923_GC;
TypeInfo t4923_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4923_MIs, t4923_PIs, NULL, NULL, NULL, NULL, NULL, &t4923_TI, t4923_ITIs, NULL, &EmptyCustomAttributesCache, &t4923_TI, &t4923_0_0_0, &t4923_1_0_0, NULL, &t4923_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3414.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3414_TI;
#include "t3414MD.h"

extern TypeInfo t1406_TI;
extern MethodInfo m18944_MI;
extern MethodInfo m25501_MI;
struct t20;
#define m25501(__this, p0, method) (t1406 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.DispIdAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3414_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3414_TI, offsetof(t3414, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3414_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3414_TI, offsetof(t3414, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3414_FIs[] =
{
	&t3414_f0_FieldInfo,
	&t3414_f1_FieldInfo,
	NULL
};
extern MethodInfo m18941_MI;
static PropertyInfo t3414____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3414_TI, "System.Collections.IEnumerator.Current", &m18941_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3414____Current_PropertyInfo = 
{
	&t3414_TI, "Current", &m18944_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3414_PIs[] =
{
	&t3414____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3414____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3414_m18940_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18940_GM;
MethodInfo m18940_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3414_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3414_m18940_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18940_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18941_GM;
MethodInfo m18941_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3414_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18941_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18942_GM;
MethodInfo m18942_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3414_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18942_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18943_GM;
MethodInfo m18943_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3414_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18943_GM};
extern Il2CppType t1406_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18944_GM;
MethodInfo m18944_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3414_TI, &t1406_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18944_GM};
static MethodInfo* t3414_MIs[] =
{
	&m18940_MI,
	&m18941_MI,
	&m18942_MI,
	&m18943_MI,
	&m18944_MI,
	NULL
};
extern MethodInfo m18943_MI;
extern MethodInfo m18942_MI;
static MethodInfo* t3414_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18941_MI,
	&m18943_MI,
	&m18942_MI,
	&m18944_MI,
};
static TypeInfo* t3414_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4923_TI,
};
static Il2CppInterfaceOffsetPair t3414_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4923_TI, 7},
};
extern TypeInfo t1406_TI;
static Il2CppRGCTXData t3414_RGCTXData[3] = 
{
	&m18944_MI/* Method Usage */,
	&t1406_TI/* Class Usage */,
	&m25501_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3414_0_0_0;
extern Il2CppType t3414_1_0_0;
extern Il2CppGenericClass t3414_GC;
TypeInfo t3414_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3414_MIs, t3414_PIs, t3414_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3414_TI, t3414_ITIs, t3414_VT, &EmptyCustomAttributesCache, &t3414_TI, &t3414_0_0_0, &t3414_1_0_0, t3414_IOs, &t3414_GC, NULL, NULL, NULL, t3414_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3414)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6429_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.DispIdAttribute>
extern MethodInfo m33304_MI;
static PropertyInfo t6429____Count_PropertyInfo = 
{
	&t6429_TI, "Count", &m33304_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33305_MI;
static PropertyInfo t6429____IsReadOnly_PropertyInfo = 
{
	&t6429_TI, "IsReadOnly", &m33305_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6429_PIs[] =
{
	&t6429____Count_PropertyInfo,
	&t6429____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33304_GM;
MethodInfo m33304_MI = 
{
	"get_Count", NULL, &t6429_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33304_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33305_GM;
MethodInfo m33305_MI = 
{
	"get_IsReadOnly", NULL, &t6429_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33305_GM};
extern Il2CppType t1406_0_0_0;
extern Il2CppType t1406_0_0_0;
static ParameterInfo t6429_m33306_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1406_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33306_GM;
MethodInfo m33306_MI = 
{
	"Add", NULL, &t6429_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6429_m33306_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33306_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33307_GM;
MethodInfo m33307_MI = 
{
	"Clear", NULL, &t6429_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33307_GM};
extern Il2CppType t1406_0_0_0;
static ParameterInfo t6429_m33308_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1406_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33308_GM;
MethodInfo m33308_MI = 
{
	"Contains", NULL, &t6429_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6429_m33308_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33308_GM};
extern Il2CppType t3669_0_0_0;
extern Il2CppType t3669_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6429_m33309_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3669_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33309_GM;
MethodInfo m33309_MI = 
{
	"CopyTo", NULL, &t6429_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6429_m33309_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33309_GM};
extern Il2CppType t1406_0_0_0;
static ParameterInfo t6429_m33310_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1406_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33310_GM;
MethodInfo m33310_MI = 
{
	"Remove", NULL, &t6429_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6429_m33310_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33310_GM};
static MethodInfo* t6429_MIs[] =
{
	&m33304_MI,
	&m33305_MI,
	&m33306_MI,
	&m33307_MI,
	&m33308_MI,
	&m33309_MI,
	&m33310_MI,
	NULL
};
extern TypeInfo t6431_TI;
static TypeInfo* t6429_ITIs[] = 
{
	&t603_TI,
	&t6431_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6429_0_0_0;
extern Il2CppType t6429_1_0_0;
struct t6429;
extern Il2CppGenericClass t6429_GC;
TypeInfo t6429_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6429_MIs, t6429_PIs, NULL, NULL, NULL, NULL, NULL, &t6429_TI, t6429_ITIs, NULL, &EmptyCustomAttributesCache, &t6429_TI, &t6429_0_0_0, &t6429_1_0_0, NULL, &t6429_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.DispIdAttribute>
extern Il2CppType t4923_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33311_GM;
MethodInfo m33311_MI = 
{
	"GetEnumerator", NULL, &t6431_TI, &t4923_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33311_GM};
static MethodInfo* t6431_MIs[] =
{
	&m33311_MI,
	NULL
};
static TypeInfo* t6431_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6431_0_0_0;
extern Il2CppType t6431_1_0_0;
struct t6431;
extern Il2CppGenericClass t6431_GC;
TypeInfo t6431_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6431_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6431_TI, t6431_ITIs, NULL, &EmptyCustomAttributesCache, &t6431_TI, &t6431_0_0_0, &t6431_1_0_0, NULL, &t6431_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6430_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.DispIdAttribute>
extern MethodInfo m33312_MI;
extern MethodInfo m33313_MI;
static PropertyInfo t6430____Item_PropertyInfo = 
{
	&t6430_TI, "Item", &m33312_MI, &m33313_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6430_PIs[] =
{
	&t6430____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1406_0_0_0;
static ParameterInfo t6430_m33314_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1406_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33314_GM;
MethodInfo m33314_MI = 
{
	"IndexOf", NULL, &t6430_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6430_m33314_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33314_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1406_0_0_0;
static ParameterInfo t6430_m33315_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1406_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33315_GM;
MethodInfo m33315_MI = 
{
	"Insert", NULL, &t6430_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6430_m33315_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33315_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6430_m33316_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33316_GM;
MethodInfo m33316_MI = 
{
	"RemoveAt", NULL, &t6430_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6430_m33316_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33316_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6430_m33312_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1406_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33312_GM;
MethodInfo m33312_MI = 
{
	"get_Item", NULL, &t6430_TI, &t1406_0_0_0, RuntimeInvoker_t29_t44, t6430_m33312_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33312_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1406_0_0_0;
static ParameterInfo t6430_m33313_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1406_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33313_GM;
MethodInfo m33313_MI = 
{
	"set_Item", NULL, &t6430_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6430_m33313_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33313_GM};
static MethodInfo* t6430_MIs[] =
{
	&m33314_MI,
	&m33315_MI,
	&m33316_MI,
	&m33312_MI,
	&m33313_MI,
	NULL
};
static TypeInfo* t6430_ITIs[] = 
{
	&t603_TI,
	&t6429_TI,
	&t6431_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6430_0_0_0;
extern Il2CppType t6430_1_0_0;
struct t6430;
extern Il2CppGenericClass t6430_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6430_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6430_MIs, t6430_PIs, NULL, NULL, NULL, NULL, NULL, &t6430_TI, t6430_ITIs, NULL, &t1908__CustomAttributeCache, &t6430_TI, &t6430_0_0_0, &t6430_1_0_0, NULL, &t6430_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4925_TI;

#include "t1408.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.GCHandleType>
extern MethodInfo m33317_MI;
static PropertyInfo t4925____Current_PropertyInfo = 
{
	&t4925_TI, "Current", &m33317_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4925_PIs[] =
{
	&t4925____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1408_0_0_0;
extern void* RuntimeInvoker_t1408 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33317_GM;
MethodInfo m33317_MI = 
{
	"get_Current", NULL, &t4925_TI, &t1408_0_0_0, RuntimeInvoker_t1408, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33317_GM};
static MethodInfo* t4925_MIs[] =
{
	&m33317_MI,
	NULL
};
static TypeInfo* t4925_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4925_0_0_0;
extern Il2CppType t4925_1_0_0;
struct t4925;
extern Il2CppGenericClass t4925_GC;
TypeInfo t4925_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4925_MIs, t4925_PIs, NULL, NULL, NULL, NULL, NULL, &t4925_TI, t4925_ITIs, NULL, &EmptyCustomAttributesCache, &t4925_TI, &t4925_0_0_0, &t4925_1_0_0, NULL, &t4925_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3415.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3415_TI;
#include "t3415MD.h"

extern TypeInfo t1408_TI;
extern MethodInfo m18949_MI;
extern MethodInfo m25512_MI;
struct t20;
 int32_t m25512 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18945_MI;
 void m18945 (t3415 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18946_MI;
 t29 * m18946 (t3415 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18949(__this, &m18949_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1408_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18947_MI;
 void m18947 (t3415 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18948_MI;
 bool m18948 (t3415 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18949 (t3415 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25512(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25512_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>
extern Il2CppType t20_0_0_1;
FieldInfo t3415_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3415_TI, offsetof(t3415, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3415_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3415_TI, offsetof(t3415, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3415_FIs[] =
{
	&t3415_f0_FieldInfo,
	&t3415_f1_FieldInfo,
	NULL
};
static PropertyInfo t3415____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3415_TI, "System.Collections.IEnumerator.Current", &m18946_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3415____Current_PropertyInfo = 
{
	&t3415_TI, "Current", &m18949_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3415_PIs[] =
{
	&t3415____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3415____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3415_m18945_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18945_GM;
MethodInfo m18945_MI = 
{
	".ctor", (methodPointerType)&m18945, &t3415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3415_m18945_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18945_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18946_GM;
MethodInfo m18946_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18946, &t3415_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18946_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18947_GM;
MethodInfo m18947_MI = 
{
	"Dispose", (methodPointerType)&m18947, &t3415_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18947_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18948_GM;
MethodInfo m18948_MI = 
{
	"MoveNext", (methodPointerType)&m18948, &t3415_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18948_GM};
extern Il2CppType t1408_0_0_0;
extern void* RuntimeInvoker_t1408 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18949_GM;
MethodInfo m18949_MI = 
{
	"get_Current", (methodPointerType)&m18949, &t3415_TI, &t1408_0_0_0, RuntimeInvoker_t1408, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18949_GM};
static MethodInfo* t3415_MIs[] =
{
	&m18945_MI,
	&m18946_MI,
	&m18947_MI,
	&m18948_MI,
	&m18949_MI,
	NULL
};
static MethodInfo* t3415_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18946_MI,
	&m18948_MI,
	&m18947_MI,
	&m18949_MI,
};
static TypeInfo* t3415_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4925_TI,
};
static Il2CppInterfaceOffsetPair t3415_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4925_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3415_0_0_0;
extern Il2CppType t3415_1_0_0;
extern Il2CppGenericClass t3415_GC;
TypeInfo t3415_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3415_MIs, t3415_PIs, t3415_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3415_TI, t3415_ITIs, t3415_VT, &EmptyCustomAttributesCache, &t3415_TI, &t3415_0_0_0, &t3415_1_0_0, t3415_IOs, &t3415_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3415)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6432_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.GCHandleType>
extern MethodInfo m33318_MI;
static PropertyInfo t6432____Count_PropertyInfo = 
{
	&t6432_TI, "Count", &m33318_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33319_MI;
static PropertyInfo t6432____IsReadOnly_PropertyInfo = 
{
	&t6432_TI, "IsReadOnly", &m33319_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6432_PIs[] =
{
	&t6432____Count_PropertyInfo,
	&t6432____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33318_GM;
MethodInfo m33318_MI = 
{
	"get_Count", NULL, &t6432_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33318_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33319_GM;
MethodInfo m33319_MI = 
{
	"get_IsReadOnly", NULL, &t6432_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33319_GM};
extern Il2CppType t1408_0_0_0;
extern Il2CppType t1408_0_0_0;
static ParameterInfo t6432_m33320_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33320_GM;
MethodInfo m33320_MI = 
{
	"Add", NULL, &t6432_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6432_m33320_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33320_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33321_GM;
MethodInfo m33321_MI = 
{
	"Clear", NULL, &t6432_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33321_GM};
extern Il2CppType t1408_0_0_0;
static ParameterInfo t6432_m33322_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33322_GM;
MethodInfo m33322_MI = 
{
	"Contains", NULL, &t6432_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6432_m33322_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33322_GM};
extern Il2CppType t3670_0_0_0;
extern Il2CppType t3670_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6432_m33323_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3670_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33323_GM;
MethodInfo m33323_MI = 
{
	"CopyTo", NULL, &t6432_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6432_m33323_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33323_GM};
extern Il2CppType t1408_0_0_0;
static ParameterInfo t6432_m33324_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33324_GM;
MethodInfo m33324_MI = 
{
	"Remove", NULL, &t6432_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6432_m33324_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33324_GM};
static MethodInfo* t6432_MIs[] =
{
	&m33318_MI,
	&m33319_MI,
	&m33320_MI,
	&m33321_MI,
	&m33322_MI,
	&m33323_MI,
	&m33324_MI,
	NULL
};
extern TypeInfo t6434_TI;
static TypeInfo* t6432_ITIs[] = 
{
	&t603_TI,
	&t6434_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6432_0_0_0;
extern Il2CppType t6432_1_0_0;
struct t6432;
extern Il2CppGenericClass t6432_GC;
TypeInfo t6432_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6432_MIs, t6432_PIs, NULL, NULL, NULL, NULL, NULL, &t6432_TI, t6432_ITIs, NULL, &EmptyCustomAttributesCache, &t6432_TI, &t6432_0_0_0, &t6432_1_0_0, NULL, &t6432_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.GCHandleType>
extern Il2CppType t4925_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33325_GM;
MethodInfo m33325_MI = 
{
	"GetEnumerator", NULL, &t6434_TI, &t4925_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33325_GM};
static MethodInfo* t6434_MIs[] =
{
	&m33325_MI,
	NULL
};
static TypeInfo* t6434_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6434_0_0_0;
extern Il2CppType t6434_1_0_0;
struct t6434;
extern Il2CppGenericClass t6434_GC;
TypeInfo t6434_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6434_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6434_TI, t6434_ITIs, NULL, &EmptyCustomAttributesCache, &t6434_TI, &t6434_0_0_0, &t6434_1_0_0, NULL, &t6434_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6433_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.GCHandleType>
extern MethodInfo m33326_MI;
extern MethodInfo m33327_MI;
static PropertyInfo t6433____Item_PropertyInfo = 
{
	&t6433_TI, "Item", &m33326_MI, &m33327_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6433_PIs[] =
{
	&t6433____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1408_0_0_0;
static ParameterInfo t6433_m33328_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33328_GM;
MethodInfo m33328_MI = 
{
	"IndexOf", NULL, &t6433_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6433_m33328_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33328_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1408_0_0_0;
static ParameterInfo t6433_m33329_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33329_GM;
MethodInfo m33329_MI = 
{
	"Insert", NULL, &t6433_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6433_m33329_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33329_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6433_m33330_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33330_GM;
MethodInfo m33330_MI = 
{
	"RemoveAt", NULL, &t6433_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6433_m33330_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33330_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6433_m33326_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1408_0_0_0;
extern void* RuntimeInvoker_t1408_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33326_GM;
MethodInfo m33326_MI = 
{
	"get_Item", NULL, &t6433_TI, &t1408_0_0_0, RuntimeInvoker_t1408_t44, t6433_m33326_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33326_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1408_0_0_0;
static ParameterInfo t6433_m33327_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33327_GM;
MethodInfo m33327_MI = 
{
	"set_Item", NULL, &t6433_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6433_m33327_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33327_GM};
static MethodInfo* t6433_MIs[] =
{
	&m33328_MI,
	&m33329_MI,
	&m33330_MI,
	&m33326_MI,
	&m33327_MI,
	NULL
};
static TypeInfo* t6433_ITIs[] = 
{
	&t603_TI,
	&t6432_TI,
	&t6434_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6433_0_0_0;
extern Il2CppType t6433_1_0_0;
struct t6433;
extern Il2CppGenericClass t6433_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6433_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6433_MIs, t6433_PIs, NULL, NULL, NULL, NULL, NULL, &t6433_TI, t6433_ITIs, NULL, &t1908__CustomAttributeCache, &t6433_TI, &t6433_0_0_0, &t6433_1_0_0, NULL, &t6433_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4927_TI;

#include "t1409.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>
extern MethodInfo m33331_MI;
static PropertyInfo t4927____Current_PropertyInfo = 
{
	&t4927_TI, "Current", &m33331_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4927_PIs[] =
{
	&t4927____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1409_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33331_GM;
MethodInfo m33331_MI = 
{
	"get_Current", NULL, &t4927_TI, &t1409_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33331_GM};
static MethodInfo* t4927_MIs[] =
{
	&m33331_MI,
	NULL
};
static TypeInfo* t4927_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4927_0_0_0;
extern Il2CppType t4927_1_0_0;
struct t4927;
extern Il2CppGenericClass t4927_GC;
TypeInfo t4927_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4927_MIs, t4927_PIs, NULL, NULL, NULL, NULL, NULL, &t4927_TI, t4927_ITIs, NULL, &EmptyCustomAttributesCache, &t4927_TI, &t4927_0_0_0, &t4927_1_0_0, NULL, &t4927_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3416.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3416_TI;
#include "t3416MD.h"

extern TypeInfo t1409_TI;
extern MethodInfo m18954_MI;
extern MethodInfo m25523_MI;
struct t20;
#define m25523(__this, p0, method) (t1409 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.InterfaceTypeAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3416_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3416_TI, offsetof(t3416, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3416_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3416_TI, offsetof(t3416, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3416_FIs[] =
{
	&t3416_f0_FieldInfo,
	&t3416_f1_FieldInfo,
	NULL
};
extern MethodInfo m18951_MI;
static PropertyInfo t3416____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3416_TI, "System.Collections.IEnumerator.Current", &m18951_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3416____Current_PropertyInfo = 
{
	&t3416_TI, "Current", &m18954_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3416_PIs[] =
{
	&t3416____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3416____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3416_m18950_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18950_GM;
MethodInfo m18950_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3416_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3416_m18950_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18950_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18951_GM;
MethodInfo m18951_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3416_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18951_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18952_GM;
MethodInfo m18952_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3416_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18952_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18953_GM;
MethodInfo m18953_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3416_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18953_GM};
extern Il2CppType t1409_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18954_GM;
MethodInfo m18954_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3416_TI, &t1409_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18954_GM};
static MethodInfo* t3416_MIs[] =
{
	&m18950_MI,
	&m18951_MI,
	&m18952_MI,
	&m18953_MI,
	&m18954_MI,
	NULL
};
extern MethodInfo m18953_MI;
extern MethodInfo m18952_MI;
static MethodInfo* t3416_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18951_MI,
	&m18953_MI,
	&m18952_MI,
	&m18954_MI,
};
static TypeInfo* t3416_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4927_TI,
};
static Il2CppInterfaceOffsetPair t3416_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4927_TI, 7},
};
extern TypeInfo t1409_TI;
static Il2CppRGCTXData t3416_RGCTXData[3] = 
{
	&m18954_MI/* Method Usage */,
	&t1409_TI/* Class Usage */,
	&m25523_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3416_0_0_0;
extern Il2CppType t3416_1_0_0;
extern Il2CppGenericClass t3416_GC;
TypeInfo t3416_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3416_MIs, t3416_PIs, t3416_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3416_TI, t3416_ITIs, t3416_VT, &EmptyCustomAttributesCache, &t3416_TI, &t3416_0_0_0, &t3416_1_0_0, t3416_IOs, &t3416_GC, NULL, NULL, NULL, t3416_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3416)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6435_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.InterfaceTypeAttribute>
extern MethodInfo m33332_MI;
static PropertyInfo t6435____Count_PropertyInfo = 
{
	&t6435_TI, "Count", &m33332_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33333_MI;
static PropertyInfo t6435____IsReadOnly_PropertyInfo = 
{
	&t6435_TI, "IsReadOnly", &m33333_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6435_PIs[] =
{
	&t6435____Count_PropertyInfo,
	&t6435____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33332_GM;
MethodInfo m33332_MI = 
{
	"get_Count", NULL, &t6435_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33332_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33333_GM;
MethodInfo m33333_MI = 
{
	"get_IsReadOnly", NULL, &t6435_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33333_GM};
extern Il2CppType t1409_0_0_0;
extern Il2CppType t1409_0_0_0;
static ParameterInfo t6435_m33334_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1409_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33334_GM;
MethodInfo m33334_MI = 
{
	"Add", NULL, &t6435_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6435_m33334_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33334_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33335_GM;
MethodInfo m33335_MI = 
{
	"Clear", NULL, &t6435_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33335_GM};
extern Il2CppType t1409_0_0_0;
static ParameterInfo t6435_m33336_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1409_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33336_GM;
MethodInfo m33336_MI = 
{
	"Contains", NULL, &t6435_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6435_m33336_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33336_GM};
extern Il2CppType t3671_0_0_0;
extern Il2CppType t3671_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6435_m33337_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3671_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33337_GM;
MethodInfo m33337_MI = 
{
	"CopyTo", NULL, &t6435_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6435_m33337_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33337_GM};
extern Il2CppType t1409_0_0_0;
static ParameterInfo t6435_m33338_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1409_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33338_GM;
MethodInfo m33338_MI = 
{
	"Remove", NULL, &t6435_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6435_m33338_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33338_GM};
static MethodInfo* t6435_MIs[] =
{
	&m33332_MI,
	&m33333_MI,
	&m33334_MI,
	&m33335_MI,
	&m33336_MI,
	&m33337_MI,
	&m33338_MI,
	NULL
};
extern TypeInfo t6437_TI;
static TypeInfo* t6435_ITIs[] = 
{
	&t603_TI,
	&t6437_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6435_0_0_0;
extern Il2CppType t6435_1_0_0;
struct t6435;
extern Il2CppGenericClass t6435_GC;
TypeInfo t6435_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6435_MIs, t6435_PIs, NULL, NULL, NULL, NULL, NULL, &t6435_TI, t6435_ITIs, NULL, &EmptyCustomAttributesCache, &t6435_TI, &t6435_0_0_0, &t6435_1_0_0, NULL, &t6435_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.InterfaceTypeAttribute>
extern Il2CppType t4927_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33339_GM;
MethodInfo m33339_MI = 
{
	"GetEnumerator", NULL, &t6437_TI, &t4927_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33339_GM};
static MethodInfo* t6437_MIs[] =
{
	&m33339_MI,
	NULL
};
static TypeInfo* t6437_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6437_0_0_0;
extern Il2CppType t6437_1_0_0;
struct t6437;
extern Il2CppGenericClass t6437_GC;
TypeInfo t6437_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6437_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6437_TI, t6437_ITIs, NULL, &EmptyCustomAttributesCache, &t6437_TI, &t6437_0_0_0, &t6437_1_0_0, NULL, &t6437_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6436_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.InterfaceTypeAttribute>
extern MethodInfo m33340_MI;
extern MethodInfo m33341_MI;
static PropertyInfo t6436____Item_PropertyInfo = 
{
	&t6436_TI, "Item", &m33340_MI, &m33341_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6436_PIs[] =
{
	&t6436____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1409_0_0_0;
static ParameterInfo t6436_m33342_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1409_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33342_GM;
MethodInfo m33342_MI = 
{
	"IndexOf", NULL, &t6436_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6436_m33342_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33342_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1409_0_0_0;
static ParameterInfo t6436_m33343_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1409_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33343_GM;
MethodInfo m33343_MI = 
{
	"Insert", NULL, &t6436_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6436_m33343_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33343_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6436_m33344_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33344_GM;
MethodInfo m33344_MI = 
{
	"RemoveAt", NULL, &t6436_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6436_m33344_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33344_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6436_m33340_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1409_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33340_GM;
MethodInfo m33340_MI = 
{
	"get_Item", NULL, &t6436_TI, &t1409_0_0_0, RuntimeInvoker_t29_t44, t6436_m33340_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33340_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1409_0_0_0;
static ParameterInfo t6436_m33341_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1409_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33341_GM;
MethodInfo m33341_MI = 
{
	"set_Item", NULL, &t6436_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6436_m33341_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33341_GM};
static MethodInfo* t6436_MIs[] =
{
	&m33342_MI,
	&m33343_MI,
	&m33344_MI,
	&m33340_MI,
	&m33341_MI,
	NULL
};
static TypeInfo* t6436_ITIs[] = 
{
	&t603_TI,
	&t6435_TI,
	&t6437_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6436_0_0_0;
extern Il2CppType t6436_1_0_0;
struct t6436;
extern Il2CppGenericClass t6436_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6436_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6436_MIs, t6436_PIs, NULL, NULL, NULL, NULL, NULL, &t6436_TI, t6436_ITIs, NULL, &t1908__CustomAttributeCache, &t6436_TI, &t6436_0_0_0, &t6436_1_0_0, NULL, &t6436_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4929_TI;

#include "t1412.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>
extern MethodInfo m33345_MI;
static PropertyInfo t4929____Current_PropertyInfo = 
{
	&t4929_TI, "Current", &m33345_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4929_PIs[] =
{
	&t4929____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1412_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33345_GM;
MethodInfo m33345_MI = 
{
	"get_Current", NULL, &t4929_TI, &t1412_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33345_GM};
static MethodInfo* t4929_MIs[] =
{
	&m33345_MI,
	NULL
};
static TypeInfo* t4929_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4929_0_0_0;
extern Il2CppType t4929_1_0_0;
struct t4929;
extern Il2CppGenericClass t4929_GC;
TypeInfo t4929_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4929_MIs, t4929_PIs, NULL, NULL, NULL, NULL, NULL, &t4929_TI, t4929_ITIs, NULL, &EmptyCustomAttributesCache, &t4929_TI, &t4929_0_0_0, &t4929_1_0_0, NULL, &t4929_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3417.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3417_TI;
#include "t3417MD.h"

extern TypeInfo t1412_TI;
extern MethodInfo m18959_MI;
extern MethodInfo m25534_MI;
struct t20;
#define m25534(__this, p0, method) (t1412 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.PreserveSigAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3417_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3417_TI, offsetof(t3417, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3417_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3417_TI, offsetof(t3417, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3417_FIs[] =
{
	&t3417_f0_FieldInfo,
	&t3417_f1_FieldInfo,
	NULL
};
extern MethodInfo m18956_MI;
static PropertyInfo t3417____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3417_TI, "System.Collections.IEnumerator.Current", &m18956_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3417____Current_PropertyInfo = 
{
	&t3417_TI, "Current", &m18959_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3417_PIs[] =
{
	&t3417____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3417____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3417_m18955_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18955_GM;
MethodInfo m18955_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3417_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3417_m18955_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18955_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18956_GM;
MethodInfo m18956_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3417_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18956_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18957_GM;
MethodInfo m18957_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3417_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18957_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18958_GM;
MethodInfo m18958_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3417_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18958_GM};
extern Il2CppType t1412_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18959_GM;
MethodInfo m18959_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3417_TI, &t1412_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18959_GM};
static MethodInfo* t3417_MIs[] =
{
	&m18955_MI,
	&m18956_MI,
	&m18957_MI,
	&m18958_MI,
	&m18959_MI,
	NULL
};
extern MethodInfo m18958_MI;
extern MethodInfo m18957_MI;
static MethodInfo* t3417_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18956_MI,
	&m18958_MI,
	&m18957_MI,
	&m18959_MI,
};
static TypeInfo* t3417_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4929_TI,
};
static Il2CppInterfaceOffsetPair t3417_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4929_TI, 7},
};
extern TypeInfo t1412_TI;
static Il2CppRGCTXData t3417_RGCTXData[3] = 
{
	&m18959_MI/* Method Usage */,
	&t1412_TI/* Class Usage */,
	&m25534_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3417_0_0_0;
extern Il2CppType t3417_1_0_0;
extern Il2CppGenericClass t3417_GC;
TypeInfo t3417_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3417_MIs, t3417_PIs, t3417_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3417_TI, t3417_ITIs, t3417_VT, &EmptyCustomAttributesCache, &t3417_TI, &t3417_0_0_0, &t3417_1_0_0, t3417_IOs, &t3417_GC, NULL, NULL, NULL, t3417_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3417)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6438_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.PreserveSigAttribute>
extern MethodInfo m33346_MI;
static PropertyInfo t6438____Count_PropertyInfo = 
{
	&t6438_TI, "Count", &m33346_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33347_MI;
static PropertyInfo t6438____IsReadOnly_PropertyInfo = 
{
	&t6438_TI, "IsReadOnly", &m33347_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6438_PIs[] =
{
	&t6438____Count_PropertyInfo,
	&t6438____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33346_GM;
MethodInfo m33346_MI = 
{
	"get_Count", NULL, &t6438_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33346_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33347_GM;
MethodInfo m33347_MI = 
{
	"get_IsReadOnly", NULL, &t6438_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33347_GM};
extern Il2CppType t1412_0_0_0;
extern Il2CppType t1412_0_0_0;
static ParameterInfo t6438_m33348_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1412_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33348_GM;
MethodInfo m33348_MI = 
{
	"Add", NULL, &t6438_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6438_m33348_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33348_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33349_GM;
MethodInfo m33349_MI = 
{
	"Clear", NULL, &t6438_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33349_GM};
extern Il2CppType t1412_0_0_0;
static ParameterInfo t6438_m33350_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1412_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33350_GM;
MethodInfo m33350_MI = 
{
	"Contains", NULL, &t6438_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6438_m33350_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33350_GM};
extern Il2CppType t3672_0_0_0;
extern Il2CppType t3672_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6438_m33351_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3672_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33351_GM;
MethodInfo m33351_MI = 
{
	"CopyTo", NULL, &t6438_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6438_m33351_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33351_GM};
extern Il2CppType t1412_0_0_0;
static ParameterInfo t6438_m33352_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1412_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33352_GM;
MethodInfo m33352_MI = 
{
	"Remove", NULL, &t6438_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6438_m33352_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33352_GM};
static MethodInfo* t6438_MIs[] =
{
	&m33346_MI,
	&m33347_MI,
	&m33348_MI,
	&m33349_MI,
	&m33350_MI,
	&m33351_MI,
	&m33352_MI,
	NULL
};
extern TypeInfo t6440_TI;
static TypeInfo* t6438_ITIs[] = 
{
	&t603_TI,
	&t6440_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6438_0_0_0;
extern Il2CppType t6438_1_0_0;
struct t6438;
extern Il2CppGenericClass t6438_GC;
TypeInfo t6438_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6438_MIs, t6438_PIs, NULL, NULL, NULL, NULL, NULL, &t6438_TI, t6438_ITIs, NULL, &EmptyCustomAttributesCache, &t6438_TI, &t6438_0_0_0, &t6438_1_0_0, NULL, &t6438_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.PreserveSigAttribute>
extern Il2CppType t4929_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33353_GM;
MethodInfo m33353_MI = 
{
	"GetEnumerator", NULL, &t6440_TI, &t4929_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33353_GM};
static MethodInfo* t6440_MIs[] =
{
	&m33353_MI,
	NULL
};
static TypeInfo* t6440_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6440_0_0_0;
extern Il2CppType t6440_1_0_0;
struct t6440;
extern Il2CppGenericClass t6440_GC;
TypeInfo t6440_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6440_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6440_TI, t6440_ITIs, NULL, &EmptyCustomAttributesCache, &t6440_TI, &t6440_0_0_0, &t6440_1_0_0, NULL, &t6440_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6439_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.PreserveSigAttribute>
extern MethodInfo m33354_MI;
extern MethodInfo m33355_MI;
static PropertyInfo t6439____Item_PropertyInfo = 
{
	&t6439_TI, "Item", &m33354_MI, &m33355_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6439_PIs[] =
{
	&t6439____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1412_0_0_0;
static ParameterInfo t6439_m33356_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1412_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33356_GM;
MethodInfo m33356_MI = 
{
	"IndexOf", NULL, &t6439_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6439_m33356_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33356_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1412_0_0_0;
static ParameterInfo t6439_m33357_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1412_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33357_GM;
MethodInfo m33357_MI = 
{
	"Insert", NULL, &t6439_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6439_m33357_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33357_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6439_m33358_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33358_GM;
MethodInfo m33358_MI = 
{
	"RemoveAt", NULL, &t6439_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6439_m33358_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33358_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6439_m33354_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1412_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33354_GM;
MethodInfo m33354_MI = 
{
	"get_Item", NULL, &t6439_TI, &t1412_0_0_0, RuntimeInvoker_t29_t44, t6439_m33354_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33354_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1412_0_0_0;
static ParameterInfo t6439_m33355_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1412_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33355_GM;
MethodInfo m33355_MI = 
{
	"set_Item", NULL, &t6439_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6439_m33355_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33355_GM};
static MethodInfo* t6439_MIs[] =
{
	&m33356_MI,
	&m33357_MI,
	&m33358_MI,
	&m33354_MI,
	&m33355_MI,
	NULL
};
static TypeInfo* t6439_ITIs[] = 
{
	&t603_TI,
	&t6438_TI,
	&t6440_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6439_0_0_0;
extern Il2CppType t6439_1_0_0;
struct t6439;
extern Il2CppGenericClass t6439_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6439_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6439_MIs, t6439_PIs, NULL, NULL, NULL, NULL, NULL, &t6439_TI, t6439_ITIs, NULL, &t1908__CustomAttributeCache, &t6439_TI, &t6439_0_0_0, &t6439_1_0_0, NULL, &t6439_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4931_TI;

#include "t1413.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>
extern MethodInfo m33359_MI;
static PropertyInfo t4931____Current_PropertyInfo = 
{
	&t4931_TI, "Current", &m33359_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4931_PIs[] =
{
	&t4931____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1413_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33359_GM;
MethodInfo m33359_MI = 
{
	"get_Current", NULL, &t4931_TI, &t1413_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33359_GM};
static MethodInfo* t4931_MIs[] =
{
	&m33359_MI,
	NULL
};
static TypeInfo* t4931_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4931_0_0_0;
extern Il2CppType t4931_1_0_0;
struct t4931;
extern Il2CppGenericClass t4931_GC;
TypeInfo t4931_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4931_MIs, t4931_PIs, NULL, NULL, NULL, NULL, NULL, &t4931_TI, t4931_ITIs, NULL, &EmptyCustomAttributesCache, &t4931_TI, &t4931_0_0_0, &t4931_1_0_0, NULL, &t4931_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3418.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3418_TI;
#include "t3418MD.h"

extern TypeInfo t1413_TI;
extern MethodInfo m18964_MI;
extern MethodInfo m25545_MI;
struct t20;
#define m25545(__this, p0, method) (t1413 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3418_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3418_TI, offsetof(t3418, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3418_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3418_TI, offsetof(t3418, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3418_FIs[] =
{
	&t3418_f0_FieldInfo,
	&t3418_f1_FieldInfo,
	NULL
};
extern MethodInfo m18961_MI;
static PropertyInfo t3418____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3418_TI, "System.Collections.IEnumerator.Current", &m18961_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3418____Current_PropertyInfo = 
{
	&t3418_TI, "Current", &m18964_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3418_PIs[] =
{
	&t3418____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3418____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3418_m18960_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18960_GM;
MethodInfo m18960_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3418_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3418_m18960_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18960_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18961_GM;
MethodInfo m18961_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3418_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18961_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18962_GM;
MethodInfo m18962_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3418_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18962_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18963_GM;
MethodInfo m18963_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3418_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18963_GM};
extern Il2CppType t1413_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18964_GM;
MethodInfo m18964_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3418_TI, &t1413_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18964_GM};
static MethodInfo* t3418_MIs[] =
{
	&m18960_MI,
	&m18961_MI,
	&m18962_MI,
	&m18963_MI,
	&m18964_MI,
	NULL
};
extern MethodInfo m18963_MI;
extern MethodInfo m18962_MI;
static MethodInfo* t3418_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18961_MI,
	&m18963_MI,
	&m18962_MI,
	&m18964_MI,
};
static TypeInfo* t3418_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4931_TI,
};
static Il2CppInterfaceOffsetPair t3418_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4931_TI, 7},
};
extern TypeInfo t1413_TI;
static Il2CppRGCTXData t3418_RGCTXData[3] = 
{
	&m18964_MI/* Method Usage */,
	&t1413_TI/* Class Usage */,
	&m25545_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3418_0_0_0;
extern Il2CppType t3418_1_0_0;
extern Il2CppGenericClass t3418_GC;
TypeInfo t3418_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3418_MIs, t3418_PIs, t3418_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3418_TI, t3418_ITIs, t3418_VT, &EmptyCustomAttributesCache, &t3418_TI, &t3418_0_0_0, &t3418_1_0_0, t3418_IOs, &t3418_GC, NULL, NULL, NULL, t3418_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3418)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6441_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>
extern MethodInfo m33360_MI;
static PropertyInfo t6441____Count_PropertyInfo = 
{
	&t6441_TI, "Count", &m33360_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33361_MI;
static PropertyInfo t6441____IsReadOnly_PropertyInfo = 
{
	&t6441_TI, "IsReadOnly", &m33361_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6441_PIs[] =
{
	&t6441____Count_PropertyInfo,
	&t6441____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33360_GM;
MethodInfo m33360_MI = 
{
	"get_Count", NULL, &t6441_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33360_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33361_GM;
MethodInfo m33361_MI = 
{
	"get_IsReadOnly", NULL, &t6441_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33361_GM};
extern Il2CppType t1413_0_0_0;
extern Il2CppType t1413_0_0_0;
static ParameterInfo t6441_m33362_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1413_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33362_GM;
MethodInfo m33362_MI = 
{
	"Add", NULL, &t6441_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6441_m33362_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33362_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33363_GM;
MethodInfo m33363_MI = 
{
	"Clear", NULL, &t6441_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33363_GM};
extern Il2CppType t1413_0_0_0;
static ParameterInfo t6441_m33364_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1413_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33364_GM;
MethodInfo m33364_MI = 
{
	"Contains", NULL, &t6441_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6441_m33364_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33364_GM};
extern Il2CppType t3673_0_0_0;
extern Il2CppType t3673_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6441_m33365_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3673_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33365_GM;
MethodInfo m33365_MI = 
{
	"CopyTo", NULL, &t6441_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6441_m33365_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33365_GM};
extern Il2CppType t1413_0_0_0;
static ParameterInfo t6441_m33366_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1413_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33366_GM;
MethodInfo m33366_MI = 
{
	"Remove", NULL, &t6441_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6441_m33366_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33366_GM};
static MethodInfo* t6441_MIs[] =
{
	&m33360_MI,
	&m33361_MI,
	&m33362_MI,
	&m33363_MI,
	&m33364_MI,
	&m33365_MI,
	&m33366_MI,
	NULL
};
extern TypeInfo t6443_TI;
static TypeInfo* t6441_ITIs[] = 
{
	&t603_TI,
	&t6443_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6441_0_0_0;
extern Il2CppType t6441_1_0_0;
struct t6441;
extern Il2CppGenericClass t6441_GC;
TypeInfo t6441_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6441_MIs, t6441_PIs, NULL, NULL, NULL, NULL, NULL, &t6441_TI, t6441_ITIs, NULL, &EmptyCustomAttributesCache, &t6441_TI, &t6441_0_0_0, &t6441_1_0_0, NULL, &t6441_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>
extern Il2CppType t4931_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33367_GM;
MethodInfo m33367_MI = 
{
	"GetEnumerator", NULL, &t6443_TI, &t4931_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33367_GM};
static MethodInfo* t6443_MIs[] =
{
	&m33367_MI,
	NULL
};
static TypeInfo* t6443_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6443_0_0_0;
extern Il2CppType t6443_1_0_0;
struct t6443;
extern Il2CppGenericClass t6443_GC;
TypeInfo t6443_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6443_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6443_TI, t6443_ITIs, NULL, &EmptyCustomAttributesCache, &t6443_TI, &t6443_0_0_0, &t6443_1_0_0, NULL, &t6443_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6442_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibImportClassAttribute>
extern MethodInfo m33368_MI;
extern MethodInfo m33369_MI;
static PropertyInfo t6442____Item_PropertyInfo = 
{
	&t6442_TI, "Item", &m33368_MI, &m33369_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6442_PIs[] =
{
	&t6442____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1413_0_0_0;
static ParameterInfo t6442_m33370_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1413_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33370_GM;
MethodInfo m33370_MI = 
{
	"IndexOf", NULL, &t6442_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6442_m33370_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33370_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1413_0_0_0;
static ParameterInfo t6442_m33371_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1413_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33371_GM;
MethodInfo m33371_MI = 
{
	"Insert", NULL, &t6442_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6442_m33371_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33371_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6442_m33372_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33372_GM;
MethodInfo m33372_MI = 
{
	"RemoveAt", NULL, &t6442_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6442_m33372_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33372_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6442_m33368_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1413_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33368_GM;
MethodInfo m33368_MI = 
{
	"get_Item", NULL, &t6442_TI, &t1413_0_0_0, RuntimeInvoker_t29_t44, t6442_m33368_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33368_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1413_0_0_0;
static ParameterInfo t6442_m33369_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1413_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33369_GM;
MethodInfo m33369_MI = 
{
	"set_Item", NULL, &t6442_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6442_m33369_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33369_GM};
static MethodInfo* t6442_MIs[] =
{
	&m33370_MI,
	&m33371_MI,
	&m33372_MI,
	&m33368_MI,
	&m33369_MI,
	NULL
};
static TypeInfo* t6442_ITIs[] = 
{
	&t603_TI,
	&t6441_TI,
	&t6443_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6442_0_0_0;
extern Il2CppType t6442_1_0_0;
struct t6442;
extern Il2CppGenericClass t6442_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6442_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6442_MIs, t6442_PIs, NULL, NULL, NULL, NULL, NULL, &t6442_TI, t6442_ITIs, NULL, &t1908__CustomAttributeCache, &t6442_TI, &t6442_0_0_0, &t6442_1_0_0, NULL, &t6442_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4933_TI;

#include "t1414.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>
extern MethodInfo m33373_MI;
static PropertyInfo t4933____Current_PropertyInfo = 
{
	&t4933_TI, "Current", &m33373_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4933_PIs[] =
{
	&t4933____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1414_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33373_GM;
MethodInfo m33373_MI = 
{
	"get_Current", NULL, &t4933_TI, &t1414_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33373_GM};
static MethodInfo* t4933_MIs[] =
{
	&m33373_MI,
	NULL
};
static TypeInfo* t4933_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4933_0_0_0;
extern Il2CppType t4933_1_0_0;
struct t4933;
extern Il2CppGenericClass t4933_GC;
TypeInfo t4933_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4933_MIs, t4933_PIs, NULL, NULL, NULL, NULL, NULL, &t4933_TI, t4933_ITIs, NULL, &EmptyCustomAttributesCache, &t4933_TI, &t4933_0_0_0, &t4933_1_0_0, NULL, &t4933_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3419.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3419_TI;
#include "t3419MD.h"

extern TypeInfo t1414_TI;
extern MethodInfo m18969_MI;
extern MethodInfo m25556_MI;
struct t20;
#define m25556(__this, p0, method) (t1414 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.TypeLibVersionAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3419_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3419_TI, offsetof(t3419, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3419_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3419_TI, offsetof(t3419, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3419_FIs[] =
{
	&t3419_f0_FieldInfo,
	&t3419_f1_FieldInfo,
	NULL
};
extern MethodInfo m18966_MI;
static PropertyInfo t3419____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3419_TI, "System.Collections.IEnumerator.Current", &m18966_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3419____Current_PropertyInfo = 
{
	&t3419_TI, "Current", &m18969_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3419_PIs[] =
{
	&t3419____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3419____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3419_m18965_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18965_GM;
MethodInfo m18965_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3419_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3419_m18965_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18965_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18966_GM;
MethodInfo m18966_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3419_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18966_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18967_GM;
MethodInfo m18967_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3419_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18967_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18968_GM;
MethodInfo m18968_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3419_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18968_GM};
extern Il2CppType t1414_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18969_GM;
MethodInfo m18969_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3419_TI, &t1414_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18969_GM};
static MethodInfo* t3419_MIs[] =
{
	&m18965_MI,
	&m18966_MI,
	&m18967_MI,
	&m18968_MI,
	&m18969_MI,
	NULL
};
extern MethodInfo m18968_MI;
extern MethodInfo m18967_MI;
static MethodInfo* t3419_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18966_MI,
	&m18968_MI,
	&m18967_MI,
	&m18969_MI,
};
static TypeInfo* t3419_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4933_TI,
};
static Il2CppInterfaceOffsetPair t3419_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4933_TI, 7},
};
extern TypeInfo t1414_TI;
static Il2CppRGCTXData t3419_RGCTXData[3] = 
{
	&m18969_MI/* Method Usage */,
	&t1414_TI/* Class Usage */,
	&m25556_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3419_0_0_0;
extern Il2CppType t3419_1_0_0;
extern Il2CppGenericClass t3419_GC;
TypeInfo t3419_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3419_MIs, t3419_PIs, t3419_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3419_TI, t3419_ITIs, t3419_VT, &EmptyCustomAttributesCache, &t3419_TI, &t3419_0_0_0, &t3419_1_0_0, t3419_IOs, &t3419_GC, NULL, NULL, NULL, t3419_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3419)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6444_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.TypeLibVersionAttribute>
extern MethodInfo m33374_MI;
static PropertyInfo t6444____Count_PropertyInfo = 
{
	&t6444_TI, "Count", &m33374_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33375_MI;
static PropertyInfo t6444____IsReadOnly_PropertyInfo = 
{
	&t6444_TI, "IsReadOnly", &m33375_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6444_PIs[] =
{
	&t6444____Count_PropertyInfo,
	&t6444____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33374_GM;
MethodInfo m33374_MI = 
{
	"get_Count", NULL, &t6444_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33374_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33375_GM;
MethodInfo m33375_MI = 
{
	"get_IsReadOnly", NULL, &t6444_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33375_GM};
extern Il2CppType t1414_0_0_0;
extern Il2CppType t1414_0_0_0;
static ParameterInfo t6444_m33376_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1414_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33376_GM;
MethodInfo m33376_MI = 
{
	"Add", NULL, &t6444_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6444_m33376_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33376_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33377_GM;
MethodInfo m33377_MI = 
{
	"Clear", NULL, &t6444_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33377_GM};
extern Il2CppType t1414_0_0_0;
static ParameterInfo t6444_m33378_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1414_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33378_GM;
MethodInfo m33378_MI = 
{
	"Contains", NULL, &t6444_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6444_m33378_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33378_GM};
extern Il2CppType t3674_0_0_0;
extern Il2CppType t3674_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6444_m33379_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3674_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33379_GM;
MethodInfo m33379_MI = 
{
	"CopyTo", NULL, &t6444_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6444_m33379_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33379_GM};
extern Il2CppType t1414_0_0_0;
static ParameterInfo t6444_m33380_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1414_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33380_GM;
MethodInfo m33380_MI = 
{
	"Remove", NULL, &t6444_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6444_m33380_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33380_GM};
static MethodInfo* t6444_MIs[] =
{
	&m33374_MI,
	&m33375_MI,
	&m33376_MI,
	&m33377_MI,
	&m33378_MI,
	&m33379_MI,
	&m33380_MI,
	NULL
};
extern TypeInfo t6446_TI;
static TypeInfo* t6444_ITIs[] = 
{
	&t603_TI,
	&t6446_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6444_0_0_0;
extern Il2CppType t6444_1_0_0;
struct t6444;
extern Il2CppGenericClass t6444_GC;
TypeInfo t6444_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6444_MIs, t6444_PIs, NULL, NULL, NULL, NULL, NULL, &t6444_TI, t6444_ITIs, NULL, &EmptyCustomAttributesCache, &t6444_TI, &t6444_0_0_0, &t6444_1_0_0, NULL, &t6444_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.TypeLibVersionAttribute>
extern Il2CppType t4933_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33381_GM;
MethodInfo m33381_MI = 
{
	"GetEnumerator", NULL, &t6446_TI, &t4933_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33381_GM};
static MethodInfo* t6446_MIs[] =
{
	&m33381_MI,
	NULL
};
static TypeInfo* t6446_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6446_0_0_0;
extern Il2CppType t6446_1_0_0;
struct t6446;
extern Il2CppGenericClass t6446_GC;
TypeInfo t6446_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6446_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6446_TI, t6446_ITIs, NULL, &EmptyCustomAttributesCache, &t6446_TI, &t6446_0_0_0, &t6446_1_0_0, NULL, &t6446_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6445_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.TypeLibVersionAttribute>
extern MethodInfo m33382_MI;
extern MethodInfo m33383_MI;
static PropertyInfo t6445____Item_PropertyInfo = 
{
	&t6445_TI, "Item", &m33382_MI, &m33383_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6445_PIs[] =
{
	&t6445____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1414_0_0_0;
static ParameterInfo t6445_m33384_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1414_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33384_GM;
MethodInfo m33384_MI = 
{
	"IndexOf", NULL, &t6445_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6445_m33384_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33384_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1414_0_0_0;
static ParameterInfo t6445_m33385_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1414_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33385_GM;
MethodInfo m33385_MI = 
{
	"Insert", NULL, &t6445_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6445_m33385_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33385_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6445_m33386_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33386_GM;
MethodInfo m33386_MI = 
{
	"RemoveAt", NULL, &t6445_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6445_m33386_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33386_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6445_m33382_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1414_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33382_GM;
MethodInfo m33382_MI = 
{
	"get_Item", NULL, &t6445_TI, &t1414_0_0_0, RuntimeInvoker_t29_t44, t6445_m33382_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33382_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1414_0_0_0;
static ParameterInfo t6445_m33383_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1414_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33383_GM;
MethodInfo m33383_MI = 
{
	"set_Item", NULL, &t6445_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6445_m33383_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33383_GM};
static MethodInfo* t6445_MIs[] =
{
	&m33384_MI,
	&m33385_MI,
	&m33386_MI,
	&m33382_MI,
	&m33383_MI,
	NULL
};
static TypeInfo* t6445_ITIs[] = 
{
	&t603_TI,
	&t6444_TI,
	&t6446_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6445_0_0_0;
extern Il2CppType t6445_1_0_0;
struct t6445;
extern Il2CppGenericClass t6445_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6445_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6445_MIs, t6445_PIs, NULL, NULL, NULL, NULL, NULL, &t6445_TI, t6445_ITIs, NULL, &t1908__CustomAttributeCache, &t6445_TI, &t6445_0_0_0, &t6445_1_0_0, NULL, &t6445_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4935_TI;

#include "t1156.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.UnmanagedType>
extern MethodInfo m33387_MI;
static PropertyInfo t4935____Current_PropertyInfo = 
{
	&t4935_TI, "Current", &m33387_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4935_PIs[] =
{
	&t4935____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1156_0_0_0;
extern void* RuntimeInvoker_t1156 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33387_GM;
MethodInfo m33387_MI = 
{
	"get_Current", NULL, &t4935_TI, &t1156_0_0_0, RuntimeInvoker_t1156, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33387_GM};
static MethodInfo* t4935_MIs[] =
{
	&m33387_MI,
	NULL
};
static TypeInfo* t4935_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4935_0_0_0;
extern Il2CppType t4935_1_0_0;
struct t4935;
extern Il2CppGenericClass t4935_GC;
TypeInfo t4935_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4935_MIs, t4935_PIs, NULL, NULL, NULL, NULL, NULL, &t4935_TI, t4935_ITIs, NULL, &EmptyCustomAttributesCache, &t4935_TI, &t4935_0_0_0, &t4935_1_0_0, NULL, &t4935_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3420.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3420_TI;
#include "t3420MD.h"

extern TypeInfo t1156_TI;
extern MethodInfo m18974_MI;
extern MethodInfo m25567_MI;
struct t20;
 int32_t m25567 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18970_MI;
 void m18970 (t3420 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18971_MI;
 t29 * m18971 (t3420 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18974(__this, &m18974_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1156_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18972_MI;
 void m18972 (t3420 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18973_MI;
 bool m18973 (t3420 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18974 (t3420 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25567(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25567_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.UnmanagedType>
extern Il2CppType t20_0_0_1;
FieldInfo t3420_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3420_TI, offsetof(t3420, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3420_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3420_TI, offsetof(t3420, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3420_FIs[] =
{
	&t3420_f0_FieldInfo,
	&t3420_f1_FieldInfo,
	NULL
};
static PropertyInfo t3420____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3420_TI, "System.Collections.IEnumerator.Current", &m18971_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3420____Current_PropertyInfo = 
{
	&t3420_TI, "Current", &m18974_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3420_PIs[] =
{
	&t3420____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3420____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3420_m18970_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18970_GM;
MethodInfo m18970_MI = 
{
	".ctor", (methodPointerType)&m18970, &t3420_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3420_m18970_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18970_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18971_GM;
MethodInfo m18971_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18971, &t3420_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18971_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18972_GM;
MethodInfo m18972_MI = 
{
	"Dispose", (methodPointerType)&m18972, &t3420_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18972_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18973_GM;
MethodInfo m18973_MI = 
{
	"MoveNext", (methodPointerType)&m18973, &t3420_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18973_GM};
extern Il2CppType t1156_0_0_0;
extern void* RuntimeInvoker_t1156 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18974_GM;
MethodInfo m18974_MI = 
{
	"get_Current", (methodPointerType)&m18974, &t3420_TI, &t1156_0_0_0, RuntimeInvoker_t1156, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18974_GM};
static MethodInfo* t3420_MIs[] =
{
	&m18970_MI,
	&m18971_MI,
	&m18972_MI,
	&m18973_MI,
	&m18974_MI,
	NULL
};
static MethodInfo* t3420_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18971_MI,
	&m18973_MI,
	&m18972_MI,
	&m18974_MI,
};
static TypeInfo* t3420_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4935_TI,
};
static Il2CppInterfaceOffsetPair t3420_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4935_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3420_0_0_0;
extern Il2CppType t3420_1_0_0;
extern Il2CppGenericClass t3420_GC;
TypeInfo t3420_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3420_MIs, t3420_PIs, t3420_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3420_TI, t3420_ITIs, t3420_VT, &EmptyCustomAttributesCache, &t3420_TI, &t3420_0_0_0, &t3420_1_0_0, t3420_IOs, &t3420_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3420)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6447_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.UnmanagedType>
extern MethodInfo m33388_MI;
static PropertyInfo t6447____Count_PropertyInfo = 
{
	&t6447_TI, "Count", &m33388_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33389_MI;
static PropertyInfo t6447____IsReadOnly_PropertyInfo = 
{
	&t6447_TI, "IsReadOnly", &m33389_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6447_PIs[] =
{
	&t6447____Count_PropertyInfo,
	&t6447____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33388_GM;
MethodInfo m33388_MI = 
{
	"get_Count", NULL, &t6447_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33388_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33389_GM;
MethodInfo m33389_MI = 
{
	"get_IsReadOnly", NULL, &t6447_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33389_GM};
extern Il2CppType t1156_0_0_0;
extern Il2CppType t1156_0_0_0;
static ParameterInfo t6447_m33390_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33390_GM;
MethodInfo m33390_MI = 
{
	"Add", NULL, &t6447_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6447_m33390_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33390_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33391_GM;
MethodInfo m33391_MI = 
{
	"Clear", NULL, &t6447_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33391_GM};
extern Il2CppType t1156_0_0_0;
static ParameterInfo t6447_m33392_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1156_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33392_GM;
MethodInfo m33392_MI = 
{
	"Contains", NULL, &t6447_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6447_m33392_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33392_GM};
extern Il2CppType t3675_0_0_0;
extern Il2CppType t3675_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6447_m33393_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3675_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33393_GM;
MethodInfo m33393_MI = 
{
	"CopyTo", NULL, &t6447_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6447_m33393_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33393_GM};
extern Il2CppType t1156_0_0_0;
static ParameterInfo t6447_m33394_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1156_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33394_GM;
MethodInfo m33394_MI = 
{
	"Remove", NULL, &t6447_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6447_m33394_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33394_GM};
static MethodInfo* t6447_MIs[] =
{
	&m33388_MI,
	&m33389_MI,
	&m33390_MI,
	&m33391_MI,
	&m33392_MI,
	&m33393_MI,
	&m33394_MI,
	NULL
};
extern TypeInfo t6449_TI;
static TypeInfo* t6447_ITIs[] = 
{
	&t603_TI,
	&t6449_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6447_0_0_0;
extern Il2CppType t6447_1_0_0;
struct t6447;
extern Il2CppGenericClass t6447_GC;
TypeInfo t6447_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6447_MIs, t6447_PIs, NULL, NULL, NULL, NULL, NULL, &t6447_TI, t6447_ITIs, NULL, &EmptyCustomAttributesCache, &t6447_TI, &t6447_0_0_0, &t6447_1_0_0, NULL, &t6447_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices.UnmanagedType>
extern Il2CppType t4935_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33395_GM;
MethodInfo m33395_MI = 
{
	"GetEnumerator", NULL, &t6449_TI, &t4935_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33395_GM};
static MethodInfo* t6449_MIs[] =
{
	&m33395_MI,
	NULL
};
static TypeInfo* t6449_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6449_0_0_0;
extern Il2CppType t6449_1_0_0;
struct t6449;
extern Il2CppGenericClass t6449_GC;
TypeInfo t6449_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6449_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6449_TI, t6449_ITIs, NULL, &EmptyCustomAttributesCache, &t6449_TI, &t6449_0_0_0, &t6449_1_0_0, NULL, &t6449_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6448_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices.UnmanagedType>
extern MethodInfo m33396_MI;
extern MethodInfo m33397_MI;
static PropertyInfo t6448____Item_PropertyInfo = 
{
	&t6448_TI, "Item", &m33396_MI, &m33397_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6448_PIs[] =
{
	&t6448____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1156_0_0_0;
static ParameterInfo t6448_m33398_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1156_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33398_GM;
MethodInfo m33398_MI = 
{
	"IndexOf", NULL, &t6448_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6448_m33398_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33398_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1156_0_0_0;
static ParameterInfo t6448_m33399_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33399_GM;
MethodInfo m33399_MI = 
{
	"Insert", NULL, &t6448_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6448_m33399_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33399_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6448_m33400_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33400_GM;
MethodInfo m33400_MI = 
{
	"RemoveAt", NULL, &t6448_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6448_m33400_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33400_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6448_m33396_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1156_0_0_0;
extern void* RuntimeInvoker_t1156_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33396_GM;
MethodInfo m33396_MI = 
{
	"get_Item", NULL, &t6448_TI, &t1156_0_0_0, RuntimeInvoker_t1156_t44, t6448_m33396_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33396_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1156_0_0_0;
static ParameterInfo t6448_m33397_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33397_GM;
MethodInfo m33397_MI = 
{
	"set_Item", NULL, &t6448_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6448_m33397_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33397_GM};
static MethodInfo* t6448_MIs[] =
{
	&m33398_MI,
	&m33399_MI,
	&m33400_MI,
	&m33396_MI,
	&m33397_MI,
	NULL
};
static TypeInfo* t6448_ITIs[] = 
{
	&t603_TI,
	&t6447_TI,
	&t6449_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6448_0_0_0;
extern Il2CppType t6448_1_0_0;
struct t6448;
extern Il2CppGenericClass t6448_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6448_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6448_MIs, t6448_PIs, NULL, NULL, NULL, NULL, NULL, &t6448_TI, t6448_ITIs, NULL, &t1908__CustomAttributeCache, &t6448_TI, &t6448_0_0_0, &t6448_1_0_0, NULL, &t6448_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4937_TI;

#include "t1422.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>
extern MethodInfo m33401_MI;
static PropertyInfo t4937____Current_PropertyInfo = 
{
	&t4937_TI, "Current", &m33401_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4937_PIs[] =
{
	&t4937____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1422_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33401_GM;
MethodInfo m33401_MI = 
{
	"get_Current", NULL, &t4937_TI, &t1422_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33401_GM};
static MethodInfo* t4937_MIs[] =
{
	&m33401_MI,
	NULL
};
static TypeInfo* t4937_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4937_0_0_0;
extern Il2CppType t4937_1_0_0;
struct t4937;
extern Il2CppGenericClass t4937_GC;
TypeInfo t4937_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4937_MIs, t4937_PIs, NULL, NULL, NULL, NULL, NULL, &t4937_TI, t4937_ITIs, NULL, &EmptyCustomAttributesCache, &t4937_TI, &t4937_0_0_0, &t4937_1_0_0, NULL, &t4937_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3421.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3421_TI;
#include "t3421MD.h"

extern TypeInfo t1422_TI;
extern MethodInfo m18979_MI;
extern MethodInfo m25578_MI;
struct t20;
#define m25578(__this, p0, method) (t1422 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Activation.UrlAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3421_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3421_TI, offsetof(t3421, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3421_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3421_TI, offsetof(t3421, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3421_FIs[] =
{
	&t3421_f0_FieldInfo,
	&t3421_f1_FieldInfo,
	NULL
};
extern MethodInfo m18976_MI;
static PropertyInfo t3421____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3421_TI, "System.Collections.IEnumerator.Current", &m18976_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3421____Current_PropertyInfo = 
{
	&t3421_TI, "Current", &m18979_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3421_PIs[] =
{
	&t3421____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3421____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3421_m18975_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18975_GM;
MethodInfo m18975_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3421_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3421_m18975_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18975_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18976_GM;
MethodInfo m18976_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3421_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18976_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18977_GM;
MethodInfo m18977_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3421_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18977_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18978_GM;
MethodInfo m18978_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3421_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18978_GM};
extern Il2CppType t1422_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18979_GM;
MethodInfo m18979_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3421_TI, &t1422_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18979_GM};
static MethodInfo* t3421_MIs[] =
{
	&m18975_MI,
	&m18976_MI,
	&m18977_MI,
	&m18978_MI,
	&m18979_MI,
	NULL
};
extern MethodInfo m18978_MI;
extern MethodInfo m18977_MI;
static MethodInfo* t3421_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18976_MI,
	&m18978_MI,
	&m18977_MI,
	&m18979_MI,
};
static TypeInfo* t3421_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4937_TI,
};
static Il2CppInterfaceOffsetPair t3421_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4937_TI, 7},
};
extern TypeInfo t1422_TI;
static Il2CppRGCTXData t3421_RGCTXData[3] = 
{
	&m18979_MI/* Method Usage */,
	&t1422_TI/* Class Usage */,
	&m25578_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3421_0_0_0;
extern Il2CppType t3421_1_0_0;
extern Il2CppGenericClass t3421_GC;
TypeInfo t3421_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3421_MIs, t3421_PIs, t3421_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3421_TI, t3421_ITIs, t3421_VT, &EmptyCustomAttributesCache, &t3421_TI, &t3421_0_0_0, &t3421_1_0_0, t3421_IOs, &t3421_GC, NULL, NULL, NULL, t3421_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3421)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6450_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Activation.UrlAttribute>
extern MethodInfo m33402_MI;
static PropertyInfo t6450____Count_PropertyInfo = 
{
	&t6450_TI, "Count", &m33402_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33403_MI;
static PropertyInfo t6450____IsReadOnly_PropertyInfo = 
{
	&t6450_TI, "IsReadOnly", &m33403_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6450_PIs[] =
{
	&t6450____Count_PropertyInfo,
	&t6450____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33402_GM;
MethodInfo m33402_MI = 
{
	"get_Count", NULL, &t6450_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33402_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33403_GM;
MethodInfo m33403_MI = 
{
	"get_IsReadOnly", NULL, &t6450_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33403_GM};
extern Il2CppType t1422_0_0_0;
extern Il2CppType t1422_0_0_0;
static ParameterInfo t6450_m33404_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1422_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33404_GM;
MethodInfo m33404_MI = 
{
	"Add", NULL, &t6450_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6450_m33404_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33404_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33405_GM;
MethodInfo m33405_MI = 
{
	"Clear", NULL, &t6450_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33405_GM};
extern Il2CppType t1422_0_0_0;
static ParameterInfo t6450_m33406_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1422_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33406_GM;
MethodInfo m33406_MI = 
{
	"Contains", NULL, &t6450_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6450_m33406_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33406_GM};
extern Il2CppType t3676_0_0_0;
extern Il2CppType t3676_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6450_m33407_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3676_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33407_GM;
MethodInfo m33407_MI = 
{
	"CopyTo", NULL, &t6450_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6450_m33407_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33407_GM};
extern Il2CppType t1422_0_0_0;
static ParameterInfo t6450_m33408_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1422_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33408_GM;
MethodInfo m33408_MI = 
{
	"Remove", NULL, &t6450_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6450_m33408_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33408_GM};
static MethodInfo* t6450_MIs[] =
{
	&m33402_MI,
	&m33403_MI,
	&m33404_MI,
	&m33405_MI,
	&m33406_MI,
	&m33407_MI,
	&m33408_MI,
	NULL
};
extern TypeInfo t6452_TI;
static TypeInfo* t6450_ITIs[] = 
{
	&t603_TI,
	&t6452_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6450_0_0_0;
extern Il2CppType t6450_1_0_0;
struct t6450;
extern Il2CppGenericClass t6450_GC;
TypeInfo t6450_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6450_MIs, t6450_PIs, NULL, NULL, NULL, NULL, NULL, &t6450_TI, t6450_ITIs, NULL, &EmptyCustomAttributesCache, &t6450_TI, &t6450_0_0_0, &t6450_1_0_0, NULL, &t6450_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Activation.UrlAttribute>
extern Il2CppType t4937_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33409_GM;
MethodInfo m33409_MI = 
{
	"GetEnumerator", NULL, &t6452_TI, &t4937_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33409_GM};
static MethodInfo* t6452_MIs[] =
{
	&m33409_MI,
	NULL
};
static TypeInfo* t6452_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6452_0_0_0;
extern Il2CppType t6452_1_0_0;
struct t6452;
extern Il2CppGenericClass t6452_GC;
TypeInfo t6452_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6452_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6452_TI, t6452_ITIs, NULL, &EmptyCustomAttributesCache, &t6452_TI, &t6452_0_0_0, &t6452_1_0_0, NULL, &t6452_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6451_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Activation.UrlAttribute>
extern MethodInfo m33410_MI;
extern MethodInfo m33411_MI;
static PropertyInfo t6451____Item_PropertyInfo = 
{
	&t6451_TI, "Item", &m33410_MI, &m33411_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6451_PIs[] =
{
	&t6451____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1422_0_0_0;
static ParameterInfo t6451_m33412_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1422_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33412_GM;
MethodInfo m33412_MI = 
{
	"IndexOf", NULL, &t6451_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6451_m33412_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33412_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1422_0_0_0;
static ParameterInfo t6451_m33413_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1422_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33413_GM;
MethodInfo m33413_MI = 
{
	"Insert", NULL, &t6451_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6451_m33413_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33413_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6451_m33414_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33414_GM;
MethodInfo m33414_MI = 
{
	"RemoveAt", NULL, &t6451_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6451_m33414_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33414_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6451_m33410_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1422_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33410_GM;
MethodInfo m33410_MI = 
{
	"get_Item", NULL, &t6451_TI, &t1422_0_0_0, RuntimeInvoker_t29_t44, t6451_m33410_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33410_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1422_0_0_0;
static ParameterInfo t6451_m33411_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1422_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33411_GM;
MethodInfo m33411_MI = 
{
	"set_Item", NULL, &t6451_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6451_m33411_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33411_GM};
static MethodInfo* t6451_MIs[] =
{
	&m33412_MI,
	&m33413_MI,
	&m33414_MI,
	&m33410_MI,
	&m33411_MI,
	NULL
};
static TypeInfo* t6451_ITIs[] = 
{
	&t603_TI,
	&t6450_TI,
	&t6452_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6451_0_0_0;
extern Il2CppType t6451_1_0_0;
struct t6451;
extern Il2CppGenericClass t6451_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6451_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6451_MIs, t6451_PIs, NULL, NULL, NULL, NULL, NULL, &t6451_TI, t6451_ITIs, NULL, &t1908__CustomAttributeCache, &t6451_TI, &t6451_0_0_0, &t6451_1_0_0, NULL, &t6451_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6453_TI;

#include "t1423.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.ContextAttribute>
extern MethodInfo m33415_MI;
static PropertyInfo t6453____Count_PropertyInfo = 
{
	&t6453_TI, "Count", &m33415_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33416_MI;
static PropertyInfo t6453____IsReadOnly_PropertyInfo = 
{
	&t6453_TI, "IsReadOnly", &m33416_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6453_PIs[] =
{
	&t6453____Count_PropertyInfo,
	&t6453____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33415_GM;
MethodInfo m33415_MI = 
{
	"get_Count", NULL, &t6453_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33415_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33416_GM;
MethodInfo m33416_MI = 
{
	"get_IsReadOnly", NULL, &t6453_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33416_GM};
extern Il2CppType t1423_0_0_0;
extern Il2CppType t1423_0_0_0;
static ParameterInfo t6453_m33417_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1423_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33417_GM;
MethodInfo m33417_MI = 
{
	"Add", NULL, &t6453_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6453_m33417_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33417_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33418_GM;
MethodInfo m33418_MI = 
{
	"Clear", NULL, &t6453_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33418_GM};
extern Il2CppType t1423_0_0_0;
static ParameterInfo t6453_m33419_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1423_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33419_GM;
MethodInfo m33419_MI = 
{
	"Contains", NULL, &t6453_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6453_m33419_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33419_GM};
extern Il2CppType t3677_0_0_0;
extern Il2CppType t3677_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6453_m33420_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3677_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33420_GM;
MethodInfo m33420_MI = 
{
	"CopyTo", NULL, &t6453_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6453_m33420_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33420_GM};
extern Il2CppType t1423_0_0_0;
static ParameterInfo t6453_m33421_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1423_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33421_GM;
MethodInfo m33421_MI = 
{
	"Remove", NULL, &t6453_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6453_m33421_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33421_GM};
static MethodInfo* t6453_MIs[] =
{
	&m33415_MI,
	&m33416_MI,
	&m33417_MI,
	&m33418_MI,
	&m33419_MI,
	&m33420_MI,
	&m33421_MI,
	NULL
};
extern TypeInfo t6455_TI;
static TypeInfo* t6453_ITIs[] = 
{
	&t603_TI,
	&t6455_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6453_0_0_0;
extern Il2CppType t6453_1_0_0;
struct t6453;
extern Il2CppGenericClass t6453_GC;
TypeInfo t6453_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6453_MIs, t6453_PIs, NULL, NULL, NULL, NULL, NULL, &t6453_TI, t6453_ITIs, NULL, &EmptyCustomAttributesCache, &t6453_TI, &t6453_0_0_0, &t6453_1_0_0, NULL, &t6453_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.ContextAttribute>
extern Il2CppType t4939_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33422_GM;
MethodInfo m33422_MI = 
{
	"GetEnumerator", NULL, &t6455_TI, &t4939_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33422_GM};
static MethodInfo* t6455_MIs[] =
{
	&m33422_MI,
	NULL
};
static TypeInfo* t6455_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6455_0_0_0;
extern Il2CppType t6455_1_0_0;
struct t6455;
extern Il2CppGenericClass t6455_GC;
TypeInfo t6455_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6455_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6455_TI, t6455_ITIs, NULL, &EmptyCustomAttributesCache, &t6455_TI, &t6455_0_0_0, &t6455_1_0_0, NULL, &t6455_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4939_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>
extern MethodInfo m33423_MI;
static PropertyInfo t4939____Current_PropertyInfo = 
{
	&t4939_TI, "Current", &m33423_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4939_PIs[] =
{
	&t4939____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1423_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33423_GM;
MethodInfo m33423_MI = 
{
	"get_Current", NULL, &t4939_TI, &t1423_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33423_GM};
static MethodInfo* t4939_MIs[] =
{
	&m33423_MI,
	NULL
};
static TypeInfo* t4939_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4939_0_0_0;
extern Il2CppType t4939_1_0_0;
struct t4939;
extern Il2CppGenericClass t4939_GC;
TypeInfo t4939_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4939_MIs, t4939_PIs, NULL, NULL, NULL, NULL, NULL, &t4939_TI, t4939_ITIs, NULL, &EmptyCustomAttributesCache, &t4939_TI, &t4939_0_0_0, &t4939_1_0_0, NULL, &t4939_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3422.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3422_TI;
#include "t3422MD.h"

extern TypeInfo t1423_TI;
extern MethodInfo m18984_MI;
extern MethodInfo m25589_MI;
struct t20;
#define m25589(__this, p0, method) (t1423 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.ContextAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3422_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3422_TI, offsetof(t3422, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3422_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3422_TI, offsetof(t3422, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3422_FIs[] =
{
	&t3422_f0_FieldInfo,
	&t3422_f1_FieldInfo,
	NULL
};
extern MethodInfo m18981_MI;
static PropertyInfo t3422____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3422_TI, "System.Collections.IEnumerator.Current", &m18981_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3422____Current_PropertyInfo = 
{
	&t3422_TI, "Current", &m18984_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3422_PIs[] =
{
	&t3422____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3422____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3422_m18980_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18980_GM;
MethodInfo m18980_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3422_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3422_m18980_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18980_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18981_GM;
MethodInfo m18981_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3422_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18981_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18982_GM;
MethodInfo m18982_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3422_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18982_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18983_GM;
MethodInfo m18983_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3422_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18983_GM};
extern Il2CppType t1423_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18984_GM;
MethodInfo m18984_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3422_TI, &t1423_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18984_GM};
static MethodInfo* t3422_MIs[] =
{
	&m18980_MI,
	&m18981_MI,
	&m18982_MI,
	&m18983_MI,
	&m18984_MI,
	NULL
};
extern MethodInfo m18983_MI;
extern MethodInfo m18982_MI;
static MethodInfo* t3422_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18981_MI,
	&m18983_MI,
	&m18982_MI,
	&m18984_MI,
};
static TypeInfo* t3422_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4939_TI,
};
static Il2CppInterfaceOffsetPair t3422_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4939_TI, 7},
};
extern TypeInfo t1423_TI;
static Il2CppRGCTXData t3422_RGCTXData[3] = 
{
	&m18984_MI/* Method Usage */,
	&t1423_TI/* Class Usage */,
	&m25589_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3422_0_0_0;
extern Il2CppType t3422_1_0_0;
extern Il2CppGenericClass t3422_GC;
TypeInfo t3422_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3422_MIs, t3422_PIs, t3422_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3422_TI, t3422_ITIs, t3422_VT, &EmptyCustomAttributesCache, &t3422_TI, &t3422_0_0_0, &t3422_1_0_0, t3422_IOs, &t3422_GC, NULL, NULL, NULL, t3422_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3422)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6454_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.ContextAttribute>
extern MethodInfo m33424_MI;
extern MethodInfo m33425_MI;
static PropertyInfo t6454____Item_PropertyInfo = 
{
	&t6454_TI, "Item", &m33424_MI, &m33425_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6454_PIs[] =
{
	&t6454____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1423_0_0_0;
static ParameterInfo t6454_m33426_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1423_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33426_GM;
MethodInfo m33426_MI = 
{
	"IndexOf", NULL, &t6454_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6454_m33426_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33426_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1423_0_0_0;
static ParameterInfo t6454_m33427_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1423_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33427_GM;
MethodInfo m33427_MI = 
{
	"Insert", NULL, &t6454_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6454_m33427_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33427_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6454_m33428_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33428_GM;
MethodInfo m33428_MI = 
{
	"RemoveAt", NULL, &t6454_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6454_m33428_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33428_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6454_m33424_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1423_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33424_GM;
MethodInfo m33424_MI = 
{
	"get_Item", NULL, &t6454_TI, &t1423_0_0_0, RuntimeInvoker_t29_t44, t6454_m33424_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33424_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1423_0_0_0;
static ParameterInfo t6454_m33425_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1423_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33425_GM;
MethodInfo m33425_MI = 
{
	"set_Item", NULL, &t6454_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6454_m33425_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33425_GM};
static MethodInfo* t6454_MIs[] =
{
	&m33426_MI,
	&m33427_MI,
	&m33428_MI,
	&m33424_MI,
	&m33425_MI,
	NULL
};
static TypeInfo* t6454_ITIs[] = 
{
	&t603_TI,
	&t6453_TI,
	&t6455_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6454_0_0_0;
extern Il2CppType t6454_1_0_0;
struct t6454;
extern Il2CppGenericClass t6454_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6454_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6454_MIs, t6454_PIs, NULL, NULL, NULL, NULL, NULL, &t6454_TI, t6454_ITIs, NULL, &t1908__CustomAttributeCache, &t6454_TI, &t6454_0_0_0, &t6454_1_0_0, NULL, &t6454_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6456_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextAttribute>
extern MethodInfo m33429_MI;
static PropertyInfo t6456____Count_PropertyInfo = 
{
	&t6456_TI, "Count", &m33429_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33430_MI;
static PropertyInfo t6456____IsReadOnly_PropertyInfo = 
{
	&t6456_TI, "IsReadOnly", &m33430_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6456_PIs[] =
{
	&t6456____Count_PropertyInfo,
	&t6456____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33429_GM;
MethodInfo m33429_MI = 
{
	"get_Count", NULL, &t6456_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33429_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33430_GM;
MethodInfo m33430_MI = 
{
	"get_IsReadOnly", NULL, &t6456_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33430_GM};
extern Il2CppType t2040_0_0_0;
extern Il2CppType t2040_0_0_0;
static ParameterInfo t6456_m33431_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2040_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33431_GM;
MethodInfo m33431_MI = 
{
	"Add", NULL, &t6456_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6456_m33431_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33431_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33432_GM;
MethodInfo m33432_MI = 
{
	"Clear", NULL, &t6456_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33432_GM};
extern Il2CppType t2040_0_0_0;
static ParameterInfo t6456_m33433_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2040_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33433_GM;
MethodInfo m33433_MI = 
{
	"Contains", NULL, &t6456_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6456_m33433_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33433_GM};
extern Il2CppType t1474_0_0_0;
extern Il2CppType t1474_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6456_m33434_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1474_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33434_GM;
MethodInfo m33434_MI = 
{
	"CopyTo", NULL, &t6456_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6456_m33434_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33434_GM};
extern Il2CppType t2040_0_0_0;
static ParameterInfo t6456_m33435_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2040_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33435_GM;
MethodInfo m33435_MI = 
{
	"Remove", NULL, &t6456_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6456_m33435_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33435_GM};
static MethodInfo* t6456_MIs[] =
{
	&m33429_MI,
	&m33430_MI,
	&m33431_MI,
	&m33432_MI,
	&m33433_MI,
	&m33434_MI,
	&m33435_MI,
	NULL
};
extern TypeInfo t6458_TI;
static TypeInfo* t6456_ITIs[] = 
{
	&t603_TI,
	&t6458_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6456_0_0_0;
extern Il2CppType t6456_1_0_0;
struct t6456;
extern Il2CppGenericClass t6456_GC;
TypeInfo t6456_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6456_MIs, t6456_PIs, NULL, NULL, NULL, NULL, NULL, &t6456_TI, t6456_ITIs, NULL, &EmptyCustomAttributesCache, &t6456_TI, &t6456_0_0_0, &t6456_1_0_0, NULL, &t6456_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContextAttribute>
extern Il2CppType t4941_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33436_GM;
MethodInfo m33436_MI = 
{
	"GetEnumerator", NULL, &t6458_TI, &t4941_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33436_GM};
static MethodInfo* t6458_MIs[] =
{
	&m33436_MI,
	NULL
};
static TypeInfo* t6458_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6458_0_0_0;
extern Il2CppType t6458_1_0_0;
struct t6458;
extern Il2CppGenericClass t6458_GC;
TypeInfo t6458_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6458_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6458_TI, t6458_ITIs, NULL, &EmptyCustomAttributesCache, &t6458_TI, &t6458_0_0_0, &t6458_1_0_0, NULL, &t6458_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4941_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>
extern MethodInfo m33437_MI;
static PropertyInfo t4941____Current_PropertyInfo = 
{
	&t4941_TI, "Current", &m33437_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4941_PIs[] =
{
	&t4941____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2040_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33437_GM;
MethodInfo m33437_MI = 
{
	"get_Current", NULL, &t4941_TI, &t2040_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33437_GM};
static MethodInfo* t4941_MIs[] =
{
	&m33437_MI,
	NULL
};
static TypeInfo* t4941_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4941_0_0_0;
extern Il2CppType t4941_1_0_0;
struct t4941;
extern Il2CppGenericClass t4941_GC;
TypeInfo t4941_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4941_MIs, t4941_PIs, NULL, NULL, NULL, NULL, NULL, &t4941_TI, t4941_ITIs, NULL, &EmptyCustomAttributesCache, &t4941_TI, &t4941_0_0_0, &t4941_1_0_0, NULL, &t4941_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3423.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3423_TI;
#include "t3423MD.h"

extern TypeInfo t2040_TI;
extern MethodInfo m18989_MI;
extern MethodInfo m25600_MI;
struct t20;
#define m25600(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3423_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3423_TI, offsetof(t3423, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3423_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3423_TI, offsetof(t3423, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3423_FIs[] =
{
	&t3423_f0_FieldInfo,
	&t3423_f1_FieldInfo,
	NULL
};
extern MethodInfo m18986_MI;
static PropertyInfo t3423____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3423_TI, "System.Collections.IEnumerator.Current", &m18986_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3423____Current_PropertyInfo = 
{
	&t3423_TI, "Current", &m18989_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3423_PIs[] =
{
	&t3423____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3423____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3423_m18985_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18985_GM;
MethodInfo m18985_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3423_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3423_m18985_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18985_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18986_GM;
MethodInfo m18986_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3423_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18986_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18987_GM;
MethodInfo m18987_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3423_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18987_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18988_GM;
MethodInfo m18988_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3423_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18988_GM};
extern Il2CppType t2040_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18989_GM;
MethodInfo m18989_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3423_TI, &t2040_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18989_GM};
static MethodInfo* t3423_MIs[] =
{
	&m18985_MI,
	&m18986_MI,
	&m18987_MI,
	&m18988_MI,
	&m18989_MI,
	NULL
};
extern MethodInfo m18988_MI;
extern MethodInfo m18987_MI;
static MethodInfo* t3423_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18986_MI,
	&m18988_MI,
	&m18987_MI,
	&m18989_MI,
};
static TypeInfo* t3423_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4941_TI,
};
static Il2CppInterfaceOffsetPair t3423_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4941_TI, 7},
};
extern TypeInfo t2040_TI;
static Il2CppRGCTXData t3423_RGCTXData[3] = 
{
	&m18989_MI/* Method Usage */,
	&t2040_TI/* Class Usage */,
	&m25600_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3423_0_0_0;
extern Il2CppType t3423_1_0_0;
extern Il2CppGenericClass t3423_GC;
TypeInfo t3423_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3423_MIs, t3423_PIs, t3423_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3423_TI, t3423_ITIs, t3423_VT, &EmptyCustomAttributesCache, &t3423_TI, &t3423_0_0_0, &t3423_1_0_0, t3423_IOs, &t3423_GC, NULL, NULL, NULL, t3423_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3423)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6457_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextAttribute>
extern MethodInfo m33438_MI;
extern MethodInfo m33439_MI;
static PropertyInfo t6457____Item_PropertyInfo = 
{
	&t6457_TI, "Item", &m33438_MI, &m33439_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6457_PIs[] =
{
	&t6457____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2040_0_0_0;
static ParameterInfo t6457_m33440_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2040_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33440_GM;
MethodInfo m33440_MI = 
{
	"IndexOf", NULL, &t6457_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6457_m33440_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33440_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2040_0_0_0;
static ParameterInfo t6457_m33441_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2040_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33441_GM;
MethodInfo m33441_MI = 
{
	"Insert", NULL, &t6457_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6457_m33441_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33441_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6457_m33442_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33442_GM;
MethodInfo m33442_MI = 
{
	"RemoveAt", NULL, &t6457_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6457_m33442_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33442_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6457_m33438_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2040_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33438_GM;
MethodInfo m33438_MI = 
{
	"get_Item", NULL, &t6457_TI, &t2040_0_0_0, RuntimeInvoker_t29_t44, t6457_m33438_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33438_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2040_0_0_0;
static ParameterInfo t6457_m33439_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2040_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33439_GM;
MethodInfo m33439_MI = 
{
	"set_Item", NULL, &t6457_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6457_m33439_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33439_GM};
static MethodInfo* t6457_MIs[] =
{
	&m33440_MI,
	&m33441_MI,
	&m33442_MI,
	&m33438_MI,
	&m33439_MI,
	NULL
};
static TypeInfo* t6457_ITIs[] = 
{
	&t603_TI,
	&t6456_TI,
	&t6458_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6457_0_0_0;
extern Il2CppType t6457_1_0_0;
struct t6457;
extern Il2CppGenericClass t6457_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6457_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6457_MIs, t6457_PIs, NULL, NULL, NULL, NULL, NULL, &t6457_TI, t6457_ITIs, NULL, &t1908__CustomAttributeCache, &t6457_TI, &t6457_0_0_0, &t6457_1_0_0, NULL, &t6457_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6459_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.IContextProperty>
extern MethodInfo m33443_MI;
static PropertyInfo t6459____Count_PropertyInfo = 
{
	&t6459_TI, "Count", &m33443_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33444_MI;
static PropertyInfo t6459____IsReadOnly_PropertyInfo = 
{
	&t6459_TI, "IsReadOnly", &m33444_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6459_PIs[] =
{
	&t6459____Count_PropertyInfo,
	&t6459____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33443_GM;
MethodInfo m33443_MI = 
{
	"get_Count", NULL, &t6459_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33443_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33444_GM;
MethodInfo m33444_MI = 
{
	"get_IsReadOnly", NULL, &t6459_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33444_GM};
extern Il2CppType t1433_0_0_0;
extern Il2CppType t1433_0_0_0;
static ParameterInfo t6459_m33445_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1433_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33445_GM;
MethodInfo m33445_MI = 
{
	"Add", NULL, &t6459_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6459_m33445_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33445_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33446_GM;
MethodInfo m33446_MI = 
{
	"Clear", NULL, &t6459_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33446_GM};
extern Il2CppType t1433_0_0_0;
static ParameterInfo t6459_m33447_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1433_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33447_GM;
MethodInfo m33447_MI = 
{
	"Contains", NULL, &t6459_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6459_m33447_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33447_GM};
extern Il2CppType t3678_0_0_0;
extern Il2CppType t3678_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6459_m33448_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3678_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33448_GM;
MethodInfo m33448_MI = 
{
	"CopyTo", NULL, &t6459_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6459_m33448_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33448_GM};
extern Il2CppType t1433_0_0_0;
static ParameterInfo t6459_m33449_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1433_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33449_GM;
MethodInfo m33449_MI = 
{
	"Remove", NULL, &t6459_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6459_m33449_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33449_GM};
static MethodInfo* t6459_MIs[] =
{
	&m33443_MI,
	&m33444_MI,
	&m33445_MI,
	&m33446_MI,
	&m33447_MI,
	&m33448_MI,
	&m33449_MI,
	NULL
};
extern TypeInfo t6461_TI;
static TypeInfo* t6459_ITIs[] = 
{
	&t603_TI,
	&t6461_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6459_0_0_0;
extern Il2CppType t6459_1_0_0;
struct t6459;
extern Il2CppGenericClass t6459_GC;
TypeInfo t6459_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6459_MIs, t6459_PIs, NULL, NULL, NULL, NULL, NULL, &t6459_TI, t6459_ITIs, NULL, &EmptyCustomAttributesCache, &t6459_TI, &t6459_0_0_0, &t6459_1_0_0, NULL, &t6459_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.IContextProperty>
extern Il2CppType t4943_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33450_GM;
MethodInfo m33450_MI = 
{
	"GetEnumerator", NULL, &t6461_TI, &t4943_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33450_GM};
static MethodInfo* t6461_MIs[] =
{
	&m33450_MI,
	NULL
};
static TypeInfo* t6461_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6461_0_0_0;
extern Il2CppType t6461_1_0_0;
struct t6461;
extern Il2CppGenericClass t6461_GC;
TypeInfo t6461_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6461_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6461_TI, t6461_ITIs, NULL, &EmptyCustomAttributesCache, &t6461_TI, &t6461_0_0_0, &t6461_1_0_0, NULL, &t6461_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4943_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>
extern MethodInfo m33451_MI;
static PropertyInfo t4943____Current_PropertyInfo = 
{
	&t4943_TI, "Current", &m33451_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4943_PIs[] =
{
	&t4943____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1433_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33451_GM;
MethodInfo m33451_MI = 
{
	"get_Current", NULL, &t4943_TI, &t1433_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33451_GM};
static MethodInfo* t4943_MIs[] =
{
	&m33451_MI,
	NULL
};
static TypeInfo* t4943_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4943_0_0_0;
extern Il2CppType t4943_1_0_0;
struct t4943;
extern Il2CppGenericClass t4943_GC;
TypeInfo t4943_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4943_MIs, t4943_PIs, NULL, NULL, NULL, NULL, NULL, &t4943_TI, t4943_ITIs, NULL, &EmptyCustomAttributesCache, &t4943_TI, &t4943_0_0_0, &t4943_1_0_0, NULL, &t4943_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3424.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3424_TI;
#include "t3424MD.h"

extern TypeInfo t1433_TI;
extern MethodInfo m18994_MI;
extern MethodInfo m25611_MI;
struct t20;
#define m25611(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextProperty>
extern Il2CppType t20_0_0_1;
FieldInfo t3424_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3424_TI, offsetof(t3424, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3424_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3424_TI, offsetof(t3424, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3424_FIs[] =
{
	&t3424_f0_FieldInfo,
	&t3424_f1_FieldInfo,
	NULL
};
extern MethodInfo m18991_MI;
static PropertyInfo t3424____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3424_TI, "System.Collections.IEnumerator.Current", &m18991_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3424____Current_PropertyInfo = 
{
	&t3424_TI, "Current", &m18994_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3424_PIs[] =
{
	&t3424____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3424____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3424_m18990_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18990_GM;
MethodInfo m18990_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3424_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3424_m18990_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18990_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18991_GM;
MethodInfo m18991_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3424_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18991_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18992_GM;
MethodInfo m18992_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3424_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18992_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18993_GM;
MethodInfo m18993_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3424_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18993_GM};
extern Il2CppType t1433_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18994_GM;
MethodInfo m18994_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3424_TI, &t1433_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18994_GM};
static MethodInfo* t3424_MIs[] =
{
	&m18990_MI,
	&m18991_MI,
	&m18992_MI,
	&m18993_MI,
	&m18994_MI,
	NULL
};
extern MethodInfo m18993_MI;
extern MethodInfo m18992_MI;
static MethodInfo* t3424_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18991_MI,
	&m18993_MI,
	&m18992_MI,
	&m18994_MI,
};
static TypeInfo* t3424_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4943_TI,
};
static Il2CppInterfaceOffsetPair t3424_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4943_TI, 7},
};
extern TypeInfo t1433_TI;
static Il2CppRGCTXData t3424_RGCTXData[3] = 
{
	&m18994_MI/* Method Usage */,
	&t1433_TI/* Class Usage */,
	&m25611_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3424_0_0_0;
extern Il2CppType t3424_1_0_0;
extern Il2CppGenericClass t3424_GC;
TypeInfo t3424_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3424_MIs, t3424_PIs, t3424_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3424_TI, t3424_ITIs, t3424_VT, &EmptyCustomAttributesCache, &t3424_TI, &t3424_0_0_0, &t3424_1_0_0, t3424_IOs, &t3424_GC, NULL, NULL, NULL, t3424_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3424)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6460_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.IContextProperty>
extern MethodInfo m33452_MI;
extern MethodInfo m33453_MI;
static PropertyInfo t6460____Item_PropertyInfo = 
{
	&t6460_TI, "Item", &m33452_MI, &m33453_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6460_PIs[] =
{
	&t6460____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1433_0_0_0;
static ParameterInfo t6460_m33454_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1433_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33454_GM;
MethodInfo m33454_MI = 
{
	"IndexOf", NULL, &t6460_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6460_m33454_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33454_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1433_0_0_0;
static ParameterInfo t6460_m33455_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1433_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33455_GM;
MethodInfo m33455_MI = 
{
	"Insert", NULL, &t6460_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6460_m33455_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33455_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6460_m33456_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33456_GM;
MethodInfo m33456_MI = 
{
	"RemoveAt", NULL, &t6460_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6460_m33456_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33456_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6460_m33452_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1433_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33452_GM;
MethodInfo m33452_MI = 
{
	"get_Item", NULL, &t6460_TI, &t1433_0_0_0, RuntimeInvoker_t29_t44, t6460_m33452_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33452_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1433_0_0_0;
static ParameterInfo t6460_m33453_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1433_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33453_GM;
MethodInfo m33453_MI = 
{
	"set_Item", NULL, &t6460_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6460_m33453_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33453_GM};
static MethodInfo* t6460_MIs[] =
{
	&m33454_MI,
	&m33455_MI,
	&m33456_MI,
	&m33452_MI,
	&m33453_MI,
	NULL
};
static TypeInfo* t6460_ITIs[] = 
{
	&t603_TI,
	&t6459_TI,
	&t6461_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6460_0_0_0;
extern Il2CppType t6460_1_0_0;
struct t6460;
extern Il2CppGenericClass t6460_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6460_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6460_MIs, t6460_PIs, NULL, NULL, NULL, NULL, NULL, &t6460_TI, t6460_ITIs, NULL, &t1908__CustomAttributeCache, &t6460_TI, &t6460_0_0_0, &t6460_1_0_0, NULL, &t6460_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4945_TI;

#include "t1434.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>
extern MethodInfo m33457_MI;
static PropertyInfo t4945____Current_PropertyInfo = 
{
	&t4945_TI, "Current", &m33457_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4945_PIs[] =
{
	&t4945____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1434_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33457_GM;
MethodInfo m33457_MI = 
{
	"get_Current", NULL, &t4945_TI, &t1434_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33457_GM};
static MethodInfo* t4945_MIs[] =
{
	&m33457_MI,
	NULL
};
static TypeInfo* t4945_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4945_0_0_0;
extern Il2CppType t4945_1_0_0;
struct t4945;
extern Il2CppGenericClass t4945_GC;
TypeInfo t4945_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4945_MIs, t4945_PIs, NULL, NULL, NULL, NULL, NULL, &t4945_TI, t4945_ITIs, NULL, &EmptyCustomAttributesCache, &t4945_TI, &t4945_0_0_0, &t4945_1_0_0, NULL, &t4945_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3425.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3425_TI;
#include "t3425MD.h"

extern TypeInfo t1434_TI;
extern MethodInfo m18999_MI;
extern MethodInfo m25622_MI;
struct t20;
#define m25622(__this, p0, method) (t1434 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3425_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3425_TI, offsetof(t3425, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3425_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3425_TI, offsetof(t3425, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3425_FIs[] =
{
	&t3425_f0_FieldInfo,
	&t3425_f1_FieldInfo,
	NULL
};
extern MethodInfo m18996_MI;
static PropertyInfo t3425____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3425_TI, "System.Collections.IEnumerator.Current", &m18996_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3425____Current_PropertyInfo = 
{
	&t3425_TI, "Current", &m18999_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3425_PIs[] =
{
	&t3425____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3425____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3425_m18995_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18995_GM;
MethodInfo m18995_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3425_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3425_m18995_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18995_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18996_GM;
MethodInfo m18996_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3425_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18996_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18997_GM;
MethodInfo m18997_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3425_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18997_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18998_GM;
MethodInfo m18998_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3425_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18998_GM};
extern Il2CppType t1434_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18999_GM;
MethodInfo m18999_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3425_TI, &t1434_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18999_GM};
static MethodInfo* t3425_MIs[] =
{
	&m18995_MI,
	&m18996_MI,
	&m18997_MI,
	&m18998_MI,
	&m18999_MI,
	NULL
};
extern MethodInfo m18998_MI;
extern MethodInfo m18997_MI;
static MethodInfo* t3425_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18996_MI,
	&m18998_MI,
	&m18997_MI,
	&m18999_MI,
};
static TypeInfo* t3425_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4945_TI,
};
static Il2CppInterfaceOffsetPair t3425_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4945_TI, 7},
};
extern TypeInfo t1434_TI;
static Il2CppRGCTXData t3425_RGCTXData[3] = 
{
	&m18999_MI/* Method Usage */,
	&t1434_TI/* Class Usage */,
	&m25622_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3425_0_0_0;
extern Il2CppType t3425_1_0_0;
extern Il2CppGenericClass t3425_GC;
TypeInfo t3425_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3425_MIs, t3425_PIs, t3425_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3425_TI, t3425_ITIs, t3425_VT, &EmptyCustomAttributesCache, &t3425_TI, &t3425_0_0_0, &t3425_1_0_0, t3425_IOs, &t3425_GC, NULL, NULL, NULL, t3425_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3425)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6462_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>
extern MethodInfo m33458_MI;
static PropertyInfo t6462____Count_PropertyInfo = 
{
	&t6462_TI, "Count", &m33458_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33459_MI;
static PropertyInfo t6462____IsReadOnly_PropertyInfo = 
{
	&t6462_TI, "IsReadOnly", &m33459_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6462_PIs[] =
{
	&t6462____Count_PropertyInfo,
	&t6462____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33458_GM;
MethodInfo m33458_MI = 
{
	"get_Count", NULL, &t6462_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33458_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33459_GM;
MethodInfo m33459_MI = 
{
	"get_IsReadOnly", NULL, &t6462_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33459_GM};
extern Il2CppType t1434_0_0_0;
extern Il2CppType t1434_0_0_0;
static ParameterInfo t6462_m33460_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1434_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33460_GM;
MethodInfo m33460_MI = 
{
	"Add", NULL, &t6462_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6462_m33460_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33460_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33461_GM;
MethodInfo m33461_MI = 
{
	"Clear", NULL, &t6462_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33461_GM};
extern Il2CppType t1434_0_0_0;
static ParameterInfo t6462_m33462_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1434_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33462_GM;
MethodInfo m33462_MI = 
{
	"Contains", NULL, &t6462_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6462_m33462_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33462_GM};
extern Il2CppType t3679_0_0_0;
extern Il2CppType t3679_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6462_m33463_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3679_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33463_GM;
MethodInfo m33463_MI = 
{
	"CopyTo", NULL, &t6462_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6462_m33463_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33463_GM};
extern Il2CppType t1434_0_0_0;
static ParameterInfo t6462_m33464_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1434_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33464_GM;
MethodInfo m33464_MI = 
{
	"Remove", NULL, &t6462_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6462_m33464_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33464_GM};
static MethodInfo* t6462_MIs[] =
{
	&m33458_MI,
	&m33459_MI,
	&m33460_MI,
	&m33461_MI,
	&m33462_MI,
	&m33463_MI,
	&m33464_MI,
	NULL
};
extern TypeInfo t6464_TI;
static TypeInfo* t6462_ITIs[] = 
{
	&t603_TI,
	&t6464_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6462_0_0_0;
extern Il2CppType t6462_1_0_0;
struct t6462;
extern Il2CppGenericClass t6462_GC;
TypeInfo t6462_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6462_MIs, t6462_PIs, NULL, NULL, NULL, NULL, NULL, &t6462_TI, t6462_ITIs, NULL, &EmptyCustomAttributesCache, &t6462_TI, &t6462_0_0_0, &t6462_1_0_0, NULL, &t6462_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>
extern Il2CppType t4945_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33465_GM;
MethodInfo m33465_MI = 
{
	"GetEnumerator", NULL, &t6464_TI, &t4945_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33465_GM};
static MethodInfo* t6464_MIs[] =
{
	&m33465_MI,
	NULL
};
static TypeInfo* t6464_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6464_0_0_0;
extern Il2CppType t6464_1_0_0;
struct t6464;
extern Il2CppGenericClass t6464_GC;
TypeInfo t6464_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6464_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6464_TI, t6464_ITIs, NULL, &EmptyCustomAttributesCache, &t6464_TI, &t6464_0_0_0, &t6464_1_0_0, NULL, &t6464_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6463_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Remoting.Contexts.SynchronizationAttribute>
extern MethodInfo m33466_MI;
extern MethodInfo m33467_MI;
static PropertyInfo t6463____Item_PropertyInfo = 
{
	&t6463_TI, "Item", &m33466_MI, &m33467_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6463_PIs[] =
{
	&t6463____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1434_0_0_0;
static ParameterInfo t6463_m33468_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1434_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33468_GM;
MethodInfo m33468_MI = 
{
	"IndexOf", NULL, &t6463_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6463_m33468_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33468_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1434_0_0_0;
static ParameterInfo t6463_m33469_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1434_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33469_GM;
MethodInfo m33469_MI = 
{
	"Insert", NULL, &t6463_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6463_m33469_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33469_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6463_m33470_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33470_GM;
MethodInfo m33470_MI = 
{
	"RemoveAt", NULL, &t6463_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6463_m33470_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33470_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6463_m33466_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1434_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33466_GM;
MethodInfo m33466_MI = 
{
	"get_Item", NULL, &t6463_TI, &t1434_0_0_0, RuntimeInvoker_t29_t44, t6463_m33466_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33466_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1434_0_0_0;
static ParameterInfo t6463_m33467_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1434_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33467_GM;
MethodInfo m33467_MI = 
{
	"set_Item", NULL, &t6463_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6463_m33467_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33467_GM};
static MethodInfo* t6463_MIs[] =
{
	&m33468_MI,
	&m33469_MI,
	&m33470_MI,
	&m33466_MI,
	&m33467_MI,
	NULL
};
static TypeInfo* t6463_ITIs[] = 
{
	&t603_TI,
	&t6462_TI,
	&t6464_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6463_0_0_0;
extern Il2CppType t6463_1_0_0;
struct t6463;
extern Il2CppGenericClass t6463_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6463_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6463_MIs, t6463_PIs, NULL, NULL, NULL, NULL, NULL, &t6463_TI, t6463_ITIs, NULL, &t1908__CustomAttributeCache, &t6463_TI, &t6463_0_0_0, &t6463_1_0_0, NULL, &t6463_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
