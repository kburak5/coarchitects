﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3408;
struct t29;
struct t20;
#include "t1153.h"

 void m18910 (t3408 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18911 (t3408 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18912 (t3408 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18913 (t3408 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18914 (t3408 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
