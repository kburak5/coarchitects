﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t2230.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t2230_TI;
#include "t2230MD.h"

#include "t29.h"
#include "t44.h"
#include "t914.h"
#include "t21.h"
#include "t2229.h"
#include "mscorlib_ArrayTypes.h"
#include "t40.h"
extern TypeInfo t29_TI;
extern TypeInfo t914_TI;
extern TypeInfo t2229_TI;
#include "t914MD.h"
extern MethodInfo m11079_MI;
extern MethodInfo m3974_MI;

#include "t20.h"

extern MethodInfo m11075_MI;
 void m11075_gshared (t2230 * __this, t2229 * p0, MethodInfo* method)
{
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		int32_t L_0 = (p0->f3);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m11076_MI;
 t29 * m11076_gshared (t2230 * __this, MethodInfo* method)
{
	{
		t29 * L_0 = (( t29 * (*) (t2230 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		t29 * L_1 = L_0;
		return ((t29 *)L_1);
	}
}
extern MethodInfo m11077_MI;
 void m11077_gshared (t2230 * __this, MethodInfo* method)
{
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m11078_MI;
 bool m11078_gshared (t2230 * __this, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = (__this->f2);
		t2229 * L_1 = (__this->f0);
		int32_t L_2 = (L_1->f3);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_001c;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_3, &m3974_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_001c:
	{
		int32_t L_4 = (__this->f1);
		if ((((uint32_t)L_4) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_003a;
		}
	}
	{
		t2229 * L_5 = (__this->f0);
		int32_t L_6 = (L_5->f2);
		__this->f1 = L_6;
	}

IL_003a:
	{
		int32_t L_7 = (__this->f1);
		if ((((int32_t)L_7) == ((int32_t)(-1))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_8 = (__this->f1);
		int32_t L_9 = ((int32_t)(L_8-1));
		V_0 = L_9;
		__this->f1 = L_9;
		G_B7_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0060;
	}

IL_005f:
	{
		G_B7_0 = 0;
	}

IL_0060:
	{
		return G_B7_0;
	}
}
 t29 * m11079_gshared (t2230 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_1, &m3974_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0012:
	{
		t2229 * L_2 = (__this->f0);
		t316* L_3 = (L_2->f1);
		int32_t L_4 = (__this->f1);
		int32_t L_5 = L_4;
		return (*(t29 **)(t29 **)SZArrayLdElema(L_3, L_5));
	}
}
// Metadata Definition System.Collections.Generic.Stack`1/Enumerator<System.Object>
extern Il2CppType t2229_0_0_1;
FieldInfo t2230_f0_FieldInfo = 
{
	"parent", &t2229_0_0_1, &t2230_TI, offsetof(t2230, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2230_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2230_TI, offsetof(t2230, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2230_f2_FieldInfo = 
{
	"_version", &t44_0_0_1, &t2230_TI, offsetof(t2230, f2) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2230_FIs[] =
{
	&t2230_f0_FieldInfo,
	&t2230_f1_FieldInfo,
	&t2230_f2_FieldInfo,
	NULL
};
static PropertyInfo t2230____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2230_TI, "System.Collections.IEnumerator.Current", &m11076_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2230____Current_PropertyInfo = 
{
	&t2230_TI, "Current", &m11079_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2230_PIs[] =
{
	&t2230____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2230____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2229_0_0_0;
extern Il2CppType t2229_0_0_0;
static ParameterInfo t2230_m11075_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &t2229_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11075_GM;
MethodInfo m11075_MI = 
{
	".ctor", (methodPointerType)&m11075_gshared, &t2230_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2230_m11075_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11075_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11076_GM;
MethodInfo m11076_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m11076_gshared, &t2230_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11076_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11077_GM;
MethodInfo m11077_MI = 
{
	"Dispose", (methodPointerType)&m11077_gshared, &t2230_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11077_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11078_GM;
MethodInfo m11078_MI = 
{
	"MoveNext", (methodPointerType)&m11078_gshared, &t2230_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11078_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11079_GM;
MethodInfo m11079_MI = 
{
	"get_Current", (methodPointerType)&m11079_gshared, &t2230_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11079_GM};
static MethodInfo* t2230_MIs[] =
{
	&m11075_MI,
	&m11076_MI,
	&m11077_MI,
	&m11078_MI,
	&m11079_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t2230_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11076_MI,
	&m11078_MI,
	&m11077_MI,
	&m11079_MI,
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
extern TypeInfo t346_TI;
static TypeInfo* t2230_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t346_TI,
};
static Il2CppInterfaceOffsetPair t2230_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t346_TI, 7},
};
extern TypeInfo t29_TI;
static Il2CppRGCTXData t2230_RGCTXData[2] = 
{
	&m11079_MI/* Method Usage */,
	&t29_TI/* Class Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t2230_0_0_0;
extern Il2CppType t2230_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2230_GC;
extern TypeInfo t717_TI;
TypeInfo t2230_TI = 
{
	&g_System_dll_Image, NULL, "Enumerator", "", t2230_MIs, t2230_PIs, t2230_FIs, NULL, &t110_TI, NULL, &t717_TI, &t2230_TI, t2230_ITIs, t2230_VT, &EmptyCustomAttributesCache, &t2230_TI, &t2230_0_0_0, &t2230_1_0_0, t2230_IOs, &t2230_GC, NULL, NULL, NULL, t2230_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2230)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 3, 0, 0, 8, 3, 3};
#include "t2227.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2227_TI;
#include "t2227MD.h"

#include "t105.h"
#include "t305.h"
#include "t2233.h"
extern TypeInfo t20_TI;
extern TypeInfo t21_TI;
extern TypeInfo t44_TI;
extern TypeInfo t305_TI;
extern TypeInfo t1622_TI;
extern TypeInfo t2233_TI;
extern TypeInfo t105_TI;
#include "t29MD.h"
#include "t20MD.h"
#include "t305MD.h"
#include "t2233MD.h"
extern MethodInfo m1331_MI;
extern MethodInfo m4199_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m8852_MI;
extern MethodInfo m11090_MI;
extern MethodInfo m20054_MI;
extern MethodInfo m11111_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m20054(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)


 t2233  m11090 (t2227 * __this, MethodInfo* method){
	{
		t2233  L_0 = {0};
		m11111(&L_0, __this, &m11111_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
extern Il2CppType t44_0_0_32849;
FieldInfo t2227_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t2227_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2231_0_0_1;
FieldInfo t2227_f1_FieldInfo = 
{
	"_array", &t2231_0_0_1, &t2227_TI, offsetof(t2227, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2227_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t2227_TI, offsetof(t2227, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2227_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t2227_TI, offsetof(t2227, f3), &EmptyCustomAttributesCache};
static FieldInfo* t2227_FIs[] =
{
	&t2227_f0_FieldInfo,
	&t2227_f1_FieldInfo,
	&t2227_f2_FieldInfo,
	&t2227_f3_FieldInfo,
	NULL
};
static const int32_t t2227_f0_DefaultValueData = 16;
extern Il2CppType t44_0_0_0;
static Il2CppFieldDefaultValueEntry t2227_f0_DefaultValue = 
{
	&t2227_f0_FieldInfo, { (char*)&t2227_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t2227_FDVs[] = 
{
	&t2227_f0_DefaultValue,
	NULL
};
extern MethodInfo m11081_MI;
static PropertyInfo t2227____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2227_TI, "System.Collections.ICollection.IsSynchronized", &m11081_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11082_MI;
static PropertyInfo t2227____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2227_TI, "System.Collections.ICollection.SyncRoot", &m11082_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11089_MI;
static PropertyInfo t2227____Count_PropertyInfo = 
{
	&t2227_TI, "Count", &m11089_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2227_PIs[] =
{
	&t2227____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2227____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2227____Count_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11080_GM;
MethodInfo m11080_MI = 
{
	".ctor", (methodPointerType)&m11064_gshared, &t2227_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11080_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11081_GM;
MethodInfo m11081_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m11065_gshared, &t2227_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11081_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11082_GM;
MethodInfo m11082_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m11066_gshared, &t2227_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11082_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2227_m11083_ParameterInfos[] = 
{
	{"dest", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"idx", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11083_GM;
MethodInfo m11083_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m11067_gshared, &t2227_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2227_m11083_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11083_GM};
extern Il2CppType t2232_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11084_GM;
MethodInfo m11084_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m11068_gshared, &t2227_TI, &t2232_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11084_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11085_GM;
MethodInfo m11085_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m11069_gshared, &t2227_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11085_GM};
extern Il2CppType t105_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11086_GM;
MethodInfo m11086_MI = 
{
	"Peek", (methodPointerType)&m11070_gshared, &t2227_TI, &t105_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11086_GM};
extern Il2CppType t105_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11087_GM;
MethodInfo m11087_MI = 
{
	"Pop", (methodPointerType)&m11071_gshared, &t2227_TI, &t105_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11087_GM};
extern Il2CppType t105_0_0_0;
extern Il2CppType t105_0_0_0;
static ParameterInfo t2227_m11088_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &t105_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11088_GM;
MethodInfo m11088_MI = 
{
	"Push", (methodPointerType)&m11072_gshared, &t2227_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2227_m11088_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11088_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11089_GM;
MethodInfo m11089_MI = 
{
	"get_Count", (methodPointerType)&m11073_gshared, &t2227_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11089_GM};
extern Il2CppType t2233_0_0_0;
extern void* RuntimeInvoker_t2233 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11090_GM;
MethodInfo m11090_MI = 
{
	"GetEnumerator", (methodPointerType)&m11090, &t2227_TI, &t2233_0_0_0, RuntimeInvoker_t2233, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11090_GM};
static MethodInfo* t2227_MIs[] =
{
	&m11080_MI,
	&m11081_MI,
	&m11082_MI,
	&m11083_MI,
	&m11084_MI,
	&m11085_MI,
	&m11086_MI,
	&m11087_MI,
	&m11088_MI,
	&m11089_MI,
	&m11090_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m11083_MI;
extern MethodInfo m11085_MI;
extern MethodInfo m11084_MI;
static MethodInfo* t2227_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11089_MI,
	&m11081_MI,
	&m11082_MI,
	&m11083_MI,
	&m11085_MI,
	&m11084_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t603_TI;
extern TypeInfo t5168_TI;
static TypeInfo* t2227_ITIs[] = 
{
	&t674_TI,
	&t603_TI,
	&t5168_TI,
};
static Il2CppInterfaceOffsetPair t2227_IOs[] = 
{
	{ &t674_TI, 4},
	{ &t603_TI, 8},
	{ &t5168_TI, 9},
};
extern TypeInfo t2233_TI;
static Il2CppRGCTXData t2227_RGCTXData[4] = 
{
	&m11090_MI/* Method Usage */,
	&t2233_TI/* Class Usage */,
	&m20054_MI/* Method Usage */,
	&m11111_MI/* Method Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t2227_0_0_0;
extern Il2CppType t2227_1_0_0;
struct t2227;
extern Il2CppGenericClass t2227_GC;
extern CustomAttributesCache t717__CustomAttributeCache;
TypeInfo t2227_TI = 
{
	&g_System_dll_Image, NULL, "Stack`1", "System.Collections.Generic", t2227_MIs, t2227_PIs, t2227_FIs, NULL, &t29_TI, NULL, NULL, &t2227_TI, t2227_ITIs, t2227_VT, &t717__CustomAttributeCache, &t2227_TI, &t2227_0_0_0, &t2227_1_0_0, t2227_IOs, &t2227_GC, NULL, t2227_FDVs, NULL, t2227_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2227), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 11, 3, 4, 0, 0, 10, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
extern Il2CppType t2232_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26860_GM;
MethodInfo m26860_MI = 
{
	"GetEnumerator", NULL, &t5168_TI, &t2232_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26860_GM};
static MethodInfo* t5168_MIs[] =
{
	&m26860_MI,
	NULL
};
static TypeInfo* t5168_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5168_0_0_0;
extern Il2CppType t5168_1_0_0;
struct t5168;
extern Il2CppGenericClass t5168_GC;
TypeInfo t5168_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5168_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5168_TI, t5168_ITIs, NULL, &EmptyCustomAttributesCache, &t5168_TI, &t5168_0_0_0, &t5168_1_0_0, NULL, &t5168_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2232_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
extern MethodInfo m26861_MI;
static PropertyInfo t2232____Current_PropertyInfo = 
{
	&t2232_TI, "Current", &m26861_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2232_PIs[] =
{
	&t2232____Current_PropertyInfo,
	NULL
};
extern Il2CppType t105_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26861_GM;
MethodInfo m26861_MI = 
{
	"get_Current", NULL, &t2232_TI, &t105_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26861_GM};
static MethodInfo* t2232_MIs[] =
{
	&m26861_MI,
	NULL
};
static TypeInfo* t2232_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2232_0_0_0;
extern Il2CppType t2232_1_0_0;
struct t2232;
extern Il2CppGenericClass t2232_GC;
TypeInfo t2232_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2232_MIs, t2232_PIs, NULL, NULL, NULL, NULL, NULL, &t2232_TI, t2232_ITIs, NULL, &EmptyCustomAttributesCache, &t2232_TI, &t2232_0_0_0, &t2232_1_0_0, NULL, &t2232_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2234.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2234_TI;
#include "t2234MD.h"

#include "t7.h"
extern MethodInfo m11095_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m20010_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m20010(__this, p0, method) (t105 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
extern Il2CppType t20_0_0_1;
FieldInfo t2234_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2234_TI, offsetof(t2234, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2234_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2234_TI, offsetof(t2234, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2234_FIs[] =
{
	&t2234_f0_FieldInfo,
	&t2234_f1_FieldInfo,
	NULL
};
extern MethodInfo m11092_MI;
static PropertyInfo t2234____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2234_TI, "System.Collections.IEnumerator.Current", &m11092_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2234____Current_PropertyInfo = 
{
	&t2234_TI, "Current", &m11095_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2234_PIs[] =
{
	&t2234____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2234____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2234_m11091_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11091_GM;
MethodInfo m11091_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2234_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2234_m11091_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11091_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11092_GM;
MethodInfo m11092_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2234_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11092_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11093_GM;
MethodInfo m11093_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2234_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11093_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11094_GM;
MethodInfo m11094_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2234_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11094_GM};
extern Il2CppType t105_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11095_GM;
MethodInfo m11095_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2234_TI, &t105_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11095_GM};
static MethodInfo* t2234_MIs[] =
{
	&m11091_MI,
	&m11092_MI,
	&m11093_MI,
	&m11094_MI,
	&m11095_MI,
	NULL
};
extern MethodInfo m11094_MI;
extern MethodInfo m11093_MI;
static MethodInfo* t2234_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11092_MI,
	&m11094_MI,
	&m11093_MI,
	&m11095_MI,
};
static TypeInfo* t2234_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2232_TI,
};
static Il2CppInterfaceOffsetPair t2234_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2232_TI, 7},
};
extern TypeInfo t105_TI;
static Il2CppRGCTXData t2234_RGCTXData[3] = 
{
	&m11095_MI/* Method Usage */,
	&t105_TI/* Class Usage */,
	&m20010_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2234_0_0_0;
extern Il2CppType t2234_1_0_0;
extern Il2CppGenericClass t2234_GC;
TypeInfo t2234_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2234_MIs, t2234_PIs, t2234_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2234_TI, t2234_ITIs, t2234_VT, &EmptyCustomAttributesCache, &t2234_TI, &t2234_0_0_0, &t2234_1_0_0, t2234_IOs, &t2234_GC, NULL, NULL, NULL, t2234_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2234)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5166_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
extern MethodInfo m26862_MI;
static PropertyInfo t5166____Count_PropertyInfo = 
{
	&t5166_TI, "Count", &m26862_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26863_MI;
static PropertyInfo t5166____IsReadOnly_PropertyInfo = 
{
	&t5166_TI, "IsReadOnly", &m26863_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5166_PIs[] =
{
	&t5166____Count_PropertyInfo,
	&t5166____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26862_GM;
MethodInfo m26862_MI = 
{
	"get_Count", NULL, &t5166_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26862_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26863_GM;
MethodInfo m26863_MI = 
{
	"get_IsReadOnly", NULL, &t5166_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26863_GM};
extern Il2CppType t105_0_0_0;
static ParameterInfo t5166_m26864_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t105_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26864_GM;
MethodInfo m26864_MI = 
{
	"Add", NULL, &t5166_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5166_m26864_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26864_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26865_GM;
MethodInfo m26865_MI = 
{
	"Clear", NULL, &t5166_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26865_GM};
extern Il2CppType t105_0_0_0;
static ParameterInfo t5166_m26866_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t105_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26866_GM;
MethodInfo m26866_MI = 
{
	"Contains", NULL, &t5166_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5166_m26866_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26866_GM};
extern Il2CppType t2231_0_0_0;
extern Il2CppType t2231_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5166_m26867_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2231_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26867_GM;
MethodInfo m26867_MI = 
{
	"CopyTo", NULL, &t5166_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5166_m26867_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26867_GM};
extern Il2CppType t105_0_0_0;
static ParameterInfo t5166_m26868_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t105_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26868_GM;
MethodInfo m26868_MI = 
{
	"Remove", NULL, &t5166_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5166_m26868_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26868_GM};
static MethodInfo* t5166_MIs[] =
{
	&m26862_MI,
	&m26863_MI,
	&m26864_MI,
	&m26865_MI,
	&m26866_MI,
	&m26867_MI,
	&m26868_MI,
	NULL
};
static TypeInfo* t5166_ITIs[] = 
{
	&t603_TI,
	&t5168_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5166_0_0_0;
extern Il2CppType t5166_1_0_0;
struct t5166;
extern Il2CppGenericClass t5166_GC;
TypeInfo t5166_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5166_MIs, t5166_PIs, NULL, NULL, NULL, NULL, NULL, &t5166_TI, t5166_ITIs, NULL, &EmptyCustomAttributesCache, &t5166_TI, &t5166_0_0_0, &t5166_1_0_0, NULL, &t5166_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5167_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
extern MethodInfo m26869_MI;
extern MethodInfo m26870_MI;
static PropertyInfo t5167____Item_PropertyInfo = 
{
	&t5167_TI, "Item", &m26869_MI, &m26870_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5167_PIs[] =
{
	&t5167____Item_PropertyInfo,
	NULL
};
extern Il2CppType t105_0_0_0;
static ParameterInfo t5167_m26871_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t105_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26871_GM;
MethodInfo m26871_MI = 
{
	"IndexOf", NULL, &t5167_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5167_m26871_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26871_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t105_0_0_0;
static ParameterInfo t5167_m26872_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t105_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26872_GM;
MethodInfo m26872_MI = 
{
	"Insert", NULL, &t5167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5167_m26872_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26872_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5167_m26873_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26873_GM;
MethodInfo m26873_MI = 
{
	"RemoveAt", NULL, &t5167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5167_m26873_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26873_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5167_m26869_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t105_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26869_GM;
MethodInfo m26869_MI = 
{
	"get_Item", NULL, &t5167_TI, &t105_0_0_0, RuntimeInvoker_t29_t44, t5167_m26869_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26869_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t105_0_0_0;
static ParameterInfo t5167_m26870_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t105_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26870_GM;
MethodInfo m26870_MI = 
{
	"set_Item", NULL, &t5167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5167_m26870_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26870_GM};
static MethodInfo* t5167_MIs[] =
{
	&m26871_MI,
	&m26872_MI,
	&m26873_MI,
	&m26869_MI,
	&m26870_MI,
	NULL
};
static TypeInfo* t5167_ITIs[] = 
{
	&t603_TI,
	&t5166_TI,
	&t5168_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5167_0_0_0;
extern Il2CppType t5167_1_0_0;
struct t5167;
extern Il2CppGenericClass t5167_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5167_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5167_MIs, t5167_PIs, NULL, NULL, NULL, NULL, NULL, &t5167_TI, t5167_ITIs, NULL, &t1908__CustomAttributeCache, &t5167_TI, &t5167_0_0_0, &t5167_1_0_0, NULL, &t5167_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5169_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>
extern MethodInfo m26874_MI;
static PropertyInfo t5169____Count_PropertyInfo = 
{
	&t5169_TI, "Count", &m26874_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26875_MI;
static PropertyInfo t5169____IsReadOnly_PropertyInfo = 
{
	&t5169_TI, "IsReadOnly", &m26875_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5169_PIs[] =
{
	&t5169____Count_PropertyInfo,
	&t5169____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26874_GM;
MethodInfo m26874_MI = 
{
	"get_Count", NULL, &t5169_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26874_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26875_GM;
MethodInfo m26875_MI = 
{
	"get_IsReadOnly", NULL, &t5169_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26875_GM};
extern Il2CppType t603_0_0_0;
extern Il2CppType t603_0_0_0;
static ParameterInfo t5169_m26876_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t603_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26876_GM;
MethodInfo m26876_MI = 
{
	"Add", NULL, &t5169_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5169_m26876_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26876_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26877_GM;
MethodInfo m26877_MI = 
{
	"Clear", NULL, &t5169_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26877_GM};
extern Il2CppType t603_0_0_0;
static ParameterInfo t5169_m26878_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t603_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26878_GM;
MethodInfo m26878_MI = 
{
	"Contains", NULL, &t5169_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5169_m26878_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26878_GM};
extern Il2CppType t3535_0_0_0;
extern Il2CppType t3535_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5169_m26879_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3535_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26879_GM;
MethodInfo m26879_MI = 
{
	"CopyTo", NULL, &t5169_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5169_m26879_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26879_GM};
extern Il2CppType t603_0_0_0;
static ParameterInfo t5169_m26880_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t603_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26880_GM;
MethodInfo m26880_MI = 
{
	"Remove", NULL, &t5169_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5169_m26880_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26880_GM};
static MethodInfo* t5169_MIs[] =
{
	&m26874_MI,
	&m26875_MI,
	&m26876_MI,
	&m26877_MI,
	&m26878_MI,
	&m26879_MI,
	&m26880_MI,
	NULL
};
extern TypeInfo t5171_TI;
static TypeInfo* t5169_ITIs[] = 
{
	&t603_TI,
	&t5171_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5169_0_0_0;
extern Il2CppType t5169_1_0_0;
struct t5169;
extern Il2CppGenericClass t5169_GC;
TypeInfo t5169_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5169_MIs, t5169_PIs, NULL, NULL, NULL, NULL, NULL, &t5169_TI, t5169_ITIs, NULL, &EmptyCustomAttributesCache, &t5169_TI, &t5169_0_0_0, &t5169_1_0_0, NULL, &t5169_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>
extern Il2CppType t4024_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26881_GM;
MethodInfo m26881_MI = 
{
	"GetEnumerator", NULL, &t5171_TI, &t4024_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26881_GM};
static MethodInfo* t5171_MIs[] =
{
	&m26881_MI,
	NULL
};
static TypeInfo* t5171_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5171_0_0_0;
extern Il2CppType t5171_1_0_0;
struct t5171;
extern Il2CppGenericClass t5171_GC;
TypeInfo t5171_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5171_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5171_TI, t5171_ITIs, NULL, &EmptyCustomAttributesCache, &t5171_TI, &t5171_0_0_0, &t5171_1_0_0, NULL, &t5171_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4024_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.IEnumerable>
extern MethodInfo m26882_MI;
static PropertyInfo t4024____Current_PropertyInfo = 
{
	&t4024_TI, "Current", &m26882_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4024_PIs[] =
{
	&t4024____Current_PropertyInfo,
	NULL
};
extern Il2CppType t603_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26882_GM;
MethodInfo m26882_MI = 
{
	"get_Current", NULL, &t4024_TI, &t603_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26882_GM};
static MethodInfo* t4024_MIs[] =
{
	&m26882_MI,
	NULL
};
static TypeInfo* t4024_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4024_0_0_0;
extern Il2CppType t4024_1_0_0;
struct t4024;
extern Il2CppGenericClass t4024_GC;
TypeInfo t4024_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4024_MIs, t4024_PIs, NULL, NULL, NULL, NULL, NULL, &t4024_TI, t4024_ITIs, NULL, &EmptyCustomAttributesCache, &t4024_TI, &t4024_0_0_0, &t4024_1_0_0, NULL, &t4024_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2235.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2235_TI;
#include "t2235MD.h"

extern MethodInfo m11100_MI;
extern MethodInfo m20021_MI;
struct t20;
#define m20021(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.IEnumerable>
extern Il2CppType t20_0_0_1;
FieldInfo t2235_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2235_TI, offsetof(t2235, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2235_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2235_TI, offsetof(t2235, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2235_FIs[] =
{
	&t2235_f0_FieldInfo,
	&t2235_f1_FieldInfo,
	NULL
};
extern MethodInfo m11097_MI;
static PropertyInfo t2235____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2235_TI, "System.Collections.IEnumerator.Current", &m11097_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2235____Current_PropertyInfo = 
{
	&t2235_TI, "Current", &m11100_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2235_PIs[] =
{
	&t2235____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2235____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2235_m11096_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11096_GM;
MethodInfo m11096_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2235_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2235_m11096_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11096_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11097_GM;
MethodInfo m11097_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2235_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11097_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11098_GM;
MethodInfo m11098_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2235_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11098_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11099_GM;
MethodInfo m11099_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2235_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11099_GM};
extern Il2CppType t603_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11100_GM;
MethodInfo m11100_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2235_TI, &t603_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11100_GM};
static MethodInfo* t2235_MIs[] =
{
	&m11096_MI,
	&m11097_MI,
	&m11098_MI,
	&m11099_MI,
	&m11100_MI,
	NULL
};
extern MethodInfo m11099_MI;
extern MethodInfo m11098_MI;
static MethodInfo* t2235_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11097_MI,
	&m11099_MI,
	&m11098_MI,
	&m11100_MI,
};
static TypeInfo* t2235_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4024_TI,
};
static Il2CppInterfaceOffsetPair t2235_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4024_TI, 7},
};
extern TypeInfo t603_TI;
static Il2CppRGCTXData t2235_RGCTXData[3] = 
{
	&m11100_MI/* Method Usage */,
	&t603_TI/* Class Usage */,
	&m20021_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2235_0_0_0;
extern Il2CppType t2235_1_0_0;
extern Il2CppGenericClass t2235_GC;
TypeInfo t2235_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2235_MIs, t2235_PIs, t2235_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2235_TI, t2235_ITIs, t2235_VT, &EmptyCustomAttributesCache, &t2235_TI, &t2235_0_0_0, &t2235_1_0_0, t2235_IOs, &t2235_GC, NULL, NULL, NULL, t2235_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2235)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5170_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.IEnumerable>
extern MethodInfo m26883_MI;
extern MethodInfo m26884_MI;
static PropertyInfo t5170____Item_PropertyInfo = 
{
	&t5170_TI, "Item", &m26883_MI, &m26884_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5170_PIs[] =
{
	&t5170____Item_PropertyInfo,
	NULL
};
extern Il2CppType t603_0_0_0;
static ParameterInfo t5170_m26885_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t603_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26885_GM;
MethodInfo m26885_MI = 
{
	"IndexOf", NULL, &t5170_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5170_m26885_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26885_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t603_0_0_0;
static ParameterInfo t5170_m26886_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t603_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26886_GM;
MethodInfo m26886_MI = 
{
	"Insert", NULL, &t5170_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5170_m26886_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26886_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5170_m26887_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26887_GM;
MethodInfo m26887_MI = 
{
	"RemoveAt", NULL, &t5170_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5170_m26887_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26887_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5170_m26883_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t603_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26883_GM;
MethodInfo m26883_MI = 
{
	"get_Item", NULL, &t5170_TI, &t603_0_0_0, RuntimeInvoker_t29_t44, t5170_m26883_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26883_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t603_0_0_0;
static ParameterInfo t5170_m26884_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t603_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26884_GM;
MethodInfo m26884_MI = 
{
	"set_Item", NULL, &t5170_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5170_m26884_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26884_GM};
static MethodInfo* t5170_MIs[] =
{
	&m26885_MI,
	&m26886_MI,
	&m26887_MI,
	&m26883_MI,
	&m26884_MI,
	NULL
};
static TypeInfo* t5170_ITIs[] = 
{
	&t603_TI,
	&t5169_TI,
	&t5171_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5170_0_0_0;
extern Il2CppType t5170_1_0_0;
struct t5170;
extern Il2CppGenericClass t5170_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5170_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5170_MIs, t5170_PIs, NULL, NULL, NULL, NULL, NULL, &t5170_TI, t5170_ITIs, NULL, &t1908__CustomAttributeCache, &t5170_TI, &t5170_0_0_0, &t5170_1_0_0, NULL, &t5170_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5172_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.ICollection>
extern MethodInfo m26888_MI;
static PropertyInfo t5172____Count_PropertyInfo = 
{
	&t5172_TI, "Count", &m26888_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26889_MI;
static PropertyInfo t5172____IsReadOnly_PropertyInfo = 
{
	&t5172_TI, "IsReadOnly", &m26889_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5172_PIs[] =
{
	&t5172____Count_PropertyInfo,
	&t5172____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26888_GM;
MethodInfo m26888_MI = 
{
	"get_Count", NULL, &t5172_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26888_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26889_GM;
MethodInfo m26889_MI = 
{
	"get_IsReadOnly", NULL, &t5172_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26889_GM};
extern Il2CppType t674_0_0_0;
extern Il2CppType t674_0_0_0;
static ParameterInfo t5172_m26890_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t674_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26890_GM;
MethodInfo m26890_MI = 
{
	"Add", NULL, &t5172_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5172_m26890_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26890_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26891_GM;
MethodInfo m26891_MI = 
{
	"Clear", NULL, &t5172_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26891_GM};
extern Il2CppType t674_0_0_0;
static ParameterInfo t5172_m26892_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t674_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26892_GM;
MethodInfo m26892_MI = 
{
	"Contains", NULL, &t5172_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5172_m26892_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26892_GM};
extern Il2CppType t3536_0_0_0;
extern Il2CppType t3536_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5172_m26893_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3536_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26893_GM;
MethodInfo m26893_MI = 
{
	"CopyTo", NULL, &t5172_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5172_m26893_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26893_GM};
extern Il2CppType t674_0_0_0;
static ParameterInfo t5172_m26894_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t674_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26894_GM;
MethodInfo m26894_MI = 
{
	"Remove", NULL, &t5172_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5172_m26894_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26894_GM};
static MethodInfo* t5172_MIs[] =
{
	&m26888_MI,
	&m26889_MI,
	&m26890_MI,
	&m26891_MI,
	&m26892_MI,
	&m26893_MI,
	&m26894_MI,
	NULL
};
extern TypeInfo t5174_TI;
static TypeInfo* t5172_ITIs[] = 
{
	&t603_TI,
	&t5174_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5172_0_0_0;
extern Il2CppType t5172_1_0_0;
struct t5172;
extern Il2CppGenericClass t5172_GC;
TypeInfo t5172_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5172_MIs, t5172_PIs, NULL, NULL, NULL, NULL, NULL, &t5172_TI, t5172_ITIs, NULL, &EmptyCustomAttributesCache, &t5172_TI, &t5172_0_0_0, &t5172_1_0_0, NULL, &t5172_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.ICollection>
extern Il2CppType t4026_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26895_GM;
MethodInfo m26895_MI = 
{
	"GetEnumerator", NULL, &t5174_TI, &t4026_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26895_GM};
static MethodInfo* t5174_MIs[] =
{
	&m26895_MI,
	NULL
};
static TypeInfo* t5174_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5174_0_0_0;
extern Il2CppType t5174_1_0_0;
struct t5174;
extern Il2CppGenericClass t5174_GC;
TypeInfo t5174_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5174_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5174_TI, t5174_ITIs, NULL, &EmptyCustomAttributesCache, &t5174_TI, &t5174_0_0_0, &t5174_1_0_0, NULL, &t5174_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4026_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.ICollection>
extern MethodInfo m26896_MI;
static PropertyInfo t4026____Current_PropertyInfo = 
{
	&t4026_TI, "Current", &m26896_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4026_PIs[] =
{
	&t4026____Current_PropertyInfo,
	NULL
};
extern Il2CppType t674_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26896_GM;
MethodInfo m26896_MI = 
{
	"get_Current", NULL, &t4026_TI, &t674_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26896_GM};
static MethodInfo* t4026_MIs[] =
{
	&m26896_MI,
	NULL
};
static TypeInfo* t4026_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4026_0_0_0;
extern Il2CppType t4026_1_0_0;
struct t4026;
extern Il2CppGenericClass t4026_GC;
TypeInfo t4026_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4026_MIs, t4026_PIs, NULL, NULL, NULL, NULL, NULL, &t4026_TI, t4026_ITIs, NULL, &EmptyCustomAttributesCache, &t4026_TI, &t4026_0_0_0, &t4026_1_0_0, NULL, &t4026_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2236.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2236_TI;
#include "t2236MD.h"

extern MethodInfo m11105_MI;
extern MethodInfo m20032_MI;
struct t20;
#define m20032(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.ICollection>
extern Il2CppType t20_0_0_1;
FieldInfo t2236_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2236_TI, offsetof(t2236, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2236_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2236_TI, offsetof(t2236, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2236_FIs[] =
{
	&t2236_f0_FieldInfo,
	&t2236_f1_FieldInfo,
	NULL
};
extern MethodInfo m11102_MI;
static PropertyInfo t2236____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2236_TI, "System.Collections.IEnumerator.Current", &m11102_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2236____Current_PropertyInfo = 
{
	&t2236_TI, "Current", &m11105_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2236_PIs[] =
{
	&t2236____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2236____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2236_m11101_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11101_GM;
MethodInfo m11101_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2236_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2236_m11101_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11101_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11102_GM;
MethodInfo m11102_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2236_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11102_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11103_GM;
MethodInfo m11103_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2236_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11103_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11104_GM;
MethodInfo m11104_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2236_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11104_GM};
extern Il2CppType t674_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11105_GM;
MethodInfo m11105_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2236_TI, &t674_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11105_GM};
static MethodInfo* t2236_MIs[] =
{
	&m11101_MI,
	&m11102_MI,
	&m11103_MI,
	&m11104_MI,
	&m11105_MI,
	NULL
};
extern MethodInfo m11104_MI;
extern MethodInfo m11103_MI;
static MethodInfo* t2236_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11102_MI,
	&m11104_MI,
	&m11103_MI,
	&m11105_MI,
};
static TypeInfo* t2236_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4026_TI,
};
static Il2CppInterfaceOffsetPair t2236_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4026_TI, 7},
};
extern TypeInfo t674_TI;
static Il2CppRGCTXData t2236_RGCTXData[3] = 
{
	&m11105_MI/* Method Usage */,
	&t674_TI/* Class Usage */,
	&m20032_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2236_0_0_0;
extern Il2CppType t2236_1_0_0;
extern Il2CppGenericClass t2236_GC;
TypeInfo t2236_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2236_MIs, t2236_PIs, t2236_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2236_TI, t2236_ITIs, t2236_VT, &EmptyCustomAttributesCache, &t2236_TI, &t2236_0_0_0, &t2236_1_0_0, t2236_IOs, &t2236_GC, NULL, NULL, NULL, t2236_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2236)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5173_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.ICollection>
extern MethodInfo m26897_MI;
extern MethodInfo m26898_MI;
static PropertyInfo t5173____Item_PropertyInfo = 
{
	&t5173_TI, "Item", &m26897_MI, &m26898_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5173_PIs[] =
{
	&t5173____Item_PropertyInfo,
	NULL
};
extern Il2CppType t674_0_0_0;
static ParameterInfo t5173_m26899_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t674_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26899_GM;
MethodInfo m26899_MI = 
{
	"IndexOf", NULL, &t5173_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5173_m26899_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26899_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t674_0_0_0;
static ParameterInfo t5173_m26900_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t674_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26900_GM;
MethodInfo m26900_MI = 
{
	"Insert", NULL, &t5173_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5173_m26900_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26900_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5173_m26901_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26901_GM;
MethodInfo m26901_MI = 
{
	"RemoveAt", NULL, &t5173_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5173_m26901_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26901_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5173_m26897_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t674_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26897_GM;
MethodInfo m26897_MI = 
{
	"get_Item", NULL, &t5173_TI, &t674_0_0_0, RuntimeInvoker_t29_t44, t5173_m26897_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26897_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t674_0_0_0;
static ParameterInfo t5173_m26898_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t674_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26898_GM;
MethodInfo m26898_MI = 
{
	"set_Item", NULL, &t5173_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5173_m26898_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26898_GM};
static MethodInfo* t5173_MIs[] =
{
	&m26899_MI,
	&m26900_MI,
	&m26901_MI,
	&m26897_MI,
	&m26898_MI,
	NULL
};
static TypeInfo* t5173_ITIs[] = 
{
	&t603_TI,
	&t5172_TI,
	&t5174_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5173_0_0_0;
extern Il2CppType t5173_1_0_0;
struct t5173;
extern Il2CppGenericClass t5173_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5173_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5173_MIs, t5173_PIs, NULL, NULL, NULL, NULL, NULL, &t5173_TI, t5173_ITIs, NULL, &t1908__CustomAttributeCache, &t5173_TI, &t5173_0_0_0, &t5173_1_0_0, NULL, &t5173_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5175_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.IList>
extern MethodInfo m26902_MI;
static PropertyInfo t5175____Count_PropertyInfo = 
{
	&t5175_TI, "Count", &m26902_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26903_MI;
static PropertyInfo t5175____IsReadOnly_PropertyInfo = 
{
	&t5175_TI, "IsReadOnly", &m26903_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5175_PIs[] =
{
	&t5175____Count_PropertyInfo,
	&t5175____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26902_GM;
MethodInfo m26902_MI = 
{
	"get_Count", NULL, &t5175_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26902_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26903_GM;
MethodInfo m26903_MI = 
{
	"get_IsReadOnly", NULL, &t5175_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26903_GM};
extern Il2CppType t868_0_0_0;
extern Il2CppType t868_0_0_0;
static ParameterInfo t5175_m26904_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t868_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26904_GM;
MethodInfo m26904_MI = 
{
	"Add", NULL, &t5175_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5175_m26904_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26904_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26905_GM;
MethodInfo m26905_MI = 
{
	"Clear", NULL, &t5175_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26905_GM};
extern Il2CppType t868_0_0_0;
static ParameterInfo t5175_m26906_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t868_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26906_GM;
MethodInfo m26906_MI = 
{
	"Contains", NULL, &t5175_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5175_m26906_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26906_GM};
extern Il2CppType t3537_0_0_0;
extern Il2CppType t3537_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5175_m26907_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3537_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26907_GM;
MethodInfo m26907_MI = 
{
	"CopyTo", NULL, &t5175_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5175_m26907_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26907_GM};
extern Il2CppType t868_0_0_0;
static ParameterInfo t5175_m26908_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t868_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26908_GM;
MethodInfo m26908_MI = 
{
	"Remove", NULL, &t5175_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5175_m26908_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26908_GM};
static MethodInfo* t5175_MIs[] =
{
	&m26902_MI,
	&m26903_MI,
	&m26904_MI,
	&m26905_MI,
	&m26906_MI,
	&m26907_MI,
	&m26908_MI,
	NULL
};
extern TypeInfo t5177_TI;
static TypeInfo* t5175_ITIs[] = 
{
	&t603_TI,
	&t5177_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5175_0_0_0;
extern Il2CppType t5175_1_0_0;
struct t5175;
extern Il2CppGenericClass t5175_GC;
TypeInfo t5175_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5175_MIs, t5175_PIs, NULL, NULL, NULL, NULL, NULL, &t5175_TI, t5175_ITIs, NULL, &EmptyCustomAttributesCache, &t5175_TI, &t5175_0_0_0, &t5175_1_0_0, NULL, &t5175_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.IList>
extern Il2CppType t4028_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26909_GM;
MethodInfo m26909_MI = 
{
	"GetEnumerator", NULL, &t5177_TI, &t4028_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26909_GM};
static MethodInfo* t5177_MIs[] =
{
	&m26909_MI,
	NULL
};
static TypeInfo* t5177_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5177_0_0_0;
extern Il2CppType t5177_1_0_0;
struct t5177;
extern Il2CppGenericClass t5177_GC;
TypeInfo t5177_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5177_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5177_TI, t5177_ITIs, NULL, &EmptyCustomAttributesCache, &t5177_TI, &t5177_0_0_0, &t5177_1_0_0, NULL, &t5177_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4028_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.IList>
extern MethodInfo m26910_MI;
static PropertyInfo t4028____Current_PropertyInfo = 
{
	&t4028_TI, "Current", &m26910_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4028_PIs[] =
{
	&t4028____Current_PropertyInfo,
	NULL
};
extern Il2CppType t868_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26910_GM;
MethodInfo m26910_MI = 
{
	"get_Current", NULL, &t4028_TI, &t868_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26910_GM};
static MethodInfo* t4028_MIs[] =
{
	&m26910_MI,
	NULL
};
static TypeInfo* t4028_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4028_0_0_0;
extern Il2CppType t4028_1_0_0;
struct t4028;
extern Il2CppGenericClass t4028_GC;
TypeInfo t4028_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4028_MIs, t4028_PIs, NULL, NULL, NULL, NULL, NULL, &t4028_TI, t4028_ITIs, NULL, &EmptyCustomAttributesCache, &t4028_TI, &t4028_0_0_0, &t4028_1_0_0, NULL, &t4028_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2237.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2237_TI;
#include "t2237MD.h"

extern TypeInfo t868_TI;
extern MethodInfo m11110_MI;
extern MethodInfo m20043_MI;
struct t20;
#define m20043(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.IList>
extern Il2CppType t20_0_0_1;
FieldInfo t2237_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2237_TI, offsetof(t2237, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2237_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2237_TI, offsetof(t2237, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2237_FIs[] =
{
	&t2237_f0_FieldInfo,
	&t2237_f1_FieldInfo,
	NULL
};
extern MethodInfo m11107_MI;
static PropertyInfo t2237____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2237_TI, "System.Collections.IEnumerator.Current", &m11107_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2237____Current_PropertyInfo = 
{
	&t2237_TI, "Current", &m11110_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2237_PIs[] =
{
	&t2237____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2237____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2237_m11106_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11106_GM;
MethodInfo m11106_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2237_m11106_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11106_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11107_GM;
MethodInfo m11107_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2237_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11107_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11108_GM;
MethodInfo m11108_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2237_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11108_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11109_GM;
MethodInfo m11109_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2237_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11109_GM};
extern Il2CppType t868_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11110_GM;
MethodInfo m11110_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2237_TI, &t868_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11110_GM};
static MethodInfo* t2237_MIs[] =
{
	&m11106_MI,
	&m11107_MI,
	&m11108_MI,
	&m11109_MI,
	&m11110_MI,
	NULL
};
extern MethodInfo m11109_MI;
extern MethodInfo m11108_MI;
static MethodInfo* t2237_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11107_MI,
	&m11109_MI,
	&m11108_MI,
	&m11110_MI,
};
static TypeInfo* t2237_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4028_TI,
};
static Il2CppInterfaceOffsetPair t2237_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4028_TI, 7},
};
extern TypeInfo t868_TI;
static Il2CppRGCTXData t2237_RGCTXData[3] = 
{
	&m11110_MI/* Method Usage */,
	&t868_TI/* Class Usage */,
	&m20043_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2237_0_0_0;
extern Il2CppType t2237_1_0_0;
extern Il2CppGenericClass t2237_GC;
TypeInfo t2237_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2237_MIs, t2237_PIs, t2237_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2237_TI, t2237_ITIs, t2237_VT, &EmptyCustomAttributesCache, &t2237_TI, &t2237_0_0_0, &t2237_1_0_0, t2237_IOs, &t2237_GC, NULL, NULL, NULL, t2237_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2237)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5176_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.IList>
extern MethodInfo m26911_MI;
extern MethodInfo m26912_MI;
static PropertyInfo t5176____Item_PropertyInfo = 
{
	&t5176_TI, "Item", &m26911_MI, &m26912_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5176_PIs[] =
{
	&t5176____Item_PropertyInfo,
	NULL
};
extern Il2CppType t868_0_0_0;
static ParameterInfo t5176_m26913_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t868_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26913_GM;
MethodInfo m26913_MI = 
{
	"IndexOf", NULL, &t5176_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5176_m26913_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26913_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t868_0_0_0;
static ParameterInfo t5176_m26914_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t868_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26914_GM;
MethodInfo m26914_MI = 
{
	"Insert", NULL, &t5176_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5176_m26914_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26914_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5176_m26915_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26915_GM;
MethodInfo m26915_MI = 
{
	"RemoveAt", NULL, &t5176_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5176_m26915_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26915_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5176_m26911_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t868_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26911_GM;
MethodInfo m26911_MI = 
{
	"get_Item", NULL, &t5176_TI, &t868_0_0_0, RuntimeInvoker_t29_t44, t5176_m26911_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26911_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t868_0_0_0;
static ParameterInfo t5176_m26912_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t868_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26912_GM;
MethodInfo m26912_MI = 
{
	"set_Item", NULL, &t5176_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5176_m26912_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26912_GM};
static MethodInfo* t5176_MIs[] =
{
	&m26913_MI,
	&m26914_MI,
	&m26915_MI,
	&m26911_MI,
	&m26912_MI,
	NULL
};
static TypeInfo* t5176_ITIs[] = 
{
	&t603_TI,
	&t5175_TI,
	&t5177_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5176_0_0_0;
extern Il2CppType t5176_1_0_0;
struct t5176;
extern Il2CppGenericClass t5176_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5176_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5176_MIs, t5176_PIs, NULL, NULL, NULL, NULL, NULL, &t5176_TI, t5176_ITIs, NULL, &t1908__CustomAttributeCache, &t5176_TI, &t5176_0_0_0, &t5176_1_0_0, NULL, &t5176_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m11115_MI;


// Metadata Definition System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
extern Il2CppType t2227_0_0_1;
FieldInfo t2233_f0_FieldInfo = 
{
	"parent", &t2227_0_0_1, &t2233_TI, offsetof(t2233, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2233_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2233_TI, offsetof(t2233, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2233_f2_FieldInfo = 
{
	"_version", &t44_0_0_1, &t2233_TI, offsetof(t2233, f2) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2233_FIs[] =
{
	&t2233_f0_FieldInfo,
	&t2233_f1_FieldInfo,
	&t2233_f2_FieldInfo,
	NULL
};
extern MethodInfo m11112_MI;
static PropertyInfo t2233____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2233_TI, "System.Collections.IEnumerator.Current", &m11112_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2233____Current_PropertyInfo = 
{
	&t2233_TI, "Current", &m11115_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2233_PIs[] =
{
	&t2233____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2233____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2227_0_0_0;
static ParameterInfo t2233_m11111_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &t2227_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11111_GM;
MethodInfo m11111_MI = 
{
	".ctor", (methodPointerType)&m11075_gshared, &t2233_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2233_m11111_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11111_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11112_GM;
MethodInfo m11112_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m11076_gshared, &t2233_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11112_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11113_GM;
MethodInfo m11113_MI = 
{
	"Dispose", (methodPointerType)&m11077_gshared, &t2233_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11113_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11114_GM;
MethodInfo m11114_MI = 
{
	"MoveNext", (methodPointerType)&m11078_gshared, &t2233_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11114_GM};
extern Il2CppType t105_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11115_GM;
MethodInfo m11115_MI = 
{
	"get_Current", (methodPointerType)&m11079_gshared, &t2233_TI, &t105_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11115_GM};
static MethodInfo* t2233_MIs[] =
{
	&m11111_MI,
	&m11112_MI,
	&m11113_MI,
	&m11114_MI,
	&m11115_MI,
	NULL
};
extern MethodInfo m11114_MI;
extern MethodInfo m11113_MI;
static MethodInfo* t2233_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11112_MI,
	&m11114_MI,
	&m11113_MI,
	&m11115_MI,
};
static TypeInfo* t2233_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2232_TI,
};
static Il2CppInterfaceOffsetPair t2233_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2232_TI, 7},
};
extern TypeInfo t105_TI;
static Il2CppRGCTXData t2233_RGCTXData[2] = 
{
	&m11115_MI/* Method Usage */,
	&t105_TI/* Class Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t2233_0_0_0;
extern Il2CppType t2233_1_0_0;
extern Il2CppGenericClass t2233_GC;
TypeInfo t2233_TI = 
{
	&g_System_dll_Image, NULL, "Enumerator", "", t2233_MIs, t2233_PIs, t2233_FIs, NULL, &t110_TI, NULL, &t717_TI, &t2233_TI, t2233_ITIs, t2233_VT, &EmptyCustomAttributesCache, &t2233_TI, &t2233_0_0_0, &t2233_1_0_0, t2233_IOs, &t2233_GC, NULL, NULL, NULL, t2233_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2233)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 3, 0, 0, 8, 3, 3};
#include "t88.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t88_TI;
#include "t88MD.h"

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t88_m1357_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1357_GM;
MethodInfo m1357_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t88_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t88_m1357_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1357_GM};
extern Il2CppType t105_0_0_0;
static ParameterInfo t88_m11116_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t105_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11116_GM;
MethodInfo m11116_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t88_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t88_m11116_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11116_GM};
extern Il2CppType t105_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t88_m11117_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t105_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11117_GM;
MethodInfo m11117_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t88_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t88_m11117_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11117_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t88_m11118_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11118_GM;
MethodInfo m11118_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t88_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t88_m11118_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11118_GM};
static MethodInfo* t88_MIs[] =
{
	&m1357_MI,
	&m11116_MI,
	&m11117_MI,
	&m11118_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m11116_MI;
extern MethodInfo m11117_MI;
extern MethodInfo m11118_MI;
static MethodInfo* t88_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11116_MI,
	&m11117_MI,
	&m11118_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t88_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t88_0_0_0;
extern Il2CppType t88_1_0_0;
extern TypeInfo t195_TI;
struct t88;
extern Il2CppGenericClass t88_GC;
TypeInfo t88_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t88_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t88_TI, NULL, t88_VT, &EmptyCustomAttributesCache, &t88_TI, &t88_0_0_0, &t88_1_0_0, t88_IOs, &t88_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t88), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t268.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t268_TI;
#include "t268MD.h"

#include "t28.h"
#include "UnityEngine_ArrayTypes.h"
#include "t2244.h"
#include "t2242.h"
#include "t267.h"
#include "t338.h"
#include "t2249.h"
#include "t2243.h"
extern TypeInfo t28_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2238_TI;
extern TypeInfo t2244_TI;
extern TypeInfo t40_TI;
extern TypeInfo t2240_TI;
extern TypeInfo t2241_TI;
extern TypeInfo t2239_TI;
extern TypeInfo t2242_TI;
extern TypeInfo t338_TI;
extern TypeInfo t267_TI;
extern TypeInfo t2249_TI;
#include "t915MD.h"
#include "t602MD.h"
#include "t2242MD.h"
#include "t338MD.h"
#include "t267MD.h"
#include "t2244MD.h"
#include "t2249MD.h"
extern MethodInfo m1556_MI;
extern MethodInfo m11163_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m20061_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m11151_MI;
extern MethodInfo m11148_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m11137_MI;
extern MethodInfo m11143_MI;
extern MethodInfo m11149_MI;
extern MethodInfo m11152_MI;
extern MethodInfo m11154_MI;
extern MethodInfo m11138_MI;
extern MethodInfo m11161_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m11162_MI;
extern MethodInfo m26308_MI;
extern MethodInfo m26313_MI;
extern MethodInfo m26315_MI;
extern MethodInfo m26316_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m11153_MI;
extern MethodInfo m11139_MI;
extern MethodInfo m11140_MI;
extern MethodInfo m11170_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m20063_MI;
extern MethodInfo m11146_MI;
extern MethodInfo m11147_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m11244_MI;
extern MethodInfo m11164_MI;
extern MethodInfo m11150_MI;
extern MethodInfo m11155_MI;
extern MethodInfo m11250_MI;
extern MethodInfo m20065_MI;
extern MethodInfo m20073_MI;
extern MethodInfo m5951_MI;
struct t20;
#define m20061(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2247.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m20063(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m20065(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m20073(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2244  m11148 (t268 * __this, MethodInfo* method){
	{
		t2244  L_0 = {0};
		m11164(&L_0, __this, &m11164_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.Component>
extern Il2CppType t44_0_0_32849;
FieldInfo t268_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t268_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2238_0_0_1;
FieldInfo t268_f1_FieldInfo = 
{
	"_items", &t2238_0_0_1, &t268_TI, offsetof(t268, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t268_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t268_TI, offsetof(t268, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t268_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t268_TI, offsetof(t268, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2238_0_0_49;
FieldInfo t268_f4_FieldInfo = 
{
	"EmptyArray", &t2238_0_0_49, &t268_TI, offsetof(t268_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t268_FIs[] =
{
	&t268_f0_FieldInfo,
	&t268_f1_FieldInfo,
	&t268_f2_FieldInfo,
	&t268_f3_FieldInfo,
	&t268_f4_FieldInfo,
	NULL
};
static const int32_t t268_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t268_f0_DefaultValue = 
{
	&t268_f0_FieldInfo, { (char*)&t268_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t268_FDVs[] = 
{
	&t268_f0_DefaultValue,
	NULL
};
extern MethodInfo m11130_MI;
static PropertyInfo t268____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t268_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11130_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11131_MI;
static PropertyInfo t268____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t268_TI, "System.Collections.ICollection.IsSynchronized", &m11131_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11132_MI;
static PropertyInfo t268____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t268_TI, "System.Collections.ICollection.SyncRoot", &m11132_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11133_MI;
static PropertyInfo t268____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t268_TI, "System.Collections.IList.IsFixedSize", &m11133_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11134_MI;
static PropertyInfo t268____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t268_TI, "System.Collections.IList.IsReadOnly", &m11134_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11135_MI;
extern MethodInfo m11136_MI;
static PropertyInfo t268____System_Collections_IList_Item_PropertyInfo = 
{
	&t268_TI, "System.Collections.IList.Item", &m11135_MI, &m11136_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t268____Capacity_PropertyInfo = 
{
	&t268_TI, "Capacity", &m11161_MI, &m11162_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1558_MI;
static PropertyInfo t268____Count_PropertyInfo = 
{
	&t268_TI, "Count", &m1558_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t268____Item_PropertyInfo = 
{
	&t268_TI, "Item", &m1556_MI, &m11163_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t268_PIs[] =
{
	&t268____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t268____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t268____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t268____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t268____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t268____System_Collections_IList_Item_PropertyInfo,
	&t268____Capacity_PropertyInfo,
	&t268____Count_PropertyInfo,
	&t268____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11119_GM;
MethodInfo m11119_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11119_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t268_m11120_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11120_GM;
MethodInfo m11120_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t268_m11120_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11120_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11121_GM;
MethodInfo m11121_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11121_GM};
extern Il2CppType t2239_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11122_GM;
MethodInfo m11122_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t268_TI, &t2239_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11122_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t268_m11123_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11123_GM;
MethodInfo m11123_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t268_m11123_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11123_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11124_GM;
MethodInfo m11124_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t268_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11124_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t268_m11125_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11125_GM;
MethodInfo m11125_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t268_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t268_m11125_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11125_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t268_m11126_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11126_GM;
MethodInfo m11126_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t268_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t268_m11126_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11126_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t268_m11127_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11127_GM;
MethodInfo m11127_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t268_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t268_m11127_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11127_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t268_m11128_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11128_GM;
MethodInfo m11128_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t268_m11128_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11128_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t268_m11129_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11129_GM;
MethodInfo m11129_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t268_m11129_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11129_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11130_GM;
MethodInfo m11130_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t268_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11130_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11131_GM;
MethodInfo m11131_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t268_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11131_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11132_GM;
MethodInfo m11132_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t268_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11132_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11133_GM;
MethodInfo m11133_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t268_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11133_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11134_GM;
MethodInfo m11134_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t268_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11134_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t268_m11135_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11135_GM;
MethodInfo m11135_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t268_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t268_m11135_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11135_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t268_m11136_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11136_GM;
MethodInfo m11136_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t268_m11136_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11136_GM};
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t268_m11137_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11137_GM;
MethodInfo m11137_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t268_m11137_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11137_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t268_m11138_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11138_GM;
MethodInfo m11138_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t268_m11138_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11138_GM};
extern Il2CppType t2240_0_0_0;
extern Il2CppType t2240_0_0_0;
static ParameterInfo t268_m11139_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2240_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11139_GM;
MethodInfo m11139_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t268_m11139_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11139_GM};
extern Il2CppType t2241_0_0_0;
extern Il2CppType t2241_0_0_0;
static ParameterInfo t268_m11140_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2241_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11140_GM;
MethodInfo m11140_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t268_m11140_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11140_GM};
extern Il2CppType t2241_0_0_0;
static ParameterInfo t268_m11141_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2241_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11141_GM;
MethodInfo m11141_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t268_m11141_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11141_GM};
extern Il2CppType t2242_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11142_GM;
MethodInfo m11142_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t268_TI, &t2242_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11142_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2035_GM;
MethodInfo m2035_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2035_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t268_m11143_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11143_GM;
MethodInfo m11143_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t268_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t268_m11143_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11143_GM};
extern Il2CppType t2238_0_0_0;
extern Il2CppType t2238_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t268_m11144_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2238_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11144_GM;
MethodInfo m11144_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t268_m11144_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11144_GM};
extern Il2CppType t267_0_0_0;
extern Il2CppType t267_0_0_0;
static ParameterInfo t268_m11145_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t267_0_0_0},
};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11145_GM;
MethodInfo m11145_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t268_TI, &t28_0_0_0, RuntimeInvoker_t29_t29, t268_m11145_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11145_GM};
extern Il2CppType t267_0_0_0;
static ParameterInfo t268_m11146_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t267_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11146_GM;
MethodInfo m11146_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t268_m11146_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11146_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t267_0_0_0;
static ParameterInfo t268_m11147_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t267_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11147_GM;
MethodInfo m11147_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t268_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t268_m11147_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11147_GM};
extern Il2CppType t2244_0_0_0;
extern void* RuntimeInvoker_t2244 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11148_GM;
MethodInfo m11148_MI = 
{
	"GetEnumerator", (methodPointerType)&m11148, &t268_TI, &t2244_0_0_0, RuntimeInvoker_t2244, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11148_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t268_m11149_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11149_GM;
MethodInfo m11149_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t268_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t268_m11149_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11149_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t268_m11150_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11150_GM;
MethodInfo m11150_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t268_m11150_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11150_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t268_m11151_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11151_GM;
MethodInfo m11151_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t268_m11151_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11151_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t268_m11152_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11152_GM;
MethodInfo m11152_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t268_m11152_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11152_GM};
extern Il2CppType t2241_0_0_0;
static ParameterInfo t268_m11153_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2241_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11153_GM;
MethodInfo m11153_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t268_m11153_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11153_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t268_m11154_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11154_GM;
MethodInfo m11154_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t268_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t268_m11154_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11154_GM};
extern Il2CppType t267_0_0_0;
static ParameterInfo t268_m2001_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t267_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2001_GM;
MethodInfo m2001_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t268_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t268_m2001_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2001_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t268_m11155_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11155_GM;
MethodInfo m11155_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t268_m11155_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11155_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11156_GM;
MethodInfo m11156_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11156_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11157_GM;
MethodInfo m11157_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11157_GM};
extern Il2CppType t2243_0_0_0;
extern Il2CppType t2243_0_0_0;
static ParameterInfo t268_m11158_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2243_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11158_GM;
MethodInfo m11158_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t268_m11158_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11158_GM};
extern Il2CppType t2238_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11159_GM;
MethodInfo m11159_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t268_TI, &t2238_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11159_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11160_GM;
MethodInfo m11160_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11160_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11161_GM;
MethodInfo m11161_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t268_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11161_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t268_m11162_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11162_GM;
MethodInfo m11162_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t268_m11162_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11162_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1558_GM;
MethodInfo m1558_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t268_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1558_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t268_m1556_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1556_GM;
MethodInfo m1556_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t268_TI, &t28_0_0_0, RuntimeInvoker_t29_t44, t268_m1556_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1556_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t268_m11163_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11163_GM;
MethodInfo m11163_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t268_m11163_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11163_GM};
static MethodInfo* t268_MIs[] =
{
	&m11119_MI,
	&m11120_MI,
	&m11121_MI,
	&m11122_MI,
	&m11123_MI,
	&m11124_MI,
	&m11125_MI,
	&m11126_MI,
	&m11127_MI,
	&m11128_MI,
	&m11129_MI,
	&m11130_MI,
	&m11131_MI,
	&m11132_MI,
	&m11133_MI,
	&m11134_MI,
	&m11135_MI,
	&m11136_MI,
	&m11137_MI,
	&m11138_MI,
	&m11139_MI,
	&m11140_MI,
	&m11141_MI,
	&m11142_MI,
	&m2035_MI,
	&m11143_MI,
	&m11144_MI,
	&m11145_MI,
	&m11146_MI,
	&m11147_MI,
	&m11148_MI,
	&m11149_MI,
	&m11150_MI,
	&m11151_MI,
	&m11152_MI,
	&m11153_MI,
	&m11154_MI,
	&m2001_MI,
	&m11155_MI,
	&m11156_MI,
	&m11157_MI,
	&m11158_MI,
	&m11159_MI,
	&m11160_MI,
	&m11161_MI,
	&m11162_MI,
	&m1558_MI,
	&m1556_MI,
	&m11163_MI,
	NULL
};
extern MethodInfo m11124_MI;
extern MethodInfo m11123_MI;
extern MethodInfo m11125_MI;
extern MethodInfo m2035_MI;
extern MethodInfo m11126_MI;
extern MethodInfo m11127_MI;
extern MethodInfo m11128_MI;
extern MethodInfo m11129_MI;
extern MethodInfo m11144_MI;
extern MethodInfo m11122_MI;
static MethodInfo* t268_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11124_MI,
	&m1558_MI,
	&m11131_MI,
	&m11132_MI,
	&m11123_MI,
	&m11133_MI,
	&m11134_MI,
	&m11135_MI,
	&m11136_MI,
	&m11125_MI,
	&m2035_MI,
	&m11126_MI,
	&m11127_MI,
	&m11128_MI,
	&m11129_MI,
	&m11155_MI,
	&m1558_MI,
	&m11130_MI,
	&m11137_MI,
	&m2035_MI,
	&m11143_MI,
	&m11144_MI,
	&m11154_MI,
	&m11122_MI,
	&m11149_MI,
	&m11152_MI,
	&m11155_MI,
	&m1556_MI,
	&m11163_MI,
};
extern TypeInfo t2245_TI;
static TypeInfo* t268_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2240_TI,
	&t2241_TI,
	&t2245_TI,
};
static Il2CppInterfaceOffsetPair t268_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2240_TI, 20},
	{ &t2241_TI, 27},
	{ &t2245_TI, 28},
};
extern TypeInfo t268_TI;
extern TypeInfo t2238_TI;
extern TypeInfo t2244_TI;
extern TypeInfo t28_TI;
extern TypeInfo t2240_TI;
extern TypeInfo t2242_TI;
static Il2CppRGCTXData t268_RGCTXData[37] = 
{
	&t268_TI/* Static Usage */,
	&t2238_TI/* Array Usage */,
	&m11148_MI/* Method Usage */,
	&t2244_TI/* Class Usage */,
	&t28_TI/* Class Usage */,
	&m11137_MI/* Method Usage */,
	&m11143_MI/* Method Usage */,
	&m11149_MI/* Method Usage */,
	&m11151_MI/* Method Usage */,
	&m11152_MI/* Method Usage */,
	&m11154_MI/* Method Usage */,
	&m1556_MI/* Method Usage */,
	&m11163_MI/* Method Usage */,
	&m11138_MI/* Method Usage */,
	&m11161_MI/* Method Usage */,
	&m11162_MI/* Method Usage */,
	&m26308_MI/* Method Usage */,
	&m26313_MI/* Method Usage */,
	&m26315_MI/* Method Usage */,
	&m26316_MI/* Method Usage */,
	&m11153_MI/* Method Usage */,
	&t2240_TI/* Class Usage */,
	&m11139_MI/* Method Usage */,
	&m11140_MI/* Method Usage */,
	&t2242_TI/* Class Usage */,
	&m11170_MI/* Method Usage */,
	&m20063_MI/* Method Usage */,
	&m11146_MI/* Method Usage */,
	&m11147_MI/* Method Usage */,
	&m11244_MI/* Method Usage */,
	&m11164_MI/* Method Usage */,
	&m11150_MI/* Method Usage */,
	&m11155_MI/* Method Usage */,
	&m11250_MI/* Method Usage */,
	&m20065_MI/* Method Usage */,
	&m20073_MI/* Method Usage */,
	&m20061_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t268_0_0_0;
extern Il2CppType t268_1_0_0;
struct t268;
extern Il2CppGenericClass t268_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t268_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t268_MIs, t268_PIs, t268_FIs, NULL, &t29_TI, NULL, NULL, &t268_TI, t268_ITIs, t268_VT, &t1261__CustomAttributeCache, &t268_TI, &t268_0_0_0, &t268_1_0_0, t268_IOs, &t268_GC, NULL, t268_FDVs, NULL, t268_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t268), 0, -1, sizeof(t268_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif

#include "t42.h"
#include "t1101.h"
extern TypeInfo t42_TI;
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t42MD.h"
#include "t1101MD.h"
extern MethodInfo m11167_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>
extern Il2CppType t268_0_0_1;
FieldInfo t2244_f0_FieldInfo = 
{
	"l", &t268_0_0_1, &t2244_TI, offsetof(t2244, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2244_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2244_TI, offsetof(t2244, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2244_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2244_TI, offsetof(t2244, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t28_0_0_1;
FieldInfo t2244_f3_FieldInfo = 
{
	"current", &t28_0_0_1, &t2244_TI, offsetof(t2244, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2244_FIs[] =
{
	&t2244_f0_FieldInfo,
	&t2244_f1_FieldInfo,
	&t2244_f2_FieldInfo,
	&t2244_f3_FieldInfo,
	NULL
};
extern MethodInfo m11165_MI;
static PropertyInfo t2244____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2244_TI, "System.Collections.IEnumerator.Current", &m11165_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11169_MI;
static PropertyInfo t2244____Current_PropertyInfo = 
{
	&t2244_TI, "Current", &m11169_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2244_PIs[] =
{
	&t2244____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2244____Current_PropertyInfo,
	NULL
};
extern Il2CppType t268_0_0_0;
static ParameterInfo t2244_m11164_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11164_GM;
MethodInfo m11164_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2244_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2244_m11164_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11164_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11165_GM;
MethodInfo m11165_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2244_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11165_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11166_GM;
MethodInfo m11166_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2244_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11166_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11167_GM;
MethodInfo m11167_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2244_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11167_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11168_GM;
MethodInfo m11168_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2244_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11168_GM};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11169_GM;
MethodInfo m11169_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2244_TI, &t28_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11169_GM};
static MethodInfo* t2244_MIs[] =
{
	&m11164_MI,
	&m11165_MI,
	&m11166_MI,
	&m11167_MI,
	&m11168_MI,
	&m11169_MI,
	NULL
};
extern MethodInfo m11168_MI;
extern MethodInfo m11166_MI;
static MethodInfo* t2244_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11165_MI,
	&m11168_MI,
	&m11166_MI,
	&m11169_MI,
};
static TypeInfo* t2244_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2239_TI,
};
static Il2CppInterfaceOffsetPair t2244_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2239_TI, 7},
};
extern TypeInfo t28_TI;
extern TypeInfo t2244_TI;
static Il2CppRGCTXData t2244_RGCTXData[3] = 
{
	&m11167_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
	&t2244_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2244_0_0_0;
extern Il2CppType t2244_1_0_0;
extern Il2CppGenericClass t2244_GC;
extern TypeInfo t1261_TI;
TypeInfo t2244_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2244_MIs, t2244_PIs, t2244_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2244_TI, t2244_ITIs, t2244_VT, &EmptyCustomAttributesCache, &t2244_TI, &t2244_0_0_0, &t2244_1_0_0, t2244_IOs, &t2244_GC, NULL, NULL, NULL, t2244_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2244)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t2246MD.h"
extern MethodInfo m11199_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m26317_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m11231_MI;
extern MethodInfo m26312_MI;
extern MethodInfo m26319_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Component>
extern Il2CppType t2245_0_0_1;
FieldInfo t2242_f0_FieldInfo = 
{
	"list", &t2245_0_0_1, &t2242_TI, offsetof(t2242, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2242_FIs[] =
{
	&t2242_f0_FieldInfo,
	NULL
};
extern MethodInfo m11176_MI;
extern MethodInfo m11177_MI;
static PropertyInfo t2242____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2242_TI, "System.Collections.Generic.IList<T>.Item", &m11176_MI, &m11177_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11178_MI;
static PropertyInfo t2242____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2242_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11178_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11188_MI;
static PropertyInfo t2242____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2242_TI, "System.Collections.ICollection.IsSynchronized", &m11188_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11189_MI;
static PropertyInfo t2242____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2242_TI, "System.Collections.ICollection.SyncRoot", &m11189_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11190_MI;
static PropertyInfo t2242____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2242_TI, "System.Collections.IList.IsFixedSize", &m11190_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11191_MI;
static PropertyInfo t2242____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2242_TI, "System.Collections.IList.IsReadOnly", &m11191_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11192_MI;
extern MethodInfo m11193_MI;
static PropertyInfo t2242____System_Collections_IList_Item_PropertyInfo = 
{
	&t2242_TI, "System.Collections.IList.Item", &m11192_MI, &m11193_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11198_MI;
static PropertyInfo t2242____Count_PropertyInfo = 
{
	&t2242_TI, "Count", &m11198_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2242____Item_PropertyInfo = 
{
	&t2242_TI, "Item", &m11199_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2242_PIs[] =
{
	&t2242____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2242____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2242____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2242____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2242____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2242____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2242____System_Collections_IList_Item_PropertyInfo,
	&t2242____Count_PropertyInfo,
	&t2242____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2245_0_0_0;
extern Il2CppType t2245_0_0_0;
static ParameterInfo t2242_m11170_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2245_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11170_GM;
MethodInfo m11170_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2242_m11170_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11170_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2242_m11171_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11171_GM;
MethodInfo m11171_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2242_m11171_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11171_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11172_GM;
MethodInfo m11172_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11172_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2242_m11173_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11173_GM;
MethodInfo m11173_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2242_m11173_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11173_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2242_m11174_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11174_GM;
MethodInfo m11174_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2242_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2242_m11174_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11174_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2242_m11175_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11175_GM;
MethodInfo m11175_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2242_m11175_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11175_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2242_m11176_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11176_GM;
MethodInfo m11176_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2242_TI, &t28_0_0_0, RuntimeInvoker_t29_t44, t2242_m11176_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11176_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2242_m11177_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11177_GM;
MethodInfo m11177_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2242_m11177_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11177_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11178_GM;
MethodInfo m11178_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2242_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11178_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2242_m11179_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11179_GM;
MethodInfo m11179_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2242_m11179_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11179_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11180_GM;
MethodInfo m11180_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2242_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11180_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2242_m11181_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11181_GM;
MethodInfo m11181_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2242_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2242_m11181_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11181_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11182_GM;
MethodInfo m11182_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11182_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2242_m11183_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11183_GM;
MethodInfo m11183_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2242_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2242_m11183_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11183_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2242_m11184_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11184_GM;
MethodInfo m11184_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2242_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2242_m11184_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11184_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2242_m11185_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11185_GM;
MethodInfo m11185_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2242_m11185_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11185_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2242_m11186_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11186_GM;
MethodInfo m11186_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2242_m11186_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11186_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2242_m11187_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11187_GM;
MethodInfo m11187_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2242_m11187_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11187_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11188_GM;
MethodInfo m11188_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2242_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11188_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11189_GM;
MethodInfo m11189_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2242_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11189_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11190_GM;
MethodInfo m11190_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2242_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11190_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11191_GM;
MethodInfo m11191_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2242_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11191_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2242_m11192_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11192_GM;
MethodInfo m11192_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2242_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2242_m11192_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11192_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2242_m11193_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11193_GM;
MethodInfo m11193_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2242_m11193_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11193_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2242_m11194_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11194_GM;
MethodInfo m11194_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2242_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2242_m11194_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11194_GM};
extern Il2CppType t2238_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2242_m11195_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2238_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11195_GM;
MethodInfo m11195_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2242_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2242_m11195_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11195_GM};
extern Il2CppType t2239_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11196_GM;
MethodInfo m11196_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2242_TI, &t2239_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11196_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2242_m11197_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11197_GM;
MethodInfo m11197_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2242_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2242_m11197_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11197_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11198_GM;
MethodInfo m11198_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2242_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11198_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2242_m11199_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11199_GM;
MethodInfo m11199_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2242_TI, &t28_0_0_0, RuntimeInvoker_t29_t44, t2242_m11199_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11199_GM};
static MethodInfo* t2242_MIs[] =
{
	&m11170_MI,
	&m11171_MI,
	&m11172_MI,
	&m11173_MI,
	&m11174_MI,
	&m11175_MI,
	&m11176_MI,
	&m11177_MI,
	&m11178_MI,
	&m11179_MI,
	&m11180_MI,
	&m11181_MI,
	&m11182_MI,
	&m11183_MI,
	&m11184_MI,
	&m11185_MI,
	&m11186_MI,
	&m11187_MI,
	&m11188_MI,
	&m11189_MI,
	&m11190_MI,
	&m11191_MI,
	&m11192_MI,
	&m11193_MI,
	&m11194_MI,
	&m11195_MI,
	&m11196_MI,
	&m11197_MI,
	&m11198_MI,
	&m11199_MI,
	NULL
};
extern MethodInfo m11180_MI;
extern MethodInfo m11179_MI;
extern MethodInfo m11181_MI;
extern MethodInfo m11182_MI;
extern MethodInfo m11183_MI;
extern MethodInfo m11184_MI;
extern MethodInfo m11185_MI;
extern MethodInfo m11186_MI;
extern MethodInfo m11187_MI;
extern MethodInfo m11171_MI;
extern MethodInfo m11172_MI;
extern MethodInfo m11194_MI;
extern MethodInfo m11195_MI;
extern MethodInfo m11174_MI;
extern MethodInfo m11197_MI;
extern MethodInfo m11173_MI;
extern MethodInfo m11175_MI;
extern MethodInfo m11196_MI;
static MethodInfo* t2242_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11180_MI,
	&m11198_MI,
	&m11188_MI,
	&m11189_MI,
	&m11179_MI,
	&m11190_MI,
	&m11191_MI,
	&m11192_MI,
	&m11193_MI,
	&m11181_MI,
	&m11182_MI,
	&m11183_MI,
	&m11184_MI,
	&m11185_MI,
	&m11186_MI,
	&m11187_MI,
	&m11198_MI,
	&m11178_MI,
	&m11171_MI,
	&m11172_MI,
	&m11194_MI,
	&m11195_MI,
	&m11174_MI,
	&m11197_MI,
	&m11173_MI,
	&m11175_MI,
	&m11176_MI,
	&m11177_MI,
	&m11196_MI,
	&m11199_MI,
};
static TypeInfo* t2242_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
};
static Il2CppInterfaceOffsetPair t2242_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2240_TI, 20},
	{ &t2245_TI, 27},
	{ &t2241_TI, 32},
};
extern TypeInfo t28_TI;
static Il2CppRGCTXData t2242_RGCTXData[9] = 
{
	&m11199_MI/* Method Usage */,
	&m11231_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
	&m26312_MI/* Method Usage */,
	&m26319_MI/* Method Usage */,
	&m26317_MI/* Method Usage */,
	&m26313_MI/* Method Usage */,
	&m26315_MI/* Method Usage */,
	&m26308_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2242_0_0_0;
extern Il2CppType t2242_1_0_0;
struct t2242;
extern Il2CppGenericClass t2242_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2242_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2242_MIs, t2242_PIs, t2242_FIs, NULL, &t29_TI, NULL, NULL, &t2242_TI, t2242_ITIs, t2242_VT, &t1263__CustomAttributeCache, &t2242_TI, &t2242_0_0_0, &t2242_1_0_0, t2242_IOs, &t2242_GC, NULL, NULL, NULL, t2242_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2242), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2246.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2246_TI;

#include "t43.h"
extern MethodInfo m26309_MI;
extern MethodInfo m11234_MI;
extern MethodInfo m11235_MI;
extern MethodInfo m11232_MI;
extern MethodInfo m11230_MI;
extern MethodInfo m11119_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m11223_MI;
extern MethodInfo m11233_MI;
extern MethodInfo m11221_MI;
extern MethodInfo m11226_MI;
extern MethodInfo m11217_MI;
extern MethodInfo m26311_MI;
extern MethodInfo m26320_MI;
extern MethodInfo m26321_MI;
extern MethodInfo m26318_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.Component>
extern Il2CppType t2245_0_0_1;
FieldInfo t2246_f0_FieldInfo = 
{
	"list", &t2245_0_0_1, &t2246_TI, offsetof(t2246, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2246_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2246_TI, offsetof(t2246, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2246_FIs[] =
{
	&t2246_f0_FieldInfo,
	&t2246_f1_FieldInfo,
	NULL
};
extern MethodInfo m11201_MI;
static PropertyInfo t2246____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2246_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11201_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11209_MI;
static PropertyInfo t2246____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2246_TI, "System.Collections.ICollection.IsSynchronized", &m11209_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11210_MI;
static PropertyInfo t2246____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2246_TI, "System.Collections.ICollection.SyncRoot", &m11210_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11211_MI;
static PropertyInfo t2246____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2246_TI, "System.Collections.IList.IsFixedSize", &m11211_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11212_MI;
static PropertyInfo t2246____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2246_TI, "System.Collections.IList.IsReadOnly", &m11212_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11213_MI;
extern MethodInfo m11214_MI;
static PropertyInfo t2246____System_Collections_IList_Item_PropertyInfo = 
{
	&t2246_TI, "System.Collections.IList.Item", &m11213_MI, &m11214_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11227_MI;
static PropertyInfo t2246____Count_PropertyInfo = 
{
	&t2246_TI, "Count", &m11227_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11228_MI;
extern MethodInfo m11229_MI;
static PropertyInfo t2246____Item_PropertyInfo = 
{
	&t2246_TI, "Item", &m11228_MI, &m11229_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2246_PIs[] =
{
	&t2246____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2246____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2246____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2246____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2246____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2246____System_Collections_IList_Item_PropertyInfo,
	&t2246____Count_PropertyInfo,
	&t2246____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11200_GM;
MethodInfo m11200_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11200_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11201_GM;
MethodInfo m11201_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2246_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11201_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2246_m11202_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11202_GM;
MethodInfo m11202_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2246_m11202_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11202_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11203_GM;
MethodInfo m11203_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2246_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11203_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2246_m11204_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11204_GM;
MethodInfo m11204_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2246_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2246_m11204_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11204_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2246_m11205_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11205_GM;
MethodInfo m11205_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2246_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2246_m11205_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11205_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2246_m11206_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11206_GM;
MethodInfo m11206_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2246_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2246_m11206_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11206_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2246_m11207_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11207_GM;
MethodInfo m11207_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2246_m11207_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11207_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2246_m11208_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11208_GM;
MethodInfo m11208_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2246_m11208_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11208_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11209_GM;
MethodInfo m11209_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2246_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11209_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11210_GM;
MethodInfo m11210_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2246_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11210_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11211_GM;
MethodInfo m11211_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2246_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11211_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11212_GM;
MethodInfo m11212_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2246_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11212_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2246_m11213_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11213_GM;
MethodInfo m11213_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2246_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2246_m11213_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11213_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2246_m11214_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11214_GM;
MethodInfo m11214_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2246_m11214_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11214_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2246_m11215_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11215_GM;
MethodInfo m11215_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2246_m11215_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11215_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11216_GM;
MethodInfo m11216_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11216_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11217_GM;
MethodInfo m11217_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11217_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2246_m11218_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11218_GM;
MethodInfo m11218_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2246_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2246_m11218_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11218_GM};
extern Il2CppType t2238_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2246_m11219_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2238_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11219_GM;
MethodInfo m11219_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2246_m11219_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11219_GM};
extern Il2CppType t2239_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11220_GM;
MethodInfo m11220_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2246_TI, &t2239_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11220_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2246_m11221_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11221_GM;
MethodInfo m11221_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2246_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2246_m11221_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11221_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2246_m11222_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11222_GM;
MethodInfo m11222_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2246_m11222_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11222_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2246_m11223_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11223_GM;
MethodInfo m11223_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2246_m11223_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11223_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2246_m11224_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11224_GM;
MethodInfo m11224_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2246_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2246_m11224_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11224_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2246_m11225_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11225_GM;
MethodInfo m11225_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2246_m11225_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11225_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2246_m11226_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11226_GM;
MethodInfo m11226_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2246_m11226_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11226_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11227_GM;
MethodInfo m11227_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2246_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11227_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2246_m11228_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11228_GM;
MethodInfo m11228_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2246_TI, &t28_0_0_0, RuntimeInvoker_t29_t44, t2246_m11228_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11228_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2246_m11229_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11229_GM;
MethodInfo m11229_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2246_m11229_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11229_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2246_m11230_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11230_GM;
MethodInfo m11230_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2246_m11230_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11230_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2246_m11231_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11231_GM;
MethodInfo m11231_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2246_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2246_m11231_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11231_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2246_m11232_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t28_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11232_GM;
MethodInfo m11232_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2246_TI, &t28_0_0_0, RuntimeInvoker_t29_t29, t2246_m11232_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11232_GM};
extern Il2CppType t2245_0_0_0;
static ParameterInfo t2246_m11233_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2245_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11233_GM;
MethodInfo m11233_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2246_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2246_m11233_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11233_GM};
extern Il2CppType t2245_0_0_0;
static ParameterInfo t2246_m11234_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2245_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11234_GM;
MethodInfo m11234_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2246_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2246_m11234_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11234_GM};
extern Il2CppType t2245_0_0_0;
static ParameterInfo t2246_m11235_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2245_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11235_GM;
MethodInfo m11235_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2246_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2246_m11235_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11235_GM};
static MethodInfo* t2246_MIs[] =
{
	&m11200_MI,
	&m11201_MI,
	&m11202_MI,
	&m11203_MI,
	&m11204_MI,
	&m11205_MI,
	&m11206_MI,
	&m11207_MI,
	&m11208_MI,
	&m11209_MI,
	&m11210_MI,
	&m11211_MI,
	&m11212_MI,
	&m11213_MI,
	&m11214_MI,
	&m11215_MI,
	&m11216_MI,
	&m11217_MI,
	&m11218_MI,
	&m11219_MI,
	&m11220_MI,
	&m11221_MI,
	&m11222_MI,
	&m11223_MI,
	&m11224_MI,
	&m11225_MI,
	&m11226_MI,
	&m11227_MI,
	&m11228_MI,
	&m11229_MI,
	&m11230_MI,
	&m11231_MI,
	&m11232_MI,
	&m11233_MI,
	&m11234_MI,
	&m11235_MI,
	NULL
};
extern MethodInfo m11203_MI;
extern MethodInfo m11202_MI;
extern MethodInfo m11204_MI;
extern MethodInfo m11216_MI;
extern MethodInfo m11205_MI;
extern MethodInfo m11206_MI;
extern MethodInfo m11207_MI;
extern MethodInfo m11208_MI;
extern MethodInfo m11225_MI;
extern MethodInfo m11215_MI;
extern MethodInfo m11218_MI;
extern MethodInfo m11219_MI;
extern MethodInfo m11224_MI;
extern MethodInfo m11222_MI;
extern MethodInfo m11220_MI;
static MethodInfo* t2246_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11203_MI,
	&m11227_MI,
	&m11209_MI,
	&m11210_MI,
	&m11202_MI,
	&m11211_MI,
	&m11212_MI,
	&m11213_MI,
	&m11214_MI,
	&m11204_MI,
	&m11216_MI,
	&m11205_MI,
	&m11206_MI,
	&m11207_MI,
	&m11208_MI,
	&m11225_MI,
	&m11227_MI,
	&m11201_MI,
	&m11215_MI,
	&m11216_MI,
	&m11218_MI,
	&m11219_MI,
	&m11224_MI,
	&m11221_MI,
	&m11222_MI,
	&m11225_MI,
	&m11228_MI,
	&m11229_MI,
	&m11220_MI,
	&m11217_MI,
	&m11223_MI,
	&m11226_MI,
	&m11230_MI,
};
static TypeInfo* t2246_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2240_TI,
	&t2245_TI,
	&t2241_TI,
};
static Il2CppInterfaceOffsetPair t2246_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2240_TI, 20},
	{ &t2245_TI, 27},
	{ &t2241_TI, 32},
};
extern TypeInfo t268_TI;
extern TypeInfo t28_TI;
static Il2CppRGCTXData t2246_RGCTXData[25] = 
{
	&t268_TI/* Class Usage */,
	&m11119_MI/* Method Usage */,
	&m26309_MI/* Method Usage */,
	&m26315_MI/* Method Usage */,
	&m26308_MI/* Method Usage */,
	&m11232_MI/* Method Usage */,
	&m11223_MI/* Method Usage */,
	&m11231_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
	&m26312_MI/* Method Usage */,
	&m26319_MI/* Method Usage */,
	&m11233_MI/* Method Usage */,
	&m11221_MI/* Method Usage */,
	&m11226_MI/* Method Usage */,
	&m11234_MI/* Method Usage */,
	&m11235_MI/* Method Usage */,
	&m26317_MI/* Method Usage */,
	&m11230_MI/* Method Usage */,
	&m11217_MI/* Method Usage */,
	&m26311_MI/* Method Usage */,
	&m26313_MI/* Method Usage */,
	&m26320_MI/* Method Usage */,
	&m26321_MI/* Method Usage */,
	&m26318_MI/* Method Usage */,
	&t28_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2246_0_0_0;
extern Il2CppType t2246_1_0_0;
struct t2246;
extern Il2CppGenericClass t2246_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2246_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2246_MIs, t2246_PIs, t2246_FIs, NULL, &t29_TI, NULL, NULL, &t2246_TI, t2246_ITIs, t2246_VT, &t1262__CustomAttributeCache, &t2246_TI, &t2246_0_0_0, &t2246_1_0_0, t2246_IOs, &t2246_GC, NULL, NULL, NULL, t2246_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2246), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2247_TI;
#include "t2247MD.h"

#include "t1257.h"
#include "t2248.h"
extern TypeInfo t6632_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2248_TI;
#include "t931MD.h"
#include "t2248MD.h"
extern Il2CppType t6632_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m11241_MI;
extern MethodInfo m26916_MI;
extern MethodInfo m20062_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.Component>
extern Il2CppType t2247_0_0_49;
FieldInfo t2247_f0_FieldInfo = 
{
	"_default", &t2247_0_0_49, &t2247_TI, offsetof(t2247_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2247_FIs[] =
{
	&t2247_f0_FieldInfo,
	NULL
};
extern MethodInfo m11240_MI;
static PropertyInfo t2247____Default_PropertyInfo = 
{
	&t2247_TI, "Default", &m11240_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2247_PIs[] =
{
	&t2247____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11236_GM;
MethodInfo m11236_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2247_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11236_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11237_GM;
MethodInfo m11237_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2247_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11237_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2247_m11238_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11238_GM;
MethodInfo m11238_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2247_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2247_m11238_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11238_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2247_m11239_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11239_GM;
MethodInfo m11239_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2247_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2247_m11239_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11239_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2247_m26916_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26916_GM;
MethodInfo m26916_MI = 
{
	"GetHashCode", NULL, &t2247_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2247_m26916_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26916_GM};
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2247_m20062_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20062_GM;
MethodInfo m20062_MI = 
{
	"Equals", NULL, &t2247_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2247_m20062_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20062_GM};
extern Il2CppType t2247_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11240_GM;
MethodInfo m11240_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2247_TI, &t2247_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11240_GM};
static MethodInfo* t2247_MIs[] =
{
	&m11236_MI,
	&m11237_MI,
	&m11238_MI,
	&m11239_MI,
	&m26916_MI,
	&m20062_MI,
	&m11240_MI,
	NULL
};
extern MethodInfo m11239_MI;
extern MethodInfo m11238_MI;
static MethodInfo* t2247_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m20062_MI,
	&m26916_MI,
	&m11239_MI,
	&m11238_MI,
	NULL,
	NULL,
};
extern TypeInfo t6633_TI;
extern TypeInfo t734_TI;
static TypeInfo* t2247_ITIs[] = 
{
	&t6633_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2247_IOs[] = 
{
	{ &t6633_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2247_TI;
extern TypeInfo t2247_TI;
extern TypeInfo t2248_TI;
extern TypeInfo t28_TI;
static Il2CppRGCTXData t2247_RGCTXData[9] = 
{
	&t6632_0_0_0/* Type Usage */,
	&t28_0_0_0/* Type Usage */,
	&t2247_TI/* Class Usage */,
	&t2247_TI/* Static Usage */,
	&t2248_TI/* Class Usage */,
	&m11241_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
	&m26916_MI/* Method Usage */,
	&m20062_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2247_0_0_0;
extern Il2CppType t2247_1_0_0;
struct t2247;
extern Il2CppGenericClass t2247_GC;
TypeInfo t2247_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2247_MIs, t2247_PIs, t2247_FIs, NULL, &t29_TI, NULL, NULL, &t2247_TI, t2247_ITIs, t2247_VT, &EmptyCustomAttributesCache, &t2247_TI, &t2247_0_0_0, &t2247_1_0_0, t2247_IOs, &t2247_GC, NULL, NULL, NULL, t2247_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2247), 0, -1, sizeof(t2247_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.Component>
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t6633_m26917_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26917_GM;
MethodInfo m26917_MI = 
{
	"Equals", NULL, &t6633_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6633_m26917_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26917_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t6633_m26918_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26918_GM;
MethodInfo m26918_MI = 
{
	"GetHashCode", NULL, &t6633_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6633_m26918_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26918_GM};
static MethodInfo* t6633_MIs[] =
{
	&m26917_MI,
	&m26918_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6633_0_0_0;
extern Il2CppType t6633_1_0_0;
struct t6633;
extern Il2CppGenericClass t6633_GC;
TypeInfo t6633_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6633_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6633_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6633_TI, &t6633_0_0_0, &t6633_1_0_0, NULL, &t6633_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.Component>
extern Il2CppType t28_0_0_0;
static ParameterInfo t6632_m26919_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26919_GM;
MethodInfo m26919_MI = 
{
	"Equals", NULL, &t6632_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6632_m26919_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26919_GM};
static MethodInfo* t6632_MIs[] =
{
	&m26919_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6632_1_0_0;
struct t6632;
extern Il2CppGenericClass t6632_GC;
TypeInfo t6632_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6632_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6632_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6632_TI, &t6632_0_0_0, &t6632_1_0_0, NULL, &t6632_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m11236_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Component>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11241_GM;
MethodInfo m11241_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2248_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11241_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t2248_m11242_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11242_GM;
MethodInfo m11242_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2248_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2248_m11242_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11242_GM};
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2248_m11243_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11243_GM;
MethodInfo m11243_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2248_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2248_m11243_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11243_GM};
static MethodInfo* t2248_MIs[] =
{
	&m11241_MI,
	&m11242_MI,
	&m11243_MI,
	NULL
};
extern MethodInfo m11243_MI;
extern MethodInfo m11242_MI;
static MethodInfo* t2248_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11243_MI,
	&m11242_MI,
	&m11239_MI,
	&m11238_MI,
	&m11242_MI,
	&m11243_MI,
};
static Il2CppInterfaceOffsetPair t2248_IOs[] = 
{
	{ &t6633_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2247_TI;
extern TypeInfo t2247_TI;
extern TypeInfo t2248_TI;
extern TypeInfo t28_TI;
extern TypeInfo t28_TI;
static Il2CppRGCTXData t2248_RGCTXData[11] = 
{
	&t6632_0_0_0/* Type Usage */,
	&t28_0_0_0/* Type Usage */,
	&t2247_TI/* Class Usage */,
	&t2247_TI/* Static Usage */,
	&t2248_TI/* Class Usage */,
	&m11241_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
	&m26916_MI/* Method Usage */,
	&m20062_MI/* Method Usage */,
	&m11236_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2248_0_0_0;
extern Il2CppType t2248_1_0_0;
struct t2248;
extern Il2CppGenericClass t2248_GC;
extern TypeInfo t1256_TI;
TypeInfo t2248_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2248_MIs, NULL, NULL, NULL, &t2247_TI, NULL, &t1256_TI, &t2248_TI, NULL, t2248_VT, &EmptyCustomAttributesCache, &t2248_TI, &t2248_0_0_0, &t2248_1_0_0, t2248_IOs, &t2248_GC, NULL, NULL, NULL, t2248_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2248), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.Component>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t267_m2000_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2000_GM;
MethodInfo m2000_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t267_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t267_m2000_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m2000_GM};
extern Il2CppType t28_0_0_0;
static ParameterInfo t267_m11244_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11244_GM;
MethodInfo m11244_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t267_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t267_m11244_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11244_GM};
extern Il2CppType t28_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t267_m11245_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11245_GM;
MethodInfo m11245_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t267_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t267_m11245_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11245_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t267_m11246_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11246_GM;
MethodInfo m11246_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t267_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t267_m11246_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11246_GM};
static MethodInfo* t267_MIs[] =
{
	&m2000_MI,
	&m11244_MI,
	&m11245_MI,
	&m11246_MI,
	NULL
};
extern MethodInfo m11245_MI;
extern MethodInfo m11246_MI;
static MethodInfo* t267_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11244_MI,
	&m11245_MI,
	&m11246_MI,
};
static Il2CppInterfaceOffsetPair t267_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t267_1_0_0;
struct t267;
extern Il2CppGenericClass t267_GC;
TypeInfo t267_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t267_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t267_TI, NULL, t267_VT, &EmptyCustomAttributesCache, &t267_TI, &t267_0_0_0, &t267_1_0_0, t267_IOs, &t267_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t267), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t2250.h"
extern TypeInfo t4032_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t2250_TI;
#include "t2250MD.h"
extern Il2CppType t4032_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m11251_MI;
extern MethodInfo m26920_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.Component>
extern Il2CppType t2249_0_0_49;
FieldInfo t2249_f0_FieldInfo = 
{
	"_default", &t2249_0_0_49, &t2249_TI, offsetof(t2249_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2249_FIs[] =
{
	&t2249_f0_FieldInfo,
	NULL
};
static PropertyInfo t2249____Default_PropertyInfo = 
{
	&t2249_TI, "Default", &m11250_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2249_PIs[] =
{
	&t2249____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11247_GM;
MethodInfo m11247_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2249_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11247_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11248_GM;
MethodInfo m11248_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2249_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11248_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2249_m11249_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11249_GM;
MethodInfo m11249_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2249_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2249_m11249_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11249_GM};
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2249_m26920_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26920_GM;
MethodInfo m26920_MI = 
{
	"Compare", NULL, &t2249_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2249_m26920_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26920_GM};
extern Il2CppType t2249_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11250_GM;
MethodInfo m11250_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2249_TI, &t2249_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11250_GM};
static MethodInfo* t2249_MIs[] =
{
	&m11247_MI,
	&m11248_MI,
	&m11249_MI,
	&m26920_MI,
	&m11250_MI,
	NULL
};
extern MethodInfo m11249_MI;
static MethodInfo* t2249_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m26920_MI,
	&m11249_MI,
	NULL,
};
extern TypeInfo t4031_TI;
extern TypeInfo t726_TI;
static TypeInfo* t2249_ITIs[] = 
{
	&t4031_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2249_IOs[] = 
{
	{ &t4031_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2249_TI;
extern TypeInfo t2249_TI;
extern TypeInfo t2250_TI;
extern TypeInfo t28_TI;
static Il2CppRGCTXData t2249_RGCTXData[8] = 
{
	&t4032_0_0_0/* Type Usage */,
	&t28_0_0_0/* Type Usage */,
	&t2249_TI/* Class Usage */,
	&t2249_TI/* Static Usage */,
	&t2250_TI/* Class Usage */,
	&m11251_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
	&m26920_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2249_0_0_0;
extern Il2CppType t2249_1_0_0;
struct t2249;
extern Il2CppGenericClass t2249_GC;
TypeInfo t2249_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2249_MIs, t2249_PIs, t2249_FIs, NULL, &t29_TI, NULL, NULL, &t2249_TI, t2249_ITIs, t2249_VT, &EmptyCustomAttributesCache, &t2249_TI, &t2249_0_0_0, &t2249_1_0_0, t2249_IOs, &t2249_GC, NULL, NULL, NULL, t2249_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2249), 0, -1, sizeof(t2249_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.Component>
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t4031_m20070_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20070_GM;
MethodInfo m20070_MI = 
{
	"Compare", NULL, &t4031_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4031_m20070_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20070_GM};
static MethodInfo* t4031_MIs[] =
{
	&m20070_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4031_0_0_0;
extern Il2CppType t4031_1_0_0;
struct t4031;
extern Il2CppGenericClass t4031_GC;
TypeInfo t4031_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4031_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4031_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4031_TI, &t4031_0_0_0, &t4031_1_0_0, NULL, &t4031_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.Component>
extern Il2CppType t28_0_0_0;
static ParameterInfo t4032_m20071_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20071_GM;
MethodInfo m20071_MI = 
{
	"CompareTo", NULL, &t4032_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4032_m20071_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20071_GM};
static MethodInfo* t4032_MIs[] =
{
	&m20071_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4032_1_0_0;
struct t4032;
extern Il2CppGenericClass t4032_GC;
TypeInfo t4032_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4032_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4032_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4032_TI, &t4032_0_0_0, &t4032_1_0_0, NULL, &t4032_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m11247_MI;
extern MethodInfo m20071_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Component>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11251_GM;
MethodInfo m11251_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2250_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11251_GM};
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2250_m11252_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11252_GM;
MethodInfo m11252_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2250_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2250_m11252_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11252_GM};
static MethodInfo* t2250_MIs[] =
{
	&m11251_MI,
	&m11252_MI,
	NULL
};
extern MethodInfo m11252_MI;
static MethodInfo* t2250_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11252_MI,
	&m11249_MI,
	&m11252_MI,
};
static Il2CppInterfaceOffsetPair t2250_IOs[] = 
{
	{ &t4031_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2249_TI;
extern TypeInfo t2249_TI;
extern TypeInfo t2250_TI;
extern TypeInfo t28_TI;
extern TypeInfo t28_TI;
extern TypeInfo t4032_TI;
static Il2CppRGCTXData t2250_RGCTXData[12] = 
{
	&t4032_0_0_0/* Type Usage */,
	&t28_0_0_0/* Type Usage */,
	&t2249_TI/* Class Usage */,
	&t2249_TI/* Static Usage */,
	&t2250_TI/* Class Usage */,
	&m11251_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
	&m26920_MI/* Method Usage */,
	&m11247_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
	&t4032_TI/* Class Usage */,
	&m20071_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2250_0_0_0;
extern Il2CppType t2250_1_0_0;
struct t2250;
extern Il2CppGenericClass t2250_GC;
extern TypeInfo t1246_TI;
TypeInfo t2250_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2250_MIs, NULL, NULL, NULL, &t2249_TI, NULL, &t1246_TI, &t2250_TI, NULL, t2250_VT, &EmptyCustomAttributesCache, &t2250_TI, &t2250_0_0_0, &t2250_1_0_0, t2250_IOs, &t2250_GC, NULL, NULL, NULL, t2250_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2250), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2243_TI;
#include "t2243MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.Component>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2243_m11253_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11253_GM;
MethodInfo m11253_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2243_m11253_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11253_GM};
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2243_m11254_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11254_GM;
MethodInfo m11254_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2243_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2243_m11254_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11254_GM};
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2243_m11255_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11255_GM;
MethodInfo m11255_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2243_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2243_m11255_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11255_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2243_m11256_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11256_GM;
MethodInfo m11256_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2243_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2243_m11256_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11256_GM};
static MethodInfo* t2243_MIs[] =
{
	&m11253_MI,
	&m11254_MI,
	&m11255_MI,
	&m11256_MI,
	NULL
};
extern MethodInfo m11254_MI;
extern MethodInfo m11255_MI;
extern MethodInfo m11256_MI;
static MethodInfo* t2243_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11254_MI,
	&m11255_MI,
	&m11256_MI,
};
static Il2CppInterfaceOffsetPair t2243_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2243_1_0_0;
struct t2243;
extern Il2CppGenericClass t2243_GC;
TypeInfo t2243_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2243_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2243_TI, NULL, t2243_VT, &EmptyCustomAttributesCache, &t2243_TI, &t2243_0_0_0, &t2243_1_0_0, t2243_IOs, &t2243_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2243), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t81.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t81_TI;
#include "t81MD.h"

#include "t53.h"


// Metadata Definition UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t81_m1352_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1352_GM;
MethodInfo m1352_MI = 
{
	".ctor", (methodPointerType)&m10902_gshared, &t81_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t81_m1352_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1352_GM};
extern Il2CppType t99_0_0_0;
extern Il2CppType t99_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t81_m11257_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t99_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11257_GM;
MethodInfo m11257_MI = 
{
	"Invoke", (methodPointerType)&m10904_gshared, &t81_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t81_m11257_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11257_GM};
extern Il2CppType t99_0_0_0;
extern Il2CppType t53_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t81_m11258_ParameterInfos[] = 
{
	{"handler", 0, 134217728, &EmptyCustomAttributesCache, &t99_0_0_0},
	{"eventData", 1, 134217728, &EmptyCustomAttributesCache, &t53_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11258_GM;
MethodInfo m11258_MI = 
{
	"BeginInvoke", (methodPointerType)&m10906_gshared, &t81_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t81_m11258_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11258_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t81_m11259_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11259_GM;
MethodInfo m11259_MI = 
{
	"EndInvoke", (methodPointerType)&m10908_gshared, &t81_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t81_m11259_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11259_GM};
static MethodInfo* t81_MIs[] =
{
	&m1352_MI,
	&m11257_MI,
	&m11258_MI,
	&m11259_MI,
	NULL
};
extern MethodInfo m11257_MI;
extern MethodInfo m11258_MI;
extern MethodInfo m11259_MI;
static MethodInfo* t81_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11257_MI,
	&m11258_MI,
	&m11259_MI,
};
static Il2CppInterfaceOffsetPair t81_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t81_0_0_0;
extern Il2CppType t81_1_0_0;
struct t81;
extern Il2CppGenericClass t81_GC;
extern TypeInfo t68_TI;
TypeInfo t81_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "EventFunction`1", "", t81_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t68_TI, &t81_TI, NULL, t81_VT, &EmptyCustomAttributesCache, &t81_TI, &t81_0_0_0, &t81_1_0_0, t81_IOs, &t81_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t81), 0, -1, 0, 0, -1, 258, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t56.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t56_TI;
#include "t56MD.h"

#include "t57.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "t2257.h"
#include "t2255.h"
#include "t2256.h"
#include "t2263.h"
#include "t54.h"
extern TypeInfo t57_TI;
extern TypeInfo t2251_TI;
extern TypeInfo t2257_TI;
extern TypeInfo t2253_TI;
extern TypeInfo t2254_TI;
extern TypeInfo t2252_TI;
extern TypeInfo t2255_TI;
extern TypeInfo t2256_TI;
extern TypeInfo t2263_TI;
#include "t2255MD.h"
#include "t2256MD.h"
#include "t2257MD.h"
#include "t2263MD.h"
extern MethodInfo m1399_MI;
extern MethodInfo m11302_MI;
extern MethodInfo m20089_MI;
extern MethodInfo m11290_MI;
extern MethodInfo m11287_MI;
extern MethodInfo m1478_MI;
extern MethodInfo m11282_MI;
extern MethodInfo m11288_MI;
extern MethodInfo m11291_MI;
extern MethodInfo m11293_MI;
extern MethodInfo m11277_MI;
extern MethodInfo m11300_MI;
extern MethodInfo m11301_MI;
extern MethodInfo m26921_MI;
extern MethodInfo m26922_MI;
extern MethodInfo m26923_MI;
extern MethodInfo m26924_MI;
extern MethodInfo m11292_MI;
extern MethodInfo m11278_MI;
extern MethodInfo m11279_MI;
extern MethodInfo m11314_MI;
extern MethodInfo m20091_MI;
extern MethodInfo m11285_MI;
extern MethodInfo m11286_MI;
extern MethodInfo m11389_MI;
extern MethodInfo m11308_MI;
extern MethodInfo m11289_MI;
extern MethodInfo m11295_MI;
extern MethodInfo m11395_MI;
extern MethodInfo m20093_MI;
extern MethodInfo m20101_MI;
struct t20;
 void m20089 (t29 * __this, t2251** p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t20;
#include "t2261.h"
 int32_t m20091 (t29 * __this, t2251* p0, t57  p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t20;
 void m20093 (t29 * __this, t2251* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t20;
 void m20101 (t29 * __this, t2251* p0, int32_t p1, t54 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m1397_MI;
 void m1397 (t56 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t56_TI));
		__this->f1 = (((t56_SFs*)InitializedTypeInfo(&t56_TI)->static_fields)->f4);
		return;
	}
}
extern MethodInfo m11260_MI;
 void m11260 (t56 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0015:
	{
		__this->f1 = ((t2251*)SZArrayNew(InitializedTypeInfo(&t2251_TI), p0));
		return;
	}
}
extern MethodInfo m11261_MI;
 void m11261 (t29 * __this, MethodInfo* method){
	{
		((t56_SFs*)InitializedTypeInfo(&t56_TI)->static_fields)->f4 = ((t2251*)SZArrayNew(InitializedTypeInfo(&t2251_TI), 0));
		return;
	}
}
extern MethodInfo m11262_MI;
 t29* m11262 (t56 * __this, MethodInfo* method){
	{
		t2257  L_0 = m11287(__this, &m11287_MI);
		t2257  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2257_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m11263_MI;
 void m11263 (t56 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t2251* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_0, 0, p0, p1, L_1, &m5952_MI);
		return;
	}
}
extern MethodInfo m11264_MI;
 t29 * m11264 (t56 * __this, MethodInfo* method){
	{
		t2257  L_0 = m11287(__this, &m11287_MI);
		t2257  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2257_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m11265_MI;
 int32_t m11265 (t56 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker1< t57  >::Invoke(&m1478_MI, __this, ((*(t57 *)((t57 *)UnBox (p0, InitializedTypeInfo(&t57_TI))))));
			int32_t L_0 = (__this->f2);
			V_0 = ((int32_t)(L_0-1));
			// IL_0015: leave.s IL_002a
			goto IL_002a;
		}

IL_0017:
		{
			// IL_0017: leave.s IL_001f
			goto IL_001f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0019;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_001c;
		throw e;
	}

IL_0019:
	{ // begin catch(System.NullReferenceException)
		// IL_001a: leave.s IL_001f
		goto IL_001f;
	} // end catch (depth: 1)

IL_001c:
	{ // begin catch(System.InvalidCastException)
		// IL_001d: leave.s IL_001f
		goto IL_001f;
	} // end catch (depth: 1)

IL_001f:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_002a:
	{
		return V_0;
	}
}
extern MethodInfo m11266_MI;
 bool m11266 (t56 * __this, t29 * p0, MethodInfo* method){
	bool V_0 = false;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = (bool)VirtFuncInvoker1< bool, t57  >::Invoke(&m11282_MI, __this, ((*(t57 *)((t57 *)UnBox (p0, InitializedTypeInfo(&t57_TI))))));
			V_0 = L_0;
			// IL_000d: leave.s IL_0019
			goto IL_0019;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return 0;
	}

IL_0019:
	{
		return V_0;
	}
}
extern MethodInfo m11267_MI;
 int32_t m11267 (t56 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t57  >::Invoke(&m11288_MI, __this, ((*(t57 *)((t57 *)UnBox (p0, InitializedTypeInfo(&t57_TI))))));
			V_0 = L_0;
			// IL_000d: leave.s IL_0019
			goto IL_0019;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return (-1);
	}

IL_0019:
	{
		return V_0;
	}
}
extern MethodInfo m11268_MI;
 void m11268 (t56 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		m11290(__this, p0, &m11290_MI);
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker2< int32_t, t57  >::Invoke(&m11291_MI, __this, p0, ((*(t57 *)((t57 *)UnBox (p1, InitializedTypeInfo(&t57_TI))))));
			// IL_0014: leave.s IL_0029
			goto IL_0029;
		}

IL_0016:
		{
			// IL_0016: leave.s IL_001e
			goto IL_001e;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0018;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_001b;
		throw e;
	}

IL_0018:
	{ // begin catch(System.NullReferenceException)
		// IL_0019: leave.s IL_001e
		goto IL_001e;
	} // end catch (depth: 1)

IL_001b:
	{ // begin catch(System.InvalidCastException)
		// IL_001c: leave.s IL_001e
		goto IL_001e;
	} // end catch (depth: 1)

IL_001e:
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0029:
	{
		return;
	}
}
extern MethodInfo m11269_MI;
 void m11269 (t56 * __this, t29 * p0, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtFuncInvoker1< bool, t57  >::Invoke(&m11293_MI, __this, ((*(t57 *)((t57 *)UnBox (p0, InitializedTypeInfo(&t57_TI))))));
			// IL_000d: leave.s IL_0017
			goto IL_0017;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return;
	}
}
extern MethodInfo m11270_MI;
 bool m11270 (t56 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m11271_MI;
 bool m11271 (t56 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m11272_MI;
 t29 * m11272 (t56 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m11273_MI;
 bool m11273 (t56 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m11274_MI;
 bool m11274 (t56 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m11275_MI;
 t29 * m11275 (t56 * __this, int32_t p0, MethodInfo* method){
	{
		t57  L_0 = (t57 )VirtFuncInvoker1< t57 , int32_t >::Invoke(&m1399_MI, __this, p0);
		t57  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t57_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m11276_MI;
 void m11276 (t56 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker2< int32_t, t57  >::Invoke(&m11302_MI, __this, p0, ((*(t57 *)((t57 *)UnBox (p1, InitializedTypeInfo(&t57_TI))))));
			// IL_000d: leave.s IL_0022
			goto IL_0022;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral401, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0022:
	{
		return;
	}
}
 void m1478 (t56 * __this, t57  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f2);
		t2251* L_1 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_0017;
		}
	}
	{
		m11277(__this, 1, &m11277_MI);
	}

IL_0017:
	{
		t2251* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		int32_t L_4 = L_3;
		V_0 = L_4;
		__this->f2 = ((int32_t)(L_4+1));
		*((t57 *)(t57 *)SZArrayLdElema(L_2, V_0)) = (t57 )p0;
		int32_t L_5 = (__this->f3);
		__this->f3 = ((int32_t)(L_5+1));
		return;
	}
}
 void m11277 (t56 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f2);
		V_0 = ((int32_t)(L_0+p0));
		t2251* L_1 = (__this->f1);
		if ((((int32_t)V_0) <= ((int32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = m11300(__this, &m11300_MI);
		int32_t L_3 = m5139(NULL, ((int32_t)((int32_t)L_2*(int32_t)2)), 4, &m5139_MI);
		int32_t L_4 = m5139(NULL, L_3, V_0, &m5139_MI);
		m11301(__this, L_4, &m11301_MI);
	}

IL_002e:
	{
		return;
	}
}
 void m11278 (t56 * __this, t29* p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m26921_MI, p0);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		m11277(__this, V_0, &m11277_MI);
		t2251* L_1 = (__this->f1);
		int32_t L_2 = (__this->f2);
		InterfaceActionInvoker2< t2251*, int32_t >::Invoke(&m26922_MI, p0, L_1, L_2);
		int32_t L_3 = (__this->f2);
		__this->f2 = ((int32_t)(L_3+V_0));
		return;
	}
}
 void m11279 (t56 * __this, t29* p0, MethodInfo* method){
	t57  V_0 = {0};
	t29* V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t29* L_0 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m26923_MI, p0);
		V_1 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0017;
		}

IL_0009:
		{
			t57  L_1 = (t57 )InterfaceFuncInvoker0< t57  >::Invoke(&m26924_MI, V_1);
			V_0 = L_1;
			VirtActionInvoker1< t57  >::Invoke(&m1478_MI, __this, V_0);
		}

IL_0017:
		{
			bool L_2 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_1);
			if (L_2)
			{
				goto IL_0009;
			}
		}

IL_001f:
		{
			// IL_001f: leave.s IL_002c
			leaveInstructions[0] = 0x2C; // 1
			THROW_SENTINEL(IL_002c);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0021;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0021;
	}

IL_0021:
	{ // begin finally (depth: 1)
		{
			if (V_1)
			{
				goto IL_0025;
			}
		}

IL_0024:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x2C:
					goto IL_002c;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_0025:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_1);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x2C:
					goto IL_002c;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_002c:
	{
		return;
	}
}
extern MethodInfo m11280_MI;
 void m11280 (t56 * __this, t29* p0, MethodInfo* method){
	t29* V_0 = {0};
	{
		m11292(__this, p0, &m11292_MI);
		V_0 = ((t29*)IsInst(p0, InitializedTypeInfo(&t2253_TI)));
		if (!V_0)
		{
			goto IL_001a;
		}
	}
	{
		m11278(__this, V_0, &m11278_MI);
		goto IL_0021;
	}

IL_001a:
	{
		m11279(__this, p0, &m11279_MI);
	}

IL_0021:
	{
		int32_t L_0 = (__this->f3);
		__this->f3 = ((int32_t)(L_0+1));
		return;
	}
}
extern MethodInfo m11281_MI;
 t2255 * m11281 (t56 * __this, MethodInfo* method){
	{
		t2255 * L_0 = (t2255 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2255_TI));
		m11314(L_0, __this, &m11314_MI);
		return L_0;
	}
}
extern MethodInfo m1305_MI;
 void m1305 (t56 * __this, MethodInfo* method){
	{
		t2251* L_0 = (__this->f1);
		t2251* L_1 = (__this->f1);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		__this->f2 = 0;
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
 bool m11282 (t56 * __this, t57  p0, MethodInfo* method){
	{
		t2251* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		int32_t L_2 = m20091(NULL, L_0, p0, 0, L_1, &m20091_MI);
		return ((((int32_t)((((int32_t)L_2) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m11283_MI;
 void m11283 (t56 * __this, t2251* p0, int32_t p1, MethodInfo* method){
	{
		t2251* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_0, 0, (t20 *)(t20 *)p0, p1, L_1, &m5952_MI);
		return;
	}
}
extern MethodInfo m11284_MI;
 t57  m11284 (t56 * __this, t2256 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t57  V_1 = {0};
	t57  G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t56_TI));
		m11285(NULL, p0, &m11285_MI);
		int32_t L_0 = (__this->f2);
		int32_t L_1 = m11286(__this, 0, L_0, p0, &m11286_MI);
		V_0 = L_1;
		if ((((int32_t)V_0) == ((int32_t)(-1))))
		{
			goto IL_0027;
		}
	}
	{
		t2251* L_2 = (__this->f1);
		int32_t L_3 = V_0;
		G_B3_0 = (*(t57 *)(t57 *)SZArrayLdElema(L_2, L_3));
		goto IL_0030;
	}

IL_0027:
	{
		Initobj (&t57_TI, (&V_1));
		G_B3_0 = V_1;
	}

IL_0030:
	{
		return G_B3_0;
	}
}
 void m11285 (t29 * __this, t2256 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1033, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		return;
	}
}
 int32_t m11286 (t56 * __this, int32_t p0, int32_t p1, t2256 * p2, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = ((int32_t)(p0+p1));
		V_1 = p0;
		goto IL_0022;
	}

IL_0008:
	{
		t2251* L_0 = (__this->f1);
		int32_t L_1 = V_1;
		bool L_2 = (bool)VirtFuncInvoker1< bool, t57  >::Invoke(&m11389_MI, p2, (*(t57 *)(t57 *)SZArrayLdElema(L_0, L_1)));
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return V_1;
	}

IL_001e:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0022:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0008;
		}
	}
	{
		return (-1);
	}
}
 t2257  m11287 (t56 * __this, MethodInfo* method){
	{
		t2257  L_0 = {0};
		m11308(&L_0, __this, &m11308_MI);
		return L_0;
	}
}
 int32_t m11288 (t56 * __this, t57  p0, MethodInfo* method){
	{
		t2251* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		int32_t L_2 = m20091(NULL, L_0, p0, 0, L_1, &m20091_MI);
		return L_2;
	}
}
 void m11289 (t56 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		p0 = ((int32_t)(p0-p1));
	}

IL_000b:
	{
		int32_t L_0 = (__this->f2);
		if ((((int32_t)p0) >= ((int32_t)L_0)))
		{
			goto IL_0031;
		}
	}
	{
		t2251* L_1 = (__this->f1);
		t2251* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_1, p0, (t20 *)(t20 *)L_2, ((int32_t)(p0+p1)), ((int32_t)(L_3-p0)), &m5952_MI);
	}

IL_0031:
	{
		int32_t L_4 = (__this->f2);
		__this->f2 = ((int32_t)(L_4+p1));
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_0056;
		}
	}
	{
		t2251* L_5 = (__this->f1);
		int32_t L_6 = (__this->f2);
		m5112(NULL, (t20 *)(t20 *)L_5, L_6, ((-p1)), &m5112_MI);
	}

IL_0056:
	{
		return;
	}
}
 void m11290 (t56 * __this, int32_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) <= ((uint32_t)L_0)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0018:
	{
		return;
	}
}
 void m11291 (t56 * __this, int32_t p0, t57  p1, MethodInfo* method){
	{
		m11290(__this, p0, &m11290_MI);
		int32_t L_0 = (__this->f2);
		t2251* L_1 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_001e;
		}
	}
	{
		m11277(__this, 1, &m11277_MI);
	}

IL_001e:
	{
		m11289(__this, p0, 1, &m11289_MI);
		t2251* L_2 = (__this->f1);
		*((t57 *)(t57 *)SZArrayLdElema(L_2, p0)) = (t57 )p1;
		int32_t L_3 = (__this->f3);
		__this->f3 = ((int32_t)(L_3+1));
		return;
	}
}
 void m11292 (t56 * __this, t29* p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1177, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		return;
	}
}
 bool m11293 (t56 * __this, t57  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t57  >::Invoke(&m11288_MI, __this, p0);
		V_0 = L_0;
		if ((((int32_t)V_0) == ((int32_t)(-1))))
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker1< int32_t >::Invoke(&m11295_MI, __this, V_0);
	}

IL_0013:
	{
		return ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m11294_MI;
 int32_t m11294 (t56 * __this, t2256 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t56_TI));
		m11285(NULL, p0, &m11285_MI);
		V_0 = 0;
		V_1 = 0;
		V_0 = 0;
		goto IL_0028;
	}

IL_000e:
	{
		t2251* L_0 = (__this->f1);
		int32_t L_1 = V_0;
		bool L_2 = (bool)VirtFuncInvoker1< bool, t57  >::Invoke(&m11389_MI, p0, (*(t57 *)(t57 *)SZArrayLdElema(L_0, L_1)));
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		goto IL_0031;
	}

IL_0024:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0028:
	{
		int32_t L_3 = (__this->f2);
		if ((((int32_t)V_0) < ((int32_t)L_3)))
		{
			goto IL_000e;
		}
	}

IL_0031:
	{
		int32_t L_4 = (__this->f2);
		if ((((uint32_t)V_0) != ((uint32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		return 0;
	}

IL_003c:
	{
		int32_t L_5 = (__this->f3);
		__this->f3 = ((int32_t)(L_5+1));
		V_1 = ((int32_t)(V_0+1));
		goto IL_0084;
	}

IL_0050:
	{
		t2251* L_6 = (__this->f1);
		int32_t L_7 = V_1;
		bool L_8 = (bool)VirtFuncInvoker1< bool, t57  >::Invoke(&m11389_MI, p0, (*(t57 *)(t57 *)SZArrayLdElema(L_6, L_7)));
		if (L_8)
		{
			goto IL_0080;
		}
	}
	{
		t2251* L_9 = (__this->f1);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)(L_10+1));
		t2251* L_11 = (__this->f1);
		int32_t L_12 = V_1;
		*((t57 *)(t57 *)SZArrayLdElema(L_9, L_10)) = (t57 )(*(t57 *)(t57 *)SZArrayLdElema(L_11, L_12));
	}

IL_0080:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0084:
	{
		int32_t L_13 = (__this->f2);
		if ((((int32_t)V_1) < ((int32_t)L_13)))
		{
			goto IL_0050;
		}
	}
	{
		if ((((int32_t)((int32_t)(V_1-V_0))) <= ((int32_t)0)))
		{
			goto IL_00a2;
		}
	}
	{
		t2251* L_14 = (__this->f1);
		m5112(NULL, (t20 *)(t20 *)L_14, V_0, ((int32_t)(V_1-V_0)), &m5112_MI);
	}

IL_00a2:
	{
		__this->f2 = V_0;
		return ((int32_t)(V_1-V_0));
	}
}
 void m11295 (t56 * __this, int32_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0018:
	{
		m11289(__this, p0, (-1), &m11289_MI);
		t2251* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		m5112(NULL, (t20 *)(t20 *)L_2, L_3, 1, &m5112_MI);
		int32_t L_4 = (__this->f3);
		__this->f3 = ((int32_t)(L_4+1));
		return;
	}
}
extern MethodInfo m11296_MI;
 void m11296 (t56 * __this, MethodInfo* method){
	{
		t2251* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5171(NULL, (t20 *)(t20 *)L_0, 0, L_1, &m5171_MI);
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
extern MethodInfo m11297_MI;
 void m11297 (t56 * __this, MethodInfo* method){
	{
		t2251* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2263_TI));
		t2263 * L_2 = m11395(NULL, &m11395_MI);
		m20093(NULL, L_0, 0, L_1, L_2, &m20093_MI);
		int32_t L_3 = (__this->f3);
		__this->f3 = ((int32_t)(L_3+1));
		return;
	}
}
extern MethodInfo m1309_MI;
 void m1309 (t56 * __this, t54 * p0, MethodInfo* method){
	{
		t2251* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m20101(NULL, L_0, L_1, p0, &m20101_MI);
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
extern MethodInfo m11298_MI;
 t2251* m11298 (t56 * __this, MethodInfo* method){
	t2251* V_0 = {0};
	{
		int32_t L_0 = (__this->f2);
		V_0 = ((t2251*)SZArrayNew(InitializedTypeInfo(&t2251_TI), L_0));
		t2251* L_1 = (__this->f1);
		int32_t L_2 = (__this->f2);
		m5951(NULL, (t20 *)(t20 *)L_1, (t20 *)(t20 *)V_0, L_2, &m5951_MI);
		return V_0;
	}
}
extern MethodInfo m11299_MI;
 void m11299 (t56 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		m11301(__this, L_0, &m11301_MI);
		return;
	}
}
 int32_t m11300 (t56 * __this, MethodInfo* method){
	{
		t2251* L_0 = (__this->f1);
		return (((int32_t)(((t20 *)L_0)->max_length)));
	}
}
 void m11301 (t56 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) >= ((uint32_t)L_0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m4198(L_1, &m4198_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t2251** L_2 = &(__this->f1);
		m20089(NULL, L_2, p0, &m20089_MI);
		return;
	}
}
extern MethodInfo m1400_MI;
 int32_t m1400 (t56 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
 t57  m1399 (t56 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		t2251* L_2 = (__this->f1);
		int32_t L_3 = p0;
		return (*(t57 *)(t57 *)SZArrayLdElema(L_2, L_3));
	}
}
 void m11302 (t56 * __this, int32_t p0, t57  p1, MethodInfo* method){
	{
		m11290(__this, p0, &m11290_MI);
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) != ((uint32_t)L_0)))
		{
			goto IL_001b;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001b:
	{
		t2251* L_2 = (__this->f1);
		*((t57 *)(t57 *)SZArrayLdElema(L_2, p0)) = (t57 )p1;
		return;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t44_0_0_32849;
FieldInfo t56_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t56_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2251_0_0_1;
FieldInfo t56_f1_FieldInfo = 
{
	"_items", &t2251_0_0_1, &t56_TI, offsetof(t56, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t56_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t56_TI, offsetof(t56, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t56_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t56_TI, offsetof(t56, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2251_0_0_49;
FieldInfo t56_f4_FieldInfo = 
{
	"EmptyArray", &t2251_0_0_49, &t56_TI, offsetof(t56_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t56_FIs[] =
{
	&t56_f0_FieldInfo,
	&t56_f1_FieldInfo,
	&t56_f2_FieldInfo,
	&t56_f3_FieldInfo,
	&t56_f4_FieldInfo,
	NULL
};
static const int32_t t56_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t56_f0_DefaultValue = 
{
	&t56_f0_FieldInfo, { (char*)&t56_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t56_FDVs[] = 
{
	&t56_f0_DefaultValue,
	NULL
};
static PropertyInfo t56____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t56_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11270_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t56____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t56_TI, "System.Collections.ICollection.IsSynchronized", &m11271_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t56____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t56_TI, "System.Collections.ICollection.SyncRoot", &m11272_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t56____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t56_TI, "System.Collections.IList.IsFixedSize", &m11273_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t56____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t56_TI, "System.Collections.IList.IsReadOnly", &m11274_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t56____System_Collections_IList_Item_PropertyInfo = 
{
	&t56_TI, "System.Collections.IList.Item", &m11275_MI, &m11276_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t56____Capacity_PropertyInfo = 
{
	&t56_TI, "Capacity", &m11300_MI, &m11301_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t56____Count_PropertyInfo = 
{
	&t56_TI, "Count", &m1400_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t56____Item_PropertyInfo = 
{
	&t56_TI, "Item", &m1399_MI, &m11302_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t56_PIs[] =
{
	&t56____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t56____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t56____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t56____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t56____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t56____System_Collections_IList_Item_PropertyInfo,
	&t56____Capacity_PropertyInfo,
	&t56____Count_PropertyInfo,
	&t56____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1397_GM;
MethodInfo m1397_MI = 
{
	".ctor", (methodPointerType)&m1397, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1397_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t56_m11260_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11260_GM;
MethodInfo m11260_MI = 
{
	".ctor", (methodPointerType)&m11260, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t56_m11260_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11260_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11261_GM;
MethodInfo m11261_MI = 
{
	".cctor", (methodPointerType)&m11261, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11261_GM};
extern Il2CppType t2252_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11262_GM;
MethodInfo m11262_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m11262, &t56_TI, &t2252_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11262_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t56_m11263_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11263_GM;
MethodInfo m11263_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m11263, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t56_m11263_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11263_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11264_GM;
MethodInfo m11264_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m11264, &t56_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11264_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t56_m11265_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11265_GM;
MethodInfo m11265_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m11265, &t56_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t56_m11265_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11265_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t56_m11266_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11266_GM;
MethodInfo m11266_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m11266, &t56_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t56_m11266_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11266_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t56_m11267_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11267_GM;
MethodInfo m11267_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m11267, &t56_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t56_m11267_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11267_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t56_m11268_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11268_GM;
MethodInfo m11268_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m11268, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t56_m11268_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11268_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t56_m11269_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11269_GM;
MethodInfo m11269_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m11269, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t56_m11269_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11269_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11270_GM;
MethodInfo m11270_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m11270, &t56_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11270_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11271_GM;
MethodInfo m11271_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m11271, &t56_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11271_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11272_GM;
MethodInfo m11272_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m11272, &t56_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11272_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11273_GM;
MethodInfo m11273_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m11273, &t56_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11273_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11274_GM;
MethodInfo m11274_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m11274, &t56_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11274_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t56_m11275_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11275_GM;
MethodInfo m11275_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m11275, &t56_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t56_m11275_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11275_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t56_m11276_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11276_GM;
MethodInfo m11276_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m11276, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t56_m11276_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11276_GM};
extern Il2CppType t57_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t56_m1478_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1478_GM;
MethodInfo m1478_MI = 
{
	"Add", (methodPointerType)&m1478, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t57, t56_m1478_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1478_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t56_m11277_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11277_GM;
MethodInfo m11277_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m11277, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t56_m11277_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11277_GM};
extern Il2CppType t2253_0_0_0;
extern Il2CppType t2253_0_0_0;
static ParameterInfo t56_m11278_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2253_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11278_GM;
MethodInfo m11278_MI = 
{
	"AddCollection", (methodPointerType)&m11278, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t56_m11278_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11278_GM};
extern Il2CppType t2254_0_0_0;
extern Il2CppType t2254_0_0_0;
static ParameterInfo t56_m11279_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2254_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11279_GM;
MethodInfo m11279_MI = 
{
	"AddEnumerable", (methodPointerType)&m11279, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t56_m11279_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11279_GM};
extern Il2CppType t2254_0_0_0;
static ParameterInfo t56_m11280_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2254_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11280_GM;
MethodInfo m11280_MI = 
{
	"AddRange", (methodPointerType)&m11280, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t56_m11280_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11280_GM};
extern Il2CppType t2255_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11281_GM;
MethodInfo m11281_MI = 
{
	"AsReadOnly", (methodPointerType)&m11281, &t56_TI, &t2255_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11281_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1305_GM;
MethodInfo m1305_MI = 
{
	"Clear", (methodPointerType)&m1305, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1305_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t56_m11282_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11282_GM;
MethodInfo m11282_MI = 
{
	"Contains", (methodPointerType)&m11282, &t56_TI, &t40_0_0_0, RuntimeInvoker_t40_t57, t56_m11282_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11282_GM};
extern Il2CppType t2251_0_0_0;
extern Il2CppType t2251_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t56_m11283_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2251_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11283_GM;
MethodInfo m11283_MI = 
{
	"CopyTo", (methodPointerType)&m11283, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t56_m11283_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11283_GM};
extern Il2CppType t2256_0_0_0;
extern Il2CppType t2256_0_0_0;
static ParameterInfo t56_m11284_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2256_0_0_0},
};
extern Il2CppType t57_0_0_0;
extern void* RuntimeInvoker_t57_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11284_GM;
MethodInfo m11284_MI = 
{
	"Find", (methodPointerType)&m11284, &t56_TI, &t57_0_0_0, RuntimeInvoker_t57_t29, t56_m11284_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11284_GM};
extern Il2CppType t2256_0_0_0;
static ParameterInfo t56_m11285_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2256_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11285_GM;
MethodInfo m11285_MI = 
{
	"CheckMatch", (methodPointerType)&m11285, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t56_m11285_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11285_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2256_0_0_0;
static ParameterInfo t56_m11286_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2256_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11286_GM;
MethodInfo m11286_MI = 
{
	"GetIndex", (methodPointerType)&m11286, &t56_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t56_m11286_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11286_GM};
extern Il2CppType t2257_0_0_0;
extern void* RuntimeInvoker_t2257 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11287_GM;
MethodInfo m11287_MI = 
{
	"GetEnumerator", (methodPointerType)&m11287, &t56_TI, &t2257_0_0_0, RuntimeInvoker_t2257, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11287_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t56_m11288_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11288_GM;
MethodInfo m11288_MI = 
{
	"IndexOf", (methodPointerType)&m11288, &t56_TI, &t44_0_0_0, RuntimeInvoker_t44_t57, t56_m11288_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11288_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t56_m11289_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11289_GM;
MethodInfo m11289_MI = 
{
	"Shift", (methodPointerType)&m11289, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t56_m11289_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11289_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t56_m11290_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11290_GM;
MethodInfo m11290_MI = 
{
	"CheckIndex", (methodPointerType)&m11290, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t56_m11290_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11290_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t56_m11291_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11291_GM;
MethodInfo m11291_MI = 
{
	"Insert", (methodPointerType)&m11291, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t57, t56_m11291_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11291_GM};
extern Il2CppType t2254_0_0_0;
static ParameterInfo t56_m11292_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2254_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11292_GM;
MethodInfo m11292_MI = 
{
	"CheckCollection", (methodPointerType)&m11292, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t56_m11292_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11292_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t56_m11293_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11293_GM;
MethodInfo m11293_MI = 
{
	"Remove", (methodPointerType)&m11293, &t56_TI, &t40_0_0_0, RuntimeInvoker_t40_t57, t56_m11293_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11293_GM};
extern Il2CppType t2256_0_0_0;
static ParameterInfo t56_m11294_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2256_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11294_GM;
MethodInfo m11294_MI = 
{
	"RemoveAll", (methodPointerType)&m11294, &t56_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t56_m11294_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11294_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t56_m11295_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11295_GM;
MethodInfo m11295_MI = 
{
	"RemoveAt", (methodPointerType)&m11295, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t56_m11295_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11295_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11296_GM;
MethodInfo m11296_MI = 
{
	"Reverse", (methodPointerType)&m11296, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11296_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11297_GM;
MethodInfo m11297_MI = 
{
	"Sort", (methodPointerType)&m11297, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11297_GM};
extern Il2CppType t54_0_0_0;
extern Il2CppType t54_0_0_0;
static ParameterInfo t56_m1309_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t54_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1309_GM;
MethodInfo m1309_MI = 
{
	"Sort", (methodPointerType)&m1309, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t56_m1309_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1309_GM};
extern Il2CppType t2251_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11298_GM;
MethodInfo m11298_MI = 
{
	"ToArray", (methodPointerType)&m11298, &t56_TI, &t2251_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11298_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11299_GM;
MethodInfo m11299_MI = 
{
	"TrimExcess", (methodPointerType)&m11299, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11299_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11300_GM;
MethodInfo m11300_MI = 
{
	"get_Capacity", (methodPointerType)&m11300, &t56_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11300_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t56_m11301_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11301_GM;
MethodInfo m11301_MI = 
{
	"set_Capacity", (methodPointerType)&m11301, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t56_m11301_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11301_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1400_GM;
MethodInfo m1400_MI = 
{
	"get_Count", (methodPointerType)&m1400, &t56_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1400_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t56_m1399_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t57_0_0_0;
extern void* RuntimeInvoker_t57_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1399_GM;
MethodInfo m1399_MI = 
{
	"get_Item", (methodPointerType)&m1399, &t56_TI, &t57_0_0_0, RuntimeInvoker_t57_t44, t56_m1399_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1399_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t56_m11302_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11302_GM;
MethodInfo m11302_MI = 
{
	"set_Item", (methodPointerType)&m11302, &t56_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t57, t56_m11302_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11302_GM};
static MethodInfo* t56_MIs[] =
{
	&m1397_MI,
	&m11260_MI,
	&m11261_MI,
	&m11262_MI,
	&m11263_MI,
	&m11264_MI,
	&m11265_MI,
	&m11266_MI,
	&m11267_MI,
	&m11268_MI,
	&m11269_MI,
	&m11270_MI,
	&m11271_MI,
	&m11272_MI,
	&m11273_MI,
	&m11274_MI,
	&m11275_MI,
	&m11276_MI,
	&m1478_MI,
	&m11277_MI,
	&m11278_MI,
	&m11279_MI,
	&m11280_MI,
	&m11281_MI,
	&m1305_MI,
	&m11282_MI,
	&m11283_MI,
	&m11284_MI,
	&m11285_MI,
	&m11286_MI,
	&m11287_MI,
	&m11288_MI,
	&m11289_MI,
	&m11290_MI,
	&m11291_MI,
	&m11292_MI,
	&m11293_MI,
	&m11294_MI,
	&m11295_MI,
	&m11296_MI,
	&m11297_MI,
	&m1309_MI,
	&m11298_MI,
	&m11299_MI,
	&m11300_MI,
	&m11301_MI,
	&m1400_MI,
	&m1399_MI,
	&m11302_MI,
	NULL
};
static MethodInfo* t56_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11264_MI,
	&m1400_MI,
	&m11271_MI,
	&m11272_MI,
	&m11263_MI,
	&m11273_MI,
	&m11274_MI,
	&m11275_MI,
	&m11276_MI,
	&m11265_MI,
	&m1305_MI,
	&m11266_MI,
	&m11267_MI,
	&m11268_MI,
	&m11269_MI,
	&m11295_MI,
	&m1400_MI,
	&m11270_MI,
	&m1478_MI,
	&m1305_MI,
	&m11282_MI,
	&m11283_MI,
	&m11293_MI,
	&m11262_MI,
	&m11288_MI,
	&m11291_MI,
	&m11295_MI,
	&m1399_MI,
	&m11302_MI,
};
extern TypeInfo t2259_TI;
static TypeInfo* t56_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2253_TI,
	&t2254_TI,
	&t2259_TI,
};
static Il2CppInterfaceOffsetPair t56_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2253_TI, 20},
	{ &t2254_TI, 27},
	{ &t2259_TI, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t56_0_0_0;
extern Il2CppType t56_1_0_0;
struct t56;
extern Il2CppGenericClass t56_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t56_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t56_MIs, t56_PIs, t56_FIs, NULL, &t29_TI, NULL, NULL, &t56_TI, t56_ITIs, t56_VT, &t1261__CustomAttributeCache, &t56_TI, &t56_0_0_0, &t56_1_0_0, t56_IOs, &t56_GC, NULL, t56_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t56), 0, -1, sizeof(t56_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.RaycastResult>
static PropertyInfo t2253____Count_PropertyInfo = 
{
	&t2253_TI, "Count", &m26921_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26925_MI;
static PropertyInfo t2253____IsReadOnly_PropertyInfo = 
{
	&t2253_TI, "IsReadOnly", &m26925_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2253_PIs[] =
{
	&t2253____Count_PropertyInfo,
	&t2253____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26921_GM;
MethodInfo m26921_MI = 
{
	"get_Count", NULL, &t2253_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26921_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26925_GM;
MethodInfo m26925_MI = 
{
	"get_IsReadOnly", NULL, &t2253_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26925_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2253_m26926_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26926_GM;
MethodInfo m26926_MI = 
{
	"Add", NULL, &t2253_TI, &t21_0_0_0, RuntimeInvoker_t21_t57, t2253_m26926_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26926_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26927_GM;
MethodInfo m26927_MI = 
{
	"Clear", NULL, &t2253_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26927_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2253_m26928_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26928_GM;
MethodInfo m26928_MI = 
{
	"Contains", NULL, &t2253_TI, &t40_0_0_0, RuntimeInvoker_t40_t57, t2253_m26928_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26928_GM};
extern Il2CppType t2251_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2253_m26922_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2251_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26922_GM;
MethodInfo m26922_MI = 
{
	"CopyTo", NULL, &t2253_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2253_m26922_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26922_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2253_m26929_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26929_GM;
MethodInfo m26929_MI = 
{
	"Remove", NULL, &t2253_TI, &t40_0_0_0, RuntimeInvoker_t40_t57, t2253_m26929_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26929_GM};
static MethodInfo* t2253_MIs[] =
{
	&m26921_MI,
	&m26925_MI,
	&m26926_MI,
	&m26927_MI,
	&m26928_MI,
	&m26922_MI,
	&m26929_MI,
	NULL
};
static TypeInfo* t2253_ITIs[] = 
{
	&t603_TI,
	&t2254_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2253_1_0_0;
struct t2253;
extern Il2CppGenericClass t2253_GC;
TypeInfo t2253_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2253_MIs, t2253_PIs, NULL, NULL, NULL, NULL, NULL, &t2253_TI, t2253_ITIs, NULL, &EmptyCustomAttributesCache, &t2253_TI, &t2253_0_0_0, &t2253_1_0_0, NULL, &t2253_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t2252_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26923_GM;
MethodInfo m26923_MI = 
{
	"GetEnumerator", NULL, &t2254_TI, &t2252_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26923_GM};
static MethodInfo* t2254_MIs[] =
{
	&m26923_MI,
	NULL
};
static TypeInfo* t2254_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2254_1_0_0;
struct t2254;
extern Il2CppGenericClass t2254_GC;
TypeInfo t2254_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2254_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2254_TI, t2254_ITIs, NULL, &EmptyCustomAttributesCache, &t2254_TI, &t2254_0_0_0, &t2254_1_0_0, NULL, &t2254_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
static PropertyInfo t2252____Current_PropertyInfo = 
{
	&t2252_TI, "Current", &m26924_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2252_PIs[] =
{
	&t2252____Current_PropertyInfo,
	NULL
};
extern Il2CppType t57_0_0_0;
extern void* RuntimeInvoker_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26924_GM;
MethodInfo m26924_MI = 
{
	"get_Current", NULL, &t2252_TI, &t57_0_0_0, RuntimeInvoker_t57, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26924_GM};
static MethodInfo* t2252_MIs[] =
{
	&m26924_MI,
	NULL
};
static TypeInfo* t2252_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2252_0_0_0;
extern Il2CppType t2252_1_0_0;
struct t2252;
extern Il2CppGenericClass t2252_GC;
TypeInfo t2252_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2252_MIs, t2252_PIs, NULL, NULL, NULL, NULL, NULL, &t2252_TI, t2252_ITIs, NULL, &EmptyCustomAttributesCache, &t2252_TI, &t2252_0_0_0, &t2252_1_0_0, NULL, &t2252_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2258.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2258_TI;
#include "t2258MD.h"

extern MethodInfo m11307_MI;
extern MethodInfo m20078_MI;
struct t20;
 t57  m20078 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m11303_MI;
 void m11303 (t2258 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m11304_MI;
 t29 * m11304 (t2258 * __this, MethodInfo* method){
	{
		t57  L_0 = m11307(__this, &m11307_MI);
		t57  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t57_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m11305_MI;
 void m11305 (t2258 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m11306_MI;
 bool m11306 (t2258 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t57  m11307 (t2258 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t57  L_8 = m20078(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20078_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t20_0_0_1;
FieldInfo t2258_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2258_TI, offsetof(t2258, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2258_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2258_TI, offsetof(t2258, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2258_FIs[] =
{
	&t2258_f0_FieldInfo,
	&t2258_f1_FieldInfo,
	NULL
};
static PropertyInfo t2258____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2258_TI, "System.Collections.IEnumerator.Current", &m11304_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2258____Current_PropertyInfo = 
{
	&t2258_TI, "Current", &m11307_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2258_PIs[] =
{
	&t2258____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2258____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2258_m11303_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11303_GM;
MethodInfo m11303_MI = 
{
	".ctor", (methodPointerType)&m11303, &t2258_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2258_m11303_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11303_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11304_GM;
MethodInfo m11304_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m11304, &t2258_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11304_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11305_GM;
MethodInfo m11305_MI = 
{
	"Dispose", (methodPointerType)&m11305, &t2258_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11305_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11306_GM;
MethodInfo m11306_MI = 
{
	"MoveNext", (methodPointerType)&m11306, &t2258_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11306_GM};
extern Il2CppType t57_0_0_0;
extern void* RuntimeInvoker_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11307_GM;
MethodInfo m11307_MI = 
{
	"get_Current", (methodPointerType)&m11307, &t2258_TI, &t57_0_0_0, RuntimeInvoker_t57, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11307_GM};
static MethodInfo* t2258_MIs[] =
{
	&m11303_MI,
	&m11304_MI,
	&m11305_MI,
	&m11306_MI,
	&m11307_MI,
	NULL
};
static MethodInfo* t2258_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11304_MI,
	&m11306_MI,
	&m11305_MI,
	&m11307_MI,
};
static TypeInfo* t2258_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2252_TI,
};
static Il2CppInterfaceOffsetPair t2258_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2252_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2258_0_0_0;
extern Il2CppType t2258_1_0_0;
extern Il2CppGenericClass t2258_GC;
TypeInfo t2258_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2258_MIs, t2258_PIs, t2258_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2258_TI, t2258_ITIs, t2258_VT, &EmptyCustomAttributesCache, &t2258_TI, &t2258_0_0_0, &t2258_1_0_0, t2258_IOs, &t2258_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2258)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.RaycastResult>
extern MethodInfo m26930_MI;
extern MethodInfo m26931_MI;
static PropertyInfo t2259____Item_PropertyInfo = 
{
	&t2259_TI, "Item", &m26930_MI, &m26931_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2259_PIs[] =
{
	&t2259____Item_PropertyInfo,
	NULL
};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2259_m26932_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26932_GM;
MethodInfo m26932_MI = 
{
	"IndexOf", NULL, &t2259_TI, &t44_0_0_0, RuntimeInvoker_t44_t57, t2259_m26932_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26932_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t2259_m26933_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26933_GM;
MethodInfo m26933_MI = 
{
	"Insert", NULL, &t2259_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t57, t2259_m26933_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26933_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2259_m26934_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26934_GM;
MethodInfo m26934_MI = 
{
	"RemoveAt", NULL, &t2259_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2259_m26934_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26934_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2259_m26930_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t57_0_0_0;
extern void* RuntimeInvoker_t57_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26930_GM;
MethodInfo m26930_MI = 
{
	"get_Item", NULL, &t2259_TI, &t57_0_0_0, RuntimeInvoker_t57_t44, t2259_m26930_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26930_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t2259_m26931_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26931_GM;
MethodInfo m26931_MI = 
{
	"set_Item", NULL, &t2259_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t57, t2259_m26931_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26931_GM};
static MethodInfo* t2259_MIs[] =
{
	&m26932_MI,
	&m26933_MI,
	&m26934_MI,
	&m26930_MI,
	&m26931_MI,
	NULL
};
static TypeInfo* t2259_ITIs[] = 
{
	&t603_TI,
	&t2253_TI,
	&t2254_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2259_0_0_0;
extern Il2CppType t2259_1_0_0;
struct t2259;
extern Il2CppGenericClass t2259_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2259_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2259_MIs, t2259_PIs, NULL, NULL, NULL, NULL, NULL, &t2259_TI, t2259_ITIs, NULL, &t1908__CustomAttributeCache, &t2259_TI, &t2259_0_0_0, &t2259_1_0_0, NULL, &t2259_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m11311_MI;


 void m11308 (t2257 * __this, t56 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f3);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m11309_MI;
 t29 * m11309 (t2257 * __this, MethodInfo* method){
	{
		m11311(__this, &m11311_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_1, &m3974_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		t57  L_2 = (__this->f3);
		t57  L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t57_TI), &L_3);
		return L_4;
	}
}
extern MethodInfo m11310_MI;
 void m11310 (t2257 * __this, MethodInfo* method){
	{
		__this->f0 = (t56 *)NULL;
		return;
	}
}
 void m11311 (t2257 * __this, MethodInfo* method){
	{
		t56 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		t2257  L_1 = (*(t2257 *)__this);
		t29 * L_2 = Box(InitializedTypeInfo(&t2257_TI), &L_1);
		t42 * L_3 = m1430(L_2, &m1430_MI);
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_3);
		t1101 * L_5 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_5, L_4, &m5150_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0023:
	{
		int32_t L_6 = (__this->f2);
		t56 * L_7 = (__this->f0);
		int32_t L_8 = (L_7->f3);
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0041;
		}
	}
	{
		t914 * L_9 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_9, (t7*) &_stringLiteral1178, &m3964_MI);
		il2cpp_codegen_raise_exception(L_9);
	}

IL_0041:
	{
		return;
	}
}
extern MethodInfo m11312_MI;
 bool m11312 (t2257 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	{
		m11311(__this, &m11311_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		int32_t L_1 = (__this->f1);
		t56 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f2);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_004d;
		}
	}
	{
		t56 * L_4 = (__this->f0);
		t2251* L_5 = (L_4->f1);
		int32_t L_6 = (__this->f1);
		int32_t L_7 = L_6;
		V_0 = L_7;
		__this->f1 = ((int32_t)(L_7+1));
		int32_t L_8 = V_0;
		__this->f3 = (*(t57 *)(t57 *)SZArrayLdElema(L_5, L_8));
		return 1;
	}

IL_004d:
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m11313_MI;
 t57  m11313 (t2257 * __this, MethodInfo* method){
	{
		t57  L_0 = (__this->f3);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t56_0_0_1;
FieldInfo t2257_f0_FieldInfo = 
{
	"l", &t56_0_0_1, &t2257_TI, offsetof(t2257, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2257_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2257_TI, offsetof(t2257, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2257_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2257_TI, offsetof(t2257, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t57_0_0_1;
FieldInfo t2257_f3_FieldInfo = 
{
	"current", &t57_0_0_1, &t2257_TI, offsetof(t2257, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2257_FIs[] =
{
	&t2257_f0_FieldInfo,
	&t2257_f1_FieldInfo,
	&t2257_f2_FieldInfo,
	&t2257_f3_FieldInfo,
	NULL
};
static PropertyInfo t2257____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2257_TI, "System.Collections.IEnumerator.Current", &m11309_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2257____Current_PropertyInfo = 
{
	&t2257_TI, "Current", &m11313_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2257_PIs[] =
{
	&t2257____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2257____Current_PropertyInfo,
	NULL
};
extern Il2CppType t56_0_0_0;
static ParameterInfo t2257_m11308_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t56_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11308_GM;
MethodInfo m11308_MI = 
{
	".ctor", (methodPointerType)&m11308, &t2257_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2257_m11308_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11308_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11309_GM;
MethodInfo m11309_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m11309, &t2257_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11309_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11310_GM;
MethodInfo m11310_MI = 
{
	"Dispose", (methodPointerType)&m11310, &t2257_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11310_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11311_GM;
MethodInfo m11311_MI = 
{
	"VerifyState", (methodPointerType)&m11311, &t2257_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11311_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11312_GM;
MethodInfo m11312_MI = 
{
	"MoveNext", (methodPointerType)&m11312, &t2257_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11312_GM};
extern Il2CppType t57_0_0_0;
extern void* RuntimeInvoker_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11313_GM;
MethodInfo m11313_MI = 
{
	"get_Current", (methodPointerType)&m11313, &t2257_TI, &t57_0_0_0, RuntimeInvoker_t57, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11313_GM};
static MethodInfo* t2257_MIs[] =
{
	&m11308_MI,
	&m11309_MI,
	&m11310_MI,
	&m11311_MI,
	&m11312_MI,
	&m11313_MI,
	NULL
};
static MethodInfo* t2257_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11309_MI,
	&m11312_MI,
	&m11310_MI,
	&m11313_MI,
};
static TypeInfo* t2257_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2252_TI,
};
static Il2CppInterfaceOffsetPair t2257_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2252_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2257_0_0_0;
extern Il2CppType t2257_1_0_0;
extern Il2CppGenericClass t2257_GC;
TypeInfo t2257_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2257_MIs, t2257_PIs, t2257_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2257_TI, t2257_ITIs, t2257_VT, &EmptyCustomAttributesCache, &t2257_TI, &t2257_0_0_0, &t2257_1_0_0, t2257_IOs, &t2257_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2257)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t2260MD.h"
extern MethodInfo m11343_MI;
extern MethodInfo m11375_MI;
extern MethodInfo m26928_MI;
extern MethodInfo m26932_MI;


 void m11314 (t2255 * __this, t29* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1179, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m11315_MI;
 void m11315 (t2255 * __this, t57  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m11316_MI;
 void m11316 (t2255 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m11317_MI;
 void m11317 (t2255 * __this, int32_t p0, t57  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m11318_MI;
 bool m11318 (t2255 * __this, t57  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m11319_MI;
 void m11319 (t2255 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m11320_MI;
 t57  m11320 (t2255 * __this, int32_t p0, MethodInfo* method){
	{
		t57  L_0 = (t57 )VirtFuncInvoker1< t57 , int32_t >::Invoke(&m11343_MI, __this, p0);
		return L_0;
	}
}
extern MethodInfo m11321_MI;
 void m11321 (t2255 * __this, int32_t p0, t57  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m11322_MI;
 bool m11322 (t2255 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m11323_MI;
 void m11323 (t2255 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t20 *, int32_t >::Invoke(&m3960_MI, ((t29 *)Castclass(L_0, InitializedTypeInfo(&t674_TI))), p0, p1);
		return;
	}
}
extern MethodInfo m11324_MI;
 t29 * m11324 (t2255 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m4154_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m11325_MI;
 int32_t m11325 (t2255 * __this, t29 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m11326_MI;
 void m11326 (t2255 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m11327_MI;
 bool m11327 (t2255 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m11375(NULL, p0, &m11375_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, t57  >::Invoke(&m26928_MI, L_1, ((*(t57 *)((t57 *)UnBox (p0, InitializedTypeInfo(&t57_TI))))));
		return L_2;
	}

IL_001a:
	{
		return 0;
	}
}
extern MethodInfo m11328_MI;
 int32_t m11328 (t2255 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m11375(NULL, p0, &m11375_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, t57  >::Invoke(&m26932_MI, L_1, ((*(t57 *)((t57 *)UnBox (p0, InitializedTypeInfo(&t57_TI))))));
		return L_2;
	}

IL_001a:
	{
		return (-1);
	}
}
extern MethodInfo m11329_MI;
 void m11329 (t2255 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m11330_MI;
 void m11330 (t2255 * __this, t29 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m11331_MI;
 void m11331 (t2255 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m11332_MI;
 bool m11332 (t2255 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m11333_MI;
 t29 * m11333 (t2255 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m11334_MI;
 bool m11334 (t2255 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m11335_MI;
 bool m11335 (t2255 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m11336_MI;
 t29 * m11336 (t2255 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t57  L_1 = (t57 )InterfaceFuncInvoker1< t57 , int32_t >::Invoke(&m26930_MI, L_0, p0);
		t57  L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t57_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m11337_MI;
 void m11337 (t2255 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m11338_MI;
 bool m11338 (t2255 * __this, t57  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker1< bool, t57  >::Invoke(&m26928_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m11339_MI;
 void m11339 (t2255 * __this, t2251* p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t2251*, int32_t >::Invoke(&m26922_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m11340_MI;
 t29* m11340 (t2255 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m26923_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m11341_MI;
 int32_t m11341 (t2255 * __this, t57  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker1< int32_t, t57  >::Invoke(&m26932_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m11342_MI;
 int32_t m11342 (t2255 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m26921_MI, L_0);
		return L_1;
	}
}
 t57  m11343 (t2255 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t57  L_1 = (t57 )InterfaceFuncInvoker1< t57 , int32_t >::Invoke(&m26930_MI, L_0, p0);
		return L_1;
	}
}
// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t2259_0_0_1;
FieldInfo t2255_f0_FieldInfo = 
{
	"list", &t2259_0_0_1, &t2255_TI, offsetof(t2255, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2255_FIs[] =
{
	&t2255_f0_FieldInfo,
	NULL
};
static PropertyInfo t2255____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2255_TI, "System.Collections.Generic.IList<T>.Item", &m11320_MI, &m11321_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2255____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2255_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11322_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2255____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2255_TI, "System.Collections.ICollection.IsSynchronized", &m11332_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2255____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2255_TI, "System.Collections.ICollection.SyncRoot", &m11333_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2255____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2255_TI, "System.Collections.IList.IsFixedSize", &m11334_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2255____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2255_TI, "System.Collections.IList.IsReadOnly", &m11335_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2255____System_Collections_IList_Item_PropertyInfo = 
{
	&t2255_TI, "System.Collections.IList.Item", &m11336_MI, &m11337_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2255____Count_PropertyInfo = 
{
	&t2255_TI, "Count", &m11342_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2255____Item_PropertyInfo = 
{
	&t2255_TI, "Item", &m11343_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2255_PIs[] =
{
	&t2255____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2255____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2255____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2255____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2255____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2255____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2255____System_Collections_IList_Item_PropertyInfo,
	&t2255____Count_PropertyInfo,
	&t2255____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2259_0_0_0;
static ParameterInfo t2255_m11314_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2259_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11314_GM;
MethodInfo m11314_MI = 
{
	".ctor", (methodPointerType)&m11314, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2255_m11314_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11314_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2255_m11315_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11315_GM;
MethodInfo m11315_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m11315, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21_t57, t2255_m11315_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11315_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11316_GM;
MethodInfo m11316_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m11316, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11316_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t2255_m11317_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11317_GM;
MethodInfo m11317_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m11317, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t57, t2255_m11317_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11317_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2255_m11318_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11318_GM;
MethodInfo m11318_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m11318, &t2255_TI, &t40_0_0_0, RuntimeInvoker_t40_t57, t2255_m11318_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11318_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2255_m11319_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11319_GM;
MethodInfo m11319_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m11319, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2255_m11319_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11319_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2255_m11320_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t57_0_0_0;
extern void* RuntimeInvoker_t57_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11320_GM;
MethodInfo m11320_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m11320, &t2255_TI, &t57_0_0_0, RuntimeInvoker_t57_t44, t2255_m11320_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11320_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t2255_m11321_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11321_GM;
MethodInfo m11321_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m11321, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t57, t2255_m11321_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11321_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11322_GM;
MethodInfo m11322_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m11322, &t2255_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11322_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2255_m11323_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11323_GM;
MethodInfo m11323_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m11323, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2255_m11323_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11323_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11324_GM;
MethodInfo m11324_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m11324, &t2255_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11324_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2255_m11325_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11325_GM;
MethodInfo m11325_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m11325, &t2255_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2255_m11325_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11325_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11326_GM;
MethodInfo m11326_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m11326, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11326_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2255_m11327_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11327_GM;
MethodInfo m11327_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m11327, &t2255_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2255_m11327_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11327_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2255_m11328_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11328_GM;
MethodInfo m11328_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m11328, &t2255_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2255_m11328_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11328_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2255_m11329_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11329_GM;
MethodInfo m11329_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m11329, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2255_m11329_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11329_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2255_m11330_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11330_GM;
MethodInfo m11330_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m11330, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2255_m11330_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11330_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2255_m11331_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11331_GM;
MethodInfo m11331_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m11331, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2255_m11331_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11331_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11332_GM;
MethodInfo m11332_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m11332, &t2255_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11332_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11333_GM;
MethodInfo m11333_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m11333, &t2255_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11333_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11334_GM;
MethodInfo m11334_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m11334, &t2255_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11334_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11335_GM;
MethodInfo m11335_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m11335, &t2255_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11335_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2255_m11336_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11336_GM;
MethodInfo m11336_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m11336, &t2255_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2255_m11336_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11336_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2255_m11337_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11337_GM;
MethodInfo m11337_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m11337, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2255_m11337_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11337_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2255_m11338_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11338_GM;
MethodInfo m11338_MI = 
{
	"Contains", (methodPointerType)&m11338, &t2255_TI, &t40_0_0_0, RuntimeInvoker_t40_t57, t2255_m11338_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11338_GM};
extern Il2CppType t2251_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2255_m11339_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2251_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11339_GM;
MethodInfo m11339_MI = 
{
	"CopyTo", (methodPointerType)&m11339, &t2255_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2255_m11339_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11339_GM};
extern Il2CppType t2252_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11340_GM;
MethodInfo m11340_MI = 
{
	"GetEnumerator", (methodPointerType)&m11340, &t2255_TI, &t2252_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11340_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2255_m11341_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11341_GM;
MethodInfo m11341_MI = 
{
	"IndexOf", (methodPointerType)&m11341, &t2255_TI, &t44_0_0_0, RuntimeInvoker_t44_t57, t2255_m11341_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11341_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11342_GM;
MethodInfo m11342_MI = 
{
	"get_Count", (methodPointerType)&m11342, &t2255_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11342_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2255_m11343_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t57_0_0_0;
extern void* RuntimeInvoker_t57_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11343_GM;
MethodInfo m11343_MI = 
{
	"get_Item", (methodPointerType)&m11343, &t2255_TI, &t57_0_0_0, RuntimeInvoker_t57_t44, t2255_m11343_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11343_GM};
static MethodInfo* t2255_MIs[] =
{
	&m11314_MI,
	&m11315_MI,
	&m11316_MI,
	&m11317_MI,
	&m11318_MI,
	&m11319_MI,
	&m11320_MI,
	&m11321_MI,
	&m11322_MI,
	&m11323_MI,
	&m11324_MI,
	&m11325_MI,
	&m11326_MI,
	&m11327_MI,
	&m11328_MI,
	&m11329_MI,
	&m11330_MI,
	&m11331_MI,
	&m11332_MI,
	&m11333_MI,
	&m11334_MI,
	&m11335_MI,
	&m11336_MI,
	&m11337_MI,
	&m11338_MI,
	&m11339_MI,
	&m11340_MI,
	&m11341_MI,
	&m11342_MI,
	&m11343_MI,
	NULL
};
static MethodInfo* t2255_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11324_MI,
	&m11342_MI,
	&m11332_MI,
	&m11333_MI,
	&m11323_MI,
	&m11334_MI,
	&m11335_MI,
	&m11336_MI,
	&m11337_MI,
	&m11325_MI,
	&m11326_MI,
	&m11327_MI,
	&m11328_MI,
	&m11329_MI,
	&m11330_MI,
	&m11331_MI,
	&m11342_MI,
	&m11322_MI,
	&m11315_MI,
	&m11316_MI,
	&m11338_MI,
	&m11339_MI,
	&m11318_MI,
	&m11341_MI,
	&m11317_MI,
	&m11319_MI,
	&m11320_MI,
	&m11321_MI,
	&m11340_MI,
	&m11343_MI,
};
static TypeInfo* t2255_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2253_TI,
	&t2259_TI,
	&t2254_TI,
};
static Il2CppInterfaceOffsetPair t2255_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2253_TI, 20},
	{ &t2259_TI, 27},
	{ &t2254_TI, 32},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2255_0_0_0;
extern Il2CppType t2255_1_0_0;
struct t2255;
extern Il2CppGenericClass t2255_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2255_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2255_MIs, t2255_PIs, t2255_FIs, NULL, &t29_TI, NULL, NULL, &t2255_TI, t2255_ITIs, t2255_VT, &t1263__CustomAttributeCache, &t2255_TI, &t2255_0_0_0, &t2255_1_0_0, t2255_IOs, &t2255_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2255), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2260.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2260_TI;

extern MethodInfo m11378_MI;
extern MethodInfo m11379_MI;
extern MethodInfo m11376_MI;
extern MethodInfo m11374_MI;
extern MethodInfo m11367_MI;
extern MethodInfo m11377_MI;
extern MethodInfo m11365_MI;
extern MethodInfo m11370_MI;
extern MethodInfo m11361_MI;
extern MethodInfo m26927_MI;
extern MethodInfo m26933_MI;
extern MethodInfo m26934_MI;


extern MethodInfo m11344_MI;
 void m11344 (t2260 * __this, MethodInfo* method){
	t56 * V_0 = {0};
	t29 * V_1 = {0};
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t56_TI));
		t56 * L_0 = (t56 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t56_TI));
		m1397(L_0, &m1397_MI);
		V_0 = L_0;
		V_1 = V_0;
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, V_1);
		__this->f1 = L_1;
		__this->f0 = V_0;
		return;
	}
}
extern MethodInfo m11345_MI;
 bool m11345 (t2260 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m26925_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m11346_MI;
 void m11346 (t2260 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t20 *, int32_t >::Invoke(&m3960_MI, ((t29 *)Castclass(L_0, InitializedTypeInfo(&t674_TI))), p0, p1);
		return;
	}
}
extern MethodInfo m11347_MI;
 t29 * m11347 (t2260 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m26923_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m11348_MI;
 int32_t m11348 (t2260 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m26921_MI, L_0);
		V_0 = L_1;
		t57  L_2 = m11376(NULL, p0, &m11376_MI);
		VirtActionInvoker2< int32_t, t57  >::Invoke(&m11367_MI, __this, V_0, L_2);
		return V_0;
	}
}
extern MethodInfo m11349_MI;
 bool m11349 (t2260 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m11375(NULL, p0, &m11375_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, t57  >::Invoke(&m26928_MI, L_1, ((*(t57 *)((t57 *)UnBox (p0, InitializedTypeInfo(&t57_TI))))));
		return L_2;
	}

IL_001a:
	{
		return 0;
	}
}
extern MethodInfo m11350_MI;
 int32_t m11350 (t2260 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m11375(NULL, p0, &m11375_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, t57  >::Invoke(&m26932_MI, L_1, ((*(t57 *)((t57 *)UnBox (p0, InitializedTypeInfo(&t57_TI))))));
		return L_2;
	}

IL_001a:
	{
		return (-1);
	}
}
extern MethodInfo m11351_MI;
 void m11351 (t2260 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t57  L_0 = m11376(NULL, p1, &m11376_MI);
		VirtActionInvoker2< int32_t, t57  >::Invoke(&m11367_MI, __this, p0, L_0);
		return;
	}
}
extern MethodInfo m11352_MI;
 void m11352 (t2260 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		m11377(NULL, L_0, &m11377_MI);
		t57  L_1 = m11376(NULL, p0, &m11376_MI);
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, t57  >::Invoke(&m11365_MI, __this, L_1);
		V_0 = L_2;
		VirtActionInvoker1< int32_t >::Invoke(&m11370_MI, __this, V_0);
		return;
	}
}
extern MethodInfo m11353_MI;
 bool m11353 (t2260 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = m11378(NULL, L_0, &m11378_MI);
		return L_1;
	}
}
extern MethodInfo m11354_MI;
 t29 * m11354 (t2260 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m11355_MI;
 bool m11355 (t2260 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = m11379(NULL, L_0, &m11379_MI);
		return L_1;
	}
}
extern MethodInfo m11356_MI;
 bool m11356 (t2260 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m26925_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m11357_MI;
 t29 * m11357 (t2260 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t57  L_1 = (t57 )InterfaceFuncInvoker1< t57 , int32_t >::Invoke(&m26930_MI, L_0, p0);
		t57  L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t57_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m11358_MI;
 void m11358 (t2260 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t57  L_0 = m11376(NULL, p1, &m11376_MI);
		VirtActionInvoker2< int32_t, t57  >::Invoke(&m11374_MI, __this, p0, L_0);
		return;
	}
}
extern MethodInfo m11359_MI;
 void m11359 (t2260 * __this, t57  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m26921_MI, L_0);
		V_0 = L_1;
		VirtActionInvoker2< int32_t, t57  >::Invoke(&m11367_MI, __this, V_0, p0);
		return;
	}
}
extern MethodInfo m11360_MI;
 void m11360 (t2260 * __this, MethodInfo* method){
	{
		VirtActionInvoker0::Invoke(&m11361_MI, __this);
		return;
	}
}
 void m11361 (t2260 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker0::Invoke(&m26927_MI, L_0);
		return;
	}
}
extern MethodInfo m11362_MI;
 bool m11362 (t2260 * __this, t57  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker1< bool, t57  >::Invoke(&m26928_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m11363_MI;
 void m11363 (t2260 * __this, t2251* p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t2251*, int32_t >::Invoke(&m26922_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m11364_MI;
 t29* m11364 (t2260 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m26923_MI, L_0);
		return L_1;
	}
}
 int32_t m11365 (t2260 * __this, t57  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker1< int32_t, t57  >::Invoke(&m26932_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m11366_MI;
 void m11366 (t2260 * __this, int32_t p0, t57  p1, MethodInfo* method){
	{
		VirtActionInvoker2< int32_t, t57  >::Invoke(&m11367_MI, __this, p0, p1);
		return;
	}
}
 void m11367 (t2260 * __this, int32_t p0, t57  p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< int32_t, t57  >::Invoke(&m26933_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m11368_MI;
 bool m11368 (t2260 * __this, t57  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t57  >::Invoke(&m11365_MI, __this, p0);
		V_0 = L_0;
		if ((((uint32_t)V_0) != ((uint32_t)(-1))))
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		VirtActionInvoker1< int32_t >::Invoke(&m11370_MI, __this, V_0);
		return 1;
	}
}
extern MethodInfo m11369_MI;
 void m11369 (t2260 * __this, int32_t p0, MethodInfo* method){
	{
		VirtActionInvoker1< int32_t >::Invoke(&m11370_MI, __this, p0);
		return;
	}
}
 void m11370 (t2260 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker1< int32_t >::Invoke(&m26934_MI, L_0, p0);
		return;
	}
}
extern MethodInfo m11371_MI;
 int32_t m11371 (t2260 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m26921_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m11372_MI;
 t57  m11372 (t2260 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t57  L_1 = (t57 )InterfaceFuncInvoker1< t57 , int32_t >::Invoke(&m26930_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m11373_MI;
 void m11373 (t2260 * __this, int32_t p0, t57  p1, MethodInfo* method){
	{
		VirtActionInvoker2< int32_t, t57  >::Invoke(&m11374_MI, __this, p0, p1);
		return;
	}
}
 void m11374 (t2260 * __this, int32_t p0, t57  p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< int32_t, t57  >::Invoke(&m26931_MI, L_0, p0, p1);
		return;
	}
}
 bool m11375 (t29 * __this, t29 * p0, MethodInfo* method){
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t57_TI))))
		{
			goto IL_0022;
		}
	}
	{
		if (p0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t57_0_0_0), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		G_B4_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0020;
	}

IL_001f:
	{
		G_B4_0 = 0;
	}

IL_0020:
	{
		G_B6_0 = G_B4_0;
		goto IL_0023;
	}

IL_0022:
	{
		G_B6_0 = 1;
	}

IL_0023:
	{
		return G_B6_0;
	}
}
 t57  m11376 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m11375(NULL, p0, &m11375_MI);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		return ((*(t57 *)((t57 *)UnBox (p0, InitializedTypeInfo(&t57_TI)))));
	}

IL_000f:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
 void m11377 (t29 * __this, t29* p0, MethodInfo* method){
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m26925_MI, p0);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		t345 * L_1 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_1, &m1516_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		return;
	}
}
 bool m11378 (t29 * __this, t29* p0, MethodInfo* method){
	t29 * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		V_0 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t674_TI)));
		if (!V_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m9815_MI, V_0);
		G_B3_0 = ((int32_t)(L_0));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
 bool m11379 (t29 * __this, t29* p0, MethodInfo* method){
	t29 * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		V_0 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t868_TI)));
		if (!V_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m9817_MI, V_0);
		G_B3_0 = ((int32_t)(L_0));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t2259_0_0_1;
FieldInfo t2260_f0_FieldInfo = 
{
	"list", &t2259_0_0_1, &t2260_TI, offsetof(t2260, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2260_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2260_TI, offsetof(t2260, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2260_FIs[] =
{
	&t2260_f0_FieldInfo,
	&t2260_f1_FieldInfo,
	NULL
};
static PropertyInfo t2260____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2260_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11345_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2260____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2260_TI, "System.Collections.ICollection.IsSynchronized", &m11353_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2260____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2260_TI, "System.Collections.ICollection.SyncRoot", &m11354_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2260____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2260_TI, "System.Collections.IList.IsFixedSize", &m11355_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2260____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2260_TI, "System.Collections.IList.IsReadOnly", &m11356_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2260____System_Collections_IList_Item_PropertyInfo = 
{
	&t2260_TI, "System.Collections.IList.Item", &m11357_MI, &m11358_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2260____Count_PropertyInfo = 
{
	&t2260_TI, "Count", &m11371_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2260____Item_PropertyInfo = 
{
	&t2260_TI, "Item", &m11372_MI, &m11373_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2260_PIs[] =
{
	&t2260____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2260____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2260____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2260____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2260____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2260____System_Collections_IList_Item_PropertyInfo,
	&t2260____Count_PropertyInfo,
	&t2260____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11344_GM;
MethodInfo m11344_MI = 
{
	".ctor", (methodPointerType)&m11344, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11344_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11345_GM;
MethodInfo m11345_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m11345, &t2260_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11345_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2260_m11346_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11346_GM;
MethodInfo m11346_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m11346, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2260_m11346_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11346_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11347_GM;
MethodInfo m11347_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m11347, &t2260_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11347_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2260_m11348_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11348_GM;
MethodInfo m11348_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m11348, &t2260_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2260_m11348_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11348_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2260_m11349_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11349_GM;
MethodInfo m11349_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m11349, &t2260_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2260_m11349_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11349_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2260_m11350_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11350_GM;
MethodInfo m11350_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m11350, &t2260_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2260_m11350_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11350_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2260_m11351_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11351_GM;
MethodInfo m11351_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m11351, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2260_m11351_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11351_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2260_m11352_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11352_GM;
MethodInfo m11352_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m11352, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2260_m11352_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11352_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11353_GM;
MethodInfo m11353_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m11353, &t2260_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11353_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11354_GM;
MethodInfo m11354_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m11354, &t2260_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11354_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11355_GM;
MethodInfo m11355_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m11355, &t2260_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11355_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11356_GM;
MethodInfo m11356_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m11356, &t2260_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11356_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2260_m11357_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11357_GM;
MethodInfo m11357_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m11357, &t2260_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2260_m11357_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11357_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2260_m11358_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11358_GM;
MethodInfo m11358_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m11358, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2260_m11358_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11358_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2260_m11359_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11359_GM;
MethodInfo m11359_MI = 
{
	"Add", (methodPointerType)&m11359, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t57, t2260_m11359_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11359_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11360_GM;
MethodInfo m11360_MI = 
{
	"Clear", (methodPointerType)&m11360, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11360_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11361_GM;
MethodInfo m11361_MI = 
{
	"ClearItems", (methodPointerType)&m11361, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11361_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2260_m11362_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11362_GM;
MethodInfo m11362_MI = 
{
	"Contains", (methodPointerType)&m11362, &t2260_TI, &t40_0_0_0, RuntimeInvoker_t40_t57, t2260_m11362_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11362_GM};
extern Il2CppType t2251_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2260_m11363_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2251_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11363_GM;
MethodInfo m11363_MI = 
{
	"CopyTo", (methodPointerType)&m11363, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2260_m11363_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11363_GM};
extern Il2CppType t2252_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11364_GM;
MethodInfo m11364_MI = 
{
	"GetEnumerator", (methodPointerType)&m11364, &t2260_TI, &t2252_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11364_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2260_m11365_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11365_GM;
MethodInfo m11365_MI = 
{
	"IndexOf", (methodPointerType)&m11365, &t2260_TI, &t44_0_0_0, RuntimeInvoker_t44_t57, t2260_m11365_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11365_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t2260_m11366_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11366_GM;
MethodInfo m11366_MI = 
{
	"Insert", (methodPointerType)&m11366, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t57, t2260_m11366_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11366_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t2260_m11367_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11367_GM;
MethodInfo m11367_MI = 
{
	"InsertItem", (methodPointerType)&m11367, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t57, t2260_m11367_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11367_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2260_m11368_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11368_GM;
MethodInfo m11368_MI = 
{
	"Remove", (methodPointerType)&m11368, &t2260_TI, &t40_0_0_0, RuntimeInvoker_t40_t57, t2260_m11368_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11368_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2260_m11369_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11369_GM;
MethodInfo m11369_MI = 
{
	"RemoveAt", (methodPointerType)&m11369, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2260_m11369_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11369_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2260_m11370_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11370_GM;
MethodInfo m11370_MI = 
{
	"RemoveItem", (methodPointerType)&m11370, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2260_m11370_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11370_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11371_GM;
MethodInfo m11371_MI = 
{
	"get_Count", (methodPointerType)&m11371, &t2260_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11371_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2260_m11372_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t57_0_0_0;
extern void* RuntimeInvoker_t57_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11372_GM;
MethodInfo m11372_MI = 
{
	"get_Item", (methodPointerType)&m11372, &t2260_TI, &t57_0_0_0, RuntimeInvoker_t57_t44, t2260_m11372_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11372_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t2260_m11373_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11373_GM;
MethodInfo m11373_MI = 
{
	"set_Item", (methodPointerType)&m11373, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t57, t2260_m11373_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11373_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t2260_m11374_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11374_GM;
MethodInfo m11374_MI = 
{
	"SetItem", (methodPointerType)&m11374, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t57, t2260_m11374_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11374_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2260_m11375_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11375_GM;
MethodInfo m11375_MI = 
{
	"IsValidItem", (methodPointerType)&m11375, &t2260_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2260_m11375_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11375_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2260_m11376_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t57_0_0_0;
extern void* RuntimeInvoker_t57_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11376_GM;
MethodInfo m11376_MI = 
{
	"ConvertItem", (methodPointerType)&m11376, &t2260_TI, &t57_0_0_0, RuntimeInvoker_t57_t29, t2260_m11376_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11376_GM};
extern Il2CppType t2259_0_0_0;
static ParameterInfo t2260_m11377_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2259_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11377_GM;
MethodInfo m11377_MI = 
{
	"CheckWritable", (methodPointerType)&m11377, &t2260_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2260_m11377_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11377_GM};
extern Il2CppType t2259_0_0_0;
static ParameterInfo t2260_m11378_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2259_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11378_GM;
MethodInfo m11378_MI = 
{
	"IsSynchronized", (methodPointerType)&m11378, &t2260_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2260_m11378_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11378_GM};
extern Il2CppType t2259_0_0_0;
static ParameterInfo t2260_m11379_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2259_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11379_GM;
MethodInfo m11379_MI = 
{
	"IsFixedSize", (methodPointerType)&m11379, &t2260_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2260_m11379_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11379_GM};
static MethodInfo* t2260_MIs[] =
{
	&m11344_MI,
	&m11345_MI,
	&m11346_MI,
	&m11347_MI,
	&m11348_MI,
	&m11349_MI,
	&m11350_MI,
	&m11351_MI,
	&m11352_MI,
	&m11353_MI,
	&m11354_MI,
	&m11355_MI,
	&m11356_MI,
	&m11357_MI,
	&m11358_MI,
	&m11359_MI,
	&m11360_MI,
	&m11361_MI,
	&m11362_MI,
	&m11363_MI,
	&m11364_MI,
	&m11365_MI,
	&m11366_MI,
	&m11367_MI,
	&m11368_MI,
	&m11369_MI,
	&m11370_MI,
	&m11371_MI,
	&m11372_MI,
	&m11373_MI,
	&m11374_MI,
	&m11375_MI,
	&m11376_MI,
	&m11377_MI,
	&m11378_MI,
	&m11379_MI,
	NULL
};
static MethodInfo* t2260_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11347_MI,
	&m11371_MI,
	&m11353_MI,
	&m11354_MI,
	&m11346_MI,
	&m11355_MI,
	&m11356_MI,
	&m11357_MI,
	&m11358_MI,
	&m11348_MI,
	&m11360_MI,
	&m11349_MI,
	&m11350_MI,
	&m11351_MI,
	&m11352_MI,
	&m11369_MI,
	&m11371_MI,
	&m11345_MI,
	&m11359_MI,
	&m11360_MI,
	&m11362_MI,
	&m11363_MI,
	&m11368_MI,
	&m11365_MI,
	&m11366_MI,
	&m11369_MI,
	&m11372_MI,
	&m11373_MI,
	&m11364_MI,
	&m11361_MI,
	&m11367_MI,
	&m11370_MI,
	&m11374_MI,
};
static TypeInfo* t2260_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2253_TI,
	&t2259_TI,
	&t2254_TI,
};
static Il2CppInterfaceOffsetPair t2260_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2253_TI, 20},
	{ &t2259_TI, 27},
	{ &t2254_TI, 32},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2260_0_0_0;
extern Il2CppType t2260_1_0_0;
struct t2260;
extern Il2CppGenericClass t2260_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2260_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2260_MIs, t2260_PIs, t2260_FIs, NULL, &t29_TI, NULL, NULL, &t2260_TI, t2260_ITIs, t2260_VT, &t1262__CustomAttributeCache, &t2260_TI, &t2260_0_0_0, &t2260_1_0_0, t2260_IOs, &t2260_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2260), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2261_TI;
#include "t2261MD.h"

#include "t2262.h"
extern TypeInfo t6634_TI;
extern TypeInfo t2262_TI;
#include "t2262MD.h"
extern Il2CppType t6634_0_0_0;
extern MethodInfo m11385_MI;
extern MethodInfo m26935_MI;
extern MethodInfo m20090_MI;


extern MethodInfo m11380_MI;
 void m11380 (t2261 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m11381_MI;
 void m11381 (t29 * __this, MethodInfo* method){
	t2262 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t2262 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t2262_TI));
	m11385(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m11385_MI);
	((t2261_SFs*)InitializedTypeInfo(&t2261_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m11382_MI;
 int32_t m11382 (t2261 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t57  >::Invoke(&m26935_MI, __this, ((*(t57 *)((t57 *)UnBox (p0, InitializedTypeInfo(&t57_TI))))));
		return L_0;
	}
}
extern MethodInfo m11383_MI;
 bool m11383 (t2261 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, t57 , t57  >::Invoke(&m20090_MI, __this, ((*(t57 *)((t57 *)UnBox (p0, InitializedTypeInfo(&t57_TI))))), ((*(t57 *)((t57 *)UnBox (p1, InitializedTypeInfo(&t57_TI))))));
		return L_0;
	}
}
extern MethodInfo m11384_MI;
 t2261 * m11384 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2261_TI));
		return (((t2261_SFs*)InitializedTypeInfo(&t2261_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t2261_0_0_49;
FieldInfo t2261_f0_FieldInfo = 
{
	"_default", &t2261_0_0_49, &t2261_TI, offsetof(t2261_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2261_FIs[] =
{
	&t2261_f0_FieldInfo,
	NULL
};
static PropertyInfo t2261____Default_PropertyInfo = 
{
	&t2261_TI, "Default", &m11384_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2261_PIs[] =
{
	&t2261____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11380_GM;
MethodInfo m11380_MI = 
{
	".ctor", (methodPointerType)&m11380, &t2261_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11380_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11381_GM;
MethodInfo m11381_MI = 
{
	".cctor", (methodPointerType)&m11381, &t2261_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11381_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2261_m11382_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11382_GM;
MethodInfo m11382_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m11382, &t2261_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2261_m11382_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11382_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2261_m11383_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11383_GM;
MethodInfo m11383_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m11383, &t2261_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2261_m11383_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11383_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2261_m26935_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26935_GM;
MethodInfo m26935_MI = 
{
	"GetHashCode", NULL, &t2261_TI, &t44_0_0_0, RuntimeInvoker_t44_t57, t2261_m26935_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26935_GM};
extern Il2CppType t57_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t2261_m20090_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20090_GM;
MethodInfo m20090_MI = 
{
	"Equals", NULL, &t2261_TI, &t40_0_0_0, RuntimeInvoker_t40_t57_t57, t2261_m20090_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20090_GM};
extern Il2CppType t2261_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11384_GM;
MethodInfo m11384_MI = 
{
	"get_Default", (methodPointerType)&m11384, &t2261_TI, &t2261_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11384_GM};
static MethodInfo* t2261_MIs[] =
{
	&m11380_MI,
	&m11381_MI,
	&m11382_MI,
	&m11383_MI,
	&m26935_MI,
	&m20090_MI,
	&m11384_MI,
	NULL
};
static MethodInfo* t2261_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m20090_MI,
	&m26935_MI,
	&m11383_MI,
	&m11382_MI,
	NULL,
	NULL,
};
extern TypeInfo t6635_TI;
static TypeInfo* t2261_ITIs[] = 
{
	&t6635_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2261_IOs[] = 
{
	{ &t6635_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2261_0_0_0;
extern Il2CppType t2261_1_0_0;
struct t2261;
extern Il2CppGenericClass t2261_GC;
TypeInfo t2261_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2261_MIs, t2261_PIs, t2261_FIs, NULL, &t29_TI, NULL, NULL, &t2261_TI, t2261_ITIs, t2261_VT, &EmptyCustomAttributesCache, &t2261_TI, &t2261_0_0_0, &t2261_1_0_0, t2261_IOs, &t2261_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2261), 0, -1, sizeof(t2261_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t57_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t6635_m26936_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26936_GM;
MethodInfo m26936_MI = 
{
	"Equals", NULL, &t6635_TI, &t40_0_0_0, RuntimeInvoker_t40_t57_t57, t6635_m26936_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26936_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t6635_m26937_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26937_GM;
MethodInfo m26937_MI = 
{
	"GetHashCode", NULL, &t6635_TI, &t44_0_0_0, RuntimeInvoker_t44_t57, t6635_m26937_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26937_GM};
static MethodInfo* t6635_MIs[] =
{
	&m26936_MI,
	&m26937_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6635_0_0_0;
extern Il2CppType t6635_1_0_0;
struct t6635;
extern Il2CppGenericClass t6635_GC;
TypeInfo t6635_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6635_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6635_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6635_TI, &t6635_0_0_0, &t6635_1_0_0, NULL, &t6635_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t57_0_0_0;
static ParameterInfo t6634_m26938_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26938_GM;
MethodInfo m26938_MI = 
{
	"Equals", NULL, &t6634_TI, &t40_0_0_0, RuntimeInvoker_t40_t57, t6634_m26938_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26938_GM};
static MethodInfo* t6634_MIs[] =
{
	&m26938_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6634_1_0_0;
struct t6634;
extern Il2CppGenericClass t6634_GC;
TypeInfo t6634_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6634_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6634_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6634_TI, &t6634_0_0_0, &t6634_1_0_0, NULL, &t6634_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m11385 (t2262 * __this, MethodInfo* method){
	{
		m11380(__this, &m11380_MI);
		return;
	}
}
extern MethodInfo m11386_MI;
 int32_t m11386 (t2262 * __this, t57  p0, MethodInfo* method){
	{
		t57  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t57_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t57_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m11387_MI;
 bool m11387 (t2262 * __this, t57  p0, t57  p1, MethodInfo* method){
	{
		t57  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t57_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		t57  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t57_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		t57  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t57_TI), &L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t57_TI), &(*(&p0))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11385_GM;
MethodInfo m11385_MI = 
{
	".ctor", (methodPointerType)&m11385, &t2262_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11385_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2262_m11386_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11386_GM;
MethodInfo m11386_MI = 
{
	"GetHashCode", (methodPointerType)&m11386, &t2262_TI, &t44_0_0_0, RuntimeInvoker_t44_t57, t2262_m11386_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11386_GM};
extern Il2CppType t57_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t2262_m11387_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11387_GM;
MethodInfo m11387_MI = 
{
	"Equals", (methodPointerType)&m11387, &t2262_TI, &t40_0_0_0, RuntimeInvoker_t40_t57_t57, t2262_m11387_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11387_GM};
static MethodInfo* t2262_MIs[] =
{
	&m11385_MI,
	&m11386_MI,
	&m11387_MI,
	NULL
};
static MethodInfo* t2262_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11387_MI,
	&m11386_MI,
	&m11383_MI,
	&m11382_MI,
	&m11386_MI,
	&m11387_MI,
};
static Il2CppInterfaceOffsetPair t2262_IOs[] = 
{
	{ &t6635_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2262_0_0_0;
extern Il2CppType t2262_1_0_0;
struct t2262;
extern Il2CppGenericClass t2262_GC;
TypeInfo t2262_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2262_MIs, NULL, NULL, NULL, &t2261_TI, NULL, &t1256_TI, &t2262_TI, NULL, t2262_VT, &EmptyCustomAttributesCache, &t2262_TI, &t2262_0_0_0, &t2262_1_0_0, t2262_IOs, &t2262_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2262), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



extern MethodInfo m11388_MI;
 void m11388 (t2256 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
 bool m11389 (t2256 * __this, t57  p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m11389((t2256 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t57  p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	typedef bool (*FunctionPointerType) (t29 * __this, t57  p0, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m11390_MI;
 t29 * m11390 (t2256 * __this, t57  p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t57_TI), &p0);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m11391_MI;
 bool m11391 (t2256 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2256_m11388_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11388_GM;
MethodInfo m11388_MI = 
{
	".ctor", (methodPointerType)&m11388, &t2256_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2256_m11388_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11388_GM};
extern Il2CppType t57_0_0_0;
static ParameterInfo t2256_m11389_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11389_GM;
MethodInfo m11389_MI = 
{
	"Invoke", (methodPointerType)&m11389, &t2256_TI, &t40_0_0_0, RuntimeInvoker_t40_t57, t2256_m11389_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11389_GM};
extern Il2CppType t57_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2256_m11390_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t57_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11390_GM;
MethodInfo m11390_MI = 
{
	"BeginInvoke", (methodPointerType)&m11390, &t2256_TI, &t66_0_0_0, RuntimeInvoker_t29_t57_t29_t29, t2256_m11390_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11390_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2256_m11391_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11391_GM;
MethodInfo m11391_MI = 
{
	"EndInvoke", (methodPointerType)&m11391, &t2256_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2256_m11391_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11391_GM};
static MethodInfo* t2256_MIs[] =
{
	&m11388_MI,
	&m11389_MI,
	&m11390_MI,
	&m11391_MI,
	NULL
};
static MethodInfo* t2256_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11389_MI,
	&m11390_MI,
	&m11391_MI,
};
static Il2CppInterfaceOffsetPair t2256_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2256_1_0_0;
struct t2256;
extern Il2CppGenericClass t2256_GC;
TypeInfo t2256_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2256_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2256_TI, NULL, t2256_VT, &EmptyCustomAttributesCache, &t2256_TI, &t2256_0_0_0, &t2256_1_0_0, t2256_IOs, &t2256_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2256), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t2264.h"
extern TypeInfo t4036_TI;
extern TypeInfo t2264_TI;
#include "t2264MD.h"
extern Il2CppType t4036_0_0_0;
extern MethodInfo m11396_MI;
extern MethodInfo m26939_MI;


extern MethodInfo m11392_MI;
 void m11392 (t2263 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m11393_MI;
 void m11393 (t29 * __this, MethodInfo* method){
	t2264 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t2264 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t2264_TI));
	m11396(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m11396_MI);
	((t2263_SFs*)InitializedTypeInfo(&t2263_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m11394_MI;
 int32_t m11394 (t2263 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		if (p1)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		if (p1)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t57_TI))))
		{
			goto IL_0033;
		}
	}
	{
		if (!((t29 *)IsInst(p1, InitializedTypeInfo(&t57_TI))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, t57 , t57  >::Invoke(&m26939_MI, __this, ((*(t57 *)((t57 *)UnBox (p0, InitializedTypeInfo(&t57_TI))))), ((*(t57 *)((t57 *)UnBox (p1, InitializedTypeInfo(&t57_TI))))));
		return L_0;
	}

IL_0033:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_1, &m8852_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
 t2263 * m11395 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2263_TI));
		return (((t2263_SFs*)InitializedTypeInfo(&t2263_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t2263_0_0_49;
FieldInfo t2263_f0_FieldInfo = 
{
	"_default", &t2263_0_0_49, &t2263_TI, offsetof(t2263_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2263_FIs[] =
{
	&t2263_f0_FieldInfo,
	NULL
};
static PropertyInfo t2263____Default_PropertyInfo = 
{
	&t2263_TI, "Default", &m11395_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2263_PIs[] =
{
	&t2263____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11392_GM;
MethodInfo m11392_MI = 
{
	".ctor", (methodPointerType)&m11392, &t2263_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11392_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11393_GM;
MethodInfo m11393_MI = 
{
	".cctor", (methodPointerType)&m11393, &t2263_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11393_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2263_m11394_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11394_GM;
MethodInfo m11394_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m11394, &t2263_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2263_m11394_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11394_GM};
extern Il2CppType t57_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t2263_m26939_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t57_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26939_GM;
MethodInfo m26939_MI = 
{
	"Compare", NULL, &t2263_TI, &t44_0_0_0, RuntimeInvoker_t44_t57_t57, t2263_m26939_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26939_GM};
extern Il2CppType t2263_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11395_GM;
MethodInfo m11395_MI = 
{
	"get_Default", (methodPointerType)&m11395, &t2263_TI, &t2263_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11395_GM};
static MethodInfo* t2263_MIs[] =
{
	&m11392_MI,
	&m11393_MI,
	&m11394_MI,
	&m26939_MI,
	&m11395_MI,
	NULL
};
static MethodInfo* t2263_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m26939_MI,
	&m11394_MI,
	NULL,
};
extern TypeInfo t4035_TI;
static TypeInfo* t2263_ITIs[] = 
{
	&t4035_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2263_IOs[] = 
{
	{ &t4035_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2263_0_0_0;
extern Il2CppType t2263_1_0_0;
struct t2263;
extern Il2CppGenericClass t2263_GC;
TypeInfo t2263_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2263_MIs, t2263_PIs, t2263_FIs, NULL, &t29_TI, NULL, NULL, &t2263_TI, t2263_ITIs, t2263_VT, &EmptyCustomAttributesCache, &t2263_TI, &t2263_0_0_0, &t2263_1_0_0, t2263_IOs, &t2263_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2263), 0, -1, sizeof(t2263_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t57_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t4035_m20098_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t57_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20098_GM;
MethodInfo m20098_MI = 
{
	"Compare", NULL, &t4035_TI, &t44_0_0_0, RuntimeInvoker_t44_t57_t57, t4035_m20098_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20098_GM};
static MethodInfo* t4035_MIs[] =
{
	&m20098_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4035_0_0_0;
extern Il2CppType t4035_1_0_0;
struct t4035;
extern Il2CppGenericClass t4035_GC;
TypeInfo t4035_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4035_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4035_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4035_TI, &t4035_0_0_0, &t4035_1_0_0, NULL, &t4035_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t57_0_0_0;
static ParameterInfo t4036_m20099_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20099_GM;
MethodInfo m20099_MI = 
{
	"CompareTo", NULL, &t4036_TI, &t44_0_0_0, RuntimeInvoker_t44_t57, t4036_m20099_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20099_GM};
static MethodInfo* t4036_MIs[] =
{
	&m20099_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4036_1_0_0;
struct t4036;
extern Il2CppGenericClass t4036_GC;
TypeInfo t4036_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4036_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4036_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4036_TI, &t4036_0_0_0, &t4036_1_0_0, NULL, &t4036_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m20099_MI;


 void m11396 (t2264 * __this, MethodInfo* method){
	{
		m11392(__this, &m11392_MI);
		return;
	}
}
extern MethodInfo m11397_MI;
 int32_t m11397 (t2264 * __this, t57  p0, t57  p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		t57  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t57_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		t57  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t57_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t57  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t57_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		t57  L_6 = p0;
		t29 * L_7 = Box(InitializedTypeInfo(&t57_TI), &L_6);
		if (!((t29*)IsInst(L_7, InitializedTypeInfo(&t4036_TI))))
		{
			goto IL_003e;
		}
	}
	{
		t57  L_8 = p0;
		t29 * L_9 = Box(InitializedTypeInfo(&t57_TI), &L_8);
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, t57  >::Invoke(&m20099_MI, ((t29*)Castclass(L_9, InitializedTypeInfo(&t4036_TI))), p1);
		return L_10;
	}

IL_003e:
	{
		t57  L_11 = p0;
		t29 * L_12 = Box(InitializedTypeInfo(&t57_TI), &L_11);
		if (!((t29 *)IsInst(L_12, InitializedTypeInfo(&t290_TI))))
		{
			goto IL_0062;
		}
	}
	{
		t57  L_13 = p0;
		t29 * L_14 = Box(InitializedTypeInfo(&t57_TI), &L_13);
		t57  L_15 = p1;
		t29 * L_16 = Box(InitializedTypeInfo(&t57_TI), &L_15);
		int32_t L_17 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m9672_MI, ((t29 *)Castclass(L_14, InitializedTypeInfo(&t290_TI))), L_16);
		return L_17;
	}

IL_0062:
	{
		t305 * L_18 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_18, (t7*) &_stringLiteral1162, &m1935_MI);
		il2cpp_codegen_raise_exception(L_18);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11396_GM;
MethodInfo m11396_MI = 
{
	".ctor", (methodPointerType)&m11396, &t2264_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11396_GM};
extern Il2CppType t57_0_0_0;
extern Il2CppType t57_0_0_0;
static ParameterInfo t2264_m11397_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t57_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t57_t57 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11397_GM;
MethodInfo m11397_MI = 
{
	"Compare", (methodPointerType)&m11397, &t2264_TI, &t44_0_0_0, RuntimeInvoker_t44_t57_t57, t2264_m11397_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11397_GM};
static MethodInfo* t2264_MIs[] =
{
	&m11396_MI,
	&m11397_MI,
	NULL
};
static MethodInfo* t2264_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11397_MI,
	&m11394_MI,
	&m11397_MI,
};
static Il2CppInterfaceOffsetPair t2264_IOs[] = 
{
	{ &t4035_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2264_0_0_0;
extern Il2CppType t2264_1_0_0;
struct t2264;
extern Il2CppGenericClass t2264_GC;
TypeInfo t2264_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2264_MIs, NULL, NULL, NULL, &t2263_TI, NULL, &t1246_TI, &t2264_TI, NULL, t2264_VT, &EmptyCustomAttributesCache, &t2264_TI, &t2264_0_0_0, &t2264_1_0_0, t2264_IOs, &t2264_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2264), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t108.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t108_TI;
#include "t108MD.h"

#include "t109.h"
#include "t2272.h"
#include "t2269.h"
#include "t2270.h"
#include "t2278.h"
#include "t2271.h"
extern TypeInfo t109_TI;
extern TypeInfo t2265_TI;
extern TypeInfo t2272_TI;
extern TypeInfo t2267_TI;
extern TypeInfo t2268_TI;
extern TypeInfo t2266_TI;
extern TypeInfo t2269_TI;
extern TypeInfo t2270_TI;
extern TypeInfo t2278_TI;
#include "t2269MD.h"
#include "t2270MD.h"
#include "t2272MD.h"
#include "t2278MD.h"
extern MethodInfo m1306_MI;
extern MethodInfo m11440_MI;
extern MethodInfo m20115_MI;
extern MethodInfo m11428_MI;
extern MethodInfo m11425_MI;
extern MethodInfo m1383_MI;
extern MethodInfo m1382_MI;
extern MethodInfo m11426_MI;
extern MethodInfo m11429_MI;
extern MethodInfo m1384_MI;
extern MethodInfo m11415_MI;
extern MethodInfo m11438_MI;
extern MethodInfo m11439_MI;
extern MethodInfo m26940_MI;
extern MethodInfo m26941_MI;
extern MethodInfo m26942_MI;
extern MethodInfo m26943_MI;
extern MethodInfo m11430_MI;
extern MethodInfo m11416_MI;
extern MethodInfo m11417_MI;
extern MethodInfo m11452_MI;
extern MethodInfo m20117_MI;
extern MethodInfo m11423_MI;
extern MethodInfo m11424_MI;
extern MethodInfo m11527_MI;
extern MethodInfo m11446_MI;
extern MethodInfo m11427_MI;
extern MethodInfo m11432_MI;
extern MethodInfo m11533_MI;
extern MethodInfo m20119_MI;
extern MethodInfo m20127_MI;
struct t20;
#define m20115(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2276.h"
#define m20117(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
#define m20119(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#define m20127(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2272  m11425 (t108 * __this, MethodInfo* method){
	{
		t2272  L_0 = {0};
		m11446(&L_0, __this, &m11446_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t44_0_0_32849;
FieldInfo t108_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t108_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2265_0_0_1;
FieldInfo t108_f1_FieldInfo = 
{
	"_items", &t2265_0_0_1, &t108_TI, offsetof(t108, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t108_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t108_TI, offsetof(t108, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t108_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t108_TI, offsetof(t108, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2265_0_0_49;
FieldInfo t108_f4_FieldInfo = 
{
	"EmptyArray", &t2265_0_0_49, &t108_TI, offsetof(t108_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t108_FIs[] =
{
	&t108_f0_FieldInfo,
	&t108_f1_FieldInfo,
	&t108_f2_FieldInfo,
	&t108_f3_FieldInfo,
	&t108_f4_FieldInfo,
	NULL
};
static const int32_t t108_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t108_f0_DefaultValue = 
{
	&t108_f0_FieldInfo, { (char*)&t108_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t108_FDVs[] = 
{
	&t108_f0_DefaultValue,
	NULL
};
extern MethodInfo m11408_MI;
static PropertyInfo t108____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t108_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11408_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11409_MI;
static PropertyInfo t108____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t108_TI, "System.Collections.ICollection.IsSynchronized", &m11409_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11410_MI;
static PropertyInfo t108____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t108_TI, "System.Collections.ICollection.SyncRoot", &m11410_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11411_MI;
static PropertyInfo t108____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t108_TI, "System.Collections.IList.IsFixedSize", &m11411_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11412_MI;
static PropertyInfo t108____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t108_TI, "System.Collections.IList.IsReadOnly", &m11412_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11413_MI;
extern MethodInfo m11414_MI;
static PropertyInfo t108____System_Collections_IList_Item_PropertyInfo = 
{
	&t108_TI, "System.Collections.IList.Item", &m11413_MI, &m11414_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t108____Capacity_PropertyInfo = 
{
	&t108_TI, "Capacity", &m11438_MI, &m11439_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1308_MI;
static PropertyInfo t108____Count_PropertyInfo = 
{
	&t108_TI, "Count", &m1308_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t108____Item_PropertyInfo = 
{
	&t108_TI, "Item", &m1306_MI, &m11440_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t108_PIs[] =
{
	&t108____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t108____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t108____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t108____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t108____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t108____System_Collections_IList_Item_PropertyInfo,
	&t108____Capacity_PropertyInfo,
	&t108____Count_PropertyInfo,
	&t108____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1381_GM;
MethodInfo m1381_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1381_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t108_m11398_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11398_GM;
MethodInfo m11398_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t108_m11398_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11398_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11399_GM;
MethodInfo m11399_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11399_GM};
extern Il2CppType t2266_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11400_GM;
MethodInfo m11400_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t108_TI, &t2266_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11400_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t108_m11401_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11401_GM;
MethodInfo m11401_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t108_m11401_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11401_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11402_GM;
MethodInfo m11402_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t108_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11402_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t108_m11403_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11403_GM;
MethodInfo m11403_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t108_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t108_m11403_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11403_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t108_m11404_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11404_GM;
MethodInfo m11404_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t108_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t108_m11404_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11404_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t108_m11405_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11405_GM;
MethodInfo m11405_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t108_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t108_m11405_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11405_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t108_m11406_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11406_GM;
MethodInfo m11406_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t108_m11406_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11406_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t108_m11407_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11407_GM;
MethodInfo m11407_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t108_m11407_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11407_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11408_GM;
MethodInfo m11408_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t108_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11408_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11409_GM;
MethodInfo m11409_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t108_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11409_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11410_GM;
MethodInfo m11410_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t108_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11410_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11411_GM;
MethodInfo m11411_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t108_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11411_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11412_GM;
MethodInfo m11412_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t108_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11412_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t108_m11413_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11413_GM;
MethodInfo m11413_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t108_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t108_m11413_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11413_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t108_m11414_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11414_GM;
MethodInfo m11414_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t108_m11414_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11414_GM};
extern Il2CppType t109_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t108_m1383_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1383_GM;
MethodInfo m1383_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t108_m1383_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1383_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t108_m11415_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11415_GM;
MethodInfo m11415_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t108_m11415_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11415_GM};
extern Il2CppType t2267_0_0_0;
extern Il2CppType t2267_0_0_0;
static ParameterInfo t108_m11416_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2267_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11416_GM;
MethodInfo m11416_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t108_m11416_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11416_GM};
extern Il2CppType t2268_0_0_0;
extern Il2CppType t2268_0_0_0;
static ParameterInfo t108_m11417_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11417_GM;
MethodInfo m11417_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t108_m11417_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11417_GM};
extern Il2CppType t2268_0_0_0;
static ParameterInfo t108_m11418_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11418_GM;
MethodInfo m11418_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t108_m11418_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11418_GM};
extern Il2CppType t2269_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11419_GM;
MethodInfo m11419_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t108_TI, &t2269_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11419_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11420_GM;
MethodInfo m11420_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11420_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t108_m1382_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1382_GM;
MethodInfo m1382_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t108_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t108_m1382_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1382_GM};
extern Il2CppType t2265_0_0_0;
extern Il2CppType t2265_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t108_m11421_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2265_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11421_GM;
MethodInfo m11421_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t108_m11421_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11421_GM};
extern Il2CppType t2270_0_0_0;
extern Il2CppType t2270_0_0_0;
static ParameterInfo t108_m11422_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2270_0_0_0},
};
extern Il2CppType t109_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11422_GM;
MethodInfo m11422_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t108_TI, &t109_0_0_0, RuntimeInvoker_t29_t29, t108_m11422_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11422_GM};
extern Il2CppType t2270_0_0_0;
static ParameterInfo t108_m11423_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2270_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11423_GM;
MethodInfo m11423_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t108_m11423_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11423_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2270_0_0_0;
static ParameterInfo t108_m11424_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2270_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11424_GM;
MethodInfo m11424_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t108_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t108_m11424_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11424_GM};
extern Il2CppType t2272_0_0_0;
extern void* RuntimeInvoker_t2272 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11425_GM;
MethodInfo m11425_MI = 
{
	"GetEnumerator", (methodPointerType)&m11425, &t108_TI, &t2272_0_0_0, RuntimeInvoker_t2272, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11425_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t108_m11426_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11426_GM;
MethodInfo m11426_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t108_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t108_m11426_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11426_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t108_m11427_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11427_GM;
MethodInfo m11427_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t108_m11427_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11427_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t108_m11428_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11428_GM;
MethodInfo m11428_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t108_m11428_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11428_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t108_m11429_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11429_GM;
MethodInfo m11429_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t108_m11429_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11429_GM};
extern Il2CppType t2268_0_0_0;
static ParameterInfo t108_m11430_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11430_GM;
MethodInfo m11430_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t108_m11430_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11430_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t108_m1384_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1384_GM;
MethodInfo m1384_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t108_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t108_m1384_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1384_GM};
extern Il2CppType t2270_0_0_0;
static ParameterInfo t108_m11431_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2270_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11431_GM;
MethodInfo m11431_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t108_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t108_m11431_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11431_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t108_m11432_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11432_GM;
MethodInfo m11432_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t108_m11432_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11432_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11433_GM;
MethodInfo m11433_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11433_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11434_GM;
MethodInfo m11434_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11434_GM};
extern Il2CppType t2271_0_0_0;
extern Il2CppType t2271_0_0_0;
static ParameterInfo t108_m11435_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2271_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11435_GM;
MethodInfo m11435_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t108_m11435_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11435_GM};
extern Il2CppType t2265_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11436_GM;
MethodInfo m11436_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t108_TI, &t2265_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11436_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11437_GM;
MethodInfo m11437_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11437_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11438_GM;
MethodInfo m11438_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t108_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11438_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t108_m11439_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11439_GM;
MethodInfo m11439_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t108_m11439_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11439_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1308_GM;
MethodInfo m1308_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t108_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1308_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t108_m1306_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t109_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1306_GM;
MethodInfo m1306_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t108_TI, &t109_0_0_0, RuntimeInvoker_t29_t44, t108_m1306_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1306_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t108_m11440_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11440_GM;
MethodInfo m11440_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t108_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t108_m11440_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11440_GM};
static MethodInfo* t108_MIs[] =
{
	&m1381_MI,
	&m11398_MI,
	&m11399_MI,
	&m11400_MI,
	&m11401_MI,
	&m11402_MI,
	&m11403_MI,
	&m11404_MI,
	&m11405_MI,
	&m11406_MI,
	&m11407_MI,
	&m11408_MI,
	&m11409_MI,
	&m11410_MI,
	&m11411_MI,
	&m11412_MI,
	&m11413_MI,
	&m11414_MI,
	&m1383_MI,
	&m11415_MI,
	&m11416_MI,
	&m11417_MI,
	&m11418_MI,
	&m11419_MI,
	&m11420_MI,
	&m1382_MI,
	&m11421_MI,
	&m11422_MI,
	&m11423_MI,
	&m11424_MI,
	&m11425_MI,
	&m11426_MI,
	&m11427_MI,
	&m11428_MI,
	&m11429_MI,
	&m11430_MI,
	&m1384_MI,
	&m11431_MI,
	&m11432_MI,
	&m11433_MI,
	&m11434_MI,
	&m11435_MI,
	&m11436_MI,
	&m11437_MI,
	&m11438_MI,
	&m11439_MI,
	&m1308_MI,
	&m1306_MI,
	&m11440_MI,
	NULL
};
extern MethodInfo m11402_MI;
extern MethodInfo m11401_MI;
extern MethodInfo m11403_MI;
extern MethodInfo m11420_MI;
extern MethodInfo m11404_MI;
extern MethodInfo m11405_MI;
extern MethodInfo m11406_MI;
extern MethodInfo m11407_MI;
extern MethodInfo m11421_MI;
extern MethodInfo m11400_MI;
static MethodInfo* t108_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11402_MI,
	&m1308_MI,
	&m11409_MI,
	&m11410_MI,
	&m11401_MI,
	&m11411_MI,
	&m11412_MI,
	&m11413_MI,
	&m11414_MI,
	&m11403_MI,
	&m11420_MI,
	&m11404_MI,
	&m11405_MI,
	&m11406_MI,
	&m11407_MI,
	&m11432_MI,
	&m1308_MI,
	&m11408_MI,
	&m1383_MI,
	&m11420_MI,
	&m1382_MI,
	&m11421_MI,
	&m1384_MI,
	&m11400_MI,
	&m11426_MI,
	&m11429_MI,
	&m11432_MI,
	&m1306_MI,
	&m11440_MI,
};
extern TypeInfo t2274_TI;
static TypeInfo* t108_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2267_TI,
	&t2268_TI,
	&t2274_TI,
};
static Il2CppInterfaceOffsetPair t108_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2267_TI, 20},
	{ &t2268_TI, 27},
	{ &t2274_TI, 28},
};
extern TypeInfo t108_TI;
extern TypeInfo t2265_TI;
extern TypeInfo t2272_TI;
extern TypeInfo t109_TI;
extern TypeInfo t2267_TI;
extern TypeInfo t2269_TI;
static Il2CppRGCTXData t108_RGCTXData[37] = 
{
	&t108_TI/* Static Usage */,
	&t2265_TI/* Array Usage */,
	&m11425_MI/* Method Usage */,
	&t2272_TI/* Class Usage */,
	&t109_TI/* Class Usage */,
	&m1383_MI/* Method Usage */,
	&m1382_MI/* Method Usage */,
	&m11426_MI/* Method Usage */,
	&m11428_MI/* Method Usage */,
	&m11429_MI/* Method Usage */,
	&m1384_MI/* Method Usage */,
	&m1306_MI/* Method Usage */,
	&m11440_MI/* Method Usage */,
	&m11415_MI/* Method Usage */,
	&m11438_MI/* Method Usage */,
	&m11439_MI/* Method Usage */,
	&m26940_MI/* Method Usage */,
	&m26941_MI/* Method Usage */,
	&m26942_MI/* Method Usage */,
	&m26943_MI/* Method Usage */,
	&m11430_MI/* Method Usage */,
	&t2267_TI/* Class Usage */,
	&m11416_MI/* Method Usage */,
	&m11417_MI/* Method Usage */,
	&t2269_TI/* Class Usage */,
	&m11452_MI/* Method Usage */,
	&m20117_MI/* Method Usage */,
	&m11423_MI/* Method Usage */,
	&m11424_MI/* Method Usage */,
	&m11527_MI/* Method Usage */,
	&m11446_MI/* Method Usage */,
	&m11427_MI/* Method Usage */,
	&m11432_MI/* Method Usage */,
	&m11533_MI/* Method Usage */,
	&m20119_MI/* Method Usage */,
	&m20127_MI/* Method Usage */,
	&m20115_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t108_0_0_0;
extern Il2CppType t108_1_0_0;
struct t108;
extern Il2CppGenericClass t108_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t108_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t108_MIs, t108_PIs, t108_FIs, NULL, &t29_TI, NULL, NULL, &t108_TI, t108_ITIs, t108_VT, &t1261__CustomAttributeCache, &t108_TI, &t108_0_0_0, &t108_1_0_0, t108_IOs, &t108_GC, NULL, t108_FDVs, NULL, t108_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t108), 0, -1, sizeof(t108_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.BaseRaycaster>
static PropertyInfo t2267____Count_PropertyInfo = 
{
	&t2267_TI, "Count", &m26940_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26944_MI;
static PropertyInfo t2267____IsReadOnly_PropertyInfo = 
{
	&t2267_TI, "IsReadOnly", &m26944_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2267_PIs[] =
{
	&t2267____Count_PropertyInfo,
	&t2267____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26940_GM;
MethodInfo m26940_MI = 
{
	"get_Count", NULL, &t2267_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26940_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26944_GM;
MethodInfo m26944_MI = 
{
	"get_IsReadOnly", NULL, &t2267_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26944_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2267_m26945_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26945_GM;
MethodInfo m26945_MI = 
{
	"Add", NULL, &t2267_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2267_m26945_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26945_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26946_GM;
MethodInfo m26946_MI = 
{
	"Clear", NULL, &t2267_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26946_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2267_m26947_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26947_GM;
MethodInfo m26947_MI = 
{
	"Contains", NULL, &t2267_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2267_m26947_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26947_GM};
extern Il2CppType t2265_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2267_m26941_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2265_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26941_GM;
MethodInfo m26941_MI = 
{
	"CopyTo", NULL, &t2267_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2267_m26941_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26941_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2267_m26948_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26948_GM;
MethodInfo m26948_MI = 
{
	"Remove", NULL, &t2267_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2267_m26948_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26948_GM};
static MethodInfo* t2267_MIs[] =
{
	&m26940_MI,
	&m26944_MI,
	&m26945_MI,
	&m26946_MI,
	&m26947_MI,
	&m26941_MI,
	&m26948_MI,
	NULL
};
static TypeInfo* t2267_ITIs[] = 
{
	&t603_TI,
	&t2268_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2267_1_0_0;
struct t2267;
extern Il2CppGenericClass t2267_GC;
TypeInfo t2267_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2267_MIs, t2267_PIs, NULL, NULL, NULL, NULL, NULL, &t2267_TI, t2267_ITIs, NULL, &EmptyCustomAttributesCache, &t2267_TI, &t2267_0_0_0, &t2267_1_0_0, NULL, &t2267_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t2266_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26942_GM;
MethodInfo m26942_MI = 
{
	"GetEnumerator", NULL, &t2268_TI, &t2266_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26942_GM};
static MethodInfo* t2268_MIs[] =
{
	&m26942_MI,
	NULL
};
static TypeInfo* t2268_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2268_1_0_0;
struct t2268;
extern Il2CppGenericClass t2268_GC;
TypeInfo t2268_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2268_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2268_TI, t2268_ITIs, NULL, &EmptyCustomAttributesCache, &t2268_TI, &t2268_0_0_0, &t2268_1_0_0, NULL, &t2268_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.BaseRaycaster>
static PropertyInfo t2266____Current_PropertyInfo = 
{
	&t2266_TI, "Current", &m26943_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2266_PIs[] =
{
	&t2266____Current_PropertyInfo,
	NULL
};
extern Il2CppType t109_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26943_GM;
MethodInfo m26943_MI = 
{
	"get_Current", NULL, &t2266_TI, &t109_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26943_GM};
static MethodInfo* t2266_MIs[] =
{
	&m26943_MI,
	NULL
};
static TypeInfo* t2266_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2266_0_0_0;
extern Il2CppType t2266_1_0_0;
struct t2266;
extern Il2CppGenericClass t2266_GC;
TypeInfo t2266_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2266_MIs, t2266_PIs, NULL, NULL, NULL, NULL, NULL, &t2266_TI, t2266_ITIs, NULL, &EmptyCustomAttributesCache, &t2266_TI, &t2266_0_0_0, &t2266_1_0_0, NULL, &t2266_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2273.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2273_TI;
#include "t2273MD.h"

extern MethodInfo m11445_MI;
extern MethodInfo m20104_MI;
struct t20;
#define m20104(__this, p0, method) (t109 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t20_0_0_1;
FieldInfo t2273_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2273_TI, offsetof(t2273, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2273_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2273_TI, offsetof(t2273, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2273_FIs[] =
{
	&t2273_f0_FieldInfo,
	&t2273_f1_FieldInfo,
	NULL
};
extern MethodInfo m11442_MI;
static PropertyInfo t2273____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2273_TI, "System.Collections.IEnumerator.Current", &m11442_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2273____Current_PropertyInfo = 
{
	&t2273_TI, "Current", &m11445_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2273_PIs[] =
{
	&t2273____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2273____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2273_m11441_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11441_GM;
MethodInfo m11441_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2273_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2273_m11441_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11441_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11442_GM;
MethodInfo m11442_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2273_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11442_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11443_GM;
MethodInfo m11443_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2273_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11443_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11444_GM;
MethodInfo m11444_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2273_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11444_GM};
extern Il2CppType t109_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11445_GM;
MethodInfo m11445_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2273_TI, &t109_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11445_GM};
static MethodInfo* t2273_MIs[] =
{
	&m11441_MI,
	&m11442_MI,
	&m11443_MI,
	&m11444_MI,
	&m11445_MI,
	NULL
};
extern MethodInfo m11444_MI;
extern MethodInfo m11443_MI;
static MethodInfo* t2273_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11442_MI,
	&m11444_MI,
	&m11443_MI,
	&m11445_MI,
};
static TypeInfo* t2273_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2266_TI,
};
static Il2CppInterfaceOffsetPair t2273_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2266_TI, 7},
};
extern TypeInfo t109_TI;
static Il2CppRGCTXData t2273_RGCTXData[3] = 
{
	&m11445_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
	&m20104_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2273_0_0_0;
extern Il2CppType t2273_1_0_0;
extern Il2CppGenericClass t2273_GC;
TypeInfo t2273_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2273_MIs, t2273_PIs, t2273_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2273_TI, t2273_ITIs, t2273_VT, &EmptyCustomAttributesCache, &t2273_TI, &t2273_0_0_0, &t2273_1_0_0, t2273_IOs, &t2273_GC, NULL, NULL, NULL, t2273_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2273)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.BaseRaycaster>
extern MethodInfo m26949_MI;
extern MethodInfo m26950_MI;
static PropertyInfo t2274____Item_PropertyInfo = 
{
	&t2274_TI, "Item", &m26949_MI, &m26950_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2274_PIs[] =
{
	&t2274____Item_PropertyInfo,
	NULL
};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2274_m26951_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26951_GM;
MethodInfo m26951_MI = 
{
	"IndexOf", NULL, &t2274_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2274_m26951_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26951_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2274_m26952_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26952_GM;
MethodInfo m26952_MI = 
{
	"Insert", NULL, &t2274_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2274_m26952_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26952_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2274_m26953_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26953_GM;
MethodInfo m26953_MI = 
{
	"RemoveAt", NULL, &t2274_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2274_m26953_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26953_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2274_m26949_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t109_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26949_GM;
MethodInfo m26949_MI = 
{
	"get_Item", NULL, &t2274_TI, &t109_0_0_0, RuntimeInvoker_t29_t44, t2274_m26949_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26949_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2274_m26950_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26950_GM;
MethodInfo m26950_MI = 
{
	"set_Item", NULL, &t2274_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2274_m26950_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26950_GM};
static MethodInfo* t2274_MIs[] =
{
	&m26951_MI,
	&m26952_MI,
	&m26953_MI,
	&m26949_MI,
	&m26950_MI,
	NULL
};
static TypeInfo* t2274_ITIs[] = 
{
	&t603_TI,
	&t2267_TI,
	&t2268_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2274_0_0_0;
extern Il2CppType t2274_1_0_0;
struct t2274;
extern Il2CppGenericClass t2274_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2274_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2274_MIs, t2274_PIs, NULL, NULL, NULL, NULL, NULL, &t2274_TI, t2274_ITIs, NULL, &t1908__CustomAttributeCache, &t2274_TI, &t2274_0_0_0, &t2274_1_0_0, NULL, &t2274_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m11449_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t108_0_0_1;
FieldInfo t2272_f0_FieldInfo = 
{
	"l", &t108_0_0_1, &t2272_TI, offsetof(t2272, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2272_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2272_TI, offsetof(t2272, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2272_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2272_TI, offsetof(t2272, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t109_0_0_1;
FieldInfo t2272_f3_FieldInfo = 
{
	"current", &t109_0_0_1, &t2272_TI, offsetof(t2272, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2272_FIs[] =
{
	&t2272_f0_FieldInfo,
	&t2272_f1_FieldInfo,
	&t2272_f2_FieldInfo,
	&t2272_f3_FieldInfo,
	NULL
};
extern MethodInfo m11447_MI;
static PropertyInfo t2272____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2272_TI, "System.Collections.IEnumerator.Current", &m11447_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11451_MI;
static PropertyInfo t2272____Current_PropertyInfo = 
{
	&t2272_TI, "Current", &m11451_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2272_PIs[] =
{
	&t2272____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2272____Current_PropertyInfo,
	NULL
};
extern Il2CppType t108_0_0_0;
static ParameterInfo t2272_m11446_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t108_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11446_GM;
MethodInfo m11446_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2272_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2272_m11446_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11446_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11447_GM;
MethodInfo m11447_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2272_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11447_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11448_GM;
MethodInfo m11448_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2272_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11448_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11449_GM;
MethodInfo m11449_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2272_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11449_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11450_GM;
MethodInfo m11450_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2272_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11450_GM};
extern Il2CppType t109_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11451_GM;
MethodInfo m11451_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2272_TI, &t109_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11451_GM};
static MethodInfo* t2272_MIs[] =
{
	&m11446_MI,
	&m11447_MI,
	&m11448_MI,
	&m11449_MI,
	&m11450_MI,
	&m11451_MI,
	NULL
};
extern MethodInfo m11450_MI;
extern MethodInfo m11448_MI;
static MethodInfo* t2272_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11447_MI,
	&m11450_MI,
	&m11448_MI,
	&m11451_MI,
};
static TypeInfo* t2272_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2266_TI,
};
static Il2CppInterfaceOffsetPair t2272_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2266_TI, 7},
};
extern TypeInfo t109_TI;
extern TypeInfo t2272_TI;
static Il2CppRGCTXData t2272_RGCTXData[3] = 
{
	&m11449_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
	&t2272_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2272_0_0_0;
extern Il2CppType t2272_1_0_0;
extern Il2CppGenericClass t2272_GC;
TypeInfo t2272_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2272_MIs, t2272_PIs, t2272_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2272_TI, t2272_ITIs, t2272_VT, &EmptyCustomAttributesCache, &t2272_TI, &t2272_0_0_0, &t2272_1_0_0, t2272_IOs, &t2272_GC, NULL, NULL, NULL, t2272_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2272)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t2275MD.h"
extern MethodInfo m11481_MI;
extern MethodInfo m11513_MI;
extern MethodInfo m26947_MI;
extern MethodInfo m26951_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t2274_0_0_1;
FieldInfo t2269_f0_FieldInfo = 
{
	"list", &t2274_0_0_1, &t2269_TI, offsetof(t2269, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2269_FIs[] =
{
	&t2269_f0_FieldInfo,
	NULL
};
extern MethodInfo m11458_MI;
extern MethodInfo m11459_MI;
static PropertyInfo t2269____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2269_TI, "System.Collections.Generic.IList<T>.Item", &m11458_MI, &m11459_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11460_MI;
static PropertyInfo t2269____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2269_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11460_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11470_MI;
static PropertyInfo t2269____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2269_TI, "System.Collections.ICollection.IsSynchronized", &m11470_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11471_MI;
static PropertyInfo t2269____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2269_TI, "System.Collections.ICollection.SyncRoot", &m11471_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11472_MI;
static PropertyInfo t2269____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2269_TI, "System.Collections.IList.IsFixedSize", &m11472_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11473_MI;
static PropertyInfo t2269____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2269_TI, "System.Collections.IList.IsReadOnly", &m11473_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11474_MI;
extern MethodInfo m11475_MI;
static PropertyInfo t2269____System_Collections_IList_Item_PropertyInfo = 
{
	&t2269_TI, "System.Collections.IList.Item", &m11474_MI, &m11475_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11480_MI;
static PropertyInfo t2269____Count_PropertyInfo = 
{
	&t2269_TI, "Count", &m11480_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2269____Item_PropertyInfo = 
{
	&t2269_TI, "Item", &m11481_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2269_PIs[] =
{
	&t2269____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2269____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2269____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2269____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2269____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2269____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2269____System_Collections_IList_Item_PropertyInfo,
	&t2269____Count_PropertyInfo,
	&t2269____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2274_0_0_0;
static ParameterInfo t2269_m11452_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2274_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11452_GM;
MethodInfo m11452_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2269_m11452_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11452_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2269_m11453_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11453_GM;
MethodInfo m11453_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2269_m11453_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11453_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11454_GM;
MethodInfo m11454_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11454_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2269_m11455_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11455_GM;
MethodInfo m11455_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2269_m11455_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11455_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2269_m11456_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11456_GM;
MethodInfo m11456_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2269_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2269_m11456_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11456_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2269_m11457_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11457_GM;
MethodInfo m11457_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2269_m11457_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11457_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2269_m11458_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t109_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11458_GM;
MethodInfo m11458_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2269_TI, &t109_0_0_0, RuntimeInvoker_t29_t44, t2269_m11458_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11458_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2269_m11459_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11459_GM;
MethodInfo m11459_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2269_m11459_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11459_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11460_GM;
MethodInfo m11460_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2269_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11460_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2269_m11461_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11461_GM;
MethodInfo m11461_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2269_m11461_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11461_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11462_GM;
MethodInfo m11462_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2269_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11462_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2269_m11463_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11463_GM;
MethodInfo m11463_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2269_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2269_m11463_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11463_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11464_GM;
MethodInfo m11464_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11464_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2269_m11465_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11465_GM;
MethodInfo m11465_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2269_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2269_m11465_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11465_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2269_m11466_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11466_GM;
MethodInfo m11466_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2269_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2269_m11466_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11466_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2269_m11467_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11467_GM;
MethodInfo m11467_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2269_m11467_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11467_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2269_m11468_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11468_GM;
MethodInfo m11468_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2269_m11468_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11468_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2269_m11469_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11469_GM;
MethodInfo m11469_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2269_m11469_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11469_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11470_GM;
MethodInfo m11470_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2269_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11470_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11471_GM;
MethodInfo m11471_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2269_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11471_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11472_GM;
MethodInfo m11472_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2269_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11472_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11473_GM;
MethodInfo m11473_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2269_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11473_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2269_m11474_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11474_GM;
MethodInfo m11474_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2269_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2269_m11474_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11474_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2269_m11475_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11475_GM;
MethodInfo m11475_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2269_m11475_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11475_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2269_m11476_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11476_GM;
MethodInfo m11476_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2269_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2269_m11476_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11476_GM};
extern Il2CppType t2265_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2269_m11477_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2265_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11477_GM;
MethodInfo m11477_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2269_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2269_m11477_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11477_GM};
extern Il2CppType t2266_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11478_GM;
MethodInfo m11478_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2269_TI, &t2266_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11478_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2269_m11479_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11479_GM;
MethodInfo m11479_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2269_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2269_m11479_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11479_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11480_GM;
MethodInfo m11480_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2269_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11480_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2269_m11481_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t109_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11481_GM;
MethodInfo m11481_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2269_TI, &t109_0_0_0, RuntimeInvoker_t29_t44, t2269_m11481_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11481_GM};
static MethodInfo* t2269_MIs[] =
{
	&m11452_MI,
	&m11453_MI,
	&m11454_MI,
	&m11455_MI,
	&m11456_MI,
	&m11457_MI,
	&m11458_MI,
	&m11459_MI,
	&m11460_MI,
	&m11461_MI,
	&m11462_MI,
	&m11463_MI,
	&m11464_MI,
	&m11465_MI,
	&m11466_MI,
	&m11467_MI,
	&m11468_MI,
	&m11469_MI,
	&m11470_MI,
	&m11471_MI,
	&m11472_MI,
	&m11473_MI,
	&m11474_MI,
	&m11475_MI,
	&m11476_MI,
	&m11477_MI,
	&m11478_MI,
	&m11479_MI,
	&m11480_MI,
	&m11481_MI,
	NULL
};
extern MethodInfo m11462_MI;
extern MethodInfo m11461_MI;
extern MethodInfo m11463_MI;
extern MethodInfo m11464_MI;
extern MethodInfo m11465_MI;
extern MethodInfo m11466_MI;
extern MethodInfo m11467_MI;
extern MethodInfo m11468_MI;
extern MethodInfo m11469_MI;
extern MethodInfo m11453_MI;
extern MethodInfo m11454_MI;
extern MethodInfo m11476_MI;
extern MethodInfo m11477_MI;
extern MethodInfo m11456_MI;
extern MethodInfo m11479_MI;
extern MethodInfo m11455_MI;
extern MethodInfo m11457_MI;
extern MethodInfo m11478_MI;
static MethodInfo* t2269_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11462_MI,
	&m11480_MI,
	&m11470_MI,
	&m11471_MI,
	&m11461_MI,
	&m11472_MI,
	&m11473_MI,
	&m11474_MI,
	&m11475_MI,
	&m11463_MI,
	&m11464_MI,
	&m11465_MI,
	&m11466_MI,
	&m11467_MI,
	&m11468_MI,
	&m11469_MI,
	&m11480_MI,
	&m11460_MI,
	&m11453_MI,
	&m11454_MI,
	&m11476_MI,
	&m11477_MI,
	&m11456_MI,
	&m11479_MI,
	&m11455_MI,
	&m11457_MI,
	&m11458_MI,
	&m11459_MI,
	&m11478_MI,
	&m11481_MI,
};
static TypeInfo* t2269_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2267_TI,
	&t2274_TI,
	&t2268_TI,
};
static Il2CppInterfaceOffsetPair t2269_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2267_TI, 20},
	{ &t2274_TI, 27},
	{ &t2268_TI, 32},
};
extern TypeInfo t109_TI;
static Il2CppRGCTXData t2269_RGCTXData[9] = 
{
	&m11481_MI/* Method Usage */,
	&m11513_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
	&m26947_MI/* Method Usage */,
	&m26951_MI/* Method Usage */,
	&m26949_MI/* Method Usage */,
	&m26941_MI/* Method Usage */,
	&m26942_MI/* Method Usage */,
	&m26940_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2269_0_0_0;
extern Il2CppType t2269_1_0_0;
struct t2269;
extern Il2CppGenericClass t2269_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2269_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2269_MIs, t2269_PIs, t2269_FIs, NULL, &t29_TI, NULL, NULL, &t2269_TI, t2269_ITIs, t2269_VT, &t1263__CustomAttributeCache, &t2269_TI, &t2269_0_0_0, &t2269_1_0_0, t2269_IOs, &t2269_GC, NULL, NULL, NULL, t2269_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2269), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2275.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2275_TI;

extern MethodInfo m11516_MI;
extern MethodInfo m11517_MI;
extern MethodInfo m11514_MI;
extern MethodInfo m11512_MI;
extern MethodInfo m1381_MI;
extern MethodInfo m11505_MI;
extern MethodInfo m11515_MI;
extern MethodInfo m11503_MI;
extern MethodInfo m11508_MI;
extern MethodInfo m11499_MI;
extern MethodInfo m26946_MI;
extern MethodInfo m26952_MI;
extern MethodInfo m26953_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t2274_0_0_1;
FieldInfo t2275_f0_FieldInfo = 
{
	"list", &t2274_0_0_1, &t2275_TI, offsetof(t2275, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2275_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2275_TI, offsetof(t2275, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2275_FIs[] =
{
	&t2275_f0_FieldInfo,
	&t2275_f1_FieldInfo,
	NULL
};
extern MethodInfo m11483_MI;
static PropertyInfo t2275____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2275_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m11483_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11491_MI;
static PropertyInfo t2275____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2275_TI, "System.Collections.ICollection.IsSynchronized", &m11491_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11492_MI;
static PropertyInfo t2275____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2275_TI, "System.Collections.ICollection.SyncRoot", &m11492_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11493_MI;
static PropertyInfo t2275____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2275_TI, "System.Collections.IList.IsFixedSize", &m11493_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11494_MI;
static PropertyInfo t2275____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2275_TI, "System.Collections.IList.IsReadOnly", &m11494_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11495_MI;
extern MethodInfo m11496_MI;
static PropertyInfo t2275____System_Collections_IList_Item_PropertyInfo = 
{
	&t2275_TI, "System.Collections.IList.Item", &m11495_MI, &m11496_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11509_MI;
static PropertyInfo t2275____Count_PropertyInfo = 
{
	&t2275_TI, "Count", &m11509_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m11510_MI;
extern MethodInfo m11511_MI;
static PropertyInfo t2275____Item_PropertyInfo = 
{
	&t2275_TI, "Item", &m11510_MI, &m11511_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2275_PIs[] =
{
	&t2275____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2275____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2275____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2275____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2275____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2275____System_Collections_IList_Item_PropertyInfo,
	&t2275____Count_PropertyInfo,
	&t2275____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11482_GM;
MethodInfo m11482_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11482_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11483_GM;
MethodInfo m11483_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2275_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11483_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2275_m11484_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11484_GM;
MethodInfo m11484_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2275_m11484_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11484_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11485_GM;
MethodInfo m11485_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2275_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11485_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2275_m11486_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11486_GM;
MethodInfo m11486_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2275_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2275_m11486_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11486_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2275_m11487_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11487_GM;
MethodInfo m11487_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2275_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2275_m11487_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11487_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2275_m11488_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11488_GM;
MethodInfo m11488_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2275_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2275_m11488_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11488_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2275_m11489_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11489_GM;
MethodInfo m11489_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2275_m11489_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11489_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2275_m11490_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11490_GM;
MethodInfo m11490_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2275_m11490_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11490_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11491_GM;
MethodInfo m11491_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2275_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11491_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11492_GM;
MethodInfo m11492_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2275_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11492_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11493_GM;
MethodInfo m11493_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2275_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11493_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11494_GM;
MethodInfo m11494_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2275_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11494_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2275_m11495_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11495_GM;
MethodInfo m11495_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2275_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2275_m11495_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11495_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2275_m11496_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11496_GM;
MethodInfo m11496_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2275_m11496_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11496_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2275_m11497_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11497_GM;
MethodInfo m11497_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2275_m11497_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11497_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11498_GM;
MethodInfo m11498_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11498_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11499_GM;
MethodInfo m11499_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11499_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2275_m11500_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11500_GM;
MethodInfo m11500_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2275_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2275_m11500_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11500_GM};
extern Il2CppType t2265_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2275_m11501_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2265_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11501_GM;
MethodInfo m11501_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2275_m11501_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11501_GM};
extern Il2CppType t2266_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11502_GM;
MethodInfo m11502_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2275_TI, &t2266_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11502_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2275_m11503_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11503_GM;
MethodInfo m11503_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2275_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2275_m11503_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11503_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2275_m11504_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11504_GM;
MethodInfo m11504_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2275_m11504_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11504_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2275_m11505_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11505_GM;
MethodInfo m11505_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2275_m11505_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11505_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2275_m11506_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11506_GM;
MethodInfo m11506_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2275_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2275_m11506_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11506_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2275_m11507_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11507_GM;
MethodInfo m11507_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2275_m11507_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11507_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2275_m11508_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11508_GM;
MethodInfo m11508_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2275_m11508_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11508_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11509_GM;
MethodInfo m11509_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2275_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11509_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2275_m11510_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t109_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11510_GM;
MethodInfo m11510_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2275_TI, &t109_0_0_0, RuntimeInvoker_t29_t44, t2275_m11510_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11510_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2275_m11511_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11511_GM;
MethodInfo m11511_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2275_m11511_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11511_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2275_m11512_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11512_GM;
MethodInfo m11512_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2275_m11512_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11512_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2275_m11513_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11513_GM;
MethodInfo m11513_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2275_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2275_m11513_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11513_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2275_m11514_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t109_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11514_GM;
MethodInfo m11514_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2275_TI, &t109_0_0_0, RuntimeInvoker_t29_t29, t2275_m11514_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11514_GM};
extern Il2CppType t2274_0_0_0;
static ParameterInfo t2275_m11515_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2274_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11515_GM;
MethodInfo m11515_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2275_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2275_m11515_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11515_GM};
extern Il2CppType t2274_0_0_0;
static ParameterInfo t2275_m11516_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2274_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11516_GM;
MethodInfo m11516_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2275_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2275_m11516_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11516_GM};
extern Il2CppType t2274_0_0_0;
static ParameterInfo t2275_m11517_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2274_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11517_GM;
MethodInfo m11517_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2275_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2275_m11517_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11517_GM};
static MethodInfo* t2275_MIs[] =
{
	&m11482_MI,
	&m11483_MI,
	&m11484_MI,
	&m11485_MI,
	&m11486_MI,
	&m11487_MI,
	&m11488_MI,
	&m11489_MI,
	&m11490_MI,
	&m11491_MI,
	&m11492_MI,
	&m11493_MI,
	&m11494_MI,
	&m11495_MI,
	&m11496_MI,
	&m11497_MI,
	&m11498_MI,
	&m11499_MI,
	&m11500_MI,
	&m11501_MI,
	&m11502_MI,
	&m11503_MI,
	&m11504_MI,
	&m11505_MI,
	&m11506_MI,
	&m11507_MI,
	&m11508_MI,
	&m11509_MI,
	&m11510_MI,
	&m11511_MI,
	&m11512_MI,
	&m11513_MI,
	&m11514_MI,
	&m11515_MI,
	&m11516_MI,
	&m11517_MI,
	NULL
};
extern MethodInfo m11485_MI;
extern MethodInfo m11484_MI;
extern MethodInfo m11486_MI;
extern MethodInfo m11498_MI;
extern MethodInfo m11487_MI;
extern MethodInfo m11488_MI;
extern MethodInfo m11489_MI;
extern MethodInfo m11490_MI;
extern MethodInfo m11507_MI;
extern MethodInfo m11497_MI;
extern MethodInfo m11500_MI;
extern MethodInfo m11501_MI;
extern MethodInfo m11506_MI;
extern MethodInfo m11504_MI;
extern MethodInfo m11502_MI;
static MethodInfo* t2275_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11485_MI,
	&m11509_MI,
	&m11491_MI,
	&m11492_MI,
	&m11484_MI,
	&m11493_MI,
	&m11494_MI,
	&m11495_MI,
	&m11496_MI,
	&m11486_MI,
	&m11498_MI,
	&m11487_MI,
	&m11488_MI,
	&m11489_MI,
	&m11490_MI,
	&m11507_MI,
	&m11509_MI,
	&m11483_MI,
	&m11497_MI,
	&m11498_MI,
	&m11500_MI,
	&m11501_MI,
	&m11506_MI,
	&m11503_MI,
	&m11504_MI,
	&m11507_MI,
	&m11510_MI,
	&m11511_MI,
	&m11502_MI,
	&m11499_MI,
	&m11505_MI,
	&m11508_MI,
	&m11512_MI,
};
static TypeInfo* t2275_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2267_TI,
	&t2274_TI,
	&t2268_TI,
};
static Il2CppInterfaceOffsetPair t2275_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2267_TI, 20},
	{ &t2274_TI, 27},
	{ &t2268_TI, 32},
};
extern TypeInfo t108_TI;
extern TypeInfo t109_TI;
static Il2CppRGCTXData t2275_RGCTXData[25] = 
{
	&t108_TI/* Class Usage */,
	&m1381_MI/* Method Usage */,
	&m26944_MI/* Method Usage */,
	&m26942_MI/* Method Usage */,
	&m26940_MI/* Method Usage */,
	&m11514_MI/* Method Usage */,
	&m11505_MI/* Method Usage */,
	&m11513_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
	&m26947_MI/* Method Usage */,
	&m26951_MI/* Method Usage */,
	&m11515_MI/* Method Usage */,
	&m11503_MI/* Method Usage */,
	&m11508_MI/* Method Usage */,
	&m11516_MI/* Method Usage */,
	&m11517_MI/* Method Usage */,
	&m26949_MI/* Method Usage */,
	&m11512_MI/* Method Usage */,
	&m11499_MI/* Method Usage */,
	&m26946_MI/* Method Usage */,
	&m26941_MI/* Method Usage */,
	&m26952_MI/* Method Usage */,
	&m26953_MI/* Method Usage */,
	&m26950_MI/* Method Usage */,
	&t109_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2275_0_0_0;
extern Il2CppType t2275_1_0_0;
struct t2275;
extern Il2CppGenericClass t2275_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2275_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2275_MIs, t2275_PIs, t2275_FIs, NULL, &t29_TI, NULL, NULL, &t2275_TI, t2275_ITIs, t2275_VT, &t1262__CustomAttributeCache, &t2275_TI, &t2275_0_0_0, &t2275_1_0_0, t2275_IOs, &t2275_GC, NULL, NULL, NULL, t2275_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2275), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2276_TI;
#include "t2276MD.h"

#include "t2277.h"
extern TypeInfo t6636_TI;
extern TypeInfo t2277_TI;
#include "t2277MD.h"
extern Il2CppType t6636_0_0_0;
extern MethodInfo m11523_MI;
extern MethodInfo m26954_MI;
extern MethodInfo m20116_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t2276_0_0_49;
FieldInfo t2276_f0_FieldInfo = 
{
	"_default", &t2276_0_0_49, &t2276_TI, offsetof(t2276_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2276_FIs[] =
{
	&t2276_f0_FieldInfo,
	NULL
};
extern MethodInfo m11522_MI;
static PropertyInfo t2276____Default_PropertyInfo = 
{
	&t2276_TI, "Default", &m11522_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2276_PIs[] =
{
	&t2276____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11518_GM;
MethodInfo m11518_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2276_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11518_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11519_GM;
MethodInfo m11519_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2276_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11519_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2276_m11520_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11520_GM;
MethodInfo m11520_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2276_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2276_m11520_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11520_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2276_m11521_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11521_GM;
MethodInfo m11521_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2276_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2276_m11521_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11521_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2276_m26954_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26954_GM;
MethodInfo m26954_MI = 
{
	"GetHashCode", NULL, &t2276_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2276_m26954_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26954_GM};
extern Il2CppType t109_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2276_m20116_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20116_GM;
MethodInfo m20116_MI = 
{
	"Equals", NULL, &t2276_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2276_m20116_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20116_GM};
extern Il2CppType t2276_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11522_GM;
MethodInfo m11522_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2276_TI, &t2276_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11522_GM};
static MethodInfo* t2276_MIs[] =
{
	&m11518_MI,
	&m11519_MI,
	&m11520_MI,
	&m11521_MI,
	&m26954_MI,
	&m20116_MI,
	&m11522_MI,
	NULL
};
extern MethodInfo m11521_MI;
extern MethodInfo m11520_MI;
static MethodInfo* t2276_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m20116_MI,
	&m26954_MI,
	&m11521_MI,
	&m11520_MI,
	NULL,
	NULL,
};
extern TypeInfo t6637_TI;
static TypeInfo* t2276_ITIs[] = 
{
	&t6637_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2276_IOs[] = 
{
	{ &t6637_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2276_TI;
extern TypeInfo t2276_TI;
extern TypeInfo t2277_TI;
extern TypeInfo t109_TI;
static Il2CppRGCTXData t2276_RGCTXData[9] = 
{
	&t6636_0_0_0/* Type Usage */,
	&t109_0_0_0/* Type Usage */,
	&t2276_TI/* Class Usage */,
	&t2276_TI/* Static Usage */,
	&t2277_TI/* Class Usage */,
	&m11523_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
	&m26954_MI/* Method Usage */,
	&m20116_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2276_0_0_0;
extern Il2CppType t2276_1_0_0;
struct t2276;
extern Il2CppGenericClass t2276_GC;
TypeInfo t2276_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2276_MIs, t2276_PIs, t2276_FIs, NULL, &t29_TI, NULL, NULL, &t2276_TI, t2276_ITIs, t2276_VT, &EmptyCustomAttributesCache, &t2276_TI, &t2276_0_0_0, &t2276_1_0_0, t2276_IOs, &t2276_GC, NULL, NULL, NULL, t2276_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2276), 0, -1, sizeof(t2276_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t109_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t6637_m26955_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26955_GM;
MethodInfo m26955_MI = 
{
	"Equals", NULL, &t6637_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6637_m26955_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26955_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t6637_m26956_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26956_GM;
MethodInfo m26956_MI = 
{
	"GetHashCode", NULL, &t6637_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6637_m26956_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26956_GM};
static MethodInfo* t6637_MIs[] =
{
	&m26955_MI,
	&m26956_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6637_0_0_0;
extern Il2CppType t6637_1_0_0;
struct t6637;
extern Il2CppGenericClass t6637_GC;
TypeInfo t6637_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6637_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6637_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6637_TI, &t6637_0_0_0, &t6637_1_0_0, NULL, &t6637_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t109_0_0_0;
static ParameterInfo t6636_m26957_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26957_GM;
MethodInfo m26957_MI = 
{
	"Equals", NULL, &t6636_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6636_m26957_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26957_GM};
static MethodInfo* t6636_MIs[] =
{
	&m26957_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6636_1_0_0;
struct t6636;
extern Il2CppGenericClass t6636_GC;
TypeInfo t6636_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6636_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6636_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6636_TI, &t6636_0_0_0, &t6636_1_0_0, NULL, &t6636_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m11518_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11523_GM;
MethodInfo m11523_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2277_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11523_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2277_m11524_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11524_GM;
MethodInfo m11524_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2277_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2277_m11524_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11524_GM};
extern Il2CppType t109_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2277_m11525_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11525_GM;
MethodInfo m11525_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2277_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2277_m11525_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11525_GM};
static MethodInfo* t2277_MIs[] =
{
	&m11523_MI,
	&m11524_MI,
	&m11525_MI,
	NULL
};
extern MethodInfo m11525_MI;
extern MethodInfo m11524_MI;
static MethodInfo* t2277_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11525_MI,
	&m11524_MI,
	&m11521_MI,
	&m11520_MI,
	&m11524_MI,
	&m11525_MI,
};
static Il2CppInterfaceOffsetPair t2277_IOs[] = 
{
	{ &t6637_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2276_TI;
extern TypeInfo t2276_TI;
extern TypeInfo t2277_TI;
extern TypeInfo t109_TI;
extern TypeInfo t109_TI;
static Il2CppRGCTXData t2277_RGCTXData[11] = 
{
	&t6636_0_0_0/* Type Usage */,
	&t109_0_0_0/* Type Usage */,
	&t2276_TI/* Class Usage */,
	&t2276_TI/* Static Usage */,
	&t2277_TI/* Class Usage */,
	&m11523_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
	&m26954_MI/* Method Usage */,
	&m20116_MI/* Method Usage */,
	&m11518_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2277_0_0_0;
extern Il2CppType t2277_1_0_0;
struct t2277;
extern Il2CppGenericClass t2277_GC;
TypeInfo t2277_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2277_MIs, NULL, NULL, NULL, &t2276_TI, NULL, &t1256_TI, &t2277_TI, NULL, t2277_VT, &EmptyCustomAttributesCache, &t2277_TI, &t2277_0_0_0, &t2277_1_0_0, t2277_IOs, &t2277_GC, NULL, NULL, NULL, t2277_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2277), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2270_m11526_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11526_GM;
MethodInfo m11526_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2270_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2270_m11526_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11526_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2270_m11527_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11527_GM;
MethodInfo m11527_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2270_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2270_m11527_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11527_GM};
extern Il2CppType t109_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2270_m11528_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11528_GM;
MethodInfo m11528_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2270_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2270_m11528_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m11528_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2270_m11529_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11529_GM;
MethodInfo m11529_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2270_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2270_m11529_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11529_GM};
static MethodInfo* t2270_MIs[] =
{
	&m11526_MI,
	&m11527_MI,
	&m11528_MI,
	&m11529_MI,
	NULL
};
extern MethodInfo m11528_MI;
extern MethodInfo m11529_MI;
static MethodInfo* t2270_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11527_MI,
	&m11528_MI,
	&m11529_MI,
};
static Il2CppInterfaceOffsetPair t2270_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2270_1_0_0;
struct t2270;
extern Il2CppGenericClass t2270_GC;
TypeInfo t2270_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2270_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2270_TI, NULL, t2270_VT, &EmptyCustomAttributesCache, &t2270_TI, &t2270_0_0_0, &t2270_1_0_0, t2270_IOs, &t2270_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2270), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t2279.h"
extern TypeInfo t4040_TI;
extern TypeInfo t2279_TI;
#include "t2279MD.h"
extern Il2CppType t4040_0_0_0;
extern MethodInfo m11534_MI;
extern MethodInfo m26958_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t2278_0_0_49;
FieldInfo t2278_f0_FieldInfo = 
{
	"_default", &t2278_0_0_49, &t2278_TI, offsetof(t2278_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2278_FIs[] =
{
	&t2278_f0_FieldInfo,
	NULL
};
static PropertyInfo t2278____Default_PropertyInfo = 
{
	&t2278_TI, "Default", &m11533_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2278_PIs[] =
{
	&t2278____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11530_GM;
MethodInfo m11530_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2278_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11530_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11531_GM;
MethodInfo m11531_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2278_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11531_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2278_m11532_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11532_GM;
MethodInfo m11532_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2278_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2278_m11532_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11532_GM};
extern Il2CppType t109_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2278_m26958_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26958_GM;
MethodInfo m26958_MI = 
{
	"Compare", NULL, &t2278_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2278_m26958_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26958_GM};
extern Il2CppType t2278_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11533_GM;
MethodInfo m11533_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2278_TI, &t2278_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11533_GM};
static MethodInfo* t2278_MIs[] =
{
	&m11530_MI,
	&m11531_MI,
	&m11532_MI,
	&m26958_MI,
	&m11533_MI,
	NULL
};
extern MethodInfo m11532_MI;
static MethodInfo* t2278_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m26958_MI,
	&m11532_MI,
	NULL,
};
extern TypeInfo t4039_TI;
static TypeInfo* t2278_ITIs[] = 
{
	&t4039_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2278_IOs[] = 
{
	{ &t4039_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2278_TI;
extern TypeInfo t2278_TI;
extern TypeInfo t2279_TI;
extern TypeInfo t109_TI;
static Il2CppRGCTXData t2278_RGCTXData[8] = 
{
	&t4040_0_0_0/* Type Usage */,
	&t109_0_0_0/* Type Usage */,
	&t2278_TI/* Class Usage */,
	&t2278_TI/* Static Usage */,
	&t2279_TI/* Class Usage */,
	&m11534_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
	&m26958_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2278_0_0_0;
extern Il2CppType t2278_1_0_0;
struct t2278;
extern Il2CppGenericClass t2278_GC;
TypeInfo t2278_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2278_MIs, t2278_PIs, t2278_FIs, NULL, &t29_TI, NULL, NULL, &t2278_TI, t2278_ITIs, t2278_VT, &EmptyCustomAttributesCache, &t2278_TI, &t2278_0_0_0, &t2278_1_0_0, t2278_IOs, &t2278_GC, NULL, NULL, NULL, t2278_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2278), 0, -1, sizeof(t2278_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t109_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t4039_m20124_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20124_GM;
MethodInfo m20124_MI = 
{
	"Compare", NULL, &t4039_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4039_m20124_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20124_GM};
static MethodInfo* t4039_MIs[] =
{
	&m20124_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4039_0_0_0;
extern Il2CppType t4039_1_0_0;
struct t4039;
extern Il2CppGenericClass t4039_GC;
TypeInfo t4039_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4039_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4039_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4039_TI, &t4039_0_0_0, &t4039_1_0_0, NULL, &t4039_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t109_0_0_0;
static ParameterInfo t4040_m20125_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20125_GM;
MethodInfo m20125_MI = 
{
	"CompareTo", NULL, &t4040_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4040_m20125_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20125_GM};
static MethodInfo* t4040_MIs[] =
{
	&m20125_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4040_1_0_0;
struct t4040;
extern Il2CppGenericClass t4040_GC;
TypeInfo t4040_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4040_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4040_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4040_TI, &t4040_0_0_0, &t4040_1_0_0, NULL, &t4040_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m11530_MI;
extern MethodInfo m20125_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11534_GM;
MethodInfo m11534_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2279_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11534_GM};
extern Il2CppType t109_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2279_m11535_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11535_GM;
MethodInfo m11535_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2279_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2279_m11535_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11535_GM};
static MethodInfo* t2279_MIs[] =
{
	&m11534_MI,
	&m11535_MI,
	NULL
};
extern MethodInfo m11535_MI;
static MethodInfo* t2279_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m11535_MI,
	&m11532_MI,
	&m11535_MI,
};
static Il2CppInterfaceOffsetPair t2279_IOs[] = 
{
	{ &t4039_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2278_TI;
extern TypeInfo t2278_TI;
extern TypeInfo t2279_TI;
extern TypeInfo t109_TI;
extern TypeInfo t109_TI;
extern TypeInfo t4040_TI;
static Il2CppRGCTXData t2279_RGCTXData[12] = 
{
	&t4040_0_0_0/* Type Usage */,
	&t109_0_0_0/* Type Usage */,
	&t2278_TI/* Class Usage */,
	&t2278_TI/* Static Usage */,
	&t2279_TI/* Class Usage */,
	&m11534_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
	&m26958_MI/* Method Usage */,
	&m11530_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
	&t4040_TI/* Class Usage */,
	&m20125_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2279_0_0_0;
extern Il2CppType t2279_1_0_0;
struct t2279;
extern Il2CppGenericClass t2279_GC;
TypeInfo t2279_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2279_MIs, NULL, NULL, NULL, &t2278_TI, NULL, &t1246_TI, &t2279_TI, NULL, t2279_VT, &EmptyCustomAttributesCache, &t2279_TI, &t2279_0_0_0, &t2279_1_0_0, t2279_IOs, &t2279_GC, NULL, NULL, NULL, t2279_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2279), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2271_TI;
#include "t2271MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2271_m11536_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11536_GM;
MethodInfo m11536_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2271_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2271_m11536_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11536_GM};
extern Il2CppType t109_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2271_m11537_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11537_GM;
MethodInfo m11537_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2271_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2271_m11537_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m11537_GM};
extern Il2CppType t109_0_0_0;
extern Il2CppType t109_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2271_m11538_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11538_GM;
MethodInfo m11538_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2271_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2271_m11538_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m11538_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2271_m11539_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11539_GM;
MethodInfo m11539_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2271_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2271_m11539_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11539_GM};
static MethodInfo* t2271_MIs[] =
{
	&m11536_MI,
	&m11537_MI,
	&m11538_MI,
	&m11539_MI,
	NULL
};
extern MethodInfo m11537_MI;
extern MethodInfo m11538_MI;
extern MethodInfo m11539_MI;
static MethodInfo* t2271_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m11537_MI,
	&m11538_MI,
	&m11539_MI,
};
static Il2CppInterfaceOffsetPair t2271_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2271_1_0_0;
struct t2271;
extern Il2CppGenericClass t2271_GC;
TypeInfo t2271_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2271_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2271_TI, NULL, t2271_VT, &EmptyCustomAttributesCache, &t2271_TI, &t2271_0_0_0, &t2271_1_0_0, t2271_IOs, &t2271_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2271), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4042_TI;

#include "t62.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.EventTrigger>
extern MethodInfo m26959_MI;
static PropertyInfo t4042____Current_PropertyInfo = 
{
	&t4042_TI, "Current", &m26959_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4042_PIs[] =
{
	&t4042____Current_PropertyInfo,
	NULL
};
extern Il2CppType t62_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26959_GM;
MethodInfo m26959_MI = 
{
	"get_Current", NULL, &t4042_TI, &t62_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26959_GM};
static MethodInfo* t4042_MIs[] =
{
	&m26959_MI,
	NULL
};
static TypeInfo* t4042_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4042_0_0_0;
extern Il2CppType t4042_1_0_0;
struct t4042;
extern Il2CppGenericClass t4042_GC;
TypeInfo t4042_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4042_MIs, t4042_PIs, NULL, NULL, NULL, NULL, NULL, &t4042_TI, t4042_ITIs, NULL, &EmptyCustomAttributesCache, &t4042_TI, &t4042_0_0_0, &t4042_1_0_0, NULL, &t4042_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2280.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2280_TI;
#include "t2280MD.h"

extern TypeInfo t62_TI;
extern MethodInfo m11544_MI;
extern MethodInfo m20130_MI;
struct t20;
#define m20130(__this, p0, method) (t62 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTrigger>
extern Il2CppType t20_0_0_1;
FieldInfo t2280_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2280_TI, offsetof(t2280, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2280_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2280_TI, offsetof(t2280, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2280_FIs[] =
{
	&t2280_f0_FieldInfo,
	&t2280_f1_FieldInfo,
	NULL
};
extern MethodInfo m11541_MI;
static PropertyInfo t2280____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2280_TI, "System.Collections.IEnumerator.Current", &m11541_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2280____Current_PropertyInfo = 
{
	&t2280_TI, "Current", &m11544_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2280_PIs[] =
{
	&t2280____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2280____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2280_m11540_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11540_GM;
MethodInfo m11540_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2280_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2280_m11540_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11540_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11541_GM;
MethodInfo m11541_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2280_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11541_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11542_GM;
MethodInfo m11542_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2280_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11542_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11543_GM;
MethodInfo m11543_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2280_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11543_GM};
extern Il2CppType t62_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11544_GM;
MethodInfo m11544_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2280_TI, &t62_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11544_GM};
static MethodInfo* t2280_MIs[] =
{
	&m11540_MI,
	&m11541_MI,
	&m11542_MI,
	&m11543_MI,
	&m11544_MI,
	NULL
};
extern MethodInfo m11543_MI;
extern MethodInfo m11542_MI;
static MethodInfo* t2280_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11541_MI,
	&m11543_MI,
	&m11542_MI,
	&m11544_MI,
};
static TypeInfo* t2280_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4042_TI,
};
static Il2CppInterfaceOffsetPair t2280_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4042_TI, 7},
};
extern TypeInfo t62_TI;
static Il2CppRGCTXData t2280_RGCTXData[3] = 
{
	&m11544_MI/* Method Usage */,
	&t62_TI/* Class Usage */,
	&m20130_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2280_0_0_0;
extern Il2CppType t2280_1_0_0;
extern Il2CppGenericClass t2280_GC;
TypeInfo t2280_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2280_MIs, t2280_PIs, t2280_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2280_TI, t2280_ITIs, t2280_VT, &EmptyCustomAttributesCache, &t2280_TI, &t2280_0_0_0, &t2280_1_0_0, t2280_IOs, &t2280_GC, NULL, NULL, NULL, t2280_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2280)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5178_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.EventTrigger>
extern MethodInfo m26960_MI;
static PropertyInfo t5178____Count_PropertyInfo = 
{
	&t5178_TI, "Count", &m26960_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26961_MI;
static PropertyInfo t5178____IsReadOnly_PropertyInfo = 
{
	&t5178_TI, "IsReadOnly", &m26961_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5178_PIs[] =
{
	&t5178____Count_PropertyInfo,
	&t5178____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26960_GM;
MethodInfo m26960_MI = 
{
	"get_Count", NULL, &t5178_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26960_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26961_GM;
MethodInfo m26961_MI = 
{
	"get_IsReadOnly", NULL, &t5178_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26961_GM};
extern Il2CppType t62_0_0_0;
extern Il2CppType t62_0_0_0;
static ParameterInfo t5178_m26962_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t62_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26962_GM;
MethodInfo m26962_MI = 
{
	"Add", NULL, &t5178_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5178_m26962_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26962_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26963_GM;
MethodInfo m26963_MI = 
{
	"Clear", NULL, &t5178_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26963_GM};
extern Il2CppType t62_0_0_0;
static ParameterInfo t5178_m26964_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t62_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26964_GM;
MethodInfo m26964_MI = 
{
	"Contains", NULL, &t5178_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5178_m26964_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26964_GM};
extern Il2CppType t3803_0_0_0;
extern Il2CppType t3803_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5178_m26965_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3803_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26965_GM;
MethodInfo m26965_MI = 
{
	"CopyTo", NULL, &t5178_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5178_m26965_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26965_GM};
extern Il2CppType t62_0_0_0;
static ParameterInfo t5178_m26966_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t62_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26966_GM;
MethodInfo m26966_MI = 
{
	"Remove", NULL, &t5178_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5178_m26966_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26966_GM};
static MethodInfo* t5178_MIs[] =
{
	&m26960_MI,
	&m26961_MI,
	&m26962_MI,
	&m26963_MI,
	&m26964_MI,
	&m26965_MI,
	&m26966_MI,
	NULL
};
extern TypeInfo t5180_TI;
static TypeInfo* t5178_ITIs[] = 
{
	&t603_TI,
	&t5180_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5178_0_0_0;
extern Il2CppType t5178_1_0_0;
struct t5178;
extern Il2CppGenericClass t5178_GC;
TypeInfo t5178_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5178_MIs, t5178_PIs, NULL, NULL, NULL, NULL, NULL, &t5178_TI, t5178_ITIs, NULL, &EmptyCustomAttributesCache, &t5178_TI, &t5178_0_0_0, &t5178_1_0_0, NULL, &t5178_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.EventTrigger>
extern Il2CppType t4042_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26967_GM;
MethodInfo m26967_MI = 
{
	"GetEnumerator", NULL, &t5180_TI, &t4042_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26967_GM};
static MethodInfo* t5180_MIs[] =
{
	&m26967_MI,
	NULL
};
static TypeInfo* t5180_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5180_0_0_0;
extern Il2CppType t5180_1_0_0;
struct t5180;
extern Il2CppGenericClass t5180_GC;
TypeInfo t5180_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5180_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5180_TI, t5180_ITIs, NULL, &EmptyCustomAttributesCache, &t5180_TI, &t5180_0_0_0, &t5180_1_0_0, NULL, &t5180_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5179_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.EventTrigger>
extern MethodInfo m26968_MI;
extern MethodInfo m26969_MI;
static PropertyInfo t5179____Item_PropertyInfo = 
{
	&t5179_TI, "Item", &m26968_MI, &m26969_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5179_PIs[] =
{
	&t5179____Item_PropertyInfo,
	NULL
};
extern Il2CppType t62_0_0_0;
static ParameterInfo t5179_m26970_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t62_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26970_GM;
MethodInfo m26970_MI = 
{
	"IndexOf", NULL, &t5179_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5179_m26970_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26970_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t62_0_0_0;
static ParameterInfo t5179_m26971_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t62_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26971_GM;
MethodInfo m26971_MI = 
{
	"Insert", NULL, &t5179_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5179_m26971_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26971_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5179_m26972_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26972_GM;
MethodInfo m26972_MI = 
{
	"RemoveAt", NULL, &t5179_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5179_m26972_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26972_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5179_m26968_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t62_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26968_GM;
MethodInfo m26968_MI = 
{
	"get_Item", NULL, &t5179_TI, &t62_0_0_0, RuntimeInvoker_t29_t44, t5179_m26968_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26968_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t62_0_0_0;
static ParameterInfo t5179_m26969_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t62_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26969_GM;
MethodInfo m26969_MI = 
{
	"set_Item", NULL, &t5179_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5179_m26969_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26969_GM};
static MethodInfo* t5179_MIs[] =
{
	&m26970_MI,
	&m26971_MI,
	&m26972_MI,
	&m26968_MI,
	&m26969_MI,
	NULL
};
static TypeInfo* t5179_ITIs[] = 
{
	&t603_TI,
	&t5178_TI,
	&t5180_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5179_0_0_0;
extern Il2CppType t5179_1_0_0;
struct t5179;
extern Il2CppGenericClass t5179_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5179_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5179_MIs, t5179_PIs, NULL, NULL, NULL, NULL, NULL, &t5179_TI, t5179_ITIs, NULL, &t1908__CustomAttributeCache, &t5179_TI, &t5179_0_0_0, &t5179_1_0_0, NULL, &t5179_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5181_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerDownHandler>
extern MethodInfo m26973_MI;
static PropertyInfo t5181____Count_PropertyInfo = 
{
	&t5181_TI, "Count", &m26973_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26974_MI;
static PropertyInfo t5181____IsReadOnly_PropertyInfo = 
{
	&t5181_TI, "IsReadOnly", &m26974_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5181_PIs[] =
{
	&t5181____Count_PropertyInfo,
	&t5181____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26973_GM;
MethodInfo m26973_MI = 
{
	"get_Count", NULL, &t5181_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26973_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26974_GM;
MethodInfo m26974_MI = 
{
	"get_IsReadOnly", NULL, &t5181_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26974_GM};
extern Il2CppType t89_0_0_0;
extern Il2CppType t89_0_0_0;
static ParameterInfo t5181_m26975_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t89_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26975_GM;
MethodInfo m26975_MI = 
{
	"Add", NULL, &t5181_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5181_m26975_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26975_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26976_GM;
MethodInfo m26976_MI = 
{
	"Clear", NULL, &t5181_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26976_GM};
extern Il2CppType t89_0_0_0;
static ParameterInfo t5181_m26977_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t89_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26977_GM;
MethodInfo m26977_MI = 
{
	"Contains", NULL, &t5181_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5181_m26977_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26977_GM};
extern Il2CppType t3804_0_0_0;
extern Il2CppType t3804_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5181_m26978_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3804_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26978_GM;
MethodInfo m26978_MI = 
{
	"CopyTo", NULL, &t5181_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5181_m26978_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26978_GM};
extern Il2CppType t89_0_0_0;
static ParameterInfo t5181_m26979_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t89_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26979_GM;
MethodInfo m26979_MI = 
{
	"Remove", NULL, &t5181_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5181_m26979_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26979_GM};
static MethodInfo* t5181_MIs[] =
{
	&m26973_MI,
	&m26974_MI,
	&m26975_MI,
	&m26976_MI,
	&m26977_MI,
	&m26978_MI,
	&m26979_MI,
	NULL
};
extern TypeInfo t5183_TI;
static TypeInfo* t5181_ITIs[] = 
{
	&t603_TI,
	&t5183_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5181_0_0_0;
extern Il2CppType t5181_1_0_0;
struct t5181;
extern Il2CppGenericClass t5181_GC;
TypeInfo t5181_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5181_MIs, t5181_PIs, NULL, NULL, NULL, NULL, NULL, &t5181_TI, t5181_ITIs, NULL, &EmptyCustomAttributesCache, &t5181_TI, &t5181_0_0_0, &t5181_1_0_0, NULL, &t5181_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerDownHandler>
extern Il2CppType t4044_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26980_GM;
MethodInfo m26980_MI = 
{
	"GetEnumerator", NULL, &t5183_TI, &t4044_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26980_GM};
static MethodInfo* t5183_MIs[] =
{
	&m26980_MI,
	NULL
};
static TypeInfo* t5183_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5183_0_0_0;
extern Il2CppType t5183_1_0_0;
struct t5183;
extern Il2CppGenericClass t5183_GC;
TypeInfo t5183_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5183_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5183_TI, t5183_ITIs, NULL, &EmptyCustomAttributesCache, &t5183_TI, &t5183_0_0_0, &t5183_1_0_0, NULL, &t5183_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4044_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>
extern MethodInfo m26981_MI;
static PropertyInfo t4044____Current_PropertyInfo = 
{
	&t4044_TI, "Current", &m26981_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4044_PIs[] =
{
	&t4044____Current_PropertyInfo,
	NULL
};
extern Il2CppType t89_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26981_GM;
MethodInfo m26981_MI = 
{
	"get_Current", NULL, &t4044_TI, &t89_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26981_GM};
static MethodInfo* t4044_MIs[] =
{
	&m26981_MI,
	NULL
};
static TypeInfo* t4044_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4044_0_0_0;
extern Il2CppType t4044_1_0_0;
struct t4044;
extern Il2CppGenericClass t4044_GC;
TypeInfo t4044_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4044_MIs, t4044_PIs, NULL, NULL, NULL, NULL, NULL, &t4044_TI, t4044_ITIs, NULL, &EmptyCustomAttributesCache, &t4044_TI, &t4044_0_0_0, &t4044_1_0_0, NULL, &t4044_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2281.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2281_TI;
#include "t2281MD.h"

extern TypeInfo t89_TI;
extern MethodInfo m11549_MI;
extern MethodInfo m20141_MI;
struct t20;
#define m20141(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerDownHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2281_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2281_TI, offsetof(t2281, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2281_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2281_TI, offsetof(t2281, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2281_FIs[] =
{
	&t2281_f0_FieldInfo,
	&t2281_f1_FieldInfo,
	NULL
};
extern MethodInfo m11546_MI;
static PropertyInfo t2281____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2281_TI, "System.Collections.IEnumerator.Current", &m11546_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2281____Current_PropertyInfo = 
{
	&t2281_TI, "Current", &m11549_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2281_PIs[] =
{
	&t2281____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2281____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2281_m11545_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11545_GM;
MethodInfo m11545_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2281_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2281_m11545_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11545_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11546_GM;
MethodInfo m11546_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2281_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11546_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11547_GM;
MethodInfo m11547_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2281_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11547_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11548_GM;
MethodInfo m11548_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2281_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11548_GM};
extern Il2CppType t89_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11549_GM;
MethodInfo m11549_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2281_TI, &t89_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11549_GM};
static MethodInfo* t2281_MIs[] =
{
	&m11545_MI,
	&m11546_MI,
	&m11547_MI,
	&m11548_MI,
	&m11549_MI,
	NULL
};
extern MethodInfo m11548_MI;
extern MethodInfo m11547_MI;
static MethodInfo* t2281_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11546_MI,
	&m11548_MI,
	&m11547_MI,
	&m11549_MI,
};
static TypeInfo* t2281_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4044_TI,
};
static Il2CppInterfaceOffsetPair t2281_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4044_TI, 7},
};
extern TypeInfo t89_TI;
static Il2CppRGCTXData t2281_RGCTXData[3] = 
{
	&m11549_MI/* Method Usage */,
	&t89_TI/* Class Usage */,
	&m20141_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2281_0_0_0;
extern Il2CppType t2281_1_0_0;
extern Il2CppGenericClass t2281_GC;
TypeInfo t2281_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2281_MIs, t2281_PIs, t2281_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2281_TI, t2281_ITIs, t2281_VT, &EmptyCustomAttributesCache, &t2281_TI, &t2281_0_0_0, &t2281_1_0_0, t2281_IOs, &t2281_GC, NULL, NULL, NULL, t2281_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2281)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5182_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerDownHandler>
extern MethodInfo m26982_MI;
extern MethodInfo m26983_MI;
static PropertyInfo t5182____Item_PropertyInfo = 
{
	&t5182_TI, "Item", &m26982_MI, &m26983_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5182_PIs[] =
{
	&t5182____Item_PropertyInfo,
	NULL
};
extern Il2CppType t89_0_0_0;
static ParameterInfo t5182_m26984_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t89_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26984_GM;
MethodInfo m26984_MI = 
{
	"IndexOf", NULL, &t5182_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5182_m26984_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26984_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t89_0_0_0;
static ParameterInfo t5182_m26985_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t89_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26985_GM;
MethodInfo m26985_MI = 
{
	"Insert", NULL, &t5182_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5182_m26985_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26985_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5182_m26986_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26986_GM;
MethodInfo m26986_MI = 
{
	"RemoveAt", NULL, &t5182_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5182_m26986_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26986_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5182_m26982_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t89_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26982_GM;
MethodInfo m26982_MI = 
{
	"get_Item", NULL, &t5182_TI, &t89_0_0_0, RuntimeInvoker_t29_t44, t5182_m26982_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26982_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t89_0_0_0;
static ParameterInfo t5182_m26983_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t89_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26983_GM;
MethodInfo m26983_MI = 
{
	"set_Item", NULL, &t5182_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5182_m26983_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26983_GM};
static MethodInfo* t5182_MIs[] =
{
	&m26984_MI,
	&m26985_MI,
	&m26986_MI,
	&m26982_MI,
	&m26983_MI,
	NULL
};
static TypeInfo* t5182_ITIs[] = 
{
	&t603_TI,
	&t5181_TI,
	&t5183_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5182_0_0_0;
extern Il2CppType t5182_1_0_0;
struct t5182;
extern Il2CppGenericClass t5182_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5182_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5182_MIs, t5182_PIs, NULL, NULL, NULL, NULL, NULL, &t5182_TI, t5182_ITIs, NULL, &t1908__CustomAttributeCache, &t5182_TI, &t5182_0_0_0, &t5182_1_0_0, NULL, &t5182_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5184_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerUpHandler>
extern MethodInfo m26987_MI;
static PropertyInfo t5184____Count_PropertyInfo = 
{
	&t5184_TI, "Count", &m26987_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m26988_MI;
static PropertyInfo t5184____IsReadOnly_PropertyInfo = 
{
	&t5184_TI, "IsReadOnly", &m26988_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5184_PIs[] =
{
	&t5184____Count_PropertyInfo,
	&t5184____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26987_GM;
MethodInfo m26987_MI = 
{
	"get_Count", NULL, &t5184_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26987_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26988_GM;
MethodInfo m26988_MI = 
{
	"get_IsReadOnly", NULL, &t5184_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26988_GM};
extern Il2CppType t90_0_0_0;
extern Il2CppType t90_0_0_0;
static ParameterInfo t5184_m26989_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t90_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26989_GM;
MethodInfo m26989_MI = 
{
	"Add", NULL, &t5184_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5184_m26989_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26989_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26990_GM;
MethodInfo m26990_MI = 
{
	"Clear", NULL, &t5184_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26990_GM};
extern Il2CppType t90_0_0_0;
static ParameterInfo t5184_m26991_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t90_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26991_GM;
MethodInfo m26991_MI = 
{
	"Contains", NULL, &t5184_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5184_m26991_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26991_GM};
extern Il2CppType t3805_0_0_0;
extern Il2CppType t3805_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5184_m26992_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3805_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26992_GM;
MethodInfo m26992_MI = 
{
	"CopyTo", NULL, &t5184_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5184_m26992_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26992_GM};
extern Il2CppType t90_0_0_0;
static ParameterInfo t5184_m26993_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t90_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26993_GM;
MethodInfo m26993_MI = 
{
	"Remove", NULL, &t5184_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5184_m26993_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26993_GM};
static MethodInfo* t5184_MIs[] =
{
	&m26987_MI,
	&m26988_MI,
	&m26989_MI,
	&m26990_MI,
	&m26991_MI,
	&m26992_MI,
	&m26993_MI,
	NULL
};
extern TypeInfo t5186_TI;
static TypeInfo* t5184_ITIs[] = 
{
	&t603_TI,
	&t5186_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5184_0_0_0;
extern Il2CppType t5184_1_0_0;
struct t5184;
extern Il2CppGenericClass t5184_GC;
TypeInfo t5184_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5184_MIs, t5184_PIs, NULL, NULL, NULL, NULL, NULL, &t5184_TI, t5184_ITIs, NULL, &EmptyCustomAttributesCache, &t5184_TI, &t5184_0_0_0, &t5184_1_0_0, NULL, &t5184_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerUpHandler>
extern Il2CppType t4046_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26994_GM;
MethodInfo m26994_MI = 
{
	"GetEnumerator", NULL, &t5186_TI, &t4046_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26994_GM};
static MethodInfo* t5186_MIs[] =
{
	&m26994_MI,
	NULL
};
static TypeInfo* t5186_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5186_0_0_0;
extern Il2CppType t5186_1_0_0;
struct t5186;
extern Il2CppGenericClass t5186_GC;
TypeInfo t5186_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5186_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5186_TI, t5186_ITIs, NULL, &EmptyCustomAttributesCache, &t5186_TI, &t5186_0_0_0, &t5186_1_0_0, NULL, &t5186_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4046_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>
extern MethodInfo m26995_MI;
static PropertyInfo t4046____Current_PropertyInfo = 
{
	&t4046_TI, "Current", &m26995_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4046_PIs[] =
{
	&t4046____Current_PropertyInfo,
	NULL
};
extern Il2CppType t90_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26995_GM;
MethodInfo m26995_MI = 
{
	"get_Current", NULL, &t4046_TI, &t90_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m26995_GM};
static MethodInfo* t4046_MIs[] =
{
	&m26995_MI,
	NULL
};
static TypeInfo* t4046_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4046_0_0_0;
extern Il2CppType t4046_1_0_0;
struct t4046;
extern Il2CppGenericClass t4046_GC;
TypeInfo t4046_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4046_MIs, t4046_PIs, NULL, NULL, NULL, NULL, NULL, &t4046_TI, t4046_ITIs, NULL, &EmptyCustomAttributesCache, &t4046_TI, &t4046_0_0_0, &t4046_1_0_0, NULL, &t4046_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2282.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2282_TI;
#include "t2282MD.h"

extern TypeInfo t90_TI;
extern MethodInfo m11554_MI;
extern MethodInfo m20152_MI;
struct t20;
#define m20152(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerUpHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2282_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2282_TI, offsetof(t2282, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2282_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2282_TI, offsetof(t2282, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2282_FIs[] =
{
	&t2282_f0_FieldInfo,
	&t2282_f1_FieldInfo,
	NULL
};
extern MethodInfo m11551_MI;
static PropertyInfo t2282____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2282_TI, "System.Collections.IEnumerator.Current", &m11551_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2282____Current_PropertyInfo = 
{
	&t2282_TI, "Current", &m11554_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2282_PIs[] =
{
	&t2282____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2282____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2282_m11550_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11550_GM;
MethodInfo m11550_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2282_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2282_m11550_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11550_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11551_GM;
MethodInfo m11551_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2282_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11551_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11552_GM;
MethodInfo m11552_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2282_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11552_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11553_GM;
MethodInfo m11553_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2282_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11553_GM};
extern Il2CppType t90_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11554_GM;
MethodInfo m11554_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2282_TI, &t90_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11554_GM};
static MethodInfo* t2282_MIs[] =
{
	&m11550_MI,
	&m11551_MI,
	&m11552_MI,
	&m11553_MI,
	&m11554_MI,
	NULL
};
extern MethodInfo m11553_MI;
extern MethodInfo m11552_MI;
static MethodInfo* t2282_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11551_MI,
	&m11553_MI,
	&m11552_MI,
	&m11554_MI,
};
static TypeInfo* t2282_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4046_TI,
};
static Il2CppInterfaceOffsetPair t2282_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4046_TI, 7},
};
extern TypeInfo t90_TI;
static Il2CppRGCTXData t2282_RGCTXData[3] = 
{
	&m11554_MI/* Method Usage */,
	&t90_TI/* Class Usage */,
	&m20152_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2282_0_0_0;
extern Il2CppType t2282_1_0_0;
extern Il2CppGenericClass t2282_GC;
TypeInfo t2282_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2282_MIs, t2282_PIs, t2282_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2282_TI, t2282_ITIs, t2282_VT, &EmptyCustomAttributesCache, &t2282_TI, &t2282_0_0_0, &t2282_1_0_0, t2282_IOs, &t2282_GC, NULL, NULL, NULL, t2282_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2282)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5185_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerUpHandler>
extern MethodInfo m26996_MI;
extern MethodInfo m26997_MI;
static PropertyInfo t5185____Item_PropertyInfo = 
{
	&t5185_TI, "Item", &m26996_MI, &m26997_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5185_PIs[] =
{
	&t5185____Item_PropertyInfo,
	NULL
};
extern Il2CppType t90_0_0_0;
static ParameterInfo t5185_m26998_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t90_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26998_GM;
MethodInfo m26998_MI = 
{
	"IndexOf", NULL, &t5185_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5185_m26998_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26998_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t90_0_0_0;
static ParameterInfo t5185_m26999_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t90_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26999_GM;
MethodInfo m26999_MI = 
{
	"Insert", NULL, &t5185_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5185_m26999_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26999_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5185_m27000_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27000_GM;
MethodInfo m27000_MI = 
{
	"RemoveAt", NULL, &t5185_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5185_m27000_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27000_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5185_m26996_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t90_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26996_GM;
MethodInfo m26996_MI = 
{
	"get_Item", NULL, &t5185_TI, &t90_0_0_0, RuntimeInvoker_t29_t44, t5185_m26996_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m26996_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t90_0_0_0;
static ParameterInfo t5185_m26997_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t90_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m26997_GM;
MethodInfo m26997_MI = 
{
	"set_Item", NULL, &t5185_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5185_m26997_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m26997_GM};
static MethodInfo* t5185_MIs[] =
{
	&m26998_MI,
	&m26999_MI,
	&m27000_MI,
	&m26996_MI,
	&m26997_MI,
	NULL
};
static TypeInfo* t5185_ITIs[] = 
{
	&t603_TI,
	&t5184_TI,
	&t5186_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5185_0_0_0;
extern Il2CppType t5185_1_0_0;
struct t5185;
extern Il2CppGenericClass t5185_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5185_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5185_MIs, t5185_PIs, NULL, NULL, NULL, NULL, NULL, &t5185_TI, t5185_ITIs, NULL, &t1908__CustomAttributeCache, &t5185_TI, &t5185_0_0_0, &t5185_1_0_0, NULL, &t5185_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5187_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IPointerClickHandler>
extern MethodInfo m27001_MI;
static PropertyInfo t5187____Count_PropertyInfo = 
{
	&t5187_TI, "Count", &m27001_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27002_MI;
static PropertyInfo t5187____IsReadOnly_PropertyInfo = 
{
	&t5187_TI, "IsReadOnly", &m27002_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5187_PIs[] =
{
	&t5187____Count_PropertyInfo,
	&t5187____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27001_GM;
MethodInfo m27001_MI = 
{
	"get_Count", NULL, &t5187_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27001_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27002_GM;
MethodInfo m27002_MI = 
{
	"get_IsReadOnly", NULL, &t5187_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27002_GM};
extern Il2CppType t91_0_0_0;
extern Il2CppType t91_0_0_0;
static ParameterInfo t5187_m27003_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t91_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27003_GM;
MethodInfo m27003_MI = 
{
	"Add", NULL, &t5187_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5187_m27003_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27003_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27004_GM;
MethodInfo m27004_MI = 
{
	"Clear", NULL, &t5187_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27004_GM};
extern Il2CppType t91_0_0_0;
static ParameterInfo t5187_m27005_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t91_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27005_GM;
MethodInfo m27005_MI = 
{
	"Contains", NULL, &t5187_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5187_m27005_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27005_GM};
extern Il2CppType t3806_0_0_0;
extern Il2CppType t3806_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5187_m27006_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3806_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27006_GM;
MethodInfo m27006_MI = 
{
	"CopyTo", NULL, &t5187_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5187_m27006_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27006_GM};
extern Il2CppType t91_0_0_0;
static ParameterInfo t5187_m27007_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t91_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27007_GM;
MethodInfo m27007_MI = 
{
	"Remove", NULL, &t5187_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5187_m27007_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27007_GM};
static MethodInfo* t5187_MIs[] =
{
	&m27001_MI,
	&m27002_MI,
	&m27003_MI,
	&m27004_MI,
	&m27005_MI,
	&m27006_MI,
	&m27007_MI,
	NULL
};
extern TypeInfo t5189_TI;
static TypeInfo* t5187_ITIs[] = 
{
	&t603_TI,
	&t5189_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5187_0_0_0;
extern Il2CppType t5187_1_0_0;
struct t5187;
extern Il2CppGenericClass t5187_GC;
TypeInfo t5187_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5187_MIs, t5187_PIs, NULL, NULL, NULL, NULL, NULL, &t5187_TI, t5187_ITIs, NULL, &EmptyCustomAttributesCache, &t5187_TI, &t5187_0_0_0, &t5187_1_0_0, NULL, &t5187_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IPointerClickHandler>
extern Il2CppType t4048_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27008_GM;
MethodInfo m27008_MI = 
{
	"GetEnumerator", NULL, &t5189_TI, &t4048_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27008_GM};
static MethodInfo* t5189_MIs[] =
{
	&m27008_MI,
	NULL
};
static TypeInfo* t5189_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5189_0_0_0;
extern Il2CppType t5189_1_0_0;
struct t5189;
extern Il2CppGenericClass t5189_GC;
TypeInfo t5189_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5189_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5189_TI, t5189_ITIs, NULL, &EmptyCustomAttributesCache, &t5189_TI, &t5189_0_0_0, &t5189_1_0_0, NULL, &t5189_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4048_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>
extern MethodInfo m27009_MI;
static PropertyInfo t4048____Current_PropertyInfo = 
{
	&t4048_TI, "Current", &m27009_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4048_PIs[] =
{
	&t4048____Current_PropertyInfo,
	NULL
};
extern Il2CppType t91_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27009_GM;
MethodInfo m27009_MI = 
{
	"get_Current", NULL, &t4048_TI, &t91_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27009_GM};
static MethodInfo* t4048_MIs[] =
{
	&m27009_MI,
	NULL
};
static TypeInfo* t4048_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4048_0_0_0;
extern Il2CppType t4048_1_0_0;
struct t4048;
extern Il2CppGenericClass t4048_GC;
TypeInfo t4048_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4048_MIs, t4048_PIs, NULL, NULL, NULL, NULL, NULL, &t4048_TI, t4048_ITIs, NULL, &EmptyCustomAttributesCache, &t4048_TI, &t4048_0_0_0, &t4048_1_0_0, NULL, &t4048_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2283.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2283_TI;
#include "t2283MD.h"

extern TypeInfo t91_TI;
extern MethodInfo m11559_MI;
extern MethodInfo m20163_MI;
struct t20;
#define m20163(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IPointerClickHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2283_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2283_TI, offsetof(t2283, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2283_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2283_TI, offsetof(t2283, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2283_FIs[] =
{
	&t2283_f0_FieldInfo,
	&t2283_f1_FieldInfo,
	NULL
};
extern MethodInfo m11556_MI;
static PropertyInfo t2283____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2283_TI, "System.Collections.IEnumerator.Current", &m11556_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2283____Current_PropertyInfo = 
{
	&t2283_TI, "Current", &m11559_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2283_PIs[] =
{
	&t2283____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2283____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2283_m11555_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11555_GM;
MethodInfo m11555_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2283_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2283_m11555_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11555_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11556_GM;
MethodInfo m11556_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2283_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11556_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11557_GM;
MethodInfo m11557_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2283_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11557_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11558_GM;
MethodInfo m11558_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2283_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11558_GM};
extern Il2CppType t91_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11559_GM;
MethodInfo m11559_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2283_TI, &t91_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11559_GM};
static MethodInfo* t2283_MIs[] =
{
	&m11555_MI,
	&m11556_MI,
	&m11557_MI,
	&m11558_MI,
	&m11559_MI,
	NULL
};
extern MethodInfo m11558_MI;
extern MethodInfo m11557_MI;
static MethodInfo* t2283_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11556_MI,
	&m11558_MI,
	&m11557_MI,
	&m11559_MI,
};
static TypeInfo* t2283_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4048_TI,
};
static Il2CppInterfaceOffsetPair t2283_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4048_TI, 7},
};
extern TypeInfo t91_TI;
static Il2CppRGCTXData t2283_RGCTXData[3] = 
{
	&m11559_MI/* Method Usage */,
	&t91_TI/* Class Usage */,
	&m20163_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2283_0_0_0;
extern Il2CppType t2283_1_0_0;
extern Il2CppGenericClass t2283_GC;
TypeInfo t2283_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2283_MIs, t2283_PIs, t2283_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2283_TI, t2283_ITIs, t2283_VT, &EmptyCustomAttributesCache, &t2283_TI, &t2283_0_0_0, &t2283_1_0_0, t2283_IOs, &t2283_GC, NULL, NULL, NULL, t2283_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2283)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5188_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.IPointerClickHandler>
extern MethodInfo m27010_MI;
extern MethodInfo m27011_MI;
static PropertyInfo t5188____Item_PropertyInfo = 
{
	&t5188_TI, "Item", &m27010_MI, &m27011_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5188_PIs[] =
{
	&t5188____Item_PropertyInfo,
	NULL
};
extern Il2CppType t91_0_0_0;
static ParameterInfo t5188_m27012_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t91_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27012_GM;
MethodInfo m27012_MI = 
{
	"IndexOf", NULL, &t5188_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5188_m27012_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27012_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t91_0_0_0;
static ParameterInfo t5188_m27013_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t91_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27013_GM;
MethodInfo m27013_MI = 
{
	"Insert", NULL, &t5188_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5188_m27013_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27013_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5188_m27014_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27014_GM;
MethodInfo m27014_MI = 
{
	"RemoveAt", NULL, &t5188_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5188_m27014_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27014_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5188_m27010_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t91_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27010_GM;
MethodInfo m27010_MI = 
{
	"get_Item", NULL, &t5188_TI, &t91_0_0_0, RuntimeInvoker_t29_t44, t5188_m27010_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27010_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t91_0_0_0;
static ParameterInfo t5188_m27011_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t91_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27011_GM;
MethodInfo m27011_MI = 
{
	"set_Item", NULL, &t5188_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5188_m27011_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27011_GM};
static MethodInfo* t5188_MIs[] =
{
	&m27012_MI,
	&m27013_MI,
	&m27014_MI,
	&m27010_MI,
	&m27011_MI,
	NULL
};
static TypeInfo* t5188_ITIs[] = 
{
	&t603_TI,
	&t5187_TI,
	&t5189_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5188_0_0_0;
extern Il2CppType t5188_1_0_0;
struct t5188;
extern Il2CppGenericClass t5188_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5188_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5188_MIs, t5188_PIs, NULL, NULL, NULL, NULL, NULL, &t5188_TI, t5188_ITIs, NULL, &t1908__CustomAttributeCache, &t5188_TI, &t5188_0_0_0, &t5188_1_0_0, NULL, &t5188_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5190_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.IBeginDragHandler>
extern MethodInfo m27015_MI;
static PropertyInfo t5190____Count_PropertyInfo = 
{
	&t5190_TI, "Count", &m27015_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27016_MI;
static PropertyInfo t5190____IsReadOnly_PropertyInfo = 
{
	&t5190_TI, "IsReadOnly", &m27016_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5190_PIs[] =
{
	&t5190____Count_PropertyInfo,
	&t5190____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27015_GM;
MethodInfo m27015_MI = 
{
	"get_Count", NULL, &t5190_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27015_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27016_GM;
MethodInfo m27016_MI = 
{
	"get_IsReadOnly", NULL, &t5190_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27016_GM};
extern Il2CppType t93_0_0_0;
extern Il2CppType t93_0_0_0;
static ParameterInfo t5190_m27017_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t93_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27017_GM;
MethodInfo m27017_MI = 
{
	"Add", NULL, &t5190_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5190_m27017_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27017_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27018_GM;
MethodInfo m27018_MI = 
{
	"Clear", NULL, &t5190_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27018_GM};
extern Il2CppType t93_0_0_0;
static ParameterInfo t5190_m27019_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t93_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27019_GM;
MethodInfo m27019_MI = 
{
	"Contains", NULL, &t5190_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5190_m27019_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27019_GM};
extern Il2CppType t3807_0_0_0;
extern Il2CppType t3807_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5190_m27020_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3807_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27020_GM;
MethodInfo m27020_MI = 
{
	"CopyTo", NULL, &t5190_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5190_m27020_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27020_GM};
extern Il2CppType t93_0_0_0;
static ParameterInfo t5190_m27021_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t93_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27021_GM;
MethodInfo m27021_MI = 
{
	"Remove", NULL, &t5190_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5190_m27021_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27021_GM};
static MethodInfo* t5190_MIs[] =
{
	&m27015_MI,
	&m27016_MI,
	&m27017_MI,
	&m27018_MI,
	&m27019_MI,
	&m27020_MI,
	&m27021_MI,
	NULL
};
extern TypeInfo t5192_TI;
static TypeInfo* t5190_ITIs[] = 
{
	&t603_TI,
	&t5192_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5190_0_0_0;
extern Il2CppType t5190_1_0_0;
struct t5190;
extern Il2CppGenericClass t5190_GC;
TypeInfo t5190_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5190_MIs, t5190_PIs, NULL, NULL, NULL, NULL, NULL, &t5190_TI, t5190_ITIs, NULL, &EmptyCustomAttributesCache, &t5190_TI, &t5190_0_0_0, &t5190_1_0_0, NULL, &t5190_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.IBeginDragHandler>
extern Il2CppType t4050_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27022_GM;
MethodInfo m27022_MI = 
{
	"GetEnumerator", NULL, &t5192_TI, &t4050_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27022_GM};
static MethodInfo* t5192_MIs[] =
{
	&m27022_MI,
	NULL
};
static TypeInfo* t5192_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5192_0_0_0;
extern Il2CppType t5192_1_0_0;
struct t5192;
extern Il2CppGenericClass t5192_GC;
TypeInfo t5192_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5192_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5192_TI, t5192_ITIs, NULL, &EmptyCustomAttributesCache, &t5192_TI, &t5192_0_0_0, &t5192_1_0_0, NULL, &t5192_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4050_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>
extern MethodInfo m27023_MI;
static PropertyInfo t4050____Current_PropertyInfo = 
{
	&t4050_TI, "Current", &m27023_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4050_PIs[] =
{
	&t4050____Current_PropertyInfo,
	NULL
};
extern Il2CppType t93_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27023_GM;
MethodInfo m27023_MI = 
{
	"get_Current", NULL, &t4050_TI, &t93_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27023_GM};
static MethodInfo* t4050_MIs[] =
{
	&m27023_MI,
	NULL
};
static TypeInfo* t4050_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4050_0_0_0;
extern Il2CppType t4050_1_0_0;
struct t4050;
extern Il2CppGenericClass t4050_GC;
TypeInfo t4050_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4050_MIs, t4050_PIs, NULL, NULL, NULL, NULL, NULL, &t4050_TI, t4050_ITIs, NULL, &EmptyCustomAttributesCache, &t4050_TI, &t4050_0_0_0, &t4050_1_0_0, NULL, &t4050_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2284.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2284_TI;
#include "t2284MD.h"

extern TypeInfo t93_TI;
extern MethodInfo m11564_MI;
extern MethodInfo m20174_MI;
struct t20;
#define m20174(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.IBeginDragHandler>
extern Il2CppType t20_0_0_1;
FieldInfo t2284_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2284_TI, offsetof(t2284, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2284_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2284_TI, offsetof(t2284, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2284_FIs[] =
{
	&t2284_f0_FieldInfo,
	&t2284_f1_FieldInfo,
	NULL
};
extern MethodInfo m11561_MI;
static PropertyInfo t2284____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2284_TI, "System.Collections.IEnumerator.Current", &m11561_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2284____Current_PropertyInfo = 
{
	&t2284_TI, "Current", &m11564_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2284_PIs[] =
{
	&t2284____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2284____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2284_m11560_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11560_GM;
MethodInfo m11560_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2284_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2284_m11560_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m11560_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11561_GM;
MethodInfo m11561_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2284_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11561_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11562_GM;
MethodInfo m11562_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2284_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11562_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11563_GM;
MethodInfo m11563_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2284_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11563_GM};
extern Il2CppType t93_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m11564_GM;
MethodInfo m11564_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2284_TI, &t93_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m11564_GM};
static MethodInfo* t2284_MIs[] =
{
	&m11560_MI,
	&m11561_MI,
	&m11562_MI,
	&m11563_MI,
	&m11564_MI,
	NULL
};
extern MethodInfo m11563_MI;
extern MethodInfo m11562_MI;
static MethodInfo* t2284_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m11561_MI,
	&m11563_MI,
	&m11562_MI,
	&m11564_MI,
};
static TypeInfo* t2284_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4050_TI,
};
static Il2CppInterfaceOffsetPair t2284_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4050_TI, 7},
};
extern TypeInfo t93_TI;
static Il2CppRGCTXData t2284_RGCTXData[3] = 
{
	&m11564_MI/* Method Usage */,
	&t93_TI/* Class Usage */,
	&m20174_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2284_0_0_0;
extern Il2CppType t2284_1_0_0;
extern Il2CppGenericClass t2284_GC;
TypeInfo t2284_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2284_MIs, t2284_PIs, t2284_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2284_TI, t2284_ITIs, t2284_VT, &EmptyCustomAttributesCache, &t2284_TI, &t2284_0_0_0, &t2284_1_0_0, t2284_IOs, &t2284_GC, NULL, NULL, NULL, t2284_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2284)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
