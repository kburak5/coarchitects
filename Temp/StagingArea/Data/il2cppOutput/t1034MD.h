﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1034;
struct t781;
struct t66;
struct t67;
struct t29;
#include "t1064.h"

 void m5251 (t1034 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7134 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5225 (t1034 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5269 (t1034 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5184 (t1034 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5226 (t1034 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5215 (t1034 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5232 (t1034 * __this, t781* p0, int32_t p1, int32_t p2, t67 * p3, t29 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5242 (t1034 * __this, t781* p0, int32_t p1, int32_t p2, t67 * p3, t29 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5234 (t1034 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5243 (t1034 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
