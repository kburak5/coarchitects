﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1335;
struct t7;
struct t1337;
struct t295;
struct t1338;

 t7* m7263 (t1335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1337* m7264 (t1335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7265 (t1335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7266 (t1335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1338 * m7267 (t1335 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
