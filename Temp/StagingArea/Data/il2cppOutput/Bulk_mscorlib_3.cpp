﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t1362.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t1362_TI;
#include "t1362MD.h"


#include "t20.h"

// Metadata Definition System.Reflection.AssemblyNameFlags
extern Il2CppType t44_0_0_1542;
FieldInfo t1362_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1362_TI, offsetof(t1362, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1362_0_0_32854;
FieldInfo t1362_f2_FieldInfo = 
{
	"None", &t1362_0_0_32854, &t1362_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1362_0_0_32854;
FieldInfo t1362_f3_FieldInfo = 
{
	"PublicKey", &t1362_0_0_32854, &t1362_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1362_0_0_32854;
FieldInfo t1362_f4_FieldInfo = 
{
	"Retargetable", &t1362_0_0_32854, &t1362_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1362_0_0_32854;
FieldInfo t1362_f5_FieldInfo = 
{
	"EnableJITcompileOptimizer", &t1362_0_0_32854, &t1362_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1362_0_0_32854;
FieldInfo t1362_f6_FieldInfo = 
{
	"EnableJITcompileTracking", &t1362_0_0_32854, &t1362_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1362_FIs[] =
{
	&t1362_f1_FieldInfo,
	&t1362_f2_FieldInfo,
	&t1362_f3_FieldInfo,
	&t1362_f4_FieldInfo,
	&t1362_f5_FieldInfo,
	&t1362_f6_FieldInfo,
	NULL
};
static const int32_t t1362_f2_DefaultValueData = 0;
extern Il2CppType t44_0_0_0;
static Il2CppFieldDefaultValueEntry t1362_f2_DefaultValue = 
{
	&t1362_f2_FieldInfo, { (char*)&t1362_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1362_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1362_f3_DefaultValue = 
{
	&t1362_f3_FieldInfo, { (char*)&t1362_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1362_f4_DefaultValueData = 256;
static Il2CppFieldDefaultValueEntry t1362_f4_DefaultValue = 
{
	&t1362_f4_FieldInfo, { (char*)&t1362_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1362_f5_DefaultValueData = 16384;
static Il2CppFieldDefaultValueEntry t1362_f5_DefaultValue = 
{
	&t1362_f5_FieldInfo, { (char*)&t1362_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1362_f6_DefaultValueData = 32768;
static Il2CppFieldDefaultValueEntry t1362_f6_DefaultValue = 
{
	&t1362_f6_FieldInfo, { (char*)&t1362_f6_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1362_FDVs[] = 
{
	&t1362_f2_DefaultValue,
	&t1362_f3_DefaultValue,
	&t1362_f4_DefaultValue,
	&t1362_f5_DefaultValue,
	&t1362_f6_DefaultValue,
	NULL
};
static MethodInfo* t1362_MIs[] =
{
	NULL
};
extern MethodInfo m1248_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1249_MI;
extern MethodInfo m1250_MI;
extern MethodInfo m1251_MI;
extern MethodInfo m1252_MI;
extern MethodInfo m1253_MI;
extern MethodInfo m1254_MI;
extern MethodInfo m1255_MI;
extern MethodInfo m1256_MI;
extern MethodInfo m1257_MI;
extern MethodInfo m1258_MI;
extern MethodInfo m1259_MI;
extern MethodInfo m1260_MI;
extern MethodInfo m1261_MI;
extern MethodInfo m1262_MI;
extern MethodInfo m1263_MI;
extern MethodInfo m1264_MI;
extern MethodInfo m1265_MI;
extern MethodInfo m1266_MI;
extern MethodInfo m1267_MI;
extern MethodInfo m1268_MI;
extern MethodInfo m1269_MI;
static MethodInfo* t1362_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
extern TypeInfo t288_TI;
extern TypeInfo t289_TI;
extern TypeInfo t290_TI;
static Il2CppInterfaceOffsetPair t1362_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern TypeInfo t437_TI;
#include "t437.h"
#include "t437MD.h"
extern MethodInfo m2055_MI;
extern TypeInfo t291_TI;
#include "t291.h"
#include "t291MD.h"
extern MethodInfo m1270_MI;
void t1362_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1362__CustomAttributeCache = {
2,
NULL,
&t1362_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1362_0_0_0;
extern Il2CppType t1362_1_0_0;
extern TypeInfo t49_TI;
#include "t44.h"
extern TypeInfo t44_TI;
extern CustomAttributesCache t1362__CustomAttributeCache;
TypeInfo t1362_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyNameFlags", "System.Reflection", t1362_MIs, NULL, t1362_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1362_VT, &t1362__CustomAttributeCache, &t44_TI, &t1362_0_0_0, &t1362_1_0_0, t1362_IOs, NULL, NULL, t1362_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1362)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 6, 0, 0, 23, 0, 3};
#include "t433.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t433_TI;
#include "t433MD.h"

#include "t21.h"
#include "t7.h"
#include "t490MD.h"
extern MethodInfo m2881_MI;


extern MethodInfo m2051_MI;
 void m2051 (t433 * __this, t7* p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition System.Reflection.AssemblyProductAttribute
extern Il2CppType t7_0_0_1;
FieldInfo t433_f0_FieldInfo = 
{
	"name", &t7_0_0_1, &t433_TI, offsetof(t433, f0), &EmptyCustomAttributesCache};
static FieldInfo* t433_FIs[] =
{
	&t433_f0_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t433_m2051_ParameterInfos[] = 
{
	{"product", 0, 134221471, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2051_MI = 
{
	".ctor", (methodPointerType)&m2051, &t433_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t433_m2051_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3000, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t433_MIs[] =
{
	&m2051_MI,
	NULL
};
extern MethodInfo m2882_MI;
extern MethodInfo m2883_MI;
extern MethodInfo m1332_MI;
static MethodInfo* t433_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
extern TypeInfo t604_TI;
static Il2CppInterfaceOffsetPair t433_IOs[] = 
{
	{ &t604_TI, 4},
};
extern TypeInfo t629_TI;
#include "t629.h"
#include "t629MD.h"
extern MethodInfo m2915_MI;
extern MethodInfo m2916_MI;
void t433_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 1, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t433__CustomAttributeCache = {
2,
NULL,
&t433_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t433_0_0_0;
extern Il2CppType t433_1_0_0;
extern TypeInfo t490_TI;
struct t433;
extern CustomAttributesCache t433__CustomAttributeCache;
TypeInfo t433_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyProductAttribute", "System.Reflection", t433_MIs, NULL, t433_FIs, NULL, &t490_TI, NULL, NULL, &t433_TI, NULL, t433_VT, &t433__CustomAttributeCache, &t433_TI, &t433_0_0_0, &t433_1_0_0, t433_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t433), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t429.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t429_TI;
#include "t429MD.h"



extern MethodInfo m2047_MI;
 void m2047 (t429 * __this, t7* p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition System.Reflection.AssemblyTitleAttribute
extern Il2CppType t7_0_0_1;
FieldInfo t429_f0_FieldInfo = 
{
	"name", &t7_0_0_1, &t429_TI, offsetof(t429, f0), &EmptyCustomAttributesCache};
static FieldInfo* t429_FIs[] =
{
	&t429_f0_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t429_m2047_ParameterInfos[] = 
{
	{"title", 0, 134221472, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2047_MI = 
{
	".ctor", (methodPointerType)&m2047, &t429_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t429_m2047_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3001, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t429_MIs[] =
{
	&m2047_MI,
	NULL
};
static MethodInfo* t429_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t429_IOs[] = 
{
	{ &t604_TI, 4},
};
void t429_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 1, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t429__CustomAttributeCache = {
2,
NULL,
&t429_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t429_0_0_0;
extern Il2CppType t429_1_0_0;
struct t429;
extern CustomAttributesCache t429__CustomAttributeCache;
TypeInfo t429_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyTitleAttribute", "System.Reflection", t429_MIs, NULL, t429_FIs, NULL, &t490_TI, NULL, NULL, &t429_TI, NULL, t429_VT, &t429__CustomAttributeCache, &t429_TI, &t429_0_0_0, &t429_1_0_0, t429_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t429), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t438.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t438_TI;
#include "t438MD.h"



extern MethodInfo m2056_MI;
 void m2056 (t438 * __this, t7* p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition System.Reflection.AssemblyTrademarkAttribute
extern Il2CppType t7_0_0_1;
FieldInfo t438_f0_FieldInfo = 
{
	"name", &t7_0_0_1, &t438_TI, offsetof(t438, f0), &EmptyCustomAttributesCache};
static FieldInfo* t438_FIs[] =
{
	&t438_f0_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t438_m2056_ParameterInfos[] = 
{
	{"trademark", 0, 134221473, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2056_MI = 
{
	".ctor", (methodPointerType)&m2056, &t438_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t438_m2056_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3002, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t438_MIs[] =
{
	&m2056_MI,
	NULL
};
static MethodInfo* t438_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t438_IOs[] = 
{
	{ &t604_TI, 4},
};
void t438_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 1, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t438__CustomAttributeCache = {
2,
NULL,
&t438_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t438_0_0_0;
extern Il2CppType t438_1_0_0;
struct t438;
extern CustomAttributesCache t438__CustomAttributeCache;
TypeInfo t438_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AssemblyTrademarkAttribute", "System.Reflection", t438_MIs, NULL, t438_FIs, NULL, &t490_TI, NULL, NULL, &t438_TI, NULL, t438_VT, &t438__CustomAttributeCache, &t438_TI, &t438_0_0_0, &t438_1_0_0, t438_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t438), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t1364.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1364_TI;
#include "t1364MD.h"

#include "t636.h"
#include "t630.h"
#include "mscorlib_ArrayTypes.h"
#include "t29.h"
#include "t632.h"
#include "t633.h"
#include "t42.h"
#include "t40.h"
#include "t637.h"
#include "t194.h"
#include "t43.h"
#include "t601.h"
#include "t22.h"
#include "t35.h"
#include "t1127.h"
#include "t1166.h"
#include "t49.h"
#include "t338.h"
#include "t391.h"
#include "t1357.h"
#include "t296.h"
#include "t1150.h"
#include "t1146.h"
#include "t305.h"
extern TypeInfo t42_TI;
extern TypeInfo t537_TI;
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t636_TI;
extern TypeInfo t638_TI;
extern TypeInfo t637_TI;
extern TypeInfo t7_TI;
extern TypeInfo t40_TI;
extern TypeInfo t194_TI;
extern TypeInfo t601_TI;
extern TypeInfo t22_TI;
extern TypeInfo t35_TI;
extern TypeInfo t1095_TI;
extern TypeInfo t1166_TI;
extern TypeInfo t338_TI;
extern TypeInfo t391_TI;
extern TypeInfo t1357_TI;
extern TypeInfo t296_TI;
extern TypeInfo t1150_TI;
extern TypeInfo t305_TI;
extern TypeInfo t1146_TI;
extern TypeInfo t631_TI;
#include "t631MD.h"
#include "t42MD.h"
#include "t29MD.h"
#include "t20MD.h"
#include "t636MD.h"
#include "t637MD.h"
#include "t7MD.h"
#include "t49MD.h"
#include "t1095MD.h"
#include "t338MD.h"
#include "t1357MD.h"
#include "t296MD.h"
#include "t305MD.h"
#include "t1146MD.h"
extern Il2CppType t194_0_0_0;
extern Il2CppType t601_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t1166_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t49_0_0_0;
extern Il2CppType t391_0_0_0;
extern MethodInfo m7508_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m7502_MI;
extern MethodInfo m7495_MI;
extern MethodInfo m5951_MI;
extern MethodInfo m2939_MI;
extern MethodInfo m7704_MI;
extern MethodInfo m1713_MI;
extern MethodInfo m5993_MI;
extern MethodInfo m9711_MI;
extern MethodInfo m7496_MI;
extern MethodInfo m2981_MI;
extern MethodInfo m5994_MI;
extern MethodInfo m6020_MI;
extern MethodInfo m7499_MI;
extern MethodInfo m5997_MI;
extern MethodInfo m5878_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m6001_MI;
extern MethodInfo m9091_MI;
extern MethodInfo m5999_MI;
extern MethodInfo m5869_MI;
extern MethodInfo m6042_MI;
extern MethodInfo m6041_MI;
extern MethodInfo m6037_MI;
extern MethodInfo m6015_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m2940_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m5292_MI;
extern MethodInfo m7500_MI;
extern MethodInfo m7503_MI;
extern MethodInfo m7552_MI;
extern MethodInfo m7504_MI;
extern MethodInfo m7455_MI;
extern MethodInfo m2937_MI;
extern MethodInfo m6019_MI;
extern MethodInfo m7547_MI;
extern MethodInfo m6044_MI;
extern MethodInfo m5991_MI;
extern MethodInfo m9850_MI;
extern MethodInfo m10142_MI;
extern MethodInfo m3973_MI;
extern MethodInfo m10143_MI;
extern MethodInfo m10144_MI;
extern MethodInfo m7506_MI;
extern MethodInfo m7512_MI;
extern MethodInfo m7507_MI;
struct t20;
struct t20;
 int32_t m10145_gshared (t29 * __this, t316* p0, t29 * p1, MethodInfo* method);
#define m10145(__this, p0, p1, method) (int32_t)m10145_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, method)
#define m10142(__this, p0, p1, method) (int32_t)m10145_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, method)


extern MethodInfo m7493_MI;
 void m7493 (t1364 * __this, MethodInfo* method){
	{
		m7508(__this, &m7508_MI);
		return;
	}
}
extern MethodInfo m7494_MI;
 t636 * m7494 (t1364 * __this, int32_t p0, t1365* p1, t316** p2, t634* p3, t633 * p4, t446* p5, t29 ** p6, MethodInfo* method){
	t537* V_0 = {0};
	int32_t V_1 = 0;
	t636 * V_2 = {0};
	{
		if ((*((t316**)p2)))
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		V_0 = (((t42_SFs*)InitializedTypeInfo(&t42_TI)->static_fields)->f3);
		goto IL_0037;
	}

IL_000c:
	{
		V_0 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), (((int32_t)(((t20 *)(*((t316**)p2)))->max_length)))));
		V_1 = 0;
		goto IL_0030;
	}

IL_001a:
	{
		int32_t L_0 = V_1;
		if (!(*(t29 **)(t29 **)SZArrayLdElema((*((t316**)p2)), L_0)))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_1 = V_1;
		t42 * L_2 = m1430((*(t29 **)(t29 **)SZArrayLdElema((*((t316**)p2)), L_1)), &m1430_MI);
		ArrayElementTypeCheck (V_0, L_2);
		*((t42 **)(t42 **)SZArrayLdElema(V_0, V_1)) = (t42 *)L_2;
	}

IL_002c:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0030:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)(*((t316**)p2)))->max_length))))))
		{
			goto IL_001a;
		}
	}

IL_0037:
	{
		t636 * L_3 = m7502(__this, p0, p1, V_0, p3, 1, &m7502_MI);
		V_2 = L_3;
		*((t29 **)(p6)) = (t29 *)NULL;
		if (!p5)
		{
			goto IL_0056;
		}
	}
	{
		m7495(__this, p5, p2, V_2, &m7495_MI);
	}

IL_0056:
	{
		return V_2;
	}
}
 void m7495 (t1364 * __this, t446* p0, t316** p1, t636 * p2, MethodInfo* method){
	t316* V_0 = {0};
	t638* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		V_0 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), (((int32_t)(((t20 *)(*((t316**)p1)))->max_length)))));
		m5951(NULL, (t20 *)(t20 *)(*((t316**)p1)), (t20 *)(t20 *)V_0, (((int32_t)(((t20 *)(*((t316**)p1)))->max_length))), &m5951_MI);
		t638* L_0 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, p2);
		V_1 = L_0;
		V_2 = 0;
		goto IL_004e;
	}

IL_0021:
	{
		V_3 = 0;
		goto IL_0044;
	}

IL_0025:
	{
		int32_t L_1 = V_2;
		int32_t L_2 = V_3;
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7704_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_1, L_2)));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_4 = m1713(NULL, (*(t7**)(t7**)SZArrayLdElema(p0, L_1)), L_3, &m1713_MI);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_5 = V_2;
		ArrayElementTypeCheck (V_0, (*(t29 **)(t29 **)SZArrayLdElema((*((t316**)p1)), L_5)));
		*((t29 **)(t29 **)SZArrayLdElema(V_0, V_3)) = (t29 *)(*(t29 **)(t29 **)SZArrayLdElema((*((t316**)p1)), L_5));
		goto IL_004a;
	}

IL_0040:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_0044:
	{
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0025;
		}
	}

IL_004a:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_004e:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)p0)->max_length))))))
		{
			goto IL_0021;
		}
	}
	{
		m5951(NULL, (t20 *)(t20 *)V_0, (t20 *)(t20 *)(*((t316**)p1)), (((int32_t)(((t20 *)(*((t316**)p1)))->max_length))), &m5951_MI);
		return;
	}
}
 bool m7496 (t29 * __this, t42 * p0, t42 * p1, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5993_MI, p0);
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5993_MI, p1);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		t42 * L_2 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, p0);
		t42 * L_3 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, p1);
		bool L_4 = m7496(NULL, L_2, L_3, &m7496_MI);
		return L_4;
	}

IL_0022:
	{
		bool L_5 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m2981_MI, p1, p0);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		return 1;
	}

IL_002d:
	{
		return 0;
	}
}
extern MethodInfo m7497_MI;
 t29 * m7497 (t1364 * __this, t29 * p0, t42 * p1, t633 * p2, MethodInfo* method){
	t42 * V_0 = {0};
	{
		if (p0)
		{
			goto IL_0005;
		}
	}
	{
		return NULL;
	}

IL_0005:
	{
		t42 * L_0 = m1430(p0, &m1430_MI);
		V_0 = L_0;
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5994_MI, p1);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		t42 * L_2 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, p1);
		p1 = L_2;
	}

IL_001e:
	{
		if ((((t42 *)V_0) == ((t42 *)p1)))
		{
			goto IL_002b;
		}
	}
	{
		bool L_3 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m6020_MI, p1, p0);
		if (!L_3)
		{
			goto IL_002d;
		}
	}

IL_002b:
	{
		return p0;
	}

IL_002d:
	{
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5993_MI, V_0);
		if (!L_4)
		{
			goto IL_0052;
		}
	}
	{
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5993_MI, p1);
		if (!L_5)
		{
			goto IL_0052;
		}
	}
	{
		t42 * L_6 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, V_0);
		t42 * L_7 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, p1);
		bool L_8 = m7496(NULL, L_6, L_7, &m7496_MI);
		if (!L_8)
		{
			goto IL_0052;
		}
	}
	{
		return p0;
	}

IL_0052:
	{
		bool L_9 = m7499(NULL, V_0, p1, &m7499_MI);
		if (!L_9)
		{
			goto IL_00ce;
		}
	}
	{
		bool L_10 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p1);
		if (!L_10)
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t49_TI));
		t29 * L_11 = m5878(NULL, p1, p0, &m5878_MI);
		return L_11;
	}

IL_006e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_12 = m1554(NULL, LoadTypeToken(&t194_0_0_0), &m1554_MI);
		if ((((t42 *)V_0) != ((t42 *)L_12)))
		{
			goto IL_00af;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_13 = m1554(NULL, LoadTypeToken(&t601_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_13)))
		{
			goto IL_0095;
		}
	}
	{
		double L_14 = (((double)((*(uint16_t*)((uint16_t*)UnBox (p0, InitializedTypeInfo(&t194_TI)))))));
		t29 * L_15 = Box(InitializedTypeInfo(&t601_TI), &L_14);
		return L_15;
	}

IL_0095:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_16 = m1554(NULL, LoadTypeToken(&t22_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_16)))
		{
			goto IL_00af;
		}
	}
	{
		float L_17 = (((float)((*(uint16_t*)((uint16_t*)UnBox (p0, InitializedTypeInfo(&t194_TI)))))));
		t29 * L_18 = Box(InitializedTypeInfo(&t22_TI), &L_17);
		return L_18;
	}

IL_00af:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_19 = m1554(NULL, LoadTypeToken(&t35_0_0_0), &m1554_MI);
		if ((((t42 *)V_0) != ((t42 *)L_19)))
		{
			goto IL_00c6;
		}
	}
	{
		bool L_20 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6001_MI, p1);
		if (!L_20)
		{
			goto IL_00c6;
		}
	}
	{
		return p0;
	}

IL_00c6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1095_TI));
		t29 * L_21 = m9091(NULL, p0, p1, &m9091_MI);
		return L_21;
	}

IL_00ce:
	{
		return NULL;
	}
}
extern MethodInfo m7498_MI;
 void m7498 (t1364 * __this, t316** p0, t29 * p1, MethodInfo* method){
	{
		return;
	}
}
 bool m7499 (t29 * __this, t42 * p0, t42 * p1, MethodInfo* method){
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	int32_t V_2 = {0};
	int32_t V_3 = {0};
	int32_t G_B28_0 = 0;
	int32_t G_B30_0 = 0;
	int32_t G_B38_0 = 0;
	int32_t G_B40_0 = 0;
	int32_t G_B48_0 = 0;
	int32_t G_B50_0 = 0;
	int32_t G_B58_0 = 0;
	int32_t G_B60_0 = 0;
	int32_t G_B68_0 = 0;
	int32_t G_B70_0 = 0;
	int32_t G_B78_0 = 0;
	int32_t G_B80_0 = 0;
	int32_t G_B89_0 = 0;
	int32_t G_B91_0 = 0;
	int32_t G_B95_0 = 0;
	{
		if ((((t42 *)p0) != ((t42 *)p1)))
		{
			goto IL_0006;
		}
	}
	{
		return 1;
	}

IL_0006:
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		return 1;
	}

IL_000b:
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5994_MI, p1);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5994_MI, p0);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001b;
		}
	}
	{
		return 0;
	}

IL_001b:
	{
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5999_MI, p1);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		bool L_3 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m2981_MI, p1, p0);
		return L_3;
	}

IL_002b:
	{
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p1);
		if (!L_4)
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t49_TI));
		t42 * L_5 = m5869(NULL, p1, &m5869_MI);
		p1 = L_5;
		if ((((t42 *)p0) != ((t42 *)p1)))
		{
			goto IL_0043;
		}
	}
	{
		return 1;
	}

IL_0043:
	{
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6042_MI, p1);
		if (!L_6)
		{
			goto IL_006a;
		}
	}
	{
		t42 * L_7 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m6041_MI, p1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_8 = m1554(NULL, LoadTypeToken(&t1166_0_0_0), &m1554_MI);
		if ((((t42 *)L_7) != ((t42 *)L_8)))
		{
			goto IL_006a;
		}
	}
	{
		t537* L_9 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m6037_MI, p1);
		int32_t L_10 = 0;
		if ((((t42 *)(*(t42 **)(t42 **)SZArrayLdElema(L_9, L_10))) != ((t42 *)p0)))
		{
			goto IL_006a;
		}
	}
	{
		return 1;
	}

IL_006a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		int32_t L_11 = m6015(NULL, p0, &m6015_MI);
		V_0 = L_11;
		int32_t L_12 = m6015(NULL, p1, &m6015_MI);
		V_1 = L_12;
		V_2 = V_0;
		if (((int32_t)(V_2-4)) == 0)
		{
			goto IL_00af;
		}
		if (((int32_t)(V_2-4)) == 1)
		{
			goto IL_014a;
		}
		if (((int32_t)(V_2-4)) == 2)
		{
			goto IL_00e7;
		}
		if (((int32_t)(V_2-4)) == 3)
		{
			goto IL_01f1;
		}
		if (((int32_t)(V_2-4)) == 4)
		{
			goto IL_01a1;
		}
		if (((int32_t)(V_2-4)) == 5)
		{
			goto IL_0289;
		}
		if (((int32_t)(V_2-4)) == 6)
		{
			goto IL_0241;
		}
		if (((int32_t)(V_2-4)) == 7)
		{
			goto IL_02d1;
		}
		if (((int32_t)(V_2-4)) == 8)
		{
			goto IL_02d1;
		}
		if (((int32_t)(V_2-4)) == 9)
		{
			goto IL_030a;
		}
	}
	{
		goto IL_0320;
	}

IL_00af:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-8)) == 0)
		{
			goto IL_00d7;
		}
		if (((int32_t)(V_3-8)) == 1)
		{
			goto IL_00d7;
		}
		if (((int32_t)(V_3-8)) == 2)
		{
			goto IL_00d7;
		}
		if (((int32_t)(V_3-8)) == 3)
		{
			goto IL_00d7;
		}
		if (((int32_t)(V_3-8)) == 4)
		{
			goto IL_00d7;
		}
		if (((int32_t)(V_3-8)) == 5)
		{
			goto IL_00d7;
		}
		if (((int32_t)(V_3-8)) == 6)
		{
			goto IL_00d7;
		}
	}
	{
		goto IL_00d9;
	}

IL_00d7:
	{
		return 1;
	}

IL_00d9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_13 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		return ((((t42 *)p1) == ((t42 *)L_13))? 1 : 0);
	}

IL_00e7:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-4)) == 0)
		{
			goto IL_011f;
		}
		if (((int32_t)(V_3-4)) == 1)
		{
			goto IL_0121;
		}
		if (((int32_t)(V_3-4)) == 2)
		{
			goto IL_0121;
		}
		if (((int32_t)(V_3-4)) == 3)
		{
			goto IL_011f;
		}
		if (((int32_t)(V_3-4)) == 4)
		{
			goto IL_011f;
		}
		if (((int32_t)(V_3-4)) == 5)
		{
			goto IL_011f;
		}
		if (((int32_t)(V_3-4)) == 6)
		{
			goto IL_011f;
		}
		if (((int32_t)(V_3-4)) == 7)
		{
			goto IL_011f;
		}
		if (((int32_t)(V_3-4)) == 8)
		{
			goto IL_011f;
		}
		if (((int32_t)(V_3-4)) == 9)
		{
			goto IL_011f;
		}
		if (((int32_t)(V_3-4)) == 10)
		{
			goto IL_011f;
		}
	}
	{
		goto IL_0121;
	}

IL_011f:
	{
		return 1;
	}

IL_0121:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_14 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		if ((((t42 *)p1) == ((t42 *)L_14)))
		{
			goto IL_0148;
		}
	}
	{
		bool L_15 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_15)
		{
			goto IL_0145;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_16 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		G_B28_0 = ((((t42 *)p1) == ((t42 *)L_16))? 1 : 0);
		goto IL_0146;
	}

IL_0145:
	{
		G_B28_0 = 0;
	}

IL_0146:
	{
		G_B30_0 = G_B28_0;
		goto IL_0149;
	}

IL_0148:
	{
		G_B30_0 = 1;
	}

IL_0149:
	{
		return G_B30_0;
	}

IL_014a:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-7)) == 0)
		{
			goto IL_0176;
		}
		if (((int32_t)(V_3-7)) == 1)
		{
			goto IL_0178;
		}
		if (((int32_t)(V_3-7)) == 2)
		{
			goto IL_0176;
		}
		if (((int32_t)(V_3-7)) == 3)
		{
			goto IL_0178;
		}
		if (((int32_t)(V_3-7)) == 4)
		{
			goto IL_0176;
		}
		if (((int32_t)(V_3-7)) == 5)
		{
			goto IL_0178;
		}
		if (((int32_t)(V_3-7)) == 6)
		{
			goto IL_0176;
		}
		if (((int32_t)(V_3-7)) == 7)
		{
			goto IL_0176;
		}
	}
	{
		goto IL_0178;
	}

IL_0176:
	{
		return 1;
	}

IL_0178:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_17 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		if ((((t42 *)p1) == ((t42 *)L_17)))
		{
			goto IL_019f;
		}
	}
	{
		bool L_18 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_18)
		{
			goto IL_019c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_19 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		G_B38_0 = ((((t42 *)p1) == ((t42 *)L_19))? 1 : 0);
		goto IL_019d;
	}

IL_019c:
	{
		G_B38_0 = 0;
	}

IL_019d:
	{
		G_B40_0 = G_B38_0;
		goto IL_01a0;
	}

IL_019f:
	{
		G_B40_0 = 1;
	}

IL_01a0:
	{
		return G_B40_0;
	}

IL_01a1:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-((int32_t)9))) == 0)
		{
			goto IL_01c6;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 1)
		{
			goto IL_01c6;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 2)
		{
			goto IL_01c6;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 3)
		{
			goto IL_01c6;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 4)
		{
			goto IL_01c6;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 5)
		{
			goto IL_01c6;
		}
	}
	{
		goto IL_01c8;
	}

IL_01c6:
	{
		return 1;
	}

IL_01c8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_20 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		if ((((t42 *)p1) == ((t42 *)L_20)))
		{
			goto IL_01ef;
		}
	}
	{
		bool L_21 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_21)
		{
			goto IL_01ec;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_22 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		G_B48_0 = ((((t42 *)p1) == ((t42 *)L_22))? 1 : 0);
		goto IL_01ed;
	}

IL_01ec:
	{
		G_B48_0 = 0;
	}

IL_01ed:
	{
		G_B50_0 = G_B48_0;
		goto IL_01f0;
	}

IL_01ef:
	{
		G_B50_0 = 1;
	}

IL_01f0:
	{
		return G_B50_0;
	}

IL_01f1:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-((int32_t)9))) == 0)
		{
			goto IL_0216;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 1)
		{
			goto IL_0218;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 2)
		{
			goto IL_0216;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 3)
		{
			goto IL_0218;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 4)
		{
			goto IL_0216;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 5)
		{
			goto IL_0216;
		}
	}
	{
		goto IL_0218;
	}

IL_0216:
	{
		return 1;
	}

IL_0218:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_23 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		if ((((t42 *)p1) == ((t42 *)L_23)))
		{
			goto IL_023f;
		}
	}
	{
		bool L_24 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_24)
		{
			goto IL_023c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_25 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		G_B58_0 = ((((t42 *)p1) == ((t42 *)L_25))? 1 : 0);
		goto IL_023d;
	}

IL_023c:
	{
		G_B58_0 = 0;
	}

IL_023d:
	{
		G_B60_0 = G_B58_0;
		goto IL_0240;
	}

IL_023f:
	{
		G_B60_0 = 1;
	}

IL_0240:
	{
		return G_B60_0;
	}

IL_0241:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-((int32_t)11))) == 0)
		{
			goto IL_025e;
		}
		if (((int32_t)(V_3-((int32_t)11))) == 1)
		{
			goto IL_025e;
		}
		if (((int32_t)(V_3-((int32_t)11))) == 2)
		{
			goto IL_025e;
		}
		if (((int32_t)(V_3-((int32_t)11))) == 3)
		{
			goto IL_025e;
		}
	}
	{
		goto IL_0260;
	}

IL_025e:
	{
		return 1;
	}

IL_0260:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_26 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		if ((((t42 *)p1) == ((t42 *)L_26)))
		{
			goto IL_0287;
		}
	}
	{
		bool L_27 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_27)
		{
			goto IL_0284;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_28 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		G_B68_0 = ((((t42 *)p1) == ((t42 *)L_28))? 1 : 0);
		goto IL_0285;
	}

IL_0284:
	{
		G_B68_0 = 0;
	}

IL_0285:
	{
		G_B70_0 = G_B68_0;
		goto IL_0288;
	}

IL_0287:
	{
		G_B70_0 = 1;
	}

IL_0288:
	{
		return G_B70_0;
	}

IL_0289:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-((int32_t)11))) == 0)
		{
			goto IL_02a6;
		}
		if (((int32_t)(V_3-((int32_t)11))) == 1)
		{
			goto IL_02a8;
		}
		if (((int32_t)(V_3-((int32_t)11))) == 2)
		{
			goto IL_02a6;
		}
		if (((int32_t)(V_3-((int32_t)11))) == 3)
		{
			goto IL_02a6;
		}
	}
	{
		goto IL_02a8;
	}

IL_02a6:
	{
		return 1;
	}

IL_02a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_29 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		if ((((t42 *)p1) == ((t42 *)L_29)))
		{
			goto IL_02cf;
		}
	}
	{
		bool L_30 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_30)
		{
			goto IL_02cc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_31 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		G_B78_0 = ((((t42 *)p1) == ((t42 *)L_31))? 1 : 0);
		goto IL_02cd;
	}

IL_02cc:
	{
		G_B78_0 = 0;
	}

IL_02cd:
	{
		G_B80_0 = G_B78_0;
		goto IL_02d0;
	}

IL_02cf:
	{
		G_B80_0 = 1;
	}

IL_02d0:
	{
		return G_B80_0;
	}

IL_02d1:
	{
		V_3 = V_1;
		if ((((int32_t)V_3) == ((int32_t)((int32_t)13))))
		{
			goto IL_02df;
		}
	}
	{
		if ((((int32_t)V_3) == ((int32_t)((int32_t)14))))
		{
			goto IL_02df;
		}
	}
	{
		goto IL_02e1;
	}

IL_02df:
	{
		return 1;
	}

IL_02e1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_32 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		if ((((t42 *)p1) == ((t42 *)L_32)))
		{
			goto IL_0308;
		}
	}
	{
		bool L_33 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_33)
		{
			goto IL_0305;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_34 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		G_B89_0 = ((((t42 *)p1) == ((t42 *)L_34))? 1 : 0);
		goto IL_0306;
	}

IL_0305:
	{
		G_B89_0 = 0;
	}

IL_0306:
	{
		G_B91_0 = G_B89_0;
		goto IL_0309;
	}

IL_0308:
	{
		G_B91_0 = 1;
	}

IL_0309:
	{
		return G_B91_0;
	}

IL_030a:
	{
		if ((((int32_t)V_1) == ((int32_t)((int32_t)14))))
		{
			goto IL_031e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_35 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		G_B95_0 = ((((t42 *)p1) == ((t42 *)L_35))? 1 : 0);
		goto IL_031f;
	}

IL_031e:
	{
		G_B95_0 = 1;
	}

IL_031f:
	{
		return G_B95_0;
	}

IL_0320:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_36 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_36)))
		{
			goto IL_0337;
		}
	}
	{
		bool L_37 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, p0);
		if (!L_37)
		{
			goto IL_0337;
		}
	}
	{
		return 1;
	}

IL_0337:
	{
		bool L_38 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6001_MI, p1);
		if (!L_38)
		{
			goto IL_034e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_39 = m1554(NULL, LoadTypeToken(&t35_0_0_0), &m1554_MI);
		if ((((t42 *)p0) != ((t42 *)L_39)))
		{
			goto IL_034e;
		}
	}
	{
		return 1;
	}

IL_034e:
	{
		bool L_40 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m2981_MI, p1, p0);
		return L_40;
	}
}
 bool m7500 (t29 * __this, t537* p0, t638* p1, bool p2, MethodInfo* method){
	int32_t V_0 = 0;
	bool V_1 = false;
	t42 * V_2 = {0};
	{
		V_0 = 0;
		goto IL_0044;
	}

IL_0004:
	{
		int32_t L_0 = V_0;
		int32_t L_1 = V_0;
		t42 * L_2 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(p1, L_1)));
		bool L_3 = m7499(NULL, (*(t42 **)(t42 **)SZArrayLdElema(p0, L_0)), L_2, &m7499_MI);
		V_1 = L_3;
		if (V_1)
		{
			goto IL_003b;
		}
	}
	{
		if (!p2)
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_4 = V_0;
		t42 * L_5 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(p1, L_4)));
		V_2 = L_5;
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5994_MI, V_2);
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_7 = V_0;
		t42 * L_8 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, V_2);
		bool L_9 = m7499(NULL, (*(t42 **)(t42 **)SZArrayLdElema(p0, L_7)), L_8, &m7499_MI);
		V_1 = L_9;
	}

IL_003b:
	{
		if (V_1)
		{
			goto IL_0040;
		}
	}
	{
		return 0;
	}

IL_0040:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0044:
	{
		if ((((int32_t)V_0) < ((int32_t)(((int32_t)(((t20 *)p0)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		return 1;
	}
}
extern MethodInfo m7501_MI;
 t636 * m7501 (t1364 * __this, int32_t p0, t1365* p1, t537* p2, t634* p3, MethodInfo* method){
	{
		t636 * L_0 = m7502(__this, p0, p1, p2, p3, 0, &m7502_MI);
		return L_0;
	}
}
 t636 * m7502 (t1364 * __this, int32_t p0, t1365* p1, t537* p2, t634* p3, bool p4, MethodInfo* method){
	t636 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	t638* V_3 = {0};
	bool V_4 = false;
	t42 * V_5 = {0};
	t638* V_6 = {0};
	t636 * V_7 = {0};
	t638* V_8 = {0};
	{
		if (p1)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1033, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		V_1 = 0;
		goto IL_0050;
	}

IL_0012:
	{
		int32_t L_1 = V_1;
		V_0 = (*(t636 **)(t636 **)SZArrayLdElema(p1, L_1));
		t638* L_2 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, V_0);
		V_3 = L_2;
		if ((((int32_t)(((int32_t)(((t20 *)V_3)->max_length)))) == ((int32_t)(((int32_t)(((t20 *)p2)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		goto IL_004c;
	}

IL_0027:
	{
		V_2 = 0;
		goto IL_003e;
	}

IL_002b:
	{
		int32_t L_3 = V_2;
		int32_t L_4 = V_2;
		t42 * L_5 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_3, L_4)));
		if ((((t42 *)(*(t42 **)(t42 **)SZArrayLdElema(p2, L_3))) == ((t42 *)L_5)))
		{
			goto IL_003a;
		}
	}
	{
		goto IL_0044;
	}

IL_003a:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_003e:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)p2)->max_length))))))
		{
			goto IL_002b;
		}
	}

IL_0044:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(((int32_t)(((t20 *)p2)->max_length))))))
		{
			goto IL_004c;
		}
	}
	{
		return V_0;
	}

IL_004c:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0050:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)p1)->max_length))))))
		{
			goto IL_0012;
		}
	}
	{
		V_4 = 0;
		V_5 = (t42 *)NULL;
		V_1 = 0;
		goto IL_0102;
	}

IL_0063:
	{
		int32_t L_6 = V_1;
		V_0 = (*(t636 **)(t636 **)SZArrayLdElema(p1, L_6));
		t638* L_7 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, V_0);
		V_6 = L_7;
		if ((((int32_t)(((int32_t)(((t20 *)V_6)->max_length)))) <= ((int32_t)(((int32_t)(((t20 *)p2)->max_length))))))
		{
			goto IL_007d;
		}
	}
	{
		goto IL_00fe;
	}

IL_007d:
	{
		if ((((int32_t)(((t20 *)V_6)->max_length))))
		{
			goto IL_0088;
		}
	}
	{
		goto IL_00fe;
	}

IL_0088:
	{
		int32_t L_8 = ((int32_t)((((int32_t)(((t20 *)V_6)->max_length)))-1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t391_0_0_0), &m1554_MI);
		bool L_10 = m5292(NULL, (*(t637 **)(t637 **)SZArrayLdElema(V_6, L_8)), L_9, &m5292_MI);
		V_4 = L_10;
		if (V_4)
		{
			goto IL_00a8;
		}
	}
	{
		goto IL_00fe;
	}

IL_00a8:
	{
		int32_t L_11 = ((int32_t)((((int32_t)(((t20 *)V_6)->max_length)))-1));
		t42 * L_12 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_6, L_11)));
		t42 * L_13 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, L_12);
		V_5 = L_13;
		V_2 = 0;
		goto IL_00f0;
	}

IL_00c1:
	{
		if ((((int32_t)V_2) >= ((int32_t)((int32_t)((((int32_t)(((t20 *)V_6)->max_length)))-1)))))
		{
			goto IL_00da;
		}
	}
	{
		int32_t L_14 = V_2;
		int32_t L_15 = V_2;
		t42 * L_16 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_6, L_15)));
		if ((((t42 *)(*(t42 **)(t42 **)SZArrayLdElema(p2, L_14))) == ((t42 *)L_16)))
		{
			goto IL_00da;
		}
	}
	{
		goto IL_00f6;
	}

IL_00da:
	{
		if ((((int32_t)V_2) < ((int32_t)((int32_t)((((int32_t)(((t20 *)V_6)->max_length)))-1)))))
		{
			goto IL_00ec;
		}
	}
	{
		int32_t L_17 = V_2;
		if ((((t42 *)(*(t42 **)(t42 **)SZArrayLdElema(p2, L_17))) == ((t42 *)V_5)))
		{
			goto IL_00ec;
		}
	}
	{
		goto IL_00f6;
	}

IL_00ec:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_00f0:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)p2)->max_length))))))
		{
			goto IL_00c1;
		}
	}

IL_00f6:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(((int32_t)(((t20 *)p2)->max_length))))))
		{
			goto IL_00fe;
		}
	}
	{
		return V_0;
	}

IL_00fe:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0102:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)p1)->max_length))))))
		{
			goto IL_0063;
		}
	}
	{
		if (!((int32_t)((int32_t)p0&(int32_t)((int32_t)65536))))
		{
			goto IL_0116;
		}
	}
	{
		return (t636 *)NULL;
	}

IL_0116:
	{
		V_7 = (t636 *)NULL;
		V_1 = 0;
		goto IL_015b;
	}

IL_011d:
	{
		int32_t L_18 = V_1;
		V_0 = (*(t636 **)(t636 **)SZArrayLdElema(p1, L_18));
		t638* L_19 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, V_0);
		V_8 = L_19;
		if ((((int32_t)(((int32_t)(((t20 *)V_8)->max_length)))) == ((int32_t)(((int32_t)(((t20 *)p2)->max_length))))))
		{
			goto IL_0134;
		}
	}
	{
		goto IL_0157;
	}

IL_0134:
	{
		bool L_20 = m7500(NULL, p2, V_8, p4, &m7500_MI);
		if (L_20)
		{
			goto IL_0142;
		}
	}
	{
		goto IL_0157;
	}

IL_0142:
	{
		if (!V_7)
		{
			goto IL_0154;
		}
	}
	{
		t636 * L_21 = m7503(__this, V_7, V_0, p2, &m7503_MI);
		V_7 = L_21;
		goto IL_0157;
	}

IL_0154:
	{
		V_7 = V_0;
	}

IL_0157:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_015b:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)p1)->max_length))))))
		{
			goto IL_011d;
		}
	}
	{
		return V_7;
	}
}
 t636 * m7503 (t1364 * __this, t636 * p0, t636 * p1, t537* p2, MethodInfo* method){
	t638* V_0 = {0};
	t638* V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	t42 * V_5 = {0};
	t42 * V_6 = {0};
	bool V_7 = false;
	bool V_8 = false;
	t636 * G_B19_0 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7552_MI, p0);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7552_MI, p1);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return p1;
	}

IL_0012:
	{
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7552_MI, p1);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7552_MI, p0);
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		return p0;
	}

IL_0024:
	{
		t638* L_4 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, p0);
		V_0 = L_4;
		t638* L_5 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, p1);
		V_1 = L_5;
		V_2 = 0;
		V_3 = 0;
		goto IL_006d;
	}

IL_0038:
	{
		int32_t L_6 = V_3;
		t42 * L_7 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_0, L_6)));
		int32_t L_8 = V_3;
		t42 * L_9 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_1, L_8)));
		int32_t L_10 = m7504(__this, L_7, L_9, &m7504_MI);
		V_4 = L_10;
		if (!V_4)
		{
			goto IL_0062;
		}
	}
	{
		if (!V_2)
		{
			goto IL_0062;
		}
	}
	{
		if ((((int32_t)V_2) == ((int32_t)V_4)))
		{
			goto IL_0062;
		}
	}
	{
		t1357 * L_11 = (t1357 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1357_TI));
		m7455(L_11, &m7455_MI);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_0062:
	{
		if (!V_4)
		{
			goto IL_0069;
		}
	}
	{
		V_2 = V_4;
	}

IL_0069:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_006d:
	{
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_0038;
		}
	}
	{
		if (!V_2)
		{
			goto IL_007f;
		}
	}
	{
		if ((((int32_t)V_2) <= ((int32_t)0)))
		{
			goto IL_007d;
		}
	}
	{
		G_B19_0 = p1;
		goto IL_007e;
	}

IL_007d:
	{
		G_B19_0 = p0;
	}

IL_007e:
	{
		return G_B19_0;
	}

IL_007f:
	{
		t42 * L_12 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, p0);
		V_5 = L_12;
		t42 * L_13 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, p1);
		V_6 = L_13;
		if ((((t42 *)V_5) == ((t42 *)V_6)))
		{
			goto IL_00af;
		}
	}
	{
		bool L_14 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m6019_MI, V_5, V_6);
		if (!L_14)
		{
			goto IL_00a2;
		}
	}
	{
		return p0;
	}

IL_00a2:
	{
		bool L_15 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m6019_MI, V_6, V_5);
		if (!L_15)
		{
			goto IL_00af;
		}
	}
	{
		return p1;
	}

IL_00af:
	{
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7547_MI, p0);
		V_7 = ((((int32_t)((((int32_t)((int32_t)((int32_t)L_16&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7547_MI, p1);
		V_8 = ((((int32_t)((((int32_t)((int32_t)((int32_t)L_17&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		if (!V_7)
		{
			goto IL_00d9;
		}
	}
	{
		if (V_8)
		{
			goto IL_00d9;
		}
	}
	{
		return p1;
	}

IL_00d9:
	{
		if (!V_8)
		{
			goto IL_00e3;
		}
	}
	{
		if (V_7)
		{
			goto IL_00e3;
		}
	}
	{
		return p0;
	}

IL_00e3:
	{
		t1357 * L_18 = (t1357 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1357_TI));
		m7455(L_18, &m7455_MI);
		il2cpp_codegen_raise_exception(L_18);
	}
}
 int32_t m7504 (t1364 * __this, t42 * p0, t42 * p1, MethodInfo* method){
	{
		if ((((t42 *)p0) != ((t42 *)p1)))
		{
			goto IL_0006;
		}
	}
	{
		return 0;
	}

IL_0006:
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6044_MI, p0);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6044_MI, p1);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return 1;
	}

IL_0018:
	{
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6044_MI, p0);
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6044_MI, p1);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		return (-1);
	}

IL_002a:
	{
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5991_MI, p0);
		if (!L_4)
		{
			goto IL_004d;
		}
	}
	{
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5991_MI, p1);
		if (!L_5)
		{
			goto IL_004d;
		}
	}
	{
		t42 * L_6 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, p0);
		t42 * L_7 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, p1);
		int32_t L_8 = m7504(__this, L_6, L_7, &m7504_MI);
		return L_8;
	}

IL_004d:
	{
		bool L_9 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m6019_MI, p0, p1);
		if (!L_9)
		{
			goto IL_0058;
		}
	}
	{
		return (-1);
	}

IL_0058:
	{
		bool L_10 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m6019_MI, p1, p0);
		if (!L_10)
		{
			goto IL_0063;
		}
	}
	{
		return 1;
	}

IL_0063:
	{
		bool L_11 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5999_MI, p0);
		if (!L_11)
		{
			goto IL_007c;
		}
	}
	{
		t537* L_12 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m9850_MI, p1);
		int32_t L_13 = m10142(NULL, L_12, p0, &m10142_MI);
		if ((((int32_t)L_13) < ((int32_t)0)))
		{
			goto IL_007c;
		}
	}
	{
		return 1;
	}

IL_007c:
	{
		bool L_14 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5999_MI, p1);
		if (!L_14)
		{
			goto IL_0095;
		}
	}
	{
		t537* L_15 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m9850_MI, p0);
		int32_t L_16 = m10142(NULL, L_15, p1, &m10142_MI);
		if ((((int32_t)L_16) < ((int32_t)0)))
		{
			goto IL_0095;
		}
	}
	{
		return (-1);
	}

IL_0095:
	{
		return 0;
	}
}
extern MethodInfo m7505_MI;
 t1146 * m7505 (t1364 * __this, int32_t p0, t1366* p1, t42 * p2, t537* p3, t634* p4, MethodInfo* method){
	bool V_0 = false;
	int32_t V_1 = 0;
	t1146 * V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	t1146 * V_7 = {0};
	t638* V_8 = {0};
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t G_B6_0 = 0;
	{
		if (!p1)
		{
			goto IL_0008;
		}
	}
	{
		if ((((int32_t)(((t20 *)p1)->max_length))))
		{
			goto IL_0018;
		}
	}

IL_0008:
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_0, (t7*) &_stringLiteral1527, (t7*) &_stringLiteral1033, &m3973_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0018:
	{
		V_0 = ((((int32_t)((((t42 *)p2) == ((t29 *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		if (!p3)
		{
			goto IL_002a;
		}
	}
	{
		G_B6_0 = (((int32_t)(((t20 *)p3)->max_length)));
		goto IL_002b;
	}

IL_002a:
	{
		G_B6_0 = (-1);
	}

IL_002b:
	{
		V_1 = G_B6_0;
		V_2 = (t1146 *)NULL;
		V_4 = ((int32_t)2147483646);
		V_5 = ((int32_t)2147483647);
		V_6 = 0;
		V_3 = ((int32_t)((((int32_t)(((t20 *)p1)->max_length)))-1));
		goto IL_00d6;
	}

IL_004a:
	{
		int32_t L_1 = V_3;
		V_7 = (*(t1146 **)(t1146 **)SZArrayLdElema(p1, L_1));
		t638* L_2 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m10143_MI, V_7);
		V_8 = L_2;
		if ((((int32_t)V_1) < ((int32_t)0)))
		{
			goto IL_0068;
		}
	}
	{
		if ((((int32_t)V_1) == ((int32_t)(((int32_t)(((t20 *)V_8)->max_length))))))
		{
			goto IL_0068;
		}
	}
	{
		goto IL_00d2;
	}

IL_0068:
	{
		if (!V_0)
		{
			goto IL_0077;
		}
	}
	{
		t42 * L_3 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m10144_MI, V_7);
		if ((((t42 *)L_3) == ((t42 *)p2)))
		{
			goto IL_0077;
		}
	}
	{
		goto IL_00d2;
	}

IL_0077:
	{
		V_9 = ((int32_t)2147483646);
		if ((((int32_t)V_1) <= ((int32_t)0)))
		{
			goto IL_0094;
		}
	}
	{
		int32_t L_4 = m7506(NULL, p3, V_8, &m7506_MI);
		V_9 = L_4;
		if ((((uint32_t)V_9) != ((uint32_t)(-1))))
		{
			goto IL_0094;
		}
	}
	{
		goto IL_00d2;
	}

IL_0094:
	{
		t42 * L_5 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, V_7);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		int32_t L_6 = m7512(NULL, L_5, &m7512_MI);
		V_10 = L_6;
		if (!V_2)
		{
			goto IL_00c7;
		}
	}
	{
		if ((((int32_t)V_4) >= ((int32_t)V_9)))
		{
			goto IL_00ad;
		}
	}
	{
		goto IL_00d2;
	}

IL_00ad:
	{
		if ((((uint32_t)V_4) != ((uint32_t)V_9)))
		{
			goto IL_00c7;
		}
	}
	{
		if ((((uint32_t)V_6) != ((uint32_t)V_10)))
		{
			goto IL_00bf;
		}
	}
	{
		V_5 = V_9;
		goto IL_00d2;
	}

IL_00bf:
	{
		if ((((int32_t)V_6) <= ((int32_t)V_10)))
		{
			goto IL_00c7;
		}
	}
	{
		goto IL_00d2;
	}

IL_00c7:
	{
		V_2 = V_7;
		V_4 = V_9;
		V_6 = V_10;
	}

IL_00d2:
	{
		V_3 = ((int32_t)(V_3-1));
	}

IL_00d6:
	{
		if ((((int32_t)V_3) >= ((int32_t)0)))
		{
			goto IL_004a;
		}
	}
	{
		if ((((int32_t)V_5) > ((int32_t)V_4)))
		{
			goto IL_00e9;
		}
	}
	{
		t1357 * L_7 = (t1357 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1357_TI));
		m7455(L_7, &m7455_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_00e9:
	{
		return V_2;
	}
}
 int32_t m7506 (t29 * __this, t537* p0, t638* p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = (-1);
		V_1 = 0;
		goto IL_0027;
	}

IL_0006:
	{
		int32_t L_0 = V_1;
		int32_t L_1 = V_1;
		t42 * L_2 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(p1, L_1)));
		int32_t L_3 = m7507(NULL, (*(t42 **)(t42 **)SZArrayLdElema(p0, L_0)), L_2, &m7507_MI);
		V_2 = L_3;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_001d;
		}
	}
	{
		return (-1);
	}

IL_001d:
	{
		if ((((int32_t)V_0) >= ((int32_t)V_2)))
		{
			goto IL_0023;
		}
	}
	{
		V_0 = V_2;
	}

IL_0023:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0027:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)p0)->max_length))))))
		{
			goto IL_0006;
		}
	}
	{
		return V_0;
	}
}
 int32_t m7507 (t29 * __this, t42 * p0, t42 * p1, MethodInfo* method){
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	int32_t V_2 = {0};
	int32_t V_3 = {0};
	int32_t G_B4_0 = 0;
	int32_t G_B23_0 = 0;
	int32_t G_B31_0 = 0;
	int32_t G_B39_0 = 0;
	int32_t G_B47_0 = 0;
	int32_t G_B55_0 = 0;
	int32_t G_B63_0 = 0;
	int32_t G_B72_0 = 0;
	int32_t G_B76_0 = 0;
	int32_t G_B80_0 = 0;
	{
		if (p0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, p1);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		G_B4_0 = (-1);
		goto IL_000f;
	}

IL_000e:
	{
		G_B4_0 = 0;
	}

IL_000f:
	{
		return G_B4_0;
	}

IL_0010:
	{
		if ((((t42 *)p0) != ((t42 *)p1)))
		{
			goto IL_0016;
		}
	}
	{
		return 0;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t29_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_1)))
		{
			goto IL_0025;
		}
	}
	{
		return 4;
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		int32_t L_2 = m6015(NULL, p0, &m6015_MI);
		V_0 = L_2;
		int32_t L_3 = m6015(NULL, p1, &m6015_MI);
		V_1 = L_3;
		V_2 = V_0;
		if (((int32_t)(V_2-4)) == 0)
		{
			goto IL_006a;
		}
		if (((int32_t)(V_2-4)) == 1)
		{
			goto IL_00ec;
		}
		if (((int32_t)(V_2-4)) == 2)
		{
			goto IL_0098;
		}
		if (((int32_t)(V_2-4)) == 3)
		{
			goto IL_0175;
		}
		if (((int32_t)(V_2-4)) == 4)
		{
			goto IL_0134;
		}
		if (((int32_t)(V_2-4)) == 5)
		{
			goto IL_01ef;
		}
		if (((int32_t)(V_2-4)) == 6)
		{
			goto IL_01b6;
		}
		if (((int32_t)(V_2-4)) == 7)
		{
			goto IL_0228;
		}
		if (((int32_t)(V_2-4)) == 8)
		{
			goto IL_0228;
		}
		if (((int32_t)(V_2-4)) == 9)
		{
			goto IL_0252;
		}
	}
	{
		goto IL_025c;
	}

IL_006a:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-8)) == 0)
		{
			goto IL_0092;
		}
		if (((int32_t)(V_3-8)) == 1)
		{
			goto IL_0094;
		}
		if (((int32_t)(V_3-8)) == 2)
		{
			goto IL_0094;
		}
		if (((int32_t)(V_3-8)) == 3)
		{
			goto IL_0094;
		}
		if (((int32_t)(V_3-8)) == 4)
		{
			goto IL_0094;
		}
		if (((int32_t)(V_3-8)) == 5)
		{
			goto IL_0094;
		}
		if (((int32_t)(V_3-8)) == 6)
		{
			goto IL_0094;
		}
	}
	{
		goto IL_0096;
	}

IL_0092:
	{
		return 0;
	}

IL_0094:
	{
		return 2;
	}

IL_0096:
	{
		return (-1);
	}

IL_0098:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-4)) == 0)
		{
			goto IL_00d0;
		}
		if (((int32_t)(V_3-4)) == 1)
		{
			goto IL_00d2;
		}
		if (((int32_t)(V_3-4)) == 2)
		{
			goto IL_00d2;
		}
		if (((int32_t)(V_3-4)) == 3)
		{
			goto IL_00d0;
		}
		if (((int32_t)(V_3-4)) == 4)
		{
			goto IL_00d0;
		}
		if (((int32_t)(V_3-4)) == 5)
		{
			goto IL_00d0;
		}
		if (((int32_t)(V_3-4)) == 6)
		{
			goto IL_00d0;
		}
		if (((int32_t)(V_3-4)) == 7)
		{
			goto IL_00d0;
		}
		if (((int32_t)(V_3-4)) == 8)
		{
			goto IL_00d0;
		}
		if (((int32_t)(V_3-4)) == 9)
		{
			goto IL_00d0;
		}
		if (((int32_t)(V_3-4)) == 10)
		{
			goto IL_00d0;
		}
	}
	{
		goto IL_00d2;
	}

IL_00d0:
	{
		return 2;
	}

IL_00d2:
	{
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_4)
		{
			goto IL_00ea;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_5 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_5)))
		{
			goto IL_00ea;
		}
	}
	{
		G_B23_0 = 1;
		goto IL_00eb;
	}

IL_00ea:
	{
		G_B23_0 = (-1);
	}

IL_00eb:
	{
		return G_B23_0;
	}

IL_00ec:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-7)) == 0)
		{
			goto IL_0118;
		}
		if (((int32_t)(V_3-7)) == 1)
		{
			goto IL_011a;
		}
		if (((int32_t)(V_3-7)) == 2)
		{
			goto IL_0118;
		}
		if (((int32_t)(V_3-7)) == 3)
		{
			goto IL_011a;
		}
		if (((int32_t)(V_3-7)) == 4)
		{
			goto IL_0118;
		}
		if (((int32_t)(V_3-7)) == 5)
		{
			goto IL_011a;
		}
		if (((int32_t)(V_3-7)) == 6)
		{
			goto IL_0118;
		}
		if (((int32_t)(V_3-7)) == 7)
		{
			goto IL_0118;
		}
	}
	{
		goto IL_011a;
	}

IL_0118:
	{
		return 2;
	}

IL_011a:
	{
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_6)
		{
			goto IL_0132;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_7 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_7)))
		{
			goto IL_0132;
		}
	}
	{
		G_B31_0 = 1;
		goto IL_0133;
	}

IL_0132:
	{
		G_B31_0 = (-1);
	}

IL_0133:
	{
		return G_B31_0;
	}

IL_0134:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-((int32_t)9))) == 0)
		{
			goto IL_0159;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 1)
		{
			goto IL_0159;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 2)
		{
			goto IL_0159;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 3)
		{
			goto IL_0159;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 4)
		{
			goto IL_0159;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 5)
		{
			goto IL_0159;
		}
	}
	{
		goto IL_015b;
	}

IL_0159:
	{
		return 2;
	}

IL_015b:
	{
		bool L_8 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_8)
		{
			goto IL_0173;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_9)))
		{
			goto IL_0173;
		}
	}
	{
		G_B39_0 = 1;
		goto IL_0174;
	}

IL_0173:
	{
		G_B39_0 = (-1);
	}

IL_0174:
	{
		return G_B39_0;
	}

IL_0175:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-((int32_t)9))) == 0)
		{
			goto IL_019a;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 1)
		{
			goto IL_019c;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 2)
		{
			goto IL_019a;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 3)
		{
			goto IL_019c;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 4)
		{
			goto IL_019a;
		}
		if (((int32_t)(V_3-((int32_t)9))) == 5)
		{
			goto IL_019a;
		}
	}
	{
		goto IL_019c;
	}

IL_019a:
	{
		return 2;
	}

IL_019c:
	{
		bool L_10 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_10)
		{
			goto IL_01b4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_11 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_11)))
		{
			goto IL_01b4;
		}
	}
	{
		G_B47_0 = 1;
		goto IL_01b5;
	}

IL_01b4:
	{
		G_B47_0 = (-1);
	}

IL_01b5:
	{
		return G_B47_0;
	}

IL_01b6:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-((int32_t)11))) == 0)
		{
			goto IL_01d3;
		}
		if (((int32_t)(V_3-((int32_t)11))) == 1)
		{
			goto IL_01d3;
		}
		if (((int32_t)(V_3-((int32_t)11))) == 2)
		{
			goto IL_01d3;
		}
		if (((int32_t)(V_3-((int32_t)11))) == 3)
		{
			goto IL_01d3;
		}
	}
	{
		goto IL_01d5;
	}

IL_01d3:
	{
		return 2;
	}

IL_01d5:
	{
		bool L_12 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_12)
		{
			goto IL_01ed;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_13 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_13)))
		{
			goto IL_01ed;
		}
	}
	{
		G_B55_0 = 1;
		goto IL_01ee;
	}

IL_01ed:
	{
		G_B55_0 = (-1);
	}

IL_01ee:
	{
		return G_B55_0;
	}

IL_01ef:
	{
		V_3 = V_1;
		if (((int32_t)(V_3-((int32_t)11))) == 0)
		{
			goto IL_020c;
		}
		if (((int32_t)(V_3-((int32_t)11))) == 1)
		{
			goto IL_020e;
		}
		if (((int32_t)(V_3-((int32_t)11))) == 2)
		{
			goto IL_020c;
		}
		if (((int32_t)(V_3-((int32_t)11))) == 3)
		{
			goto IL_020c;
		}
	}
	{
		goto IL_020e;
	}

IL_020c:
	{
		return 2;
	}

IL_020e:
	{
		bool L_14 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_14)
		{
			goto IL_0226;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_15 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_15)))
		{
			goto IL_0226;
		}
	}
	{
		G_B63_0 = 1;
		goto IL_0227;
	}

IL_0226:
	{
		G_B63_0 = (-1);
	}

IL_0227:
	{
		return G_B63_0;
	}

IL_0228:
	{
		V_3 = V_1;
		if ((((int32_t)V_3) == ((int32_t)((int32_t)13))))
		{
			goto IL_0236;
		}
	}
	{
		if ((((int32_t)V_3) == ((int32_t)((int32_t)14))))
		{
			goto IL_0236;
		}
	}
	{
		goto IL_0238;
	}

IL_0236:
	{
		return 2;
	}

IL_0238:
	{
		bool L_16 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5997_MI, p0);
		if (!L_16)
		{
			goto IL_0250;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_17 = m1554(NULL, LoadTypeToken(&t49_0_0_0), &m1554_MI);
		if ((((t42 *)p1) != ((t42 *)L_17)))
		{
			goto IL_0250;
		}
	}
	{
		G_B72_0 = 1;
		goto IL_0251;
	}

IL_0250:
	{
		G_B72_0 = (-1);
	}

IL_0251:
	{
		return G_B72_0;
	}

IL_0252:
	{
		if ((((uint32_t)V_1) != ((uint32_t)((int32_t)14))))
		{
			goto IL_025a;
		}
	}
	{
		G_B76_0 = 2;
		goto IL_025b;
	}

IL_025a:
	{
		G_B76_0 = (-1);
	}

IL_025b:
	{
		return G_B76_0;
	}

IL_025c:
	{
		bool L_18 = (bool)VirtFuncInvoker1< bool, t42 * >::Invoke(&m2981_MI, p1, p0);
		if (!L_18)
		{
			goto IL_0268;
		}
	}
	{
		G_B80_0 = 3;
		goto IL_0269;
	}

IL_0268:
	{
		G_B80_0 = (-1);
	}

IL_0269:
	{
		return G_B80_0;
	}
}
// Metadata Definition System.Reflection.Binder/Default
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7493_MI = 
{
	".ctor", (methodPointerType)&m7493, &t1364_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3014, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t1365_0_0_0;
extern Il2CppType t1365_0_0_0;
extern Il2CppType t316_1_0_0;
extern Il2CppType t316_1_0_0;
extern Il2CppType t634_0_0_0;
extern Il2CppType t634_0_0_0;
extern Il2CppType t633_0_0_0;
extern Il2CppType t633_0_0_0;
extern Il2CppType t446_0_0_0;
extern Il2CppType t446_0_0_0;
extern Il2CppType t29_1_0_2;
extern Il2CppType t29_1_0_0;
static ParameterInfo t1364_m7494_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221501, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"match", 1, 134221502, &EmptyCustomAttributesCache, &t1365_0_0_0},
	{"args", 2, 134221503, &EmptyCustomAttributesCache, &t316_1_0_0},
	{"modifiers", 3, 134221504, &EmptyCustomAttributesCache, &t634_0_0_0},
	{"culture", 4, 134221505, &EmptyCustomAttributesCache, &t633_0_0_0},
	{"names", 5, 134221506, &EmptyCustomAttributesCache, &t446_0_0_0},
	{"state", 6, 134221507, &EmptyCustomAttributesCache, &t29_1_0_2},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t1704_t29_t29_t29_t2022 (MethodInfo* method, void* obj, void** args);
MethodInfo m7494_MI = 
{
	"BindToMethod", (methodPointerType)&m7494, &t1364_TI, &t636_0_0_0, RuntimeInvoker_t29_t44_t29_t1704_t29_t29_t29_t2022, t1364_m7494_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 7, false, false, 3015, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t446_0_0_0;
extern Il2CppType t316_1_0_0;
extern Il2CppType t636_0_0_0;
extern Il2CppType t636_0_0_0;
static ParameterInfo t1364_m7495_ParameterInfos[] = 
{
	{"names", 0, 134221508, &EmptyCustomAttributesCache, &t446_0_0_0},
	{"args", 1, 134221509, &EmptyCustomAttributesCache, &t316_1_0_0},
	{"selected", 2, 134221510, &EmptyCustomAttributesCache, &t636_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t1704_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7495_MI = 
{
	"ReorderParameters", (methodPointerType)&m7495, &t1364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t1704_t29, t1364_m7495_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 3016, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1364_m7496_ParameterInfos[] = 
{
	{"object_type", 0, 134221511, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"target_type", 1, 134221512, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7496_MI = 
{
	"IsArrayAssignable", (methodPointerType)&m7496, &t1364_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1364_m7496_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 3017, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t1364_m7497_ParameterInfos[] = 
{
	{"value", 0, 134221513, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"type", 1, 134221514, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"culture", 2, 134221515, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7497_MI = 
{
	"ChangeType", (methodPointerType)&m7497, &t1364_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t1364_m7497_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 3, false, false, 3018, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_1_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1364_m7498_ParameterInfos[] = 
{
	{"args", 0, 134221516, &EmptyCustomAttributesCache, &t316_1_0_0},
	{"state", 1, 134221517, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t1704_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1364__CustomAttributeCache_m7498;
MethodInfo m7498_MI = 
{
	"ReorderArgumentArray", (methodPointerType)&m7498, &t1364_TI, &t21_0_0_0, RuntimeInvoker_t21_t1704_t29, t1364_m7498_ParameterInfos, &t1364__CustomAttributeCache_m7498, 198, 0, 6, 2, false, false, 3019, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1364_m7499_ParameterInfos[] = 
{
	{"from", 0, 134221518, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"to", 1, 134221519, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7499_MI = 
{
	"check_type", (methodPointerType)&m7499, &t1364_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1364_m7499_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 3020, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t638_0_0_0;
extern Il2CppType t638_0_0_0;
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1364_m7500_ParameterInfos[] = 
{
	{"types", 0, 134221520, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"args", 1, 134221521, &EmptyCustomAttributesCache, &t638_0_0_0},
	{"allowByRefMatch", 2, 134221522, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7500_MI = 
{
	"check_arguments", (methodPointerType)&m7500, &t1364_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29_t297, t1364_m7500_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 3, false, false, 3021, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
extern Il2CppType t1365_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t634_0_0_0;
static ParameterInfo t1364_m7501_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221523, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"match", 1, 134221524, &EmptyCustomAttributesCache, &t1365_0_0_0},
	{"types", 2, 134221525, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"modifiers", 3, 134221526, &EmptyCustomAttributesCache, &t634_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7501_MI = 
{
	"SelectMethod", (methodPointerType)&m7501, &t1364_TI, &t636_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29, t1364_m7501_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 7, 4, false, false, 3022, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
extern Il2CppType t1365_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t634_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1364_m7502_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221527, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"match", 1, 134221528, &EmptyCustomAttributesCache, &t1365_0_0_0},
	{"types", 2, 134221529, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"modifiers", 3, 134221530, &EmptyCustomAttributesCache, &t634_0_0_0},
	{"allowByRefMatch", 4, 134221531, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7502_MI = 
{
	"SelectMethod", (methodPointerType)&m7502, &t1364_TI, &t636_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29_t297, t1364_m7502_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 5, false, false, 3023, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t636_0_0_0;
extern Il2CppType t636_0_0_0;
extern Il2CppType t537_0_0_0;
static ParameterInfo t1364_m7503_ParameterInfos[] = 
{
	{"m1", 0, 134221532, &EmptyCustomAttributesCache, &t636_0_0_0},
	{"m2", 1, 134221533, &EmptyCustomAttributesCache, &t636_0_0_0},
	{"types", 2, 134221534, &EmptyCustomAttributesCache, &t537_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7503_MI = 
{
	"GetBetterMethod", (methodPointerType)&m7503, &t1364_TI, &t636_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t1364_m7503_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, false, 3024, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1364_m7504_ParameterInfos[] = 
{
	{"t1", 0, 134221535, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"t2", 1, 134221536, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7504_MI = 
{
	"CompareCloserType", (methodPointerType)&m7504, &t1364_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t1364_m7504_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 3025, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
extern Il2CppType t1366_0_0_0;
extern Il2CppType t1366_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t634_0_0_0;
static ParameterInfo t1364_m7505_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221537, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"match", 1, 134221538, &EmptyCustomAttributesCache, &t1366_0_0_0},
	{"returnType", 2, 134221539, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"indexes", 3, 134221540, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"modifiers", 4, 134221541, &EmptyCustomAttributesCache, &t634_0_0_0},
};
extern Il2CppType t1146_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7505_MI = 
{
	"SelectProperty", (methodPointerType)&m7505, &t1364_TI, &t1146_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29_t29, t1364_m7505_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 5, false, false, 3026, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern Il2CppType t638_0_0_0;
static ParameterInfo t1364_m7506_ParameterInfos[] = 
{
	{"types", 0, 134221542, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"args", 1, 134221543, &EmptyCustomAttributesCache, &t638_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7506_MI = 
{
	"check_arguments_with_score", (methodPointerType)&m7506, &t1364_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t1364_m7506_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 3027, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t42_0_0_0;
static ParameterInfo t1364_m7507_ParameterInfos[] = 
{
	{"from", 0, 134221544, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"to", 1, 134221545, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7507_MI = 
{
	"check_type_with_score", (methodPointerType)&m7507, &t1364_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t1364_m7507_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 3028, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1364_MIs[] =
{
	&m7493_MI,
	&m7494_MI,
	&m7495_MI,
	&m7496_MI,
	&m7497_MI,
	&m7498_MI,
	&m7499_MI,
	&m7500_MI,
	&m7501_MI,
	&m7502_MI,
	&m7503_MI,
	&m7504_MI,
	&m7505_MI,
	&m7506_MI,
	&m7507_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
static MethodInfo* t1364_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7494_MI,
	&m7497_MI,
	&m7498_MI,
	&m7501_MI,
	&m7505_MI,
};
extern TypeInfo t1168_TI;
#include "t1168.h"
#include "t1168MD.h"
extern MethodInfo m6082_MI;
void t1364_CustomAttributesCacheGenerator_m7498(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1168 * tmp;
		tmp = (t1168 *)il2cpp_codegen_object_new (&t1168_TI);
		m6082(tmp, il2cpp_codegen_string_new_wrapper("This method does not do anything in Mono"), &m6082_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1364__CustomAttributeCache_m7498 = {
1,
NULL,
&t1364_CustomAttributesCacheGenerator_m7498
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1364_0_0_0;
extern Il2CppType t1364_1_0_0;
struct t1364;
extern CustomAttributesCache t1364__CustomAttributeCache_m7498;
TypeInfo t1364_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Default", "", t1364_MIs, NULL, NULL, NULL, &t631_TI, NULL, &t631_TI, &t1364_TI, NULL, t1364_VT, &EmptyCustomAttributesCache, &t1364_TI, &t1364_0_0_0, &t1364_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1364), 0, -1, 0, 0, -1, 1048837, 0, false, false, false, false, false, false, false, false, false, false, false, false, 15, 0, 0, 0, 0, 9, 0, 0};
#include "t631.h"
#ifndef _MSC_VER
#else
#endif

#include "t1391.h"
extern TypeInfo t1391_TI;
extern TypeInfo t633_TI;
#include "t1391MD.h"
extern MethodInfo m1331_MI;
extern MethodInfo m7725_MI;
extern MethodInfo m10146_MI;
extern MethodInfo m2908_MI;


 void m7508 (t631 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7509_MI;
 void m7509 (t29 * __this, MethodInfo* method){
	{
		t1364 * L_0 = (t1364 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1364_TI));
		m7493(L_0, &m7493_MI);
		((t631_SFs*)InitializedTypeInfo(&t631_TI)->static_fields)->f0 = L_0;
		return;
	}
}
extern MethodInfo m7510_MI;
 t631 * m7510 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		return (((t631_SFs*)InitializedTypeInfo(&t631_TI)->static_fields)->f0);
	}
}
extern MethodInfo m7511_MI;
 bool m7511 (t29 * __this, t631 * p0, t316* p1, t638* p2, t633 * p3, MethodInfo* method){
	int32_t V_0 = 0;
	t29 * V_1 = {0};
	{
		if (p1)
		{
			goto IL_0010;
		}
	}
	{
		if ((((int32_t)(((t20 *)p2)->max_length))))
		{
			goto IL_000a;
		}
	}
	{
		return 1;
	}

IL_000a:
	{
		t1391 * L_0 = (t1391 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1391_TI));
		m7725(L_0, &m7725_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0010:
	{
		if ((((int32_t)(((int32_t)(((t20 *)p2)->max_length)))) == ((int32_t)(((int32_t)(((t20 *)p1)->max_length))))))
		{
			goto IL_001e;
		}
	}
	{
		t1391 * L_1 = (t1391 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1391_TI));
		m7725(L_1, &m7725_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001e:
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0022:
	{
		int32_t L_2 = V_0;
		int32_t L_3 = V_0;
		t42 * L_4 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(p2, L_3)));
		t29 * L_5 = (t29 *)VirtFuncInvoker3< t29 *, t29 *, t42 *, t633 * >::Invoke(&m10146_MI, p0, (*(t29 **)(t29 **)SZArrayLdElema(p1, L_2)), L_4, p3);
		V_1 = L_5;
		if (V_1)
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_6 = V_0;
		if (!(*(t29 **)(t29 **)SZArrayLdElema(p1, L_6)))
		{
			goto IL_003f;
		}
	}
	{
		return 0;
	}

IL_003f:
	{
		ArrayElementTypeCheck (p1, V_1);
		*((t29 **)(t29 **)SZArrayLdElema(p1, V_0)) = (t29 *)V_1;
		V_0 = ((int32_t)(V_0+1));
	}

IL_0047:
	{
		if ((((int32_t)V_0) < ((int32_t)(((int32_t)(((t20 *)p1)->max_length))))))
		{
			goto IL_0022;
		}
	}
	{
		return 1;
	}
}
 int32_t m7512 (t29 * __this, t42 * p0, MethodInfo* method){
	t42 * V_0 = {0};
	int32_t V_1 = 0;
	{
		V_0 = p0;
		V_1 = 1;
		goto IL_0011;
	}

IL_0006:
	{
		V_1 = ((int32_t)(V_1+1));
		t42 * L_0 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, V_0);
		V_0 = L_0;
	}

IL_0011:
	{
		t42 * L_1 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2908_MI, V_0);
		if (L_1)
		{
			goto IL_0006;
		}
	}
	{
		return V_1;
	}
}
extern MethodInfo m7513_MI;
 t636 * m7513 (t29 * __this, t1365* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	t636 * V_4 = {0};
	int32_t V_5 = 0;
	t638* V_6 = {0};
	t638* V_7 = {0};
	bool V_8 = false;
	int32_t V_9 = 0;
	{
		V_0 = 0;
		V_1 = (-1);
		V_2 = (((int32_t)(((t20 *)p0)->max_length)));
		V_3 = 0;
		goto IL_009c;
	}

IL_000f:
	{
		int32_t L_0 = V_3;
		V_4 = (*(t636 **)(t636 **)SZArrayLdElema(p0, L_0));
		t42 * L_1 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, V_4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		int32_t L_2 = m7512(NULL, L_1, &m7512_MI);
		V_5 = L_2;
		if ((((uint32_t)V_5) != ((uint32_t)V_0)))
		{
			goto IL_002d;
		}
	}
	{
		t1357 * L_3 = (t1357 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1357_TI));
		m7455(L_3, &m7455_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_002d:
	{
		if ((((int32_t)V_1) < ((int32_t)0)))
		{
			goto IL_008e;
		}
	}
	{
		t638* L_4 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, V_4);
		V_6 = L_4;
		int32_t L_5 = V_1;
		t638* L_6 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, (*(t636 **)(t636 **)SZArrayLdElema(p0, L_5)));
		V_7 = L_6;
		V_8 = 1;
		if ((((int32_t)(((int32_t)(((t20 *)V_6)->max_length)))) == ((int32_t)(((int32_t)(((t20 *)V_7)->max_length))))))
		{
			goto IL_0056;
		}
	}
	{
		V_8 = 0;
		goto IL_0084;
	}

IL_0056:
	{
		V_9 = 0;
		goto IL_007c;
	}

IL_005b:
	{
		int32_t L_7 = V_9;
		t42 * L_8 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_6, L_7)));
		int32_t L_9 = V_9;
		t42 * L_10 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_7, L_9)));
		if ((((t42 *)L_8) == ((t42 *)L_10)))
		{
			goto IL_0076;
		}
	}
	{
		V_8 = 0;
		goto IL_0084;
	}

IL_0076:
	{
		V_9 = ((int32_t)(V_9+1));
	}

IL_007c:
	{
		if ((((int32_t)V_9) < ((int32_t)(((int32_t)(((t20 *)V_6)->max_length))))))
		{
			goto IL_005b;
		}
	}

IL_0084:
	{
		if (V_8)
		{
			goto IL_008e;
		}
	}
	{
		t1357 * L_11 = (t1357 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1357_TI));
		m7455(L_11, &m7455_MI);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_008e:
	{
		if ((((int32_t)V_5) <= ((int32_t)V_0)))
		{
			goto IL_0098;
		}
	}
	{
		V_0 = V_5;
		V_1 = V_3;
	}

IL_0098:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_009c:
	{
		if ((((int32_t)V_3) < ((int32_t)V_2)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_12 = V_1;
		return (*(t636 **)(t636 **)SZArrayLdElema(p0, L_12));
	}
}
// Metadata Definition System.Reflection.Binder
extern Il2CppType t631_0_0_17;
FieldInfo t631_f0_FieldInfo = 
{
	"default_binder", &t631_0_0_17, &t631_TI, offsetof(t631_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t631_FIs[] =
{
	&t631_f0_FieldInfo,
	NULL
};
static PropertyInfo t631____DefaultBinder_PropertyInfo = 
{
	&t631_TI, "DefaultBinder", &m7510_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t631_PIs[] =
{
	&t631____DefaultBinder_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7508_MI = 
{
	".ctor", (methodPointerType)&m7508, &t631_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 3003, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7509_MI = 
{
	".cctor", (methodPointerType)&m7509, &t631_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3004, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
extern Il2CppType t1365_0_0_0;
extern Il2CppType t316_1_0_0;
extern Il2CppType t634_0_0_0;
extern Il2CppType t633_0_0_0;
extern Il2CppType t446_0_0_0;
extern Il2CppType t29_1_0_2;
static ParameterInfo t631_m10147_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221474, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"match", 1, 134221475, &EmptyCustomAttributesCache, &t1365_0_0_0},
	{"args", 2, 134221476, &EmptyCustomAttributesCache, &t316_1_0_0},
	{"modifiers", 3, 134221477, &EmptyCustomAttributesCache, &t634_0_0_0},
	{"culture", 4, 134221478, &EmptyCustomAttributesCache, &t633_0_0_0},
	{"names", 5, 134221479, &EmptyCustomAttributesCache, &t446_0_0_0},
	{"state", 6, 134221480, &EmptyCustomAttributesCache, &t29_1_0_2},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t1704_t29_t29_t29_t2022 (MethodInfo* method, void* obj, void** args);
MethodInfo m10147_MI = 
{
	"BindToMethod", NULL, &t631_TI, &t636_0_0_0, RuntimeInvoker_t29_t44_t29_t1704_t29_t29_t29_t2022, t631_m10147_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 7, false, false, 3005, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t631_m10146_ParameterInfos[] = 
{
	{"value", 0, 134221481, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"type", 1, 134221482, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"culture", 2, 134221483, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10146_MI = 
{
	"ChangeType", NULL, &t631_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t631_m10146_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 3, false, false, 3006, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_1_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t631_m10148_ParameterInfos[] = 
{
	{"args", 0, 134221484, &EmptyCustomAttributesCache, &t316_1_0_0},
	{"state", 1, 134221485, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t1704_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10148_MI = 
{
	"ReorderArgumentArray", NULL, &t631_TI, &t21_0_0_0, RuntimeInvoker_t21_t1704_t29, t631_m10148_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, false, 3007, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
extern Il2CppType t1365_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t634_0_0_0;
static ParameterInfo t631_m10141_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221486, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"match", 1, 134221487, &EmptyCustomAttributesCache, &t1365_0_0_0},
	{"types", 2, 134221488, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"modifiers", 3, 134221489, &EmptyCustomAttributesCache, &t634_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10141_MI = 
{
	"SelectMethod", NULL, &t631_TI, &t636_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29, t631_m10141_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 7, 4, false, false, 3008, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
extern Il2CppType t1366_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t537_0_0_0;
extern Il2CppType t634_0_0_0;
static ParameterInfo t631_m10149_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134221490, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"match", 1, 134221491, &EmptyCustomAttributesCache, &t1366_0_0_0},
	{"returnType", 2, 134221492, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"indexes", 3, 134221493, &EmptyCustomAttributesCache, &t537_0_0_0},
	{"modifiers", 4, 134221494, &EmptyCustomAttributesCache, &t634_0_0_0},
};
extern Il2CppType t1146_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10149_MI = 
{
	"SelectProperty", NULL, &t631_TI, &t1146_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29_t29, t631_m10149_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 5, false, false, 3009, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t631_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7510_MI = 
{
	"get_DefaultBinder", (methodPointerType)&m7510, &t631_TI, &t631_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2195, 0, 255, 0, false, false, 3010, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t631_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t638_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t631_m7511_ParameterInfos[] = 
{
	{"binder", 0, 134221495, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"args", 1, 134221496, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"pinfo", 2, 134221497, &EmptyCustomAttributesCache, &t638_0_0_0},
	{"culture", 3, 134221498, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7511_MI = 
{
	"ConvertArgs", (methodPointerType)&m7511, &t631_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29_t29_t29, t631_m7511_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 4, false, false, 3011, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t631_m7512_ParameterInfos[] = 
{
	{"type", 0, 134221499, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7512_MI = 
{
	"GetDerivedLevel", (methodPointerType)&m7512, &t631_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t631_m7512_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 3012, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1365_0_0_0;
static ParameterInfo t631_m7513_ParameterInfos[] = 
{
	{"match", 0, 134221500, &EmptyCustomAttributesCache, &t1365_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7513_MI = 
{
	"FindMostDerivedMatch", (methodPointerType)&m7513, &t631_TI, &t636_0_0_0, RuntimeInvoker_t29_t29, t631_m7513_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 3013, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t631_MIs[] =
{
	&m7508_MI,
	&m7509_MI,
	&m10147_MI,
	&m10146_MI,
	&m10148_MI,
	&m10141_MI,
	&m10149_MI,
	&m7510_MI,
	&m7511_MI,
	&m7512_MI,
	&m7513_MI,
	NULL
};
extern TypeInfo t1364_TI;
static TypeInfo* t631_TI__nestedTypes[2] =
{
	&t1364_TI,
	NULL
};
static MethodInfo* t631_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
extern TypeInfo t1402_TI;
#include "t1402.h"
#include "t1402MD.h"
extern MethodInfo m7733_MI;
void t631_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1402 * tmp;
		tmp = (t1402 *)il2cpp_codegen_object_new (&t1402_TI);
		m7733(tmp, 2, &m7733_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t631__CustomAttributeCache = {
2,
NULL,
&t631_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t631_1_0_0;
struct t631;
extern CustomAttributesCache t631__CustomAttributeCache;
TypeInfo t631_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Binder", "System.Reflection", t631_MIs, t631_PIs, t631_FIs, NULL, &t29_TI, t631_TI__nestedTypes, NULL, &t631_TI, NULL, t631_VT, &t631__CustomAttributeCache, &t631_TI, &t631_0_0_0, &t631_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t631), 0, -1, sizeof(t631_SFs), 0, -1, 1056897, 0, false, false, false, false, false, false, false, false, false, true, false, false, 11, 1, 1, 0, 1, 9, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t630_TI;
#include "t630MD.h"



// Metadata Definition System.Reflection.BindingFlags
extern Il2CppType t44_0_0_1542;
FieldInfo t630_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t630_TI, offsetof(t630, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f2_FieldInfo = 
{
	"Default", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f3_FieldInfo = 
{
	"IgnoreCase", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f4_FieldInfo = 
{
	"DeclaredOnly", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f5_FieldInfo = 
{
	"Instance", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f6_FieldInfo = 
{
	"Static", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f7_FieldInfo = 
{
	"Public", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f8_FieldInfo = 
{
	"NonPublic", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f9_FieldInfo = 
{
	"FlattenHierarchy", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f10_FieldInfo = 
{
	"InvokeMethod", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f11_FieldInfo = 
{
	"CreateInstance", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f12_FieldInfo = 
{
	"GetField", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f13_FieldInfo = 
{
	"SetField", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f14_FieldInfo = 
{
	"GetProperty", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f15_FieldInfo = 
{
	"SetProperty", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f16_FieldInfo = 
{
	"PutDispProperty", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f17_FieldInfo = 
{
	"PutRefDispProperty", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f18_FieldInfo = 
{
	"ExactBinding", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f19_FieldInfo = 
{
	"SuppressChangeType", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f20_FieldInfo = 
{
	"OptionalParamBinding", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t630_0_0_32854;
FieldInfo t630_f21_FieldInfo = 
{
	"IgnoreReturn", &t630_0_0_32854, &t630_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t630_FIs[] =
{
	&t630_f1_FieldInfo,
	&t630_f2_FieldInfo,
	&t630_f3_FieldInfo,
	&t630_f4_FieldInfo,
	&t630_f5_FieldInfo,
	&t630_f6_FieldInfo,
	&t630_f7_FieldInfo,
	&t630_f8_FieldInfo,
	&t630_f9_FieldInfo,
	&t630_f10_FieldInfo,
	&t630_f11_FieldInfo,
	&t630_f12_FieldInfo,
	&t630_f13_FieldInfo,
	&t630_f14_FieldInfo,
	&t630_f15_FieldInfo,
	&t630_f16_FieldInfo,
	&t630_f17_FieldInfo,
	&t630_f18_FieldInfo,
	&t630_f19_FieldInfo,
	&t630_f20_FieldInfo,
	&t630_f21_FieldInfo,
	NULL
};
static const int32_t t630_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t630_f2_DefaultValue = 
{
	&t630_f2_FieldInfo, { (char*)&t630_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t630_f3_DefaultValue = 
{
	&t630_f3_FieldInfo, { (char*)&t630_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t630_f4_DefaultValue = 
{
	&t630_f4_FieldInfo, { (char*)&t630_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f5_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t630_f5_DefaultValue = 
{
	&t630_f5_FieldInfo, { (char*)&t630_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f6_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t630_f6_DefaultValue = 
{
	&t630_f6_FieldInfo, { (char*)&t630_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f7_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t630_f7_DefaultValue = 
{
	&t630_f7_FieldInfo, { (char*)&t630_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f8_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t630_f8_DefaultValue = 
{
	&t630_f8_FieldInfo, { (char*)&t630_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f9_DefaultValueData = 64;
static Il2CppFieldDefaultValueEntry t630_f9_DefaultValue = 
{
	&t630_f9_FieldInfo, { (char*)&t630_f9_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f10_DefaultValueData = 256;
static Il2CppFieldDefaultValueEntry t630_f10_DefaultValue = 
{
	&t630_f10_FieldInfo, { (char*)&t630_f10_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f11_DefaultValueData = 512;
static Il2CppFieldDefaultValueEntry t630_f11_DefaultValue = 
{
	&t630_f11_FieldInfo, { (char*)&t630_f11_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f12_DefaultValueData = 1024;
static Il2CppFieldDefaultValueEntry t630_f12_DefaultValue = 
{
	&t630_f12_FieldInfo, { (char*)&t630_f12_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f13_DefaultValueData = 2048;
static Il2CppFieldDefaultValueEntry t630_f13_DefaultValue = 
{
	&t630_f13_FieldInfo, { (char*)&t630_f13_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f14_DefaultValueData = 4096;
static Il2CppFieldDefaultValueEntry t630_f14_DefaultValue = 
{
	&t630_f14_FieldInfo, { (char*)&t630_f14_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f15_DefaultValueData = 8192;
static Il2CppFieldDefaultValueEntry t630_f15_DefaultValue = 
{
	&t630_f15_FieldInfo, { (char*)&t630_f15_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f16_DefaultValueData = 16384;
static Il2CppFieldDefaultValueEntry t630_f16_DefaultValue = 
{
	&t630_f16_FieldInfo, { (char*)&t630_f16_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f17_DefaultValueData = 32768;
static Il2CppFieldDefaultValueEntry t630_f17_DefaultValue = 
{
	&t630_f17_FieldInfo, { (char*)&t630_f17_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f18_DefaultValueData = 65536;
static Il2CppFieldDefaultValueEntry t630_f18_DefaultValue = 
{
	&t630_f18_FieldInfo, { (char*)&t630_f18_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f19_DefaultValueData = 131072;
static Il2CppFieldDefaultValueEntry t630_f19_DefaultValue = 
{
	&t630_f19_FieldInfo, { (char*)&t630_f19_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f20_DefaultValueData = 262144;
static Il2CppFieldDefaultValueEntry t630_f20_DefaultValue = 
{
	&t630_f20_FieldInfo, { (char*)&t630_f20_DefaultValueData, &t44_0_0_0 }};
static const int32_t t630_f21_DefaultValueData = 16777216;
static Il2CppFieldDefaultValueEntry t630_f21_DefaultValue = 
{
	&t630_f21_FieldInfo, { (char*)&t630_f21_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t630_FDVs[] = 
{
	&t630_f2_DefaultValue,
	&t630_f3_DefaultValue,
	&t630_f4_DefaultValue,
	&t630_f5_DefaultValue,
	&t630_f6_DefaultValue,
	&t630_f7_DefaultValue,
	&t630_f8_DefaultValue,
	&t630_f9_DefaultValue,
	&t630_f10_DefaultValue,
	&t630_f11_DefaultValue,
	&t630_f12_DefaultValue,
	&t630_f13_DefaultValue,
	&t630_f14_DefaultValue,
	&t630_f15_DefaultValue,
	&t630_f16_DefaultValue,
	&t630_f17_DefaultValue,
	&t630_f18_DefaultValue,
	&t630_f19_DefaultValue,
	&t630_f20_DefaultValue,
	&t630_f21_DefaultValue,
	NULL
};
static MethodInfo* t630_MIs[] =
{
	NULL
};
static MethodInfo* t630_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t630_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t630_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t630__CustomAttributeCache = {
2,
NULL,
&t630_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t630_1_0_0;
extern CustomAttributesCache t630__CustomAttributeCache;
TypeInfo t630_TI = 
{
	&g_mscorlib_dll_Image, NULL, "BindingFlags", "System.Reflection", t630_MIs, NULL, t630_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t630_VT, &t630__CustomAttributeCache, &t44_TI, &t630_0_0_0, &t630_1_0_0, t630_IOs, NULL, NULL, t630_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t630)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 21, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif
#include "t1150MD.h"



// Metadata Definition System.Reflection.CallingConventions
extern Il2CppType t44_0_0_1542;
FieldInfo t1150_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1150_TI, offsetof(t1150, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1150_0_0_32854;
FieldInfo t1150_f2_FieldInfo = 
{
	"Standard", &t1150_0_0_32854, &t1150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1150_0_0_32854;
FieldInfo t1150_f3_FieldInfo = 
{
	"VarArgs", &t1150_0_0_32854, &t1150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1150_0_0_32854;
FieldInfo t1150_f4_FieldInfo = 
{
	"Any", &t1150_0_0_32854, &t1150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1150_0_0_32854;
FieldInfo t1150_f5_FieldInfo = 
{
	"HasThis", &t1150_0_0_32854, &t1150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1150_0_0_32854;
FieldInfo t1150_f6_FieldInfo = 
{
	"ExplicitThis", &t1150_0_0_32854, &t1150_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1150_FIs[] =
{
	&t1150_f1_FieldInfo,
	&t1150_f2_FieldInfo,
	&t1150_f3_FieldInfo,
	&t1150_f4_FieldInfo,
	&t1150_f5_FieldInfo,
	&t1150_f6_FieldInfo,
	NULL
};
static const int32_t t1150_f2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1150_f2_DefaultValue = 
{
	&t1150_f2_FieldInfo, { (char*)&t1150_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1150_f3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1150_f3_DefaultValue = 
{
	&t1150_f3_FieldInfo, { (char*)&t1150_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1150_f4_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1150_f4_DefaultValue = 
{
	&t1150_f4_FieldInfo, { (char*)&t1150_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1150_f5_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t1150_f5_DefaultValue = 
{
	&t1150_f5_FieldInfo, { (char*)&t1150_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1150_f6_DefaultValueData = 64;
static Il2CppFieldDefaultValueEntry t1150_f6_DefaultValue = 
{
	&t1150_f6_FieldInfo, { (char*)&t1150_f6_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1150_FDVs[] = 
{
	&t1150_f2_DefaultValue,
	&t1150_f3_DefaultValue,
	&t1150_f4_DefaultValue,
	&t1150_f5_DefaultValue,
	&t1150_f6_DefaultValue,
	NULL
};
static MethodInfo* t1150_MIs[] =
{
	NULL
};
static MethodInfo* t1150_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1150_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1150_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1150__CustomAttributeCache = {
2,
NULL,
&t1150_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1150_0_0_0;
extern Il2CppType t1150_1_0_0;
extern CustomAttributesCache t1150__CustomAttributeCache;
TypeInfo t1150_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CallingConventions", "System.Reflection", t1150_MIs, NULL, t1150_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1150_VT, &t1150__CustomAttributeCache, &t44_TI, &t1150_0_0_0, &t1150_1_0_0, t1150_IOs, NULL, NULL, t1150_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1150)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 6, 0, 0, 23, 0, 3};
#include "t660.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t660_TI;
#include "t660MD.h"

#include "t1149.h"
extern MethodInfo m7541_MI;
extern MethodInfo m10150_MI;


extern MethodInfo m7514_MI;
 void m7514 (t660 * __this, MethodInfo* method){
	{
		m7541(__this, &m7541_MI);
		return;
	}
}
extern MethodInfo m7515_MI;
 void m7515 (t29 * __this, MethodInfo* method){
	{
		((t660_SFs*)InitializedTypeInfo(&t660_TI)->static_fields)->f0 = (t7*) &_stringLiteral1528;
		((t660_SFs*)InitializedTypeInfo(&t660_TI)->static_fields)->f1 = (t7*) &_stringLiteral1529;
		return;
	}
}
extern MethodInfo m7516_MI;
 int32_t m7516 (t660 * __this, MethodInfo* method){
	{
		return (int32_t)(1);
	}
}
extern MethodInfo m2982_MI;
 t29 * m2982 (t660 * __this, t316* p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000d;
		}
	}
	{
		p0 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 0));
	}

IL_000d:
	{
		t29 * L_0 = (t29 *)VirtFuncInvoker4< t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m10150_MI, __this, ((int32_t)512), (t631 *)NULL, p0, (t633 *)NULL);
		return L_0;
	}
}
// Metadata Definition System.Reflection.ConstructorInfo
extern Il2CppType t7_0_0_54;
extern CustomAttributesCache t660__CustomAttributeCache_ConstructorName;
FieldInfo t660_f0_FieldInfo = 
{
	"ConstructorName", &t7_0_0_54, &t660_TI, offsetof(t660_SFs, f0), &t660__CustomAttributeCache_ConstructorName};
extern Il2CppType t7_0_0_54;
extern CustomAttributesCache t660__CustomAttributeCache_TypeConstructorName;
FieldInfo t660_f1_FieldInfo = 
{
	"TypeConstructorName", &t7_0_0_54, &t660_TI, offsetof(t660_SFs, f1), &t660__CustomAttributeCache_TypeConstructorName};
static FieldInfo* t660_FIs[] =
{
	&t660_f0_FieldInfo,
	&t660_f1_FieldInfo,
	NULL
};
extern CustomAttributesCache t660__CustomAttributeCache_t660____MemberType_PropertyInfo;
static PropertyInfo t660____MemberType_PropertyInfo = 
{
	&t660_TI, "MemberType", &m7516_MI, NULL, 0, &t660__CustomAttributeCache_t660____MemberType_PropertyInfo};
static PropertyInfo* t660_PIs[] =
{
	&t660____MemberType_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7514_MI = 
{
	".ctor", (methodPointerType)&m7514, &t660_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 3029, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7515_MI = 
{
	".cctor", (methodPointerType)&m7515, &t660_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3030, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1149_0_0_0;
extern void* RuntimeInvoker_t1149 (MethodInfo* method, void* obj, void** args);
MethodInfo m7516_MI = 
{
	"get_MemberType", (methodPointerType)&m7516, &t660_TI, &t1149_0_0_0, RuntimeInvoker_t1149, NULL, &EmptyCustomAttributesCache, 2246, 0, 7, 0, false, false, 3031, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
static ParameterInfo t660_m2982_ParameterInfos[] = 
{
	{"parameters", 0, 134221546, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t660__CustomAttributeCache_m2982;
MethodInfo m2982_MI = 
{
	"Invoke", (methodPointerType)&m2982, &t660_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t660_m2982_ParameterInfos, &t660__CustomAttributeCache_m2982, 134, 0, 255, 1, false, false, 3032, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t660_m10150_ParameterInfos[] = 
{
	{"invokeAttr", 0, 134221547, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 1, 134221548, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"parameters", 2, 134221549, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"culture", 3, 134221550, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10150_MI = 
{
	"Invoke", NULL, &t660_TI, &t29_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29, t660_m10150_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 27, 4, false, false, 3033, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t660_MIs[] =
{
	&m7514_MI,
	&m7515_MI,
	&m7516_MI,
	&m2982_MI,
	&m10150_MI,
	NULL
};
extern MethodInfo m2910_MI;
extern MethodInfo m9653_MI;
extern MethodInfo m6048_MI;
extern MethodInfo m7546_MI;
extern MethodInfo m7548_MI;
extern MethodInfo m2952_MI;
extern MethodInfo m7549_MI;
extern MethodInfo m7550_MI;
extern MethodInfo m7551_MI;
extern MethodInfo m7553_MI;
static MethodInfo* t660_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m2910_MI,
	&m9653_MI,
	NULL,
	&m7516_MI,
	NULL,
	NULL,
	&m6048_MI,
	NULL,
	NULL,
	NULL,
	NULL,
	&m7546_MI,
	NULL,
	NULL,
	NULL,
	&m7547_MI,
	&m7548_MI,
	&m2952_MI,
	&m7549_MI,
	&m7550_MI,
	&m7551_MI,
	&m7552_MI,
	&m7553_MI,
	NULL,
};
extern TypeInfo t2009_TI;
static TypeInfo* t660_ITIs[] = 
{
	&t2009_TI,
};
extern TypeInfo t2010_TI;
extern TypeInfo t1657_TI;
extern TypeInfo t1915_TI;
static Il2CppInterfaceOffsetPair t660_IOs[] = 
{
	{ &t2010_TI, 14},
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
	{ &t2009_TI, 27},
};
extern TypeInfo t1404_TI;
#include "t1404.h"
#include "t1404MD.h"
extern MethodInfo m7734_MI;
void t660_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1404 * tmp;
		tmp = (t1404 *)il2cpp_codegen_object_new (&t1404_TI);
		m7734(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2009_TI)), &m7734_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1402 * tmp;
		tmp = (t1402 *)il2cpp_codegen_object_new (&t1402_TI);
		m7733(tmp, 0, &m7733_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
void t660_CustomAttributesCacheGenerator_ConstructorName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t660_CustomAttributesCacheGenerator_TypeConstructorName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t1288_TI;
#include "t1288.h"
#include "t1288MD.h"
extern MethodInfo m6773_MI;
extern TypeInfo t342_TI;
#include "t342.h"
#include "t342MD.h"
extern MethodInfo m1507_MI;
void t660_CustomAttributesCacheGenerator_m2982(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1288 * tmp;
		tmp = (t1288 *)il2cpp_codegen_object_new (&t1288_TI);
		m6773(tmp, &m6773_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t342 * tmp;
		tmp = (t342 *)il2cpp_codegen_object_new (&t342_TI);
		m1507(tmp, &m1507_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t660_CustomAttributesCacheGenerator_t660____MemberType_PropertyInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t660__CustomAttributeCache = {
3,
NULL,
&t660_CustomAttributesCacheGenerator
};
CustomAttributesCache t660__CustomAttributeCache_ConstructorName = {
1,
NULL,
&t660_CustomAttributesCacheGenerator_ConstructorName
};
CustomAttributesCache t660__CustomAttributeCache_TypeConstructorName = {
1,
NULL,
&t660_CustomAttributesCacheGenerator_TypeConstructorName
};
CustomAttributesCache t660__CustomAttributeCache_m2982 = {
2,
NULL,
&t660_CustomAttributesCacheGenerator_m2982
};
CustomAttributesCache t660__CustomAttributeCache_t660____MemberType_PropertyInfo = {
1,
NULL,
&t660_CustomAttributesCacheGenerator_t660____MemberType_PropertyInfo
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t660_0_0_0;
extern Il2CppType t660_1_0_0;
struct t660;
extern CustomAttributesCache t660__CustomAttributeCache;
extern CustomAttributesCache t660__CustomAttributeCache_ConstructorName;
extern CustomAttributesCache t660__CustomAttributeCache_TypeConstructorName;
extern CustomAttributesCache t660__CustomAttributeCache_m2982;
extern CustomAttributesCache t660__CustomAttributeCache_t660____MemberType_PropertyInfo;
TypeInfo t660_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ConstructorInfo", "System.Reflection", t660_MIs, t660_PIs, t660_FIs, NULL, &t636_TI, NULL, NULL, &t660_TI, t660_ITIs, t660_VT, &t660__CustomAttributeCache, &t660_TI, &t660_0_0_0, &t660_1_0_0, t660_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t660), 0, -1, sizeof(t660_SFs), 0, -1, 1056897, 0, false, false, false, false, false, false, false, false, false, true, false, false, 5, 1, 2, 0, 0, 28, 1, 4};
#include "t1367.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1367_TI;
#include "t1367MD.h"



// Metadata Definition System.Reflection.EventAttributes
extern Il2CppType t44_0_0_1542;
FieldInfo t1367_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1367_TI, offsetof(t1367, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1367_0_0_32854;
FieldInfo t1367_f2_FieldInfo = 
{
	"None", &t1367_0_0_32854, &t1367_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1367_0_0_32854;
FieldInfo t1367_f3_FieldInfo = 
{
	"SpecialName", &t1367_0_0_32854, &t1367_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1367_0_0_32854;
FieldInfo t1367_f4_FieldInfo = 
{
	"ReservedMask", &t1367_0_0_32854, &t1367_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1367_0_0_32854;
FieldInfo t1367_f5_FieldInfo = 
{
	"RTSpecialName", &t1367_0_0_32854, &t1367_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1367_FIs[] =
{
	&t1367_f1_FieldInfo,
	&t1367_f2_FieldInfo,
	&t1367_f3_FieldInfo,
	&t1367_f4_FieldInfo,
	&t1367_f5_FieldInfo,
	NULL
};
static const int32_t t1367_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1367_f2_DefaultValue = 
{
	&t1367_f2_FieldInfo, { (char*)&t1367_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1367_f3_DefaultValueData = 512;
static Il2CppFieldDefaultValueEntry t1367_f3_DefaultValue = 
{
	&t1367_f3_FieldInfo, { (char*)&t1367_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1367_f4_DefaultValueData = 1024;
static Il2CppFieldDefaultValueEntry t1367_f4_DefaultValue = 
{
	&t1367_f4_FieldInfo, { (char*)&t1367_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1367_f5_DefaultValueData = 1024;
static Il2CppFieldDefaultValueEntry t1367_f5_DefaultValue = 
{
	&t1367_f5_FieldInfo, { (char*)&t1367_f5_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1367_FDVs[] = 
{
	&t1367_f2_DefaultValue,
	&t1367_f3_DefaultValue,
	&t1367_f4_DefaultValue,
	&t1367_f5_DefaultValue,
	NULL
};
static MethodInfo* t1367_MIs[] =
{
	NULL
};
static MethodInfo* t1367_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1367_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1367_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1367__CustomAttributeCache = {
2,
NULL,
&t1367_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1367_0_0_0;
extern Il2CppType t1367_1_0_0;
extern CustomAttributesCache t1367__CustomAttributeCache;
TypeInfo t1367_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EventAttributes", "System.Reflection", t1367_MIs, NULL, t1367_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1367_VT, &t1367__CustomAttributeCache, &t44_TI, &t1367_0_0_0, &t1367_1_0_0, t1367_IOs, NULL, NULL, t1367_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1367)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 23, 0, 3};
#include "t1368.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1368_TI;
#include "t1368MD.h"

#include "t353.h"
#include "t67.h"


extern MethodInfo m7517_MI;
 void m7517 (t1368 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m7518_MI;
 void m7518 (t1368 * __this, t29 * p0, t353 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m7518((t1368 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, t353 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p0, t353 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef void (*FunctionPointerType) (t29 * __this, t353 * p1, MethodInfo* method);
		((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
void pinvoke_delegate_wrapper_t1368(Il2CppObject* delegate, t29 * p0, t353 * p1)
{
	typedef void (STDCALL *native_function_ptr_type)(t29 *, t353 *);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t29 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	// Marshaling of parameter 'p1' to native representation
	t353 * _p1_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Delegate'."));

	// Native function invocation
	_il2cpp_pinvoke_func(_p0_marshaled, _p1_marshaled);

	// Marshaling cleanup of parameter 'p0' native representation

	// Marshaling cleanup of parameter 'p1' native representation

}
extern MethodInfo m7519_MI;
 t29 * m7519 (t1368 * __this, t29 * p0, t353 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m7520_MI;
 void m7520 (t1368 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition System.Reflection.EventInfo/AddEventAdapter
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1368_m7517_ParameterInfos[] = 
{
	{"object", 0, 134221552, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134221553, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m7517_MI = 
{
	".ctor", (methodPointerType)&m7517, &t1368_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1368_m7517_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 3039, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t353_0_0_0;
extern Il2CppType t353_0_0_0;
static ParameterInfo t1368_m7518_ParameterInfos[] = 
{
	{"_this", 0, 134221554, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"dele", 1, 134221555, &EmptyCustomAttributesCache, &t353_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7518_MI = 
{
	"Invoke", (methodPointerType)&m7518, &t1368_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1368_m7518_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, false, 3040, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t353_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1368_m7519_ParameterInfos[] = 
{
	{"_this", 0, 134221556, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"dele", 1, 134221557, &EmptyCustomAttributesCache, &t353_0_0_0},
	{"callback", 2, 134221558, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134221559, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7519_MI = 
{
	"BeginInvoke", (methodPointerType)&m7519, &t1368_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t1368_m7519_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, false, 3041, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t1368_m7520_ParameterInfos[] = 
{
	{"result", 0, 134221560, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7520_MI = 
{
	"EndInvoke", (methodPointerType)&m7520, &t1368_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1368_m7520_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 3042, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1368_MIs[] =
{
	&m7517_MI,
	&m7518_MI,
	&m7519_MI,
	&m7520_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
static MethodInfo* t1368_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m7518_MI,
	&m7519_MI,
	&m7520_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t1368_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1368_0_0_0;
extern Il2CppType t1368_1_0_0;
extern TypeInfo t195_TI;
struct t1368;
extern TypeInfo t1143_TI;
TypeInfo t1368_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AddEventAdapter", "", t1368_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1143_TI, &t1368_TI, NULL, t1368_VT, &EmptyCustomAttributesCache, &t1368_TI, &t1368_0_0_0, &t1368_1_0_0, t1368_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1368, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1368), 0, sizeof(methodPointerType), 0, 0, -1, 259, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1143.h"
#ifndef _MSC_VER
#else
#endif
#include "t1143MD.h"

#include "t557.h"
extern TypeInfo t557_TI;
extern MethodInfo m10151_MI;
extern MethodInfo m6047_MI;


extern MethodInfo m7521_MI;
 void m7521 (t1143 * __this, MethodInfo* method){
	{
		m6047(__this, &m6047_MI);
		return;
	}
}
extern MethodInfo m7522_MI;
 t42 * m7522 (t1143 * __this, MethodInfo* method){
	t638* V_0 = {0};
	t557 * V_1 = {0};
	t42 * V_2 = {0};
	{
		t557 * L_0 = (t557 *)VirtFuncInvoker1< t557 *, bool >::Invoke(&m10151_MI, __this, 1);
		V_1 = L_0;
		t638* L_1 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, V_1);
		V_0 = L_1;
		if ((((int32_t)(((int32_t)(((t20 *)V_0)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = 0;
		t42 * L_3 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_0, L_2)));
		V_2 = L_3;
		return V_2;
	}

IL_0020:
	{
		return (t42 *)NULL;
	}
}
extern MethodInfo m7523_MI;
 int32_t m7523 (t1143 * __this, MethodInfo* method){
	{
		return (int32_t)(2);
	}
}
// Metadata Definition System.Reflection.EventInfo
extern Il2CppType t1368_0_0_1;
FieldInfo t1143_f0_FieldInfo = 
{
	"cached_add_event", &t1368_0_0_1, &t1143_TI, offsetof(t1143, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1143_FIs[] =
{
	&t1143_f0_FieldInfo,
	NULL
};
extern MethodInfo m9845_MI;
static PropertyInfo t1143____Attributes_PropertyInfo = 
{
	&t1143_TI, "Attributes", &m9845_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1143____EventHandlerType_PropertyInfo = 
{
	&t1143_TI, "EventHandlerType", &m7522_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1143____MemberType_PropertyInfo = 
{
	&t1143_TI, "MemberType", &m7523_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1143_PIs[] =
{
	&t1143____Attributes_PropertyInfo,
	&t1143____EventHandlerType_PropertyInfo,
	&t1143____MemberType_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7521_MI = 
{
	".ctor", (methodPointerType)&m7521, &t1143_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 3034, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1367_0_0_0;
extern void* RuntimeInvoker_t1367 (MethodInfo* method, void* obj, void** args);
MethodInfo m9845_MI = 
{
	"get_Attributes", NULL, &t1143_TI, &t1367_0_0_0, RuntimeInvoker_t1367, NULL, &EmptyCustomAttributesCache, 3526, 0, 14, 0, false, false, 3035, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7522_MI = 
{
	"get_EventHandlerType", (methodPointerType)&m7522, &t1143_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 15, 0, false, false, 3036, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1149_0_0_0;
extern void* RuntimeInvoker_t1149 (MethodInfo* method, void* obj, void** args);
MethodInfo m7523_MI = 
{
	"get_MemberType", (methodPointerType)&m7523, &t1143_TI, &t1149_0_0_0, RuntimeInvoker_t1149, NULL, &EmptyCustomAttributesCache, 2246, 0, 7, 0, false, false, 3037, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1143_m10151_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221551, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m10151_MI = 
{
	"GetAddMethod", NULL, &t1143_TI, &t557_0_0_0, RuntimeInvoker_t29_t297, t1143_m10151_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 16, 1, false, false, 3038, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1143_MIs[] =
{
	&m7521_MI,
	&m9845_MI,
	&m7522_MI,
	&m7523_MI,
	&m10151_MI,
	NULL
};
extern TypeInfo t1368_TI;
static TypeInfo* t1143_TI__nestedTypes[2] =
{
	&t1368_TI,
	NULL
};
static MethodInfo* t1143_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m2910_MI,
	&m9653_MI,
	NULL,
	&m7523_MI,
	NULL,
	NULL,
	&m6048_MI,
	NULL,
	NULL,
	NULL,
	NULL,
	&m7522_MI,
	NULL,
};
extern TypeInfo t2023_TI;
static TypeInfo* t1143_ITIs[] = 
{
	&t2023_TI,
};
static Il2CppInterfaceOffsetPair t1143_IOs[] = 
{
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
	{ &t2023_TI, 14},
};
void t1143_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1404 * tmp;
		tmp = (t1404 *)il2cpp_codegen_object_new (&t1404_TI);
		m7734(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2023_TI)), &m7734_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1402 * tmp;
		tmp = (t1402 *)il2cpp_codegen_object_new (&t1402_TI);
		m7733(tmp, 0, &m7733_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1143__CustomAttributeCache = {
3,
NULL,
&t1143_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1143_0_0_0;
extern Il2CppType t1143_1_0_0;
struct t1143;
extern CustomAttributesCache t1143__CustomAttributeCache;
TypeInfo t1143_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EventInfo", "System.Reflection", t1143_MIs, t1143_PIs, t1143_FIs, NULL, &t296_TI, t1143_TI__nestedTypes, NULL, &t1143_TI, t1143_ITIs, t1143_VT, &t1143__CustomAttributeCache, &t1143_TI, &t1143_0_0_0, &t1143_1_0_0, t1143_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1143), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 3, 1, 0, 1, 17, 1, 3};
#include "t1347.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1347_TI;
#include "t1347MD.h"



// Metadata Definition System.Reflection.FieldAttributes
extern Il2CppType t44_0_0_1542;
FieldInfo t1347_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1347_TI, offsetof(t1347, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f2_FieldInfo = 
{
	"FieldAccessMask", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f3_FieldInfo = 
{
	"PrivateScope", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f4_FieldInfo = 
{
	"Private", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f5_FieldInfo = 
{
	"FamANDAssem", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f6_FieldInfo = 
{
	"Assembly", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f7_FieldInfo = 
{
	"Family", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f8_FieldInfo = 
{
	"FamORAssem", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f9_FieldInfo = 
{
	"Public", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f10_FieldInfo = 
{
	"Static", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f11_FieldInfo = 
{
	"InitOnly", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f12_FieldInfo = 
{
	"Literal", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f13_FieldInfo = 
{
	"NotSerialized", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f14_FieldInfo = 
{
	"HasFieldRVA", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f15_FieldInfo = 
{
	"SpecialName", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f16_FieldInfo = 
{
	"RTSpecialName", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f17_FieldInfo = 
{
	"HasFieldMarshal", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f18_FieldInfo = 
{
	"PinvokeImpl", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f19_FieldInfo = 
{
	"HasDefault", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_32854;
FieldInfo t1347_f20_FieldInfo = 
{
	"ReservedMask", &t1347_0_0_32854, &t1347_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1347_FIs[] =
{
	&t1347_f1_FieldInfo,
	&t1347_f2_FieldInfo,
	&t1347_f3_FieldInfo,
	&t1347_f4_FieldInfo,
	&t1347_f5_FieldInfo,
	&t1347_f6_FieldInfo,
	&t1347_f7_FieldInfo,
	&t1347_f8_FieldInfo,
	&t1347_f9_FieldInfo,
	&t1347_f10_FieldInfo,
	&t1347_f11_FieldInfo,
	&t1347_f12_FieldInfo,
	&t1347_f13_FieldInfo,
	&t1347_f14_FieldInfo,
	&t1347_f15_FieldInfo,
	&t1347_f16_FieldInfo,
	&t1347_f17_FieldInfo,
	&t1347_f18_FieldInfo,
	&t1347_f19_FieldInfo,
	&t1347_f20_FieldInfo,
	NULL
};
static const int32_t t1347_f2_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry t1347_f2_DefaultValue = 
{
	&t1347_f2_FieldInfo, { (char*)&t1347_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f3_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1347_f3_DefaultValue = 
{
	&t1347_f3_FieldInfo, { (char*)&t1347_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f4_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1347_f4_DefaultValue = 
{
	&t1347_f4_FieldInfo, { (char*)&t1347_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f5_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1347_f5_DefaultValue = 
{
	&t1347_f5_FieldInfo, { (char*)&t1347_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f6_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1347_f6_DefaultValue = 
{
	&t1347_f6_FieldInfo, { (char*)&t1347_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f7_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1347_f7_DefaultValue = 
{
	&t1347_f7_FieldInfo, { (char*)&t1347_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f8_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t1347_f8_DefaultValue = 
{
	&t1347_f8_FieldInfo, { (char*)&t1347_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f9_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t1347_f9_DefaultValue = 
{
	&t1347_f9_FieldInfo, { (char*)&t1347_f9_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f10_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t1347_f10_DefaultValue = 
{
	&t1347_f10_FieldInfo, { (char*)&t1347_f10_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f11_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t1347_f11_DefaultValue = 
{
	&t1347_f11_FieldInfo, { (char*)&t1347_f11_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f12_DefaultValueData = 64;
static Il2CppFieldDefaultValueEntry t1347_f12_DefaultValue = 
{
	&t1347_f12_FieldInfo, { (char*)&t1347_f12_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f13_DefaultValueData = 128;
static Il2CppFieldDefaultValueEntry t1347_f13_DefaultValue = 
{
	&t1347_f13_FieldInfo, { (char*)&t1347_f13_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f14_DefaultValueData = 256;
static Il2CppFieldDefaultValueEntry t1347_f14_DefaultValue = 
{
	&t1347_f14_FieldInfo, { (char*)&t1347_f14_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f15_DefaultValueData = 512;
static Il2CppFieldDefaultValueEntry t1347_f15_DefaultValue = 
{
	&t1347_f15_FieldInfo, { (char*)&t1347_f15_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f16_DefaultValueData = 1024;
static Il2CppFieldDefaultValueEntry t1347_f16_DefaultValue = 
{
	&t1347_f16_FieldInfo, { (char*)&t1347_f16_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f17_DefaultValueData = 4096;
static Il2CppFieldDefaultValueEntry t1347_f17_DefaultValue = 
{
	&t1347_f17_FieldInfo, { (char*)&t1347_f17_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f18_DefaultValueData = 8192;
static Il2CppFieldDefaultValueEntry t1347_f18_DefaultValue = 
{
	&t1347_f18_FieldInfo, { (char*)&t1347_f18_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f19_DefaultValueData = 32768;
static Il2CppFieldDefaultValueEntry t1347_f19_DefaultValue = 
{
	&t1347_f19_FieldInfo, { (char*)&t1347_f19_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1347_f20_DefaultValueData = 38144;
static Il2CppFieldDefaultValueEntry t1347_f20_DefaultValue = 
{
	&t1347_f20_FieldInfo, { (char*)&t1347_f20_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1347_FDVs[] = 
{
	&t1347_f2_DefaultValue,
	&t1347_f3_DefaultValue,
	&t1347_f4_DefaultValue,
	&t1347_f5_DefaultValue,
	&t1347_f6_DefaultValue,
	&t1347_f7_DefaultValue,
	&t1347_f8_DefaultValue,
	&t1347_f9_DefaultValue,
	&t1347_f10_DefaultValue,
	&t1347_f11_DefaultValue,
	&t1347_f12_DefaultValue,
	&t1347_f13_DefaultValue,
	&t1347_f14_DefaultValue,
	&t1347_f15_DefaultValue,
	&t1347_f16_DefaultValue,
	&t1347_f17_DefaultValue,
	&t1347_f18_DefaultValue,
	&t1347_f19_DefaultValue,
	&t1347_f20_DefaultValue,
	NULL
};
static MethodInfo* t1347_MIs[] =
{
	NULL
};
static MethodInfo* t1347_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1347_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1347_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1347__CustomAttributeCache = {
2,
NULL,
&t1347_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1347_0_0_0;
extern Il2CppType t1347_1_0_0;
extern CustomAttributesCache t1347__CustomAttributeCache;
TypeInfo t1347_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FieldAttributes", "System.Reflection", t1347_MIs, NULL, t1347_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1347_VT, &t1347__CustomAttributeCache, &t44_TI, &t1347_0_0_0, &t1347_1_0_0, t1347_IOs, NULL, NULL, t1347_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1347)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 20, 0, 0, 23, 0, 3};
#include "t1144.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1144_TI;
#include "t1144MD.h"

#include "t927.h"
#include "t1346.h"
#include "t956.h"
#include "t1662.h"
#include "t1161.h"
#include "t1155.h"
extern TypeInfo t21_TI;
extern TypeInfo t956_TI;
extern TypeInfo t1346_TI;
extern TypeInfo t1662_TI;
extern TypeInfo t1161_TI;
#include "t927MD.h"
#include "t35MD.h"
#include "t956MD.h"
#include "t1662MD.h"
#include "t1161MD.h"
#include "t1346MD.h"
extern MethodInfo m9843_MI;
extern MethodInfo m7533_MI;
extern MethodInfo m10152_MI;
extern MethodInfo m6052_MI;
extern MethodInfo m2949_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m7530_MI;
extern MethodInfo m4202_MI;
extern MethodInfo m7528_MI;
extern MethodInfo m5998_MI;
extern MethodInfo m7534_MI;
extern MethodInfo m9377_MI;
extern MethodInfo m7532_MI;
extern MethodInfo m6070_MI;
extern MethodInfo m7454_MI;


extern MethodInfo m7524_MI;
 void m7524 (t1144 * __this, MethodInfo* method){
	{
		m6047(__this, &m6047_MI);
		return;
	}
}
extern MethodInfo m7525_MI;
 int32_t m7525 (t1144 * __this, MethodInfo* method){
	{
		return (int32_t)(4);
	}
}
extern MethodInfo m7526_MI;
 bool m7526 (t1144 * __this, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m9843_MI, __this);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)64)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m7527_MI;
 bool m7527 (t1144 * __this, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m9843_MI, __this);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 bool m7528 (t1144 * __this, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m9843_MI, __this);
		return ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)128)))) == ((int32_t)((int32_t)128)))? 1 : 0);
	}
}
extern MethodInfo m7529_MI;
 void m7529 (t1144 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		VirtActionInvoker5< t29 *, t29 *, int32_t, t631 *, t633 * >::Invoke(&m10152_MI, __this, p0, p1, 0, (t631 *)NULL, (t633 *)NULL);
		return;
	}
}
 t1144 * m7530 (t29 * __this, t35 p0, t35 p1, MethodInfo* method){
	typedef t1144 * (*m7530_ftn) (t35, t35);
	static m7530_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7530_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.FieldInfo::internal_from_handle_type(System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(p0, p1);
}
extern MethodInfo m7531_MI;
 t1144 * m7531 (t29 * __this, t927  p0, MethodInfo* method){
	{
		t35 L_0 = m6052((&p0), &m6052_MI);
		bool L_1 = m2949(NULL, L_0, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), &m2949_MI);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		t305 * L_2 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_2, (t7*) &_stringLiteral1530, &m1935_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_001e:
	{
		t35 L_3 = m6052((&p0), &m6052_MI);
		t1144 * L_4 = m7530(NULL, L_3, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), &m7530_MI);
		return L_4;
	}
}
 int32_t m7532 (t1144 * __this, MethodInfo* method){
	{
		t956 * L_0 = (t956 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t956_TI));
		m4202(L_0, (t7*) &_stringLiteral1531, &m4202_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
 t1346 * m7533 (t1144 * __this, MethodInfo* method){
	typedef t1346 * (*m7533_ftn) (t1144 *);
	static m7533_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7533_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.FieldInfo::GetUnmanagedMarshal()");
	return _il2cpp_icall_func(__this);
}
 t1346 * m7534 (t1144 * __this, MethodInfo* method){
	{
		t1346 * L_0 = m7533(__this, &m7533_MI);
		return L_0;
	}
}
extern MethodInfo m7535_MI;
 t316* m7535 (t1144 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t1346 * V_1 = {0};
	t316* V_2 = {0};
	{
		V_0 = 0;
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7528_MI, __this);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_000e:
	{
		t42 * L_1 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, __this);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5998_MI, L_1);
		if (!L_2)
		{
			goto IL_001f;
		}
	}
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_001f:
	{
		t1346 * L_3 = (t1346 *)VirtFuncInvoker0< t1346 * >::Invoke(&m7534_MI, __this);
		V_1 = L_3;
		if (!V_1)
		{
			goto IL_002d;
		}
	}
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_002d:
	{
		if (V_0)
		{
			goto IL_0032;
		}
	}
	{
		return (t316*)NULL;
	}

IL_0032:
	{
		V_2 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), V_0));
		V_0 = 0;
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7528_MI, __this);
		if (!L_4)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)(L_5+1));
		t1662 * L_6 = (t1662 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1662_TI));
		m9377(L_6, &m9377_MI);
		ArrayElementTypeCheck (V_2, L_6);
		*((t29 **)(t29 **)SZArrayLdElema(V_2, L_5)) = (t29 *)L_6;
	}

IL_004f:
	{
		t42 * L_7 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, __this);
		bool L_8 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5998_MI, L_7);
		if (!L_8)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)(L_9+1));
		int32_t L_10 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7532_MI, __this);
		t1161 * L_11 = (t1161 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1161_TI));
		m6070(L_11, L_10, &m6070_MI);
		ArrayElementTypeCheck (V_2, L_11);
		*((t29 **)(t29 **)SZArrayLdElema(V_2, L_9)) = (t29 *)L_11;
	}

IL_006e:
	{
		if (!V_1)
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)(L_12+1));
		t1155 * L_13 = m7454(V_1, &m7454_MI);
		ArrayElementTypeCheck (V_2, L_13);
		*((t29 **)(t29 **)SZArrayLdElema(V_2, L_12)) = (t29 *)L_13;
	}

IL_007e:
	{
		return V_2;
	}
}
// Metadata Definition System.Reflection.FieldInfo
static PropertyInfo t1144____Attributes_PropertyInfo = 
{
	&t1144_TI, "Attributes", &m9843_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10153_MI;
static PropertyInfo t1144____FieldHandle_PropertyInfo = 
{
	&t1144_TI, "FieldHandle", &m10153_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10154_MI;
static PropertyInfo t1144____FieldType_PropertyInfo = 
{
	&t1144_TI, "FieldType", &m10154_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1144____MemberType_PropertyInfo = 
{
	&t1144_TI, "MemberType", &m7525_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1144____IsLiteral_PropertyInfo = 
{
	&t1144_TI, "IsLiteral", &m7526_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1144____IsStatic_PropertyInfo = 
{
	&t1144_TI, "IsStatic", &m7527_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1144____IsNotSerialized_PropertyInfo = 
{
	&t1144_TI, "IsNotSerialized", &m7528_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1144____UMarshal_PropertyInfo = 
{
	&t1144_TI, "UMarshal", &m7534_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1144_PIs[] =
{
	&t1144____Attributes_PropertyInfo,
	&t1144____FieldHandle_PropertyInfo,
	&t1144____FieldType_PropertyInfo,
	&t1144____MemberType_PropertyInfo,
	&t1144____IsLiteral_PropertyInfo,
	&t1144____IsStatic_PropertyInfo,
	&t1144____IsNotSerialized_PropertyInfo,
	&t1144____UMarshal_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7524_MI = 
{
	".ctor", (methodPointerType)&m7524, &t1144_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 3043, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1347_0_0_0;
extern void* RuntimeInvoker_t1347 (MethodInfo* method, void* obj, void** args);
MethodInfo m9843_MI = 
{
	"get_Attributes", NULL, &t1144_TI, &t1347_0_0_0, RuntimeInvoker_t1347, NULL, &EmptyCustomAttributesCache, 3526, 0, 14, 0, false, false, 3044, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t927_0_0_0;
extern void* RuntimeInvoker_t927 (MethodInfo* method, void* obj, void** args);
MethodInfo m10153_MI = 
{
	"get_FieldHandle", NULL, &t1144_TI, &t927_0_0_0, RuntimeInvoker_t927, NULL, &EmptyCustomAttributesCache, 3526, 0, 15, 0, false, false, 3045, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10154_MI = 
{
	"get_FieldType", NULL, &t1144_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 16, 0, false, false, 3046, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1144_m10155_ParameterInfos[] = 
{
	{"obj", 0, 134221561, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10155_MI = 
{
	"GetValue", NULL, &t1144_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1144_m10155_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 17, 1, false, false, 3047, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1149_0_0_0;
extern void* RuntimeInvoker_t1149 (MethodInfo* method, void* obj, void** args);
MethodInfo m7525_MI = 
{
	"get_MemberType", (methodPointerType)&m7525, &t1144_TI, &t1149_0_0_0, RuntimeInvoker_t1149, NULL, &EmptyCustomAttributesCache, 2246, 0, 7, 0, false, false, 3048, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7526_MI = 
{
	"get_IsLiteral", (methodPointerType)&m7526, &t1144_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 18, 0, false, false, 3049, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7527_MI = 
{
	"get_IsStatic", (methodPointerType)&m7527, &t1144_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 19, 0, false, false, 3050, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7528_MI = 
{
	"get_IsNotSerialized", (methodPointerType)&m7528, &t1144_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, false, 3051, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t1144_m10152_ParameterInfos[] = 
{
	{"obj", 0, 134221562, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134221563, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"invokeAttr", 2, 134221564, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 3, 134221565, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"culture", 4, 134221566, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10152_MI = 
{
	"SetValue", NULL, &t1144_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t44_t29_t29, t1144_m10152_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 21, 5, false, false, 3052, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1144_m7529_ParameterInfos[] = 
{
	{"obj", 0, 134221567, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134221568, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1144__CustomAttributeCache_m7529;
MethodInfo m7529_MI = 
{
	"SetValue", (methodPointerType)&m7529, &t1144_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1144_m7529_ParameterInfos, &t1144__CustomAttributeCache_m7529, 486, 0, 22, 2, false, false, 3053, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1144_m7530_ParameterInfos[] = 
{
	{"field_handle", 0, 134221569, &EmptyCustomAttributesCache, &t35_0_0_0},
	{"type_handle", 1, 134221570, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t1144_0_0_0;
extern void* RuntimeInvoker_t29_t35_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m7530_MI = 
{
	"internal_from_handle_type", (methodPointerType)&m7530, &t1144_TI, &t1144_0_0_0, RuntimeInvoker_t29_t35_t35, t1144_m7530_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 2, false, false, 3054, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t927_0_0_0;
extern Il2CppType t927_0_0_0;
static ParameterInfo t1144_m7531_ParameterInfos[] = 
{
	{"handle", 0, 134221571, &EmptyCustomAttributesCache, &t927_0_0_0},
};
extern Il2CppType t1144_0_0_0;
extern void* RuntimeInvoker_t29_t927 (MethodInfo* method, void* obj, void** args);
MethodInfo m7531_MI = 
{
	"GetFieldFromHandle", (methodPointerType)&m7531, &t1144_TI, &t1144_0_0_0, RuntimeInvoker_t29_t927, t1144_m7531_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 3055, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7532_MI = 
{
	"GetFieldOffset", (methodPointerType)&m7532, &t1144_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 451, 0, 23, 0, false, false, 3056, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1346_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7533_MI = 
{
	"GetUnmanagedMarshal", (methodPointerType)&m7533, &t1144_TI, &t1346_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 129, 4096, 255, 0, false, false, 3057, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1346_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7534_MI = 
{
	"get_UMarshal", (methodPointerType)&m7534, &t1144_TI, &t1346_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2499, 0, 24, 0, false, false, 3058, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7535_MI = 
{
	"GetPseudoCustomAttributes", (methodPointerType)&m7535, &t1144_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 131, 0, 255, 0, false, false, 3059, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1144_MIs[] =
{
	&m7524_MI,
	&m9843_MI,
	&m10153_MI,
	&m10154_MI,
	&m10155_MI,
	&m7525_MI,
	&m7526_MI,
	&m7527_MI,
	&m7528_MI,
	&m10152_MI,
	&m7529_MI,
	&m7530_MI,
	&m7531_MI,
	&m7532_MI,
	&m7533_MI,
	&m7534_MI,
	&m7535_MI,
	NULL
};
static MethodInfo* t1144_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m2910_MI,
	&m9653_MI,
	NULL,
	&m7525_MI,
	NULL,
	NULL,
	&m6048_MI,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	&m7526_MI,
	&m7527_MI,
	&m7528_MI,
	NULL,
	&m7529_MI,
	&m7532_MI,
	&m7534_MI,
};
extern TypeInfo t2013_TI;
static TypeInfo* t1144_ITIs[] = 
{
	&t2013_TI,
};
static Il2CppInterfaceOffsetPair t1144_IOs[] = 
{
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
	{ &t2013_TI, 14},
};
void t1144_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1402 * tmp;
		tmp = (t1402 *)il2cpp_codegen_object_new (&t1402_TI);
		m7733(tmp, 0, &m7733_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1404 * tmp;
		tmp = (t1404 *)il2cpp_codegen_object_new (&t1404_TI);
		m7734(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2013_TI)), &m7734_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
void t1144_CustomAttributesCacheGenerator_m7529(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t342 * tmp;
		tmp = (t342 *)il2cpp_codegen_object_new (&t342_TI);
		m1507(tmp, &m1507_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1288 * tmp;
		tmp = (t1288 *)il2cpp_codegen_object_new (&t1288_TI);
		m6773(tmp, &m6773_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1144__CustomAttributeCache = {
3,
NULL,
&t1144_CustomAttributesCacheGenerator
};
CustomAttributesCache t1144__CustomAttributeCache_m7529 = {
2,
NULL,
&t1144_CustomAttributesCacheGenerator_m7529
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1144_0_0_0;
extern Il2CppType t1144_1_0_0;
struct t1144;
extern CustomAttributesCache t1144__CustomAttributeCache;
extern CustomAttributesCache t1144__CustomAttributeCache_m7529;
TypeInfo t1144_TI = 
{
	&g_mscorlib_dll_Image, NULL, "FieldInfo", "System.Reflection", t1144_MIs, t1144_PIs, NULL, NULL, &t296_TI, NULL, NULL, &t1144_TI, t1144_ITIs, t1144_VT, &t1144__CustomAttributeCache, &t1144_TI, &t1144_0_0_0, &t1144_1_0_0, t1144_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1144), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, false, false, false, false, false, false, false, false, 17, 8, 0, 0, 0, 25, 1, 3};
#include "t1369.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1369_TI;
#include "t1369MD.h"

#include "t733.h"
#include "t735.h"
#include "t929.h"
#include "t1142.h"
#include "t345.h"
#include "t917.h"
extern TypeInfo t929_TI;
extern TypeInfo t917_TI;
extern TypeInfo t1142_TI;
extern TypeInfo t345_TI;
extern TypeInfo t1147_TI;
extern TypeInfo t1145_TI;
extern TypeInfo t1149_TI;
#include "t733MD.h"
#include "t929MD.h"
#include "t1142MD.h"
#include "t345MD.h"
#include "t917MD.h"
#include "t557MD.h"
extern Il2CppType t1369_0_0_0;
extern MethodInfo m3994_MI;
extern MethodInfo m3996_MI;
extern MethodInfo m7473_MI;
extern MethodInfo m7469_MI;
extern MethodInfo m7538_MI;
extern MethodInfo m8144_MI;
extern MethodInfo m9849_MI;
extern MethodInfo m7567_MI;
extern MethodInfo m7462_MI;
extern MethodInfo m3982_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m3984_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m9854_MI;
extern MethodInfo m4180_MI;
extern MethodInfo m5610_MI;
extern MethodInfo m3986_MI;
extern MethodInfo m9853_MI;
extern MethodInfo m7559_MI;
extern MethodInfo m7558_MI;
extern MethodInfo m7557_MI;
extern MethodInfo m9852_MI;
extern MethodInfo m6025_MI;
extern MethodInfo m9851_MI;
extern MethodInfo m1535_MI;


extern MethodInfo m7536_MI;
 void m7536 (t1369 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t7* V_0 = {0};
	t7* V_1 = {0};
	t929 * V_2 = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		m1331(__this, &m1331_MI);
		t7* L_0 = m3994(p0, (t7*) &_stringLiteral1532, &m3994_MI);
		V_0 = L_0;
		t7* L_1 = m3994(p0, (t7*) &_stringLiteral1057, &m3994_MI);
		V_1 = L_1;
		t7* L_2 = m3994(p0, (t7*) &_stringLiteral1533, &m3994_MI);
		__this->f0 = L_2;
		t7* L_3 = m3994(p0, (t7*) &_stringLiteral445, &m3994_MI);
		__this->f1 = L_3;
		int32_t L_4 = m3996(p0, (t7*) &_stringLiteral1534, &m3996_MI);
		__this->f2 = L_4;
	}

IL_0051:
	try
	{ // begin try (depth: 1)
		__this->f4 = (t537*)NULL;
		// IL_0058: leave.s IL_005d
		goto IL_005d;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t917_TI, e.ex->object.klass))
			goto IL_005a;
		throw e;
	}

IL_005a:
	{ // begin catch(System.Runtime.Serialization.SerializationException)
		// IL_005b: leave.s IL_005d
		goto IL_005d;
	} // end catch (depth: 1)

IL_005d:
	{
		t929 * L_5 = m7473(NULL, V_0, &m7473_MI);
		V_2 = L_5;
		t42 * L_6 = (t42 *)VirtFuncInvoker3< t42 *, t7*, bool, bool >::Invoke(&m7469_MI, V_2, V_1, 1, 1);
		__this->f3 = L_6;
		return;
	}
}
extern MethodInfo m7537_MI;
 void m7537 (t29 * __this, t733 * p0, t7* p1, t42 * p2, t7* p3, int32_t p4, MethodInfo* method){
	{
		m7538(NULL, p0, p1, p2, p3, p4, (t537*)(t537*)NULL, &m7538_MI);
		return;
	}
}
 void m7538 (t29 * __this, t733 * p0, t7* p1, t42 * p2, t7* p3, int32_t p4, t537* p5, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t1369_0_0_0), &m1554_MI);
		m8144(p0, L_0, &m8144_MI);
		t1142 * L_1 = (t1142 *)VirtFuncInvoker0< t1142 * >::Invoke(&m9849_MI, p2);
		t929 * L_2 = m7567(L_1, &m7567_MI);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7462_MI, L_2);
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral1532, L_3, L_4, &m3982_MI);
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, p2);
		t42 * L_6 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral1057, L_5, L_6, &m3982_MI);
		t42 * L_7 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral1533, p1, L_7, &m3982_MI);
		t42 * L_8 = m1554(NULL, LoadTypeToken(&t7_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral445, p3, L_8, &m3982_MI);
		m3984(p0, (t7*) &_stringLiteral1534, p4, &m3984_MI);
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t537_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral1535, (t29 *)(t29 *)p5, L_9, &m3982_MI);
		return;
	}
}
extern MethodInfo m7539_MI;
 void m7539 (t1369 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m7540_MI;
 t29 * m7540 (t1369 * __this, t735  p0, MethodInfo* method){
	t1147* V_0 = {0};
	int32_t V_1 = 0;
	t1145* V_2 = {0};
	int32_t V_3 = 0;
	t557 * V_4 = {0};
	t1144 * V_5 = {0};
	t1146 * V_6 = {0};
	t1143 * V_7 = {0};
	int32_t V_8 = {0};
	{
		int32_t L_0 = (__this->f2);
		V_8 = L_0;
		if (((int32_t)(V_8-1)) == 0)
		{
			goto IL_003f;
		}
		if (((int32_t)(V_8-1)) == 1)
		{
			goto IL_019e;
		}
		if (((int32_t)(V_8-1)) == 2)
		{
			goto IL_0031;
		}
		if (((int32_t)(V_8-1)) == 3)
		{
			goto IL_012e;
		}
		if (((int32_t)(V_8-1)) == 4)
		{
			goto IL_0031;
		}
		if (((int32_t)(V_8-1)) == 5)
		{
			goto IL_0031;
		}
		if (((int32_t)(V_8-1)) == 6)
		{
			goto IL_0031;
		}
		if (((int32_t)(V_8-1)) == 7)
		{
			goto IL_0090;
		}
	}

IL_0031:
	{
		if ((((int32_t)V_8) == ((int32_t)((int32_t)16))))
		{
			goto IL_0166;
		}
	}
	{
		goto IL_01d6;
	}

IL_003f:
	{
		t42 * L_1 = (__this->f3);
		t1147* L_2 = (t1147*)VirtFuncInvoker1< t1147*, int32_t >::Invoke(&m9854_MI, L_1, ((int32_t)60));
		V_0 = L_2;
		V_1 = 0;
		goto IL_006e;
	}

IL_0051:
	{
		int32_t L_3 = V_1;
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, (*(t660 **)(t660 **)SZArrayLdElema(V_0, L_3)));
		t7* L_5 = (__this->f1);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t7* >::Invoke(&m4180_MI, L_4, L_5);
		if (!L_6)
		{
			goto IL_006a;
		}
	}
	{
		int32_t L_7 = V_1;
		return (*(t660 **)(t660 **)SZArrayLdElema(V_0, L_7));
	}

IL_006a:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_006e:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_0051;
		}
	}
	{
		t7* L_8 = (__this->f1);
		t42 * L_9 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_10 = m5610(NULL, (t7*) &_stringLiteral1536, L_8, L_9, &m5610_MI);
		t917 * L_11 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_11, L_10, &m3986_MI);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_0090:
	{
		t42 * L_12 = (__this->f3);
		t1145* L_13 = (t1145*)VirtFuncInvoker1< t1145*, int32_t >::Invoke(&m9853_MI, L_12, ((int32_t)60));
		V_2 = L_13;
		V_3 = 0;
		goto IL_010c;
	}

IL_00a2:
	{
		int32_t L_14 = V_3;
		t7* L_15 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, (*(t557 **)(t557 **)SZArrayLdElema(V_2, L_14)));
		t7* L_16 = (__this->f1);
		bool L_17 = (bool)VirtFuncInvoker1< bool, t7* >::Invoke(&m4180_MI, L_15, L_16);
		if (!L_17)
		{
			goto IL_00bb;
		}
	}
	{
		int32_t L_18 = V_3;
		return (*(t557 **)(t557 **)SZArrayLdElema(V_2, L_18));
	}

IL_00bb:
	{
		t537* L_19 = (__this->f4);
		if (!L_19)
		{
			goto IL_0108;
		}
	}
	{
		int32_t L_20 = V_3;
		bool L_21 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7559_MI, (*(t557 **)(t557 **)SZArrayLdElema(V_2, L_20)));
		if (!L_21)
		{
			goto IL_0108;
		}
	}
	{
		int32_t L_22 = V_3;
		t537* L_23 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m7558_MI, (*(t557 **)(t557 **)SZArrayLdElema(V_2, L_22)));
		t537* L_24 = (__this->f4);
		if ((((uint32_t)(((int32_t)(((t20 *)L_23)->max_length)))) != ((uint32_t)(((int32_t)(((t20 *)L_24)->max_length))))))
		{
			goto IL_0108;
		}
	}
	{
		int32_t L_25 = V_3;
		t537* L_26 = (__this->f4);
		t557 * L_27 = (t557 *)VirtFuncInvoker1< t557 *, t537* >::Invoke(&m7557_MI, (*(t557 **)(t557 **)SZArrayLdElema(V_2, L_25)), L_26);
		V_4 = L_27;
		t7* L_28 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, V_4);
		t7* L_29 = (__this->f1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_30 = m1713(NULL, L_28, L_29, &m1713_MI);
		if (!L_30)
		{
			goto IL_0108;
		}
	}
	{
		return V_4;
	}

IL_0108:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_010c:
	{
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)V_2)->max_length))))))
		{
			goto IL_00a2;
		}
	}
	{
		t7* L_31 = (__this->f1);
		t42 * L_32 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_33 = m5610(NULL, (t7*) &_stringLiteral1537, L_31, L_32, &m5610_MI);
		t917 * L_34 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_34, L_33, &m3986_MI);
		il2cpp_codegen_raise_exception(L_34);
	}

IL_012e:
	{
		t42 * L_35 = (__this->f3);
		t7* L_36 = (__this->f0);
		t1144 * L_37 = (t1144 *)VirtFuncInvoker2< t1144 *, t7*, int32_t >::Invoke(&m9852_MI, L_35, L_36, ((int32_t)60));
		V_5 = L_37;
		if (!V_5)
		{
			goto IL_014a;
		}
	}
	{
		return V_5;
	}

IL_014a:
	{
		t7* L_38 = (__this->f0);
		t42 * L_39 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_40 = m5610(NULL, (t7*) &_stringLiteral1538, L_38, L_39, &m5610_MI);
		t917 * L_41 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_41, L_40, &m3986_MI);
		il2cpp_codegen_raise_exception(L_41);
	}

IL_0166:
	{
		t42 * L_42 = (__this->f3);
		t7* L_43 = (__this->f0);
		t1146 * L_44 = (t1146 *)VirtFuncInvoker2< t1146 *, t7*, int32_t >::Invoke(&m6025_MI, L_42, L_43, ((int32_t)60));
		V_6 = L_44;
		if (!V_6)
		{
			goto IL_0182;
		}
	}
	{
		return V_6;
	}

IL_0182:
	{
		t7* L_45 = (__this->f0);
		t42 * L_46 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_47 = m5610(NULL, (t7*) &_stringLiteral1539, L_45, L_46, &m5610_MI);
		t917 * L_48 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_48, L_47, &m3986_MI);
		il2cpp_codegen_raise_exception(L_48);
	}

IL_019e:
	{
		t42 * L_49 = (__this->f3);
		t7* L_50 = (__this->f0);
		t1143 * L_51 = (t1143 *)VirtFuncInvoker2< t1143 *, t7*, int32_t >::Invoke(&m9851_MI, L_49, L_50, ((int32_t)60));
		V_7 = L_51;
		if (!V_7)
		{
			goto IL_01ba;
		}
	}
	{
		return V_7;
	}

IL_01ba:
	{
		t7* L_52 = (__this->f0);
		t42 * L_53 = (__this->f3);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_54 = m5610(NULL, (t7*) &_stringLiteral1540, L_52, L_53, &m5610_MI);
		t917 * L_55 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_55, L_54, &m3986_MI);
		il2cpp_codegen_raise_exception(L_55);
	}

IL_01d6:
	{
		int32_t L_56 = (__this->f2);
		int32_t L_57 = L_56;
		t29 * L_58 = Box(InitializedTypeInfo(&t1149_TI), &L_57);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_59 = m1535(NULL, (t7*) &_stringLiteral1541, L_58, &m1535_MI);
		t917 * L_60 = (t917 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t917_TI));
		m3986(L_60, L_59, &m3986_MI);
		il2cpp_codegen_raise_exception(L_60);
	}
}
// Metadata Definition System.Reflection.MemberInfoSerializationHolder
extern Il2CppType t7_0_0_33;
FieldInfo t1369_f0_FieldInfo = 
{
	"_memberName", &t7_0_0_33, &t1369_TI, offsetof(t1369, f0), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_33;
FieldInfo t1369_f1_FieldInfo = 
{
	"_memberSignature", &t7_0_0_33, &t1369_TI, offsetof(t1369, f1), &EmptyCustomAttributesCache};
extern Il2CppType t1149_0_0_33;
FieldInfo t1369_f2_FieldInfo = 
{
	"_memberType", &t1149_0_0_33, &t1369_TI, offsetof(t1369, f2), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_33;
FieldInfo t1369_f3_FieldInfo = 
{
	"_reflectedType", &t42_0_0_33, &t1369_TI, offsetof(t1369, f3), &EmptyCustomAttributesCache};
extern Il2CppType t537_0_0_33;
FieldInfo t1369_f4_FieldInfo = 
{
	"_genericArguments", &t537_0_0_33, &t1369_TI, offsetof(t1369, f4), &EmptyCustomAttributesCache};
static FieldInfo* t1369_FIs[] =
{
	&t1369_f0_FieldInfo,
	&t1369_f1_FieldInfo,
	&t1369_f2_FieldInfo,
	&t1369_f3_FieldInfo,
	&t1369_f4_FieldInfo,
	NULL
};
extern Il2CppType t733_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1369_m7536_ParameterInfos[] = 
{
	{"info", 0, 134221572, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"ctx", 1, 134221573, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7536_MI = 
{
	".ctor", (methodPointerType)&m7536, &t1369_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1369_m7536_ParameterInfos, &EmptyCustomAttributesCache, 6273, 0, 255, 2, false, false, 3060, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t1149_0_0_0;
extern Il2CppType t1149_0_0_0;
static ParameterInfo t1369_m7537_ParameterInfos[] = 
{
	{"info", 0, 134221574, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"name", 1, 134221575, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"klass", 2, 134221576, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"signature", 3, 134221577, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"type", 4, 134221578, &EmptyCustomAttributesCache, &t1149_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7537_MI = 
{
	"Serialize", (methodPointerType)&m7537, &t1369_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29_t29_t44, t1369_m7537_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 5, false, false, 3061, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t1149_0_0_0;
extern Il2CppType t537_0_0_0;
static ParameterInfo t1369_m7538_ParameterInfos[] = 
{
	{"info", 0, 134221579, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"name", 1, 134221580, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"klass", 2, 134221581, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"signature", 3, 134221582, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"type", 4, 134221583, &EmptyCustomAttributesCache, &t1149_0_0_0},
	{"genericArguments", 5, 134221584, &EmptyCustomAttributesCache, &t537_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7538_MI = 
{
	"Serialize", (methodPointerType)&m7538, &t1369_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29_t29_t44_t29, t1369_m7538_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 6, false, false, 3062, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1369_m7539_ParameterInfos[] = 
{
	{"info", 0, 134221585, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221586, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7539_MI = 
{
	"GetObjectData", (methodPointerType)&m7539, &t1369_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1369_m7539_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 2, false, false, 3063, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t735_0_0_0;
static ParameterInfo t1369_m7540_ParameterInfos[] = 
{
	{"context", 0, 134221587, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7540_MI = 
{
	"GetRealObject", (methodPointerType)&m7540, &t1369_TI, &t29_0_0_0, RuntimeInvoker_t29_t735, t1369_m7540_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 5, 1, false, false, 3064, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1369_MIs[] =
{
	&m7536_MI,
	&m7537_MI,
	&m7538_MI,
	&m7539_MI,
	&m7540_MI,
	NULL
};
static MethodInfo* t1369_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7539_MI,
	&m7540_MI,
};
extern TypeInfo t2024_TI;
static TypeInfo* t1369_ITIs[] = 
{
	&t374_TI,
	&t2024_TI,
};
static Il2CppInterfaceOffsetPair t1369_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t2024_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1369_1_0_0;
struct t1369;
TypeInfo t1369_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MemberInfoSerializationHolder", "System.Reflection", t1369_MIs, NULL, t1369_FIs, NULL, &t29_TI, NULL, NULL, &t1369_TI, t1369_ITIs, t1369_VT, &EmptyCustomAttributesCache, &t1369_TI, &t1369_0_0_0, &t1369_1_0_0, t1369_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1369), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 0, 5, 0, 0, 6, 2, 2};
#ifndef _MSC_VER
#else
#endif
#include "t1149MD.h"



// Metadata Definition System.Reflection.MemberTypes
extern Il2CppType t44_0_0_1542;
FieldInfo t1149_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1149_TI, offsetof(t1149, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1149_0_0_32854;
FieldInfo t1149_f2_FieldInfo = 
{
	"Constructor", &t1149_0_0_32854, &t1149_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1149_0_0_32854;
FieldInfo t1149_f3_FieldInfo = 
{
	"Event", &t1149_0_0_32854, &t1149_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1149_0_0_32854;
FieldInfo t1149_f4_FieldInfo = 
{
	"Field", &t1149_0_0_32854, &t1149_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1149_0_0_32854;
FieldInfo t1149_f5_FieldInfo = 
{
	"Method", &t1149_0_0_32854, &t1149_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1149_0_0_32854;
FieldInfo t1149_f6_FieldInfo = 
{
	"Property", &t1149_0_0_32854, &t1149_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1149_0_0_32854;
FieldInfo t1149_f7_FieldInfo = 
{
	"TypeInfo", &t1149_0_0_32854, &t1149_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1149_0_0_32854;
FieldInfo t1149_f8_FieldInfo = 
{
	"Custom", &t1149_0_0_32854, &t1149_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1149_0_0_32854;
FieldInfo t1149_f9_FieldInfo = 
{
	"NestedType", &t1149_0_0_32854, &t1149_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1149_0_0_32854;
FieldInfo t1149_f10_FieldInfo = 
{
	"All", &t1149_0_0_32854, &t1149_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1149_FIs[] =
{
	&t1149_f1_FieldInfo,
	&t1149_f2_FieldInfo,
	&t1149_f3_FieldInfo,
	&t1149_f4_FieldInfo,
	&t1149_f5_FieldInfo,
	&t1149_f6_FieldInfo,
	&t1149_f7_FieldInfo,
	&t1149_f8_FieldInfo,
	&t1149_f9_FieldInfo,
	&t1149_f10_FieldInfo,
	NULL
};
static const int32_t t1149_f2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1149_f2_DefaultValue = 
{
	&t1149_f2_FieldInfo, { (char*)&t1149_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1149_f3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1149_f3_DefaultValue = 
{
	&t1149_f3_FieldInfo, { (char*)&t1149_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1149_f4_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1149_f4_DefaultValue = 
{
	&t1149_f4_FieldInfo, { (char*)&t1149_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1149_f5_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t1149_f5_DefaultValue = 
{
	&t1149_f5_FieldInfo, { (char*)&t1149_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1149_f6_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t1149_f6_DefaultValue = 
{
	&t1149_f6_FieldInfo, { (char*)&t1149_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1149_f7_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t1149_f7_DefaultValue = 
{
	&t1149_f7_FieldInfo, { (char*)&t1149_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1149_f8_DefaultValueData = 64;
static Il2CppFieldDefaultValueEntry t1149_f8_DefaultValue = 
{
	&t1149_f8_FieldInfo, { (char*)&t1149_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1149_f9_DefaultValueData = 128;
static Il2CppFieldDefaultValueEntry t1149_f9_DefaultValue = 
{
	&t1149_f9_FieldInfo, { (char*)&t1149_f9_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1149_f10_DefaultValueData = 191;
static Il2CppFieldDefaultValueEntry t1149_f10_DefaultValue = 
{
	&t1149_f10_FieldInfo, { (char*)&t1149_f10_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1149_FDVs[] = 
{
	&t1149_f2_DefaultValue,
	&t1149_f3_DefaultValue,
	&t1149_f4_DefaultValue,
	&t1149_f5_DefaultValue,
	&t1149_f6_DefaultValue,
	&t1149_f7_DefaultValue,
	&t1149_f8_DefaultValue,
	&t1149_f9_DefaultValue,
	&t1149_f10_DefaultValue,
	NULL
};
static MethodInfo* t1149_MIs[] =
{
	NULL
};
static MethodInfo* t1149_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1149_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1149_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1149__CustomAttributeCache = {
2,
NULL,
&t1149_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1149_1_0_0;
extern CustomAttributesCache t1149__CustomAttributeCache;
TypeInfo t1149_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MemberTypes", "System.Reflection", t1149_MIs, NULL, t1149_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1149_VT, &t1149__CustomAttributeCache, &t44_TI, &t1149_0_0_0, &t1149_1_0_0, t1149_IOs, NULL, NULL, t1149_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1149)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 10, 0, 0, 23, 0, 3};
#include "t1342.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1342_TI;
#include "t1342MD.h"



// Metadata Definition System.Reflection.MethodAttributes
extern Il2CppType t44_0_0_1542;
FieldInfo t1342_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1342_TI, offsetof(t1342, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f2_FieldInfo = 
{
	"MemberAccessMask", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f3_FieldInfo = 
{
	"PrivateScope", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f4_FieldInfo = 
{
	"Private", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f5_FieldInfo = 
{
	"FamANDAssem", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f6_FieldInfo = 
{
	"Assembly", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f7_FieldInfo = 
{
	"Family", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f8_FieldInfo = 
{
	"FamORAssem", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f9_FieldInfo = 
{
	"Public", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f10_FieldInfo = 
{
	"Static", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f11_FieldInfo = 
{
	"Final", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f12_FieldInfo = 
{
	"Virtual", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f13_FieldInfo = 
{
	"HideBySig", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f14_FieldInfo = 
{
	"VtableLayoutMask", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f15_FieldInfo = 
{
	"CheckAccessOnOverride", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f16_FieldInfo = 
{
	"ReuseSlot", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f17_FieldInfo = 
{
	"NewSlot", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f18_FieldInfo = 
{
	"Abstract", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f19_FieldInfo = 
{
	"SpecialName", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f20_FieldInfo = 
{
	"PinvokeImpl", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f21_FieldInfo = 
{
	"UnmanagedExport", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f22_FieldInfo = 
{
	"RTSpecialName", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f23_FieldInfo = 
{
	"ReservedMask", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f24_FieldInfo = 
{
	"HasSecurity", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_32854;
FieldInfo t1342_f25_FieldInfo = 
{
	"RequireSecObject", &t1342_0_0_32854, &t1342_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1342_FIs[] =
{
	&t1342_f1_FieldInfo,
	&t1342_f2_FieldInfo,
	&t1342_f3_FieldInfo,
	&t1342_f4_FieldInfo,
	&t1342_f5_FieldInfo,
	&t1342_f6_FieldInfo,
	&t1342_f7_FieldInfo,
	&t1342_f8_FieldInfo,
	&t1342_f9_FieldInfo,
	&t1342_f10_FieldInfo,
	&t1342_f11_FieldInfo,
	&t1342_f12_FieldInfo,
	&t1342_f13_FieldInfo,
	&t1342_f14_FieldInfo,
	&t1342_f15_FieldInfo,
	&t1342_f16_FieldInfo,
	&t1342_f17_FieldInfo,
	&t1342_f18_FieldInfo,
	&t1342_f19_FieldInfo,
	&t1342_f20_FieldInfo,
	&t1342_f21_FieldInfo,
	&t1342_f22_FieldInfo,
	&t1342_f23_FieldInfo,
	&t1342_f24_FieldInfo,
	&t1342_f25_FieldInfo,
	NULL
};
static const int32_t t1342_f2_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry t1342_f2_DefaultValue = 
{
	&t1342_f2_FieldInfo, { (char*)&t1342_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f3_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1342_f3_DefaultValue = 
{
	&t1342_f3_FieldInfo, { (char*)&t1342_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f4_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1342_f4_DefaultValue = 
{
	&t1342_f4_FieldInfo, { (char*)&t1342_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f5_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1342_f5_DefaultValue = 
{
	&t1342_f5_FieldInfo, { (char*)&t1342_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f6_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1342_f6_DefaultValue = 
{
	&t1342_f6_FieldInfo, { (char*)&t1342_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f7_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1342_f7_DefaultValue = 
{
	&t1342_f7_FieldInfo, { (char*)&t1342_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f8_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t1342_f8_DefaultValue = 
{
	&t1342_f8_FieldInfo, { (char*)&t1342_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f9_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t1342_f9_DefaultValue = 
{
	&t1342_f9_FieldInfo, { (char*)&t1342_f9_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f10_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t1342_f10_DefaultValue = 
{
	&t1342_f10_FieldInfo, { (char*)&t1342_f10_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f11_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t1342_f11_DefaultValue = 
{
	&t1342_f11_FieldInfo, { (char*)&t1342_f11_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f12_DefaultValueData = 64;
static Il2CppFieldDefaultValueEntry t1342_f12_DefaultValue = 
{
	&t1342_f12_FieldInfo, { (char*)&t1342_f12_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f13_DefaultValueData = 128;
static Il2CppFieldDefaultValueEntry t1342_f13_DefaultValue = 
{
	&t1342_f13_FieldInfo, { (char*)&t1342_f13_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f14_DefaultValueData = 256;
static Il2CppFieldDefaultValueEntry t1342_f14_DefaultValue = 
{
	&t1342_f14_FieldInfo, { (char*)&t1342_f14_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f15_DefaultValueData = 512;
static Il2CppFieldDefaultValueEntry t1342_f15_DefaultValue = 
{
	&t1342_f15_FieldInfo, { (char*)&t1342_f15_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f16_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1342_f16_DefaultValue = 
{
	&t1342_f16_FieldInfo, { (char*)&t1342_f16_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f17_DefaultValueData = 256;
static Il2CppFieldDefaultValueEntry t1342_f17_DefaultValue = 
{
	&t1342_f17_FieldInfo, { (char*)&t1342_f17_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f18_DefaultValueData = 1024;
static Il2CppFieldDefaultValueEntry t1342_f18_DefaultValue = 
{
	&t1342_f18_FieldInfo, { (char*)&t1342_f18_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f19_DefaultValueData = 2048;
static Il2CppFieldDefaultValueEntry t1342_f19_DefaultValue = 
{
	&t1342_f19_FieldInfo, { (char*)&t1342_f19_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f20_DefaultValueData = 8192;
static Il2CppFieldDefaultValueEntry t1342_f20_DefaultValue = 
{
	&t1342_f20_FieldInfo, { (char*)&t1342_f20_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f21_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t1342_f21_DefaultValue = 
{
	&t1342_f21_FieldInfo, { (char*)&t1342_f21_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f22_DefaultValueData = 4096;
static Il2CppFieldDefaultValueEntry t1342_f22_DefaultValue = 
{
	&t1342_f22_FieldInfo, { (char*)&t1342_f22_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f23_DefaultValueData = 53248;
static Il2CppFieldDefaultValueEntry t1342_f23_DefaultValue = 
{
	&t1342_f23_FieldInfo, { (char*)&t1342_f23_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f24_DefaultValueData = 16384;
static Il2CppFieldDefaultValueEntry t1342_f24_DefaultValue = 
{
	&t1342_f24_FieldInfo, { (char*)&t1342_f24_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1342_f25_DefaultValueData = 32768;
static Il2CppFieldDefaultValueEntry t1342_f25_DefaultValue = 
{
	&t1342_f25_FieldInfo, { (char*)&t1342_f25_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1342_FDVs[] = 
{
	&t1342_f2_DefaultValue,
	&t1342_f3_DefaultValue,
	&t1342_f4_DefaultValue,
	&t1342_f5_DefaultValue,
	&t1342_f6_DefaultValue,
	&t1342_f7_DefaultValue,
	&t1342_f8_DefaultValue,
	&t1342_f9_DefaultValue,
	&t1342_f10_DefaultValue,
	&t1342_f11_DefaultValue,
	&t1342_f12_DefaultValue,
	&t1342_f13_DefaultValue,
	&t1342_f14_DefaultValue,
	&t1342_f15_DefaultValue,
	&t1342_f16_DefaultValue,
	&t1342_f17_DefaultValue,
	&t1342_f18_DefaultValue,
	&t1342_f19_DefaultValue,
	&t1342_f20_DefaultValue,
	&t1342_f21_DefaultValue,
	&t1342_f22_DefaultValue,
	&t1342_f23_DefaultValue,
	&t1342_f24_DefaultValue,
	&t1342_f25_DefaultValue,
	NULL
};
static MethodInfo* t1342_MIs[] =
{
	NULL
};
static MethodInfo* t1342_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1342_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1342_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1342__CustomAttributeCache = {
2,
NULL,
&t1342_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1342_0_0_0;
extern Il2CppType t1342_1_0_0;
extern CustomAttributesCache t1342__CustomAttributeCache;
TypeInfo t1342_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodAttributes", "System.Reflection", t1342_MIs, NULL, t1342_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1342_VT, &t1342__CustomAttributeCache, &t44_TI, &t1342_0_0_0, &t1342_1_0_0, t1342_IOs, NULL, NULL, t1342_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1342)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 25, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif

#include "t1343.h"
#include "t1343MD.h"
extern MethodInfo m9842_MI;
extern MethodInfo m9499_MI;
extern MethodInfo m7543_MI;
extern MethodInfo m7545_MI;
extern MethodInfo m6039_MI;
extern MethodInfo m10156_MI;


 void m7541 (t636 * __this, MethodInfo* method){
	{
		m6047(__this, &m6047_MI);
		return;
	}
}
extern MethodInfo m7542_MI;
 t636 * m7542 (t29 * __this, t1343  p0, MethodInfo* method){
	{
		t35 L_0 = m9499((&p0), &m9499_MI);
		t636 * L_1 = m7543(NULL, L_0, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), &m7543_MI);
		return L_1;
	}
}
 t636 * m7543 (t29 * __this, t35 p0, t35 p1, MethodInfo* method){
	t636 * V_0 = {0};
	{
		bool L_0 = m2949(NULL, p0, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), &m2949_MI);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral1530, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0018:
	{
		t636 * L_2 = m7545(NULL, p0, p1, &m7545_MI);
		V_0 = L_2;
		if (V_0)
		{
			goto IL_002e;
		}
	}
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, (t7*) &_stringLiteral1530, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_002e:
	{
		return V_0;
	}
}
extern MethodInfo m7544_MI;
 t636 * m7544 (t29 * __this, t1343  p0, MethodInfo* method){
	t636 * V_0 = {0};
	t42 * V_1 = {0};
	{
		t35 L_0 = m9499((&p0), &m9499_MI);
		t636 * L_1 = m7543(NULL, L_0, (((t35_SFs*)InitializedTypeInfo(&t35_TI)->static_fields)->f1), &m7543_MI);
		V_0 = L_1;
		t42 * L_2 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, V_0);
		V_1 = L_2;
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6042_MI, V_1);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6039_MI, V_1);
		if (!L_4)
		{
			goto IL_0034;
		}
	}

IL_0029:
	{
		t305 * L_5 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_5, (t7*) &_stringLiteral1542, &m1935_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0034:
	{
		return V_0;
	}
}
 t636 * m7545 (t29 * __this, t35 p0, t35 p1, MethodInfo* method){
	typedef t636 * (*m7545_ftn) (t35, t35);
	static m7545_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7545_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MethodBase::GetMethodFromHandleInternalType(System.IntPtr,System.IntPtr)");
	return _il2cpp_icall_func(p0, p1);
}
 t29 * m7546 (t636 * __this, t29 * p0, t316* p1, MethodInfo* method){
	{
		t29 * L_0 = (t29 *)VirtFuncInvoker5< t29 *, t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m10156_MI, __this, p0, 0, (t631 *)NULL, p1, (t633 *)NULL);
		return L_0;
	}
}
 int32_t m7547 (t636 * __this, MethodInfo* method){
	{
		return (int32_t)(1);
	}
}
 bool m7548 (t636 * __this, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m9842_MI, __this);
		return ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)7))) == ((int32_t)6))? 1 : 0);
	}
}
 bool m2952 (t636 * __this, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m9842_MI, __this);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 bool m7549 (t636 * __this, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m9842_MI, __this);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)64)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 t537* m7550 (t636 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
 bool m7551 (t636 * __this, MethodInfo* method){
	{
		return 0;
	}
}
 bool m7552 (t636 * __this, MethodInfo* method){
	{
		return 0;
	}
}
 bool m7553 (t636 * __this, MethodInfo* method){
	{
		return 0;
	}
}
// Metadata Definition System.Reflection.MethodBase
extern MethodInfo m10157_MI;
static PropertyInfo t636____MethodHandle_PropertyInfo = 
{
	&t636_TI, "MethodHandle", &m10157_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t636____Attributes_PropertyInfo = 
{
	&t636_TI, "Attributes", &m9842_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t636____CallingConvention_PropertyInfo = 
{
	&t636_TI, "CallingConvention", &m7547_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t636____IsPublic_PropertyInfo = 
{
	&t636_TI, "IsPublic", &m7548_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t636____IsStatic_PropertyInfo = 
{
	&t636_TI, "IsStatic", &m2952_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t636____IsVirtual_PropertyInfo = 
{
	&t636_TI, "IsVirtual", &m7549_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t636____ContainsGenericParameters_PropertyInfo = 
{
	&t636_TI, "ContainsGenericParameters", &m7551_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t636____IsGenericMethodDefinition_PropertyInfo = 
{
	&t636_TI, "IsGenericMethodDefinition", &m7552_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t636____IsGenericMethod_PropertyInfo = 
{
	&t636_TI, "IsGenericMethod", &m7553_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t636_PIs[] =
{
	&t636____MethodHandle_PropertyInfo,
	&t636____Attributes_PropertyInfo,
	&t636____CallingConvention_PropertyInfo,
	&t636____IsPublic_PropertyInfo,
	&t636____IsStatic_PropertyInfo,
	&t636____IsVirtual_PropertyInfo,
	&t636____ContainsGenericParameters_PropertyInfo,
	&t636____IsGenericMethodDefinition_PropertyInfo,
	&t636____IsGenericMethod_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7541_MI = 
{
	".ctor", (methodPointerType)&m7541, &t636_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 3065, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1343_0_0_0;
extern Il2CppType t1343_0_0_0;
static ParameterInfo t636_m7542_ParameterInfos[] = 
{
	{"handle", 0, 134221588, &EmptyCustomAttributesCache, &t1343_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t1343 (MethodInfo* method, void* obj, void** args);
MethodInfo m7542_MI = 
{
	"GetMethodFromHandleNoGenericCheck", (methodPointerType)&m7542, &t636_TI, &t636_0_0_0, RuntimeInvoker_t29_t1343, t636_m7542_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 3066, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t636_m7543_ParameterInfos[] = 
{
	{"handle", 0, 134221589, &EmptyCustomAttributesCache, &t35_0_0_0},
	{"declaringType", 1, 134221590, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t35_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m7543_MI = 
{
	"GetMethodFromIntPtr", (methodPointerType)&m7543, &t636_TI, &t636_0_0_0, RuntimeInvoker_t29_t35_t35, t636_m7543_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 3067, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1343_0_0_0;
static ParameterInfo t636_m7544_ParameterInfos[] = 
{
	{"handle", 0, 134221591, &EmptyCustomAttributesCache, &t1343_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t1343 (MethodInfo* method, void* obj, void** args);
MethodInfo m7544_MI = 
{
	"GetMethodFromHandle", (methodPointerType)&m7544, &t636_TI, &t636_0_0_0, RuntimeInvoker_t29_t1343, t636_m7544_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 3068, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t636_m7545_ParameterInfos[] = 
{
	{"method_handle", 0, 134221592, &EmptyCustomAttributesCache, &t35_0_0_0},
	{"type_handle", 1, 134221593, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t636_0_0_0;
extern void* RuntimeInvoker_t29_t35_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m7545_MI = 
{
	"GetMethodFromHandleInternalType", (methodPointerType)&m7545, &t636_TI, &t636_0_0_0, RuntimeInvoker_t29_t35_t35, t636_m7545_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 2, false, false, 3069, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t638_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2939_MI = 
{
	"GetParameters", NULL, &t636_TI, &t638_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 14, 0, false, false, 3070, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t636_m7546_ParameterInfos[] = 
{
	{"obj", 0, 134221594, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"parameters", 1, 134221595, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t636__CustomAttributeCache_m7546;
MethodInfo m7546_MI = 
{
	"Invoke", (methodPointerType)&m7546, &t636_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t636_m7546_ParameterInfos, &t636__CustomAttributeCache_m7546, 486, 0, 15, 2, false, false, 3071, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t636_m10156_ParameterInfos[] = 
{
	{"obj", 0, 134221596, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"invokeAttr", 1, 134221597, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 2, 134221598, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"parameters", 3, 134221599, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"culture", 4, 134221600, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10156_MI = 
{
	"Invoke", NULL, &t636_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29_t29, t636_m10156_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 16, 5, false, false, 3072, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1343_0_0_0;
extern void* RuntimeInvoker_t1343 (MethodInfo* method, void* obj, void** args);
MethodInfo m10157_MI = 
{
	"get_MethodHandle", NULL, &t636_TI, &t1343_0_0_0, RuntimeInvoker_t1343, NULL, &EmptyCustomAttributesCache, 3526, 0, 17, 0, false, false, 3073, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1342_0_0_0;
extern void* RuntimeInvoker_t1342 (MethodInfo* method, void* obj, void** args);
MethodInfo m9842_MI = 
{
	"get_Attributes", NULL, &t636_TI, &t1342_0_0_0, RuntimeInvoker_t1342, NULL, &EmptyCustomAttributesCache, 3526, 0, 18, 0, false, false, 3074, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1150_0_0_0;
extern void* RuntimeInvoker_t1150 (MethodInfo* method, void* obj, void** args);
MethodInfo m7547_MI = 
{
	"get_CallingConvention", (methodPointerType)&m7547, &t636_TI, &t1150_0_0_0, RuntimeInvoker_t1150, NULL, &EmptyCustomAttributesCache, 2502, 0, 19, 0, false, false, 3075, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7548_MI = 
{
	"get_IsPublic", (methodPointerType)&m7548, &t636_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, false, 3076, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m2952_MI = 
{
	"get_IsStatic", (methodPointerType)&m2952, &t636_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 21, 0, false, false, 3077, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7549_MI = 
{
	"get_IsVirtual", (methodPointerType)&m7549, &t636_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 22, 0, false, false, 3078, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t636__CustomAttributeCache_m7550;
MethodInfo m7550_MI = 
{
	"GetGenericArguments", (methodPointerType)&m7550, &t636_TI, &t537_0_0_0, RuntimeInvoker_t29, NULL, &t636__CustomAttributeCache_m7550, 454, 0, 23, 0, false, false, 3079, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7551_MI = 
{
	"get_ContainsGenericParameters", (methodPointerType)&m7551, &t636_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2502, 0, 24, 0, false, false, 3080, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7552_MI = 
{
	"get_IsGenericMethodDefinition", (methodPointerType)&m7552, &t636_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2502, 0, 25, 0, false, false, 3081, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7553_MI = 
{
	"get_IsGenericMethod", (methodPointerType)&m7553, &t636_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2502, 0, 26, 0, false, false, 3082, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t636_MIs[] =
{
	&m7541_MI,
	&m7542_MI,
	&m7543_MI,
	&m7544_MI,
	&m7545_MI,
	&m2939_MI,
	&m7546_MI,
	&m10156_MI,
	&m10157_MI,
	&m9842_MI,
	&m7547_MI,
	&m7548_MI,
	&m2952_MI,
	&m7549_MI,
	&m7550_MI,
	&m7551_MI,
	&m7552_MI,
	&m7553_MI,
	NULL
};
static MethodInfo* t636_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m2910_MI,
	&m9653_MI,
	NULL,
	NULL,
	NULL,
	NULL,
	&m6048_MI,
	NULL,
	NULL,
	NULL,
	NULL,
	&m7546_MI,
	NULL,
	NULL,
	NULL,
	&m7547_MI,
	&m7548_MI,
	&m2952_MI,
	&m7549_MI,
	&m7550_MI,
	&m7551_MI,
	&m7552_MI,
	&m7553_MI,
};
static TypeInfo* t636_ITIs[] = 
{
	&t2010_TI,
};
static Il2CppInterfaceOffsetPair t636_IOs[] = 
{
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
	{ &t2010_TI, 14},
};
void t636_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1402 * tmp;
		tmp = (t1402 *)il2cpp_codegen_object_new (&t1402_TI);
		m7733(tmp, 0, &m7733_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1404 * tmp;
		tmp = (t1404 *)il2cpp_codegen_object_new (&t1404_TI);
		m7734(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2010_TI)), &m7734_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
void t636_CustomAttributesCacheGenerator_m7546(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t342 * tmp;
		tmp = (t342 *)il2cpp_codegen_object_new (&t342_TI);
		m1507(tmp, &m1507_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1288 * tmp;
		tmp = (t1288 *)il2cpp_codegen_object_new (&t1288_TI);
		m6773(tmp, &m6773_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t636_CustomAttributesCacheGenerator_m7550(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t636__CustomAttributeCache = {
3,
NULL,
&t636_CustomAttributesCacheGenerator
};
CustomAttributesCache t636__CustomAttributeCache_m7546 = {
2,
NULL,
&t636_CustomAttributesCacheGenerator_m7546
};
CustomAttributesCache t636__CustomAttributeCache_m7550 = {
1,
NULL,
&t636_CustomAttributesCacheGenerator_m7550
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t636_1_0_0;
struct t636;
extern CustomAttributesCache t636__CustomAttributeCache;
extern CustomAttributesCache t636__CustomAttributeCache_m7546;
extern CustomAttributesCache t636__CustomAttributeCache_m7550;
TypeInfo t636_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodBase", "System.Reflection", t636_MIs, t636_PIs, NULL, NULL, &t296_TI, NULL, NULL, &t636_TI, t636_ITIs, t636_VT, &t636__CustomAttributeCache, &t636_TI, &t636_0_0_0, &t636_1_0_0, t636_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t636), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, false, false, false, false, false, false, false, false, 18, 9, 0, 0, 0, 27, 1, 3};
#include "t1370.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1370_TI;
#include "t1370MD.h"



// Metadata Definition System.Reflection.MethodImplAttributes
extern Il2CppType t44_0_0_1542;
FieldInfo t1370_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1370_TI, offsetof(t1370, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f2_FieldInfo = 
{
	"CodeTypeMask", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f3_FieldInfo = 
{
	"IL", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f4_FieldInfo = 
{
	"Native", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f5_FieldInfo = 
{
	"OPTIL", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f6_FieldInfo = 
{
	"Runtime", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f7_FieldInfo = 
{
	"ManagedMask", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f8_FieldInfo = 
{
	"Unmanaged", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f9_FieldInfo = 
{
	"Managed", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f10_FieldInfo = 
{
	"ForwardRef", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f11_FieldInfo = 
{
	"PreserveSig", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f12_FieldInfo = 
{
	"InternalCall", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f13_FieldInfo = 
{
	"Synchronized", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f14_FieldInfo = 
{
	"NoInlining", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_32854;
FieldInfo t1370_f15_FieldInfo = 
{
	"MaxMethodImplVal", &t1370_0_0_32854, &t1370_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1370_FIs[] =
{
	&t1370_f1_FieldInfo,
	&t1370_f2_FieldInfo,
	&t1370_f3_FieldInfo,
	&t1370_f4_FieldInfo,
	&t1370_f5_FieldInfo,
	&t1370_f6_FieldInfo,
	&t1370_f7_FieldInfo,
	&t1370_f8_FieldInfo,
	&t1370_f9_FieldInfo,
	&t1370_f10_FieldInfo,
	&t1370_f11_FieldInfo,
	&t1370_f12_FieldInfo,
	&t1370_f13_FieldInfo,
	&t1370_f14_FieldInfo,
	&t1370_f15_FieldInfo,
	NULL
};
static const int32_t t1370_f2_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1370_f2_DefaultValue = 
{
	&t1370_f2_FieldInfo, { (char*)&t1370_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f3_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1370_f3_DefaultValue = 
{
	&t1370_f3_FieldInfo, { (char*)&t1370_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f4_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1370_f4_DefaultValue = 
{
	&t1370_f4_FieldInfo, { (char*)&t1370_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f5_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1370_f5_DefaultValue = 
{
	&t1370_f5_FieldInfo, { (char*)&t1370_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f6_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1370_f6_DefaultValue = 
{
	&t1370_f6_FieldInfo, { (char*)&t1370_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f7_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1370_f7_DefaultValue = 
{
	&t1370_f7_FieldInfo, { (char*)&t1370_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f8_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1370_f8_DefaultValue = 
{
	&t1370_f8_FieldInfo, { (char*)&t1370_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f9_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1370_f9_DefaultValue = 
{
	&t1370_f9_FieldInfo, { (char*)&t1370_f9_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f10_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t1370_f10_DefaultValue = 
{
	&t1370_f10_FieldInfo, { (char*)&t1370_f10_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f11_DefaultValueData = 128;
static Il2CppFieldDefaultValueEntry t1370_f11_DefaultValue = 
{
	&t1370_f11_FieldInfo, { (char*)&t1370_f11_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f12_DefaultValueData = 4096;
static Il2CppFieldDefaultValueEntry t1370_f12_DefaultValue = 
{
	&t1370_f12_FieldInfo, { (char*)&t1370_f12_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f13_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t1370_f13_DefaultValue = 
{
	&t1370_f13_FieldInfo, { (char*)&t1370_f13_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f14_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t1370_f14_DefaultValue = 
{
	&t1370_f14_FieldInfo, { (char*)&t1370_f14_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1370_f15_DefaultValueData = 65535;
static Il2CppFieldDefaultValueEntry t1370_f15_DefaultValue = 
{
	&t1370_f15_FieldInfo, { (char*)&t1370_f15_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1370_FDVs[] = 
{
	&t1370_f2_DefaultValue,
	&t1370_f3_DefaultValue,
	&t1370_f4_DefaultValue,
	&t1370_f5_DefaultValue,
	&t1370_f6_DefaultValue,
	&t1370_f7_DefaultValue,
	&t1370_f8_DefaultValue,
	&t1370_f9_DefaultValue,
	&t1370_f10_DefaultValue,
	&t1370_f11_DefaultValue,
	&t1370_f12_DefaultValue,
	&t1370_f13_DefaultValue,
	&t1370_f14_DefaultValue,
	&t1370_f15_DefaultValue,
	NULL
};
static MethodInfo* t1370_MIs[] =
{
	NULL
};
static MethodInfo* t1370_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1370_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1370_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1370__CustomAttributeCache = {
1,
NULL,
&t1370_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1370_0_0_0;
extern Il2CppType t1370_1_0_0;
extern CustomAttributesCache t1370__CustomAttributeCache;
TypeInfo t1370_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodImplAttributes", "System.Reflection", t1370_MIs, NULL, t1370_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1370_VT, &t1370__CustomAttributeCache, &t44_TI, &t1370_0_0_0, &t1370_1_0_0, t1370_IOs, NULL, NULL, t1370_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1370)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 15, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m6035_MI;
extern MethodInfo m3988_MI;


extern MethodInfo m7554_MI;
 void m7554 (t557 * __this, MethodInfo* method){
	{
		m7541(__this, &m7541_MI);
		return;
	}
}
extern MethodInfo m7555_MI;
 int32_t m7555 (t557 * __this, MethodInfo* method){
	{
		return (int32_t)(8);
	}
}
extern MethodInfo m7556_MI;
 t42 * m7556 (t557 * __this, MethodInfo* method){
	{
		return (t42 *)NULL;
	}
}
 t557 * m7557 (t557 * __this, t537* p0, MethodInfo* method){
	{
		t42 * L_0 = m1430(__this, &m1430_MI);
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_0);
		t345 * L_2 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_2, L_1, &m3988_MI);
		il2cpp_codegen_raise_exception(L_2);
	}
}
 t537* m7558 (t557 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		return (((t42_SFs*)InitializedTypeInfo(&t42_TI)->static_fields)->f3);
	}
}
 bool m7559 (t557 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m7560_MI;
 bool m7560 (t557 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m7561_MI;
 bool m7561 (t557 * __this, MethodInfo* method){
	{
		return 0;
	}
}
// Metadata Definition System.Reflection.MethodInfo
static PropertyInfo t557____MemberType_PropertyInfo = 
{
	&t557_TI, "MemberType", &m7555_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t557____ReturnType_PropertyInfo = 
{
	&t557_TI, "ReturnType", &m7556_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t557____IsGenericMethod_PropertyInfo = 
{
	&t557_TI, "IsGenericMethod", &m7559_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t557____IsGenericMethodDefinition_PropertyInfo = 
{
	&t557_TI, "IsGenericMethodDefinition", &m7560_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t557____ContainsGenericParameters_PropertyInfo = 
{
	&t557_TI, "ContainsGenericParameters", &m7561_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t557_PIs[] =
{
	&t557____MemberType_PropertyInfo,
	&t557____ReturnType_PropertyInfo,
	&t557____IsGenericMethod_PropertyInfo,
	&t557____IsGenericMethodDefinition_PropertyInfo,
	&t557____ContainsGenericParameters_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7554_MI = 
{
	".ctor", (methodPointerType)&m7554, &t557_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 3083, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10158_MI = 
{
	"GetBaseDefinition", NULL, &t557_TI, &t557_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 27, 0, false, false, 3084, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1149_0_0_0;
extern void* RuntimeInvoker_t1149 (MethodInfo* method, void* obj, void** args);
MethodInfo m7555_MI = 
{
	"get_MemberType", (methodPointerType)&m7555, &t557_TI, &t1149_0_0_0, RuntimeInvoker_t1149, NULL, &EmptyCustomAttributesCache, 2246, 0, 7, 0, false, false, 3085, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7556_MI = 
{
	"get_ReturnType", (methodPointerType)&m7556, &t557_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 28, 0, false, false, 3086, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern CustomAttributesCache t557__CustomAttributeCache_t557_m7557_Arg0_ParameterInfo;
static ParameterInfo t557_m7557_ParameterInfos[] = 
{
	{"typeArguments", 0, 134221601, &t557__CustomAttributeCache_t557_m7557_Arg0_ParameterInfo, &t537_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7557_MI = 
{
	"MakeGenericMethod", (methodPointerType)&m7557, &t557_TI, &t557_0_0_0, RuntimeInvoker_t29_t29, t557_m7557_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 1, false, false, 3087, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t557__CustomAttributeCache_m7558;
MethodInfo m7558_MI = 
{
	"GetGenericArguments", (methodPointerType)&m7558, &t557_TI, &t537_0_0_0, RuntimeInvoker_t29, NULL, &t557__CustomAttributeCache_m7558, 198, 0, 23, 0, false, false, 3088, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7559_MI = 
{
	"get_IsGenericMethod", (methodPointerType)&m7559, &t557_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2246, 0, 26, 0, false, false, 3089, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7560_MI = 
{
	"get_IsGenericMethodDefinition", (methodPointerType)&m7560, &t557_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2246, 0, 25, 0, false, false, 3090, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7561_MI = 
{
	"get_ContainsGenericParameters", (methodPointerType)&m7561, &t557_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2246, 0, 24, 0, false, false, 3091, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t557_MIs[] =
{
	&m7554_MI,
	&m10158_MI,
	&m7555_MI,
	&m7556_MI,
	&m7557_MI,
	&m7558_MI,
	&m7559_MI,
	&m7560_MI,
	&m7561_MI,
	NULL
};
static MethodInfo* t557_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m2910_MI,
	&m9653_MI,
	NULL,
	&m7555_MI,
	NULL,
	NULL,
	&m6048_MI,
	NULL,
	NULL,
	NULL,
	NULL,
	&m7546_MI,
	NULL,
	NULL,
	NULL,
	&m7547_MI,
	&m7548_MI,
	&m2952_MI,
	&m7549_MI,
	&m7558_MI,
	&m7561_MI,
	&m7560_MI,
	&m7559_MI,
	NULL,
	&m7556_MI,
	&m7557_MI,
};
extern TypeInfo t2015_TI;
static TypeInfo* t557_ITIs[] = 
{
	&t2015_TI,
};
static Il2CppInterfaceOffsetPair t557_IOs[] = 
{
	{ &t2010_TI, 14},
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
	{ &t2015_TI, 27},
};
void t557_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1402 * tmp;
		tmp = (t1402 *)il2cpp_codegen_object_new (&t1402_TI);
		m7733(tmp, 0, &m7733_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1404 * tmp;
		tmp = (t1404 *)il2cpp_codegen_object_new (&t1404_TI);
		m7734(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2015_TI)), &m7734_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
#include "t391MD.h"
extern MethodInfo m1817_MI;
void t557_CustomAttributesCacheGenerator_t557_m7557_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t391 * tmp;
		tmp = (t391 *)il2cpp_codegen_object_new (&t391_TI);
		m1817(tmp, &m1817_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t557_CustomAttributesCacheGenerator_m7558(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t557__CustomAttributeCache = {
3,
NULL,
&t557_CustomAttributesCacheGenerator
};
CustomAttributesCache t557__CustomAttributeCache_t557_m7557_Arg0_ParameterInfo = {
1,
NULL,
&t557_CustomAttributesCacheGenerator_t557_m7557_Arg0_ParameterInfo
};
CustomAttributesCache t557__CustomAttributeCache_m7558 = {
1,
NULL,
&t557_CustomAttributesCacheGenerator_m7558
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_1_0_0;
struct t557;
extern CustomAttributesCache t557__CustomAttributeCache;
extern CustomAttributesCache t557__CustomAttributeCache_m7558;
TypeInfo t557_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MethodInfo", "System.Reflection", t557_MIs, t557_PIs, NULL, NULL, &t636_TI, NULL, NULL, &t557_TI, t557_ITIs, t557_VT, &t557__CustomAttributeCache, &t557_TI, &t557_0_0_0, &t557_1_0_0, t557_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t557), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, false, false, false, false, false, false, false, false, 9, 5, 0, 0, 0, 30, 1, 4};
#include "t1371.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1371_TI;
#include "t1371MD.h"

extern MethodInfo m7562_MI;


 void m7562 (t1371 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7563_MI;
 void m7563 (t29 * __this, MethodInfo* method){
	{
		t1371 * L_0 = (t1371 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1371_TI));
		m7562(L_0, &m7562_MI);
		((t1371_SFs*)InitializedTypeInfo(&t1371_TI)->static_fields)->f0 = L_0;
		return;
	}
}
extern MethodInfo m7564_MI;
 void m7564 (t1371 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition System.Reflection.Missing
extern Il2CppType t1371_0_0_54;
FieldInfo t1371_f0_FieldInfo = 
{
	"Value", &t1371_0_0_54, &t1371_TI, offsetof(t1371_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1371_FIs[] =
{
	&t1371_f0_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7562_MI = 
{
	".ctor", (methodPointerType)&m7562, &t1371_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6275, 0, 255, 0, false, false, 3092, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7563_MI = 
{
	".cctor", (methodPointerType)&m7563, &t1371_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3093, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1371_m7564_ParameterInfos[] = 
{
	{"info", 0, 134221602, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221603, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1371__CustomAttributeCache_m7564;
MethodInfo m7564_MI = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData", (methodPointerType)&m7564, &t1371_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1371_m7564_ParameterInfos, &t1371__CustomAttributeCache_m7564, 481, 0, 4, 2, false, false, 3094, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1371_MIs[] =
{
	&m7562_MI,
	&m7563_MI,
	&m7564_MI,
	NULL
};
static MethodInfo* t1371_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7564_MI,
};
static TypeInfo* t1371_ITIs[] = 
{
	&t374_TI,
};
static Il2CppInterfaceOffsetPair t1371_IOs[] = 
{
	{ &t374_TI, 4},
};
void t1371_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern MethodInfo m6081_MI;
void t1371_CustomAttributesCacheGenerator_m7564(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1168 * tmp;
		tmp = (t1168 *)il2cpp_codegen_object_new (&t1168_TI);
		m6081(tmp, &m6081_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1371__CustomAttributeCache = {
1,
NULL,
&t1371_CustomAttributesCacheGenerator
};
CustomAttributesCache t1371__CustomAttributeCache_m7564 = {
1,
NULL,
&t1371_CustomAttributesCacheGenerator_m7564
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1371_0_0_0;
extern Il2CppType t1371_1_0_0;
struct t1371;
extern CustomAttributesCache t1371__CustomAttributeCache;
extern CustomAttributesCache t1371__CustomAttributeCache_m7564;
TypeInfo t1371_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Missing", "System.Reflection", t1371_MIs, NULL, t1371_FIs, NULL, &t29_TI, NULL, NULL, &t1371_TI, t1371_ITIs, t1371_VT, &t1371__CustomAttributeCache, &t1371_TI, &t1371_0_0_0, &t1371_1_0_0, t1371_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1371), 0, -1, sizeof(t1371_SFs), 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, true, false, false, 3, 0, 1, 0, 0, 5, 1, 1};
#ifndef _MSC_VER
#else
#endif

#include "t1372.h"
extern TypeInfo t1372_TI;
extern TypeInfo t1656_TI;
#include "t1372MD.h"
#include "t1656MD.h"
#include "t1678MD.h"
extern MethodInfo m7574_MI;
extern MethodInfo m9624_MI;
extern MethodInfo m7575_MI;
extern MethodInfo m9318_MI;
extern MethodInfo m9587_MI;
extern MethodInfo m9320_MI;
extern MethodInfo m2930_MI;
extern MethodInfo m2924_MI;
extern MethodInfo m1715_MI;
extern MethodInfo m1742_MI;
extern MethodInfo m2922_MI;
extern MethodInfo m4257_MI;
extern MethodInfo m5586_MI;


extern MethodInfo m7565_MI;
 void m7565 (t1142 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7566_MI;
 void m7566 (t29 * __this, MethodInfo* method){
	{
		t35 L_0 = { &m7574_MI };
		t1372 * L_1 = (t1372 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1372_TI));
		m9624(L_1, NULL, L_0, &m9624_MI);
		((t1142_SFs*)InitializedTypeInfo(&t1142_TI)->static_fields)->f1 = L_1;
		t35 L_2 = { &m7575_MI };
		t1372 * L_3 = (t1372 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1372_TI));
		m9624(L_3, NULL, L_2, &m9624_MI);
		((t1142_SFs*)InitializedTypeInfo(&t1142_TI)->static_fields)->f2 = L_3;
		return;
	}
}
 t929 * m7567 (t1142 * __this, MethodInfo* method){
	{
		t929 * L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m7568_MI;
 t7* m7568 (t1142 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f7);
		return L_0;
	}
}
extern MethodInfo m7569_MI;
 t316* m7569 (t1142 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9318(NULL, __this, p0, p1, &m9318_MI);
		return L_0;
	}
}
extern MethodInfo m7570_MI;
 void m7570 (t1142 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		m9587(NULL, __this, p0, p1, &m9587_MI);
		return;
	}
}
extern MethodInfo m7571_MI;
 bool m7571 (t1142 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		bool L_0 = m9320(NULL, __this, p0, p1, &m9320_MI);
		return L_0;
	}
}
extern MethodInfo m7572_MI;
 bool m7572 (t1142 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f8);
		return L_0;
	}
}
extern MethodInfo m7573_MI;
 t7* m7573 (t1142 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f6);
		return L_0;
	}
}
 bool m7574 (t29 * __this, t42 * p0, t29 * p1, MethodInfo* method){
	t7* V_0 = {0};
	{
		V_0 = ((t7*)Castclass(p1, (&t7_TI)));
		bool L_0 = m2930(V_0, (t7*) &_stringLiteral515, &m2930_MI);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, p0);
		int32_t L_2 = m1715(V_0, &m1715_MI);
		t7* L_3 = m1742(V_0, 0, ((int32_t)(L_2-1)), &m1742_MI);
		bool L_4 = m2922(L_1, L_3, &m2922_MI);
		return L_4;
	}

IL_002f:
	{
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_6 = m1713(NULL, L_5, V_0, &m1713_MI);
		return L_6;
	}
}
 bool m7575 (t29 * __this, t42 * p0, t29 * p1, MethodInfo* method){
	t7* V_0 = {0};
	{
		V_0 = ((t7*)Castclass(p1, (&t7_TI)));
		bool L_0 = m2930(V_0, (t7*) &_stringLiteral515, &m2930_MI);
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, p0);
		t7* L_2 = m4257(L_1, &m4257_MI);
		int32_t L_3 = m1715(V_0, &m1715_MI);
		t7* L_4 = m1742(V_0, 0, ((int32_t)(L_3-1)), &m1742_MI);
		t7* L_5 = m4257(L_4, &m4257_MI);
		bool L_6 = m2922(L_2, L_5, &m2922_MI);
		return L_6;
	}

IL_0039:
	{
		t7* L_7 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, p0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		int32_t L_8 = m5586(NULL, L_7, V_0, 1, &m5586_MI);
		return ((((int32_t)L_8) == ((int32_t)0))? 1 : 0);
	}
}
// Metadata Definition System.Reflection.Module
extern Il2CppType t630_0_0_32849;
FieldInfo t1142_f0_FieldInfo = 
{
	"defaultBindingFlags", &t630_0_0_32849, &t1142_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1372_0_0_54;
FieldInfo t1142_f1_FieldInfo = 
{
	"FilterTypeName", &t1372_0_0_54, &t1142_TI, offsetof(t1142_SFs, f1), &EmptyCustomAttributesCache};
extern Il2CppType t1372_0_0_54;
FieldInfo t1142_f2_FieldInfo = 
{
	"FilterTypeNameIgnoreCase", &t1372_0_0_54, &t1142_TI, offsetof(t1142_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t35_0_0_1;
FieldInfo t1142_f3_FieldInfo = 
{
	"_impl", &t35_0_0_1, &t1142_TI, offsetof(t1142, f3), &EmptyCustomAttributesCache};
extern Il2CppType t929_0_0_3;
FieldInfo t1142_f4_FieldInfo = 
{
	"assembly", &t929_0_0_3, &t1142_TI, offsetof(t1142, f4), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_3;
FieldInfo t1142_f5_FieldInfo = 
{
	"fqname", &t7_0_0_3, &t1142_TI, offsetof(t1142, f5), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_3;
FieldInfo t1142_f6_FieldInfo = 
{
	"name", &t7_0_0_3, &t1142_TI, offsetof(t1142, f6), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_3;
FieldInfo t1142_f7_FieldInfo = 
{
	"scopename", &t7_0_0_3, &t1142_TI, offsetof(t1142, f7), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_3;
FieldInfo t1142_f8_FieldInfo = 
{
	"is_resource", &t40_0_0_3, &t1142_TI, offsetof(t1142, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_3;
FieldInfo t1142_f9_FieldInfo = 
{
	"token", &t44_0_0_3, &t1142_TI, offsetof(t1142, f9), &EmptyCustomAttributesCache};
static FieldInfo* t1142_FIs[] =
{
	&t1142_f0_FieldInfo,
	&t1142_f1_FieldInfo,
	&t1142_f2_FieldInfo,
	&t1142_f3_FieldInfo,
	&t1142_f4_FieldInfo,
	&t1142_f5_FieldInfo,
	&t1142_f6_FieldInfo,
	&t1142_f7_FieldInfo,
	&t1142_f8_FieldInfo,
	&t1142_f9_FieldInfo,
	NULL
};
static const int32_t t1142_f0_DefaultValueData = 28;
static Il2CppFieldDefaultValueEntry t1142_f0_DefaultValue = 
{
	&t1142_f0_FieldInfo, { (char*)&t1142_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1142_FDVs[] = 
{
	&t1142_f0_DefaultValue,
	NULL
};
static PropertyInfo t1142____Assembly_PropertyInfo = 
{
	&t1142_TI, "Assembly", &m7567_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1142____ScopeName_PropertyInfo = 
{
	&t1142_TI, "ScopeName", &m7568_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1142_PIs[] =
{
	&t1142____Assembly_PropertyInfo,
	&t1142____ScopeName_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7565_MI = 
{
	".ctor", (methodPointerType)&m7565, &t1142_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6275, 0, 255, 0, false, false, 3095, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7566_MI = 
{
	".cctor", (methodPointerType)&m7566, &t1142_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3096, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t929_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7567_MI = 
{
	"get_Assembly", (methodPointerType)&m7567, &t1142_TI, &t929_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3097, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7568_MI = 
{
	"get_ScopeName", (methodPointerType)&m7568, &t1142_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3098, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1142_m7569_ParameterInfos[] = 
{
	{"attributeType", 0, 134221604, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221605, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7569_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m7569, &t1142_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t297, t1142_m7569_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 7, 2, false, false, 3099, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1142_m7570_ParameterInfos[] = 
{
	{"info", 0, 134221606, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221607, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7570_MI = 
{
	"GetObjectData", (methodPointerType)&m7570, &t1142_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1142_m7570_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 8, 2, false, false, 3100, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1142_m7571_ParameterInfos[] = 
{
	{"attributeType", 0, 134221608, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221609, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7571_MI = 
{
	"IsDefined", (methodPointerType)&m7571, &t1142_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t297, t1142_m7571_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 9, 2, false, false, 3101, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7572_MI = 
{
	"IsResource", (methodPointerType)&m7572, &t1142_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 3102, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7573_MI = 
{
	"ToString", (methodPointerType)&m7573, &t1142_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 3103, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1142_m7574_ParameterInfos[] = 
{
	{"m", 0, 134221610, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"filterCriteria", 1, 134221611, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7574_MI = 
{
	"filter_by_type_name", (methodPointerType)&m7574, &t1142_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1142_m7574_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 3104, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1142_m7575_ParameterInfos[] = 
{
	{"m", 0, 134221612, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"filterCriteria", 1, 134221613, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7575_MI = 
{
	"filter_by_type_name_ignore_case", (methodPointerType)&m7575, &t1142_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t1142_m7575_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 3105, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1142_MIs[] =
{
	&m7565_MI,
	&m7566_MI,
	&m7567_MI,
	&m7568_MI,
	&m7569_MI,
	&m7570_MI,
	&m7571_MI,
	&m7572_MI,
	&m7573_MI,
	&m7574_MI,
	&m7575_MI,
	NULL
};
static MethodInfo* t1142_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m7573_MI,
	&m7570_MI,
	&m7569_MI,
	&m7571_MI,
	&m7569_MI,
	&m7570_MI,
	&m7571_MI,
};
extern TypeInfo t2017_TI;
static TypeInfo* t1142_ITIs[] = 
{
	&t374_TI,
	&t1657_TI,
	&t2017_TI,
};
static Il2CppInterfaceOffsetPair t1142_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t1657_TI, 5},
	{ &t2017_TI, 7},
};
void t1142_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1404 * tmp;
		tmp = (t1404 *)il2cpp_codegen_object_new (&t1404_TI);
		m7734(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2017_TI)), &m7734_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1402 * tmp;
		tmp = (t1402 *)il2cpp_codegen_object_new (&t1402_TI);
		m7733(tmp, 0, &m7733_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1142__CustomAttributeCache = {
3,
NULL,
&t1142_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1142_0_0_0;
extern Il2CppType t1142_1_0_0;
struct t1142;
extern CustomAttributesCache t1142__CustomAttributeCache;
TypeInfo t1142_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Module", "System.Reflection", t1142_MIs, t1142_PIs, t1142_FIs, NULL, &t29_TI, NULL, NULL, &t1142_TI, t1142_ITIs, t1142_VT, &t1142__CustomAttributeCache, &t1142_TI, &t1142_0_0_0, &t1142_1_0_0, t1142_IOs, NULL, NULL, t1142_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1142), 0, -1, sizeof(t1142_SFs), 0, -1, 8193, 0, false, false, false, false, false, false, false, false, false, true, false, false, 11, 2, 10, 0, 0, 10, 3, 3};
#include "t1373.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1373_TI;
#include "t1373MD.h"

#include "t1374.h"
extern MethodInfo m7576_MI;


 void m7576 (t29 * __this, t1374 * p0, t1373 * p1, MethodInfo* method){
	typedef void (*m7576_ftn) (t1374 *, t1373 *);
	static m7576_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7576_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoEventInfo::get_event_info(System.Reflection.MonoEvent,System.Reflection.MonoEventInfo&)");
	_il2cpp_icall_func(p0, p1);
}
extern MethodInfo m7577_MI;
 t1373  m7577 (t29 * __this, t1374 * p0, MethodInfo* method){
	t1373  V_0 = {0};
	{
		m7576(NULL, p0, (&V_0), &m7576_MI);
		return V_0;
	}
}
// Metadata Definition System.Reflection.MonoEventInfo
extern Il2CppType t42_0_0_6;
FieldInfo t1373_f0_FieldInfo = 
{
	"declaring_type", &t42_0_0_6, &t1373_TI, offsetof(t1373, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_6;
FieldInfo t1373_f1_FieldInfo = 
{
	"reflected_type", &t42_0_0_6, &t1373_TI, offsetof(t1373, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_6;
FieldInfo t1373_f2_FieldInfo = 
{
	"name", &t7_0_0_6, &t1373_TI, offsetof(t1373, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t557_0_0_6;
FieldInfo t1373_f3_FieldInfo = 
{
	"add_method", &t557_0_0_6, &t1373_TI, offsetof(t1373, f3) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t557_0_0_6;
FieldInfo t1373_f4_FieldInfo = 
{
	"remove_method", &t557_0_0_6, &t1373_TI, offsetof(t1373, f4) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t557_0_0_6;
FieldInfo t1373_f5_FieldInfo = 
{
	"raise_method", &t557_0_0_6, &t1373_TI, offsetof(t1373, f5) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1367_0_0_6;
FieldInfo t1373_f6_FieldInfo = 
{
	"attrs", &t1367_0_0_6, &t1373_TI, offsetof(t1373, f6) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1145_0_0_6;
FieldInfo t1373_f7_FieldInfo = 
{
	"other_methods", &t1145_0_0_6, &t1373_TI, offsetof(t1373, f7) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t1373_FIs[] =
{
	&t1373_f0_FieldInfo,
	&t1373_f1_FieldInfo,
	&t1373_f2_FieldInfo,
	&t1373_f3_FieldInfo,
	&t1373_f4_FieldInfo,
	&t1373_f5_FieldInfo,
	&t1373_f6_FieldInfo,
	&t1373_f7_FieldInfo,
	NULL
};
extern Il2CppType t1374_0_0_0;
extern Il2CppType t1374_0_0_0;
extern Il2CppType t1373_1_0_2;
extern Il2CppType t1373_1_0_0;
static ParameterInfo t1373_m7576_ParameterInfos[] = 
{
	{"ev", 0, 134221614, &EmptyCustomAttributesCache, &t1374_0_0_0},
	{"info", 1, 134221615, &EmptyCustomAttributesCache, &t1373_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t2025 (MethodInfo* method, void* obj, void** args);
MethodInfo m7576_MI = 
{
	"get_event_info", (methodPointerType)&m7576, &t1373_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t2025, t1373_m7576_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 2, false, false, 3106, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1374_0_0_0;
static ParameterInfo t1373_m7577_ParameterInfos[] = 
{
	{"ev", 0, 134221616, &EmptyCustomAttributesCache, &t1374_0_0_0},
};
extern Il2CppType t1373_0_0_0;
extern void* RuntimeInvoker_t1373_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7577_MI = 
{
	"GetEventInfo", (methodPointerType)&m7577, &t1373_TI, &t1373_0_0_0, RuntimeInvoker_t1373_t29, t1373_m7577_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 3107, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1373_MIs[] =
{
	&m7576_MI,
	&m7577_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t1373_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1373_0_0_0;
extern TypeInfo t110_TI;
TypeInfo t1373_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoEventInfo", "System.Reflection", t1373_MIs, NULL, t1373_FIs, NULL, &t110_TI, NULL, NULL, &t1373_TI, NULL, t1373_VT, &EmptyCustomAttributesCache, &t1373_TI, &t1373_0_0_0, &t1373_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1373)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048840, 0, true, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 8, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1374_TI;
#include "t1374MD.h"

extern MethodInfo m7583_MI;
extern MethodInfo m1295_MI;
extern MethodInfo m9319_MI;
extern MethodInfo m7582_MI;
extern MethodInfo m7584_MI;


extern MethodInfo m7578_MI;
 void m7578 (t1374 * __this, MethodInfo* method){
	{
		m7521(__this, &m7521_MI);
		return;
	}
}
extern MethodInfo m7579_MI;
 int32_t m7579 (t1374 * __this, MethodInfo* method){
	t1373  V_0 = {0};
	{
		t1373  L_0 = m7577(NULL, __this, &m7577_MI);
		V_0 = L_0;
		int32_t L_1 = ((&V_0)->f6);
		return L_1;
	}
}
extern MethodInfo m7580_MI;
 t557 * m7580 (t1374 * __this, bool p0, MethodInfo* method){
	t1373  V_0 = {0};
	{
		t1373  L_0 = m7577(NULL, __this, &m7577_MI);
		V_0 = L_0;
		if (p0)
		{
			goto IL_0021;
		}
	}
	{
		t557 * L_1 = ((&V_0)->f3);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		t557 * L_2 = ((&V_0)->f3);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7548_MI, L_2);
		if (!L_3)
		{
			goto IL_0029;
		}
	}

IL_0021:
	{
		t557 * L_4 = ((&V_0)->f3);
		return L_4;
	}

IL_0029:
	{
		return (t557 *)NULL;
	}
}
extern MethodInfo m7581_MI;
 t42 * m7581 (t1374 * __this, MethodInfo* method){
	t1373  V_0 = {0};
	{
		t1373  L_0 = m7577(NULL, __this, &m7577_MI);
		V_0 = L_0;
		t42 * L_1 = ((&V_0)->f0);
		return L_1;
	}
}
 t42 * m7582 (t1374 * __this, MethodInfo* method){
	t1373  V_0 = {0};
	{
		t1373  L_0 = m7577(NULL, __this, &m7577_MI);
		V_0 = L_0;
		t42 * L_1 = ((&V_0)->f1);
		return L_1;
	}
}
 t7* m7583 (t1374 * __this, MethodInfo* method){
	t1373  V_0 = {0};
	{
		t1373  L_0 = m7577(NULL, __this, &m7577_MI);
		V_0 = L_0;
		t7* L_1 = ((&V_0)->f2);
		return L_1;
	}
}
 t7* m7584 (t1374 * __this, MethodInfo* method){
	{
		t42 * L_0 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7522_MI, __this);
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7583_MI, __this);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_2 = m1295(NULL, L_0, (t7*) &_stringLiteral79, L_1, &m1295_MI);
		return L_2;
	}
}
extern MethodInfo m7585_MI;
 bool m7585 (t1374 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		bool L_0 = m9320(NULL, __this, p0, p1, &m9320_MI);
		return L_0;
	}
}
extern MethodInfo m7586_MI;
 t316* m7586 (t1374 * __this, bool p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9319(NULL, __this, p0, &m9319_MI);
		return L_0;
	}
}
extern MethodInfo m7587_MI;
 t316* m7587 (t1374 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9318(NULL, __this, p0, p1, &m9318_MI);
		return L_0;
	}
}
extern MethodInfo m7588_MI;
 void m7588 (t1374 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		t7* L_0 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7583_MI, __this);
		t42 * L_1 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7582_MI, __this);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7584_MI, __this);
		m7537(NULL, p0, L_0, L_1, L_2, 2, &m7537_MI);
		return;
	}
}
// Metadata Definition System.Reflection.MonoEvent
extern Il2CppType t35_0_0_1;
FieldInfo t1374_f1_FieldInfo = 
{
	"klass", &t35_0_0_1, &t1374_TI, offsetof(t1374, f1), &EmptyCustomAttributesCache};
extern Il2CppType t35_0_0_1;
FieldInfo t1374_f2_FieldInfo = 
{
	"handle", &t35_0_0_1, &t1374_TI, offsetof(t1374, f2), &EmptyCustomAttributesCache};
static FieldInfo* t1374_FIs[] =
{
	&t1374_f1_FieldInfo,
	&t1374_f2_FieldInfo,
	NULL
};
static PropertyInfo t1374____Attributes_PropertyInfo = 
{
	&t1374_TI, "Attributes", &m7579_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1374____DeclaringType_PropertyInfo = 
{
	&t1374_TI, "DeclaringType", &m7581_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1374____ReflectedType_PropertyInfo = 
{
	&t1374_TI, "ReflectedType", &m7582_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1374____Name_PropertyInfo = 
{
	&t1374_TI, "Name", &m7583_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1374_PIs[] =
{
	&t1374____Attributes_PropertyInfo,
	&t1374____DeclaringType_PropertyInfo,
	&t1374____ReflectedType_PropertyInfo,
	&t1374____Name_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7578_MI = 
{
	".ctor", (methodPointerType)&m7578, &t1374_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3108, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1367_0_0_0;
extern void* RuntimeInvoker_t1367 (MethodInfo* method, void* obj, void** args);
MethodInfo m7579_MI = 
{
	"get_Attributes", (methodPointerType)&m7579, &t1374_TI, &t1367_0_0_0, RuntimeInvoker_t1367, NULL, &EmptyCustomAttributesCache, 2246, 0, 14, 0, false, false, 3109, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1374_m7580_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221617, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7580_MI = 
{
	"GetAddMethod", (methodPointerType)&m7580, &t1374_TI, &t557_0_0_0, RuntimeInvoker_t29_t297, t1374_m7580_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 16, 1, false, false, 3110, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7581_MI = 
{
	"get_DeclaringType", (methodPointerType)&m7581, &t1374_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 6, 0, false, false, 3111, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7582_MI = 
{
	"get_ReflectedType", (methodPointerType)&m7582, &t1374_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 9, 0, false, false, 3112, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7583_MI = 
{
	"get_Name", (methodPointerType)&m7583, &t1374_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 8, 0, false, false, 3113, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7584_MI = 
{
	"ToString", (methodPointerType)&m7584, &t1374_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 3114, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1374_m7585_ParameterInfos[] = 
{
	{"attributeType", 0, 134221618, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221619, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7585_MI = 
{
	"IsDefined", (methodPointerType)&m7585, &t1374_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t297, t1374_m7585_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 11, 2, false, false, 3115, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1374_m7586_ParameterInfos[] = 
{
	{"inherit", 0, 134221620, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7586_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m7586, &t1374_TI, &t316_0_0_0, RuntimeInvoker_t29_t297, t1374_m7586_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 12, 1, false, false, 3116, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1374_m7587_ParameterInfos[] = 
{
	{"attributeType", 0, 134221621, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221622, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7587_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m7587, &t1374_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t297, t1374_m7587_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 13, 2, false, false, 3117, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1374_m7588_ParameterInfos[] = 
{
	{"info", 0, 134221623, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221624, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7588_MI = 
{
	"GetObjectData", (methodPointerType)&m7588, &t1374_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1374_m7588_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 17, 2, false, false, 3118, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1374_MIs[] =
{
	&m7578_MI,
	&m7579_MI,
	&m7580_MI,
	&m7581_MI,
	&m7582_MI,
	&m7583_MI,
	&m7584_MI,
	&m7585_MI,
	&m7586_MI,
	&m7587_MI,
	&m7588_MI,
	NULL
};
static MethodInfo* t1374_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m7584_MI,
	&m7587_MI,
	&m7585_MI,
	&m7581_MI,
	&m7523_MI,
	&m7583_MI,
	&m7582_MI,
	&m6048_MI,
	&m7585_MI,
	&m7586_MI,
	&m7587_MI,
	&m7579_MI,
	&m7522_MI,
	&m7580_MI,
	&m7588_MI,
};
static TypeInfo* t1374_ITIs[] = 
{
	&t374_TI,
};
static Il2CppInterfaceOffsetPair t1374_IOs[] = 
{
	{ &t2023_TI, 14},
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
	{ &t374_TI, 17},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1374_1_0_0;
struct t1374;
TypeInfo t1374_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoEvent", "System.Reflection", t1374_MIs, t1374_PIs, t1374_FIs, NULL, &t1143_TI, NULL, NULL, &t1374_TI, t1374_ITIs, t1374_VT, &EmptyCustomAttributesCache, &t1374_TI, &t1374_0_0_0, &t1374_1_0_0, t1374_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1374), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, false, false, false, false, false, false, false, false, 11, 4, 2, 0, 0, 18, 1, 4};
#include "t1375.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1375_TI;
#include "t1375MD.h"

#include "t1389.h"
#include "t1646.h"
#include "t914.h"
extern TypeInfo t1389_TI;
extern TypeInfo t1646_TI;
extern TypeInfo t914_TI;
#include "t1389MD.h"
#include "t1646MD.h"
#include "t914MD.h"
extern MethodInfo m7593_MI;
extern MethodInfo m7721_MI;
extern MethodInfo m7607_MI;
extern MethodInfo m7601_MI;
extern MethodInfo m9252_MI;
extern MethodInfo m1387_MI;
extern MethodInfo m7604_MI;
extern MethodInfo m7596_MI;
extern MethodInfo m7594_MI;
extern MethodInfo m7603_MI;
extern MethodInfo m7595_MI;
extern MethodInfo m6038_MI;
extern MethodInfo m3964_MI;


extern MethodInfo m7589_MI;
 void m7589 (t1375 * __this, MethodInfo* method){
	{
		m7524(__this, &m7524_MI);
		return;
	}
}
extern MethodInfo m7590_MI;
 int32_t m7590 (t1375 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m7591_MI;
 t927  m7591 (t1375 * __this, MethodInfo* method){
	{
		t927  L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m7592_MI;
 t42 * m7592 (t1375 * __this, MethodInfo* method){
	{
		t42 * L_0 = (__this->f3);
		return L_0;
	}
}
 t42 * m7593 (t1375 * __this, bool p0, MethodInfo* method){
	typedef t42 * (*m7593_ftn) (t1375 *, bool);
	static m7593_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7593_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoField::GetParentType(System.Boolean)");
	return _il2cpp_icall_func(__this, p0);
}
 t42 * m7594 (t1375 * __this, MethodInfo* method){
	{
		t42 * L_0 = m7593(__this, 0, &m7593_MI);
		return L_0;
	}
}
 t42 * m7595 (t1375 * __this, MethodInfo* method){
	{
		t42 * L_0 = m7593(__this, 1, &m7593_MI);
		return L_0;
	}
}
 t7* m7596 (t1375 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m7597_MI;
 bool m7597 (t1375 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		bool L_0 = m9320(NULL, __this, p0, p1, &m9320_MI);
		return L_0;
	}
}
extern MethodInfo m7598_MI;
 t316* m7598 (t1375 * __this, bool p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9319(NULL, __this, p0, &m9319_MI);
		return L_0;
	}
}
extern MethodInfo m7599_MI;
 t316* m7599 (t1375 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9318(NULL, __this, p0, p1, &m9318_MI);
		return L_0;
	}
}
extern MethodInfo m7600_MI;
 int32_t m7600 (t1375 * __this, MethodInfo* method){
	typedef int32_t (*m7600_ftn) (t1375 *);
	static m7600_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7600_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoField::GetFieldOffset()");
	return _il2cpp_icall_func(__this);
}
 t29 * m7601 (t1375 * __this, t29 * p0, MethodInfo* method){
	typedef t29 * (*m7601_ftn) (t1375 *, t29 *);
	static m7601_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7601_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoField::GetValueInternal(System.Object)");
	return _il2cpp_icall_func(__this, p0);
}
extern MethodInfo m7602_MI;
 t29 * m7602 (t1375 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7527_MI, __this);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		if (p0)
		{
			goto IL_0016;
		}
	}
	{
		t1389 * L_1 = (t1389 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1389_TI));
		m7721(L_1, (t7*) &_stringLiteral1543, &m7721_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0016:
	{
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7526_MI, __this);
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		m7607(__this, &m7607_MI);
	}

IL_0024:
	{
		t29 * L_3 = m7601(__this, p0, &m7601_MI);
		return L_3;
	}
}
 t7* m7603 (t1375 * __this, MethodInfo* method){
	{
		t42 * L_0 = (__this->f3);
		t7* L_1 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_2 = m5610(NULL, (t7*) &_stringLiteral1544, L_0, L_1, &m5610_MI);
		return L_2;
	}
}
 void m7604 (t29 * __this, t1144 * p0, t29 * p1, t29 * p2, MethodInfo* method){
	typedef void (*m7604_ftn) (t1144 *, t29 *, t29 *);
	static m7604_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7604_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoField::SetValueInternal(System.Reflection.FieldInfo,System.Object,System.Object)");
	_il2cpp_icall_func(p0, p1, p2);
}
extern MethodInfo m7605_MI;
 void m7605 (t1375 * __this, t29 * p0, t29 * p1, int32_t p2, t631 * p3, t633 * p4, MethodInfo* method){
	t29 * V_0 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7527_MI, __this);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		if (p0)
		{
			goto IL_0016;
		}
	}
	{
		t1389 * L_1 = (t1389 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1389_TI));
		m7721(L_1, (t7*) &_stringLiteral1543, &m7721_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0016:
	{
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7526_MI, __this);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		t1646 * L_3 = (t1646 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1646_TI));
		m9252(L_3, (t7*) &_stringLiteral1545, &m9252_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		if (p3)
		{
			goto IL_0036;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		t631 * L_4 = m7510(NULL, &m7510_MI);
		p3 = L_4;
	}

IL_0036:
	{
		m7607(__this, &m7607_MI);
		if (!p1)
		{
			goto IL_0090;
		}
	}
	{
		t42 * L_5 = (__this->f3);
		t29 * L_6 = (t29 *)VirtFuncInvoker3< t29 *, t29 *, t42 *, t633 * >::Invoke(&m10146_MI, p3, p1, L_5, p4);
		V_0 = L_6;
		if (V_0)
		{
			goto IL_008b;
		}
	}
	{
		t316* L_7 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 4));
		ArrayElementTypeCheck (L_7, (t7*) &_stringLiteral1546);
		*((t29 **)(t29 **)SZArrayLdElema(L_7, 0)) = (t29 *)(t7*) &_stringLiteral1546;
		t316* L_8 = L_7;
		t42 * L_9 = m1430(p1, &m1430_MI);
		ArrayElementTypeCheck (L_8, L_9);
		*((t29 **)(t29 **)SZArrayLdElema(L_8, 1)) = (t29 *)L_9;
		t316* L_10 = L_8;
		ArrayElementTypeCheck (L_10, (t7*) &_stringLiteral1547);
		*((t29 **)(t29 **)SZArrayLdElema(L_10, 2)) = (t29 *)(t7*) &_stringLiteral1547;
		t316* L_11 = L_10;
		t42 * L_12 = (__this->f3);
		ArrayElementTypeCheck (L_11, L_12);
		*((t29 **)(t29 **)SZArrayLdElema(L_11, 3)) = (t29 *)L_12;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_13 = m1387(NULL, L_11, &m1387_MI);
		t305 * L_14 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_14, L_13, (t7*) &_stringLiteral1548, &m3973_MI);
		il2cpp_codegen_raise_exception(L_14);
	}

IL_008b:
	{
		p1 = V_0;
	}

IL_0090:
	{
		m7604(NULL, __this, p0, p1, &m7604_MI);
		return;
	}
}
extern MethodInfo m7606_MI;
 void m7606 (t1375 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		t7* L_0 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7596_MI, __this);
		t42 * L_1 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7594_MI, __this);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7603_MI, __this);
		m7537(NULL, p0, L_0, L_1, L_2, 4, &m7537_MI);
		return;
	}
}
 void m7607 (t1375 * __this, MethodInfo* method){
	{
		t42 * L_0 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7595_MI, __this);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6038_MI, L_0);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		t914 * L_2 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_2, (t7*) &_stringLiteral1549, &m3964_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0018:
	{
		return;
	}
}
// Metadata Definition System.Reflection.MonoField
extern Il2CppType t35_0_0_3;
FieldInfo t1375_f0_FieldInfo = 
{
	"klass", &t35_0_0_3, &t1375_TI, offsetof(t1375, f0), &EmptyCustomAttributesCache};
extern Il2CppType t927_0_0_3;
FieldInfo t1375_f1_FieldInfo = 
{
	"fhandle", &t927_0_0_3, &t1375_TI, offsetof(t1375, f1), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1375_f2_FieldInfo = 
{
	"name", &t7_0_0_1, &t1375_TI, offsetof(t1375, f2), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_1;
FieldInfo t1375_f3_FieldInfo = 
{
	"type", &t42_0_0_1, &t1375_TI, offsetof(t1375, f3), &EmptyCustomAttributesCache};
extern Il2CppType t1347_0_0_1;
FieldInfo t1375_f4_FieldInfo = 
{
	"attrs", &t1347_0_0_1, &t1375_TI, offsetof(t1375, f4), &EmptyCustomAttributesCache};
static FieldInfo* t1375_FIs[] =
{
	&t1375_f0_FieldInfo,
	&t1375_f1_FieldInfo,
	&t1375_f2_FieldInfo,
	&t1375_f3_FieldInfo,
	&t1375_f4_FieldInfo,
	NULL
};
static PropertyInfo t1375____Attributes_PropertyInfo = 
{
	&t1375_TI, "Attributes", &m7590_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1375____FieldHandle_PropertyInfo = 
{
	&t1375_TI, "FieldHandle", &m7591_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1375____FieldType_PropertyInfo = 
{
	&t1375_TI, "FieldType", &m7592_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1375____ReflectedType_PropertyInfo = 
{
	&t1375_TI, "ReflectedType", &m7594_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1375____DeclaringType_PropertyInfo = 
{
	&t1375_TI, "DeclaringType", &m7595_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1375____Name_PropertyInfo = 
{
	&t1375_TI, "Name", &m7596_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1375_PIs[] =
{
	&t1375____Attributes_PropertyInfo,
	&t1375____FieldHandle_PropertyInfo,
	&t1375____FieldType_PropertyInfo,
	&t1375____ReflectedType_PropertyInfo,
	&t1375____DeclaringType_PropertyInfo,
	&t1375____Name_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7589_MI = 
{
	".ctor", (methodPointerType)&m7589, &t1375_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3119, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1347_0_0_0;
extern void* RuntimeInvoker_t1347 (MethodInfo* method, void* obj, void** args);
MethodInfo m7590_MI = 
{
	"get_Attributes", (methodPointerType)&m7590, &t1375_TI, &t1347_0_0_0, RuntimeInvoker_t1347, NULL, &EmptyCustomAttributesCache, 2246, 0, 14, 0, false, false, 3120, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t927_0_0_0;
extern void* RuntimeInvoker_t927 (MethodInfo* method, void* obj, void** args);
MethodInfo m7591_MI = 
{
	"get_FieldHandle", (methodPointerType)&m7591, &t1375_TI, &t927_0_0_0, RuntimeInvoker_t927, NULL, &EmptyCustomAttributesCache, 2246, 0, 15, 0, false, false, 3121, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7592_MI = 
{
	"get_FieldType", (methodPointerType)&m7592, &t1375_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 16, 0, false, false, 3122, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1375_m7593_ParameterInfos[] = 
{
	{"declaring", 0, 134221625, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7593_MI = 
{
	"GetParentType", (methodPointerType)&m7593, &t1375_TI, &t42_0_0_0, RuntimeInvoker_t29_t297, t1375_m7593_ParameterInfos, &EmptyCustomAttributesCache, 129, 4096, 255, 1, false, false, 3123, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7594_MI = 
{
	"get_ReflectedType", (methodPointerType)&m7594, &t1375_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 9, 0, false, false, 3124, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7595_MI = 
{
	"get_DeclaringType", (methodPointerType)&m7595, &t1375_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 6, 0, false, false, 3125, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7596_MI = 
{
	"get_Name", (methodPointerType)&m7596, &t1375_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 8, 0, false, false, 3126, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1375_m7597_ParameterInfos[] = 
{
	{"attributeType", 0, 134221626, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221627, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7597_MI = 
{
	"IsDefined", (methodPointerType)&m7597, &t1375_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t297, t1375_m7597_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 11, 2, false, false, 3127, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1375_m7598_ParameterInfos[] = 
{
	{"inherit", 0, 134221628, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7598_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m7598, &t1375_TI, &t316_0_0_0, RuntimeInvoker_t29_t297, t1375_m7598_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 12, 1, false, false, 3128, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1375_m7599_ParameterInfos[] = 
{
	{"attributeType", 0, 134221629, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221630, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7599_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m7599, &t1375_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t297, t1375_m7599_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 13, 2, false, false, 3129, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7600_MI = 
{
	"GetFieldOffset", (methodPointerType)&m7600, &t1375_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 195, 4096, 23, 0, false, false, 3130, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1375_m7601_ParameterInfos[] = 
{
	{"obj", 0, 134221631, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7601_MI = 
{
	"GetValueInternal", (methodPointerType)&m7601, &t1375_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1375_m7601_ParameterInfos, &EmptyCustomAttributesCache, 129, 4096, 255, 1, false, false, 3131, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1375_m7602_ParameterInfos[] = 
{
	{"obj", 0, 134221632, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7602_MI = 
{
	"GetValue", (methodPointerType)&m7602, &t1375_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1375_m7602_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 17, 1, false, false, 3132, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7603_MI = 
{
	"ToString", (methodPointerType)&m7603, &t1375_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 3133, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1144_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1375_m7604_ParameterInfos[] = 
{
	{"fi", 0, 134221633, &EmptyCustomAttributesCache, &t1144_0_0_0},
	{"obj", 1, 134221634, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 2, 134221635, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7604_MI = 
{
	"SetValueInternal", (methodPointerType)&m7604, &t1375_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t1375_m7604_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 3, false, false, 3134, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t1375_m7605_ParameterInfos[] = 
{
	{"obj", 0, 134221636, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"val", 1, 134221637, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"invokeAttr", 2, 134221638, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 3, 134221639, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"culture", 4, 134221640, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7605_MI = 
{
	"SetValue", (methodPointerType)&m7605, &t1375_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t44_t29_t29, t1375_m7605_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 21, 5, false, false, 3135, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1375_m7606_ParameterInfos[] = 
{
	{"info", 0, 134221641, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221642, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7606_MI = 
{
	"GetObjectData", (methodPointerType)&m7606, &t1375_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1375_m7606_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, false, 3136, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7607_MI = 
{
	"CheckGeneric", (methodPointerType)&m7607, &t1375_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 3137, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1375_MIs[] =
{
	&m7589_MI,
	&m7590_MI,
	&m7591_MI,
	&m7592_MI,
	&m7593_MI,
	&m7594_MI,
	&m7595_MI,
	&m7596_MI,
	&m7597_MI,
	&m7598_MI,
	&m7599_MI,
	&m7600_MI,
	&m7601_MI,
	&m7602_MI,
	&m7603_MI,
	&m7604_MI,
	&m7605_MI,
	&m7606_MI,
	&m7607_MI,
	NULL
};
static MethodInfo* t1375_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m7603_MI,
	&m7599_MI,
	&m7597_MI,
	&m7595_MI,
	&m7525_MI,
	&m7596_MI,
	&m7594_MI,
	&m6048_MI,
	&m7597_MI,
	&m7598_MI,
	&m7599_MI,
	&m7590_MI,
	&m7591_MI,
	&m7592_MI,
	&m7602_MI,
	&m7526_MI,
	&m7527_MI,
	&m7528_MI,
	&m7605_MI,
	&m7529_MI,
	&m7600_MI,
	&m7534_MI,
	&m7606_MI,
};
static TypeInfo* t1375_ITIs[] = 
{
	&t374_TI,
};
static Il2CppInterfaceOffsetPair t1375_IOs[] = 
{
	{ &t2013_TI, 14},
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
	{ &t374_TI, 25},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1375_0_0_0;
extern Il2CppType t1375_1_0_0;
struct t1375;
TypeInfo t1375_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoField", "System.Reflection", t1375_MIs, t1375_PIs, t1375_FIs, NULL, &t1144_TI, NULL, NULL, &t1375_TI, t1375_ITIs, t1375_VT, &EmptyCustomAttributesCache, &t1375_TI, &t1375_0_0_0, &t1375_1_0_0, t1375_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1375), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 19, 6, 5, 0, 0, 26, 1, 4};
#include "t1376.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1376_TI;
#include "t1376MD.h"

#include "t1377MD.h"
extern MethodInfo m7620_MI;
extern MethodInfo m3974_MI;


extern MethodInfo m7608_MI;
 void m7608 (t1376 * __this, MethodInfo* method){
	{
		m7620(__this, &m7620_MI);
		t914 * L_0 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_0, &m3974_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m7609_MI;
 t42 * m7609 (t1376 * __this, MethodInfo* method){
	typedef t42 * (*m7609_ftn) (t1376 *);
	static m7609_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7609_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoGenericMethod::get_ReflectedType()");
	return _il2cpp_icall_func(__this);
}
// Metadata Definition System.Reflection.MonoGenericMethod
static PropertyInfo t1376____ReflectedType_PropertyInfo = 
{
	&t1376_TI, "ReflectedType", &m7609_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1376_PIs[] =
{
	&t1376____ReflectedType_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7608_MI = 
{
	".ctor", (methodPointerType)&m7608, &t1376_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6275, 0, 255, 0, false, false, 3138, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7609_MI = 
{
	"get_ReflectedType", (methodPointerType)&m7609, &t1376_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 4096, 9, 0, false, false, 3139, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1376_MIs[] =
{
	&m7608_MI,
	&m7609_MI,
	NULL
};
extern MethodInfo m7640_MI;
extern MethodInfo m7636_MI;
extern MethodInfo m7634_MI;
extern MethodInfo m7632_MI;
extern MethodInfo m7633_MI;
extern MethodInfo m7635_MI;
extern MethodInfo m7625_MI;
extern MethodInfo m7627_MI;
extern MethodInfo m7628_MI;
extern MethodInfo m7629_MI;
extern MethodInfo m7630_MI;
extern MethodInfo m7644_MI;
extern MethodInfo m7647_MI;
extern MethodInfo m7645_MI;
extern MethodInfo m7646_MI;
extern MethodInfo m7623_MI;
extern MethodInfo m7624_MI;
extern MethodInfo m7642_MI;
extern MethodInfo m7641_MI;
static MethodInfo* t1376_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m7640_MI,
	&m7636_MI,
	&m7634_MI,
	&m7632_MI,
	&m7555_MI,
	&m7633_MI,
	&m7609_MI,
	&m6048_MI,
	&m7634_MI,
	&m7635_MI,
	&m7636_MI,
	&m7625_MI,
	&m7546_MI,
	&m7627_MI,
	&m7628_MI,
	&m7629_MI,
	&m7630_MI,
	&m7548_MI,
	&m2952_MI,
	&m7549_MI,
	&m7644_MI,
	&m7647_MI,
	&m7645_MI,
	&m7646_MI,
	&m7623_MI,
	&m7624_MI,
	&m7642_MI,
	&m7641_MI,
};
static Il2CppInterfaceOffsetPair t1376_IOs[] = 
{
	{ &t374_TI, 30},
	{ &t2015_TI, 27},
	{ &t2010_TI, 14},
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1376_0_0_0;
extern Il2CppType t1376_1_0_0;
extern TypeInfo t1377_TI;
struct t1376;
TypeInfo t1376_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoGenericMethod", "System.Reflection", t1376_MIs, t1376_PIs, NULL, NULL, &t1377_TI, NULL, NULL, &t1376_TI, NULL, t1376_VT, &EmptyCustomAttributesCache, &t1376_TI, &t1376_0_0_0, &t1376_1_0_0, t1376_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1376), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 1, 0, 0, 0, 31, 0, 5};
#include "t1378.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1378_TI;
#include "t1378MD.h"

#include "t1379MD.h"
extern MethodInfo m7648_MI;


extern MethodInfo m7610_MI;
 void m7610 (t1378 * __this, MethodInfo* method){
	{
		m7648(__this, &m7648_MI);
		t914 * L_0 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_0, &m3974_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m7611_MI;
 t42 * m7611 (t1378 * __this, MethodInfo* method){
	typedef t42 * (*m7611_ftn) (t1378 *);
	static m7611_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7611_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoGenericCMethod::get_ReflectedType()");
	return _il2cpp_icall_func(__this);
}
// Metadata Definition System.Reflection.MonoGenericCMethod
static PropertyInfo t1378____ReflectedType_PropertyInfo = 
{
	&t1378_TI, "ReflectedType", &m7611_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1378_PIs[] =
{
	&t1378____ReflectedType_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7610_MI = 
{
	".ctor", (methodPointerType)&m7610, &t1378_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6275, 0, 255, 0, false, false, 3140, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7611_MI = 
{
	"get_ReflectedType", (methodPointerType)&m7611, &t1378_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 4096, 9, 0, false, false, 3141, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1378_MIs[] =
{
	&m7610_MI,
	&m7611_MI,
	NULL
};
extern MethodInfo m7662_MI;
extern MethodInfo m7661_MI;
extern MethodInfo m7659_MI;
extern MethodInfo m7657_MI;
extern MethodInfo m7658_MI;
extern MethodInfo m7660_MI;
extern MethodInfo m7649_MI;
extern MethodInfo m7651_MI;
extern MethodInfo m7653_MI;
extern MethodInfo m7654_MI;
extern MethodInfo m7655_MI;
extern MethodInfo m7652_MI;
extern MethodInfo m7663_MI;
static MethodInfo* t1378_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m7662_MI,
	&m7661_MI,
	&m7659_MI,
	&m7657_MI,
	&m7516_MI,
	&m7658_MI,
	&m7611_MI,
	&m6048_MI,
	&m7659_MI,
	&m7660_MI,
	&m7661_MI,
	&m7649_MI,
	&m7546_MI,
	&m7651_MI,
	&m7653_MI,
	&m7654_MI,
	&m7655_MI,
	&m7548_MI,
	&m2952_MI,
	&m7549_MI,
	&m7550_MI,
	&m7551_MI,
	&m7552_MI,
	&m7553_MI,
	&m7652_MI,
	&m7663_MI,
};
static Il2CppInterfaceOffsetPair t1378_IOs[] = 
{
	{ &t374_TI, 28},
	{ &t2009_TI, 27},
	{ &t2010_TI, 14},
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1378_0_0_0;
extern Il2CppType t1378_1_0_0;
extern TypeInfo t1379_TI;
struct t1378;
TypeInfo t1378_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoGenericCMethod", "System.Reflection", t1378_MIs, t1378_PIs, NULL, NULL, &t1379_TI, NULL, NULL, &t1378_TI, NULL, t1378_VT, &EmptyCustomAttributesCache, &t1378_TI, &t1378_0_0_0, &t1378_1_0_0, t1378_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1378), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 1, 0, 0, 0, 29, 0, 5};
#include "t1380.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1380_TI;
#include "t1380MD.h"

extern MethodInfo m7612_MI;
extern MethodInfo m7613_MI;
extern MethodInfo m7618_MI;


 void m7612 (t29 * __this, t35 p0, t1380 * p1, MethodInfo* method){
	typedef void (*m7612_ftn) (t35, t1380 *);
	static m7612_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7612_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoMethodInfo::get_method_info(System.IntPtr,System.Reflection.MonoMethodInfo&)");
	_il2cpp_icall_func(p0, p1);
}
 t1380  m7613 (t29 * __this, t35 p0, MethodInfo* method){
	t1380  V_0 = {0};
	{
		m7612(NULL, p0, (&V_0), &m7612_MI);
		return V_0;
	}
}
extern MethodInfo m7614_MI;
 t42 * m7614 (t29 * __this, t35 p0, MethodInfo* method){
	t1380  V_0 = {0};
	{
		t1380  L_0 = m7613(NULL, p0, &m7613_MI);
		V_0 = L_0;
		t42 * L_1 = ((&V_0)->f0);
		return L_1;
	}
}
extern MethodInfo m7615_MI;
 t42 * m7615 (t29 * __this, t35 p0, MethodInfo* method){
	t1380  V_0 = {0};
	{
		t1380  L_0 = m7613(NULL, p0, &m7613_MI);
		V_0 = L_0;
		t42 * L_1 = ((&V_0)->f1);
		return L_1;
	}
}
extern MethodInfo m7616_MI;
 int32_t m7616 (t29 * __this, t35 p0, MethodInfo* method){
	t1380  V_0 = {0};
	{
		t1380  L_0 = m7613(NULL, p0, &m7613_MI);
		V_0 = L_0;
		int32_t L_1 = ((&V_0)->f2);
		return L_1;
	}
}
extern MethodInfo m7617_MI;
 int32_t m7617 (t29 * __this, t35 p0, MethodInfo* method){
	t1380  V_0 = {0};
	{
		t1380  L_0 = m7613(NULL, p0, &m7613_MI);
		V_0 = L_0;
		int32_t L_1 = ((&V_0)->f4);
		return L_1;
	}
}
 t638* m7618 (t29 * __this, t35 p0, t296 * p1, MethodInfo* method){
	typedef t638* (*m7618_ftn) (t35, t296 *);
	static m7618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7618_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoMethodInfo::get_parameter_info(System.IntPtr,System.Reflection.MemberInfo)");
	return _il2cpp_icall_func(p0, p1);
}
extern MethodInfo m7619_MI;
 t638* m7619 (t29 * __this, t35 p0, t296 * p1, MethodInfo* method){
	{
		t638* L_0 = m7618(NULL, p0, p1, &m7618_MI);
		return L_0;
	}
}
// Metadata Definition System.Reflection.MonoMethodInfo
extern Il2CppType t42_0_0_1;
FieldInfo t1380_f0_FieldInfo = 
{
	"parent", &t42_0_0_1, &t1380_TI, offsetof(t1380, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_1;
FieldInfo t1380_f1_FieldInfo = 
{
	"ret", &t42_0_0_1, &t1380_TI, offsetof(t1380, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1342_0_0_3;
FieldInfo t1380_f2_FieldInfo = 
{
	"attrs", &t1342_0_0_3, &t1380_TI, offsetof(t1380, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1370_0_0_3;
FieldInfo t1380_f3_FieldInfo = 
{
	"iattrs", &t1370_0_0_3, &t1380_TI, offsetof(t1380, f3) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1150_0_0_1;
FieldInfo t1380_f4_FieldInfo = 
{
	"callconv", &t1150_0_0_1, &t1380_TI, offsetof(t1380, f4) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t1380_FIs[] =
{
	&t1380_f0_FieldInfo,
	&t1380_f1_FieldInfo,
	&t1380_f2_FieldInfo,
	&t1380_f3_FieldInfo,
	&t1380_f4_FieldInfo,
	NULL
};
extern Il2CppType t35_0_0_0;
extern Il2CppType t1380_1_0_2;
extern Il2CppType t1380_1_0_0;
static ParameterInfo t1380_m7612_ParameterInfos[] = 
{
	{"handle", 0, 134221643, &EmptyCustomAttributesCache, &t35_0_0_0},
	{"info", 1, 134221644, &EmptyCustomAttributesCache, &t1380_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t35_t2026 (MethodInfo* method, void* obj, void** args);
MethodInfo m7612_MI = 
{
	"get_method_info", (methodPointerType)&m7612, &t1380_TI, &t21_0_0_0, RuntimeInvoker_t21_t35_t2026, t1380_m7612_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 2, false, false, 3142, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
static ParameterInfo t1380_m7613_ParameterInfos[] = 
{
	{"handle", 0, 134221645, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t1380_0_0_0;
extern void* RuntimeInvoker_t1380_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m7613_MI = 
{
	"GetMethodInfo", (methodPointerType)&m7613, &t1380_TI, &t1380_0_0_0, RuntimeInvoker_t1380_t35, t1380_m7613_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 3143, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
static ParameterInfo t1380_m7614_ParameterInfos[] = 
{
	{"handle", 0, 134221646, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m7614_MI = 
{
	"GetDeclaringType", (methodPointerType)&m7614, &t1380_TI, &t42_0_0_0, RuntimeInvoker_t29_t35, t1380_m7614_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 3144, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
static ParameterInfo t1380_m7615_ParameterInfos[] = 
{
	{"handle", 0, 134221647, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m7615_MI = 
{
	"GetReturnType", (methodPointerType)&m7615, &t1380_TI, &t42_0_0_0, RuntimeInvoker_t29_t35, t1380_m7615_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 3145, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
static ParameterInfo t1380_m7616_ParameterInfos[] = 
{
	{"handle", 0, 134221648, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t1342_0_0_0;
extern void* RuntimeInvoker_t1342_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m7616_MI = 
{
	"GetAttributes", (methodPointerType)&m7616, &t1380_TI, &t1342_0_0_0, RuntimeInvoker_t1342_t35, t1380_m7616_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 3146, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
static ParameterInfo t1380_m7617_ParameterInfos[] = 
{
	{"handle", 0, 134221649, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t1150_0_0_0;
extern void* RuntimeInvoker_t1150_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m7617_MI = 
{
	"GetCallingConvention", (methodPointerType)&m7617, &t1380_TI, &t1150_0_0_0, RuntimeInvoker_t1150_t35, t1380_m7617_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, false, 3147, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
extern Il2CppType t296_0_0_0;
extern Il2CppType t296_0_0_0;
static ParameterInfo t1380_m7618_ParameterInfos[] = 
{
	{"handle", 0, 134221650, &EmptyCustomAttributesCache, &t35_0_0_0},
	{"member", 1, 134221651, &EmptyCustomAttributesCache, &t296_0_0_0},
};
extern Il2CppType t638_0_0_0;
extern void* RuntimeInvoker_t29_t35_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7618_MI = 
{
	"get_parameter_info", (methodPointerType)&m7618, &t1380_TI, &t638_0_0_0, RuntimeInvoker_t29_t35_t29, t1380_m7618_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 2, false, false, 3148, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
extern Il2CppType t296_0_0_0;
static ParameterInfo t1380_m7619_ParameterInfos[] = 
{
	{"handle", 0, 134221652, &EmptyCustomAttributesCache, &t35_0_0_0},
	{"member", 1, 134221653, &EmptyCustomAttributesCache, &t296_0_0_0},
};
extern Il2CppType t638_0_0_0;
extern void* RuntimeInvoker_t29_t35_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7619_MI = 
{
	"GetParametersInfo", (methodPointerType)&m7619, &t1380_TI, &t638_0_0_0, RuntimeInvoker_t29_t35_t29, t1380_m7619_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 2, false, false, 3149, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1380_MIs[] =
{
	&m7612_MI,
	&m7613_MI,
	&m7614_MI,
	&m7615_MI,
	&m7616_MI,
	&m7617_MI,
	&m7618_MI,
	&m7619_MI,
	NULL
};
static MethodInfo* t1380_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1380_0_0_0;
TypeInfo t1380_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoMethodInfo", "System.Reflection", t1380_MIs, NULL, t1380_FIs, NULL, &t110_TI, NULL, NULL, &t1380_TI, NULL, t1380_VT, &EmptyCustomAttributesCache, &t1380_TI, &t1380_0_0_0, &t1380_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1380)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048840, 0, true, false, false, false, false, false, false, false, false, false, false, false, 8, 0, 5, 0, 0, 4, 0, 0};
#include "t1377.h"
#ifndef _MSC_VER
#else
#endif

#include "t295.h"
#include "t1390.h"
#include "t1152.h"
#include "t1412.h"
#include "t292.h"
extern TypeInfo t1343_TI;
extern TypeInfo t20_TI;
extern TypeInfo t1390_TI;
extern TypeInfo t1606_TI;
extern TypeInfo t1650_TI;
extern TypeInfo t295_TI;
extern TypeInfo t1412_TI;
extern TypeInfo t1152_TI;
extern TypeInfo t292_TI;
#include "t1390MD.h"
#include "t1412MD.h"
#include "t292MD.h"
extern MethodInfo m9497_MI;
extern MethodInfo m7621_MI;
extern MethodInfo m7622_MI;
extern MethodInfo m4199_MI;
extern MethodInfo m7726_MI;
extern MethodInfo m7626_MI;
extern MethodInfo m7723_MI;
extern MethodInfo m7752_MI;
extern MethodInfo m7637_MI;
extern MethodInfo m5995_MI;
extern MethodInfo m3000_MI;
extern MethodInfo m6045_MI;
extern MethodInfo m1311_MI;
extern MethodInfo m7639_MI;
extern MethodInfo m2927_MI;
extern MethodInfo m1315_MI;
extern MethodInfo m7631_MI;
extern MethodInfo m8858_MI;
extern MethodInfo m7643_MI;


 void m7620 (t1377 * __this, MethodInfo* method){
	{
		m7554(__this, &m7554_MI);
		return;
	}
}
 t7* m7621 (t29 * __this, t636 * p0, MethodInfo* method){
	typedef t7* (*m7621_ftn) (t636 *);
	static m7621_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7621_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoMethod::get_name(System.Reflection.MethodBase)");
	return _il2cpp_icall_func(p0);
}
 t1377 * m7622 (t29 * __this, t1377 * p0, MethodInfo* method){
	typedef t1377 * (*m7622_ftn) (t1377 *);
	static m7622_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7622_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoMethod::get_base_definition(System.Reflection.MonoMethod)");
	return _il2cpp_icall_func(p0);
}
 t557 * m7623 (t1377 * __this, MethodInfo* method){
	{
		t1377 * L_0 = m7622(NULL, __this, &m7622_MI);
		return L_0;
	}
}
 t42 * m7624 (t1377 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f0);
		t42 * L_1 = m7615(NULL, L_0, &m7615_MI);
		return L_1;
	}
}
 t638* m7625 (t1377 * __this, MethodInfo* method){
	t638* V_0 = {0};
	t638* V_1 = {0};
	{
		t35 L_0 = (__this->f0);
		t638* L_1 = m7619(NULL, L_0, __this, &m7619_MI);
		V_0 = L_1;
		V_1 = ((t638*)SZArrayNew(InitializedTypeInfo(&t638_TI), (((int32_t)(((t20 *)V_0)->max_length)))));
		VirtActionInvoker2< t20 *, int32_t >::Invoke(&m4199_MI, V_0, (t20 *)(t20 *)V_1, 0);
		return V_1;
	}
}
 t29 * m7626 (t1377 * __this, t29 * p0, t316* p1, t295 ** p2, MethodInfo* method){
	typedef t29 * (*m7626_ftn) (t1377 *, t29 *, t316*, t295 **);
	static m7626_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7626_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoMethod::InternalInvoke(System.Object,System.Object[],System.Exception&)");
	return _il2cpp_icall_func(__this, p0, p1, p2);
}
 t29 * m7627 (t1377 * __this, t29 * p0, int32_t p1, t631 * p2, t316* p3, t633 * p4, MethodInfo* method){
	t638* V_0 = {0};
	int32_t V_1 = 0;
	t295 * V_2 = {0};
	t29 * V_3 = {0};
	t295 * V_4 = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		if (p2)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		t631 * L_0 = m7510(NULL, &m7510_MI);
		p2 = L_0;
	}

IL_000c:
	{
		t35 L_1 = (__this->f0);
		t638* L_2 = m7619(NULL, L_1, __this, &m7619_MI);
		V_0 = L_2;
		if (p3)
		{
			goto IL_0022;
		}
	}
	{
		if ((((int32_t)(((t20 *)V_0)->max_length))))
		{
			goto IL_002f;
		}
	}

IL_0022:
	{
		if (!p3)
		{
			goto IL_003a;
		}
	}
	{
		if ((((int32_t)(((int32_t)(((t20 *)p3)->max_length)))) == ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_003a;
		}
	}

IL_002f:
	{
		t1391 * L_3 = (t1391 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1391_TI));
		m7726(L_3, (t7*) &_stringLiteral1550, &m7726_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_003a:
	{
		if (((int32_t)((int32_t)p1&(int32_t)((int32_t)65536))))
		{
			goto IL_005d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		bool L_4 = m7511(NULL, p2, p3, V_0, p4, &m7511_MI);
		if (L_4)
		{
			goto IL_005b;
		}
	}
	{
		t305 * L_5 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_5, (t7*) &_stringLiteral1551, &m1935_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_005b:
	{
		goto IL_0089;
	}

IL_005d:
	{
		V_1 = 0;
		goto IL_0083;
	}

IL_0061:
	{
		int32_t L_6 = V_1;
		t42 * L_7 = m1430((*(t29 **)(t29 **)SZArrayLdElema(p3, L_6)), &m1430_MI);
		int32_t L_8 = V_1;
		t42 * L_9 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_0, L_8)));
		if ((((t42 *)L_7) == ((t42 *)L_9)))
		{
			goto IL_007f;
		}
	}
	{
		t305 * L_10 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_10, (t7*) &_stringLiteral1550, &m1935_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_007f:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0083:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_0061;
		}
	}

IL_0089:
	{
		bool L_11 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7647_MI, __this);
		if (!L_11)
		{
			goto IL_009c;
		}
	}
	{
		t914 * L_12 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_12, (t7*) &_stringLiteral1552, &m3964_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_009c:
	{
		V_3 = NULL;
	}

IL_009e:
	try
	{ // begin try (depth: 1)
		t29 * L_13 = m7626(__this, p0, p3, (&V_2), &m7626_MI);
		V_3 = L_13;
		// IL_00aa: leave.s IL_00c2
		goto IL_00c2;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t1606_TI, e.ex->object.klass))
			goto IL_00ac;
		if(il2cpp_codegen_class_is_assignable_from (&t1650_TI, e.ex->object.klass))
			goto IL_00b1;
		if(il2cpp_codegen_class_is_assignable_from (&t295_TI, e.ex->object.klass))
			goto IL_00b6;
		throw e;
	}

IL_00ac:
	{ // begin catch(System.Threading.ThreadAbortException)
		il2cpp_codegen_raise_exception(__exception_local);
		// IL_00af: leave.s IL_00c2
		goto IL_00c2;
	} // end catch (depth: 1)

IL_00b1:
	{ // begin catch(System.MethodAccessException)
		il2cpp_codegen_raise_exception(__exception_local);
		// IL_00b4: leave.s IL_00c2
		goto IL_00c2;
	} // end catch (depth: 1)

IL_00b6:
	{ // begin catch(System.Exception)
		V_4 = ((t295 *)__exception_local);
		t1390 * L_14 = (t1390 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1390_TI));
		m7723(L_14, V_4, &m7723_MI);
		il2cpp_codegen_raise_exception(L_14);
		// IL_00c0: leave.s IL_00c2
		goto IL_00c2;
	} // end catch (depth: 1)

IL_00c2:
	{
		if (!V_2)
		{
			goto IL_00c7;
		}
	}
	{
		il2cpp_codegen_raise_exception(V_2);
	}

IL_00c7:
	{
		return V_3;
	}
}
 t1343  m7628 (t1377 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f0);
		t1343  L_1 = {0};
		m9497(&L_1, L_0, &m9497_MI);
		return L_1;
	}
}
 int32_t m7629 (t1377 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f0);
		int32_t L_1 = m7616(NULL, L_0, &m7616_MI);
		return L_1;
	}
}
 int32_t m7630 (t1377 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f0);
		int32_t L_1 = m7617(NULL, L_0, &m7617_MI);
		return L_1;
	}
}
 t42 * m7631 (t1377 * __this, MethodInfo* method){
	{
		t42 * L_0 = (__this->f2);
		return L_0;
	}
}
 t42 * m7632 (t1377 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f0);
		t42 * L_1 = m7614(NULL, L_0, &m7614_MI);
		return L_1;
	}
}
 t7* m7633 (t1377 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f1);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		t7* L_1 = (__this->f1);
		return L_1;
	}

IL_000f:
	{
		t7* L_2 = m7621(NULL, __this, &m7621_MI);
		return L_2;
	}
}
 bool m7634 (t1377 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		bool L_0 = m9320(NULL, __this, p0, p1, &m9320_MI);
		return L_0;
	}
}
 t316* m7635 (t1377 * __this, bool p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9319(NULL, __this, p0, &m9319_MI);
		return L_0;
	}
}
 t316* m7636 (t1377 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9318(NULL, __this, p0, p1, &m9318_MI);
		return L_0;
	}
}
 t1152 * m7637 (t29 * __this, t35 p0, MethodInfo* method){
	typedef t1152 * (*m7637_ftn) (t35);
	static m7637_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7637_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoMethod::GetDllImportAttribute(System.IntPtr)");
	return _il2cpp_icall_func(p0);
}
extern MethodInfo m7638_MI;
 t316* m7638 (t1377 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t1380  V_1 = {0};
	t316* V_2 = {0};
	t1152 * V_3 = {0};
	{
		V_0 = 0;
		t35 L_0 = (__this->f0);
		t1380  L_1 = m7613(NULL, L_0, &m7613_MI);
		V_1 = L_1;
		int32_t L_2 = ((&V_1)->f3);
		if (!((int32_t)((int32_t)L_2&(int32_t)((int32_t)128))))
		{
			goto IL_0021;
		}
	}
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0021:
	{
		int32_t L_3 = ((&V_1)->f2);
		if (!((int32_t)((int32_t)L_3&(int32_t)((int32_t)8192))))
		{
			goto IL_0034;
		}
	}
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0034:
	{
		if (V_0)
		{
			goto IL_0039;
		}
	}
	{
		return (t316*)NULL;
	}

IL_0039:
	{
		V_2 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), V_0));
		V_0 = 0;
		int32_t L_4 = ((&V_1)->f3);
		if (!((int32_t)((int32_t)L_4&(int32_t)((int32_t)128))))
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)(L_5+1));
		t1412 * L_6 = (t1412 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1412_TI));
		m7752(L_6, &m7752_MI);
		ArrayElementTypeCheck (V_2, L_6);
		*((t29 **)(t29 **)SZArrayLdElema(V_2, L_5)) = (t29 *)L_6;
	}

IL_005d:
	{
		int32_t L_7 = ((&V_1)->f2);
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)8192))))
		{
			goto IL_0096;
		}
	}
	{
		t35 L_8 = (__this->f0);
		t1152 * L_9 = m7637(NULL, L_8, &m7637_MI);
		V_3 = L_9;
		int32_t L_10 = ((&V_1)->f3);
		if (!((int32_t)((int32_t)L_10&(int32_t)((int32_t)128))))
		{
			goto IL_008e;
		}
	}
	{
		V_3->f5 = 1;
	}

IL_008e:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)(L_11+1));
		ArrayElementTypeCheck (V_2, V_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_2, L_11)) = (t29 *)V_3;
	}

IL_0096:
	{
		return V_2;
	}
}
 bool m7639 (t29 * __this, t42 * p0, MethodInfo* method){
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5995_MI, p0);
		if (!L_0)
		{
			goto IL_0033;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6001_MI, p0);
		if (!L_1)
		{
			goto IL_0030;
		}
	}
	{
		t42 * L_2 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, p0);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&m3000_MI, L_2);
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		t42 * L_4 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, p0);
		bool L_5 = m6045(L_4, &m6045_MI);
		G_B5_0 = ((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
		goto IL_002e;
	}

IL_002d:
	{
		G_B5_0 = 0;
	}

IL_002e:
	{
		G_B7_0 = G_B5_0;
		goto IL_0031;
	}

IL_0030:
	{
		G_B7_0 = 1;
	}

IL_0031:
	{
		G_B9_0 = G_B7_0;
		goto IL_0034;
	}

IL_0033:
	{
		G_B9_0 = 0;
	}

IL_0034:
	{
		return G_B9_0;
	}
}
 t7* m7640 (t1377 * __this, MethodInfo* method){
	t292 * V_0 = {0};
	t42 * V_1 = {0};
	t537* V_2 = {0};
	int32_t V_3 = 0;
	t638* V_4 = {0};
	int32_t V_5 = 0;
	t42 * V_6 = {0};
	bool V_7 = false;
	{
		t292 * L_0 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m1311(L_0, &m1311_MI);
		V_0 = L_0;
		t42 * L_1 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7624_MI, __this);
		V_1 = L_1;
		bool L_2 = m7639(NULL, V_1, &m7639_MI);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, V_1);
		m2927(V_0, L_3, &m2927_MI);
		goto IL_0031;
	}

IL_0024:
	{
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, V_1);
		m2927(V_0, L_4, &m2927_MI);
	}

IL_0031:
	{
		m2927(V_0, (t7*) &_stringLiteral79, &m2927_MI);
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7633_MI, __this);
		m2927(V_0, L_5, &m2927_MI);
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7646_MI, __this);
		if (!L_6)
		{
			goto IL_009e;
		}
	}
	{
		t537* L_7 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m7644_MI, __this);
		V_2 = L_7;
		m2927(V_0, (t7*) &_stringLiteral175, &m2927_MI);
		V_3 = 0;
		goto IL_008c;
	}

IL_0069:
	{
		if ((((int32_t)V_3) <= ((int32_t)0)))
		{
			goto IL_0079;
		}
	}
	{
		m2927(V_0, (t7*) &_stringLiteral1070, &m2927_MI);
	}

IL_0079:
	{
		int32_t L_8 = V_3;
		t7* L_9 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, (*(t42 **)(t42 **)SZArrayLdElema(V_2, L_8)));
		m2927(V_0, L_9, &m2927_MI);
		V_3 = ((int32_t)(V_3+1));
	}

IL_008c:
	{
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)V_2)->max_length))))))
		{
			goto IL_0069;
		}
	}
	{
		m2927(V_0, (t7*) &_stringLiteral176, &m2927_MI);
	}

IL_009e:
	{
		m2927(V_0, (t7*) &_stringLiteral183, &m2927_MI);
		t638* L_10 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m7625_MI, __this);
		V_4 = L_10;
		V_5 = 0;
		goto IL_0127;
	}

IL_00b7:
	{
		if ((((int32_t)V_5) <= ((int32_t)0)))
		{
			goto IL_00c8;
		}
	}
	{
		m2927(V_0, (t7*) &_stringLiteral184, &m2927_MI);
	}

IL_00c8:
	{
		int32_t L_11 = V_5;
		t42 * L_12 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_4, L_11)));
		V_6 = L_12;
		bool L_13 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5994_MI, V_6);
		V_7 = L_13;
		if (!V_7)
		{
			goto IL_00ea;
		}
	}
	{
		t42 * L_14 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, V_6);
		V_6 = L_14;
	}

IL_00ea:
	{
		bool L_15 = m7639(NULL, V_6, &m7639_MI);
		if (!L_15)
		{
			goto IL_0103;
		}
	}
	{
		t7* L_16 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, V_6);
		m2927(V_0, L_16, &m2927_MI);
		goto IL_0111;
	}

IL_0103:
	{
		t7* L_17 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, V_6);
		m2927(V_0, L_17, &m2927_MI);
	}

IL_0111:
	{
		if (!V_7)
		{
			goto IL_0121;
		}
	}
	{
		m2927(V_0, (t7*) &_stringLiteral1242, &m2927_MI);
	}

IL_0121:
	{
		V_5 = ((int32_t)(V_5+1));
	}

IL_0127:
	{
		if ((((int32_t)V_5) < ((int32_t)(((int32_t)(((t20 *)V_4)->max_length))))))
		{
			goto IL_00b7;
		}
	}
	{
		int32_t L_18 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7630_MI, __this);
		if (!((int32_t)((int32_t)L_18&(int32_t)2)))
		{
			goto IL_0158;
		}
	}
	{
		if ((((int32_t)(((int32_t)(((t20 *)V_4)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_014c;
		}
	}
	{
		m2927(V_0, (t7*) &_stringLiteral184, &m2927_MI);
	}

IL_014c:
	{
		m2927(V_0, (t7*) &_stringLiteral1553, &m2927_MI);
	}

IL_0158:
	{
		m2927(V_0, (t7*) &_stringLiteral60, &m2927_MI);
		t7* L_19 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_0);
		return L_19;
	}
}
 void m7641 (t1377 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t537* V_0 = {0};
	t537* G_B4_0 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7646_MI, __this);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7645_MI, __this);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		t537* L_2 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m7644_MI, __this);
		G_B4_0 = L_2;
		goto IL_0019;
	}

IL_0018:
	{
		G_B4_0 = ((t537*)(NULL));
	}

IL_0019:
	{
		V_0 = G_B4_0;
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7633_MI, __this);
		t42 * L_4 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7631_MI, __this);
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7640_MI, __this);
		m7538(NULL, p0, L_3, L_4, L_5, 8, V_0, &m7538_MI);
		return;
	}
}
 t557 * m7642 (t1377 * __this, t537* p0, MethodInfo* method){
	t42 * V_0 = {0};
	t537* V_1 = {0};
	int32_t V_2 = 0;
	t557 * V_3 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1554, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		V_1 = p0;
		V_2 = 0;
		goto IL_0025;
	}

IL_0014:
	{
		int32_t L_1 = V_2;
		V_0 = (*(t42 **)(t42 **)SZArrayLdElema(V_1, L_1));
		if (V_0)
		{
			goto IL_0021;
		}
	}
	{
		t338 * L_2 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m8858(L_2, &m8858_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0021:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_0025:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0014;
		}
	}
	{
		t557 * L_3 = m7643(__this, p0, &m7643_MI);
		V_3 = L_3;
		if (V_3)
		{
			goto IL_005b;
		}
	}
	{
		t537* L_4 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m7644_MI, __this);
		int32_t L_5 = (((int32_t)(((t20 *)L_4)->max_length)));
		t29 * L_6 = Box(InitializedTypeInfo(&t44_TI), &L_5);
		int32_t L_7 = (((int32_t)(((t20 *)p0)->max_length)));
		t29 * L_8 = Box(InitializedTypeInfo(&t44_TI), &L_7);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_9 = m5610(NULL, (t7*) &_stringLiteral1555, L_6, L_8, &m5610_MI);
		t305 * L_10 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_10, L_9, &m1935_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_005b:
	{
		return V_3;
	}
}
 t557 * m7643 (t1377 * __this, t537* p0, MethodInfo* method){
	typedef t557 * (*m7643_ftn) (t1377 *, t537*);
	static m7643_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7643_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoMethod::MakeGenericMethod_impl(System.Type[])");
	return _il2cpp_icall_func(__this, p0);
}
 t537* m7644 (t1377 * __this, MethodInfo* method){
	typedef t537* (*m7644_ftn) (t1377 *);
	static m7644_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7644_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoMethod::GetGenericArguments()");
	return _il2cpp_icall_func(__this);
}
 bool m7645 (t1377 * __this, MethodInfo* method){
	typedef bool (*m7645_ftn) (t1377 *);
	static m7645_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7645_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoMethod::get_IsGenericMethodDefinition()");
	return _il2cpp_icall_func(__this);
}
 bool m7646 (t1377 * __this, MethodInfo* method){
	typedef bool (*m7646_ftn) (t1377 *);
	static m7646_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7646_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoMethod::get_IsGenericMethod()");
	return _il2cpp_icall_func(__this);
}
 bool m7647 (t1377 * __this, MethodInfo* method){
	t42 * V_0 = {0};
	t537* V_1 = {0};
	int32_t V_2 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7646_MI, __this);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		t537* L_1 = (t537*)VirtFuncInvoker0< t537* >::Invoke(&m7644_MI, __this);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0025;
	}

IL_0013:
	{
		int32_t L_2 = V_2;
		V_0 = (*(t42 **)(t42 **)SZArrayLdElema(V_1, L_2));
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6038_MI, V_0);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		return 1;
	}

IL_0021:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_0025:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0013;
		}
	}

IL_002b:
	{
		t42 * L_4 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7632_MI, __this);
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6038_MI, L_4);
		return L_5;
	}
}
// Metadata Definition System.Reflection.MonoMethod
extern Il2CppType t35_0_0_3;
FieldInfo t1377_f0_FieldInfo = 
{
	"mhandle", &t35_0_0_3, &t1377_TI, offsetof(t1377, f0), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1377_f1_FieldInfo = 
{
	"name", &t7_0_0_1, &t1377_TI, offsetof(t1377, f1), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_1;
FieldInfo t1377_f2_FieldInfo = 
{
	"reftype", &t42_0_0_1, &t1377_TI, offsetof(t1377, f2), &EmptyCustomAttributesCache};
static FieldInfo* t1377_FIs[] =
{
	&t1377_f0_FieldInfo,
	&t1377_f1_FieldInfo,
	&t1377_f2_FieldInfo,
	NULL
};
static PropertyInfo t1377____ReturnType_PropertyInfo = 
{
	&t1377_TI, "ReturnType", &m7624_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1377____MethodHandle_PropertyInfo = 
{
	&t1377_TI, "MethodHandle", &m7628_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1377____Attributes_PropertyInfo = 
{
	&t1377_TI, "Attributes", &m7629_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1377____CallingConvention_PropertyInfo = 
{
	&t1377_TI, "CallingConvention", &m7630_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1377____ReflectedType_PropertyInfo = 
{
	&t1377_TI, "ReflectedType", &m7631_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1377____DeclaringType_PropertyInfo = 
{
	&t1377_TI, "DeclaringType", &m7632_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1377____Name_PropertyInfo = 
{
	&t1377_TI, "Name", &m7633_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1377____IsGenericMethodDefinition_PropertyInfo = 
{
	&t1377_TI, "IsGenericMethodDefinition", &m7645_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1377____IsGenericMethod_PropertyInfo = 
{
	&t1377_TI, "IsGenericMethod", &m7646_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1377____ContainsGenericParameters_PropertyInfo = 
{
	&t1377_TI, "ContainsGenericParameters", &m7647_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1377_PIs[] =
{
	&t1377____ReturnType_PropertyInfo,
	&t1377____MethodHandle_PropertyInfo,
	&t1377____Attributes_PropertyInfo,
	&t1377____CallingConvention_PropertyInfo,
	&t1377____ReflectedType_PropertyInfo,
	&t1377____DeclaringType_PropertyInfo,
	&t1377____Name_PropertyInfo,
	&t1377____IsGenericMethodDefinition_PropertyInfo,
	&t1377____IsGenericMethod_PropertyInfo,
	&t1377____ContainsGenericParameters_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7620_MI = 
{
	".ctor", (methodPointerType)&m7620, &t1377_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6275, 0, 255, 0, false, false, 3150, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t636_0_0_0;
static ParameterInfo t1377_m7621_ParameterInfos[] = 
{
	{"method", 0, 134221654, &EmptyCustomAttributesCache, &t636_0_0_0},
};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7621_MI = 
{
	"get_name", (methodPointerType)&m7621, &t1377_TI, &t7_0_0_0, RuntimeInvoker_t29_t29, t1377_m7621_ParameterInfos, &EmptyCustomAttributesCache, 147, 4096, 255, 1, false, false, 3151, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1377_0_0_0;
extern Il2CppType t1377_0_0_0;
static ParameterInfo t1377_m7622_ParameterInfos[] = 
{
	{"method", 0, 134221655, &EmptyCustomAttributesCache, &t1377_0_0_0},
};
extern Il2CppType t1377_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7622_MI = 
{
	"get_base_definition", (methodPointerType)&m7622, &t1377_TI, &t1377_0_0_0, RuntimeInvoker_t29_t29, t1377_m7622_ParameterInfos, &EmptyCustomAttributesCache, 147, 4096, 255, 1, false, false, 3152, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7623_MI = 
{
	"GetBaseDefinition", (methodPointerType)&m7623, &t1377_TI, &t557_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 27, 0, false, false, 3153, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7624_MI = 
{
	"get_ReturnType", (methodPointerType)&m7624, &t1377_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 28, 0, false, false, 3154, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t638_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7625_MI = 
{
	"GetParameters", (methodPointerType)&m7625, &t1377_TI, &t638_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 14, 0, false, false, 3155, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t295_1_0_2;
extern Il2CppType t295_1_0_0;
static ParameterInfo t1377_m7626_ParameterInfos[] = 
{
	{"obj", 0, 134221656, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"parameters", 1, 134221657, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"exc", 2, 134221658, &EmptyCustomAttributesCache, &t295_1_0_2},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t1705 (MethodInfo* method, void* obj, void** args);
MethodInfo m7626_MI = 
{
	"InternalInvoke", (methodPointerType)&m7626, &t1377_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29_t1705, t1377_m7626_ParameterInfos, &EmptyCustomAttributesCache, 131, 4096, 255, 3, false, false, 3156, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t1377_m7627_ParameterInfos[] = 
{
	{"obj", 0, 134221659, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"invokeAttr", 1, 134221660, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 2, 134221661, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"parameters", 3, 134221662, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"culture", 4, 134221663, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7627_MI = 
{
	"Invoke", (methodPointerType)&m7627, &t1377_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29_t29, t1377_m7627_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 16, 5, false, false, 3157, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1343_0_0_0;
extern void* RuntimeInvoker_t1343 (MethodInfo* method, void* obj, void** args);
MethodInfo m7628_MI = 
{
	"get_MethodHandle", (methodPointerType)&m7628, &t1377_TI, &t1343_0_0_0, RuntimeInvoker_t1343, NULL, &EmptyCustomAttributesCache, 2246, 0, 17, 0, false, false, 3158, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1342_0_0_0;
extern void* RuntimeInvoker_t1342 (MethodInfo* method, void* obj, void** args);
MethodInfo m7629_MI = 
{
	"get_Attributes", (methodPointerType)&m7629, &t1377_TI, &t1342_0_0_0, RuntimeInvoker_t1342, NULL, &EmptyCustomAttributesCache, 2246, 0, 18, 0, false, false, 3159, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1150_0_0_0;
extern void* RuntimeInvoker_t1150 (MethodInfo* method, void* obj, void** args);
MethodInfo m7630_MI = 
{
	"get_CallingConvention", (methodPointerType)&m7630, &t1377_TI, &t1150_0_0_0, RuntimeInvoker_t1150, NULL, &EmptyCustomAttributesCache, 2246, 0, 19, 0, false, false, 3160, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7631_MI = 
{
	"get_ReflectedType", (methodPointerType)&m7631, &t1377_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 9, 0, false, false, 3161, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7632_MI = 
{
	"get_DeclaringType", (methodPointerType)&m7632, &t1377_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 6, 0, false, false, 3162, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7633_MI = 
{
	"get_Name", (methodPointerType)&m7633, &t1377_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 8, 0, false, false, 3163, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1377_m7634_ParameterInfos[] = 
{
	{"attributeType", 0, 134221664, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221665, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7634_MI = 
{
	"IsDefined", (methodPointerType)&m7634, &t1377_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t297, t1377_m7634_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 11, 2, false, false, 3164, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1377_m7635_ParameterInfos[] = 
{
	{"inherit", 0, 134221666, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7635_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m7635, &t1377_TI, &t316_0_0_0, RuntimeInvoker_t29_t297, t1377_m7635_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 12, 1, false, false, 3165, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1377_m7636_ParameterInfos[] = 
{
	{"attributeType", 0, 134221667, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221668, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7636_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m7636, &t1377_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t297, t1377_m7636_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 13, 2, false, false, 3166, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
static ParameterInfo t1377_m7637_ParameterInfos[] = 
{
	{"mhandle", 0, 134221669, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t1152_0_0_0;
extern void* RuntimeInvoker_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m7637_MI = 
{
	"GetDllImportAttribute", (methodPointerType)&m7637, &t1377_TI, &t1152_0_0_0, RuntimeInvoker_t29_t35, t1377_m7637_ParameterInfos, &EmptyCustomAttributesCache, 147, 4096, 255, 1, false, false, 3167, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7638_MI = 
{
	"GetPseudoCustomAttributes", (methodPointerType)&m7638, &t1377_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 131, 0, 255, 0, false, false, 3168, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1377_m7639_ParameterInfos[] = 
{
	{"type", 0, 134221670, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7639_MI = 
{
	"ShouldPrintFullName", (methodPointerType)&m7639, &t1377_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1377_m7639_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 3169, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7640_MI = 
{
	"ToString", (methodPointerType)&m7640, &t1377_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 3170, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1377_m7641_ParameterInfos[] = 
{
	{"info", 0, 134221671, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221672, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7641_MI = 
{
	"GetObjectData", (methodPointerType)&m7641, &t1377_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1377_m7641_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 2, false, false, 3171, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
static ParameterInfo t1377_m7642_ParameterInfos[] = 
{
	{"methodInstantiation", 0, 134221673, &EmptyCustomAttributesCache, &t537_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7642_MI = 
{
	"MakeGenericMethod", (methodPointerType)&m7642, &t1377_TI, &t557_0_0_0, RuntimeInvoker_t29_t29, t1377_m7642_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 29, 1, false, false, 3172, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
static ParameterInfo t1377_m7643_ParameterInfos[] = 
{
	{"types", 0, 134221674, &EmptyCustomAttributesCache, &t537_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7643_MI = 
{
	"MakeGenericMethod_impl", (methodPointerType)&m7643, &t1377_TI, &t557_0_0_0, RuntimeInvoker_t29_t29, t1377_m7643_ParameterInfos, &EmptyCustomAttributesCache, 129, 4096, 255, 1, false, false, 3173, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7644_MI = 
{
	"GetGenericArguments", (methodPointerType)&m7644, &t1377_TI, &t537_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 4096, 23, 0, false, false, 3174, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7645_MI = 
{
	"get_IsGenericMethodDefinition", (methodPointerType)&m7645, &t1377_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2246, 4096, 25, 0, false, false, 3175, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7646_MI = 
{
	"get_IsGenericMethod", (methodPointerType)&m7646, &t1377_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2246, 4096, 26, 0, false, false, 3176, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7647_MI = 
{
	"get_ContainsGenericParameters", (methodPointerType)&m7647, &t1377_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2246, 0, 24, 0, false, false, 3177, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1377_MIs[] =
{
	&m7620_MI,
	&m7621_MI,
	&m7622_MI,
	&m7623_MI,
	&m7624_MI,
	&m7625_MI,
	&m7626_MI,
	&m7627_MI,
	&m7628_MI,
	&m7629_MI,
	&m7630_MI,
	&m7631_MI,
	&m7632_MI,
	&m7633_MI,
	&m7634_MI,
	&m7635_MI,
	&m7636_MI,
	&m7637_MI,
	&m7638_MI,
	&m7639_MI,
	&m7640_MI,
	&m7641_MI,
	&m7642_MI,
	&m7643_MI,
	&m7644_MI,
	&m7645_MI,
	&m7646_MI,
	&m7647_MI,
	NULL
};
static MethodInfo* t1377_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m7640_MI,
	&m7636_MI,
	&m7634_MI,
	&m7632_MI,
	&m7555_MI,
	&m7633_MI,
	&m7631_MI,
	&m6048_MI,
	&m7634_MI,
	&m7635_MI,
	&m7636_MI,
	&m7625_MI,
	&m7546_MI,
	&m7627_MI,
	&m7628_MI,
	&m7629_MI,
	&m7630_MI,
	&m7548_MI,
	&m2952_MI,
	&m7549_MI,
	&m7644_MI,
	&m7647_MI,
	&m7645_MI,
	&m7646_MI,
	&m7623_MI,
	&m7624_MI,
	&m7642_MI,
	&m7641_MI,
};
static TypeInfo* t1377_ITIs[] = 
{
	&t374_TI,
};
static Il2CppInterfaceOffsetPair t1377_IOs[] = 
{
	{ &t2015_TI, 27},
	{ &t2010_TI, 14},
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
	{ &t374_TI, 30},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1377_1_0_0;
struct t1377;
TypeInfo t1377_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoMethod", "System.Reflection", t1377_MIs, t1377_PIs, t1377_FIs, NULL, &t557_TI, NULL, NULL, &t1377_TI, t1377_ITIs, t1377_VT, &EmptyCustomAttributesCache, &t1377_TI, &t1377_0_0_0, &t1377_1_0_0, t1377_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1377), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 28, 10, 3, 0, 0, 31, 1, 5};
#include "t1379.h"
#ifndef _MSC_VER
#else
#endif

#include "t1647.h"
extern TypeInfo t1647_TI;
#include "t1647MD.h"
extern MethodInfo m9289_MI;
extern MethodInfo m5992_MI;
extern MethodInfo m7650_MI;
extern MethodInfo m7656_MI;


 void m7648 (t1379 * __this, MethodInfo* method){
	{
		m7514(__this, &m7514_MI);
		return;
	}
}
 t638* m7649 (t1379 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f2);
		t638* L_1 = m7619(NULL, L_0, __this, &m7619_MI);
		return L_1;
	}
}
 t29 * m7650 (t1379 * __this, t29 * p0, t316* p1, t295 ** p2, MethodInfo* method){
	typedef t29 * (*m7650_ftn) (t1379 *, t29 *, t316*, t295 **);
	static m7650_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7650_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoCMethod::InternalInvoke(System.Object,System.Object[],System.Exception&)");
	return _il2cpp_icall_func(__this, p0, p1, p2);
}
 t29 * m7651 (t1379 * __this, t29 * p0, int32_t p1, t631 * p2, t316* p3, t633 * p4, MethodInfo* method){
	t638* V_0 = {0};
	int32_t V_1 = 0;
	t295 * V_2 = {0};
	t29 * V_3 = {0};
	t295 * V_4 = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	t29 * G_B31_0 = {0};
	{
		if (p2)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		t631 * L_0 = m7510(NULL, &m7510_MI);
		p2 = L_0;
	}

IL_000c:
	{
		t638* L_1 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m7649_MI, __this);
		V_0 = L_1;
		if (p3)
		{
			goto IL_001c;
		}
	}
	{
		if ((((int32_t)(((t20 *)V_0)->max_length))))
		{
			goto IL_0029;
		}
	}

IL_001c:
	{
		if (!p3)
		{
			goto IL_0034;
		}
	}
	{
		if ((((int32_t)(((int32_t)(((t20 *)p3)->max_length)))) == ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_0034;
		}
	}

IL_0029:
	{
		t1391 * L_2 = (t1391 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1391_TI));
		m7726(L_2, (t7*) &_stringLiteral1550, &m7726_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0034:
	{
		if (((int32_t)((int32_t)p1&(int32_t)((int32_t)65536))))
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t631_TI));
		bool L_3 = m7511(NULL, p2, p3, V_0, p4, &m7511_MI);
		if (L_3)
		{
			goto IL_0055;
		}
	}
	{
		t305 * L_4 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_4, (t7*) &_stringLiteral1551, &m1935_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_0055:
	{
		goto IL_0083;
	}

IL_0057:
	{
		V_1 = 0;
		goto IL_007d;
	}

IL_005b:
	{
		int32_t L_5 = V_1;
		t42 * L_6 = m1430((*(t29 **)(t29 **)SZArrayLdElema(p3, L_5)), &m1430_MI);
		int32_t L_7 = V_1;
		t42 * L_8 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_0, L_7)));
		if ((((t42 *)L_6) == ((t42 *)L_8)))
		{
			goto IL_0079;
		}
	}
	{
		t305 * L_9 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_9, (t7*) &_stringLiteral1550, &m1935_MI);
		il2cpp_codegen_raise_exception(L_9);
	}

IL_0079:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_007d:
	{
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_005b;
		}
	}

IL_0083:
	{
		if (p0)
		{
			goto IL_00ae;
		}
	}
	{
		t42 * L_10 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7657_MI, __this);
		bool L_11 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6038_MI, L_10);
		if (!L_11)
		{
			goto IL_00ae;
		}
	}
	{
		t42 * L_12 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7657_MI, __this);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_13 = m1295(NULL, (t7*) &_stringLiteral1556, L_12, (t7*) &_stringLiteral1557, &m1295_MI);
		t1647 * L_14 = (t1647 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1647_TI));
		m9289(L_14, L_13, &m9289_MI);
		il2cpp_codegen_raise_exception(L_14);
	}

IL_00ae:
	{
		if (!((int32_t)((int32_t)p1&(int32_t)((int32_t)512))))
		{
			goto IL_00da;
		}
	}
	{
		t42 * L_15 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7657_MI, __this);
		bool L_16 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5992_MI, L_15);
		if (!L_16)
		{
			goto IL_00da;
		}
	}
	{
		t42 * L_17 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7657_MI, __this);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_18 = m1535(NULL, (t7*) &_stringLiteral1558, L_17, &m1535_MI);
		t1647 * L_19 = (t1647 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1647_TI));
		m9289(L_19, L_18, &m9289_MI);
		il2cpp_codegen_raise_exception(L_19);
	}

IL_00da:
	{
		V_2 = (t295 *)NULL;
		V_3 = NULL;
	}

IL_00de:
	try
	{ // begin try (depth: 1)
		t29 * L_20 = m7650(__this, p0, p3, (&V_2), &m7650_MI);
		V_3 = L_20;
		// IL_00ea: leave.s IL_00fd
		goto IL_00fd;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t1650_TI, e.ex->object.klass))
			goto IL_00ec;
		if(il2cpp_codegen_class_is_assignable_from (&t295_TI, e.ex->object.klass))
			goto IL_00f1;
		throw e;
	}

IL_00ec:
	{ // begin catch(System.MethodAccessException)
		il2cpp_codegen_raise_exception(__exception_local);
		// IL_00ef: leave.s IL_00fd
		goto IL_00fd;
	} // end catch (depth: 1)

IL_00f1:
	{ // begin catch(System.Exception)
		V_4 = ((t295 *)__exception_local);
		t1390 * L_21 = (t1390 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1390_TI));
		m7723(L_21, V_4, &m7723_MI);
		il2cpp_codegen_raise_exception(L_21);
		// IL_00fb: leave.s IL_00fd
		goto IL_00fd;
	} // end catch (depth: 1)

IL_00fd:
	{
		if (!V_2)
		{
			goto IL_0102;
		}
	}
	{
		il2cpp_codegen_raise_exception(V_2);
	}

IL_0102:
	{
		if (p0)
		{
			goto IL_0108;
		}
	}
	{
		G_B31_0 = V_3;
		goto IL_0109;
	}

IL_0108:
	{
		G_B31_0 = NULL;
	}

IL_0109:
	{
		return G_B31_0;
	}
}
 t29 * m7652 (t1379 * __this, int32_t p0, t631 * p1, t316* p2, t633 * p3, MethodInfo* method){
	{
		t29 * L_0 = (t29 *)VirtFuncInvoker5< t29 *, t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m7651_MI, __this, NULL, p0, p1, p2, p3);
		return L_0;
	}
}
 t1343  m7653 (t1379 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f2);
		t1343  L_1 = {0};
		m9497(&L_1, L_0, &m9497_MI);
		return L_1;
	}
}
 int32_t m7654 (t1379 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f2);
		int32_t L_1 = m7616(NULL, L_0, &m7616_MI);
		return L_1;
	}
}
 int32_t m7655 (t1379 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f2);
		int32_t L_1 = m7617(NULL, L_0, &m7617_MI);
		return L_1;
	}
}
 t42 * m7656 (t1379 * __this, MethodInfo* method){
	{
		t42 * L_0 = (__this->f4);
		return L_0;
	}
}
 t42 * m7657 (t1379 * __this, MethodInfo* method){
	{
		t35 L_0 = (__this->f2);
		t42 * L_1 = m7614(NULL, L_0, &m7614_MI);
		return L_1;
	}
}
 t7* m7658 (t1379 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f3);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		t7* L_1 = (__this->f3);
		return L_1;
	}

IL_000f:
	{
		t7* L_2 = m7621(NULL, __this, &m7621_MI);
		return L_2;
	}
}
 bool m7659 (t1379 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		bool L_0 = m9320(NULL, __this, p0, p1, &m9320_MI);
		return L_0;
	}
}
 t316* m7660 (t1379 * __this, bool p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9319(NULL, __this, p0, &m9319_MI);
		return L_0;
	}
}
 t316* m7661 (t1379 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9318(NULL, __this, p0, p1, &m9318_MI);
		return L_0;
	}
}
 t7* m7662 (t1379 * __this, MethodInfo* method){
	t292 * V_0 = {0};
	t638* V_1 = {0};
	int32_t V_2 = 0;
	{
		t292 * L_0 = (t292 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t292_TI));
		m1311(L_0, &m1311_MI);
		V_0 = L_0;
		m2927(V_0, (t7*) &_stringLiteral1559, &m2927_MI);
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7658_MI, __this);
		m2927(V_0, L_1, &m2927_MI);
		m2927(V_0, (t7*) &_stringLiteral183, &m2927_MI);
		t638* L_2 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m7649_MI, __this);
		V_1 = L_2;
		V_2 = 0;
		goto IL_005e;
	}

IL_0036:
	{
		if ((((int32_t)V_2) <= ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		m2927(V_0, (t7*) &_stringLiteral184, &m2927_MI);
	}

IL_0046:
	{
		int32_t L_3 = V_2;
		t42 * L_4 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_1, L_3)));
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, L_4);
		m2927(V_0, L_5, &m2927_MI);
		V_2 = ((int32_t)(V_2+1));
	}

IL_005e:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7655_MI, __this);
		if ((((uint32_t)L_6) != ((uint32_t)3)))
		{
			goto IL_0079;
		}
	}
	{
		m2927(V_0, (t7*) &_stringLiteral1560, &m2927_MI);
	}

IL_0079:
	{
		m2927(V_0, (t7*) &_stringLiteral60, &m2927_MI);
		t7* L_7 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1315_MI, V_0);
		return L_7;
	}
}
 void m7663 (t1379 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		t7* L_0 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7658_MI, __this);
		t42 * L_1 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7656_MI, __this);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7662_MI, __this);
		m7537(NULL, p0, L_0, L_1, L_2, 1, &m7537_MI);
		return;
	}
}
// Metadata Definition System.Reflection.MonoCMethod
extern Il2CppType t35_0_0_3;
FieldInfo t1379_f2_FieldInfo = 
{
	"mhandle", &t35_0_0_3, &t1379_TI, offsetof(t1379, f2), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1379_f3_FieldInfo = 
{
	"name", &t7_0_0_1, &t1379_TI, offsetof(t1379, f3), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_1;
FieldInfo t1379_f4_FieldInfo = 
{
	"reftype", &t42_0_0_1, &t1379_TI, offsetof(t1379, f4), &EmptyCustomAttributesCache};
static FieldInfo* t1379_FIs[] =
{
	&t1379_f2_FieldInfo,
	&t1379_f3_FieldInfo,
	&t1379_f4_FieldInfo,
	NULL
};
static PropertyInfo t1379____MethodHandle_PropertyInfo = 
{
	&t1379_TI, "MethodHandle", &m7653_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1379____Attributes_PropertyInfo = 
{
	&t1379_TI, "Attributes", &m7654_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1379____CallingConvention_PropertyInfo = 
{
	&t1379_TI, "CallingConvention", &m7655_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1379____ReflectedType_PropertyInfo = 
{
	&t1379_TI, "ReflectedType", &m7656_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1379____DeclaringType_PropertyInfo = 
{
	&t1379_TI, "DeclaringType", &m7657_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1379____Name_PropertyInfo = 
{
	&t1379_TI, "Name", &m7658_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1379_PIs[] =
{
	&t1379____MethodHandle_PropertyInfo,
	&t1379____Attributes_PropertyInfo,
	&t1379____CallingConvention_PropertyInfo,
	&t1379____ReflectedType_PropertyInfo,
	&t1379____DeclaringType_PropertyInfo,
	&t1379____Name_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7648_MI = 
{
	".ctor", (methodPointerType)&m7648, &t1379_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3178, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t638_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7649_MI = 
{
	"GetParameters", (methodPointerType)&m7649, &t1379_TI, &t638_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 14, 0, false, false, 3179, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t295_1_0_2;
static ParameterInfo t1379_m7650_ParameterInfos[] = 
{
	{"obj", 0, 134221675, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"parameters", 1, 134221676, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"exc", 2, 134221677, &EmptyCustomAttributesCache, &t295_1_0_2},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t1705 (MethodInfo* method, void* obj, void** args);
MethodInfo m7650_MI = 
{
	"InternalInvoke", (methodPointerType)&m7650, &t1379_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29_t1705, t1379_m7650_ParameterInfos, &EmptyCustomAttributesCache, 131, 4096, 255, 3, false, false, 3180, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t1379_m7651_ParameterInfos[] = 
{
	{"obj", 0, 134221678, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"invokeAttr", 1, 134221679, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 2, 134221680, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"parameters", 3, 134221681, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"culture", 4, 134221682, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7651_MI = 
{
	"Invoke", (methodPointerType)&m7651, &t1379_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29_t29, t1379_m7651_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 16, 5, false, false, 3181, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t1379_m7652_ParameterInfos[] = 
{
	{"invokeAttr", 0, 134221683, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 1, 134221684, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"parameters", 2, 134221685, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"culture", 3, 134221686, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7652_MI = 
{
	"Invoke", (methodPointerType)&m7652, &t1379_TI, &t29_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29, t1379_m7652_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 27, 4, false, false, 3182, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1343_0_0_0;
extern void* RuntimeInvoker_t1343 (MethodInfo* method, void* obj, void** args);
MethodInfo m7653_MI = 
{
	"get_MethodHandle", (methodPointerType)&m7653, &t1379_TI, &t1343_0_0_0, RuntimeInvoker_t1343, NULL, &EmptyCustomAttributesCache, 2246, 0, 17, 0, false, false, 3183, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1342_0_0_0;
extern void* RuntimeInvoker_t1342 (MethodInfo* method, void* obj, void** args);
MethodInfo m7654_MI = 
{
	"get_Attributes", (methodPointerType)&m7654, &t1379_TI, &t1342_0_0_0, RuntimeInvoker_t1342, NULL, &EmptyCustomAttributesCache, 2246, 0, 18, 0, false, false, 3184, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1150_0_0_0;
extern void* RuntimeInvoker_t1150 (MethodInfo* method, void* obj, void** args);
MethodInfo m7655_MI = 
{
	"get_CallingConvention", (methodPointerType)&m7655, &t1379_TI, &t1150_0_0_0, RuntimeInvoker_t1150, NULL, &EmptyCustomAttributesCache, 2246, 0, 19, 0, false, false, 3185, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7656_MI = 
{
	"get_ReflectedType", (methodPointerType)&m7656, &t1379_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 9, 0, false, false, 3186, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7657_MI = 
{
	"get_DeclaringType", (methodPointerType)&m7657, &t1379_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 6, 0, false, false, 3187, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7658_MI = 
{
	"get_Name", (methodPointerType)&m7658, &t1379_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 8, 0, false, false, 3188, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1379_m7659_ParameterInfos[] = 
{
	{"attributeType", 0, 134221687, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221688, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7659_MI = 
{
	"IsDefined", (methodPointerType)&m7659, &t1379_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t297, t1379_m7659_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 11, 2, false, false, 3189, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1379_m7660_ParameterInfos[] = 
{
	{"inherit", 0, 134221689, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7660_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m7660, &t1379_TI, &t316_0_0_0, RuntimeInvoker_t29_t297, t1379_m7660_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 12, 1, false, false, 3190, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1379_m7661_ParameterInfos[] = 
{
	{"attributeType", 0, 134221690, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221691, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7661_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m7661, &t1379_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t297, t1379_m7661_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 13, 2, false, false, 3191, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7662_MI = 
{
	"ToString", (methodPointerType)&m7662, &t1379_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 3192, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1379_m7663_ParameterInfos[] = 
{
	{"info", 0, 134221692, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221693, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7663_MI = 
{
	"GetObjectData", (methodPointerType)&m7663, &t1379_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1379_m7663_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, false, 3193, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1379_MIs[] =
{
	&m7648_MI,
	&m7649_MI,
	&m7650_MI,
	&m7651_MI,
	&m7652_MI,
	&m7653_MI,
	&m7654_MI,
	&m7655_MI,
	&m7656_MI,
	&m7657_MI,
	&m7658_MI,
	&m7659_MI,
	&m7660_MI,
	&m7661_MI,
	&m7662_MI,
	&m7663_MI,
	NULL
};
static MethodInfo* t1379_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m7662_MI,
	&m7661_MI,
	&m7659_MI,
	&m7657_MI,
	&m7516_MI,
	&m7658_MI,
	&m7656_MI,
	&m6048_MI,
	&m7659_MI,
	&m7660_MI,
	&m7661_MI,
	&m7649_MI,
	&m7546_MI,
	&m7651_MI,
	&m7653_MI,
	&m7654_MI,
	&m7655_MI,
	&m7548_MI,
	&m2952_MI,
	&m7549_MI,
	&m7550_MI,
	&m7551_MI,
	&m7552_MI,
	&m7553_MI,
	&m7652_MI,
	&m7663_MI,
};
static TypeInfo* t1379_ITIs[] = 
{
	&t374_TI,
};
static Il2CppInterfaceOffsetPair t1379_IOs[] = 
{
	{ &t2009_TI, 27},
	{ &t2010_TI, 14},
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
	{ &t374_TI, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1379_0_0_0;
extern Il2CppType t1379_1_0_0;
struct t1379;
TypeInfo t1379_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoCMethod", "System.Reflection", t1379_MIs, t1379_PIs, t1379_FIs, NULL, &t660_TI, NULL, NULL, &t1379_TI, t1379_ITIs, t1379_VT, &EmptyCustomAttributesCache, &t1379_TI, &t1379_0_0_0, &t1379_1_0_0, t1379_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1379), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 16, 6, 3, 0, 0, 29, 1, 5};
#include "t1381.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1381_TI;
#include "t1381MD.h"

#include "t1383.h"
#include "t1384.h"


extern MethodInfo m7664_MI;
 void m7664 (t29 * __this, t1383 * p0, t1381 * p1, int32_t p2, MethodInfo* method){
	typedef void (*m7664_ftn) (t1383 *, t1381 *, int32_t);
	static m7664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7664_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoPropertyInfo::get_property_info(System.Reflection.MonoProperty,System.Reflection.MonoPropertyInfo&,System.Reflection.PInfo)");
	_il2cpp_icall_func(p0, p1, p2);
}
extern MethodInfo m7665_MI;
 t537* m7665 (t29 * __this, t1383 * p0, bool p1, MethodInfo* method){
	typedef t537* (*m7665_ftn) (t1383 *, bool);
	static m7665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7665_ftn)il2cpp_codegen_resolve_icall ("System.Reflection.MonoPropertyInfo::GetTypeModifiers(System.Reflection.MonoProperty,System.Boolean)");
	return _il2cpp_icall_func(p0, p1);
}
// Metadata Definition System.Reflection.MonoPropertyInfo
extern Il2CppType t42_0_0_6;
FieldInfo t1381_f0_FieldInfo = 
{
	"parent", &t42_0_0_6, &t1381_TI, offsetof(t1381, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_6;
FieldInfo t1381_f1_FieldInfo = 
{
	"name", &t7_0_0_6, &t1381_TI, offsetof(t1381, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t557_0_0_6;
FieldInfo t1381_f2_FieldInfo = 
{
	"get_method", &t557_0_0_6, &t1381_TI, offsetof(t1381, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t557_0_0_6;
FieldInfo t1381_f3_FieldInfo = 
{
	"set_method", &t557_0_0_6, &t1381_TI, offsetof(t1381, f3) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1382_0_0_6;
FieldInfo t1381_f4_FieldInfo = 
{
	"attrs", &t1382_0_0_6, &t1381_TI, offsetof(t1381, f4) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t1381_FIs[] =
{
	&t1381_f0_FieldInfo,
	&t1381_f1_FieldInfo,
	&t1381_f2_FieldInfo,
	&t1381_f3_FieldInfo,
	&t1381_f4_FieldInfo,
	NULL
};
extern Il2CppType t1383_0_0_0;
extern Il2CppType t1383_0_0_0;
extern Il2CppType t1381_1_0_0;
extern Il2CppType t1381_1_0_0;
extern Il2CppType t1384_0_0_0;
extern Il2CppType t1384_0_0_0;
static ParameterInfo t1381_m7664_ParameterInfos[] = 
{
	{"prop", 0, 134221694, &EmptyCustomAttributesCache, &t1383_0_0_0},
	{"info", 1, 134221695, &EmptyCustomAttributesCache, &t1381_1_0_0},
	{"req_info", 2, 134221696, &EmptyCustomAttributesCache, &t1384_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t2027_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7664_MI = 
{
	"get_property_info", (methodPointerType)&m7664, &t1381_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t2027_t44, t1381_m7664_ParameterInfos, &EmptyCustomAttributesCache, 147, 4096, 255, 3, false, false, 3194, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1383_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1381_m7665_ParameterInfos[] = 
{
	{"prop", 0, 134221697, &EmptyCustomAttributesCache, &t1383_0_0_0},
	{"optional", 1, 134221698, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7665_MI = 
{
	"GetTypeModifiers", (methodPointerType)&m7665, &t1381_TI, &t537_0_0_0, RuntimeInvoker_t29_t29_t297, t1381_m7665_ParameterInfos, &EmptyCustomAttributesCache, 147, 4096, 255, 2, false, false, 3195, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1381_MIs[] =
{
	&m7664_MI,
	&m7665_MI,
	NULL
};
static MethodInfo* t1381_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1381_0_0_0;
TypeInfo t1381_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoPropertyInfo", "System.Reflection", t1381_MIs, NULL, t1381_FIs, NULL, &t110_TI, NULL, NULL, &t1381_TI, NULL, t1381_VT, &EmptyCustomAttributesCache, &t1381_TI, &t1381_0_0_0, &t1381_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1381)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048840, 0, true, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 5, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1384_TI;
#include "t1384MD.h"



// Metadata Definition System.Reflection.PInfo
extern Il2CppType t44_0_0_1542;
FieldInfo t1384_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1384_TI, offsetof(t1384, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1384_0_0_32854;
FieldInfo t1384_f2_FieldInfo = 
{
	"Attributes", &t1384_0_0_32854, &t1384_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1384_0_0_32854;
FieldInfo t1384_f3_FieldInfo = 
{
	"GetMethod", &t1384_0_0_32854, &t1384_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1384_0_0_32854;
FieldInfo t1384_f4_FieldInfo = 
{
	"SetMethod", &t1384_0_0_32854, &t1384_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1384_0_0_32854;
FieldInfo t1384_f5_FieldInfo = 
{
	"ReflectedType", &t1384_0_0_32854, &t1384_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1384_0_0_32854;
FieldInfo t1384_f6_FieldInfo = 
{
	"DeclaringType", &t1384_0_0_32854, &t1384_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1384_0_0_32854;
FieldInfo t1384_f7_FieldInfo = 
{
	"Name", &t1384_0_0_32854, &t1384_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1384_FIs[] =
{
	&t1384_f1_FieldInfo,
	&t1384_f2_FieldInfo,
	&t1384_f3_FieldInfo,
	&t1384_f4_FieldInfo,
	&t1384_f5_FieldInfo,
	&t1384_f6_FieldInfo,
	&t1384_f7_FieldInfo,
	NULL
};
static const int32_t t1384_f2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1384_f2_DefaultValue = 
{
	&t1384_f2_FieldInfo, { (char*)&t1384_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1384_f3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1384_f3_DefaultValue = 
{
	&t1384_f3_FieldInfo, { (char*)&t1384_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1384_f4_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1384_f4_DefaultValue = 
{
	&t1384_f4_FieldInfo, { (char*)&t1384_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1384_f5_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t1384_f5_DefaultValue = 
{
	&t1384_f5_FieldInfo, { (char*)&t1384_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1384_f6_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t1384_f6_DefaultValue = 
{
	&t1384_f6_FieldInfo, { (char*)&t1384_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1384_f7_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t1384_f7_DefaultValue = 
{
	&t1384_f7_FieldInfo, { (char*)&t1384_f7_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1384_FDVs[] = 
{
	&t1384_f2_DefaultValue,
	&t1384_f3_DefaultValue,
	&t1384_f4_DefaultValue,
	&t1384_f5_DefaultValue,
	&t1384_f6_DefaultValue,
	&t1384_f7_DefaultValue,
	NULL
};
static MethodInfo* t1384_MIs[] =
{
	NULL
};
static MethodInfo* t1384_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1384_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1384_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1384__CustomAttributeCache = {
1,
NULL,
&t1384_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1384_1_0_0;
extern CustomAttributesCache t1384__CustomAttributeCache;
TypeInfo t1384_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PInfo", "System.Reflection", t1384_MIs, NULL, t1384_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1384_VT, &t1384__CustomAttributeCache, &t44_TI, &t1384_0_0_0, &t1384_1_0_0, t1384_IOs, NULL, NULL, t1384_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1384)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 256, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 7, 0, 0, 23, 0, 3};
#include "t1385.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1385_TI;
#include "t1385MD.h"



extern MethodInfo m7666_MI;
 void m7666 (t1385 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m7667_MI;
 t29 * m7667 (t1385 * __this, t29 * p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m7667((t1385 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t29 * (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
t29 * pinvoke_delegate_wrapper_t1385(Il2CppObject* delegate, t29 * p0)
{
	typedef t29 * (STDCALL *native_function_ptr_type)(t29 *);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter 'p0' to native representation
	t29 * _p0_marshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	// Native function invocation and marshaling of return value back from native representation
	t29 * _return_value = _il2cpp_pinvoke_func(_p0_marshaled);
	t29 * __return_value_unmarshaled = { 0 };
	il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	// Marshaling cleanup of parameter 'p0' native representation

	return __return_value_unmarshaled;
}
extern MethodInfo m7668_MI;
 t29 * m7668 (t1385 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m7669_MI;
 t29 * m7669 (t1385 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t29 *)__result;
}
// Metadata Definition System.Reflection.MonoProperty/GetterAdapter
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1385_m7666_ParameterInfos[] = 
{
	{"object", 0, 134221728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134221729, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
MethodInfo m7666_MI = 
{
	".ctor", (methodPointerType)&m7666, &t1385_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t1385_m7666_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 3222, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1385_m7667_ParameterInfos[] = 
{
	{"_this", 0, 134221730, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7667_MI = 
{
	"Invoke", (methodPointerType)&m7667, &t1385_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1385_m7667_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 3223, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1385_m7668_ParameterInfos[] = 
{
	{"_this", 0, 134221731, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 1, 134221732, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134221733, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7668_MI = 
{
	"BeginInvoke", (methodPointerType)&m7668, &t1385_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t1385_m7668_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 3224, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1385_m7669_ParameterInfos[] = 
{
	{"result", 0, 134221734, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7669_MI = 
{
	"EndInvoke", (methodPointerType)&m7669, &t1385_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1385_m7669_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 3225, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1385_MIs[] =
{
	&m7666_MI,
	&m7667_MI,
	&m7668_MI,
	&m7669_MI,
	NULL
};
static MethodInfo* t1385_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m7667_MI,
	&m7668_MI,
	&m7669_MI,
};
static Il2CppInterfaceOffsetPair t1385_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1385_0_0_0;
extern Il2CppType t1385_1_0_0;
struct t1385;
extern TypeInfo t1383_TI;
TypeInfo t1385_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GetterAdapter", "", t1385_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1383_TI, &t1385_TI, NULL, t1385_VT, &EmptyCustomAttributesCache, &t1385_TI, &t1385_0_0_0, &t1385_1_0_0, t1385_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)pinvoke_delegate_wrapper_t1385, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1385), 0, sizeof(methodPointerType), 0, 0, -1, 259, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t1386.h"
extern Il2CppGenericContainer t1386_IGC;
extern TypeInfo t1386_gp_T_0_TI;
Il2CppGenericParamFull t1386_gp_T_0_TI_GenericParamFull = { { &t1386_IGC, 0}, {NULL, "T", 0, 0, NULL} };
extern TypeInfo t1386_gp_R_1_TI;
Il2CppGenericParamFull t1386_gp_R_1_TI_GenericParamFull = { { &t1386_IGC, 1}, {NULL, "R", 0, 0, NULL} };
static Il2CppGenericParamFull* t1386_IGPA[2] = 
{
	&t1386_gp_T_0_TI_GenericParamFull,
	&t1386_gp_R_1_TI_GenericParamFull,
};
extern TypeInfo t1386_TI;
Il2CppGenericContainer t1386_IGC = { { NULL, NULL }, NULL, &t1386_TI, 2, 0, t1386_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1386_m10159_ParameterInfos[] = 
{
	{"object", 0, 134221735, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134221736, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m10159_MI = 
{
	".ctor", NULL, &t1386_TI, &t21_0_0_0, NULL, t1386_m10159_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 3226, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1386_gp_0_0_0_0;
extern Il2CppType t1386_gp_0_0_0_0;
static ParameterInfo t1386_m10160_ParameterInfos[] = 
{
	{"_this", 0, 134221737, &EmptyCustomAttributesCache, &t1386_gp_0_0_0_0},
};
extern Il2CppType t1386_gp_1_0_0_0;
MethodInfo m10160_MI = 
{
	"Invoke", NULL, &t1386_TI, &t1386_gp_1_0_0_0, NULL, t1386_m10160_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 3227, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1386_gp_0_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1386_m10161_ParameterInfos[] = 
{
	{"_this", 0, 134221738, &EmptyCustomAttributesCache, &t1386_gp_0_0_0_0},
	{"callback", 1, 134221739, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134221740, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
MethodInfo m10161_MI = 
{
	"BeginInvoke", NULL, &t1386_TI, &t66_0_0_0, NULL, t1386_m10161_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 3228, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1386_m10162_ParameterInfos[] = 
{
	{"result", 0, 134221741, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t1386_gp_1_0_0_0;
MethodInfo m10162_MI = 
{
	"EndInvoke", NULL, &t1386_TI, &t1386_gp_1_0_0_0, NULL, t1386_m10162_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 3229, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1386_MIs[] =
{
	&m10159_MI,
	&m10160_MI,
	&m10161_MI,
	&m10162_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1386_0_0_0;
extern Il2CppType t1386_1_0_0;
struct t1386;
TypeInfo t1386_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Getter`2", "", t1386_MIs, NULL, NULL, NULL, NULL, NULL, &t1383_TI, &t1386_TI, NULL, NULL, NULL, NULL, &t1386_0_0_0, &t1386_1_0_0, NULL, NULL, &t1386_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 0, 0, 0};
#include "t1387.h"
extern Il2CppGenericContainer t1387_IGC;
extern TypeInfo t1387_gp_R_0_TI;
Il2CppGenericParamFull t1387_gp_R_0_TI_GenericParamFull = { { &t1387_IGC, 0}, {NULL, "R", 0, 0, NULL} };
static Il2CppGenericParamFull* t1387_IGPA[1] = 
{
	&t1387_gp_R_0_TI_GenericParamFull,
};
extern TypeInfo t1387_TI;
Il2CppGenericContainer t1387_IGC = { { NULL, NULL }, NULL, &t1387_TI, 1, 0, t1387_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t1387_m10163_ParameterInfos[] = 
{
	{"object", 0, 134221742, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134221743, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m10163_MI = 
{
	".ctor", NULL, &t1387_TI, &t21_0_0_0, NULL, t1387_m10163_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 3230, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1387_gp_0_0_0_0;
MethodInfo m10164_MI = 
{
	"Invoke", NULL, &t1387_TI, &t1387_gp_0_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 454, 3, 10, 0, false, false, 3231, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1387_m10165_ParameterInfos[] = 
{
	{"callback", 0, 134221744, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 1, 134221745, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
MethodInfo m10165_MI = 
{
	"BeginInvoke", NULL, &t1387_TI, &t66_0_0_0, NULL, t1387_m10165_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 2, false, false, 3232, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
static ParameterInfo t1387_m10166_ParameterInfos[] = 
{
	{"result", 0, 134221746, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t1387_gp_0_0_0_0;
MethodInfo m10166_MI = 
{
	"EndInvoke", NULL, &t1387_TI, &t1387_gp_0_0_0_0, NULL, t1387_m10166_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 3233, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1387_MIs[] =
{
	&m10163_MI,
	&m10164_MI,
	&m10165_MI,
	&m10166_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1387_0_0_0;
extern Il2CppType t1387_1_0_0;
struct t1387;
TypeInfo t1387_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StaticGetter`1", "", t1387_MIs, NULL, NULL, NULL, NULL, NULL, &t1383_TI, &t1387_TI, NULL, NULL, NULL, NULL, &t1387_0_0_0, &t1387_1_0_0, NULL, NULL, &t1387_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
#include "t1383MD.h"

#include "t1382.h"
#include "t1650.h"
#include "t1570.h"
extern TypeInfo t1570_TI;
#include "t353MD.h"
#include "t1650MD.h"
extern Il2CppType t1387_0_0_0;
extern Il2CppType t1386_0_0_0;
extern MethodInfo m7671_MI;
extern MethodInfo m7711_MI;
extern MethodInfo m7696_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m5848_MI;
extern MethodInfo m9291_MI;
extern MethodInfo m6023_MI;
extern MethodInfo m5847_MI;
extern MethodInfo m7688_MI;
extern MethodInfo m7680_MI;
extern MethodInfo m7678_MI;
extern MethodInfo m1685_MI;
extern MethodInfo m7682_MI;
extern MethodInfo m7675_MI;
extern MethodInfo m7676_MI;
extern MethodInfo m7690_MI;


extern MethodInfo m7670_MI;
 void m7670 (t1383 * __this, MethodInfo* method){
	{
		m7711(__this, &m7711_MI);
		return;
	}
}
 void m7671 (t1383 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f3);
		if ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)p0))) == ((int32_t)p0)))
		{
			goto IL_0026;
		}
	}
	{
		t1381 * L_1 = &(__this->f2);
		m7664(NULL, __this, L_1, p0, &m7664_MI);
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)((int32_t)L_2|(int32_t)p0));
	}

IL_0026:
	{
		return;
	}
}
extern MethodInfo m7672_MI;
 int32_t m7672 (t1383 * __this, MethodInfo* method){
	{
		m7671(__this, 1, &m7671_MI);
		t1381 * L_0 = &(__this->f2);
		int32_t L_1 = (L_0->f4);
		return L_1;
	}
}
extern MethodInfo m7673_MI;
 bool m7673 (t1383 * __this, MethodInfo* method){
	{
		m7671(__this, 2, &m7671_MI);
		t1381 * L_0 = &(__this->f2);
		t557 * L_1 = (L_0->f2);
		return ((((int32_t)((((t557 *)L_1) == ((t29 *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m7674_MI;
 bool m7674 (t1383 * __this, MethodInfo* method){
	{
		m7671(__this, 4, &m7671_MI);
		t1381 * L_0 = &(__this->f2);
		t557 * L_1 = (L_0->f3);
		return ((((int32_t)((((t557 *)L_1) == ((t29 *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 t42 * m7675 (t1383 * __this, MethodInfo* method){
	t638* V_0 = {0};
	{
		m7671(__this, 6, &m7671_MI);
		t1381 * L_0 = &(__this->f2);
		t557 * L_1 = (L_0->f2);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		t1381 * L_2 = &(__this->f2);
		t557 * L_3 = (L_2->f2);
		t42 * L_4 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7556_MI, L_3);
		return L_4;
	}

IL_0025:
	{
		t1381 * L_5 = &(__this->f2);
		t557 * L_6 = (L_5->f3);
		t638* L_7 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, L_6);
		V_0 = L_7;
		int32_t L_8 = ((int32_t)((((int32_t)(((t20 *)V_0)->max_length)))-1));
		t42 * L_9 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, (*(t637 **)(t637 **)SZArrayLdElema(V_0, L_8)));
		return L_9;
	}
}
 t42 * m7676 (t1383 * __this, MethodInfo* method){
	{
		m7671(__this, 8, &m7671_MI);
		t1381 * L_0 = &(__this->f2);
		t42 * L_1 = (L_0->f0);
		return L_1;
	}
}
extern MethodInfo m7677_MI;
 t42 * m7677 (t1383 * __this, MethodInfo* method){
	{
		m7671(__this, ((int32_t)16), &m7671_MI);
		t1381 * L_0 = &(__this->f2);
		t42 * L_1 = (L_0->f0);
		return L_1;
	}
}
 t7* m7678 (t1383 * __this, MethodInfo* method){
	{
		m7671(__this, ((int32_t)32), &m7671_MI);
		t1381 * L_0 = &(__this->f2);
		t7* L_1 = (L_0->f1);
		return L_1;
	}
}
extern MethodInfo m7679_MI;
 t1145* m7679 (t1383 * __this, bool p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t1145* V_2 = {0};
	int32_t V_3 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		m7671(__this, 6, &m7671_MI);
		t1381 * L_0 = &(__this->f2);
		t557 * L_1 = (L_0->f3);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		if (p0)
		{
			goto IL_002d;
		}
	}
	{
		t1381 * L_2 = &(__this->f2);
		t557 * L_3 = (L_2->f3);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7548_MI, L_3);
		if (!L_4)
		{
			goto IL_002f;
		}
	}

IL_002d:
	{
		V_1 = 1;
	}

IL_002f:
	{
		t1381 * L_5 = &(__this->f2);
		t557 * L_6 = (L_5->f2);
		if (!L_6)
		{
			goto IL_0053;
		}
	}
	{
		if (p0)
		{
			goto IL_0051;
		}
	}
	{
		t1381 * L_7 = &(__this->f2);
		t557 * L_8 = (L_7->f2);
		bool L_9 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7548_MI, L_8);
		if (!L_9)
		{
			goto IL_0053;
		}
	}

IL_0051:
	{
		V_0 = 1;
	}

IL_0053:
	{
		V_2 = ((t1145*)SZArrayNew(InitializedTypeInfo(&t1145_TI), ((int32_t)(V_0+V_1))));
		V_3 = 0;
		if (!V_1)
		{
			goto IL_0073;
		}
	}
	{
		int32_t L_10 = V_3;
		V_3 = ((int32_t)(L_10+1));
		t1381 * L_11 = &(__this->f2);
		t557 * L_12 = (L_11->f3);
		ArrayElementTypeCheck (V_2, L_12);
		*((t557 **)(t557 **)SZArrayLdElema(V_2, L_10)) = (t557 *)L_12;
	}

IL_0073:
	{
		if (!V_0)
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_13 = V_3;
		V_3 = ((int32_t)(L_13+1));
		t1381 * L_14 = &(__this->f2);
		t557 * L_15 = (L_14->f2);
		ArrayElementTypeCheck (V_2, L_15);
		*((t557 **)(t557 **)SZArrayLdElema(V_2, L_13)) = (t557 *)L_15;
	}

IL_0088:
	{
		return V_2;
	}
}
 t557 * m7680 (t1383 * __this, bool p0, MethodInfo* method){
	{
		m7671(__this, 2, &m7671_MI);
		t1381 * L_0 = &(__this->f2);
		t557 * L_1 = (L_0->f2);
		if (!L_1)
		{
			goto IL_0035;
		}
	}
	{
		if (p0)
		{
			goto IL_0029;
		}
	}
	{
		t1381 * L_2 = &(__this->f2);
		t557 * L_3 = (L_2->f2);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7548_MI, L_3);
		if (!L_4)
		{
			goto IL_0035;
		}
	}

IL_0029:
	{
		t1381 * L_5 = &(__this->f2);
		t557 * L_6 = (L_5->f2);
		return L_6;
	}

IL_0035:
	{
		return (t557 *)NULL;
	}
}
extern MethodInfo m7681_MI;
 t638* m7681 (t1383 * __this, MethodInfo* method){
	t638* V_0 = {0};
	t638* V_1 = {0};
	int32_t V_2 = 0;
	t637 * V_3 = {0};
	{
		m7671(__this, 6, &m7671_MI);
		t1381 * L_0 = &(__this->f2);
		t557 * L_1 = (L_0->f2);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		t1381 * L_2 = &(__this->f2);
		t557 * L_3 = (L_2->f2);
		t638* L_4 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, L_3);
		V_0 = L_4;
		goto IL_0063;
	}

IL_0027:
	{
		t1381 * L_5 = &(__this->f2);
		t557 * L_6 = (L_5->f3);
		if (!L_6)
		{
			goto IL_005c;
		}
	}
	{
		t1381 * L_7 = &(__this->f2);
		t557 * L_8 = (L_7->f3);
		t638* L_9 = (t638*)VirtFuncInvoker0< t638* >::Invoke(&m2939_MI, L_8);
		V_1 = L_9;
		V_0 = ((t638*)SZArrayNew(InitializedTypeInfo(&t638_TI), ((int32_t)((((int32_t)(((t20 *)V_1)->max_length)))-1))));
		m5951(NULL, (t20 *)(t20 *)V_1, (t20 *)(t20 *)V_0, (((int32_t)(((t20 *)V_0)->max_length))), &m5951_MI);
		goto IL_0063;
	}

IL_005c:
	{
		return ((t638*)SZArrayNew(InitializedTypeInfo(&t638_TI), 0));
	}

IL_0063:
	{
		V_2 = 0;
		goto IL_0079;
	}

IL_0067:
	{
		int32_t L_10 = V_2;
		V_3 = (*(t637 **)(t637 **)SZArrayLdElema(V_0, L_10));
		t637 * L_11 = (t637 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t637_TI));
		m7696(L_11, V_3, __this, &m7696_MI);
		ArrayElementTypeCheck (V_0, L_11);
		*((t637 **)(t637 **)SZArrayLdElema(V_0, V_2)) = (t637 *)L_11;
		V_2 = ((int32_t)(V_2+1));
	}

IL_0079:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_0)->max_length))))))
		{
			goto IL_0067;
		}
	}
	{
		return V_0;
	}
}
 t557 * m7682 (t1383 * __this, bool p0, MethodInfo* method){
	{
		m7671(__this, 4, &m7671_MI);
		t1381 * L_0 = &(__this->f2);
		t557 * L_1 = (L_0->f3);
		if (!L_1)
		{
			goto IL_0035;
		}
	}
	{
		if (p0)
		{
			goto IL_0029;
		}
	}
	{
		t1381 * L_2 = &(__this->f2);
		t557 * L_3 = (L_2->f3);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m7548_MI, L_3);
		if (!L_4)
		{
			goto IL_0035;
		}
	}

IL_0029:
	{
		t1381 * L_5 = &(__this->f2);
		t557 * L_6 = (L_5->f3);
		return L_6;
	}

IL_0035:
	{
		return (t557 *)NULL;
	}
}
extern MethodInfo m7683_MI;
 bool m7683 (t1383 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		bool L_0 = m9320(NULL, __this, p0, 0, &m9320_MI);
		return L_0;
	}
}
extern MethodInfo m7684_MI;
 t316* m7684 (t1383 * __this, bool p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9319(NULL, __this, 0, &m9319_MI);
		return L_0;
	}
}
extern MethodInfo m7685_MI;
 t316* m7685 (t1383 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9318(NULL, __this, p0, 0, &m9318_MI);
		return L_0;
	}
}
extern MethodInfo m7686_MI;
 t1385 * m7686 (t29 * __this, t557 * p0, MethodInfo* method){
	t537* V_0 = {0};
	t42 * V_1 = {0};
	t29 * V_2 = {0};
	t557 * V_3 = {0};
	t42 * V_4 = {0};
	t7* V_5 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m2952_MI, p0);
		if (!L_0)
		{
			goto IL_002d;
		}
	}
	{
		t537* L_1 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 1));
		t42 * L_2 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7556_MI, p0);
		ArrayElementTypeCheck (L_1, L_2);
		*((t42 **)(t42 **)SZArrayLdElema(L_1, 0)) = (t42 *)L_2;
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_3 = m1554(NULL, LoadTypeToken(&t1387_0_0_0), &m1554_MI);
		V_4 = L_3;
		V_5 = (t7*) &_stringLiteral1561;
		goto IL_0059;
	}

IL_002d:
	{
		t537* L_4 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 2));
		t42 * L_5 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, p0);
		ArrayElementTypeCheck (L_4, L_5);
		*((t42 **)(t42 **)SZArrayLdElema(L_4, 0)) = (t42 *)L_5;
		t537* L_6 = L_4;
		t42 * L_7 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7556_MI, p0);
		ArrayElementTypeCheck (L_6, L_7);
		*((t42 **)(t42 **)SZArrayLdElema(L_6, 1)) = (t42 *)L_7;
		V_0 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_8 = m1554(NULL, LoadTypeToken(&t1386_0_0_0), &m1554_MI);
		V_4 = L_8;
		V_5 = (t7*) &_stringLiteral1562;
	}

IL_0059:
	{
		t42 * L_9 = (t42 *)VirtFuncInvoker1< t42 *, t537* >::Invoke(&m2979_MI, V_4, V_0);
		V_1 = L_9;
		t353 * L_10 = m5848(NULL, V_1, p0, 0, &m5848_MI);
		V_2 = L_10;
		if (V_2)
		{
			goto IL_0074;
		}
	}
	{
		t1650 * L_11 = (t1650 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1650_TI));
		m9291(L_11, &m9291_MI);
		il2cpp_codegen_raise_exception(L_11);
	}

IL_0074:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_12 = m1554(NULL, LoadTypeToken(&t1383_0_0_0), &m1554_MI);
		t557 * L_13 = (t557 *)VirtFuncInvoker2< t557 *, t7*, int32_t >::Invoke(&m6023_MI, L_12, V_5, ((int32_t)40));
		V_3 = L_13;
		t557 * L_14 = (t557 *)VirtFuncInvoker1< t557 *, t537* >::Invoke(&m7557_MI, V_3, V_0);
		V_3 = L_14;
		t42 * L_15 = m1554(NULL, LoadTypeToken(&t1385_0_0_0), &m1554_MI);
		t353 * L_16 = m5847(NULL, L_15, V_2, V_3, 1, &m5847_MI);
		return ((t1385 *)Castclass(L_16, InitializedTypeInfo(&t1385_TI)));
	}
}
extern MethodInfo m7687_MI;
 t29 * m7687 (t1383 * __this, t29 * p0, t316* p1, MethodInfo* method){
	{
		if (!p1)
		{
			goto IL_0008;
		}
	}
	{
		if ((((int32_t)(((t20 *)p1)->max_length))))
		{
			goto IL_0008;
		}
	}

IL_0008:
	{
		t29 * L_0 = (t29 *)VirtFuncInvoker5< t29 *, t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m7688_MI, __this, p0, 0, (t631 *)NULL, p1, (t633 *)NULL);
		return L_0;
	}
}
 t29 * m7688 (t1383 * __this, t29 * p0, int32_t p1, t631 * p2, t316* p3, t633 * p4, MethodInfo* method){
	t29 * V_0 = {0};
	t557 * V_1 = {0};
	t1570 * V_2 = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		V_0 = NULL;
		t557 * L_0 = (t557 *)VirtFuncInvoker1< t557 *, bool >::Invoke(&m7680_MI, __this, 1);
		V_1 = L_0;
		if (V_1)
		{
			goto IL_0028;
		}
	}
	{
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7678_MI, __this);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_2 = m1685(NULL, (t7*) &_stringLiteral1563, L_1, (t7*) &_stringLiteral53, &m1685_MI);
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, L_2, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0028:
	try
	{ // begin try (depth: 1)
		{
			if (!p3)
			{
				goto IL_0032;
			}
		}

IL_002c:
		{
			if ((((int32_t)(((t20 *)p3)->max_length))))
			{
				goto IL_0041;
			}
		}

IL_0032:
		{
			t29 * L_4 = (t29 *)VirtFuncInvoker5< t29 *, t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m10156_MI, V_1, p0, p1, p2, (t316*)(t316*)NULL, p4);
			V_0 = L_4;
			goto IL_004f;
		}

IL_0041:
		{
			t29 * L_5 = (t29 *)VirtFuncInvoker5< t29 *, t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m10156_MI, V_1, p0, p1, p2, p3, p4);
			V_0 = L_5;
		}

IL_004f:
		{
			// IL_004f: leave.s IL_005b
			goto IL_005b;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t1570_TI, e.ex->object.klass))
			goto IL_0051;
		throw e;
	}

IL_0051:
	{ // begin catch(System.Security.SecurityException)
		V_2 = ((t1570 *)__exception_local);
		t1390 * L_6 = (t1390 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1390_TI));
		m7723(L_6, V_2, &m7723_MI);
		il2cpp_codegen_raise_exception(L_6);
		// IL_0059: leave.s IL_005b
		goto IL_005b;
	} // end catch (depth: 1)

IL_005b:
	{
		return V_0;
	}
}
extern MethodInfo m7689_MI;
 void m7689 (t1383 * __this, t29 * p0, t29 * p1, int32_t p2, t631 * p3, t316* p4, t633 * p5, MethodInfo* method){
	t557 * V_0 = {0};
	t316* V_1 = {0};
	int32_t V_2 = 0;
	{
		t557 * L_0 = (t557 *)VirtFuncInvoker1< t557 *, bool >::Invoke(&m7682_MI, __this, 1);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_0026;
		}
	}
	{
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7678_MI, __this);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_2 = m1685(NULL, (t7*) &_stringLiteral1564, L_1, (t7*) &_stringLiteral53, &m1685_MI);
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, L_2, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0026:
	{
		if (!p4)
		{
			goto IL_0030;
		}
	}
	{
		if ((((int32_t)(((t20 *)p4)->max_length))))
		{
			goto IL_003d;
		}
	}

IL_0030:
	{
		t316* L_4 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		ArrayElementTypeCheck (L_4, p1);
		*((t29 **)(t29 **)SZArrayLdElema(L_4, 0)) = (t29 *)p1;
		V_1 = L_4;
		goto IL_0058;
	}

IL_003d:
	{
		V_2 = (((int32_t)(((t20 *)p4)->max_length)));
		V_1 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), ((int32_t)(V_2+1))));
		VirtActionInvoker2< t20 *, int32_t >::Invoke(&m4199_MI, p4, (t20 *)(t20 *)V_1, 0);
		ArrayElementTypeCheck (V_1, p1);
		*((t29 **)(t29 **)SZArrayLdElema(V_1, V_2)) = (t29 *)p1;
	}

IL_0058:
	{
		VirtFuncInvoker5< t29 *, t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m10156_MI, V_0, p0, p2, p3, V_1, p5);
		return;
	}
}
 t7* m7690 (t1383 * __this, MethodInfo* method){
	{
		t42 * L_0 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7675_MI, __this);
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_0);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7678_MI, __this);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m1685(NULL, L_1, (t7*) &_stringLiteral79, L_2, &m1685_MI);
		return L_3;
	}
}
extern MethodInfo m7691_MI;
 t537* m7691 (t1383 * __this, MethodInfo* method){
	t537* V_0 = {0};
	{
		t537* L_0 = m7665(NULL, __this, 1, &m7665_MI);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		return (((t42_SFs*)InitializedTypeInfo(&t42_TI)->static_fields)->f3);
	}

IL_0011:
	{
		return V_0;
	}
}
extern MethodInfo m7692_MI;
 t537* m7692 (t1383 * __this, MethodInfo* method){
	t537* V_0 = {0};
	{
		t537* L_0 = m7665(NULL, __this, 0, &m7665_MI);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		return (((t42_SFs*)InitializedTypeInfo(&t42_TI)->static_fields)->f3);
	}

IL_0011:
	{
		return V_0;
	}
}
extern MethodInfo m7693_MI;
 void m7693 (t1383 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		t7* L_0 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7678_MI, __this);
		t42 * L_1 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m7676_MI, __this);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7690_MI, __this);
		m7537(NULL, p0, L_0, L_1, L_2, ((int32_t)16), &m7537_MI);
		return;
	}
}
// Metadata Definition System.Reflection.MonoProperty
extern Il2CppType t35_0_0_3;
FieldInfo t1383_f0_FieldInfo = 
{
	"klass", &t35_0_0_3, &t1383_TI, offsetof(t1383, f0), &EmptyCustomAttributesCache};
extern Il2CppType t35_0_0_3;
FieldInfo t1383_f1_FieldInfo = 
{
	"prop", &t35_0_0_3, &t1383_TI, offsetof(t1383, f1), &EmptyCustomAttributesCache};
extern Il2CppType t1381_0_0_1;
FieldInfo t1383_f2_FieldInfo = 
{
	"info", &t1381_0_0_1, &t1383_TI, offsetof(t1383, f2), &EmptyCustomAttributesCache};
extern Il2CppType t1384_0_0_1;
FieldInfo t1383_f3_FieldInfo = 
{
	"cached", &t1384_0_0_1, &t1383_TI, offsetof(t1383, f3), &EmptyCustomAttributesCache};
extern Il2CppType t1385_0_0_1;
FieldInfo t1383_f4_FieldInfo = 
{
	"cached_getter", &t1385_0_0_1, &t1383_TI, offsetof(t1383, f4), &EmptyCustomAttributesCache};
static FieldInfo* t1383_FIs[] =
{
	&t1383_f0_FieldInfo,
	&t1383_f1_FieldInfo,
	&t1383_f2_FieldInfo,
	&t1383_f3_FieldInfo,
	&t1383_f4_FieldInfo,
	NULL
};
static PropertyInfo t1383____Attributes_PropertyInfo = 
{
	&t1383_TI, "Attributes", &m7672_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1383____CanRead_PropertyInfo = 
{
	&t1383_TI, "CanRead", &m7673_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1383____CanWrite_PropertyInfo = 
{
	&t1383_TI, "CanWrite", &m7674_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1383____PropertyType_PropertyInfo = 
{
	&t1383_TI, "PropertyType", &m7675_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1383____ReflectedType_PropertyInfo = 
{
	&t1383_TI, "ReflectedType", &m7676_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1383____DeclaringType_PropertyInfo = 
{
	&t1383_TI, "DeclaringType", &m7677_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1383____Name_PropertyInfo = 
{
	&t1383_TI, "Name", &m7678_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1383_PIs[] =
{
	&t1383____Attributes_PropertyInfo,
	&t1383____CanRead_PropertyInfo,
	&t1383____CanWrite_PropertyInfo,
	&t1383____PropertyType_PropertyInfo,
	&t1383____ReflectedType_PropertyInfo,
	&t1383____DeclaringType_PropertyInfo,
	&t1383____Name_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7670_MI = 
{
	".ctor", (methodPointerType)&m7670, &t1383_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3196, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1384_0_0_0;
static ParameterInfo t1383_m7671_ParameterInfos[] = 
{
	{"flags", 0, 134221699, &EmptyCustomAttributesCache, &t1384_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7671_MI = 
{
	"CachePropertyInfo", (methodPointerType)&m7671, &t1383_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1383_m7671_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 3197, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1382_0_0_0;
extern void* RuntimeInvoker_t1382 (MethodInfo* method, void* obj, void** args);
MethodInfo m7672_MI = 
{
	"get_Attributes", (methodPointerType)&m7672, &t1383_TI, &t1382_0_0_0, RuntimeInvoker_t1382, NULL, &EmptyCustomAttributesCache, 2246, 0, 14, 0, false, false, 3198, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7673_MI = 
{
	"get_CanRead", (methodPointerType)&m7673, &t1383_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2246, 0, 15, 0, false, false, 3199, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7674_MI = 
{
	"get_CanWrite", (methodPointerType)&m7674, &t1383_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2246, 0, 16, 0, false, false, 3200, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7675_MI = 
{
	"get_PropertyType", (methodPointerType)&m7675, &t1383_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 17, 0, false, false, 3201, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7676_MI = 
{
	"get_ReflectedType", (methodPointerType)&m7676, &t1383_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 9, 0, false, false, 3202, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7677_MI = 
{
	"get_DeclaringType", (methodPointerType)&m7677, &t1383_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 6, 0, false, false, 3203, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7678_MI = 
{
	"get_Name", (methodPointerType)&m7678, &t1383_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 8, 0, false, false, 3204, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1383_m7679_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221700, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t1145_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7679_MI = 
{
	"GetAccessors", (methodPointerType)&m7679, &t1383_TI, &t1145_0_0_0, RuntimeInvoker_t29_t297, t1383_m7679_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 18, 1, false, false, 3205, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1383_m7680_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221701, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7680_MI = 
{
	"GetGetMethod", (methodPointerType)&m7680, &t1383_TI, &t557_0_0_0, RuntimeInvoker_t29_t297, t1383_m7680_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 19, 1, false, false, 3206, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t638_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7681_MI = 
{
	"GetIndexParameters", (methodPointerType)&m7681, &t1383_TI, &t638_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 20, 0, false, false, 3207, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1383_m7682_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221702, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7682_MI = 
{
	"GetSetMethod", (methodPointerType)&m7682, &t1383_TI, &t557_0_0_0, RuntimeInvoker_t29_t297, t1383_m7682_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 21, 1, false, false, 3208, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1383_m7683_ParameterInfos[] = 
{
	{"attributeType", 0, 134221703, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221704, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7683_MI = 
{
	"IsDefined", (methodPointerType)&m7683, &t1383_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t297, t1383_m7683_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 11, 2, false, false, 3209, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1383_m7684_ParameterInfos[] = 
{
	{"inherit", 0, 134221705, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7684_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m7684, &t1383_TI, &t316_0_0_0, RuntimeInvoker_t29_t297, t1383_m7684_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 12, 1, false, false, 3210, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1383_m7685_ParameterInfos[] = 
{
	{"attributeType", 0, 134221706, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221707, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7685_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m7685, &t1383_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t297, t1383_m7685_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 13, 2, false, false, 3211, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2031_0_0_0;
extern Il2CppType t2031_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1383_m10167_ParameterInfos[] = 
{
	{"getter", 0, 134221708, &EmptyCustomAttributesCache, &t2031_0_0_0},
	{"obj", 1, 134221709, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern Il2CppGenericContainer m10167_IGC;
extern TypeInfo m10167_gp_T_0_TI;
Il2CppGenericParamFull m10167_gp_T_0_TI_GenericParamFull = { { &m10167_IGC, 0}, {NULL, "T", 0, 0, NULL} };
extern TypeInfo m10167_gp_R_1_TI;
Il2CppGenericParamFull m10167_gp_R_1_TI_GenericParamFull = { { &m10167_IGC, 1}, {NULL, "R", 0, 0, NULL} };
static Il2CppGenericParamFull* m10167_IGPA[2] = 
{
	&m10167_gp_T_0_TI_GenericParamFull,
	&m10167_gp_R_1_TI_GenericParamFull,
};
extern MethodInfo m10167_MI;
Il2CppGenericContainer m10167_IGC = { { NULL, NULL }, NULL, &m10167_MI, 2, 1, m10167_IGPA };
extern Il2CppType m10167_gp_0_0_0_0;
extern Il2CppGenericMethod m10168_GM;
extern Il2CppType m10167_gp_1_0_0_0;
static Il2CppRGCTXDefinition m10167_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &m10167_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m10168_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m10167_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m10167_MI = 
{
	"GetterAdapterFrame", NULL, &t1383_TI, &t29_0_0_0, NULL, t1383_m10167_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, true, false, 3212, m10167_RGCTXData, (methodPointerType)NULL, &m10167_IGC};
extern Il2CppType t2034_0_0_0;
extern Il2CppType t2034_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t1383_m10169_ParameterInfos[] = 
{
	{"getter", 0, 134221710, &EmptyCustomAttributesCache, &t2034_0_0_0},
	{"obj", 1, 134221711, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern Il2CppGenericContainer m10169_IGC;
extern TypeInfo m10169_gp_R_0_TI;
Il2CppGenericParamFull m10169_gp_R_0_TI_GenericParamFull = { { &m10169_IGC, 0}, {NULL, "R", 0, 0, NULL} };
static Il2CppGenericParamFull* m10169_IGPA[1] = 
{
	&m10169_gp_R_0_TI_GenericParamFull,
};
extern MethodInfo m10169_MI;
Il2CppGenericContainer m10169_IGC = { { NULL, NULL }, NULL, &m10169_MI, 1, 1, m10169_IGPA };
extern Il2CppGenericMethod m10170_GM;
extern Il2CppType m10169_gp_0_0_0_0;
static Il2CppRGCTXDefinition m10169_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m10170_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m10169_gp_0_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m10169_MI = 
{
	"StaticGetterAdapterFrame", NULL, &t1383_TI, &t29_0_0_0, NULL, t1383_m10169_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, true, false, 3213, m10169_RGCTXData, (methodPointerType)NULL, &m10169_IGC};
extern Il2CppType t557_0_0_0;
static ParameterInfo t1383_m7686_ParameterInfos[] = 
{
	{"method", 0, 134221712, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t1385_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7686_MI = 
{
	"CreateGetterDelegate", (methodPointerType)&m7686, &t1383_TI, &t1385_0_0_0, RuntimeInvoker_t29_t29, t1383_m7686_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 3214, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t1383_m7687_ParameterInfos[] = 
{
	{"obj", 0, 134221713, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"index", 1, 134221714, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7687_MI = 
{
	"GetValue", (methodPointerType)&m7687, &t1383_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t1383_m7687_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 22, 2, false, false, 3215, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t1383_m7688_ParameterInfos[] = 
{
	{"obj", 0, 134221715, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"invokeAttr", 1, 134221716, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 2, 134221717, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"index", 3, 134221718, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"culture", 4, 134221719, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7688_MI = 
{
	"GetValue", (methodPointerType)&m7688, &t1383_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29_t29, t1383_m7688_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 23, 5, false, false, 3216, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t1383_m7689_ParameterInfos[] = 
{
	{"obj", 0, 134221720, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134221721, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"invokeAttr", 2, 134221722, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 3, 134221723, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"index", 4, 134221724, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"culture", 5, 134221725, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7689_MI = 
{
	"SetValue", (methodPointerType)&m7689, &t1383_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t44_t29_t29_t29, t1383_m7689_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 25, 6, false, false, 3217, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7690_MI = 
{
	"ToString", (methodPointerType)&m7690, &t1383_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 3218, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7691_MI = 
{
	"GetOptionalCustomModifiers", (methodPointerType)&m7691, &t1383_TI, &t537_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 26, 0, false, false, 3219, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7692_MI = 
{
	"GetRequiredCustomModifiers", (methodPointerType)&m7692, &t1383_TI, &t537_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 27, 0, false, false, 3220, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1383_m7693_ParameterInfos[] = 
{
	{"info", 0, 134221726, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221727, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7693_MI = 
{
	"GetObjectData", (methodPointerType)&m7693, &t1383_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1383_m7693_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, false, 3221, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1383_MIs[] =
{
	&m7670_MI,
	&m7671_MI,
	&m7672_MI,
	&m7673_MI,
	&m7674_MI,
	&m7675_MI,
	&m7676_MI,
	&m7677_MI,
	&m7678_MI,
	&m7679_MI,
	&m7680_MI,
	&m7681_MI,
	&m7682_MI,
	&m7683_MI,
	&m7684_MI,
	&m7685_MI,
	&m10167_MI,
	&m10169_MI,
	&m7686_MI,
	&m7687_MI,
	&m7688_MI,
	&m7689_MI,
	&m7690_MI,
	&m7691_MI,
	&m7692_MI,
	&m7693_MI,
	NULL
};
extern TypeInfo t1385_TI;
extern TypeInfo t1386_TI;
extern TypeInfo t1387_TI;
static TypeInfo* t1383_TI__nestedTypes[4] =
{
	&t1385_TI,
	&t1386_TI,
	&t1387_TI,
	NULL
};
extern MethodInfo m7712_MI;
extern MethodInfo m7714_MI;
static MethodInfo* t1383_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m7690_MI,
	&m7685_MI,
	&m7683_MI,
	&m7677_MI,
	&m7712_MI,
	&m7678_MI,
	&m7676_MI,
	&m6048_MI,
	&m7683_MI,
	&m7684_MI,
	&m7685_MI,
	&m7672_MI,
	&m7673_MI,
	&m7674_MI,
	&m7675_MI,
	&m7679_MI,
	&m7680_MI,
	&m7681_MI,
	&m7682_MI,
	&m7687_MI,
	&m7688_MI,
	&m7714_MI,
	&m7689_MI,
	&m7691_MI,
	&m7692_MI,
	&m7693_MI,
};
static TypeInfo* t1383_ITIs[] = 
{
	&t374_TI,
};
extern TypeInfo t2036_TI;
static Il2CppInterfaceOffsetPair t1383_IOs[] = 
{
	{ &t2036_TI, 14},
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
	{ &t374_TI, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1383_1_0_0;
struct t1383;
TypeInfo t1383_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MonoProperty", "System.Reflection", t1383_MIs, t1383_PIs, t1383_FIs, NULL, &t1146_TI, t1383_TI__nestedTypes, NULL, &t1383_TI, t1383_ITIs, t1383_VT, &EmptyCustomAttributesCache, &t1383_TI, &t1383_0_0_0, &t1383_1_0_0, t1383_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1383), 0, -1, 0, 0, -1, 1056768, 0, false, false, false, false, false, false, false, false, false, false, false, false, 26, 7, 5, 0, 3, 29, 1, 4};
#include "t1353.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1353_TI;
#include "t1353MD.h"



// Metadata Definition System.Reflection.ParameterAttributes
extern Il2CppType t44_0_0_1542;
FieldInfo t1353_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1353_TI, offsetof(t1353, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1353_0_0_32854;
FieldInfo t1353_f2_FieldInfo = 
{
	"None", &t1353_0_0_32854, &t1353_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1353_0_0_32854;
FieldInfo t1353_f3_FieldInfo = 
{
	"In", &t1353_0_0_32854, &t1353_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1353_0_0_32854;
FieldInfo t1353_f4_FieldInfo = 
{
	"Out", &t1353_0_0_32854, &t1353_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1353_0_0_32854;
FieldInfo t1353_f5_FieldInfo = 
{
	"Lcid", &t1353_0_0_32854, &t1353_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1353_0_0_32854;
FieldInfo t1353_f6_FieldInfo = 
{
	"Retval", &t1353_0_0_32854, &t1353_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1353_0_0_32854;
FieldInfo t1353_f7_FieldInfo = 
{
	"Optional", &t1353_0_0_32854, &t1353_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1353_0_0_32854;
FieldInfo t1353_f8_FieldInfo = 
{
	"ReservedMask", &t1353_0_0_32854, &t1353_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1353_0_0_32854;
FieldInfo t1353_f9_FieldInfo = 
{
	"HasDefault", &t1353_0_0_32854, &t1353_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1353_0_0_32854;
FieldInfo t1353_f10_FieldInfo = 
{
	"HasFieldMarshal", &t1353_0_0_32854, &t1353_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1353_0_0_32854;
FieldInfo t1353_f11_FieldInfo = 
{
	"Reserved3", &t1353_0_0_32854, &t1353_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1353_0_0_32854;
FieldInfo t1353_f12_FieldInfo = 
{
	"Reserved4", &t1353_0_0_32854, &t1353_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1353_FIs[] =
{
	&t1353_f1_FieldInfo,
	&t1353_f2_FieldInfo,
	&t1353_f3_FieldInfo,
	&t1353_f4_FieldInfo,
	&t1353_f5_FieldInfo,
	&t1353_f6_FieldInfo,
	&t1353_f7_FieldInfo,
	&t1353_f8_FieldInfo,
	&t1353_f9_FieldInfo,
	&t1353_f10_FieldInfo,
	&t1353_f11_FieldInfo,
	&t1353_f12_FieldInfo,
	NULL
};
static const int32_t t1353_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1353_f2_DefaultValue = 
{
	&t1353_f2_FieldInfo, { (char*)&t1353_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1353_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1353_f3_DefaultValue = 
{
	&t1353_f3_FieldInfo, { (char*)&t1353_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1353_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1353_f4_DefaultValue = 
{
	&t1353_f4_FieldInfo, { (char*)&t1353_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1353_f5_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1353_f5_DefaultValue = 
{
	&t1353_f5_FieldInfo, { (char*)&t1353_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1353_f6_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t1353_f6_DefaultValue = 
{
	&t1353_f6_FieldInfo, { (char*)&t1353_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1353_f7_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t1353_f7_DefaultValue = 
{
	&t1353_f7_FieldInfo, { (char*)&t1353_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1353_f8_DefaultValueData = 61440;
static Il2CppFieldDefaultValueEntry t1353_f8_DefaultValue = 
{
	&t1353_f8_FieldInfo, { (char*)&t1353_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1353_f9_DefaultValueData = 4096;
static Il2CppFieldDefaultValueEntry t1353_f9_DefaultValue = 
{
	&t1353_f9_FieldInfo, { (char*)&t1353_f9_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1353_f10_DefaultValueData = 8192;
static Il2CppFieldDefaultValueEntry t1353_f10_DefaultValue = 
{
	&t1353_f10_FieldInfo, { (char*)&t1353_f10_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1353_f11_DefaultValueData = 16384;
static Il2CppFieldDefaultValueEntry t1353_f11_DefaultValue = 
{
	&t1353_f11_FieldInfo, { (char*)&t1353_f11_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1353_f12_DefaultValueData = 32768;
static Il2CppFieldDefaultValueEntry t1353_f12_DefaultValue = 
{
	&t1353_f12_FieldInfo, { (char*)&t1353_f12_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1353_FDVs[] = 
{
	&t1353_f2_DefaultValue,
	&t1353_f3_DefaultValue,
	&t1353_f4_DefaultValue,
	&t1353_f5_DefaultValue,
	&t1353_f6_DefaultValue,
	&t1353_f7_DefaultValue,
	&t1353_f8_DefaultValue,
	&t1353_f9_DefaultValue,
	&t1353_f10_DefaultValue,
	&t1353_f11_DefaultValue,
	&t1353_f12_DefaultValue,
	NULL
};
static MethodInfo* t1353_MIs[] =
{
	NULL
};
static MethodInfo* t1353_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1353_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1353_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1353__CustomAttributeCache = {
2,
NULL,
&t1353_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1353_0_0_0;
extern Il2CppType t1353_1_0_0;
extern CustomAttributesCache t1353__CustomAttributeCache;
TypeInfo t1353_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ParameterAttributes", "System.Reflection", t1353_MIs, NULL, t1353_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1353_VT, &t1353__CustomAttributeCache, &t44_TI, &t1353_0_0_0, &t1353_1_0_0, t1353_IOs, NULL, NULL, t1353_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1353)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 12, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif

#include "t1352.h"
#include "t1157.h"
#include "t1159.h"
#include "t1151.h"
extern TypeInfo t1352_TI;
extern TypeInfo t1157_TI;
extern TypeInfo t1159_TI;
extern TypeInfo t1151_TI;
#include "t1352MD.h"
#include "t1157MD.h"
#include "t1159MD.h"
#include "t1151MD.h"
extern Il2CppType t21_0_0_0;
extern MethodInfo m7698_MI;
extern MethodInfo m7404_MI;
extern MethodInfo m7405_MI;
extern MethodInfo m7403_MI;
extern MethodInfo m7705_MI;
extern MethodInfo m2938_MI;
extern MethodInfo m7702_MI;
extern MethodInfo m1312_MI;
extern MethodInfo m66_MI;
extern MethodInfo m7699_MI;
extern MethodInfo m7701_MI;
extern MethodInfo m7700_MI;
extern MethodInfo m6065_MI;
extern MethodInfo m6067_MI;
extern MethodInfo m6061_MI;


extern MethodInfo m7694_MI;
 void m7694 (t637 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7695_MI;
 void m7695 (t637 * __this, t1352 * p0, t42 * p1, t296 * p2, int32_t p3, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p1;
		__this->f2 = p2;
		if (!p0)
		{
			goto IL_003f;
		}
	}
	{
		t7* L_0 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7404_MI, p0);
		__this->f3 = L_0;
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7405_MI, p0);
		__this->f4 = ((int32_t)(L_1-1));
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7403_MI, p0);
		__this->f5 = L_2;
		goto IL_0057;
	}

IL_003f:
	{
		__this->f3 = (t7*)NULL;
		__this->f4 = ((int32_t)(p3-1));
		__this->f5 = 0;
	}

IL_0057:
	{
		return;
	}
}
 void m7696 (t637 * __this, t637 * p0, t296 * p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		t42 * L_0 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2940_MI, p0);
		__this->f0 = L_0;
		__this->f2 = p1;
		t7* L_1 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m7704_MI, p0);
		__this->f3 = L_1;
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7705_MI, p0);
		__this->f4 = L_2;
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7698_MI, p0);
		__this->f5 = L_3;
		return;
	}
}
extern MethodInfo m7697_MI;
 t7* m7697 (t637 * __this, MethodInfo* method){
	t42 * V_0 = {0};
	bool V_1 = false;
	t7* V_2 = {0};
	int32_t G_B7_0 = 0;
	t7* G_B10_0 = {0};
	{
		t42 * L_0 = (__this->f0);
		V_0 = L_0;
		goto IL_0010;
	}

IL_0009:
	{
		t42 * L_1 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m9711_MI, V_0);
		V_0 = L_1;
	}

IL_0010:
	{
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5991_MI, V_0);
		if (L_2)
		{
			goto IL_0009;
		}
	}
	{
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&m3000_MI, V_0);
		if (L_3)
		{
			goto IL_0054;
		}
	}
	{
		t42 * L_4 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_5 = m1554(NULL, LoadTypeToken(&t21_0_0_0), &m1554_MI);
		if ((((t42 *)L_4) == ((t42 *)L_5)))
		{
			goto IL_0054;
		}
	}
	{
		t42 * L_6 = (__this->f0);
		t7* L_7 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2938_MI, L_6);
		t296 * L_8 = (__this->f2);
		t42 * L_9 = (t42 *)VirtFuncInvoker0< t42 * >::Invoke(&m2937_MI, L_8);
		t7* L_10 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2938_MI, L_9);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_11 = m1713(NULL, L_7, L_10, &m1713_MI);
		G_B7_0 = ((int32_t)(L_11));
		goto IL_0055;
	}

IL_0054:
	{
		G_B7_0 = 1;
	}

IL_0055:
	{
		V_1 = G_B7_0;
		if (!V_1)
		{
			goto IL_0066;
		}
	}
	{
		t42 * L_12 = (__this->f0);
		t7* L_13 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2924_MI, L_12);
		G_B10_0 = L_13;
		goto IL_0071;
	}

IL_0066:
	{
		t42 * L_14 = (__this->f0);
		t7* L_15 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_14);
		G_B10_0 = L_15;
	}

IL_0071:
	{
		V_2 = G_B10_0;
		bool L_16 = m7702(__this, &m7702_MI);
		if (L_16)
		{
			goto IL_0095;
		}
	}
	{
		uint16_t L_17 = ((int32_t)32);
		t29 * L_18 = Box(InitializedTypeInfo(&t194_TI), &L_17);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_19 = m1312(NULL, V_2, L_18, &m1312_MI);
		V_2 = L_19;
		t7* L_20 = (__this->f3);
		t7* L_21 = m66(NULL, V_2, L_20, &m66_MI);
		V_2 = L_21;
	}

IL_0095:
	{
		return V_2;
	}
}
 t42 * m2940 (t637 * __this, MethodInfo* method){
	{
		t42 * L_0 = (__this->f0);
		return L_0;
	}
}
 int32_t m7698 (t637 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f5);
		return L_0;
	}
}
 bool m7699 (t637 * __this, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7698_MI, __this);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 bool m7700 (t637 * __this, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7698_MI, __this);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)16)))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 bool m7701 (t637 * __this, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7698_MI, __this);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
 bool m7702 (t637 * __this, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m7698_MI, __this);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)8))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m7703_MI;
 t296 * m7703 (t637 * __this, MethodInfo* method){
	{
		t296 * L_0 = (__this->f2);
		return L_0;
	}
}
 t7* m7704 (t637 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f3);
		return L_0;
	}
}
 int32_t m7705 (t637 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m7706_MI;
 t316* m7706 (t637 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		t316* L_0 = m9318(NULL, __this, p0, p1, &m9318_MI);
		return L_0;
	}
}
extern MethodInfo m7707_MI;
 bool m7707 (t637 * __this, t42 * p0, bool p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1656_TI));
		bool L_0 = m9320(NULL, __this, p0, p1, &m9320_MI);
		return L_0;
	}
}
extern MethodInfo m7708_MI;
 t316* m7708 (t637 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t316* V_1 = {0};
	{
		V_0 = 0;
		bool L_0 = m7699(__this, &m7699_MI);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_000e:
	{
		bool L_1 = m7701(__this, &m7701_MI);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_001a:
	{
		bool L_2 = m7700(__this, &m7700_MI);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0026:
	{
		t1346 * L_3 = (__this->f6);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0032:
	{
		if (V_0)
		{
			goto IL_0037;
		}
	}
	{
		return (t316*)NULL;
	}

IL_0037:
	{
		V_1 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), V_0));
		V_0 = 0;
		bool L_4 = m7699(__this, &m7699_MI);
		if (!L_4)
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)(L_5+1));
		t1157 * L_6 = (t1157 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1157_TI));
		m6065(L_6, &m6065_MI);
		ArrayElementTypeCheck (V_1, L_6);
		*((t29 **)(t29 **)SZArrayLdElema(V_1, L_5)) = (t29 *)L_6;
	}

IL_0054:
	{
		bool L_7 = m7700(__this, &m7700_MI);
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)(L_8+1));
		t1159 * L_9 = (t1159 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1159_TI));
		m6067(L_9, &m6067_MI);
		ArrayElementTypeCheck (V_1, L_9);
		*((t29 **)(t29 **)SZArrayLdElema(V_1, L_8)) = (t29 *)L_9;
	}

IL_0068:
	{
		bool L_10 = m7701(__this, &m7701_MI);
		if (!L_10)
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)(L_11+1));
		t1151 * L_12 = (t1151 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1151_TI));
		m6061(L_12, &m6061_MI);
		ArrayElementTypeCheck (V_1, L_12);
		*((t29 **)(t29 **)SZArrayLdElema(V_1, L_11)) = (t29 *)L_12;
	}

IL_007c:
	{
		t1346 * L_13 = (__this->f6);
		if (!L_13)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)(L_14+1));
		t1346 * L_15 = (__this->f6);
		t1155 * L_16 = m7454(L_15, &m7454_MI);
		ArrayElementTypeCheck (V_1, L_16);
		*((t29 **)(t29 **)SZArrayLdElema(V_1, L_14)) = (t29 *)L_16;
	}

IL_0096:
	{
		return V_1;
	}
}
// Metadata Definition System.Reflection.ParameterInfo
extern Il2CppType t42_0_0_4;
FieldInfo t637_f0_FieldInfo = 
{
	"ClassImpl", &t42_0_0_4, &t637_TI, offsetof(t637, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_4;
FieldInfo t637_f1_FieldInfo = 
{
	"DefaultValueImpl", &t29_0_0_4, &t637_TI, offsetof(t637, f1), &EmptyCustomAttributesCache};
extern Il2CppType t296_0_0_4;
FieldInfo t637_f2_FieldInfo = 
{
	"MemberImpl", &t296_0_0_4, &t637_TI, offsetof(t637, f2), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_4;
FieldInfo t637_f3_FieldInfo = 
{
	"NameImpl", &t7_0_0_4, &t637_TI, offsetof(t637, f3), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_4;
FieldInfo t637_f4_FieldInfo = 
{
	"PositionImpl", &t44_0_0_4, &t637_TI, offsetof(t637, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1353_0_0_4;
FieldInfo t637_f5_FieldInfo = 
{
	"AttrsImpl", &t1353_0_0_4, &t637_TI, offsetof(t637, f5), &EmptyCustomAttributesCache};
extern Il2CppType t1346_0_0_1;
FieldInfo t637_f6_FieldInfo = 
{
	"marshalAs", &t1346_0_0_1, &t637_TI, offsetof(t637, f6), &EmptyCustomAttributesCache};
static FieldInfo* t637_FIs[] =
{
	&t637_f0_FieldInfo,
	&t637_f1_FieldInfo,
	&t637_f2_FieldInfo,
	&t637_f3_FieldInfo,
	&t637_f4_FieldInfo,
	&t637_f5_FieldInfo,
	&t637_f6_FieldInfo,
	NULL
};
static PropertyInfo t637____ParameterType_PropertyInfo = 
{
	&t637_TI, "ParameterType", &m2940_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t637____Attributes_PropertyInfo = 
{
	&t637_TI, "Attributes", &m7698_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t637____IsIn_PropertyInfo = 
{
	&t637_TI, "IsIn", &m7699_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t637____IsOptional_PropertyInfo = 
{
	&t637_TI, "IsOptional", &m7700_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t637____IsOut_PropertyInfo = 
{
	&t637_TI, "IsOut", &m7701_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t637____IsRetval_PropertyInfo = 
{
	&t637_TI, "IsRetval", &m7702_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t637____Member_PropertyInfo = 
{
	&t637_TI, "Member", &m7703_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t637____Name_PropertyInfo = 
{
	&t637_TI, "Name", &m7704_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t637____Position_PropertyInfo = 
{
	&t637_TI, "Position", &m7705_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t637_PIs[] =
{
	&t637____ParameterType_PropertyInfo,
	&t637____Attributes_PropertyInfo,
	&t637____IsIn_PropertyInfo,
	&t637____IsOptional_PropertyInfo,
	&t637____IsOut_PropertyInfo,
	&t637____IsRetval_PropertyInfo,
	&t637____Member_PropertyInfo,
	&t637____Name_PropertyInfo,
	&t637____Position_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7694_MI = 
{
	".ctor", (methodPointerType)&m7694, &t637_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 3234, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1352_0_0_0;
extern Il2CppType t1352_0_0_0;
extern Il2CppType t42_0_0_0;
extern Il2CppType t296_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t637_m7695_ParameterInfos[] = 
{
	{"pb", 0, 134221747, &EmptyCustomAttributesCache, &t1352_0_0_0},
	{"type", 1, 134221748, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"member", 2, 134221749, &EmptyCustomAttributesCache, &t296_0_0_0},
	{"position", 3, 134221750, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7695_MI = 
{
	".ctor", (methodPointerType)&m7695, &t637_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29_t44, t637_m7695_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 4, false, false, 3235, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t637_0_0_0;
extern Il2CppType t637_0_0_0;
extern Il2CppType t296_0_0_0;
static ParameterInfo t637_m7696_ParameterInfos[] = 
{
	{"pinfo", 0, 134221751, &EmptyCustomAttributesCache, &t637_0_0_0},
	{"member", 1, 134221752, &EmptyCustomAttributesCache, &t296_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7696_MI = 
{
	".ctor", (methodPointerType)&m7696, &t637_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t637_m7696_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 2, false, false, 3236, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7697_MI = 
{
	"ToString", (methodPointerType)&m7697, &t637_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 3237, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2940_MI = 
{
	"get_ParameterType", (methodPointerType)&m2940, &t637_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 6, 0, false, false, 3238, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1353_0_0_0;
extern void* RuntimeInvoker_t1353 (MethodInfo* method, void* obj, void** args);
MethodInfo m7698_MI = 
{
	"get_Attributes", (methodPointerType)&m7698, &t637_TI, &t1353_0_0_0, RuntimeInvoker_t1353, NULL, &EmptyCustomAttributesCache, 2502, 0, 7, 0, false, false, 3239, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7699_MI = 
{
	"get_IsIn", (methodPointerType)&m7699, &t637_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3240, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7700_MI = 
{
	"get_IsOptional", (methodPointerType)&m7700, &t637_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3241, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7701_MI = 
{
	"get_IsOut", (methodPointerType)&m7701, &t637_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3242, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7702_MI = 
{
	"get_IsRetval", (methodPointerType)&m7702, &t637_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3243, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t296_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7703_MI = 
{
	"get_Member", (methodPointerType)&m7703, &t637_TI, &t296_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 8, 0, false, false, 3244, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7704_MI = 
{
	"get_Name", (methodPointerType)&m7704, &t637_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 9, 0, false, false, 3245, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7705_MI = 
{
	"get_Position", (methodPointerType)&m7705, &t637_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2502, 0, 10, 0, false, false, 3246, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t637_m7706_ParameterInfos[] = 
{
	{"attributeType", 0, 134221753, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221754, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7706_MI = 
{
	"GetCustomAttributes", (methodPointerType)&m7706, &t637_TI, &t316_0_0_0, RuntimeInvoker_t29_t29_t297, t637_m7706_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 11, 2, false, false, 3247, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t637_m7707_ParameterInfos[] = 
{
	{"attributeType", 0, 134221755, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"inherit", 1, 134221756, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m7707_MI = 
{
	"IsDefined", (methodPointerType)&m7707, &t637_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t297, t637_m7707_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 12, 2, false, false, 3248, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t316_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7708_MI = 
{
	"GetPseudoCustomAttributes", (methodPointerType)&m7708, &t637_TI, &t316_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 131, 0, 255, 0, false, false, 3249, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t637_MIs[] =
{
	&m7694_MI,
	&m7695_MI,
	&m7696_MI,
	&m7697_MI,
	&m2940_MI,
	&m7698_MI,
	&m7699_MI,
	&m7700_MI,
	&m7701_MI,
	&m7702_MI,
	&m7703_MI,
	&m7704_MI,
	&m7705_MI,
	&m7706_MI,
	&m7707_MI,
	&m7708_MI,
	NULL
};
static MethodInfo* t637_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m7697_MI,
	&m7706_MI,
	&m7707_MI,
	&m2940_MI,
	&m7698_MI,
	&m7703_MI,
	&m7704_MI,
	&m7705_MI,
	&m7706_MI,
	&m7707_MI,
};
extern TypeInfo t2037_TI;
static TypeInfo* t637_ITIs[] = 
{
	&t1657_TI,
	&t2037_TI,
};
static Il2CppInterfaceOffsetPair t637_IOs[] = 
{
	{ &t1657_TI, 4},
	{ &t2037_TI, 6},
};
void t637_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1402 * tmp;
		tmp = (t1402 *)il2cpp_codegen_object_new (&t1402_TI);
		m7733(tmp, 0, &m7733_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1404 * tmp;
		tmp = (t1404 *)il2cpp_codegen_object_new (&t1404_TI);
		m7734(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2037_TI)), &m7734_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t637__CustomAttributeCache = {
3,
NULL,
&t637_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t637_1_0_0;
struct t637;
extern CustomAttributesCache t637__CustomAttributeCache;
TypeInfo t637_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ParameterInfo", "System.Reflection", t637_MIs, t637_PIs, t637_FIs, NULL, &t29_TI, NULL, NULL, &t637_TI, t637_ITIs, t637_VT, &t637__CustomAttributeCache, &t637_TI, &t637_0_0_0, &t637_1_0_0, t637_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t637), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 16, 9, 7, 0, 0, 13, 2, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t632_TI;
#include "t632MD.h"



// Conversion methods for marshalling of: System.Reflection.ParameterModifier
void t632_marshal(const t632& unmarshaled, t632_marshaled& marshaled)
{
	marshaled.f0 = il2cpp_codegen_marshal_array<int32_t>((Il2CppCodeGenArray*)unmarshaled.f0);
}
void t632_marshal_back(const t632_marshaled& marshaled, t632& unmarshaled)
{
	extern TypeInfo t40_TI;
	unmarshaled.f0 = (t771*)il2cpp_codegen_marshal_array_result(&t40_TI, marshaled.f0, 1);
}
// Conversion method for clean up from marshalling of: System.Reflection.ParameterModifier
void t632_marshal_cleanup(t632_marshaled& marshaled)
{
}
// Metadata Definition System.Reflection.ParameterModifier
extern Il2CppType t771_0_0_1;
FieldInfo t632_f0_FieldInfo = 
{
	"_byref", &t771_0_0_1, &t632_TI, offsetof(t632, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t632_FIs[] =
{
	&t632_f0_FieldInfo,
	NULL
};
static MethodInfo* t632_MIs[] =
{
	NULL
};
static MethodInfo* t632_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
extern TypeInfo t425_TI;
#include "t425.h"
#include "t425MD.h"
extern MethodInfo m2025_MI;
void t632_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t425 * tmp;
		tmp = (t425 *)il2cpp_codegen_object_new (&t425_TI);
		m2025(tmp, il2cpp_codegen_string_new_wrapper("Item"), &m2025_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t632__CustomAttributeCache = {
2,
NULL,
&t632_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t632_0_0_0;
extern Il2CppType t632_1_0_0;
extern CustomAttributesCache t632__CustomAttributeCache;
TypeInfo t632_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ParameterModifier", "System.Reflection", t632_MIs, NULL, t632_FIs, NULL, &t110_TI, NULL, NULL, &t632_TI, NULL, t632_VT, &t632__CustomAttributeCache, &t632_TI, &t632_0_0_0, &t632_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)t632_marshal, (methodPointerType)t632_marshal_back, (methodPointerType)t632_marshal_cleanup, sizeof (t632)+ sizeof (Il2CppObject), 0, sizeof(t632_marshaled), 0, 0, -1, 1057033, 0, true, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 1, 0, 0, 4, 0, 0};
#include "t1388.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1388_TI;
#include "t1388MD.h"



extern MethodInfo m7709_MI;
 void m7709 (t1388 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7710_MI;
 void m7710 (t1388 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1565, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Reflection.Pointer
extern Il2CppType t1761_0_0_1;
FieldInfo t1388_f0_FieldInfo = 
{
	"data", &t1761_0_0_1, &t1388_TI, offsetof(t1388, f0), &EmptyCustomAttributesCache};
extern Il2CppType t42_0_0_1;
FieldInfo t1388_f1_FieldInfo = 
{
	"type", &t42_0_0_1, &t1388_TI, offsetof(t1388, f1), &EmptyCustomAttributesCache};
static FieldInfo* t1388_FIs[] =
{
	&t1388_f0_FieldInfo,
	&t1388_f1_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7709_MI = 
{
	".ctor", (methodPointerType)&m7709, &t1388_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6273, 0, 255, 0, false, false, 3250, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1388_m7710_ParameterInfos[] = 
{
	{"info", 0, 134221757, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221758, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7710_MI = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData", (methodPointerType)&m7710, &t1388_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1388_m7710_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 4, 2, false, false, 3251, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1388_MIs[] =
{
	&m7709_MI,
	&m7710_MI,
	NULL
};
static MethodInfo* t1388_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7710_MI,
};
static TypeInfo* t1388_ITIs[] = 
{
	&t374_TI,
};
static Il2CppInterfaceOffsetPair t1388_IOs[] = 
{
	{ &t374_TI, 4},
};
extern TypeInfo t707_TI;
#include "t707.h"
#include "t707MD.h"
extern MethodInfo m3062_MI;
void t1388_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1388__CustomAttributeCache = {
2,
NULL,
&t1388_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1388_0_0_0;
extern Il2CppType t1388_1_0_0;
struct t1388;
extern CustomAttributesCache t1388__CustomAttributeCache;
TypeInfo t1388_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Pointer", "System.Reflection", t1388_MIs, NULL, t1388_FIs, NULL, &t29_TI, NULL, NULL, &t1388_TI, t1388_ITIs, t1388_VT, &t1388__CustomAttributeCache, &t1388_TI, &t1388_0_0_0, &t1388_1_0_0, t1388_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1388), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 2, 0, 0, 5, 1, 1};
#include "t1363.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1363_TI;
#include "t1363MD.h"



// Metadata Definition System.Reflection.ProcessorArchitecture
extern Il2CppType t44_0_0_1542;
FieldInfo t1363_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1363_TI, offsetof(t1363, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1363_0_0_32854;
FieldInfo t1363_f2_FieldInfo = 
{
	"None", &t1363_0_0_32854, &t1363_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1363_0_0_32854;
FieldInfo t1363_f3_FieldInfo = 
{
	"MSIL", &t1363_0_0_32854, &t1363_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1363_0_0_32854;
FieldInfo t1363_f4_FieldInfo = 
{
	"X86", &t1363_0_0_32854, &t1363_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1363_0_0_32854;
FieldInfo t1363_f5_FieldInfo = 
{
	"IA64", &t1363_0_0_32854, &t1363_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1363_0_0_32854;
FieldInfo t1363_f6_FieldInfo = 
{
	"Amd64", &t1363_0_0_32854, &t1363_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1363_FIs[] =
{
	&t1363_f1_FieldInfo,
	&t1363_f2_FieldInfo,
	&t1363_f3_FieldInfo,
	&t1363_f4_FieldInfo,
	&t1363_f5_FieldInfo,
	&t1363_f6_FieldInfo,
	NULL
};
static const int32_t t1363_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1363_f2_DefaultValue = 
{
	&t1363_f2_FieldInfo, { (char*)&t1363_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1363_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1363_f3_DefaultValue = 
{
	&t1363_f3_FieldInfo, { (char*)&t1363_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1363_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1363_f4_DefaultValue = 
{
	&t1363_f4_FieldInfo, { (char*)&t1363_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1363_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1363_f5_DefaultValue = 
{
	&t1363_f5_FieldInfo, { (char*)&t1363_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1363_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1363_f6_DefaultValue = 
{
	&t1363_f6_FieldInfo, { (char*)&t1363_f6_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1363_FDVs[] = 
{
	&t1363_f2_DefaultValue,
	&t1363_f3_DefaultValue,
	&t1363_f4_DefaultValue,
	&t1363_f5_DefaultValue,
	&t1363_f6_DefaultValue,
	NULL
};
static MethodInfo* t1363_MIs[] =
{
	NULL
};
static MethodInfo* t1363_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1363_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1363_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1363__CustomAttributeCache = {
1,
NULL,
&t1363_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1363_0_0_0;
extern Il2CppType t1363_1_0_0;
extern CustomAttributesCache t1363__CustomAttributeCache;
TypeInfo t1363_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ProcessorArchitecture", "System.Reflection", t1363_MIs, NULL, t1363_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1363_VT, &t1363__CustomAttributeCache, &t44_TI, &t1363_0_0_0, &t1363_1_0_0, t1363_IOs, NULL, NULL, t1363_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1363)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 6, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1382_TI;
#include "t1382MD.h"



// Metadata Definition System.Reflection.PropertyAttributes
extern Il2CppType t44_0_0_1542;
FieldInfo t1382_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1382_TI, offsetof(t1382, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1382_0_0_32854;
FieldInfo t1382_f2_FieldInfo = 
{
	"None", &t1382_0_0_32854, &t1382_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1382_0_0_32854;
FieldInfo t1382_f3_FieldInfo = 
{
	"SpecialName", &t1382_0_0_32854, &t1382_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1382_0_0_32854;
FieldInfo t1382_f4_FieldInfo = 
{
	"ReservedMask", &t1382_0_0_32854, &t1382_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1382_0_0_32854;
FieldInfo t1382_f5_FieldInfo = 
{
	"RTSpecialName", &t1382_0_0_32854, &t1382_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1382_0_0_32854;
FieldInfo t1382_f6_FieldInfo = 
{
	"HasDefault", &t1382_0_0_32854, &t1382_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1382_0_0_32854;
FieldInfo t1382_f7_FieldInfo = 
{
	"Reserved2", &t1382_0_0_32854, &t1382_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1382_0_0_32854;
FieldInfo t1382_f8_FieldInfo = 
{
	"Reserved3", &t1382_0_0_32854, &t1382_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1382_0_0_32854;
FieldInfo t1382_f9_FieldInfo = 
{
	"Reserved4", &t1382_0_0_32854, &t1382_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1382_FIs[] =
{
	&t1382_f1_FieldInfo,
	&t1382_f2_FieldInfo,
	&t1382_f3_FieldInfo,
	&t1382_f4_FieldInfo,
	&t1382_f5_FieldInfo,
	&t1382_f6_FieldInfo,
	&t1382_f7_FieldInfo,
	&t1382_f8_FieldInfo,
	&t1382_f9_FieldInfo,
	NULL
};
static const int32_t t1382_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1382_f2_DefaultValue = 
{
	&t1382_f2_FieldInfo, { (char*)&t1382_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1382_f3_DefaultValueData = 512;
static Il2CppFieldDefaultValueEntry t1382_f3_DefaultValue = 
{
	&t1382_f3_FieldInfo, { (char*)&t1382_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1382_f4_DefaultValueData = 62464;
static Il2CppFieldDefaultValueEntry t1382_f4_DefaultValue = 
{
	&t1382_f4_FieldInfo, { (char*)&t1382_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1382_f5_DefaultValueData = 1024;
static Il2CppFieldDefaultValueEntry t1382_f5_DefaultValue = 
{
	&t1382_f5_FieldInfo, { (char*)&t1382_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1382_f6_DefaultValueData = 4096;
static Il2CppFieldDefaultValueEntry t1382_f6_DefaultValue = 
{
	&t1382_f6_FieldInfo, { (char*)&t1382_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1382_f7_DefaultValueData = 8192;
static Il2CppFieldDefaultValueEntry t1382_f7_DefaultValue = 
{
	&t1382_f7_FieldInfo, { (char*)&t1382_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1382_f8_DefaultValueData = 16384;
static Il2CppFieldDefaultValueEntry t1382_f8_DefaultValue = 
{
	&t1382_f8_FieldInfo, { (char*)&t1382_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1382_f9_DefaultValueData = 32768;
static Il2CppFieldDefaultValueEntry t1382_f9_DefaultValue = 
{
	&t1382_f9_FieldInfo, { (char*)&t1382_f9_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1382_FDVs[] = 
{
	&t1382_f2_DefaultValue,
	&t1382_f3_DefaultValue,
	&t1382_f4_DefaultValue,
	&t1382_f5_DefaultValue,
	&t1382_f6_DefaultValue,
	&t1382_f7_DefaultValue,
	&t1382_f8_DefaultValue,
	&t1382_f9_DefaultValue,
	NULL
};
static MethodInfo* t1382_MIs[] =
{
	NULL
};
static MethodInfo* t1382_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1382_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1382_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1382__CustomAttributeCache = {
2,
NULL,
&t1382_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1382_0_0_0;
extern Il2CppType t1382_1_0_0;
extern CustomAttributesCache t1382__CustomAttributeCache;
TypeInfo t1382_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PropertyAttributes", "System.Reflection", t1382_MIs, NULL, t1382_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1382_VT, &t1382__CustomAttributeCache, &t44_TI, &t1382_0_0_0, &t1382_1_0_0, t1382_IOs, NULL, NULL, t1382_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1382)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 9, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m10171_MI;
extern MethodInfo m10172_MI;


 void m7711 (t1146 * __this, MethodInfo* method){
	{
		m6047(__this, &m6047_MI);
		return;
	}
}
 int32_t m7712 (t1146 * __this, MethodInfo* method){
	{
		return (int32_t)(((int32_t)16));
	}
}
extern MethodInfo m7713_MI;
 t29 * m7713 (t1146 * __this, t29 * p0, t316* p1, MethodInfo* method){
	{
		t29 * L_0 = (t29 *)VirtFuncInvoker5< t29 *, t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m10171_MI, __this, p0, 0, (t631 *)NULL, p1, (t633 *)NULL);
		return L_0;
	}
}
 void m7714 (t1146 * __this, t29 * p0, t29 * p1, t316* p2, MethodInfo* method){
	{
		VirtActionInvoker6< t29 *, t29 *, int32_t, t631 *, t316*, t633 * >::Invoke(&m10172_MI, __this, p0, p1, 0, (t631 *)NULL, p2, (t633 *)NULL);
		return;
	}
}
extern MethodInfo m7715_MI;
 t537* m7715 (t1146 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		return (((t42_SFs*)InitializedTypeInfo(&t42_TI)->static_fields)->f3);
	}
}
extern MethodInfo m7716_MI;
 t537* m7716 (t1146 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		return (((t42_SFs*)InitializedTypeInfo(&t42_TI)->static_fields)->f3);
	}
}
// Metadata Definition System.Reflection.PropertyInfo
extern MethodInfo m9844_MI;
static PropertyInfo t1146____Attributes_PropertyInfo = 
{
	&t1146_TI, "Attributes", &m9844_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10173_MI;
static PropertyInfo t1146____CanRead_PropertyInfo = 
{
	&t1146_TI, "CanRead", &m10173_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m10174_MI;
static PropertyInfo t1146____CanWrite_PropertyInfo = 
{
	&t1146_TI, "CanWrite", &m10174_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1146____MemberType_PropertyInfo = 
{
	&t1146_TI, "MemberType", &m7712_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1146____PropertyType_PropertyInfo = 
{
	&t1146_TI, "PropertyType", &m10144_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1146_PIs[] =
{
	&t1146____Attributes_PropertyInfo,
	&t1146____CanRead_PropertyInfo,
	&t1146____CanWrite_PropertyInfo,
	&t1146____MemberType_PropertyInfo,
	&t1146____PropertyType_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7711_MI = 
{
	".ctor", (methodPointerType)&m7711, &t1146_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 3252, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1382_0_0_0;
extern void* RuntimeInvoker_t1382 (MethodInfo* method, void* obj, void** args);
MethodInfo m9844_MI = 
{
	"get_Attributes", NULL, &t1146_TI, &t1382_0_0_0, RuntimeInvoker_t1382, NULL, &EmptyCustomAttributesCache, 3526, 0, 14, 0, false, false, 3253, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m10173_MI = 
{
	"get_CanRead", NULL, &t1146_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 15, 0, false, false, 3254, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m10174_MI = 
{
	"get_CanWrite", NULL, &t1146_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 16, 0, false, false, 3255, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t1149_0_0_0;
extern void* RuntimeInvoker_t1149 (MethodInfo* method, void* obj, void** args);
MethodInfo m7712_MI = 
{
	"get_MemberType", (methodPointerType)&m7712, &t1146_TI, &t1149_0_0_0, RuntimeInvoker_t1149, NULL, &EmptyCustomAttributesCache, 2246, 0, 7, 0, false, false, 3256, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10144_MI = 
{
	"get_PropertyType", NULL, &t1146_TI, &t42_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 17, 0, false, false, 3257, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1146_m10175_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221759, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t1145_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m10175_MI = 
{
	"GetAccessors", NULL, &t1146_TI, &t1145_0_0_0, RuntimeInvoker_t29_t297, t1146_m10175_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 18, 1, false, false, 3258, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1146_m10176_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221760, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m10176_MI = 
{
	"GetGetMethod", NULL, &t1146_TI, &t557_0_0_0, RuntimeInvoker_t29_t297, t1146_m10176_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 19, 1, false, false, 3259, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t638_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10143_MI = 
{
	"GetIndexParameters", NULL, &t1146_TI, &t638_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 20, 0, false, false, 3260, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1146_m10177_ParameterInfos[] = 
{
	{"nonPublic", 0, 134221761, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m10177_MI = 
{
	"GetSetMethod", NULL, &t1146_TI, &t557_0_0_0, RuntimeInvoker_t29_t297, t1146_m10177_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 21, 1, false, false, 3261, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t1146_m7713_ParameterInfos[] = 
{
	{"obj", 0, 134221762, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"index", 1, 134221763, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1146__CustomAttributeCache_m7713;
MethodInfo m7713_MI = 
{
	"GetValue", (methodPointerType)&m7713, &t1146_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t1146_m7713_ParameterInfos, &t1146__CustomAttributeCache_m7713, 454, 0, 22, 2, false, false, 3262, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t1146_m10171_ParameterInfos[] = 
{
	{"obj", 0, 134221764, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"invokeAttr", 1, 134221765, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 2, 134221766, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"index", 3, 134221767, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"culture", 4, 134221768, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10171_MI = 
{
	"GetValue", NULL, &t1146_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29_t29, t1146_m10171_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 23, 5, false, false, 3263, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t1146_m7714_ParameterInfos[] = 
{
	{"obj", 0, 134221769, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134221770, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"index", 2, 134221771, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1146__CustomAttributeCache_m7714;
MethodInfo m7714_MI = 
{
	"SetValue", (methodPointerType)&m7714, &t1146_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t1146_m7714_ParameterInfos, &t1146__CustomAttributeCache_m7714, 454, 0, 24, 3, false, false, 3264, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t630_0_0_0;
extern Il2CppType t631_0_0_0;
extern Il2CppType t316_0_0_0;
extern Il2CppType t633_0_0_0;
static ParameterInfo t1146_m10172_ParameterInfos[] = 
{
	{"obj", 0, 134221772, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134221773, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"invokeAttr", 2, 134221774, &EmptyCustomAttributesCache, &t630_0_0_0},
	{"binder", 3, 134221775, &EmptyCustomAttributesCache, &t631_0_0_0},
	{"index", 4, 134221776, &EmptyCustomAttributesCache, &t316_0_0_0},
	{"culture", 5, 134221777, &EmptyCustomAttributesCache, &t633_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m10172_MI = 
{
	"SetValue", NULL, &t1146_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t44_t29_t29_t29, t1146_m10172_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 25, 6, false, false, 3265, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7715_MI = 
{
	"GetOptionalCustomModifiers", (methodPointerType)&m7715, &t1146_TI, &t537_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 454, 0, 26, 0, false, false, 3266, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7716_MI = 
{
	"GetRequiredCustomModifiers", (methodPointerType)&m7716, &t1146_TI, &t537_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 454, 0, 27, 0, false, false, 3267, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1146_MIs[] =
{
	&m7711_MI,
	&m9844_MI,
	&m10173_MI,
	&m10174_MI,
	&m7712_MI,
	&m10144_MI,
	&m10175_MI,
	&m10176_MI,
	&m10143_MI,
	&m10177_MI,
	&m7713_MI,
	&m10171_MI,
	&m7714_MI,
	&m10172_MI,
	&m7715_MI,
	&m7716_MI,
	NULL
};
static MethodInfo* t1146_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m2910_MI,
	&m9653_MI,
	NULL,
	&m7712_MI,
	NULL,
	NULL,
	&m6048_MI,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	&m7713_MI,
	NULL,
	&m7714_MI,
	NULL,
	&m7715_MI,
	&m7716_MI,
};
static TypeInfo* t1146_ITIs[] = 
{
	&t2036_TI,
};
static Il2CppInterfaceOffsetPair t1146_IOs[] = 
{
	{ &t1657_TI, 4},
	{ &t1915_TI, 6},
	{ &t2036_TI, 14},
};
void t1146_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1402 * tmp;
		tmp = (t1402 *)il2cpp_codegen_object_new (&t1402_TI);
		m7733(tmp, 0, &m7733_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1404 * tmp;
		tmp = (t1404 *)il2cpp_codegen_object_new (&t1404_TI);
		m7734(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2036_TI)), &m7734_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
void t1146_CustomAttributesCacheGenerator_m7713(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t342 * tmp;
		tmp = (t342 *)il2cpp_codegen_object_new (&t342_TI);
		m1507(tmp, &m1507_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1288 * tmp;
		tmp = (t1288 *)il2cpp_codegen_object_new (&t1288_TI);
		m6773(tmp, &m6773_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t1146_CustomAttributesCacheGenerator_m7714(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1288 * tmp;
		tmp = (t1288 *)il2cpp_codegen_object_new (&t1288_TI);
		m6773(tmp, &m6773_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t342 * tmp;
		tmp = (t342 *)il2cpp_codegen_object_new (&t342_TI);
		m1507(tmp, &m1507_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1146__CustomAttributeCache = {
3,
NULL,
&t1146_CustomAttributesCacheGenerator
};
CustomAttributesCache t1146__CustomAttributeCache_m7713 = {
2,
NULL,
&t1146_CustomAttributesCacheGenerator_m7713
};
CustomAttributesCache t1146__CustomAttributeCache_m7714 = {
2,
NULL,
&t1146_CustomAttributesCacheGenerator_m7714
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1146_0_0_0;
extern Il2CppType t1146_1_0_0;
struct t1146;
extern CustomAttributesCache t1146__CustomAttributeCache;
extern CustomAttributesCache t1146__CustomAttributeCache_m7713;
extern CustomAttributesCache t1146__CustomAttributeCache_m7714;
TypeInfo t1146_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PropertyInfo", "System.Reflection", t1146_MIs, t1146_PIs, NULL, NULL, &t296_TI, NULL, NULL, &t1146_TI, t1146_ITIs, t1146_VT, &t1146__CustomAttributeCache, &t1146_TI, &t1146_0_0_0, &t1146_1_0_0, t1146_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1146), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, false, false, false, false, false, false, false, false, 16, 5, 0, 0, 0, 28, 1, 3};
#include "t1361.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1361_TI;
#include "t1361MD.h"

#include "t348.h"
extern TypeInfo t781_TI;
extern TypeInfo t348_TI;
extern Il2CppType t781_0_0_0;
extern MethodInfo m3985_MI;
extern MethodInfo m3987_MI;
extern MethodInfo m3997_MI;
extern MethodInfo m3983_MI;


extern MethodInfo m7717_MI;
 void m7717 (t1361 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t781_0_0_0), &m1554_MI);
		t29 * L_1 = m3985(p0, (t7*) &_stringLiteral1566, L_0, &m3985_MI);
		__this->f0 = ((t781*)Castclass(L_1, InitializedTypeInfo(&t781_TI)));
		t7* L_2 = m3994(p0, (t7*) &_stringLiteral1567, &m3994_MI);
		__this->f1 = L_2;
		bool L_3 = m3987(p0, (t7*) &_stringLiteral1568, &m3987_MI);
		__this->f2 = L_3;
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t781_0_0_0), &m1554_MI);
		t29 * L_5 = m3985(p0, (t7*) &_stringLiteral1569, L_4, &m3985_MI);
		__this->f3 = ((t781*)Castclass(L_5, InitializedTypeInfo(&t781_TI)));
		return;
	}
}
extern MethodInfo m7718_MI;
 void m7718 (t1361 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		t781* L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t781_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral1566, (t29 *)(t29 *)L_0, L_1, &m3982_MI);
		t7* L_2 = (__this->f1);
		m3997(p0, (t7*) &_stringLiteral1567, L_2, &m3997_MI);
		bool L_3 = (__this->f2);
		m3983(p0, (t7*) &_stringLiteral1568, L_3, &m3983_MI);
		t781* L_4 = (__this->f3);
		t42 * L_5 = m1554(NULL, LoadTypeToken(&t781_0_0_0), &m1554_MI);
		m3982(p0, (t7*) &_stringLiteral1569, (t29 *)(t29 *)L_4, L_5, &m3982_MI);
		return;
	}
}
extern MethodInfo m7719_MI;
 void m7719 (t1361 * __this, t29 * p0, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition System.Reflection.StrongNameKeyPair
extern Il2CppType t781_0_0_1;
FieldInfo t1361_f0_FieldInfo = 
{
	"_publicKey", &t781_0_0_1, &t1361_TI, offsetof(t1361, f0), &EmptyCustomAttributesCache};
extern Il2CppType t7_0_0_1;
FieldInfo t1361_f1_FieldInfo = 
{
	"_keyPairContainer", &t7_0_0_1, &t1361_TI, offsetof(t1361, f1), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1361_f2_FieldInfo = 
{
	"_keyPairExported", &t40_0_0_1, &t1361_TI, offsetof(t1361, f2), &EmptyCustomAttributesCache};
extern Il2CppType t781_0_0_1;
FieldInfo t1361_f3_FieldInfo = 
{
	"_keyPairArray", &t781_0_0_1, &t1361_TI, offsetof(t1361, f3), &EmptyCustomAttributesCache};
static FieldInfo* t1361_FIs[] =
{
	&t1361_f0_FieldInfo,
	&t1361_f1_FieldInfo,
	&t1361_f2_FieldInfo,
	&t1361_f3_FieldInfo,
	NULL
};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1361_m7717_ParameterInfos[] = 
{
	{"info", 0, 134221778, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221779, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7717_MI = 
{
	".ctor", (methodPointerType)&m7717, &t1361_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1361_m7717_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 3268, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1361_m7718_ParameterInfos[] = 
{
	{"info", 0, 134221780, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221781, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7718_MI = 
{
	"System.Runtime.Serialization.ISerializable.GetObjectData", (methodPointerType)&m7718, &t1361_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1361_m7718_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 4, 2, false, false, 3269, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1361_m7719_ParameterInfos[] = 
{
	{"sender", 0, 134221782, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7719_MI = 
{
	"System.Runtime.Serialization.IDeserializationCallback.OnDeserialization", (methodPointerType)&m7719, &t1361_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1361_m7719_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 1, false, false, 3270, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1361_MIs[] =
{
	&m7717_MI,
	&m7718_MI,
	&m7719_MI,
	NULL
};
static MethodInfo* t1361_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m7718_MI,
	&m7719_MI,
};
extern TypeInfo t918_TI;
static TypeInfo* t1361_ITIs[] = 
{
	&t374_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t1361_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t918_TI, 5},
};
void t1361_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1361__CustomAttributeCache = {
1,
NULL,
&t1361_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1361_0_0_0;
extern Il2CppType t1361_1_0_0;
struct t1361;
extern CustomAttributesCache t1361__CustomAttributeCache;
TypeInfo t1361_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StrongNameKeyPair", "System.Reflection", t1361_MIs, NULL, t1361_FIs, NULL, &t29_TI, NULL, NULL, &t1361_TI, t1361_ITIs, t1361_VT, &t1361__CustomAttributeCache, &t1361_TI, &t1361_0_0_0, &t1361_1_0_0, t1361_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1361), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 4, 0, 0, 6, 2, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1167MD.h"
#include "t295MD.h"
extern MethodInfo m6079_MI;
extern MethodInfo m2945_MI;
extern MethodInfo m5271_MI;


extern MethodInfo m7720_MI;
 void m7720 (t1389 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral1570, &m6079_MI);
		m2945(__this, L_0, &m2945_MI);
		return;
	}
}
 void m7721 (t1389 * __this, t7* p0, MethodInfo* method){
	{
		m2945(__this, p0, &m2945_MI);
		return;
	}
}
extern MethodInfo m7722_MI;
 void m7722 (t1389 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m5271(__this, p0, p1, &m5271_MI);
		return;
	}
}
// Metadata Definition System.Reflection.TargetException
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7720_MI = 
{
	".ctor", (methodPointerType)&m7720, &t1389_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3271, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1389_m7721_ParameterInfos[] = 
{
	{"message", 0, 134221783, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7721_MI = 
{
	".ctor", (methodPointerType)&m7721, &t1389_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1389_m7721_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3272, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1389_m7722_ParameterInfos[] = 
{
	{"info", 0, 134221784, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221785, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7722_MI = 
{
	".ctor", (methodPointerType)&m7722, &t1389_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1389_m7722_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 3273, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1389_MIs[] =
{
	&m7720_MI,
	&m7721_MI,
	&m7722_MI,
	NULL
};
extern MethodInfo m2856_MI;
extern MethodInfo m2857_MI;
extern MethodInfo m2858_MI;
extern MethodInfo m1684_MI;
extern MethodInfo m2859_MI;
extern MethodInfo m2860_MI;
extern MethodInfo m2861_MI;
static MethodInfo* t1389_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
extern TypeInfo t590_TI;
static Il2CppInterfaceOffsetPair t1389_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1389_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1389__CustomAttributeCache = {
1,
NULL,
&t1389_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1389_0_0_0;
extern Il2CppType t1389_1_0_0;
struct t1389;
extern CustomAttributesCache t1389__CustomAttributeCache;
TypeInfo t1389_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TargetException", "System.Reflection", t1389_MIs, NULL, NULL, NULL, &t295_TI, NULL, NULL, &t1389_TI, NULL, t1389_VT, &t1389__CustomAttributeCache, &t1389_TI, &t1389_0_0_0, &t1389_1_0_0, t1389_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1389), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 11, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m2947_MI;


 void m7723 (t1390 * __this, t295 * p0, MethodInfo* method){
	{
		m2947(__this, (t7*) &_stringLiteral1571, p0, &m2947_MI);
		return;
	}
}
extern MethodInfo m7724_MI;
 void m7724 (t1390 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m5271(__this, p0, p1, &m5271_MI);
		return;
	}
}
// Metadata Definition System.Reflection.TargetInvocationException
extern Il2CppType t295_0_0_0;
extern Il2CppType t295_0_0_0;
static ParameterInfo t1390_m7723_ParameterInfos[] = 
{
	{"inner", 0, 134221786, &EmptyCustomAttributesCache, &t295_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7723_MI = 
{
	".ctor", (methodPointerType)&m7723, &t1390_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1390_m7723_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3274, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1390_m7724_ParameterInfos[] = 
{
	{"info", 0, 134221787, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"sc", 1, 134221788, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7724_MI = 
{
	".ctor", (methodPointerType)&m7724, &t1390_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1390_m7724_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 2, false, false, 3275, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1390_MIs[] =
{
	&m7723_MI,
	&m7724_MI,
	NULL
};
static MethodInfo* t1390_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1390_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1390_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1390__CustomAttributeCache = {
1,
NULL,
&t1390_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1390_0_0_0;
extern Il2CppType t1390_1_0_0;
struct t1390;
extern CustomAttributesCache t1390__CustomAttributeCache;
TypeInfo t1390_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TargetInvocationException", "System.Reflection", t1390_MIs, NULL, NULL, NULL, &t295_TI, NULL, NULL, &t1390_TI, NULL, t1390_VT, &t1390__CustomAttributeCache, &t1390_TI, &t1390_0_0_0, &t1390_1_0_0, t1390_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1390), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 11, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m7725 (t1391 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral1572, &m6079_MI);
		m2945(__this, L_0, &m2945_MI);
		return;
	}
}
 void m7726 (t1391 * __this, t7* p0, MethodInfo* method){
	{
		m2945(__this, p0, &m2945_MI);
		return;
	}
}
extern MethodInfo m7727_MI;
 void m7727 (t1391 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m5271(__this, p0, p1, &m5271_MI);
		return;
	}
}
// Metadata Definition System.Reflection.TargetParameterCountException
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7725_MI = 
{
	".ctor", (methodPointerType)&m7725, &t1391_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3276, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t1391_m7726_ParameterInfos[] = 
{
	{"message", 0, 134221789, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7726_MI = 
{
	".ctor", (methodPointerType)&m7726, &t1391_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1391_m7726_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3277, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1391_m7727_ParameterInfos[] = 
{
	{"info", 0, 134221790, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221791, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7727_MI = 
{
	".ctor", (methodPointerType)&m7727, &t1391_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1391_m7727_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 2, false, false, 3278, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1391_MIs[] =
{
	&m7725_MI,
	&m7726_MI,
	&m7727_MI,
	NULL
};
static MethodInfo* t1391_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1391_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1391_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1391__CustomAttributeCache = {
1,
NULL,
&t1391_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1391_0_0_0;
extern Il2CppType t1391_1_0_0;
struct t1391;
extern CustomAttributesCache t1391__CustomAttributeCache;
TypeInfo t1391_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TargetParameterCountException", "System.Reflection", t1391_MIs, NULL, NULL, NULL, &t295_TI, NULL, NULL, &t1391_TI, NULL, t1391_VT, &t1391__CustomAttributeCache, &t1391_TI, &t1391_0_0_0, &t1391_1_0_0, t1391_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1391), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 11, 0, 2};
#include "t1148.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1148_TI;
#include "t1148MD.h"



// Metadata Definition System.Reflection.TypeAttributes
extern Il2CppType t44_0_0_1542;
FieldInfo t1148_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1148_TI, offsetof(t1148, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f2_FieldInfo = 
{
	"VisibilityMask", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f3_FieldInfo = 
{
	"NotPublic", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f4_FieldInfo = 
{
	"Public", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f5_FieldInfo = 
{
	"NestedPublic", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f6_FieldInfo = 
{
	"NestedPrivate", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f7_FieldInfo = 
{
	"NestedFamily", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f8_FieldInfo = 
{
	"NestedAssembly", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f9_FieldInfo = 
{
	"NestedFamANDAssem", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f10_FieldInfo = 
{
	"NestedFamORAssem", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f11_FieldInfo = 
{
	"LayoutMask", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f12_FieldInfo = 
{
	"AutoLayout", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f13_FieldInfo = 
{
	"SequentialLayout", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f14_FieldInfo = 
{
	"ExplicitLayout", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f15_FieldInfo = 
{
	"ClassSemanticsMask", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f16_FieldInfo = 
{
	"Class", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f17_FieldInfo = 
{
	"Interface", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f18_FieldInfo = 
{
	"Abstract", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f19_FieldInfo = 
{
	"Sealed", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f20_FieldInfo = 
{
	"SpecialName", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f21_FieldInfo = 
{
	"Import", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f22_FieldInfo = 
{
	"Serializable", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f23_FieldInfo = 
{
	"StringFormatMask", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f24_FieldInfo = 
{
	"AnsiClass", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f25_FieldInfo = 
{
	"UnicodeClass", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f26_FieldInfo = 
{
	"AutoClass", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f27_FieldInfo = 
{
	"BeforeFieldInit", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f28_FieldInfo = 
{
	"ReservedMask", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f29_FieldInfo = 
{
	"RTSpecialName", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f30_FieldInfo = 
{
	"HasSecurity", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f31_FieldInfo = 
{
	"CustomFormatClass", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1148_0_0_32854;
FieldInfo t1148_f32_FieldInfo = 
{
	"CustomFormatMask", &t1148_0_0_32854, &t1148_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1148_FIs[] =
{
	&t1148_f1_FieldInfo,
	&t1148_f2_FieldInfo,
	&t1148_f3_FieldInfo,
	&t1148_f4_FieldInfo,
	&t1148_f5_FieldInfo,
	&t1148_f6_FieldInfo,
	&t1148_f7_FieldInfo,
	&t1148_f8_FieldInfo,
	&t1148_f9_FieldInfo,
	&t1148_f10_FieldInfo,
	&t1148_f11_FieldInfo,
	&t1148_f12_FieldInfo,
	&t1148_f13_FieldInfo,
	&t1148_f14_FieldInfo,
	&t1148_f15_FieldInfo,
	&t1148_f16_FieldInfo,
	&t1148_f17_FieldInfo,
	&t1148_f18_FieldInfo,
	&t1148_f19_FieldInfo,
	&t1148_f20_FieldInfo,
	&t1148_f21_FieldInfo,
	&t1148_f22_FieldInfo,
	&t1148_f23_FieldInfo,
	&t1148_f24_FieldInfo,
	&t1148_f25_FieldInfo,
	&t1148_f26_FieldInfo,
	&t1148_f27_FieldInfo,
	&t1148_f28_FieldInfo,
	&t1148_f29_FieldInfo,
	&t1148_f30_FieldInfo,
	&t1148_f31_FieldInfo,
	&t1148_f32_FieldInfo,
	NULL
};
static const int32_t t1148_f2_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry t1148_f2_DefaultValue = 
{
	&t1148_f2_FieldInfo, { (char*)&t1148_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f3_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1148_f3_DefaultValue = 
{
	&t1148_f3_FieldInfo, { (char*)&t1148_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f4_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1148_f4_DefaultValue = 
{
	&t1148_f4_FieldInfo, { (char*)&t1148_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f5_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1148_f5_DefaultValue = 
{
	&t1148_f5_FieldInfo, { (char*)&t1148_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f6_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1148_f6_DefaultValue = 
{
	&t1148_f6_FieldInfo, { (char*)&t1148_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f7_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1148_f7_DefaultValue = 
{
	&t1148_f7_FieldInfo, { (char*)&t1148_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f8_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t1148_f8_DefaultValue = 
{
	&t1148_f8_FieldInfo, { (char*)&t1148_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f9_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t1148_f9_DefaultValue = 
{
	&t1148_f9_FieldInfo, { (char*)&t1148_f9_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f10_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry t1148_f10_DefaultValue = 
{
	&t1148_f10_FieldInfo, { (char*)&t1148_f10_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f11_DefaultValueData = 24;
static Il2CppFieldDefaultValueEntry t1148_f11_DefaultValue = 
{
	&t1148_f11_FieldInfo, { (char*)&t1148_f11_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f12_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1148_f12_DefaultValue = 
{
	&t1148_f12_FieldInfo, { (char*)&t1148_f12_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f13_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t1148_f13_DefaultValue = 
{
	&t1148_f13_FieldInfo, { (char*)&t1148_f13_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f14_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t1148_f14_DefaultValue = 
{
	&t1148_f14_FieldInfo, { (char*)&t1148_f14_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f15_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t1148_f15_DefaultValue = 
{
	&t1148_f15_FieldInfo, { (char*)&t1148_f15_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f16_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1148_f16_DefaultValue = 
{
	&t1148_f16_FieldInfo, { (char*)&t1148_f16_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f17_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t1148_f17_DefaultValue = 
{
	&t1148_f17_FieldInfo, { (char*)&t1148_f17_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f18_DefaultValueData = 128;
static Il2CppFieldDefaultValueEntry t1148_f18_DefaultValue = 
{
	&t1148_f18_FieldInfo, { (char*)&t1148_f18_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f19_DefaultValueData = 256;
static Il2CppFieldDefaultValueEntry t1148_f19_DefaultValue = 
{
	&t1148_f19_FieldInfo, { (char*)&t1148_f19_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f20_DefaultValueData = 1024;
static Il2CppFieldDefaultValueEntry t1148_f20_DefaultValue = 
{
	&t1148_f20_FieldInfo, { (char*)&t1148_f20_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f21_DefaultValueData = 4096;
static Il2CppFieldDefaultValueEntry t1148_f21_DefaultValue = 
{
	&t1148_f21_FieldInfo, { (char*)&t1148_f21_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f22_DefaultValueData = 8192;
static Il2CppFieldDefaultValueEntry t1148_f22_DefaultValue = 
{
	&t1148_f22_FieldInfo, { (char*)&t1148_f22_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f23_DefaultValueData = 196608;
static Il2CppFieldDefaultValueEntry t1148_f23_DefaultValue = 
{
	&t1148_f23_FieldInfo, { (char*)&t1148_f23_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f24_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1148_f24_DefaultValue = 
{
	&t1148_f24_FieldInfo, { (char*)&t1148_f24_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f25_DefaultValueData = 65536;
static Il2CppFieldDefaultValueEntry t1148_f25_DefaultValue = 
{
	&t1148_f25_FieldInfo, { (char*)&t1148_f25_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f26_DefaultValueData = 131072;
static Il2CppFieldDefaultValueEntry t1148_f26_DefaultValue = 
{
	&t1148_f26_FieldInfo, { (char*)&t1148_f26_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f27_DefaultValueData = 1048576;
static Il2CppFieldDefaultValueEntry t1148_f27_DefaultValue = 
{
	&t1148_f27_FieldInfo, { (char*)&t1148_f27_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f28_DefaultValueData = 264192;
static Il2CppFieldDefaultValueEntry t1148_f28_DefaultValue = 
{
	&t1148_f28_FieldInfo, { (char*)&t1148_f28_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f29_DefaultValueData = 2048;
static Il2CppFieldDefaultValueEntry t1148_f29_DefaultValue = 
{
	&t1148_f29_FieldInfo, { (char*)&t1148_f29_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f30_DefaultValueData = 262144;
static Il2CppFieldDefaultValueEntry t1148_f30_DefaultValue = 
{
	&t1148_f30_FieldInfo, { (char*)&t1148_f30_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f31_DefaultValueData = 196608;
static Il2CppFieldDefaultValueEntry t1148_f31_DefaultValue = 
{
	&t1148_f31_FieldInfo, { (char*)&t1148_f31_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1148_f32_DefaultValueData = 12582912;
static Il2CppFieldDefaultValueEntry t1148_f32_DefaultValue = 
{
	&t1148_f32_FieldInfo, { (char*)&t1148_f32_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1148_FDVs[] = 
{
	&t1148_f2_DefaultValue,
	&t1148_f3_DefaultValue,
	&t1148_f4_DefaultValue,
	&t1148_f5_DefaultValue,
	&t1148_f6_DefaultValue,
	&t1148_f7_DefaultValue,
	&t1148_f8_DefaultValue,
	&t1148_f9_DefaultValue,
	&t1148_f10_DefaultValue,
	&t1148_f11_DefaultValue,
	&t1148_f12_DefaultValue,
	&t1148_f13_DefaultValue,
	&t1148_f14_DefaultValue,
	&t1148_f15_DefaultValue,
	&t1148_f16_DefaultValue,
	&t1148_f17_DefaultValue,
	&t1148_f18_DefaultValue,
	&t1148_f19_DefaultValue,
	&t1148_f20_DefaultValue,
	&t1148_f21_DefaultValue,
	&t1148_f22_DefaultValue,
	&t1148_f23_DefaultValue,
	&t1148_f24_DefaultValue,
	&t1148_f25_DefaultValue,
	&t1148_f26_DefaultValue,
	&t1148_f27_DefaultValue,
	&t1148_f28_DefaultValue,
	&t1148_f29_DefaultValue,
	&t1148_f30_DefaultValue,
	&t1148_f31_DefaultValue,
	&t1148_f32_DefaultValue,
	NULL
};
static MethodInfo* t1148_MIs[] =
{
	NULL
};
static MethodInfo* t1148_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1148_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1148_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1148__CustomAttributeCache = {
2,
NULL,
&t1148_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1148_0_0_0;
extern Il2CppType t1148_1_0_0;
extern CustomAttributesCache t1148__CustomAttributeCache;
TypeInfo t1148_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeAttributes", "System.Reflection", t1148_MIs, NULL, t1148_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1148_VT, &t1148__CustomAttributeCache, &t44_TI, &t1148_0_0_0, &t1148_1_0_0, t1148_IOs, NULL, NULL, t1148_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1148)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 32, 0, 0, 23, 0, 3};
#include "t706.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t706_TI;
#include "t706MD.h"



extern MethodInfo m3061_MI;
 void m3061 (t706 * __this, t7* p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1573, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition System.Resources.NeutralResourcesLanguageAttribute
extern Il2CppType t7_0_0_1;
FieldInfo t706_f0_FieldInfo = 
{
	"culture", &t7_0_0_1, &t706_TI, offsetof(t706, f0), &EmptyCustomAttributesCache};
static FieldInfo* t706_FIs[] =
{
	&t706_f0_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t706_m3061_ParameterInfos[] = 
{
	{"cultureName", 0, 134221792, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3061_MI = 
{
	".ctor", (methodPointerType)&m3061, &t706_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t706_m3061_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3279, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t706_MIs[] =
{
	&m3061_MI,
	NULL
};
static MethodInfo* t706_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t706_IOs[] = 
{
	{ &t604_TI, 4},
};
void t706_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 1, &m2915_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t706__CustomAttributeCache = {
2,
NULL,
&t706_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t706_0_0_0;
extern Il2CppType t706_1_0_0;
struct t706;
extern CustomAttributesCache t706__CustomAttributeCache;
TypeInfo t706_TI = 
{
	&g_mscorlib_dll_Image, NULL, "NeutralResourcesLanguageAttribute", "System.Resources", t706_MIs, NULL, t706_FIs, NULL, &t490_TI, NULL, NULL, &t706_TI, NULL, t706_VT, &t706__CustomAttributeCache, &t706_TI, &t706_0_0_0, &t706_1_0_0, t706_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t706), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t704.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t704_TI;
#include "t704MD.h"

#include "t760.h"
extern TypeInfo t760_TI;
#include "t760MD.h"
extern MethodInfo m9591_MI;


extern MethodInfo m3059_MI;
 void m3059 (t704 * __this, t7* p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		t760 * L_0 = (t760 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t760_TI));
		m9591(L_0, p0, &m9591_MI);
		__this->f0 = L_0;
		return;
	}
}
// Metadata Definition System.Resources.SatelliteContractVersionAttribute
extern Il2CppType t760_0_0_1;
FieldInfo t704_f0_FieldInfo = 
{
	"ver", &t760_0_0_1, &t704_TI, offsetof(t704, f0), &EmptyCustomAttributesCache};
static FieldInfo* t704_FIs[] =
{
	&t704_f0_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
static ParameterInfo t704_m3059_ParameterInfos[] = 
{
	{"version", 0, 134221793, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3059_MI = 
{
	".ctor", (methodPointerType)&m3059, &t704_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t704_m3059_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3280, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t704_MIs[] =
{
	&m3059_MI,
	NULL
};
static MethodInfo* t704_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t704_IOs[] = 
{
	{ &t604_TI, 4},
};
void t704_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 1, &m2915_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t704__CustomAttributeCache = {
2,
NULL,
&t704_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t704_0_0_0;
extern Il2CppType t704_1_0_0;
struct t704;
extern CustomAttributesCache t704__CustomAttributeCache;
TypeInfo t704_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SatelliteContractVersionAttribute", "System.Resources", t704_MIs, NULL, t704_FIs, NULL, &t490_TI, NULL, NULL, &t704_TI, NULL, t704_VT, &t704__CustomAttributeCache, &t704_TI, &t704_0_0_0, &t704_1_0_0, t704_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t704), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t1392.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1392_TI;
#include "t1392MD.h"



// Metadata Definition System.Runtime.CompilerServices.CompilationRelaxations
extern Il2CppType t44_0_0_1542;
FieldInfo t1392_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1392_TI, offsetof(t1392, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1392_0_0_32854;
FieldInfo t1392_f2_FieldInfo = 
{
	"NoStringInterning", &t1392_0_0_32854, &t1392_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1392_FIs[] =
{
	&t1392_f1_FieldInfo,
	&t1392_f2_FieldInfo,
	NULL
};
static const int32_t t1392_f2_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t1392_f2_DefaultValue = 
{
	&t1392_f2_FieldInfo, { (char*)&t1392_f2_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1392_FDVs[] = 
{
	&t1392_f2_DefaultValue,
	NULL
};
static MethodInfo* t1392_MIs[] =
{
	NULL
};
static MethodInfo* t1392_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1392_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1392_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t291 * tmp;
		tmp = (t291 *)il2cpp_codegen_object_new (&t291_TI);
		m1270(tmp, &m1270_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1392__CustomAttributeCache = {
2,
NULL,
&t1392_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1392_0_0_0;
extern Il2CppType t1392_1_0_0;
extern CustomAttributesCache t1392__CustomAttributeCache;
TypeInfo t1392_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CompilationRelaxations", "System.Runtime.CompilerServices", t1392_MIs, NULL, t1392_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1392_VT, &t1392__CustomAttributeCache, &t44_TI, &t1392_0_0_0, &t1392_1_0_0, t1392_IOs, NULL, NULL, t1392_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1392)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 2, 0, 0, 23, 0, 3};
#include "t709.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t709_TI;
#include "t709MD.h"



extern MethodInfo m3064_MI;
 void m3064 (t709 * __this, int32_t p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition System.Runtime.CompilerServices.CompilationRelaxationsAttribute
extern Il2CppType t44_0_0_1;
FieldInfo t709_f0_FieldInfo = 
{
	"relax", &t44_0_0_1, &t709_TI, offsetof(t709, f0), &EmptyCustomAttributesCache};
static FieldInfo* t709_FIs[] =
{
	&t709_f0_FieldInfo,
	NULL
};
extern Il2CppType t1392_0_0_0;
static ParameterInfo t709_m3064_ParameterInfos[] = 
{
	{"relaxations", 0, 134221794, &EmptyCustomAttributesCache, &t1392_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m3064_MI = 
{
	".ctor", (methodPointerType)&m3064, &t709_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t709_m3064_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3281, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t709_MIs[] =
{
	&m3064_MI,
	NULL
};
static MethodInfo* t709_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t709_IOs[] = 
{
	{ &t604_TI, 4},
};
void t709_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 71, &m2915_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t709__CustomAttributeCache = {
2,
NULL,
&t709_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t709_0_0_0;
extern Il2CppType t709_1_0_0;
struct t709;
extern CustomAttributesCache t709__CustomAttributeCache;
TypeInfo t709_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CompilationRelaxationsAttribute", "System.Runtime.CompilerServices", t709_MIs, NULL, t709_FIs, NULL, &t490_TI, NULL, NULL, &t709_TI, NULL, t709_VT, &t709__CustomAttributeCache, &t709_TI, &t709_0_0_0, &t709_1_0_0, t709_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t709), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t1393.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1393_TI;
#include "t1393MD.h"

#include "t1394.h"


extern MethodInfo m7728_MI;
 void m7728 (t1393 * __this, int32_t p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition System.Runtime.CompilerServices.DefaultDependencyAttribute
extern Il2CppType t1394_0_0_1;
FieldInfo t1393_f0_FieldInfo = 
{
	"hint", &t1394_0_0_1, &t1393_TI, offsetof(t1393, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1393_FIs[] =
{
	&t1393_f0_FieldInfo,
	NULL
};
extern Il2CppType t1394_0_0_0;
extern Il2CppType t1394_0_0_0;
static ParameterInfo t1393_m7728_ParameterInfos[] = 
{
	{"loadHintArgument", 0, 134221795, &EmptyCustomAttributesCache, &t1394_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7728_MI = 
{
	".ctor", (methodPointerType)&m7728, &t1393_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1393_m7728_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3282, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1393_MIs[] =
{
	&m7728_MI,
	NULL
};
static MethodInfo* t1393_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t1393_IOs[] = 
{
	{ &t604_TI, 4},
};
void t1393_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 1, &m2915_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1393__CustomAttributeCache = {
1,
NULL,
&t1393_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1393_0_0_0;
extern Il2CppType t1393_1_0_0;
struct t1393;
extern CustomAttributesCache t1393__CustomAttributeCache;
TypeInfo t1393_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultDependencyAttribute", "System.Runtime.CompilerServices", t1393_MIs, NULL, t1393_FIs, NULL, &t490_TI, NULL, NULL, &t1393_TI, NULL, t1393_VT, &t1393__CustomAttributeCache, &t1393_TI, &t1393_0_0_0, &t1393_1_0_0, t1393_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1393), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t1395.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1395_TI;
#include "t1395MD.h"



// Metadata Definition System.Runtime.CompilerServices.IsVolatile
static MethodInfo* t1395_MIs[] =
{
	NULL
};
static MethodInfo* t1395_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t1395_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1395__CustomAttributeCache = {
1,
NULL,
&t1395_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1395_0_0_0;
extern Il2CppType t1395_1_0_0;
struct t1395;
extern CustomAttributesCache t1395__CustomAttributeCache;
TypeInfo t1395_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IsVolatile", "System.Runtime.CompilerServices", t1395_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t1395_TI, NULL, t1395_VT, &t1395__CustomAttributeCache, &t1395_TI, &t1395_0_0_0, &t1395_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1395), 0, -1, 0, 0, -1, 1048961, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1394_TI;
#include "t1394MD.h"



// Metadata Definition System.Runtime.CompilerServices.LoadHint
extern Il2CppType t44_0_0_1542;
FieldInfo t1394_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1394_TI, offsetof(t1394, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1394_0_0_32854;
FieldInfo t1394_f2_FieldInfo = 
{
	"Default", &t1394_0_0_32854, &t1394_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1394_0_0_32854;
FieldInfo t1394_f3_FieldInfo = 
{
	"Always", &t1394_0_0_32854, &t1394_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1394_0_0_32854;
FieldInfo t1394_f4_FieldInfo = 
{
	"Sometimes", &t1394_0_0_32854, &t1394_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1394_FIs[] =
{
	&t1394_f1_FieldInfo,
	&t1394_f2_FieldInfo,
	&t1394_f3_FieldInfo,
	&t1394_f4_FieldInfo,
	NULL
};
static const int32_t t1394_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1394_f2_DefaultValue = 
{
	&t1394_f2_FieldInfo, { (char*)&t1394_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1394_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1394_f3_DefaultValue = 
{
	&t1394_f3_FieldInfo, { (char*)&t1394_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1394_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1394_f4_DefaultValue = 
{
	&t1394_f4_FieldInfo, { (char*)&t1394_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1394_FDVs[] = 
{
	&t1394_f2_DefaultValue,
	&t1394_f3_DefaultValue,
	&t1394_f4_DefaultValue,
	NULL
};
static MethodInfo* t1394_MIs[] =
{
	NULL
};
static MethodInfo* t1394_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1394_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1394_1_0_0;
TypeInfo t1394_TI = 
{
	&g_mscorlib_dll_Image, NULL, "LoadHint", "System.Runtime.CompilerServices", t1394_MIs, NULL, t1394_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1394_VT, &EmptyCustomAttributesCache, &t44_TI, &t1394_0_0_0, &t1394_1_0_0, t1394_IOs, NULL, NULL, t1394_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1394)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#include "t1396.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1396_TI;
#include "t1396MD.h"



extern MethodInfo m7729_MI;
 void m7729 (t1396 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition System.Runtime.CompilerServices.StringFreezingAttribute
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7729_MI = 
{
	".ctor", (methodPointerType)&m7729, &t1396_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3283, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1396_MIs[] =
{
	&m7729_MI,
	NULL
};
static MethodInfo* t1396_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t1396_IOs[] = 
{
	{ &t604_TI, 4},
};
void t1396_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 1, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1396__CustomAttributeCache = {
1,
NULL,
&t1396_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1396_0_0_0;
extern Il2CppType t1396_1_0_0;
struct t1396;
extern CustomAttributesCache t1396__CustomAttributeCache;
TypeInfo t1396_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StringFreezingAttribute", "System.Runtime.CompilerServices", t1396_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t1396_TI, NULL, t1396_VT, &t1396__CustomAttributeCache, &t1396_TI, &t1396_0_0_0, &t1396_1_0_0, t1396_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1396), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t1397.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1397_TI;
#include "t1397MD.h"



// Metadata Definition System.Runtime.ConstrainedExecution.Cer
extern Il2CppType t44_0_0_1542;
FieldInfo t1397_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1397_TI, offsetof(t1397, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1397_0_0_32854;
FieldInfo t1397_f2_FieldInfo = 
{
	"None", &t1397_0_0_32854, &t1397_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1397_0_0_32854;
FieldInfo t1397_f3_FieldInfo = 
{
	"MayFail", &t1397_0_0_32854, &t1397_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1397_0_0_32854;
FieldInfo t1397_f4_FieldInfo = 
{
	"Success", &t1397_0_0_32854, &t1397_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1397_FIs[] =
{
	&t1397_f1_FieldInfo,
	&t1397_f2_FieldInfo,
	&t1397_f3_FieldInfo,
	&t1397_f4_FieldInfo,
	NULL
};
static const int32_t t1397_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1397_f2_DefaultValue = 
{
	&t1397_f2_FieldInfo, { (char*)&t1397_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1397_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1397_f3_DefaultValue = 
{
	&t1397_f3_FieldInfo, { (char*)&t1397_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1397_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1397_f4_DefaultValue = 
{
	&t1397_f4_FieldInfo, { (char*)&t1397_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1397_FDVs[] = 
{
	&t1397_f2_DefaultValue,
	&t1397_f3_DefaultValue,
	&t1397_f4_DefaultValue,
	NULL
};
static MethodInfo* t1397_MIs[] =
{
	NULL
};
static MethodInfo* t1397_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1397_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1397_0_0_0;
extern Il2CppType t1397_1_0_0;
TypeInfo t1397_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Cer", "System.Runtime.ConstrainedExecution", t1397_MIs, NULL, t1397_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1397_VT, &EmptyCustomAttributesCache, &t44_TI, &t1397_0_0_0, &t1397_1_0_0, t1397_IOs, NULL, NULL, t1397_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1397)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#include "t1398.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1398_TI;
#include "t1398MD.h"



// Metadata Definition System.Runtime.ConstrainedExecution.Consistency
extern Il2CppType t44_0_0_1542;
FieldInfo t1398_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1398_TI, offsetof(t1398, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1398_0_0_32854;
FieldInfo t1398_f2_FieldInfo = 
{
	"MayCorruptAppDomain", &t1398_0_0_32854, &t1398_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1398_0_0_32854;
FieldInfo t1398_f3_FieldInfo = 
{
	"MayCorruptInstance", &t1398_0_0_32854, &t1398_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1398_0_0_32854;
FieldInfo t1398_f4_FieldInfo = 
{
	"MayCorruptProcess", &t1398_0_0_32854, &t1398_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1398_0_0_32854;
FieldInfo t1398_f5_FieldInfo = 
{
	"WillNotCorruptState", &t1398_0_0_32854, &t1398_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1398_FIs[] =
{
	&t1398_f1_FieldInfo,
	&t1398_f2_FieldInfo,
	&t1398_f3_FieldInfo,
	&t1398_f4_FieldInfo,
	&t1398_f5_FieldInfo,
	NULL
};
static const int32_t t1398_f2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1398_f2_DefaultValue = 
{
	&t1398_f2_FieldInfo, { (char*)&t1398_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1398_f3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1398_f3_DefaultValue = 
{
	&t1398_f3_FieldInfo, { (char*)&t1398_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1398_f4_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1398_f4_DefaultValue = 
{
	&t1398_f4_FieldInfo, { (char*)&t1398_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1398_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1398_f5_DefaultValue = 
{
	&t1398_f5_FieldInfo, { (char*)&t1398_f5_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1398_FDVs[] = 
{
	&t1398_f2_DefaultValue,
	&t1398_f3_DefaultValue,
	&t1398_f4_DefaultValue,
	&t1398_f5_DefaultValue,
	NULL
};
static MethodInfo* t1398_MIs[] =
{
	NULL
};
static MethodInfo* t1398_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1398_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1398_0_0_0;
extern Il2CppType t1398_1_0_0;
TypeInfo t1398_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Consistency", "System.Runtime.ConstrainedExecution", t1398_MIs, NULL, t1398_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1398_VT, &EmptyCustomAttributesCache, &t44_TI, &t1398_0_0_0, &t1398_1_0_0, t1398_IOs, NULL, NULL, t1398_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1398)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 23, 0, 3};
#include "t1399.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1399_TI;
#include "t1399MD.h"



extern MethodInfo m7730_MI;
 void m7730 (t1399 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m7731_MI;
 void m7731 (t1399 * __this, MethodInfo* method){
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		// IL_0000: leave.s IL_0009
		leaveInstructions[0] = 0x9; // 1
		THROW_SENTINEL(IL_0009);
		// finally target depth: 1
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0002;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0002;
	}

IL_0002:
	{ // begin finally (depth: 1)
		m46(__this, &m46_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x9:
				goto IL_0009;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0009:
	{
		return;
	}
}
// Metadata Definition System.Runtime.ConstrainedExecution.CriticalFinalizerObject
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1399__CustomAttributeCache_m7730;
MethodInfo m7730_MI = 
{
	".ctor", (methodPointerType)&m7730, &t1399_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t1399__CustomAttributeCache_m7730, 6276, 0, 255, 0, false, false, 3284, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1399__CustomAttributeCache_m7731;
MethodInfo m7731_MI = 
{
	"Finalize", (methodPointerType)&m7731, &t1399_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t1399__CustomAttributeCache_m7731, 196, 0, 1, 0, false, false, 3285, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1399_MIs[] =
{
	&m7730_MI,
	&m7731_MI,
	NULL
};
static MethodInfo* t1399_VT[] =
{
	&m1321_MI,
	&m7731_MI,
	&m1322_MI,
	&m1332_MI,
};
void t1399_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t1400_TI;
#include "t1400.h"
#include "t1400MD.h"
extern MethodInfo m7732_MI;
void t1399_CustomAttributesCacheGenerator_m7730(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 1, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1399_CustomAttributesCacheGenerator_m7731(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1399__CustomAttributeCache = {
1,
NULL,
&t1399_CustomAttributesCacheGenerator
};
CustomAttributesCache t1399__CustomAttributeCache_m7730 = {
1,
NULL,
&t1399_CustomAttributesCacheGenerator_m7730
};
CustomAttributesCache t1399__CustomAttributeCache_m7731 = {
1,
NULL,
&t1399_CustomAttributesCacheGenerator_m7731
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1399_0_0_0;
extern Il2CppType t1399_1_0_0;
struct t1399;
extern CustomAttributesCache t1399__CustomAttributeCache;
extern CustomAttributesCache t1399__CustomAttributeCache_m7730;
extern CustomAttributesCache t1399__CustomAttributeCache_m7731;
TypeInfo t1399_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CriticalFinalizerObject", "System.Runtime.ConstrainedExecution", t1399_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t1399_TI, NULL, t1399_VT, &t1399__CustomAttributeCache, &t1399_TI, &t1399_0_0_0, &t1399_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1399), 0, -1, 0, 0, -1, 1048705, 0, false, false, false, false, false, false, false, false, true, false, false, false, 2, 0, 0, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m7732 (t1400 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		__this->f1 = p1;
		return;
	}
}
// Metadata Definition System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
extern Il2CppType t1398_0_0_1;
FieldInfo t1400_f0_FieldInfo = 
{
	"consistency", &t1398_0_0_1, &t1400_TI, offsetof(t1400, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1397_0_0_1;
FieldInfo t1400_f1_FieldInfo = 
{
	"cer", &t1397_0_0_1, &t1400_TI, offsetof(t1400, f1), &EmptyCustomAttributesCache};
static FieldInfo* t1400_FIs[] =
{
	&t1400_f0_FieldInfo,
	&t1400_f1_FieldInfo,
	NULL
};
extern Il2CppType t1398_0_0_0;
extern Il2CppType t1397_0_0_0;
static ParameterInfo t1400_m7732_ParameterInfos[] = 
{
	{"consistencyGuarantee", 0, 134221796, &EmptyCustomAttributesCache, &t1398_0_0_0},
	{"cer", 1, 134221797, &EmptyCustomAttributesCache, &t1397_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7732_MI = 
{
	".ctor", (methodPointerType)&m7732, &t1400_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t1400_m7732_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 3286, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1400_MIs[] =
{
	&m7732_MI,
	NULL
};
static MethodInfo* t1400_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t1400_IOs[] = 
{
	{ &t604_TI, 4},
};
void t1400_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 1133, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1400__CustomAttributeCache = {
1,
NULL,
&t1400_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1400_0_0_0;
extern Il2CppType t1400_1_0_0;
struct t1400;
extern CustomAttributesCache t1400__CustomAttributeCache;
TypeInfo t1400_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReliabilityContractAttribute", "System.Runtime.ConstrainedExecution", t1400_MIs, NULL, t1400_FIs, NULL, &t490_TI, NULL, NULL, &t1400_TI, NULL, t1400_VT, &t1400__CustomAttributeCache, &t1400_TI, &t1400_0_0_0, &t1400_1_0_0, t1400_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1400), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 2, 0, 0, 4, 0, 1};
#include "t1401.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1401_TI;
#include "t1401MD.h"



// Metadata Definition System.Runtime.Hosting.ActivationArguments
static MethodInfo* t1401_MIs[] =
{
	NULL
};
static MethodInfo* t1401_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t1401_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1401__CustomAttributeCache = {
1,
NULL,
&t1401_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1401_0_0_0;
extern Il2CppType t1401_1_0_0;
struct t1401;
extern CustomAttributesCache t1401__CustomAttributeCache;
TypeInfo t1401_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ActivationArguments", "System.Runtime.Hosting", t1401_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t1401_TI, NULL, t1401_VT, &t1401__CustomAttributeCache, &t1401_TI, &t1401_0_0_0, &t1401_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1401), 0, -1, 0, 0, -1, 1057025, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 4, 0, 0};
#include "t1153.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1153_TI;
#include "t1153MD.h"



// Metadata Definition System.Runtime.InteropServices.CallingConvention
extern Il2CppType t44_0_0_1542;
FieldInfo t1153_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1153_TI, offsetof(t1153, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1153_0_0_32854;
FieldInfo t1153_f2_FieldInfo = 
{
	"Winapi", &t1153_0_0_32854, &t1153_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1153_0_0_32854;
FieldInfo t1153_f3_FieldInfo = 
{
	"Cdecl", &t1153_0_0_32854, &t1153_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1153_0_0_32854;
FieldInfo t1153_f4_FieldInfo = 
{
	"StdCall", &t1153_0_0_32854, &t1153_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1153_0_0_32854;
FieldInfo t1153_f5_FieldInfo = 
{
	"ThisCall", &t1153_0_0_32854, &t1153_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1153_0_0_32854;
FieldInfo t1153_f6_FieldInfo = 
{
	"FastCall", &t1153_0_0_32854, &t1153_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1153_FIs[] =
{
	&t1153_f1_FieldInfo,
	&t1153_f2_FieldInfo,
	&t1153_f3_FieldInfo,
	&t1153_f4_FieldInfo,
	&t1153_f5_FieldInfo,
	&t1153_f6_FieldInfo,
	NULL
};
static const int32_t t1153_f2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1153_f2_DefaultValue = 
{
	&t1153_f2_FieldInfo, { (char*)&t1153_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1153_f3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1153_f3_DefaultValue = 
{
	&t1153_f3_FieldInfo, { (char*)&t1153_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1153_f4_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1153_f4_DefaultValue = 
{
	&t1153_f4_FieldInfo, { (char*)&t1153_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1153_f5_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1153_f5_DefaultValue = 
{
	&t1153_f5_FieldInfo, { (char*)&t1153_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1153_f6_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t1153_f6_DefaultValue = 
{
	&t1153_f6_FieldInfo, { (char*)&t1153_f6_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1153_FDVs[] = 
{
	&t1153_f2_DefaultValue,
	&t1153_f3_DefaultValue,
	&t1153_f4_DefaultValue,
	&t1153_f5_DefaultValue,
	&t1153_f6_DefaultValue,
	NULL
};
static MethodInfo* t1153_MIs[] =
{
	NULL
};
static MethodInfo* t1153_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1153_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1153_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1153__CustomAttributeCache = {
1,
NULL,
&t1153_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1153_0_0_0;
extern Il2CppType t1153_1_0_0;
extern CustomAttributesCache t1153__CustomAttributeCache;
TypeInfo t1153_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CallingConvention", "System.Runtime.InteropServices", t1153_MIs, NULL, t1153_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1153_VT, &t1153__CustomAttributeCache, &t44_TI, &t1153_0_0_0, &t1153_1_0_0, t1153_IOs, NULL, NULL, t1153_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1153)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 6, 0, 0, 23, 0, 3};
#include "t1154.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1154_TI;
#include "t1154MD.h"



// Metadata Definition System.Runtime.InteropServices.CharSet
extern Il2CppType t44_0_0_1542;
FieldInfo t1154_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1154_TI, offsetof(t1154, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1154_0_0_32854;
FieldInfo t1154_f2_FieldInfo = 
{
	"None", &t1154_0_0_32854, &t1154_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1154_0_0_32854;
FieldInfo t1154_f3_FieldInfo = 
{
	"Ansi", &t1154_0_0_32854, &t1154_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1154_0_0_32854;
FieldInfo t1154_f4_FieldInfo = 
{
	"Unicode", &t1154_0_0_32854, &t1154_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1154_0_0_32854;
FieldInfo t1154_f5_FieldInfo = 
{
	"Auto", &t1154_0_0_32854, &t1154_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1154_FIs[] =
{
	&t1154_f1_FieldInfo,
	&t1154_f2_FieldInfo,
	&t1154_f3_FieldInfo,
	&t1154_f4_FieldInfo,
	&t1154_f5_FieldInfo,
	NULL
};
static const int32_t t1154_f2_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1154_f2_DefaultValue = 
{
	&t1154_f2_FieldInfo, { (char*)&t1154_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1154_f3_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1154_f3_DefaultValue = 
{
	&t1154_f3_FieldInfo, { (char*)&t1154_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1154_f4_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1154_f4_DefaultValue = 
{
	&t1154_f4_FieldInfo, { (char*)&t1154_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1154_f5_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1154_f5_DefaultValue = 
{
	&t1154_f5_FieldInfo, { (char*)&t1154_f5_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1154_FDVs[] = 
{
	&t1154_f2_DefaultValue,
	&t1154_f3_DefaultValue,
	&t1154_f4_DefaultValue,
	&t1154_f5_DefaultValue,
	NULL
};
static MethodInfo* t1154_MIs[] =
{
	NULL
};
static MethodInfo* t1154_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1154_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1154_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1154__CustomAttributeCache = {
1,
NULL,
&t1154_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1154_0_0_0;
extern Il2CppType t1154_1_0_0;
extern CustomAttributesCache t1154__CustomAttributeCache;
TypeInfo t1154_TI = 
{
	&g_mscorlib_dll_Image, NULL, "CharSet", "System.Runtime.InteropServices", t1154_MIs, NULL, t1154_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1154_VT, &t1154__CustomAttributeCache, &t44_TI, &t1154_0_0_0, &t1154_1_0_0, t1154_IOs, NULL, NULL, t1154_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1154)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif

#include "t1403.h"


 void m7733 (t1402 * __this, int32_t p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition System.Runtime.InteropServices.ClassInterfaceAttribute
extern Il2CppType t1403_0_0_1;
FieldInfo t1402_f0_FieldInfo = 
{
	"ciType", &t1403_0_0_1, &t1402_TI, offsetof(t1402, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1402_FIs[] =
{
	&t1402_f0_FieldInfo,
	NULL
};
extern Il2CppType t1403_0_0_0;
extern Il2CppType t1403_0_0_0;
static ParameterInfo t1402_m7733_ParameterInfos[] = 
{
	{"classInterfaceType", 0, 134221798, &EmptyCustomAttributesCache, &t1403_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7733_MI = 
{
	".ctor", (methodPointerType)&m7733, &t1402_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1402_m7733_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3287, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1402_MIs[] =
{
	&m7733_MI,
	NULL
};
static MethodInfo* t1402_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t1402_IOs[] = 
{
	{ &t604_TI, 4},
};
void t1402_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 5, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1402__CustomAttributeCache = {
2,
NULL,
&t1402_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1402_0_0_0;
extern Il2CppType t1402_1_0_0;
struct t1402;
extern CustomAttributesCache t1402__CustomAttributeCache;
TypeInfo t1402_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ClassInterfaceAttribute", "System.Runtime.InteropServices", t1402_MIs, NULL, t1402_FIs, NULL, &t490_TI, NULL, NULL, &t1402_TI, NULL, t1402_VT, &t1402__CustomAttributeCache, &t1402_TI, &t1402_0_0_0, &t1402_1_0_0, t1402_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1402), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1403_TI;
#include "t1403MD.h"



// Metadata Definition System.Runtime.InteropServices.ClassInterfaceType
extern Il2CppType t44_0_0_1542;
FieldInfo t1403_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1403_TI, offsetof(t1403, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1403_0_0_32854;
FieldInfo t1403_f2_FieldInfo = 
{
	"None", &t1403_0_0_32854, &t1403_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1403_0_0_32854;
FieldInfo t1403_f3_FieldInfo = 
{
	"AutoDispatch", &t1403_0_0_32854, &t1403_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1403_0_0_32854;
FieldInfo t1403_f4_FieldInfo = 
{
	"AutoDual", &t1403_0_0_32854, &t1403_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1403_FIs[] =
{
	&t1403_f1_FieldInfo,
	&t1403_f2_FieldInfo,
	&t1403_f3_FieldInfo,
	&t1403_f4_FieldInfo,
	NULL
};
static const int32_t t1403_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1403_f2_DefaultValue = 
{
	&t1403_f2_FieldInfo, { (char*)&t1403_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1403_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1403_f3_DefaultValue = 
{
	&t1403_f3_FieldInfo, { (char*)&t1403_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1403_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1403_f4_DefaultValue = 
{
	&t1403_f4_FieldInfo, { (char*)&t1403_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1403_FDVs[] = 
{
	&t1403_f2_DefaultValue,
	&t1403_f3_DefaultValue,
	&t1403_f4_DefaultValue,
	NULL
};
static MethodInfo* t1403_MIs[] =
{
	NULL
};
static MethodInfo* t1403_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1403_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1403_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1403__CustomAttributeCache = {
1,
NULL,
&t1403_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1403_1_0_0;
extern CustomAttributesCache t1403__CustomAttributeCache;
TypeInfo t1403_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ClassInterfaceType", "System.Runtime.InteropServices", t1403_MIs, NULL, t1403_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1403_VT, &t1403__CustomAttributeCache, &t44_TI, &t1403_0_0_0, &t1403_1_0_0, t1403_IOs, NULL, NULL, t1403_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1403)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif



 void m7734 (t1404 * __this, t42 * p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition System.Runtime.InteropServices.ComDefaultInterfaceAttribute
extern Il2CppType t42_0_0_1;
FieldInfo t1404_f0_FieldInfo = 
{
	"_type", &t42_0_0_1, &t1404_TI, offsetof(t1404, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1404_FIs[] =
{
	&t1404_f0_FieldInfo,
	NULL
};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1404_m7734_ParameterInfos[] = 
{
	{"defaultInterface", 0, 134221799, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7734_MI = 
{
	".ctor", (methodPointerType)&m7734, &t1404_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1404_m7734_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3288, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1404_MIs[] =
{
	&m7734_MI,
	NULL
};
static MethodInfo* t1404_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t1404_IOs[] = 
{
	{ &t604_TI, 4},
};
void t1404_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 4, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1404__CustomAttributeCache = {
2,
NULL,
&t1404_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1404_0_0_0;
extern Il2CppType t1404_1_0_0;
struct t1404;
extern CustomAttributesCache t1404__CustomAttributeCache;
TypeInfo t1404_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ComDefaultInterfaceAttribute", "System.Runtime.InteropServices", t1404_MIs, NULL, t1404_FIs, NULL, &t490_TI, NULL, NULL, &t1404_TI, NULL, t1404_VT, &t1404__CustomAttributeCache, &t1404_TI, &t1404_0_0_0, &t1404_1_0_0, t1404_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1404), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t1405.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1405_TI;
#include "t1405MD.h"



// Metadata Definition System.Runtime.InteropServices.ComInterfaceType
extern Il2CppType t44_0_0_1542;
FieldInfo t1405_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1405_TI, offsetof(t1405, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1405_0_0_32854;
FieldInfo t1405_f2_FieldInfo = 
{
	"InterfaceIsDual", &t1405_0_0_32854, &t1405_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1405_0_0_32854;
FieldInfo t1405_f3_FieldInfo = 
{
	"InterfaceIsIUnknown", &t1405_0_0_32854, &t1405_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1405_0_0_32854;
FieldInfo t1405_f4_FieldInfo = 
{
	"InterfaceIsIDispatch", &t1405_0_0_32854, &t1405_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1405_FIs[] =
{
	&t1405_f1_FieldInfo,
	&t1405_f2_FieldInfo,
	&t1405_f3_FieldInfo,
	&t1405_f4_FieldInfo,
	NULL
};
static const int32_t t1405_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1405_f2_DefaultValue = 
{
	&t1405_f2_FieldInfo, { (char*)&t1405_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1405_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1405_f3_DefaultValue = 
{
	&t1405_f3_FieldInfo, { (char*)&t1405_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1405_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1405_f4_DefaultValue = 
{
	&t1405_f4_FieldInfo, { (char*)&t1405_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1405_FDVs[] = 
{
	&t1405_f2_DefaultValue,
	&t1405_f3_DefaultValue,
	&t1405_f4_DefaultValue,
	NULL
};
static MethodInfo* t1405_MIs[] =
{
	NULL
};
static MethodInfo* t1405_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1405_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1405_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1405__CustomAttributeCache = {
1,
NULL,
&t1405_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1405_0_0_0;
extern Il2CppType t1405_1_0_0;
extern CustomAttributesCache t1405__CustomAttributeCache;
TypeInfo t1405_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ComInterfaceType", "System.Runtime.InteropServices", t1405_MIs, NULL, t1405_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1405_VT, &t1405__CustomAttributeCache, &t44_TI, &t1405_0_0_0, &t1405_1_0_0, t1405_IOs, NULL, NULL, t1405_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1405)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#include "t1406.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1406_TI;
#include "t1406MD.h"



extern MethodInfo m7735_MI;
 void m7735 (t1406 * __this, int32_t p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition System.Runtime.InteropServices.DispIdAttribute
extern Il2CppType t44_0_0_1;
FieldInfo t1406_f0_FieldInfo = 
{
	"id", &t44_0_0_1, &t1406_TI, offsetof(t1406, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1406_FIs[] =
{
	&t1406_f0_FieldInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1406_m7735_ParameterInfos[] = 
{
	{"dispId", 0, 134221800, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7735_MI = 
{
	".ctor", (methodPointerType)&m7735, &t1406_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1406_m7735_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3289, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1406_MIs[] =
{
	&m7735_MI,
	NULL
};
static MethodInfo* t1406_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t1406_IOs[] = 
{
	{ &t604_TI, 4},
};
void t1406_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 960, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1406__CustomAttributeCache = {
2,
NULL,
&t1406_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1406_0_0_0;
extern Il2CppType t1406_1_0_0;
struct t1406;
extern CustomAttributesCache t1406__CustomAttributeCache;
TypeInfo t1406_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DispIdAttribute", "System.Runtime.InteropServices", t1406_MIs, NULL, t1406_FIs, NULL, &t490_TI, NULL, NULL, &t1406_TI, NULL, t1406_VT, &t1406__CustomAttributeCache, &t1406_TI, &t1406_0_0_0, &t1406_1_0_0, t1406_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1406), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t1407.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1407_TI;
#include "t1407MD.h"

#include "t1408.h"
#include "t44MD.h"
extern MethodInfo m7737_MI;
extern MethodInfo m7741_MI;
extern MethodInfo m7742_MI;
extern MethodInfo m7736_MI;
extern MethodInfo m7743_MI;
extern MethodInfo m2840_MI;


 void m7736 (t1407 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	{
		if ((((int32_t)p1) < ((int32_t)0)))
		{
			goto IL_0008;
		}
	}
	{
		if ((((int32_t)p1) <= ((int32_t)3)))
		{
			goto IL_000d;
		}
	}

IL_0008:
	{
		p1 = 2;
	}

IL_000d:
	{
		int32_t L_0 = m7742(NULL, p0, 0, p1, &m7742_MI);
		__this->f0 = L_0;
		return;
	}
}
 bool m7737 (t1407 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m7738_MI;
 t29 * m7738 (t1407 * __this, MethodInfo* method){
	{
		bool L_0 = m7737(__this, &m7737_MI);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral1574, &m6079_MI);
		t914 * L_2 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_2, L_1, &m3964_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0018:
	{
		int32_t L_3 = (__this->f0);
		t29 * L_4 = m7741(NULL, L_3, &m7741_MI);
		return L_4;
	}
}
extern MethodInfo m7739_MI;
 t1407  m7739 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method){
	{
		t1407  L_0 = {0};
		m7736(&L_0, p0, p1, &m7736_MI);
		return L_0;
	}
}
extern MethodInfo m7740_MI;
 void m7740 (t1407 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f0);
		m7743(NULL, L_0, &m7743_MI);
		__this->f0 = 0;
		return;
	}
}
 t29 * m7741 (t29 * __this, int32_t p0, MethodInfo* method){
	typedef t29 * (*m7741_ftn) (int32_t);
	static m7741_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7741_ftn)il2cpp_codegen_resolve_icall ("System.Runtime.InteropServices.GCHandle::GetTarget(System.Int32)");
	return _il2cpp_icall_func(p0);
}
 int32_t m7742 (t29 * __this, t29 * p0, int32_t p1, int32_t p2, MethodInfo* method){
	typedef int32_t (*m7742_ftn) (t29 *, int32_t, int32_t);
	static m7742_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7742_ftn)il2cpp_codegen_resolve_icall ("System.Runtime.InteropServices.GCHandle::GetTargetHandle(System.Object,System.Int32,System.Runtime.InteropServices.GCHandleType)");
	return _il2cpp_icall_func(p0, p1, p2);
}
 void m7743 (t29 * __this, int32_t p0, MethodInfo* method){
	typedef void (*m7743_ftn) (int32_t);
	static m7743_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7743_ftn)il2cpp_codegen_resolve_icall ("System.Runtime.InteropServices.GCHandle::FreeHandle(System.Int32)");
	_il2cpp_icall_func(p0);
}
extern MethodInfo m7744_MI;
 bool m7744 (t1407 * __this, t29 * p0, MethodInfo* method){
	t1407  V_0 = {0};
	{
		if (!p0)
		{
			goto IL_000b;
		}
	}
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t1407_TI))))
		{
			goto IL_000d;
		}
	}

IL_000b:
	{
		return 0;
	}

IL_000d:
	{
		int32_t L_0 = (__this->f0);
		V_0 = ((*(t1407 *)((t1407 *)UnBox (p0, InitializedTypeInfo(&t1407_TI)))));
		int32_t L_1 = ((&V_0)->f0);
		return ((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
extern MethodInfo m7745_MI;
 int32_t m7745 (t1407 * __this, MethodInfo* method){
	{
		int32_t* L_0 = &(__this->f0);
		int32_t L_1 = m2840(L_0, &m2840_MI);
		return L_1;
	}
}
// Metadata Definition System.Runtime.InteropServices.GCHandle
extern Il2CppType t44_0_0_1;
FieldInfo t1407_f0_FieldInfo = 
{
	"handle", &t44_0_0_1, &t1407_TI, offsetof(t1407, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t1407_FIs[] =
{
	&t1407_f0_FieldInfo,
	NULL
};
static PropertyInfo t1407____IsAllocated_PropertyInfo = 
{
	&t1407_TI, "IsAllocated", &m7737_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t1407____Target_PropertyInfo = 
{
	&t1407_TI, "Target", &m7738_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1407_PIs[] =
{
	&t1407____IsAllocated_PropertyInfo,
	&t1407____Target_PropertyInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t1408_0_0_0;
extern Il2CppType t1408_0_0_0;
static ParameterInfo t1407_m7736_ParameterInfos[] = 
{
	{"value", 0, 134221801, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"type", 1, 134221802, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7736_MI = 
{
	".ctor", (methodPointerType)&m7736, &t1407_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t1407_m7736_ParameterInfos, &EmptyCustomAttributesCache, 6273, 0, 255, 2, false, false, 3290, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m7737_MI = 
{
	"get_IsAllocated", (methodPointerType)&m7737, &t1407_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3291, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7738_MI = 
{
	"get_Target", (methodPointerType)&m7738, &t1407_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 3292, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t1408_0_0_0;
static ParameterInfo t1407_m7739_ParameterInfos[] = 
{
	{"value", 0, 134221803, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"type", 1, 134221804, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t1407_0_0_0;
extern void* RuntimeInvoker_t1407_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7739_MI = 
{
	"Alloc", (methodPointerType)&m7739, &t1407_TI, &t1407_0_0_0, RuntimeInvoker_t1407_t29_t44, t1407_m7739_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 3293, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7740_MI = 
{
	"Free", (methodPointerType)&m7740, &t1407_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 3294, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1407_m7741_ParameterInfos[] = 
{
	{"handle", 0, 134221805, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7741_MI = 
{
	"GetTarget", (methodPointerType)&m7741, &t1407_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t1407_m7741_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 1, false, false, 3295, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1408_0_0_0;
static ParameterInfo t1407_m7742_ParameterInfos[] = 
{
	{"obj", 0, 134221806, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"handle", 1, 134221807, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"type", 2, 134221808, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7742_MI = 
{
	"GetTargetHandle", (methodPointerType)&m7742, &t1407_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t44_t44, t1407_m7742_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 3, false, false, 3296, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t1407_m7743_ParameterInfos[] = 
{
	{"handle", 0, 134221809, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7743_MI = 
{
	"FreeHandle", (methodPointerType)&m7743, &t1407_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1407_m7743_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 1, false, false, 3297, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
static ParameterInfo t1407_m7744_ParameterInfos[] = 
{
	{"o", 0, 134221810, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7744_MI = 
{
	"Equals", (methodPointerType)&m7744, &t1407_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t1407_m7744_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 0, 1, false, false, 3298, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7745_MI = 
{
	"GetHashCode", (methodPointerType)&m7745, &t1407_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 198, 0, 2, 0, false, false, 3299, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1407_MIs[] =
{
	&m7736_MI,
	&m7737_MI,
	&m7738_MI,
	&m7739_MI,
	&m7740_MI,
	&m7741_MI,
	&m7742_MI,
	&m7743_MI,
	&m7744_MI,
	&m7745_MI,
	NULL
};
static MethodInfo* t1407_VT[] =
{
	&m7744_MI,
	&m46_MI,
	&m7745_MI,
	&m1500_MI,
};
void t1407_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1168 * tmp;
		tmp = (t1168 *)il2cpp_codegen_object_new (&t1168_TI);
		m6082(tmp, il2cpp_codegen_string_new_wrapper("Struct should be [StructLayout(LayoutKind.Sequential)] but will need to be reordered for that."), &m6082_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1407__CustomAttributeCache = {
2,
NULL,
&t1407_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1407_0_0_0;
extern Il2CppType t1407_1_0_0;
extern CustomAttributesCache t1407__CustomAttributeCache;
TypeInfo t1407_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GCHandle", "System.Runtime.InteropServices", t1407_MIs, t1407_PIs, t1407_FIs, NULL, &t110_TI, NULL, NULL, &t1407_TI, NULL, t1407_VT, &t1407__CustomAttributeCache, &t1407_TI, &t1407_0_0_0, &t1407_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1407)+ sizeof (Il2CppObject), 0, sizeof(t1407 ), 0, 0, -1, 1048841, 0, true, false, false, false, false, false, false, false, false, false, true, true, 10, 2, 1, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1408_TI;
#include "t1408MD.h"



// Metadata Definition System.Runtime.InteropServices.GCHandleType
extern Il2CppType t44_0_0_1542;
FieldInfo t1408_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1408_TI, offsetof(t1408, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1408_0_0_32854;
FieldInfo t1408_f2_FieldInfo = 
{
	"Weak", &t1408_0_0_32854, &t1408_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1408_0_0_32854;
FieldInfo t1408_f3_FieldInfo = 
{
	"WeakTrackResurrection", &t1408_0_0_32854, &t1408_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1408_0_0_32854;
FieldInfo t1408_f4_FieldInfo = 
{
	"Normal", &t1408_0_0_32854, &t1408_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1408_0_0_32854;
FieldInfo t1408_f5_FieldInfo = 
{
	"Pinned", &t1408_0_0_32854, &t1408_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1408_FIs[] =
{
	&t1408_f1_FieldInfo,
	&t1408_f2_FieldInfo,
	&t1408_f3_FieldInfo,
	&t1408_f4_FieldInfo,
	&t1408_f5_FieldInfo,
	NULL
};
static const int32_t t1408_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t1408_f2_DefaultValue = 
{
	&t1408_f2_FieldInfo, { (char*)&t1408_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1408_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t1408_f3_DefaultValue = 
{
	&t1408_f3_FieldInfo, { (char*)&t1408_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1408_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1408_f4_DefaultValue = 
{
	&t1408_f4_FieldInfo, { (char*)&t1408_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1408_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1408_f5_DefaultValue = 
{
	&t1408_f5_FieldInfo, { (char*)&t1408_f5_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1408_FDVs[] = 
{
	&t1408_f2_DefaultValue,
	&t1408_f3_DefaultValue,
	&t1408_f4_DefaultValue,
	&t1408_f5_DefaultValue,
	NULL
};
static MethodInfo* t1408_MIs[] =
{
	NULL
};
static MethodInfo* t1408_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1408_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1408_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1408__CustomAttributeCache = {
1,
NULL,
&t1408_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1408_1_0_0;
extern CustomAttributesCache t1408__CustomAttributeCache;
TypeInfo t1408_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GCHandleType", "System.Runtime.InteropServices", t1408_MIs, NULL, t1408_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1408_VT, &t1408__CustomAttributeCache, &t44_TI, &t1408_0_0_0, &t1408_1_0_0, t1408_IOs, NULL, NULL, t1408_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1408)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 23, 0, 3};
#include "t1409.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1409_TI;
#include "t1409MD.h"



extern MethodInfo m7746_MI;
 void m7746 (t1409 * __this, int32_t p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		return;
	}
}
// Metadata Definition System.Runtime.InteropServices.InterfaceTypeAttribute
extern Il2CppType t1405_0_0_1;
FieldInfo t1409_f0_FieldInfo = 
{
	"intType", &t1405_0_0_1, &t1409_TI, offsetof(t1409, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1409_FIs[] =
{
	&t1409_f0_FieldInfo,
	NULL
};
extern Il2CppType t1405_0_0_0;
static ParameterInfo t1409_m7746_ParameterInfos[] = 
{
	{"interfaceType", 0, 134221811, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7746_MI = 
{
	".ctor", (methodPointerType)&m7746, &t1409_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t1409_m7746_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3300, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1409_MIs[] =
{
	&m7746_MI,
	NULL
};
static MethodInfo* t1409_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t1409_IOs[] = 
{
	{ &t604_TI, 4},
};
void t1409_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 1024, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1409__CustomAttributeCache = {
2,
NULL,
&t1409_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1409_0_0_0;
extern Il2CppType t1409_1_0_0;
struct t1409;
extern CustomAttributesCache t1409__CustomAttributeCache;
TypeInfo t1409_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InterfaceTypeAttribute", "System.Runtime.InteropServices", t1409_MIs, NULL, t1409_FIs, NULL, &t490_TI, NULL, NULL, &t1409_TI, NULL, t1409_VT, &t1409__CustomAttributeCache, &t1409_TI, &t1409_0_0_0, &t1409_1_0_0, t1409_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1409), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t1410.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1410_TI;
#include "t1410MD.h"

#include "t1643.h"
#include "t1644.h"
#include "t941MD.h"
#include "t1643MD.h"
extern MethodInfo m9240_MI;
extern MethodInfo m9486_MI;
extern MethodInfo m7748_MI;


extern MethodInfo m7747_MI;
 void m7747 (t29 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		((t1410_SFs*)InitializedTypeInfo(&t1410_TI)->static_fields)->f0 = 2;
		t1643 * L_0 = m9240(NULL, &m9240_MI);
		int32_t L_1 = m9486(L_0, &m9486_MI);
		if ((((uint32_t)L_1) != ((uint32_t)2)))
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 2;
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 1;
	}

IL_0017:
	{
		((t1410_SFs*)InitializedTypeInfo(&t1410_TI)->static_fields)->f1 = G_B3_0;
		return;
	}
}
 void m7748 (t29 * __this, t35 p0, int32_t p1, t20 * p2, int32_t p3, MethodInfo* method){
	typedef void (*m7748_ftn) (t35, int32_t, t20 *, int32_t);
	static m7748_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7748_ftn)il2cpp_codegen_resolve_icall ("System.Runtime.InteropServices.Marshal::copy_from_unmanaged(System.IntPtr,System.Int32,System.Array,System.Int32)");
	_il2cpp_icall_func(p0, p1, p2, p3);
}
extern MethodInfo m7749_MI;
 void m7749 (t29 * __this, t35 p0, t200* p1, int32_t p2, int32_t p3, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1410_TI));
		m7748(NULL, p0, p2, (t20 *)(t20 *)p1, p3, &m7748_MI);
		return;
	}
}
// Metadata Definition System.Runtime.InteropServices.Marshal
extern Il2CppType t44_0_0_54;
FieldInfo t1410_f0_FieldInfo = 
{
	"SystemMaxDBCSCharSize", &t44_0_0_54, &t1410_TI, offsetof(t1410_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_54;
FieldInfo t1410_f1_FieldInfo = 
{
	"SystemDefaultCharSize", &t44_0_0_54, &t1410_TI, offsetof(t1410_SFs, f1), &EmptyCustomAttributesCache};
static FieldInfo* t1410_FIs[] =
{
	&t1410_f0_FieldInfo,
	&t1410_f1_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7747_MI = 
{
	".cctor", (methodPointerType)&m7747, &t1410_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 3301, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1410_m7748_ParameterInfos[] = 
{
	{"source", 0, 134221812, &EmptyCustomAttributesCache, &t35_0_0_0},
	{"startIndex", 1, 134221813, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"destination", 2, 134221814, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"length", 3, 134221815, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t35_t44_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7748_MI = 
{
	"copy_from_unmanaged", (methodPointerType)&m7748, &t1410_TI, &t21_0_0_0, RuntimeInvoker_t21_t35_t44_t29_t44, t1410_m7748_ParameterInfos, &EmptyCustomAttributesCache, 145, 4096, 255, 4, false, false, 3302, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
extern Il2CppType t200_0_0_0;
extern Il2CppType t200_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1410_m7749_ParameterInfos[] = 
{
	{"source", 0, 134221816, &EmptyCustomAttributesCache, &t35_0_0_0},
	{"destination", 1, 134221817, &EmptyCustomAttributesCache, &t200_0_0_0},
	{"startIndex", 2, 134221818, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"length", 3, 134221819, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t35_t29_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7749_MI = 
{
	"Copy", (methodPointerType)&m7749, &t1410_TI, &t21_0_0_0, RuntimeInvoker_t21_t35_t29_t44_t44, t1410_m7749_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 4, false, false, 3303, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1410_MIs[] =
{
	&m7747_MI,
	&m7748_MI,
	&m7749_MI,
	NULL
};
static MethodInfo* t1410_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t1572_TI;
#include "t1572.h"
#include "t1572MD.h"
extern MethodInfo m8529_MI;
void t1410_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1572 * tmp;
		tmp = (t1572 *)il2cpp_codegen_object_new (&t1572_TI);
		m8529(tmp, &m8529_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1410__CustomAttributeCache = {
1,
NULL,
&t1410_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1410_0_0_0;
extern Il2CppType t1410_1_0_0;
struct t1410;
extern CustomAttributesCache t1410__CustomAttributeCache;
TypeInfo t1410_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Marshal", "System.Runtime.InteropServices", t1410_MIs, NULL, t1410_FIs, NULL, &t29_TI, NULL, NULL, &t1410_TI, NULL, t1410_VT, &t1410__CustomAttributeCache, &t1410_TI, &t1410_0_0_0, &t1410_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1410), 0, -1, sizeof(t1410_SFs), 0, -1, 262529, 0, false, false, false, false, false, false, false, false, false, true, false, false, 3, 0, 2, 0, 0, 4, 0, 0};
#include "t1411.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1411_TI;
#include "t1411MD.h"

extern MethodInfo m2946_MI;
extern MethodInfo m9517_MI;


extern MethodInfo m7750_MI;
 void m7750 (t1411 * __this, MethodInfo* method){
	{
		t7* L_0 = m6079(NULL, (t7*) &_stringLiteral1575, &m6079_MI);
		m4202(__this, L_0, &m4202_MI);
		m2946(__this, ((int32_t)-2146233035), &m2946_MI);
		return;
	}
}
extern MethodInfo m7751_MI;
 void m7751 (t1411 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m9517(__this, p0, p1, &m9517_MI);
		return;
	}
}
// Metadata Definition System.Runtime.InteropServices.MarshalDirectiveException
extern Il2CppType t44_0_0_32849;
FieldInfo t1411_f11_FieldInfo = 
{
	"ErrorCode", &t44_0_0_32849, &t1411_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1411_FIs[] =
{
	&t1411_f11_FieldInfo,
	NULL
};
static const int32_t t1411_f11_DefaultValueData = -2146233035;
static Il2CppFieldDefaultValueEntry t1411_f11_DefaultValue = 
{
	&t1411_f11_FieldInfo, { (char*)&t1411_f11_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1411_FDVs[] = 
{
	&t1411_f11_DefaultValue,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7750_MI = 
{
	".ctor", (methodPointerType)&m7750, &t1411_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3304, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t1411_m7751_ParameterInfos[] = 
{
	{"info", 0, 134221820, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134221821, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
MethodInfo m7751_MI = 
{
	".ctor", (methodPointerType)&m7751, &t1411_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t1411_m7751_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, false, 3305, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1411_MIs[] =
{
	&m7750_MI,
	&m7751_MI,
	NULL
};
static MethodInfo* t1411_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m2856_MI,
	&m2857_MI,
	&m2858_MI,
	&m1684_MI,
	&m2859_MI,
	&m2860_MI,
	&m2857_MI,
	&m2861_MI,
};
static Il2CppInterfaceOffsetPair t1411_IOs[] = 
{
	{ &t374_TI, 4},
	{ &t590_TI, 5},
};
void t1411_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1411__CustomAttributeCache = {
1,
NULL,
&t1411_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1411_0_0_0;
extern Il2CppType t1411_1_0_0;
struct t1411;
extern CustomAttributesCache t1411__CustomAttributeCache;
TypeInfo t1411_TI = 
{
	&g_mscorlib_dll_Image, NULL, "MarshalDirectiveException", "System.Runtime.InteropServices", t1411_MIs, NULL, t1411_FIs, NULL, &t956_TI, NULL, NULL, &t1411_TI, NULL, t1411_VT, &t1411__CustomAttributeCache, &t1411_TI, &t1411_0_0_0, &t1411_1_0_0, t1411_IOs, NULL, NULL, t1411_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1411), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 11, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m7752 (t1412 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition System.Runtime.InteropServices.PreserveSigAttribute
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7752_MI = 
{
	".ctor", (methodPointerType)&m7752, &t1412_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3306, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1412_MIs[] =
{
	&m7752_MI,
	NULL
};
static MethodInfo* t1412_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t1412_IOs[] = 
{
	{ &t604_TI, 4},
};
void t1412_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 64, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1412__CustomAttributeCache = {
2,
NULL,
&t1412_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1412_0_0_0;
extern Il2CppType t1412_1_0_0;
struct t1412;
extern CustomAttributesCache t1412__CustomAttributeCache;
TypeInfo t1412_TI = 
{
	&g_mscorlib_dll_Image, NULL, "PreserveSigAttribute", "System.Runtime.InteropServices", t1412_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t1412_TI, NULL, t1412_VT, &t1412__CustomAttributeCache, &t1412_TI, &t1412_0_0_0, &t1412_1_0_0, t1412_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1412), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t1171.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1171_TI;
#include "t1171MD.h"

#include "t1101.h"
extern TypeInfo t1101_TI;
#include "t1101MD.h"
#include "t1601MD.h"
#include "t1097MD.h"
extern MethodInfo m5150_MI;
extern MethodInfo m8777_MI;
extern MethodInfo m10178_MI;
extern MethodInfo m10179_MI;
extern MethodInfo m7759_MI;
extern MethodInfo m5113_MI;
extern MethodInfo m7754_MI;


extern MethodInfo m7753_MI;
 void m7753 (t1171 * __this, t35 p0, bool p1, MethodInfo* method){
	{
		m7730(__this, &m7730_MI);
		__this->f1 = p0;
		__this->f3 = p1;
		__this->f2 = 1;
		return;
	}
}
 void m7754 (t1171 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (__this->f2);
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		t42 * L_1 = m1430(__this, &m1430_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_1);
		t1101 * L_3 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_3, L_2, &m5150_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0019:
	{
		int32_t L_4 = (__this->f2);
		V_1 = L_4;
		V_0 = ((int32_t)(V_1-1));
		int32_t* L_5 = &(__this->f2);
		int32_t L_6 = m8777(NULL, L_5, V_0, V_1, &m8777_MI);
		if ((((uint32_t)L_6) != ((uint32_t)V_1)))
		{
			goto IL_0019;
		}
	}
	{
		if (V_0)
		{
			goto IL_0061;
		}
	}
	{
		bool L_7 = (__this->f3);
		if (!L_7)
		{
			goto IL_0061;
		}
	}
	{
		bool L_8 = (bool)VirtFuncInvoker0< bool >::Invoke(&m10178_MI, __this);
		if (L_8)
		{
			goto IL_0061;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(&m10179_MI, __this);
		t35 L_9 = (__this->f1);
		__this->f0 = L_9;
		__this->f2 = (-1);
	}

IL_0061:
	{
		return;
	}
}
extern MethodInfo m7755_MI;
 void m7755 (t1171 * __this, bool* p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (__this->f2);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		t42 * L_1 = m1430(__this, &m1430_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_1);
		t1101 * L_3 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_3, L_2, &m5150_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_001a:
	{
		int32_t L_4 = (__this->f2);
		V_1 = L_4;
		V_0 = ((int32_t)(V_1+1));
		if ((((int32_t)V_1) > ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		t42 * L_5 = m1430(__this, &m1430_MI);
		t7* L_6 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_5);
		t1101 * L_7 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_7, L_6, &m5150_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_003a:
	{
		int32_t* L_8 = &(__this->f2);
		int32_t L_9 = m8777(NULL, L_8, V_0, V_1, &m8777_MI);
		if ((((uint32_t)L_9) != ((uint32_t)V_1)))
		{
			goto IL_001a;
		}
	}
	{
		*((int8_t*)(p0)) = (int8_t)1;
		return;
	}
}
extern MethodInfo m7756_MI;
 t35 m7756 (t1171 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		t42 * L_1 = m1430(__this, &m1430_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_1);
		t1101 * L_3 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_3, L_2, &m5150_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_001a:
	{
		t35 L_4 = (__this->f0);
		return L_4;
	}
}
extern MethodInfo m7757_MI;
 void m7757 (t1171 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (__this->f2);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		t42 * L_1 = m1430(__this, &m1430_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_1);
		t1101 * L_3 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_3, L_2, &m5150_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_001a:
	{
		int32_t L_4 = (__this->f2);
		V_1 = L_4;
		V_0 = ((int32_t)(V_1-1));
		int32_t* L_5 = &(__this->f2);
		int32_t L_6 = m8777(NULL, L_5, V_0, V_1, &m8777_MI);
		if ((((uint32_t)L_6) != ((uint32_t)V_1)))
		{
			goto IL_001a;
		}
	}
	{
		if (V_0)
		{
			goto IL_005b;
		}
	}
	{
		bool L_7 = (__this->f3);
		if (!L_7)
		{
			goto IL_005b;
		}
	}
	{
		bool L_8 = (bool)VirtFuncInvoker0< bool >::Invoke(&m10178_MI, __this);
		if (L_8)
		{
			goto IL_005b;
		}
	}
	{
		VirtFuncInvoker0< bool >::Invoke(&m10179_MI, __this);
		t35 L_9 = (__this->f1);
		__this->f0 = L_9;
	}

IL_005b:
	{
		return;
	}
}
extern MethodInfo m7758_MI;
 void m7758 (t1171 * __this, MethodInfo* method){
	{
		VirtActionInvoker1< bool >::Invoke(&m7759_MI, __this, 1);
		m5113(NULL, __this, &m5113_MI);
		return;
	}
}
 void m7759 (t1171 * __this, bool p0, MethodInfo* method){
	{
		if (!p0)
		{
			goto IL_000b;
		}
	}
	{
		m7754(__this, &m7754_MI);
		goto IL_000b;
	}

IL_000b:
	{
		return;
	}
}
extern MethodInfo m7760_MI;
 void m7760 (t1171 * __this, t35 p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m7761_MI;
 void m7761 (t1171 * __this, MethodInfo* method){
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = (__this->f3);
			if (!L_0)
			{
				goto IL_0023;
			}
		}

IL_0008:
		{
			bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m10178_MI, __this);
			if (L_1)
			{
				goto IL_0023;
			}
		}

IL_0010:
		{
			VirtFuncInvoker0< bool >::Invoke(&m10179_MI, __this);
			t35 L_2 = (__this->f1);
			__this->f0 = L_2;
		}

IL_0023:
		{
			// IL_0023: leave.s IL_002c
			leaveInstructions[0] = 0x2C; // 1
			THROW_SENTINEL(IL_002c);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0025;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0025;
	}

IL_0025:
	{ // begin finally (depth: 1)
		m7731(__this, &m7731_MI);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x2C:
				goto IL_002c;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				t295 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_002c:
	{
		return;
	}
}
// Metadata Definition System.Runtime.InteropServices.SafeHandle
extern Il2CppType t35_0_0_4;
FieldInfo t1171_f0_FieldInfo = 
{
	"handle", &t35_0_0_4, &t1171_TI, offsetof(t1171, f0), &EmptyCustomAttributesCache};
extern Il2CppType t35_0_0_1;
FieldInfo t1171_f1_FieldInfo = 
{
	"invalid_handle_value", &t35_0_0_1, &t1171_TI, offsetof(t1171, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1171_f2_FieldInfo = 
{
	"refcount", &t44_0_0_1, &t1171_TI, offsetof(t1171, f2), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t1171_f3_FieldInfo = 
{
	"owns_handle", &t40_0_0_1, &t1171_TI, offsetof(t1171, f3), &EmptyCustomAttributesCache};
static FieldInfo* t1171_FIs[] =
{
	&t1171_f0_FieldInfo,
	&t1171_f1_FieldInfo,
	&t1171_f2_FieldInfo,
	&t1171_f3_FieldInfo,
	NULL
};
static PropertyInfo t1171____IsInvalid_PropertyInfo = 
{
	&t1171_TI, "IsInvalid", &m10178_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1171_PIs[] =
{
	&t1171____IsInvalid_PropertyInfo,
	NULL
};
extern Il2CppType t35_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t1171_m7753_ParameterInfos[] = 
{
	{"invalidHandleValue", 0, 134221822, &EmptyCustomAttributesCache, &t35_0_0_0},
	{"ownsHandle", 1, 134221823, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t35_t297 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1171__CustomAttributeCache_m7753;
MethodInfo m7753_MI = 
{
	".ctor", (methodPointerType)&m7753, &t1171_TI, &t21_0_0_0, RuntimeInvoker_t21_t35_t297, t1171_m7753_ParameterInfos, &t1171__CustomAttributeCache_m7753, 6276, 0, 255, 2, false, false, 3307, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1171__CustomAttributeCache_m7754;
MethodInfo m7754_MI = 
{
	"Close", (methodPointerType)&m7754, &t1171_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t1171__CustomAttributeCache_m7754, 134, 0, 255, 0, false, false, 3308, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_1_0_0;
extern Il2CppType t40_1_0_0;
static ParameterInfo t1171_m7755_ParameterInfos[] = 
{
	{"success", 0, 134221824, &EmptyCustomAttributesCache, &t40_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t326 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1171__CustomAttributeCache_m7755;
MethodInfo m7755_MI = 
{
	"DangerousAddRef", (methodPointerType)&m7755, &t1171_TI, &t21_0_0_0, RuntimeInvoker_t21_t326, t1171_m7755_ParameterInfos, &t1171__CustomAttributeCache_m7755, 134, 0, 255, 1, false, false, 3309, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
extern void* RuntimeInvoker_t35 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1171__CustomAttributeCache_m7756;
MethodInfo m7756_MI = 
{
	"DangerousGetHandle", (methodPointerType)&m7756, &t1171_TI, &t35_0_0_0, RuntimeInvoker_t35, NULL, &t1171__CustomAttributeCache_m7756, 134, 0, 255, 0, false, false, 3310, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1171__CustomAttributeCache_m7757;
MethodInfo m7757_MI = 
{
	"DangerousRelease", (methodPointerType)&m7757, &t1171_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t1171__CustomAttributeCache_m7757, 134, 0, 255, 0, false, false, 3311, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1171__CustomAttributeCache_m7758;
MethodInfo m7758_MI = 
{
	"Dispose", (methodPointerType)&m7758, &t1171_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t1171__CustomAttributeCache_m7758, 486, 0, 4, 0, false, false, 3312, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t1171_m7759_ParameterInfos[] = 
{
	{"disposing", 0, 134221825, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1171__CustomAttributeCache_m7759;
MethodInfo m7759_MI = 
{
	"Dispose", (methodPointerType)&m7759, &t1171_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t1171_m7759_ParameterInfos, &t1171__CustomAttributeCache_m7759, 452, 0, 5, 1, false, false, 3313, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1171__CustomAttributeCache_m10179;
MethodInfo m10179_MI = 
{
	"ReleaseHandle", NULL, &t1171_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &t1171__CustomAttributeCache_m10179, 1476, 0, 6, 0, false, false, 3314, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t35_0_0_0;
static ParameterInfo t1171_m7760_ParameterInfos[] = 
{
	{"handle", 0, 134221826, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t35 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1171__CustomAttributeCache_m7760;
MethodInfo m7760_MI = 
{
	"SetHandle", (methodPointerType)&m7760, &t1171_TI, &t21_0_0_0, RuntimeInvoker_t21_t35, t1171_m7760_ParameterInfos, &t1171__CustomAttributeCache_m7760, 132, 0, 255, 1, false, false, 3315, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1171__CustomAttributeCache_m10178;
MethodInfo m10178_MI = 
{
	"get_IsInvalid", NULL, &t1171_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &t1171__CustomAttributeCache_m10178, 3526, 0, 7, 0, false, false, 3316, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7761_MI = 
{
	"Finalize", (methodPointerType)&m7761, &t1171_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 1, 0, false, false, 3317, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1171_MIs[] =
{
	&m7753_MI,
	&m7754_MI,
	&m7755_MI,
	&m7756_MI,
	&m7757_MI,
	&m7758_MI,
	&m7759_MI,
	&m10179_MI,
	&m7760_MI,
	&m10178_MI,
	&m7761_MI,
	NULL
};
static MethodInfo* t1171_VT[] =
{
	&m1321_MI,
	&m7761_MI,
	&m1322_MI,
	&m1332_MI,
	&m7758_MI,
	&m7759_MI,
	NULL,
	NULL,
};
extern TypeInfo t324_TI;
static TypeInfo* t1171_ITIs[] = 
{
	&t324_TI,
};
static Il2CppInterfaceOffsetPair t1171_IOs[] = 
{
	{ &t324_TI, 4},
};
void t1171_CustomAttributesCacheGenerator_m7753(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 1, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1171_CustomAttributesCacheGenerator_m7754(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1171_CustomAttributesCacheGenerator_m7755(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 1, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1171_CustomAttributesCacheGenerator_m7756(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1171_CustomAttributesCacheGenerator_m7757(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1171_CustomAttributesCacheGenerator_m7758(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1171_CustomAttributesCacheGenerator_m7759(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1171_CustomAttributesCacheGenerator_m10179(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1171_CustomAttributesCacheGenerator_m7760(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t1171_CustomAttributesCacheGenerator_m10178(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1400 * tmp;
		tmp = (t1400 *)il2cpp_codegen_object_new (&t1400_TI);
		m7732(tmp, 3, 2, &m7732_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1171__CustomAttributeCache_m7753 = {
1,
NULL,
&t1171_CustomAttributesCacheGenerator_m7753
};
CustomAttributesCache t1171__CustomAttributeCache_m7754 = {
1,
NULL,
&t1171_CustomAttributesCacheGenerator_m7754
};
CustomAttributesCache t1171__CustomAttributeCache_m7755 = {
1,
NULL,
&t1171_CustomAttributesCacheGenerator_m7755
};
CustomAttributesCache t1171__CustomAttributeCache_m7756 = {
1,
NULL,
&t1171_CustomAttributesCacheGenerator_m7756
};
CustomAttributesCache t1171__CustomAttributeCache_m7757 = {
1,
NULL,
&t1171_CustomAttributesCacheGenerator_m7757
};
CustomAttributesCache t1171__CustomAttributeCache_m7758 = {
1,
NULL,
&t1171_CustomAttributesCacheGenerator_m7758
};
CustomAttributesCache t1171__CustomAttributeCache_m7759 = {
1,
NULL,
&t1171_CustomAttributesCacheGenerator_m7759
};
CustomAttributesCache t1171__CustomAttributeCache_m10179 = {
1,
NULL,
&t1171_CustomAttributesCacheGenerator_m10179
};
CustomAttributesCache t1171__CustomAttributeCache_m7760 = {
1,
NULL,
&t1171_CustomAttributesCacheGenerator_m7760
};
CustomAttributesCache t1171__CustomAttributeCache_m10178 = {
1,
NULL,
&t1171_CustomAttributesCacheGenerator_m10178
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1171_0_0_0;
extern Il2CppType t1171_1_0_0;
struct t1171;
extern CustomAttributesCache t1171__CustomAttributeCache_m7753;
extern CustomAttributesCache t1171__CustomAttributeCache_m7754;
extern CustomAttributesCache t1171__CustomAttributeCache_m7755;
extern CustomAttributesCache t1171__CustomAttributeCache_m7756;
extern CustomAttributesCache t1171__CustomAttributeCache_m7757;
extern CustomAttributesCache t1171__CustomAttributeCache_m7758;
extern CustomAttributesCache t1171__CustomAttributeCache_m7759;
extern CustomAttributesCache t1171__CustomAttributeCache_m10179;
extern CustomAttributesCache t1171__CustomAttributeCache_m7760;
extern CustomAttributesCache t1171__CustomAttributeCache_m10178;
TypeInfo t1171_TI = 
{
	&g_mscorlib_dll_Image, NULL, "SafeHandle", "System.Runtime.InteropServices", t1171_MIs, t1171_PIs, t1171_FIs, NULL, &t1399_TI, NULL, NULL, &t1171_TI, t1171_ITIs, t1171_VT, &EmptyCustomAttributesCache, &t1171_TI, &t1171_0_0_0, &t1171_1_0_0, t1171_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1171), 0, sizeof(void*), 0, 0, -1, 1048705, 0, false, false, false, false, false, false, false, false, true, false, false, false, 11, 1, 4, 0, 0, 8, 1, 1};
#include "t1413.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1413_TI;
#include "t1413MD.h"



extern MethodInfo m7762_MI;
 void m7762 (t1413 * __this, t42 * p0, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		t7* L_0 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, p0);
		__this->f0 = L_0;
		return;
	}
}
// Metadata Definition System.Runtime.InteropServices.TypeLibImportClassAttribute
extern Il2CppType t7_0_0_1;
FieldInfo t1413_f0_FieldInfo = 
{
	"_importClass", &t7_0_0_1, &t1413_TI, offsetof(t1413, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1413_FIs[] =
{
	&t1413_f0_FieldInfo,
	NULL
};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1413_m7762_ParameterInfos[] = 
{
	{"importClass", 0, 134221827, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7762_MI = 
{
	".ctor", (methodPointerType)&m7762, &t1413_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t1413_m7762_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, false, 3318, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1413_MIs[] =
{
	&m7762_MI,
	NULL
};
static MethodInfo* t1413_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t1413_IOs[] = 
{
	{ &t604_TI, 4},
};
void t1413_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 1024, &m2915_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1413__CustomAttributeCache = {
2,
NULL,
&t1413_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1413_0_0_0;
extern Il2CppType t1413_1_0_0;
struct t1413;
extern CustomAttributesCache t1413__CustomAttributeCache;
TypeInfo t1413_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeLibImportClassAttribute", "System.Runtime.InteropServices", t1413_MIs, NULL, t1413_FIs, NULL, &t490_TI, NULL, NULL, &t1413_TI, NULL, t1413_VT, &t1413__CustomAttributeCache, &t1413_TI, &t1413_0_0_0, &t1413_1_0_0, t1413_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1413), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 1, 0, 0, 4, 0, 1};
#include "t1414.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1414_TI;
#include "t1414MD.h"



extern MethodInfo m7763_MI;
 void m7763 (t1414 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		__this->f0 = p0;
		__this->f1 = p1;
		return;
	}
}
// Metadata Definition System.Runtime.InteropServices.TypeLibVersionAttribute
extern Il2CppType t44_0_0_1;
FieldInfo t1414_f0_FieldInfo = 
{
	"major", &t44_0_0_1, &t1414_TI, offsetof(t1414, f0), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t1414_f1_FieldInfo = 
{
	"minor", &t44_0_0_1, &t1414_TI, offsetof(t1414, f1), &EmptyCustomAttributesCache};
static FieldInfo* t1414_FIs[] =
{
	&t1414_f0_FieldInfo,
	&t1414_f1_FieldInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t1414_m7763_ParameterInfos[] = 
{
	{"major", 0, 134221828, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"minor", 1, 134221829, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m7763_MI = 
{
	".ctor", (methodPointerType)&m7763, &t1414_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t1414_m7763_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 3319, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1414_MIs[] =
{
	&m7763_MI,
	NULL
};
static MethodInfo* t1414_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
static Il2CppInterfaceOffsetPair t1414_IOs[] = 
{
	{ &t604_TI, 4},
};
extern MethodInfo m2917_MI;
void t1414_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 1, &m2915_MI);
		m2917(tmp, false, &m2917_MI);
		m2916(tmp, false, &m2916_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1414__CustomAttributeCache = {
2,
NULL,
&t1414_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1414_0_0_0;
extern Il2CppType t1414_1_0_0;
struct t1414;
extern CustomAttributesCache t1414__CustomAttributeCache;
TypeInfo t1414_TI = 
{
	&g_mscorlib_dll_Image, NULL, "TypeLibVersionAttribute", "System.Runtime.InteropServices", t1414_MIs, NULL, t1414_FIs, NULL, &t490_TI, NULL, NULL, &t1414_TI, NULL, t1414_VT, &t1414__CustomAttributeCache, &t1414_TI, &t1414_0_0_0, &t1414_1_0_0, t1414_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1414), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 2, 0, 0, 4, 0, 1};
#include "t1156.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1156_TI;
#include "t1156MD.h"



// Metadata Definition System.Runtime.InteropServices.UnmanagedType
extern Il2CppType t44_0_0_1542;
FieldInfo t1156_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t1156_TI, offsetof(t1156, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f2_FieldInfo = 
{
	"Bool", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f3_FieldInfo = 
{
	"I1", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f4_FieldInfo = 
{
	"U1", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f5_FieldInfo = 
{
	"I2", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f6_FieldInfo = 
{
	"U2", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f7_FieldInfo = 
{
	"I4", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f8_FieldInfo = 
{
	"U4", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f9_FieldInfo = 
{
	"I8", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f10_FieldInfo = 
{
	"U8", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f11_FieldInfo = 
{
	"R4", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f12_FieldInfo = 
{
	"R8", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f13_FieldInfo = 
{
	"Currency", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f14_FieldInfo = 
{
	"BStr", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f15_FieldInfo = 
{
	"LPStr", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f16_FieldInfo = 
{
	"LPWStr", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f17_FieldInfo = 
{
	"LPTStr", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f18_FieldInfo = 
{
	"ByValTStr", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f19_FieldInfo = 
{
	"IUnknown", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f20_FieldInfo = 
{
	"IDispatch", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f21_FieldInfo = 
{
	"Struct", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f22_FieldInfo = 
{
	"Interface", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f23_FieldInfo = 
{
	"SafeArray", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f24_FieldInfo = 
{
	"ByValArray", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f25_FieldInfo = 
{
	"SysInt", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f26_FieldInfo = 
{
	"SysUInt", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f27_FieldInfo = 
{
	"VBByRefStr", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f28_FieldInfo = 
{
	"AnsiBStr", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f29_FieldInfo = 
{
	"TBStr", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f30_FieldInfo = 
{
	"VariantBool", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f31_FieldInfo = 
{
	"FunctionPtr", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f32_FieldInfo = 
{
	"AsAny", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f33_FieldInfo = 
{
	"LPArray", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f34_FieldInfo = 
{
	"LPStruct", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f35_FieldInfo = 
{
	"CustomMarshaler", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t1156_0_0_32854;
FieldInfo t1156_f36_FieldInfo = 
{
	"Error", &t1156_0_0_32854, &t1156_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t1156_FIs[] =
{
	&t1156_f1_FieldInfo,
	&t1156_f2_FieldInfo,
	&t1156_f3_FieldInfo,
	&t1156_f4_FieldInfo,
	&t1156_f5_FieldInfo,
	&t1156_f6_FieldInfo,
	&t1156_f7_FieldInfo,
	&t1156_f8_FieldInfo,
	&t1156_f9_FieldInfo,
	&t1156_f10_FieldInfo,
	&t1156_f11_FieldInfo,
	&t1156_f12_FieldInfo,
	&t1156_f13_FieldInfo,
	&t1156_f14_FieldInfo,
	&t1156_f15_FieldInfo,
	&t1156_f16_FieldInfo,
	&t1156_f17_FieldInfo,
	&t1156_f18_FieldInfo,
	&t1156_f19_FieldInfo,
	&t1156_f20_FieldInfo,
	&t1156_f21_FieldInfo,
	&t1156_f22_FieldInfo,
	&t1156_f23_FieldInfo,
	&t1156_f24_FieldInfo,
	&t1156_f25_FieldInfo,
	&t1156_f26_FieldInfo,
	&t1156_f27_FieldInfo,
	&t1156_f28_FieldInfo,
	&t1156_f29_FieldInfo,
	&t1156_f30_FieldInfo,
	&t1156_f31_FieldInfo,
	&t1156_f32_FieldInfo,
	&t1156_f33_FieldInfo,
	&t1156_f34_FieldInfo,
	&t1156_f35_FieldInfo,
	&t1156_f36_FieldInfo,
	NULL
};
static const int32_t t1156_f2_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t1156_f2_DefaultValue = 
{
	&t1156_f2_FieldInfo, { (char*)&t1156_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f3_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t1156_f3_DefaultValue = 
{
	&t1156_f3_FieldInfo, { (char*)&t1156_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f4_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t1156_f4_DefaultValue = 
{
	&t1156_f4_FieldInfo, { (char*)&t1156_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f5_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry t1156_f5_DefaultValue = 
{
	&t1156_f5_FieldInfo, { (char*)&t1156_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f6_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry t1156_f6_DefaultValue = 
{
	&t1156_f6_FieldInfo, { (char*)&t1156_f6_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f7_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry t1156_f7_DefaultValue = 
{
	&t1156_f7_FieldInfo, { (char*)&t1156_f7_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f8_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry t1156_f8_DefaultValue = 
{
	&t1156_f8_FieldInfo, { (char*)&t1156_f8_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f9_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry t1156_f9_DefaultValue = 
{
	&t1156_f9_FieldInfo, { (char*)&t1156_f9_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f10_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t1156_f10_DefaultValue = 
{
	&t1156_f10_FieldInfo, { (char*)&t1156_f10_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f11_DefaultValueData = 11;
static Il2CppFieldDefaultValueEntry t1156_f11_DefaultValue = 
{
	&t1156_f11_FieldInfo, { (char*)&t1156_f11_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f12_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry t1156_f12_DefaultValue = 
{
	&t1156_f12_FieldInfo, { (char*)&t1156_f12_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f13_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry t1156_f13_DefaultValue = 
{
	&t1156_f13_FieldInfo, { (char*)&t1156_f13_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f14_DefaultValueData = 19;
static Il2CppFieldDefaultValueEntry t1156_f14_DefaultValue = 
{
	&t1156_f14_FieldInfo, { (char*)&t1156_f14_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f15_DefaultValueData = 20;
static Il2CppFieldDefaultValueEntry t1156_f15_DefaultValue = 
{
	&t1156_f15_FieldInfo, { (char*)&t1156_f15_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f16_DefaultValueData = 21;
static Il2CppFieldDefaultValueEntry t1156_f16_DefaultValue = 
{
	&t1156_f16_FieldInfo, { (char*)&t1156_f16_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f17_DefaultValueData = 22;
static Il2CppFieldDefaultValueEntry t1156_f17_DefaultValue = 
{
	&t1156_f17_FieldInfo, { (char*)&t1156_f17_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f18_DefaultValueData = 23;
static Il2CppFieldDefaultValueEntry t1156_f18_DefaultValue = 
{
	&t1156_f18_FieldInfo, { (char*)&t1156_f18_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f19_DefaultValueData = 25;
static Il2CppFieldDefaultValueEntry t1156_f19_DefaultValue = 
{
	&t1156_f19_FieldInfo, { (char*)&t1156_f19_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f20_DefaultValueData = 26;
static Il2CppFieldDefaultValueEntry t1156_f20_DefaultValue = 
{
	&t1156_f20_FieldInfo, { (char*)&t1156_f20_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f21_DefaultValueData = 27;
static Il2CppFieldDefaultValueEntry t1156_f21_DefaultValue = 
{
	&t1156_f21_FieldInfo, { (char*)&t1156_f21_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f22_DefaultValueData = 28;
static Il2CppFieldDefaultValueEntry t1156_f22_DefaultValue = 
{
	&t1156_f22_FieldInfo, { (char*)&t1156_f22_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f23_DefaultValueData = 29;
static Il2CppFieldDefaultValueEntry t1156_f23_DefaultValue = 
{
	&t1156_f23_FieldInfo, { (char*)&t1156_f23_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f24_DefaultValueData = 30;
static Il2CppFieldDefaultValueEntry t1156_f24_DefaultValue = 
{
	&t1156_f24_FieldInfo, { (char*)&t1156_f24_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f25_DefaultValueData = 31;
static Il2CppFieldDefaultValueEntry t1156_f25_DefaultValue = 
{
	&t1156_f25_FieldInfo, { (char*)&t1156_f25_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f26_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry t1156_f26_DefaultValue = 
{
	&t1156_f26_FieldInfo, { (char*)&t1156_f26_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f27_DefaultValueData = 34;
static Il2CppFieldDefaultValueEntry t1156_f27_DefaultValue = 
{
	&t1156_f27_FieldInfo, { (char*)&t1156_f27_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f28_DefaultValueData = 35;
static Il2CppFieldDefaultValueEntry t1156_f28_DefaultValue = 
{
	&t1156_f28_FieldInfo, { (char*)&t1156_f28_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f29_DefaultValueData = 36;
static Il2CppFieldDefaultValueEntry t1156_f29_DefaultValue = 
{
	&t1156_f29_FieldInfo, { (char*)&t1156_f29_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f30_DefaultValueData = 37;
static Il2CppFieldDefaultValueEntry t1156_f30_DefaultValue = 
{
	&t1156_f30_FieldInfo, { (char*)&t1156_f30_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f31_DefaultValueData = 38;
static Il2CppFieldDefaultValueEntry t1156_f31_DefaultValue = 
{
	&t1156_f31_FieldInfo, { (char*)&t1156_f31_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f32_DefaultValueData = 40;
static Il2CppFieldDefaultValueEntry t1156_f32_DefaultValue = 
{
	&t1156_f32_FieldInfo, { (char*)&t1156_f32_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f33_DefaultValueData = 42;
static Il2CppFieldDefaultValueEntry t1156_f33_DefaultValue = 
{
	&t1156_f33_FieldInfo, { (char*)&t1156_f33_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f34_DefaultValueData = 43;
static Il2CppFieldDefaultValueEntry t1156_f34_DefaultValue = 
{
	&t1156_f34_FieldInfo, { (char*)&t1156_f34_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f35_DefaultValueData = 44;
static Il2CppFieldDefaultValueEntry t1156_f35_DefaultValue = 
{
	&t1156_f35_FieldInfo, { (char*)&t1156_f35_DefaultValueData, &t44_0_0_0 }};
static const int32_t t1156_f36_DefaultValueData = 45;
static Il2CppFieldDefaultValueEntry t1156_f36_DefaultValue = 
{
	&t1156_f36_FieldInfo, { (char*)&t1156_f36_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t1156_FDVs[] = 
{
	&t1156_f2_DefaultValue,
	&t1156_f3_DefaultValue,
	&t1156_f4_DefaultValue,
	&t1156_f5_DefaultValue,
	&t1156_f6_DefaultValue,
	&t1156_f7_DefaultValue,
	&t1156_f8_DefaultValue,
	&t1156_f9_DefaultValue,
	&t1156_f10_DefaultValue,
	&t1156_f11_DefaultValue,
	&t1156_f12_DefaultValue,
	&t1156_f13_DefaultValue,
	&t1156_f14_DefaultValue,
	&t1156_f15_DefaultValue,
	&t1156_f16_DefaultValue,
	&t1156_f17_DefaultValue,
	&t1156_f18_DefaultValue,
	&t1156_f19_DefaultValue,
	&t1156_f20_DefaultValue,
	&t1156_f21_DefaultValue,
	&t1156_f22_DefaultValue,
	&t1156_f23_DefaultValue,
	&t1156_f24_DefaultValue,
	&t1156_f25_DefaultValue,
	&t1156_f26_DefaultValue,
	&t1156_f27_DefaultValue,
	&t1156_f28_DefaultValue,
	&t1156_f29_DefaultValue,
	&t1156_f30_DefaultValue,
	&t1156_f31_DefaultValue,
	&t1156_f32_DefaultValue,
	&t1156_f33_DefaultValue,
	&t1156_f34_DefaultValue,
	&t1156_f35_DefaultValue,
	&t1156_f36_DefaultValue,
	NULL
};
static MethodInfo* t1156_MIs[] =
{
	NULL
};
static MethodInfo* t1156_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t1156_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
void t1156_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t1156__CustomAttributeCache = {
1,
NULL,
&t1156_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1156_0_0_0;
extern Il2CppType t1156_1_0_0;
extern CustomAttributesCache t1156__CustomAttributeCache;
TypeInfo t1156_TI = 
{
	&g_mscorlib_dll_Image, NULL, "UnmanagedType", "System.Runtime.InteropServices", t1156_MIs, NULL, t1156_FIs, NULL, &t49_TI, NULL, NULL, &t44_TI, NULL, t1156_VT, &t1156__CustomAttributeCache, &t44_TI, &t1156_0_0_0, &t1156_1_0_0, t1156_IOs, NULL, NULL, t1156_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1156)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 8449, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 36, 0, 0, 23, 0, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2038_TI;



// Metadata Definition System.Runtime.InteropServices._Activator
static MethodInfo* t2038_MIs[] =
{
	NULL
};
extern TypeInfo t931_TI;
#include "t931.h"
extern TypeInfo t436_TI;
#include "t436.h"
#include "t436MD.h"
extern MethodInfo m2054_MI;
void t2038_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t931_TI)), &m7762_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("03973551-57A1-3900-A2B5-9083E3FF2943"), &m2054_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2038__CustomAttributeCache = {
5,
NULL,
&t2038_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2038_0_0_0;
extern Il2CppType t2038_1_0_0;
struct t2038;
extern CustomAttributesCache t2038__CustomAttributeCache;
TypeInfo t2038_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_Activator", "System.Runtime.InteropServices", t2038_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2038_TI, NULL, NULL, &t2038__CustomAttributeCache, &t2038_TI, &t2038_0_0_0, &t2038_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2007_TI;



// Metadata Definition System.Runtime.InteropServices._Assembly
static MethodInfo* t2007_MIs[] =
{
	NULL
};
void t2007_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 0, &m7746_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t929_TI)), &m7762_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("17156360-2F1A-384A-BC52-FDE93C215C5B"), &m2054_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2007__CustomAttributeCache = {
5,
NULL,
&t2007_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2007_0_0_0;
extern Il2CppType t2007_1_0_0;
struct t2007;
extern CustomAttributesCache t2007__CustomAttributeCache;
TypeInfo t2007_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_Assembly", "System.Runtime.InteropServices", t2007_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2007_TI, NULL, NULL, &t2007__CustomAttributeCache, &t2007_TI, &t2007_0_0_0, &t2007_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2006_TI;



// Metadata Definition System.Runtime.InteropServices._AssemblyBuilder
static MethodInfo* t2006_MIs[] =
{
	NULL
};
extern TypeInfo t1335_TI;
#include "t1335.h"
void t2006_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("BEBB2505-8B54-3443-AEAD-142A16DD9CC7"), &m2054_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1335_TI)), &m7762_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2006__CustomAttributeCache = {
5,
NULL,
&t2006_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2006_0_0_0;
extern Il2CppType t2006_1_0_0;
struct t2006;
extern CustomAttributesCache t2006__CustomAttributeCache;
TypeInfo t2006_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_AssemblyBuilder", "System.Runtime.InteropServices", t2006_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2006_TI, NULL, NULL, &t2006__CustomAttributeCache, &t2006_TI, &t2006_0_0_0, &t2006_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2021_TI;



// Metadata Definition System.Runtime.InteropServices._AssemblyName
static MethodInfo* t2021_MIs[] =
{
	NULL
};
extern TypeInfo t1338_TI;
#include "t1338.h"
void t2021_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("B42B6AAC-317E-34D5-9FA9-093BB4160C50"), &m2054_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1338_TI)), &m7762_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2021__CustomAttributeCache = {
5,
NULL,
&t2021_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2021_0_0_0;
extern Il2CppType t2021_1_0_0;
struct t2021;
extern CustomAttributesCache t2021__CustomAttributeCache;
TypeInfo t2021_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_AssemblyName", "System.Runtime.InteropServices", t2021_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2021_TI, NULL, NULL, &t2021__CustomAttributeCache, &t2021_TI, &t2021_0_0_0, &t2021_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2008_TI;



// Metadata Definition System.Runtime.InteropServices._ConstructorBuilder
static MethodInfo* t2008_MIs[] =
{
	NULL
};
extern TypeInfo t1339_TI;
#include "t1339.h"
void t2008_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("ED3E4384-D7E2-3FA7-8FFD-8940D330519A"), &m2054_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1339_TI)), &m7762_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2008__CustomAttributeCache = {
5,
NULL,
&t2008_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2008_0_0_0;
extern Il2CppType t2008_1_0_0;
struct t2008;
extern CustomAttributesCache t2008__CustomAttributeCache;
TypeInfo t2008_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_ConstructorBuilder", "System.Runtime.InteropServices", t2008_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2008_TI, NULL, NULL, &t2008__CustomAttributeCache, &t2008_TI, &t2008_0_0_0, &t2008_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.InteropServices._ConstructorInfo
static MethodInfo* t2009_MIs[] =
{
	NULL
};
void t2009_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t660_TI)), &m7762_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("E9A19478-9646-3679-9B10-8411AE1FD57D"), &m2054_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2009__CustomAttributeCache = {
5,
NULL,
&t2009_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2009_0_0_0;
extern Il2CppType t2009_1_0_0;
struct t2009;
extern CustomAttributesCache t2009__CustomAttributeCache;
TypeInfo t2009_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_ConstructorInfo", "System.Runtime.InteropServices", t2009_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2009_TI, NULL, NULL, &t2009__CustomAttributeCache, &t2009_TI, &t2009_0_0_0, &t2009_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2011_TI;



// Metadata Definition System.Runtime.InteropServices._EnumBuilder
static MethodInfo* t2011_MIs[] =
{
	NULL
};
extern TypeInfo t1344_TI;
#include "t1344.h"
void t2011_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1344_TI)), &m7762_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("C7BD73DE-9F85-3290-88EE-090B8BDFE2DF"), &m2054_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2011__CustomAttributeCache = {
5,
NULL,
&t2011_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2011_0_0_0;
extern Il2CppType t2011_1_0_0;
struct t2011;
extern CustomAttributesCache t2011__CustomAttributeCache;
TypeInfo t2011_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_EnumBuilder", "System.Runtime.InteropServices", t2011_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2011_TI, NULL, NULL, &t2011__CustomAttributeCache, &t2011_TI, &t2011_0_0_0, &t2011_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.InteropServices._EventInfo
static MethodInfo* t2023_MIs[] =
{
	NULL
};
void t2023_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1143_TI)), &m7762_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("9DE59C64-D889-35A1-B897-587D74469E5B"), &m2054_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2023__CustomAttributeCache = {
5,
NULL,
&t2023_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2023_0_0_0;
extern Il2CppType t2023_1_0_0;
struct t2023;
extern CustomAttributesCache t2023__CustomAttributeCache;
TypeInfo t2023_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_EventInfo", "System.Runtime.InteropServices", t2023_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2023_TI, NULL, NULL, &t2023__CustomAttributeCache, &t2023_TI, &t2023_0_0_0, &t2023_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2012_TI;



// Metadata Definition System.Runtime.InteropServices._FieldBuilder
static MethodInfo* t2012_MIs[] =
{
	NULL
};
extern TypeInfo t1345_TI;
#include "t1345.h"
void t2012_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1345_TI)), &m7762_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("CE1A3BF5-975E-30CC-97C9-1EF70F8F3993"), &m2054_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2012__CustomAttributeCache = {
5,
NULL,
&t2012_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2012_0_0_0;
extern Il2CppType t2012_1_0_0;
struct t2012;
extern CustomAttributesCache t2012__CustomAttributeCache;
TypeInfo t2012_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_FieldBuilder", "System.Runtime.InteropServices", t2012_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2012_TI, NULL, NULL, &t2012__CustomAttributeCache, &t2012_TI, &t2012_0_0_0, &t2012_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.InteropServices._FieldInfo
static MethodInfo* t2013_MIs[] =
{
	NULL
};
void t2013_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1144_TI)), &m7762_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("8A7C1442-A9FB-366B-80D8-4939FFA6DBE0"), &m2054_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2013__CustomAttributeCache = {
5,
NULL,
&t2013_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2013_0_0_0;
extern Il2CppType t2013_1_0_0;
struct t2013;
extern CustomAttributesCache t2013__CustomAttributeCache;
TypeInfo t2013_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_FieldInfo", "System.Runtime.InteropServices", t2013_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2013_TI, NULL, NULL, &t2013__CustomAttributeCache, &t2013_TI, &t2013_0_0_0, &t2013_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.InteropServices._MethodBase
static MethodInfo* t2010_MIs[] =
{
	NULL
};
void t2010_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t636_TI)), &m7762_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("6240837A-707F-3181-8E98-A36AE086766B"), &m2054_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2010__CustomAttributeCache = {
5,
NULL,
&t2010_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2010_0_0_0;
extern Il2CppType t2010_1_0_0;
struct t2010;
extern CustomAttributesCache t2010__CustomAttributeCache;
TypeInfo t2010_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_MethodBase", "System.Runtime.InteropServices", t2010_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2010_TI, NULL, NULL, &t2010__CustomAttributeCache, &t2010_TI, &t2010_0_0_0, &t2010_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2014_TI;



// Metadata Definition System.Runtime.InteropServices._MethodBuilder
static MethodInfo* t2014_MIs[] =
{
	NULL
};
extern TypeInfo t1349_TI;
#include "t1349.h"
void t2014_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1349_TI)), &m7762_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("007D8A14-FDF3-363E-9A0B-FEC0618260A2"), &m2054_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2014__CustomAttributeCache = {
5,
NULL,
&t2014_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2014_0_0_0;
extern Il2CppType t2014_1_0_0;
struct t2014;
extern CustomAttributesCache t2014__CustomAttributeCache;
TypeInfo t2014_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_MethodBuilder", "System.Runtime.InteropServices", t2014_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2014_TI, NULL, NULL, &t2014__CustomAttributeCache, &t2014_TI, &t2014_0_0_0, &t2014_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.InteropServices._MethodInfo
static MethodInfo* t2015_MIs[] =
{
	NULL
};
void t2015_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t557_TI)), &m7762_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("FFCC1B5D-ECB8-38DD-9B01-3DC8ABC2AA5F"), &m2054_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2015__CustomAttributeCache = {
5,
NULL,
&t2015_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2015_0_0_0;
extern Il2CppType t2015_1_0_0;
struct t2015;
extern CustomAttributesCache t2015__CustomAttributeCache;
TypeInfo t2015_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_MethodInfo", "System.Runtime.InteropServices", t2015_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2015_TI, NULL, NULL, &t2015__CustomAttributeCache, &t2015_TI, &t2015_0_0_0, &t2015_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.InteropServices._Module
static MethodInfo* t2017_MIs[] =
{
	NULL
};
void t2017_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1142_TI)), &m7762_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("D002E9BA-D9E3-3749-B1D3-D565A08B13E7"), &m2054_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2017__CustomAttributeCache = {
5,
NULL,
&t2017_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2017_0_0_0;
extern Il2CppType t2017_1_0_0;
struct t2017;
extern CustomAttributesCache t2017__CustomAttributeCache;
TypeInfo t2017_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_Module", "System.Runtime.InteropServices", t2017_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2017_TI, NULL, NULL, &t2017__CustomAttributeCache, &t2017_TI, &t2017_0_0_0, &t2017_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2016_TI;



// Metadata Definition System.Runtime.InteropServices._ModuleBuilder
static MethodInfo* t2016_MIs[] =
{
	NULL
};
extern TypeInfo t1351_TI;
#include "t1351.h"
void t2016_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1351_TI)), &m7762_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("D05FFA9A-04AF-3519-8EE1-8D93AD73430B"), &m2054_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2016__CustomAttributeCache = {
5,
NULL,
&t2016_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2016_0_0_0;
extern Il2CppType t2016_1_0_0;
struct t2016;
extern CustomAttributesCache t2016__CustomAttributeCache;
TypeInfo t2016_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_ModuleBuilder", "System.Runtime.InteropServices", t2016_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2016_TI, NULL, NULL, &t2016__CustomAttributeCache, &t2016_TI, &t2016_0_0_0, &t2016_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2018_TI;



// Metadata Definition System.Runtime.InteropServices._ParameterBuilder
static MethodInfo* t2018_MIs[] =
{
	NULL
};
void t2018_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("36329EBA-F97A-3565-BC07-0ED5C6EF19FC"), &m2054_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1352_TI)), &m7762_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2018__CustomAttributeCache = {
5,
NULL,
&t2018_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2018_0_0_0;
extern Il2CppType t2018_1_0_0;
struct t2018;
extern CustomAttributesCache t2018__CustomAttributeCache;
TypeInfo t2018_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_ParameterBuilder", "System.Runtime.InteropServices", t2018_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2018_TI, NULL, NULL, &t2018__CustomAttributeCache, &t2018_TI, &t2018_0_0_0, &t2018_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.InteropServices._ParameterInfo
static MethodInfo* t2037_MIs[] =
{
	NULL
};
void t2037_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t637_TI)), &m7762_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("993634C4-E47A-32CC-BE08-85F567DC27D6"), &m2054_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2037__CustomAttributeCache = {
5,
NULL,
&t2037_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2037_0_0_0;
extern Il2CppType t2037_1_0_0;
struct t2037;
extern CustomAttributesCache t2037__CustomAttributeCache;
TypeInfo t2037_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_ParameterInfo", "System.Runtime.InteropServices", t2037_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2037_TI, NULL, NULL, &t2037__CustomAttributeCache, &t2037_TI, &t2037_0_0_0, &t2037_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Runtime.InteropServices._PropertyInfo
static MethodInfo* t2036_MIs[] =
{
	NULL
};
void t2036_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("F59ED4E4-E68F-3218-BD77-061AA82824BF"), &m2054_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1146_TI)), &m7762_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2036__CustomAttributeCache = {
5,
NULL,
&t2036_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2036_0_0_0;
extern Il2CppType t2036_1_0_0;
struct t2036;
extern CustomAttributesCache t2036__CustomAttributeCache;
TypeInfo t2036_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_PropertyInfo", "System.Runtime.InteropServices", t2036_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2036_TI, NULL, NULL, &t2036__CustomAttributeCache, &t2036_TI, &t2036_0_0_0, &t2036_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2039_TI;



// Metadata Definition System.Runtime.InteropServices._Thread
static MethodInfo* t2039_MIs[] =
{
	NULL
};
extern TypeInfo t1436_TI;
#include "t1436.h"
void t2039_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1436_TI)), &m7762_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("C281C7F1-4AA9-3517-961A-463CFED57E75"), &m2054_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2039__CustomAttributeCache = {
5,
NULL,
&t2039_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2039_0_0_0;
extern Il2CppType t2039_1_0_0;
struct t2039;
extern CustomAttributesCache t2039__CustomAttributeCache;
TypeInfo t2039_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_Thread", "System.Runtime.InteropServices", t2039_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2039_TI, NULL, NULL, &t2039__CustomAttributeCache, &t2039_TI, &t2039_0_0_0, &t2039_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2019_TI;



// Metadata Definition System.Runtime.InteropServices._TypeBuilder
static MethodInfo* t2019_MIs[] =
{
	NULL
};
extern TypeInfo t1340_TI;
#include "t1340.h"
void t2019_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, false, &m3062_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t1413 * tmp;
		tmp = (t1413 *)il2cpp_codegen_object_new (&t1413_TI);
		m7762(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t1340_TI)), &m7762_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t1409 * tmp;
		tmp = (t1409 *)il2cpp_codegen_object_new (&t1409_TI);
		m7746(tmp, 1, &m7746_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, true, &m2055_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t436 * tmp;
		tmp = (t436 *)il2cpp_codegen_object_new (&t436_TI);
		m2054(tmp, il2cpp_codegen_string_new_wrapper("7E5678EE-48B3-3F83-B076-C58543498A58"), &m2054_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t2019__CustomAttributeCache = {
5,
NULL,
&t2019_CustomAttributesCacheGenerator
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2019_0_0_0;
extern Il2CppType t2019_1_0_0;
struct t2019;
extern CustomAttributesCache t2019__CustomAttributeCache;
TypeInfo t2019_TI = 
{
	&g_mscorlib_dll_Image, NULL, "_TypeBuilder", "System.Runtime.InteropServices", t2019_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2019_TI, NULL, NULL, &t2019__CustomAttributeCache, &t2019_TI, &t2019_0_0_0, &t2019_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#include "t1415.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1415_TI;
#include "t1415MD.h"

#include "t1419.h"
#include "t1472.h"
#include "t1481.h"
#include "t1422.h"
#include "t1417.h"
#include "t731.h"
#include "t1425.h"
#include "t1418.h"
#include "t1420.h"
#include "t1427.h"
extern TypeInfo t1419_TI;
extern TypeInfo t2040_TI;
extern TypeInfo t1481_TI;
extern TypeInfo t1422_TI;
extern TypeInfo t1480_TI;
extern TypeInfo t1482_TI;
extern TypeInfo t1417_TI;
extern TypeInfo t1418_TI;
extern TypeInfo t1416_TI;
extern TypeInfo t1420_TI;
extern TypeInfo t731_TI;
extern TypeInfo t674_TI;
extern TypeInfo t1427_TI;
extern TypeInfo t136_TI;
extern TypeInfo t1425_TI;
extern TypeInfo t1424_TI;
#include "t1419MD.h"
#include "t1481MD.h"
#include "t1422MD.h"
#include "t966MD.h"
#include "t1480MD.h"
#include "t1482MD.h"
#include "t1417MD.h"
#include "t1418MD.h"
#include "t1420MD.h"
#include "t731MD.h"
#include "t1427MD.h"
#include "t1436MD.h"
extern MethodInfo m7769_MI;
extern MethodInfo m7999_MI;
extern MethodInfo m7771_MI;
extern MethodInfo m4292_MI;
extern MethodInfo m7997_MI;
extern MethodInfo m8011_MI;
extern MethodInfo m5996_MI;
extern MethodInfo m8012_MI;
extern MethodInfo m7832_MI;
extern MethodInfo m7764_MI;
extern MethodInfo m7768_MI;
extern MethodInfo m7839_MI;
extern MethodInfo m7835_MI;
extern MethodInfo m7770_MI;
extern MethodInfo m3980_MI;
extern MethodInfo m4124_MI;
extern MethodInfo m8789_MI;
extern MethodInfo m3981_MI;
extern MethodInfo m3970_MI;
extern MethodInfo m10180_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m9856_MI;
extern MethodInfo m3991_MI;
extern MethodInfo m6654_MI;
extern MethodInfo m7841_MI;
extern MethodInfo m10181_MI;
extern MethodInfo m1740_MI;


 t29 * m7764 (t29 * __this, MethodInfo* method){
	{
		if ((((t1415_SFs*)InitializedTypeInfo(&t1415_TI)->static_fields)->f0))
		{
			goto IL_0011;
		}
	}
	{
		t1419 * L_0 = (t1419 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1419_TI));
		m7769(L_0, &m7769_MI);
		((t1415_SFs*)InitializedTypeInfo(&t1415_TI)->static_fields)->f0 = L_0;
	}

IL_0011:
	{
		return (((t1415_SFs*)InitializedTypeInfo(&t1415_TI)->static_fields)->f0);
	}
}
extern MethodInfo m7765_MI;
 t29 * m7765 (t29 * __this, t42 * p0, t316* p1, MethodInfo* method){
	t7* V_0 = {0};
	t29 * V_1 = {0};
	t316* V_2 = {0};
	int32_t V_3 = 0;
	t1472 * V_4 = {0};
	{
		V_0 = (t7*)NULL;
		V_2 = p1;
		V_3 = 0;
		goto IL_0037;
	}

IL_0008:
	{
		int32_t L_0 = V_3;
		V_1 = (*(t29 **)(t29 **)SZArrayLdElema(V_2, L_0));
		if (((t29 *)IsInst(V_1, InitializedTypeInfo(&t2040_TI))))
		{
			goto IL_001f;
		}
	}
	{
		t1481 * L_1 = (t1481 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1481_TI));
		m7999(L_1, (t7*) &_stringLiteral1576, &m7999_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001f:
	{
		if (!((t1422 *)IsInst(V_1, InitializedTypeInfo(&t1422_TI))))
		{
			goto IL_0033;
		}
	}
	{
		t7* L_2 = m7771(((t1422 *)Castclass(V_1, InitializedTypeInfo(&t1422_TI))), &m7771_MI);
		V_0 = L_2;
	}

IL_0033:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_0037:
	{
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)V_2)->max_length))))))
		{
			goto IL_0008;
		}
	}
	{
		if (!V_0)
		{
			goto IL_0049;
		}
	}
	{
		t29 * L_3 = m4292(NULL, p0, V_0, p1, &m4292_MI);
		return L_3;
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1480_TI));
		t1472 * L_4 = m7997(NULL, p0, &m7997_MI);
		V_4 = L_4;
		if (!V_4)
		{
			goto IL_005e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t29 * L_5 = m8011(NULL, V_4, p1, &m8011_MI);
		return L_5;
	}

IL_005e:
	{
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5996_MI, p0);
		if (!L_6)
		{
			goto IL_006e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1482_TI));
		t29 * L_7 = m8012(NULL, p0, p1, &m8012_MI);
		return L_7;
	}

IL_006e:
	{
		return NULL;
	}
}
extern MethodInfo m7766_MI;
 t1417 * m7766 (t29 * __this, t42 * p0, t7* p1, t316* p2, MethodInfo* method){
	t1417 * V_0 = {0};
	t29 * V_1 = {0};
	t731 * V_2 = {0};
	bool V_3 = false;
	t1425 * V_4 = {0};
	t29 * V_5 = {0};
	t29 * V_6 = {0};
	t316* V_7 = {0};
	t29 * V_8 = {0};
	t316* V_9 = {0};
	int32_t V_10 = 0;
	t29 * V_11 = {0};
	t29 * V_12 = {0};
	t29 * V_13 = {0};
	t29 * V_14 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t G_B19_0 = 0;
	{
		t1417 * L_0 = (t1417 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1417_TI));
		m7832(L_0, p0, &m7832_MI);
		V_0 = L_0;
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m5996_MI, p0);
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		t29 * L_2 = m7764(NULL, &m7764_MI);
		t1418 * L_3 = (t1418 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1418_TI));
		m7768(L_3, p1, L_2, &m7768_MI);
		VirtActionInvoker1< t29 * >::Invoke(&m7839_MI, V_0, L_3);
		m7835(V_0, 0, &m7835_MI);
		return V_0;
	}

IL_0029:
	{
		t29 * L_4 = m7764(NULL, &m7764_MI);
		V_1 = L_4;
		t1420 * L_5 = (t1420 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1420_TI));
		m7770(L_5, V_1, &m7770_MI);
		V_1 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t731_TI));
		t731 * L_6 = (t731 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t731_TI));
		m3980(L_6, &m3980_MI);
		V_2 = L_6;
		if (!p2)
		{
			goto IL_0046;
		}
	}
	{
		VirtActionInvoker1< t29 * >::Invoke(&m4124_MI, V_2, (t29 *)(t29 *)p2);
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_7 = m1713(NULL, p1, (((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f3), &m1713_MI);
		V_3 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1436_TI));
		t1425 * L_8 = m8789(NULL, &m8789_MI);
		V_4 = L_8;
		if (!V_3)
		{
			goto IL_00a5;
		}
	}
	{
		t29 * L_9 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m3981_MI, V_2);
		V_6 = L_9;
	}

IL_0064:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0084;
		}

IL_0066:
		{
			t29 * L_10 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_6);
			V_5 = ((t29 *)Castclass(L_10, InitializedTypeInfo(&t2040_TI)));
			bool L_11 = (bool)InterfaceFuncInvoker2< bool, t1425 *, t29 * >::Invoke(&m10180_MI, V_5, V_4, V_0);
			if (L_11)
			{
				goto IL_0084;
			}
		}

IL_0080:
		{
			V_3 = 0;
			goto IL_008d;
		}

IL_0084:
		{
			bool L_12 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_6);
			if (L_12)
			{
				goto IL_0066;
			}
		}

IL_008d:
		{
			// IL_008d: leave.s IL_00a5
			leaveInstructions[0] = 0xA5; // 1
			THROW_SENTINEL(IL_00a5);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_008f;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_008f;
	}

IL_008f:
	{ // begin finally (depth: 1)
		{
			V_13 = ((t29 *)IsInst(V_6, InitializedTypeInfo(&t324_TI)));
			if (V_13)
			{
				goto IL_009d;
			}
		}

IL_009c:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0xA5:
					goto IL_00a5;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_009d:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_13);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0xA5:
					goto IL_00a5;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_00a5:
	{
		t316* L_13 = (t316*)VirtFuncInvoker1< t316*, bool >::Invoke(&m9856_MI, p0, 1);
		V_7 = L_13;
		V_9 = V_7;
		V_10 = 0;
		goto IL_00ec;
	}

IL_00b7:
	{
		int32_t L_14 = V_10;
		V_8 = (*(t29 **)(t29 **)SZArrayLdElema(V_9, L_14));
		if (!((t29 *)IsInst(V_8, InitializedTypeInfo(&t2040_TI))))
		{
			goto IL_00e6;
		}
	}
	{
		if (!V_3)
		{
			goto IL_00db;
		}
	}
	{
		bool L_15 = (bool)InterfaceFuncInvoker2< bool, t1425 *, t29 * >::Invoke(&m10180_MI, ((t29 *)Castclass(V_8, InitializedTypeInfo(&t2040_TI))), V_4, V_0);
		G_B19_0 = ((int32_t)(L_15));
		goto IL_00dc;
	}

IL_00db:
	{
		G_B19_0 = 0;
	}

IL_00dc:
	{
		V_3 = G_B19_0;
		VirtFuncInvoker1< int32_t, t29 * >::Invoke(&m3991_MI, V_2, V_8);
	}

IL_00e6:
	{
		V_10 = ((int32_t)(V_10+1));
	}

IL_00ec:
	{
		if ((((int32_t)V_10) < ((int32_t)(((int32_t)(((t20 *)V_9)->max_length))))))
		{
			goto IL_00b7;
		}
	}
	{
		if (V_3)
		{
			goto IL_0144;
		}
	}
	{
		t316* L_16 = (t316*)VirtFuncInvoker0< t316* >::Invoke(&m6654_MI, V_2);
		m7841(V_0, L_16, &m7841_MI);
		t29 * L_17 = (t29 *)VirtFuncInvoker0< t29 * >::Invoke(&m3981_MI, V_2);
		V_12 = L_17;
	}

IL_010b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0123;
		}

IL_010d:
		{
			t29 * L_18 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m3970_MI, V_12);
			V_11 = ((t29 *)Castclass(L_18, InitializedTypeInfo(&t2040_TI)));
			InterfaceActionInvoker1< t29 * >::Invoke(&m10181_MI, V_11, V_0);
		}

IL_0123:
		{
			bool L_19 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_12);
			if (L_19)
			{
				goto IL_010d;
			}
		}

IL_012c:
		{
			// IL_012c: leave.s IL_0144
			leaveInstructions[0] = 0x144; // 1
			THROW_SENTINEL(IL_0144);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_012e;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_012e;
	}

IL_012e:
	{ // begin finally (depth: 1)
		{
			V_14 = ((t29 *)IsInst(V_12, InitializedTypeInfo(&t324_TI)));
			if (V_14)
			{
				goto IL_013c;
			}
		}

IL_013b:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x144:
					goto IL_0144;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_013c:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_14);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x144:
					goto IL_0144;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0144:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t1427_TI));
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_20 = m1740(NULL, p1, (((t1427_SFs*)InitializedTypeInfo(&t1427_TI)->static_fields)->f3), &m1740_MI);
		if (!L_20)
		{
			goto IL_0159;
		}
	}
	{
		t1418 * L_21 = (t1418 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1418_TI));
		m7768(L_21, p1, V_1, &m7768_MI);
		V_1 = L_21;
	}

IL_0159:
	{
		VirtActionInvoker1< t29 * >::Invoke(&m7839_MI, V_0, V_1);
		m7835(V_0, V_3, &m7835_MI);
		return V_0;
	}
}
extern MethodInfo m7767_MI;
 t29 * m7767 (t29 * __this, t42 * p0, MethodInfo* method){
	typedef t29 * (*m7767_ftn) (t42 *);
	static m7767_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (m7767_ftn)il2cpp_codegen_resolve_icall ("System.Runtime.Remoting.Activation.ActivationServices::AllocateUninitializedClassInstance(System.Type)");
	return _il2cpp_icall_func(p0);
}
// Metadata Definition System.Runtime.Remoting.Activation.ActivationServices
extern Il2CppType t1416_0_0_17;
FieldInfo t1415_f0_FieldInfo = 
{
	"_constructionActivator", &t1416_0_0_17, &t1415_TI, offsetof(t1415_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t1415_FIs[] =
{
	&t1415_f0_FieldInfo,
	NULL
};
static PropertyInfo t1415____ConstructionActivator_PropertyInfo = 
{
	&t1415_TI, "ConstructionActivator", &m7764_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t1415_PIs[] =
{
	&t1415____ConstructionActivator_PropertyInfo,
	NULL
};
extern Il2CppType t1416_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7764_MI = 
{
	"get_ConstructionActivator", (methodPointerType)&m7764, &t1415_TI, &t1416_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2193, 0, 255, 0, false, false, 3320, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t1415_m7765_ParameterInfos[] = 
{
	{"type", 0, 134221830, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"activationAttributes", 1, 134221831, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7765_MI = 
{
	"CreateProxyFromAttributes", (methodPointerType)&m7765, &t1415_TI, &t29_0_0_0, RuntimeInvoker_t29_t29_t29, t1415_m7765_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 3321, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t1415_m7766_ParameterInfos[] = 
{
	{"type", 0, 134221832, &EmptyCustomAttributesCache, &t42_0_0_0},
	{"activationUrl", 1, 134221833, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"activationAttributes", 2, 134221834, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t1417_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7766_MI = 
{
	"CreateConstructionCall", (methodPointerType)&m7766, &t1415_TI, &t1417_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t1415_m7766_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 3322, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t42_0_0_0;
static ParameterInfo t1415_m7767_ParameterInfos[] = 
{
	{"type", 0, 134221835, &EmptyCustomAttributesCache, &t42_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7767_MI = 
{
	"AllocateUninitializedClassInstance", (methodPointerType)&m7767, &t1415_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t1415_m7767_ParameterInfos, &EmptyCustomAttributesCache, 150, 4096, 255, 1, false, false, 3323, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1415_MIs[] =
{
	&m7764_MI,
	&m7765_MI,
	&m7766_MI,
	&m7767_MI,
	NULL
};
static MethodInfo* t1415_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1415_0_0_0;
extern Il2CppType t1415_1_0_0;
struct t1415;
TypeInfo t1415_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ActivationServices", "System.Runtime.Remoting.Activation", t1415_MIs, t1415_PIs, t1415_FIs, NULL, &t29_TI, NULL, NULL, &t1415_TI, NULL, t1415_VT, &EmptyCustomAttributesCache, &t1415_TI, &t1415_0_0_0, &t1415_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1415), 0, -1, sizeof(t1415_SFs), 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 1, 1, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m7768 (t1418 * __this, t7* p0, t29 * p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f0 = p0;
		__this->f1 = p1;
		return;
	}
}
// Metadata Definition System.Runtime.Remoting.Activation.AppDomainLevelActivator
extern Il2CppType t7_0_0_1;
FieldInfo t1418_f0_FieldInfo = 
{
	"_activationUrl", &t7_0_0_1, &t1418_TI, offsetof(t1418, f0), &EmptyCustomAttributesCache};
extern Il2CppType t1416_0_0_1;
FieldInfo t1418_f1_FieldInfo = 
{
	"_next", &t1416_0_0_1, &t1418_TI, offsetof(t1418, f1), &EmptyCustomAttributesCache};
static FieldInfo* t1418_FIs[] =
{
	&t1418_f0_FieldInfo,
	&t1418_f1_FieldInfo,
	NULL
};
extern Il2CppType t7_0_0_0;
extern Il2CppType t1416_0_0_0;
extern Il2CppType t1416_0_0_0;
static ParameterInfo t1418_m7768_ParameterInfos[] = 
{
	{"activationUrl", 0, 134221836, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"next", 1, 134221837, &EmptyCustomAttributesCache, &t1416_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m7768_MI = 
{
	".ctor", (methodPointerType)&m7768, &t1418_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t1418_m7768_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 3324, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1418_MIs[] =
{
	&m7768_MI,
	NULL
};
static MethodInfo* t1418_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
static TypeInfo* t1418_ITIs[] = 
{
	&t1416_TI,
};
static Il2CppInterfaceOffsetPair t1418_IOs[] = 
{
	{ &t1416_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1418_0_0_0;
extern Il2CppType t1418_1_0_0;
struct t1418;
TypeInfo t1418_TI = 
{
	&g_mscorlib_dll_Image, NULL, "AppDomainLevelActivator", "System.Runtime.Remoting.Activation", t1418_MIs, NULL, t1418_FIs, NULL, &t29_TI, NULL, NULL, &t1418_TI, t1418_ITIs, t1418_VT, &EmptyCustomAttributesCache, &t1418_TI, &t1418_0_0_0, &t1418_1_0_0, t1418_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1418), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 2, 0, 0, 4, 1, 1};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
