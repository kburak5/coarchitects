﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1660;
struct t42;
struct t929;
struct t7;
struct t1142;
struct t660;
struct t631;
struct t537;
struct t634;
struct t1147;
struct t1143;
struct t1144;
struct t1145;
struct t557;
struct t1366;
struct t1146;
struct t29;
struct t316;
struct t633;
struct t446;
struct t733;
struct t636;
#include "t1149.h"
#include "t43.h"
#include "t1148.h"
#include "t630.h"
#include "t1150.h"
#include "t735.h"

 int32_t m9327 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t660 * m9328 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9329 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t660 * m9330 (t1660 * __this, int32_t p0, t631 * p1, int32_t p2, t537* p3, t634* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1147* m9331 (t1660 * __this, int32_t p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1147* m9332 (t1660 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1143 * m9333 (t1660 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1143 * m9334 (t1660 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1144 * m9335 (t1660 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m9336 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1145* m9337 (t1660 * __this, t7* p0, int32_t p1, bool p2, t42 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1145* m9338 (t1660 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m9339 (t1660 * __this, t7* p0, int32_t p1, t631 * p2, int32_t p3, t537* p4, t634* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1366* m9340 (t1660 * __this, t7* p0, int32_t p1, bool p2, t42 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1146 * m9341 (t1660 * __this, t7* p0, int32_t p1, t631 * p2, t42 * p3, t537* p4, t634* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9342 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9343 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9344 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9345 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9346 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9347 (t1660 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9348 (t1660 * __this, t7* p0, int32_t p1, t631 * p2, t29 * p3, t316* p4, t634* p5, t633 * p6, t446* p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m9349 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m9350 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t929 * m9351 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9352 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9353 (t1660 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m9354 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9355 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9356 (t1660 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m9357 (t1660 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m9358 (t1660 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9359 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9360 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9361 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1142 * m9362 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m9363 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m9364 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t43  m9365 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9366 (t1660 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9367 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m9368 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9369 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9370 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m9371 (t1660 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m9372 (t1660 * __this, t636 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9373 (t1660 * __this, t316** p0, t636 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
