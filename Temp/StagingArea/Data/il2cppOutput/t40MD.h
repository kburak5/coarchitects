﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t40;
struct t29;
struct t42;
struct t1094;
struct t7;
#include "t465.h"
#include "t1126.h"
#include "t1127.h"

 void m5782 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5783 (bool* __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5784 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5785 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5786 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5787 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5788 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5789 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5790 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5791 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5792 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5793 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5794 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5795 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5796 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5797 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5798 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5799 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5800 (bool* __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5801 (bool* __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5802 (bool* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5803 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5804 (bool* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5805 (bool* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5806 (bool* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
