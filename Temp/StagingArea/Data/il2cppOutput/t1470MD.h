﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1470;
struct t7;
struct t42;
struct t1469;
struct t316;

 void m7955 (t1470 * __this, t42 * p0, t1469 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7956 (t1470 * __this, t42 * p0, t7* p1, t316* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7957 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7958 (t1470 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7959 (t1470 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
