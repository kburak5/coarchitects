﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1741;
struct t29;
struct t7;
struct t3276;
struct t20;
struct t136;
struct t3277;
struct t3278;
struct t3279;
struct t446;
struct t3280;
struct t3281;
#include "t3282.h"

#include "t294MD.h"
#define m9676(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m18176(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m18177(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m18178(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m18179(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m18180(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m18181(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m18182(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m18183(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m18184(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m18185(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m18186(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m18187(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m18188(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m18189(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m18190(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m18191(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m18192(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m9678(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m18193(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m18194(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m18195(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m18196(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m18197(__this, method) (t3279 *)m10583_gshared((t294 *)__this, method)
#define m18198(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m18199(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m18200(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m18201(__this, p0, method) (t7*)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m18202(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m18203(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t3282  m18204 (t1741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m18205(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m18206(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m18207(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m18208(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m18209(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m18210(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m18211(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m18212(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m18213(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m18214(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m18215(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m9679(__this, method) (t446*)m10619_gshared((t294 *)__this, method)
#define m18216(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m18217(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m18218(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m9677(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m18219(__this, p0, method) (t7*)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m18220(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
