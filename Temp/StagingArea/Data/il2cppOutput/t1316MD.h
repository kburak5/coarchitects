﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1316;
struct t29;
struct t781;
struct t66;
struct t67;
#include "t35.h"

 void m7026 (t1316 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7027 (t1316 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7028 (t1316 * __this, t781* p0, int32_t p1, int32_t p2, t67 * p3, t29 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7029 (t1316 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
