﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2608;
struct t29;
struct t20;
#include "t186.h"

 void m14049 (t2608 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14050 (t2608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14051 (t2608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14052 (t2608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14053 (t2608 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
