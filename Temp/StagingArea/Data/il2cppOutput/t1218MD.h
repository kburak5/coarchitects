﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1218;
struct t7;
struct t1219;

 void m6413 (t1218 * __this, t7* p0, t1219 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6414 (t1218 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1219 * m6415 (t1218 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
