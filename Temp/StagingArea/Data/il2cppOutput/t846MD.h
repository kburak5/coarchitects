﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t846;
struct t7;
struct t29;
#include "t842.h"

 void m3575 (t846 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3576 (t846 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3577 (t846 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3578 (t846 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
