﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1227;
struct t29;
struct t1225;
struct t1222;

 void m6464 (t1227 * __this, t1222 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6465 (t1227 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6466 (t1227 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1225 * m6467 (t1227 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6468 (t1227 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
