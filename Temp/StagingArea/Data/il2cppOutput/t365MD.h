﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t365;
struct t29;
struct t145;
struct t2442;
struct t2440;
struct t733;
struct t2443;
struct t20;
struct t136;
struct t2445;
struct t722;
#include "t735.h"
#include "t2444.h"
#include "t2446.h"
#include "t725.h"

 void m12731 (t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12732 (t365 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12733 (t365 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12734 (t365 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12735 (t365 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12736 (t365 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12737 (t365 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12738 (t365 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12739 (t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12740 (t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12741 (t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12742 (t365 * __this, t2444  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12743 (t365 * __this, t2444  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12744 (t365 * __this, t2443* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12745 (t365 * __this, t2444  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12746 (t365 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12747 (t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m12748 (t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12749 (t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12750 (t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12751 (t365 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12752 (t365 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12753 (t365 * __this, int32_t p0, t29* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12754 (t365 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12755 (t365 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2444  m12756 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12757 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12758 (t365 * __this, t2443* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12759 (t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12760 (t365 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12761 (t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12762 (t365 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12763 (t365 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12764 (t365 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12765 (t365 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12766 (t365 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12767 (t365 * __this, t29 * p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2442 * m12768 (t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12769 (t365 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12770 (t365 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12771 (t365 * __this, t2444  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2446  m12772 (t365 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m12773 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
