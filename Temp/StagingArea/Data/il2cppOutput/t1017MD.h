﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1017;
struct t7;
#include "t1015.h"
#include "t1016.h"

 void m4606 (t1017 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4607 (t1017 * __this, uint8_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m4608 (t1017 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m4609 (t1017 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4610 (t1017 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4611 (t1017 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4612 (t1017 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4613 (t29 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
