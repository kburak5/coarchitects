﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3250;
struct t29;
struct t20;
#include "t1018.h"

 void m18046 (t3250 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18047 (t3250 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18048 (t3250 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18049 (t3250 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18050 (t3250 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
