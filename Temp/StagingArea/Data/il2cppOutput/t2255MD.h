﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2255;
struct t29;
struct t2259;
struct t20;
struct t136;
struct t2251;
struct t2252;
#include "t57.h"

 void m11314 (t2255 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11315 (t2255 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11316 (t2255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11317 (t2255 * __this, int32_t p0, t57  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11318 (t2255 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11319 (t2255 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t57  m11320 (t2255 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11321 (t2255 * __this, int32_t p0, t57  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11322 (t2255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11323 (t2255 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11324 (t2255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11325 (t2255 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11326 (t2255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11327 (t2255 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11328 (t2255 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11329 (t2255 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11330 (t2255 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11331 (t2255 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11332 (t2255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11333 (t2255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11334 (t2255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11335 (t2255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11336 (t2255 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11337 (t2255 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11338 (t2255 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11339 (t2255 * __this, t2251* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m11340 (t2255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11341 (t2255 * __this, t57  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11342 (t2255 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t57  m11343 (t2255 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
