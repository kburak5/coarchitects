﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3128;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m17271_gshared (t3128 * __this, t29 * p0, t35 p1, MethodInfo* method);
#define m17271(__this, p0, p1, method) (void)m17271_gshared((t3128 *)__this, (t29 *)p0, (t35)p1, method)
 void m17272_gshared (t3128 * __this, t29 * p0, t29 * p1, t29 * p2, t29 * p3, MethodInfo* method);
#define m17272(__this, p0, p1, p2, p3, method) (void)m17272_gshared((t3128 *)__this, (t29 *)p0, (t29 *)p1, (t29 *)p2, (t29 *)p3, method)
 t29 * m17273_gshared (t3128 * __this, t29 * p0, t29 * p1, t29 * p2, t29 * p3, t67 * p4, t29 * p5, MethodInfo* method);
#define m17273(__this, p0, p1, p2, p3, p4, p5, method) (t29 *)m17273_gshared((t3128 *)__this, (t29 *)p0, (t29 *)p1, (t29 *)p2, (t29 *)p3, (t67 *)p4, (t29 *)p5, method)
 void m17274_gshared (t3128 * __this, t29 * p0, MethodInfo* method);
#define m17274(__this, p0, method) (void)m17274_gshared((t3128 *)__this, (t29 *)p0, method)
