﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t942;
struct t793;
struct t7;
#include "t465.h"

 t793 * m4075 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t793 * m4376 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4071 (t29 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4179 (t29 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m4377 (t29 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
