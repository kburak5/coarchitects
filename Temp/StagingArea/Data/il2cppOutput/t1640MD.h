﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1640;
struct t29;

 void m9226 (t1640 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9227 (t1640 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9228 (t1640 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
