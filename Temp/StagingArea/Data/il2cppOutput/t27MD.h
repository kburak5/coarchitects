﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t27;

 void m2420 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1676 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1675 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1961 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1401 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1949 (t29 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1765 (t29 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1889 (t29 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1763 (t29 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1848 (t29 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1948 (t29 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1800 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m64 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1960 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1922 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1656 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1854 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m43 (t29 * __this, float p0, float p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1540 (t29 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1643 (t29 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1674 (t29 * __this, float p0, float p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1442 (t29 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1847 (t29 * __this, float p0, float p1, float* p2, float p3, float p4, float p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1689 (t29 * __this, float p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1688 (t29 * __this, float p0, float p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
