﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1462;
struct t7;
struct t316;
struct t1449;
struct t636;
struct t29;
struct t721;
struct t295;
struct t1463;

 void m7928 (t1462 * __this, t29 * p0, t316* p1, int32_t p2, t1449 * p3, t29 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7929 (t1462 * __this, t295 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7930 (t1462 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7931 (t1462 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1449 * m7932 (t1462 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m7933 (t1462 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7934 (t1462 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7935 (t1462 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7936 (t1462 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7937 (t1462 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7938 (t1462 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7939 (t1462 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7940 (t1462 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7941 (t1462 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7942 (t1462 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
