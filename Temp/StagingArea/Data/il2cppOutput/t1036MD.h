﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1036;
struct t781;
#include "t1064.h"

 void m4986 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4987 (t1036 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4988 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4989 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4990 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4991 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m4992 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4993 (t1036 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m4994 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4995 (t1036 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m4996 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m4997 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4998 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4999 (t1036 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5000 (t1036 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5001 (t1036 * __this, int16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5002 (t1036 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5003 (t1036 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5004 (t1036 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5005 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m5006 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5007 (t1036 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5008 (t1036 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5009 (t1036 * __this, int64_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5010 (t1036 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5011 (t1036 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
