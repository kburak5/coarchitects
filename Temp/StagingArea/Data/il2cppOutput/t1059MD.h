﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1059;
struct t29;
struct t1057;
struct t945;
struct t66;
struct t67;
#include "t35.h"

 void m5088 (t1059 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1057 * m5089 (t1059 * __this, t945 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5090 (t1059 * __this, t945 * p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1057 * m5091 (t1059 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
