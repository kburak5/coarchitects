﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t25;
struct t136;
#include "t23.h"
#include "t362.h"
#include "t396.h"

 void m2562 (t25 * __this, t23 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2563 (t25 * __this, t23 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m40 (t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m69 (t25 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2564 (t25 * __this, t23 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2565 (t25 * __this, t23 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m42 (t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m44 (t25 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1623 (t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2566 (t25 * __this, t362 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t362  m1619 (t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2567 (t25 * __this, t362 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2568 (t25 * __this, t362 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t362  m1789 (t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1795 (t25 * __this, t362  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2569 (t25 * __this, t23 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2570 (t25 * __this, t23 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1791 (t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m50 (t25 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m1365 (t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1819 (t25 * __this, t25 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m2571 (t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2572 (t25 * __this, t25 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1779 (t25 * __this, t25 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2573 (t25 * __this, t25 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2574 (t25 * __this, t396 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m1859 (t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1885 (t25 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2575 (t29 * __this, t25 * p0, t23 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1744 (t25 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2576 (t29 * __this, t25 * p0, t23 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1993 (t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1780 (t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2577 (t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m1991 (t25 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
