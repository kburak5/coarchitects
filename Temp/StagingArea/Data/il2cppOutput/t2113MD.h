﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2113;
struct t29;
struct t317;
struct t20;

#include "t2111MD.h"
#define m10302(__this, p0, method) (void)m10288_gshared((t2111 *)__this, (t20 *)p0, method)
#define m10303(__this, method) (t29 *)m10290_gshared((t2111 *)__this, method)
#define m10304(__this, method) (void)m10292_gshared((t2111 *)__this, method)
#define m10305(__this, method) (bool)m10294_gshared((t2111 *)__this, method)
#define m10306(__this, method) (t317 *)m10296_gshared((t2111 *)__this, method)
