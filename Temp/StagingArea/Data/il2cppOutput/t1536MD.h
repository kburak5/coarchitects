﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1536;
struct t781;

 void m8270 (t1536 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8271 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8272 (t1536 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8273 (t1536 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8274 (t1536 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8275 (t1536 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8276 (t1536 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8277 (t1536 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8278 (t1536 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8279 (t1536 * __this, uint64_t p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
