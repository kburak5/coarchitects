﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2632;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t17.h"

 void m14155 (t2632 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14156 (t2632 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14157 (t2632 * __this, t17  p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14158 (t2632 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
