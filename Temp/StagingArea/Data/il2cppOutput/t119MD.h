﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t119;
struct t119_marshaled;
#include "t17.h"
#include "t319.h"

 int32_t m1413 (t119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1415 (t119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1414 (t119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t119_marshal(const t119& unmarshaled, t119_marshaled& marshaled);
void t119_marshal_back(const t119_marshaled& marshaled, t119& unmarshaled);
void t119_marshal_cleanup(t119_marshaled& marshaled);
