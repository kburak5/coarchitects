﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1122;
struct t7;
struct t295;
struct t733;
#include "t735.h"

 void m7076 (t1122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7077 (t1122 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5246 (t1122 * __this, t7* p0, t295 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7078 (t1122 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7079 (t1122 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
