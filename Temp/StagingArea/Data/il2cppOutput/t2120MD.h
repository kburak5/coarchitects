﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2120;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m10325_gshared (t2120 * __this, t29 * p0, t35 p1, MethodInfo* method);
#define m10325(__this, p0, p1, method) (void)m10325_gshared((t2120 *)__this, (t29 *)p0, (t35)p1, method)
 void m10326_gshared (t2120 * __this, t29 * p0, MethodInfo* method);
#define m10326(__this, p0, method) (void)m10326_gshared((t2120 *)__this, (t29 *)p0, method)
 t29 * m10327_gshared (t2120 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method);
#define m10327(__this, p0, p1, p2, method) (t29 *)m10327_gshared((t2120 *)__this, (t29 *)p0, (t67 *)p1, (t29 *)p2, method)
 void m10328_gshared (t2120 * __this, t29 * p0, MethodInfo* method);
#define m10328(__this, p0, method) (void)m10328_gshared((t2120 *)__this, (t29 *)p0, method)
