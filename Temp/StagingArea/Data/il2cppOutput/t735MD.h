﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t735;
struct t29;
#include "t1523.h"

 void m8157 (t735 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8158 (t735 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8159 (t735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8160 (t735 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8161 (t735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
