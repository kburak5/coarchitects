﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3323;
struct t29;
struct t20;
#include "t1197.h"

 void m18497 (t3323 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18498 (t3323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18499 (t3323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18500 (t3323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18501 (t3323 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
