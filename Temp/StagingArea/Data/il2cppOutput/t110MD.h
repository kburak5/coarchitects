﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t110;
struct t29;
struct t316;
struct t7;

 void m5285 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5286 (t29 * __this, t29 * p0, t29 * p1, t316** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5287 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1388 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5288 (t29 * __this, t29 * p0, t316** p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1389 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1500 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
