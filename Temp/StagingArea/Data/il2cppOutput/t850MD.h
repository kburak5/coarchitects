﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t850;
struct t7;
#include "t849.h"
#include "t851.h"

 uint16_t m3587 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3588 (t29 * __this, uint16_t p0, uint16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3589 (t29 * __this, int32_t p0, uint16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
