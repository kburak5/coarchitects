﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1131;
struct t733;
struct t29;
struct t7;
#include "t21.h"
#include "t735.h"
#include "t1131.h"

 void m5821 (t1131 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5822 (t1131 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5823 (t1131 * __this, void* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5824 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5825 (t1131 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5826 (t1131 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5827 (t1131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5828 (t1131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5829 (t1131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void* m5830 (t1131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5831 (t1131 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5832 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5833 (t29 * __this, t1131  p0, t1131  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5834 (t29 * __this, t1131  p0, t1131  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5835 (t29 * __this, t1131  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5836 (t29 * __this, t1131  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1131  m5837 (t29 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1131  m5838 (t29 * __this, void* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void* m5839 (t29 * __this, t1131  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1131  m5840 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
