﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2760;
struct t29;
struct t20;
#include "t257.h"

 void m15109 (t2760 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15110 (t2760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15111 (t2760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15112 (t2760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15113 (t2760 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
