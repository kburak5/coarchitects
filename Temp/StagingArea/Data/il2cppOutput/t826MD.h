﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t826;
struct t29;
struct t813;

 void m3498 (t826 * __this, t813 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3499 (t826 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3500 (t826 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
