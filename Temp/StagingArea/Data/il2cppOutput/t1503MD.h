﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1503;
struct t29;
struct t1483;
struct t1304;
struct t1451;
struct t733;
struct t1500;
struct t296;
struct t841;
struct t20;
struct t42;
struct t7;
#include "t1490.h"
#include "t1491.h"

 void m8052 (t1503 * __this, t1483 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8053 (t1503 * __this, t1304 * p0, bool p1, t29 ** p2, t1451** p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8054 (t1503 * __this, uint8_t p0, t1304 * p1, bool p2, t29 ** p3, t1451** p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8055 (t1503 * __this, uint8_t p0, t1304 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8056 (t1503 * __this, t1304 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8057 (t1503 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8058 (t1503 * __this, uint8_t p0, t1304 * p1, int64_t* p2, t29 ** p3, t733 ** p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8059 (t1503 * __this, t1304 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8060 (t1503 * __this, t1304 * p0, bool p1, bool p2, int64_t* p3, t29 ** p4, t733 ** p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8061 (t1503 * __this, t1304 * p0, int64_t* p1, t29 ** p2, t733 ** p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8062 (t1503 * __this, t1304 * p0, t1500 * p1, int64_t p2, t29 ** p3, t733 ** p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8063 (t1503 * __this, int64_t p0, t29 * p1, t733 * p2, int64_t p3, t296 * p4, t841* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8064 (t1503 * __this, t1304 * p0, int64_t* p1, t29 ** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8065 (t1503 * __this, t1304 * p0, int64_t* p1, t29 ** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8066 (t1503 * __this, t1304 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8067 (t1503 * __this, t1304 * p0, int64_t* p1, t29 ** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8068 (t1503 * __this, t1304 * p0, t20 * p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8069 (t1503 * __this, t1304 * p0, int64_t* p1, t29 ** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8070 (t1503 * __this, t1304 * p0, int64_t* p1, t29 ** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8071 (t1503 * __this, t1304 * p0, t42 * p1, int64_t* p2, t29 ** p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1500 * m8072 (t1503 * __this, t1304 * p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8073 (t1503 * __this, t1304 * p0, t29 * p1, int64_t p2, t733 * p3, t42 * p4, t7* p5, t296 * p6, t841* p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8074 (t1503 * __this, t29 * p0, t7* p1, t296 * p2, t733 * p3, t29 * p4, t42 * p5, t841* p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8075 (t1503 * __this, int64_t p0, int64_t p1, t29 * p2, t733 * p3, t7* p4, t296 * p5, t841* p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m8076 (t1503 * __this, int64_t p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m8077 (t1503 * __this, t1304 * p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8078 (t29 * __this, t1304 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
