﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3125;
struct t29;
struct t557;
struct t316;

 void m17261_gshared (t3125 * __this, t29 * p0, t557 * p1, MethodInfo* method);
#define m17261(__this, p0, p1, method) (void)m17261_gshared((t3125 *)__this, (t29 *)p0, (t557 *)p1, method)
 void m17262_gshared (t3125 * __this, t316* p0, MethodInfo* method);
#define m17262(__this, p0, method) (void)m17262_gshared((t3125 *)__this, (t316*)p0, method)
 bool m17263_gshared (t3125 * __this, t29 * p0, t557 * p1, MethodInfo* method);
#define m17263(__this, p0, p1, method) (bool)m17263_gshared((t3125 *)__this, (t29 *)p0, (t557 *)p1, method)
