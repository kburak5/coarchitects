﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1180;
struct t29;

 void m6097 (t1180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6098 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6099 (t1180 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
