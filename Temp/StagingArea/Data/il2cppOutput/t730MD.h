﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t730;
struct t29;
struct t729;
struct t20;
struct t136;

 void m3117 (t730 * __this, t729 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3118 (t730 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3119 (t730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3120 (t730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3121 (t730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3122 (t730 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
