﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2404;
struct t29;
struct t20;
#include "t128.h"

 void m12426 (t2404 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12427 (t2404 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12428 (t2404 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12429 (t2404 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12430 (t2404 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
