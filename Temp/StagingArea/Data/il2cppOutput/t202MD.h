﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t202;
struct t403;
struct t388;
struct t387;
struct t7;
struct t148;
struct t29;
struct t201;
struct t530;
struct t531;
struct t528;
struct t529;
struct t163;
#include "t164.h"
#include "t132.h"
#include "t149.h"
#include "t152.h"
#include "t151.h"
#include "t150.h"
#include "t17.h"
#include "t238.h"

 void m1709 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1912 (t202 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2693 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2694 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2695 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2696 (t202 * __this, t7* p0, t148 * p1, t132  p2, int32_t p3, float p4, int32_t p5, bool p6, bool p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, bool p12, int32_t p13, t17  p14, t17  p15, bool p16, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2697 (t202 * __this, t7* p0, t148 * p1, t132  p2, int32_t p3, float p4, int32_t p5, bool p6, bool p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, bool p12, int32_t p13, float p14, float p15, float p16, float p17, bool p18, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2698 (t29 * __this, t202 * p0, t7* p1, t148 * p2, t132 * p3, int32_t p4, float p5, int32_t p6, bool p7, bool p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, bool p13, int32_t p14, float p15, float p16, float p17, float p18, bool p19, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t164  m1776 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2699 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2700 (t202 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t201* m2701 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2702 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1751 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2703 (t202 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t530* m2704 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1750 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2705 (t202 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t531* m2706 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1801 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2707 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t238  m2708 (t202 * __this, t238  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1921 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2709 (t202 * __this, t528 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2710 (t202 * __this, t529 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2711 (t202 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1919 (t202 * __this, t7* p0, t238  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1920 (t202 * __this, t7* p0, t238  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1774 (t202 * __this, t7* p0, t238  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2712 (t202 * __this, t7* p0, t238  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m1923 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m1752 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m1748 (t202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
