﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1235;
struct t731;
struct t7;
struct t292;
struct t1569;

 void m8500 (t1235 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8501 (t1235 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8502 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t731 * m8503 (t1235 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8504 (t1235 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8505 (t1235 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8506 (t1235 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8507 (t1235 * __this, t1235 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8508 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8509 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8510 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8511 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8512 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8513 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1235 * m8514 (t1235 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8515 (t1235 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8516 (t1235 * __this, t292 ** p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1569 * m8517 (t1235 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
