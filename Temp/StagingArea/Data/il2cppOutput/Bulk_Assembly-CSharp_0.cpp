﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t0.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t0_TI;
#include "t0MD.h"


#include "t20.h"

// Metadata Definition <Module>
static MethodInfo* t0_MIs[] =
{
	NULL
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t0_0_0_0;
extern Il2CppType t0_1_0_0;
struct t0;
TypeInfo t0_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "<Module>", "", t0_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t0_TI, NULL, NULL, &EmptyCustomAttributesCache, &t0_TI, &t0_0_0_0, &t0_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t0), 0, -1, 0, 0, -1, 0, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#include "t1.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1_TI;
#include "t1MD.h"

#include "t21.h"
#include "t22.h"
#include "t23.h"
#include "t24.h"
#include "t25.h"
#include "t2.h"
extern TypeInfo t26_TI;
extern TypeInfo t23_TI;
extern TypeInfo t27_TI;
#include "t4MD.h"
#include "t26MD.h"
#include "t24MD.h"
#include "t28MD.h"
#include "t25MD.h"
#include "t27MD.h"
extern MethodInfo m36_MI;
extern MethodInfo m37_MI;
extern MethodInfo m38_MI;
extern MethodInfo m39_MI;
extern MethodInfo m40_MI;
extern MethodInfo m41_MI;
extern MethodInfo m42_MI;
extern MethodInfo m43_MI;
extern MethodInfo m44_MI;


extern MethodInfo m0_MI;
 void m0 (t1 * __this, MethodInfo* method){
	{
		__this->f3 = (1.8f);
		m36(__this, &m36_MI);
		return;
	}
}
extern MethodInfo m1_MI;
 void m1 (t1 * __this, MethodInfo* method){
	t23  V_0 = {0};
	t23  V_1 = {0};
	t23  V_2 = {0};
	t23  V_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t26_TI));
		t23  L_0 = m37(NULL, &m37_MI);
		V_0 = L_0;
		t24 * L_1 = m38(NULL, &m38_MI);
		t25 * L_2 = m39(L_1, &m39_MI);
		t23  L_3 = m40(L_2, &m40_MI);
		V_3 = L_3;
		float L_4 = ((&V_3)->f3);
		(&V_0)->f3 = ((-L_4));
		t24 * L_5 = m38(NULL, &m38_MI);
		t23  L_6 = m41(L_5, V_0, &m41_MI);
		V_1 = L_6;
		t2 * L_7 = (__this->f2);
		t25 * L_8 = m39(L_7, &m39_MI);
		t23  L_9 = m42(L_8, &m42_MI);
		V_2 = L_9;
		float L_10 = ((&V_1)->f1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_11 = m43(NULL, L_10, (-30.0f), (30.0f), &m43_MI);
		(&V_2)->f1 = ((float)((float)L_11*(float)(-1.0f)));
		float L_12 = ((&V_1)->f2);
		float L_13 = m43(NULL, L_12, (-19.0f), (19.0f), &m43_MI);
		(&V_2)->f2 = ((float)((float)L_13*(float)(-1.0f)));
		t2 * L_14 = (__this->f2);
		t25 * L_15 = m39(L_14, &m39_MI);
		m44(L_15, V_2, &m44_MI);
		return;
	}
}
// Metadata Definition BackgroundMove
extern Il2CppType t2_0_0_6;
FieldInfo t1_f2_FieldInfo = 
{
	"background", &t2_0_0_6, &t1_TI, offsetof(t1, f2), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_6;
FieldInfo t1_f3_FieldInfo = 
{
	"speed", &t22_0_0_6, &t1_TI, offsetof(t1, f3), &EmptyCustomAttributesCache};
extern Il2CppType t3_0_0_6;
FieldInfo t1_f4_FieldInfo = 
{
	"c", &t3_0_0_6, &t1_TI, offsetof(t1, f4), &EmptyCustomAttributesCache};
static FieldInfo* t1_FIs[] =
{
	&t1_f2_FieldInfo,
	&t1_f3_FieldInfo,
	&t1_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m0_MI = 
{
	".ctor", (methodPointerType)&m0, &t1_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1_MI = 
{
	"Update", (methodPointerType)&m1, &t1_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 2, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t1_MIs[] =
{
	&m0_MI,
	&m1_MI,
	NULL
};
extern MethodInfo m45_MI;
extern MethodInfo m46_MI;
extern MethodInfo m47_MI;
extern MethodInfo m48_MI;
static MethodInfo* t1_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t1_0_0_0;
extern Il2CppType t1_1_0_0;
extern TypeInfo t4_TI;
struct t1;
TypeInfo t1_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "BackgroundMove", "", t1_MIs, NULL, t1_FIs, NULL, &t4_TI, NULL, NULL, &t1_TI, NULL, t1_VT, &EmptyCustomAttributesCache, &t1_TI, &t1_0_0_0, &t1_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t1), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 3, 0, 0, 4, 0, 0};
#include "t5.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5_TI;
#include "t5MD.h"

#include "t6.h"
#include "t7.h"
#include "t23MD.h"
extern MethodInfo m49_MI;
extern MethodInfo m50_MI;


extern MethodInfo m2_MI;
 void m2 (t5 * __this, MethodInfo* method){
	{
		__this->f2 = (0.5f);
		m36(__this, &m36_MI);
		return;
	}
}
extern MethodInfo m3_MI;
 void m3 (t5 * __this, t6 * p0, MethodInfo* method){
	{
		t25 * L_0 = m39(__this, &m39_MI);
		t23  L_1 = {0};
		m49(&L_1, (1.06f), (1.06f), (0.0f), &m49_MI);
		m50(L_0, L_1, &m50_MI);
		return;
	}
}
extern MethodInfo m4_MI;
 void m4 (t5 * __this, t6 * p0, MethodInfo* method){
	{
		t25 * L_0 = m39(__this, &m39_MI);
		t23  L_1 = {0};
		m49(&L_1, (1.0f), (1.0f), (0.0f), &m49_MI);
		m50(L_0, L_1, &m50_MI);
		return;
	}
}
extern MethodInfo m5_MI;
 void m5 (t5 * __this, t7* p0, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition ButtonScale
extern Il2CppType t22_0_0_6;
FieldInfo t5_f2_FieldInfo = 
{
	"speed", &t22_0_0_6, &t5_TI, offsetof(t5, f2), &EmptyCustomAttributesCache};
static FieldInfo* t5_FIs[] =
{
	&t5_f2_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m2_MI = 
{
	".ctor", (methodPointerType)&m2, &t5_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 3, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t5_m3_ParameterInfos[] = 
{
	{"eventData", 0, 134217729, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3_MI = 
{
	"OnPointerEnter", (methodPointerType)&m3, &t5_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5_m3_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 1, false, false, 4, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t5_m4_ParameterInfos[] = 
{
	{"eventData", 0, 134217730, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m4_MI = 
{
	"OnPointerExit", (methodPointerType)&m4, &t5_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5_m4_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 5, 1, false, false, 5, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t5_m5_ParameterInfos[] = 
{
	{"s", 0, 134217731, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m5_MI = 
{
	"FunctionWorks", (methodPointerType)&m5, &t5_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5_m5_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 6, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t5_MIs[] =
{
	&m2_MI,
	&m3_MI,
	&m4_MI,
	&m5_MI,
	NULL
};
static MethodInfo* t5_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m3_MI,
	&m4_MI,
};
extern TypeInfo t30_TI;
extern TypeInfo t31_TI;
extern TypeInfo t32_TI;
static TypeInfo* t5_ITIs[] = 
{
	&t30_TI,
	&t31_TI,
	&t32_TI,
};
static Il2CppInterfaceOffsetPair t5_IOs[] = 
{
	{ &t30_TI, 4},
	{ &t31_TI, 5},
	{ &t32_TI, 5},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t5_0_0_0;
extern Il2CppType t5_1_0_0;
struct t5;
TypeInfo t5_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "ButtonScale", "", t5_MIs, NULL, t5_FIs, NULL, &t4_TI, NULL, NULL, &t5_TI, t5_ITIs, t5_VT, &EmptyCustomAttributesCache, &t5_TI, &t5_0_0_0, &t5_1_0_0, t5_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t5), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 3, 3};
#include "t8.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t8_TI;
#include "t8MD.h"

#include "t9.h"
#include "t33.h"
#include "t34.h"
#include "t29.h"
#include "t35.h"
extern TypeInfo t34_TI;
#include "t9MD.h"
#include "t34MD.h"
#include "t36MD.h"
#include "t37MD.h"
extern MethodInfo m51_MI;
extern MethodInfo m9_MI;
extern MethodInfo m52_MI;
extern MethodInfo m53_MI;
extern MethodInfo m54_MI;
extern MethodInfo m8_MI;


extern MethodInfo m6_MI;
 void m6 (t8 * __this, MethodInfo* method){
	{
		m36(__this, &m36_MI);
		return;
	}
}
extern MethodInfo m7_MI;
 void m7 (t8 * __this, MethodInfo* method){
	{
		t9 * L_0 = (__this->f2);
		t33 * L_1 = m51(L_0, &m51_MI);
		t35 L_2 = { &m9_MI };
		t34 * L_3 = (t34 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t34_TI));
		m52(L_3, __this, L_2, &m52_MI);
		m53(L_1, L_3, &m53_MI);
		return;
	}
}
 void m8 (t8 * __this, t7* p0, MethodInfo* method){
	{
		m54(NULL, p0, &m54_MI);
		return;
	}
}
 void m9 (t8 * __this, MethodInfo* method){
	{
		m8(__this, (t7*) &_stringLiteral1, &m8_MI);
		return;
	}
}
// Metadata Definition LoadScene_0
extern Il2CppType t9_0_0_6;
FieldInfo t8_f2_FieldInfo = 
{
	"selectButton", &t9_0_0_6, &t8_TI, offsetof(t8, f2), &EmptyCustomAttributesCache};
static FieldInfo* t8_FIs[] =
{
	&t8_f2_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m6_MI = 
{
	".ctor", (methodPointerType)&m6, &t8_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 7, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m7_MI = 
{
	"Awake", (methodPointerType)&m7, &t8_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 8, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t8_m8_ParameterInfos[] = 
{
	{"s", 0, 134217732, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m8_MI = 
{
	"LoadScene", (methodPointerType)&m8, &t8_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t8_m8_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 9, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t8__CustomAttributeCache_m9;
MethodInfo m9_MI = 
{
	"<Awake>m__0", (methodPointerType)&m9, &t8_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t8__CustomAttributeCache_m9, 129, 0, 255, 0, false, false, 10, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t8_MIs[] =
{
	&m6_MI,
	&m7_MI,
	&m8_MI,
	&m9_MI,
	NULL
};
static MethodInfo* t8_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
extern TypeInfo t38_TI;
#include "t38.h"
#include "t38MD.h"
extern MethodInfo m55_MI;
void t8_CustomAttributesCacheGenerator_m9(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t8__CustomAttributeCache_m9 = {
1,
NULL,
&t8_CustomAttributesCacheGenerator_m9
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t8_0_0_0;
extern Il2CppType t8_1_0_0;
struct t8;
extern CustomAttributesCache t8__CustomAttributeCache_m9;
TypeInfo t8_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "LoadScene_0", "", t8_MIs, NULL, t8_FIs, NULL, &t4_TI, NULL, NULL, &t8_TI, NULL, t8_VT, &EmptyCustomAttributesCache, &t8_TI, &t8_0_0_0, &t8_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t8), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 4, 0, 0};
#include "t10.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t10_TI;
#include "t10MD.h"

extern MethodInfo m13_MI;
extern MethodInfo m12_MI;


extern MethodInfo m10_MI;
 void m10 (t10 * __this, MethodInfo* method){
	{
		m36(__this, &m36_MI);
		return;
	}
}
extern MethodInfo m11_MI;
 void m11 (t10 * __this, MethodInfo* method){
	{
		t9 * L_0 = (__this->f2);
		t33 * L_1 = m51(L_0, &m51_MI);
		t35 L_2 = { &m13_MI };
		t34 * L_3 = (t34 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t34_TI));
		m52(L_3, __this, L_2, &m52_MI);
		m53(L_1, L_3, &m53_MI);
		return;
	}
}
 void m12 (t10 * __this, t7* p0, MethodInfo* method){
	{
		m54(NULL, p0, &m54_MI);
		return;
	}
}
 void m13 (t10 * __this, MethodInfo* method){
	{
		m12(__this, (t7*) &_stringLiteral2, &m12_MI);
		return;
	}
}
// Metadata Definition LoadScene_1
extern Il2CppType t9_0_0_6;
FieldInfo t10_f2_FieldInfo = 
{
	"startButton", &t9_0_0_6, &t10_TI, offsetof(t10, f2), &EmptyCustomAttributesCache};
static FieldInfo* t10_FIs[] =
{
	&t10_f2_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m10_MI = 
{
	".ctor", (methodPointerType)&m10, &t10_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 11, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m11_MI = 
{
	"Awake", (methodPointerType)&m11, &t10_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 12, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t10_m12_ParameterInfos[] = 
{
	{"s", 0, 134217733, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m12_MI = 
{
	"LoadScene", (methodPointerType)&m12, &t10_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t10_m12_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 13, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t10__CustomAttributeCache_m13;
MethodInfo m13_MI = 
{
	"<Awake>m__1", (methodPointerType)&m13, &t10_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t10__CustomAttributeCache_m13, 129, 0, 255, 0, false, false, 14, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t10_MIs[] =
{
	&m10_MI,
	&m11_MI,
	&m12_MI,
	&m13_MI,
	NULL
};
static MethodInfo* t10_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
void t10_CustomAttributesCacheGenerator_m13(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t10__CustomAttributeCache_m13 = {
1,
NULL,
&t10_CustomAttributesCacheGenerator_m13
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t10_0_0_0;
extern Il2CppType t10_1_0_0;
struct t10;
extern CustomAttributesCache t10__CustomAttributeCache_m13;
TypeInfo t10_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "LoadScene_1", "", t10_MIs, NULL, t10_FIs, NULL, &t4_TI, NULL, NULL, &t10_TI, NULL, t10_VT, &EmptyCustomAttributesCache, &t10_TI, &t10_0_0_0, &t10_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t10), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 4, 0, 0};
#include "t11.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t11_TI;
#include "t11MD.h"

extern MethodInfo m17_MI;
extern MethodInfo m16_MI;


extern MethodInfo m14_MI;
 void m14 (t11 * __this, MethodInfo* method){
	{
		m36(__this, &m36_MI);
		return;
	}
}
extern MethodInfo m15_MI;
 void m15 (t11 * __this, MethodInfo* method){
	{
		t9 * L_0 = (__this->f2);
		t33 * L_1 = m51(L_0, &m51_MI);
		t35 L_2 = { &m17_MI };
		t34 * L_3 = (t34 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t34_TI));
		m52(L_3, __this, L_2, &m52_MI);
		m53(L_1, L_3, &m53_MI);
		return;
	}
}
 void m16 (t11 * __this, t7* p0, MethodInfo* method){
	{
		m54(NULL, p0, &m54_MI);
		return;
	}
}
 void m17 (t11 * __this, MethodInfo* method){
	{
		m16(__this, (t7*) &_stringLiteral3, &m16_MI);
		return;
	}
}
// Metadata Definition LoadScene_2
extern Il2CppType t9_0_0_6;
FieldInfo t11_f2_FieldInfo = 
{
	"polygonButton", &t9_0_0_6, &t11_TI, offsetof(t11, f2), &EmptyCustomAttributesCache};
static FieldInfo* t11_FIs[] =
{
	&t11_f2_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m14_MI = 
{
	".ctor", (methodPointerType)&m14, &t11_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 15, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m15_MI = 
{
	"Start", (methodPointerType)&m15, &t11_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 16, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t11_m16_ParameterInfos[] = 
{
	{"s", 0, 134217734, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m16_MI = 
{
	"LoadLevel", (methodPointerType)&m16, &t11_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t11_m16_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 17, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t11__CustomAttributeCache_m17;
MethodInfo m17_MI = 
{
	"<Start>m__2", (methodPointerType)&m17, &t11_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t11__CustomAttributeCache_m17, 129, 0, 255, 0, false, false, 18, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t11_MIs[] =
{
	&m14_MI,
	&m15_MI,
	&m16_MI,
	&m17_MI,
	NULL
};
static MethodInfo* t11_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
void t11_CustomAttributesCacheGenerator_m17(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t11__CustomAttributeCache_m17 = {
1,
NULL,
&t11_CustomAttributesCacheGenerator_m17
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t11_0_0_0;
extern Il2CppType t11_1_0_0;
struct t11;
extern CustomAttributesCache t11__CustomAttributeCache_m17;
TypeInfo t11_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "LoadScene_2", "", t11_MIs, NULL, t11_FIs, NULL, &t4_TI, NULL, NULL, &t11_TI, NULL, t11_VT, &EmptyCustomAttributesCache, &t11_TI, &t11_0_0_0, &t11_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t11), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 4, 0, 0};
#include "t12.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t12_TI;
#include "t12MD.h"

extern MethodInfo m21_MI;
extern MethodInfo m20_MI;


extern MethodInfo m18_MI;
 void m18 (t12 * __this, MethodInfo* method){
	{
		m36(__this, &m36_MI);
		return;
	}
}
extern MethodInfo m19_MI;
 void m19 (t12 * __this, MethodInfo* method){
	{
		t9 * L_0 = (__this->f2);
		t33 * L_1 = m51(L_0, &m51_MI);
		t35 L_2 = { &m21_MI };
		t34 * L_3 = (t34 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t34_TI));
		m52(L_3, __this, L_2, &m52_MI);
		m53(L_1, L_3, &m53_MI);
		return;
	}
}
 void m20 (t12 * __this, t7* p0, MethodInfo* method){
	{
		m54(NULL, p0, &m54_MI);
		return;
	}
}
 void m21 (t12 * __this, MethodInfo* method){
	{
		m20(__this, (t7*) &_stringLiteral4, &m20_MI);
		return;
	}
}
// Metadata Definition LoadScene_3
extern Il2CppType t9_0_0_6;
FieldInfo t12_f2_FieldInfo = 
{
	"selectButton", &t9_0_0_6, &t12_TI, offsetof(t12, f2), &EmptyCustomAttributesCache};
static FieldInfo* t12_FIs[] =
{
	&t12_f2_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m18_MI = 
{
	".ctor", (methodPointerType)&m18, &t12_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 19, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m19_MI = 
{
	"Start", (methodPointerType)&m19, &t12_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 20, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t12_m20_ParameterInfos[] = 
{
	{"s", 0, 134217735, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m20_MI = 
{
	"LoadLevel", (methodPointerType)&m20, &t12_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t12_m20_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 21, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t12__CustomAttributeCache_m21;
MethodInfo m21_MI = 
{
	"<Start>m__3", (methodPointerType)&m21, &t12_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t12__CustomAttributeCache_m21, 129, 0, 255, 0, false, false, 22, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t12_MIs[] =
{
	&m18_MI,
	&m19_MI,
	&m20_MI,
	&m21_MI,
	NULL
};
static MethodInfo* t12_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
void t12_CustomAttributesCacheGenerator_m21(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t12__CustomAttributeCache_m21 = {
1,
NULL,
&t12_CustomAttributesCacheGenerator_m21
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t12_0_0_0;
extern Il2CppType t12_1_0_0;
struct t12;
extern CustomAttributesCache t12__CustomAttributeCache_m21;
TypeInfo t12_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "LoadScene_3", "", t12_MIs, NULL, t12_FIs, NULL, &t4_TI, NULL, NULL, &t12_TI, NULL, t12_VT, &EmptyCustomAttributesCache, &t12_TI, &t12_0_0_0, &t12_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t12), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 4, 0, 0};
#include "t13.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t13_TI;
#include "t13MD.h"

extern MethodInfo m25_MI;
extern MethodInfo m24_MI;


extern MethodInfo m22_MI;
 void m22 (t13 * __this, MethodInfo* method){
	{
		m36(__this, &m36_MI);
		return;
	}
}
extern MethodInfo m23_MI;
 void m23 (t13 * __this, MethodInfo* method){
	{
		t9 * L_0 = (__this->f2);
		t33 * L_1 = m51(L_0, &m51_MI);
		t35 L_2 = { &m25_MI };
		t34 * L_3 = (t34 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t34_TI));
		m52(L_3, __this, L_2, &m52_MI);
		m53(L_1, L_3, &m53_MI);
		return;
	}
}
 void m24 (t13 * __this, t7* p0, MethodInfo* method){
	{
		m54(NULL, p0, &m54_MI);
		return;
	}
}
 void m25 (t13 * __this, MethodInfo* method){
	{
		m24(__this, (t7*) &_stringLiteral5, &m24_MI);
		return;
	}
}
// Metadata Definition LoadScene_4
extern Il2CppType t9_0_0_6;
FieldInfo t13_f2_FieldInfo = 
{
	"doneButton", &t9_0_0_6, &t13_TI, offsetof(t13, f2), &EmptyCustomAttributesCache};
static FieldInfo* t13_FIs[] =
{
	&t13_f2_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m22_MI = 
{
	".ctor", (methodPointerType)&m22, &t13_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 23, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m23_MI = 
{
	"Start", (methodPointerType)&m23, &t13_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 24, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t13_m24_ParameterInfos[] = 
{
	{"s", 0, 134217736, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m24_MI = 
{
	"LoadLevel", (methodPointerType)&m24, &t13_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t13_m24_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 25, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t13__CustomAttributeCache_m25;
MethodInfo m25_MI = 
{
	"<Start>m__4", (methodPointerType)&m25, &t13_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t13__CustomAttributeCache_m25, 129, 0, 255, 0, false, false, 26, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t13_MIs[] =
{
	&m22_MI,
	&m23_MI,
	&m24_MI,
	&m25_MI,
	NULL
};
static MethodInfo* t13_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
void t13_CustomAttributesCacheGenerator_m25(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t13__CustomAttributeCache_m25 = {
1,
NULL,
&t13_CustomAttributesCacheGenerator_m25
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t13_0_0_0;
extern Il2CppType t13_1_0_0;
struct t13;
extern CustomAttributesCache t13__CustomAttributeCache_m25;
TypeInfo t13_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "LoadScene_4", "", t13_MIs, NULL, t13_FIs, NULL, &t4_TI, NULL, NULL, &t13_TI, NULL, t13_VT, &EmptyCustomAttributesCache, &t13_TI, &t13_0_0_0, &t13_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t13), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 4, 0, 0};
#include "t14.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t14_TI;
#include "t14MD.h"

#include "t15.h"
#include "t39.h"
#include "t40.h"
#include "t17.h"
#include "t16.h"
#include "t41.h"
extern TypeInfo t15_TI;
extern TypeInfo t21_TI;
extern TypeInfo t7_TI;
extern TypeInfo t40_TI;
extern TypeInfo t17_TI;
extern TypeInfo t22_TI;
#include "t15MD.h"
#include "t39MD.h"
#include "t2MD.h"
#include "t17MD.h"
#include "t22MD.h"
#include "t7MD.h"
#include "t41MD.h"
extern MethodInfo m56_MI;
extern MethodInfo m57_MI;
extern MethodInfo m58_MI;
extern MethodInfo m59_MI;
extern MethodInfo m60_MI;
extern MethodInfo m61_MI;
extern MethodInfo m62_MI;
extern MethodInfo m63_MI;
extern MethodInfo m64_MI;
extern MethodInfo m65_MI;
extern MethodInfo m66_MI;
extern MethodInfo m29_MI;
extern MethodInfo m67_MI;
struct t28;
#include "t28.h"
#include "t42.h"
#include "t43.h"
struct t28;
 t29 * m68_gshared (t28 * __this, MethodInfo* method);
#define m68(__this, method) (t29 *)m68_gshared((t28 *)__this, method)
#define m57(__this, method) (t39 *)m68_gshared((t28 *)__this, method)
struct t28;
#define m60(__this, method) (t2 *)m68_gshared((t28 *)__this, method)


extern MethodInfo m26_MI;
 void m26 (t14 * __this, MethodInfo* method){
	{
		__this->f5 = (352.0f);
		m36(__this, &m36_MI);
		return;
	}
}
extern MethodInfo m27_MI;
 void m27 (t14 * __this, MethodInfo* method){
	{
		t15 * L_0 = (__this->f3);
		VirtActionInvoker1< t7* >::Invoke(&m56_MI, L_0, (t7*) &_stringLiteral6);
		t15 * L_1 = (__this->f4);
		VirtActionInvoker1< t7* >::Invoke(&m56_MI, L_1, (t7*) &_stringLiteral6);
		return;
	}
}
extern MethodInfo m28_MI;
 void m28 (t14 * __this, MethodInfo* method){
	t23  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		t39 * L_0 = m57(__this, &m57_MI);
		bool L_1 = m58(L_0, &m58_MI);
		__this->f2 = L_1;
		bool L_2 = (__this->f2);
		bool L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t40_TI), &L_3);
		m59(NULL, L_4, &m59_MI);
		bool L_5 = (__this->f2);
		if (!L_5)
		{
			goto IL_00f7;
		}
	}
	{
		t2 * L_6 = m60(__this, &m60_MI);
		t17  L_7 = m61(L_6, &m61_MI);
		__this->f7 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t26_TI));
		t23  L_8 = m37(NULL, &m37_MI);
		V_0 = L_8;
		float L_9 = ((&V_0)->f2);
		float L_10 = (__this->f5);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_11 = m43(NULL, L_9, (0.0f), L_10, &m43_MI);
		t17  L_12 = {0};
		m62(&L_12, (12.0f), L_11, &m62_MI);
		__this->f7 = L_12;
		t2 * L_13 = m60(__this, &m60_MI);
		t17  L_14 = (__this->f7);
		m63(L_13, L_14, &m63_MI);
		t15 * L_15 = (__this->f3);
		float L_16 = (__this->f5);
		t17 * L_17 = &(__this->f7);
		float L_18 = (L_17->f2);
		float L_19 = roundf(((float)(L_16-L_18)));
		V_1 = L_19;
		t7* L_20 = m65((&V_1), &m65_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_21 = m66(NULL, L_20, (t7*) &_stringLiteral7, &m66_MI);
		VirtActionInvoker1< t7* >::Invoke(&m56_MI, L_15, L_21);
		t15 * L_22 = (__this->f4);
		t17 * L_23 = &(__this->f7);
		float L_24 = (L_23->f2);
		float L_25 = roundf(L_24);
		V_2 = L_25;
		t7* L_26 = m65((&V_2), &m65_MI);
		t7* L_27 = m66(NULL, L_26, (t7*) &_stringLiteral7, &m66_MI);
		VirtActionInvoker1< t7* >::Invoke(&m56_MI, L_22, L_27);
		t17 * L_28 = &(__this->f7);
		float L_29 = (L_28->f2);
		float L_30 = L_29;
		t29 * L_31 = Box(InitializedTypeInfo(&t22_TI), &L_30);
		m59(NULL, L_31, &m59_MI);
		m29(__this, &m29_MI);
	}

IL_00f7:
	{
		return;
	}
}
 void m29 (t14 * __this, MethodInfo* method){
	{
		t16 * L_0 = (__this->f6);
		m67(NULL, L_0, &m67_MI);
		return;
	}
}
// Metadata Definition ModifyTransform
extern Il2CppType t40_0_0_6;
FieldInfo t14_f2_FieldInfo = 
{
	"areaEditingBool", &t40_0_0_6, &t14_TI, offsetof(t14, f2), &EmptyCustomAttributesCache};
extern Il2CppType t15_0_0_6;
FieldInfo t14_f3_FieldInfo = 
{
	"upText", &t15_0_0_6, &t14_TI, offsetof(t14, f3), &EmptyCustomAttributesCache};
extern Il2CppType t15_0_0_6;
FieldInfo t14_f4_FieldInfo = 
{
	"downText", &t15_0_0_6, &t14_TI, offsetof(t14, f4), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_6;
FieldInfo t14_f5_FieldInfo = 
{
	"maxHeight", &t22_0_0_6, &t14_TI, offsetof(t14, f5), &EmptyCustomAttributesCache};
extern Il2CppType t16_0_0_6;
FieldInfo t14_f6_FieldInfo = 
{
	"infoDestroy", &t16_0_0_6, &t14_TI, offsetof(t14, f6), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_6;
FieldInfo t14_f7_FieldInfo = 
{
	"scale", &t17_0_0_6, &t14_TI, offsetof(t14, f7), &EmptyCustomAttributesCache};
static FieldInfo* t14_FIs[] =
{
	&t14_f2_FieldInfo,
	&t14_f3_FieldInfo,
	&t14_f4_FieldInfo,
	&t14_f5_FieldInfo,
	&t14_f6_FieldInfo,
	&t14_f7_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m26_MI = 
{
	".ctor", (methodPointerType)&m26, &t14_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 27, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m27_MI = 
{
	"Start", (methodPointerType)&m27, &t14_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 28, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m28_MI = 
{
	"Update", (methodPointerType)&m28, &t14_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 29, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m29_MI = 
{
	"InfoFade", (methodPointerType)&m29, &t14_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 30, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t14_MIs[] =
{
	&m26_MI,
	&m27_MI,
	&m28_MI,
	&m29_MI,
	NULL
};
static MethodInfo* t14_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t14_0_0_0;
extern Il2CppType t14_1_0_0;
struct t14;
TypeInfo t14_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "ModifyTransform", "", t14_MIs, NULL, t14_FIs, NULL, &t4_TI, NULL, NULL, &t14_TI, NULL, t14_VT, &EmptyCustomAttributesCache, &t14_TI, &t14_0_0_0, &t14_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t14), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 4, 0, 6, 0, 0, 4, 0, 0};
#include "t18.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t18_TI;
#include "t18MD.h"

extern MethodInfo m69_MI;


extern MethodInfo m30_MI;
 void m30 (t18 * __this, MethodInfo* method){
	{
		m36(__this, &m36_MI);
		return;
	}
}
extern MethodInfo m31_MI;
 void m31 (t18 * __this, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m32_MI;
 void m32 (t18 * __this, MethodInfo* method){
	t23  V_0 = {0};
	t23  V_1 = {0};
	{
		t25 * L_0 = m39(__this, &m39_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t26_TI));
		t23  L_1 = m37(NULL, &m37_MI);
		V_0 = L_1;
		float L_2 = ((&V_0)->f1);
		t23  L_3 = m37(NULL, &m37_MI);
		V_1 = L_3;
		float L_4 = ((&V_1)->f2);
		t23  L_5 = {0};
		m49(&L_5, L_2, L_4, (0.0f), &m49_MI);
		m69(L_0, L_5, &m69_MI);
		return;
	}
}
// Metadata Definition PointerMovement
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m30_MI = 
{
	".ctor", (methodPointerType)&m30, &t18_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 31, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m31_MI = 
{
	"Start", (methodPointerType)&m31, &t18_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 32, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m32_MI = 
{
	"Update", (methodPointerType)&m32, &t18_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 33, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t18_MIs[] =
{
	&m30_MI,
	&m31_MI,
	&m32_MI,
	NULL
};
static MethodInfo* t18_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t18_0_0_0;
extern Il2CppType t18_1_0_0;
struct t18;
TypeInfo t18_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "PointerMovement", "", t18_MIs, NULL, NULL, NULL, &t4_TI, NULL, NULL, &t18_TI, NULL, t18_VT, &EmptyCustomAttributesCache, &t18_TI, &t18_0_0_0, &t18_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t18), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 4, 0, 0};
#include "t19.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t19_TI;
#include "t19MD.h"

#include "t44.h"
#include "t45MD.h"
extern MethodInfo m70_MI;


extern MethodInfo m33_MI;
 void m33 (t19 * __this, MethodInfo* method){
	{
		m36(__this, &m36_MI);
		return;
	}
}
extern MethodInfo m34_MI;
 void m34 (t19 * __this, MethodInfo* method){
	{
		m70(NULL, ((int32_t)667), ((int32_t)375), 0, &m70_MI);
		return;
	}
}
extern MethodInfo m35_MI;
 void m35 (t19 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition ScreenResolution
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m33_MI = 
{
	".ctor", (methodPointerType)&m33, &t19_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 34, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m34_MI = 
{
	"Start", (methodPointerType)&m34, &t19_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 35, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m35_MI = 
{
	"Update", (methodPointerType)&m35, &t19_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 36, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t19_MIs[] =
{
	&m33_MI,
	&m34_MI,
	&m35_MI,
	NULL
};
static MethodInfo* t19_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType t19_0_0_0;
extern Il2CppType t19_1_0_0;
struct t19;
TypeInfo t19_TI = 
{
	&g_AssemblyU2DCSharp_dll_Image, NULL, "ScreenResolution", "", t19_MIs, NULL, NULL, NULL, &t4_TI, NULL, NULL, &t19_TI, NULL, t19_VT, &EmptyCustomAttributesCache, &t19_TI, &t19_0_0_0, &t19_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t19), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 4, 0, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
