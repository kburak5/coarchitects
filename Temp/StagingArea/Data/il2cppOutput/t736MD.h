﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t736;
struct t733;
struct t7;
struct t731;
#include "t735.h"

 void m3140 (t736 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3141 (t736 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3142 (t736 * __this, t7* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3143 (t736 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3144 (t29 * __this, t731 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3145 (t736 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3146 (t736 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
