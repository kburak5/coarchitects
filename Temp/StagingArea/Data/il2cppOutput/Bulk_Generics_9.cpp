﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t4157_TI;

#include "t44.h"
#include "t3.h"

#include "t20.h"

// Metadata Definition System.IComparable`1<UnityEngine.Canvas>
extern Il2CppType t3_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t4157_m20969_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20969_GM;
MethodInfo m20969_MI = 
{
	"CompareTo", NULL, &t4157_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4157_m20969_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20969_GM};
static MethodInfo* t4157_MIs[] =
{
	&m20969_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4157_0_0_0;
extern Il2CppType t4157_1_0_0;
struct t4157;
extern Il2CppGenericClass t4157_GC;
TypeInfo t4157_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4157_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4157_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4157_TI, &t4157_0_0_0, &t4157_1_0_0, NULL, &t4157_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t2540.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2540_TI;
#include "t2540MD.h"

#include "t21.h"
#include "t29.h"
#include "t7.h"
#include "t305.h"
extern TypeInfo t3_TI;
extern TypeInfo t44_TI;
extern TypeInfo t290_TI;
extern TypeInfo t29_TI;
extern TypeInfo t305_TI;
#include "t2539MD.h"
#include "t305MD.h"
extern MethodInfo m13550_MI;
extern MethodInfo m20969_MI;
extern MethodInfo m9672_MI;
extern MethodInfo m1935_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Canvas>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13554_GM;
MethodInfo m13554_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2540_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13554_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2540_m13555_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13555_GM;
MethodInfo m13555_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2540_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2540_m13555_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13555_GM};
static MethodInfo* t2540_MIs[] =
{
	&m13554_MI,
	&m13555_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m13555_MI;
extern MethodInfo m13552_MI;
static MethodInfo* t2540_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13555_MI,
	&m13552_MI,
	&m13555_MI,
};
extern TypeInfo t4156_TI;
extern TypeInfo t726_TI;
static Il2CppInterfaceOffsetPair t2540_IOs[] = 
{
	{ &t4156_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2539_TI;
extern TypeInfo t2539_TI;
extern TypeInfo t2540_TI;
extern MethodInfo m13554_MI;
extern TypeInfo t3_TI;
extern MethodInfo m27858_MI;
extern TypeInfo t3_TI;
extern TypeInfo t4157_TI;
static Il2CppRGCTXData t2540_RGCTXData[12] = 
{
	&t4157_0_0_0/* Type Usage */,
	&t3_0_0_0/* Type Usage */,
	&t2539_TI/* Class Usage */,
	&t2539_TI/* Static Usage */,
	&t2540_TI/* Class Usage */,
	&m13554_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m27858_MI/* Method Usage */,
	&m13550_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&t4157_TI/* Class Usage */,
	&m20969_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2540_0_0_0;
extern Il2CppType t2540_1_0_0;
extern TypeInfo t2539_TI;
struct t2540;
extern Il2CppGenericClass t2540_GC;
extern TypeInfo t1246_TI;
TypeInfo t2540_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2540_MIs, NULL, NULL, NULL, &t2539_TI, NULL, &t1246_TI, &t2540_TI, NULL, t2540_VT, &EmptyCustomAttributesCache, &t2540_TI, &t2540_0_0_0, &t2540_1_0_0, t2540_IOs, &t2540_GC, NULL, NULL, NULL, t2540_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2540), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t2532.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2532_TI;
#include "t2532MD.h"

#include "t35.h"
#include "t67.h"


// Metadata Definition System.Comparison`1<UnityEngine.Canvas>
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2532_m13556_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13556_GM;
MethodInfo m13556_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2532_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2532_m13556_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13556_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2532_m13557_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13557_GM;
MethodInfo m13557_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2532_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2532_m13557_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13557_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t3_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2532_m13558_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13558_GM;
MethodInfo m13558_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2532_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2532_m13558_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13558_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2532_m13559_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13559_GM;
MethodInfo m13559_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2532_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2532_m13559_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13559_GM};
static MethodInfo* t2532_MIs[] =
{
	&m13556_MI,
	&m13557_MI,
	&m13558_MI,
	&m13559_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m13557_MI;
extern MethodInfo m13558_MI;
extern MethodInfo m13559_MI;
static MethodInfo* t2532_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13557_MI,
	&m13558_MI,
	&m13559_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2532_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2532_0_0_0;
extern Il2CppType t2532_1_0_0;
extern TypeInfo t195_TI;
struct t2532;
extern Il2CppGenericClass t2532_GC;
TypeInfo t2532_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2532_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2532_TI, NULL, t2532_VT, &EmptyCustomAttributesCache, &t2532_TI, &t2532_0_0_0, &t2532_1_0_0, t2532_IOs, &t2532_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2532), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4159_TI;

#include "t166.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.GraphicRaycaster>
extern MethodInfo m27859_MI;
static PropertyInfo t4159____Current_PropertyInfo = 
{
	&t4159_TI, "Current", &m27859_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4159_PIs[] =
{
	&t4159____Current_PropertyInfo,
	NULL
};
extern Il2CppType t166_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27859_GM;
MethodInfo m27859_MI = 
{
	"get_Current", NULL, &t4159_TI, &t166_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27859_GM};
static MethodInfo* t4159_MIs[] =
{
	&m27859_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4159_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4159_0_0_0;
extern Il2CppType t4159_1_0_0;
struct t4159;
extern Il2CppGenericClass t4159_GC;
TypeInfo t4159_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4159_MIs, t4159_PIs, NULL, NULL, NULL, NULL, NULL, &t4159_TI, t4159_ITIs, NULL, &EmptyCustomAttributesCache, &t4159_TI, &t4159_0_0_0, &t4159_1_0_0, NULL, &t4159_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2541.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2541_TI;
#include "t2541MD.h"

#include "t914.h"
#include "t40.h"
extern TypeInfo t166_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m13564_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m20974_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m20974(__this, p0, method) (t166 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.GraphicRaycaster>
extern Il2CppType t20_0_0_1;
FieldInfo t2541_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2541_TI, offsetof(t2541, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2541_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2541_TI, offsetof(t2541, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2541_FIs[] =
{
	&t2541_f0_FieldInfo,
	&t2541_f1_FieldInfo,
	NULL
};
extern MethodInfo m13561_MI;
static PropertyInfo t2541____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2541_TI, "System.Collections.IEnumerator.Current", &m13561_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2541____Current_PropertyInfo = 
{
	&t2541_TI, "Current", &m13564_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2541_PIs[] =
{
	&t2541____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2541____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2541_m13560_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13560_GM;
MethodInfo m13560_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2541_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2541_m13560_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13560_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13561_GM;
MethodInfo m13561_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2541_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13561_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13562_GM;
MethodInfo m13562_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2541_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13562_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13563_GM;
MethodInfo m13563_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2541_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13563_GM};
extern Il2CppType t166_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13564_GM;
MethodInfo m13564_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2541_TI, &t166_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13564_GM};
static MethodInfo* t2541_MIs[] =
{
	&m13560_MI,
	&m13561_MI,
	&m13562_MI,
	&m13563_MI,
	&m13564_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m13563_MI;
extern MethodInfo m13562_MI;
static MethodInfo* t2541_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13561_MI,
	&m13563_MI,
	&m13562_MI,
	&m13564_MI,
};
static TypeInfo* t2541_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4159_TI,
};
static Il2CppInterfaceOffsetPair t2541_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4159_TI, 7},
};
extern TypeInfo t166_TI;
static Il2CppRGCTXData t2541_RGCTXData[3] = 
{
	&m13564_MI/* Method Usage */,
	&t166_TI/* Class Usage */,
	&m20974_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2541_0_0_0;
extern Il2CppType t2541_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2541_GC;
extern TypeInfo t20_TI;
TypeInfo t2541_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2541_MIs, t2541_PIs, t2541_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2541_TI, t2541_ITIs, t2541_VT, &EmptyCustomAttributesCache, &t2541_TI, &t2541_0_0_0, &t2541_1_0_0, t2541_IOs, &t2541_GC, NULL, NULL, NULL, t2541_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2541)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5325_TI;

#include "UnityEngine.UI_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.GraphicRaycaster>
extern MethodInfo m27860_MI;
static PropertyInfo t5325____Count_PropertyInfo = 
{
	&t5325_TI, "Count", &m27860_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27861_MI;
static PropertyInfo t5325____IsReadOnly_PropertyInfo = 
{
	&t5325_TI, "IsReadOnly", &m27861_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5325_PIs[] =
{
	&t5325____Count_PropertyInfo,
	&t5325____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27860_GM;
MethodInfo m27860_MI = 
{
	"get_Count", NULL, &t5325_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27860_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27861_GM;
MethodInfo m27861_MI = 
{
	"get_IsReadOnly", NULL, &t5325_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27861_GM};
extern Il2CppType t166_0_0_0;
extern Il2CppType t166_0_0_0;
static ParameterInfo t5325_m27862_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t166_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27862_GM;
MethodInfo m27862_MI = 
{
	"Add", NULL, &t5325_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5325_m27862_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27862_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27863_GM;
MethodInfo m27863_MI = 
{
	"Clear", NULL, &t5325_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27863_GM};
extern Il2CppType t166_0_0_0;
static ParameterInfo t5325_m27864_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t166_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27864_GM;
MethodInfo m27864_MI = 
{
	"Contains", NULL, &t5325_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5325_m27864_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27864_GM};
extern Il2CppType t3836_0_0_0;
extern Il2CppType t3836_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5325_m27865_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3836_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27865_GM;
MethodInfo m27865_MI = 
{
	"CopyTo", NULL, &t5325_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5325_m27865_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27865_GM};
extern Il2CppType t166_0_0_0;
static ParameterInfo t5325_m27866_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t166_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27866_GM;
MethodInfo m27866_MI = 
{
	"Remove", NULL, &t5325_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5325_m27866_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27866_GM};
static MethodInfo* t5325_MIs[] =
{
	&m27860_MI,
	&m27861_MI,
	&m27862_MI,
	&m27863_MI,
	&m27864_MI,
	&m27865_MI,
	&m27866_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5327_TI;
static TypeInfo* t5325_ITIs[] = 
{
	&t603_TI,
	&t5327_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5325_0_0_0;
extern Il2CppType t5325_1_0_0;
struct t5325;
extern Il2CppGenericClass t5325_GC;
TypeInfo t5325_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5325_MIs, t5325_PIs, NULL, NULL, NULL, NULL, NULL, &t5325_TI, t5325_ITIs, NULL, &EmptyCustomAttributesCache, &t5325_TI, &t5325_0_0_0, &t5325_1_0_0, NULL, &t5325_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.GraphicRaycaster>
extern Il2CppType t4159_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27867_GM;
MethodInfo m27867_MI = 
{
	"GetEnumerator", NULL, &t5327_TI, &t4159_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27867_GM};
static MethodInfo* t5327_MIs[] =
{
	&m27867_MI,
	NULL
};
static TypeInfo* t5327_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5327_0_0_0;
extern Il2CppType t5327_1_0_0;
struct t5327;
extern Il2CppGenericClass t5327_GC;
TypeInfo t5327_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5327_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5327_TI, t5327_ITIs, NULL, &EmptyCustomAttributesCache, &t5327_TI, &t5327_0_0_0, &t5327_1_0_0, NULL, &t5327_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5326_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.GraphicRaycaster>
extern MethodInfo m27868_MI;
extern MethodInfo m27869_MI;
static PropertyInfo t5326____Item_PropertyInfo = 
{
	&t5326_TI, "Item", &m27868_MI, &m27869_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5326_PIs[] =
{
	&t5326____Item_PropertyInfo,
	NULL
};
extern Il2CppType t166_0_0_0;
static ParameterInfo t5326_m27870_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t166_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27870_GM;
MethodInfo m27870_MI = 
{
	"IndexOf", NULL, &t5326_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5326_m27870_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27870_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t166_0_0_0;
static ParameterInfo t5326_m27871_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t166_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27871_GM;
MethodInfo m27871_MI = 
{
	"Insert", NULL, &t5326_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5326_m27871_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27871_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5326_m27872_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27872_GM;
MethodInfo m27872_MI = 
{
	"RemoveAt", NULL, &t5326_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5326_m27872_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27872_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5326_m27868_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t166_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27868_GM;
MethodInfo m27868_MI = 
{
	"get_Item", NULL, &t5326_TI, &t166_0_0_0, RuntimeInvoker_t29_t44, t5326_m27868_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27868_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t166_0_0_0;
static ParameterInfo t5326_m27869_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t166_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27869_GM;
MethodInfo m27869_MI = 
{
	"set_Item", NULL, &t5326_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5326_m27869_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27869_GM};
static MethodInfo* t5326_MIs[] =
{
	&m27870_MI,
	&m27871_MI,
	&m27872_MI,
	&m27868_MI,
	&m27869_MI,
	NULL
};
static TypeInfo* t5326_ITIs[] = 
{
	&t603_TI,
	&t5325_TI,
	&t5327_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5326_0_0_0;
extern Il2CppType t5326_1_0_0;
struct t5326;
extern Il2CppGenericClass t5326_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5326_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5326_MIs, t5326_PIs, NULL, NULL, NULL, NULL, NULL, &t5326_TI, t5326_ITIs, NULL, &t1908__CustomAttributeCache, &t5326_TI, &t5326_0_0_0, &t5326_1_0_0, NULL, &t5326_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2542.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2542_TI;
#include "t2542MD.h"

#include "t41.h"
#include "t557.h"
#include "mscorlib_ArrayTypes.h"
#include "t2543.h"
extern TypeInfo t316_TI;
extern TypeInfo t2543_TI;
extern TypeInfo t21_TI;
#include "t2543MD.h"
extern MethodInfo m13567_MI;
extern MethodInfo m13569_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.GraphicRaycaster>
extern Il2CppType t316_0_0_33;
FieldInfo t2542_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2542_TI, offsetof(t2542, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2542_FIs[] =
{
	&t2542_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t166_0_0_0;
static ParameterInfo t2542_m13565_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t166_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13565_GM;
MethodInfo m13565_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2542_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2542_m13565_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13565_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2542_m13566_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13566_GM;
MethodInfo m13566_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2542_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2542_m13566_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13566_GM};
static MethodInfo* t2542_MIs[] =
{
	&m13565_MI,
	&m13566_MI,
	NULL
};
extern MethodInfo m13566_MI;
extern MethodInfo m13570_MI;
static MethodInfo* t2542_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13566_MI,
	&m13570_MI,
};
extern Il2CppType t2544_0_0_0;
extern TypeInfo t2544_TI;
extern MethodInfo m20984_MI;
extern TypeInfo t166_TI;
extern MethodInfo m13572_MI;
extern TypeInfo t166_TI;
static Il2CppRGCTXData t2542_RGCTXData[8] = 
{
	&t2544_0_0_0/* Type Usage */,
	&t2544_TI/* Class Usage */,
	&m20984_MI/* Method Usage */,
	&t166_TI/* Class Usage */,
	&m13572_MI/* Method Usage */,
	&m13567_MI/* Method Usage */,
	&t166_TI/* Class Usage */,
	&m13569_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2542_0_0_0;
extern Il2CppType t2542_1_0_0;
struct t2542;
extern Il2CppGenericClass t2542_GC;
TypeInfo t2542_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2542_MIs, NULL, t2542_FIs, NULL, &t2543_TI, NULL, NULL, &t2542_TI, NULL, t2542_VT, &EmptyCustomAttributesCache, &t2542_TI, &t2542_0_0_0, &t2542_1_0_0, NULL, &t2542_GC, NULL, NULL, NULL, t2542_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2542), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2544.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
extern TypeInfo t2544_TI;
extern TypeInfo t42_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t2544MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m20984(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.GraphicRaycaster>
extern Il2CppType t2544_0_0_1;
FieldInfo t2543_f0_FieldInfo = 
{
	"Delegate", &t2544_0_0_1, &t2543_TI, offsetof(t2543, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2543_FIs[] =
{
	&t2543_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2543_m13567_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13567_GM;
MethodInfo m13567_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2543_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2543_m13567_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13567_GM};
extern Il2CppType t2544_0_0_0;
static ParameterInfo t2543_m13568_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2544_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13568_GM;
MethodInfo m13568_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2543_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2543_m13568_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13568_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2543_m13569_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13569_GM;
MethodInfo m13569_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2543_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2543_m13569_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13569_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2543_m13570_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13570_GM;
MethodInfo m13570_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2543_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2543_m13570_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13570_GM};
static MethodInfo* t2543_MIs[] =
{
	&m13567_MI,
	&m13568_MI,
	&m13569_MI,
	&m13570_MI,
	NULL
};
static MethodInfo* t2543_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13569_MI,
	&m13570_MI,
};
extern TypeInfo t2544_TI;
extern TypeInfo t166_TI;
static Il2CppRGCTXData t2543_RGCTXData[5] = 
{
	&t2544_0_0_0/* Type Usage */,
	&t2544_TI/* Class Usage */,
	&m20984_MI/* Method Usage */,
	&t166_TI/* Class Usage */,
	&m13572_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2543_0_0_0;
extern Il2CppType t2543_1_0_0;
extern TypeInfo t556_TI;
struct t2543;
extern Il2CppGenericClass t2543_GC;
TypeInfo t2543_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2543_MIs, NULL, t2543_FIs, NULL, &t556_TI, NULL, NULL, &t2543_TI, NULL, t2543_VT, &EmptyCustomAttributesCache, &t2543_TI, &t2543_0_0_0, &t2543_1_0_0, NULL, &t2543_GC, NULL, NULL, NULL, t2543_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2543), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.GraphicRaycaster>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2544_m13571_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13571_GM;
MethodInfo m13571_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2544_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2544_m13571_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13571_GM};
extern Il2CppType t166_0_0_0;
static ParameterInfo t2544_m13572_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t166_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13572_GM;
MethodInfo m13572_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2544_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2544_m13572_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13572_GM};
extern Il2CppType t166_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2544_m13573_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t166_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13573_GM;
MethodInfo m13573_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2544_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2544_m13573_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13573_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2544_m13574_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13574_GM;
MethodInfo m13574_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2544_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2544_m13574_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13574_GM};
static MethodInfo* t2544_MIs[] =
{
	&m13571_MI,
	&m13572_MI,
	&m13573_MI,
	&m13574_MI,
	NULL
};
extern MethodInfo m13573_MI;
extern MethodInfo m13574_MI;
static MethodInfo* t2544_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13572_MI,
	&m13573_MI,
	&m13574_MI,
};
static Il2CppInterfaceOffsetPair t2544_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2544_1_0_0;
struct t2544;
extern Il2CppGenericClass t2544_GC;
TypeInfo t2544_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2544_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2544_TI, NULL, t2544_VT, &EmptyCustomAttributesCache, &t2544_TI, &t2544_0_0_0, &t2544_1_0_0, t2544_IOs, &t2544_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2544), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t167.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t167_TI;
#include "t167MD.h"

#include "t155.h"
#include "t2550.h"
#include "t2548.h"
#include "t2549.h"
#include "t338.h"
#include "t2554.h"
#include "t168.h"
extern TypeInfo t155_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2545_TI;
extern TypeInfo t2550_TI;
extern TypeInfo t40_TI;
extern TypeInfo t363_TI;
extern TypeInfo t2547_TI;
extern TypeInfo t2546_TI;
extern TypeInfo t2548_TI;
extern TypeInfo t338_TI;
extern TypeInfo t2549_TI;
extern TypeInfo t2554_TI;
#include "t915MD.h"
#include "t29MD.h"
#include "t602MD.h"
#include "t2548MD.h"
#include "t338MD.h"
#include "t2549MD.h"
#include "t2550MD.h"
#include "t2554MD.h"
extern MethodInfo m1618_MI;
extern MethodInfo m13617_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m20986_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m13605_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m13602_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m1629_MI;
extern MethodInfo m13597_MI;
extern MethodInfo m13603_MI;
extern MethodInfo m13606_MI;
extern MethodInfo m13608_MI;
extern MethodInfo m13592_MI;
extern MethodInfo m13615_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m13616_MI;
extern MethodInfo m1630_MI;
extern MethodInfo m27756_MI;
extern MethodInfo m27758_MI;
extern MethodInfo m27759_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m13607_MI;
extern MethodInfo m13593_MI;
extern MethodInfo m13594_MI;
extern MethodInfo m13624_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m20988_MI;
extern MethodInfo m13600_MI;
extern MethodInfo m13601_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m13699_MI;
extern MethodInfo m13618_MI;
extern MethodInfo m13604_MI;
extern MethodInfo m13610_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m13705_MI;
extern MethodInfo m20990_MI;
extern MethodInfo m20998_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m20986(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2552.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m20988(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m20990(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m20998(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2550  m13602 (t167 * __this, MethodInfo* method){
	{
		t2550  L_0 = {0};
		m13618(&L_0, __this, &m13618_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
extern Il2CppType t44_0_0_32849;
FieldInfo t167_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t167_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2545_0_0_1;
FieldInfo t167_f1_FieldInfo = 
{
	"_items", &t2545_0_0_1, &t167_TI, offsetof(t167, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t167_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t167_TI, offsetof(t167, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t167_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t167_TI, offsetof(t167, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2545_0_0_49;
FieldInfo t167_f4_FieldInfo = 
{
	"EmptyArray", &t2545_0_0_49, &t167_TI, offsetof(t167_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t167_FIs[] =
{
	&t167_f0_FieldInfo,
	&t167_f1_FieldInfo,
	&t167_f2_FieldInfo,
	&t167_f3_FieldInfo,
	&t167_f4_FieldInfo,
	NULL
};
static const int32_t t167_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t167_f0_DefaultValue = 
{
	&t167_f0_FieldInfo, { (char*)&t167_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t167_FDVs[] = 
{
	&t167_f0_DefaultValue,
	NULL
};
extern MethodInfo m13585_MI;
static PropertyInfo t167____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t167_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m13585_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13586_MI;
static PropertyInfo t167____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t167_TI, "System.Collections.ICollection.IsSynchronized", &m13586_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13587_MI;
static PropertyInfo t167____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t167_TI, "System.Collections.ICollection.SyncRoot", &m13587_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13588_MI;
static PropertyInfo t167____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t167_TI, "System.Collections.IList.IsFixedSize", &m13588_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13589_MI;
static PropertyInfo t167____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t167_TI, "System.Collections.IList.IsReadOnly", &m13589_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13590_MI;
extern MethodInfo m13591_MI;
static PropertyInfo t167____System_Collections_IList_Item_PropertyInfo = 
{
	&t167_TI, "System.Collections.IList.Item", &m13590_MI, &m13591_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t167____Capacity_PropertyInfo = 
{
	&t167_TI, "Capacity", &m13615_MI, &m13616_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1626_MI;
static PropertyInfo t167____Count_PropertyInfo = 
{
	&t167_TI, "Count", &m1626_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t167____Item_PropertyInfo = 
{
	&t167_TI, "Item", &m1618_MI, &m13617_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t167_PIs[] =
{
	&t167____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t167____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t167____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t167____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t167____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t167____System_Collections_IList_Item_PropertyInfo,
	&t167____Capacity_PropertyInfo,
	&t167____Count_PropertyInfo,
	&t167____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1610_GM;
MethodInfo m1610_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1610_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t167_m13575_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13575_GM;
MethodInfo m13575_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t167_m13575_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13575_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13576_GM;
MethodInfo m13576_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13576_GM};
extern Il2CppType t2546_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13577_GM;
MethodInfo m13577_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t167_TI, &t2546_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13577_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t167_m13578_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13578_GM;
MethodInfo m13578_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t167_m13578_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13578_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13579_GM;
MethodInfo m13579_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t167_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13579_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t167_m13580_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13580_GM;
MethodInfo m13580_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t167_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t167_m13580_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13580_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t167_m13581_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13581_GM;
MethodInfo m13581_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t167_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t167_m13581_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13581_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t167_m13582_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13582_GM;
MethodInfo m13582_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t167_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t167_m13582_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13582_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t167_m13583_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13583_GM;
MethodInfo m13583_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t167_m13583_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13583_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t167_m13584_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13584_GM;
MethodInfo m13584_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t167_m13584_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13584_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13585_GM;
MethodInfo m13585_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t167_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13585_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13586_GM;
MethodInfo m13586_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t167_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13586_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13587_GM;
MethodInfo m13587_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t167_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13587_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13588_GM;
MethodInfo m13588_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t167_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13588_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13589_GM;
MethodInfo m13589_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t167_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13589_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t167_m13590_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13590_GM;
MethodInfo m13590_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t167_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t167_m13590_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13590_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t167_m13591_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13591_GM;
MethodInfo m13591_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t167_m13591_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13591_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t167_m1629_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1629_GM;
MethodInfo m1629_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t167_m1629_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1629_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t167_m13592_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13592_GM;
MethodInfo m13592_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t167_m13592_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13592_GM};
extern Il2CppType t363_0_0_0;
extern Il2CppType t363_0_0_0;
static ParameterInfo t167_m13593_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t363_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13593_GM;
MethodInfo m13593_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t167_m13593_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13593_GM};
extern Il2CppType t2547_0_0_0;
extern Il2CppType t2547_0_0_0;
static ParameterInfo t167_m13594_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2547_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13594_GM;
MethodInfo m13594_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t167_m13594_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13594_GM};
extern Il2CppType t2547_0_0_0;
static ParameterInfo t167_m13595_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2547_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13595_GM;
MethodInfo m13595_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t167_m13595_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13595_GM};
extern Il2CppType t2548_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13596_GM;
MethodInfo m13596_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t167_TI, &t2548_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13596_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1617_GM;
MethodInfo m1617_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1617_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t167_m13597_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13597_GM;
MethodInfo m13597_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t167_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t167_m13597_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13597_GM};
extern Il2CppType t2545_0_0_0;
extern Il2CppType t2545_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t167_m13598_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2545_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13598_GM;
MethodInfo m13598_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t167_m13598_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13598_GM};
extern Il2CppType t2549_0_0_0;
extern Il2CppType t2549_0_0_0;
static ParameterInfo t167_m13599_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2549_0_0_0},
};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13599_GM;
MethodInfo m13599_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t167_TI, &t155_0_0_0, RuntimeInvoker_t29_t29, t167_m13599_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13599_GM};
extern Il2CppType t2549_0_0_0;
static ParameterInfo t167_m13600_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2549_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13600_GM;
MethodInfo m13600_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t167_m13600_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13600_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2549_0_0_0;
static ParameterInfo t167_m13601_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2549_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13601_GM;
MethodInfo m13601_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t167_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t167_m13601_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13601_GM};
extern Il2CppType t2550_0_0_0;
extern void* RuntimeInvoker_t2550 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13602_GM;
MethodInfo m13602_MI = 
{
	"GetEnumerator", (methodPointerType)&m13602, &t167_TI, &t2550_0_0_0, RuntimeInvoker_t2550, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13602_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t167_m13603_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13603_GM;
MethodInfo m13603_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t167_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t167_m13603_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13603_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t167_m13604_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13604_GM;
MethodInfo m13604_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t167_m13604_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13604_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t167_m13605_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13605_GM;
MethodInfo m13605_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t167_m13605_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13605_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t167_m13606_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13606_GM;
MethodInfo m13606_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t167_m13606_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13606_GM};
extern Il2CppType t2547_0_0_0;
static ParameterInfo t167_m13607_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2547_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13607_GM;
MethodInfo m13607_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t167_m13607_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13607_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t167_m13608_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13608_GM;
MethodInfo m13608_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t167_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t167_m13608_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13608_GM};
extern Il2CppType t2549_0_0_0;
static ParameterInfo t167_m13609_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2549_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13609_GM;
MethodInfo m13609_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t167_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t167_m13609_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13609_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t167_m13610_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13610_GM;
MethodInfo m13610_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t167_m13610_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13610_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13611_GM;
MethodInfo m13611_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13611_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13612_GM;
MethodInfo m13612_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13612_GM};
extern Il2CppType t168_0_0_0;
extern Il2CppType t168_0_0_0;
static ParameterInfo t167_m1632_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t168_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1632_GM;
MethodInfo m1632_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t167_m1632_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1632_GM};
extern Il2CppType t2545_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13613_GM;
MethodInfo m13613_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t167_TI, &t2545_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13613_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13614_GM;
MethodInfo m13614_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13614_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13615_GM;
MethodInfo m13615_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t167_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13615_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t167_m13616_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13616_GM;
MethodInfo m13616_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t167_m13616_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13616_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1626_GM;
MethodInfo m1626_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t167_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1626_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t167_m1618_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1618_GM;
MethodInfo m1618_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t167_TI, &t155_0_0_0, RuntimeInvoker_t29_t44, t167_m1618_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1618_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t167_m13617_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13617_GM;
MethodInfo m13617_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t167_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t167_m13617_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13617_GM};
static MethodInfo* t167_MIs[] =
{
	&m1610_MI,
	&m13575_MI,
	&m13576_MI,
	&m13577_MI,
	&m13578_MI,
	&m13579_MI,
	&m13580_MI,
	&m13581_MI,
	&m13582_MI,
	&m13583_MI,
	&m13584_MI,
	&m13585_MI,
	&m13586_MI,
	&m13587_MI,
	&m13588_MI,
	&m13589_MI,
	&m13590_MI,
	&m13591_MI,
	&m1629_MI,
	&m13592_MI,
	&m13593_MI,
	&m13594_MI,
	&m13595_MI,
	&m13596_MI,
	&m1617_MI,
	&m13597_MI,
	&m13598_MI,
	&m13599_MI,
	&m13600_MI,
	&m13601_MI,
	&m13602_MI,
	&m13603_MI,
	&m13604_MI,
	&m13605_MI,
	&m13606_MI,
	&m13607_MI,
	&m13608_MI,
	&m13609_MI,
	&m13610_MI,
	&m13611_MI,
	&m13612_MI,
	&m1632_MI,
	&m13613_MI,
	&m13614_MI,
	&m13615_MI,
	&m13616_MI,
	&m1626_MI,
	&m1618_MI,
	&m13617_MI,
	NULL
};
extern MethodInfo m13579_MI;
extern MethodInfo m13578_MI;
extern MethodInfo m13580_MI;
extern MethodInfo m1617_MI;
extern MethodInfo m13581_MI;
extern MethodInfo m13582_MI;
extern MethodInfo m13583_MI;
extern MethodInfo m13584_MI;
extern MethodInfo m13598_MI;
extern MethodInfo m13577_MI;
static MethodInfo* t167_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13579_MI,
	&m1626_MI,
	&m13586_MI,
	&m13587_MI,
	&m13578_MI,
	&m13588_MI,
	&m13589_MI,
	&m13590_MI,
	&m13591_MI,
	&m13580_MI,
	&m1617_MI,
	&m13581_MI,
	&m13582_MI,
	&m13583_MI,
	&m13584_MI,
	&m13610_MI,
	&m1626_MI,
	&m13585_MI,
	&m1629_MI,
	&m1617_MI,
	&m13597_MI,
	&m13598_MI,
	&m13608_MI,
	&m13577_MI,
	&m13603_MI,
	&m13606_MI,
	&m13610_MI,
	&m1618_MI,
	&m13617_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
extern TypeInfo t171_TI;
static TypeInfo* t167_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t363_TI,
	&t2547_TI,
	&t171_TI,
};
static Il2CppInterfaceOffsetPair t167_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t363_TI, 20},
	{ &t2547_TI, 27},
	{ &t171_TI, 28},
};
extern TypeInfo t167_TI;
extern TypeInfo t2545_TI;
extern TypeInfo t2550_TI;
extern TypeInfo t155_TI;
extern TypeInfo t363_TI;
extern TypeInfo t2548_TI;
static Il2CppRGCTXData t167_RGCTXData[37] = 
{
	&t167_TI/* Static Usage */,
	&t2545_TI/* Array Usage */,
	&m13602_MI/* Method Usage */,
	&t2550_TI/* Class Usage */,
	&t155_TI/* Class Usage */,
	&m1629_MI/* Method Usage */,
	&m13597_MI/* Method Usage */,
	&m13603_MI/* Method Usage */,
	&m13605_MI/* Method Usage */,
	&m13606_MI/* Method Usage */,
	&m13608_MI/* Method Usage */,
	&m1618_MI/* Method Usage */,
	&m13617_MI/* Method Usage */,
	&m13592_MI/* Method Usage */,
	&m13615_MI/* Method Usage */,
	&m13616_MI/* Method Usage */,
	&m1630_MI/* Method Usage */,
	&m27756_MI/* Method Usage */,
	&m27758_MI/* Method Usage */,
	&m27759_MI/* Method Usage */,
	&m13607_MI/* Method Usage */,
	&t363_TI/* Class Usage */,
	&m13593_MI/* Method Usage */,
	&m13594_MI/* Method Usage */,
	&t2548_TI/* Class Usage */,
	&m13624_MI/* Method Usage */,
	&m20988_MI/* Method Usage */,
	&m13600_MI/* Method Usage */,
	&m13601_MI/* Method Usage */,
	&m13699_MI/* Method Usage */,
	&m13618_MI/* Method Usage */,
	&m13604_MI/* Method Usage */,
	&m13610_MI/* Method Usage */,
	&m13705_MI/* Method Usage */,
	&m20990_MI/* Method Usage */,
	&m20998_MI/* Method Usage */,
	&m20986_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t167_0_0_0;
extern Il2CppType t167_1_0_0;
struct t167;
extern Il2CppGenericClass t167_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t167_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t167_MIs, t167_PIs, t167_FIs, NULL, &t29_TI, NULL, NULL, &t167_TI, t167_ITIs, t167_VT, &t1261__CustomAttributeCache, &t167_TI, &t167_0_0_0, &t167_1_0_0, t167_IOs, &t167_GC, NULL, t167_FDVs, NULL, t167_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t167), 0, -1, sizeof(t167_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m13621_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Graphic>
extern Il2CppType t167_0_0_1;
FieldInfo t2550_f0_FieldInfo = 
{
	"l", &t167_0_0_1, &t2550_TI, offsetof(t2550, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2550_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2550_TI, offsetof(t2550, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2550_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2550_TI, offsetof(t2550, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t155_0_0_1;
FieldInfo t2550_f3_FieldInfo = 
{
	"current", &t155_0_0_1, &t2550_TI, offsetof(t2550, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2550_FIs[] =
{
	&t2550_f0_FieldInfo,
	&t2550_f1_FieldInfo,
	&t2550_f2_FieldInfo,
	&t2550_f3_FieldInfo,
	NULL
};
extern MethodInfo m13619_MI;
static PropertyInfo t2550____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2550_TI, "System.Collections.IEnumerator.Current", &m13619_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13623_MI;
static PropertyInfo t2550____Current_PropertyInfo = 
{
	&t2550_TI, "Current", &m13623_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2550_PIs[] =
{
	&t2550____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2550____Current_PropertyInfo,
	NULL
};
extern Il2CppType t167_0_0_0;
static ParameterInfo t2550_m13618_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t167_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13618_GM;
MethodInfo m13618_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2550_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2550_m13618_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13618_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13619_GM;
MethodInfo m13619_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2550_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13619_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13620_GM;
MethodInfo m13620_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2550_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13620_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13621_GM;
MethodInfo m13621_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2550_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13621_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13622_GM;
MethodInfo m13622_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2550_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13622_GM};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13623_GM;
MethodInfo m13623_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2550_TI, &t155_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13623_GM};
static MethodInfo* t2550_MIs[] =
{
	&m13618_MI,
	&m13619_MI,
	&m13620_MI,
	&m13621_MI,
	&m13622_MI,
	&m13623_MI,
	NULL
};
extern MethodInfo m13622_MI;
extern MethodInfo m13620_MI;
static MethodInfo* t2550_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13619_MI,
	&m13622_MI,
	&m13620_MI,
	&m13623_MI,
};
static TypeInfo* t2550_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2546_TI,
};
static Il2CppInterfaceOffsetPair t2550_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2546_TI, 7},
};
extern TypeInfo t155_TI;
extern TypeInfo t2550_TI;
static Il2CppRGCTXData t2550_RGCTXData[3] = 
{
	&m13621_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
	&t2550_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2550_0_0_0;
extern Il2CppType t2550_1_0_0;
extern Il2CppGenericClass t2550_GC;
extern TypeInfo t1261_TI;
TypeInfo t2550_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2550_MIs, t2550_PIs, t2550_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2550_TI, t2550_ITIs, t2550_VT, &EmptyCustomAttributesCache, &t2550_TI, &t2550_0_0_0, &t2550_1_0_0, t2550_IOs, &t2550_GC, NULL, NULL, NULL, t2550_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2550)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t2551MD.h"
extern MethodInfo m13653_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m1627_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m13685_MI;
extern MethodInfo m27755_MI;
extern MethodInfo m27761_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.Graphic>
extern Il2CppType t171_0_0_1;
FieldInfo t2548_f0_FieldInfo = 
{
	"list", &t171_0_0_1, &t2548_TI, offsetof(t2548, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2548_FIs[] =
{
	&t2548_f0_FieldInfo,
	NULL
};
extern MethodInfo m13630_MI;
extern MethodInfo m13631_MI;
static PropertyInfo t2548____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2548_TI, "System.Collections.Generic.IList<T>.Item", &m13630_MI, &m13631_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13632_MI;
static PropertyInfo t2548____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2548_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m13632_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13642_MI;
static PropertyInfo t2548____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2548_TI, "System.Collections.ICollection.IsSynchronized", &m13642_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13643_MI;
static PropertyInfo t2548____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2548_TI, "System.Collections.ICollection.SyncRoot", &m13643_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13644_MI;
static PropertyInfo t2548____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2548_TI, "System.Collections.IList.IsFixedSize", &m13644_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13645_MI;
static PropertyInfo t2548____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2548_TI, "System.Collections.IList.IsReadOnly", &m13645_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13646_MI;
extern MethodInfo m13647_MI;
static PropertyInfo t2548____System_Collections_IList_Item_PropertyInfo = 
{
	&t2548_TI, "System.Collections.IList.Item", &m13646_MI, &m13647_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13652_MI;
static PropertyInfo t2548____Count_PropertyInfo = 
{
	&t2548_TI, "Count", &m13652_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2548____Item_PropertyInfo = 
{
	&t2548_TI, "Item", &m13653_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2548_PIs[] =
{
	&t2548____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2548____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2548____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2548____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2548____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2548____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2548____System_Collections_IList_Item_PropertyInfo,
	&t2548____Count_PropertyInfo,
	&t2548____Item_PropertyInfo,
	NULL
};
extern Il2CppType t171_0_0_0;
extern Il2CppType t171_0_0_0;
static ParameterInfo t2548_m13624_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t171_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13624_GM;
MethodInfo m13624_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2548_m13624_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13624_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2548_m13625_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13625_GM;
MethodInfo m13625_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2548_m13625_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13625_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13626_GM;
MethodInfo m13626_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13626_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t2548_m13627_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13627_GM;
MethodInfo m13627_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2548_m13627_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13627_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2548_m13628_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13628_GM;
MethodInfo m13628_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2548_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2548_m13628_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13628_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2548_m13629_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13629_GM;
MethodInfo m13629_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2548_m13629_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13629_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2548_m13630_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13630_GM;
MethodInfo m13630_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2548_TI, &t155_0_0_0, RuntimeInvoker_t29_t44, t2548_m13630_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13630_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t2548_m13631_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13631_GM;
MethodInfo m13631_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2548_m13631_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13631_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13632_GM;
MethodInfo m13632_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2548_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13632_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2548_m13633_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13633_GM;
MethodInfo m13633_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2548_m13633_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13633_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13634_GM;
MethodInfo m13634_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2548_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13634_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2548_m13635_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13635_GM;
MethodInfo m13635_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2548_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2548_m13635_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13635_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13636_GM;
MethodInfo m13636_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13636_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2548_m13637_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13637_GM;
MethodInfo m13637_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2548_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2548_m13637_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13637_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2548_m13638_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13638_GM;
MethodInfo m13638_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2548_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2548_m13638_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13638_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2548_m13639_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13639_GM;
MethodInfo m13639_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2548_m13639_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13639_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2548_m13640_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13640_GM;
MethodInfo m13640_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2548_m13640_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13640_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2548_m13641_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13641_GM;
MethodInfo m13641_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2548_m13641_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13641_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13642_GM;
MethodInfo m13642_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2548_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13642_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13643_GM;
MethodInfo m13643_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2548_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13643_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13644_GM;
MethodInfo m13644_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2548_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13644_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13645_GM;
MethodInfo m13645_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2548_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13645_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2548_m13646_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13646_GM;
MethodInfo m13646_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2548_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2548_m13646_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13646_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2548_m13647_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13647_GM;
MethodInfo m13647_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2548_m13647_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13647_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2548_m13648_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13648_GM;
MethodInfo m13648_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2548_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2548_m13648_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13648_GM};
extern Il2CppType t2545_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2548_m13649_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2545_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13649_GM;
MethodInfo m13649_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2548_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2548_m13649_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13649_GM};
extern Il2CppType t2546_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13650_GM;
MethodInfo m13650_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2548_TI, &t2546_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13650_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2548_m13651_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13651_GM;
MethodInfo m13651_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2548_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2548_m13651_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13651_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13652_GM;
MethodInfo m13652_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2548_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13652_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2548_m13653_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13653_GM;
MethodInfo m13653_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2548_TI, &t155_0_0_0, RuntimeInvoker_t29_t44, t2548_m13653_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13653_GM};
static MethodInfo* t2548_MIs[] =
{
	&m13624_MI,
	&m13625_MI,
	&m13626_MI,
	&m13627_MI,
	&m13628_MI,
	&m13629_MI,
	&m13630_MI,
	&m13631_MI,
	&m13632_MI,
	&m13633_MI,
	&m13634_MI,
	&m13635_MI,
	&m13636_MI,
	&m13637_MI,
	&m13638_MI,
	&m13639_MI,
	&m13640_MI,
	&m13641_MI,
	&m13642_MI,
	&m13643_MI,
	&m13644_MI,
	&m13645_MI,
	&m13646_MI,
	&m13647_MI,
	&m13648_MI,
	&m13649_MI,
	&m13650_MI,
	&m13651_MI,
	&m13652_MI,
	&m13653_MI,
	NULL
};
extern MethodInfo m13634_MI;
extern MethodInfo m13633_MI;
extern MethodInfo m13635_MI;
extern MethodInfo m13636_MI;
extern MethodInfo m13637_MI;
extern MethodInfo m13638_MI;
extern MethodInfo m13639_MI;
extern MethodInfo m13640_MI;
extern MethodInfo m13641_MI;
extern MethodInfo m13625_MI;
extern MethodInfo m13626_MI;
extern MethodInfo m13648_MI;
extern MethodInfo m13649_MI;
extern MethodInfo m13628_MI;
extern MethodInfo m13651_MI;
extern MethodInfo m13627_MI;
extern MethodInfo m13629_MI;
extern MethodInfo m13650_MI;
static MethodInfo* t2548_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13634_MI,
	&m13652_MI,
	&m13642_MI,
	&m13643_MI,
	&m13633_MI,
	&m13644_MI,
	&m13645_MI,
	&m13646_MI,
	&m13647_MI,
	&m13635_MI,
	&m13636_MI,
	&m13637_MI,
	&m13638_MI,
	&m13639_MI,
	&m13640_MI,
	&m13641_MI,
	&m13652_MI,
	&m13632_MI,
	&m13625_MI,
	&m13626_MI,
	&m13648_MI,
	&m13649_MI,
	&m13628_MI,
	&m13651_MI,
	&m13627_MI,
	&m13629_MI,
	&m13630_MI,
	&m13631_MI,
	&m13650_MI,
	&m13653_MI,
};
static TypeInfo* t2548_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t363_TI,
	&t171_TI,
	&t2547_TI,
};
static Il2CppInterfaceOffsetPair t2548_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t363_TI, 20},
	{ &t171_TI, 27},
	{ &t2547_TI, 32},
};
extern TypeInfo t155_TI;
static Il2CppRGCTXData t2548_RGCTXData[9] = 
{
	&m13653_MI/* Method Usage */,
	&m13685_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
	&m27755_MI/* Method Usage */,
	&m27761_MI/* Method Usage */,
	&m1627_MI/* Method Usage */,
	&m27756_MI/* Method Usage */,
	&m27758_MI/* Method Usage */,
	&m1630_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2548_0_0_0;
extern Il2CppType t2548_1_0_0;
struct t2548;
extern Il2CppGenericClass t2548_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2548_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2548_MIs, t2548_PIs, t2548_FIs, NULL, &t29_TI, NULL, NULL, &t2548_TI, t2548_ITIs, t2548_VT, &t1263__CustomAttributeCache, &t2548_TI, &t2548_0_0_0, &t2548_1_0_0, t2548_IOs, &t2548_GC, NULL, NULL, NULL, t2548_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2548), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2551.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2551_TI;

extern MethodInfo m27752_MI;
extern MethodInfo m13688_MI;
extern MethodInfo m13689_MI;
extern MethodInfo m13686_MI;
extern MethodInfo m13684_MI;
extern MethodInfo m1610_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m13677_MI;
extern MethodInfo m13687_MI;
extern MethodInfo m13675_MI;
extern MethodInfo m13680_MI;
extern MethodInfo m13671_MI;
extern MethodInfo m27754_MI;
extern MethodInfo m27762_MI;
extern MethodInfo m27763_MI;
extern MethodInfo m27760_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.UI.Graphic>
extern Il2CppType t171_0_0_1;
FieldInfo t2551_f0_FieldInfo = 
{
	"list", &t171_0_0_1, &t2551_TI, offsetof(t2551, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2551_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2551_TI, offsetof(t2551, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2551_FIs[] =
{
	&t2551_f0_FieldInfo,
	&t2551_f1_FieldInfo,
	NULL
};
extern MethodInfo m13655_MI;
static PropertyInfo t2551____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2551_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m13655_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13663_MI;
static PropertyInfo t2551____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2551_TI, "System.Collections.ICollection.IsSynchronized", &m13663_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13664_MI;
static PropertyInfo t2551____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2551_TI, "System.Collections.ICollection.SyncRoot", &m13664_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13665_MI;
static PropertyInfo t2551____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2551_TI, "System.Collections.IList.IsFixedSize", &m13665_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13666_MI;
static PropertyInfo t2551____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2551_TI, "System.Collections.IList.IsReadOnly", &m13666_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13667_MI;
extern MethodInfo m13668_MI;
static PropertyInfo t2551____System_Collections_IList_Item_PropertyInfo = 
{
	&t2551_TI, "System.Collections.IList.Item", &m13667_MI, &m13668_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13681_MI;
static PropertyInfo t2551____Count_PropertyInfo = 
{
	&t2551_TI, "Count", &m13681_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13682_MI;
extern MethodInfo m13683_MI;
static PropertyInfo t2551____Item_PropertyInfo = 
{
	&t2551_TI, "Item", &m13682_MI, &m13683_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2551_PIs[] =
{
	&t2551____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2551____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2551____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2551____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2551____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2551____System_Collections_IList_Item_PropertyInfo,
	&t2551____Count_PropertyInfo,
	&t2551____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13654_GM;
MethodInfo m13654_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13654_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13655_GM;
MethodInfo m13655_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2551_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13655_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2551_m13656_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13656_GM;
MethodInfo m13656_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2551_m13656_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13656_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13657_GM;
MethodInfo m13657_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2551_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13657_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2551_m13658_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13658_GM;
MethodInfo m13658_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2551_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2551_m13658_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13658_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2551_m13659_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13659_GM;
MethodInfo m13659_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2551_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2551_m13659_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13659_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2551_m13660_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13660_GM;
MethodInfo m13660_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2551_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2551_m13660_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13660_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2551_m13661_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13661_GM;
MethodInfo m13661_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2551_m13661_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13661_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2551_m13662_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13662_GM;
MethodInfo m13662_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2551_m13662_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13662_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13663_GM;
MethodInfo m13663_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2551_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13663_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13664_GM;
MethodInfo m13664_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2551_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13664_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13665_GM;
MethodInfo m13665_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2551_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13665_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13666_GM;
MethodInfo m13666_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2551_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13666_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2551_m13667_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13667_GM;
MethodInfo m13667_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2551_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2551_m13667_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13667_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2551_m13668_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13668_GM;
MethodInfo m13668_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2551_m13668_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13668_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2551_m13669_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13669_GM;
MethodInfo m13669_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2551_m13669_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13669_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13670_GM;
MethodInfo m13670_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13670_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13671_GM;
MethodInfo m13671_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13671_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2551_m13672_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13672_GM;
MethodInfo m13672_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2551_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2551_m13672_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13672_GM};
extern Il2CppType t2545_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2551_m13673_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2545_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13673_GM;
MethodInfo m13673_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2551_m13673_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13673_GM};
extern Il2CppType t2546_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13674_GM;
MethodInfo m13674_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2551_TI, &t2546_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13674_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2551_m13675_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13675_GM;
MethodInfo m13675_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2551_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2551_m13675_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13675_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t2551_m13676_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13676_GM;
MethodInfo m13676_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2551_m13676_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13676_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t2551_m13677_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13677_GM;
MethodInfo m13677_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2551_m13677_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13677_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2551_m13678_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13678_GM;
MethodInfo m13678_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2551_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2551_m13678_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13678_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2551_m13679_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13679_GM;
MethodInfo m13679_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2551_m13679_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13679_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2551_m13680_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13680_GM;
MethodInfo m13680_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2551_m13680_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13680_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13681_GM;
MethodInfo m13681_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2551_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13681_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2551_m13682_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13682_GM;
MethodInfo m13682_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2551_TI, &t155_0_0_0, RuntimeInvoker_t29_t44, t2551_m13682_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13682_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t2551_m13683_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13683_GM;
MethodInfo m13683_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2551_m13683_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13683_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t2551_m13684_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13684_GM;
MethodInfo m13684_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2551_m13684_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13684_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2551_m13685_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13685_GM;
MethodInfo m13685_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2551_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2551_m13685_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13685_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2551_m13686_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13686_GM;
MethodInfo m13686_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2551_TI, &t155_0_0_0, RuntimeInvoker_t29_t29, t2551_m13686_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13686_GM};
extern Il2CppType t171_0_0_0;
static ParameterInfo t2551_m13687_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t171_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13687_GM;
MethodInfo m13687_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2551_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2551_m13687_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13687_GM};
extern Il2CppType t171_0_0_0;
static ParameterInfo t2551_m13688_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t171_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13688_GM;
MethodInfo m13688_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2551_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2551_m13688_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13688_GM};
extern Il2CppType t171_0_0_0;
static ParameterInfo t2551_m13689_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t171_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13689_GM;
MethodInfo m13689_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2551_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2551_m13689_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13689_GM};
static MethodInfo* t2551_MIs[] =
{
	&m13654_MI,
	&m13655_MI,
	&m13656_MI,
	&m13657_MI,
	&m13658_MI,
	&m13659_MI,
	&m13660_MI,
	&m13661_MI,
	&m13662_MI,
	&m13663_MI,
	&m13664_MI,
	&m13665_MI,
	&m13666_MI,
	&m13667_MI,
	&m13668_MI,
	&m13669_MI,
	&m13670_MI,
	&m13671_MI,
	&m13672_MI,
	&m13673_MI,
	&m13674_MI,
	&m13675_MI,
	&m13676_MI,
	&m13677_MI,
	&m13678_MI,
	&m13679_MI,
	&m13680_MI,
	&m13681_MI,
	&m13682_MI,
	&m13683_MI,
	&m13684_MI,
	&m13685_MI,
	&m13686_MI,
	&m13687_MI,
	&m13688_MI,
	&m13689_MI,
	NULL
};
extern MethodInfo m13657_MI;
extern MethodInfo m13656_MI;
extern MethodInfo m13658_MI;
extern MethodInfo m13670_MI;
extern MethodInfo m13659_MI;
extern MethodInfo m13660_MI;
extern MethodInfo m13661_MI;
extern MethodInfo m13662_MI;
extern MethodInfo m13679_MI;
extern MethodInfo m13669_MI;
extern MethodInfo m13672_MI;
extern MethodInfo m13673_MI;
extern MethodInfo m13678_MI;
extern MethodInfo m13676_MI;
extern MethodInfo m13674_MI;
static MethodInfo* t2551_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13657_MI,
	&m13681_MI,
	&m13663_MI,
	&m13664_MI,
	&m13656_MI,
	&m13665_MI,
	&m13666_MI,
	&m13667_MI,
	&m13668_MI,
	&m13658_MI,
	&m13670_MI,
	&m13659_MI,
	&m13660_MI,
	&m13661_MI,
	&m13662_MI,
	&m13679_MI,
	&m13681_MI,
	&m13655_MI,
	&m13669_MI,
	&m13670_MI,
	&m13672_MI,
	&m13673_MI,
	&m13678_MI,
	&m13675_MI,
	&m13676_MI,
	&m13679_MI,
	&m13682_MI,
	&m13683_MI,
	&m13674_MI,
	&m13671_MI,
	&m13677_MI,
	&m13680_MI,
	&m13684_MI,
};
static TypeInfo* t2551_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t363_TI,
	&t171_TI,
	&t2547_TI,
};
static Il2CppInterfaceOffsetPair t2551_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t363_TI, 20},
	{ &t171_TI, 27},
	{ &t2547_TI, 32},
};
extern TypeInfo t167_TI;
extern TypeInfo t155_TI;
static Il2CppRGCTXData t2551_RGCTXData[25] = 
{
	&t167_TI/* Class Usage */,
	&m1610_MI/* Method Usage */,
	&m27752_MI/* Method Usage */,
	&m27758_MI/* Method Usage */,
	&m1630_MI/* Method Usage */,
	&m13686_MI/* Method Usage */,
	&m13677_MI/* Method Usage */,
	&m13685_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
	&m27755_MI/* Method Usage */,
	&m27761_MI/* Method Usage */,
	&m13687_MI/* Method Usage */,
	&m13675_MI/* Method Usage */,
	&m13680_MI/* Method Usage */,
	&m13688_MI/* Method Usage */,
	&m13689_MI/* Method Usage */,
	&m1627_MI/* Method Usage */,
	&m13684_MI/* Method Usage */,
	&m13671_MI/* Method Usage */,
	&m27754_MI/* Method Usage */,
	&m27756_MI/* Method Usage */,
	&m27762_MI/* Method Usage */,
	&m27763_MI/* Method Usage */,
	&m27760_MI/* Method Usage */,
	&t155_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2551_0_0_0;
extern Il2CppType t2551_1_0_0;
struct t2551;
extern Il2CppGenericClass t2551_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2551_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2551_MIs, t2551_PIs, t2551_FIs, NULL, &t29_TI, NULL, NULL, &t2551_TI, t2551_ITIs, t2551_VT, &t1262__CustomAttributeCache, &t2551_TI, &t2551_0_0_0, &t2551_1_0_0, t2551_IOs, &t2551_GC, NULL, NULL, NULL, t2551_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2551), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2552_TI;
#include "t2552MD.h"

#include "t1257.h"
#include "t2553.h"
extern TypeInfo t6685_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2553_TI;
#include "t931MD.h"
#include "t2553MD.h"
extern Il2CppType t6685_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m13695_MI;
extern MethodInfo m27873_MI;
extern MethodInfo m20987_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Graphic>
extern Il2CppType t2552_0_0_49;
FieldInfo t2552_f0_FieldInfo = 
{
	"_default", &t2552_0_0_49, &t2552_TI, offsetof(t2552_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2552_FIs[] =
{
	&t2552_f0_FieldInfo,
	NULL
};
extern MethodInfo m13694_MI;
static PropertyInfo t2552____Default_PropertyInfo = 
{
	&t2552_TI, "Default", &m13694_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2552_PIs[] =
{
	&t2552____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13690_GM;
MethodInfo m13690_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2552_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13690_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13691_GM;
MethodInfo m13691_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2552_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13691_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2552_m13692_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13692_GM;
MethodInfo m13692_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2552_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2552_m13692_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13692_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2552_m13693_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13693_GM;
MethodInfo m13693_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2552_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2552_m13693_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13693_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2552_m27873_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27873_GM;
MethodInfo m27873_MI = 
{
	"GetHashCode", NULL, &t2552_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2552_m27873_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27873_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t2552_m20987_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20987_GM;
MethodInfo m20987_MI = 
{
	"Equals", NULL, &t2552_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2552_m20987_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20987_GM};
extern Il2CppType t2552_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13694_GM;
MethodInfo m13694_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2552_TI, &t2552_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13694_GM};
static MethodInfo* t2552_MIs[] =
{
	&m13690_MI,
	&m13691_MI,
	&m13692_MI,
	&m13693_MI,
	&m27873_MI,
	&m20987_MI,
	&m13694_MI,
	NULL
};
extern MethodInfo m13693_MI;
extern MethodInfo m13692_MI;
static MethodInfo* t2552_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m20987_MI,
	&m27873_MI,
	&m13693_MI,
	&m13692_MI,
	NULL,
	NULL,
};
extern TypeInfo t2565_TI;
extern TypeInfo t734_TI;
static TypeInfo* t2552_ITIs[] = 
{
	&t2565_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2552_IOs[] = 
{
	{ &t2565_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2552_TI;
extern TypeInfo t2552_TI;
extern TypeInfo t2553_TI;
extern TypeInfo t155_TI;
static Il2CppRGCTXData t2552_RGCTXData[9] = 
{
	&t6685_0_0_0/* Type Usage */,
	&t155_0_0_0/* Type Usage */,
	&t2552_TI/* Class Usage */,
	&t2552_TI/* Static Usage */,
	&t2553_TI/* Class Usage */,
	&m13695_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
	&m27873_MI/* Method Usage */,
	&m20987_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2552_0_0_0;
extern Il2CppType t2552_1_0_0;
struct t2552;
extern Il2CppGenericClass t2552_GC;
TypeInfo t2552_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2552_MIs, t2552_PIs, t2552_FIs, NULL, &t29_TI, NULL, NULL, &t2552_TI, t2552_ITIs, t2552_VT, &EmptyCustomAttributesCache, &t2552_TI, &t2552_0_0_0, &t2552_1_0_0, t2552_IOs, &t2552_GC, NULL, NULL, NULL, t2552_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2552), 0, -1, sizeof(t2552_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.UI.Graphic>
extern Il2CppType t155_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t2565_m27874_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27874_GM;
MethodInfo m27874_MI = 
{
	"Equals", NULL, &t2565_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2565_m27874_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27874_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2565_m27875_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27875_GM;
MethodInfo m27875_MI = 
{
	"GetHashCode", NULL, &t2565_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2565_m27875_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27875_GM};
static MethodInfo* t2565_MIs[] =
{
	&m27874_MI,
	&m27875_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2565_0_0_0;
extern Il2CppType t2565_1_0_0;
struct t2565;
extern Il2CppGenericClass t2565_GC;
TypeInfo t2565_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t2565_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2565_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2565_TI, &t2565_0_0_0, &t2565_1_0_0, NULL, &t2565_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.UI.Graphic>
extern Il2CppType t155_0_0_0;
static ParameterInfo t6685_m27876_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27876_GM;
MethodInfo m27876_MI = 
{
	"Equals", NULL, &t6685_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6685_m27876_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27876_GM};
static MethodInfo* t6685_MIs[] =
{
	&m27876_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6685_1_0_0;
struct t6685;
extern Il2CppGenericClass t6685_GC;
TypeInfo t6685_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6685_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6685_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6685_TI, &t6685_0_0_0, &t6685_1_0_0, NULL, &t6685_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13690_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Graphic>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13695_GM;
MethodInfo m13695_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2553_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13695_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2553_m13696_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13696_GM;
MethodInfo m13696_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2553_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2553_m13696_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13696_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t2553_m13697_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13697_GM;
MethodInfo m13697_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2553_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2553_m13697_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13697_GM};
static MethodInfo* t2553_MIs[] =
{
	&m13695_MI,
	&m13696_MI,
	&m13697_MI,
	NULL
};
extern MethodInfo m13697_MI;
extern MethodInfo m13696_MI;
static MethodInfo* t2553_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13697_MI,
	&m13696_MI,
	&m13693_MI,
	&m13692_MI,
	&m13696_MI,
	&m13697_MI,
};
static Il2CppInterfaceOffsetPair t2553_IOs[] = 
{
	{ &t2565_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2552_TI;
extern TypeInfo t2552_TI;
extern TypeInfo t2553_TI;
extern TypeInfo t155_TI;
extern TypeInfo t155_TI;
static Il2CppRGCTXData t2553_RGCTXData[11] = 
{
	&t6685_0_0_0/* Type Usage */,
	&t155_0_0_0/* Type Usage */,
	&t2552_TI/* Class Usage */,
	&t2552_TI/* Static Usage */,
	&t2553_TI/* Class Usage */,
	&m13695_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
	&m27873_MI/* Method Usage */,
	&m20987_MI/* Method Usage */,
	&m13690_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2553_0_0_0;
extern Il2CppType t2553_1_0_0;
struct t2553;
extern Il2CppGenericClass t2553_GC;
extern TypeInfo t1256_TI;
TypeInfo t2553_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2553_MIs, NULL, NULL, NULL, &t2552_TI, NULL, &t1256_TI, &t2553_TI, NULL, t2553_VT, &EmptyCustomAttributesCache, &t2553_TI, &t2553_0_0_0, &t2553_1_0_0, t2553_IOs, &t2553_GC, NULL, NULL, NULL, t2553_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2553), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.UI.Graphic>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2549_m13698_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13698_GM;
MethodInfo m13698_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2549_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2549_m13698_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13698_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2549_m13699_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13699_GM;
MethodInfo m13699_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2549_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2549_m13699_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13699_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2549_m13700_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13700_GM;
MethodInfo m13700_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2549_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2549_m13700_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13700_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2549_m13701_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13701_GM;
MethodInfo m13701_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2549_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2549_m13701_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13701_GM};
static MethodInfo* t2549_MIs[] =
{
	&m13698_MI,
	&m13699_MI,
	&m13700_MI,
	&m13701_MI,
	NULL
};
extern MethodInfo m13700_MI;
extern MethodInfo m13701_MI;
static MethodInfo* t2549_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13699_MI,
	&m13700_MI,
	&m13701_MI,
};
static Il2CppInterfaceOffsetPair t2549_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2549_1_0_0;
struct t2549;
extern Il2CppGenericClass t2549_GC;
TypeInfo t2549_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2549_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2549_TI, NULL, t2549_VT, &EmptyCustomAttributesCache, &t2549_TI, &t2549_0_0_0, &t2549_1_0_0, t2549_IOs, &t2549_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2549), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t2555.h"
extern TypeInfo t4162_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t2555_TI;
#include "t2555MD.h"
extern Il2CppType t4162_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m13706_MI;
extern MethodInfo m27877_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.UI.Graphic>
extern Il2CppType t2554_0_0_49;
FieldInfo t2554_f0_FieldInfo = 
{
	"_default", &t2554_0_0_49, &t2554_TI, offsetof(t2554_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2554_FIs[] =
{
	&t2554_f0_FieldInfo,
	NULL
};
static PropertyInfo t2554____Default_PropertyInfo = 
{
	&t2554_TI, "Default", &m13705_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2554_PIs[] =
{
	&t2554____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13702_GM;
MethodInfo m13702_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2554_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13702_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13703_GM;
MethodInfo m13703_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2554_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13703_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2554_m13704_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13704_GM;
MethodInfo m13704_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2554_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2554_m13704_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13704_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t2554_m27877_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27877_GM;
MethodInfo m27877_MI = 
{
	"Compare", NULL, &t2554_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2554_m27877_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27877_GM};
extern Il2CppType t2554_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13705_GM;
MethodInfo m13705_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2554_TI, &t2554_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13705_GM};
static MethodInfo* t2554_MIs[] =
{
	&m13702_MI,
	&m13703_MI,
	&m13704_MI,
	&m27877_MI,
	&m13705_MI,
	NULL
};
extern MethodInfo m13704_MI;
static MethodInfo* t2554_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27877_MI,
	&m13704_MI,
	NULL,
};
extern TypeInfo t4161_TI;
static TypeInfo* t2554_ITIs[] = 
{
	&t4161_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2554_IOs[] = 
{
	{ &t4161_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2554_TI;
extern TypeInfo t2554_TI;
extern TypeInfo t2555_TI;
extern TypeInfo t155_TI;
static Il2CppRGCTXData t2554_RGCTXData[8] = 
{
	&t4162_0_0_0/* Type Usage */,
	&t155_0_0_0/* Type Usage */,
	&t2554_TI/* Class Usage */,
	&t2554_TI/* Static Usage */,
	&t2555_TI/* Class Usage */,
	&m13706_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
	&m27877_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2554_0_0_0;
extern Il2CppType t2554_1_0_0;
struct t2554;
extern Il2CppGenericClass t2554_GC;
TypeInfo t2554_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2554_MIs, t2554_PIs, t2554_FIs, NULL, &t29_TI, NULL, NULL, &t2554_TI, t2554_ITIs, t2554_VT, &EmptyCustomAttributesCache, &t2554_TI, &t2554_0_0_0, &t2554_1_0_0, t2554_IOs, &t2554_GC, NULL, NULL, NULL, t2554_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2554), 0, -1, sizeof(t2554_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.UI.Graphic>
extern Il2CppType t155_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t4161_m20995_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20995_GM;
MethodInfo m20995_MI = 
{
	"Compare", NULL, &t4161_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4161_m20995_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20995_GM};
static MethodInfo* t4161_MIs[] =
{
	&m20995_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4161_0_0_0;
extern Il2CppType t4161_1_0_0;
struct t4161;
extern Il2CppGenericClass t4161_GC;
TypeInfo t4161_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4161_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4161_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4161_TI, &t4161_0_0_0, &t4161_1_0_0, NULL, &t4161_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.UI.Graphic>
extern Il2CppType t155_0_0_0;
static ParameterInfo t4162_m20996_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20996_GM;
MethodInfo m20996_MI = 
{
	"CompareTo", NULL, &t4162_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4162_m20996_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20996_GM};
static MethodInfo* t4162_MIs[] =
{
	&m20996_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4162_1_0_0;
struct t4162;
extern Il2CppGenericClass t4162_GC;
TypeInfo t4162_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4162_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4162_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4162_TI, &t4162_0_0_0, &t4162_1_0_0, NULL, &t4162_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13702_MI;
extern MethodInfo m20996_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UI.Graphic>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13706_GM;
MethodInfo m13706_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2555_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13706_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t2555_m13707_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13707_GM;
MethodInfo m13707_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2555_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2555_m13707_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13707_GM};
static MethodInfo* t2555_MIs[] =
{
	&m13706_MI,
	&m13707_MI,
	NULL
};
extern MethodInfo m13707_MI;
static MethodInfo* t2555_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13707_MI,
	&m13704_MI,
	&m13707_MI,
};
static Il2CppInterfaceOffsetPair t2555_IOs[] = 
{
	{ &t4161_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2554_TI;
extern TypeInfo t2554_TI;
extern TypeInfo t2555_TI;
extern TypeInfo t155_TI;
extern TypeInfo t155_TI;
extern TypeInfo t4162_TI;
static Il2CppRGCTXData t2555_RGCTXData[12] = 
{
	&t4162_0_0_0/* Type Usage */,
	&t155_0_0_0/* Type Usage */,
	&t2554_TI/* Class Usage */,
	&t2554_TI/* Static Usage */,
	&t2555_TI/* Class Usage */,
	&m13706_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
	&m27877_MI/* Method Usage */,
	&m13702_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
	&t4162_TI/* Class Usage */,
	&m20996_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2555_0_0_0;
extern Il2CppType t2555_1_0_0;
struct t2555;
extern Il2CppGenericClass t2555_GC;
TypeInfo t2555_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2555_MIs, NULL, NULL, NULL, &t2554_TI, NULL, &t1246_TI, &t2555_TI, NULL, t2555_VT, &EmptyCustomAttributesCache, &t2555_TI, &t2555_0_0_0, &t2555_1_0_0, t2555_IOs, &t2555_GC, NULL, NULL, NULL, t2555_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2555), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t168_TI;
#include "t168MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.UI.Graphic>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t168_m1631_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1631_GM;
MethodInfo m1631_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t168_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t168_m1631_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1631_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t168_m13708_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13708_GM;
MethodInfo m13708_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t168_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t168_m13708_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13708_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t155_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t168_m13709_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13709_GM;
MethodInfo m13709_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t168_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t168_m13709_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13709_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t168_m13710_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13710_GM;
MethodInfo m13710_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t168_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t168_m13710_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13710_GM};
static MethodInfo* t168_MIs[] =
{
	&m1631_MI,
	&m13708_MI,
	&m13709_MI,
	&m13710_MI,
	NULL
};
extern MethodInfo m13708_MI;
extern MethodInfo m13709_MI;
extern MethodInfo m13710_MI;
static MethodInfo* t168_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13708_MI,
	&m13709_MI,
	&m13710_MI,
};
static Il2CppInterfaceOffsetPair t168_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t168_1_0_0;
struct t168;
extern Il2CppGenericClass t168_GC;
TypeInfo t168_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t168_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t168_TI, NULL, t168_VT, &EmptyCustomAttributesCache, &t168_TI, &t168_0_0_0, &t168_1_0_0, t168_IOs, &t168_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t168), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4164_TI;

#include "t165.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.GraphicRaycaster/BlockingObjects>
extern MethodInfo m27878_MI;
static PropertyInfo t4164____Current_PropertyInfo = 
{
	&t4164_TI, "Current", &m27878_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4164_PIs[] =
{
	&t4164____Current_PropertyInfo,
	NULL
};
extern Il2CppType t165_0_0_0;
extern void* RuntimeInvoker_t165 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27878_GM;
MethodInfo m27878_MI = 
{
	"get_Current", NULL, &t4164_TI, &t165_0_0_0, RuntimeInvoker_t165, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27878_GM};
static MethodInfo* t4164_MIs[] =
{
	&m27878_MI,
	NULL
};
static TypeInfo* t4164_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4164_0_0_0;
extern Il2CppType t4164_1_0_0;
struct t4164;
extern Il2CppGenericClass t4164_GC;
TypeInfo t4164_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4164_MIs, t4164_PIs, NULL, NULL, NULL, NULL, NULL, &t4164_TI, t4164_ITIs, NULL, &EmptyCustomAttributesCache, &t4164_TI, &t4164_0_0_0, &t4164_1_0_0, NULL, &t4164_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2556.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2556_TI;
#include "t2556MD.h"

extern TypeInfo t165_TI;
extern MethodInfo m13715_MI;
extern MethodInfo m21001_MI;
struct t20;
 int32_t m21001 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m13711_MI;
 void m13711 (t2556 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13712_MI;
 t29 * m13712 (t2556 * __this, MethodInfo* method){
	{
		int32_t L_0 = m13715(__this, &m13715_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t165_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13713_MI;
 void m13713 (t2556 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13714_MI;
 bool m13714 (t2556 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m13715 (t2556 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21001(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21001_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.GraphicRaycaster/BlockingObjects>
extern Il2CppType t20_0_0_1;
FieldInfo t2556_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2556_TI, offsetof(t2556, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2556_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2556_TI, offsetof(t2556, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2556_FIs[] =
{
	&t2556_f0_FieldInfo,
	&t2556_f1_FieldInfo,
	NULL
};
static PropertyInfo t2556____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2556_TI, "System.Collections.IEnumerator.Current", &m13712_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2556____Current_PropertyInfo = 
{
	&t2556_TI, "Current", &m13715_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2556_PIs[] =
{
	&t2556____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2556____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2556_m13711_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13711_GM;
MethodInfo m13711_MI = 
{
	".ctor", (methodPointerType)&m13711, &t2556_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2556_m13711_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13711_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13712_GM;
MethodInfo m13712_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13712, &t2556_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13712_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13713_GM;
MethodInfo m13713_MI = 
{
	"Dispose", (methodPointerType)&m13713, &t2556_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13713_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13714_GM;
MethodInfo m13714_MI = 
{
	"MoveNext", (methodPointerType)&m13714, &t2556_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13714_GM};
extern Il2CppType t165_0_0_0;
extern void* RuntimeInvoker_t165 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13715_GM;
MethodInfo m13715_MI = 
{
	"get_Current", (methodPointerType)&m13715, &t2556_TI, &t165_0_0_0, RuntimeInvoker_t165, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13715_GM};
static MethodInfo* t2556_MIs[] =
{
	&m13711_MI,
	&m13712_MI,
	&m13713_MI,
	&m13714_MI,
	&m13715_MI,
	NULL
};
static MethodInfo* t2556_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13712_MI,
	&m13714_MI,
	&m13713_MI,
	&m13715_MI,
};
static TypeInfo* t2556_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4164_TI,
};
static Il2CppInterfaceOffsetPair t2556_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4164_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2556_0_0_0;
extern Il2CppType t2556_1_0_0;
extern Il2CppGenericClass t2556_GC;
TypeInfo t2556_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2556_MIs, t2556_PIs, t2556_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2556_TI, t2556_ITIs, t2556_VT, &EmptyCustomAttributesCache, &t2556_TI, &t2556_0_0_0, &t2556_1_0_0, t2556_IOs, &t2556_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2556)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5328_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.GraphicRaycaster/BlockingObjects>
extern MethodInfo m27879_MI;
static PropertyInfo t5328____Count_PropertyInfo = 
{
	&t5328_TI, "Count", &m27879_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27880_MI;
static PropertyInfo t5328____IsReadOnly_PropertyInfo = 
{
	&t5328_TI, "IsReadOnly", &m27880_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5328_PIs[] =
{
	&t5328____Count_PropertyInfo,
	&t5328____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27879_GM;
MethodInfo m27879_MI = 
{
	"get_Count", NULL, &t5328_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27879_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27880_GM;
MethodInfo m27880_MI = 
{
	"get_IsReadOnly", NULL, &t5328_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27880_GM};
extern Il2CppType t165_0_0_0;
extern Il2CppType t165_0_0_0;
static ParameterInfo t5328_m27881_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t165_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27881_GM;
MethodInfo m27881_MI = 
{
	"Add", NULL, &t5328_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5328_m27881_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27881_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27882_GM;
MethodInfo m27882_MI = 
{
	"Clear", NULL, &t5328_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27882_GM};
extern Il2CppType t165_0_0_0;
static ParameterInfo t5328_m27883_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t165_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27883_GM;
MethodInfo m27883_MI = 
{
	"Contains", NULL, &t5328_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5328_m27883_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27883_GM};
extern Il2CppType t3837_0_0_0;
extern Il2CppType t3837_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5328_m27884_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3837_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27884_GM;
MethodInfo m27884_MI = 
{
	"CopyTo", NULL, &t5328_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5328_m27884_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27884_GM};
extern Il2CppType t165_0_0_0;
static ParameterInfo t5328_m27885_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t165_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27885_GM;
MethodInfo m27885_MI = 
{
	"Remove", NULL, &t5328_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5328_m27885_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27885_GM};
static MethodInfo* t5328_MIs[] =
{
	&m27879_MI,
	&m27880_MI,
	&m27881_MI,
	&m27882_MI,
	&m27883_MI,
	&m27884_MI,
	&m27885_MI,
	NULL
};
extern TypeInfo t5330_TI;
static TypeInfo* t5328_ITIs[] = 
{
	&t603_TI,
	&t5330_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5328_0_0_0;
extern Il2CppType t5328_1_0_0;
struct t5328;
extern Il2CppGenericClass t5328_GC;
TypeInfo t5328_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5328_MIs, t5328_PIs, NULL, NULL, NULL, NULL, NULL, &t5328_TI, t5328_ITIs, NULL, &EmptyCustomAttributesCache, &t5328_TI, &t5328_0_0_0, &t5328_1_0_0, NULL, &t5328_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.GraphicRaycaster/BlockingObjects>
extern Il2CppType t4164_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27886_GM;
MethodInfo m27886_MI = 
{
	"GetEnumerator", NULL, &t5330_TI, &t4164_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27886_GM};
static MethodInfo* t5330_MIs[] =
{
	&m27886_MI,
	NULL
};
static TypeInfo* t5330_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5330_0_0_0;
extern Il2CppType t5330_1_0_0;
struct t5330;
extern Il2CppGenericClass t5330_GC;
TypeInfo t5330_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5330_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5330_TI, t5330_ITIs, NULL, &EmptyCustomAttributesCache, &t5330_TI, &t5330_0_0_0, &t5330_1_0_0, NULL, &t5330_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5329_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.GraphicRaycaster/BlockingObjects>
extern MethodInfo m27887_MI;
extern MethodInfo m27888_MI;
static PropertyInfo t5329____Item_PropertyInfo = 
{
	&t5329_TI, "Item", &m27887_MI, &m27888_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5329_PIs[] =
{
	&t5329____Item_PropertyInfo,
	NULL
};
extern Il2CppType t165_0_0_0;
static ParameterInfo t5329_m27889_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t165_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27889_GM;
MethodInfo m27889_MI = 
{
	"IndexOf", NULL, &t5329_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5329_m27889_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27889_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t165_0_0_0;
static ParameterInfo t5329_m27890_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t165_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27890_GM;
MethodInfo m27890_MI = 
{
	"Insert", NULL, &t5329_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5329_m27890_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27890_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5329_m27891_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27891_GM;
MethodInfo m27891_MI = 
{
	"RemoveAt", NULL, &t5329_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5329_m27891_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27891_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5329_m27887_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t165_0_0_0;
extern void* RuntimeInvoker_t165_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27887_GM;
MethodInfo m27887_MI = 
{
	"get_Item", NULL, &t5329_TI, &t165_0_0_0, RuntimeInvoker_t165_t44, t5329_m27887_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27887_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t165_0_0_0;
static ParameterInfo t5329_m27888_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t165_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27888_GM;
MethodInfo m27888_MI = 
{
	"set_Item", NULL, &t5329_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5329_m27888_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27888_GM};
static MethodInfo* t5329_MIs[] =
{
	&m27889_MI,
	&m27890_MI,
	&m27891_MI,
	&m27887_MI,
	&m27888_MI,
	NULL
};
static TypeInfo* t5329_ITIs[] = 
{
	&t603_TI,
	&t5328_TI,
	&t5330_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5329_0_0_0;
extern Il2CppType t5329_1_0_0;
struct t5329;
extern Il2CppGenericClass t5329_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5329_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5329_MIs, t5329_PIs, NULL, NULL, NULL, NULL, NULL, &t5329_TI, t5329_ITIs, NULL, &t1908__CustomAttributeCache, &t5329_TI, &t5329_0_0_0, &t5329_1_0_0, NULL, &t5329_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t170.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t170_TI;
#include "t170MD.h"

#include "t366.h"
#include "t1248.h"
#include "UnityEngine_ArrayTypes.h"
#include "t1258.h"
#include "t2560.h"
#include "t733.h"
#include "t735.h"
#include "t2562.h"
#include "t725.h"
#include "t2559.h"
#include "t2582.h"
#include "t2564.h"
#include "t2583.h"
#include "t2537.h"
#include "t2584.h"
extern TypeInfo t366_TI;
extern TypeInfo t2558_TI;
extern TypeInfo t1248_TI;
extern TypeInfo t1258_TI;
extern TypeInfo t2560_TI;
extern TypeInfo t2561_TI;
extern TypeInfo t2562_TI;
extern TypeInfo t3541_TI;
extern TypeInfo t725_TI;
extern TypeInfo t2559_TI;
extern TypeInfo t2582_TI;
extern TypeInfo t2564_TI;
extern TypeInfo t2583_TI;
extern TypeInfo t2537_TI;
extern TypeInfo t841_TI;
extern TypeInfo t1965_TI;
extern TypeInfo t2526_TI;
extern TypeInfo t2557_TI;
extern TypeInfo t719_TI;
extern TypeInfo t2584_TI;
extern TypeInfo t6686_TI;
#include "t1258MD.h"
#include "t2560MD.h"
#include "t2562MD.h"
#include "t2559MD.h"
#include "t2582MD.h"
#include "t2564MD.h"
#include "t2583MD.h"
#include "t2537MD.h"
#include "t719MD.h"
#include "t2584MD.h"
#include "t733MD.h"
#include "t7MD.h"
#include "t725MD.h"
extern Il2CppType t2558_0_0_0;
extern Il2CppType t2561_0_0_0;
extern Il2CppType t366_0_0_0;
extern MethodInfo m13745_MI;
extern MethodInfo m13751_MI;
extern MethodInfo m13735_MI;
extern MethodInfo m13752_MI;
extern MethodInfo m13736_MI;
extern MethodInfo m27856_MI;
extern MethodInfo m27855_MI;
extern MethodInfo m6569_MI;
extern MethodInfo m13743_MI;
extern MethodInfo m13889_MI;
extern MethodInfo m13737_MI;
extern MethodInfo m1637_MI;
extern MethodInfo m13749_MI;
extern MethodInfo m13757_MI;
extern MethodInfo m13759_MI;
extern MethodInfo m13753_MI;
extern MethodInfo m13742_MI;
extern MethodInfo m13739_MI;
extern MethodInfo m13755_MI;
extern MethodInfo m13924_MI;
extern MethodInfo m21054_MI;
extern MethodInfo m13740_MI;
extern MethodInfo m13928_MI;
extern MethodInfo m21056_MI;
extern MethodInfo m13908_MI;
extern MethodInfo m13932_MI;
extern MethodInfo m13542_MI;
extern MethodInfo m13738_MI;
extern MethodInfo m13734_MI;
extern MethodInfo m13756_MI;
extern MethodInfo m21057_MI;
extern MethodInfo m6736_MI;
extern MethodInfo m13942_MI;
extern MethodInfo m27892_MI;
extern MethodInfo m3984_MI;
extern MethodInfo m3997_MI;
extern MethodInfo m3996_MI;
extern MethodInfo m3985_MI;
extern MethodInfo m6035_MI;
extern MethodInfo m66_MI;
extern MethodInfo m3973_MI;
extern MethodInfo m1634_MI;
extern MethodInfo m27893_MI;
extern MethodInfo m3965_MI;
struct t170;
 void m21054 (t170 * __this, t3541* p0, int32_t p1, t2559 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t170;
 void m21056 (t170 * __this, t20 * p0, int32_t p1, t2582 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t170;
 void m21057 (t170 * __this, t2561* p0, int32_t p1, t2582 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m13726_MI;
 void m13726 (t170 * __this, t2562  p0, MethodInfo* method){
	{
		t3 * L_0 = m13757((&p0), &m13757_MI);
		t366 * L_1 = m13759((&p0), &m13759_MI);
		VirtActionInvoker2< t3 *, t366 * >::Invoke(&m1637_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m13727_MI;
 bool m13727 (t170 * __this, t2562  p0, MethodInfo* method){
	{
		bool L_0 = m13753(__this, p0, &m13753_MI);
		return L_0;
	}
}
extern MethodInfo m13729_MI;
 bool m13729 (t170 * __this, t2562  p0, MethodInfo* method){
	{
		bool L_0 = m13753(__this, p0, &m13753_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}

IL_000b:
	{
		t3 * L_1 = m13757((&p0), &m13757_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, t3 * >::Invoke(&m13749_MI, __this, L_1);
		return L_2;
	}
}
 t2562  m13740 (t29 * __this, t3 * p0, t366 * p1, MethodInfo* method){
	{
		t2562  L_0 = {0};
		m13756(&L_0, p0, p1, &m13756_MI);
		return L_0;
	}
}
 bool m13753 (t170 * __this, t2562  p0, MethodInfo* method){
	t366 * V_0 = {0};
	{
		t3 * L_0 = m13757((&p0), &m13757_MI);
		bool L_1 = (bool)VirtFuncInvoker2< bool, t3 *, t366 ** >::Invoke(&m1634_MI, __this, L_0, (&V_0));
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2584_TI));
		t2584 * L_2 = m13942(NULL, &m13942_MI);
		t366 * L_3 = m13759((&p0), &m13759_MI);
		bool L_4 = (bool)VirtFuncInvoker2< bool, t366 *, t366 * >::Invoke(&m27893_MI, L_2, L_3, V_0);
		return L_4;
	}
}
extern MethodInfo m13754_MI;
 t2564  m13754 (t170 * __this, MethodInfo* method){
	{
		t2564  L_0 = {0};
		m13908(&L_0, __this, &m13908_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t44_0_0_32849;
FieldInfo t170_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t170_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_32849;
FieldInfo t170_f1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR", &t22_0_0_32849, &t170_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t170_f2_FieldInfo = 
{
	"NO_SLOT", &t44_0_0_32849, &t170_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t170_f3_FieldInfo = 
{
	"HASH_FLAG", &t44_0_0_32849, &t170_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t170_f4_FieldInfo = 
{
	"table", &t841_0_0_1, &t170_TI, offsetof(t170, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1965_0_0_1;
FieldInfo t170_f5_FieldInfo = 
{
	"linkSlots", &t1965_0_0_1, &t170_TI, offsetof(t170, f5), &EmptyCustomAttributesCache};
extern Il2CppType t2526_0_0_1;
FieldInfo t170_f6_FieldInfo = 
{
	"keySlots", &t2526_0_0_1, &t170_TI, offsetof(t170, f6), &EmptyCustomAttributesCache};
extern Il2CppType t2557_0_0_1;
FieldInfo t170_f7_FieldInfo = 
{
	"valueSlots", &t2557_0_0_1, &t170_TI, offsetof(t170, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t170_f8_FieldInfo = 
{
	"touchedSlots", &t44_0_0_1, &t170_TI, offsetof(t170, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t170_f9_FieldInfo = 
{
	"emptySlot", &t44_0_0_1, &t170_TI, offsetof(t170, f9), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t170_f10_FieldInfo = 
{
	"count", &t44_0_0_1, &t170_TI, offsetof(t170, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t170_f11_FieldInfo = 
{
	"threshold", &t44_0_0_1, &t170_TI, offsetof(t170, f11), &EmptyCustomAttributesCache};
extern Il2CppType t2558_0_0_1;
FieldInfo t170_f12_FieldInfo = 
{
	"hcp", &t2558_0_0_1, &t170_TI, offsetof(t170, f12), &EmptyCustomAttributesCache};
extern Il2CppType t733_0_0_1;
FieldInfo t170_f13_FieldInfo = 
{
	"serialization_info", &t733_0_0_1, &t170_TI, offsetof(t170, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t170_f14_FieldInfo = 
{
	"generation", &t44_0_0_1, &t170_TI, offsetof(t170, f14), &EmptyCustomAttributesCache};
extern Il2CppType t2559_0_0_17;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
FieldInfo t170_f15_FieldInfo = 
{
	"<>f__am$cacheB", &t2559_0_0_17, &t170_TI, offsetof(t170_SFs, f15), &t1254__CustomAttributeCache_U3CU3Ef__am$cacheB};
static FieldInfo* t170_FIs[] =
{
	&t170_f0_FieldInfo,
	&t170_f1_FieldInfo,
	&t170_f2_FieldInfo,
	&t170_f3_FieldInfo,
	&t170_f4_FieldInfo,
	&t170_f5_FieldInfo,
	&t170_f6_FieldInfo,
	&t170_f7_FieldInfo,
	&t170_f8_FieldInfo,
	&t170_f9_FieldInfo,
	&t170_f10_FieldInfo,
	&t170_f11_FieldInfo,
	&t170_f12_FieldInfo,
	&t170_f13_FieldInfo,
	&t170_f14_FieldInfo,
	&t170_f15_FieldInfo,
	NULL
};
static const int32_t t170_f0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t170_f0_DefaultValue = 
{
	&t170_f0_FieldInfo, { (char*)&t170_f0_DefaultValueData, &t44_0_0_0 }};
static const float t170_f1_DefaultValueData = 0.9f;
extern Il2CppType t22_0_0_0;
static Il2CppFieldDefaultValueEntry t170_f1_DefaultValue = 
{
	&t170_f1_FieldInfo, { (char*)&t170_f1_DefaultValueData, &t22_0_0_0 }};
static const int32_t t170_f2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t170_f2_DefaultValue = 
{
	&t170_f2_FieldInfo, { (char*)&t170_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t170_f3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry t170_f3_DefaultValue = 
{
	&t170_f3_FieldInfo, { (char*)&t170_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t170_FDVs[] = 
{
	&t170_f0_DefaultValue,
	&t170_f1_DefaultValue,
	&t170_f2_DefaultValue,
	&t170_f3_DefaultValue,
	NULL
};
extern MethodInfo m13719_MI;
extern MethodInfo m13720_MI;
static PropertyInfo t170____System_Collections_IDictionary_Item_PropertyInfo = 
{
	&t170_TI, "System.Collections.IDictionary.Item", &m13719_MI, &m13720_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13723_MI;
static PropertyInfo t170____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t170_TI, "System.Collections.ICollection.IsSynchronized", &m13723_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13724_MI;
static PropertyInfo t170____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t170_TI, "System.Collections.ICollection.SyncRoot", &m13724_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13725_MI;
static PropertyInfo t170____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo = 
{
	&t170_TI, "System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.IsReadOnly", &m13725_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t170____Count_PropertyInfo = 
{
	&t170_TI, "Count", &m13734_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t170____Item_PropertyInfo = 
{
	&t170_TI, "Item", &m13735_MI, &m13736_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13750_MI;
static PropertyInfo t170____Values_PropertyInfo = 
{
	&t170_TI, "Values", &m13750_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t170_PIs[] =
{
	&t170____System_Collections_IDictionary_Item_PropertyInfo,
	&t170____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t170____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t170____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo,
	&t170____Count_PropertyInfo,
	&t170____Item_PropertyInfo,
	&t170____Values_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1633_GM;
MethodInfo m1633_MI = 
{
	".ctor", (methodPointerType)&m12834_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1633_GM};
extern Il2CppType t2558_0_0_0;
static ParameterInfo t170_m13716_ParameterInfos[] = 
{
	{"comparer", 0, 134217728, &EmptyCustomAttributesCache, &t2558_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13716_GM;
MethodInfo m13716_MI = 
{
	".ctor", (methodPointerType)&m12836_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t170_m13716_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13716_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t170_m13717_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13717_GM;
MethodInfo m13717_MI = 
{
	".ctor", (methodPointerType)&m12838_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t170_m13717_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13717_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t170_m13718_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13718_GM;
MethodInfo m13718_MI = 
{
	".ctor", (methodPointerType)&m12840_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t170_m13718_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13718_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t170_m13719_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13719_GM;
MethodInfo m13719_MI = 
{
	"System.Collections.IDictionary.get_Item", (methodPointerType)&m12842_gshared, &t170_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t170_m13719_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13719_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t170_m13720_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13720_GM;
MethodInfo m13720_MI = 
{
	"System.Collections.IDictionary.set_Item", (methodPointerType)&m12844_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t170_m13720_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 20, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13720_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t170_m13721_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13721_GM;
MethodInfo m13721_MI = 
{
	"System.Collections.IDictionary.Add", (methodPointerType)&m12846_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t170_m13721_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 21, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13721_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t170_m13722_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13722_GM;
MethodInfo m13722_MI = 
{
	"System.Collections.IDictionary.Remove", (methodPointerType)&m12848_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t170_m13722_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 23, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13722_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13723_GM;
MethodInfo m13723_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12850_gshared, &t170_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13723_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13724_GM;
MethodInfo m13724_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12852_gshared, &t170_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13724_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13725_GM;
MethodInfo m13725_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly", (methodPointerType)&m12854_gshared, &t170_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13725_GM};
extern Il2CppType t2562_0_0_0;
extern Il2CppType t2562_0_0_0;
static ParameterInfo t170_m13726_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2562_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13726_GM;
MethodInfo m13726_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add", (methodPointerType)&m13726, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t2562, t170_m13726_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13726_GM};
extern Il2CppType t2562_0_0_0;
static ParameterInfo t170_m13727_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2562_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13727_GM;
MethodInfo m13727_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains", (methodPointerType)&m13727, &t170_TI, &t40_0_0_0, RuntimeInvoker_t40_t2562, t170_m13727_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13727_GM};
extern Il2CppType t2561_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t170_m13728_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2561_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13728_GM;
MethodInfo m13728_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo", (methodPointerType)&m12858_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t170_m13728_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13728_GM};
extern Il2CppType t2562_0_0_0;
static ParameterInfo t170_m13729_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2562_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13729_GM;
MethodInfo m13729_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove", (methodPointerType)&m13729, &t170_TI, &t40_0_0_0, RuntimeInvoker_t40_t2562, t170_m13729_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13729_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t170_m13730_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13730_GM;
MethodInfo m13730_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12861_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t170_m13730_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13730_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13731_GM;
MethodInfo m13731_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12863_gshared, &t170_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13731_GM};
extern Il2CppType t2563_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13732_GM;
MethodInfo m13732_MI = 
{
	"System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator", (methodPointerType)&m12865_gshared, &t170_TI, &t2563_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13732_GM};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13733_GM;
MethodInfo m13733_MI = 
{
	"System.Collections.IDictionary.GetEnumerator", (methodPointerType)&m12867_gshared, &t170_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 22, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13733_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13734_GM;
MethodInfo m13734_MI = 
{
	"get_Count", (methodPointerType)&m12869_gshared, &t170_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13734_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t170_m13735_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t366_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13735_GM;
MethodInfo m13735_MI = 
{
	"get_Item", (methodPointerType)&m12871_gshared, &t170_TI, &t366_0_0_0, RuntimeInvoker_t29_t29, t170_m13735_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 25, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13735_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t170_m13736_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13736_GM;
MethodInfo m13736_MI = 
{
	"set_Item", (methodPointerType)&m12873_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t170_m13736_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 26, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13736_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2558_0_0_0;
static ParameterInfo t170_m13737_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hcp", 1, 134217728, &EmptyCustomAttributesCache, &t2558_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13737_GM;
MethodInfo m13737_MI = 
{
	"Init", (methodPointerType)&m12875_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t170_m13737_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13737_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t170_m13738_ParameterInfos[] = 
{
	{"size", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13738_GM;
MethodInfo m13738_MI = 
{
	"InitArrays", (methodPointerType)&m12877_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t170_m13738_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13738_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t170_m13739_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13739_GM;
MethodInfo m13739_MI = 
{
	"CopyToCheck", (methodPointerType)&m12879_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t170_m13739_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13739_GM};
extern Il2CppType t1957_0_0_0;
extern Il2CppType t1957_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6687_0_0_0;
extern Il2CppType t6687_0_0_0;
static ParameterInfo t170_m27894_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1957_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6687_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27894_IGC;
extern TypeInfo m27894_gp_TRet_0_TI;
Il2CppGenericParamFull m27894_gp_TRet_0_TI_GenericParamFull = { { &m27894_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
extern TypeInfo m27894_gp_TElem_1_TI;
Il2CppGenericParamFull m27894_gp_TElem_1_TI_GenericParamFull = { { &m27894_IGC, 1}, {NULL, "TElem", 0, 0, NULL} };
static Il2CppGenericParamFull* m27894_IGPA[2] = 
{
	&m27894_gp_TRet_0_TI_GenericParamFull,
	&m27894_gp_TElem_1_TI_GenericParamFull,
};
extern MethodInfo m27894_MI;
Il2CppGenericContainer m27894_IGC = { { NULL, NULL }, NULL, &m27894_MI, 2, 1, m27894_IGPA };
extern Il2CppGenericMethod m27895_GM;
extern Il2CppType m9954_gp_0_0_0_0;
extern Il2CppType m9954_gp_1_0_0_0;
static Il2CppRGCTXDefinition m27894_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m27895_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27894_GM;
MethodInfo m27894_MI = 
{
	"Do_CopyTo", NULL, &t170_TI, &t21_0_0_0, NULL, t170_m27894_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27894_RGCTXData, (methodPointerType)NULL, &m27894_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t170_m13740_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t2562_0_0_0;
extern void* RuntimeInvoker_t2562_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13740_GM;
MethodInfo m13740_MI = 
{
	"make_pair", (methodPointerType)&m13740, &t170_TI, &t2562_0_0_0, RuntimeInvoker_t2562_t29_t29, t170_m13740_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13740_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t170_m13741_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t366_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13741_GM;
MethodInfo m13741_MI = 
{
	"pick_value", (methodPointerType)&m12882_gshared, &t170_TI, &t366_0_0_0, RuntimeInvoker_t29_t29_t29, t170_m13741_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13741_GM};
extern Il2CppType t2561_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t170_m13742_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2561_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13742_GM;
MethodInfo m13742_MI = 
{
	"CopyTo", (methodPointerType)&m12884_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t170_m13742_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13742_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6690_0_0_0;
extern Il2CppType t6690_0_0_0;
static ParameterInfo t170_m27896_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6690_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27896_IGC;
extern TypeInfo m27896_gp_TRet_0_TI;
Il2CppGenericParamFull m27896_gp_TRet_0_TI_GenericParamFull = { { &m27896_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
static Il2CppGenericParamFull* m27896_IGPA[1] = 
{
	&m27896_gp_TRet_0_TI_GenericParamFull,
};
extern MethodInfo m27896_MI;
Il2CppGenericContainer m27896_IGC = { { NULL, NULL }, NULL, &m27896_MI, 1, 1, m27896_IGPA };
extern Il2CppType m9959_gp_0_0_0_0;
extern Il2CppGenericMethod m27897_GM;
static Il2CppRGCTXDefinition m27896_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m9959_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m27897_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27896_GM;
MethodInfo m27896_MI = 
{
	"Do_ICollectionCopyTo", NULL, &t170_TI, &t21_0_0_0, NULL, t170_m27896_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27896_RGCTXData, (methodPointerType)NULL, &m27896_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13743_GM;
MethodInfo m13743_MI = 
{
	"Resize", (methodPointerType)&m12886_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13743_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t170_m1637_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1637_GM;
MethodInfo m1637_MI = 
{
	"Add", (methodPointerType)&m12887_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t170_m1637_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1637_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13744_GM;
MethodInfo m13744_MI = 
{
	"Clear", (methodPointerType)&m12889_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13744_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t170_m13745_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13745_GM;
MethodInfo m13745_MI = 
{
	"ContainsKey", (methodPointerType)&m12891_gshared, &t170_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t170_m13745_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13745_GM};
extern Il2CppType t366_0_0_0;
static ParameterInfo t170_m13746_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13746_GM;
MethodInfo m13746_MI = 
{
	"ContainsValue", (methodPointerType)&m12893_gshared, &t170_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t170_m13746_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13746_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t170_m13747_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13747_GM;
MethodInfo m13747_MI = 
{
	"GetObjectData", (methodPointerType)&m12895_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t170_m13747_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13747_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t170_m13748_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13748_GM;
MethodInfo m13748_MI = 
{
	"OnDeserialization", (methodPointerType)&m12897_gshared, &t170_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t170_m13748_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13748_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t170_m13749_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13749_GM;
MethodInfo m13749_MI = 
{
	"Remove", (methodPointerType)&m12899_gshared, &t170_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t170_m13749_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13749_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_1_0_2;
extern Il2CppType t366_1_0_0;
static ParameterInfo t170_m1634_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t4167 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1634_GM;
MethodInfo m1634_MI = 
{
	"TryGetValue", (methodPointerType)&m12900_gshared, &t170_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t4167, t170_m1634_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1634_GM};
extern Il2CppType t2560_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13750_GM;
MethodInfo m13750_MI = 
{
	"get_Values", (methodPointerType)&m12902_gshared, &t170_TI, &t2560_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13750_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t170_m13751_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13751_GM;
MethodInfo m13751_MI = 
{
	"ToTKey", (methodPointerType)&m12904_gshared, &t170_TI, &t3_0_0_0, RuntimeInvoker_t29_t29, t170_m13751_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13751_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t170_m13752_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t366_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13752_GM;
MethodInfo m13752_MI = 
{
	"ToTValue", (methodPointerType)&m12906_gshared, &t170_TI, &t366_0_0_0, RuntimeInvoker_t29_t29, t170_m13752_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13752_GM};
extern Il2CppType t2562_0_0_0;
static ParameterInfo t170_m13753_ParameterInfos[] = 
{
	{"pair", 0, 134217728, &EmptyCustomAttributesCache, &t2562_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13753_GM;
MethodInfo m13753_MI = 
{
	"ContainsKeyValuePair", (methodPointerType)&m13753, &t170_TI, &t40_0_0_0, RuntimeInvoker_t40_t2562, t170_m13753_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13753_GM};
extern Il2CppType t2564_0_0_0;
extern void* RuntimeInvoker_t2564 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13754_GM;
MethodInfo m13754_MI = 
{
	"GetEnumerator", (methodPointerType)&m13754, &t170_TI, &t2564_0_0_0, RuntimeInvoker_t2564, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13754_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t170_m13755_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
extern Il2CppGenericMethod m13755_GM;
MethodInfo m13755_MI = 
{
	"<CopyTo>m__0", (methodPointerType)&m12910_gshared, &t170_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t29, t170_m13755_ParameterInfos, &t1254__CustomAttributeCache_m9975, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13755_GM};
static MethodInfo* t170_MIs[] =
{
	&m1633_MI,
	&m13716_MI,
	&m13717_MI,
	&m13718_MI,
	&m13719_MI,
	&m13720_MI,
	&m13721_MI,
	&m13722_MI,
	&m13723_MI,
	&m13724_MI,
	&m13725_MI,
	&m13726_MI,
	&m13727_MI,
	&m13728_MI,
	&m13729_MI,
	&m13730_MI,
	&m13731_MI,
	&m13732_MI,
	&m13733_MI,
	&m13734_MI,
	&m13735_MI,
	&m13736_MI,
	&m13737_MI,
	&m13738_MI,
	&m13739_MI,
	&m27894_MI,
	&m13740_MI,
	&m13741_MI,
	&m13742_MI,
	&m27896_MI,
	&m13743_MI,
	&m1637_MI,
	&m13744_MI,
	&m13745_MI,
	&m13746_MI,
	&m13747_MI,
	&m13748_MI,
	&m13749_MI,
	&m1634_MI,
	&m13750_MI,
	&m13751_MI,
	&m13752_MI,
	&m13753_MI,
	&m13754_MI,
	&m13755_MI,
	NULL
};
extern MethodInfo m13731_MI;
extern MethodInfo m13747_MI;
extern MethodInfo m13730_MI;
extern MethodInfo m13744_MI;
extern MethodInfo m13728_MI;
extern MethodInfo m13732_MI;
extern MethodInfo m13721_MI;
extern MethodInfo m13733_MI;
extern MethodInfo m13722_MI;
extern MethodInfo m13748_MI;
static MethodInfo* t170_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13731_MI,
	&m13747_MI,
	&m13734_MI,
	&m13723_MI,
	&m13724_MI,
	&m13730_MI,
	&m13734_MI,
	&m13725_MI,
	&m13726_MI,
	&m13744_MI,
	&m13727_MI,
	&m13728_MI,
	&m13729_MI,
	&m13732_MI,
	&m13749_MI,
	&m13719_MI,
	&m13720_MI,
	&m13721_MI,
	&m13733_MI,
	&m13722_MI,
	&m13748_MI,
	&m13735_MI,
	&m13736_MI,
	&m1637_MI,
	&m13745_MI,
	&m13747_MI,
	&m13748_MI,
	&m1634_MI,
};
extern TypeInfo t5334_TI;
extern TypeInfo t5336_TI;
extern TypeInfo t6692_TI;
extern TypeInfo t721_TI;
extern TypeInfo t918_TI;
static TypeInfo* t170_ITIs[] = 
{
	&t603_TI,
	&t374_TI,
	&t674_TI,
	&t5334_TI,
	&t5336_TI,
	&t6692_TI,
	&t721_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t170_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t374_TI, 5},
	{ &t674_TI, 6},
	{ &t5334_TI, 10},
	{ &t5336_TI, 17},
	{ &t6692_TI, 18},
	{ &t721_TI, 19},
	{ &t918_TI, 24},
};
extern TypeInfo t3_TI;
extern TypeInfo t366_TI;
extern TypeInfo t2561_TI;
extern TypeInfo t170_TI;
extern TypeInfo t2559_TI;
extern TypeInfo t2582_TI;
extern TypeInfo t2564_TI;
extern TypeInfo t2583_TI;
extern TypeInfo t2526_TI;
extern TypeInfo t2557_TI;
extern TypeInfo t2562_TI;
extern TypeInfo t2561_TI;
extern TypeInfo t2558_TI;
extern TypeInfo t2560_TI;
static Il2CppRGCTXData t170_RGCTXData[52] = 
{
	&m13737_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m13745_MI/* Method Usage */,
	&m13751_MI/* Method Usage */,
	&m13735_MI/* Method Usage */,
	&t366_TI/* Class Usage */,
	&m13752_MI/* Method Usage */,
	&m13736_MI/* Method Usage */,
	&m1637_MI/* Method Usage */,
	&m13749_MI/* Method Usage */,
	&m13757_MI/* Method Usage */,
	&m13759_MI/* Method Usage */,
	&m13753_MI/* Method Usage */,
	&m13742_MI/* Method Usage */,
	&t2561_TI/* Class Usage */,
	&m13739_MI/* Method Usage */,
	&t170_TI/* Static Usage */,
	&m13755_MI/* Method Usage */,
	&t2559_TI/* Class Usage */,
	&m13924_MI/* Method Usage */,
	&m21054_MI/* Method Usage */,
	&m13740_MI/* Method Usage */,
	&t2582_TI/* Class Usage */,
	&m13928_MI/* Method Usage */,
	&m21056_MI/* Method Usage */,
	&t2564_TI/* Class Usage */,
	&m13908_MI/* Method Usage */,
	&t2583_TI/* Class Usage */,
	&m13932_MI/* Method Usage */,
	&m27856_MI/* Method Usage */,
	&m27855_MI/* Method Usage */,
	&m13743_MI/* Method Usage */,
	&m13542_MI/* Method Usage */,
	&m13738_MI/* Method Usage */,
	&t2526_TI/* Array Usage */,
	&t2557_TI/* Array Usage */,
	&m13734_MI/* Method Usage */,
	&t2562_TI/* Class Usage */,
	&m13756_MI/* Method Usage */,
	&m21057_MI/* Method Usage */,
	&m13942_MI/* Method Usage */,
	&m27892_MI/* Method Usage */,
	&t2561_TI/* Array Usage */,
	&t2558_0_0_0/* Type Usage */,
	&t2558_TI/* Class Usage */,
	&t2561_0_0_0/* Type Usage */,
	&t2560_TI/* Class Usage */,
	&m13889_MI/* Method Usage */,
	&t3_0_0_0/* Type Usage */,
	&t366_0_0_0/* Type Usage */,
	&m1634_MI/* Method Usage */,
	&m27893_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t170_0_0_0;
extern Il2CppType t170_1_0_0;
struct t170;
extern Il2CppGenericClass t170_GC;
extern CustomAttributesCache t1254__CustomAttributeCache;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
TypeInfo t170_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Dictionary`2", "System.Collections.Generic", t170_MIs, t170_PIs, t170_FIs, NULL, &t29_TI, NULL, NULL, &t170_TI, t170_ITIs, t170_VT, &t1254__CustomAttributeCache, &t170_TI, &t170_0_0_0, &t170_1_0_0, t170_IOs, &t170_GC, NULL, t170_FDVs, NULL, t170_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t170), 0, -1, sizeof(t170_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 45, 7, 16, 0, 0, 32, 8, 8};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>>
extern MethodInfo m27898_MI;
static PropertyInfo t5334____Count_PropertyInfo = 
{
	&t5334_TI, "Count", &m27898_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27899_MI;
static PropertyInfo t5334____IsReadOnly_PropertyInfo = 
{
	&t5334_TI, "IsReadOnly", &m27899_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5334_PIs[] =
{
	&t5334____Count_PropertyInfo,
	&t5334____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27898_GM;
MethodInfo m27898_MI = 
{
	"get_Count", NULL, &t5334_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27898_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27899_GM;
MethodInfo m27899_MI = 
{
	"get_IsReadOnly", NULL, &t5334_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27899_GM};
extern Il2CppType t2562_0_0_0;
static ParameterInfo t5334_m27900_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2562_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27900_GM;
MethodInfo m27900_MI = 
{
	"Add", NULL, &t5334_TI, &t21_0_0_0, RuntimeInvoker_t21_t2562, t5334_m27900_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27900_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27901_GM;
MethodInfo m27901_MI = 
{
	"Clear", NULL, &t5334_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27901_GM};
extern Il2CppType t2562_0_0_0;
static ParameterInfo t5334_m27902_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2562_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27902_GM;
MethodInfo m27902_MI = 
{
	"Contains", NULL, &t5334_TI, &t40_0_0_0, RuntimeInvoker_t40_t2562, t5334_m27902_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27902_GM};
extern Il2CppType t2561_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5334_m27903_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2561_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27903_GM;
MethodInfo m27903_MI = 
{
	"CopyTo", NULL, &t5334_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5334_m27903_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27903_GM};
extern Il2CppType t2562_0_0_0;
static ParameterInfo t5334_m27904_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2562_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27904_GM;
MethodInfo m27904_MI = 
{
	"Remove", NULL, &t5334_TI, &t40_0_0_0, RuntimeInvoker_t40_t2562, t5334_m27904_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27904_GM};
static MethodInfo* t5334_MIs[] =
{
	&m27898_MI,
	&m27899_MI,
	&m27900_MI,
	&m27901_MI,
	&m27902_MI,
	&m27903_MI,
	&m27904_MI,
	NULL
};
static TypeInfo* t5334_ITIs[] = 
{
	&t603_TI,
	&t5336_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5334_0_0_0;
extern Il2CppType t5334_1_0_0;
struct t5334;
extern Il2CppGenericClass t5334_GC;
TypeInfo t5334_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5334_MIs, t5334_PIs, NULL, NULL, NULL, NULL, NULL, &t5334_TI, t5334_ITIs, NULL, &EmptyCustomAttributesCache, &t5334_TI, &t5334_0_0_0, &t5334_1_0_0, NULL, &t5334_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>>
extern Il2CppType t2563_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27905_GM;
MethodInfo m27905_MI = 
{
	"GetEnumerator", NULL, &t5336_TI, &t2563_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27905_GM};
static MethodInfo* t5336_MIs[] =
{
	&m27905_MI,
	NULL
};
static TypeInfo* t5336_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5336_0_0_0;
extern Il2CppType t5336_1_0_0;
struct t5336;
extern Il2CppGenericClass t5336_GC;
TypeInfo t5336_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5336_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5336_TI, t5336_ITIs, NULL, &EmptyCustomAttributesCache, &t5336_TI, &t5336_0_0_0, &t5336_1_0_0, NULL, &t5336_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2563_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>>
extern MethodInfo m27906_MI;
static PropertyInfo t2563____Current_PropertyInfo = 
{
	&t2563_TI, "Current", &m27906_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2563_PIs[] =
{
	&t2563____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2562_0_0_0;
extern void* RuntimeInvoker_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27906_GM;
MethodInfo m27906_MI = 
{
	"get_Current", NULL, &t2563_TI, &t2562_0_0_0, RuntimeInvoker_t2562, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27906_GM};
static MethodInfo* t2563_MIs[] =
{
	&m27906_MI,
	NULL
};
static TypeInfo* t2563_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2563_0_0_0;
extern Il2CppType t2563_1_0_0;
struct t2563;
extern Il2CppGenericClass t2563_GC;
TypeInfo t2563_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2563_MIs, t2563_PIs, NULL, NULL, NULL, NULL, NULL, &t2563_TI, t2563_ITIs, NULL, &EmptyCustomAttributesCache, &t2563_TI, &t2563_0_0_0, &t2563_1_0_0, NULL, &t2563_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t446_TI;
extern MethodInfo m13758_MI;
extern MethodInfo m13760_MI;
extern MethodInfo m4008_MI;


// Metadata Definition System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t3_0_0_1;
FieldInfo t2562_f0_FieldInfo = 
{
	"key", &t3_0_0_1, &t2562_TI, offsetof(t2562, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t366_0_0_1;
FieldInfo t2562_f1_FieldInfo = 
{
	"value", &t366_0_0_1, &t2562_TI, offsetof(t2562, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2562_FIs[] =
{
	&t2562_f0_FieldInfo,
	&t2562_f1_FieldInfo,
	NULL
};
static PropertyInfo t2562____Key_PropertyInfo = 
{
	&t2562_TI, "Key", &m13757_MI, &m13758_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2562____Value_PropertyInfo = 
{
	&t2562_TI, "Value", &m13759_MI, &m13760_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2562_PIs[] =
{
	&t2562____Key_PropertyInfo,
	&t2562____Value_PropertyInfo,
	NULL
};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t2562_m13756_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13756_GM;
MethodInfo m13756_MI = 
{
	".ctor", (methodPointerType)&m12917_gshared, &t2562_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2562_m13756_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13756_GM};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13757_GM;
MethodInfo m13757_MI = 
{
	"get_Key", (methodPointerType)&m12918_gshared, &t2562_TI, &t3_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13757_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2562_m13758_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13758_GM;
MethodInfo m13758_MI = 
{
	"set_Key", (methodPointerType)&m12919_gshared, &t2562_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2562_m13758_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13758_GM};
extern Il2CppType t366_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13759_GM;
MethodInfo m13759_MI = 
{
	"get_Value", (methodPointerType)&m12920_gshared, &t2562_TI, &t366_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13759_GM};
extern Il2CppType t366_0_0_0;
static ParameterInfo t2562_m13760_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13760_GM;
MethodInfo m13760_MI = 
{
	"set_Value", (methodPointerType)&m12921_gshared, &t2562_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2562_m13760_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13760_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13761_GM;
MethodInfo m13761_MI = 
{
	"ToString", (methodPointerType)&m12922_gshared, &t2562_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13761_GM};
static MethodInfo* t2562_MIs[] =
{
	&m13756_MI,
	&m13757_MI,
	&m13758_MI,
	&m13759_MI,
	&m13760_MI,
	&m13761_MI,
	NULL
};
extern MethodInfo m13761_MI;
static MethodInfo* t2562_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m13761_MI,
};
extern TypeInfo t3_TI;
extern TypeInfo t366_TI;
static Il2CppRGCTXData t2562_RGCTXData[6] = 
{
	&m13758_MI/* Method Usage */,
	&m13760_MI/* Method Usage */,
	&m13757_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m13759_MI/* Method Usage */,
	&t366_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2562_1_0_0;
extern Il2CppGenericClass t2562_GC;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2562_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2", "System.Collections.Generic", t2562_MIs, t2562_PIs, t2562_FIs, NULL, &t110_TI, NULL, NULL, &t2562_TI, NULL, t2562_VT, &t1259__CustomAttributeCache, &t2562_TI, &t2562_0_0_0, &t2562_1_0_0, NULL, &t2562_GC, NULL, NULL, NULL, t2562_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2562)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 2, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif
#include "t366MD.h"

#include "t364.h"
#include "t930.h"
extern TypeInfo t364_TI;
extern TypeInfo t930_TI;
#include "t364MD.h"
#include "t930MD.h"
extern MethodInfo m13811_MI;
extern MethodInfo m13805_MI;
extern MethodInfo m13776_MI;
extern MethodInfo m13763_MI;
extern MethodInfo m13807_MI;
extern MethodInfo m13812_MI;
extern MethodInfo m13771_MI;
extern MethodInfo m9378_MI;
extern MethodInfo m13806_MI;
extern MethodInfo m3988_MI;
extern MethodInfo m13797_MI;
extern MethodInfo m1638_MI;
extern MethodInfo m1632_MI;


// Metadata Definition UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
extern Il2CppType t167_0_0_33;
FieldInfo t366_f0_FieldInfo = 
{
	"m_List", &t167_0_0_33, &t366_TI, offsetof(t366, f0), &EmptyCustomAttributesCache};
extern Il2CppType t364_0_0_1;
FieldInfo t366_f1_FieldInfo = 
{
	"m_Dictionary", &t364_0_0_1, &t366_TI, offsetof(t366, f1), &EmptyCustomAttributesCache};
static FieldInfo* t366_FIs[] =
{
	&t366_f0_FieldInfo,
	&t366_f1_FieldInfo,
	NULL
};
extern MethodInfo m13767_MI;
static PropertyInfo t366____Count_PropertyInfo = 
{
	&t366_TI, "Count", &m13767_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13768_MI;
static PropertyInfo t366____IsReadOnly_PropertyInfo = 
{
	&t366_TI, "IsReadOnly", &m13768_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13772_MI;
extern MethodInfo m13773_MI;
static PropertyInfo t366____Item_PropertyInfo = 
{
	&t366_TI, "Item", &m13772_MI, &m13773_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t366_PIs[] =
{
	&t366____Count_PropertyInfo,
	&t366____IsReadOnly_PropertyInfo,
	&t366____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1636_GM;
MethodInfo m1636_MI = 
{
	".ctor", (methodPointerType)&m12456_gshared, &t366_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1636_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13762_GM;
MethodInfo m13762_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12458_gshared, &t366_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13762_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t366_m1635_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1635_GM;
MethodInfo m1635_MI = 
{
	"Add", (methodPointerType)&m12459_gshared, &t366_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t366_m1635_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1635_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t366_m1638_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1638_GM;
MethodInfo m1638_MI = 
{
	"Remove", (methodPointerType)&m12460_gshared, &t366_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t366_m1638_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1638_GM};
extern Il2CppType t2546_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13763_GM;
MethodInfo m13763_MI = 
{
	"GetEnumerator", (methodPointerType)&m12462_gshared, &t366_TI, &t2546_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13763_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13764_GM;
MethodInfo m13764_MI = 
{
	"Clear", (methodPointerType)&m12463_gshared, &t366_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13764_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t366_m13765_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13765_GM;
MethodInfo m13765_MI = 
{
	"Contains", (methodPointerType)&m12465_gshared, &t366_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t366_m13765_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13765_GM};
extern Il2CppType t2545_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t366_m13766_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2545_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13766_GM;
MethodInfo m13766_MI = 
{
	"CopyTo", (methodPointerType)&m12467_gshared, &t366_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t366_m13766_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13766_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13767_GM;
MethodInfo m13767_MI = 
{
	"get_Count", (methodPointerType)&m12468_gshared, &t366_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13767_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13768_GM;
MethodInfo m13768_MI = 
{
	"get_IsReadOnly", (methodPointerType)&m12470_gshared, &t366_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13768_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t366_m13769_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13769_GM;
MethodInfo m13769_MI = 
{
	"IndexOf", (methodPointerType)&m12472_gshared, &t366_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t366_m13769_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13769_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t366_m13770_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13770_GM;
MethodInfo m13770_MI = 
{
	"Insert", (methodPointerType)&m12474_gshared, &t366_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t366_m13770_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13770_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t366_m13771_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13771_GM;
MethodInfo m13771_MI = 
{
	"RemoveAt", (methodPointerType)&m12476_gshared, &t366_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t366_m13771_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13771_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t366_m13772_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13772_GM;
MethodInfo m13772_MI = 
{
	"get_Item", (methodPointerType)&m12477_gshared, &t366_TI, &t155_0_0_0, RuntimeInvoker_t29_t44, t366_m13772_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13772_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t366_m13773_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13773_GM;
MethodInfo m13773_MI = 
{
	"set_Item", (methodPointerType)&m12479_gshared, &t366_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t366_m13773_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13773_GM};
extern Il2CppType t2549_0_0_0;
static ParameterInfo t366_m13774_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2549_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13774_GM;
MethodInfo m13774_MI = 
{
	"RemoveAll", (methodPointerType)&m12480_gshared, &t366_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t366_m13774_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13774_GM};
extern Il2CppType t168_0_0_0;
static ParameterInfo t366_m13775_ParameterInfos[] = 
{
	{"sortLayoutFunction", 0, 134217728, &EmptyCustomAttributesCache, &t168_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13775_GM;
MethodInfo m13775_MI = 
{
	"Sort", (methodPointerType)&m12481_gshared, &t366_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t366_m13775_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13775_GM};
static MethodInfo* t366_MIs[] =
{
	&m1636_MI,
	&m13762_MI,
	&m1635_MI,
	&m1638_MI,
	&m13763_MI,
	&m13764_MI,
	&m13765_MI,
	&m13766_MI,
	&m13767_MI,
	&m13768_MI,
	&m13769_MI,
	&m13770_MI,
	&m13771_MI,
	&m13772_MI,
	&m13773_MI,
	&m13774_MI,
	&m13775_MI,
	NULL
};
extern MethodInfo m13769_MI;
extern MethodInfo m13770_MI;
extern MethodInfo m1635_MI;
extern MethodInfo m13764_MI;
extern MethodInfo m13765_MI;
extern MethodInfo m13766_MI;
extern MethodInfo m13762_MI;
static MethodInfo* t366_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13769_MI,
	&m13770_MI,
	&m13771_MI,
	&m13772_MI,
	&m13773_MI,
	&m13767_MI,
	&m13768_MI,
	&m1635_MI,
	&m13764_MI,
	&m13765_MI,
	&m13766_MI,
	&m1638_MI,
	&m13763_MI,
	&m13762_MI,
};
static TypeInfo* t366_ITIs[] = 
{
	&t171_TI,
	&t363_TI,
	&t2547_TI,
	&t603_TI,
};
static Il2CppInterfaceOffsetPair t366_IOs[] = 
{
	{ &t171_TI, 4},
	{ &t363_TI, 9},
	{ &t2547_TI, 16},
	{ &t603_TI, 17},
};
extern TypeInfo t167_TI;
extern TypeInfo t364_TI;
static Il2CppRGCTXData t366_RGCTXData[22] = 
{
	&t167_TI/* Class Usage */,
	&m1610_MI/* Method Usage */,
	&t364_TI/* Class Usage */,
	&m13776_MI/* Method Usage */,
	&m13763_MI/* Method Usage */,
	&m13807_MI/* Method Usage */,
	&m1629_MI/* Method Usage */,
	&m1626_MI/* Method Usage */,
	&m13805_MI/* Method Usage */,
	&m13812_MI/* Method Usage */,
	&m13771_MI/* Method Usage */,
	&m1617_MI/* Method Usage */,
	&m13806_MI/* Method Usage */,
	&m13598_MI/* Method Usage */,
	&m1618_MI/* Method Usage */,
	&m13811_MI/* Method Usage */,
	&m13610_MI/* Method Usage */,
	&m13617_MI/* Method Usage */,
	&m13797_MI/* Method Usage */,
	&m13699_MI/* Method Usage */,
	&m1638_MI/* Method Usage */,
	&m1632_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
struct t366;
extern Il2CppGenericClass t366_GC;
extern CustomAttributesCache t274__CustomAttributeCache;
TypeInfo t366_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IndexedSet`1", "UnityEngine.UI.Collections", t366_MIs, t366_PIs, t366_FIs, NULL, &t29_TI, NULL, NULL, &t366_TI, t366_ITIs, t366_VT, &t274__CustomAttributeCache, &t366_TI, &t366_0_0_0, &t366_1_0_0, t366_IOs, &t366_GC, NULL, NULL, NULL, t366_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t366), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 17, 3, 2, 0, 0, 18, 4, 4};
#ifndef _MSC_VER
#else
#endif

#include "t2567.h"
#include "t2569.h"
#include "t2566.h"
#include "t2575.h"
#include "t2571.h"
#include "t2576.h"
#include "t2361.h"
extern TypeInfo t2567_TI;
extern TypeInfo t2568_TI;
extern TypeInfo t2569_TI;
extern TypeInfo t2566_TI;
extern TypeInfo t2575_TI;
extern TypeInfo t2571_TI;
extern TypeInfo t2576_TI;
extern TypeInfo t2361_TI;
extern TypeInfo t2344_TI;
#include "t2567MD.h"
#include "t2569MD.h"
#include "t2566MD.h"
#include "t2575MD.h"
#include "t2571MD.h"
#include "t2576MD.h"
#include "t2361MD.h"
extern Il2CppType t2568_0_0_0;
extern MethodInfo m13814_MI;
extern MethodInfo m13796_MI;
extern MethodInfo m13815_MI;
extern MethodInfo m27875_MI;
extern MethodInfo m27874_MI;
extern MethodInfo m13804_MI;
extern MethodInfo m13830_MI;
extern MethodInfo m13798_MI;
extern MethodInfo m13820_MI;
extern MethodInfo m13822_MI;
extern MethodInfo m13816_MI;
extern MethodInfo m13803_MI;
extern MethodInfo m13800_MI;
extern MethodInfo m13818_MI;
extern MethodInfo m13865_MI;
extern MethodInfo m21025_MI;
extern MethodInfo m13801_MI;
extern MethodInfo m13869_MI;
extern MethodInfo m21027_MI;
extern MethodInfo m13849_MI;
extern MethodInfo m13873_MI;
extern MethodInfo m13799_MI;
extern MethodInfo m13795_MI;
extern MethodInfo m13819_MI;
extern MethodInfo m21028_MI;
extern MethodInfo m12170_MI;
extern MethodInfo m27290_MI;
extern MethodInfo m27426_MI;
struct t364;
 void m21025 (t364 * __this, t3541* p0, int32_t p1, t2566 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t364;
 void m21027 (t364 * __this, t20 * p0, int32_t p1, t2575 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t364;
 void m21028 (t364 * __this, t2568* p0, int32_t p1, t2575 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m13776 (t364 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m13798(__this, ((int32_t)10), (t29*)NULL, &m13798_MI);
		return;
	}
}
extern MethodInfo m13777_MI;
 void m13777 (t364 * __this, t29* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m13798(__this, ((int32_t)10), p0, &m13798_MI);
		return;
	}
}
extern MethodInfo m13778_MI;
 void m13778 (t364 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		m13798(__this, p0, (t29*)NULL, &m13798_MI);
		return;
	}
}
extern MethodInfo m13779_MI;
 void m13779 (t364 * __this, t733 * p0, t735  p1, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		__this->f13 = p0;
		return;
	}
}
extern MethodInfo m13780_MI;
 t29 * m13780 (t364 * __this, t29 * p0, MethodInfo* method){
	{
		if (!((t155 *)IsInst(p0, InitializedTypeInfo(&t155_TI))))
		{
			goto IL_0029;
		}
	}
	{
		bool L_0 = (bool)VirtFuncInvoker1< bool, t155 * >::Invoke(&m13807_MI, __this, ((t155 *)Castclass(p0, InitializedTypeInfo(&t155_TI))));
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		t155 * L_1 = m13814(__this, p0, &m13814_MI);
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, t155 * >::Invoke(&m13796_MI, __this, L_1);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		return L_4;
	}

IL_0029:
	{
		return NULL;
	}
}
extern MethodInfo m13781_MI;
 void m13781 (t364 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t155 * L_0 = m13814(__this, p0, &m13814_MI);
		int32_t L_1 = m13815(__this, p1, &m13815_MI);
		VirtActionInvoker2< t155 *, int32_t >::Invoke(&m13797_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m13782_MI;
 void m13782 (t364 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		t155 * L_0 = m13814(__this, p0, &m13814_MI);
		int32_t L_1 = m13815(__this, p1, &m13815_MI);
		VirtActionInvoker2< t155 *, int32_t >::Invoke(&m13805_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m13783_MI;
 void m13783 (t364 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (!((t155 *)IsInst(p0, InitializedTypeInfo(&t155_TI))))
		{
			goto IL_0023;
		}
	}
	{
		VirtFuncInvoker1< bool, t155 * >::Invoke(&m13811_MI, __this, ((t155 *)Castclass(p0, InitializedTypeInfo(&t155_TI))));
	}

IL_0023:
	{
		return;
	}
}
extern MethodInfo m13784_MI;
 bool m13784 (t364 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m13785_MI;
 t29 * m13785 (t364 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m13786_MI;
 bool m13786 (t364 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m13787_MI;
 void m13787 (t364 * __this, t2569  p0, MethodInfo* method){
	{
		t155 * L_0 = m13820((&p0), &m13820_MI);
		int32_t L_1 = m13822((&p0), &m13822_MI);
		VirtActionInvoker2< t155 *, int32_t >::Invoke(&m13805_MI, __this, L_0, L_1);
		return;
	}
}
extern MethodInfo m13788_MI;
 bool m13788 (t364 * __this, t2569  p0, MethodInfo* method){
	{
		bool L_0 = m13816(__this, p0, &m13816_MI);
		return L_0;
	}
}
extern MethodInfo m13789_MI;
 void m13789 (t364 * __this, t2568* p0, int32_t p1, MethodInfo* method){
	{
		m13803(__this, p0, p1, &m13803_MI);
		return;
	}
}
extern MethodInfo m13790_MI;
 bool m13790 (t364 * __this, t2569  p0, MethodInfo* method){
	{
		bool L_0 = m13816(__this, p0, &m13816_MI);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		return 0;
	}

IL_000b:
	{
		t155 * L_1 = m13820((&p0), &m13820_MI);
		bool L_2 = (bool)VirtFuncInvoker1< bool, t155 * >::Invoke(&m13811_MI, __this, L_1);
		return L_2;
	}
}
extern MethodInfo m13791_MI;
 void m13791 (t364 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t2568* V_0 = {0};
	t3541* V_1 = {0};
	int32_t G_B5_0 = 0;
	t3541* G_B5_1 = {0};
	t364 * G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t3541* G_B4_1 = {0};
	t364 * G_B4_2 = {0};
	{
		V_0 = ((t2568*)IsInst(p0, InitializedTypeInfo(&t2568_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		m13803(__this, V_0, p1, &m13803_MI);
		return;
	}

IL_0013:
	{
		m13800(__this, p0, p1, &m13800_MI);
		V_1 = ((t3541*)IsInst(p0, InitializedTypeInfo(&t3541_TI)));
		if (!V_1)
		{
			goto IL_004b;
		}
	}
	{
		G_B4_0 = p1;
		G_B4_1 = V_1;
		G_B4_2 = ((t364 *)(__this));
		if ((((t364_SFs*)InitializedTypeInfo(&t364_TI)->static_fields)->f15))
		{
			G_B5_0 = p1;
			G_B5_1 = V_1;
			G_B5_2 = ((t364 *)(__this));
			goto IL_0040;
		}
	}
	{
		t35 L_0 = { &m13818_MI };
		t2566 * L_1 = (t2566 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2566_TI));
		m13865(L_1, NULL, L_0, &m13865_MI);
		((t364_SFs*)InitializedTypeInfo(&t364_TI)->static_fields)->f15 = L_1;
		G_B5_0 = G_B4_0;
		G_B5_1 = G_B4_1;
		G_B5_2 = ((t364 *)(G_B4_2));
	}

IL_0040:
	{
		m21025(G_B5_2, G_B5_1, G_B5_0, (((t364_SFs*)InitializedTypeInfo(&t364_TI)->static_fields)->f15), &m21025_MI);
		return;
	}

IL_004b:
	{
		t35 L_2 = { &m13801_MI };
		t2575 * L_3 = (t2575 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2575_TI));
		m13869(L_3, NULL, L_2, &m13869_MI);
		m21027(__this, p0, p1, L_3, &m21027_MI);
		return;
	}
}
extern MethodInfo m13792_MI;
 t29 * m13792 (t364 * __this, MethodInfo* method){
	{
		t2571  L_0 = {0};
		m13849(&L_0, __this, &m13849_MI);
		t2571  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2571_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m13793_MI;
 t29* m13793 (t364 * __this, MethodInfo* method){
	{
		t2571  L_0 = {0};
		m13849(&L_0, __this, &m13849_MI);
		t2571  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2571_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m13794_MI;
 t29 * m13794 (t364 * __this, MethodInfo* method){
	{
		t2576 * L_0 = (t2576 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2576_TI));
		m13873(L_0, __this, &m13873_MI);
		return L_0;
	}
}
 int32_t m13795 (t364 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f10);
		return L_0;
	}
}
 int32_t m13796 (t364 * __this, t155 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t155 * L_0 = p0;
		if (((t155 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t155 * >::Invoke(&m27875_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_008f;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t2545* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t155 *, t155 * >::Invoke(&m27874_MI, L_9, (*(t155 **)(t155 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t841* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		return (*(int32_t*)(int32_t*)SZArrayLdElema(L_13, L_14));
	}

IL_007d:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_008f:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		t1258 * L_17 = (t1258 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1258_TI));
		m6569(L_17, &m6569_MI);
		il2cpp_codegen_raise_exception(L_17);
	}
}
 void m13797 (t364 * __this, t155 * p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		t155 * L_0 = p0;
		if (((t155 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t155 * >::Invoke(&m27875_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		V_3 = (-1);
		if ((((int32_t)V_2) == ((int32_t)(-1))))
		{
			goto IL_0090;
		}
	}

IL_0048:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0078;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t2545* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t155 *, t155 * >::Invoke(&m27874_MI, L_9, (*(t155 **)(t155 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0078;
		}
	}
	{
		goto IL_0090;
	}

IL_0078:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}

IL_0090:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0148;
		}
	}
	{
		int32_t L_15 = (__this->f10);
		int32_t L_16 = ((int32_t)(L_15+1));
		V_4 = L_16;
		__this->f10 = L_16;
		int32_t L_17 = (__this->f11);
		if ((((int32_t)V_4) <= ((int32_t)L_17)))
		{
			goto IL_00c9;
		}
	}
	{
		m13804(__this, &m13804_MI);
		t841* L_18 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_18)->max_length)))));
	}

IL_00c9:
	{
		int32_t L_19 = (__this->f9);
		V_2 = L_19;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_20 = (__this->f8);
		int32_t L_21 = L_20;
		V_4 = L_21;
		__this->f8 = ((int32_t)(L_21+1));
		V_2 = V_4;
		goto IL_0101;
	}

IL_00ea:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1);
		__this->f9 = L_23;
	}

IL_0101:
	{
		t1965* L_24 = (__this->f5);
		t841* L_25 = (__this->f4);
		int32_t L_26 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_25, L_26))-1));
		t841* L_27 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_27, V_1)) = (int32_t)((int32_t)(V_2+1));
		t1965* L_28 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_28, V_2))->f0 = V_0;
		t2545* L_29 = (__this->f6);
		*((t155 **)(t155 **)SZArrayLdElema(L_29, V_2)) = (t155 *)p0;
		goto IL_0194;
	}

IL_0148:
	{
		if ((((int32_t)V_3) == ((int32_t)(-1))))
		{
			goto IL_0194;
		}
	}
	{
		t1965* L_30 = (__this->f5);
		t1965* L_31 = (__this->f5);
		int32_t L_32 = (((t1248 *)(t1248 *)SZArrayLdElema(L_31, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_30, V_3))->f1 = L_32;
		t1965* L_33 = (__this->f5);
		t841* L_34 = (__this->f4);
		int32_t L_35 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_33, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_34, L_35))-1));
		t841* L_36 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_36, V_1)) = (int32_t)((int32_t)(V_2+1));
	}

IL_0194:
	{
		t841* L_37 = (__this->f7);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_37, V_2)) = (int32_t)p1;
		int32_t L_38 = (__this->f14);
		__this->f14 = ((int32_t)(L_38+1));
		return;
	}
}
 void m13798 (t364 * __this, int32_t p0, t29* p1, MethodInfo* method){
	t29* V_0 = {0};
	t364 * G_B4_0 = {0};
	t364 * G_B3_0 = {0};
	t29* G_B5_0 = {0};
	t364 * G_B5_1 = {0};
	{
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000f:
	{
		G_B3_0 = ((t364 *)(__this));
		if (!p1)
		{
			G_B4_0 = ((t364 *)(__this));
			goto IL_0018;
		}
	}
	{
		V_0 = p1;
		G_B5_0 = V_0;
		G_B5_1 = ((t364 *)(G_B3_0));
		goto IL_001d;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2552_TI));
		t2552 * L_1 = m13694(NULL, &m13694_MI);
		G_B5_0 = ((t29*)(L_1));
		G_B5_1 = ((t364 *)(G_B4_0));
	}

IL_001d:
	{
		G_B5_1->f12 = G_B5_0;
		if (p0)
		{
			goto IL_002b;
		}
	}
	{
		p0 = ((int32_t)10);
	}

IL_002b:
	{
		p0 = ((int32_t)((((int32_t)((float)((float)(((float)p0))/(float)(0.9f)))))+1));
		m13799(__this, p0, &m13799_MI);
		__this->f14 = 0;
		return;
	}
}
 void m13799 (t364 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f4 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f5 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), p0));
		__this->f9 = (-1);
		__this->f6 = ((t2545*)SZArrayNew(InitializedTypeInfo(&t2545_TI), p0));
		__this->f7 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), p0));
		__this->f8 = 0;
		t841* L_0 = (__this->f4);
		__this->f11 = (((int32_t)((float)((float)(((float)(((int32_t)(((t20 *)L_0)->max_length)))))*(float)(0.9f)))));
		int32_t L_1 = (__this->f11);
		if (L_1)
		{
			goto IL_006e;
		}
	}
	{
		t841* L_2 = (__this->f4);
		if ((((int32_t)(((int32_t)(((t20 *)L_2)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_006e;
		}
	}
	{
		__this->f11 = 1;
	}

IL_006e:
	{
		return;
	}
}
 void m13800 (t364 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001d:
	{
		int32_t L_2 = m3969(p0, &m3969_MI);
		if ((((int32_t)p1) <= ((int32_t)L_2)))
		{
			goto IL_0031;
		}
	}
	{
		t305 * L_3 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_3, (t7*) &_stringLiteral1164, &m1935_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0031:
	{
		int32_t L_4 = m3969(p0, &m3969_MI);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m13795_MI, __this);
		if ((((int32_t)((int32_t)(L_4-p1))) >= ((int32_t)L_5)))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_6 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_6, (t7*) &_stringLiteral1165, &m1935_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_004c:
	{
		return;
	}
}
 t2569  m13801 (t29 * __this, t155 * p0, int32_t p1, MethodInfo* method){
	{
		t2569  L_0 = {0};
		m13819(&L_0, p0, p1, &m13819_MI);
		return L_0;
	}
}
extern MethodInfo m13802_MI;
 int32_t m13802 (t29 * __this, t155 * p0, int32_t p1, MethodInfo* method){
	{
		return p1;
	}
}
 void m13803 (t364 * __this, t2568* p0, int32_t p1, MethodInfo* method){
	{
		m13800(__this, (t20 *)(t20 *)p0, p1, &m13800_MI);
		t35 L_0 = { &m13801_MI };
		t2575 * L_1 = (t2575 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2575_TI));
		m13869(L_1, NULL, L_0, &m13869_MI);
		m21028(__this, p0, p1, L_1, &m21028_MI);
		return;
	}
}
 void m13804 (t364 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t841* V_1 = {0};
	t1965* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	t2545* V_7 = {0};
	t841* V_8 = {0};
	int32_t V_9 = 0;
	{
		t841* L_0 = (__this->f4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t719_TI));
		int32_t L_1 = m6736(NULL, ((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((t20 *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), &m6736_MI);
		V_0 = L_1;
		V_1 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		V_2 = ((t1965*)SZArrayNew(InitializedTypeInfo(&t1965_TI), V_0));
		V_3 = 0;
		goto IL_00ab;
	}

IL_0027:
	{
		t841* L_2 = (__this->f4);
		int32_t L_3 = V_3;
		V_4 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_2, L_3))-1));
		goto IL_00a2;
	}

IL_0035:
	{
		t29* L_4 = (__this->f12);
		t2545* L_5 = (__this->f6);
		int32_t L_6 = V_4;
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, t155 * >::Invoke(&m27875_MI, L_4, (*(t155 **)(t155 **)SZArrayLdElema(L_5, L_6)));
		int32_t L_8 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)-2147483648)));
		V_9 = L_8;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f0 = L_8;
		V_5 = V_9;
		V_6 = ((int32_t)(((int32_t)((int32_t)V_5&(int32_t)((int32_t)2147483647)))%V_0));
		int32_t L_9 = V_6;
		((t1248 *)(t1248 *)SZArrayLdElema(V_2, V_4))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(V_1, L_9))-1));
		*((int32_t*)(int32_t*)SZArrayLdElema(V_1, V_6)) = (int32_t)((int32_t)(V_4+1));
		t1965* L_10 = (__this->f5);
		int32_t L_11 = (((t1248 *)(t1248 *)SZArrayLdElema(L_10, V_4))->f1);
		V_4 = L_11;
	}

IL_00a2:
	{
		if ((((uint32_t)V_4) != ((uint32_t)(-1))))
		{
			goto IL_0035;
		}
	}
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_00ab:
	{
		t841* L_12 = (__this->f4);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((t20 *)L_12)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		__this->f4 = V_1;
		__this->f5 = V_2;
		V_7 = ((t2545*)SZArrayNew(InitializedTypeInfo(&t2545_TI), V_0));
		V_8 = ((t841*)SZArrayNew(InitializedTypeInfo(&t841_TI), V_0));
		t2545* L_13 = (__this->f6);
		int32_t L_14 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_13, 0, (t20 *)(t20 *)V_7, 0, L_14, &m5952_MI);
		t841* L_15 = (__this->f7);
		int32_t L_16 = (__this->f8);
		m5952(NULL, (t20 *)(t20 *)L_15, 0, (t20 *)(t20 *)V_8, 0, L_16, &m5952_MI);
		__this->f6 = V_7;
		__this->f7 = V_8;
		__this->f11 = (((int32_t)((float)((float)(((float)V_0))*(float)(0.9f)))));
		return;
	}
}
 void m13805 (t364 * __this, t155 * p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		t155 * L_0 = p0;
		if (((t155 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t155 * >::Invoke(&m27875_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		goto IL_008f;
	}

IL_0044:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007d;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t2545* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t155 *, t155 * >::Invoke(&m27874_MI, L_9, (*(t155 **)(t155 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		t305 * L_13 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_13, (t7*) &_stringLiteral1167, &m1935_MI);
		il2cpp_codegen_raise_exception(L_13);
	}

IL_007d:
	{
		t1965* L_14 = (__this->f5);
		int32_t L_15 = (((t1248 *)(t1248 *)SZArrayLdElema(L_14, V_2))->f1);
		V_2 = L_15;
	}

IL_008f:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_16 = (__this->f10);
		int32_t L_17 = ((int32_t)(L_16+1));
		V_3 = L_17;
		__this->f10 = L_17;
		int32_t L_18 = (__this->f11);
		if ((((int32_t)V_3) <= ((int32_t)L_18)))
		{
			goto IL_00c3;
		}
	}
	{
		m13804(__this, &m13804_MI);
		t841* L_19 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_19)->max_length)))));
	}

IL_00c3:
	{
		int32_t L_20 = (__this->f9);
		V_2 = L_20;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_21 = (__this->f8);
		int32_t L_22 = L_21;
		V_3 = L_22;
		__this->f8 = ((int32_t)(L_22+1));
		V_2 = V_3;
		goto IL_00f9;
	}

IL_00e2:
	{
		t1965* L_23 = (__this->f5);
		int32_t L_24 = (((t1248 *)(t1248 *)SZArrayLdElema(L_23, V_2))->f1);
		__this->f9 = L_24;
	}

IL_00f9:
	{
		t1965* L_25 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_25, V_2))->f0 = V_0;
		t1965* L_26 = (__this->f5);
		t841* L_27 = (__this->f4);
		int32_t L_28 = V_1;
		((t1248 *)(t1248 *)SZArrayLdElema(L_26, V_2))->f1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_28))-1));
		t841* L_29 = (__this->f4);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_29, V_1)) = (int32_t)((int32_t)(V_2+1));
		t2545* L_30 = (__this->f6);
		*((t155 **)(t155 **)SZArrayLdElema(L_30, V_2)) = (t155 *)p0;
		t841* L_31 = (__this->f7);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_31, V_2)) = (int32_t)p1;
		int32_t L_32 = (__this->f14);
		__this->f14 = ((int32_t)(L_32+1));
		return;
	}
}
 void m13806 (t364 * __this, MethodInfo* method){
	{
		__this->f10 = 0;
		t841* L_0 = (__this->f4);
		t841* L_1 = (__this->f4);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		t2545* L_2 = (__this->f6);
		t2545* L_3 = (__this->f6);
		m5112(NULL, (t20 *)(t20 *)L_2, 0, (((int32_t)(((t20 *)L_3)->max_length))), &m5112_MI);
		t841* L_4 = (__this->f7);
		t841* L_5 = (__this->f7);
		m5112(NULL, (t20 *)(t20 *)L_4, 0, (((int32_t)(((t20 *)L_5)->max_length))), &m5112_MI);
		t1965* L_6 = (__this->f5);
		t1965* L_7 = (__this->f5);
		m5112(NULL, (t20 *)(t20 *)L_6, 0, (((int32_t)(((t20 *)L_7)->max_length))), &m5112_MI);
		__this->f9 = (-1);
		__this->f8 = 0;
		int32_t L_8 = (__this->f14);
		__this->f14 = ((int32_t)(L_8+1));
		return;
	}
}
 bool m13807 (t364 * __this, t155 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		t155 * L_0 = p0;
		if (((t155 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t155 * >::Invoke(&m27875_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0084;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0072;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t2545* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t155 *, t155 * >::Invoke(&m27874_MI, L_9, (*(t155 **)(t155 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0072;
		}
	}
	{
		return 1;
	}

IL_0072:
	{
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_1))->f1);
		V_1 = L_14;
	}

IL_0084:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m13808_MI;
 bool m13808 (t364 * __this, int32_t p0, MethodInfo* method){
	t29* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2361_TI));
		t2361 * L_0 = m12170(NULL, &m12170_MI);
		V_0 = L_0;
		V_1 = 0;
		goto IL_0048;
	}

IL_000a:
	{
		t841* L_1 = (__this->f4);
		int32_t L_2 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_2))-1));
		goto IL_0040;
	}

IL_0017:
	{
		t841* L_3 = (__this->f7);
		int32_t L_4 = V_2;
		bool L_5 = (bool)InterfaceFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27290_MI, V_0, (*(int32_t*)(int32_t*)SZArrayLdElema(L_3, L_4)), p0);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		t1965* L_6 = (__this->f5);
		int32_t L_7 = (((t1248 *)(t1248 *)SZArrayLdElema(L_6, V_2))->f1);
		V_2 = L_7;
	}

IL_0040:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0017;
		}
	}
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0048:
	{
		t841* L_8 = (__this->f4);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((t20 *)L_8)->max_length))))))
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}
}
extern MethodInfo m13809_MI;
 void m13809 (t364 * __this, t733 * p0, t735  p1, MethodInfo* method){
	t2568* V_0 = {0};
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral206, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = (__this->f14);
		m3984(p0, (t7*) &_stringLiteral208, L_1, &m3984_MI);
		t29* L_2 = (__this->f12);
		m3997(p0, (t7*) &_stringLiteral210, L_2, &m3997_MI);
		V_0 = (t2568*)NULL;
		int32_t L_3 = (__this->f10);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_4 = (__this->f10);
		V_0 = ((t2568*)SZArrayNew(InitializedTypeInfo(&t2568_TI), L_4));
		m13803(__this, V_0, 0, &m13803_MI);
	}

IL_004f:
	{
		t841* L_5 = (__this->f4);
		m3984(p0, (t7*) &_stringLiteral1168, (((int32_t)(((t20 *)L_5)->max_length))), &m3984_MI);
		m3997(p0, (t7*) &_stringLiteral1169, (t29 *)(t29 *)V_0, &m3997_MI);
		return;
	}
}
extern MethodInfo m13810_MI;
 void m13810 (t364 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t2568* V_1 = {0};
	int32_t V_2 = 0;
	{
		t733 * L_0 = (__this->f13);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		t733 * L_1 = (__this->f13);
		int32_t L_2 = m3996(L_1, (t7*) &_stringLiteral208, &m3996_MI);
		__this->f14 = L_2;
		t733 * L_3 = (__this->f13);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t2565_0_0_0), &m1554_MI);
		t29 * L_5 = m3985(L_3, (t7*) &_stringLiteral210, L_4, &m3985_MI);
		__this->f12 = ((t29*)Castclass(L_5, InitializedTypeInfo(&t2565_TI)));
		t733 * L_6 = (__this->f13);
		int32_t L_7 = m3996(L_6, (t7*) &_stringLiteral1168, &m3996_MI);
		V_0 = L_7;
		t733 * L_8 = (__this->f13);
		t42 * L_9 = m1554(NULL, LoadTypeToken(&t2568_0_0_0), &m1554_MI);
		t29 * L_10 = m3985(L_8, (t7*) &_stringLiteral1169, L_9, &m3985_MI);
		V_1 = ((t2568*)Castclass(L_10, InitializedTypeInfo(&t2568_TI)));
		if ((((int32_t)V_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_007d;
		}
	}
	{
		V_0 = ((int32_t)10);
	}

IL_007d:
	{
		m13799(__this, V_0, &m13799_MI);
		__this->f10 = 0;
		if (!V_1)
		{
			goto IL_00ba;
		}
	}
	{
		V_2 = 0;
		goto IL_00b4;
	}

IL_0092:
	{
		t155 * L_11 = m13820(((t2569 *)(t2569 *)SZArrayLdElema(V_1, V_2)), &m13820_MI);
		int32_t L_12 = m13822(((t2569 *)(t2569 *)SZArrayLdElema(V_1, V_2)), &m13822_MI);
		VirtActionInvoker2< t155 *, int32_t >::Invoke(&m13805_MI, __this, L_11, L_12);
		V_2 = ((int32_t)(V_2+1));
	}

IL_00b4:
	{
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((t20 *)V_1)->max_length))))))
		{
			goto IL_0092;
		}
	}

IL_00ba:
	{
		int32_t L_13 = (__this->f14);
		__this->f14 = ((int32_t)(L_13+1));
		__this->f13 = (t733 *)NULL;
		return;
	}
}
 bool m13811 (t364 * __this, t155 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	t155 * V_4 = {0};
	int32_t V_5 = 0;
	{
		t155 * L_0 = p0;
		if (((t155 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t155 * >::Invoke(&m27875_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		V_1 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_4)->max_length)))));
		t841* L_5 = (__this->f4);
		int32_t L_6 = V_1;
		V_2 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_6))-1));
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0048;
		}
	}
	{
		return 0;
	}

IL_0048:
	{
		V_3 = (-1);
	}

IL_004a:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_2))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_007a;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t2545* L_10 = (__this->f6);
		int32_t L_11 = V_2;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t155 *, t155 * >::Invoke(&m27874_MI, L_9, (*(t155 **)(t155 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_007a;
		}
	}
	{
		goto IL_0092;
	}

IL_007a:
	{
		V_3 = V_2;
		t1965* L_13 = (__this->f5);
		int32_t L_14 = (((t1248 *)(t1248 *)SZArrayLdElema(L_13, V_2))->f1);
		V_2 = L_14;
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_004a;
		}
	}

IL_0092:
	{
		if ((((uint32_t)V_2) != ((uint32_t)(-1))))
		{
			goto IL_0098;
		}
	}
	{
		return 0;
	}

IL_0098:
	{
		int32_t L_15 = (__this->f10);
		__this->f10 = ((int32_t)(L_15-1));
		if ((((uint32_t)V_3) != ((uint32_t)(-1))))
		{
			goto IL_00c7;
		}
	}
	{
		t841* L_16 = (__this->f4);
		t1965* L_17 = (__this->f5);
		int32_t L_18 = (((t1248 *)(t1248 *)SZArrayLdElema(L_17, V_2))->f1);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_16, V_1)) = (int32_t)((int32_t)(L_18+1));
		goto IL_00e9;
	}

IL_00c7:
	{
		t1965* L_19 = (__this->f5);
		t1965* L_20 = (__this->f5);
		int32_t L_21 = (((t1248 *)(t1248 *)SZArrayLdElema(L_20, V_2))->f1);
		((t1248 *)(t1248 *)SZArrayLdElema(L_19, V_3))->f1 = L_21;
	}

IL_00e9:
	{
		t1965* L_22 = (__this->f5);
		int32_t L_23 = (__this->f9);
		((t1248 *)(t1248 *)SZArrayLdElema(L_22, V_2))->f1 = L_23;
		__this->f9 = V_2;
		t1965* L_24 = (__this->f5);
		((t1248 *)(t1248 *)SZArrayLdElema(L_24, V_2))->f0 = 0;
		t2545* L_25 = (__this->f6);
		Initobj (&t155_TI, (&V_4));
		*((t155 **)(t155 **)SZArrayLdElema(L_25, V_2)) = (t155 *)V_4;
		t841* L_26 = (__this->f7);
		Initobj (&t44_TI, (&V_5));
		*((int32_t*)(int32_t*)SZArrayLdElema(L_26, V_2)) = (int32_t)V_5;
		int32_t L_27 = (__this->f14);
		__this->f14 = ((int32_t)(L_27+1));
		return 1;
	}
}
 bool m13812 (t364 * __this, t155 * p0, int32_t* p1, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		t155 * L_0 = p0;
		if (((t155 *)L_0))
		{
			goto IL_0013;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0013:
	{
		t29* L_2 = (__this->f12);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, t155 * >::Invoke(&m27875_MI, L_2, p0);
		V_0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)-2147483648)));
		t841* L_4 = (__this->f4);
		t841* L_5 = (__this->f4);
		int32_t L_6 = ((int32_t)(((int32_t)((int32_t)V_0&(int32_t)((int32_t)2147483647)))%(((int32_t)(((t20 *)L_5)->max_length)))));
		V_1 = ((int32_t)((*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-1));
		goto IL_0096;
	}

IL_0042:
	{
		t1965* L_7 = (__this->f5);
		int32_t L_8 = (((t1248 *)(t1248 *)SZArrayLdElema(L_7, V_1))->f0);
		if ((((uint32_t)L_8) != ((uint32_t)V_0)))
		{
			goto IL_0084;
		}
	}
	{
		t29* L_9 = (__this->f12);
		t2545* L_10 = (__this->f6);
		int32_t L_11 = V_1;
		bool L_12 = (bool)InterfaceFuncInvoker2< bool, t155 *, t155 * >::Invoke(&m27874_MI, L_9, (*(t155 **)(t155 **)SZArrayLdElema(L_10, L_11)), p0);
		if (!L_12)
		{
			goto IL_0084;
		}
	}
	{
		t841* L_13 = (__this->f7);
		int32_t L_14 = V_1;
		*p1 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_13, L_14));
		return 1;
	}

IL_0084:
	{
		t1965* L_15 = (__this->f5);
		int32_t L_16 = (((t1248 *)(t1248 *)SZArrayLdElema(L_15, V_1))->f1);
		V_1 = L_16;
	}

IL_0096:
	{
		if ((((uint32_t)V_1) != ((uint32_t)(-1))))
		{
			goto IL_0042;
		}
	}
	{
		Initobj (&t44_TI, (&V_2));
		*p1 = V_2;
		return 0;
	}
}
extern MethodInfo m13813_MI;
 t2567 * m13813 (t364 * __this, MethodInfo* method){
	{
		t2567 * L_0 = (t2567 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2567_TI));
		m13830(L_0, __this, &m13830_MI);
		return L_0;
	}
}
 t155 * m13814 (t364 * __this, t29 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral195, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		if (((t155 *)IsInst(p0, InitializedTypeInfo(&t155_TI))))
		{
			goto IL_003a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t155_0_0_0), &m1554_MI);
		t7* L_2 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m66(NULL, (t7*) &_stringLiteral1170, L_2, &m66_MI);
		t305 * L_4 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_4, L_3, (t7*) &_stringLiteral195, &m3973_MI);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_003a:
	{
		return ((t155 *)Castclass(p0, InitializedTypeInfo(&t155_TI)));
	}
}
 int32_t m13815 (t364 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		if (p0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		Initobj (&t44_TI, (&V_0));
		return V_0;
	}

IL_001e:
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t44_TI))))
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t44_0_0_0), &m1554_MI);
		t7* L_3 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m6035_MI, L_2);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_4 = m66(NULL, (t7*) &_stringLiteral1170, L_3, &m66_MI);
		t305 * L_5 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m3973(L_5, L_4, (t7*) &_stringLiteral401, &m3973_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_004a:
	{
		return ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI)))));
	}
}
 bool m13816 (t364 * __this, t2569  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t155 * L_0 = m13820((&p0), &m13820_MI);
		bool L_1 = (bool)VirtFuncInvoker2< bool, t155 *, int32_t* >::Invoke(&m13812_MI, __this, L_0, (&V_0));
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2361_TI));
		t2361 * L_2 = m12170(NULL, &m12170_MI);
		int32_t L_3 = m13822((&p0), &m13822_MI);
		bool L_4 = (bool)VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27426_MI, L_2, L_3, V_0);
		return L_4;
	}
}
extern MethodInfo m13817_MI;
 t2571  m13817 (t364 * __this, MethodInfo* method){
	{
		t2571  L_0 = {0};
		m13849(&L_0, __this, &m13849_MI);
		return L_0;
	}
}
 t725  m13818 (t29 * __this, t155 * p0, int32_t p1, MethodInfo* method){
	{
		t155 * L_0 = p0;
		int32_t L_1 = p1;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		t725  L_3 = {0};
		m3965(&L_3, ((t155 *)L_0), L_2, &m3965_MI);
		return L_3;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2<UnityEngine.UI.Graphic,System.Int32>
extern Il2CppType t44_0_0_32849;
FieldInfo t364_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t364_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_32849;
FieldInfo t364_f1_FieldInfo = 
{
	"DEFAULT_LOAD_FACTOR", &t22_0_0_32849, &t364_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t364_f2_FieldInfo = 
{
	"NO_SLOT", &t44_0_0_32849, &t364_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_32849;
FieldInfo t364_f3_FieldInfo = 
{
	"HASH_FLAG", &t44_0_0_32849, &t364_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t364_f4_FieldInfo = 
{
	"table", &t841_0_0_1, &t364_TI, offsetof(t364, f4), &EmptyCustomAttributesCache};
extern Il2CppType t1965_0_0_1;
FieldInfo t364_f5_FieldInfo = 
{
	"linkSlots", &t1965_0_0_1, &t364_TI, offsetof(t364, f5), &EmptyCustomAttributesCache};
extern Il2CppType t2545_0_0_1;
FieldInfo t364_f6_FieldInfo = 
{
	"keySlots", &t2545_0_0_1, &t364_TI, offsetof(t364, f6), &EmptyCustomAttributesCache};
extern Il2CppType t841_0_0_1;
FieldInfo t364_f7_FieldInfo = 
{
	"valueSlots", &t841_0_0_1, &t364_TI, offsetof(t364, f7), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t364_f8_FieldInfo = 
{
	"touchedSlots", &t44_0_0_1, &t364_TI, offsetof(t364, f8), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t364_f9_FieldInfo = 
{
	"emptySlot", &t44_0_0_1, &t364_TI, offsetof(t364, f9), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t364_f10_FieldInfo = 
{
	"count", &t44_0_0_1, &t364_TI, offsetof(t364, f10), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t364_f11_FieldInfo = 
{
	"threshold", &t44_0_0_1, &t364_TI, offsetof(t364, f11), &EmptyCustomAttributesCache};
extern Il2CppType t2565_0_0_1;
FieldInfo t364_f12_FieldInfo = 
{
	"hcp", &t2565_0_0_1, &t364_TI, offsetof(t364, f12), &EmptyCustomAttributesCache};
extern Il2CppType t733_0_0_1;
FieldInfo t364_f13_FieldInfo = 
{
	"serialization_info", &t733_0_0_1, &t364_TI, offsetof(t364, f13), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t364_f14_FieldInfo = 
{
	"generation", &t44_0_0_1, &t364_TI, offsetof(t364, f14), &EmptyCustomAttributesCache};
extern Il2CppType t2566_0_0_17;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
FieldInfo t364_f15_FieldInfo = 
{
	"<>f__am$cacheB", &t2566_0_0_17, &t364_TI, offsetof(t364_SFs, f15), &t1254__CustomAttributeCache_U3CU3Ef__am$cacheB};
static FieldInfo* t364_FIs[] =
{
	&t364_f0_FieldInfo,
	&t364_f1_FieldInfo,
	&t364_f2_FieldInfo,
	&t364_f3_FieldInfo,
	&t364_f4_FieldInfo,
	&t364_f5_FieldInfo,
	&t364_f6_FieldInfo,
	&t364_f7_FieldInfo,
	&t364_f8_FieldInfo,
	&t364_f9_FieldInfo,
	&t364_f10_FieldInfo,
	&t364_f11_FieldInfo,
	&t364_f12_FieldInfo,
	&t364_f13_FieldInfo,
	&t364_f14_FieldInfo,
	&t364_f15_FieldInfo,
	NULL
};
static const int32_t t364_f0_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry t364_f0_DefaultValue = 
{
	&t364_f0_FieldInfo, { (char*)&t364_f0_DefaultValueData, &t44_0_0_0 }};
static const float t364_f1_DefaultValueData = 0.9f;
static Il2CppFieldDefaultValueEntry t364_f1_DefaultValue = 
{
	&t364_f1_FieldInfo, { (char*)&t364_f1_DefaultValueData, &t22_0_0_0 }};
static const int32_t t364_f2_DefaultValueData = -1;
static Il2CppFieldDefaultValueEntry t364_f2_DefaultValue = 
{
	&t364_f2_FieldInfo, { (char*)&t364_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t364_f3_DefaultValueData = -2147483648;
static Il2CppFieldDefaultValueEntry t364_f3_DefaultValue = 
{
	&t364_f3_FieldInfo, { (char*)&t364_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t364_FDVs[] = 
{
	&t364_f0_DefaultValue,
	&t364_f1_DefaultValue,
	&t364_f2_DefaultValue,
	&t364_f3_DefaultValue,
	NULL
};
static PropertyInfo t364____System_Collections_IDictionary_Item_PropertyInfo = 
{
	&t364_TI, "System.Collections.IDictionary.Item", &m13780_MI, &m13781_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t364____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t364_TI, "System.Collections.ICollection.IsSynchronized", &m13784_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t364____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t364_TI, "System.Collections.ICollection.SyncRoot", &m13785_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t364____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo = 
{
	&t364_TI, "System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.IsReadOnly", &m13786_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t364____Count_PropertyInfo = 
{
	&t364_TI, "Count", &m13795_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t364____Item_PropertyInfo = 
{
	&t364_TI, "Item", &m13796_MI, &m13797_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t364____Values_PropertyInfo = 
{
	&t364_TI, "Values", &m13813_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t364_PIs[] =
{
	&t364____System_Collections_IDictionary_Item_PropertyInfo,
	&t364____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t364____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t364____System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_IsReadOnly_PropertyInfo,
	&t364____Count_PropertyInfo,
	&t364____Item_PropertyInfo,
	&t364____Values_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13776_GM;
MethodInfo m13776_MI = 
{
	".ctor", (methodPointerType)&m13776, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13776_GM};
extern Il2CppType t2565_0_0_0;
static ParameterInfo t364_m13777_ParameterInfos[] = 
{
	{"comparer", 0, 134217728, &EmptyCustomAttributesCache, &t2565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13777_GM;
MethodInfo m13777_MI = 
{
	".ctor", (methodPointerType)&m13777, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t364_m13777_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13777_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t364_m13778_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13778_GM;
MethodInfo m13778_MI = 
{
	".ctor", (methodPointerType)&m13778, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t364_m13778_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13778_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t364_m13779_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13779_GM;
MethodInfo m13779_MI = 
{
	".ctor", (methodPointerType)&m13779, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t364_m13779_ParameterInfos, &EmptyCustomAttributesCache, 6276, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13779_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t364_m13780_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13780_GM;
MethodInfo m13780_MI = 
{
	"System.Collections.IDictionary.get_Item", (methodPointerType)&m13780, &t364_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t364_m13780_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13780_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t364_m13781_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13781_GM;
MethodInfo m13781_MI = 
{
	"System.Collections.IDictionary.set_Item", (methodPointerType)&m13781, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t364_m13781_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 20, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13781_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t364_m13782_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13782_GM;
MethodInfo m13782_MI = 
{
	"System.Collections.IDictionary.Add", (methodPointerType)&m13782, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t364_m13782_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 21, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13782_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t364_m13783_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13783_GM;
MethodInfo m13783_MI = 
{
	"System.Collections.IDictionary.Remove", (methodPointerType)&m13783, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t364_m13783_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 23, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13783_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13784_GM;
MethodInfo m13784_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m13784, &t364_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13784_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13785_GM;
MethodInfo m13785_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m13785, &t364_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13785_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13786_GM;
MethodInfo m13786_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly", (methodPointerType)&m13786, &t364_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 11, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13786_GM};
extern Il2CppType t2569_0_0_0;
extern Il2CppType t2569_0_0_0;
static ParameterInfo t364_m13787_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2569_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13787_GM;
MethodInfo m13787_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add", (methodPointerType)&m13787, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t2569, t364_m13787_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13787_GM};
extern Il2CppType t2569_0_0_0;
static ParameterInfo t364_m13788_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2569_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13788_GM;
MethodInfo m13788_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains", (methodPointerType)&m13788, &t364_TI, &t40_0_0_0, RuntimeInvoker_t40_t2569, t364_m13788_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 14, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13788_GM};
extern Il2CppType t2568_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t364_m13789_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2568_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13789_GM;
MethodInfo m13789_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo", (methodPointerType)&m13789, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t364_m13789_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13789_GM};
extern Il2CppType t2569_0_0_0;
static ParameterInfo t364_m13790_ParameterInfos[] = 
{
	{"keyValuePair", 0, 134217728, &EmptyCustomAttributesCache, &t2569_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13790_GM;
MethodInfo m13790_MI = 
{
	"System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove", (methodPointerType)&m13790, &t364_TI, &t40_0_0_0, RuntimeInvoker_t40_t2569, t364_m13790_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13790_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t364_m13791_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13791_GM;
MethodInfo m13791_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m13791, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t364_m13791_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13791_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13792_GM;
MethodInfo m13792_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m13792, &t364_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13792_GM};
extern Il2CppType t2570_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13793_GM;
MethodInfo m13793_MI = 
{
	"System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator", (methodPointerType)&m13793, &t364_TI, &t2570_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13793_GM};
extern Il2CppType t722_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13794_GM;
MethodInfo m13794_MI = 
{
	"System.Collections.IDictionary.GetEnumerator", (methodPointerType)&m13794, &t364_TI, &t722_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 22, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13794_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13795_GM;
MethodInfo m13795_MI = 
{
	"get_Count", (methodPointerType)&m13795, &t364_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13795_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t364_m13796_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13796_GM;
MethodInfo m13796_MI = 
{
	"get_Item", (methodPointerType)&m13796, &t364_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t364_m13796_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 25, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13796_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t364_m13797_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13797_GM;
MethodInfo m13797_MI = 
{
	"set_Item", (methodPointerType)&m13797, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t364_m13797_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 26, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13797_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2565_0_0_0;
static ParameterInfo t364_m13798_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"hcp", 1, 134217728, &EmptyCustomAttributesCache, &t2565_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13798_GM;
MethodInfo m13798_MI = 
{
	"Init", (methodPointerType)&m13798, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t364_m13798_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13798_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t364_m13799_ParameterInfos[] = 
{
	{"size", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13799_GM;
MethodInfo m13799_MI = 
{
	"InitArrays", (methodPointerType)&m13799, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t364_m13799_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13799_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t364_m13800_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13800_GM;
MethodInfo m13800_MI = 
{
	"CopyToCheck", (methodPointerType)&m13800, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t364_m13800_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13800_GM};
extern Il2CppType t1957_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6693_0_0_0;
extern Il2CppType t6693_0_0_0;
static ParameterInfo t364_m27907_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1957_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6693_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27907_IGC;
extern TypeInfo m27907_gp_TRet_0_TI;
Il2CppGenericParamFull m27907_gp_TRet_0_TI_GenericParamFull = { { &m27907_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
extern TypeInfo m27907_gp_TElem_1_TI;
Il2CppGenericParamFull m27907_gp_TElem_1_TI_GenericParamFull = { { &m27907_IGC, 1}, {NULL, "TElem", 0, 0, NULL} };
static Il2CppGenericParamFull* m27907_IGPA[2] = 
{
	&m27907_gp_TRet_0_TI_GenericParamFull,
	&m27907_gp_TElem_1_TI_GenericParamFull,
};
extern MethodInfo m27907_MI;
Il2CppGenericContainer m27907_IGC = { { NULL, NULL }, NULL, &m27907_MI, 2, 1, m27907_IGPA };
extern Il2CppGenericMethod m27908_GM;
static Il2CppRGCTXDefinition m27907_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m27908_GM }/* Method Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_CLASS, &m9954_gp_1_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27907_GM;
MethodInfo m27907_MI = 
{
	"Do_CopyTo", NULL, &t364_TI, &t21_0_0_0, NULL, t364_m27907_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27907_RGCTXData, (methodPointerType)NULL, &m27907_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t364_m13801_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2569_0_0_0;
extern void* RuntimeInvoker_t2569_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13801_GM;
MethodInfo m13801_MI = 
{
	"make_pair", (methodPointerType)&m13801, &t364_TI, &t2569_0_0_0, RuntimeInvoker_t2569_t29_t44, t364_m13801_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13801_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t364_m13802_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13802_GM;
MethodInfo m13802_MI = 
{
	"pick_value", (methodPointerType)&m13802, &t364_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t44, t364_m13802_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13802_GM};
extern Il2CppType t2568_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t364_m13803_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2568_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13803_GM;
MethodInfo m13803_MI = 
{
	"CopyTo", (methodPointerType)&m13803, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t364_m13803_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13803_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6696_0_0_0;
extern Il2CppType t6696_0_0_0;
static ParameterInfo t364_m27909_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"transform", 2, 134217728, &EmptyCustomAttributesCache, &t6696_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m27909_IGC;
extern TypeInfo m27909_gp_TRet_0_TI;
Il2CppGenericParamFull m27909_gp_TRet_0_TI_GenericParamFull = { { &m27909_IGC, 0}, {NULL, "TRet", 0, 0, NULL} };
static Il2CppGenericParamFull* m27909_IGPA[1] = 
{
	&m27909_gp_TRet_0_TI_GenericParamFull,
};
extern MethodInfo m27909_MI;
Il2CppGenericContainer m27909_IGC = { { NULL, NULL }, NULL, &m27909_MI, 1, 1, m27909_IGPA };
extern Il2CppGenericMethod m27910_GM;
static Il2CppRGCTXDefinition m27909_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, &m9959_gp_0_0_0_0 }/* Type Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m27910_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppGenericMethod m27909_GM;
MethodInfo m27909_MI = 
{
	"Do_ICollectionCopyTo", NULL, &t364_TI, &t21_0_0_0, NULL, t364_m27909_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, true, true, 0, m27909_RGCTXData, (methodPointerType)NULL, &m27909_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13804_GM;
MethodInfo m13804_MI = 
{
	"Resize", (methodPointerType)&m13804, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13804_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t364_m13805_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13805_GM;
MethodInfo m13805_MI = 
{
	"Add", (methodPointerType)&m13805, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t364_m13805_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13805_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13806_GM;
MethodInfo m13806_MI = 
{
	"Clear", (methodPointerType)&m13806, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 13, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13806_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t364_m13807_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13807_GM;
MethodInfo m13807_MI = 
{
	"ContainsKey", (methodPointerType)&m13807, &t364_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t364_m13807_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13807_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t364_m13808_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13808_GM;
MethodInfo m13808_MI = 
{
	"ContainsValue", (methodPointerType)&m13808, &t364_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t364_m13808_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13808_GM};
extern Il2CppType t733_0_0_0;
extern Il2CppType t735_0_0_0;
static ParameterInfo t364_m13809_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t733_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &t735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13809_GM;
MethodInfo m13809_MI = 
{
	"GetObjectData", (methodPointerType)&m13809, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t735, t364_m13809_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13809_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t364_m13810_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13810_GM;
MethodInfo m13810_MI = 
{
	"OnDeserialization", (methodPointerType)&m13810, &t364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t364_m13810_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13810_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t364_m13811_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13811_GM;
MethodInfo m13811_MI = 
{
	"Remove", (methodPointerType)&m13811, &t364_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t364_m13811_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13811_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_1_0_2;
extern Il2CppType t44_1_0_0;
static ParameterInfo t364_m13812_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_1_0_2},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t390 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13812_GM;
MethodInfo m13812_MI = 
{
	"TryGetValue", (methodPointerType)&m13812, &t364_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t390, t364_m13812_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13812_GM};
extern Il2CppType t2567_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13813_GM;
MethodInfo m13813_MI = 
{
	"get_Values", (methodPointerType)&m13813, &t364_TI, &t2567_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13813_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t364_m13814_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13814_GM;
MethodInfo m13814_MI = 
{
	"ToTKey", (methodPointerType)&m13814, &t364_TI, &t155_0_0_0, RuntimeInvoker_t29_t29, t364_m13814_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13814_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t364_m13815_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13815_GM;
MethodInfo m13815_MI = 
{
	"ToTValue", (methodPointerType)&m13815, &t364_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t364_m13815_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13815_GM};
extern Il2CppType t2569_0_0_0;
static ParameterInfo t364_m13816_ParameterInfos[] = 
{
	{"pair", 0, 134217728, &EmptyCustomAttributesCache, &t2569_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13816_GM;
MethodInfo m13816_MI = 
{
	"ContainsKeyValuePair", (methodPointerType)&m13816, &t364_TI, &t40_0_0_0, RuntimeInvoker_t40_t2569, t364_m13816_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13816_GM};
extern Il2CppType t2571_0_0_0;
extern void* RuntimeInvoker_t2571 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13817_GM;
MethodInfo m13817_MI = 
{
	"GetEnumerator", (methodPointerType)&m13817, &t364_TI, &t2571_0_0_0, RuntimeInvoker_t2571, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13817_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t364_m13818_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
extern Il2CppGenericMethod m13818_GM;
MethodInfo m13818_MI = 
{
	"<CopyTo>m__0", (methodPointerType)&m13818, &t364_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t44, t364_m13818_ParameterInfos, &t1254__CustomAttributeCache_m9975, 145, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13818_GM};
static MethodInfo* t364_MIs[] =
{
	&m13776_MI,
	&m13777_MI,
	&m13778_MI,
	&m13779_MI,
	&m13780_MI,
	&m13781_MI,
	&m13782_MI,
	&m13783_MI,
	&m13784_MI,
	&m13785_MI,
	&m13786_MI,
	&m13787_MI,
	&m13788_MI,
	&m13789_MI,
	&m13790_MI,
	&m13791_MI,
	&m13792_MI,
	&m13793_MI,
	&m13794_MI,
	&m13795_MI,
	&m13796_MI,
	&m13797_MI,
	&m13798_MI,
	&m13799_MI,
	&m13800_MI,
	&m27907_MI,
	&m13801_MI,
	&m13802_MI,
	&m13803_MI,
	&m27909_MI,
	&m13804_MI,
	&m13805_MI,
	&m13806_MI,
	&m13807_MI,
	&m13808_MI,
	&m13809_MI,
	&m13810_MI,
	&m13811_MI,
	&m13812_MI,
	&m13813_MI,
	&m13814_MI,
	&m13815_MI,
	&m13816_MI,
	&m13817_MI,
	&m13818_MI,
	NULL
};
static MethodInfo* t364_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13792_MI,
	&m13809_MI,
	&m13795_MI,
	&m13784_MI,
	&m13785_MI,
	&m13791_MI,
	&m13795_MI,
	&m13786_MI,
	&m13787_MI,
	&m13806_MI,
	&m13788_MI,
	&m13789_MI,
	&m13790_MI,
	&m13793_MI,
	&m13811_MI,
	&m13780_MI,
	&m13781_MI,
	&m13782_MI,
	&m13794_MI,
	&m13783_MI,
	&m13810_MI,
	&m13796_MI,
	&m13797_MI,
	&m13805_MI,
	&m13807_MI,
	&m13809_MI,
	&m13810_MI,
	&m13812_MI,
};
extern TypeInfo t5331_TI;
extern TypeInfo t5333_TI;
extern TypeInfo t6698_TI;
static TypeInfo* t364_ITIs[] = 
{
	&t603_TI,
	&t374_TI,
	&t674_TI,
	&t5331_TI,
	&t5333_TI,
	&t6698_TI,
	&t721_TI,
	&t918_TI,
};
static Il2CppInterfaceOffsetPair t364_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t374_TI, 5},
	{ &t674_TI, 6},
	{ &t5331_TI, 10},
	{ &t5333_TI, 17},
	{ &t6698_TI, 18},
	{ &t721_TI, 19},
	{ &t918_TI, 24},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t364_0_0_0;
extern Il2CppType t364_1_0_0;
struct t364;
extern Il2CppGenericClass t364_GC;
extern CustomAttributesCache t1254__CustomAttributeCache;
extern CustomAttributesCache t1254__CustomAttributeCache_U3CU3Ef__am$cacheB;
extern CustomAttributesCache t1254__CustomAttributeCache_m9975;
TypeInfo t364_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Dictionary`2", "System.Collections.Generic", t364_MIs, t364_PIs, t364_FIs, NULL, &t29_TI, NULL, NULL, &t364_TI, t364_ITIs, t364_VT, &t1254__CustomAttributeCache, &t364_TI, &t364_0_0_0, &t364_1_0_0, t364_IOs, &t364_GC, NULL, t364_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t364), 0, -1, sizeof(t364_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 45, 7, 16, 0, 0, 32, 8, 8};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>>
extern MethodInfo m27911_MI;
static PropertyInfo t5331____Count_PropertyInfo = 
{
	&t5331_TI, "Count", &m27911_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27912_MI;
static PropertyInfo t5331____IsReadOnly_PropertyInfo = 
{
	&t5331_TI, "IsReadOnly", &m27912_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5331_PIs[] =
{
	&t5331____Count_PropertyInfo,
	&t5331____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27911_GM;
MethodInfo m27911_MI = 
{
	"get_Count", NULL, &t5331_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27911_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27912_GM;
MethodInfo m27912_MI = 
{
	"get_IsReadOnly", NULL, &t5331_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27912_GM};
extern Il2CppType t2569_0_0_0;
static ParameterInfo t5331_m27913_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2569_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27913_GM;
MethodInfo m27913_MI = 
{
	"Add", NULL, &t5331_TI, &t21_0_0_0, RuntimeInvoker_t21_t2569, t5331_m27913_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27913_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27914_GM;
MethodInfo m27914_MI = 
{
	"Clear", NULL, &t5331_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27914_GM};
extern Il2CppType t2569_0_0_0;
static ParameterInfo t5331_m27915_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2569_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27915_GM;
MethodInfo m27915_MI = 
{
	"Contains", NULL, &t5331_TI, &t40_0_0_0, RuntimeInvoker_t40_t2569, t5331_m27915_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27915_GM};
extern Il2CppType t2568_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5331_m27916_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2568_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27916_GM;
MethodInfo m27916_MI = 
{
	"CopyTo", NULL, &t5331_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5331_m27916_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27916_GM};
extern Il2CppType t2569_0_0_0;
static ParameterInfo t5331_m27917_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2569_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27917_GM;
MethodInfo m27917_MI = 
{
	"Remove", NULL, &t5331_TI, &t40_0_0_0, RuntimeInvoker_t40_t2569, t5331_m27917_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27917_GM};
static MethodInfo* t5331_MIs[] =
{
	&m27911_MI,
	&m27912_MI,
	&m27913_MI,
	&m27914_MI,
	&m27915_MI,
	&m27916_MI,
	&m27917_MI,
	NULL
};
static TypeInfo* t5331_ITIs[] = 
{
	&t603_TI,
	&t5333_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5331_0_0_0;
extern Il2CppType t5331_1_0_0;
struct t5331;
extern Il2CppGenericClass t5331_GC;
TypeInfo t5331_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5331_MIs, t5331_PIs, NULL, NULL, NULL, NULL, NULL, &t5331_TI, t5331_ITIs, NULL, &EmptyCustomAttributesCache, &t5331_TI, &t5331_0_0_0, &t5331_1_0_0, NULL, &t5331_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>>
extern Il2CppType t2570_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27918_GM;
MethodInfo m27918_MI = 
{
	"GetEnumerator", NULL, &t5333_TI, &t2570_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27918_GM};
static MethodInfo* t5333_MIs[] =
{
	&m27918_MI,
	NULL
};
static TypeInfo* t5333_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5333_0_0_0;
extern Il2CppType t5333_1_0_0;
struct t5333;
extern Il2CppGenericClass t5333_GC;
TypeInfo t5333_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5333_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5333_TI, t5333_ITIs, NULL, &EmptyCustomAttributesCache, &t5333_TI, &t5333_0_0_0, &t5333_1_0_0, NULL, &t5333_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2570_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>>
extern MethodInfo m27919_MI;
static PropertyInfo t2570____Current_PropertyInfo = 
{
	&t2570_TI, "Current", &m27919_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2570_PIs[] =
{
	&t2570____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2569_0_0_0;
extern void* RuntimeInvoker_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27919_GM;
MethodInfo m27919_MI = 
{
	"get_Current", NULL, &t2570_TI, &t2569_0_0_0, RuntimeInvoker_t2569, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27919_GM};
static MethodInfo* t2570_MIs[] =
{
	&m27919_MI,
	NULL
};
static TypeInfo* t2570_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2570_0_0_0;
extern Il2CppType t2570_1_0_0;
struct t2570;
extern Il2CppGenericClass t2570_GC;
TypeInfo t2570_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2570_MIs, t2570_PIs, NULL, NULL, NULL, NULL, NULL, &t2570_TI, t2570_ITIs, NULL, &EmptyCustomAttributesCache, &t2570_TI, &t2570_0_0_0, &t2570_1_0_0, NULL, &t2570_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13821_MI;
extern MethodInfo m13823_MI;


 void m13819 (t2569 * __this, t155 * p0, int32_t p1, MethodInfo* method){
	{
		m13821(__this, p0, &m13821_MI);
		m13823(__this, p1, &m13823_MI);
		return;
	}
}
 t155 * m13820 (t2569 * __this, MethodInfo* method){
	{
		t155 * L_0 = (__this->f0);
		return L_0;
	}
}
 void m13821 (t2569 * __this, t155 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
 int32_t m13822 (t2569 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
 void m13823 (t2569 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
extern MethodInfo m13824_MI;
 t7* m13824 (t2569 * __this, MethodInfo* method){
	t155 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	t446* G_B2_1 = {0};
	t446* G_B2_2 = {0};
	int32_t G_B1_0 = 0;
	t446* G_B1_1 = {0};
	t446* G_B1_2 = {0};
	t7* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	t446* G_B3_2 = {0};
	t446* G_B3_3 = {0};
	int32_t G_B5_0 = 0;
	t446* G_B5_1 = {0};
	t446* G_B5_2 = {0};
	int32_t G_B4_0 = 0;
	t446* G_B4_1 = {0};
	t446* G_B4_2 = {0};
	t7* G_B6_0 = {0};
	int32_t G_B6_1 = 0;
	t446* G_B6_2 = {0};
	t446* G_B6_3 = {0};
	{
		t446* L_0 = ((t446*)SZArrayNew(InitializedTypeInfo(&t446_TI), 5));
		ArrayElementTypeCheck (L_0, (t7*) &_stringLiteral175);
		*((t7**)(t7**)SZArrayLdElema(L_0, 0)) = (t7*)(t7*) &_stringLiteral175;
		t446* L_1 = L_0;
		t155 * L_2 = m13820(__this, &m13820_MI);
		t155 * L_3 = L_2;
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!((t155 *)L_3))
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0033;
		}
	}
	{
		t155 * L_4 = m13820(__this, &m13820_MI);
		V_0 = L_4;
		t7* L_5 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, (*(&V_0)));
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_0038;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B3_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_0038:
	{
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((t7**)(t7**)SZArrayLdElema(G_B3_2, G_B3_1)) = (t7*)G_B3_0;
		t446* L_6 = G_B3_3;
		ArrayElementTypeCheck (L_6, (t7*) &_stringLiteral184);
		*((t7**)(t7**)SZArrayLdElema(L_6, 2)) = (t7*)(t7*) &_stringLiteral184;
		t446* L_7 = L_6;
		int32_t L_8 = m13822(__this, &m13822_MI);
		int32_t L_9 = L_8;
		t29 * L_10 = Box(InitializedTypeInfo(&t44_TI), &L_9);
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_10)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0066;
		}
	}
	{
		int32_t L_11 = m13822(__this, &m13822_MI);
		V_1 = L_11;
		t7* L_12 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m1332_MI, Box(InitializedTypeInfo(&t44_TI), &(*(&V_1))));
		G_B6_0 = L_12;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_006b;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		G_B6_0 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_006b:
	{
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		*((t7**)(t7**)SZArrayLdElema(G_B6_2, G_B6_1)) = (t7*)G_B6_0;
		t446* L_13 = G_B6_3;
		ArrayElementTypeCheck (L_13, (t7*) &_stringLiteral176);
		*((t7**)(t7**)SZArrayLdElema(L_13, 4)) = (t7*)(t7*) &_stringLiteral176;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_14 = m4008(NULL, L_13, &m4008_MI);
		return L_14;
	}
}
// Metadata Definition System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>
extern Il2CppType t155_0_0_1;
FieldInfo t2569_f0_FieldInfo = 
{
	"key", &t155_0_0_1, &t2569_TI, offsetof(t2569, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2569_f1_FieldInfo = 
{
	"value", &t44_0_0_1, &t2569_TI, offsetof(t2569, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2569_FIs[] =
{
	&t2569_f0_FieldInfo,
	&t2569_f1_FieldInfo,
	NULL
};
static PropertyInfo t2569____Key_PropertyInfo = 
{
	&t2569_TI, "Key", &m13820_MI, &m13821_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2569____Value_PropertyInfo = 
{
	&t2569_TI, "Value", &m13822_MI, &m13823_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2569_PIs[] =
{
	&t2569____Key_PropertyInfo,
	&t2569____Value_PropertyInfo,
	NULL
};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2569_m13819_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13819_GM;
MethodInfo m13819_MI = 
{
	".ctor", (methodPointerType)&m13819, &t2569_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2569_m13819_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13819_GM};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13820_GM;
MethodInfo m13820_MI = 
{
	"get_Key", (methodPointerType)&m13820, &t2569_TI, &t155_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13820_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2569_m13821_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13821_GM;
MethodInfo m13821_MI = 
{
	"set_Key", (methodPointerType)&m13821, &t2569_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2569_m13821_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13821_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13822_GM;
MethodInfo m13822_MI = 
{
	"get_Value", (methodPointerType)&m13822, &t2569_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13822_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2569_m13823_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13823_GM;
MethodInfo m13823_MI = 
{
	"set_Value", (methodPointerType)&m13823, &t2569_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2569_m13823_ParameterInfos, &EmptyCustomAttributesCache, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13823_GM};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13824_GM;
MethodInfo m13824_MI = 
{
	"ToString", (methodPointerType)&m13824, &t2569_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13824_GM};
static MethodInfo* t2569_MIs[] =
{
	&m13819_MI,
	&m13820_MI,
	&m13821_MI,
	&m13822_MI,
	&m13823_MI,
	&m13824_MI,
	NULL
};
static MethodInfo* t2569_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m13824_MI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2569_1_0_0;
extern Il2CppGenericClass t2569_GC;
extern CustomAttributesCache t1259__CustomAttributeCache;
TypeInfo t2569_TI = 
{
	&g_mscorlib_dll_Image, NULL, "KeyValuePair`2", "System.Collections.Generic", t2569_MIs, t2569_PIs, t2569_FIs, NULL, &t110_TI, NULL, NULL, &t2569_TI, NULL, t2569_VT, &t1259__CustomAttributeCache, &t2569_TI, &t2569_0_0_0, &t2569_1_0_0, NULL, &t2569_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2569)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 2, 0, 0, 4, 0, 0};
#include "t2572.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2572_TI;
#include "t2572MD.h"

extern MethodInfo m13829_MI;
extern MethodInfo m21012_MI;
struct t20;
 t2569  m21012 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m13825_MI;
 void m13825 (t2572 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13826_MI;
 t29 * m13826 (t2572 * __this, MethodInfo* method){
	{
		t2569  L_0 = m13829(__this, &m13829_MI);
		t2569  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2569_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13827_MI;
 void m13827 (t2572 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13828_MI;
 bool m13828 (t2572 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t2569  m13829 (t2572 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t2569  L_8 = m21012(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21012_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>>
extern Il2CppType t20_0_0_1;
FieldInfo t2572_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2572_TI, offsetof(t2572, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2572_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2572_TI, offsetof(t2572, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2572_FIs[] =
{
	&t2572_f0_FieldInfo,
	&t2572_f1_FieldInfo,
	NULL
};
static PropertyInfo t2572____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2572_TI, "System.Collections.IEnumerator.Current", &m13826_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2572____Current_PropertyInfo = 
{
	&t2572_TI, "Current", &m13829_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2572_PIs[] =
{
	&t2572____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2572____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2572_m13825_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13825_GM;
MethodInfo m13825_MI = 
{
	".ctor", (methodPointerType)&m13825, &t2572_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2572_m13825_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13825_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13826_GM;
MethodInfo m13826_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13826, &t2572_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13826_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13827_GM;
MethodInfo m13827_MI = 
{
	"Dispose", (methodPointerType)&m13827, &t2572_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13827_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13828_GM;
MethodInfo m13828_MI = 
{
	"MoveNext", (methodPointerType)&m13828, &t2572_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13828_GM};
extern Il2CppType t2569_0_0_0;
extern void* RuntimeInvoker_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13829_GM;
MethodInfo m13829_MI = 
{
	"get_Current", (methodPointerType)&m13829, &t2572_TI, &t2569_0_0_0, RuntimeInvoker_t2569, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13829_GM};
static MethodInfo* t2572_MIs[] =
{
	&m13825_MI,
	&m13826_MI,
	&m13827_MI,
	&m13828_MI,
	&m13829_MI,
	NULL
};
static MethodInfo* t2572_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13826_MI,
	&m13828_MI,
	&m13827_MI,
	&m13829_MI,
};
static TypeInfo* t2572_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2570_TI,
};
static Il2CppInterfaceOffsetPair t2572_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2570_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2572_0_0_0;
extern Il2CppType t2572_1_0_0;
extern Il2CppGenericClass t2572_GC;
TypeInfo t2572_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2572_MIs, t2572_PIs, t2572_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2572_TI, t2572_ITIs, t2572_VT, &EmptyCustomAttributesCache, &t2572_TI, &t2572_0_0_0, &t2572_1_0_0, t2572_IOs, &t2572_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2572)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5332_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>>
extern MethodInfo m27920_MI;
extern MethodInfo m27921_MI;
static PropertyInfo t5332____Item_PropertyInfo = 
{
	&t5332_TI, "Item", &m27920_MI, &m27921_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5332_PIs[] =
{
	&t5332____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2569_0_0_0;
static ParameterInfo t5332_m27922_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2569_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27922_GM;
MethodInfo m27922_MI = 
{
	"IndexOf", NULL, &t5332_TI, &t44_0_0_0, RuntimeInvoker_t44_t2569, t5332_m27922_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27922_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2569_0_0_0;
static ParameterInfo t5332_m27923_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2569_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27923_GM;
MethodInfo m27923_MI = 
{
	"Insert", NULL, &t5332_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2569, t5332_m27923_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27923_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5332_m27924_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27924_GM;
MethodInfo m27924_MI = 
{
	"RemoveAt", NULL, &t5332_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5332_m27924_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27924_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5332_m27920_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2569_0_0_0;
extern void* RuntimeInvoker_t2569_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27920_GM;
MethodInfo m27920_MI = 
{
	"get_Item", NULL, &t5332_TI, &t2569_0_0_0, RuntimeInvoker_t2569_t44, t5332_m27920_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27920_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2569_0_0_0;
static ParameterInfo t5332_m27921_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2569_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27921_GM;
MethodInfo m27921_MI = 
{
	"set_Item", NULL, &t5332_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2569, t5332_m27921_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27921_GM};
static MethodInfo* t5332_MIs[] =
{
	&m27922_MI,
	&m27923_MI,
	&m27924_MI,
	&m27920_MI,
	&m27921_MI,
	NULL
};
static TypeInfo* t5332_ITIs[] = 
{
	&t603_TI,
	&t5331_TI,
	&t5333_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5332_0_0_0;
extern Il2CppType t5332_1_0_0;
struct t5332;
extern Il2CppGenericClass t5332_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5332_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5332_MIs, t5332_PIs, NULL, NULL, NULL, NULL, NULL, &t5332_TI, t5332_ITIs, NULL, &t1908__CustomAttributeCache, &t5332_TI, &t5332_0_0_0, &t5332_1_0_0, NULL, &t5332_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IDictionary`2<UnityEngine.UI.Graphic,System.Int32>
extern Il2CppType t155_0_0_0;
static ParameterInfo t6698_m27925_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27925_GM;
MethodInfo m27925_MI = 
{
	"Remove", NULL, &t6698_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6698_m27925_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27925_GM};
static MethodInfo* t6698_MIs[] =
{
	&m27925_MI,
	NULL
};
static TypeInfo* t6698_ITIs[] = 
{
	&t603_TI,
	&t5331_TI,
	&t5333_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6698_0_0_0;
extern Il2CppType t6698_1_0_0;
struct t6698;
extern Il2CppGenericClass t6698_GC;
extern CustomAttributesCache t1975__CustomAttributeCache;
TypeInfo t6698_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDictionary`2", "System.Collections.Generic", t6698_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6698_TI, t6698_ITIs, NULL, &t1975__CustomAttributeCache, &t6698_TI, &t6698_0_0_0, &t6698_1_0_0, NULL, &t6698_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2573.h"
#include "t2574.h"
extern TypeInfo t2573_TI;
extern TypeInfo t2574_TI;
#include "t2574MD.h"
#include "t2573MD.h"
extern MethodInfo m13842_MI;
extern MethodInfo m13841_MI;
extern MethodInfo m13861_MI;
extern MethodInfo m21023_MI;
extern MethodInfo m21024_MI;
extern MethodInfo m13844_MI;
struct t364;
 void m21023 (t364 * __this, t20 * p0, int32_t p1, t2574 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t364;
 void m21024 (t364 * __this, t841* p0, int32_t p1, t2574 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m13830 (t2567 * __this, t364 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1173, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m13831_MI;
 void m13831 (t2567 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13832_MI;
 void m13832 (t2567 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13833_MI;
 bool m13833 (t2567 * __this, int32_t p0, MethodInfo* method){
	{
		t364 * L_0 = (__this->f0);
		bool L_1 = m13808(L_0, p0, &m13808_MI);
		return L_1;
	}
}
extern MethodInfo m13834_MI;
 bool m13834 (t2567 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral1174, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13835_MI;
 t29* m13835 (t2567 * __this, MethodInfo* method){
	{
		t2573  L_0 = m13842(__this, &m13842_MI);
		t2573  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2573_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m13836_MI;
 void m13836 (t2567 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	t841* V_0 = {0};
	{
		V_0 = ((t841*)IsInst(p0, InitializedTypeInfo(&t841_TI)));
		if (!V_0)
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker2< t841*, int32_t >::Invoke(&m13841_MI, __this, V_0, p1);
		return;
	}

IL_0013:
	{
		t364 * L_0 = (__this->f0);
		m13800(L_0, p0, p1, &m13800_MI);
		t364 * L_1 = (__this->f0);
		t35 L_2 = { &m13802_MI };
		t2574 * L_3 = (t2574 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2574_TI));
		m13861(L_3, NULL, L_2, &m13861_MI);
		m21023(L_1, p0, p1, L_3, &m21023_MI);
		return;
	}
}
extern MethodInfo m13837_MI;
 t29 * m13837 (t2567 * __this, MethodInfo* method){
	{
		t2573  L_0 = m13842(__this, &m13842_MI);
		t2573  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2573_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m13838_MI;
 bool m13838 (t2567 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m13839_MI;
 bool m13839 (t2567 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m13840_MI;
 t29 * m13840 (t2567 * __this, MethodInfo* method){
	{
		t364 * L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, L_0);
		return L_1;
	}
}
 void m13841 (t2567 * __this, t841* p0, int32_t p1, MethodInfo* method){
	{
		t364 * L_0 = (__this->f0);
		m13800(L_0, (t20 *)(t20 *)p0, p1, &m13800_MI);
		t364 * L_1 = (__this->f0);
		t35 L_2 = { &m13802_MI };
		t2574 * L_3 = (t2574 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2574_TI));
		m13861(L_3, NULL, L_2, &m13861_MI);
		m21024(L_1, p0, p1, L_3, &m21024_MI);
		return;
	}
}
 t2573  m13842 (t2567 * __this, MethodInfo* method){
	{
		t364 * L_0 = (__this->f0);
		t2573  L_1 = {0};
		m13844(&L_1, L_0, &m13844_MI);
		return L_1;
	}
}
extern MethodInfo m13843_MI;
 int32_t m13843 (t2567 * __this, MethodInfo* method){
	{
		t364 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m13795_MI, L_0);
		return L_1;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.UI.Graphic,System.Int32>
extern Il2CppType t364_0_0_1;
FieldInfo t2567_f0_FieldInfo = 
{
	"dictionary", &t364_0_0_1, &t2567_TI, offsetof(t2567, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2567_FIs[] =
{
	&t2567_f0_FieldInfo,
	NULL
};
static PropertyInfo t2567____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo = 
{
	&t2567_TI, "System.Collections.Generic.ICollection<TValue>.IsReadOnly", &m13838_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2567____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2567_TI, "System.Collections.ICollection.IsSynchronized", &m13839_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2567____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2567_TI, "System.Collections.ICollection.SyncRoot", &m13840_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2567____Count_PropertyInfo = 
{
	&t2567_TI, "Count", &m13843_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2567_PIs[] =
{
	&t2567____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo,
	&t2567____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2567____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2567____Count_PropertyInfo,
	NULL
};
extern Il2CppType t364_0_0_0;
static ParameterInfo t2567_m13830_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t364_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13830_GM;
MethodInfo m13830_MI = 
{
	".ctor", (methodPointerType)&m13830, &t2567_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2567_m13830_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13830_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2567_m13831_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13831_GM;
MethodInfo m13831_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Add", (methodPointerType)&m13831, &t2567_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2567_m13831_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13831_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13832_GM;
MethodInfo m13832_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Clear", (methodPointerType)&m13832, &t2567_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13832_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2567_m13833_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13833_GM;
MethodInfo m13833_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Contains", (methodPointerType)&m13833, &t2567_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t2567_m13833_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13833_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2567_m13834_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13834_GM;
MethodInfo m13834_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Remove", (methodPointerType)&m13834, &t2567_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t2567_m13834_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13834_GM};
extern Il2CppType t2424_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13835_GM;
MethodInfo m13835_MI = 
{
	"System.Collections.Generic.IEnumerable<TValue>.GetEnumerator", (methodPointerType)&m13835, &t2567_TI, &t2424_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13835_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2567_m13836_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13836_GM;
MethodInfo m13836_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m13836, &t2567_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2567_m13836_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13836_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13837_GM;
MethodInfo m13837_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m13837, &t2567_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13837_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13838_GM;
MethodInfo m13838_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.get_IsReadOnly", (methodPointerType)&m13838, &t2567_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13838_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13839_GM;
MethodInfo m13839_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m13839, &t2567_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13839_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13840_GM;
MethodInfo m13840_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m13840, &t2567_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13840_GM};
extern Il2CppType t841_0_0_0;
extern Il2CppType t841_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2567_m13841_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t841_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13841_GM;
MethodInfo m13841_MI = 
{
	"CopyTo", (methodPointerType)&m13841, &t2567_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2567_m13841_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13841_GM};
extern Il2CppType t2573_0_0_0;
extern void* RuntimeInvoker_t2573 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13842_GM;
MethodInfo m13842_MI = 
{
	"GetEnumerator", (methodPointerType)&m13842, &t2567_TI, &t2573_0_0_0, RuntimeInvoker_t2573, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13842_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13843_GM;
MethodInfo m13843_MI = 
{
	"get_Count", (methodPointerType)&m13843, &t2567_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13843_GM};
static MethodInfo* t2567_MIs[] =
{
	&m13830_MI,
	&m13831_MI,
	&m13832_MI,
	&m13833_MI,
	&m13834_MI,
	&m13835_MI,
	&m13836_MI,
	&m13837_MI,
	&m13838_MI,
	&m13839_MI,
	&m13840_MI,
	&m13841_MI,
	&m13842_MI,
	&m13843_MI,
	NULL
};
static MethodInfo* t2567_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13837_MI,
	&m13843_MI,
	&m13839_MI,
	&m13840_MI,
	&m13836_MI,
	&m13843_MI,
	&m13838_MI,
	&m13831_MI,
	&m13832_MI,
	&m13833_MI,
	&m13841_MI,
	&m13834_MI,
	&m13835_MI,
};
extern TypeInfo t5140_TI;
extern TypeInfo t5142_TI;
static TypeInfo* t2567_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t5140_TI,
	&t5142_TI,
};
static Il2CppInterfaceOffsetPair t2567_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t5140_TI, 9},
	{ &t5142_TI, 16},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2567_0_0_0;
extern Il2CppType t2567_1_0_0;
struct t2567;
extern Il2CppGenericClass t2567_GC;
extern TypeInfo t1254_TI;
extern CustomAttributesCache t1252__CustomAttributeCache;
TypeInfo t2567_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueCollection", "", t2567_MIs, t2567_PIs, t2567_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2567_TI, t2567_ITIs, t2567_VT, &t1252__CustomAttributeCache, &t2567_TI, &t2567_0_0_0, &t2567_1_0_0, t2567_IOs, &t2567_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2567), 0, -1, 0, 0, -1, 1057026, 0, false, false, false, false, true, false, false, false, false, false, false, false, 14, 4, 1, 0, 0, 17, 4, 4};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13857_MI;
extern MethodInfo m13860_MI;
extern MethodInfo m13854_MI;


 void m13844 (t2573 * __this, t364 * p0, MethodInfo* method){
	{
		t2571  L_0 = m13817(p0, &m13817_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m13845_MI;
 t29 * m13845 (t2573 * __this, MethodInfo* method){
	{
		t2571 * L_0 = &(__this->f0);
		int32_t L_1 = m13857(L_0, &m13857_MI);
		int32_t L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t44_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m13846_MI;
 void m13846 (t2573 * __this, MethodInfo* method){
	{
		t2571 * L_0 = &(__this->f0);
		m13860(L_0, &m13860_MI);
		return;
	}
}
extern MethodInfo m13847_MI;
 bool m13847 (t2573 * __this, MethodInfo* method){
	{
		t2571 * L_0 = &(__this->f0);
		bool L_1 = m13854(L_0, &m13854_MI);
		return L_1;
	}
}
extern MethodInfo m13848_MI;
 int32_t m13848 (t2573 * __this, MethodInfo* method){
	{
		t2571 * L_0 = &(__this->f0);
		t2569 * L_1 = &(L_0->f3);
		int32_t L_2 = m13822(L_1, &m13822_MI);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.UI.Graphic,System.Int32>
extern Il2CppType t2571_0_0_1;
FieldInfo t2573_f0_FieldInfo = 
{
	"host_enumerator", &t2571_0_0_1, &t2573_TI, offsetof(t2573, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2573_FIs[] =
{
	&t2573_f0_FieldInfo,
	NULL
};
static PropertyInfo t2573____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2573_TI, "System.Collections.IEnumerator.Current", &m13845_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2573____Current_PropertyInfo = 
{
	&t2573_TI, "Current", &m13848_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2573_PIs[] =
{
	&t2573____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2573____Current_PropertyInfo,
	NULL
};
extern Il2CppType t364_0_0_0;
static ParameterInfo t2573_m13844_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t364_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13844_GM;
MethodInfo m13844_MI = 
{
	".ctor", (methodPointerType)&m13844, &t2573_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2573_m13844_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13844_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13845_GM;
MethodInfo m13845_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13845, &t2573_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13845_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13846_GM;
MethodInfo m13846_MI = 
{
	"Dispose", (methodPointerType)&m13846, &t2573_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13846_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13847_GM;
MethodInfo m13847_MI = 
{
	"MoveNext", (methodPointerType)&m13847, &t2573_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13847_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13848_GM;
MethodInfo m13848_MI = 
{
	"get_Current", (methodPointerType)&m13848, &t2573_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13848_GM};
static MethodInfo* t2573_MIs[] =
{
	&m13844_MI,
	&m13845_MI,
	&m13846_MI,
	&m13847_MI,
	&m13848_MI,
	NULL
};
static MethodInfo* t2573_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13845_MI,
	&m13847_MI,
	&m13846_MI,
	&m13848_MI,
};
extern TypeInfo t2424_TI;
static TypeInfo* t2573_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2424_TI,
};
static Il2CppInterfaceOffsetPair t2573_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2424_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2573_0_0_0;
extern Il2CppType t2573_1_0_0;
extern Il2CppGenericClass t2573_GC;
extern TypeInfo t1252_TI;
TypeInfo t2573_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2573_MIs, t2573_PIs, t2573_FIs, NULL, &t110_TI, NULL, &t1252_TI, &t2573_TI, t2573_ITIs, t2573_VT, &EmptyCustomAttributesCache, &t2573_TI, &t2573_0_0_0, &t2573_1_0_0, t2573_IOs, &t2573_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2573)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13859_MI;
extern MethodInfo m13856_MI;
extern MethodInfo m13858_MI;


 void m13849 (t2571 * __this, t364 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f14);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m13850_MI;
 t29 * m13850 (t2571 * __this, MethodInfo* method){
	{
		m13859(__this, &m13859_MI);
		t2569  L_0 = (__this->f3);
		t2569  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2569_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13851_MI;
 t725  m13851 (t2571 * __this, MethodInfo* method){
	{
		m13859(__this, &m13859_MI);
		t2569 * L_0 = &(__this->f3);
		t155 * L_1 = m13820(L_0, &m13820_MI);
		t155 * L_2 = L_1;
		t2569 * L_3 = &(__this->f3);
		int32_t L_4 = m13822(L_3, &m13822_MI);
		int32_t L_5 = L_4;
		t29 * L_6 = Box(InitializedTypeInfo(&t44_TI), &L_5);
		t725  L_7 = {0};
		m3965(&L_7, ((t155 *)L_2), L_6, &m3965_MI);
		return L_7;
	}
}
extern MethodInfo m13852_MI;
 t29 * m13852 (t2571 * __this, MethodInfo* method){
	{
		t155 * L_0 = m13856(__this, &m13856_MI);
		t155 * L_1 = L_0;
		return ((t155 *)L_1);
	}
}
extern MethodInfo m13853_MI;
 t29 * m13853 (t2571 * __this, MethodInfo* method){
	{
		int32_t L_0 = m13857(__this, &m13857_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t44_TI), &L_1);
		return L_2;
	}
}
 bool m13854 (t2571 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		m13858(__this, &m13858_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		goto IL_0072;
	}

IL_0013:
	{
		int32_t L_1 = (__this->f1);
		int32_t L_2 = L_1;
		V_1 = L_2;
		__this->f1 = ((int32_t)(L_2+1));
		V_0 = V_1;
		t364 * L_3 = (__this->f0);
		t1965* L_4 = (L_3->f5);
		int32_t L_5 = (((t1248 *)(t1248 *)SZArrayLdElema(L_4, V_0))->f0);
		if (!((int32_t)((int32_t)L_5&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_0072;
		}
	}
	{
		t364 * L_6 = (__this->f0);
		t2545* L_7 = (L_6->f6);
		int32_t L_8 = V_0;
		t364 * L_9 = (__this->f0);
		t841* L_10 = (L_9->f7);
		int32_t L_11 = V_0;
		t2569  L_12 = {0};
		m13819(&L_12, (*(t155 **)(t155 **)SZArrayLdElema(L_7, L_8)), (*(int32_t*)(int32_t*)SZArrayLdElema(L_10, L_11)), &m13819_MI);
		__this->f3 = L_12;
		return 1;
	}

IL_0072:
	{
		int32_t L_13 = (__this->f1);
		t364 * L_14 = (__this->f0);
		int32_t L_15 = (L_14->f8);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0013;
		}
	}
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m13855_MI;
 t2569  m13855 (t2571 * __this, MethodInfo* method){
	{
		t2569  L_0 = (__this->f3);
		return L_0;
	}
}
 t155 * m13856 (t2571 * __this, MethodInfo* method){
	{
		m13859(__this, &m13859_MI);
		t2569 * L_0 = &(__this->f3);
		t155 * L_1 = m13820(L_0, &m13820_MI);
		return L_1;
	}
}
 int32_t m13857 (t2571 * __this, MethodInfo* method){
	{
		m13859(__this, &m13859_MI);
		t2569 * L_0 = &(__this->f3);
		int32_t L_1 = m13822(L_0, &m13822_MI);
		return L_1;
	}
}
 void m13858 (t2571 * __this, MethodInfo* method){
	{
		t364 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		t1101 * L_1 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_1, (t7*)NULL, &m5150_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t364 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f14);
		int32_t L_4 = (__this->f2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		t914 * L_5 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_5, (t7*) &_stringLiteral1171, &m3964_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_002d:
	{
		return;
	}
}
 void m13859 (t2571 * __this, MethodInfo* method){
	{
		m13858(__this, &m13858_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1172, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001a:
	{
		return;
	}
}
 void m13860 (t2571 * __this, MethodInfo* method){
	{
		__this->f0 = (t364 *)NULL;
		return;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>
extern Il2CppType t364_0_0_1;
FieldInfo t2571_f0_FieldInfo = 
{
	"dictionary", &t364_0_0_1, &t2571_TI, offsetof(t2571, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2571_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2571_TI, offsetof(t2571, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2571_f2_FieldInfo = 
{
	"stamp", &t44_0_0_1, &t2571_TI, offsetof(t2571, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t2569_0_0_3;
FieldInfo t2571_f3_FieldInfo = 
{
	"current", &t2569_0_0_3, &t2571_TI, offsetof(t2571, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2571_FIs[] =
{
	&t2571_f0_FieldInfo,
	&t2571_f1_FieldInfo,
	&t2571_f2_FieldInfo,
	&t2571_f3_FieldInfo,
	NULL
};
static PropertyInfo t2571____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2571_TI, "System.Collections.IEnumerator.Current", &m13850_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2571____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo = 
{
	&t2571_TI, "System.Collections.IDictionaryEnumerator.Entry", &m13851_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2571____System_Collections_IDictionaryEnumerator_Key_PropertyInfo = 
{
	&t2571_TI, "System.Collections.IDictionaryEnumerator.Key", &m13852_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2571____System_Collections_IDictionaryEnumerator_Value_PropertyInfo = 
{
	&t2571_TI, "System.Collections.IDictionaryEnumerator.Value", &m13853_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2571____Current_PropertyInfo = 
{
	&t2571_TI, "Current", &m13855_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2571____CurrentKey_PropertyInfo = 
{
	&t2571_TI, "CurrentKey", &m13856_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2571____CurrentValue_PropertyInfo = 
{
	&t2571_TI, "CurrentValue", &m13857_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2571_PIs[] =
{
	&t2571____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2571____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo,
	&t2571____System_Collections_IDictionaryEnumerator_Key_PropertyInfo,
	&t2571____System_Collections_IDictionaryEnumerator_Value_PropertyInfo,
	&t2571____Current_PropertyInfo,
	&t2571____CurrentKey_PropertyInfo,
	&t2571____CurrentValue_PropertyInfo,
	NULL
};
extern Il2CppType t364_0_0_0;
static ParameterInfo t2571_m13849_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t364_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13849_GM;
MethodInfo m13849_MI = 
{
	".ctor", (methodPointerType)&m13849, &t2571_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2571_m13849_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13849_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13850_GM;
MethodInfo m13850_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13850, &t2571_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13850_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13851_GM;
MethodInfo m13851_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Entry", (methodPointerType)&m13851, &t2571_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13851_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13852_GM;
MethodInfo m13852_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Key", (methodPointerType)&m13852, &t2571_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13852_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13853_GM;
MethodInfo m13853_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Value", (methodPointerType)&m13853, &t2571_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13853_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13854_GM;
MethodInfo m13854_MI = 
{
	"MoveNext", (methodPointerType)&m13854, &t2571_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13854_GM};
extern Il2CppType t2569_0_0_0;
extern void* RuntimeInvoker_t2569 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13855_GM;
MethodInfo m13855_MI = 
{
	"get_Current", (methodPointerType)&m13855, &t2571_TI, &t2569_0_0_0, RuntimeInvoker_t2569, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13855_GM};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13856_GM;
MethodInfo m13856_MI = 
{
	"get_CurrentKey", (methodPointerType)&m13856, &t2571_TI, &t155_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13856_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13857_GM;
MethodInfo m13857_MI = 
{
	"get_CurrentValue", (methodPointerType)&m13857, &t2571_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13857_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13858_GM;
MethodInfo m13858_MI = 
{
	"VerifyState", (methodPointerType)&m13858, &t2571_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13858_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13859_GM;
MethodInfo m13859_MI = 
{
	"VerifyCurrent", (methodPointerType)&m13859, &t2571_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13859_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13860_GM;
MethodInfo m13860_MI = 
{
	"Dispose", (methodPointerType)&m13860, &t2571_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13860_GM};
static MethodInfo* t2571_MIs[] =
{
	&m13849_MI,
	&m13850_MI,
	&m13851_MI,
	&m13852_MI,
	&m13853_MI,
	&m13854_MI,
	&m13855_MI,
	&m13856_MI,
	&m13857_MI,
	&m13858_MI,
	&m13859_MI,
	&m13860_MI,
	NULL
};
static MethodInfo* t2571_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13850_MI,
	&m13854_MI,
	&m13860_MI,
	&m13855_MI,
	&m13851_MI,
	&m13852_MI,
	&m13853_MI,
};
extern TypeInfo t722_TI;
static TypeInfo* t2571_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2570_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2571_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2570_TI, 7},
	{ &t722_TI, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2571_0_0_0;
extern Il2CppType t2571_1_0_0;
extern Il2CppGenericClass t2571_GC;
TypeInfo t2571_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2571_MIs, t2571_PIs, t2571_FIs, NULL, &t110_TI, NULL, &t1254_TI, &t2571_TI, t2571_ITIs, t2571_VT, &EmptyCustomAttributesCache, &t2571_TI, &t2571_0_0_0, &t2571_1_0_0, t2571_IOs, &t2571_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2571)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 12, 7, 4, 0, 0, 11, 4, 4};
#ifndef _MSC_VER
#else
#endif



 void m13861 (t2574 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m13862_MI;
 int32_t m13862 (t2574 * __this, t155 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m13862((t2574 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 *, t29 * __this, t155 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 * __this, t155 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m13863_MI;
 t29 * m13863 (t2574 * __this, t155 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t44_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m13864_MI;
 int32_t m13864 (t2574 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.Graphic,System.Int32,System.Int32>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2574_m13861_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13861_GM;
MethodInfo m13861_MI = 
{
	".ctor", (methodPointerType)&m13861, &t2574_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2574_m13861_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13861_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2574_m13862_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13862_GM;
MethodInfo m13862_MI = 
{
	"Invoke", (methodPointerType)&m13862, &t2574_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t44, t2574_m13862_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13862_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2574_m13863_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13863_GM;
MethodInfo m13863_MI = 
{
	"BeginInvoke", (methodPointerType)&m13863, &t2574_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t2574_m13863_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13863_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2574_m13864_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13864_GM;
MethodInfo m13864_MI = 
{
	"EndInvoke", (methodPointerType)&m13864, &t2574_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2574_m13864_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13864_GM};
static MethodInfo* t2574_MIs[] =
{
	&m13861_MI,
	&m13862_MI,
	&m13863_MI,
	&m13864_MI,
	NULL
};
static MethodInfo* t2574_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13862_MI,
	&m13863_MI,
	&m13864_MI,
};
static Il2CppInterfaceOffsetPair t2574_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2574_0_0_0;
extern Il2CppType t2574_1_0_0;
struct t2574;
extern Il2CppGenericClass t2574_GC;
TypeInfo t2574_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2574_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2574_TI, NULL, t2574_VT, &EmptyCustomAttributesCache, &t2574_TI, &t2574_0_0_0, &t2574_1_0_0, t2574_IOs, &t2574_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2574), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m13865 (t2566 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m13866_MI;
 t725  m13866 (t2566 * __this, t155 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m13866((t2566 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 *, t29 * __this, t155 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t155 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m13867_MI;
 t29 * m13867 (t2566 * __this, t155 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t44_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m13868_MI;
 t725  m13868 (t2566 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t725 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.Graphic,System.Int32,System.Collections.DictionaryEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2566_m13865_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13865_GM;
MethodInfo m13865_MI = 
{
	".ctor", (methodPointerType)&m13865, &t2566_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2566_m13865_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13865_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2566_m13866_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13866_GM;
MethodInfo m13866_MI = 
{
	"Invoke", (methodPointerType)&m13866, &t2566_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t44, t2566_m13866_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13866_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2566_m13867_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13867_GM;
MethodInfo m13867_MI = 
{
	"BeginInvoke", (methodPointerType)&m13867, &t2566_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t2566_m13867_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13867_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2566_m13868_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13868_GM;
MethodInfo m13868_MI = 
{
	"EndInvoke", (methodPointerType)&m13868, &t2566_TI, &t725_0_0_0, RuntimeInvoker_t725_t29, t2566_m13868_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13868_GM};
static MethodInfo* t2566_MIs[] =
{
	&m13865_MI,
	&m13866_MI,
	&m13867_MI,
	&m13868_MI,
	NULL
};
static MethodInfo* t2566_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13866_MI,
	&m13867_MI,
	&m13868_MI,
};
static Il2CppInterfaceOffsetPair t2566_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2566_0_0_0;
extern Il2CppType t2566_1_0_0;
struct t2566;
extern Il2CppGenericClass t2566_GC;
TypeInfo t2566_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2566_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2566_TI, NULL, t2566_VT, &EmptyCustomAttributesCache, &t2566_TI, &t2566_0_0_0, &t2566_1_0_0, t2566_IOs, &t2566_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2566), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m13869 (t2575 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m13870_MI;
 t2569  m13870 (t2575 * __this, t155 * p0, int32_t p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m13870((t2575 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t2569  (*FunctionPointerType) (t29 *, t29 * __this, t155 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t2569  (*FunctionPointerType) (t29 * __this, t155 * p0, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t2569  (*FunctionPointerType) (t29 * __this, int32_t p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m13871_MI;
 t29 * m13871 (t2575 * __this, t155 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = Box(InitializedTypeInfo(&t44_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m13872_MI;
 t2569  m13872 (t2575 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t2569 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.UI.Graphic,System.Int32,System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2575_m13869_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13869_GM;
MethodInfo m13869_MI = 
{
	".ctor", (methodPointerType)&m13869, &t2575_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2575_m13869_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13869_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2575_m13870_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2569_0_0_0;
extern void* RuntimeInvoker_t2569_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13870_GM;
MethodInfo m13870_MI = 
{
	"Invoke", (methodPointerType)&m13870, &t2575_TI, &t2569_0_0_0, RuntimeInvoker_t2569_t29_t44, t2575_m13870_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13870_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2575_m13871_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13871_GM;
MethodInfo m13871_MI = 
{
	"BeginInvoke", (methodPointerType)&m13871, &t2575_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t44_t29_t29, t2575_m13871_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13871_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2575_m13872_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t2569_0_0_0;
extern void* RuntimeInvoker_t2569_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13872_GM;
MethodInfo m13872_MI = 
{
	"EndInvoke", (methodPointerType)&m13872, &t2575_TI, &t2569_0_0_0, RuntimeInvoker_t2569_t29, t2575_m13872_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13872_GM};
static MethodInfo* t2575_MIs[] =
{
	&m13869_MI,
	&m13870_MI,
	&m13871_MI,
	&m13872_MI,
	NULL
};
static MethodInfo* t2575_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13870_MI,
	&m13871_MI,
	&m13872_MI,
};
static Il2CppInterfaceOffsetPair t2575_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2575_0_0_0;
extern Il2CppType t2575_1_0_0;
struct t2575;
extern Il2CppGenericClass t2575_GC;
TypeInfo t2575_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2575_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2575_TI, NULL, t2575_VT, &EmptyCustomAttributesCache, &t2575_TI, &t2575_0_0_0, &t2575_1_0_0, t2575_IOs, &t2575_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2575), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m10122_MI;
extern MethodInfo m13875_MI;


 void m13873 (t2576 * __this, t364 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		t2571  L_0 = m13817(p0, &m13817_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m13874_MI;
 bool m13874 (t2576 * __this, MethodInfo* method){
	{
		t2571 * L_0 = &(__this->f0);
		bool L_1 = m13854(L_0, &m13854_MI);
		return L_1;
	}
}
 t725  m13875 (t2576 * __this, MethodInfo* method){
	{
		t2571  L_0 = (__this->f0);
		t2571  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2571_TI), &L_1);
		t725  L_3 = (t725 )InterfaceFuncInvoker0< t725  >::Invoke(&m10122_MI, L_2);
		return L_3;
	}
}
extern MethodInfo m13876_MI;
 t29 * m13876 (t2576 * __this, MethodInfo* method){
	t2569  V_0 = {0};
	{
		t2571 * L_0 = &(__this->f0);
		t2569  L_1 = m13855(L_0, &m13855_MI);
		V_0 = L_1;
		t155 * L_2 = m13820((&V_0), &m13820_MI);
		t155 * L_3 = L_2;
		return ((t155 *)L_3);
	}
}
extern MethodInfo m13877_MI;
 t29 * m13877 (t2576 * __this, MethodInfo* method){
	t2569  V_0 = {0};
	{
		t2571 * L_0 = &(__this->f0);
		t2569  L_1 = m13855(L_0, &m13855_MI);
		V_0 = L_1;
		int32_t L_2 = m13822((&V_0), &m13822_MI);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		return L_4;
	}
}
extern MethodInfo m13878_MI;
 t29 * m13878 (t2576 * __this, MethodInfo* method){
	{
		t725  L_0 = (t725 )VirtFuncInvoker0< t725  >::Invoke(&m13875_MI, __this);
		t725  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t725_TI), &L_1);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.UI.Graphic,System.Int32>
extern Il2CppType t2571_0_0_1;
FieldInfo t2576_f0_FieldInfo = 
{
	"host_enumerator", &t2571_0_0_1, &t2576_TI, offsetof(t2576, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2576_FIs[] =
{
	&t2576_f0_FieldInfo,
	NULL
};
static PropertyInfo t2576____Entry_PropertyInfo = 
{
	&t2576_TI, "Entry", &m13875_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2576____Key_PropertyInfo = 
{
	&t2576_TI, "Key", &m13876_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2576____Value_PropertyInfo = 
{
	&t2576_TI, "Value", &m13877_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2576____Current_PropertyInfo = 
{
	&t2576_TI, "Current", &m13878_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2576_PIs[] =
{
	&t2576____Entry_PropertyInfo,
	&t2576____Key_PropertyInfo,
	&t2576____Value_PropertyInfo,
	&t2576____Current_PropertyInfo,
	NULL
};
extern Il2CppType t364_0_0_0;
static ParameterInfo t2576_m13873_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t364_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13873_GM;
MethodInfo m13873_MI = 
{
	".ctor", (methodPointerType)&m13873, &t2576_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2576_m13873_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13873_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13874_GM;
MethodInfo m13874_MI = 
{
	"MoveNext", (methodPointerType)&m13874, &t2576_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13874_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13875_GM;
MethodInfo m13875_MI = 
{
	"get_Entry", (methodPointerType)&m13875, &t2576_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13875_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13876_GM;
MethodInfo m13876_MI = 
{
	"get_Key", (methodPointerType)&m13876, &t2576_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13876_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13877_GM;
MethodInfo m13877_MI = 
{
	"get_Value", (methodPointerType)&m13877, &t2576_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13877_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13878_GM;
MethodInfo m13878_MI = 
{
	"get_Current", (methodPointerType)&m13878, &t2576_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13878_GM};
static MethodInfo* t2576_MIs[] =
{
	&m13873_MI,
	&m13874_MI,
	&m13875_MI,
	&m13876_MI,
	&m13877_MI,
	&m13878_MI,
	NULL
};
static MethodInfo* t2576_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13878_MI,
	&m13874_MI,
	&m13875_MI,
	&m13876_MI,
	&m13877_MI,
};
static TypeInfo* t2576_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2576_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2576_0_0_0;
extern Il2CppType t2576_1_0_0;
struct t2576;
extern Il2CppGenericClass t2576_GC;
TypeInfo t2576_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ShimEnumerator", "", t2576_MIs, t2576_PIs, t2576_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2576_TI, t2576_ITIs, t2576_VT, &EmptyCustomAttributesCache, &t2576_TI, &t2576_0_0_0, &t2576_1_0_0, t2576_IOs, &t2576_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2576), 0, -1, 0, 0, -1, 1056771, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 4, 1, 0, 0, 9, 2, 2};
#include "t2577.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2577_TI;
#include "t2577MD.h"

extern MethodInfo m13883_MI;
extern MethodInfo m21030_MI;
struct t20;
 t2562  m21030 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m13879_MI;
 void m13879 (t2577 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13880_MI;
 t29 * m13880 (t2577 * __this, MethodInfo* method){
	{
		t2562  L_0 = m13883(__this, &m13883_MI);
		t2562  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2562_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13881_MI;
 void m13881 (t2577 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13882_MI;
 bool m13882 (t2577 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t2562  m13883 (t2577 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t2562  L_8 = m21030(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21030_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>>
extern Il2CppType t20_0_0_1;
FieldInfo t2577_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2577_TI, offsetof(t2577, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2577_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2577_TI, offsetof(t2577, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2577_FIs[] =
{
	&t2577_f0_FieldInfo,
	&t2577_f1_FieldInfo,
	NULL
};
static PropertyInfo t2577____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2577_TI, "System.Collections.IEnumerator.Current", &m13880_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2577____Current_PropertyInfo = 
{
	&t2577_TI, "Current", &m13883_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2577_PIs[] =
{
	&t2577____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2577____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2577_m13879_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13879_GM;
MethodInfo m13879_MI = 
{
	".ctor", (methodPointerType)&m13879, &t2577_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2577_m13879_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13879_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13880_GM;
MethodInfo m13880_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13880, &t2577_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13880_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13881_GM;
MethodInfo m13881_MI = 
{
	"Dispose", (methodPointerType)&m13881, &t2577_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13881_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13882_GM;
MethodInfo m13882_MI = 
{
	"MoveNext", (methodPointerType)&m13882, &t2577_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13882_GM};
extern Il2CppType t2562_0_0_0;
extern void* RuntimeInvoker_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13883_GM;
MethodInfo m13883_MI = 
{
	"get_Current", (methodPointerType)&m13883, &t2577_TI, &t2562_0_0_0, RuntimeInvoker_t2562, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13883_GM};
static MethodInfo* t2577_MIs[] =
{
	&m13879_MI,
	&m13880_MI,
	&m13881_MI,
	&m13882_MI,
	&m13883_MI,
	NULL
};
static MethodInfo* t2577_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13880_MI,
	&m13882_MI,
	&m13881_MI,
	&m13883_MI,
};
static TypeInfo* t2577_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2563_TI,
};
static Il2CppInterfaceOffsetPair t2577_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2563_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2577_0_0_0;
extern Il2CppType t2577_1_0_0;
extern Il2CppGenericClass t2577_GC;
TypeInfo t2577_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2577_MIs, t2577_PIs, t2577_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2577_TI, t2577_ITIs, t2577_VT, &EmptyCustomAttributesCache, &t2577_TI, &t2577_0_0_0, &t2577_1_0_0, t2577_IOs, &t2577_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2577)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5335_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>>
extern MethodInfo m27926_MI;
extern MethodInfo m27927_MI;
static PropertyInfo t5335____Item_PropertyInfo = 
{
	&t5335_TI, "Item", &m27926_MI, &m27927_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5335_PIs[] =
{
	&t5335____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2562_0_0_0;
static ParameterInfo t5335_m27928_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2562_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27928_GM;
MethodInfo m27928_MI = 
{
	"IndexOf", NULL, &t5335_TI, &t44_0_0_0, RuntimeInvoker_t44_t2562, t5335_m27928_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27928_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2562_0_0_0;
static ParameterInfo t5335_m27929_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2562_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27929_GM;
MethodInfo m27929_MI = 
{
	"Insert", NULL, &t5335_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2562, t5335_m27929_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27929_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5335_m27930_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27930_GM;
MethodInfo m27930_MI = 
{
	"RemoveAt", NULL, &t5335_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5335_m27930_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27930_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5335_m27926_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2562_0_0_0;
extern void* RuntimeInvoker_t2562_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27926_GM;
MethodInfo m27926_MI = 
{
	"get_Item", NULL, &t5335_TI, &t2562_0_0_0, RuntimeInvoker_t2562_t44, t5335_m27926_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27926_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2562_0_0_0;
static ParameterInfo t5335_m27927_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2562_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27927_GM;
MethodInfo m27927_MI = 
{
	"set_Item", NULL, &t5335_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2562, t5335_m27927_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27927_GM};
static MethodInfo* t5335_MIs[] =
{
	&m27928_MI,
	&m27929_MI,
	&m27930_MI,
	&m27926_MI,
	&m27927_MI,
	NULL
};
static TypeInfo* t5335_ITIs[] = 
{
	&t603_TI,
	&t5334_TI,
	&t5336_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5335_0_0_0;
extern Il2CppType t5335_1_0_0;
struct t5335;
extern Il2CppGenericClass t5335_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5335_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5335_MIs, t5335_PIs, NULL, NULL, NULL, NULL, NULL, &t5335_TI, t5335_ITIs, NULL, &t1908__CustomAttributeCache, &t5335_TI, &t5335_0_0_0, &t5335_1_0_0, NULL, &t5335_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IDictionary`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t3_0_0_0;
static ParameterInfo t6692_m27931_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27931_GM;
MethodInfo m27931_MI = 
{
	"Remove", NULL, &t6692_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6692_m27931_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27931_GM};
static MethodInfo* t6692_MIs[] =
{
	&m27931_MI,
	NULL
};
static TypeInfo* t6692_ITIs[] = 
{
	&t603_TI,
	&t5334_TI,
	&t5336_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6692_0_0_0;
extern Il2CppType t6692_1_0_0;
struct t6692;
extern Il2CppGenericClass t6692_GC;
extern CustomAttributesCache t1975__CustomAttributeCache;
TypeInfo t6692_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDictionary`2", "System.Collections.Generic", t6692_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6692_TI, t6692_ITIs, NULL, &t1975__CustomAttributeCache, &t6692_TI, &t6692_0_0_0, &t6692_1_0_0, NULL, &t6692_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2579_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern MethodInfo m27932_MI;
static PropertyInfo t2579____Current_PropertyInfo = 
{
	&t2579_TI, "Current", &m27932_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2579_PIs[] =
{
	&t2579____Current_PropertyInfo,
	NULL
};
extern Il2CppType t366_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27932_GM;
MethodInfo m27932_MI = 
{
	"get_Current", NULL, &t2579_TI, &t366_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27932_GM};
static MethodInfo* t2579_MIs[] =
{
	&m27932_MI,
	NULL
};
static TypeInfo* t2579_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2579_0_0_0;
extern Il2CppType t2579_1_0_0;
struct t2579;
extern Il2CppGenericClass t2579_GC;
TypeInfo t2579_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2579_MIs, t2579_PIs, NULL, NULL, NULL, NULL, NULL, &t2579_TI, t2579_ITIs, NULL, &EmptyCustomAttributesCache, &t2579_TI, &t2579_0_0_0, &t2579_1_0_0, NULL, &t2579_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2578.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2578_TI;
#include "t2578MD.h"

extern MethodInfo m13888_MI;
extern MethodInfo m21041_MI;
struct t20;
#define m21041(__this, p0, method) (t366 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t20_0_0_1;
FieldInfo t2578_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2578_TI, offsetof(t2578, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2578_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2578_TI, offsetof(t2578, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2578_FIs[] =
{
	&t2578_f0_FieldInfo,
	&t2578_f1_FieldInfo,
	NULL
};
extern MethodInfo m13885_MI;
static PropertyInfo t2578____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2578_TI, "System.Collections.IEnumerator.Current", &m13885_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2578____Current_PropertyInfo = 
{
	&t2578_TI, "Current", &m13888_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2578_PIs[] =
{
	&t2578____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2578____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2578_m13884_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13884_GM;
MethodInfo m13884_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2578_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2578_m13884_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13884_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13885_GM;
MethodInfo m13885_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2578_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13885_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13886_GM;
MethodInfo m13886_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2578_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13886_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13887_GM;
MethodInfo m13887_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2578_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13887_GM};
extern Il2CppType t366_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13888_GM;
MethodInfo m13888_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2578_TI, &t366_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13888_GM};
static MethodInfo* t2578_MIs[] =
{
	&m13884_MI,
	&m13885_MI,
	&m13886_MI,
	&m13887_MI,
	&m13888_MI,
	NULL
};
extern MethodInfo m13887_MI;
extern MethodInfo m13886_MI;
static MethodInfo* t2578_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13885_MI,
	&m13887_MI,
	&m13886_MI,
	&m13888_MI,
};
static TypeInfo* t2578_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2579_TI,
};
static Il2CppInterfaceOffsetPair t2578_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2579_TI, 7},
};
extern TypeInfo t366_TI;
static Il2CppRGCTXData t2578_RGCTXData[3] = 
{
	&m13888_MI/* Method Usage */,
	&t366_TI/* Class Usage */,
	&m21041_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2578_0_0_0;
extern Il2CppType t2578_1_0_0;
extern Il2CppGenericClass t2578_GC;
TypeInfo t2578_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2578_MIs, t2578_PIs, t2578_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2578_TI, t2578_ITIs, t2578_VT, &EmptyCustomAttributesCache, &t2578_TI, &t2578_0_0_0, &t2578_1_0_0, t2578_IOs, &t2578_GC, NULL, NULL, NULL, t2578_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2578)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5337_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern MethodInfo m27933_MI;
static PropertyInfo t5337____Count_PropertyInfo = 
{
	&t5337_TI, "Count", &m27933_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27934_MI;
static PropertyInfo t5337____IsReadOnly_PropertyInfo = 
{
	&t5337_TI, "IsReadOnly", &m27934_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5337_PIs[] =
{
	&t5337____Count_PropertyInfo,
	&t5337____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27933_GM;
MethodInfo m27933_MI = 
{
	"get_Count", NULL, &t5337_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27933_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27934_GM;
MethodInfo m27934_MI = 
{
	"get_IsReadOnly", NULL, &t5337_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27934_GM};
extern Il2CppType t366_0_0_0;
static ParameterInfo t5337_m27935_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27935_GM;
MethodInfo m27935_MI = 
{
	"Add", NULL, &t5337_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5337_m27935_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27935_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27936_GM;
MethodInfo m27936_MI = 
{
	"Clear", NULL, &t5337_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27936_GM};
extern Il2CppType t366_0_0_0;
static ParameterInfo t5337_m27937_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27937_GM;
MethodInfo m27937_MI = 
{
	"Contains", NULL, &t5337_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5337_m27937_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27937_GM};
extern Il2CppType t2557_0_0_0;
extern Il2CppType t2557_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5337_m27938_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2557_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27938_GM;
MethodInfo m27938_MI = 
{
	"CopyTo", NULL, &t5337_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5337_m27938_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27938_GM};
extern Il2CppType t366_0_0_0;
static ParameterInfo t5337_m27939_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27939_GM;
MethodInfo m27939_MI = 
{
	"Remove", NULL, &t5337_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5337_m27939_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27939_GM};
static MethodInfo* t5337_MIs[] =
{
	&m27933_MI,
	&m27934_MI,
	&m27935_MI,
	&m27936_MI,
	&m27937_MI,
	&m27938_MI,
	&m27939_MI,
	NULL
};
extern TypeInfo t5339_TI;
static TypeInfo* t5337_ITIs[] = 
{
	&t603_TI,
	&t5339_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5337_0_0_0;
extern Il2CppType t5337_1_0_0;
struct t5337;
extern Il2CppGenericClass t5337_GC;
TypeInfo t5337_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5337_MIs, t5337_PIs, NULL, NULL, NULL, NULL, NULL, &t5337_TI, t5337_ITIs, NULL, &EmptyCustomAttributesCache, &t5337_TI, &t5337_0_0_0, &t5337_1_0_0, NULL, &t5337_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t2579_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27940_GM;
MethodInfo m27940_MI = 
{
	"GetEnumerator", NULL, &t5339_TI, &t2579_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27940_GM};
static MethodInfo* t5339_MIs[] =
{
	&m27940_MI,
	NULL
};
static TypeInfo* t5339_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5339_0_0_0;
extern Il2CppType t5339_1_0_0;
struct t5339;
extern Il2CppGenericClass t5339_GC;
TypeInfo t5339_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5339_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5339_TI, t5339_ITIs, NULL, &EmptyCustomAttributesCache, &t5339_TI, &t5339_0_0_0, &t5339_1_0_0, NULL, &t5339_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5338_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern MethodInfo m27941_MI;
extern MethodInfo m27942_MI;
static PropertyInfo t5338____Item_PropertyInfo = 
{
	&t5338_TI, "Item", &m27941_MI, &m27942_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5338_PIs[] =
{
	&t5338____Item_PropertyInfo,
	NULL
};
extern Il2CppType t366_0_0_0;
static ParameterInfo t5338_m27943_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27943_GM;
MethodInfo m27943_MI = 
{
	"IndexOf", NULL, &t5338_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5338_m27943_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27943_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t5338_m27944_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27944_GM;
MethodInfo m27944_MI = 
{
	"Insert", NULL, &t5338_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5338_m27944_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27944_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5338_m27945_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27945_GM;
MethodInfo m27945_MI = 
{
	"RemoveAt", NULL, &t5338_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5338_m27945_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27945_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5338_m27941_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t366_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27941_GM;
MethodInfo m27941_MI = 
{
	"get_Item", NULL, &t5338_TI, &t366_0_0_0, RuntimeInvoker_t29_t44, t5338_m27941_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27941_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t5338_m27942_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27942_GM;
MethodInfo m27942_MI = 
{
	"set_Item", NULL, &t5338_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5338_m27942_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27942_GM};
static MethodInfo* t5338_MIs[] =
{
	&m27943_MI,
	&m27944_MI,
	&m27945_MI,
	&m27941_MI,
	&m27942_MI,
	NULL
};
static TypeInfo* t5338_ITIs[] = 
{
	&t603_TI,
	&t5337_TI,
	&t5339_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5338_0_0_0;
extern Il2CppType t5338_1_0_0;
struct t5338;
extern Il2CppGenericClass t5338_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5338_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5338_MIs, t5338_PIs, NULL, NULL, NULL, NULL, NULL, &t5338_TI, t5338_ITIs, NULL, &t1908__CustomAttributeCache, &t5338_TI, &t5338_0_0_0, &t5338_1_0_0, NULL, &t5338_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2580.h"
#include "t2581.h"
extern TypeInfo t2580_TI;
extern TypeInfo t2581_TI;
#include "t2581MD.h"
#include "t2580MD.h"
extern MethodInfo m13746_MI;
extern MethodInfo m13901_MI;
extern MethodInfo m13900_MI;
extern MethodInfo m13741_MI;
extern MethodInfo m13920_MI;
extern MethodInfo m21052_MI;
extern MethodInfo m21053_MI;
extern MethodInfo m13903_MI;
struct t170;
struct t2461;
#include "t2461.h"
#include "t2470.h"
 void m20791_gshared (t2461 * __this, t20 * p0, int32_t p1, t2470 * p2, MethodInfo* method);
#define m20791(__this, p0, p1, p2, method) (void)m20791_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, (t2470 *)p2, method)
#define m21052(__this, p0, p1, p2, method) (void)m20791_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, (t2470 *)p2, method)
struct t170;
 void m21053 (t170 * __this, t2557* p0, int32_t p1, t2581 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 t2580  m13901 (t2560 * __this, MethodInfo* method){
	{
		t170 * L_0 = (__this->f0);
		t2580  L_1 = {0};
		m13903(&L_1, L_0, &m13903_MI);
		return L_1;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t170_0_0_1;
FieldInfo t2560_f0_FieldInfo = 
{
	"dictionary", &t170_0_0_1, &t2560_TI, offsetof(t2560, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2560_FIs[] =
{
	&t2560_f0_FieldInfo,
	NULL
};
extern MethodInfo m13897_MI;
static PropertyInfo t2560____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo = 
{
	&t2560_TI, "System.Collections.Generic.ICollection<TValue>.IsReadOnly", &m13897_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13898_MI;
static PropertyInfo t2560____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2560_TI, "System.Collections.ICollection.IsSynchronized", &m13898_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13899_MI;
static PropertyInfo t2560____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2560_TI, "System.Collections.ICollection.SyncRoot", &m13899_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13902_MI;
static PropertyInfo t2560____Count_PropertyInfo = 
{
	&t2560_TI, "Count", &m13902_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2560_PIs[] =
{
	&t2560____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo,
	&t2560____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2560____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2560____Count_PropertyInfo,
	NULL
};
extern Il2CppType t170_0_0_0;
static ParameterInfo t2560_m13889_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t170_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13889_GM;
MethodInfo m13889_MI = 
{
	".ctor", (methodPointerType)&m12928_gshared, &t2560_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2560_m13889_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13889_GM};
extern Il2CppType t366_0_0_0;
static ParameterInfo t2560_m13890_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13890_GM;
MethodInfo m13890_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Add", (methodPointerType)&m12929_gshared, &t2560_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2560_m13890_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13890_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13891_GM;
MethodInfo m13891_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Clear", (methodPointerType)&m12930_gshared, &t2560_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13891_GM};
extern Il2CppType t366_0_0_0;
static ParameterInfo t2560_m13892_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13892_GM;
MethodInfo m13892_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Contains", (methodPointerType)&m12931_gshared, &t2560_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2560_m13892_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13892_GM};
extern Il2CppType t366_0_0_0;
static ParameterInfo t2560_m13893_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13893_GM;
MethodInfo m13893_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Remove", (methodPointerType)&m12932_gshared, &t2560_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2560_m13893_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13893_GM};
extern Il2CppType t2579_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13894_GM;
MethodInfo m13894_MI = 
{
	"System.Collections.Generic.IEnumerable<TValue>.GetEnumerator", (methodPointerType)&m12933_gshared, &t2560_TI, &t2579_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13894_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2560_m13895_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13895_GM;
MethodInfo m13895_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12934_gshared, &t2560_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2560_m13895_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13895_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13896_GM;
MethodInfo m13896_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12935_gshared, &t2560_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13896_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13897_GM;
MethodInfo m13897_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.get_IsReadOnly", (methodPointerType)&m12936_gshared, &t2560_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13897_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13898_GM;
MethodInfo m13898_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12937_gshared, &t2560_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13898_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13899_GM;
MethodInfo m13899_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12938_gshared, &t2560_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13899_GM};
extern Il2CppType t2557_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2560_m13900_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2557_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13900_GM;
MethodInfo m13900_MI = 
{
	"CopyTo", (methodPointerType)&m12939_gshared, &t2560_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2560_m13900_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13900_GM};
extern Il2CppType t2580_0_0_0;
extern void* RuntimeInvoker_t2580 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13901_GM;
MethodInfo m13901_MI = 
{
	"GetEnumerator", (methodPointerType)&m13901, &t2560_TI, &t2580_0_0_0, RuntimeInvoker_t2580, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13901_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13902_GM;
MethodInfo m13902_MI = 
{
	"get_Count", (methodPointerType)&m12941_gshared, &t2560_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13902_GM};
static MethodInfo* t2560_MIs[] =
{
	&m13889_MI,
	&m13890_MI,
	&m13891_MI,
	&m13892_MI,
	&m13893_MI,
	&m13894_MI,
	&m13895_MI,
	&m13896_MI,
	&m13897_MI,
	&m13898_MI,
	&m13899_MI,
	&m13900_MI,
	&m13901_MI,
	&m13902_MI,
	NULL
};
extern MethodInfo m13896_MI;
extern MethodInfo m13895_MI;
extern MethodInfo m13890_MI;
extern MethodInfo m13891_MI;
extern MethodInfo m13892_MI;
extern MethodInfo m13893_MI;
extern MethodInfo m13894_MI;
static MethodInfo* t2560_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13896_MI,
	&m13902_MI,
	&m13898_MI,
	&m13899_MI,
	&m13895_MI,
	&m13902_MI,
	&m13897_MI,
	&m13890_MI,
	&m13891_MI,
	&m13892_MI,
	&m13900_MI,
	&m13893_MI,
	&m13894_MI,
};
static TypeInfo* t2560_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t5337_TI,
	&t5339_TI,
};
static Il2CppInterfaceOffsetPair t2560_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t5337_TI, 9},
	{ &t5339_TI, 16},
};
extern TypeInfo t2580_TI;
extern TypeInfo t2557_TI;
extern TypeInfo t2581_TI;
static Il2CppRGCTXData t2560_RGCTXData[13] = 
{
	&m13746_MI/* Method Usage */,
	&m13901_MI/* Method Usage */,
	&t2580_TI/* Class Usage */,
	&t2557_TI/* Class Usage */,
	&m13900_MI/* Method Usage */,
	&m13739_MI/* Method Usage */,
	&m13741_MI/* Method Usage */,
	&t2581_TI/* Class Usage */,
	&m13920_MI/* Method Usage */,
	&m21052_MI/* Method Usage */,
	&m21053_MI/* Method Usage */,
	&m13903_MI/* Method Usage */,
	&m13734_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2560_0_0_0;
extern Il2CppType t2560_1_0_0;
struct t2560;
extern Il2CppGenericClass t2560_GC;
extern CustomAttributesCache t1252__CustomAttributeCache;
TypeInfo t2560_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueCollection", "", t2560_MIs, t2560_PIs, t2560_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2560_TI, t2560_ITIs, t2560_VT, &t1252__CustomAttributeCache, &t2560_TI, &t2560_0_0_0, &t2560_1_0_0, t2560_IOs, &t2560_GC, NULL, NULL, NULL, t2560_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2560), 0, -1, 0, 0, -1, 1057026, 0, false, false, false, false, true, false, false, false, false, false, false, false, 14, 4, 1, 0, 0, 17, 4, 4};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13916_MI;
extern MethodInfo m13919_MI;
extern MethodInfo m13913_MI;


// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t2564_0_0_1;
FieldInfo t2580_f0_FieldInfo = 
{
	"host_enumerator", &t2564_0_0_1, &t2580_TI, offsetof(t2580, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2580_FIs[] =
{
	&t2580_f0_FieldInfo,
	NULL
};
extern MethodInfo m13904_MI;
static PropertyInfo t2580____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2580_TI, "System.Collections.IEnumerator.Current", &m13904_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13907_MI;
static PropertyInfo t2580____Current_PropertyInfo = 
{
	&t2580_TI, "Current", &m13907_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2580_PIs[] =
{
	&t2580____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2580____Current_PropertyInfo,
	NULL
};
extern Il2CppType t170_0_0_0;
static ParameterInfo t2580_m13903_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t170_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13903_GM;
MethodInfo m13903_MI = 
{
	".ctor", (methodPointerType)&m12942_gshared, &t2580_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2580_m13903_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13903_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13904_GM;
MethodInfo m13904_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12943_gshared, &t2580_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13904_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13905_GM;
MethodInfo m13905_MI = 
{
	"Dispose", (methodPointerType)&m12944_gshared, &t2580_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13905_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13906_GM;
MethodInfo m13906_MI = 
{
	"MoveNext", (methodPointerType)&m12945_gshared, &t2580_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13906_GM};
extern Il2CppType t366_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13907_GM;
MethodInfo m13907_MI = 
{
	"get_Current", (methodPointerType)&m12946_gshared, &t2580_TI, &t366_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13907_GM};
static MethodInfo* t2580_MIs[] =
{
	&m13903_MI,
	&m13904_MI,
	&m13905_MI,
	&m13906_MI,
	&m13907_MI,
	NULL
};
extern MethodInfo m13906_MI;
extern MethodInfo m13905_MI;
static MethodInfo* t2580_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13904_MI,
	&m13906_MI,
	&m13905_MI,
	&m13907_MI,
};
static TypeInfo* t2580_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2579_TI,
};
static Il2CppInterfaceOffsetPair t2580_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2579_TI, 7},
};
extern TypeInfo t366_TI;
static Il2CppRGCTXData t2580_RGCTXData[6] = 
{
	&m13754_MI/* Method Usage */,
	&m13916_MI/* Method Usage */,
	&t366_TI/* Class Usage */,
	&m13919_MI/* Method Usage */,
	&m13913_MI/* Method Usage */,
	&m13759_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2580_0_0_0;
extern Il2CppType t2580_1_0_0;
extern Il2CppGenericClass t2580_GC;
TypeInfo t2580_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2580_MIs, t2580_PIs, t2580_FIs, NULL, &t110_TI, NULL, &t1252_TI, &t2580_TI, t2580_ITIs, t2580_VT, &EmptyCustomAttributesCache, &t2580_TI, &t2580_0_0_0, &t2580_1_0_0, t2580_IOs, &t2580_GC, NULL, NULL, NULL, t2580_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2580)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13918_MI;
extern MethodInfo m13915_MI;
extern MethodInfo m13917_MI;


extern MethodInfo m13914_MI;
 t2562  m13914 (t2564 * __this, MethodInfo* method){
	{
		t2562  L_0 = (__this->f3);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t170_0_0_1;
FieldInfo t2564_f0_FieldInfo = 
{
	"dictionary", &t170_0_0_1, &t2564_TI, offsetof(t2564, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2564_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2564_TI, offsetof(t2564, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2564_f2_FieldInfo = 
{
	"stamp", &t44_0_0_1, &t2564_TI, offsetof(t2564, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t2562_0_0_3;
FieldInfo t2564_f3_FieldInfo = 
{
	"current", &t2562_0_0_3, &t2564_TI, offsetof(t2564, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2564_FIs[] =
{
	&t2564_f0_FieldInfo,
	&t2564_f1_FieldInfo,
	&t2564_f2_FieldInfo,
	&t2564_f3_FieldInfo,
	NULL
};
extern MethodInfo m13909_MI;
static PropertyInfo t2564____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2564_TI, "System.Collections.IEnumerator.Current", &m13909_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13910_MI;
static PropertyInfo t2564____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo = 
{
	&t2564_TI, "System.Collections.IDictionaryEnumerator.Entry", &m13910_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13911_MI;
static PropertyInfo t2564____System_Collections_IDictionaryEnumerator_Key_PropertyInfo = 
{
	&t2564_TI, "System.Collections.IDictionaryEnumerator.Key", &m13911_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13912_MI;
static PropertyInfo t2564____System_Collections_IDictionaryEnumerator_Value_PropertyInfo = 
{
	&t2564_TI, "System.Collections.IDictionaryEnumerator.Value", &m13912_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2564____Current_PropertyInfo = 
{
	&t2564_TI, "Current", &m13914_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2564____CurrentKey_PropertyInfo = 
{
	&t2564_TI, "CurrentKey", &m13915_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2564____CurrentValue_PropertyInfo = 
{
	&t2564_TI, "CurrentValue", &m13916_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2564_PIs[] =
{
	&t2564____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2564____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo,
	&t2564____System_Collections_IDictionaryEnumerator_Key_PropertyInfo,
	&t2564____System_Collections_IDictionaryEnumerator_Value_PropertyInfo,
	&t2564____Current_PropertyInfo,
	&t2564____CurrentKey_PropertyInfo,
	&t2564____CurrentValue_PropertyInfo,
	NULL
};
extern Il2CppType t170_0_0_0;
static ParameterInfo t2564_m13908_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t170_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13908_GM;
MethodInfo m13908_MI = 
{
	".ctor", (methodPointerType)&m12947_gshared, &t2564_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2564_m13908_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13908_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13909_GM;
MethodInfo m13909_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12948_gshared, &t2564_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13909_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13910_GM;
MethodInfo m13910_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Entry", (methodPointerType)&m12949_gshared, &t2564_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13910_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13911_GM;
MethodInfo m13911_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Key", (methodPointerType)&m12950_gshared, &t2564_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13911_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13912_GM;
MethodInfo m13912_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Value", (methodPointerType)&m12951_gshared, &t2564_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13912_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13913_GM;
MethodInfo m13913_MI = 
{
	"MoveNext", (methodPointerType)&m12952_gshared, &t2564_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13913_GM};
extern Il2CppType t2562_0_0_0;
extern void* RuntimeInvoker_t2562 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13914_GM;
MethodInfo m13914_MI = 
{
	"get_Current", (methodPointerType)&m13914, &t2564_TI, &t2562_0_0_0, RuntimeInvoker_t2562, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13914_GM};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13915_GM;
MethodInfo m13915_MI = 
{
	"get_CurrentKey", (methodPointerType)&m12954_gshared, &t2564_TI, &t3_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13915_GM};
extern Il2CppType t366_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13916_GM;
MethodInfo m13916_MI = 
{
	"get_CurrentValue", (methodPointerType)&m12955_gshared, &t2564_TI, &t366_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13916_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13917_GM;
MethodInfo m13917_MI = 
{
	"VerifyState", (methodPointerType)&m12956_gshared, &t2564_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13917_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13918_GM;
MethodInfo m13918_MI = 
{
	"VerifyCurrent", (methodPointerType)&m12957_gshared, &t2564_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13918_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13919_GM;
MethodInfo m13919_MI = 
{
	"Dispose", (methodPointerType)&m12958_gshared, &t2564_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13919_GM};
static MethodInfo* t2564_MIs[] =
{
	&m13908_MI,
	&m13909_MI,
	&m13910_MI,
	&m13911_MI,
	&m13912_MI,
	&m13913_MI,
	&m13914_MI,
	&m13915_MI,
	&m13916_MI,
	&m13917_MI,
	&m13918_MI,
	&m13919_MI,
	NULL
};
static MethodInfo* t2564_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13909_MI,
	&m13913_MI,
	&m13919_MI,
	&m13914_MI,
	&m13910_MI,
	&m13911_MI,
	&m13912_MI,
};
static TypeInfo* t2564_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2563_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2564_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2563_TI, 7},
	{ &t722_TI, 8},
};
extern TypeInfo t2562_TI;
extern TypeInfo t3_TI;
extern TypeInfo t366_TI;
static Il2CppRGCTXData t2564_RGCTXData[10] = 
{
	&m13918_MI/* Method Usage */,
	&t2562_TI/* Class Usage */,
	&m13757_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m13759_MI/* Method Usage */,
	&t366_TI/* Class Usage */,
	&m13915_MI/* Method Usage */,
	&m13916_MI/* Method Usage */,
	&m13917_MI/* Method Usage */,
	&m13756_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2564_0_0_0;
extern Il2CppType t2564_1_0_0;
extern Il2CppGenericClass t2564_GC;
TypeInfo t2564_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2564_MIs, t2564_PIs, t2564_FIs, NULL, &t110_TI, NULL, &t1254_TI, &t2564_TI, t2564_ITIs, t2564_VT, &EmptyCustomAttributesCache, &t2564_TI, &t2564_0_0_0, &t2564_1_0_0, t2564_IOs, &t2564_GC, NULL, NULL, NULL, t2564_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2564)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 12, 7, 4, 0, 0, 11, 4, 4};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2581_m13920_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13920_GM;
MethodInfo m13920_MI = 
{
	".ctor", (methodPointerType)&m12959_gshared, &t2581_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2581_m13920_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13920_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t2581_m13921_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t366_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13921_GM;
MethodInfo m13921_MI = 
{
	"Invoke", (methodPointerType)&m12960_gshared, &t2581_TI, &t366_0_0_0, RuntimeInvoker_t29_t29_t29, t2581_m13921_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13921_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2581_m13922_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13922_GM;
MethodInfo m13922_MI = 
{
	"BeginInvoke", (methodPointerType)&m12961_gshared, &t2581_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2581_m13922_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13922_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2581_m13923_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t366_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13923_GM;
MethodInfo m13923_MI = 
{
	"EndInvoke", (methodPointerType)&m12962_gshared, &t2581_TI, &t366_0_0_0, RuntimeInvoker_t29_t29, t2581_m13923_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13923_GM};
static MethodInfo* t2581_MIs[] =
{
	&m13920_MI,
	&m13921_MI,
	&m13922_MI,
	&m13923_MI,
	NULL
};
extern MethodInfo m13921_MI;
extern MethodInfo m13922_MI;
extern MethodInfo m13923_MI;
static MethodInfo* t2581_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13921_MI,
	&m13922_MI,
	&m13923_MI,
};
static Il2CppInterfaceOffsetPair t2581_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2581_0_0_0;
extern Il2CppType t2581_1_0_0;
struct t2581;
extern Il2CppGenericClass t2581_GC;
TypeInfo t2581_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2581_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2581_TI, NULL, t2581_VT, &EmptyCustomAttributesCache, &t2581_TI, &t2581_0_0_0, &t2581_1_0_0, t2581_IOs, &t2581_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2581), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m13924 (t2559 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m13925_MI;
 t725  m13925 (t2559 * __this, t3 * p0, t366 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m13925((t2559 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 *, t29 * __this, t3 * p0, t366 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t3 * p0, t366 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t366 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m13926_MI;
 t29 * m13926 (t2559 * __this, t3 * p0, t366 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m13927_MI;
 t725  m13927 (t2559 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t725 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>,System.Collections.DictionaryEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2559_m13924_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13924_GM;
MethodInfo m13924_MI = 
{
	".ctor", (methodPointerType)&m13924, &t2559_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2559_m13924_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13924_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t2559_m13925_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13925_GM;
MethodInfo m13925_MI = 
{
	"Invoke", (methodPointerType)&m13925, &t2559_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t29, t2559_m13925_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13925_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2559_m13926_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13926_GM;
MethodInfo m13926_MI = 
{
	"BeginInvoke", (methodPointerType)&m13926, &t2559_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2559_m13926_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13926_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2559_m13927_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13927_GM;
MethodInfo m13927_MI = 
{
	"EndInvoke", (methodPointerType)&m13927, &t2559_TI, &t725_0_0_0, RuntimeInvoker_t725_t29, t2559_m13927_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13927_GM};
static MethodInfo* t2559_MIs[] =
{
	&m13924_MI,
	&m13925_MI,
	&m13926_MI,
	&m13927_MI,
	NULL
};
static MethodInfo* t2559_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13925_MI,
	&m13926_MI,
	&m13927_MI,
};
static Il2CppInterfaceOffsetPair t2559_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2559_0_0_0;
extern Il2CppType t2559_1_0_0;
struct t2559;
extern Il2CppGenericClass t2559_GC;
TypeInfo t2559_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2559_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2559_TI, NULL, t2559_VT, &EmptyCustomAttributesCache, &t2559_TI, &t2559_0_0_0, &t2559_1_0_0, t2559_IOs, &t2559_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2559), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m13928 (t2582 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m13929_MI;
 t2562  m13929 (t2582 * __this, t3 * p0, t366 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m13929((t2582 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t2562  (*FunctionPointerType) (t29 *, t29 * __this, t3 * p0, t366 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t2562  (*FunctionPointerType) (t29 * __this, t3 * p0, t366 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t2562  (*FunctionPointerType) (t29 * __this, t366 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m13930_MI;
 t29 * m13930 (t2582 * __this, t3 * p0, t366 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m13931_MI;
 t2562  m13931 (t2582 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t2562 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>,System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2582_m13928_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13928_GM;
MethodInfo m13928_MI = 
{
	".ctor", (methodPointerType)&m13928, &t2582_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2582_m13928_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13928_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t2582_m13929_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t2562_0_0_0;
extern void* RuntimeInvoker_t2562_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13929_GM;
MethodInfo m13929_MI = 
{
	"Invoke", (methodPointerType)&m13929, &t2582_TI, &t2562_0_0_0, RuntimeInvoker_t2562_t29_t29, t2582_m13929_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13929_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t366_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2582_m13930_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13930_GM;
MethodInfo m13930_MI = 
{
	"BeginInvoke", (methodPointerType)&m13930, &t2582_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2582_m13930_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13930_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2582_m13931_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t2562_0_0_0;
extern void* RuntimeInvoker_t2562_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13931_GM;
MethodInfo m13931_MI = 
{
	"EndInvoke", (methodPointerType)&m13931, &t2582_TI, &t2562_0_0_0, RuntimeInvoker_t2562_t29, t2582_m13931_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13931_GM};
static MethodInfo* t2582_MIs[] =
{
	&m13928_MI,
	&m13929_MI,
	&m13930_MI,
	&m13931_MI,
	NULL
};
static MethodInfo* t2582_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13929_MI,
	&m13930_MI,
	&m13931_MI,
};
static Il2CppInterfaceOffsetPair t2582_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2582_0_0_0;
extern Il2CppType t2582_1_0_0;
struct t2582;
extern Il2CppGenericClass t2582_GC;
TypeInfo t2582_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2582_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2582_TI, NULL, t2582_VT, &EmptyCustomAttributesCache, &t2582_TI, &t2582_0_0_0, &t2582_1_0_0, t2582_IOs, &t2582_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2582), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13934_MI;


// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t2564_0_0_1;
FieldInfo t2583_f0_FieldInfo = 
{
	"host_enumerator", &t2564_0_0_1, &t2583_TI, offsetof(t2583, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2583_FIs[] =
{
	&t2583_f0_FieldInfo,
	NULL
};
static PropertyInfo t2583____Entry_PropertyInfo = 
{
	&t2583_TI, "Entry", &m13934_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13935_MI;
static PropertyInfo t2583____Key_PropertyInfo = 
{
	&t2583_TI, "Key", &m13935_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13936_MI;
static PropertyInfo t2583____Value_PropertyInfo = 
{
	&t2583_TI, "Value", &m13936_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13937_MI;
static PropertyInfo t2583____Current_PropertyInfo = 
{
	&t2583_TI, "Current", &m13937_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2583_PIs[] =
{
	&t2583____Entry_PropertyInfo,
	&t2583____Key_PropertyInfo,
	&t2583____Value_PropertyInfo,
	&t2583____Current_PropertyInfo,
	NULL
};
extern Il2CppType t170_0_0_0;
static ParameterInfo t2583_m13932_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t170_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13932_GM;
MethodInfo m13932_MI = 
{
	".ctor", (methodPointerType)&m12971_gshared, &t2583_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2583_m13932_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13932_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13933_GM;
MethodInfo m13933_MI = 
{
	"MoveNext", (methodPointerType)&m12972_gshared, &t2583_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13933_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13934_GM;
MethodInfo m13934_MI = 
{
	"get_Entry", (methodPointerType)&m12973_gshared, &t2583_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13934_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13935_GM;
MethodInfo m13935_MI = 
{
	"get_Key", (methodPointerType)&m12974_gshared, &t2583_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13935_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13936_GM;
MethodInfo m13936_MI = 
{
	"get_Value", (methodPointerType)&m12975_gshared, &t2583_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13936_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13937_GM;
MethodInfo m13937_MI = 
{
	"get_Current", (methodPointerType)&m12976_gshared, &t2583_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13937_GM};
static MethodInfo* t2583_MIs[] =
{
	&m13932_MI,
	&m13933_MI,
	&m13934_MI,
	&m13935_MI,
	&m13936_MI,
	&m13937_MI,
	NULL
};
extern MethodInfo m13933_MI;
static MethodInfo* t2583_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13937_MI,
	&m13933_MI,
	&m13934_MI,
	&m13935_MI,
	&m13936_MI,
};
static TypeInfo* t2583_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2583_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern TypeInfo t2564_TI;
extern TypeInfo t3_TI;
extern TypeInfo t366_TI;
static Il2CppRGCTXData t2583_RGCTXData[9] = 
{
	&m13754_MI/* Method Usage */,
	&m13913_MI/* Method Usage */,
	&t2564_TI/* Class Usage */,
	&m13914_MI/* Method Usage */,
	&m13757_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m13759_MI/* Method Usage */,
	&t366_TI/* Class Usage */,
	&m13934_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2583_0_0_0;
extern Il2CppType t2583_1_0_0;
struct t2583;
extern Il2CppGenericClass t2583_GC;
TypeInfo t2583_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ShimEnumerator", "", t2583_MIs, t2583_PIs, t2583_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2583_TI, t2583_ITIs, t2583_VT, &EmptyCustomAttributesCache, &t2583_TI, &t2583_0_0_0, &t2583_1_0_0, t2583_IOs, &t2583_GC, NULL, NULL, NULL, t2583_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2583), 0, -1, 0, 0, -1, 1056771, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 4, 1, 0, 0, 9, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t366_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t6686_m27892_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27892_GM;
MethodInfo m27892_MI = 
{
	"Equals", NULL, &t6686_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6686_m27892_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27892_GM};
extern Il2CppType t366_0_0_0;
static ParameterInfo t6686_m27946_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27946_GM;
MethodInfo m27946_MI = 
{
	"GetHashCode", NULL, &t6686_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6686_m27946_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27946_GM};
static MethodInfo* t6686_MIs[] =
{
	&m27892_MI,
	&m27946_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6686_0_0_0;
extern Il2CppType t6686_1_0_0;
struct t6686;
extern Il2CppGenericClass t6686_GC;
TypeInfo t6686_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6686_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6686_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6686_TI, &t6686_0_0_0, &t6686_1_0_0, NULL, &t6686_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2585.h"
extern TypeInfo t6699_TI;
extern TypeInfo t2585_TI;
#include "t2585MD.h"
extern Il2CppType t6699_0_0_0;
extern MethodInfo m13943_MI;
extern MethodInfo m27947_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t2584_0_0_49;
FieldInfo t2584_f0_FieldInfo = 
{
	"_default", &t2584_0_0_49, &t2584_TI, offsetof(t2584_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2584_FIs[] =
{
	&t2584_f0_FieldInfo,
	NULL
};
static PropertyInfo t2584____Default_PropertyInfo = 
{
	&t2584_TI, "Default", &m13942_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2584_PIs[] =
{
	&t2584____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13938_GM;
MethodInfo m13938_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2584_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13938_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13939_GM;
MethodInfo m13939_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2584_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13939_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2584_m13940_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13940_GM;
MethodInfo m13940_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2584_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2584_m13940_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13940_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2584_m13941_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13941_GM;
MethodInfo m13941_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2584_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2584_m13941_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13941_GM};
extern Il2CppType t366_0_0_0;
static ParameterInfo t2584_m27947_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27947_GM;
MethodInfo m27947_MI = 
{
	"GetHashCode", NULL, &t2584_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2584_m27947_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27947_GM};
extern Il2CppType t366_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t2584_m27893_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27893_GM;
MethodInfo m27893_MI = 
{
	"Equals", NULL, &t2584_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2584_m27893_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27893_GM};
extern Il2CppType t2584_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13942_GM;
MethodInfo m13942_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2584_TI, &t2584_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13942_GM};
static MethodInfo* t2584_MIs[] =
{
	&m13938_MI,
	&m13939_MI,
	&m13940_MI,
	&m13941_MI,
	&m27947_MI,
	&m27893_MI,
	&m13942_MI,
	NULL
};
extern MethodInfo m13941_MI;
extern MethodInfo m13940_MI;
static MethodInfo* t2584_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27893_MI,
	&m27947_MI,
	&m13941_MI,
	&m13940_MI,
	NULL,
	NULL,
};
static TypeInfo* t2584_ITIs[] = 
{
	&t6686_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2584_IOs[] = 
{
	{ &t6686_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2584_TI;
extern TypeInfo t2584_TI;
extern TypeInfo t2585_TI;
extern TypeInfo t366_TI;
static Il2CppRGCTXData t2584_RGCTXData[9] = 
{
	&t6699_0_0_0/* Type Usage */,
	&t366_0_0_0/* Type Usage */,
	&t2584_TI/* Class Usage */,
	&t2584_TI/* Static Usage */,
	&t2585_TI/* Class Usage */,
	&m13943_MI/* Method Usage */,
	&t366_TI/* Class Usage */,
	&m27947_MI/* Method Usage */,
	&m27893_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2584_0_0_0;
extern Il2CppType t2584_1_0_0;
struct t2584;
extern Il2CppGenericClass t2584_GC;
TypeInfo t2584_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2584_MIs, t2584_PIs, t2584_FIs, NULL, &t29_TI, NULL, NULL, &t2584_TI, t2584_ITIs, t2584_VT, &EmptyCustomAttributesCache, &t2584_TI, &t2584_0_0_0, &t2584_1_0_0, t2584_IOs, &t2584_GC, NULL, NULL, NULL, t2584_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2584), 0, -1, sizeof(t2584_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t366_0_0_0;
static ParameterInfo t6699_m27948_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27948_GM;
MethodInfo m27948_MI = 
{
	"Equals", NULL, &t6699_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6699_m27948_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27948_GM};
static MethodInfo* t6699_MIs[] =
{
	&m27948_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6699_1_0_0;
struct t6699;
extern Il2CppGenericClass t6699_GC;
TypeInfo t6699_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6699_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6699_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6699_TI, &t6699_0_0_0, &t6699_1_0_0, NULL, &t6699_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13938_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13943_GM;
MethodInfo m13943_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2585_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13943_GM};
extern Il2CppType t366_0_0_0;
static ParameterInfo t2585_m13944_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13944_GM;
MethodInfo m13944_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2585_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2585_m13944_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13944_GM};
extern Il2CppType t366_0_0_0;
extern Il2CppType t366_0_0_0;
static ParameterInfo t2585_m13945_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t366_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13945_GM;
MethodInfo m13945_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2585_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2585_m13945_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13945_GM};
static MethodInfo* t2585_MIs[] =
{
	&m13943_MI,
	&m13944_MI,
	&m13945_MI,
	NULL
};
extern MethodInfo m13945_MI;
extern MethodInfo m13944_MI;
static MethodInfo* t2585_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13945_MI,
	&m13944_MI,
	&m13941_MI,
	&m13940_MI,
	&m13944_MI,
	&m13945_MI,
};
static Il2CppInterfaceOffsetPair t2585_IOs[] = 
{
	{ &t6686_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2584_TI;
extern TypeInfo t2584_TI;
extern TypeInfo t2585_TI;
extern TypeInfo t366_TI;
extern TypeInfo t366_TI;
static Il2CppRGCTXData t2585_RGCTXData[11] = 
{
	&t6699_0_0_0/* Type Usage */,
	&t366_0_0_0/* Type Usage */,
	&t2584_TI/* Class Usage */,
	&t2584_TI/* Static Usage */,
	&t2585_TI/* Class Usage */,
	&m13943_MI/* Method Usage */,
	&t366_TI/* Class Usage */,
	&m27947_MI/* Method Usage */,
	&m27893_MI/* Method Usage */,
	&m13938_MI/* Method Usage */,
	&t366_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2585_0_0_0;
extern Il2CppType t2585_1_0_0;
struct t2585;
extern Il2CppGenericClass t2585_GC;
TypeInfo t2585_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2585_MIs, NULL, NULL, NULL, &t2584_TI, NULL, &t1256_TI, &t2585_TI, NULL, t2585_VT, &EmptyCustomAttributesCache, &t2585_TI, &t2585_0_0_0, &t2585_1_0_0, t2585_IOs, &t2585_GC, NULL, NULL, NULL, t2585_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2585), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4169_TI;

#include "t179.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image>
extern MethodInfo m27949_MI;
static PropertyInfo t4169____Current_PropertyInfo = 
{
	&t4169_TI, "Current", &m27949_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4169_PIs[] =
{
	&t4169____Current_PropertyInfo,
	NULL
};
extern Il2CppType t179_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27949_GM;
MethodInfo m27949_MI = 
{
	"get_Current", NULL, &t4169_TI, &t179_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27949_GM};
static MethodInfo* t4169_MIs[] =
{
	&m27949_MI,
	NULL
};
static TypeInfo* t4169_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4169_0_0_0;
extern Il2CppType t4169_1_0_0;
struct t4169;
extern Il2CppGenericClass t4169_GC;
TypeInfo t4169_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4169_MIs, t4169_PIs, NULL, NULL, NULL, NULL, NULL, &t4169_TI, t4169_ITIs, NULL, &EmptyCustomAttributesCache, &t4169_TI, &t4169_0_0_0, &t4169_1_0_0, NULL, &t4169_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2586.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2586_TI;
#include "t2586MD.h"

extern TypeInfo t179_TI;
extern MethodInfo m13950_MI;
extern MethodInfo m21059_MI;
struct t20;
#define m21059(__this, p0, method) (t179 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image>
extern Il2CppType t20_0_0_1;
FieldInfo t2586_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2586_TI, offsetof(t2586, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2586_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2586_TI, offsetof(t2586, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2586_FIs[] =
{
	&t2586_f0_FieldInfo,
	&t2586_f1_FieldInfo,
	NULL
};
extern MethodInfo m13947_MI;
static PropertyInfo t2586____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2586_TI, "System.Collections.IEnumerator.Current", &m13947_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2586____Current_PropertyInfo = 
{
	&t2586_TI, "Current", &m13950_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2586_PIs[] =
{
	&t2586____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2586____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2586_m13946_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13946_GM;
MethodInfo m13946_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2586_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2586_m13946_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13946_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13947_GM;
MethodInfo m13947_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2586_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13947_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13948_GM;
MethodInfo m13948_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2586_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13948_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13949_GM;
MethodInfo m13949_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2586_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13949_GM};
extern Il2CppType t179_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13950_GM;
MethodInfo m13950_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2586_TI, &t179_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13950_GM};
static MethodInfo* t2586_MIs[] =
{
	&m13946_MI,
	&m13947_MI,
	&m13948_MI,
	&m13949_MI,
	&m13950_MI,
	NULL
};
extern MethodInfo m13949_MI;
extern MethodInfo m13948_MI;
static MethodInfo* t2586_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13947_MI,
	&m13949_MI,
	&m13948_MI,
	&m13950_MI,
};
static TypeInfo* t2586_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4169_TI,
};
static Il2CppInterfaceOffsetPair t2586_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4169_TI, 7},
};
extern TypeInfo t179_TI;
static Il2CppRGCTXData t2586_RGCTXData[3] = 
{
	&m13950_MI/* Method Usage */,
	&t179_TI/* Class Usage */,
	&m21059_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2586_0_0_0;
extern Il2CppType t2586_1_0_0;
extern Il2CppGenericClass t2586_GC;
TypeInfo t2586_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2586_MIs, t2586_PIs, t2586_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2586_TI, t2586_ITIs, t2586_VT, &EmptyCustomAttributesCache, &t2586_TI, &t2586_0_0_0, &t2586_1_0_0, t2586_IOs, &t2586_GC, NULL, NULL, NULL, t2586_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2586)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5340_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image>
extern MethodInfo m27950_MI;
static PropertyInfo t5340____Count_PropertyInfo = 
{
	&t5340_TI, "Count", &m27950_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27951_MI;
static PropertyInfo t5340____IsReadOnly_PropertyInfo = 
{
	&t5340_TI, "IsReadOnly", &m27951_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5340_PIs[] =
{
	&t5340____Count_PropertyInfo,
	&t5340____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27950_GM;
MethodInfo m27950_MI = 
{
	"get_Count", NULL, &t5340_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27950_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27951_GM;
MethodInfo m27951_MI = 
{
	"get_IsReadOnly", NULL, &t5340_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27951_GM};
extern Il2CppType t179_0_0_0;
extern Il2CppType t179_0_0_0;
static ParameterInfo t5340_m27952_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t179_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27952_GM;
MethodInfo m27952_MI = 
{
	"Add", NULL, &t5340_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5340_m27952_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27952_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27953_GM;
MethodInfo m27953_MI = 
{
	"Clear", NULL, &t5340_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27953_GM};
extern Il2CppType t179_0_0_0;
static ParameterInfo t5340_m27954_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t179_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27954_GM;
MethodInfo m27954_MI = 
{
	"Contains", NULL, &t5340_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5340_m27954_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27954_GM};
extern Il2CppType t3838_0_0_0;
extern Il2CppType t3838_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5340_m27955_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3838_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27955_GM;
MethodInfo m27955_MI = 
{
	"CopyTo", NULL, &t5340_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5340_m27955_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27955_GM};
extern Il2CppType t179_0_0_0;
static ParameterInfo t5340_m27956_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t179_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27956_GM;
MethodInfo m27956_MI = 
{
	"Remove", NULL, &t5340_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5340_m27956_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27956_GM};
static MethodInfo* t5340_MIs[] =
{
	&m27950_MI,
	&m27951_MI,
	&m27952_MI,
	&m27953_MI,
	&m27954_MI,
	&m27955_MI,
	&m27956_MI,
	NULL
};
extern TypeInfo t5342_TI;
static TypeInfo* t5340_ITIs[] = 
{
	&t603_TI,
	&t5342_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5340_0_0_0;
extern Il2CppType t5340_1_0_0;
struct t5340;
extern Il2CppGenericClass t5340_GC;
TypeInfo t5340_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5340_MIs, t5340_PIs, NULL, NULL, NULL, NULL, NULL, &t5340_TI, t5340_ITIs, NULL, &EmptyCustomAttributesCache, &t5340_TI, &t5340_0_0_0, &t5340_1_0_0, NULL, &t5340_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image>
extern Il2CppType t4169_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27957_GM;
MethodInfo m27957_MI = 
{
	"GetEnumerator", NULL, &t5342_TI, &t4169_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27957_GM};
static MethodInfo* t5342_MIs[] =
{
	&m27957_MI,
	NULL
};
static TypeInfo* t5342_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5342_0_0_0;
extern Il2CppType t5342_1_0_0;
struct t5342;
extern Il2CppGenericClass t5342_GC;
TypeInfo t5342_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5342_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5342_TI, t5342_ITIs, NULL, &EmptyCustomAttributesCache, &t5342_TI, &t5342_0_0_0, &t5342_1_0_0, NULL, &t5342_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5341_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image>
extern MethodInfo m27958_MI;
extern MethodInfo m27959_MI;
static PropertyInfo t5341____Item_PropertyInfo = 
{
	&t5341_TI, "Item", &m27958_MI, &m27959_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5341_PIs[] =
{
	&t5341____Item_PropertyInfo,
	NULL
};
extern Il2CppType t179_0_0_0;
static ParameterInfo t5341_m27960_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t179_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27960_GM;
MethodInfo m27960_MI = 
{
	"IndexOf", NULL, &t5341_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5341_m27960_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27960_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t179_0_0_0;
static ParameterInfo t5341_m27961_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t179_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27961_GM;
MethodInfo m27961_MI = 
{
	"Insert", NULL, &t5341_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5341_m27961_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27961_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5341_m27962_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27962_GM;
MethodInfo m27962_MI = 
{
	"RemoveAt", NULL, &t5341_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5341_m27962_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27962_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5341_m27958_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t179_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27958_GM;
MethodInfo m27958_MI = 
{
	"get_Item", NULL, &t5341_TI, &t179_0_0_0, RuntimeInvoker_t29_t44, t5341_m27958_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27958_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t179_0_0_0;
static ParameterInfo t5341_m27959_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t179_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27959_GM;
MethodInfo m27959_MI = 
{
	"set_Item", NULL, &t5341_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5341_m27959_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27959_GM};
static MethodInfo* t5341_MIs[] =
{
	&m27960_MI,
	&m27961_MI,
	&m27962_MI,
	&m27958_MI,
	&m27959_MI,
	NULL
};
static TypeInfo* t5341_ITIs[] = 
{
	&t603_TI,
	&t5340_TI,
	&t5342_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5341_0_0_0;
extern Il2CppType t5341_1_0_0;
struct t5341;
extern Il2CppGenericClass t5341_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5341_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5341_MIs, t5341_PIs, NULL, NULL, NULL, NULL, NULL, &t5341_TI, t5341_ITIs, NULL, &t1908__CustomAttributeCache, &t5341_TI, &t5341_0_0_0, &t5341_1_0_0, NULL, &t5341_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5343_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.ICanvasRaycastFilter>
extern MethodInfo m27963_MI;
static PropertyInfo t5343____Count_PropertyInfo = 
{
	&t5343_TI, "Count", &m27963_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27964_MI;
static PropertyInfo t5343____IsReadOnly_PropertyInfo = 
{
	&t5343_TI, "IsReadOnly", &m27964_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5343_PIs[] =
{
	&t5343____Count_PropertyInfo,
	&t5343____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27963_GM;
MethodInfo m27963_MI = 
{
	"get_Count", NULL, &t5343_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27963_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27964_GM;
MethodInfo m27964_MI = 
{
	"get_IsReadOnly", NULL, &t5343_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27964_GM};
extern Il2CppType t357_0_0_0;
extern Il2CppType t357_0_0_0;
static ParameterInfo t5343_m27965_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t357_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27965_GM;
MethodInfo m27965_MI = 
{
	"Add", NULL, &t5343_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5343_m27965_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27965_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27966_GM;
MethodInfo m27966_MI = 
{
	"Clear", NULL, &t5343_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27966_GM};
extern Il2CppType t357_0_0_0;
static ParameterInfo t5343_m27967_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t357_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27967_GM;
MethodInfo m27967_MI = 
{
	"Contains", NULL, &t5343_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5343_m27967_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27967_GM};
extern Il2CppType t3730_0_0_0;
extern Il2CppType t3730_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5343_m27968_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3730_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27968_GM;
MethodInfo m27968_MI = 
{
	"CopyTo", NULL, &t5343_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5343_m27968_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27968_GM};
extern Il2CppType t357_0_0_0;
static ParameterInfo t5343_m27969_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t357_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27969_GM;
MethodInfo m27969_MI = 
{
	"Remove", NULL, &t5343_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5343_m27969_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27969_GM};
static MethodInfo* t5343_MIs[] =
{
	&m27963_MI,
	&m27964_MI,
	&m27965_MI,
	&m27966_MI,
	&m27967_MI,
	&m27968_MI,
	&m27969_MI,
	NULL
};
extern TypeInfo t5345_TI;
static TypeInfo* t5343_ITIs[] = 
{
	&t603_TI,
	&t5345_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5343_0_0_0;
extern Il2CppType t5343_1_0_0;
struct t5343;
extern Il2CppGenericClass t5343_GC;
TypeInfo t5343_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5343_MIs, t5343_PIs, NULL, NULL, NULL, NULL, NULL, &t5343_TI, t5343_ITIs, NULL, &EmptyCustomAttributesCache, &t5343_TI, &t5343_0_0_0, &t5343_1_0_0, NULL, &t5343_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.ICanvasRaycastFilter>
extern Il2CppType t4171_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27970_GM;
MethodInfo m27970_MI = 
{
	"GetEnumerator", NULL, &t5345_TI, &t4171_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27970_GM};
static MethodInfo* t5345_MIs[] =
{
	&m27970_MI,
	NULL
};
static TypeInfo* t5345_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5345_0_0_0;
extern Il2CppType t5345_1_0_0;
struct t5345;
extern Il2CppGenericClass t5345_GC;
TypeInfo t5345_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5345_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5345_TI, t5345_ITIs, NULL, &EmptyCustomAttributesCache, &t5345_TI, &t5345_0_0_0, &t5345_1_0_0, NULL, &t5345_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4171_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.ICanvasRaycastFilter>
extern MethodInfo m27971_MI;
static PropertyInfo t4171____Current_PropertyInfo = 
{
	&t4171_TI, "Current", &m27971_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4171_PIs[] =
{
	&t4171____Current_PropertyInfo,
	NULL
};
extern Il2CppType t357_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27971_GM;
MethodInfo m27971_MI = 
{
	"get_Current", NULL, &t4171_TI, &t357_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27971_GM};
static MethodInfo* t4171_MIs[] =
{
	&m27971_MI,
	NULL
};
static TypeInfo* t4171_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4171_0_0_0;
extern Il2CppType t4171_1_0_0;
struct t4171;
extern Il2CppGenericClass t4171_GC;
TypeInfo t4171_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4171_MIs, t4171_PIs, NULL, NULL, NULL, NULL, NULL, &t4171_TI, t4171_ITIs, NULL, &EmptyCustomAttributesCache, &t4171_TI, &t4171_0_0_0, &t4171_1_0_0, NULL, &t4171_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2587.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2587_TI;
#include "t2587MD.h"

extern TypeInfo t357_TI;
extern MethodInfo m13955_MI;
extern MethodInfo m21070_MI;
struct t20;
#define m21070(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.ICanvasRaycastFilter>
extern Il2CppType t20_0_0_1;
FieldInfo t2587_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2587_TI, offsetof(t2587, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2587_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2587_TI, offsetof(t2587, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2587_FIs[] =
{
	&t2587_f0_FieldInfo,
	&t2587_f1_FieldInfo,
	NULL
};
extern MethodInfo m13952_MI;
static PropertyInfo t2587____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2587_TI, "System.Collections.IEnumerator.Current", &m13952_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2587____Current_PropertyInfo = 
{
	&t2587_TI, "Current", &m13955_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2587_PIs[] =
{
	&t2587____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2587____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2587_m13951_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13951_GM;
MethodInfo m13951_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2587_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2587_m13951_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13951_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13952_GM;
MethodInfo m13952_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2587_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13952_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13953_GM;
MethodInfo m13953_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2587_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13953_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13954_GM;
MethodInfo m13954_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2587_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13954_GM};
extern Il2CppType t357_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13955_GM;
MethodInfo m13955_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2587_TI, &t357_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13955_GM};
static MethodInfo* t2587_MIs[] =
{
	&m13951_MI,
	&m13952_MI,
	&m13953_MI,
	&m13954_MI,
	&m13955_MI,
	NULL
};
extern MethodInfo m13954_MI;
extern MethodInfo m13953_MI;
static MethodInfo* t2587_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13952_MI,
	&m13954_MI,
	&m13953_MI,
	&m13955_MI,
};
static TypeInfo* t2587_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4171_TI,
};
static Il2CppInterfaceOffsetPair t2587_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4171_TI, 7},
};
extern TypeInfo t357_TI;
static Il2CppRGCTXData t2587_RGCTXData[3] = 
{
	&m13955_MI/* Method Usage */,
	&t357_TI/* Class Usage */,
	&m21070_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2587_0_0_0;
extern Il2CppType t2587_1_0_0;
extern Il2CppGenericClass t2587_GC;
TypeInfo t2587_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2587_MIs, t2587_PIs, t2587_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2587_TI, t2587_ITIs, t2587_VT, &EmptyCustomAttributesCache, &t2587_TI, &t2587_0_0_0, &t2587_1_0_0, t2587_IOs, &t2587_GC, NULL, NULL, NULL, t2587_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2587)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5344_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.ICanvasRaycastFilter>
extern MethodInfo m27972_MI;
extern MethodInfo m27973_MI;
static PropertyInfo t5344____Item_PropertyInfo = 
{
	&t5344_TI, "Item", &m27972_MI, &m27973_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5344_PIs[] =
{
	&t5344____Item_PropertyInfo,
	NULL
};
extern Il2CppType t357_0_0_0;
static ParameterInfo t5344_m27974_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t357_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27974_GM;
MethodInfo m27974_MI = 
{
	"IndexOf", NULL, &t5344_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5344_m27974_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27974_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t357_0_0_0;
static ParameterInfo t5344_m27975_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t357_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27975_GM;
MethodInfo m27975_MI = 
{
	"Insert", NULL, &t5344_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5344_m27975_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27975_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5344_m27976_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27976_GM;
MethodInfo m27976_MI = 
{
	"RemoveAt", NULL, &t5344_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5344_m27976_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27976_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5344_m27972_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t357_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27972_GM;
MethodInfo m27972_MI = 
{
	"get_Item", NULL, &t5344_TI, &t357_0_0_0, RuntimeInvoker_t29_t44, t5344_m27972_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27972_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t357_0_0_0;
static ParameterInfo t5344_m27973_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t357_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27973_GM;
MethodInfo m27973_MI = 
{
	"set_Item", NULL, &t5344_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5344_m27973_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27973_GM};
static MethodInfo* t5344_MIs[] =
{
	&m27974_MI,
	&m27975_MI,
	&m27976_MI,
	&m27972_MI,
	&m27973_MI,
	NULL
};
static TypeInfo* t5344_ITIs[] = 
{
	&t603_TI,
	&t5343_TI,
	&t5345_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5344_0_0_0;
extern Il2CppType t5344_1_0_0;
struct t5344;
extern Il2CppGenericClass t5344_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5344_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5344_MIs, t5344_PIs, NULL, NULL, NULL, NULL, NULL, &t5344_TI, t5344_ITIs, NULL, &t1908__CustomAttributeCache, &t5344_TI, &t5344_0_0_0, &t5344_1_0_0, NULL, &t5344_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5346_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.ISerializationCallbackReceiver>
extern MethodInfo m27977_MI;
static PropertyInfo t5346____Count_PropertyInfo = 
{
	&t5346_TI, "Count", &m27977_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27978_MI;
static PropertyInfo t5346____IsReadOnly_PropertyInfo = 
{
	&t5346_TI, "IsReadOnly", &m27978_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5346_PIs[] =
{
	&t5346____Count_PropertyInfo,
	&t5346____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27977_GM;
MethodInfo m27977_MI = 
{
	"get_Count", NULL, &t5346_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27977_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27978_GM;
MethodInfo m27978_MI = 
{
	"get_IsReadOnly", NULL, &t5346_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27978_GM};
extern Il2CppType t301_0_0_0;
extern Il2CppType t301_0_0_0;
static ParameterInfo t5346_m27979_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t301_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27979_GM;
MethodInfo m27979_MI = 
{
	"Add", NULL, &t5346_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5346_m27979_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27979_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27980_GM;
MethodInfo m27980_MI = 
{
	"Clear", NULL, &t5346_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27980_GM};
extern Il2CppType t301_0_0_0;
static ParameterInfo t5346_m27981_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t301_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27981_GM;
MethodInfo m27981_MI = 
{
	"Contains", NULL, &t5346_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5346_m27981_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27981_GM};
extern Il2CppType t3731_0_0_0;
extern Il2CppType t3731_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5346_m27982_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3731_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27982_GM;
MethodInfo m27982_MI = 
{
	"CopyTo", NULL, &t5346_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5346_m27982_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27982_GM};
extern Il2CppType t301_0_0_0;
static ParameterInfo t5346_m27983_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t301_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27983_GM;
MethodInfo m27983_MI = 
{
	"Remove", NULL, &t5346_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5346_m27983_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27983_GM};
static MethodInfo* t5346_MIs[] =
{
	&m27977_MI,
	&m27978_MI,
	&m27979_MI,
	&m27980_MI,
	&m27981_MI,
	&m27982_MI,
	&m27983_MI,
	NULL
};
extern TypeInfo t5348_TI;
static TypeInfo* t5346_ITIs[] = 
{
	&t603_TI,
	&t5348_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5346_0_0_0;
extern Il2CppType t5346_1_0_0;
struct t5346;
extern Il2CppGenericClass t5346_GC;
TypeInfo t5346_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5346_MIs, t5346_PIs, NULL, NULL, NULL, NULL, NULL, &t5346_TI, t5346_ITIs, NULL, &EmptyCustomAttributesCache, &t5346_TI, &t5346_0_0_0, &t5346_1_0_0, NULL, &t5346_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.ISerializationCallbackReceiver>
extern Il2CppType t4173_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27984_GM;
MethodInfo m27984_MI = 
{
	"GetEnumerator", NULL, &t5348_TI, &t4173_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27984_GM};
static MethodInfo* t5348_MIs[] =
{
	&m27984_MI,
	NULL
};
static TypeInfo* t5348_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5348_0_0_0;
extern Il2CppType t5348_1_0_0;
struct t5348;
extern Il2CppGenericClass t5348_GC;
TypeInfo t5348_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5348_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5348_TI, t5348_ITIs, NULL, &EmptyCustomAttributesCache, &t5348_TI, &t5348_0_0_0, &t5348_1_0_0, NULL, &t5348_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4173_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.ISerializationCallbackReceiver>
extern MethodInfo m27985_MI;
static PropertyInfo t4173____Current_PropertyInfo = 
{
	&t4173_TI, "Current", &m27985_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4173_PIs[] =
{
	&t4173____Current_PropertyInfo,
	NULL
};
extern Il2CppType t301_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27985_GM;
MethodInfo m27985_MI = 
{
	"get_Current", NULL, &t4173_TI, &t301_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27985_GM};
static MethodInfo* t4173_MIs[] =
{
	&m27985_MI,
	NULL
};
static TypeInfo* t4173_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4173_0_0_0;
extern Il2CppType t4173_1_0_0;
struct t4173;
extern Il2CppGenericClass t4173_GC;
TypeInfo t4173_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4173_MIs, t4173_PIs, NULL, NULL, NULL, NULL, NULL, &t4173_TI, t4173_ITIs, NULL, &EmptyCustomAttributesCache, &t4173_TI, &t4173_0_0_0, &t4173_1_0_0, NULL, &t4173_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2588.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2588_TI;
#include "t2588MD.h"

extern TypeInfo t301_TI;
extern MethodInfo m13960_MI;
extern MethodInfo m21081_MI;
struct t20;
#define m21081(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.ISerializationCallbackReceiver>
extern Il2CppType t20_0_0_1;
FieldInfo t2588_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2588_TI, offsetof(t2588, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2588_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2588_TI, offsetof(t2588, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2588_FIs[] =
{
	&t2588_f0_FieldInfo,
	&t2588_f1_FieldInfo,
	NULL
};
extern MethodInfo m13957_MI;
static PropertyInfo t2588____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2588_TI, "System.Collections.IEnumerator.Current", &m13957_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2588____Current_PropertyInfo = 
{
	&t2588_TI, "Current", &m13960_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2588_PIs[] =
{
	&t2588____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2588____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2588_m13956_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13956_GM;
MethodInfo m13956_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2588_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2588_m13956_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13956_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13957_GM;
MethodInfo m13957_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2588_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13957_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13958_GM;
MethodInfo m13958_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2588_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13958_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13959_GM;
MethodInfo m13959_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2588_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13959_GM};
extern Il2CppType t301_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13960_GM;
MethodInfo m13960_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2588_TI, &t301_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13960_GM};
static MethodInfo* t2588_MIs[] =
{
	&m13956_MI,
	&m13957_MI,
	&m13958_MI,
	&m13959_MI,
	&m13960_MI,
	NULL
};
extern MethodInfo m13959_MI;
extern MethodInfo m13958_MI;
static MethodInfo* t2588_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13957_MI,
	&m13959_MI,
	&m13958_MI,
	&m13960_MI,
};
static TypeInfo* t2588_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4173_TI,
};
static Il2CppInterfaceOffsetPair t2588_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4173_TI, 7},
};
extern TypeInfo t301_TI;
static Il2CppRGCTXData t2588_RGCTXData[3] = 
{
	&m13960_MI/* Method Usage */,
	&t301_TI/* Class Usage */,
	&m21081_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2588_0_0_0;
extern Il2CppType t2588_1_0_0;
extern Il2CppGenericClass t2588_GC;
TypeInfo t2588_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2588_MIs, t2588_PIs, t2588_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2588_TI, t2588_ITIs, t2588_VT, &EmptyCustomAttributesCache, &t2588_TI, &t2588_0_0_0, &t2588_1_0_0, t2588_IOs, &t2588_GC, NULL, NULL, NULL, t2588_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2588)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5347_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.ISerializationCallbackReceiver>
extern MethodInfo m27986_MI;
extern MethodInfo m27987_MI;
static PropertyInfo t5347____Item_PropertyInfo = 
{
	&t5347_TI, "Item", &m27986_MI, &m27987_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5347_PIs[] =
{
	&t5347____Item_PropertyInfo,
	NULL
};
extern Il2CppType t301_0_0_0;
static ParameterInfo t5347_m27988_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t301_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27988_GM;
MethodInfo m27988_MI = 
{
	"IndexOf", NULL, &t5347_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5347_m27988_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27988_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t301_0_0_0;
static ParameterInfo t5347_m27989_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t301_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27989_GM;
MethodInfo m27989_MI = 
{
	"Insert", NULL, &t5347_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5347_m27989_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27989_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5347_m27990_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27990_GM;
MethodInfo m27990_MI = 
{
	"RemoveAt", NULL, &t5347_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5347_m27990_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27990_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5347_m27986_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t301_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27986_GM;
MethodInfo m27986_MI = 
{
	"get_Item", NULL, &t5347_TI, &t301_0_0_0, RuntimeInvoker_t29_t44, t5347_m27986_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27986_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t301_0_0_0;
static ParameterInfo t5347_m27987_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t301_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27987_GM;
MethodInfo m27987_MI = 
{
	"set_Item", NULL, &t5347_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5347_m27987_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27987_GM};
static MethodInfo* t5347_MIs[] =
{
	&m27988_MI,
	&m27989_MI,
	&m27990_MI,
	&m27986_MI,
	&m27987_MI,
	NULL
};
static TypeInfo* t5347_ITIs[] = 
{
	&t603_TI,
	&t5346_TI,
	&t5348_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5347_0_0_0;
extern Il2CppType t5347_1_0_0;
struct t5347;
extern Il2CppGenericClass t5347_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5347_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5347_MIs, t5347_PIs, NULL, NULL, NULL, NULL, NULL, &t5347_TI, t5347_ITIs, NULL, &t1908__CustomAttributeCache, &t5347_TI, &t5347_0_0_0, &t5347_1_0_0, NULL, &t5347_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2589.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2589_TI;
#include "t2589MD.h"

#include "t2590.h"
extern TypeInfo t2590_TI;
#include "t2590MD.h"
extern MethodInfo m13963_MI;
extern MethodInfo m13965_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Image>
extern Il2CppType t316_0_0_33;
FieldInfo t2589_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2589_TI, offsetof(t2589, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2589_FIs[] =
{
	&t2589_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t179_0_0_0;
static ParameterInfo t2589_m13961_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t179_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13961_GM;
MethodInfo m13961_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2589_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2589_m13961_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13961_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2589_m13962_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13962_GM;
MethodInfo m13962_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2589_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2589_m13962_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13962_GM};
static MethodInfo* t2589_MIs[] =
{
	&m13961_MI,
	&m13962_MI,
	NULL
};
extern MethodInfo m13962_MI;
extern MethodInfo m13966_MI;
static MethodInfo* t2589_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13962_MI,
	&m13966_MI,
};
extern Il2CppType t2591_0_0_0;
extern TypeInfo t2591_TI;
extern MethodInfo m21091_MI;
extern TypeInfo t179_TI;
extern MethodInfo m13968_MI;
extern TypeInfo t179_TI;
static Il2CppRGCTXData t2589_RGCTXData[8] = 
{
	&t2591_0_0_0/* Type Usage */,
	&t2591_TI/* Class Usage */,
	&m21091_MI/* Method Usage */,
	&t179_TI/* Class Usage */,
	&m13968_MI/* Method Usage */,
	&m13963_MI/* Method Usage */,
	&t179_TI/* Class Usage */,
	&m13965_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2589_0_0_0;
extern Il2CppType t2589_1_0_0;
struct t2589;
extern Il2CppGenericClass t2589_GC;
TypeInfo t2589_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2589_MIs, NULL, t2589_FIs, NULL, &t2590_TI, NULL, NULL, &t2589_TI, NULL, t2589_VT, &EmptyCustomAttributesCache, &t2589_TI, &t2589_0_0_0, &t2589_1_0_0, NULL, &t2589_GC, NULL, NULL, NULL, t2589_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2589), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2591.h"
extern TypeInfo t2591_TI;
#include "t2591MD.h"
struct t556;
#define m21091(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Image>
extern Il2CppType t2591_0_0_1;
FieldInfo t2590_f0_FieldInfo = 
{
	"Delegate", &t2591_0_0_1, &t2590_TI, offsetof(t2590, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2590_FIs[] =
{
	&t2590_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2590_m13963_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13963_GM;
MethodInfo m13963_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2590_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2590_m13963_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13963_GM};
extern Il2CppType t2591_0_0_0;
static ParameterInfo t2590_m13964_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2591_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13964_GM;
MethodInfo m13964_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2590_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2590_m13964_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13964_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2590_m13965_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13965_GM;
MethodInfo m13965_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2590_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2590_m13965_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13965_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2590_m13966_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13966_GM;
MethodInfo m13966_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2590_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2590_m13966_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13966_GM};
static MethodInfo* t2590_MIs[] =
{
	&m13963_MI,
	&m13964_MI,
	&m13965_MI,
	&m13966_MI,
	NULL
};
static MethodInfo* t2590_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13965_MI,
	&m13966_MI,
};
extern TypeInfo t2591_TI;
extern TypeInfo t179_TI;
static Il2CppRGCTXData t2590_RGCTXData[5] = 
{
	&t2591_0_0_0/* Type Usage */,
	&t2591_TI/* Class Usage */,
	&m21091_MI/* Method Usage */,
	&t179_TI/* Class Usage */,
	&m13968_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2590_0_0_0;
extern Il2CppType t2590_1_0_0;
struct t2590;
extern Il2CppGenericClass t2590_GC;
TypeInfo t2590_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2590_MIs, NULL, t2590_FIs, NULL, &t556_TI, NULL, NULL, &t2590_TI, NULL, t2590_VT, &EmptyCustomAttributesCache, &t2590_TI, &t2590_0_0_0, &t2590_1_0_0, NULL, &t2590_GC, NULL, NULL, NULL, t2590_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2590), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Image>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2591_m13967_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13967_GM;
MethodInfo m13967_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2591_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2591_m13967_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13967_GM};
extern Il2CppType t179_0_0_0;
static ParameterInfo t2591_m13968_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t179_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13968_GM;
MethodInfo m13968_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2591_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2591_m13968_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13968_GM};
extern Il2CppType t179_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2591_m13969_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t179_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13969_GM;
MethodInfo m13969_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2591_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2591_m13969_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13969_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2591_m13970_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13970_GM;
MethodInfo m13970_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2591_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2591_m13970_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13970_GM};
static MethodInfo* t2591_MIs[] =
{
	&m13967_MI,
	&m13968_MI,
	&m13969_MI,
	&m13970_MI,
	NULL
};
extern MethodInfo m13969_MI;
extern MethodInfo m13970_MI;
static MethodInfo* t2591_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13968_MI,
	&m13969_MI,
	&m13970_MI,
};
static Il2CppInterfaceOffsetPair t2591_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2591_1_0_0;
struct t2591;
extern Il2CppGenericClass t2591_GC;
TypeInfo t2591_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2591_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2591_TI, NULL, t2591_VT, &EmptyCustomAttributesCache, &t2591_TI, &t2591_0_0_0, &t2591_1_0_0, t2591_IOs, &t2591_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2591), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4177_TI;

#include "t17.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
extern MethodInfo m27991_MI;
static PropertyInfo t4177____Current_PropertyInfo = 
{
	&t4177_TI, "Current", &m27991_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4177_PIs[] =
{
	&t4177____Current_PropertyInfo,
	NULL
};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27991_GM;
MethodInfo m27991_MI = 
{
	"get_Current", NULL, &t4177_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27991_GM};
static MethodInfo* t4177_MIs[] =
{
	&m27991_MI,
	NULL
};
static TypeInfo* t4177_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4177_0_0_0;
extern Il2CppType t4177_1_0_0;
struct t4177;
extern Il2CppGenericClass t4177_GC;
TypeInfo t4177_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4177_MIs, t4177_PIs, NULL, NULL, NULL, NULL, NULL, &t4177_TI, t4177_ITIs, NULL, &EmptyCustomAttributesCache, &t4177_TI, &t4177_0_0_0, &t4177_1_0_0, NULL, &t4177_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2592.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2592_TI;
#include "t2592MD.h"

extern TypeInfo t17_TI;
extern MethodInfo m13975_MI;
extern MethodInfo m21093_MI;
struct t20;
 t17  m21093 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m13971_MI;
 void m13971 (t2592 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13972_MI;
 t29 * m13972 (t2592 * __this, MethodInfo* method){
	{
		t17  L_0 = m13975(__this, &m13975_MI);
		t17  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t17_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13973_MI;
 void m13973 (t2592 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13974_MI;
 bool m13974 (t2592 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t17  m13975 (t2592 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t17  L_8 = m21093(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21093_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Vector2>
extern Il2CppType t20_0_0_1;
FieldInfo t2592_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2592_TI, offsetof(t2592, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2592_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2592_TI, offsetof(t2592, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2592_FIs[] =
{
	&t2592_f0_FieldInfo,
	&t2592_f1_FieldInfo,
	NULL
};
static PropertyInfo t2592____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2592_TI, "System.Collections.IEnumerator.Current", &m13972_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2592____Current_PropertyInfo = 
{
	&t2592_TI, "Current", &m13975_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2592_PIs[] =
{
	&t2592____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2592____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2592_m13971_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13971_GM;
MethodInfo m13971_MI = 
{
	".ctor", (methodPointerType)&m13971, &t2592_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2592_m13971_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13971_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13972_GM;
MethodInfo m13972_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13972, &t2592_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13972_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13973_GM;
MethodInfo m13973_MI = 
{
	"Dispose", (methodPointerType)&m13973, &t2592_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13973_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13974_GM;
MethodInfo m13974_MI = 
{
	"MoveNext", (methodPointerType)&m13974, &t2592_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13974_GM};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13975_GM;
MethodInfo m13975_MI = 
{
	"get_Current", (methodPointerType)&m13975, &t2592_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13975_GM};
static MethodInfo* t2592_MIs[] =
{
	&m13971_MI,
	&m13972_MI,
	&m13973_MI,
	&m13974_MI,
	&m13975_MI,
	NULL
};
static MethodInfo* t2592_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13972_MI,
	&m13974_MI,
	&m13973_MI,
	&m13975_MI,
};
static TypeInfo* t2592_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4177_TI,
};
static Il2CppInterfaceOffsetPair t2592_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4177_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2592_0_0_0;
extern Il2CppType t2592_1_0_0;
extern Il2CppGenericClass t2592_GC;
TypeInfo t2592_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2592_MIs, t2592_PIs, t2592_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2592_TI, t2592_ITIs, t2592_VT, &EmptyCustomAttributesCache, &t2592_TI, &t2592_0_0_0, &t2592_1_0_0, t2592_IOs, &t2592_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2592)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5349_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Vector2>
extern MethodInfo m27992_MI;
static PropertyInfo t5349____Count_PropertyInfo = 
{
	&t5349_TI, "Count", &m27992_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27993_MI;
static PropertyInfo t5349____IsReadOnly_PropertyInfo = 
{
	&t5349_TI, "IsReadOnly", &m27993_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5349_PIs[] =
{
	&t5349____Count_PropertyInfo,
	&t5349____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27992_GM;
MethodInfo m27992_MI = 
{
	"get_Count", NULL, &t5349_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27992_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27993_GM;
MethodInfo m27993_MI = 
{
	"get_IsReadOnly", NULL, &t5349_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27993_GM};
extern Il2CppType t17_0_0_0;
extern Il2CppType t17_0_0_0;
static ParameterInfo t5349_m27994_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27994_GM;
MethodInfo m27994_MI = 
{
	"Add", NULL, &t5349_TI, &t21_0_0_0, RuntimeInvoker_t21_t17, t5349_m27994_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27994_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27995_GM;
MethodInfo m27995_MI = 
{
	"Clear", NULL, &t5349_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27995_GM};
extern Il2CppType t17_0_0_0;
static ParameterInfo t5349_m27996_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t17 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27996_GM;
MethodInfo m27996_MI = 
{
	"Contains", NULL, &t5349_TI, &t40_0_0_0, RuntimeInvoker_t40_t17, t5349_m27996_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27996_GM};
extern Il2CppType t181_0_0_0;
extern Il2CppType t181_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5349_m27997_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t181_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27997_GM;
MethodInfo m27997_MI = 
{
	"CopyTo", NULL, &t5349_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5349_m27997_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27997_GM};
extern Il2CppType t17_0_0_0;
static ParameterInfo t5349_m27998_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t17 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27998_GM;
MethodInfo m27998_MI = 
{
	"Remove", NULL, &t5349_TI, &t40_0_0_0, RuntimeInvoker_t40_t17, t5349_m27998_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27998_GM};
static MethodInfo* t5349_MIs[] =
{
	&m27992_MI,
	&m27993_MI,
	&m27994_MI,
	&m27995_MI,
	&m27996_MI,
	&m27997_MI,
	&m27998_MI,
	NULL
};
extern TypeInfo t5351_TI;
static TypeInfo* t5349_ITIs[] = 
{
	&t603_TI,
	&t5351_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5349_0_0_0;
extern Il2CppType t5349_1_0_0;
struct t5349;
extern Il2CppGenericClass t5349_GC;
TypeInfo t5349_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5349_MIs, t5349_PIs, NULL, NULL, NULL, NULL, NULL, &t5349_TI, t5349_ITIs, NULL, &EmptyCustomAttributesCache, &t5349_TI, &t5349_0_0_0, &t5349_1_0_0, NULL, &t5349_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
extern Il2CppType t4177_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27999_GM;
MethodInfo m27999_MI = 
{
	"GetEnumerator", NULL, &t5351_TI, &t4177_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27999_GM};
static MethodInfo* t5351_MIs[] =
{
	&m27999_MI,
	NULL
};
static TypeInfo* t5351_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5351_0_0_0;
extern Il2CppType t5351_1_0_0;
struct t5351;
extern Il2CppGenericClass t5351_GC;
TypeInfo t5351_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5351_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5351_TI, t5351_ITIs, NULL, &EmptyCustomAttributesCache, &t5351_TI, &t5351_0_0_0, &t5351_1_0_0, NULL, &t5351_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5350_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Vector2>
extern MethodInfo m28000_MI;
extern MethodInfo m28001_MI;
static PropertyInfo t5350____Item_PropertyInfo = 
{
	&t5350_TI, "Item", &m28000_MI, &m28001_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5350_PIs[] =
{
	&t5350____Item_PropertyInfo,
	NULL
};
extern Il2CppType t17_0_0_0;
static ParameterInfo t5350_m28002_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t17 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28002_GM;
MethodInfo m28002_MI = 
{
	"IndexOf", NULL, &t5350_TI, &t44_0_0_0, RuntimeInvoker_t44_t17, t5350_m28002_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28002_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t17_0_0_0;
static ParameterInfo t5350_m28003_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t17 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28003_GM;
MethodInfo m28003_MI = 
{
	"Insert", NULL, &t5350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t17, t5350_m28003_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28003_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5350_m28004_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28004_GM;
MethodInfo m28004_MI = 
{
	"RemoveAt", NULL, &t5350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5350_m28004_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28004_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5350_m28000_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28000_GM;
MethodInfo m28000_MI = 
{
	"get_Item", NULL, &t5350_TI, &t17_0_0_0, RuntimeInvoker_t17_t44, t5350_m28000_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28000_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t17_0_0_0;
static ParameterInfo t5350_m28001_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t17 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28001_GM;
MethodInfo m28001_MI = 
{
	"set_Item", NULL, &t5350_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t17, t5350_m28001_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28001_GM};
static MethodInfo* t5350_MIs[] =
{
	&m28002_MI,
	&m28003_MI,
	&m28004_MI,
	&m28000_MI,
	&m28001_MI,
	NULL
};
static TypeInfo* t5350_ITIs[] = 
{
	&t603_TI,
	&t5349_TI,
	&t5351_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5350_0_0_0;
extern Il2CppType t5350_1_0_0;
struct t5350;
extern Il2CppGenericClass t5350_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5350_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5350_MIs, t5350_PIs, NULL, NULL, NULL, NULL, NULL, &t5350_TI, t5350_ITIs, NULL, &t1908__CustomAttributeCache, &t5350_TI, &t5350_0_0_0, &t5350_1_0_0, NULL, &t5350_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4178_TI;

#include "t172.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Type>
extern MethodInfo m28005_MI;
static PropertyInfo t4178____Current_PropertyInfo = 
{
	&t4178_TI, "Current", &m28005_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4178_PIs[] =
{
	&t4178____Current_PropertyInfo,
	NULL
};
extern Il2CppType t172_0_0_0;
extern void* RuntimeInvoker_t172 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28005_GM;
MethodInfo m28005_MI = 
{
	"get_Current", NULL, &t4178_TI, &t172_0_0_0, RuntimeInvoker_t172, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28005_GM};
static MethodInfo* t4178_MIs[] =
{
	&m28005_MI,
	NULL
};
static TypeInfo* t4178_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4178_0_0_0;
extern Il2CppType t4178_1_0_0;
struct t4178;
extern Il2CppGenericClass t4178_GC;
TypeInfo t4178_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4178_MIs, t4178_PIs, NULL, NULL, NULL, NULL, NULL, &t4178_TI, t4178_ITIs, NULL, &EmptyCustomAttributesCache, &t4178_TI, &t4178_0_0_0, &t4178_1_0_0, NULL, &t4178_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2593.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2593_TI;
#include "t2593MD.h"

extern TypeInfo t172_TI;
extern MethodInfo m13980_MI;
extern MethodInfo m21104_MI;
struct t20;
 int32_t m21104 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m13976_MI;
 void m13976 (t2593 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13977_MI;
 t29 * m13977 (t2593 * __this, MethodInfo* method){
	{
		int32_t L_0 = m13980(__this, &m13980_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t172_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13978_MI;
 void m13978 (t2593 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13979_MI;
 bool m13979 (t2593 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m13980 (t2593 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21104(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21104_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>
extern Il2CppType t20_0_0_1;
FieldInfo t2593_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2593_TI, offsetof(t2593, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2593_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2593_TI, offsetof(t2593, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2593_FIs[] =
{
	&t2593_f0_FieldInfo,
	&t2593_f1_FieldInfo,
	NULL
};
static PropertyInfo t2593____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2593_TI, "System.Collections.IEnumerator.Current", &m13977_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2593____Current_PropertyInfo = 
{
	&t2593_TI, "Current", &m13980_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2593_PIs[] =
{
	&t2593____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2593____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2593_m13976_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13976_GM;
MethodInfo m13976_MI = 
{
	".ctor", (methodPointerType)&m13976, &t2593_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2593_m13976_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13976_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13977_GM;
MethodInfo m13977_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13977, &t2593_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13977_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13978_GM;
MethodInfo m13978_MI = 
{
	"Dispose", (methodPointerType)&m13978, &t2593_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13978_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13979_GM;
MethodInfo m13979_MI = 
{
	"MoveNext", (methodPointerType)&m13979, &t2593_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13979_GM};
extern Il2CppType t172_0_0_0;
extern void* RuntimeInvoker_t172 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13980_GM;
MethodInfo m13980_MI = 
{
	"get_Current", (methodPointerType)&m13980, &t2593_TI, &t172_0_0_0, RuntimeInvoker_t172, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13980_GM};
static MethodInfo* t2593_MIs[] =
{
	&m13976_MI,
	&m13977_MI,
	&m13978_MI,
	&m13979_MI,
	&m13980_MI,
	NULL
};
static MethodInfo* t2593_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13977_MI,
	&m13979_MI,
	&m13978_MI,
	&m13980_MI,
};
static TypeInfo* t2593_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4178_TI,
};
static Il2CppInterfaceOffsetPair t2593_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4178_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2593_0_0_0;
extern Il2CppType t2593_1_0_0;
extern Il2CppGenericClass t2593_GC;
TypeInfo t2593_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2593_MIs, t2593_PIs, t2593_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2593_TI, t2593_ITIs, t2593_VT, &EmptyCustomAttributesCache, &t2593_TI, &t2593_0_0_0, &t2593_1_0_0, t2593_IOs, &t2593_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2593)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5352_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>
extern MethodInfo m28006_MI;
static PropertyInfo t5352____Count_PropertyInfo = 
{
	&t5352_TI, "Count", &m28006_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28007_MI;
static PropertyInfo t5352____IsReadOnly_PropertyInfo = 
{
	&t5352_TI, "IsReadOnly", &m28007_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5352_PIs[] =
{
	&t5352____Count_PropertyInfo,
	&t5352____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28006_GM;
MethodInfo m28006_MI = 
{
	"get_Count", NULL, &t5352_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28006_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28007_GM;
MethodInfo m28007_MI = 
{
	"get_IsReadOnly", NULL, &t5352_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28007_GM};
extern Il2CppType t172_0_0_0;
extern Il2CppType t172_0_0_0;
static ParameterInfo t5352_m28008_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t172_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28008_GM;
MethodInfo m28008_MI = 
{
	"Add", NULL, &t5352_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5352_m28008_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28008_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28009_GM;
MethodInfo m28009_MI = 
{
	"Clear", NULL, &t5352_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28009_GM};
extern Il2CppType t172_0_0_0;
static ParameterInfo t5352_m28010_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t172_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28010_GM;
MethodInfo m28010_MI = 
{
	"Contains", NULL, &t5352_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5352_m28010_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28010_GM};
extern Il2CppType t3839_0_0_0;
extern Il2CppType t3839_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5352_m28011_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3839_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28011_GM;
MethodInfo m28011_MI = 
{
	"CopyTo", NULL, &t5352_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5352_m28011_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28011_GM};
extern Il2CppType t172_0_0_0;
static ParameterInfo t5352_m28012_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t172_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28012_GM;
MethodInfo m28012_MI = 
{
	"Remove", NULL, &t5352_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5352_m28012_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28012_GM};
static MethodInfo* t5352_MIs[] =
{
	&m28006_MI,
	&m28007_MI,
	&m28008_MI,
	&m28009_MI,
	&m28010_MI,
	&m28011_MI,
	&m28012_MI,
	NULL
};
extern TypeInfo t5354_TI;
static TypeInfo* t5352_ITIs[] = 
{
	&t603_TI,
	&t5354_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5352_0_0_0;
extern Il2CppType t5352_1_0_0;
struct t5352;
extern Il2CppGenericClass t5352_GC;
TypeInfo t5352_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5352_MIs, t5352_PIs, NULL, NULL, NULL, NULL, NULL, &t5352_TI, t5352_ITIs, NULL, &EmptyCustomAttributesCache, &t5352_TI, &t5352_0_0_0, &t5352_1_0_0, NULL, &t5352_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Type>
extern Il2CppType t4178_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28013_GM;
MethodInfo m28013_MI = 
{
	"GetEnumerator", NULL, &t5354_TI, &t4178_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28013_GM};
static MethodInfo* t5354_MIs[] =
{
	&m28013_MI,
	NULL
};
static TypeInfo* t5354_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5354_0_0_0;
extern Il2CppType t5354_1_0_0;
struct t5354;
extern Il2CppGenericClass t5354_GC;
TypeInfo t5354_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5354_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5354_TI, t5354_ITIs, NULL, &EmptyCustomAttributesCache, &t5354_TI, &t5354_0_0_0, &t5354_1_0_0, NULL, &t5354_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5353_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/Type>
extern MethodInfo m28014_MI;
extern MethodInfo m28015_MI;
static PropertyInfo t5353____Item_PropertyInfo = 
{
	&t5353_TI, "Item", &m28014_MI, &m28015_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5353_PIs[] =
{
	&t5353____Item_PropertyInfo,
	NULL
};
extern Il2CppType t172_0_0_0;
static ParameterInfo t5353_m28016_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t172_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28016_GM;
MethodInfo m28016_MI = 
{
	"IndexOf", NULL, &t5353_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5353_m28016_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28016_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t172_0_0_0;
static ParameterInfo t5353_m28017_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t172_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28017_GM;
MethodInfo m28017_MI = 
{
	"Insert", NULL, &t5353_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5353_m28017_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28017_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5353_m28018_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28018_GM;
MethodInfo m28018_MI = 
{
	"RemoveAt", NULL, &t5353_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5353_m28018_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28018_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5353_m28014_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t172_0_0_0;
extern void* RuntimeInvoker_t172_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28014_GM;
MethodInfo m28014_MI = 
{
	"get_Item", NULL, &t5353_TI, &t172_0_0_0, RuntimeInvoker_t172_t44, t5353_m28014_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28014_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t172_0_0_0;
static ParameterInfo t5353_m28015_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t172_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28015_GM;
MethodInfo m28015_MI = 
{
	"set_Item", NULL, &t5353_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5353_m28015_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28015_GM};
static MethodInfo* t5353_MIs[] =
{
	&m28016_MI,
	&m28017_MI,
	&m28018_MI,
	&m28014_MI,
	&m28015_MI,
	NULL
};
static TypeInfo* t5353_ITIs[] = 
{
	&t603_TI,
	&t5352_TI,
	&t5354_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5353_0_0_0;
extern Il2CppType t5353_1_0_0;
struct t5353;
extern Il2CppGenericClass t5353_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5353_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5353_MIs, t5353_PIs, NULL, NULL, NULL, NULL, NULL, &t5353_TI, t5353_ITIs, NULL, &t1908__CustomAttributeCache, &t5353_TI, &t5353_0_0_0, &t5353_1_0_0, NULL, &t5353_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4179_TI;

#include "t173.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/FillMethod>
extern MethodInfo m28019_MI;
static PropertyInfo t4179____Current_PropertyInfo = 
{
	&t4179_TI, "Current", &m28019_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4179_PIs[] =
{
	&t4179____Current_PropertyInfo,
	NULL
};
extern Il2CppType t173_0_0_0;
extern void* RuntimeInvoker_t173 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28019_GM;
MethodInfo m28019_MI = 
{
	"get_Current", NULL, &t4179_TI, &t173_0_0_0, RuntimeInvoker_t173, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28019_GM};
static MethodInfo* t4179_MIs[] =
{
	&m28019_MI,
	NULL
};
static TypeInfo* t4179_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4179_0_0_0;
extern Il2CppType t4179_1_0_0;
struct t4179;
extern Il2CppGenericClass t4179_GC;
TypeInfo t4179_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4179_MIs, t4179_PIs, NULL, NULL, NULL, NULL, NULL, &t4179_TI, t4179_ITIs, NULL, &EmptyCustomAttributesCache, &t4179_TI, &t4179_0_0_0, &t4179_1_0_0, NULL, &t4179_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2594.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2594_TI;
#include "t2594MD.h"

extern TypeInfo t173_TI;
extern MethodInfo m13985_MI;
extern MethodInfo m21115_MI;
struct t20;
 int32_t m21115 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m13981_MI;
 void m13981 (t2594 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13982_MI;
 t29 * m13982 (t2594 * __this, MethodInfo* method){
	{
		int32_t L_0 = m13985(__this, &m13985_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t173_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13983_MI;
 void m13983 (t2594 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13984_MI;
 bool m13984 (t2594 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m13985 (t2594 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21115(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21115_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>
extern Il2CppType t20_0_0_1;
FieldInfo t2594_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2594_TI, offsetof(t2594, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2594_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2594_TI, offsetof(t2594, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2594_FIs[] =
{
	&t2594_f0_FieldInfo,
	&t2594_f1_FieldInfo,
	NULL
};
static PropertyInfo t2594____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2594_TI, "System.Collections.IEnumerator.Current", &m13982_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2594____Current_PropertyInfo = 
{
	&t2594_TI, "Current", &m13985_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2594_PIs[] =
{
	&t2594____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2594____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2594_m13981_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13981_GM;
MethodInfo m13981_MI = 
{
	".ctor", (methodPointerType)&m13981, &t2594_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2594_m13981_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13981_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13982_GM;
MethodInfo m13982_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13982, &t2594_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13982_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13983_GM;
MethodInfo m13983_MI = 
{
	"Dispose", (methodPointerType)&m13983, &t2594_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13983_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13984_GM;
MethodInfo m13984_MI = 
{
	"MoveNext", (methodPointerType)&m13984, &t2594_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13984_GM};
extern Il2CppType t173_0_0_0;
extern void* RuntimeInvoker_t173 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13985_GM;
MethodInfo m13985_MI = 
{
	"get_Current", (methodPointerType)&m13985, &t2594_TI, &t173_0_0_0, RuntimeInvoker_t173, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13985_GM};
static MethodInfo* t2594_MIs[] =
{
	&m13981_MI,
	&m13982_MI,
	&m13983_MI,
	&m13984_MI,
	&m13985_MI,
	NULL
};
static MethodInfo* t2594_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13982_MI,
	&m13984_MI,
	&m13983_MI,
	&m13985_MI,
};
static TypeInfo* t2594_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4179_TI,
};
static Il2CppInterfaceOffsetPair t2594_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4179_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2594_0_0_0;
extern Il2CppType t2594_1_0_0;
extern Il2CppGenericClass t2594_GC;
TypeInfo t2594_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2594_MIs, t2594_PIs, t2594_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2594_TI, t2594_ITIs, t2594_VT, &EmptyCustomAttributesCache, &t2594_TI, &t2594_0_0_0, &t2594_1_0_0, t2594_IOs, &t2594_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2594)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
