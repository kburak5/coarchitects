﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3290;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m18322_gshared (t3290 * __this, t29 * p0, t35 p1, MethodInfo* method);
#define m18322(__this, p0, p1, method) (void)m18322_gshared((t3290 *)__this, (t29 *)p0, (t35)p1, method)
 t29 * m18323_gshared (t3290 * __this, t29 * p0, MethodInfo* method);
#define m18323(__this, p0, method) (t29 *)m18323_gshared((t3290 *)__this, (t29 *)p0, method)
 t29 * m18324_gshared (t3290 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method);
#define m18324(__this, p0, p1, p2, method) (t29 *)m18324_gshared((t3290 *)__this, (t29 *)p0, (t67 *)p1, (t29 *)p2, method)
 t29 * m18325_gshared (t3290 * __this, t29 * p0, MethodInfo* method);
#define m18325(__this, p0, method) (t29 *)m18325_gshared((t3290 *)__this, (t29 *)p0, method)
