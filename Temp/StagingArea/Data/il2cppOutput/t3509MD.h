﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3509;
struct t29;
struct t20;
#include "t1677.h"

 void m19484 (t3509 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19485 (t3509 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19486 (t3509 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19487 (t3509 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m19488 (t3509 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
