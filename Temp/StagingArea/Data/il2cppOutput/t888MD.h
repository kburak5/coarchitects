﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t888;
struct t878;
struct t879;
#include "t845.h"

 void m3829 (t888 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3830 (t888 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3831 (t888 * __this, int32_t* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3832 (t888 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t879 * m3833 (t888 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
