﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3010;
struct t29;
struct t20;
#include "t517.h"

 void m16454 (t3010 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16455 (t3010 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16456 (t3010 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16457 (t3010 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t517  m16458 (t3010 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
