﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t220.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t220_TI;
#include "t220MD.h"

#include "t21.h"
#include "t221MD.h"
extern MethodInfo m1838_MI;

#include "t20.h"

extern MethodInfo m774_MI;
 void m774 (t220 * __this, MethodInfo* method){
	{
		m1838(__this, &m1838_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.ScrollRect/ScrollRectEvent
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m774_MI = 
{
	".ctor", (methodPointerType)&m774, &t220_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 810, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t220_MIs[] =
{
	&m774_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1323_MI;
extern MethodInfo m1324_MI;
extern MethodInfo m1325_MI;
extern MethodInfo m1839_MI;
extern MethodInfo m1840_MI;
static MethodInfo* t220_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m1839_MI,
	&m1840_MI,
};
extern TypeInfo t301_TI;
static Il2CppInterfaceOffsetPair t220_IOs[] = 
{
	{ &t301_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t220_0_0_0;
extern Il2CppType t220_1_0_0;
extern TypeInfo t221_TI;
struct t220;
extern TypeInfo t222_TI;
TypeInfo t220_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ScrollRectEvent", "", t220_MIs, NULL, NULL, NULL, &t221_TI, NULL, &t222_TI, &t220_TI, NULL, t220_VT, &EmptyCustomAttributesCache, &t220_TI, &t220_0_0_0, &t220_1_0_0, t220_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t220), 0, -1, 0, 0, -1, 1056770, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 8, 0, 1};
#include "t222.h"
#ifndef _MSC_VER
#else
#endif
#include "t222MD.h"

#include "t2.h"
#include "t40.h"
#include "t219.h"
#include "t22.h"
#include "t217.h"
#include "t41.h"
#include "t213.h"
#include "t395.h"
#include "t29.h"
#include "t35.h"
#include "t25.h"
#include "t17.h"
#include "t44.h"
#include "t23.h"
#include "t224.h"
#include "UnityEngine_ArrayTypes.h"
#include "t140.h"
#include "t55.h"
#include "t6.h"
#include "t111.h"
#include "t24.h"
#include "t164.h"
#include "t396.h"
extern TypeInfo t395_TI;
extern TypeInfo t2_TI;
extern TypeInfo t17_TI;
extern TypeInfo t23_TI;
extern TypeInfo t223_TI;
extern TypeInfo t55_TI;
extern TypeInfo t21_TI;
extern TypeInfo t141_TI;
extern TypeInfo t40_TI;
extern TypeInfo t27_TI;
extern TypeInfo t358_TI;
extern TypeInfo t224_TI;
#include "t41MD.h"
#include "t217MD.h"
#include "t395MD.h"
#include "t214MD.h"
#include "t28MD.h"
#include "t17MD.h"
#include "t224MD.h"
#include "t55MD.h"
#include "t141MD.h"
#include "t3MD.h"
#include "t6MD.h"
#include "t27MD.h"
#include "t2MD.h"
#include "t358MD.h"
#include "t328MD.h"
#include "t23MD.h"
#include "t25MD.h"
#include "t164MD.h"
#include "t396MD.h"
extern MethodInfo m1293_MI;
extern MethodInfo m745_MI;
extern MethodInfo m822_MI;
extern MethodInfo m1841_MI;
extern MethodInfo m1842_MI;
extern MethodInfo m1843_MI;
extern MethodInfo m823_MI;
extern MethodInfo m1297_MI;
extern MethodInfo m39_MI;
extern MethodInfo m818_MI;
extern MethodInfo m820_MI;
extern MethodInfo m62_MI;
extern MethodInfo m824_MI;
extern MethodInfo m826_MI;
extern MethodInfo m1844_MI;
extern MethodInfo m1845_MI;
extern MethodInfo m1394_MI;
extern MethodInfo m168_MI;
extern MethodInfo m815_MI;
extern MethodInfo m814_MI;
extern MethodInfo m170_MI;
extern MethodInfo m386_MI;
extern MethodInfo m390_MI;
extern MethodInfo m172_MI;
extern MethodInfo m174_MI;
extern MethodInfo m1300_MI;
extern MethodInfo m393_MI;
extern MethodInfo m1846_MI;
extern MethodInfo m804_MI;
extern MethodInfo m805_MI;
extern MethodInfo m224_MI;
extern MethodInfo m780_MI;
extern MethodInfo m778_MI;
extern MethodInfo m1401_MI;
extern MethodInfo m1794_MI;
extern MethodInfo m1668_MI;
extern MethodInfo m1670_MI;
extern MethodInfo m1416_MI;
extern MethodInfo m828_MI;
extern MethodInfo m812_MI;
extern MethodInfo m230_MI;
extern MethodInfo m798_MI;
extern MethodInfo m210_MI;
extern MethodInfo m235_MI;
extern MethodInfo m1677_MI;
extern MethodInfo m825_MI;
extern MethodInfo m1792_MI;
extern MethodInfo m1797_MI;
extern MethodInfo m1515_MI;
extern MethodInfo m1672_MI;
extern MethodInfo m1847_MI;
extern MethodInfo m1687_MI;
extern MethodInfo m1848_MI;
extern MethodInfo m1754_MI;
extern MethodInfo m1463_MI;
extern MethodInfo m1849_MI;
extern MethodInfo m1419_MI;
extern MethodInfo m1850_MI;
extern MethodInfo m816_MI;
extern MethodInfo m1851_MI;
extern MethodInfo m1643_MI;
extern MethodInfo m742_MI;
extern MethodInfo m740_MI;
extern MethodInfo m1852_MI;
extern MethodInfo m42_MI;
extern MethodInfo m1853_MI;
extern MethodInfo m44_MI;
extern MethodInfo m1854_MI;
extern MethodInfo m1572_MI;
extern MethodInfo m1834_MI;
extern MethodInfo m1653_MI;
extern MethodInfo m1855_MI;
extern MethodInfo m827_MI;
extern MethodInfo m1856_MI;
extern MethodInfo m1624_MI;
extern MethodInfo m1658_MI;
extern MethodInfo m1857_MI;
extern MethodInfo m1858_MI;
extern MethodInfo m49_MI;
extern MethodInfo m1859_MI;
extern MethodInfo m1860_MI;
extern MethodInfo m1861_MI;
extern MethodInfo m1862_MI;
extern MethodInfo m1863_MI;
extern MethodInfo m1395_MI;
extern MethodInfo m1864_MI;
extern MethodInfo m1865_MI;
extern MethodInfo m180_MI;


extern MethodInfo m775_MI;
 void m775 (t222 * __this, MethodInfo* method){
	{
		__this->f3 = 1;
		__this->f4 = 1;
		__this->f5 = 1;
		__this->f6 = (0.1f);
		__this->f7 = 1;
		__this->f8 = (0.135f);
		__this->f9 = (1.0f);
		t220 * L_0 = (t220 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t220_TI));
		m774(L_0, &m774_MI);
		__this->f12 = L_0;
		t17  L_1 = m1394(NULL, &m1394_MI);
		__this->f13 = L_1;
		t17  L_2 = m1394(NULL, &m1394_MI);
		__this->f14 = L_2;
		t17  L_3 = m1394(NULL, &m1394_MI);
		__this->f20 = L_3;
		__this->f24 = ((t223*)SZArrayNew(InitializedTypeInfo(&t223_TI), 4));
		m168(__this, &m168_MI);
		return;
	}
}
extern MethodInfo m776_MI;
 t2 * m776 (t222 * __this, MethodInfo* method){
	{
		t2 * L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m777_MI;
 void m777 (t222 * __this, t2 * p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
 bool m778 (t222 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m779_MI;
 void m779 (t222 * __this, bool p0, MethodInfo* method){
	{
		__this->f3 = p0;
		return;
	}
}
 bool m780 (t222 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m781_MI;
 void m781 (t222 * __this, bool p0, MethodInfo* method){
	{
		__this->f4 = p0;
		return;
	}
}
extern MethodInfo m782_MI;
 int32_t m782 (t222 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f5);
		return L_0;
	}
}
extern MethodInfo m783_MI;
 void m783 (t222 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f5 = p0;
		return;
	}
}
extern MethodInfo m784_MI;
 float m784 (t222 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f6);
		return L_0;
	}
}
extern MethodInfo m785_MI;
 void m785 (t222 * __this, float p0, MethodInfo* method){
	{
		__this->f6 = p0;
		return;
	}
}
extern MethodInfo m786_MI;
 bool m786 (t222 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f7);
		return L_0;
	}
}
extern MethodInfo m787_MI;
 void m787 (t222 * __this, bool p0, MethodInfo* method){
	{
		__this->f7 = p0;
		return;
	}
}
extern MethodInfo m788_MI;
 float m788 (t222 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f8);
		return L_0;
	}
}
extern MethodInfo m789_MI;
 void m789 (t222 * __this, float p0, MethodInfo* method){
	{
		__this->f8 = p0;
		return;
	}
}
extern MethodInfo m790_MI;
 float m790 (t222 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f9);
		return L_0;
	}
}
extern MethodInfo m791_MI;
 void m791 (t222 * __this, float p0, MethodInfo* method){
	{
		__this->f9 = p0;
		return;
	}
}
extern MethodInfo m792_MI;
 t217 * m792 (t222 * __this, MethodInfo* method){
	{
		t217 * L_0 = (__this->f10);
		return L_0;
	}
}
extern MethodInfo m793_MI;
 void m793 (t222 * __this, t217 * p0, MethodInfo* method){
	{
		t217 * L_0 = (__this->f10);
		bool L_1 = m1293(NULL, L_0, &m1293_MI);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		t217 * L_2 = (__this->f10);
		t213 * L_3 = m745(L_2, &m745_MI);
		t35 L_4 = { &m822_MI };
		t395 * L_5 = (t395 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t395_TI));
		m1841(L_5, __this, L_4, &m1841_MI);
		m1842(L_3, L_5, &m1842_MI);
	}

IL_002c:
	{
		__this->f10 = p0;
		t217 * L_6 = (__this->f10);
		bool L_7 = m1293(NULL, L_6, &m1293_MI);
		if (!L_7)
		{
			goto IL_005f;
		}
	}
	{
		t217 * L_8 = (__this->f10);
		t213 * L_9 = m745(L_8, &m745_MI);
		t35 L_10 = { &m822_MI };
		t395 * L_11 = (t395 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t395_TI));
		m1841(L_11, __this, L_10, &m1841_MI);
		m1843(L_9, L_11, &m1843_MI);
	}

IL_005f:
	{
		return;
	}
}
extern MethodInfo m794_MI;
 t217 * m794 (t222 * __this, MethodInfo* method){
	{
		t217 * L_0 = (__this->f11);
		return L_0;
	}
}
extern MethodInfo m795_MI;
 void m795 (t222 * __this, t217 * p0, MethodInfo* method){
	{
		t217 * L_0 = (__this->f11);
		bool L_1 = m1293(NULL, L_0, &m1293_MI);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		t217 * L_2 = (__this->f11);
		t213 * L_3 = m745(L_2, &m745_MI);
		t35 L_4 = { &m823_MI };
		t395 * L_5 = (t395 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t395_TI));
		m1841(L_5, __this, L_4, &m1841_MI);
		m1842(L_3, L_5, &m1842_MI);
	}

IL_002c:
	{
		__this->f11 = p0;
		t217 * L_6 = (__this->f11);
		bool L_7 = m1293(NULL, L_6, &m1293_MI);
		if (!L_7)
		{
			goto IL_005f;
		}
	}
	{
		t217 * L_8 = (__this->f11);
		t213 * L_9 = m745(L_8, &m745_MI);
		t35 L_10 = { &m823_MI };
		t395 * L_11 = (t395 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t395_TI));
		m1841(L_11, __this, L_10, &m1841_MI);
		m1843(L_9, L_11, &m1843_MI);
	}

IL_005f:
	{
		return;
	}
}
extern MethodInfo m796_MI;
 t220 * m796 (t222 * __this, MethodInfo* method){
	{
		t220 * L_0 = (__this->f12);
		return L_0;
	}
}
extern MethodInfo m797_MI;
 void m797 (t222 * __this, t220 * p0, MethodInfo* method){
	{
		__this->f12 = p0;
		return;
	}
}
 t2 * m798 (t222 * __this, MethodInfo* method){
	{
		t2 * L_0 = (__this->f15);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		t25 * L_2 = m39(__this, &m39_MI);
		__this->f15 = ((t2 *)Castclass(L_2, InitializedTypeInfo(&t2_TI)));
	}

IL_0022:
	{
		t2 * L_3 = (__this->f15);
		return L_3;
	}
}
extern MethodInfo m799_MI;
 t17  m799 (t222 * __this, MethodInfo* method){
	{
		t17  L_0 = (__this->f18);
		return L_0;
	}
}
extern MethodInfo m800_MI;
 void m800 (t222 * __this, t17  p0, MethodInfo* method){
	{
		__this->f18 = p0;
		return;
	}
}
extern MethodInfo m801_MI;
 void m801 (t222 * __this, int32_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) == ((int32_t)2)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		m826(__this, &m826_MI);
		t17  L_0 = m1394(NULL, &m1394_MI);
		m815(__this, L_0, &m815_MI);
		m814(__this, &m814_MI);
		__this->f23 = 1;
		return;
	}
}
extern MethodInfo m802_MI;
 void m802 (t222 * __this, MethodInfo* method){
	{
		m170(__this, &m170_MI);
		t217 * L_0 = (__this->f10);
		bool L_1 = m1293(NULL, L_0, &m1293_MI);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		t217 * L_2 = (__this->f10);
		t213 * L_3 = m745(L_2, &m745_MI);
		t35 L_4 = { &m822_MI };
		t395 * L_5 = (t395 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t395_TI));
		m1841(L_5, __this, L_4, &m1841_MI);
		m1843(L_3, L_5, &m1843_MI);
	}

IL_0032:
	{
		t217 * L_6 = (__this->f11);
		bool L_7 = m1293(NULL, L_6, &m1293_MI);
		if (!L_7)
		{
			goto IL_005e;
		}
	}
	{
		t217 * L_8 = (__this->f11);
		t213 * L_9 = m745(L_8, &m745_MI);
		t35 L_10 = { &m823_MI };
		t395 * L_11 = (t395 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t395_TI));
		m1841(L_11, __this, L_10, &m1841_MI);
		m1843(L_9, L_11, &m1843_MI);
	}

IL_005e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t141_TI));
		m386(NULL, __this, &m386_MI);
		return;
	}
}
extern MethodInfo m803_MI;
 void m803 (t222 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t141_TI));
		m390(NULL, __this, &m390_MI);
		t217 * L_0 = (__this->f10);
		bool L_1 = m1293(NULL, L_0, &m1293_MI);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		t217 * L_2 = (__this->f10);
		t213 * L_3 = m745(L_2, &m745_MI);
		t35 L_4 = { &m822_MI };
		t395 * L_5 = (t395 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t395_TI));
		m1841(L_5, __this, L_4, &m1841_MI);
		m1842(L_3, L_5, &m1842_MI);
	}

IL_0032:
	{
		t217 * L_6 = (__this->f11);
		bool L_7 = m1293(NULL, L_6, &m1293_MI);
		if (!L_7)
		{
			goto IL_005e;
		}
	}
	{
		t217 * L_8 = (__this->f11);
		t213 * L_9 = m745(L_8, &m745_MI);
		t35 L_10 = { &m823_MI };
		t395 * L_11 = (t395 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t395_TI));
		m1841(L_11, __this, L_10, &m1841_MI);
		m1842(L_9, L_11, &m1842_MI);
	}

IL_005e:
	{
		__this->f23 = 0;
		m172(__this, &m172_MI);
		return;
	}
}
 bool m804 (t222 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		bool L_0 = m174(__this, &m174_MI);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		t2 * L_1 = (__this->f2);
		bool L_2 = m1300(NULL, L_1, (t41 *)NULL, &m1300_MI);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		return G_B3_0;
	}
}
 void m805 (t222 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f23);
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t141_TI));
		bool L_1 = m393(NULL, &m393_MI);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		m1846(NULL, &m1846_MI);
	}

IL_001a:
	{
		return;
	}
}
extern MethodInfo m806_MI;
 void m806 (t222 * __this, MethodInfo* method){
	{
		t17  L_0 = m1394(NULL, &m1394_MI);
		__this->f18 = L_0;
		return;
	}
}
extern MethodInfo m807_MI;
 void m807 (t222 * __this, t6 * p0, MethodInfo* method){
	t17  V_0 = {0};
	t17  V_1 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m804_MI, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		m805(__this, &m805_MI);
		m826(__this, &m826_MI);
		t17  L_1 = m224(p0, &m224_MI);
		V_0 = L_1;
		t17 * L_2 = (&V_0);
		float L_3 = (L_2->f2);
		L_2->f2 = ((float)((float)L_3*(float)(-1.0f)));
		bool L_4 = m780(__this, &m780_MI);
		if (!L_4)
		{
			goto IL_007f;
		}
	}
	{
		bool L_5 = m778(__this, &m778_MI);
		if (L_5)
		{
			goto IL_007f;
		}
	}
	{
		float L_6 = ((&V_0)->f1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_7 = fabsf(L_6);
		float L_8 = ((&V_0)->f2);
		float L_9 = fabsf(L_8);
		if ((((float)L_7) <= ((float)L_9)))
		{
			goto IL_0073;
		}
	}
	{
		float L_10 = ((&V_0)->f1);
		(&V_0)->f2 = L_10;
	}

IL_0073:
	{
		(&V_0)->f1 = (0.0f);
	}

IL_007f:
	{
		bool L_11 = m778(__this, &m778_MI);
		if (!L_11)
		{
			goto IL_00cc;
		}
	}
	{
		bool L_12 = m780(__this, &m780_MI);
		if (L_12)
		{
			goto IL_00cc;
		}
	}
	{
		float L_13 = ((&V_0)->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_14 = fabsf(L_13);
		float L_15 = ((&V_0)->f1);
		float L_16 = fabsf(L_15);
		if ((((float)L_14) <= ((float)L_16)))
		{
			goto IL_00c0;
		}
	}
	{
		float L_17 = ((&V_0)->f2);
		(&V_0)->f1 = L_17;
	}

IL_00c0:
	{
		(&V_0)->f2 = (0.0f);
	}

IL_00cc:
	{
		t2 * L_18 = (__this->f2);
		t17  L_19 = m1794(L_18, &m1794_MI);
		V_1 = L_19;
		float L_20 = (__this->f9);
		t17  L_21 = m1668(NULL, V_0, L_20, &m1668_MI);
		t17  L_22 = m1670(NULL, V_1, L_21, &m1670_MI);
		V_1 = L_22;
		int32_t L_23 = (__this->f5);
		if ((((uint32_t)L_23) != ((uint32_t)2)))
		{
			goto IL_0115;
		}
	}
	{
		t2 * L_24 = (__this->f2);
		t17  L_25 = m1794(L_24, &m1794_MI);
		t17  L_26 = m1416(NULL, V_1, L_25, &m1416_MI);
		t17  L_27 = m828(__this, L_26, &m828_MI);
		t17  L_28 = m1670(NULL, V_1, L_27, &m1670_MI);
		V_1 = L_28;
	}

IL_0115:
	{
		VirtActionInvoker1< t17  >::Invoke(&m812_MI, __this, V_1);
		m826(__this, &m826_MI);
		return;
	}
}
extern MethodInfo m808_MI;
 void m808 (t222 * __this, t6 * p0, MethodInfo* method){
	{
		t17  L_0 = m1394(NULL, &m1394_MI);
		__this->f18 = L_0;
		return;
	}
}
extern MethodInfo m809_MI;
 void m809 (t222 * __this, t6 * p0, MethodInfo* method){
	{
		int32_t L_0 = m230(p0, &m230_MI);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m804_MI, __this);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		m826(__this, &m826_MI);
		t17  L_2 = m1394(NULL, &m1394_MI);
		__this->f13 = L_2;
		t2 * L_3 = m798(__this, &m798_MI);
		t17  L_4 = m210(p0, &m210_MI);
		t24 * L_5 = m235(p0, &m235_MI);
		t17 * L_6 = &(__this->f13);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		m1677(NULL, L_3, L_4, L_5, L_6, &m1677_MI);
		t2 * L_7 = (__this->f2);
		t17  L_8 = m1794(L_7, &m1794_MI);
		__this->f14 = L_8;
		__this->f19 = 1;
		return;
	}
}
extern MethodInfo m810_MI;
 void m810 (t222 * __this, t6 * p0, MethodInfo* method){
	{
		int32_t L_0 = m230(p0, &m230_MI);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->f19 = 0;
		return;
	}
}
extern MethodInfo m811_MI;
 void m811 (t222 * __this, t6 * p0, MethodInfo* method){
	t17  V_0 = {0};
	t17  V_1 = {0};
	t17  V_2 = {0};
	t17  V_3 = {0};
	t23  V_4 = {0};
	t23  V_5 = {0};
	{
		int32_t L_0 = m230(p0, &m230_MI);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m804_MI, __this);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return;
	}

IL_0018:
	{
		t2 * L_2 = m798(__this, &m798_MI);
		t17  L_3 = m210(p0, &m210_MI);
		t24 * L_4 = m235(p0, &m235_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		bool L_5 = m1677(NULL, L_2, L_3, L_4, (&V_0), &m1677_MI);
		if (L_5)
		{
			goto IL_0037;
		}
	}
	{
		return;
	}

IL_0037:
	{
		m826(__this, &m826_MI);
		t17  L_6 = (__this->f13);
		t17  L_7 = m1416(NULL, V_0, L_6, &m1416_MI);
		V_1 = L_7;
		t17  L_8 = (__this->f14);
		t17  L_9 = m1670(NULL, L_8, V_1, &m1670_MI);
		V_2 = L_9;
		t2 * L_10 = (__this->f2);
		t17  L_11 = m1794(L_10, &m1794_MI);
		t17  L_12 = m1416(NULL, V_2, L_11, &m1416_MI);
		t17  L_13 = m828(__this, L_12, &m828_MI);
		V_3 = L_13;
		t17  L_14 = m1670(NULL, V_2, V_3, &m1670_MI);
		V_2 = L_14;
		int32_t L_15 = (__this->f5);
		if ((((uint32_t)L_15) != ((uint32_t)1)))
		{
			goto IL_0103;
		}
	}
	{
		float L_16 = ((&V_3)->f1);
		if ((((float)L_16) == ((float)(0.0f))))
		{
			goto IL_00c3;
		}
	}
	{
		float L_17 = ((&V_2)->f1);
		float L_18 = ((&V_3)->f1);
		t224 * L_19 = &(__this->f17);
		t23  L_20 = m1844(L_19, &m1844_MI);
		V_4 = L_20;
		float L_21 = ((&V_4)->f1);
		float L_22 = m825(NULL, L_18, L_21, &m825_MI);
		(&V_2)->f1 = ((float)(L_17-L_22));
	}

IL_00c3:
	{
		float L_23 = ((&V_3)->f2);
		if ((((float)L_23) == ((float)(0.0f))))
		{
			goto IL_0103;
		}
	}
	{
		float L_24 = ((&V_2)->f2);
		float L_25 = ((&V_3)->f2);
		t224 * L_26 = &(__this->f17);
		t23  L_27 = m1844(L_26, &m1844_MI);
		V_5 = L_27;
		float L_28 = ((&V_5)->f2);
		float L_29 = m825(NULL, L_25, L_28, &m825_MI);
		(&V_2)->f2 = ((float)(L_24-L_29));
	}

IL_0103:
	{
		VirtActionInvoker1< t17  >::Invoke(&m812_MI, __this, V_2);
		return;
	}
}
 void m812 (t222 * __this, t17  p0, MethodInfo* method){
	t17  V_0 = {0};
	t17  V_1 = {0};
	{
		bool L_0 = (__this->f3);
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		t2 * L_1 = (__this->f2);
		t17  L_2 = m1794(L_1, &m1794_MI);
		V_0 = L_2;
		float L_3 = ((&V_0)->f1);
		(&p0)->f1 = L_3;
	}

IL_0025:
	{
		bool L_4 = (__this->f4);
		if (L_4)
		{
			goto IL_004a;
		}
	}
	{
		t2 * L_5 = (__this->f2);
		t17  L_6 = m1794(L_5, &m1794_MI);
		V_1 = L_6;
		float L_7 = ((&V_1)->f2);
		(&p0)->f2 = L_7;
	}

IL_004a:
	{
		t2 * L_8 = (__this->f2);
		t17  L_9 = m1794(L_8, &m1794_MI);
		bool L_10 = m1792(NULL, p0, L_9, &m1792_MI);
		if (!L_10)
		{
			goto IL_0072;
		}
	}
	{
		t2 * L_11 = (__this->f2);
		m1797(L_11, p0, &m1797_MI);
		m826(__this, &m826_MI);
	}

IL_0072:
	{
		return;
	}
}
extern MethodInfo m813_MI;
 void m813 (t222 * __this, MethodInfo* method){
	float V_0 = 0.0f;
	t17  V_1 = {0};
	t17  V_2 = {0};
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	t23  V_5 = {0};
	t17  V_6 = {0};
	t17  V_7 = {0};
	t17 * V_8 = {0};
	int32_t V_9 = 0;
	float V_10 = 0.0f;
	t17 * V_11 = {0};
	{
		t2 * L_0 = (__this->f2);
		bool L_1 = m1293(NULL, L_0, &m1293_MI);
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		m805(__this, &m805_MI);
		m826(__this, &m826_MI);
		float L_2 = m1515(NULL, &m1515_MI);
		V_0 = L_2;
		t17  L_3 = m1394(NULL, &m1394_MI);
		t17  L_4 = m828(__this, L_3, &m828_MI);
		V_1 = L_4;
		bool L_5 = (__this->f19);
		if (L_5)
		{
			goto IL_01f5;
		}
	}
	{
		t17  L_6 = m1394(NULL, &m1394_MI);
		bool L_7 = m1792(NULL, V_1, L_6, &m1792_MI);
		if (L_7)
		{
			goto IL_005f;
		}
	}
	{
		t17  L_8 = (__this->f18);
		t17  L_9 = m1394(NULL, &m1394_MI);
		bool L_10 = m1792(NULL, L_8, L_9, &m1792_MI);
		if (!L_10)
		{
			goto IL_01f5;
		}
	}

IL_005f:
	{
		t2 * L_11 = (__this->f2);
		t17  L_12 = m1794(L_11, &m1794_MI);
		V_2 = L_12;
		V_3 = 0;
		goto IL_01a6;
	}

IL_0072:
	{
		int32_t L_13 = (__this->f5);
		if ((((uint32_t)L_13) != ((uint32_t)1)))
		{
			goto IL_00ff;
		}
	}
	{
		float L_14 = m1672((&V_1), V_3, &m1672_MI);
		if ((((float)L_14) == ((float)(0.0f))))
		{
			goto IL_00ff;
		}
	}
	{
		t17 * L_15 = &(__this->f18);
		float L_16 = m1672(L_15, V_3, &m1672_MI);
		V_4 = L_16;
		t2 * L_17 = (__this->f2);
		t17  L_18 = m1794(L_17, &m1794_MI);
		V_6 = L_18;
		float L_19 = m1672((&V_6), V_3, &m1672_MI);
		t2 * L_20 = (__this->f2);
		t17  L_21 = m1794(L_20, &m1794_MI);
		V_7 = L_21;
		float L_22 = m1672((&V_7), V_3, &m1672_MI);
		float L_23 = m1672((&V_1), V_3, &m1672_MI);
		float L_24 = (__this->f6);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_25 = m1847(NULL, L_19, ((float)(L_22+L_23)), (&V_4), L_24, (std::numeric_limits<float>::infinity()), V_0, &m1847_MI);
		m1687((&V_2), V_3, L_25, &m1687_MI);
		t17 * L_26 = &(__this->f18);
		m1687(L_26, V_3, V_4, &m1687_MI);
		goto IL_01a2;
	}

IL_00ff:
	{
		bool L_27 = (__this->f7);
		if (!L_27)
		{
			goto IL_0191;
		}
	}
	{
		t17 * L_28 = &(__this->f18);
		t17 * L_29 = L_28;
		V_8 = L_29;
		int32_t L_30 = V_3;
		V_9 = L_30;
		float L_31 = m1672(V_8, V_9, &m1672_MI);
		V_10 = L_31;
		float L_32 = (__this->f8);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_33 = powf(L_32, V_0);
		m1687(L_29, L_30, ((float)((float)V_10*(float)L_33)), &m1687_MI);
		t17 * L_34 = &(__this->f18);
		float L_35 = m1672(L_34, V_3, &m1672_MI);
		float L_36 = fabsf(L_35);
		if ((((float)L_36) >= ((float)(1.0f))))
		{
			goto IL_0162;
		}
	}
	{
		t17 * L_37 = &(__this->f18);
		m1687(L_37, V_3, (0.0f), &m1687_MI);
	}

IL_0162:
	{
		t17 * L_38 = (&V_2);
		V_11 = L_38;
		int32_t L_39 = V_3;
		V_9 = L_39;
		float L_40 = m1672(V_11, V_9, &m1672_MI);
		V_10 = L_40;
		t17 * L_41 = &(__this->f18);
		float L_42 = m1672(L_41, V_3, &m1672_MI);
		m1687(L_38, L_39, ((float)(V_10+((float)((float)L_42*(float)V_0)))), &m1687_MI);
		goto IL_01a2;
	}

IL_0191:
	{
		t17 * L_43 = &(__this->f18);
		m1687(L_43, V_3, (0.0f), &m1687_MI);
	}

IL_01a2:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_01a6:
	{
		if ((((int32_t)V_3) < ((int32_t)2)))
		{
			goto IL_0072;
		}
	}
	{
		t17  L_44 = (__this->f18);
		t17  L_45 = m1394(NULL, &m1394_MI);
		bool L_46 = m1792(NULL, L_44, L_45, &m1792_MI);
		if (!L_46)
		{
			goto IL_01f5;
		}
	}
	{
		int32_t L_47 = (__this->f5);
		if ((((uint32_t)L_47) != ((uint32_t)2)))
		{
			goto IL_01ee;
		}
	}
	{
		t2 * L_48 = (__this->f2);
		t17  L_49 = m1794(L_48, &m1794_MI);
		t17  L_50 = m1416(NULL, V_2, L_49, &m1416_MI);
		t17  L_51 = m828(__this, L_50, &m828_MI);
		V_1 = L_51;
		t17  L_52 = m1670(NULL, V_2, V_1, &m1670_MI);
		V_2 = L_52;
	}

IL_01ee:
	{
		VirtActionInvoker1< t17  >::Invoke(&m812_MI, __this, V_2);
	}

IL_01f5:
	{
		bool L_53 = (__this->f19);
		if (!L_53)
		{
			goto IL_0252;
		}
	}
	{
		bool L_54 = (__this->f7);
		if (!L_54)
		{
			goto IL_0252;
		}
	}
	{
		t2 * L_55 = (__this->f2);
		t17  L_56 = m1794(L_55, &m1794_MI);
		t17  L_57 = (__this->f20);
		t17  L_58 = m1416(NULL, L_56, L_57, &m1416_MI);
		t17  L_59 = m1754(NULL, L_58, V_0, &m1754_MI);
		t23  L_60 = m1463(NULL, L_59, &m1463_MI);
		V_5 = L_60;
		t17  L_61 = (__this->f18);
		t23  L_62 = m1463(NULL, L_61, &m1463_MI);
		t23  L_63 = m1849(NULL, L_62, V_5, ((float)((float)V_0*(float)(10.0f))), &m1849_MI);
		t17  L_64 = m1419(NULL, L_63, &m1419_MI);
		__this->f18 = L_64;
	}

IL_0252:
	{
		t224  L_65 = (__this->f17);
		t224  L_66 = (__this->f22);
		bool L_67 = m1850(NULL, L_65, L_66, &m1850_MI);
		if (L_67)
		{
			goto IL_0299;
		}
	}
	{
		t224  L_68 = (__this->f16);
		t224  L_69 = (__this->f21);
		bool L_70 = m1850(NULL, L_68, L_69, &m1850_MI);
		if (L_70)
		{
			goto IL_0299;
		}
	}
	{
		t2 * L_71 = (__this->f2);
		t17  L_72 = m1794(L_71, &m1794_MI);
		t17  L_73 = (__this->f20);
		bool L_74 = m1792(NULL, L_72, L_73, &m1792_MI);
		if (!L_74)
		{
			goto IL_02b7;
		}
	}

IL_0299:
	{
		m815(__this, V_1, &m815_MI);
		t220 * L_75 = (__this->f12);
		t17  L_76 = m816(__this, &m816_MI);
		m1851(L_75, L_76, &m1851_MI);
		m814(__this, &m814_MI);
	}

IL_02b7:
	{
		return;
	}
}
 void m814 (t222 * __this, MethodInfo* method){
	{
		t2 * L_0 = (__this->f2);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		t17  L_2 = m1394(NULL, &m1394_MI);
		__this->f20 = L_2;
		goto IL_0032;
	}

IL_0021:
	{
		t2 * L_3 = (__this->f2);
		t17  L_4 = m1794(L_3, &m1794_MI);
		__this->f20 = L_4;
	}

IL_0032:
	{
		t224  L_5 = (__this->f17);
		__this->f22 = L_5;
		t224  L_6 = (__this->f16);
		__this->f21 = L_6;
		return;
	}
}
 void m815 (t222 * __this, t17  p0, MethodInfo* method){
	t23  V_0 = {0};
	t23  V_1 = {0};
	t23  V_2 = {0};
	t23  V_3 = {0};
	t23  V_4 = {0};
	t23  V_5 = {0};
	{
		t217 * L_0 = (__this->f10);
		bool L_1 = m1293(NULL, L_0, &m1293_MI);
		if (!L_1)
		{
			goto IL_0097;
		}
	}
	{
		t224 * L_2 = &(__this->f16);
		t23  L_3 = m1844(L_2, &m1844_MI);
		V_0 = L_3;
		float L_4 = ((&V_0)->f1);
		if ((((float)L_4) <= ((float)(0.0f))))
		{
			goto IL_0076;
		}
	}
	{
		t217 * L_5 = (__this->f10);
		t224 * L_6 = &(__this->f17);
		t23  L_7 = m1844(L_6, &m1844_MI);
		V_1 = L_7;
		float L_8 = ((&V_1)->f1);
		float L_9 = ((&p0)->f1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_10 = fabsf(L_9);
		t224 * L_11 = &(__this->f16);
		t23  L_12 = m1844(L_11, &m1844_MI);
		V_2 = L_12;
		float L_13 = ((&V_2)->f1);
		float L_14 = m1643(NULL, ((float)((float)((float)(L_8-L_10))/(float)L_13)), &m1643_MI);
		m742(L_5, L_14, &m742_MI);
		goto IL_0086;
	}

IL_0076:
	{
		t217 * L_15 = (__this->f10);
		m742(L_15, (1.0f), &m742_MI);
	}

IL_0086:
	{
		t217 * L_16 = (__this->f10);
		float L_17 = m818(__this, &m818_MI);
		m740(L_16, L_17, &m740_MI);
	}

IL_0097:
	{
		t217 * L_18 = (__this->f11);
		bool L_19 = m1293(NULL, L_18, &m1293_MI);
		if (!L_19)
		{
			goto IL_0130;
		}
	}
	{
		t224 * L_20 = &(__this->f16);
		t23  L_21 = m1844(L_20, &m1844_MI);
		V_3 = L_21;
		float L_22 = ((&V_3)->f2);
		if ((((float)L_22) <= ((float)(0.0f))))
		{
			goto IL_010f;
		}
	}
	{
		t217 * L_23 = (__this->f11);
		t224 * L_24 = &(__this->f17);
		t23  L_25 = m1844(L_24, &m1844_MI);
		V_4 = L_25;
		float L_26 = ((&V_4)->f2);
		float L_27 = ((&p0)->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_28 = fabsf(L_27);
		t224 * L_29 = &(__this->f16);
		t23  L_30 = m1844(L_29, &m1844_MI);
		V_5 = L_30;
		float L_31 = ((&V_5)->f2);
		float L_32 = m1643(NULL, ((float)((float)((float)(L_26-L_28))/(float)L_31)), &m1643_MI);
		m742(L_23, L_32, &m742_MI);
		goto IL_011f;
	}

IL_010f:
	{
		t217 * L_33 = (__this->f11);
		m742(L_33, (1.0f), &m742_MI);
	}

IL_011f:
	{
		t217 * L_34 = (__this->f11);
		float L_35 = m820(__this, &m820_MI);
		m740(L_34, L_35, &m740_MI);
	}

IL_0130:
	{
		return;
	}
}
 t17  m816 (t222 * __this, MethodInfo* method){
	{
		float L_0 = m818(__this, &m818_MI);
		float L_1 = m820(__this, &m820_MI);
		t17  L_2 = {0};
		m62(&L_2, L_0, L_1, &m62_MI);
		return L_2;
	}
}
extern MethodInfo m817_MI;
 void m817 (t222 * __this, t17  p0, MethodInfo* method){
	{
		float L_0 = ((&p0)->f1);
		m824(__this, L_0, 0, &m824_MI);
		float L_1 = ((&p0)->f2);
		m824(__this, L_1, 1, &m824_MI);
		return;
	}
}
 float m818 (t222 * __this, MethodInfo* method){
	t23  V_0 = {0};
	t23  V_1 = {0};
	t23  V_2 = {0};
	t23  V_3 = {0};
	t23  V_4 = {0};
	t23  V_5 = {0};
	t23  V_6 = {0};
	t23  V_7 = {0};
	int32_t G_B4_0 = 0;
	{
		m826(__this, &m826_MI);
		t224 * L_0 = &(__this->f16);
		t23  L_1 = m1844(L_0, &m1844_MI);
		V_0 = L_1;
		float L_2 = ((&V_0)->f1);
		t224 * L_3 = &(__this->f17);
		t23  L_4 = m1844(L_3, &m1844_MI);
		V_1 = L_4;
		float L_5 = ((&V_1)->f1);
		if ((((float)L_2) > ((float)L_5)))
		{
			goto IL_0065;
		}
	}
	{
		t224 * L_6 = &(__this->f17);
		t23  L_7 = m1845(L_6, &m1845_MI);
		V_2 = L_7;
		float L_8 = ((&V_2)->f1);
		t224 * L_9 = &(__this->f16);
		t23  L_10 = m1845(L_9, &m1845_MI);
		V_3 = L_10;
		float L_11 = ((&V_3)->f1);
		if ((((float)L_8) <= ((float)L_11)))
		{
			goto IL_0062;
		}
	}
	{
		G_B4_0 = 1;
		goto IL_0063;
	}

IL_0062:
	{
		G_B4_0 = 0;
	}

IL_0063:
	{
		return (((float)G_B4_0));
	}

IL_0065:
	{
		t224 * L_12 = &(__this->f17);
		t23  L_13 = m1845(L_12, &m1845_MI);
		V_4 = L_13;
		float L_14 = ((&V_4)->f1);
		t224 * L_15 = &(__this->f16);
		t23  L_16 = m1845(L_15, &m1845_MI);
		V_5 = L_16;
		float L_17 = ((&V_5)->f1);
		t224 * L_18 = &(__this->f16);
		t23  L_19 = m1844(L_18, &m1844_MI);
		V_6 = L_19;
		float L_20 = ((&V_6)->f1);
		t224 * L_21 = &(__this->f17);
		t23  L_22 = m1844(L_21, &m1844_MI);
		V_7 = L_22;
		float L_23 = ((&V_7)->f1);
		return ((float)((float)((float)(L_14-L_17))/(float)((float)(L_20-L_23))));
	}
}
extern MethodInfo m819_MI;
 void m819 (t222 * __this, float p0, MethodInfo* method){
	{
		m824(__this, p0, 0, &m824_MI);
		return;
	}
}
 float m820 (t222 * __this, MethodInfo* method){
	t23  V_0 = {0};
	t23  V_1 = {0};
	t23  V_2 = {0};
	t23  V_3 = {0};
	t23  V_4 = {0};
	t23  V_5 = {0};
	t23  V_6 = {0};
	t23  V_7 = {0};
	int32_t G_B4_0 = 0;
	{
		m826(__this, &m826_MI);
		t224 * L_0 = &(__this->f16);
		t23  L_1 = m1844(L_0, &m1844_MI);
		V_0 = L_1;
		float L_2 = ((&V_0)->f2);
		t224 * L_3 = &(__this->f17);
		t23  L_4 = m1844(L_3, &m1844_MI);
		V_1 = L_4;
		float L_5 = ((&V_1)->f2);
		if ((((float)L_2) > ((float)L_5)))
		{
			goto IL_0065;
		}
	}
	{
		t224 * L_6 = &(__this->f17);
		t23  L_7 = m1845(L_6, &m1845_MI);
		V_2 = L_7;
		float L_8 = ((&V_2)->f2);
		t224 * L_9 = &(__this->f16);
		t23  L_10 = m1845(L_9, &m1845_MI);
		V_3 = L_10;
		float L_11 = ((&V_3)->f2);
		if ((((float)L_8) <= ((float)L_11)))
		{
			goto IL_0062;
		}
	}
	{
		G_B4_0 = 1;
		goto IL_0063;
	}

IL_0062:
	{
		G_B4_0 = 0;
	}

IL_0063:
	{
		return (((float)G_B4_0));
	}

IL_0065:
	{
		t224 * L_12 = &(__this->f17);
		t23  L_13 = m1845(L_12, &m1845_MI);
		V_4 = L_13;
		float L_14 = ((&V_4)->f2);
		t224 * L_15 = &(__this->f16);
		t23  L_16 = m1845(L_15, &m1845_MI);
		V_5 = L_16;
		float L_17 = ((&V_5)->f2);
		t224 * L_18 = &(__this->f16);
		t23  L_19 = m1844(L_18, &m1844_MI);
		V_6 = L_19;
		float L_20 = ((&V_6)->f2);
		t224 * L_21 = &(__this->f17);
		t23  L_22 = m1844(L_21, &m1844_MI);
		V_7 = L_22;
		float L_23 = ((&V_7)->f2);
		return ((float)((float)((float)(L_14-L_17))/(float)((float)(L_20-L_23))));
	}
}
extern MethodInfo m821_MI;
 void m821 (t222 * __this, float p0, MethodInfo* method){
	{
		m824(__this, p0, 1, &m824_MI);
		return;
	}
}
 void m822 (t222 * __this, float p0, MethodInfo* method){
	{
		m824(__this, p0, 0, &m824_MI);
		return;
	}
}
 void m823 (t222 * __this, float p0, MethodInfo* method){
	{
		m824(__this, p0, 1, &m824_MI);
		return;
	}
}
 void m824 (t222 * __this, float p0, int32_t p1, MethodInfo* method){
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	t23  V_3 = {0};
	t23  V_4 = {0};
	t23  V_5 = {0};
	t23  V_6 = {0};
	t23  V_7 = {0};
	t23  V_8 = {0};
	{
		m805(__this, &m805_MI);
		m826(__this, &m826_MI);
		t224 * L_0 = &(__this->f16);
		t23  L_1 = m1844(L_0, &m1844_MI);
		V_4 = L_1;
		float L_2 = m1852((&V_4), p1, &m1852_MI);
		t224 * L_3 = &(__this->f17);
		t23  L_4 = m1844(L_3, &m1844_MI);
		V_5 = L_4;
		float L_5 = m1852((&V_5), p1, &m1852_MI);
		V_0 = ((float)(L_2-L_5));
		t224 * L_6 = &(__this->f17);
		t23  L_7 = m1845(L_6, &m1845_MI);
		V_6 = L_7;
		float L_8 = m1852((&V_6), p1, &m1852_MI);
		V_1 = ((float)(L_8-((float)((float)p0*(float)V_0))));
		t2 * L_9 = (__this->f2);
		t23  L_10 = m42(L_9, &m42_MI);
		V_7 = L_10;
		float L_11 = m1852((&V_7), p1, &m1852_MI);
		t224 * L_12 = &(__this->f16);
		t23  L_13 = m1845(L_12, &m1845_MI);
		V_8 = L_13;
		float L_14 = m1852((&V_8), p1, &m1852_MI);
		V_2 = ((float)(((float)(L_11+V_1))-L_14));
		t2 * L_15 = (__this->f2);
		t23  L_16 = m42(L_15, &m42_MI);
		V_3 = L_16;
		float L_17 = m1852((&V_3), p1, &m1852_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_18 = fabsf(((float)(L_17-V_2)));
		if ((((float)L_18) <= ((float)(0.01f))))
		{
			goto IL_00d1;
		}
	}
	{
		m1853((&V_3), p1, V_2, &m1853_MI);
		t2 * L_19 = (__this->f2);
		m44(L_19, V_3, &m44_MI);
		t17 * L_20 = &(__this->f18);
		m1687(L_20, p1, (0.0f), &m1687_MI);
		m826(__this, &m826_MI);
	}

IL_00d1:
	{
		return;
	}
}
 float m825 (t29 * __this, float p0, float p1, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_0 = fabsf(p0);
		float L_1 = m1854(NULL, p0, &m1854_MI);
		return ((float)((float)((float)((float)((float)((1.0f)-((float)((float)(1.0f)/(float)((float)(((float)((float)((float)((float)L_0*(float)(0.55f)))/(float)p1))+(1.0f)))))))*(float)p1))*(float)L_1));
	}
}
 void m826 (t222 * __this, MethodInfo* method){
	t23  V_0 = {0};
	t23  V_1 = {0};
	t23  V_2 = {0};
	t164  V_3 = {0};
	t164  V_4 = {0};
	t17  V_5 = {0};
	t23  V_6 = {0};
	t17  V_7 = {0};
	t23  V_8 = {0};
	{
		t2 * L_0 = m798(__this, &m798_MI);
		t164  L_1 = m1572(L_0, &m1572_MI);
		V_3 = L_1;
		t17  L_2 = m1834((&V_3), &m1834_MI);
		t23  L_3 = m1463(NULL, L_2, &m1463_MI);
		t2 * L_4 = m798(__this, &m798_MI);
		t164  L_5 = m1572(L_4, &m1572_MI);
		V_4 = L_5;
		t17  L_6 = m1653((&V_4), &m1653_MI);
		t23  L_7 = m1463(NULL, L_6, &m1463_MI);
		t224  L_8 = {0};
		m1855(&L_8, L_3, L_7, &m1855_MI);
		__this->f17 = L_8;
		t224  L_9 = m827(__this, &m827_MI);
		__this->f16 = L_9;
		t2 * L_10 = (__this->f2);
		bool L_11 = m1297(NULL, L_10, (t41 *)NULL, &m1297_MI);
		if (!L_11)
		{
			goto IL_005a;
		}
	}
	{
		return;
	}

IL_005a:
	{
		t224 * L_12 = &(__this->f16);
		t23  L_13 = m1844(L_12, &m1844_MI);
		V_0 = L_13;
		t224 * L_14 = &(__this->f16);
		t23  L_15 = m1856(L_14, &m1856_MI);
		V_1 = L_15;
		t224 * L_16 = &(__this->f17);
		t23  L_17 = m1844(L_16, &m1844_MI);
		t23  L_18 = m1624(NULL, L_17, V_0, &m1624_MI);
		V_2 = L_18;
		float L_19 = ((&V_2)->f1);
		if ((((float)L_19) <= ((float)(0.0f))))
		{
			goto IL_00e0;
		}
	}
	{
		t23 * L_20 = (&V_1);
		float L_21 = (L_20->f1);
		float L_22 = ((&V_2)->f1);
		t2 * L_23 = (__this->f2);
		t17  L_24 = m1658(L_23, &m1658_MI);
		V_5 = L_24;
		float L_25 = ((&V_5)->f1);
		L_20->f1 = ((float)(L_21-((float)((float)L_22*(float)((float)(L_25-(0.5f)))))));
		t224 * L_26 = &(__this->f17);
		t23  L_27 = m1844(L_26, &m1844_MI);
		V_6 = L_27;
		float L_28 = ((&V_6)->f1);
		(&V_0)->f1 = L_28;
	}

IL_00e0:
	{
		float L_29 = ((&V_2)->f2);
		if ((((float)L_29) <= ((float)(0.0f))))
		{
			goto IL_013c;
		}
	}
	{
		t23 * L_30 = (&V_1);
		float L_31 = (L_30->f2);
		float L_32 = ((&V_2)->f2);
		t2 * L_33 = (__this->f2);
		t17  L_34 = m1658(L_33, &m1658_MI);
		V_7 = L_34;
		float L_35 = ((&V_7)->f2);
		L_30->f2 = ((float)(L_31-((float)((float)L_32*(float)((float)(L_35-(0.5f)))))));
		t224 * L_36 = &(__this->f17);
		t23  L_37 = m1844(L_36, &m1844_MI);
		V_8 = L_37;
		float L_38 = ((&V_8)->f2);
		(&V_0)->f2 = L_38;
	}

IL_013c:
	{
		t224 * L_39 = &(__this->f16);
		m1857(L_39, V_0, &m1857_MI);
		t224 * L_40 = &(__this->f16);
		m1858(L_40, V_1, &m1858_MI);
		return;
	}
}
 t224  m827 (t222 * __this, MethodInfo* method){
	t23  V_0 = {0};
	t23  V_1 = {0};
	t396  V_2 = {0};
	int32_t V_3 = 0;
	t23  V_4 = {0};
	t224  V_5 = {0};
	t224  V_6 = {0};
	{
		t2 * L_0 = (__this->f2);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Initobj (&t224_TI, (&V_6));
		return V_6;
	}

IL_001c:
	{
		m49((&V_0), (std::numeric_limits<float>::max()), (std::numeric_limits<float>::max()), (std::numeric_limits<float>::max()), &m49_MI);
		m49((&V_1), (-std::numeric_limits<float>::max()), (-std::numeric_limits<float>::max()), (-std::numeric_limits<float>::max()), &m49_MI);
		t2 * L_2 = m798(__this, &m798_MI);
		t396  L_3 = m1859(L_2, &m1859_MI);
		V_2 = L_3;
		t2 * L_4 = (__this->f2);
		t223* L_5 = (__this->f24);
		m1860(L_4, L_5, &m1860_MI);
		V_3 = 0;
		goto IL_009c;
	}

IL_006c:
	{
		t223* L_6 = (__this->f24);
		t23  L_7 = m1861((&V_2), (*(t23 *)((t23 *)(t23 *)SZArrayLdElema(L_6, V_3))), &m1861_MI);
		V_4 = L_7;
		t23  L_8 = m1862(NULL, V_4, V_0, &m1862_MI);
		V_0 = L_8;
		t23  L_9 = m1863(NULL, V_4, V_1, &m1863_MI);
		V_1 = L_9;
		V_3 = ((int32_t)(V_3+1));
	}

IL_009c:
	{
		if ((((int32_t)V_3) < ((int32_t)4)))
		{
			goto IL_006c;
		}
	}
	{
		t23  L_10 = m1395(NULL, &m1395_MI);
		m1855((&V_5), V_0, L_10, &m1855_MI);
		m1864((&V_5), V_1, &m1864_MI);
		return V_5;
	}
}
 t17  m828 (t222 * __this, t17  p0, MethodInfo* method){
	t17  V_0 = {0};
	t17  V_1 = {0};
	t17  V_2 = {0};
	t23  V_3 = {0};
	t23  V_4 = {0};
	t23  V_5 = {0};
	t23  V_6 = {0};
	t23  V_7 = {0};
	t23  V_8 = {0};
	t23  V_9 = {0};
	t23  V_10 = {0};
	{
		t17  L_0 = m1394(NULL, &m1394_MI);
		V_0 = L_0;
		int32_t L_1 = (__this->f5);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		return V_0;
	}

IL_0013:
	{
		t224 * L_2 = &(__this->f16);
		t23  L_3 = m1845(L_2, &m1845_MI);
		t17  L_4 = m1419(NULL, L_3, &m1419_MI);
		V_1 = L_4;
		t224 * L_5 = &(__this->f16);
		t23  L_6 = m1865(L_5, &m1865_MI);
		t17  L_7 = m1419(NULL, L_6, &m1419_MI);
		V_2 = L_7;
		bool L_8 = (__this->f3);
		if (!L_8)
		{
			goto IL_00f4;
		}
	}
	{
		t17 * L_9 = (&V_1);
		float L_10 = (L_9->f1);
		float L_11 = ((&p0)->f1);
		L_9->f1 = ((float)(L_10+L_11));
		t17 * L_12 = (&V_2);
		float L_13 = (L_12->f1);
		float L_14 = ((&p0)->f1);
		L_12->f1 = ((float)(L_13+L_14));
		float L_15 = ((&V_1)->f1);
		t224 * L_16 = &(__this->f17);
		t23  L_17 = m1845(L_16, &m1845_MI);
		V_3 = L_17;
		float L_18 = ((&V_3)->f1);
		if ((((float)L_15) <= ((float)L_18)))
		{
			goto IL_00b1;
		}
	}
	{
		t224 * L_19 = &(__this->f17);
		t23  L_20 = m1845(L_19, &m1845_MI);
		V_4 = L_20;
		float L_21 = ((&V_4)->f1);
		float L_22 = ((&V_1)->f1);
		(&V_0)->f1 = ((float)(L_21-L_22));
		goto IL_00f4;
	}

IL_00b1:
	{
		float L_23 = ((&V_2)->f1);
		t224 * L_24 = &(__this->f17);
		t23  L_25 = m1865(L_24, &m1865_MI);
		V_5 = L_25;
		float L_26 = ((&V_5)->f1);
		if ((((float)L_23) >= ((float)L_26)))
		{
			goto IL_00f4;
		}
	}
	{
		t224 * L_27 = &(__this->f17);
		t23  L_28 = m1865(L_27, &m1865_MI);
		V_6 = L_28;
		float L_29 = ((&V_6)->f1);
		float L_30 = ((&V_2)->f1);
		(&V_0)->f1 = ((float)(L_29-L_30));
	}

IL_00f4:
	{
		bool L_31 = (__this->f4);
		if (!L_31)
		{
			goto IL_01b4;
		}
	}
	{
		t17 * L_32 = (&V_1);
		float L_33 = (L_32->f2);
		float L_34 = ((&p0)->f2);
		L_32->f2 = ((float)(L_33+L_34));
		t17 * L_35 = (&V_2);
		float L_36 = (L_35->f2);
		float L_37 = ((&p0)->f2);
		L_35->f2 = ((float)(L_36+L_37));
		float L_38 = ((&V_2)->f2);
		t224 * L_39 = &(__this->f17);
		t23  L_40 = m1865(L_39, &m1865_MI);
		V_7 = L_40;
		float L_41 = ((&V_7)->f2);
		if ((((float)L_38) >= ((float)L_41)))
		{
			goto IL_0171;
		}
	}
	{
		t224 * L_42 = &(__this->f17);
		t23  L_43 = m1865(L_42, &m1865_MI);
		V_8 = L_43;
		float L_44 = ((&V_8)->f2);
		float L_45 = ((&V_2)->f2);
		(&V_0)->f2 = ((float)(L_44-L_45));
		goto IL_01b4;
	}

IL_0171:
	{
		float L_46 = ((&V_1)->f2);
		t224 * L_47 = &(__this->f17);
		t23  L_48 = m1845(L_47, &m1845_MI);
		V_9 = L_48;
		float L_49 = ((&V_9)->f2);
		if ((((float)L_46) <= ((float)L_49)))
		{
			goto IL_01b4;
		}
	}
	{
		t224 * L_50 = &(__this->f17);
		t23  L_51 = m1845(L_50, &m1845_MI);
		V_10 = L_51;
		float L_52 = ((&V_10)->f2);
		float L_53 = ((&V_1)->f2);
		(&V_0)->f2 = ((float)(L_52-L_53));
	}

IL_01b4:
	{
		return V_0;
	}
}
extern MethodInfo m829_MI;
 bool m829 (t222 * __this, MethodInfo* method){
	{
		bool L_0 = m180(__this, &m180_MI);
		return L_0;
	}
}
extern MethodInfo m830_MI;
 t25 * m830 (t222 * __this, MethodInfo* method){
	{
		t25 * L_0 = m39(__this, &m39_MI);
		return L_0;
	}
}
// Metadata Definition UnityEngine.UI.ScrollRect
extern Il2CppType t2_0_0_1;
extern CustomAttributesCache t222__CustomAttributeCache_m_Content;
FieldInfo t222_f2_FieldInfo = 
{
	"m_Content", &t2_0_0_1, &t222_TI, offsetof(t222, f2), &t222__CustomAttributeCache_m_Content};
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t222__CustomAttributeCache_m_Horizontal;
FieldInfo t222_f3_FieldInfo = 
{
	"m_Horizontal", &t40_0_0_1, &t222_TI, offsetof(t222, f3), &t222__CustomAttributeCache_m_Horizontal};
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t222__CustomAttributeCache_m_Vertical;
FieldInfo t222_f4_FieldInfo = 
{
	"m_Vertical", &t40_0_0_1, &t222_TI, offsetof(t222, f4), &t222__CustomAttributeCache_m_Vertical};
extern Il2CppType t219_0_0_1;
extern CustomAttributesCache t222__CustomAttributeCache_m_MovementType;
FieldInfo t222_f5_FieldInfo = 
{
	"m_MovementType", &t219_0_0_1, &t222_TI, offsetof(t222, f5), &t222__CustomAttributeCache_m_MovementType};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t222__CustomAttributeCache_m_Elasticity;
FieldInfo t222_f6_FieldInfo = 
{
	"m_Elasticity", &t22_0_0_1, &t222_TI, offsetof(t222, f6), &t222__CustomAttributeCache_m_Elasticity};
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t222__CustomAttributeCache_m_Inertia;
FieldInfo t222_f7_FieldInfo = 
{
	"m_Inertia", &t40_0_0_1, &t222_TI, offsetof(t222, f7), &t222__CustomAttributeCache_m_Inertia};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t222__CustomAttributeCache_m_DecelerationRate;
FieldInfo t222_f8_FieldInfo = 
{
	"m_DecelerationRate", &t22_0_0_1, &t222_TI, offsetof(t222, f8), &t222__CustomAttributeCache_m_DecelerationRate};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t222__CustomAttributeCache_m_ScrollSensitivity;
FieldInfo t222_f9_FieldInfo = 
{
	"m_ScrollSensitivity", &t22_0_0_1, &t222_TI, offsetof(t222, f9), &t222__CustomAttributeCache_m_ScrollSensitivity};
extern Il2CppType t217_0_0_1;
extern CustomAttributesCache t222__CustomAttributeCache_m_HorizontalScrollbar;
FieldInfo t222_f10_FieldInfo = 
{
	"m_HorizontalScrollbar", &t217_0_0_1, &t222_TI, offsetof(t222, f10), &t222__CustomAttributeCache_m_HorizontalScrollbar};
extern Il2CppType t217_0_0_1;
extern CustomAttributesCache t222__CustomAttributeCache_m_VerticalScrollbar;
FieldInfo t222_f11_FieldInfo = 
{
	"m_VerticalScrollbar", &t217_0_0_1, &t222_TI, offsetof(t222, f11), &t222__CustomAttributeCache_m_VerticalScrollbar};
extern Il2CppType t220_0_0_1;
extern CustomAttributesCache t222__CustomAttributeCache_m_OnValueChanged;
FieldInfo t222_f12_FieldInfo = 
{
	"m_OnValueChanged", &t220_0_0_1, &t222_TI, offsetof(t222, f12), &t222__CustomAttributeCache_m_OnValueChanged};
extern Il2CppType t17_0_0_1;
FieldInfo t222_f13_FieldInfo = 
{
	"m_PointerStartLocalCursor", &t17_0_0_1, &t222_TI, offsetof(t222, f13), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_1;
FieldInfo t222_f14_FieldInfo = 
{
	"m_ContentStartPosition", &t17_0_0_1, &t222_TI, offsetof(t222, f14), &EmptyCustomAttributesCache};
extern Il2CppType t2_0_0_1;
FieldInfo t222_f15_FieldInfo = 
{
	"m_ViewRect", &t2_0_0_1, &t222_TI, offsetof(t222, f15), &EmptyCustomAttributesCache};
extern Il2CppType t224_0_0_1;
FieldInfo t222_f16_FieldInfo = 
{
	"m_ContentBounds", &t224_0_0_1, &t222_TI, offsetof(t222, f16), &EmptyCustomAttributesCache};
extern Il2CppType t224_0_0_1;
FieldInfo t222_f17_FieldInfo = 
{
	"m_ViewBounds", &t224_0_0_1, &t222_TI, offsetof(t222, f17), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_1;
FieldInfo t222_f18_FieldInfo = 
{
	"m_Velocity", &t17_0_0_1, &t222_TI, offsetof(t222, f18), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
FieldInfo t222_f19_FieldInfo = 
{
	"m_Dragging", &t40_0_0_1, &t222_TI, offsetof(t222, f19), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_1;
FieldInfo t222_f20_FieldInfo = 
{
	"m_PrevPosition", &t17_0_0_1, &t222_TI, offsetof(t222, f20), &EmptyCustomAttributesCache};
extern Il2CppType t224_0_0_1;
FieldInfo t222_f21_FieldInfo = 
{
	"m_PrevContentBounds", &t224_0_0_1, &t222_TI, offsetof(t222, f21), &EmptyCustomAttributesCache};
extern Il2CppType t224_0_0_1;
FieldInfo t222_f22_FieldInfo = 
{
	"m_PrevViewBounds", &t224_0_0_1, &t222_TI, offsetof(t222, f22), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_129;
FieldInfo t222_f23_FieldInfo = 
{
	"m_HasRebuiltLayout", &t40_0_0_129, &t222_TI, offsetof(t222, f23), &EmptyCustomAttributesCache};
extern Il2CppType t223_0_0_33;
FieldInfo t222_f24_FieldInfo = 
{
	"m_Corners", &t223_0_0_33, &t222_TI, offsetof(t222, f24), &EmptyCustomAttributesCache};
static FieldInfo* t222_FIs[] =
{
	&t222_f2_FieldInfo,
	&t222_f3_FieldInfo,
	&t222_f4_FieldInfo,
	&t222_f5_FieldInfo,
	&t222_f6_FieldInfo,
	&t222_f7_FieldInfo,
	&t222_f8_FieldInfo,
	&t222_f9_FieldInfo,
	&t222_f10_FieldInfo,
	&t222_f11_FieldInfo,
	&t222_f12_FieldInfo,
	&t222_f13_FieldInfo,
	&t222_f14_FieldInfo,
	&t222_f15_FieldInfo,
	&t222_f16_FieldInfo,
	&t222_f17_FieldInfo,
	&t222_f18_FieldInfo,
	&t222_f19_FieldInfo,
	&t222_f20_FieldInfo,
	&t222_f21_FieldInfo,
	&t222_f22_FieldInfo,
	&t222_f23_FieldInfo,
	&t222_f24_FieldInfo,
	NULL
};
static PropertyInfo t222____content_PropertyInfo = 
{
	&t222_TI, "content", &m776_MI, &m777_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____horizontal_PropertyInfo = 
{
	&t222_TI, "horizontal", &m778_MI, &m779_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____vertical_PropertyInfo = 
{
	&t222_TI, "vertical", &m780_MI, &m781_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____movementType_PropertyInfo = 
{
	&t222_TI, "movementType", &m782_MI, &m783_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____elasticity_PropertyInfo = 
{
	&t222_TI, "elasticity", &m784_MI, &m785_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____inertia_PropertyInfo = 
{
	&t222_TI, "inertia", &m786_MI, &m787_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____decelerationRate_PropertyInfo = 
{
	&t222_TI, "decelerationRate", &m788_MI, &m789_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____scrollSensitivity_PropertyInfo = 
{
	&t222_TI, "scrollSensitivity", &m790_MI, &m791_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____horizontalScrollbar_PropertyInfo = 
{
	&t222_TI, "horizontalScrollbar", &m792_MI, &m793_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____verticalScrollbar_PropertyInfo = 
{
	&t222_TI, "verticalScrollbar", &m794_MI, &m795_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____onValueChanged_PropertyInfo = 
{
	&t222_TI, "onValueChanged", &m796_MI, &m797_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____viewRect_PropertyInfo = 
{
	&t222_TI, "viewRect", &m798_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____velocity_PropertyInfo = 
{
	&t222_TI, "velocity", &m799_MI, &m800_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____normalizedPosition_PropertyInfo = 
{
	&t222_TI, "normalizedPosition", &m816_MI, &m817_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____horizontalNormalizedPosition_PropertyInfo = 
{
	&t222_TI, "horizontalNormalizedPosition", &m818_MI, &m819_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t222____verticalNormalizedPosition_PropertyInfo = 
{
	&t222_TI, "verticalNormalizedPosition", &m820_MI, &m821_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t222_PIs[] =
{
	&t222____content_PropertyInfo,
	&t222____horizontal_PropertyInfo,
	&t222____vertical_PropertyInfo,
	&t222____movementType_PropertyInfo,
	&t222____elasticity_PropertyInfo,
	&t222____inertia_PropertyInfo,
	&t222____decelerationRate_PropertyInfo,
	&t222____scrollSensitivity_PropertyInfo,
	&t222____horizontalScrollbar_PropertyInfo,
	&t222____verticalScrollbar_PropertyInfo,
	&t222____onValueChanged_PropertyInfo,
	&t222____viewRect_PropertyInfo,
	&t222____velocity_PropertyInfo,
	&t222____normalizedPosition_PropertyInfo,
	&t222____horizontalNormalizedPosition_PropertyInfo,
	&t222____verticalNormalizedPosition_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m775_MI = 
{
	".ctor", (methodPointerType)&m775, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 754, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m776_MI = 
{
	"get_content", (methodPointerType)&m776, &t222_TI, &t2_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 755, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t222_m777_ParameterInfos[] = 
{
	{"value", 0, 134218189, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m777_MI = 
{
	"set_content", (methodPointerType)&m777, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t222_m777_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 756, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m778_MI = 
{
	"get_horizontal", (methodPointerType)&m778, &t222_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 757, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t222_m779_ParameterInfos[] = 
{
	{"value", 0, 134218190, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m779_MI = 
{
	"set_horizontal", (methodPointerType)&m779, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t222_m779_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 758, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m780_MI = 
{
	"get_vertical", (methodPointerType)&m780, &t222_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 759, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t222_m781_ParameterInfos[] = 
{
	{"value", 0, 134218191, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m781_MI = 
{
	"set_vertical", (methodPointerType)&m781, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t222_m781_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 760, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t219_0_0_0;
extern void* RuntimeInvoker_t219 (MethodInfo* method, void* obj, void** args);
MethodInfo m782_MI = 
{
	"get_movementType", (methodPointerType)&m782, &t222_TI, &t219_0_0_0, RuntimeInvoker_t219, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 761, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t219_0_0_0;
extern Il2CppType t219_0_0_0;
static ParameterInfo t222_m783_ParameterInfos[] = 
{
	{"value", 0, 134218192, &EmptyCustomAttributesCache, &t219_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m783_MI = 
{
	"set_movementType", (methodPointerType)&m783, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t222_m783_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 762, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m784_MI = 
{
	"get_elasticity", (methodPointerType)&m784, &t222_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 763, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t222_m785_ParameterInfos[] = 
{
	{"value", 0, 134218193, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m785_MI = 
{
	"set_elasticity", (methodPointerType)&m785, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t222_m785_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 764, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m786_MI = 
{
	"get_inertia", (methodPointerType)&m786, &t222_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 765, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t222_m787_ParameterInfos[] = 
{
	{"value", 0, 134218194, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m787_MI = 
{
	"set_inertia", (methodPointerType)&m787, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t222_m787_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 766, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m788_MI = 
{
	"get_decelerationRate", (methodPointerType)&m788, &t222_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 767, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t222_m789_ParameterInfos[] = 
{
	{"value", 0, 134218195, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m789_MI = 
{
	"set_decelerationRate", (methodPointerType)&m789, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t222_m789_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 768, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m790_MI = 
{
	"get_scrollSensitivity", (methodPointerType)&m790, &t222_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 769, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t222_m791_ParameterInfos[] = 
{
	{"value", 0, 134218196, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m791_MI = 
{
	"set_scrollSensitivity", (methodPointerType)&m791, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t222_m791_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 770, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t217_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m792_MI = 
{
	"get_horizontalScrollbar", (methodPointerType)&m792, &t222_TI, &t217_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 771, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t217_0_0_0;
extern Il2CppType t217_0_0_0;
static ParameterInfo t222_m793_ParameterInfos[] = 
{
	{"value", 0, 134218197, &EmptyCustomAttributesCache, &t217_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m793_MI = 
{
	"set_horizontalScrollbar", (methodPointerType)&m793, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t222_m793_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 772, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t217_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m794_MI = 
{
	"get_verticalScrollbar", (methodPointerType)&m794, &t222_TI, &t217_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 773, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t217_0_0_0;
static ParameterInfo t222_m795_ParameterInfos[] = 
{
	{"value", 0, 134218198, &EmptyCustomAttributesCache, &t217_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m795_MI = 
{
	"set_verticalScrollbar", (methodPointerType)&m795, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t222_m795_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 774, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t220_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m796_MI = 
{
	"get_onValueChanged", (methodPointerType)&m796, &t222_TI, &t220_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 775, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t220_0_0_0;
static ParameterInfo t222_m797_ParameterInfos[] = 
{
	{"value", 0, 134218199, &EmptyCustomAttributesCache, &t220_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m797_MI = 
{
	"set_onValueChanged", (methodPointerType)&m797, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t222_m797_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 776, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m798_MI = 
{
	"get_viewRect", (methodPointerType)&m798, &t222_TI, &t2_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2180, 0, 255, 0, false, false, 777, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m799_MI = 
{
	"get_velocity", (methodPointerType)&m799, &t222_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 778, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern Il2CppType t17_0_0_0;
static ParameterInfo t222_m800_ParameterInfos[] = 
{
	{"value", 0, 134218200, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m800_MI = 
{
	"set_velocity", (methodPointerType)&m800, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t17, t222_m800_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 779, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t140_0_0_0;
extern Il2CppType t140_0_0_0;
static ParameterInfo t222_m801_ParameterInfos[] = 
{
	{"executing", 0, 134218201, &EmptyCustomAttributesCache, &t140_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m801_MI = 
{
	"Rebuild", (methodPointerType)&m801, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t222_m801_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 23, 1, false, false, 780, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m802_MI = 
{
	"OnEnable", (methodPointerType)&m802, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 5, 0, false, false, 781, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m803_MI = 
{
	"OnDisable", (methodPointerType)&m803, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 7, 0, false, false, 782, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m804_MI = 
{
	"IsActive", (methodPointerType)&m804, &t222_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 198, 0, 9, 0, false, false, 783, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m805_MI = 
{
	"EnsureLayoutHasRebuilt", (methodPointerType)&m805, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 784, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m806_MI = 
{
	"StopMovement", (methodPointerType)&m806, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 24, 0, false, false, 785, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t222_m807_ParameterInfos[] = 
{
	{"data", 0, 134218202, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m807_MI = 
{
	"OnScroll", (methodPointerType)&m807, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t222_m807_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 25, 1, false, false, 786, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t222_m808_ParameterInfos[] = 
{
	{"eventData", 0, 134218203, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m808_MI = 
{
	"OnInitializePotentialDrag", (methodPointerType)&m808, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t222_m808_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 26, 1, false, false, 787, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t222_m809_ParameterInfos[] = 
{
	{"eventData", 0, 134218204, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m809_MI = 
{
	"OnBeginDrag", (methodPointerType)&m809, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t222_m809_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 27, 1, false, false, 788, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t222_m810_ParameterInfos[] = 
{
	{"eventData", 0, 134218205, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m810_MI = 
{
	"OnEndDrag", (methodPointerType)&m810, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t222_m810_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 28, 1, false, false, 789, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t222_m811_ParameterInfos[] = 
{
	{"eventData", 0, 134218206, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m811_MI = 
{
	"OnDrag", (methodPointerType)&m811, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t222_m811_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 1, false, false, 790, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
static ParameterInfo t222_m812_ParameterInfos[] = 
{
	{"position", 0, 134218207, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m812_MI = 
{
	"SetContentAnchoredPosition", (methodPointerType)&m812, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t17, t222_m812_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 30, 1, false, false, 791, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m813_MI = 
{
	"LateUpdate", (methodPointerType)&m813, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 31, 0, false, false, 792, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m814_MI = 
{
	"UpdatePrevData", (methodPointerType)&m814, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 793, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
static ParameterInfo t222_m815_ParameterInfos[] = 
{
	{"offset", 0, 134218208, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m815_MI = 
{
	"UpdateScrollbars", (methodPointerType)&m815, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t17, t222_m815_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 794, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m816_MI = 
{
	"get_normalizedPosition", (methodPointerType)&m816, &t222_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 795, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
static ParameterInfo t222_m817_ParameterInfos[] = 
{
	{"value", 0, 134218209, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m817_MI = 
{
	"set_normalizedPosition", (methodPointerType)&m817, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t17, t222_m817_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 796, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m818_MI = 
{
	"get_horizontalNormalizedPosition", (methodPointerType)&m818, &t222_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 797, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t222_m819_ParameterInfos[] = 
{
	{"value", 0, 134218210, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m819_MI = 
{
	"set_horizontalNormalizedPosition", (methodPointerType)&m819, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t222_m819_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 798, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m820_MI = 
{
	"get_verticalNormalizedPosition", (methodPointerType)&m820, &t222_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 799, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t222_m821_ParameterInfos[] = 
{
	{"value", 0, 134218211, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m821_MI = 
{
	"set_verticalNormalizedPosition", (methodPointerType)&m821, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t222_m821_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 800, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t222_m822_ParameterInfos[] = 
{
	{"value", 0, 134218212, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m822_MI = 
{
	"SetHorizontalNormalizedPosition", (methodPointerType)&m822, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t222_m822_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 801, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t222_m823_ParameterInfos[] = 
{
	{"value", 0, 134218213, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m823_MI = 
{
	"SetVerticalNormalizedPosition", (methodPointerType)&m823, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t222_m823_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 802, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t222_m824_ParameterInfos[] = 
{
	{"value", 0, 134218214, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"axis", 1, 134218215, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m824_MI = 
{
	"SetNormalizedPosition", (methodPointerType)&m824, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21_t22_t44, t222_m824_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 803, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t222_m825_ParameterInfos[] = 
{
	{"overStretching", 0, 134218216, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"viewSize", 1, 134218217, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t22_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m825_MI = 
{
	"RubberDelta", (methodPointerType)&m825, &t222_TI, &t22_0_0_0, RuntimeInvoker_t22_t22_t22, t222_m825_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 804, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m826_MI = 
{
	"UpdateBounds", (methodPointerType)&m826, &t222_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 805, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t224_0_0_0;
extern void* RuntimeInvoker_t224 (MethodInfo* method, void* obj, void** args);
MethodInfo m827_MI = 
{
	"GetBounds", (methodPointerType)&m827, &t222_TI, &t224_0_0_0, RuntimeInvoker_t224, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 806, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
static ParameterInfo t222_m828_ParameterInfos[] = 
{
	{"delta", 0, 134218218, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m828_MI = 
{
	"CalculateOffset", (methodPointerType)&m828, &t222_TI, &t17_0_0_0, RuntimeInvoker_t17_t17, t222_m828_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 807, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m829_MI = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed", (methodPointerType)&m829, &t222_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 960, 0, 32, 0, false, false, 808, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m830_MI = 
{
	"UnityEngine.UI.ICanvasElement.get_transform", (methodPointerType)&m830, &t222_TI, &t25_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 960, 0, 33, 0, false, false, 809, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t222_MIs[] =
{
	&m775_MI,
	&m776_MI,
	&m777_MI,
	&m778_MI,
	&m779_MI,
	&m780_MI,
	&m781_MI,
	&m782_MI,
	&m783_MI,
	&m784_MI,
	&m785_MI,
	&m786_MI,
	&m787_MI,
	&m788_MI,
	&m789_MI,
	&m790_MI,
	&m791_MI,
	&m792_MI,
	&m793_MI,
	&m794_MI,
	&m795_MI,
	&m796_MI,
	&m797_MI,
	&m798_MI,
	&m799_MI,
	&m800_MI,
	&m801_MI,
	&m802_MI,
	&m803_MI,
	&m804_MI,
	&m805_MI,
	&m806_MI,
	&m807_MI,
	&m808_MI,
	&m809_MI,
	&m810_MI,
	&m811_MI,
	&m812_MI,
	&m813_MI,
	&m814_MI,
	&m815_MI,
	&m816_MI,
	&m817_MI,
	&m818_MI,
	&m819_MI,
	&m820_MI,
	&m821_MI,
	&m822_MI,
	&m823_MI,
	&m824_MI,
	&m825_MI,
	&m826_MI,
	&m827_MI,
	&m828_MI,
	&m829_MI,
	&m830_MI,
	NULL
};
extern TypeInfo t219_TI;
extern TypeInfo t220_TI;
static TypeInfo* t222_TI__nestedTypes[3] =
{
	&t219_TI,
	&t220_TI,
	NULL
};
extern MethodInfo m45_MI;
extern MethodInfo m47_MI;
extern MethodInfo m48_MI;
extern MethodInfo m169_MI;
extern MethodInfo m171_MI;
extern MethodInfo m173_MI;
extern MethodInfo m175_MI;
extern MethodInfo m176_MI;
extern MethodInfo m177_MI;
extern MethodInfo m178_MI;
extern MethodInfo m179_MI;
static MethodInfo* t222_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m802_MI,
	&m171_MI,
	&m803_MI,
	&m173_MI,
	&m804_MI,
	&m175_MI,
	&m176_MI,
	&m177_MI,
	&m178_MI,
	&m179_MI,
	&m809_MI,
	&m808_MI,
	&m811_MI,
	&m810_MI,
	&m807_MI,
	&m801_MI,
	&m830_MI,
	&m829_MI,
	&m801_MI,
	&m806_MI,
	&m807_MI,
	&m808_MI,
	&m809_MI,
	&m810_MI,
	&m811_MI,
	&m812_MI,
	&m813_MI,
	&m829_MI,
	&m830_MI,
};
extern TypeInfo t31_TI;
extern TypeInfo t93_TI;
extern TypeInfo t92_TI;
extern TypeInfo t94_TI;
extern TypeInfo t95_TI;
extern TypeInfo t97_TI;
extern TypeInfo t145_TI;
static TypeInfo* t222_ITIs[] = 
{
	&t31_TI,
	&t93_TI,
	&t92_TI,
	&t94_TI,
	&t95_TI,
	&t97_TI,
	&t145_TI,
};
static Il2CppInterfaceOffsetPair t222_IOs[] = 
{
	{ &t31_TI, 15},
	{ &t93_TI, 15},
	{ &t92_TI, 16},
	{ &t94_TI, 17},
	{ &t95_TI, 18},
	{ &t97_TI, 19},
	{ &t145_TI, 20},
};
extern TypeInfo t360_TI;
#include "t360.h"
#include "t360MD.h"
extern MethodInfo m1604_MI;
extern TypeInfo t397_TI;
#include "t397.h"
#include "t397MD.h"
extern MethodInfo m1866_MI;
extern TypeInfo t298_TI;
#include "t298.h"
#include "t298MD.h"
extern MethodInfo m1519_MI;
extern TypeInfo t318_TI;
#include "t318.h"
#include "t318MD.h"
extern MethodInfo m1404_MI;
void t222_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t360 * tmp;
		tmp = (t360 *)il2cpp_codegen_object_new (&t360_TI);
		m1604(tmp, &m1604_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t397 * tmp;
		tmp = (t397 *)il2cpp_codegen_object_new (&t397_TI);
		m1866(tmp, &m1866_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("UI/Scroll Rect"), 33, &m1519_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t318 * tmp;
		tmp = (t318 *)il2cpp_codegen_object_new (&t318_TI);
		m1404(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2_TI)), &m1404_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t300_TI;
#include "t300.h"
#include "t300MD.h"
extern MethodInfo m1319_MI;
void t222_CustomAttributesCacheGenerator_m_Content(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t222_CustomAttributesCacheGenerator_m_Horizontal(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t222_CustomAttributesCacheGenerator_m_Vertical(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t222_CustomAttributesCacheGenerator_m_MovementType(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t222_CustomAttributesCacheGenerator_m_Elasticity(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t222_CustomAttributesCacheGenerator_m_Inertia(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t222_CustomAttributesCacheGenerator_m_DecelerationRate(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t222_CustomAttributesCacheGenerator_m_ScrollSensitivity(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t222_CustomAttributesCacheGenerator_m_HorizontalScrollbar(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t222_CustomAttributesCacheGenerator_m_VerticalScrollbar(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t222_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t222__CustomAttributeCache = {
4,
NULL,
&t222_CustomAttributesCacheGenerator
};
CustomAttributesCache t222__CustomAttributeCache_m_Content = {
1,
NULL,
&t222_CustomAttributesCacheGenerator_m_Content
};
CustomAttributesCache t222__CustomAttributeCache_m_Horizontal = {
1,
NULL,
&t222_CustomAttributesCacheGenerator_m_Horizontal
};
CustomAttributesCache t222__CustomAttributeCache_m_Vertical = {
1,
NULL,
&t222_CustomAttributesCacheGenerator_m_Vertical
};
CustomAttributesCache t222__CustomAttributeCache_m_MovementType = {
1,
NULL,
&t222_CustomAttributesCacheGenerator_m_MovementType
};
CustomAttributesCache t222__CustomAttributeCache_m_Elasticity = {
1,
NULL,
&t222_CustomAttributesCacheGenerator_m_Elasticity
};
CustomAttributesCache t222__CustomAttributeCache_m_Inertia = {
1,
NULL,
&t222_CustomAttributesCacheGenerator_m_Inertia
};
CustomAttributesCache t222__CustomAttributeCache_m_DecelerationRate = {
1,
NULL,
&t222_CustomAttributesCacheGenerator_m_DecelerationRate
};
CustomAttributesCache t222__CustomAttributeCache_m_ScrollSensitivity = {
1,
NULL,
&t222_CustomAttributesCacheGenerator_m_ScrollSensitivity
};
CustomAttributesCache t222__CustomAttributeCache_m_HorizontalScrollbar = {
1,
NULL,
&t222_CustomAttributesCacheGenerator_m_HorizontalScrollbar
};
CustomAttributesCache t222__CustomAttributeCache_m_VerticalScrollbar = {
1,
NULL,
&t222_CustomAttributesCacheGenerator_m_VerticalScrollbar
};
CustomAttributesCache t222__CustomAttributeCache_m_OnValueChanged = {
1,
NULL,
&t222_CustomAttributesCacheGenerator_m_OnValueChanged
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t222_0_0_0;
extern Il2CppType t222_1_0_0;
struct t222;
extern CustomAttributesCache t222__CustomAttributeCache;
extern CustomAttributesCache t222__CustomAttributeCache_m_Content;
extern CustomAttributesCache t222__CustomAttributeCache_m_Horizontal;
extern CustomAttributesCache t222__CustomAttributeCache_m_Vertical;
extern CustomAttributesCache t222__CustomAttributeCache_m_MovementType;
extern CustomAttributesCache t222__CustomAttributeCache_m_Elasticity;
extern CustomAttributesCache t222__CustomAttributeCache_m_Inertia;
extern CustomAttributesCache t222__CustomAttributeCache_m_DecelerationRate;
extern CustomAttributesCache t222__CustomAttributeCache_m_ScrollSensitivity;
extern CustomAttributesCache t222__CustomAttributeCache_m_HorizontalScrollbar;
extern CustomAttributesCache t222__CustomAttributeCache_m_VerticalScrollbar;
extern CustomAttributesCache t222__CustomAttributeCache_m_OnValueChanged;
TypeInfo t222_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ScrollRect", "UnityEngine.UI", t222_MIs, t222_PIs, t222_FIs, NULL, &t55_TI, t222_TI__nestedTypes, NULL, &t222_TI, t222_ITIs, t222_VT, &t222__CustomAttributeCache, &t222_TI, &t222_0_0_0, &t222_1_0_0, t222_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t222), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 56, 16, 23, 0, 2, 34, 7, 7};
#include "t225.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t225_TI;
#include "t225MD.h"



// Metadata Definition UnityEngine.UI.Selectable/Transition
extern Il2CppType t44_0_0_1542;
FieldInfo t225_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t225_TI, offsetof(t225, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t225_0_0_32854;
FieldInfo t225_f2_FieldInfo = 
{
	"None", &t225_0_0_32854, &t225_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t225_0_0_32854;
FieldInfo t225_f3_FieldInfo = 
{
	"ColorTint", &t225_0_0_32854, &t225_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t225_0_0_32854;
FieldInfo t225_f4_FieldInfo = 
{
	"SpriteSwap", &t225_0_0_32854, &t225_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t225_0_0_32854;
FieldInfo t225_f5_FieldInfo = 
{
	"Animation", &t225_0_0_32854, &t225_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t225_FIs[] =
{
	&t225_f1_FieldInfo,
	&t225_f2_FieldInfo,
	&t225_f3_FieldInfo,
	&t225_f4_FieldInfo,
	&t225_f5_FieldInfo,
	NULL
};
static const int32_t t225_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t225_f2_DefaultValue = 
{
	&t225_f2_FieldInfo, { (char*)&t225_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t225_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t225_f3_DefaultValue = 
{
	&t225_f3_FieldInfo, { (char*)&t225_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t225_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t225_f4_DefaultValue = 
{
	&t225_f4_FieldInfo, { (char*)&t225_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t225_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t225_f5_DefaultValue = 
{
	&t225_f5_FieldInfo, { (char*)&t225_f5_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t225_FDVs[] = 
{
	&t225_f2_DefaultValue,
	&t225_f3_DefaultValue,
	&t225_f4_DefaultValue,
	&t225_f5_DefaultValue,
	NULL
};
static MethodInfo* t225_MIs[] =
{
	NULL
};
extern MethodInfo m1248_MI;
extern MethodInfo m1249_MI;
extern MethodInfo m1250_MI;
extern MethodInfo m1251_MI;
extern MethodInfo m1252_MI;
extern MethodInfo m1253_MI;
extern MethodInfo m1254_MI;
extern MethodInfo m1255_MI;
extern MethodInfo m1256_MI;
extern MethodInfo m1257_MI;
extern MethodInfo m1258_MI;
extern MethodInfo m1259_MI;
extern MethodInfo m1260_MI;
extern MethodInfo m1261_MI;
extern MethodInfo m1262_MI;
extern MethodInfo m1263_MI;
extern MethodInfo m1264_MI;
extern MethodInfo m1265_MI;
extern MethodInfo m1266_MI;
extern MethodInfo m1267_MI;
extern MethodInfo m1268_MI;
extern MethodInfo m1269_MI;
static MethodInfo* t225_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
extern TypeInfo t288_TI;
extern TypeInfo t289_TI;
extern TypeInfo t290_TI;
static Il2CppInterfaceOffsetPair t225_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t225_0_0_0;
extern Il2CppType t225_1_0_0;
extern TypeInfo t49_TI;
extern TypeInfo t44_TI;
extern TypeInfo t139_TI;
TypeInfo t225_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Transition", "", t225_MIs, NULL, t225_FIs, NULL, &t49_TI, NULL, &t139_TI, &t44_TI, NULL, t225_VT, &EmptyCustomAttributesCache, &t44_TI, &t225_0_0_0, &t225_1_0_0, t225_IOs, NULL, NULL, t225_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t225)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 258, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 23, 0, 3};
#include "t207.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t207_TI;
#include "t207MD.h"



// Metadata Definition UnityEngine.UI.Selectable/SelectionState
extern Il2CppType t44_0_0_1542;
FieldInfo t207_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t207_TI, offsetof(t207, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t207_0_0_32854;
FieldInfo t207_f2_FieldInfo = 
{
	"Normal", &t207_0_0_32854, &t207_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t207_0_0_32854;
FieldInfo t207_f3_FieldInfo = 
{
	"Highlighted", &t207_0_0_32854, &t207_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t207_0_0_32854;
FieldInfo t207_f4_FieldInfo = 
{
	"Pressed", &t207_0_0_32854, &t207_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t207_0_0_32854;
FieldInfo t207_f5_FieldInfo = 
{
	"Disabled", &t207_0_0_32854, &t207_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t207_FIs[] =
{
	&t207_f1_FieldInfo,
	&t207_f2_FieldInfo,
	&t207_f3_FieldInfo,
	&t207_f4_FieldInfo,
	&t207_f5_FieldInfo,
	NULL
};
static const int32_t t207_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t207_f2_DefaultValue = 
{
	&t207_f2_FieldInfo, { (char*)&t207_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t207_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t207_f3_DefaultValue = 
{
	&t207_f3_FieldInfo, { (char*)&t207_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t207_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t207_f4_DefaultValue = 
{
	&t207_f4_FieldInfo, { (char*)&t207_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t207_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t207_f5_DefaultValue = 
{
	&t207_f5_FieldInfo, { (char*)&t207_f5_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t207_FDVs[] = 
{
	&t207_f2_DefaultValue,
	&t207_f3_DefaultValue,
	&t207_f4_DefaultValue,
	&t207_f5_DefaultValue,
	NULL
};
static MethodInfo* t207_MIs[] =
{
	NULL
};
static MethodInfo* t207_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t207_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t207_0_0_0;
extern Il2CppType t207_1_0_0;
TypeInfo t207_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "SelectionState", "", t207_MIs, NULL, t207_FIs, NULL, &t49_TI, NULL, &t139_TI, &t44_TI, NULL, t207_VT, &EmptyCustomAttributesCache, &t44_TI, &t207_0_0_0, &t207_1_0_0, t207_IOs, NULL, NULL, t207_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t207)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 260, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 23, 0, 3};
#include "t139.h"
#ifndef _MSC_VER
#else
#endif
#include "t139MD.h"

#include "t226.h"
#include "t210.h"
#include "t146.h"
#include "t228.h"
#include "t137.h"
#include "t155.h"
#include "t179.h"
#include "t229.h"
#include "t227.h"
#include "t352.h"
#include "t7.h"
#include "t132.h"
#include "t180.h"
#include "t16.h"
#include "t362.h"
#include "t209.h"
#include "t64.h"
#include "t106.h"
#include "t398.h"
#include "t53.h"
#include "t50.h"
extern TypeInfo t179_TI;
extern TypeInfo t137_TI;
extern TypeInfo t227_TI;
extern TypeInfo t226_TI;
extern TypeInfo t352_TI;
extern TypeInfo t7_TI;
extern TypeInfo t6_TI;
extern TypeInfo t50_TI;
#include "t230MD.h"
#include "t210MD.h"
#include "t146MD.h"
#include "t137MD.h"
#include "t227MD.h"
#include "t226MD.h"
#include "t352MD.h"
#include "t132MD.h"
#include "t228MD.h"
#include "t7MD.h"
#include "t16MD.h"
#include "t362MD.h"
#include "t53MD.h"
#include "t64MD.h"
#include "t155MD.h"
#include "t179MD.h"
#include "t317MD.h"
#include "t229MD.h"
#include "t50MD.h"
extern MethodInfo m1867_MI;
extern MethodInfo m862_MI;
extern MethodInfo m1868_MI;
extern MethodInfo m1869_MI;
extern MethodInfo m1870_MI;
extern MethodInfo m1871_MI;
extern MethodInfo m1717_MI;
extern MethodInfo m1641_MI;
extern MethodInfo m1872_MI;
extern MethodInfo m718_MI;
extern MethodInfo m409_MI;
extern MethodInfo m357_MI;
extern MethodInfo m1873_MI;
extern MethodInfo m1874_MI;
extern MethodInfo m1875_MI;
extern MethodInfo m1876_MI;
extern MethodInfo m1877_MI;
extern MethodInfo m1878_MI;
extern MethodInfo m1586_MI;
extern MethodInfo m1879_MI;
extern MethodInfo m1365_MI;
extern MethodInfo m1880_MI;
extern MethodInfo m852_MI;
extern MethodInfo m883_MI;
extern MethodInfo m1881_MI;
extern MethodInfo m865_MI;
extern MethodInfo m358_MI;
extern MethodInfo m849_MI;
extern MethodInfo m851_MI;
extern MethodInfo m853_MI;
extern MethodInfo m1559_MI;
extern MethodInfo m875_MI;
extern MethodInfo m876_MI;
extern MethodInfo m877_MI;
extern MethodInfo m397_MI;
extern MethodInfo m399_MI;
extern MethodInfo m936_MI;
extern MethodInfo m360_MI;
extern MethodInfo m401_MI;
extern MethodInfo m938_MI;
extern MethodInfo m362_MI;
extern MethodInfo m403_MI;
extern MethodInfo m940_MI;
extern MethodInfo m364_MI;
extern MethodInfo m1596_MI;
extern MethodInfo m1392_MI;
extern MethodInfo m1393_MI;
extern MethodInfo m405_MI;
extern MethodInfo m1882_MI;
extern MethodInfo m1883_MI;
extern MethodInfo m1619_MI;
extern MethodInfo m1884_MI;
extern MethodInfo m1621_MI;
extern MethodInfo m868_MI;
extern MethodInfo m1885_MI;
extern MethodInfo m1886_MI;
extern MethodInfo m859_MI;
extern MethodInfo m834_MI;
extern MethodInfo m708_MI;
extern MethodInfo m1622_MI;
extern MethodInfo m1887_MI;
extern MethodInfo m1888_MI;
extern MethodInfo m1889_MI;
extern MethodInfo m1799_MI;
extern MethodInfo m192_MI;
extern MethodInfo m714_MI;
extern MethodInfo m1890_MI;
extern MethodInfo m867_MI;
extern MethodInfo m716_MI;
extern MethodInfo m1891_MI;
extern MethodInfo m710_MI;
extern MethodInfo m1892_MI;
extern MethodInfo m712_MI;
extern MethodInfo m1893_MI;
extern MethodInfo m184_MI;
extern MethodInfo m871_MI;
extern MethodInfo m869_MI;
extern MethodInfo m872_MI;
extern MethodInfo m870_MI;
extern MethodInfo m873_MI;
extern MethodInfo m407_MI;
extern MethodInfo m474_MI;
extern MethodInfo m854_MI;
extern MethodInfo m512_MI;
extern MethodInfo m856_MI;
extern MethodInfo m1390_MI;
extern MethodInfo m1391_MI;
extern MethodInfo m1894_MI;
extern MethodInfo m1772_MI;
extern MethodInfo m1895_MI;
extern MethodInfo m1896_MI;
extern MethodInfo m880_MI;
extern MethodInfo m850_MI;
extern MethodInfo m848_MI;
extern MethodInfo m236_MI;
extern MethodInfo m878_MI;
extern MethodInfo m881_MI;
extern MethodInfo m866_MI;
extern MethodInfo m75_MI;
extern MethodInfo m88_MI;
extern MethodInfo m882_MI;
extern MethodInfo m87_MI;
extern MethodInfo m90_MI;
struct t230;
#include "t230.h"
#include "t110.h"
 bool m1867 (t29 * __this, t210 * p0, t210  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t230;
 bool m1868 (t29 * __this, int32_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t230;
 bool m1869 (t29 * __this, t146 * p0, t146  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t230;
 bool m1870 (t29 * __this, t228 * p0, t228  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t230;
struct t230;
 bool m1690_gshared (t29 * __this, t29 ** p0, t29 * p1, MethodInfo* method);
#define m1690(__this, p0, p1, method) (bool)m1690_gshared((t29 *)__this, (t29 **)p0, (t29 *)p1, method)
#define m1871(__this, p0, p1, method) (bool)m1690_gshared((t29 *)__this, (t29 **)p0, (t29 *)p1, method)
struct t230;
#define m1717(__this, p0, p1, method) (bool)m1690_gshared((t29 *)__this, (t29 **)p0, (t29 *)p1, method)
struct t230;
 bool m1641 (t29 * __this, bool* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t28;
#include "t28.h"
#include "t42.h"
#include "t43.h"
struct t28;
 t29 * m68_gshared (t28 * __this, MethodInfo* method);
#define m68(__this, method) (t29 *)m68_gshared((t28 *)__this, method)
#define m1872(__this, method) (t229 *)m68_gshared((t28 *)__this, method)
struct t28;
#define m1875(__this, method) (t155 *)m68_gshared((t28 *)__this, method)
struct t28;
struct t28;
#include "t294.h"
 void m1316_gshared (t28 * __this, t294 * p0, MethodInfo* method);
#define m1316(__this, p0, method) (void)m1316_gshared((t28 *)__this, (t294 *)p0, method)
#define m1876(__this, p0, method) (void)m1316_gshared((t28 *)__this, (t294 *)p0, method)


extern MethodInfo m831_MI;
 void m831 (t139 * __this, MethodInfo* method){
	{
		t210  L_0 = m718(NULL, &m718_MI);
		__this->f3 = L_0;
		__this->f4 = 1;
		t146  L_1 = m409(NULL, &m409_MI);
		__this->f5 = L_1;
		t137 * L_2 = (t137 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t137_TI));
		m357(L_2, &m357_MI);
		__this->f7 = L_2;
		__this->f8 = 1;
		__this->f10 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t227_TI));
		t227 * L_3 = (t227 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t227_TI));
		m1873(L_3, &m1873_MI);
		__this->f12 = L_3;
		m168(__this, &m168_MI);
		return;
	}
}
extern MethodInfo m832_MI;
 void m832 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t226_TI));
		t226 * L_0 = (t226 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t226_TI));
		m1874(L_0, &m1874_MI);
		((t139_SFs*)InitializedTypeInfo(&t139_TI)->static_fields)->f2 = L_0;
		return;
	}
}
extern MethodInfo m833_MI;
 t226 * m833 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t139_TI));
		return (((t139_SFs*)InitializedTypeInfo(&t139_TI)->static_fields)->f2);
	}
}
 t210  m834 (t139 * __this, MethodInfo* method){
	{
		t210  L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m835_MI;
 void m835 (t139 * __this, t210  p0, MethodInfo* method){
	{
		t210 * L_0 = &(__this->f3);
		bool L_1 = m1867(NULL, L_0, p0, &m1867_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m862(__this, &m862_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m836_MI;
 int32_t m836 (t139 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m837_MI;
 void m837 (t139 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t* L_0 = &(__this->f4);
		bool L_1 = m1868(NULL, L_0, p0, &m1868_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m862(__this, &m862_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m838_MI;
 t146  m838 (t139 * __this, MethodInfo* method){
	{
		t146  L_0 = (__this->f5);
		return L_0;
	}
}
extern MethodInfo m839_MI;
 void m839 (t139 * __this, t146  p0, MethodInfo* method){
	{
		t146 * L_0 = &(__this->f5);
		bool L_1 = m1869(NULL, L_0, p0, &m1869_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m862(__this, &m862_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m840_MI;
 t228  m840 (t139 * __this, MethodInfo* method){
	{
		t228  L_0 = (__this->f6);
		return L_0;
	}
}
extern MethodInfo m841_MI;
 void m841 (t139 * __this, t228  p0, MethodInfo* method){
	{
		t228 * L_0 = &(__this->f6);
		bool L_1 = m1870(NULL, L_0, p0, &m1870_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m862(__this, &m862_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m842_MI;
 t137 * m842 (t139 * __this, MethodInfo* method){
	{
		t137 * L_0 = (__this->f7);
		return L_0;
	}
}
extern MethodInfo m843_MI;
 void m843 (t139 * __this, t137 * p0, MethodInfo* method){
	{
		t137 ** L_0 = &(__this->f7);
		bool L_1 = m1871(NULL, L_0, p0, &m1871_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m862(__this, &m862_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m844_MI;
 t155 * m844 (t139 * __this, MethodInfo* method){
	{
		t155 * L_0 = (__this->f9);
		return L_0;
	}
}
extern MethodInfo m845_MI;
 void m845 (t139 * __this, t155 * p0, MethodInfo* method){
	{
		t155 ** L_0 = &(__this->f9);
		bool L_1 = m1717(NULL, L_0, p0, &m1717_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m862(__this, &m862_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m846_MI;
 bool m846 (t139 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f8);
		return L_0;
	}
}
extern MethodInfo m847_MI;
 void m847 (t139 * __this, bool p0, MethodInfo* method){
	{
		bool* L_0 = &(__this->f8);
		bool L_1 = m1641(NULL, L_0, p0, &m1641_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m862(__this, &m862_MI);
	}

IL_0017:
	{
		return;
	}
}
 bool m848 (t139 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f13);
		return L_0;
	}
}
 void m849 (t139 * __this, bool p0, MethodInfo* method){
	{
		__this->f13 = p0;
		return;
	}
}
 bool m850 (t139 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f14);
		return L_0;
	}
}
 void m851 (t139 * __this, bool p0, MethodInfo* method){
	{
		__this->f14 = p0;
		return;
	}
}
 bool m852 (t139 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f15);
		return L_0;
	}
}
 void m853 (t139 * __this, bool p0, MethodInfo* method){
	{
		__this->f15 = p0;
		return;
	}
}
 t179 * m854 (t139 * __this, MethodInfo* method){
	{
		t155 * L_0 = (__this->f9);
		return ((t179 *)IsInst(L_0, InitializedTypeInfo(&t179_TI)));
	}
}
extern MethodInfo m855_MI;
 void m855 (t139 * __this, t179 * p0, MethodInfo* method){
	{
		__this->f9 = p0;
		return;
	}
}
 t229 * m856 (t139 * __this, MethodInfo* method){
	{
		t229 * L_0 = m1872(__this, &m1872_MI);
		return L_0;
	}
}
extern MethodInfo m857_MI;
 void m857 (t139 * __this, MethodInfo* method){
	{
		t155 * L_0 = (__this->f9);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		t155 * L_2 = m1875(__this, &m1875_MI);
		__this->f9 = L_2;
	}

IL_001d:
	{
		return;
	}
}
extern MethodInfo m858_MI;
 void m858 (t139 * __this, MethodInfo* method){
	bool V_0 = false;
	t25 * V_1 = {0};
	bool V_2 = false;
	int32_t V_3 = 0;
	{
		V_0 = 1;
		t25 * L_0 = m39(__this, &m39_MI);
		V_1 = L_0;
		goto IL_007c;
	}

IL_000e:
	{
		t227 * L_1 = (__this->f12);
		m1876(V_1, L_1, &m1876_MI);
		V_2 = 0;
		V_3 = 0;
		goto IL_0059;
	}

IL_0023:
	{
		t227 * L_2 = (__this->f12);
		t352 * L_3 = (t352 *)VirtFuncInvoker1< t352 *, int32_t >::Invoke(&m1877_MI, L_2, V_3);
		bool L_4 = m1878(L_3, &m1878_MI);
		if (L_4)
		{
			goto IL_003d;
		}
	}
	{
		V_0 = 0;
		V_2 = 1;
	}

IL_003d:
	{
		t227 * L_5 = (__this->f12);
		t352 * L_6 = (t352 *)VirtFuncInvoker1< t352 *, int32_t >::Invoke(&m1877_MI, L_5, V_3);
		bool L_7 = m1586(L_6, &m1586_MI);
		if (!L_7)
		{
			goto IL_0055;
		}
	}
	{
		V_2 = 1;
	}

IL_0055:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_0059:
	{
		t227 * L_8 = (__this->f12);
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1879_MI, L_8);
		if ((((int32_t)V_3) < ((int32_t)L_9)))
		{
			goto IL_0023;
		}
	}
	{
		if (!V_2)
		{
			goto IL_0075;
		}
	}
	{
		goto IL_0088;
	}

IL_0075:
	{
		t25 * L_10 = m1365(V_1, &m1365_MI);
		V_1 = L_10;
	}

IL_007c:
	{
		bool L_11 = m1300(NULL, V_1, (t41 *)NULL, &m1300_MI);
		if (L_11)
		{
			goto IL_000e;
		}
	}

IL_0088:
	{
		bool L_12 = (__this->f10);
		if ((((int32_t)V_0) == ((int32_t)L_12)))
		{
			goto IL_00a1;
		}
	}
	{
		__this->f10 = V_0;
		m862(__this, &m862_MI);
	}

IL_00a1:
	{
		return;
	}
}
 bool m859 (t139 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (__this->f10);
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		bool L_1 = (__this->f8);
		G_B3_0 = ((int32_t)(L_1));
		goto IL_0014;
	}

IL_0013:
	{
		G_B3_0 = 0;
	}

IL_0014:
	{
		return G_B3_0;
	}
}
extern MethodInfo m860_MI;
 void m860 (t139 * __this, MethodInfo* method){
	{
		m862(__this, &m862_MI);
		return;
	}
}
extern MethodInfo m861_MI;
 void m861 (t139 * __this, MethodInfo* method){
	int32_t V_0 = {0};
	{
		m170(__this, &m170_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t139_TI));
		VirtActionInvoker1< t139 * >::Invoke(&m1880_MI, (((t139_SFs*)InitializedTypeInfo(&t139_TI)->static_fields)->f2), __this);
		V_0 = 0;
		bool L_0 = m852(__this, &m852_MI);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		V_0 = 1;
	}

IL_0020:
	{
		__this->f11 = V_0;
		m883(__this, 1, &m883_MI);
		return;
	}
}
 void m862 (t139 * __this, MethodInfo* method){
	{
		m883(__this, 0, &m883_MI);
		return;
	}
}
extern MethodInfo m863_MI;
 void m863 (t139 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t139_TI));
		VirtFuncInvoker1< bool, t139 * >::Invoke(&m1881_MI, (((t139_SFs*)InitializedTypeInfo(&t139_TI)->static_fields)->f2), __this);
		VirtActionInvoker0::Invoke(&m865_MI, __this);
		m172(__this, &m172_MI);
		return;
	}
}
extern MethodInfo m864_MI;
 int32_t m864 (t139 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f11);
		return L_0;
	}
}
 void m865 (t139 * __this, MethodInfo* method){
	t7* V_0 = {0};
	int32_t V_1 = {0};
	{
		t137 * L_0 = (__this->f7);
		t7* L_1 = m358(L_0, &m358_MI);
		V_0 = L_1;
		m849(__this, 0, &m849_MI);
		m851(__this, 0, &m851_MI);
		m853(__this, 0, &m853_MI);
		int32_t L_2 = (__this->f4);
		V_1 = L_2;
		if (((int32_t)(V_1-1)) == 0)
		{
			goto IL_0041;
		}
		if (((int32_t)(V_1-1)) == 1)
		{
			goto IL_0052;
		}
		if (((int32_t)(V_1-1)) == 2)
		{
			goto IL_005e;
		}
	}
	{
		goto IL_006a;
	}

IL_0041:
	{
		t132  L_3 = m1559(NULL, &m1559_MI);
		m875(__this, L_3, 1, &m875_MI);
		goto IL_006a;
	}

IL_0052:
	{
		m876(__this, (t180 *)NULL, &m876_MI);
		goto IL_006a;
	}

IL_005e:
	{
		m877(__this, V_0, &m877_MI);
		goto IL_006a;
	}

IL_006a:
	{
		return;
	}
}
 void m866 (t139 * __this, int32_t p0, bool p1, MethodInfo* method){
	t132  V_0 = {0};
	t180 * V_1 = {0};
	t7* V_2 = {0};
	int32_t V_3 = {0};
	int32_t V_4 = {0};
	{
		V_3 = p0;
		if (V_3 == 0)
		{
			goto IL_001d;
		}
		if (V_3 == 1)
		{
			goto IL_003c;
		}
		if (V_3 == 2)
		{
			goto IL_0065;
		}
		if (V_3 == 3)
		{
			goto IL_008e;
		}
	}
	{
		goto IL_00b7;
	}

IL_001d:
	{
		t146 * L_0 = &(__this->f5);
		t132  L_1 = m397(L_0, &m397_MI);
		V_0 = L_1;
		V_1 = (t180 *)NULL;
		t137 * L_2 = (__this->f7);
		t7* L_3 = m358(L_2, &m358_MI);
		V_2 = L_3;
		goto IL_00ca;
	}

IL_003c:
	{
		t146 * L_4 = &(__this->f5);
		t132  L_5 = m399(L_4, &m399_MI);
		V_0 = L_5;
		t228 * L_6 = &(__this->f6);
		t180 * L_7 = m936(L_6, &m936_MI);
		V_1 = L_7;
		t137 * L_8 = (__this->f7);
		t7* L_9 = m360(L_8, &m360_MI);
		V_2 = L_9;
		goto IL_00ca;
	}

IL_0065:
	{
		t146 * L_10 = &(__this->f5);
		t132  L_11 = m401(L_10, &m401_MI);
		V_0 = L_11;
		t228 * L_12 = &(__this->f6);
		t180 * L_13 = m938(L_12, &m938_MI);
		V_1 = L_13;
		t137 * L_14 = (__this->f7);
		t7* L_15 = m362(L_14, &m362_MI);
		V_2 = L_15;
		goto IL_00ca;
	}

IL_008e:
	{
		t146 * L_16 = &(__this->f5);
		t132  L_17 = m403(L_16, &m403_MI);
		V_0 = L_17;
		t228 * L_18 = &(__this->f6);
		t180 * L_19 = m940(L_18, &m940_MI);
		V_1 = L_19;
		t137 * L_20 = (__this->f7);
		t7* L_21 = m364(L_20, &m364_MI);
		V_2 = L_21;
		goto IL_00ca;
	}

IL_00b7:
	{
		t132  L_22 = m1596(NULL, &m1596_MI);
		V_0 = L_22;
		V_1 = (t180 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		V_2 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		goto IL_00ca;
	}

IL_00ca:
	{
		t16 * L_23 = m1392(__this, &m1392_MI);
		bool L_24 = m1393(L_23, &m1393_MI);
		if (!L_24)
		{
			goto IL_0131;
		}
	}
	{
		int32_t L_25 = (__this->f4);
		V_4 = L_25;
		if (((int32_t)(V_4-1)) == 0)
		{
			goto IL_00fc;
		}
		if (((int32_t)(V_4-1)) == 1)
		{
			goto IL_0119;
		}
		if (((int32_t)(V_4-1)) == 2)
		{
			goto IL_0125;
		}
	}
	{
		goto IL_0131;
	}

IL_00fc:
	{
		t146 * L_26 = &(__this->f5);
		float L_27 = m405(L_26, &m405_MI);
		t132  L_28 = m1882(NULL, V_0, L_27, &m1882_MI);
		m875(__this, L_28, p1, &m875_MI);
		goto IL_0131;
	}

IL_0119:
	{
		m876(__this, V_1, &m876_MI);
		goto IL_0131;
	}

IL_0125:
	{
		m877(__this, V_2, &m877_MI);
		goto IL_0131;
	}

IL_0131:
	{
		return;
	}
}
 t139 * m867 (t139 * __this, t23  p0, MethodInfo* method){
	t23  V_0 = {0};
	t23  V_1 = {0};
	float V_2 = 0.0f;
	t139 * V_3 = {0};
	int32_t V_4 = 0;
	t139 * V_5 = {0};
	t2 * V_6 = {0};
	t23  V_7 = {0};
	t23  V_8 = {0};
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	t210  V_11 = {0};
	t164  V_12 = {0};
	t23  G_B10_0 = {0};
	{
		t23  L_0 = m1883((&p0), &m1883_MI);
		p0 = L_0;
		t25 * L_1 = m39(__this, &m39_MI);
		t362  L_2 = m1619(L_1, &m1619_MI);
		t362  L_3 = m1884(NULL, L_2, &m1884_MI);
		t23  L_4 = m1621(NULL, L_3, p0, &m1621_MI);
		V_0 = L_4;
		t25 * L_5 = m39(__this, &m39_MI);
		t25 * L_6 = m39(__this, &m39_MI);
		t17  L_7 = m1419(NULL, V_0, &m1419_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t139_TI));
		t23  L_8 = m868(NULL, ((t2 *)IsInst(L_6, InitializedTypeInfo(&t2_TI))), L_7, &m868_MI);
		t23  L_9 = m1885(L_5, L_8, &m1885_MI);
		V_1 = L_9;
		V_2 = (-std::numeric_limits<float>::infinity());
		V_3 = (t139 *)NULL;
		V_4 = 0;
		goto IL_0132;
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t139_TI));
		t139 * L_10 = (t139 *)VirtFuncInvoker1< t139 *, int32_t >::Invoke(&m1886_MI, (((t139_SFs*)InitializedTypeInfo(&t139_TI)->static_fields)->f2), V_4);
		V_5 = L_10;
		bool L_11 = m1297(NULL, V_5, __this, &m1297_MI);
		if (L_11)
		{
			goto IL_007a;
		}
	}
	{
		bool L_12 = m1297(NULL, V_5, (t41 *)NULL, &m1297_MI);
		if (!L_12)
		{
			goto IL_007f;
		}
	}

IL_007a:
	{
		goto IL_012c;
	}

IL_007f:
	{
		bool L_13 = (bool)VirtFuncInvoker0< bool >::Invoke(&m859_MI, V_5);
		if (!L_13)
		{
			goto IL_00a0;
		}
	}
	{
		t210  L_14 = m834(V_5, &m834_MI);
		V_11 = L_14;
		int32_t L_15 = m708((&V_11), &m708_MI);
		if (L_15)
		{
			goto IL_00a5;
		}
	}

IL_00a0:
	{
		goto IL_012c;
	}

IL_00a5:
	{
		t25 * L_16 = m39(V_5, &m39_MI);
		V_6 = ((t2 *)IsInst(L_16, InitializedTypeInfo(&t2_TI)));
		bool L_17 = m1300(NULL, V_6, (t41 *)NULL, &m1300_MI);
		if (!L_17)
		{
			goto IL_00da;
		}
	}
	{
		t164  L_18 = m1572(V_6, &m1572_MI);
		V_12 = L_18;
		t17  L_19 = m1834((&V_12), &m1834_MI);
		t23  L_20 = m1463(NULL, L_19, &m1463_MI);
		G_B10_0 = L_20;
		goto IL_00df;
	}

IL_00da:
	{
		t23  L_21 = m1395(NULL, &m1395_MI);
		G_B10_0 = L_21;
	}

IL_00df:
	{
		V_7 = G_B10_0;
		t25 * L_22 = m39(V_5, &m39_MI);
		t23  L_23 = m1885(L_22, V_7, &m1885_MI);
		t23  L_24 = m1624(NULL, L_23, V_1, &m1624_MI);
		V_8 = L_24;
		float L_25 = m1622(NULL, p0, V_8, &m1622_MI);
		V_9 = L_25;
		if ((((float)V_9) > ((float)(0.0f))))
		{
			goto IL_0112;
		}
	}
	{
		goto IL_012c;
	}

IL_0112:
	{
		float L_26 = m1887((&V_8), &m1887_MI);
		V_10 = ((float)((float)V_9/(float)L_26));
		if ((((float)V_10) <= ((float)V_2)))
		{
			goto IL_012c;
		}
	}
	{
		V_2 = V_10;
		V_3 = V_5;
	}

IL_012c:
	{
		V_4 = ((int32_t)(V_4+1));
	}

IL_0132:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t139_TI));
		int32_t L_27 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1888_MI, (((t139_SFs*)InitializedTypeInfo(&t139_TI)->static_fields)->f2));
		if ((((int32_t)V_4) < ((int32_t)L_27)))
		{
			goto IL_0052;
		}
	}
	{
		return V_3;
	}
}
 t23  m868 (t29 * __this, t2 * p0, t17  p1, MethodInfo* method){
	t164  V_0 = {0};
	t164  V_1 = {0};
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		t23  L_1 = m1395(NULL, &m1395_MI);
		return L_1;
	}

IL_0012:
	{
		t17  L_2 = m1394(NULL, &m1394_MI);
		bool L_3 = m1792(NULL, p1, L_2, &m1792_MI);
		if (!L_3)
		{
			goto IL_0047;
		}
	}
	{
		float L_4 = ((&p1)->f1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_5 = fabsf(L_4);
		float L_6 = ((&p1)->f2);
		float L_7 = fabsf(L_6);
		float L_8 = m1889(NULL, L_5, L_7, &m1889_MI);
		t17  L_9 = m1754(NULL, p1, L_8, &m1754_MI);
		p1 = L_9;
	}

IL_0047:
	{
		t164  L_10 = m1572(p0, &m1572_MI);
		V_0 = L_10;
		t17  L_11 = m1834((&V_0), &m1834_MI);
		t164  L_12 = m1572(p0, &m1572_MI);
		V_1 = L_12;
		t17  L_13 = m1653((&V_1), &m1653_MI);
		t17  L_14 = m1668(NULL, p1, (0.5f), &m1668_MI);
		t17  L_15 = m1799(NULL, L_13, L_14, &m1799_MI);
		t17  L_16 = m1670(NULL, L_11, L_15, &m1670_MI);
		p1 = L_16;
		t23  L_17 = m1463(NULL, p1, &m1463_MI);
		return L_17;
	}
}
 void m869 (t139 * __this, t64 * p0, t139 * p1, MethodInfo* method){
	{
		bool L_0 = m1300(NULL, p1, (t41 *)NULL, &m1300_MI);
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, p1);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		t16 * L_2 = m1392(p1, &m1392_MI);
		m192(p0, L_2, &m192_MI);
	}

IL_0023:
	{
		return;
	}
}
 t139 * m870 (t139 * __this, MethodInfo* method){
	{
		t210 * L_0 = &(__this->f3);
		int32_t L_1 = m708(L_0, &m708_MI);
		if ((((uint32_t)L_1) != ((uint32_t)4)))
		{
			goto IL_001d;
		}
	}
	{
		t210 * L_2 = &(__this->f3);
		t139 * L_3 = m714(L_2, &m714_MI);
		return L_3;
	}

IL_001d:
	{
		t210 * L_4 = &(__this->f3);
		int32_t L_5 = m708(L_4, &m708_MI);
		if (!((int32_t)((int32_t)L_5&(int32_t)1)))
		{
			goto IL_004b;
		}
	}
	{
		t25 * L_6 = m39(__this, &m39_MI);
		t362  L_7 = m1619(L_6, &m1619_MI);
		t23  L_8 = m1890(NULL, &m1890_MI);
		t23  L_9 = m1621(NULL, L_7, L_8, &m1621_MI);
		t139 * L_10 = m867(__this, L_9, &m867_MI);
		return L_10;
	}

IL_004b:
	{
		return (t139 *)NULL;
	}
}
 t139 * m871 (t139 * __this, MethodInfo* method){
	{
		t210 * L_0 = &(__this->f3);
		int32_t L_1 = m708(L_0, &m708_MI);
		if ((((uint32_t)L_1) != ((uint32_t)4)))
		{
			goto IL_001d;
		}
	}
	{
		t210 * L_2 = &(__this->f3);
		t139 * L_3 = m716(L_2, &m716_MI);
		return L_3;
	}

IL_001d:
	{
		t210 * L_4 = &(__this->f3);
		int32_t L_5 = m708(L_4, &m708_MI);
		if (!((int32_t)((int32_t)L_5&(int32_t)1)))
		{
			goto IL_004b;
		}
	}
	{
		t25 * L_6 = m39(__this, &m39_MI);
		t362  L_7 = m1619(L_6, &m1619_MI);
		t23  L_8 = m1891(NULL, &m1891_MI);
		t23  L_9 = m1621(NULL, L_7, L_8, &m1621_MI);
		t139 * L_10 = m867(__this, L_9, &m867_MI);
		return L_10;
	}

IL_004b:
	{
		return (t139 *)NULL;
	}
}
 t139 * m872 (t139 * __this, MethodInfo* method){
	{
		t210 * L_0 = &(__this->f3);
		int32_t L_1 = m708(L_0, &m708_MI);
		if ((((uint32_t)L_1) != ((uint32_t)4)))
		{
			goto IL_001d;
		}
	}
	{
		t210 * L_2 = &(__this->f3);
		t139 * L_3 = m710(L_2, &m710_MI);
		return L_3;
	}

IL_001d:
	{
		t210 * L_4 = &(__this->f3);
		int32_t L_5 = m708(L_4, &m708_MI);
		if (!((int32_t)((int32_t)L_5&(int32_t)2)))
		{
			goto IL_004b;
		}
	}
	{
		t25 * L_6 = m39(__this, &m39_MI);
		t362  L_7 = m1619(L_6, &m1619_MI);
		t23  L_8 = m1892(NULL, &m1892_MI);
		t23  L_9 = m1621(NULL, L_7, L_8, &m1621_MI);
		t139 * L_10 = m867(__this, L_9, &m867_MI);
		return L_10;
	}

IL_004b:
	{
		return (t139 *)NULL;
	}
}
 t139 * m873 (t139 * __this, MethodInfo* method){
	{
		t210 * L_0 = &(__this->f3);
		int32_t L_1 = m708(L_0, &m708_MI);
		if ((((uint32_t)L_1) != ((uint32_t)4)))
		{
			goto IL_001d;
		}
	}
	{
		t210 * L_2 = &(__this->f3);
		t139 * L_3 = m712(L_2, &m712_MI);
		return L_3;
	}

IL_001d:
	{
		t210 * L_4 = &(__this->f3);
		int32_t L_5 = m708(L_4, &m708_MI);
		if (!((int32_t)((int32_t)L_5&(int32_t)2)))
		{
			goto IL_004b;
		}
	}
	{
		t25 * L_6 = m39(__this, &m39_MI);
		t362  L_7 = m1619(L_6, &m1619_MI);
		t23  L_8 = m1893(NULL, &m1893_MI);
		t23  L_9 = m1621(NULL, L_7, L_8, &m1621_MI);
		t139 * L_10 = m867(__this, L_9, &m867_MI);
		return L_10;
	}

IL_004b:
	{
		return (t139 *)NULL;
	}
}
extern MethodInfo m874_MI;
 void m874 (t139 * __this, t64 * p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m184(p0, &m184_MI);
		V_0 = L_0;
		if (V_0 == 0)
		{
			goto IL_0046;
		}
		if (V_0 == 1)
		{
			goto IL_0034;
		}
		if (V_0 == 2)
		{
			goto IL_0022;
		}
		if (V_0 == 3)
		{
			goto IL_0058;
		}
	}
	{
		goto IL_006a;
	}

IL_0022:
	{
		t139 * L_1 = (t139 *)VirtFuncInvoker0< t139 * >::Invoke(&m871_MI, __this);
		m869(__this, p0, L_1, &m869_MI);
		goto IL_006a;
	}

IL_0034:
	{
		t139 * L_2 = (t139 *)VirtFuncInvoker0< t139 * >::Invoke(&m872_MI, __this);
		m869(__this, p0, L_2, &m869_MI);
		goto IL_006a;
	}

IL_0046:
	{
		t139 * L_3 = (t139 *)VirtFuncInvoker0< t139 * >::Invoke(&m870_MI, __this);
		m869(__this, p0, L_3, &m869_MI);
		goto IL_006a;
	}

IL_0058:
	{
		t139 * L_4 = (t139 *)VirtFuncInvoker0< t139 * >::Invoke(&m873_MI, __this);
		m869(__this, p0, L_4, &m869_MI);
		goto IL_006a;
	}

IL_006a:
	{
		return;
	}
}
 void m875 (t139 * __this, t132  p0, bool p1, MethodInfo* method){
	t132  G_B4_0 = {0};
	t155 * G_B4_1 = {0};
	t132  G_B3_0 = {0};
	t155 * G_B3_1 = {0};
	float G_B5_0 = 0.0f;
	t132  G_B5_1 = {0};
	t155 * G_B5_2 = {0};
	{
		t155 * L_0 = (__this->f9);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t155 * L_2 = (__this->f9);
		G_B3_0 = p0;
		G_B3_1 = L_2;
		if (!p1)
		{
			G_B4_0 = p0;
			G_B4_1 = L_2;
			goto IL_0029;
		}
	}
	{
		G_B5_0 = (0.0f);
		G_B5_1 = G_B3_0;
		G_B5_2 = G_B3_1;
		goto IL_0034;
	}

IL_0029:
	{
		t146 * L_3 = &(__this->f5);
		float L_4 = m407(L_3, &m407_MI);
		G_B5_0 = L_4;
		G_B5_1 = G_B4_0;
		G_B5_2 = G_B4_1;
	}

IL_0034:
	{
		m474(G_B5_2, G_B5_1, G_B5_0, 1, 1, &m474_MI);
		return;
	}
}
 void m876 (t139 * __this, t180 * p0, MethodInfo* method){
	{
		t179 * L_0 = m854(__this, &m854_MI);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t179 * L_2 = m854(__this, &m854_MI);
		m512(L_2, p0, &m512_MI);
		return;
	}
}
 void m877 (t139 * __this, t7* p0, MethodInfo* method){
	{
		t229 * L_0 = m856(__this, &m856_MI);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (L_1)
		{
			goto IL_0052;
		}
	}
	{
		t229 * L_2 = m856(__this, &m856_MI);
		bool L_3 = m1390(L_2, &m1390_MI);
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		t229 * L_4 = m856(__this, &m856_MI);
		bool L_5 = m1391(L_4, &m1391_MI);
		if (!L_5)
		{
			goto IL_0052;
		}
	}
	{
		t229 * L_6 = m856(__this, &m856_MI);
		t398 * L_7 = m1894(L_6, &m1894_MI);
		bool L_8 = m1297(NULL, L_7, (t41 *)NULL, &m1297_MI);
		if (L_8)
		{
			goto IL_0052;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_9 = m1772(NULL, p0, &m1772_MI);
		if (!L_9)
		{
			goto IL_0053;
		}
	}

IL_0052:
	{
		return;
	}

IL_0053:
	{
		t229 * L_10 = m856(__this, &m856_MI);
		t137 * L_11 = (__this->f7);
		t7* L_12 = m358(L_11, &m358_MI);
		m1895(L_10, L_12, &m1895_MI);
		t229 * L_13 = m856(__this, &m856_MI);
		t137 * L_14 = (__this->f7);
		t7* L_15 = m362(L_14, &m362_MI);
		m1895(L_13, L_15, &m1895_MI);
		t229 * L_16 = m856(__this, &m856_MI);
		t137 * L_17 = (__this->f7);
		t7* L_18 = m360(L_17, &m360_MI);
		m1895(L_16, L_18, &m1895_MI);
		t229 * L_19 = m856(__this, &m856_MI);
		t137 * L_20 = (__this->f7);
		t7* L_21 = m364(L_20, &m364_MI);
		m1895(L_19, L_21, &m1895_MI);
		t229 * L_22 = m856(__this, &m856_MI);
		m1896(L_22, p0, &m1896_MI);
		return;
	}
}
 bool m878 (t139 * __this, t53 * p0, MethodInfo* method){
	bool V_0 = false;
	t6 * V_1 = {0};
	bool G_B8_0 = false;
	bool G_B6_0 = false;
	bool G_B7_0 = false;
	bool G_B16_0 = false;
	bool G_B11_0 = false;
	bool G_B9_0 = false;
	bool G_B10_0 = false;
	bool G_B14_0 = false;
	bool G_B12_0 = false;
	bool G_B13_0 = false;
	int32_t G_B15_0 = 0;
	bool G_B15_1 = false;
	int32_t G_B17_0 = 0;
	bool G_B17_1 = false;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		bool L_1 = m880(__this, &m880_MI);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		return 0;
	}

IL_001a:
	{
		bool L_2 = m852(__this, &m852_MI);
		V_0 = L_2;
		if (!((t6 *)IsInst(p0, InitializedTypeInfo(&t6_TI))))
		{
			goto IL_00bb;
		}
	}
	{
		V_1 = ((t6 *)IsInst(p0, InitializedTypeInfo(&t6_TI)));
		bool L_3 = m850(__this, &m850_MI);
		G_B6_0 = V_0;
		if (!L_3)
		{
			G_B8_0 = V_0;
			goto IL_0060;
		}
	}
	{
		bool L_4 = m848(__this, &m848_MI);
		G_B7_0 = G_B6_0;
		if (L_4)
		{
			G_B8_0 = G_B6_0;
			goto IL_0060;
		}
	}
	{
		t16 * L_5 = m236(V_1, &m236_MI);
		t16 * L_6 = m1392(__this, &m1392_MI);
		bool L_7 = m1297(NULL, L_5, L_6, &m1297_MI);
		G_B8_0 = G_B7_0;
		if (L_7)
		{
			G_B16_0 = G_B7_0;
			goto IL_00b3;
		}
	}

IL_0060:
	{
		bool L_8 = m850(__this, &m850_MI);
		G_B9_0 = G_B8_0;
		if (L_8)
		{
			G_B11_0 = G_B8_0;
			goto IL_008c;
		}
	}
	{
		bool L_9 = m848(__this, &m848_MI);
		G_B10_0 = G_B9_0;
		if (!L_9)
		{
			G_B11_0 = G_B9_0;
			goto IL_008c;
		}
	}
	{
		t16 * L_10 = m236(V_1, &m236_MI);
		t16 * L_11 = m1392(__this, &m1392_MI);
		bool L_12 = m1297(NULL, L_10, L_11, &m1297_MI);
		G_B11_0 = G_B10_0;
		if (L_12)
		{
			G_B16_0 = G_B10_0;
			goto IL_00b3;
		}
	}

IL_008c:
	{
		bool L_13 = m850(__this, &m850_MI);
		G_B12_0 = G_B11_0;
		if (L_13)
		{
			G_B14_0 = G_B11_0;
			goto IL_00b0;
		}
	}
	{
		bool L_14 = m848(__this, &m848_MI);
		G_B13_0 = G_B12_0;
		if (!L_14)
		{
			G_B14_0 = G_B12_0;
			goto IL_00b0;
		}
	}
	{
		t16 * L_15 = m236(V_1, &m236_MI);
		bool L_16 = m1297(NULL, L_15, (t41 *)NULL, &m1297_MI);
		G_B15_0 = ((int32_t)(L_16));
		G_B15_1 = G_B13_0;
		goto IL_00b1;
	}

IL_00b0:
	{
		G_B15_0 = 0;
		G_B15_1 = G_B14_0;
	}

IL_00b1:
	{
		G_B17_0 = G_B15_0;
		G_B17_1 = G_B15_1;
		goto IL_00b4;
	}

IL_00b3:
	{
		G_B17_0 = 1;
		G_B17_1 = G_B16_0;
	}

IL_00b4:
	{
		V_0 = ((int32_t)((int32_t)G_B17_1|(int32_t)G_B17_0));
		goto IL_00c4;
	}

IL_00bb:
	{
		bool L_17 = m848(__this, &m848_MI);
		V_0 = ((int32_t)((int32_t)V_0|(int32_t)L_17));
	}

IL_00c4:
	{
		return V_0;
	}
}
extern MethodInfo m879_MI;
 bool m879 (t139 * __this, t53 * p0, MethodInfo* method){
	{
		bool L_0 = m880(__this, &m880_MI);
		return L_0;
	}
}
 bool m880 (t139 * __this, MethodInfo* method){
	int32_t G_B5_0 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		bool L_1 = m848(__this, &m848_MI);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		bool L_2 = m850(__this, &m850_MI);
		G_B5_0 = ((int32_t)(L_2));
		goto IL_0021;
	}

IL_0020:
	{
		G_B5_0 = 0;
	}

IL_0021:
	{
		return G_B5_0;
	}
}
 void m881 (t139 * __this, t53 * p0, MethodInfo* method){
	{
		bool L_0 = m880(__this, &m880_MI);
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		__this->f11 = 2;
		return;
	}

IL_0013:
	{
		bool L_1 = m878(__this, p0, &m878_MI);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		__this->f11 = 1;
		return;
	}

IL_0027:
	{
		__this->f11 = 0;
		return;
	}
}
 void m882 (t139 * __this, t53 * p0, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		m881(__this, p0, &m881_MI);
		m883(__this, 0, &m883_MI);
		return;
	}
}
 void m883 (t139 * __this, bool p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = (__this->f11);
		V_0 = L_0;
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&m859_MI, __this);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		V_0 = 3;
	}

IL_001f:
	{
		VirtActionInvoker2< int32_t, bool >::Invoke(&m866_MI, __this, V_0, p0);
		return;
	}
}
extern MethodInfo m884_MI;
 void m884 (t139 * __this, t6 * p0, MethodInfo* method){
	t210  V_0 = {0};
	{
		int32_t L_0 = m230(p0, &m230_MI);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m859_MI, __this);
		if (!L_1)
		{
			goto IL_003b;
		}
	}
	{
		t210  L_2 = m834(__this, &m834_MI);
		V_0 = L_2;
		int32_t L_3 = m708((&V_0), &m708_MI);
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t50_TI));
		t50 * L_4 = m75(NULL, &m75_MI);
		t16 * L_5 = m1392(__this, &m1392_MI);
		m88(L_4, L_5, p0, &m88_MI);
	}

IL_003b:
	{
		m851(__this, 1, &m851_MI);
		m882(__this, p0, &m882_MI);
		return;
	}
}
extern MethodInfo m885_MI;
 void m885 (t139 * __this, t6 * p0, MethodInfo* method){
	{
		int32_t L_0 = m230(p0, &m230_MI);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		m851(__this, 0, &m851_MI);
		m882(__this, p0, &m882_MI);
		return;
	}
}
extern MethodInfo m886_MI;
 void m886 (t139 * __this, t6 * p0, MethodInfo* method){
	{
		m849(__this, 1, &m849_MI);
		m882(__this, p0, &m882_MI);
		return;
	}
}
extern MethodInfo m887_MI;
 void m887 (t139 * __this, t6 * p0, MethodInfo* method){
	{
		m849(__this, 0, &m849_MI);
		m882(__this, p0, &m882_MI);
		return;
	}
}
extern MethodInfo m888_MI;
 void m888 (t139 * __this, t53 * p0, MethodInfo* method){
	{
		m853(__this, 1, &m853_MI);
		m882(__this, p0, &m882_MI);
		return;
	}
}
extern MethodInfo m889_MI;
 void m889 (t139 * __this, t53 * p0, MethodInfo* method){
	{
		m853(__this, 0, &m853_MI);
		m882(__this, p0, &m882_MI);
		return;
	}
}
extern MethodInfo m890_MI;
 void m890 (t139 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t50_TI));
		t50 * L_0 = m75(NULL, &m75_MI);
		bool L_1 = m87(L_0, &m87_MI);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t50_TI));
		t50 * L_2 = m75(NULL, &m75_MI);
		t16 * L_3 = m1392(__this, &m1392_MI);
		m90(L_2, L_3, &m90_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.Selectable
extern Il2CppType t226_0_0_17;
FieldInfo t139_f2_FieldInfo = 
{
	"s_List", &t226_0_0_17, &t139_TI, offsetof(t139_SFs, f2), &EmptyCustomAttributesCache};
extern Il2CppType t210_0_0_1;
extern CustomAttributesCache t139__CustomAttributeCache_m_Navigation;
FieldInfo t139_f3_FieldInfo = 
{
	"m_Navigation", &t210_0_0_1, &t139_TI, offsetof(t139, f3), &t139__CustomAttributeCache_m_Navigation};
extern Il2CppType t225_0_0_1;
extern CustomAttributesCache t139__CustomAttributeCache_m_Transition;
FieldInfo t139_f4_FieldInfo = 
{
	"m_Transition", &t225_0_0_1, &t139_TI, offsetof(t139, f4), &t139__CustomAttributeCache_m_Transition};
extern Il2CppType t146_0_0_1;
extern CustomAttributesCache t139__CustomAttributeCache_m_Colors;
FieldInfo t139_f5_FieldInfo = 
{
	"m_Colors", &t146_0_0_1, &t139_TI, offsetof(t139, f5), &t139__CustomAttributeCache_m_Colors};
extern Il2CppType t228_0_0_1;
extern CustomAttributesCache t139__CustomAttributeCache_m_SpriteState;
FieldInfo t139_f6_FieldInfo = 
{
	"m_SpriteState", &t228_0_0_1, &t139_TI, offsetof(t139, f6), &t139__CustomAttributeCache_m_SpriteState};
extern Il2CppType t137_0_0_1;
extern CustomAttributesCache t139__CustomAttributeCache_m_AnimationTriggers;
FieldInfo t139_f7_FieldInfo = 
{
	"m_AnimationTriggers", &t137_0_0_1, &t139_TI, offsetof(t139, f7), &t139__CustomAttributeCache_m_AnimationTriggers};
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t139__CustomAttributeCache_m_Interactable;
FieldInfo t139_f8_FieldInfo = 
{
	"m_Interactable", &t40_0_0_1, &t139_TI, offsetof(t139, f8), &t139__CustomAttributeCache_m_Interactable};
extern Il2CppType t155_0_0_1;
extern CustomAttributesCache t139__CustomAttributeCache_m_TargetGraphic;
FieldInfo t139_f9_FieldInfo = 
{
	"m_TargetGraphic", &t155_0_0_1, &t139_TI, offsetof(t139, f9), &t139__CustomAttributeCache_m_TargetGraphic};
extern Il2CppType t40_0_0_1;
FieldInfo t139_f10_FieldInfo = 
{
	"m_GroupsAllowInteraction", &t40_0_0_1, &t139_TI, offsetof(t139, f10), &EmptyCustomAttributesCache};
extern Il2CppType t207_0_0_1;
FieldInfo t139_f11_FieldInfo = 
{
	"m_CurrentSelectionState", &t207_0_0_1, &t139_TI, offsetof(t139, f11), &EmptyCustomAttributesCache};
extern Il2CppType t227_0_0_33;
FieldInfo t139_f12_FieldInfo = 
{
	"m_CanvasGroupCache", &t227_0_0_33, &t139_TI, offsetof(t139, f12), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t139__CustomAttributeCache_U3CisPointerInsideU3Ek__BackingField;
FieldInfo t139_f13_FieldInfo = 
{
	"<isPointerInside>k__BackingField", &t40_0_0_1, &t139_TI, offsetof(t139, f13), &t139__CustomAttributeCache_U3CisPointerInsideU3Ek__BackingField};
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t139__CustomAttributeCache_U3CisPointerDownU3Ek__BackingField;
FieldInfo t139_f14_FieldInfo = 
{
	"<isPointerDown>k__BackingField", &t40_0_0_1, &t139_TI, offsetof(t139, f14), &t139__CustomAttributeCache_U3CisPointerDownU3Ek__BackingField};
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t139__CustomAttributeCache_U3ChasSelectionU3Ek__BackingField;
FieldInfo t139_f15_FieldInfo = 
{
	"<hasSelection>k__BackingField", &t40_0_0_1, &t139_TI, offsetof(t139, f15), &t139__CustomAttributeCache_U3ChasSelectionU3Ek__BackingField};
static FieldInfo* t139_FIs[] =
{
	&t139_f2_FieldInfo,
	&t139_f3_FieldInfo,
	&t139_f4_FieldInfo,
	&t139_f5_FieldInfo,
	&t139_f6_FieldInfo,
	&t139_f7_FieldInfo,
	&t139_f8_FieldInfo,
	&t139_f9_FieldInfo,
	&t139_f10_FieldInfo,
	&t139_f11_FieldInfo,
	&t139_f12_FieldInfo,
	&t139_f13_FieldInfo,
	&t139_f14_FieldInfo,
	&t139_f15_FieldInfo,
	NULL
};
static PropertyInfo t139____allSelectables_PropertyInfo = 
{
	&t139_TI, "allSelectables", &m833_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____navigation_PropertyInfo = 
{
	&t139_TI, "navigation", &m834_MI, &m835_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____transition_PropertyInfo = 
{
	&t139_TI, "transition", &m836_MI, &m837_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____colors_PropertyInfo = 
{
	&t139_TI, "colors", &m838_MI, &m839_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____spriteState_PropertyInfo = 
{
	&t139_TI, "spriteState", &m840_MI, &m841_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____animationTriggers_PropertyInfo = 
{
	&t139_TI, "animationTriggers", &m842_MI, &m843_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____targetGraphic_PropertyInfo = 
{
	&t139_TI, "targetGraphic", &m844_MI, &m845_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____interactable_PropertyInfo = 
{
	&t139_TI, "interactable", &m846_MI, &m847_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____isPointerInside_PropertyInfo = 
{
	&t139_TI, "isPointerInside", &m848_MI, &m849_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____isPointerDown_PropertyInfo = 
{
	&t139_TI, "isPointerDown", &m850_MI, &m851_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____hasSelection_PropertyInfo = 
{
	&t139_TI, "hasSelection", &m852_MI, &m853_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____image_PropertyInfo = 
{
	&t139_TI, "image", &m854_MI, &m855_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____animator_PropertyInfo = 
{
	&t139_TI, "animator", &m856_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t139____currentSelectionState_PropertyInfo = 
{
	&t139_TI, "currentSelectionState", &m864_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t139_PIs[] =
{
	&t139____allSelectables_PropertyInfo,
	&t139____navigation_PropertyInfo,
	&t139____transition_PropertyInfo,
	&t139____colors_PropertyInfo,
	&t139____spriteState_PropertyInfo,
	&t139____animationTriggers_PropertyInfo,
	&t139____targetGraphic_PropertyInfo,
	&t139____interactable_PropertyInfo,
	&t139____isPointerInside_PropertyInfo,
	&t139____isPointerDown_PropertyInfo,
	&t139____hasSelection_PropertyInfo,
	&t139____image_PropertyInfo,
	&t139____animator_PropertyInfo,
	&t139____currentSelectionState_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m831_MI = 
{
	".ctor", (methodPointerType)&m831, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 811, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m832_MI = 
{
	".cctor", (methodPointerType)&m832, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 812, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t226_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m833_MI = 
{
	"get_allSelectables", (methodPointerType)&m833, &t139_TI, &t226_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, false, 813, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t210_0_0_0;
extern void* RuntimeInvoker_t210 (MethodInfo* method, void* obj, void** args);
MethodInfo m834_MI = 
{
	"get_navigation", (methodPointerType)&m834, &t139_TI, &t210_0_0_0, RuntimeInvoker_t210, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 814, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t210_0_0_0;
extern Il2CppType t210_0_0_0;
static ParameterInfo t139_m835_ParameterInfos[] = 
{
	{"value", 0, 134218219, &EmptyCustomAttributesCache, &t210_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t210 (MethodInfo* method, void* obj, void** args);
MethodInfo m835_MI = 
{
	"set_navigation", (methodPointerType)&m835, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t210, t139_m835_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 815, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t225_0_0_0;
extern void* RuntimeInvoker_t225 (MethodInfo* method, void* obj, void** args);
MethodInfo m836_MI = 
{
	"get_transition", (methodPointerType)&m836, &t139_TI, &t225_0_0_0, RuntimeInvoker_t225, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 816, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t225_0_0_0;
static ParameterInfo t139_m837_ParameterInfos[] = 
{
	{"value", 0, 134218220, &EmptyCustomAttributesCache, &t225_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m837_MI = 
{
	"set_transition", (methodPointerType)&m837, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t139_m837_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 817, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t146_0_0_0;
extern void* RuntimeInvoker_t146 (MethodInfo* method, void* obj, void** args);
MethodInfo m838_MI = 
{
	"get_colors", (methodPointerType)&m838, &t139_TI, &t146_0_0_0, RuntimeInvoker_t146, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 818, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t146_0_0_0;
extern Il2CppType t146_0_0_0;
static ParameterInfo t139_m839_ParameterInfos[] = 
{
	{"value", 0, 134218221, &EmptyCustomAttributesCache, &t146_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t146 (MethodInfo* method, void* obj, void** args);
MethodInfo m839_MI = 
{
	"set_colors", (methodPointerType)&m839, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t146, t139_m839_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 819, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t228_0_0_0;
extern void* RuntimeInvoker_t228 (MethodInfo* method, void* obj, void** args);
MethodInfo m840_MI = 
{
	"get_spriteState", (methodPointerType)&m840, &t139_TI, &t228_0_0_0, RuntimeInvoker_t228, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 820, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t228_0_0_0;
extern Il2CppType t228_0_0_0;
static ParameterInfo t139_m841_ParameterInfos[] = 
{
	{"value", 0, 134218222, &EmptyCustomAttributesCache, &t228_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t228 (MethodInfo* method, void* obj, void** args);
MethodInfo m841_MI = 
{
	"set_spriteState", (methodPointerType)&m841, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t228, t139_m841_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 821, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t137_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m842_MI = 
{
	"get_animationTriggers", (methodPointerType)&m842, &t139_TI, &t137_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 822, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t137_0_0_0;
extern Il2CppType t137_0_0_0;
static ParameterInfo t139_m843_ParameterInfos[] = 
{
	{"value", 0, 134218223, &EmptyCustomAttributesCache, &t137_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m843_MI = 
{
	"set_animationTriggers", (methodPointerType)&m843, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m843_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 823, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m844_MI = 
{
	"get_targetGraphic", (methodPointerType)&m844, &t139_TI, &t155_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 824, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t155_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t139_m845_ParameterInfos[] = 
{
	{"value", 0, 134218224, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m845_MI = 
{
	"set_targetGraphic", (methodPointerType)&m845, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m845_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 825, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m846_MI = 
{
	"get_interactable", (methodPointerType)&m846, &t139_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 826, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t139_m847_ParameterInfos[] = 
{
	{"value", 0, 134218225, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m847_MI = 
{
	"set_interactable", (methodPointerType)&m847, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t139_m847_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 827, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t139__CustomAttributeCache_m848;
MethodInfo m848_MI = 
{
	"get_isPointerInside", (methodPointerType)&m848, &t139_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &t139__CustomAttributeCache_m848, 2177, 0, 255, 0, false, false, 828, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t139_m849_ParameterInfos[] = 
{
	{"value", 0, 134218226, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t139__CustomAttributeCache_m849;
MethodInfo m849_MI = 
{
	"set_isPointerInside", (methodPointerType)&m849, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t139_m849_ParameterInfos, &t139__CustomAttributeCache_m849, 2177, 0, 255, 1, false, false, 829, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t139__CustomAttributeCache_m850;
MethodInfo m850_MI = 
{
	"get_isPointerDown", (methodPointerType)&m850, &t139_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &t139__CustomAttributeCache_m850, 2177, 0, 255, 0, false, false, 830, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t139_m851_ParameterInfos[] = 
{
	{"value", 0, 134218227, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t139__CustomAttributeCache_m851;
MethodInfo m851_MI = 
{
	"set_isPointerDown", (methodPointerType)&m851, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t139_m851_ParameterInfos, &t139__CustomAttributeCache_m851, 2177, 0, 255, 1, false, false, 831, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t139__CustomAttributeCache_m852;
MethodInfo m852_MI = 
{
	"get_hasSelection", (methodPointerType)&m852, &t139_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &t139__CustomAttributeCache_m852, 2177, 0, 255, 0, false, false, 832, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t139_m853_ParameterInfos[] = 
{
	{"value", 0, 134218228, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t139__CustomAttributeCache_m853;
MethodInfo m853_MI = 
{
	"set_hasSelection", (methodPointerType)&m853, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t139_m853_ParameterInfos, &t139__CustomAttributeCache_m853, 2177, 0, 255, 1, false, false, 833, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t179_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m854_MI = 
{
	"get_image", (methodPointerType)&m854, &t139_TI, &t179_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 834, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t179_0_0_0;
extern Il2CppType t179_0_0_0;
static ParameterInfo t139_m855_ParameterInfos[] = 
{
	{"value", 0, 134218229, &EmptyCustomAttributesCache, &t179_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m855_MI = 
{
	"set_image", (methodPointerType)&m855, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m855_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 835, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t229_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m856_MI = 
{
	"get_animator", (methodPointerType)&m856, &t139_TI, &t229_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 836, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m857_MI = 
{
	"Awake", (methodPointerType)&m857, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 4, 0, false, false, 837, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m858_MI = 
{
	"OnCanvasGroupChanged", (methodPointerType)&m858, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 14, 0, false, false, 838, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m859_MI = 
{
	"IsInteractable", (methodPointerType)&m859, &t139_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 454, 0, 22, 0, false, false, 839, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m860_MI = 
{
	"OnDidApplyAnimationProperties", (methodPointerType)&m860, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 13, 0, false, false, 840, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m861_MI = 
{
	"OnEnable", (methodPointerType)&m861, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 5, 0, false, false, 841, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m862_MI = 
{
	"OnSetProperty", (methodPointerType)&m862, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 842, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m863_MI = 
{
	"OnDisable", (methodPointerType)&m863, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 7, 0, false, false, 843, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t207_0_0_0;
extern void* RuntimeInvoker_t207 (MethodInfo* method, void* obj, void** args);
MethodInfo m864_MI = 
{
	"get_currentSelectionState", (methodPointerType)&m864, &t139_TI, &t207_0_0_0, RuntimeInvoker_t207, NULL, &EmptyCustomAttributesCache, 2180, 0, 255, 0, false, false, 844, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m865_MI = 
{
	"InstantClearState", (methodPointerType)&m865, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 23, 0, false, false, 845, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t207_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t139_m866_ParameterInfos[] = 
{
	{"state", 0, 134218230, &EmptyCustomAttributesCache, &t207_0_0_0},
	{"instant", 1, 134218231, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m866_MI = 
{
	"DoStateTransition", (methodPointerType)&m866, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297, t139_m866_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 24, 2, false, false, 846, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t23_0_0_0;
extern Il2CppType t23_0_0_0;
static ParameterInfo t139_m867_ParameterInfos[] = 
{
	{"dir", 0, 134218232, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29_t23 (MethodInfo* method, void* obj, void** args);
MethodInfo m867_MI = 
{
	"FindSelectable", (methodPointerType)&m867, &t139_TI, &t139_0_0_0, RuntimeInvoker_t29_t23, t139_m867_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 847, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t17_0_0_0;
static ParameterInfo t139_m868_ParameterInfos[] = 
{
	{"rect", 0, 134218233, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"dir", 1, 134218234, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23_t29_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m868_MI = 
{
	"GetPointOnRectEdge", (methodPointerType)&m868, &t139_TI, &t23_0_0_0, RuntimeInvoker_t23_t29_t17, t139_m868_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 2, false, false, 848, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t64_0_0_0;
extern Il2CppType t64_0_0_0;
extern Il2CppType t139_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t139_m869_ParameterInfos[] = 
{
	{"eventData", 0, 134218235, &EmptyCustomAttributesCache, &t64_0_0_0},
	{"sel", 1, 134218236, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m869_MI = 
{
	"Navigate", (methodPointerType)&m869, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t139_m869_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 849, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m870_MI = 
{
	"FindSelectableOnLeft", (methodPointerType)&m870, &t139_TI, &t139_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 454, 0, 25, 0, false, false, 850, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m871_MI = 
{
	"FindSelectableOnRight", (methodPointerType)&m871, &t139_TI, &t139_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 454, 0, 26, 0, false, false, 851, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m872_MI = 
{
	"FindSelectableOnUp", (methodPointerType)&m872, &t139_TI, &t139_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 454, 0, 27, 0, false, false, 852, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m873_MI = 
{
	"FindSelectableOnDown", (methodPointerType)&m873, &t139_TI, &t139_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 454, 0, 28, 0, false, false, 853, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t64_0_0_0;
static ParameterInfo t139_m874_ParameterInfos[] = 
{
	{"eventData", 0, 134218237, &EmptyCustomAttributesCache, &t64_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m874_MI = 
{
	"OnMove", (methodPointerType)&m874, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m874_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 29, 1, false, false, 854, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t132_0_0_0;
extern Il2CppType t132_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t139_m875_ParameterInfos[] = 
{
	{"targetColor", 0, 134218238, &EmptyCustomAttributesCache, &t132_0_0_0},
	{"instant", 1, 134218239, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t132_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m875_MI = 
{
	"StartColorTween", (methodPointerType)&m875, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t132_t297, t139_m875_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 855, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t180_0_0_0;
extern Il2CppType t180_0_0_0;
static ParameterInfo t139_m876_ParameterInfos[] = 
{
	{"newSprite", 0, 134218240, &EmptyCustomAttributesCache, &t180_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m876_MI = 
{
	"DoSpriteSwap", (methodPointerType)&m876, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m876_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 856, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
static ParameterInfo t139_m877_ParameterInfos[] = 
{
	{"triggername", 0, 134218241, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m877_MI = 
{
	"TriggerAnimation", (methodPointerType)&m877, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m877_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 857, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t53_0_0_0;
extern Il2CppType t53_0_0_0;
static ParameterInfo t139_m878_ParameterInfos[] = 
{
	{"eventData", 0, 134218242, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m878_MI = 
{
	"IsHighlighted", (methodPointerType)&m878, &t139_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t139_m878_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 1, false, false, 858, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t53_0_0_0;
static ParameterInfo t139_m879_ParameterInfos[] = 
{
	{"eventData", 0, 134218243, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t139__CustomAttributeCache_m879;
MethodInfo m879_MI = 
{
	"IsPressed", (methodPointerType)&m879, &t139_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t139_m879_ParameterInfos, &t139__CustomAttributeCache_m879, 132, 0, 255, 1, false, false, 859, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m880_MI = 
{
	"IsPressed", (methodPointerType)&m880, &t139_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 132, 0, 255, 0, false, false, 860, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t53_0_0_0;
static ParameterInfo t139_m881_ParameterInfos[] = 
{
	{"eventData", 0, 134218244, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m881_MI = 
{
	"UpdateSelectionState", (methodPointerType)&m881, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m881_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 1, false, false, 861, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t53_0_0_0;
static ParameterInfo t139_m882_ParameterInfos[] = 
{
	{"eventData", 0, 134218245, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m882_MI = 
{
	"EvaluateAndTransitionToSelectionState", (methodPointerType)&m882, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m882_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 862, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t139_m883_ParameterInfos[] = 
{
	{"instant", 0, 134218246, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m883_MI = 
{
	"InternalEvaluateAndTransitionToSelectionState", (methodPointerType)&m883, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t139_m883_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 863, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t139_m884_ParameterInfos[] = 
{
	{"eventData", 0, 134218247, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m884_MI = 
{
	"OnPointerDown", (methodPointerType)&m884, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m884_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 30, 1, false, false, 864, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t139_m885_ParameterInfos[] = 
{
	{"eventData", 0, 134218248, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m885_MI = 
{
	"OnPointerUp", (methodPointerType)&m885, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m885_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 31, 1, false, false, 865, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t139_m886_ParameterInfos[] = 
{
	{"eventData", 0, 134218249, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m886_MI = 
{
	"OnPointerEnter", (methodPointerType)&m886, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m886_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 32, 1, false, false, 866, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t139_m887_ParameterInfos[] = 
{
	{"eventData", 0, 134218250, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m887_MI = 
{
	"OnPointerExit", (methodPointerType)&m887, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m887_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 33, 1, false, false, 867, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t53_0_0_0;
static ParameterInfo t139_m888_ParameterInfos[] = 
{
	{"eventData", 0, 134218251, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m888_MI = 
{
	"OnSelect", (methodPointerType)&m888, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m888_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 34, 1, false, false, 868, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t53_0_0_0;
static ParameterInfo t139_m889_ParameterInfos[] = 
{
	{"eventData", 0, 134218252, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m889_MI = 
{
	"OnDeselect", (methodPointerType)&m889, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t139_m889_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 35, 1, false, false, 869, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m890_MI = 
{
	"Select", (methodPointerType)&m890, &t139_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 36, 0, false, false, 870, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t139_MIs[] =
{
	&m831_MI,
	&m832_MI,
	&m833_MI,
	&m834_MI,
	&m835_MI,
	&m836_MI,
	&m837_MI,
	&m838_MI,
	&m839_MI,
	&m840_MI,
	&m841_MI,
	&m842_MI,
	&m843_MI,
	&m844_MI,
	&m845_MI,
	&m846_MI,
	&m847_MI,
	&m848_MI,
	&m849_MI,
	&m850_MI,
	&m851_MI,
	&m852_MI,
	&m853_MI,
	&m854_MI,
	&m855_MI,
	&m856_MI,
	&m857_MI,
	&m858_MI,
	&m859_MI,
	&m860_MI,
	&m861_MI,
	&m862_MI,
	&m863_MI,
	&m864_MI,
	&m865_MI,
	&m866_MI,
	&m867_MI,
	&m868_MI,
	&m869_MI,
	&m870_MI,
	&m871_MI,
	&m872_MI,
	&m873_MI,
	&m874_MI,
	&m875_MI,
	&m876_MI,
	&m877_MI,
	&m878_MI,
	&m879_MI,
	&m880_MI,
	&m881_MI,
	&m882_MI,
	&m883_MI,
	&m884_MI,
	&m885_MI,
	&m886_MI,
	&m887_MI,
	&m888_MI,
	&m889_MI,
	&m890_MI,
	NULL
};
extern TypeInfo t225_TI;
extern TypeInfo t207_TI;
static TypeInfo* t139_TI__nestedTypes[3] =
{
	&t225_TI,
	&t207_TI,
	NULL
};
static MethodInfo* t139_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m857_MI,
	&m861_MI,
	&m171_MI,
	&m863_MI,
	&m173_MI,
	&m174_MI,
	&m175_MI,
	&m176_MI,
	&m177_MI,
	&m860_MI,
	&m858_MI,
	&m886_MI,
	&m887_MI,
	&m884_MI,
	&m885_MI,
	&m888_MI,
	&m889_MI,
	&m874_MI,
	&m859_MI,
	&m865_MI,
	&m866_MI,
	&m870_MI,
	&m871_MI,
	&m872_MI,
	&m873_MI,
	&m874_MI,
	&m884_MI,
	&m885_MI,
	&m886_MI,
	&m887_MI,
	&m888_MI,
	&m889_MI,
	&m890_MI,
};
extern TypeInfo t30_TI;
extern TypeInfo t32_TI;
extern TypeInfo t89_TI;
extern TypeInfo t90_TI;
extern TypeInfo t99_TI;
extern TypeInfo t100_TI;
extern TypeInfo t101_TI;
static TypeInfo* t139_ITIs[] = 
{
	&t31_TI,
	&t30_TI,
	&t32_TI,
	&t89_TI,
	&t90_TI,
	&t99_TI,
	&t100_TI,
	&t101_TI,
};
static Il2CppInterfaceOffsetPair t139_IOs[] = 
{
	{ &t31_TI, 15},
	{ &t30_TI, 15},
	{ &t32_TI, 16},
	{ &t89_TI, 17},
	{ &t90_TI, 18},
	{ &t99_TI, 19},
	{ &t100_TI, 20},
	{ &t101_TI, 21},
};
extern TypeInfo t359_TI;
#include "t359.h"
#include "t359MD.h"
extern MethodInfo m1603_MI;
void t139_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t359 * tmp;
		tmp = (t359 *)il2cpp_codegen_object_new (&t359_TI);
		m1603(tmp, &m1603_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t360 * tmp;
		tmp = (t360 *)il2cpp_codegen_object_new (&t360_TI);
		m1604(tmp, &m1604_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t397 * tmp;
		tmp = (t397 *)il2cpp_codegen_object_new (&t397_TI);
		m1866(tmp, &m1866_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("UI/Selectable"), 70, &m1519_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t299_TI;
#include "t299.h"
#include "t299MD.h"
extern MethodInfo m1318_MI;
void t139_CustomAttributesCacheGenerator_m_Navigation(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("navigation"), &m1318_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_m_Transition(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("transition"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_m_Colors(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("colors"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_m_SpriteState(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("spriteState"), &m1318_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_m_AnimationTriggers(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("animationTriggers"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t399_TI;
#include "t399.h"
#include "t399MD.h"
extern MethodInfo m1897_MI;
void t139_CustomAttributesCacheGenerator_m_Interactable(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t399 * tmp;
		tmp = (t399 *)il2cpp_codegen_object_new (&t399_TI);
		m1897(tmp, il2cpp_codegen_string_new_wrapper("Can the Selectable be interacted with?"), &m1897_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_m_TargetGraphic(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("m_HighlightGraphic"), &m1318_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("highlightGraphic"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t38_TI;
#include "t38.h"
#include "t38MD.h"
extern MethodInfo m55_MI;
void t139_CustomAttributesCacheGenerator_U3CisPointerInsideU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_U3CisPointerDownU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_U3ChasSelectionU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_m848(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_m849(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_m850(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_m851(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_m852(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t139_CustomAttributesCacheGenerator_m853(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t327_TI;
#include "t327.h"
#include "t327MD.h"
extern MethodInfo m1438_MI;
void t139_CustomAttributesCacheGenerator_m879(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t327 * tmp;
		tmp = (t327 *)il2cpp_codegen_object_new (&t327_TI);
		m1438(tmp, il2cpp_codegen_string_new_wrapper("Is Pressed no longer requires eventData"), false, &m1438_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t139__CustomAttributeCache = {
4,
NULL,
&t139_CustomAttributesCacheGenerator
};
CustomAttributesCache t139__CustomAttributeCache_m_Navigation = {
2,
NULL,
&t139_CustomAttributesCacheGenerator_m_Navigation
};
CustomAttributesCache t139__CustomAttributeCache_m_Transition = {
2,
NULL,
&t139_CustomAttributesCacheGenerator_m_Transition
};
CustomAttributesCache t139__CustomAttributeCache_m_Colors = {
2,
NULL,
&t139_CustomAttributesCacheGenerator_m_Colors
};
CustomAttributesCache t139__CustomAttributeCache_m_SpriteState = {
2,
NULL,
&t139_CustomAttributesCacheGenerator_m_SpriteState
};
CustomAttributesCache t139__CustomAttributeCache_m_AnimationTriggers = {
2,
NULL,
&t139_CustomAttributesCacheGenerator_m_AnimationTriggers
};
CustomAttributesCache t139__CustomAttributeCache_m_Interactable = {
2,
NULL,
&t139_CustomAttributesCacheGenerator_m_Interactable
};
CustomAttributesCache t139__CustomAttributeCache_m_TargetGraphic = {
3,
NULL,
&t139_CustomAttributesCacheGenerator_m_TargetGraphic
};
CustomAttributesCache t139__CustomAttributeCache_U3CisPointerInsideU3Ek__BackingField = {
1,
NULL,
&t139_CustomAttributesCacheGenerator_U3CisPointerInsideU3Ek__BackingField
};
CustomAttributesCache t139__CustomAttributeCache_U3CisPointerDownU3Ek__BackingField = {
1,
NULL,
&t139_CustomAttributesCacheGenerator_U3CisPointerDownU3Ek__BackingField
};
CustomAttributesCache t139__CustomAttributeCache_U3ChasSelectionU3Ek__BackingField = {
1,
NULL,
&t139_CustomAttributesCacheGenerator_U3ChasSelectionU3Ek__BackingField
};
CustomAttributesCache t139__CustomAttributeCache_m848 = {
1,
NULL,
&t139_CustomAttributesCacheGenerator_m848
};
CustomAttributesCache t139__CustomAttributeCache_m849 = {
1,
NULL,
&t139_CustomAttributesCacheGenerator_m849
};
CustomAttributesCache t139__CustomAttributeCache_m850 = {
1,
NULL,
&t139_CustomAttributesCacheGenerator_m850
};
CustomAttributesCache t139__CustomAttributeCache_m851 = {
1,
NULL,
&t139_CustomAttributesCacheGenerator_m851
};
CustomAttributesCache t139__CustomAttributeCache_m852 = {
1,
NULL,
&t139_CustomAttributesCacheGenerator_m852
};
CustomAttributesCache t139__CustomAttributeCache_m853 = {
1,
NULL,
&t139_CustomAttributesCacheGenerator_m853
};
CustomAttributesCache t139__CustomAttributeCache_m879 = {
1,
NULL,
&t139_CustomAttributesCacheGenerator_m879
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t139_1_0_0;
struct t139;
extern CustomAttributesCache t139__CustomAttributeCache;
extern CustomAttributesCache t139__CustomAttributeCache_m_Navigation;
extern CustomAttributesCache t139__CustomAttributeCache_m_Transition;
extern CustomAttributesCache t139__CustomAttributeCache_m_Colors;
extern CustomAttributesCache t139__CustomAttributeCache_m_SpriteState;
extern CustomAttributesCache t139__CustomAttributeCache_m_AnimationTriggers;
extern CustomAttributesCache t139__CustomAttributeCache_m_Interactable;
extern CustomAttributesCache t139__CustomAttributeCache_m_TargetGraphic;
extern CustomAttributesCache t139__CustomAttributeCache_U3CisPointerInsideU3Ek__BackingField;
extern CustomAttributesCache t139__CustomAttributeCache_U3CisPointerDownU3Ek__BackingField;
extern CustomAttributesCache t139__CustomAttributeCache_U3ChasSelectionU3Ek__BackingField;
extern CustomAttributesCache t139__CustomAttributeCache_m848;
extern CustomAttributesCache t139__CustomAttributeCache_m849;
extern CustomAttributesCache t139__CustomAttributeCache_m850;
extern CustomAttributesCache t139__CustomAttributeCache_m851;
extern CustomAttributesCache t139__CustomAttributeCache_m852;
extern CustomAttributesCache t139__CustomAttributeCache_m853;
extern CustomAttributesCache t139__CustomAttributeCache_m879;
TypeInfo t139_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Selectable", "UnityEngine.UI", t139_MIs, t139_PIs, t139_FIs, NULL, &t55_TI, t139_TI__nestedTypes, NULL, &t139_TI, t139_ITIs, t139_VT, &t139__CustomAttributeCache, &t139_TI, &t139_0_0_0, &t139_1_0_0, t139_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t139), 0, -1, sizeof(t139_SFs), 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, true, false, false, 60, 14, 14, 0, 2, 37, 8, 8};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t230_TI;

extern TypeInfo t132_TI;


extern MethodInfo m891_MI;
 bool m891 (t29 * __this, t132 * p0, t132  p1, MethodInfo* method){
	{
		float L_0 = (p0->f0);
		float L_1 = ((&p1)->f0);
		if ((((float)L_0) != ((float)L_1)))
		{
			goto IL_004a;
		}
	}
	{
		float L_2 = (p0->f1);
		float L_3 = ((&p1)->f1);
		if ((((float)L_2) != ((float)L_3)))
		{
			goto IL_004a;
		}
	}
	{
		float L_4 = (p0->f2);
		float L_5 = ((&p1)->f2);
		if ((((float)L_4) != ((float)L_5)))
		{
			goto IL_004a;
		}
	}
	{
		float L_6 = (p0->f3);
		float L_7 = ((&p1)->f3);
		if ((((float)L_6) != ((float)L_7)))
		{
			goto IL_004a;
		}
	}
	{
		return 0;
	}

IL_004a:
	{
		*p0 = p1;
		return 1;
	}
}
// Metadata Definition UnityEngine.UI.SetPropertyUtility
extern Il2CppType t132_1_0_0;
extern Il2CppType t132_1_0_0;
extern Il2CppType t132_0_0_0;
static ParameterInfo t230_m891_ParameterInfos[] = 
{
	{"currentValue", 0, 134218253, &EmptyCustomAttributesCache, &t132_1_0_0},
	{"newValue", 1, 134218254, &EmptyCustomAttributesCache, &t132_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t400_t132 (MethodInfo* method, void* obj, void** args);
MethodInfo m891_MI = 
{
	"SetColor", (methodPointerType)&m891, &t230_TI, &t40_0_0_0, RuntimeInvoker_t40_t400_t132, t230_m891_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 871, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType m1898_gp_0_1_0_0;
extern Il2CppType m1898_gp_0_1_0_0;
extern Il2CppType m1898_gp_0_0_0_0;
extern Il2CppType m1898_gp_0_0_0_0;
static ParameterInfo t230_m1898_ParameterInfos[] = 
{
	{"currentValue", 0, 134218255, &EmptyCustomAttributesCache, &m1898_gp_0_1_0_0},
	{"newValue", 1, 134218256, &EmptyCustomAttributesCache, &m1898_gp_0_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericContainer m1898_IGC;
extern TypeInfo m1898_gp_T_0_TI;
extern Il2CppType t110_0_0_0;
static const Il2CppType* m1898_gp_T_0_TI_constraints[] = { 
&t110_0_0_0 , 
 NULL };
Il2CppGenericParamFull m1898_gp_T_0_TI_GenericParamFull = { { &m1898_IGC, 0}, {NULL, "T", 24, 0, m1898_gp_T_0_TI_constraints} };
static Il2CppGenericParamFull* m1898_IGPA[1] = 
{
	&m1898_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m1898_MI;
Il2CppGenericContainer m1898_IGC = { { NULL, NULL }, NULL, &m1898_MI, 1, 1, m1898_IGPA };
static Il2CppRGCTXDefinition m1898_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &m1898_gp_0_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m1898_MI = 
{
	"SetStruct", NULL, &t230_TI, &t40_0_0_0, NULL, t230_m1898_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, true, false, 872, m1898_RGCTXData, (methodPointerType)NULL, &m1898_IGC};
extern Il2CppType m1899_gp_0_1_0_0;
extern Il2CppType m1899_gp_0_1_0_0;
extern Il2CppType m1899_gp_0_0_0_0;
extern Il2CppType m1899_gp_0_0_0_0;
static ParameterInfo t230_m1899_ParameterInfos[] = 
{
	{"currentValue", 0, 134218257, &EmptyCustomAttributesCache, &m1899_gp_0_1_0_0},
	{"newValue", 1, 134218258, &EmptyCustomAttributesCache, &m1899_gp_0_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericContainer m1899_IGC;
extern TypeInfo m1899_gp_T_0_TI;
Il2CppGenericParamFull m1899_gp_T_0_TI_GenericParamFull = { { &m1899_IGC, 0}, {NULL, "T", 4, 0, NULL} };
static Il2CppGenericParamFull* m1899_IGPA[1] = 
{
	&m1899_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m1899_MI;
Il2CppGenericContainer m1899_IGC = { { NULL, NULL }, NULL, &m1899_MI, 1, 1, m1899_IGPA };
static Il2CppRGCTXDefinition m1899_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &m1899_gp_0_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m1899_MI = 
{
	"SetClass", NULL, &t230_TI, &t40_0_0_0, NULL, t230_m1899_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, true, false, 873, m1899_RGCTXData, (methodPointerType)NULL, &m1899_IGC};
static MethodInfo* t230_MIs[] =
{
	&m891_MI,
	&m1898_MI,
	&m1899_MI,
	NULL
};
extern MethodInfo m1332_MI;
static MethodInfo* t230_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t230_0_0_0;
extern Il2CppType t230_1_0_0;
extern TypeInfo t29_TI;
struct t230;
TypeInfo t230_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "SetPropertyUtility", "UnityEngine.UI", t230_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t230_TI, NULL, t230_VT, &EmptyCustomAttributesCache, &t230_TI, &t230_0_0_0, &t230_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t230), 0, -1, 0, 0, -1, 1048960, 0, false, false, false, false, false, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 4, 0, 0};
#include "t231.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t231_TI;
#include "t231MD.h"



// Metadata Definition UnityEngine.UI.Slider/Direction
extern Il2CppType t44_0_0_1542;
FieldInfo t231_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t231_TI, offsetof(t231, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t231_0_0_32854;
FieldInfo t231_f2_FieldInfo = 
{
	"LeftToRight", &t231_0_0_32854, &t231_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t231_0_0_32854;
FieldInfo t231_f3_FieldInfo = 
{
	"RightToLeft", &t231_0_0_32854, &t231_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t231_0_0_32854;
FieldInfo t231_f4_FieldInfo = 
{
	"BottomToTop", &t231_0_0_32854, &t231_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t231_0_0_32854;
FieldInfo t231_f5_FieldInfo = 
{
	"TopToBottom", &t231_0_0_32854, &t231_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t231_FIs[] =
{
	&t231_f1_FieldInfo,
	&t231_f2_FieldInfo,
	&t231_f3_FieldInfo,
	&t231_f4_FieldInfo,
	&t231_f5_FieldInfo,
	NULL
};
static const int32_t t231_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t231_f2_DefaultValue = 
{
	&t231_f2_FieldInfo, { (char*)&t231_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t231_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t231_f3_DefaultValue = 
{
	&t231_f3_FieldInfo, { (char*)&t231_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t231_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t231_f4_DefaultValue = 
{
	&t231_f4_FieldInfo, { (char*)&t231_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t231_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t231_f5_DefaultValue = 
{
	&t231_f5_FieldInfo, { (char*)&t231_f5_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t231_FDVs[] = 
{
	&t231_f2_DefaultValue,
	&t231_f3_DefaultValue,
	&t231_f4_DefaultValue,
	&t231_f5_DefaultValue,
	NULL
};
static MethodInfo* t231_MIs[] =
{
	NULL
};
static MethodInfo* t231_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t231_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t231_0_0_0;
extern Il2CppType t231_1_0_0;
extern TypeInfo t234_TI;
TypeInfo t231_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Direction", "", t231_MIs, NULL, t231_FIs, NULL, &t49_TI, NULL, &t234_TI, &t44_TI, NULL, t231_VT, &EmptyCustomAttributesCache, &t44_TI, &t231_0_0_0, &t231_1_0_0, t231_IOs, NULL, NULL, t231_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t231)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 258, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 23, 0, 3};
#include "t232.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t232_TI;
#include "t232MD.h"

extern MethodInfo m1824_MI;


extern MethodInfo m892_MI;
 void m892 (t232 * __this, MethodInfo* method){
	{
		m1824(__this, &m1824_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.Slider/SliderEvent
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m892_MI = 
{
	".ctor", (methodPointerType)&m892, &t232_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 917, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t232_MIs[] =
{
	&m892_MI,
	NULL
};
extern MethodInfo m1825_MI;
extern MethodInfo m1826_MI;
static MethodInfo* t232_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m1825_MI,
	&m1826_MI,
};
static Il2CppInterfaceOffsetPair t232_IOs[] = 
{
	{ &t301_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t232_0_0_0;
extern Il2CppType t232_1_0_0;
extern TypeInfo t214_TI;
struct t232;
TypeInfo t232_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "SliderEvent", "", t232_MIs, NULL, NULL, NULL, &t214_TI, NULL, &t234_TI, &t232_TI, NULL, t232_VT, &EmptyCustomAttributesCache, &t232_TI, &t232_0_0_0, &t232_1_0_0, t232_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t232), 0, -1, 0, 0, -1, 1056770, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 8, 0, 1};
#include "t233.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t233_TI;
#include "t233MD.h"



// Metadata Definition UnityEngine.UI.Slider/Axis
extern Il2CppType t44_0_0_1542;
FieldInfo t233_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t233_TI, offsetof(t233, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t233_0_0_32854;
FieldInfo t233_f2_FieldInfo = 
{
	"Horizontal", &t233_0_0_32854, &t233_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t233_0_0_32854;
FieldInfo t233_f3_FieldInfo = 
{
	"Vertical", &t233_0_0_32854, &t233_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t233_FIs[] =
{
	&t233_f1_FieldInfo,
	&t233_f2_FieldInfo,
	&t233_f3_FieldInfo,
	NULL
};
static const int32_t t233_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t233_f2_DefaultValue = 
{
	&t233_f2_FieldInfo, { (char*)&t233_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t233_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t233_f3_DefaultValue = 
{
	&t233_f3_FieldInfo, { (char*)&t233_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t233_FDVs[] = 
{
	&t233_f2_DefaultValue,
	&t233_f3_DefaultValue,
	NULL
};
static MethodInfo* t233_MIs[] =
{
	NULL
};
static MethodInfo* t233_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t233_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t233_0_0_0;
extern Il2CppType t233_1_0_0;
TypeInfo t233_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Axis", "", t233_MIs, NULL, t233_FIs, NULL, &t49_TI, NULL, &t234_TI, &t44_TI, NULL, t233_VT, &EmptyCustomAttributesCache, &t44_TI, &t233_0_0_0, &t233_1_0_0, t233_IOs, NULL, NULL, t233_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t233)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 259, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 3, 0, 0, 23, 0, 3};
#include "t234.h"
#ifndef _MSC_VER
#else
#endif
#include "t234MD.h"

#include "t218.h"
#include "t393.h"
#include "t172.h"
extern TypeInfo t64_TI;
#include "t218MD.h"
extern MethodInfo m1829_MI;
extern MethodInfo m916_MI;
extern MethodInfo m922_MI;
extern MethodInfo m1900_MI;
extern MethodInfo m1644_MI;
extern MethodInfo m917_MI;
extern MethodInfo m904_MI;
extern MethodInfo m64_MI;
extern MethodInfo m900_MI;
extern MethodInfo m902_MI;
extern MethodInfo m1442_MI;
extern MethodInfo m906_MI;
extern MethodInfo m1688_MI;
extern MethodInfo m1674_MI;
extern MethodInfo m907_MI;
extern MethodInfo m918_MI;
extern MethodInfo m1831_MI;
extern MethodInfo m1901_MI;
extern MethodInfo m60_MI;
extern MethodInfo m43_MI;
extern MethodInfo m1832_MI;
extern MethodInfo m1833_MI;
extern MethodInfo m1667_MI;
extern MethodInfo m513_MI;
extern MethodInfo m908_MI;
extern MethodInfo m522_MI;
extern MethodInfo m921_MI;
extern MethodInfo m920_MI;
extern MethodInfo m1796_MI;
extern MethodInfo m1663_MI;
extern MethodInfo m1669_MI;
extern MethodInfo m909_MI;
extern MethodInfo m924_MI;
extern MethodInfo m234_MI;
extern MethodInfo m1628_MI;
extern MethodInfo m923_MI;
extern MethodInfo m928_MI;
extern MethodInfo m912_MI;
extern MethodInfo m929_MI;
extern MethodInfo m930_MI;
extern MethodInfo m931_MI;
extern MethodInfo m227_MI;
extern MethodInfo m899_MI;
extern MethodInfo m1835_MI;
extern MethodInfo m1836_MI;
struct t230;
#define m1829(__this, p0, p1, method) (bool)m1690_gshared((t29 *)__this, (t29 **)p0, (t29 *)p1, method)
struct t230;
 bool m1900 (t29 * __this, int32_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t230;
 bool m1644 (t29 * __this, float* p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t28;
#define m1901(__this, method) (t179 *)m68_gshared((t28 *)__this, method)
struct t28;
#define m60(__this, method) (t2 *)m68_gshared((t28 *)__this, method)


extern MethodInfo m893_MI;
 void m893 (t234 * __this, MethodInfo* method){
	{
		__this->f20 = (1.0f);
		__this->f22 = (1.0f);
		t232 * L_0 = (t232 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t232_TI));
		m892(L_0, &m892_MI);
		__this->f23 = L_0;
		t17  L_1 = m1394(NULL, &m1394_MI);
		__this->f29 = L_1;
		m831(__this, &m831_MI);
		return;
	}
}
extern MethodInfo m894_MI;
 t2 * m894 (t234 * __this, MethodInfo* method){
	{
		t2 * L_0 = (__this->f16);
		return L_0;
	}
}
extern MethodInfo m895_MI;
 void m895 (t234 * __this, t2 * p0, MethodInfo* method){
	{
		t2 ** L_0 = &(__this->f16);
		bool L_1 = m1829(NULL, L_0, p0, &m1829_MI);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		m916(__this, &m916_MI);
		m922(__this, &m922_MI);
	}

IL_001d:
	{
		return;
	}
}
extern MethodInfo m896_MI;
 t2 * m896 (t234 * __this, MethodInfo* method){
	{
		t2 * L_0 = (__this->f17);
		return L_0;
	}
}
extern MethodInfo m897_MI;
 void m897 (t234 * __this, t2 * p0, MethodInfo* method){
	{
		t2 ** L_0 = &(__this->f17);
		bool L_1 = m1829(NULL, L_0, p0, &m1829_MI);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		m916(__this, &m916_MI);
		m922(__this, &m922_MI);
	}

IL_001d:
	{
		return;
	}
}
extern MethodInfo m898_MI;
 int32_t m898 (t234 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f18);
		return L_0;
	}
}
 void m899 (t234 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t* L_0 = &(__this->f18);
		bool L_1 = m1900(NULL, L_0, p0, &m1900_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m922(__this, &m922_MI);
	}

IL_0017:
	{
		return;
	}
}
 float m900 (t234 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f19);
		return L_0;
	}
}
extern MethodInfo m901_MI;
 void m901 (t234 * __this, float p0, MethodInfo* method){
	{
		float* L_0 = &(__this->f19);
		bool L_1 = m1644(NULL, L_0, p0, &m1644_MI);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		float L_2 = (__this->f22);
		m917(__this, L_2, &m917_MI);
		m922(__this, &m922_MI);
	}

IL_0023:
	{
		return;
	}
}
 float m902 (t234 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f20);
		return L_0;
	}
}
extern MethodInfo m903_MI;
 void m903 (t234 * __this, float p0, MethodInfo* method){
	{
		float* L_0 = &(__this->f20);
		bool L_1 = m1644(NULL, L_0, p0, &m1644_MI);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		float L_2 = (__this->f22);
		m917(__this, L_2, &m917_MI);
		m922(__this, &m922_MI);
	}

IL_0023:
	{
		return;
	}
}
 bool m904 (t234 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f21);
		return L_0;
	}
}
extern MethodInfo m905_MI;
 void m905 (t234 * __this, bool p0, MethodInfo* method){
	{
		bool* L_0 = &(__this->f21);
		bool L_1 = m1641(NULL, L_0, p0, &m1641_MI);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		float L_2 = (__this->f22);
		m917(__this, L_2, &m917_MI);
		m922(__this, &m922_MI);
	}

IL_0023:
	{
		return;
	}
}
 float m906 (t234 * __this, MethodInfo* method){
	{
		bool L_0 = m904(__this, &m904_MI);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		float L_1 = (__this->f22);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_2 = roundf(L_1);
		return L_2;
	}

IL_0017:
	{
		float L_3 = (__this->f22);
		return L_3;
	}
}
 void m907 (t234 * __this, float p0, MethodInfo* method){
	{
		m917(__this, p0, &m917_MI);
		return;
	}
}
 float m908 (t234 * __this, MethodInfo* method){
	{
		float L_0 = m900(__this, &m900_MI);
		float L_1 = m902(__this, &m902_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		bool L_2 = m1442(NULL, L_0, L_1, &m1442_MI);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		return (0.0f);
	}

IL_001c:
	{
		float L_3 = m900(__this, &m900_MI);
		float L_4 = m902(__this, &m902_MI);
		float L_5 = m906(__this, &m906_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_6 = m1688(NULL, L_3, L_4, L_5, &m1688_MI);
		return L_6;
	}
}
 void m909 (t234 * __this, float p0, MethodInfo* method){
	{
		float L_0 = m900(__this, &m900_MI);
		float L_1 = m902(__this, &m902_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_2 = m1674(NULL, L_0, L_1, p0, &m1674_MI);
		m907(__this, L_2, &m907_MI);
		return;
	}
}
extern MethodInfo m910_MI;
 t232 * m910 (t234 * __this, MethodInfo* method){
	{
		t232 * L_0 = (__this->f23);
		return L_0;
	}
}
extern MethodInfo m911_MI;
 void m911 (t234 * __this, t232 * p0, MethodInfo* method){
	{
		__this->f23 = p0;
		return;
	}
}
 float m912 (t234 * __this, MethodInfo* method){
	float G_B3_0 = 0.0f;
	{
		bool L_0 = m904(__this, &m904_MI);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		G_B3_0 = (1.0f);
		goto IL_0028;
	}

IL_0015:
	{
		float L_1 = m902(__this, &m902_MI);
		float L_2 = m900(__this, &m900_MI);
		G_B3_0 = ((float)((float)((float)(L_1-L_2))*(float)(0.1f)));
	}

IL_0028:
	{
		return G_B3_0;
	}
}
extern MethodInfo m913_MI;
 void m913 (t234 * __this, int32_t p0, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m914_MI;
 void m914 (t234 * __this, MethodInfo* method){
	{
		m861(__this, &m861_MI);
		m916(__this, &m916_MI);
		float L_0 = (__this->f22);
		m918(__this, L_0, 0, &m918_MI);
		m922(__this, &m922_MI);
		return;
	}
}
extern MethodInfo m915_MI;
 void m915 (t234 * __this, MethodInfo* method){
	{
		t218 * L_0 = &(__this->f30);
		m1831(L_0, &m1831_MI);
		m863(__this, &m863_MI);
		return;
	}
}
 void m916 (t234 * __this, MethodInfo* method){
	{
		t2 * L_0 = (__this->f16);
		bool L_1 = m1293(NULL, L_0, &m1293_MI);
		if (!L_1)
		{
			goto IL_0063;
		}
	}
	{
		t2 * L_2 = (__this->f16);
		t25 * L_3 = m39(L_2, &m39_MI);
		__this->f25 = L_3;
		t2 * L_4 = (__this->f16);
		t179 * L_5 = m1901(L_4, &m1901_MI);
		__this->f24 = L_5;
		t25 * L_6 = (__this->f25);
		t25 * L_7 = m1365(L_6, &m1365_MI);
		bool L_8 = m1300(NULL, L_7, (t41 *)NULL, &m1300_MI);
		if (!L_8)
		{
			goto IL_005e;
		}
	}
	{
		t25 * L_9 = (__this->f25);
		t25 * L_10 = m1365(L_9, &m1365_MI);
		t2 * L_11 = m60(L_10, &m60_MI);
		__this->f26 = L_11;
	}

IL_005e:
	{
		goto IL_0071;
	}

IL_0063:
	{
		__this->f26 = (t2 *)NULL;
		__this->f24 = (t179 *)NULL;
	}

IL_0071:
	{
		t2 * L_12 = (__this->f17);
		bool L_13 = m1293(NULL, L_12, &m1293_MI);
		if (!L_13)
		{
			goto IL_00c3;
		}
	}
	{
		t2 * L_14 = (__this->f17);
		t25 * L_15 = m39(L_14, &m39_MI);
		__this->f27 = L_15;
		t25 * L_16 = (__this->f27);
		t25 * L_17 = m1365(L_16, &m1365_MI);
		bool L_18 = m1300(NULL, L_17, (t41 *)NULL, &m1300_MI);
		if (!L_18)
		{
			goto IL_00be;
		}
	}
	{
		t25 * L_19 = (__this->f27);
		t25 * L_20 = m1365(L_19, &m1365_MI);
		t2 * L_21 = m60(L_20, &m60_MI);
		__this->f28 = L_21;
	}

IL_00be:
	{
		goto IL_00ca;
	}

IL_00c3:
	{
		__this->f28 = (t2 *)NULL;
	}

IL_00ca:
	{
		return;
	}
}
 void m917 (t234 * __this, float p0, MethodInfo* method){
	{
		m918(__this, p0, 1, &m918_MI);
		return;
	}
}
 void m918 (t234 * __this, float p0, bool p1, MethodInfo* method){
	float V_0 = 0.0f;
	{
		float L_0 = m900(__this, &m900_MI);
		float L_1 = m902(__this, &m902_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_2 = m43(NULL, p0, L_0, L_1, &m43_MI);
		V_0 = L_2;
		bool L_3 = m904(__this, &m904_MI);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_4 = roundf(V_0);
		V_0 = L_4;
	}

IL_0025:
	{
		float L_5 = (__this->f22);
		if ((((float)L_5) != ((float)V_0)))
		{
			goto IL_0032;
		}
	}
	{
		return;
	}

IL_0032:
	{
		__this->f22 = V_0;
		m922(__this, &m922_MI);
		if (!p1)
		{
			goto IL_0051;
		}
	}
	{
		t232 * L_6 = (__this->f23);
		m1832(L_6, V_0, &m1832_MI);
	}

IL_0051:
	{
		return;
	}
}
extern MethodInfo m919_MI;
 void m919 (t234 * __this, MethodInfo* method){
	{
		m175(__this, &m175_MI);
		m922(__this, &m922_MI);
		return;
	}
}
 int32_t m920 (t234 * __this, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = (__this->f18);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = (__this->f18);
		if ((((uint32_t)L_1) != ((uint32_t)1)))
		{
			goto IL_001d;
		}
	}

IL_0017:
	{
		G_B4_0 = 0;
		goto IL_001e;
	}

IL_001d:
	{
		G_B4_0 = 1;
	}

IL_001e:
	{
		return (int32_t)(G_B4_0);
	}
}
 bool m921 (t234 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = (__this->f18);
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = (__this->f18);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)3))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
	}

IL_0018:
	{
		return G_B3_0;
	}
}
 void m922 (t234 * __this, MethodInfo* method){
	t17  V_0 = {0};
	t17  V_1 = {0};
	t17  V_2 = {0};
	t17  V_3 = {0};
	float V_4 = 0.0f;
	int32_t G_B11_0 = {0};
	t17 * G_B11_1 = {0};
	int32_t G_B10_0 = {0};
	t17 * G_B10_1 = {0};
	float G_B12_0 = 0.0f;
	int32_t G_B12_1 = {0};
	t17 * G_B12_2 = {0};
	{
		t218 * L_0 = &(__this->f30);
		m1831(L_0, &m1831_MI);
		t2 * L_1 = (__this->f26);
		bool L_2 = m1300(NULL, L_1, (t41 *)NULL, &m1300_MI);
		if (!L_2)
		{
			goto IL_00cb;
		}
	}
	{
		t218 * L_3 = &(__this->f30);
		t2 * L_4 = (__this->f16);
		m1833(L_3, __this, L_4, ((int32_t)3840), &m1833_MI);
		t17  L_5 = m1394(NULL, &m1394_MI);
		V_0 = L_5;
		t17  L_6 = m1667(NULL, &m1667_MI);
		V_1 = L_6;
		t179 * L_7 = (__this->f24);
		bool L_8 = m1300(NULL, L_7, (t41 *)NULL, &m1300_MI);
		if (!L_8)
		{
			goto IL_0077;
		}
	}
	{
		t179 * L_9 = (__this->f24);
		int32_t L_10 = m513(L_9, &m513_MI);
		if ((((uint32_t)L_10) != ((uint32_t)3)))
		{
			goto IL_0077;
		}
	}
	{
		t179 * L_11 = (__this->f24);
		float L_12 = m908(__this, &m908_MI);
		m522(L_11, L_12, &m522_MI);
		goto IL_00b3;
	}

IL_0077:
	{
		bool L_13 = m921(__this, &m921_MI);
		if (!L_13)
		{
			goto IL_00a0;
		}
	}
	{
		int32_t L_14 = m920(__this, &m920_MI);
		float L_15 = m908(__this, &m908_MI);
		m1687((&V_0), L_14, ((float)((1.0f)-L_15)), &m1687_MI);
		goto IL_00b3;
	}

IL_00a0:
	{
		int32_t L_16 = m920(__this, &m920_MI);
		float L_17 = m908(__this, &m908_MI);
		m1687((&V_1), L_16, L_17, &m1687_MI);
	}

IL_00b3:
	{
		t2 * L_18 = (__this->f16);
		m1796(L_18, V_0, &m1796_MI);
		t2 * L_19 = (__this->f16);
		m1663(L_19, V_1, &m1663_MI);
	}

IL_00cb:
	{
		t2 * L_20 = (__this->f28);
		bool L_21 = m1300(NULL, L_20, (t41 *)NULL, &m1300_MI);
		if (!L_21)
		{
			goto IL_0159;
		}
	}
	{
		t218 * L_22 = &(__this->f30);
		t2 * L_23 = (__this->f17);
		m1833(L_22, __this, L_23, ((int32_t)3840), &m1833_MI);
		t17  L_24 = m1394(NULL, &m1394_MI);
		V_2 = L_24;
		t17  L_25 = m1667(NULL, &m1667_MI);
		V_3 = L_25;
		int32_t L_26 = m920(__this, &m920_MI);
		bool L_27 = m921(__this, &m921_MI);
		G_B10_0 = L_26;
		G_B10_1 = (&V_2);
		if (!L_27)
		{
			G_B11_0 = L_26;
			G_B11_1 = (&V_2);
			goto IL_0123;
		}
	}
	{
		float L_28 = m908(__this, &m908_MI);
		G_B12_0 = ((float)((1.0f)-L_28));
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_0129;
	}

IL_0123:
	{
		float L_29 = m908(__this, &m908_MI);
		G_B12_0 = L_29;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_0129:
	{
		V_4 = G_B12_0;
		int32_t L_30 = m920(__this, &m920_MI);
		m1687((&V_3), L_30, V_4, &m1687_MI);
		m1687(G_B12_2, G_B12_1, V_4, &m1687_MI);
		t2 * L_31 = (__this->f17);
		m1796(L_31, V_2, &m1796_MI);
		t2 * L_32 = (__this->f17);
		m1663(L_32, V_3, &m1663_MI);
	}

IL_0159:
	{
		return;
	}
}
 void m923 (t234 * __this, t6 * p0, t24 * p1, MethodInfo* method){
	t2 * V_0 = {0};
	t17  V_1 = {0};
	float V_2 = 0.0f;
	t164  V_3 = {0};
	t17  V_4 = {0};
	t164  V_5 = {0};
	t17  V_6 = {0};
	t164  V_7 = {0};
	t17  V_8 = {0};
	t2 * G_B2_0 = {0};
	t2 * G_B1_0 = {0};
	t234 * G_B8_0 = {0};
	t234 * G_B7_0 = {0};
	float G_B9_0 = 0.0f;
	t234 * G_B9_1 = {0};
	{
		t2 * L_0 = (__this->f28);
		t2 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0013;
		}
	}
	{
		t2 * L_2 = (__this->f26);
		G_B2_0 = L_2;
	}

IL_0013:
	{
		V_0 = G_B2_0;
		bool L_3 = m1300(NULL, V_0, (t41 *)NULL, &m1300_MI);
		if (!L_3)
		{
			goto IL_00d0;
		}
	}
	{
		t164  L_4 = m1572(V_0, &m1572_MI);
		V_3 = L_4;
		t17  L_5 = m1653((&V_3), &m1653_MI);
		V_4 = L_5;
		int32_t L_6 = m920(__this, &m920_MI);
		float L_7 = m1672((&V_4), L_6, &m1672_MI);
		if ((((float)L_7) <= ((float)(0.0f))))
		{
			goto IL_00d0;
		}
	}
	{
		t17  L_8 = m210(p0, &m210_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		bool L_9 = m1677(NULL, V_0, L_8, p1, (&V_1), &m1677_MI);
		if (L_9)
		{
			goto IL_005c;
		}
	}
	{
		return;
	}

IL_005c:
	{
		t164  L_10 = m1572(V_0, &m1572_MI);
		V_5 = L_10;
		t17  L_11 = m1669((&V_5), &m1669_MI);
		t17  L_12 = m1416(NULL, V_1, L_11, &m1416_MI);
		V_1 = L_12;
		t17  L_13 = (__this->f29);
		t17  L_14 = m1416(NULL, V_1, L_13, &m1416_MI);
		V_6 = L_14;
		int32_t L_15 = m920(__this, &m920_MI);
		float L_16 = m1672((&V_6), L_15, &m1672_MI);
		t164  L_17 = m1572(V_0, &m1572_MI);
		V_7 = L_17;
		t17  L_18 = m1653((&V_7), &m1653_MI);
		V_8 = L_18;
		int32_t L_19 = m920(__this, &m920_MI);
		float L_20 = m1672((&V_8), L_19, &m1672_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_21 = m1643(NULL, ((float)((float)L_16/(float)L_20)), &m1643_MI);
		V_2 = L_21;
		bool L_22 = m921(__this, &m921_MI);
		G_B7_0 = __this;
		if (!L_22)
		{
			G_B8_0 = __this;
			goto IL_00ca;
		}
	}
	{
		G_B9_0 = ((float)((1.0f)-V_2));
		G_B9_1 = G_B7_0;
		goto IL_00cb;
	}

IL_00ca:
	{
		G_B9_0 = V_2;
		G_B9_1 = G_B8_0;
	}

IL_00cb:
	{
		m909(G_B9_1, G_B9_0, &m909_MI);
	}

IL_00d0:
	{
		return;
	}
}
 bool m924 (t234 * __this, t6 * p0, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m859_MI, __this);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = m230(p0, &m230_MI);
		G_B4_0 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 0;
	}

IL_0022:
	{
		return G_B4_0;
	}
}
extern MethodInfo m925_MI;
 void m925 (t234 * __this, t6 * p0, MethodInfo* method){
	t17  V_0 = {0};
	{
		bool L_0 = m924(__this, p0, &m924_MI);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		m884(__this, p0, &m884_MI);
		t17  L_1 = m1394(NULL, &m1394_MI);
		__this->f29 = L_1;
		t2 * L_2 = (__this->f28);
		bool L_3 = m1300(NULL, L_2, (t41 *)NULL, &m1300_MI);
		if (!L_3)
		{
			goto IL_0076;
		}
	}
	{
		t2 * L_4 = (__this->f17);
		t17  L_5 = m210(p0, &m210_MI);
		t24 * L_6 = m234(p0, &m234_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		bool L_7 = m1628(NULL, L_4, L_5, L_6, &m1628_MI);
		if (!L_7)
		{
			goto IL_0076;
		}
	}
	{
		t2 * L_8 = (__this->f17);
		t17  L_9 = m210(p0, &m210_MI);
		t24 * L_10 = m235(p0, &m235_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		bool L_11 = m1677(NULL, L_8, L_9, L_10, (&V_0), &m1677_MI);
		if (!L_11)
		{
			goto IL_0071;
		}
	}
	{
		__this->f29 = V_0;
	}

IL_0071:
	{
		goto IL_0083;
	}

IL_0076:
	{
		t24 * L_12 = m235(p0, &m235_MI);
		m923(__this, p0, L_12, &m923_MI);
	}

IL_0083:
	{
		return;
	}
}
extern MethodInfo m926_MI;
 void m926 (t234 * __this, t6 * p0, MethodInfo* method){
	{
		bool L_0 = m924(__this, p0, &m924_MI);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		t24 * L_1 = m235(p0, &m235_MI);
		m923(__this, p0, L_1, &m923_MI);
		return;
	}
}
extern MethodInfo m927_MI;
 void m927 (t234 * __this, t64 * p0, MethodInfo* method){
	int32_t V_0 = {0};
	t234 * G_B9_0 = {0};
	t234 * G_B8_0 = {0};
	float G_B10_0 = 0.0f;
	t234 * G_B10_1 = {0};
	t234 * G_B17_0 = {0};
	t234 * G_B16_0 = {0};
	float G_B18_0 = 0.0f;
	t234 * G_B18_1 = {0};
	t234 * G_B25_0 = {0};
	t234 * G_B24_0 = {0};
	float G_B26_0 = 0.0f;
	t234 * G_B26_1 = {0};
	t234 * G_B33_0 = {0};
	t234 * G_B32_0 = {0};
	float G_B34_0 = 0.0f;
	t234 * G_B34_1 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m859_MI, __this);
		if (L_1)
		{
			goto IL_001e;
		}
	}

IL_0016:
	{
		m874(__this, p0, &m874_MI);
		return;
	}

IL_001e:
	{
		int32_t L_2 = m184(p0, &m184_MI);
		V_0 = L_2;
		if (V_0 == 0)
		{
			goto IL_0040;
		}
		if (V_0 == 1)
		{
			goto IL_00fa;
		}
		if (V_0 == 2)
		{
			goto IL_009d;
		}
		if (V_0 == 3)
		{
			goto IL_0158;
		}
	}
	{
		goto IL_01b6;
	}

IL_0040:
	{
		int32_t L_3 = m920(__this, &m920_MI);
		if (L_3)
		{
			goto IL_0091;
		}
	}
	{
		t139 * L_4 = (t139 *)VirtFuncInvoker0< t139 * >::Invoke(&m928_MI, __this);
		bool L_5 = m1297(NULL, L_4, (t41 *)NULL, &m1297_MI);
		if (!L_5)
		{
			goto IL_0091;
		}
	}
	{
		bool L_6 = m921(__this, &m921_MI);
		G_B8_0 = __this;
		if (!L_6)
		{
			G_B9_0 = __this;
			goto IL_007a;
		}
	}
	{
		float L_7 = m906(__this, &m906_MI);
		float L_8 = m912(__this, &m912_MI);
		G_B10_0 = ((float)(L_7+L_8));
		G_B10_1 = G_B8_0;
		goto IL_0087;
	}

IL_007a:
	{
		float L_9 = m906(__this, &m906_MI);
		float L_10 = m912(__this, &m912_MI);
		G_B10_0 = ((float)(L_9-L_10));
		G_B10_1 = G_B9_0;
	}

IL_0087:
	{
		m917(G_B10_1, G_B10_0, &m917_MI);
		goto IL_0098;
	}

IL_0091:
	{
		m874(__this, p0, &m874_MI);
	}

IL_0098:
	{
		goto IL_01b6;
	}

IL_009d:
	{
		int32_t L_11 = m920(__this, &m920_MI);
		if (L_11)
		{
			goto IL_00ee;
		}
	}
	{
		t139 * L_12 = (t139 *)VirtFuncInvoker0< t139 * >::Invoke(&m929_MI, __this);
		bool L_13 = m1297(NULL, L_12, (t41 *)NULL, &m1297_MI);
		if (!L_13)
		{
			goto IL_00ee;
		}
	}
	{
		bool L_14 = m921(__this, &m921_MI);
		G_B16_0 = __this;
		if (!L_14)
		{
			G_B17_0 = __this;
			goto IL_00d7;
		}
	}
	{
		float L_15 = m906(__this, &m906_MI);
		float L_16 = m912(__this, &m912_MI);
		G_B18_0 = ((float)(L_15-L_16));
		G_B18_1 = G_B16_0;
		goto IL_00e4;
	}

IL_00d7:
	{
		float L_17 = m906(__this, &m906_MI);
		float L_18 = m912(__this, &m912_MI);
		G_B18_0 = ((float)(L_17+L_18));
		G_B18_1 = G_B17_0;
	}

IL_00e4:
	{
		m917(G_B18_1, G_B18_0, &m917_MI);
		goto IL_00f5;
	}

IL_00ee:
	{
		m874(__this, p0, &m874_MI);
	}

IL_00f5:
	{
		goto IL_01b6;
	}

IL_00fa:
	{
		int32_t L_19 = m920(__this, &m920_MI);
		if ((((uint32_t)L_19) != ((uint32_t)1)))
		{
			goto IL_014c;
		}
	}
	{
		t139 * L_20 = (t139 *)VirtFuncInvoker0< t139 * >::Invoke(&m930_MI, __this);
		bool L_21 = m1297(NULL, L_20, (t41 *)NULL, &m1297_MI);
		if (!L_21)
		{
			goto IL_014c;
		}
	}
	{
		bool L_22 = m921(__this, &m921_MI);
		G_B24_0 = __this;
		if (!L_22)
		{
			G_B25_0 = __this;
			goto IL_0135;
		}
	}
	{
		float L_23 = m906(__this, &m906_MI);
		float L_24 = m912(__this, &m912_MI);
		G_B26_0 = ((float)(L_23-L_24));
		G_B26_1 = G_B24_0;
		goto IL_0142;
	}

IL_0135:
	{
		float L_25 = m906(__this, &m906_MI);
		float L_26 = m912(__this, &m912_MI);
		G_B26_0 = ((float)(L_25+L_26));
		G_B26_1 = G_B25_0;
	}

IL_0142:
	{
		m917(G_B26_1, G_B26_0, &m917_MI);
		goto IL_0153;
	}

IL_014c:
	{
		m874(__this, p0, &m874_MI);
	}

IL_0153:
	{
		goto IL_01b6;
	}

IL_0158:
	{
		int32_t L_27 = m920(__this, &m920_MI);
		if ((((uint32_t)L_27) != ((uint32_t)1)))
		{
			goto IL_01aa;
		}
	}
	{
		t139 * L_28 = (t139 *)VirtFuncInvoker0< t139 * >::Invoke(&m931_MI, __this);
		bool L_29 = m1297(NULL, L_28, (t41 *)NULL, &m1297_MI);
		if (!L_29)
		{
			goto IL_01aa;
		}
	}
	{
		bool L_30 = m921(__this, &m921_MI);
		G_B32_0 = __this;
		if (!L_30)
		{
			G_B33_0 = __this;
			goto IL_0193;
		}
	}
	{
		float L_31 = m906(__this, &m906_MI);
		float L_32 = m912(__this, &m912_MI);
		G_B34_0 = ((float)(L_31+L_32));
		G_B34_1 = G_B32_0;
		goto IL_01a0;
	}

IL_0193:
	{
		float L_33 = m906(__this, &m906_MI);
		float L_34 = m912(__this, &m912_MI);
		G_B34_0 = ((float)(L_33-L_34));
		G_B34_1 = G_B33_0;
	}

IL_01a0:
	{
		m917(G_B34_1, G_B34_0, &m917_MI);
		goto IL_01b1;
	}

IL_01aa:
	{
		m874(__this, p0, &m874_MI);
	}

IL_01b1:
	{
		goto IL_01b6;
	}

IL_01b6:
	{
		return;
	}
}
 t139 * m928 (t234 * __this, MethodInfo* method){
	t210  V_0 = {0};
	{
		t210  L_0 = m834(__this, &m834_MI);
		V_0 = L_0;
		int32_t L_1 = m708((&V_0), &m708_MI);
		if ((((uint32_t)L_1) != ((uint32_t)3)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = m920(__this, &m920_MI);
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (t139 *)NULL;
	}

IL_0021:
	{
		t139 * L_3 = m870(__this, &m870_MI);
		return L_3;
	}
}
 t139 * m929 (t234 * __this, MethodInfo* method){
	t210  V_0 = {0};
	{
		t210  L_0 = m834(__this, &m834_MI);
		V_0 = L_0;
		int32_t L_1 = m708((&V_0), &m708_MI);
		if ((((uint32_t)L_1) != ((uint32_t)3)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_2 = m920(__this, &m920_MI);
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (t139 *)NULL;
	}

IL_0021:
	{
		t139 * L_3 = m871(__this, &m871_MI);
		return L_3;
	}
}
 t139 * m930 (t234 * __this, MethodInfo* method){
	t210  V_0 = {0};
	{
		t210  L_0 = m834(__this, &m834_MI);
		V_0 = L_0;
		int32_t L_1 = m708((&V_0), &m708_MI);
		if ((((uint32_t)L_1) != ((uint32_t)3)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = m920(__this, &m920_MI);
		if ((((uint32_t)L_2) != ((uint32_t)1)))
		{
			goto IL_0022;
		}
	}
	{
		return (t139 *)NULL;
	}

IL_0022:
	{
		t139 * L_3 = m872(__this, &m872_MI);
		return L_3;
	}
}
 t139 * m931 (t234 * __this, MethodInfo* method){
	t210  V_0 = {0};
	{
		t210  L_0 = m834(__this, &m834_MI);
		V_0 = L_0;
		int32_t L_1 = m708((&V_0), &m708_MI);
		if ((((uint32_t)L_1) != ((uint32_t)3)))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = m920(__this, &m920_MI);
		if ((((uint32_t)L_2) != ((uint32_t)1)))
		{
			goto IL_0022;
		}
	}
	{
		return (t139 *)NULL;
	}

IL_0022:
	{
		t139 * L_3 = m873(__this, &m873_MI);
		return L_3;
	}
}
extern MethodInfo m932_MI;
 void m932 (t234 * __this, t6 * p0, MethodInfo* method){
	{
		m227(p0, 0, &m227_MI);
		return;
	}
}
extern MethodInfo m933_MI;
 void m933 (t234 * __this, int32_t p0, bool p1, MethodInfo* method){
	int32_t V_0 = {0};
	bool V_1 = false;
	{
		int32_t L_0 = m920(__this, &m920_MI);
		V_0 = L_0;
		bool L_1 = m921(__this, &m921_MI);
		V_1 = L_1;
		m899(__this, p0, &m899_MI);
		if (p1)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		int32_t L_2 = m920(__this, &m920_MI);
		if ((((int32_t)L_2) == ((int32_t)V_0)))
		{
			goto IL_003a;
		}
	}
	{
		t25 * L_3 = m39(__this, &m39_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		m1835(NULL, ((t2 *)IsInst(L_3, InitializedTypeInfo(&t2_TI))), 1, 1, &m1835_MI);
	}

IL_003a:
	{
		bool L_4 = m921(__this, &m921_MI);
		if ((((int32_t)L_4) == ((int32_t)V_1)))
		{
			goto IL_005e;
		}
	}
	{
		t25 * L_5 = m39(__this, &m39_MI);
		int32_t L_6 = m920(__this, &m920_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		m1836(NULL, ((t2 *)IsInst(L_5, InitializedTypeInfo(&t2_TI))), L_6, 1, 1, &m1836_MI);
	}

IL_005e:
	{
		return;
	}
}
extern MethodInfo m934_MI;
 bool m934 (t234 * __this, MethodInfo* method){
	{
		bool L_0 = m180(__this, &m180_MI);
		return L_0;
	}
}
extern MethodInfo m935_MI;
 t25 * m935 (t234 * __this, MethodInfo* method){
	{
		t25 * L_0 = m39(__this, &m39_MI);
		return L_0;
	}
}
// Metadata Definition UnityEngine.UI.Slider
extern Il2CppType t2_0_0_1;
extern CustomAttributesCache t234__CustomAttributeCache_m_FillRect;
FieldInfo t234_f16_FieldInfo = 
{
	"m_FillRect", &t2_0_0_1, &t234_TI, offsetof(t234, f16), &t234__CustomAttributeCache_m_FillRect};
extern Il2CppType t2_0_0_1;
extern CustomAttributesCache t234__CustomAttributeCache_m_HandleRect;
FieldInfo t234_f17_FieldInfo = 
{
	"m_HandleRect", &t2_0_0_1, &t234_TI, offsetof(t234, f17), &t234__CustomAttributeCache_m_HandleRect};
extern Il2CppType t231_0_0_1;
extern CustomAttributesCache t234__CustomAttributeCache_m_Direction;
FieldInfo t234_f18_FieldInfo = 
{
	"m_Direction", &t231_0_0_1, &t234_TI, offsetof(t234, f18), &t234__CustomAttributeCache_m_Direction};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t234__CustomAttributeCache_m_MinValue;
FieldInfo t234_f19_FieldInfo = 
{
	"m_MinValue", &t22_0_0_1, &t234_TI, offsetof(t234, f19), &t234__CustomAttributeCache_m_MinValue};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t234__CustomAttributeCache_m_MaxValue;
FieldInfo t234_f20_FieldInfo = 
{
	"m_MaxValue", &t22_0_0_1, &t234_TI, offsetof(t234, f20), &t234__CustomAttributeCache_m_MaxValue};
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t234__CustomAttributeCache_m_WholeNumbers;
FieldInfo t234_f21_FieldInfo = 
{
	"m_WholeNumbers", &t40_0_0_1, &t234_TI, offsetof(t234, f21), &t234__CustomAttributeCache_m_WholeNumbers};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t234__CustomAttributeCache_m_Value;
FieldInfo t234_f22_FieldInfo = 
{
	"m_Value", &t22_0_0_1, &t234_TI, offsetof(t234, f22), &t234__CustomAttributeCache_m_Value};
extern Il2CppType t232_0_0_1;
extern CustomAttributesCache t234__CustomAttributeCache_m_OnValueChanged;
FieldInfo t234_f23_FieldInfo = 
{
	"m_OnValueChanged", &t232_0_0_1, &t234_TI, offsetof(t234, f23), &t234__CustomAttributeCache_m_OnValueChanged};
extern Il2CppType t179_0_0_1;
FieldInfo t234_f24_FieldInfo = 
{
	"m_FillImage", &t179_0_0_1, &t234_TI, offsetof(t234, f24), &EmptyCustomAttributesCache};
extern Il2CppType t25_0_0_1;
FieldInfo t234_f25_FieldInfo = 
{
	"m_FillTransform", &t25_0_0_1, &t234_TI, offsetof(t234, f25), &EmptyCustomAttributesCache};
extern Il2CppType t2_0_0_1;
FieldInfo t234_f26_FieldInfo = 
{
	"m_FillContainerRect", &t2_0_0_1, &t234_TI, offsetof(t234, f26), &EmptyCustomAttributesCache};
extern Il2CppType t25_0_0_1;
FieldInfo t234_f27_FieldInfo = 
{
	"m_HandleTransform", &t25_0_0_1, &t234_TI, offsetof(t234, f27), &EmptyCustomAttributesCache};
extern Il2CppType t2_0_0_1;
FieldInfo t234_f28_FieldInfo = 
{
	"m_HandleContainerRect", &t2_0_0_1, &t234_TI, offsetof(t234, f28), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_1;
FieldInfo t234_f29_FieldInfo = 
{
	"m_Offset", &t17_0_0_1, &t234_TI, offsetof(t234, f29), &EmptyCustomAttributesCache};
extern Il2CppType t218_0_0_1;
FieldInfo t234_f30_FieldInfo = 
{
	"m_Tracker", &t218_0_0_1, &t234_TI, offsetof(t234, f30), &EmptyCustomAttributesCache};
static FieldInfo* t234_FIs[] =
{
	&t234_f16_FieldInfo,
	&t234_f17_FieldInfo,
	&t234_f18_FieldInfo,
	&t234_f19_FieldInfo,
	&t234_f20_FieldInfo,
	&t234_f21_FieldInfo,
	&t234_f22_FieldInfo,
	&t234_f23_FieldInfo,
	&t234_f24_FieldInfo,
	&t234_f25_FieldInfo,
	&t234_f26_FieldInfo,
	&t234_f27_FieldInfo,
	&t234_f28_FieldInfo,
	&t234_f29_FieldInfo,
	&t234_f30_FieldInfo,
	NULL
};
static PropertyInfo t234____fillRect_PropertyInfo = 
{
	&t234_TI, "fillRect", &m894_MI, &m895_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t234____handleRect_PropertyInfo = 
{
	&t234_TI, "handleRect", &m896_MI, &m897_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t234____direction_PropertyInfo = 
{
	&t234_TI, "direction", &m898_MI, &m899_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t234____minValue_PropertyInfo = 
{
	&t234_TI, "minValue", &m900_MI, &m901_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t234____maxValue_PropertyInfo = 
{
	&t234_TI, "maxValue", &m902_MI, &m903_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t234____wholeNumbers_PropertyInfo = 
{
	&t234_TI, "wholeNumbers", &m904_MI, &m905_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t234____value_PropertyInfo = 
{
	&t234_TI, "value", &m906_MI, &m907_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t234____normalizedValue_PropertyInfo = 
{
	&t234_TI, "normalizedValue", &m908_MI, &m909_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t234____onValueChanged_PropertyInfo = 
{
	&t234_TI, "onValueChanged", &m910_MI, &m911_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t234____stepSize_PropertyInfo = 
{
	&t234_TI, "stepSize", &m912_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t234____axis_PropertyInfo = 
{
	&t234_TI, "axis", &m920_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t234____reverseValue_PropertyInfo = 
{
	&t234_TI, "reverseValue", &m921_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t234_PIs[] =
{
	&t234____fillRect_PropertyInfo,
	&t234____handleRect_PropertyInfo,
	&t234____direction_PropertyInfo,
	&t234____minValue_PropertyInfo,
	&t234____maxValue_PropertyInfo,
	&t234____wholeNumbers_PropertyInfo,
	&t234____value_PropertyInfo,
	&t234____normalizedValue_PropertyInfo,
	&t234____onValueChanged_PropertyInfo,
	&t234____stepSize_PropertyInfo,
	&t234____axis_PropertyInfo,
	&t234____reverseValue_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m893_MI = 
{
	".ctor", (methodPointerType)&m893, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 874, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m894_MI = 
{
	"get_fillRect", (methodPointerType)&m894, &t234_TI, &t2_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 875, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t234_m895_ParameterInfos[] = 
{
	{"value", 0, 134218259, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m895_MI = 
{
	"set_fillRect", (methodPointerType)&m895, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t234_m895_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 876, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m896_MI = 
{
	"get_handleRect", (methodPointerType)&m896, &t234_TI, &t2_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 877, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t234_m897_ParameterInfos[] = 
{
	{"value", 0, 134218260, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m897_MI = 
{
	"set_handleRect", (methodPointerType)&m897, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t234_m897_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 878, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t231_0_0_0;
extern void* RuntimeInvoker_t231 (MethodInfo* method, void* obj, void** args);
MethodInfo m898_MI = 
{
	"get_direction", (methodPointerType)&m898, &t234_TI, &t231_0_0_0, RuntimeInvoker_t231, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 879, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t231_0_0_0;
static ParameterInfo t234_m899_ParameterInfos[] = 
{
	{"value", 0, 134218261, &EmptyCustomAttributesCache, &t231_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m899_MI = 
{
	"set_direction", (methodPointerType)&m899, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t234_m899_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 880, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m900_MI = 
{
	"get_minValue", (methodPointerType)&m900, &t234_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 881, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t234_m901_ParameterInfos[] = 
{
	{"value", 0, 134218262, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m901_MI = 
{
	"set_minValue", (methodPointerType)&m901, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t234_m901_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 882, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m902_MI = 
{
	"get_maxValue", (methodPointerType)&m902, &t234_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 883, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t234_m903_ParameterInfos[] = 
{
	{"value", 0, 134218263, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m903_MI = 
{
	"set_maxValue", (methodPointerType)&m903, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t234_m903_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 884, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m904_MI = 
{
	"get_wholeNumbers", (methodPointerType)&m904, &t234_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 885, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t234_m905_ParameterInfos[] = 
{
	{"value", 0, 134218264, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m905_MI = 
{
	"set_wholeNumbers", (methodPointerType)&m905, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t234_m905_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 886, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m906_MI = 
{
	"get_value", (methodPointerType)&m906, &t234_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 887, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t234_m907_ParameterInfos[] = 
{
	{"value", 0, 134218265, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m907_MI = 
{
	"set_value", (methodPointerType)&m907, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t234_m907_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 888, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m908_MI = 
{
	"get_normalizedValue", (methodPointerType)&m908, &t234_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 889, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t234_m909_ParameterInfos[] = 
{
	{"value", 0, 134218266, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m909_MI = 
{
	"set_normalizedValue", (methodPointerType)&m909, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t234_m909_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 890, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t232_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m910_MI = 
{
	"get_onValueChanged", (methodPointerType)&m910, &t234_TI, &t232_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 891, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t232_0_0_0;
static ParameterInfo t234_m911_ParameterInfos[] = 
{
	{"value", 0, 134218267, &EmptyCustomAttributesCache, &t232_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m911_MI = 
{
	"set_onValueChanged", (methodPointerType)&m911, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t234_m911_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 892, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m912_MI = 
{
	"get_stepSize", (methodPointerType)&m912, &t234_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 893, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t140_0_0_0;
static ParameterInfo t234_m913_ParameterInfos[] = 
{
	{"executing", 0, 134218268, &EmptyCustomAttributesCache, &t140_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m913_MI = 
{
	"Rebuild", (methodPointerType)&m913, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t234_m913_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 42, 1, false, false, 894, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m914_MI = 
{
	"OnEnable", (methodPointerType)&m914, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 5, 0, false, false, 895, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m915_MI = 
{
	"OnDisable", (methodPointerType)&m915, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 7, 0, false, false, 896, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m916_MI = 
{
	"UpdateCachedReferences", (methodPointerType)&m916, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 897, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t234_m917_ParameterInfos[] = 
{
	{"input", 0, 134218269, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m917_MI = 
{
	"Set", (methodPointerType)&m917, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t234_m917_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 898, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t234_m918_ParameterInfos[] = 
{
	{"input", 0, 134218270, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"sendCallback", 1, 134218271, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m918_MI = 
{
	"Set", (methodPointerType)&m918, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t22_t297, t234_m918_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 899, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m919_MI = 
{
	"OnRectTransformDimensionsChange", (methodPointerType)&m919, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 10, 0, false, false, 900, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t233_0_0_0;
extern void* RuntimeInvoker_t233 (MethodInfo* method, void* obj, void** args);
MethodInfo m920_MI = 
{
	"get_axis", (methodPointerType)&m920, &t234_TI, &t233_0_0_0, RuntimeInvoker_t233, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 901, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m921_MI = 
{
	"get_reverseValue", (methodPointerType)&m921, &t234_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 902, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m922_MI = 
{
	"UpdateVisuals", (methodPointerType)&m922, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 903, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
extern Il2CppType t24_0_0_0;
extern Il2CppType t24_0_0_0;
static ParameterInfo t234_m923_ParameterInfos[] = 
{
	{"eventData", 0, 134218272, &EmptyCustomAttributesCache, &t6_0_0_0},
	{"cam", 1, 134218273, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m923_MI = 
{
	"UpdateDrag", (methodPointerType)&m923, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t234_m923_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 904, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t234_m924_ParameterInfos[] = 
{
	{"eventData", 0, 134218274, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m924_MI = 
{
	"MayDrag", (methodPointerType)&m924, &t234_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t234_m924_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 905, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t234_m925_ParameterInfos[] = 
{
	{"eventData", 0, 134218275, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m925_MI = 
{
	"OnPointerDown", (methodPointerType)&m925, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t234_m925_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 30, 1, false, false, 906, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t234_m926_ParameterInfos[] = 
{
	{"eventData", 0, 134218276, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m926_MI = 
{
	"OnDrag", (methodPointerType)&m926, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t234_m926_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 43, 1, false, false, 907, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t64_0_0_0;
static ParameterInfo t234_m927_ParameterInfos[] = 
{
	{"eventData", 0, 134218277, &EmptyCustomAttributesCache, &t64_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m927_MI = 
{
	"OnMove", (methodPointerType)&m927, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t234_m927_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 29, 1, false, false, 908, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m928_MI = 
{
	"FindSelectableOnLeft", (methodPointerType)&m928, &t234_TI, &t139_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 25, 0, false, false, 909, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m929_MI = 
{
	"FindSelectableOnRight", (methodPointerType)&m929, &t234_TI, &t139_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 26, 0, false, false, 910, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m930_MI = 
{
	"FindSelectableOnUp", (methodPointerType)&m930, &t234_TI, &t139_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 27, 0, false, false, 911, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m931_MI = 
{
	"FindSelectableOnDown", (methodPointerType)&m931, &t234_TI, &t139_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 28, 0, false, false, 912, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t234_m932_ParameterInfos[] = 
{
	{"eventData", 0, 134218278, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m932_MI = 
{
	"OnInitializePotentialDrag", (methodPointerType)&m932, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t234_m932_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 44, 1, false, false, 913, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t231_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t234_m933_ParameterInfos[] = 
{
	{"direction", 0, 134218279, &EmptyCustomAttributesCache, &t231_0_0_0},
	{"includeRectLayouts", 1, 134218280, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m933_MI = 
{
	"SetDirection", (methodPointerType)&m933, &t234_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297, t234_m933_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 2, false, false, 914, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m934_MI = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed", (methodPointerType)&m934, &t234_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 960, 0, 45, 0, false, false, 915, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m935_MI = 
{
	"UnityEngine.UI.ICanvasElement.get_transform", (methodPointerType)&m935, &t234_TI, &t25_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 960, 0, 46, 0, false, false, 916, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t234_MIs[] =
{
	&m893_MI,
	&m894_MI,
	&m895_MI,
	&m896_MI,
	&m897_MI,
	&m898_MI,
	&m899_MI,
	&m900_MI,
	&m901_MI,
	&m902_MI,
	&m903_MI,
	&m904_MI,
	&m905_MI,
	&m906_MI,
	&m907_MI,
	&m908_MI,
	&m909_MI,
	&m910_MI,
	&m911_MI,
	&m912_MI,
	&m913_MI,
	&m914_MI,
	&m915_MI,
	&m916_MI,
	&m917_MI,
	&m918_MI,
	&m919_MI,
	&m920_MI,
	&m921_MI,
	&m922_MI,
	&m923_MI,
	&m924_MI,
	&m925_MI,
	&m926_MI,
	&m927_MI,
	&m928_MI,
	&m929_MI,
	&m930_MI,
	&m931_MI,
	&m932_MI,
	&m933_MI,
	&m934_MI,
	&m935_MI,
	NULL
};
extern TypeInfo t231_TI;
extern TypeInfo t232_TI;
extern TypeInfo t233_TI;
static TypeInfo* t234_TI__nestedTypes[4] =
{
	&t231_TI,
	&t232_TI,
	&t233_TI,
	NULL
};
static MethodInfo* t234_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m857_MI,
	&m914_MI,
	&m171_MI,
	&m915_MI,
	&m173_MI,
	&m174_MI,
	&m919_MI,
	&m176_MI,
	&m177_MI,
	&m860_MI,
	&m858_MI,
	&m886_MI,
	&m887_MI,
	&m925_MI,
	&m885_MI,
	&m888_MI,
	&m889_MI,
	&m927_MI,
	&m859_MI,
	&m865_MI,
	&m866_MI,
	&m928_MI,
	&m929_MI,
	&m930_MI,
	&m931_MI,
	&m927_MI,
	&m925_MI,
	&m885_MI,
	&m886_MI,
	&m887_MI,
	&m888_MI,
	&m889_MI,
	&m890_MI,
	&m932_MI,
	&m926_MI,
	&m913_MI,
	&m935_MI,
	&m934_MI,
	&m913_MI,
	&m926_MI,
	&m932_MI,
	&m934_MI,
	&m935_MI,
};
static TypeInfo* t234_ITIs[] = 
{
	&t31_TI,
	&t92_TI,
	&t94_TI,
	&t145_TI,
};
static Il2CppInterfaceOffsetPair t234_IOs[] = 
{
	{ &t31_TI, 15},
	{ &t30_TI, 15},
	{ &t32_TI, 16},
	{ &t89_TI, 17},
	{ &t90_TI, 18},
	{ &t99_TI, 19},
	{ &t100_TI, 20},
	{ &t101_TI, 21},
	{ &t92_TI, 37},
	{ &t94_TI, 38},
	{ &t145_TI, 39},
};
void t234_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t318 * tmp;
		tmp = (t318 *)il2cpp_codegen_object_new (&t318_TI);
		m1404(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2_TI)), &m1404_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("UI/Slider"), 34, &m1519_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t234_CustomAttributesCacheGenerator_m_FillRect(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t234_CustomAttributesCacheGenerator_m_HandleRect(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t394_TI;
#include "t394.h"
#include "t394MD.h"
extern MethodInfo m1837_MI;
void t234_CustomAttributesCacheGenerator_m_Direction(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t394 * tmp;
		tmp = (t394 *)il2cpp_codegen_object_new (&t394_TI);
		m1837(tmp, 6.0f, &m1837_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t234_CustomAttributesCacheGenerator_m_MinValue(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t234_CustomAttributesCacheGenerator_m_MaxValue(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t234_CustomAttributesCacheGenerator_m_WholeNumbers(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t234_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t234_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t394 * tmp;
		tmp = (t394 *)il2cpp_codegen_object_new (&t394_TI);
		m1837(tmp, 6.0f, &m1837_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t234__CustomAttributeCache = {
2,
NULL,
&t234_CustomAttributesCacheGenerator
};
CustomAttributesCache t234__CustomAttributeCache_m_FillRect = {
1,
NULL,
&t234_CustomAttributesCacheGenerator_m_FillRect
};
CustomAttributesCache t234__CustomAttributeCache_m_HandleRect = {
1,
NULL,
&t234_CustomAttributesCacheGenerator_m_HandleRect
};
CustomAttributesCache t234__CustomAttributeCache_m_Direction = {
2,
NULL,
&t234_CustomAttributesCacheGenerator_m_Direction
};
CustomAttributesCache t234__CustomAttributeCache_m_MinValue = {
1,
NULL,
&t234_CustomAttributesCacheGenerator_m_MinValue
};
CustomAttributesCache t234__CustomAttributeCache_m_MaxValue = {
1,
NULL,
&t234_CustomAttributesCacheGenerator_m_MaxValue
};
CustomAttributesCache t234__CustomAttributeCache_m_WholeNumbers = {
1,
NULL,
&t234_CustomAttributesCacheGenerator_m_WholeNumbers
};
CustomAttributesCache t234__CustomAttributeCache_m_Value = {
1,
NULL,
&t234_CustomAttributesCacheGenerator_m_Value
};
CustomAttributesCache t234__CustomAttributeCache_m_OnValueChanged = {
2,
NULL,
&t234_CustomAttributesCacheGenerator_m_OnValueChanged
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t234_0_0_0;
extern Il2CppType t234_1_0_0;
struct t234;
extern CustomAttributesCache t234__CustomAttributeCache;
extern CustomAttributesCache t234__CustomAttributeCache_m_FillRect;
extern CustomAttributesCache t234__CustomAttributeCache_m_HandleRect;
extern CustomAttributesCache t234__CustomAttributeCache_m_Direction;
extern CustomAttributesCache t234__CustomAttributeCache_m_MinValue;
extern CustomAttributesCache t234__CustomAttributeCache_m_MaxValue;
extern CustomAttributesCache t234__CustomAttributeCache_m_WholeNumbers;
extern CustomAttributesCache t234__CustomAttributeCache_m_Value;
extern CustomAttributesCache t234__CustomAttributeCache_m_OnValueChanged;
TypeInfo t234_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Slider", "UnityEngine.UI", t234_MIs, t234_PIs, t234_FIs, NULL, &t139_TI, t234_TI__nestedTypes, NULL, &t234_TI, t234_ITIs, t234_VT, &t234__CustomAttributeCache, &t234_TI, &t234_0_0_0, &t234_1_0_0, t234_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t234), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 43, 12, 15, 0, 3, 47, 4, 11};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t228_TI;



 t180 * m936 (t228 * __this, MethodInfo* method){
	{
		t180 * L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m937_MI;
 void m937 (t228 * __this, t180 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
 t180 * m938 (t228 * __this, MethodInfo* method){
	{
		t180 * L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m939_MI;
 void m939 (t228 * __this, t180 * p0, MethodInfo* method){
	{
		__this->f1 = p0;
		return;
	}
}
 t180 * m940 (t228 * __this, MethodInfo* method){
	{
		t180 * L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m941_MI;
 void m941 (t228 * __this, t180 * p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
// Metadata Definition UnityEngine.UI.SpriteState
extern Il2CppType t180_0_0_1;
extern CustomAttributesCache t228__CustomAttributeCache_m_HighlightedSprite;
FieldInfo t228_f0_FieldInfo = 
{
	"m_HighlightedSprite", &t180_0_0_1, &t228_TI, offsetof(t228, f0) + sizeof(t29), &t228__CustomAttributeCache_m_HighlightedSprite};
extern Il2CppType t180_0_0_1;
extern CustomAttributesCache t228__CustomAttributeCache_m_PressedSprite;
FieldInfo t228_f1_FieldInfo = 
{
	"m_PressedSprite", &t180_0_0_1, &t228_TI, offsetof(t228, f1) + sizeof(t29), &t228__CustomAttributeCache_m_PressedSprite};
extern Il2CppType t180_0_0_1;
extern CustomAttributesCache t228__CustomAttributeCache_m_DisabledSprite;
FieldInfo t228_f2_FieldInfo = 
{
	"m_DisabledSprite", &t180_0_0_1, &t228_TI, offsetof(t228, f2) + sizeof(t29), &t228__CustomAttributeCache_m_DisabledSprite};
static FieldInfo* t228_FIs[] =
{
	&t228_f0_FieldInfo,
	&t228_f1_FieldInfo,
	&t228_f2_FieldInfo,
	NULL
};
static PropertyInfo t228____highlightedSprite_PropertyInfo = 
{
	&t228_TI, "highlightedSprite", &m936_MI, &m937_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t228____pressedSprite_PropertyInfo = 
{
	&t228_TI, "pressedSprite", &m938_MI, &m939_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t228____disabledSprite_PropertyInfo = 
{
	&t228_TI, "disabledSprite", &m940_MI, &m941_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t228_PIs[] =
{
	&t228____highlightedSprite_PropertyInfo,
	&t228____pressedSprite_PropertyInfo,
	&t228____disabledSprite_PropertyInfo,
	NULL
};
extern Il2CppType t180_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m936_MI = 
{
	"get_highlightedSprite", (methodPointerType)&m936, &t228_TI, &t180_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 918, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t180_0_0_0;
static ParameterInfo t228_m937_ParameterInfos[] = 
{
	{"value", 0, 134218281, &EmptyCustomAttributesCache, &t180_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m937_MI = 
{
	"set_highlightedSprite", (methodPointerType)&m937, &t228_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t228_m937_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 919, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t180_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m938_MI = 
{
	"get_pressedSprite", (methodPointerType)&m938, &t228_TI, &t180_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 920, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t180_0_0_0;
static ParameterInfo t228_m939_ParameterInfos[] = 
{
	{"value", 0, 134218282, &EmptyCustomAttributesCache, &t180_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m939_MI = 
{
	"set_pressedSprite", (methodPointerType)&m939, &t228_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t228_m939_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 921, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t180_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m940_MI = 
{
	"get_disabledSprite", (methodPointerType)&m940, &t228_TI, &t180_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 922, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t180_0_0_0;
static ParameterInfo t228_m941_ParameterInfos[] = 
{
	{"value", 0, 134218283, &EmptyCustomAttributesCache, &t180_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m941_MI = 
{
	"set_disabledSprite", (methodPointerType)&m941, &t228_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t228_m941_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 923, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t228_MIs[] =
{
	&m936_MI,
	&m937_MI,
	&m938_MI,
	&m939_MI,
	&m940_MI,
	&m941_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t228_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
};
void t228_CustomAttributesCacheGenerator_m_HighlightedSprite(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedSprite"), &m1318_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("highlightedSprite"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
void t228_CustomAttributesCacheGenerator_m_PressedSprite(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("pressedSprite"), &m1318_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t228_CustomAttributesCacheGenerator_m_DisabledSprite(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("disabledSprite"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t228__CustomAttributeCache_m_HighlightedSprite = {
3,
NULL,
&t228_CustomAttributesCacheGenerator_m_HighlightedSprite
};
CustomAttributesCache t228__CustomAttributeCache_m_PressedSprite = {
2,
NULL,
&t228_CustomAttributesCacheGenerator_m_PressedSprite
};
CustomAttributesCache t228__CustomAttributeCache_m_DisabledSprite = {
2,
NULL,
&t228_CustomAttributesCacheGenerator_m_DisabledSprite
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t228_1_0_0;
extern TypeInfo t110_TI;
extern CustomAttributesCache t228__CustomAttributeCache_m_HighlightedSprite;
extern CustomAttributesCache t228__CustomAttributeCache_m_PressedSprite;
extern CustomAttributesCache t228__CustomAttributeCache_m_DisabledSprite;
TypeInfo t228_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "SpriteState", "UnityEngine.UI", t228_MIs, t228_PIs, t228_FIs, NULL, &t110_TI, NULL, NULL, &t228_TI, NULL, t228_VT, &EmptyCustomAttributesCache, &t228_TI, &t228_0_0_0, &t228_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t228)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057033, 0, true, false, false, false, false, false, false, false, false, false, false, false, 6, 3, 3, 0, 0, 4, 0, 0};
#include "t235.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t235_TI;
#include "t235MD.h"

#include "t29MD.h"
extern MethodInfo m1331_MI;


extern MethodInfo m942_MI;
 void m942 (t235 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.StencilMaterial/MatEntry
extern Il2CppType t156_0_0_6;
FieldInfo t235_f0_FieldInfo = 
{
	"baseMat", &t156_0_0_6, &t235_TI, offsetof(t235, f0), &EmptyCustomAttributesCache};
extern Il2CppType t156_0_0_6;
FieldInfo t235_f1_FieldInfo = 
{
	"customMat", &t156_0_0_6, &t235_TI, offsetof(t235, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t235_f2_FieldInfo = 
{
	"count", &t44_0_0_6, &t235_TI, offsetof(t235, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_6;
FieldInfo t235_f3_FieldInfo = 
{
	"stencilID", &t44_0_0_6, &t235_TI, offsetof(t235, f3), &EmptyCustomAttributesCache};
static FieldInfo* t235_FIs[] =
{
	&t235_f0_FieldInfo,
	&t235_f1_FieldInfo,
	&t235_f2_FieldInfo,
	&t235_f3_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m942_MI = 
{
	".ctor", (methodPointerType)&m942, &t235_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 927, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t235_MIs[] =
{
	&m942_MI,
	NULL
};
static MethodInfo* t235_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t235_0_0_0;
extern Il2CppType t235_1_0_0;
struct t235;
extern TypeInfo t236_TI;
TypeInfo t235_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "MatEntry", "", t235_MIs, NULL, t235_FIs, NULL, &t29_TI, NULL, &t236_TI, &t235_TI, NULL, t235_VT, &EmptyCustomAttributesCache, &t235_TI, &t235_0_0_0, &t235_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t235), 0, -1, 0, 0, -1, 1048579, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 4, 0, 0, 4, 0, 0};
#include "t236.h"
#ifndef _MSC_VER
#else
#endif
#include "t236MD.h"

#include "t237.h"
#include "t156.h"
#include "mscorlib_ArrayTypes.h"
#include "t385.h"
extern TypeInfo t237_TI;
extern TypeInfo t156_TI;
extern TypeInfo t316_TI;
#include "t237MD.h"
#include "t156MD.h"
#include "t293MD.h"
#include "t208MD.h"
extern MethodInfo m1902_MI;
extern MethodInfo m1903_MI;
extern MethodInfo m1385_MI;
extern MethodInfo m1685_MI;
extern MethodInfo m1904_MI;
extern MethodInfo m1905_MI;
extern MethodInfo m1906_MI;
extern MethodInfo m1907_MI;
extern MethodInfo m1387_MI;
extern MethodInfo m1908_MI;
extern MethodInfo m1778_MI;
extern MethodInfo m1909_MI;
extern MethodInfo m1910_MI;
extern MethodInfo m707_MI;
extern MethodInfo m1911_MI;


extern MethodInfo m943_MI;
 void m943 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t237_TI));
		t237 * L_0 = (t237 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t237_TI));
		m1902(L_0, &m1902_MI);
		((t236_SFs*)InitializedTypeInfo(&t236_TI)->static_fields)->f0 = L_0;
		return;
	}
}
extern MethodInfo m944_MI;
 t156 * m944 (t29 * __this, t156 * p0, int32_t p1, MethodInfo* method){
	int32_t V_0 = 0;
	t235 * V_1 = {0};
	t235 * V_2 = {0};
	{
		if ((((int32_t)p1) <= ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_0015;
		}
	}

IL_0013:
	{
		return (t156 *)NULL;
	}

IL_0015:
	{
		bool L_1 = m1903(p0, (t7*) &_stringLiteral55, &m1903_MI);
		if (L_1)
		{
			goto IL_0042;
		}
	}
	{
		t7* L_2 = m1385(p0, &m1385_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m1685(NULL, (t7*) &_stringLiteral56, L_2, (t7*) &_stringLiteral57, &m1685_MI);
		m1904(NULL, L_3, p0, &m1904_MI);
		return (t156 *)NULL;
	}

IL_0042:
	{
		V_0 = 0;
		goto IL_008b;
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t236_TI));
		t235 * L_4 = (t235 *)VirtFuncInvoker1< t235 *, int32_t >::Invoke(&m1905_MI, (((t236_SFs*)InitializedTypeInfo(&t236_TI)->static_fields)->f0), V_0);
		V_1 = L_4;
		t156 * L_5 = (V_1->f0);
		bool L_6 = m1297(NULL, L_5, p0, &m1297_MI);
		if (!L_6)
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_7 = (V_1->f3);
		if ((((uint32_t)L_7) != ((uint32_t)p1)))
		{
			goto IL_0087;
		}
	}
	{
		t235 * L_8 = V_1;
		int32_t L_9 = (L_8->f2);
		L_8->f2 = ((int32_t)(L_9+1));
		t156 * L_10 = (V_1->f1);
		return L_10;
	}

IL_0087:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_008b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t236_TI));
		int32_t L_11 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1906_MI, (((t236_SFs*)InitializedTypeInfo(&t236_TI)->static_fields)->f0));
		if ((((int32_t)V_0) < ((int32_t)L_11)))
		{
			goto IL_0049;
		}
	}
	{
		t235 * L_12 = (t235 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t235_TI));
		m942(L_12, &m942_MI);
		V_2 = L_12;
		V_2->f2 = 1;
		V_2->f0 = p0;
		t156 * L_13 = (t156 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t156_TI));
		m1907(L_13, p0, &m1907_MI);
		V_2->f1 = L_13;
		t156 * L_14 = (V_2->f1);
		t316* L_15 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 5));
		ArrayElementTypeCheck (L_15, (t7*) &_stringLiteral58);
		*((t29 **)(t29 **)SZArrayLdElema(L_15, 0)) = (t29 *)(t7*) &_stringLiteral58;
		t316* L_16 = L_15;
		int32_t L_17 = p1;
		t29 * L_18 = Box(InitializedTypeInfo(&t44_TI), &L_17);
		ArrayElementTypeCheck (L_16, L_18);
		*((t29 **)(t29 **)SZArrayLdElema(L_16, 1)) = (t29 *)L_18;
		t316* L_19 = L_16;
		ArrayElementTypeCheck (L_19, (t7*) &_stringLiteral59);
		*((t29 **)(t29 **)SZArrayLdElema(L_19, 2)) = (t29 *)(t7*) &_stringLiteral59;
		t316* L_20 = L_19;
		t7* L_21 = m1385(p0, &m1385_MI);
		ArrayElementTypeCheck (L_20, L_21);
		*((t29 **)(t29 **)SZArrayLdElema(L_20, 3)) = (t29 *)L_21;
		t316* L_22 = L_20;
		ArrayElementTypeCheck (L_22, (t7*) &_stringLiteral60);
		*((t29 **)(t29 **)SZArrayLdElema(L_22, 4)) = (t29 *)(t7*) &_stringLiteral60;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_23 = m1387(NULL, L_22, &m1387_MI);
		m1908(L_14, L_23, &m1908_MI);
		t156 * L_24 = (V_2->f1);
		m1778(L_24, ((int32_t)61), &m1778_MI);
		V_2->f3 = p1;
		bool L_25 = m1903(p0, (t7*) &_stringLiteral61, &m1903_MI);
		if (!L_25)
		{
			goto IL_0130;
		}
	}
	{
		t156 * L_26 = (V_2->f1);
		m1909(L_26, (t7*) &_stringLiteral61, 3, &m1909_MI);
	}

IL_0130:
	{
		t156 * L_27 = (V_2->f1);
		m1909(L_27, (t7*) &_stringLiteral55, p1, &m1909_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t236_TI));
		VirtActionInvoker1< t235 * >::Invoke(&m1910_MI, (((t236_SFs*)InitializedTypeInfo(&t236_TI)->static_fields)->f0), V_2);
		t156 * L_28 = (V_2->f1);
		return L_28;
	}
}
extern MethodInfo m945_MI;
 void m945 (t29 * __this, t156 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t235 * V_1 = {0};
	int32_t V_2 = 0;
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		V_0 = 0;
		goto IL_006e;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t236_TI));
		t235 * L_1 = (t235 *)VirtFuncInvoker1< t235 *, int32_t >::Invoke(&m1905_MI, (((t236_SFs*)InitializedTypeInfo(&t236_TI)->static_fields)->f0), V_0);
		V_1 = L_1;
		t156 * L_2 = (V_1->f1);
		bool L_3 = m1300(NULL, L_2, p0, &m1300_MI);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_006a;
	}

IL_0036:
	{
		t235 * L_4 = V_1;
		int32_t L_5 = (L_4->f2);
		int32_t L_6 = ((int32_t)(L_5-1));
		V_2 = L_6;
		L_4->f2 = L_6;
		if (V_2)
		{
			goto IL_0069;
		}
	}
	{
		t156 * L_7 = (V_1->f1);
		m707(NULL, L_7, &m707_MI);
		V_1->f0 = (t156 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t236_TI));
		VirtActionInvoker1< int32_t >::Invoke(&m1911_MI, (((t236_SFs*)InitializedTypeInfo(&t236_TI)->static_fields)->f0), V_0);
	}

IL_0069:
	{
		return;
	}

IL_006a:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_006e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t236_TI));
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1906_MI, (((t236_SFs*)InitializedTypeInfo(&t236_TI)->static_fields)->f0));
		if ((((int32_t)V_0) < ((int32_t)L_8)))
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// Metadata Definition UnityEngine.UI.StencilMaterial
extern Il2CppType t237_0_0_17;
FieldInfo t236_f0_FieldInfo = 
{
	"m_List", &t237_0_0_17, &t236_TI, offsetof(t236_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t236_FIs[] =
{
	&t236_f0_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m943_MI = 
{
	".cctor", (methodPointerType)&m943, &t236_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 924, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t156_0_0_0;
extern Il2CppType t156_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t236_m944_ParameterInfos[] = 
{
	{"baseMat", 0, 134218284, &EmptyCustomAttributesCache, &t156_0_0_0},
	{"stencilID", 1, 134218285, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t156_0_0_0;
extern void* RuntimeInvoker_t29_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m944_MI = 
{
	"Add", (methodPointerType)&m944, &t236_TI, &t156_0_0_0, RuntimeInvoker_t29_t29_t44, t236_m944_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 925, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t156_0_0_0;
static ParameterInfo t236_m945_ParameterInfos[] = 
{
	{"customMat", 0, 134218286, &EmptyCustomAttributesCache, &t156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m945_MI = 
{
	"Remove", (methodPointerType)&m945, &t236_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t236_m945_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 926, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t236_MIs[] =
{
	&m943_MI,
	&m944_MI,
	&m945_MI,
	NULL
};
extern TypeInfo t235_TI;
static TypeInfo* t236_TI__nestedTypes[2] =
{
	&t235_TI,
	NULL
};
static MethodInfo* t236_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t236_0_0_0;
extern Il2CppType t236_1_0_0;
struct t236;
TypeInfo t236_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "StencilMaterial", "UnityEngine.UI", t236_MIs, NULL, t236_FIs, NULL, &t29_TI, t236_TI__nestedTypes, NULL, &t236_TI, NULL, t236_VT, &EmptyCustomAttributesCache, &t236_TI, &t236_0_0_0, &t236_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t236), 0, -1, sizeof(t236_SFs), 0, -1, 1048961, 0, false, false, false, false, false, false, false, false, false, true, false, false, 3, 0, 1, 0, 1, 4, 0, 0};
#include "t15.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t15_TI;
#include "t15MD.h"

#include "t202.h"
#include "t162.h"
#include "t148.h"
#include "t147.h"
#include "t150.h"
#include "t151.h"
#include "t152.h"
#include "t149.h"
#include "t3.h"
#include "t238.h"
#include "t182.h"
#include "t163.h"
#include "t184.h"
extern TypeInfo t202_TI;
extern TypeInfo t155_TI;
extern TypeInfo t162_TI;
extern TypeInfo t153_TI;
extern TypeInfo t182_TI;
extern TypeInfo t238_TI;
extern TypeInfo t403_TI;
extern TypeInfo t184_TI;
extern TypeInfo t163_TI;
extern TypeInfo t404_TI;
#include "t202MD.h"
#include "t148MD.h"
#include "t147MD.h"
#include "t153MD.h"
#include "t182MD.h"
#include "t163MD.h"
extern MethodInfo m1715_MI;
extern MethodInfo m1912_MI;
extern MethodInfo m1709_MI;
extern MethodInfo m1913_MI;
extern MethodInfo m953_MI;
extern MethodInfo m1914_MI;
extern MethodInfo m1915_MI;
extern MethodInfo m461_MI;
extern MethodInfo m414_MI;
extern MethodInfo m439_MI;
extern MethodInfo m415_MI;
extern MethodInfo m437_MI;
extern MethodInfo m445_MI;
extern MethodInfo m447_MI;
extern MethodInfo m1740_MI;
extern MethodInfo m446_MI;
extern MethodInfo m428_MI;
extern MethodInfo m429_MI;
extern MethodInfo m420_MI;
extern MethodInfo m421_MI;
extern MethodInfo m422_MI;
extern MethodInfo m423_MI;
extern MethodInfo m424_MI;
extern MethodInfo m425_MI;
extern MethodInfo m426_MI;
extern MethodInfo m427_MI;
extern MethodInfo m416_MI;
extern MethodInfo m417_MI;
extern MethodInfo m430_MI;
extern MethodInfo m431_MI;
extern MethodInfo m432_MI;
extern MethodInfo m433_MI;
extern MethodInfo m434_MI;
extern MethodInfo m435_MI;
extern MethodInfo m418_MI;
extern MethodInfo m419_MI;
extern MethodInfo m454_MI;
extern MethodInfo m1916_MI;
extern MethodInfo m1917_MI;
extern MethodInfo m1918_MI;
extern MethodInfo m980_MI;
extern MethodInfo m949_MI;
extern MethodInfo m1919_MI;
extern MethodInfo m976_MI;
extern MethodInfo m453_MI;
extern MethodInfo m1920_MI;
extern MethodInfo m413_MI;
extern MethodInfo m693_MI;
extern MethodInfo m948_MI;
extern MethodInfo m1921_MI;
extern MethodInfo m394_MI;
extern MethodInfo m979_MI;
extern MethodInfo m699_MI;
extern MethodInfo m700_MI;
extern MethodInfo m466_MI;
extern MethodInfo m1922_MI;
extern MethodInfo m1765_MI;
extern MethodInfo m443_MI;
extern MethodInfo m1774_MI;
extern MethodInfo m981_MI;
extern MethodInfo m1679_MI;
extern MethodInfo m1707_MI;
extern MethodInfo m1706_MI;
extern MethodInfo m1681_MI;
extern MethodInfo m472_MI;
extern MethodInfo m1923_MI;
extern MethodInfo m1924_MI;
extern MethodInfo m1925_MI;
extern MethodInfo m1584_MI;
extern MethodInfo m1926_MI;


extern MethodInfo m946_MI;
 void m946 (t15 * __this, MethodInfo* method){
	{
		t147 * L_0 = m413(NULL, &m413_MI);
		__this->f23 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		__this->f24 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		m693(__this, &m693_MI);
		return;
	}
}
extern MethodInfo m947_MI;
 void m947 (t29 * __this, MethodInfo* method){
	{
		((t15_SFs*)InitializedTypeInfo(&t15_TI)->static_fields)->f27 = (0.0001f);
		return;
	}
}
 t202 * m948 (t15 * __this, MethodInfo* method){
	t202 * V_0 = {0};
	t202 * G_B5_0 = {0};
	t202 * G_B1_0 = {0};
	t15 * G_B3_0 = {0};
	t15 * G_B2_0 = {0};
	t202 * G_B4_0 = {0};
	t15 * G_B4_1 = {0};
	{
		t202 * L_0 = (__this->f25);
		t202 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B5_0 = L_1;
			goto IL_0040;
		}
	}
	{
		t7* L_2 = (__this->f24);
		int32_t L_3 = m1715(L_2, &m1715_MI);
		G_B2_0 = __this;
		if (!L_3)
		{
			G_B3_0 = __this;
			goto IL_0033;
		}
	}
	{
		t7* L_4 = (__this->f24);
		int32_t L_5 = m1715(L_4, &m1715_MI);
		t202 * L_6 = (t202 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t202_TI));
		m1912(L_6, L_5, &m1912_MI);
		G_B4_0 = L_6;
		G_B4_1 = G_B2_0;
		goto IL_0038;
	}

IL_0033:
	{
		t202 * L_7 = (t202 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t202_TI));
		m1709(L_7, &m1709_MI);
		G_B4_0 = L_7;
		G_B4_1 = G_B3_0;
	}

IL_0038:
	{
		t202 * L_8 = G_B4_0;
		V_0 = L_8;
		G_B4_1->f25 = L_8;
		G_B5_0 = V_0;
	}

IL_0040:
	{
		return G_B5_0;
	}
}
 t202 * m949 (t15 * __this, MethodInfo* method){
	t202 * V_0 = {0};
	t202 * G_B2_0 = {0};
	t202 * G_B1_0 = {0};
	{
		t202 * L_0 = (__this->f26);
		t202 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001b;
		}
	}
	{
		t202 * L_2 = (t202 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t202_TI));
		m1709(L_2, &m1709_MI);
		t202 * L_3 = L_2;
		V_0 = L_3;
		__this->f26 = L_3;
		G_B2_0 = V_0;
	}

IL_001b:
	{
		return G_B2_0;
	}
}
extern MethodInfo m950_MI;
 t156 * m950 (t15 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t15_TI));
		bool L_0 = m1297(NULL, (((t15_SFs*)InitializedTypeInfo(&t15_TI)->static_fields)->f28), (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t156 * L_1 = m1913(NULL, &m1913_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t15_TI));
		((t15_SFs*)InitializedTypeInfo(&t15_TI)->static_fields)->f28 = L_1;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t15_TI));
		return (((t15_SFs*)InitializedTypeInfo(&t15_TI)->static_fields)->f28);
	}
}
extern MethodInfo m951_MI;
 t162 * m951 (t15 * __this, MethodInfo* method){
	{
		t148 * L_0 = m953(__this, &m953_MI);
		bool L_1 = m1300(NULL, L_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		t148 * L_2 = m953(__this, &m953_MI);
		t156 * L_3 = m1914(L_2, &m1914_MI);
		bool L_4 = m1300(NULL, L_3, (t41 *)NULL, &m1300_MI);
		if (!L_4)
		{
			goto IL_0053;
		}
	}
	{
		t148 * L_5 = m953(__this, &m953_MI);
		t156 * L_6 = m1914(L_5, &m1914_MI);
		t162 * L_7 = m1915(L_6, &m1915_MI);
		bool L_8 = m1300(NULL, L_7, (t41 *)NULL, &m1300_MI);
		if (!L_8)
		{
			goto IL_0053;
		}
	}
	{
		t148 * L_9 = m953(__this, &m953_MI);
		t156 * L_10 = m1914(L_9, &m1914_MI);
		t162 * L_11 = m1915(L_10, &m1915_MI);
		return L_11;
	}

IL_0053:
	{
		t156 * L_12 = (__this->f5);
		bool L_13 = m1300(NULL, L_12, (t41 *)NULL, &m1300_MI);
		if (!L_13)
		{
			goto IL_0070;
		}
	}
	{
		t156 * L_14 = (__this->f5);
		t162 * L_15 = m1915(L_14, &m1915_MI);
		return L_15;
	}

IL_0070:
	{
		t162 * L_16 = m461(__this, &m461_MI);
		return L_16;
	}
}
extern MethodInfo m952_MI;
 void m952 (t15 * __this, MethodInfo* method){
	{
		bool L_0 = m1293(NULL, __this, &m1293_MI);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t153_TI));
		m439(NULL, __this, &m439_MI);
		return;
	}

IL_0012:
	{
		bool L_1 = (__this->f29);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		t202 * L_2 = m948(__this, &m948_MI);
		m1921(L_2, &m1921_MI);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_3)
		{
			goto IL_0035;
		}
	}
	{
		return;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t141_TI));
		bool L_4 = m394(NULL, &m394_MI);
		if (L_4)
		{
			goto IL_0049;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t141_TI));
		bool L_5 = m393(NULL, &m393_MI);
		if (!L_5)
		{
			goto IL_0054;
		}
	}

IL_0049:
	{
		VirtActionInvoker0::Invoke(&m979_MI, __this);
		goto IL_005a;
	}

IL_0054:
	{
		VirtActionInvoker0::Invoke(&m445_MI, __this);
	}

IL_005a:
	{
		return;
	}
}
 t148 * m953 (t15 * __this, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		t148 * L_1 = m414(L_0, &m414_MI);
		return L_1;
	}
}
extern MethodInfo m954_MI;
 void m954 (t15 * __this, t148 * p0, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		t148 * L_1 = m414(L_0, &m414_MI);
		bool L_2 = m1297(NULL, L_1, p0, &m1297_MI);
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		return;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t153_TI));
		m439(NULL, __this, &m439_MI);
		t147 * L_3 = (__this->f23);
		m415(L_3, p0, &m415_MI);
		m437(NULL, __this, &m437_MI);
		VirtActionInvoker0::Invoke(&m445_MI, __this);
		return;
	}
}
extern MethodInfo m955_MI;
 t7* m955 (t15 * __this, MethodInfo* method){
	{
		t7* L_0 = (__this->f24);
		return L_0;
	}
}
extern MethodInfo m56_MI;
 void m56 (t15 * __this, t7* p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_0 = m1772(NULL, p0, &m1772_MI);
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		t7* L_1 = (__this->f24);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_2 = m1772(NULL, L_1, &m1772_MI);
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		__this->f24 = (((t7_SFs*)(&t7_TI)->static_fields)->f2);
		VirtActionInvoker0::Invoke(&m447_MI, __this);
		goto IL_0056;
	}

IL_0032:
	{
		t7* L_3 = (__this->f24);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		bool L_4 = m1740(NULL, L_3, p0, &m1740_MI);
		if (!L_4)
		{
			goto IL_0056;
		}
	}
	{
		__this->f24 = p0;
		VirtActionInvoker0::Invoke(&m447_MI, __this);
		VirtActionInvoker0::Invoke(&m446_MI, __this);
	}

IL_0056:
	{
		return;
	}
}
extern MethodInfo m956_MI;
 bool m956 (t15 * __this, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		bool L_1 = m428(L_0, &m428_MI);
		return L_1;
	}
}
extern MethodInfo m957_MI;
 void m957 (t15 * __this, bool p0, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		bool L_1 = m428(L_0, &m428_MI);
		if ((((uint32_t)L_1) != ((uint32_t)p0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t147 * L_2 = (__this->f23);
		m429(L_2, p0, &m429_MI);
		VirtActionInvoker0::Invoke(&m447_MI, __this);
		VirtActionInvoker0::Invoke(&m446_MI, __this);
		return;
	}
}
extern MethodInfo m958_MI;
 bool m958 (t15 * __this, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		bool L_1 = m420(L_0, &m420_MI);
		return L_1;
	}
}
extern MethodInfo m959_MI;
 void m959 (t15 * __this, bool p0, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		bool L_1 = m420(L_0, &m420_MI);
		if ((((uint32_t)L_1) != ((uint32_t)p0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t147 * L_2 = (__this->f23);
		m421(L_2, p0, &m421_MI);
		VirtActionInvoker0::Invoke(&m447_MI, __this);
		VirtActionInvoker0::Invoke(&m446_MI, __this);
		return;
	}
}
extern MethodInfo m960_MI;
 int32_t m960 (t15 * __this, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m422(L_0, &m422_MI);
		return L_1;
	}
}
extern MethodInfo m961_MI;
 void m961 (t15 * __this, int32_t p0, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m422(L_0, &m422_MI);
		if ((((uint32_t)L_1) != ((uint32_t)p0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t147 * L_2 = (__this->f23);
		m423(L_2, p0, &m423_MI);
		VirtActionInvoker0::Invoke(&m447_MI, __this);
		VirtActionInvoker0::Invoke(&m446_MI, __this);
		return;
	}
}
extern MethodInfo m962_MI;
 int32_t m962 (t15 * __this, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m424(L_0, &m424_MI);
		return L_1;
	}
}
extern MethodInfo m963_MI;
 void m963 (t15 * __this, int32_t p0, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m424(L_0, &m424_MI);
		if ((((uint32_t)L_1) != ((uint32_t)p0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t147 * L_2 = (__this->f23);
		m425(L_2, p0, &m425_MI);
		VirtActionInvoker0::Invoke(&m447_MI, __this);
		VirtActionInvoker0::Invoke(&m446_MI, __this);
		return;
	}
}
extern MethodInfo m964_MI;
 int32_t m964 (t15 * __this, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m426(L_0, &m426_MI);
		return L_1;
	}
}
extern MethodInfo m965_MI;
 void m965 (t15 * __this, int32_t p0, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m426(L_0, &m426_MI);
		if ((((uint32_t)L_1) != ((uint32_t)p0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t147 * L_2 = (__this->f23);
		m427(L_2, p0, &m427_MI);
		VirtActionInvoker0::Invoke(&m447_MI, __this);
		VirtActionInvoker0::Invoke(&m446_MI, __this);
		return;
	}
}
extern MethodInfo m966_MI;
 int32_t m966 (t15 * __this, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m416(L_0, &m416_MI);
		return L_1;
	}
}
extern MethodInfo m967_MI;
 void m967 (t15 * __this, int32_t p0, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m416(L_0, &m416_MI);
		if ((((uint32_t)L_1) != ((uint32_t)p0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t147 * L_2 = (__this->f23);
		m417(L_2, p0, &m417_MI);
		VirtActionInvoker0::Invoke(&m447_MI, __this);
		VirtActionInvoker0::Invoke(&m446_MI, __this);
		return;
	}
}
extern MethodInfo m968_MI;
 int32_t m968 (t15 * __this, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m430(L_0, &m430_MI);
		return L_1;
	}
}
extern MethodInfo m969_MI;
 void m969 (t15 * __this, int32_t p0, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m430(L_0, &m430_MI);
		if ((((uint32_t)L_1) != ((uint32_t)p0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t147 * L_2 = (__this->f23);
		m431(L_2, p0, &m431_MI);
		VirtActionInvoker0::Invoke(&m447_MI, __this);
		VirtActionInvoker0::Invoke(&m446_MI, __this);
		return;
	}
}
extern MethodInfo m970_MI;
 int32_t m970 (t15 * __this, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m432(L_0, &m432_MI);
		return L_1;
	}
}
extern MethodInfo m971_MI;
 void m971 (t15 * __this, int32_t p0, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m432(L_0, &m432_MI);
		if ((((uint32_t)L_1) != ((uint32_t)p0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t147 * L_2 = (__this->f23);
		m433(L_2, p0, &m433_MI);
		VirtActionInvoker0::Invoke(&m447_MI, __this);
		VirtActionInvoker0::Invoke(&m446_MI, __this);
		return;
	}
}
extern MethodInfo m972_MI;
 float m972 (t15 * __this, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		float L_1 = m434(L_0, &m434_MI);
		return L_1;
	}
}
extern MethodInfo m973_MI;
 void m973 (t15 * __this, float p0, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		float L_1 = m434(L_0, &m434_MI);
		if ((((float)L_1) != ((float)p0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t147 * L_2 = (__this->f23);
		m435(L_2, p0, &m435_MI);
		VirtActionInvoker0::Invoke(&m447_MI, __this);
		VirtActionInvoker0::Invoke(&m446_MI, __this);
		return;
	}
}
extern MethodInfo m974_MI;
 int32_t m974 (t15 * __this, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m418(L_0, &m418_MI);
		return L_1;
	}
}
extern MethodInfo m975_MI;
 void m975 (t15 * __this, int32_t p0, MethodInfo* method){
	{
		t147 * L_0 = (__this->f23);
		int32_t L_1 = m418(L_0, &m418_MI);
		if ((((uint32_t)L_1) != ((uint32_t)p0)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t147 * L_2 = (__this->f23);
		m419(L_2, p0, &m419_MI);
		VirtActionInvoker0::Invoke(&m447_MI, __this);
		VirtActionInvoker0::Invoke(&m446_MI, __this);
		return;
	}
}
 float m976 (t15 * __this, MethodInfo* method){
	t3 * V_0 = {0};
	{
		t3 * L_0 = m454(__this, &m454_MI);
		V_0 = L_0;
		bool L_1 = m1293(NULL, V_0, &m1293_MI);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		return (1.0f);
	}

IL_0018:
	{
		t148 * L_2 = m953(__this, &m953_MI);
		bool L_3 = m1293(NULL, L_2, &m1293_MI);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		t148 * L_4 = m953(__this, &m953_MI);
		bool L_5 = m1916(L_4, &m1916_MI);
		if (!L_5)
		{
			goto IL_003f;
		}
	}

IL_0038:
	{
		float L_6 = m1917(V_0, &m1917_MI);
		return L_6;
	}

IL_003f:
	{
		t147 * L_7 = (__this->f23);
		int32_t L_8 = m416(L_7, &m416_MI);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		t148 * L_9 = m953(__this, &m953_MI);
		int32_t L_10 = m1918(L_9, &m1918_MI);
		if ((((int32_t)L_10) > ((int32_t)0)))
		{
			goto IL_0067;
		}
	}

IL_0061:
	{
		return (1.0f);
	}

IL_0067:
	{
		t148 * L_11 = m953(__this, &m953_MI);
		int32_t L_12 = m1918(L_11, &m1918_MI);
		t147 * L_13 = (__this->f23);
		int32_t L_14 = m416(L_13, &m416_MI);
		return ((float)((float)(((float)L_12))/(float)(((float)L_14))));
	}
}
extern MethodInfo m977_MI;
 void m977 (t15 * __this, MethodInfo* method){
	{
		m699(__this, &m699_MI);
		t202 * L_0 = m948(__this, &m948_MI);
		m1921(L_0, &m1921_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t153_TI));
		m437(NULL, __this, &m437_MI);
		return;
	}
}
extern MethodInfo m978_MI;
 void m978 (t15 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t153_TI));
		m439(NULL, __this, &m439_MI);
		m700(__this, &m700_MI);
		return;
	}
}
 void m979 (t15 * __this, MethodInfo* method){
	{
		t148 * L_0 = m953(__this, &m953_MI);
		bool L_1 = m1300(NULL, L_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m466(__this, &m466_MI);
	}

IL_0017:
	{
		return;
	}
}
 t238  m980 (t15 * __this, t17  p0, MethodInfo* method){
	t238  V_0 = {0};
	float V_1 = 0.0f;
	{
		Initobj (&t238_TI, (&V_0));
		float L_0 = m976(__this, &m976_MI);
		V_1 = L_0;
		t17  L_1 = m1668(NULL, p0, V_1, &m1668_MI);
		t17  L_2 = m1667(NULL, &m1667_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t15_TI));
		t17  L_3 = m1668(NULL, L_2, (((t15_SFs*)InitializedTypeInfo(&t15_TI)->static_fields)->f27), &m1668_MI);
		t17  L_4 = m1670(NULL, L_1, L_3, &m1670_MI);
		(&V_0)->f13 = L_4;
		t148 * L_5 = m953(__this, &m953_MI);
		bool L_6 = m1300(NULL, L_5, (t41 *)NULL, &m1300_MI);
		if (!L_6)
		{
			goto IL_00be;
		}
	}
	{
		t148 * L_7 = m953(__this, &m953_MI);
		bool L_8 = m1916(L_7, &m1916_MI);
		if (!L_8)
		{
			goto IL_00be;
		}
	}
	{
		t147 * L_9 = (__this->f23);
		int32_t L_10 = m416(L_9, &m416_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		int32_t L_11 = m1922(NULL, ((float)((float)(((float)L_10))*(float)V_1)), &m1922_MI);
		int32_t L_12 = m1765(NULL, L_11, ((int32_t)1000), &m1765_MI);
		(&V_0)->f2 = L_12;
		t147 * L_13 = (__this->f23);
		int32_t L_14 = m422(L_13, &m422_MI);
		int32_t L_15 = m1922(NULL, ((float)((float)(((float)L_14))*(float)V_1)), &m1922_MI);
		int32_t L_16 = m1765(NULL, L_15, ((int32_t)1000), &m1765_MI);
		(&V_0)->f8 = L_16;
		t147 * L_17 = (__this->f23);
		int32_t L_18 = m424(L_17, &m424_MI);
		int32_t L_19 = m1922(NULL, ((float)((float)(((float)L_18))*(float)V_1)), &m1922_MI);
		int32_t L_20 = m1765(NULL, L_19, ((int32_t)1000), &m1765_MI);
		(&V_0)->f9 = L_20;
	}

IL_00be:
	{
		t147 * L_21 = (__this->f23);
		int32_t L_22 = m426(L_21, &m426_MI);
		(&V_0)->f6 = L_22;
		t132  L_23 = m443(__this, &m443_MI);
		(&V_0)->f1 = L_23;
		t148 * L_24 = m953(__this, &m953_MI);
		(&V_0)->f0 = L_24;
		t2 * L_25 = m453(__this, &m453_MI);
		t17  L_26 = m1658(L_25, &m1658_MI);
		(&V_0)->f14 = L_26;
		t147 * L_27 = (__this->f23);
		bool L_28 = m428(L_27, &m428_MI);
		(&V_0)->f4 = L_28;
		t147 * L_29 = (__this->f23);
		float L_30 = m434(L_29, &m434_MI);
		(&V_0)->f3 = L_30;
		t147 * L_31 = (__this->f23);
		int32_t L_32 = m418(L_31, &m418_MI);
		(&V_0)->f5 = L_32;
		t147 * L_33 = (__this->f23);
		bool L_34 = m420(L_33, &m420_MI);
		(&V_0)->f7 = L_34;
		(&V_0)->f10 = 0;
		t147 * L_35 = (__this->f23);
		int32_t L_36 = m430(L_35, &m430_MI);
		(&V_0)->f12 = L_36;
		t147 * L_37 = (__this->f23);
		int32_t L_38 = m432(L_37, &m432_MI);
		(&V_0)->f11 = L_38;
		return V_0;
	}
}
 t17  m981 (t29 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		V_0 = p0;
		if (V_0 == 0)
		{
			goto IL_0091;
		}
		if (V_0 == 1)
		{
			goto IL_00a1;
		}
		if (V_0 == 2)
		{
			goto IL_00b1;
		}
		if (V_0 == 3)
		{
			goto IL_0061;
		}
		if (V_0 == 4)
		{
			goto IL_0071;
		}
		if (V_0 == 5)
		{
			goto IL_0081;
		}
		if (V_0 == 6)
		{
			goto IL_0031;
		}
		if (V_0 == 7)
		{
			goto IL_0041;
		}
		if (V_0 == 8)
		{
			goto IL_0051;
		}
	}
	{
		goto IL_00c1;
	}

IL_0031:
	{
		t17  L_0 = {0};
		m62(&L_0, (0.0f), (0.0f), &m62_MI);
		return L_0;
	}

IL_0041:
	{
		t17  L_1 = {0};
		m62(&L_1, (0.5f), (0.0f), &m62_MI);
		return L_1;
	}

IL_0051:
	{
		t17  L_2 = {0};
		m62(&L_2, (1.0f), (0.0f), &m62_MI);
		return L_2;
	}

IL_0061:
	{
		t17  L_3 = {0};
		m62(&L_3, (0.0f), (0.5f), &m62_MI);
		return L_3;
	}

IL_0071:
	{
		t17  L_4 = {0};
		m62(&L_4, (0.5f), (0.5f), &m62_MI);
		return L_4;
	}

IL_0081:
	{
		t17  L_5 = {0};
		m62(&L_5, (1.0f), (0.5f), &m62_MI);
		return L_5;
	}

IL_0091:
	{
		t17  L_6 = {0};
		m62(&L_6, (0.0f), (1.0f), &m62_MI);
		return L_6;
	}

IL_00a1:
	{
		t17  L_7 = {0};
		m62(&L_7, (0.5f), (1.0f), &m62_MI);
		return L_7;
	}

IL_00b1:
	{
		t17  L_8 = {0};
		m62(&L_8, (1.0f), (1.0f), &m62_MI);
		return L_8;
	}

IL_00c1:
	{
		t17  L_9 = m1394(NULL, &m1394_MI);
		return L_9;
	}
}
extern MethodInfo m982_MI;
 void m982 (t15 * __this, t163 * p0, MethodInfo* method){
	t17  V_0 = {0};
	t238  V_1 = {0};
	t164  V_2 = {0};
	t17  V_3 = {0};
	t17  V_4 = {0};
	t17  V_5 = {0};
	t29* V_6 = {0};
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	t184  V_9 = {0};
	int32_t V_10 = 0;
	t184  V_11 = {0};
	t164  V_12 = {0};
	t17 * G_B4_0 = {0};
	t17 * G_B3_0 = {0};
	float G_B5_0 = 0.0f;
	t17 * G_B5_1 = {0};
	t17 * G_B7_0 = {0};
	t17 * G_B6_0 = {0};
	float G_B8_0 = 0.0f;
	t17 * G_B8_1 = {0};
	{
		t148 * L_0 = m953(__this, &m953_MI);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		__this->f29 = 1;
		t2 * L_2 = m453(__this, &m453_MI);
		t164  L_3 = m1572(L_2, &m1572_MI);
		V_12 = L_3;
		t17  L_4 = m1653((&V_12), &m1653_MI);
		V_0 = L_4;
		t238  L_5 = m980(__this, V_0, &m980_MI);
		V_1 = L_5;
		t202 * L_6 = m948(__this, &m948_MI);
		t7* L_7 = (__this->f24);
		m1774(L_6, L_7, V_1, &m1774_MI);
		t2 * L_8 = m453(__this, &m453_MI);
		t164  L_9 = m1572(L_8, &m1572_MI);
		V_2 = L_9;
		t147 * L_10 = (__this->f23);
		int32_t L_11 = m426(L_10, &m426_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t15_TI));
		t17  L_12 = m981(NULL, L_11, &m981_MI);
		V_3 = L_12;
		t17  L_13 = m1394(NULL, &m1394_MI);
		V_4 = L_13;
		float L_14 = ((&V_3)->f1);
		G_B3_0 = (&V_4);
		if ((((float)L_14) != ((float)(1.0f))))
		{
			G_B4_0 = (&V_4);
			goto IL_008c;
		}
	}
	{
		float L_15 = m1679((&V_2), &m1679_MI);
		G_B5_0 = L_15;
		G_B5_1 = G_B3_0;
		goto IL_0093;
	}

IL_008c:
	{
		float L_16 = m1707((&V_2), &m1707_MI);
		G_B5_0 = L_16;
		G_B5_1 = G_B4_0;
	}

IL_0093:
	{
		G_B5_1->f1 = G_B5_0;
		float L_17 = ((&V_3)->f2);
		G_B6_0 = (&V_4);
		if ((((float)L_17) != ((float)(0.0f))))
		{
			G_B7_0 = (&V_4);
			goto IL_00b7;
		}
	}
	{
		float L_18 = m1706((&V_2), &m1706_MI);
		G_B8_0 = L_18;
		G_B8_1 = G_B6_0;
		goto IL_00be;
	}

IL_00b7:
	{
		float L_19 = m1681((&V_2), &m1681_MI);
		G_B8_0 = L_19;
		G_B8_1 = G_B7_0;
	}

IL_00be:
	{
		G_B8_1->f2 = G_B8_0;
		t17  L_20 = m472(__this, V_4, &m472_MI);
		t17  L_21 = m1416(NULL, L_20, V_4, &m1416_MI);
		V_5 = L_21;
		t202 * L_22 = m948(__this, &m948_MI);
		t29* L_23 = m1923(L_22, &m1923_MI);
		V_6 = L_23;
		float L_24 = m976(__this, &m976_MI);
		V_7 = ((float)((float)(1.0f)/(float)L_24));
		t17  L_25 = m1394(NULL, &m1394_MI);
		bool L_26 = m1792(NULL, V_5, L_25, &m1792_MI);
		if (!L_26)
		{
			goto IL_017c;
		}
	}
	{
		V_8 = 0;
		goto IL_0169;
	}

IL_0108:
	{
		t184  L_27 = (t184 )InterfaceFuncInvoker1< t184 , int32_t >::Invoke(&m1924_MI, V_6, V_8);
		V_9 = L_27;
		t184 * L_28 = (&V_9);
		t23  L_29 = (L_28->f0);
		t23  L_30 = m1925(NULL, L_29, V_7, &m1925_MI);
		L_28->f0 = L_30;
		t23 * L_31 = &((&V_9)->f0);
		t23 * L_32 = L_31;
		float L_33 = (L_32->f1);
		float L_34 = ((&V_5)->f1);
		L_32->f1 = ((float)(L_33+L_34));
		t23 * L_35 = &((&V_9)->f0);
		t23 * L_36 = L_35;
		float L_37 = (L_36->f2);
		float L_38 = ((&V_5)->f2);
		L_36->f2 = ((float)(L_37+L_38));
		VirtActionInvoker1< t184  >::Invoke(&m1584_MI, p0, V_9);
		V_8 = ((int32_t)(V_8+1));
	}

IL_0169:
	{
		int32_t L_39 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1926_MI, V_6);
		if ((((int32_t)V_8) < ((int32_t)L_39)))
		{
			goto IL_0108;
		}
	}
	{
		goto IL_01bf;
	}

IL_017c:
	{
		V_10 = 0;
		goto IL_01b1;
	}

IL_0184:
	{
		t184  L_40 = (t184 )InterfaceFuncInvoker1< t184 , int32_t >::Invoke(&m1924_MI, V_6, V_10);
		V_11 = L_40;
		t184 * L_41 = (&V_11);
		t23  L_42 = (L_41->f0);
		t23  L_43 = m1925(NULL, L_42, V_7, &m1925_MI);
		L_41->f0 = L_43;
		VirtActionInvoker1< t184  >::Invoke(&m1584_MI, p0, V_11);
		V_10 = ((int32_t)(V_10+1));
	}

IL_01b1:
	{
		int32_t L_44 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1926_MI, V_6);
		if ((((int32_t)V_10) < ((int32_t)L_44)))
		{
			goto IL_0184;
		}
	}

IL_01bf:
	{
		__this->f29 = 0;
		return;
	}
}
extern MethodInfo m983_MI;
 void m983 (t15 * __this, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m984_MI;
 void m984 (t15 * __this, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m985_MI;
 float m985 (t15 * __this, MethodInfo* method){
	{
		return (0.0f);
	}
}
extern MethodInfo m986_MI;
 float m986 (t15 * __this, MethodInfo* method){
	t238  V_0 = {0};
	{
		t17  L_0 = m1394(NULL, &m1394_MI);
		t238  L_1 = m980(__this, L_0, &m980_MI);
		V_0 = L_1;
		t202 * L_2 = m949(__this, &m949_MI);
		t7* L_3 = (__this->f24);
		float L_4 = m1919(L_2, L_3, V_0, &m1919_MI);
		float L_5 = m976(__this, &m976_MI);
		return ((float)((float)L_4/(float)L_5));
	}
}
extern MethodInfo m987_MI;
 float m987 (t15 * __this, MethodInfo* method){
	{
		return (-1.0f);
	}
}
extern MethodInfo m988_MI;
 float m988 (t15 * __this, MethodInfo* method){
	{
		return (0.0f);
	}
}
extern MethodInfo m989_MI;
 float m989 (t15 * __this, MethodInfo* method){
	t238  V_0 = {0};
	t164  V_1 = {0};
	t17  V_2 = {0};
	{
		t2 * L_0 = m453(__this, &m453_MI);
		t164  L_1 = m1572(L_0, &m1572_MI);
		V_1 = L_1;
		t17  L_2 = m1653((&V_1), &m1653_MI);
		V_2 = L_2;
		float L_3 = ((&V_2)->f1);
		t17  L_4 = {0};
		m62(&L_4, L_3, (0.0f), &m62_MI);
		t238  L_5 = m980(__this, L_4, &m980_MI);
		V_0 = L_5;
		t202 * L_6 = m949(__this, &m949_MI);
		t7* L_7 = (__this->f24);
		float L_8 = m1920(L_6, L_7, V_0, &m1920_MI);
		float L_9 = m976(__this, &m976_MI);
		return ((float)((float)L_8/(float)L_9));
	}
}
extern MethodInfo m990_MI;
 float m990 (t15 * __this, MethodInfo* method){
	{
		return (-1.0f);
	}
}
extern MethodInfo m991_MI;
 int32_t m991 (t15 * __this, MethodInfo* method){
	{
		return 0;
	}
}
// Metadata Definition UnityEngine.UI.Text
extern Il2CppType t147_0_0_1;
extern CustomAttributesCache t15__CustomAttributeCache_m_FontData;
FieldInfo t15_f23_FieldInfo = 
{
	"m_FontData", &t147_0_0_1, &t15_TI, offsetof(t15, f23), &t15__CustomAttributeCache_m_FontData};
extern Il2CppType t7_0_0_4;
extern CustomAttributesCache t15__CustomAttributeCache_m_Text;
FieldInfo t15_f24_FieldInfo = 
{
	"m_Text", &t7_0_0_4, &t15_TI, offsetof(t15, f24), &t15__CustomAttributeCache_m_Text};
extern Il2CppType t202_0_0_1;
FieldInfo t15_f25_FieldInfo = 
{
	"m_TextCache", &t202_0_0_1, &t15_TI, offsetof(t15, f25), &EmptyCustomAttributesCache};
extern Il2CppType t202_0_0_1;
FieldInfo t15_f26_FieldInfo = 
{
	"m_TextCacheForLayout", &t202_0_0_1, &t15_TI, offsetof(t15, f26), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_17;
FieldInfo t15_f27_FieldInfo = 
{
	"kEpsilon", &t22_0_0_17, &t15_TI, offsetof(t15_SFs, f27), &EmptyCustomAttributesCache};
extern Il2CppType t156_0_0_20;
FieldInfo t15_f28_FieldInfo = 
{
	"s_DefaultText", &t156_0_0_20, &t15_TI, offsetof(t15_SFs, f28), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_129;
FieldInfo t15_f29_FieldInfo = 
{
	"m_DisableFontTextureRebuiltCallback", &t40_0_0_129, &t15_TI, offsetof(t15, f29), &EmptyCustomAttributesCache};
static FieldInfo* t15_FIs[] =
{
	&t15_f23_FieldInfo,
	&t15_f24_FieldInfo,
	&t15_f25_FieldInfo,
	&t15_f26_FieldInfo,
	&t15_f27_FieldInfo,
	&t15_f28_FieldInfo,
	&t15_f29_FieldInfo,
	NULL
};
static PropertyInfo t15____cachedTextGenerator_PropertyInfo = 
{
	&t15_TI, "cachedTextGenerator", &m948_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____cachedTextGeneratorForLayout_PropertyInfo = 
{
	&t15_TI, "cachedTextGeneratorForLayout", &m949_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____defaultMaterial_PropertyInfo = 
{
	&t15_TI, "defaultMaterial", &m950_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____mainTexture_PropertyInfo = 
{
	&t15_TI, "mainTexture", &m951_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____font_PropertyInfo = 
{
	&t15_TI, "font", &m953_MI, &m954_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____text_PropertyInfo = 
{
	&t15_TI, "text", &m955_MI, &m56_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____supportRichText_PropertyInfo = 
{
	&t15_TI, "supportRichText", &m956_MI, &m957_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____resizeTextForBestFit_PropertyInfo = 
{
	&t15_TI, "resizeTextForBestFit", &m958_MI, &m959_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____resizeTextMinSize_PropertyInfo = 
{
	&t15_TI, "resizeTextMinSize", &m960_MI, &m961_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____resizeTextMaxSize_PropertyInfo = 
{
	&t15_TI, "resizeTextMaxSize", &m962_MI, &m963_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____alignment_PropertyInfo = 
{
	&t15_TI, "alignment", &m964_MI, &m965_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____fontSize_PropertyInfo = 
{
	&t15_TI, "fontSize", &m966_MI, &m967_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____horizontalOverflow_PropertyInfo = 
{
	&t15_TI, "horizontalOverflow", &m968_MI, &m969_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____verticalOverflow_PropertyInfo = 
{
	&t15_TI, "verticalOverflow", &m970_MI, &m971_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____lineSpacing_PropertyInfo = 
{
	&t15_TI, "lineSpacing", &m972_MI, &m973_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____fontStyle_PropertyInfo = 
{
	&t15_TI, "fontStyle", &m974_MI, &m975_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____pixelsPerUnit_PropertyInfo = 
{
	&t15_TI, "pixelsPerUnit", &m976_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____minWidth_PropertyInfo = 
{
	&t15_TI, "minWidth", &m985_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____preferredWidth_PropertyInfo = 
{
	&t15_TI, "preferredWidth", &m986_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____flexibleWidth_PropertyInfo = 
{
	&t15_TI, "flexibleWidth", &m987_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____minHeight_PropertyInfo = 
{
	&t15_TI, "minHeight", &m988_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____preferredHeight_PropertyInfo = 
{
	&t15_TI, "preferredHeight", &m989_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____flexibleHeight_PropertyInfo = 
{
	&t15_TI, "flexibleHeight", &m990_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t15____layoutPriority_PropertyInfo = 
{
	&t15_TI, "layoutPriority", &m991_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t15_PIs[] =
{
	&t15____cachedTextGenerator_PropertyInfo,
	&t15____cachedTextGeneratorForLayout_PropertyInfo,
	&t15____defaultMaterial_PropertyInfo,
	&t15____mainTexture_PropertyInfo,
	&t15____font_PropertyInfo,
	&t15____text_PropertyInfo,
	&t15____supportRichText_PropertyInfo,
	&t15____resizeTextForBestFit_PropertyInfo,
	&t15____resizeTextMinSize_PropertyInfo,
	&t15____resizeTextMaxSize_PropertyInfo,
	&t15____alignment_PropertyInfo,
	&t15____fontSize_PropertyInfo,
	&t15____horizontalOverflow_PropertyInfo,
	&t15____verticalOverflow_PropertyInfo,
	&t15____lineSpacing_PropertyInfo,
	&t15____fontStyle_PropertyInfo,
	&t15____pixelsPerUnit_PropertyInfo,
	&t15____minWidth_PropertyInfo,
	&t15____preferredWidth_PropertyInfo,
	&t15____flexibleWidth_PropertyInfo,
	&t15____minHeight_PropertyInfo,
	&t15____preferredHeight_PropertyInfo,
	&t15____flexibleHeight_PropertyInfo,
	&t15____layoutPriority_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m946_MI = 
{
	".ctor", (methodPointerType)&m946, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 928, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m947_MI = 
{
	".cctor", (methodPointerType)&m947, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 929, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t202_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m948_MI = 
{
	"get_cachedTextGenerator", (methodPointerType)&m948, &t15_TI, &t202_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 930, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t202_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m949_MI = 
{
	"get_cachedTextGeneratorForLayout", (methodPointerType)&m949, &t15_TI, &t202_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 931, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t156_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m950_MI = 
{
	"get_defaultMaterial", (methodPointerType)&m950, &t15_TI, &t156_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 22, 0, false, false, 932, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t162_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m951_MI = 
{
	"get_mainTexture", (methodPointerType)&m951, &t15_TI, &t162_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2246, 0, 26, 0, false, false, 933, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m952_MI = 
{
	"FontTextureChanged", (methodPointerType)&m952, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 934, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t148_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m953_MI = 
{
	"get_font", (methodPointerType)&m953, &t15_TI, &t148_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 935, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t148_0_0_0;
extern Il2CppType t148_0_0_0;
static ParameterInfo t15_m954_ParameterInfos[] = 
{
	{"value", 0, 134218287, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m954_MI = 
{
	"set_font", (methodPointerType)&m954, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t15_m954_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 936, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m955_MI = 
{
	"get_text", (methodPointerType)&m955, &t15_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2502, 0, 46, 0, false, false, 937, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
static ParameterInfo t15_m56_ParameterInfos[] = 
{
	{"value", 0, 134218288, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m56_MI = 
{
	"set_text", (methodPointerType)&m56, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t15_m56_ParameterInfos, &EmptyCustomAttributesCache, 2502, 0, 47, 1, false, false, 938, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m956_MI = 
{
	"get_supportRichText", (methodPointerType)&m956, &t15_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 939, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t15_m957_ParameterInfos[] = 
{
	{"value", 0, 134218289, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m957_MI = 
{
	"set_supportRichText", (methodPointerType)&m957, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t15_m957_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 940, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m958_MI = 
{
	"get_resizeTextForBestFit", (methodPointerType)&m958, &t15_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 941, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t15_m959_ParameterInfos[] = 
{
	{"value", 0, 134218290, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m959_MI = 
{
	"set_resizeTextForBestFit", (methodPointerType)&m959, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t15_m959_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 942, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m960_MI = 
{
	"get_resizeTextMinSize", (methodPointerType)&m960, &t15_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 943, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t15_m961_ParameterInfos[] = 
{
	{"value", 0, 134218291, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m961_MI = 
{
	"set_resizeTextMinSize", (methodPointerType)&m961, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t15_m961_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 944, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m962_MI = 
{
	"get_resizeTextMaxSize", (methodPointerType)&m962, &t15_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 945, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t15_m963_ParameterInfos[] = 
{
	{"value", 0, 134218292, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m963_MI = 
{
	"set_resizeTextMaxSize", (methodPointerType)&m963, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t15_m963_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 946, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t150_0_0_0;
extern void* RuntimeInvoker_t150 (MethodInfo* method, void* obj, void** args);
MethodInfo m964_MI = 
{
	"get_alignment", (methodPointerType)&m964, &t15_TI, &t150_0_0_0, RuntimeInvoker_t150, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 947, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t150_0_0_0;
extern Il2CppType t150_0_0_0;
static ParameterInfo t15_m965_ParameterInfos[] = 
{
	{"value", 0, 134218293, &EmptyCustomAttributesCache, &t150_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m965_MI = 
{
	"set_alignment", (methodPointerType)&m965, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t15_m965_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 948, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m966_MI = 
{
	"get_fontSize", (methodPointerType)&m966, &t15_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 949, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t15_m967_ParameterInfos[] = 
{
	{"value", 0, 134218294, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m967_MI = 
{
	"set_fontSize", (methodPointerType)&m967, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t15_m967_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 950, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t151_0_0_0;
extern void* RuntimeInvoker_t151 (MethodInfo* method, void* obj, void** args);
MethodInfo m968_MI = 
{
	"get_horizontalOverflow", (methodPointerType)&m968, &t15_TI, &t151_0_0_0, RuntimeInvoker_t151, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 951, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t151_0_0_0;
extern Il2CppType t151_0_0_0;
static ParameterInfo t15_m969_ParameterInfos[] = 
{
	{"value", 0, 134218295, &EmptyCustomAttributesCache, &t151_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m969_MI = 
{
	"set_horizontalOverflow", (methodPointerType)&m969, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t15_m969_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 952, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t152_0_0_0;
extern void* RuntimeInvoker_t152 (MethodInfo* method, void* obj, void** args);
MethodInfo m970_MI = 
{
	"get_verticalOverflow", (methodPointerType)&m970, &t15_TI, &t152_0_0_0, RuntimeInvoker_t152, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 953, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t152_0_0_0;
extern Il2CppType t152_0_0_0;
static ParameterInfo t15_m971_ParameterInfos[] = 
{
	{"value", 0, 134218296, &EmptyCustomAttributesCache, &t152_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m971_MI = 
{
	"set_verticalOverflow", (methodPointerType)&m971, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t15_m971_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 954, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m972_MI = 
{
	"get_lineSpacing", (methodPointerType)&m972, &t15_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 955, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t15_m973_ParameterInfos[] = 
{
	{"value", 0, 134218297, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m973_MI = 
{
	"set_lineSpacing", (methodPointerType)&m973, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t15_m973_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 956, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t149_0_0_0;
extern void* RuntimeInvoker_t149 (MethodInfo* method, void* obj, void** args);
MethodInfo m974_MI = 
{
	"get_fontStyle", (methodPointerType)&m974, &t15_TI, &t149_0_0_0, RuntimeInvoker_t149, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 957, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t149_0_0_0;
extern Il2CppType t149_0_0_0;
static ParameterInfo t15_m975_ParameterInfos[] = 
{
	{"value", 0, 134218298, &EmptyCustomAttributesCache, &t149_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m975_MI = 
{
	"set_fontStyle", (methodPointerType)&m975, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t15_m975_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 958, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m976_MI = 
{
	"get_pixelsPerUnit", (methodPointerType)&m976, &t15_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 959, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m977_MI = 
{
	"OnEnable", (methodPointerType)&m977, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 5, 0, false, false, 960, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m978_MI = 
{
	"OnDisable", (methodPointerType)&m978, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 7, 0, false, false, 961, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m979_MI = 
{
	"UpdateGeometry", (methodPointerType)&m979, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 28, 0, false, false, 962, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
static ParameterInfo t15_m980_ParameterInfos[] = 
{
	{"extents", 0, 134218299, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t238_0_0_0;
extern void* RuntimeInvoker_t238_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m980_MI = 
{
	"GetGenerationSettings", (methodPointerType)&m980, &t15_TI, &t238_0_0_0, RuntimeInvoker_t238_t17, t15_m980_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 963, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t150_0_0_0;
static ParameterInfo t15_m981_ParameterInfos[] = 
{
	{"anchor", 0, 134218300, &EmptyCustomAttributesCache, &t150_0_0_0},
};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m981_MI = 
{
	"GetTextAnchorPivot", (methodPointerType)&m981, &t15_TI, &t17_0_0_0, RuntimeInvoker_t17_t44, t15_m981_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 964, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t163_0_0_0;
extern Il2CppType t163_0_0_0;
static ParameterInfo t15_m982_ParameterInfos[] = 
{
	{"vbo", 0, 134218301, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m982_MI = 
{
	"OnFillVBO", (methodPointerType)&m982, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t15_m982_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 30, 1, false, false, 965, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m983_MI = 
{
	"CalculateLayoutInputHorizontal", (methodPointerType)&m983, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 48, 0, false, false, 966, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m984_MI = 
{
	"CalculateLayoutInputVertical", (methodPointerType)&m984, &t15_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 49, 0, false, false, 967, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m985_MI = 
{
	"get_minWidth", (methodPointerType)&m985, &t15_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 50, 0, false, false, 968, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m986_MI = 
{
	"get_preferredWidth", (methodPointerType)&m986, &t15_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 51, 0, false, false, 969, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m987_MI = 
{
	"get_flexibleWidth", (methodPointerType)&m987, &t15_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 52, 0, false, false, 970, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m988_MI = 
{
	"get_minHeight", (methodPointerType)&m988, &t15_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 53, 0, false, false, 971, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m989_MI = 
{
	"get_preferredHeight", (methodPointerType)&m989, &t15_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 54, 0, false, false, 972, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m990_MI = 
{
	"get_flexibleHeight", (methodPointerType)&m990, &t15_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 55, 0, false, false, 973, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m991_MI = 
{
	"get_layoutPriority", (methodPointerType)&m991, &t15_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2502, 0, 56, 0, false, false, 974, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t15_MIs[] =
{
	&m946_MI,
	&m947_MI,
	&m948_MI,
	&m949_MI,
	&m950_MI,
	&m951_MI,
	&m952_MI,
	&m953_MI,
	&m954_MI,
	&m955_MI,
	&m56_MI,
	&m956_MI,
	&m957_MI,
	&m958_MI,
	&m959_MI,
	&m960_MI,
	&m961_MI,
	&m962_MI,
	&m963_MI,
	&m964_MI,
	&m965_MI,
	&m966_MI,
	&m967_MI,
	&m968_MI,
	&m969_MI,
	&m970_MI,
	&m971_MI,
	&m972_MI,
	&m973_MI,
	&m974_MI,
	&m975_MI,
	&m976_MI,
	&m977_MI,
	&m978_MI,
	&m979_MI,
	&m980_MI,
	&m981_MI,
	&m982_MI,
	&m983_MI,
	&m984_MI,
	&m985_MI,
	&m986_MI,
	&m987_MI,
	&m988_MI,
	&m989_MI,
	&m990_MI,
	&m991_MI,
	NULL
};
extern MethodInfo m449_MI;
extern MethodInfo m450_MI;
extern MethodInfo m701_MI;
extern MethodInfo m469_MI;
extern MethodInfo m465_MI;
extern MethodInfo m487_MI;
extern MethodInfo m486_MI;
extern MethodInfo m704_MI;
extern MethodInfo m696_MI;
extern MethodInfo m697_MI;
extern MethodInfo m460_MI;
extern MethodInfo m467_MI;
extern MethodInfo m470_MI;
extern MethodInfo m471_MI;
extern MethodInfo m702_MI;
static MethodInfo* t15_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m977_MI,
	&m171_MI,
	&m978_MI,
	&m173_MI,
	&m174_MI,
	&m449_MI,
	&m450_MI,
	&m701_MI,
	&m469_MI,
	&m179_MI,
	&m465_MI,
	&m487_MI,
	&m486_MI,
	&m445_MI,
	&m446_MI,
	&m447_MI,
	&m704_MI,
	&m950_MI,
	&m696_MI,
	&m697_MI,
	&m460_MI,
	&m951_MI,
	&m465_MI,
	&m979_MI,
	&m467_MI,
	&m982_MI,
	&m470_MI,
	&m471_MI,
	&m486_MI,
	&m487_MI,
	&m702_MI,
	&m702_MI,
	&m983_MI,
	&m984_MI,
	&m985_MI,
	&m986_MI,
	&m987_MI,
	&m988_MI,
	&m989_MI,
	&m990_MI,
	&m991_MI,
	&m955_MI,
	&m56_MI,
	&m983_MI,
	&m984_MI,
	&m985_MI,
	&m986_MI,
	&m987_MI,
	&m988_MI,
	&m989_MI,
	&m990_MI,
	&m991_MI,
};
extern TypeInfo t271_TI;
static TypeInfo* t15_ITIs[] = 
{
	&t271_TI,
};
extern TypeInfo t369_TI;
static Il2CppInterfaceOffsetPair t15_IOs[] = 
{
	{ &t369_TI, 35},
	{ &t145_TI, 15},
	{ &t271_TI, 37},
};
void t15_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("UI/Text"), 11, &m1519_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t15_CustomAttributesCacheGenerator_m_FontData(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t405_TI;
#include "t405.h"
#include "t405MD.h"
extern MethodInfo m1927_MI;
void t15_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t405 * tmp;
		tmp = (t405 *)il2cpp_codegen_object_new (&t405_TI);
		m1927(tmp, 3, 10, &m1927_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t15__CustomAttributeCache = {
1,
NULL,
&t15_CustomAttributesCacheGenerator
};
CustomAttributesCache t15__CustomAttributeCache_m_FontData = {
1,
NULL,
&t15_CustomAttributesCacheGenerator_m_FontData
};
CustomAttributesCache t15__CustomAttributeCache_m_Text = {
2,
NULL,
&t15_CustomAttributesCacheGenerator_m_Text
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t15_0_0_0;
extern Il2CppType t15_1_0_0;
struct t15;
extern CustomAttributesCache t15__CustomAttributeCache;
extern CustomAttributesCache t15__CustomAttributeCache_m_FontData;
extern CustomAttributesCache t15__CustomAttributeCache_m_Text;
TypeInfo t15_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Text", "UnityEngine.UI", t15_MIs, t15_PIs, t15_FIs, NULL, &t182_TI, NULL, NULL, &t15_TI, t15_ITIs, t15_VT, &t15__CustomAttributeCache, &t15_TI, &t15_0_0_0, &t15_1_0_0, t15_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t15), 0, -1, sizeof(t15_SFs), 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, true, false, false, 47, 24, 7, 0, 0, 57, 1, 3};
#include "t239.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t239_TI;
#include "t239MD.h"



// Metadata Definition UnityEngine.UI.Toggle/ToggleTransition
extern Il2CppType t44_0_0_1542;
FieldInfo t239_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t239_TI, offsetof(t239, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t239_0_0_32854;
FieldInfo t239_f2_FieldInfo = 
{
	"None", &t239_0_0_32854, &t239_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t239_0_0_32854;
FieldInfo t239_f3_FieldInfo = 
{
	"Fade", &t239_0_0_32854, &t239_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t239_FIs[] =
{
	&t239_f1_FieldInfo,
	&t239_f2_FieldInfo,
	&t239_f3_FieldInfo,
	NULL
};
static const int32_t t239_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t239_f2_DefaultValue = 
{
	&t239_f2_FieldInfo, { (char*)&t239_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t239_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t239_f3_DefaultValue = 
{
	&t239_f3_FieldInfo, { (char*)&t239_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t239_FDVs[] = 
{
	&t239_f2_DefaultValue,
	&t239_f3_DefaultValue,
	NULL
};
static MethodInfo* t239_MIs[] =
{
	NULL
};
static MethodInfo* t239_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t239_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t239_0_0_0;
extern Il2CppType t239_1_0_0;
extern TypeInfo t39_TI;
TypeInfo t239_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ToggleTransition", "", t239_MIs, NULL, t239_FIs, NULL, &t49_TI, NULL, &t39_TI, &t44_TI, NULL, t239_VT, &EmptyCustomAttributesCache, &t44_TI, &t239_0_0_0, &t239_1_0_0, t239_IOs, NULL, NULL, t239_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t239)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 258, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 3, 0, 0, 23, 0, 3};
#include "t240.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t240_TI;
#include "t240MD.h"

#include "t241MD.h"
extern MethodInfo m1928_MI;


extern MethodInfo m992_MI;
 void m992 (t240 * __this, MethodInfo* method){
	{
		m1928(__this, &m1928_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.Toggle/ToggleEvent
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m992_MI = 
{
	".ctor", (methodPointerType)&m992, &t240_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 993, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t240_MIs[] =
{
	&m992_MI,
	NULL
};
extern MethodInfo m1929_MI;
extern MethodInfo m1930_MI;
static MethodInfo* t240_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m1929_MI,
	&m1930_MI,
};
static Il2CppInterfaceOffsetPair t240_IOs[] = 
{
	{ &t301_TI, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t240_0_0_0;
extern Il2CppType t240_1_0_0;
extern TypeInfo t241_TI;
struct t240;
TypeInfo t240_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ToggleEvent", "", t240_MIs, NULL, NULL, NULL, &t241_TI, NULL, &t39_TI, &t240_TI, NULL, t240_VT, &EmptyCustomAttributesCache, &t240_TI, &t240_0_0_0, &t240_1_0_0, t240_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t240), 0, -1, 0, 0, -1, 1056770, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 8, 0, 1};
#include "t39.h"
#ifndef _MSC_VER
#else
#endif
#include "t39MD.h"

#include "t242.h"
#include "t242MD.h"
extern MethodInfo m999_MI;
extern MethodInfo m1003_MI;
extern MethodInfo m1001_MI;
extern MethodInfo m1015_MI;
extern MethodInfo m1016_MI;
extern MethodInfo m58_MI;
extern MethodInfo m1014_MI;
extern MethodInfo m1002_MI;
extern MethodInfo m1017_MI;
extern MethodInfo m1011_MI;
extern MethodInfo m1931_MI;
extern MethodInfo m477_MI;
extern MethodInfo m1000_MI;
extern MethodInfo m1005_MI;


extern MethodInfo m993_MI;
 void m993 (t39 * __this, MethodInfo* method){
	{
		__this->f16 = 1;
		t240 * L_0 = (t240 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t240_TI));
		m992(L_0, &m992_MI);
		__this->f19 = L_0;
		m831(__this, &m831_MI);
		return;
	}
}
extern MethodInfo m994_MI;
 t242 * m994 (t39 * __this, MethodInfo* method){
	{
		t242 * L_0 = (__this->f18);
		return L_0;
	}
}
extern MethodInfo m995_MI;
 void m995 (t39 * __this, t242 * p0, MethodInfo* method){
	{
		__this->f18 = p0;
		t242 * L_0 = (__this->f18);
		m999(__this, L_0, 1, &m999_MI);
		m1003(__this, 1, &m1003_MI);
		return;
	}
}
extern MethodInfo m996_MI;
 void m996 (t39 * __this, int32_t p0, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m997_MI;
 void m997 (t39 * __this, MethodInfo* method){
	{
		m861(__this, &m861_MI);
		t242 * L_0 = (__this->f18);
		m999(__this, L_0, 0, &m999_MI);
		m1003(__this, 1, &m1003_MI);
		return;
	}
}
extern MethodInfo m998_MI;
 void m998 (t39 * __this, MethodInfo* method){
	{
		m999(__this, (t242 *)NULL, 0, &m999_MI);
		m863(__this, &m863_MI);
		return;
	}
}
 void m999 (t39 * __this, t242 * p0, bool p1, MethodInfo* method){
	t242 * V_0 = {0};
	{
		t242 * L_0 = (__this->f18);
		V_0 = L_0;
		t242 * L_1 = (__this->f18);
		bool L_2 = m1300(NULL, L_1, (t41 *)NULL, &m1300_MI);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		t242 * L_3 = (__this->f18);
		m1015(L_3, __this, &m1015_MI);
	}

IL_0024:
	{
		if (!p1)
		{
			goto IL_0031;
		}
	}
	{
		__this->f18 = p0;
	}

IL_0031:
	{
		t242 * L_4 = (__this->f18);
		bool L_5 = m1300(NULL, L_4, (t41 *)NULL, &m1300_MI);
		if (!L_5)
		{
			goto IL_0059;
		}
	}
	{
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (!L_6)
		{
			goto IL_0059;
		}
	}
	{
		t242 * L_7 = (__this->f18);
		m1016(L_7, __this, &m1016_MI);
	}

IL_0059:
	{
		bool L_8 = m1300(NULL, p0, (t41 *)NULL, &m1300_MI);
		if (!L_8)
		{
			goto IL_0093;
		}
	}
	{
		bool L_9 = m1300(NULL, p0, V_0, &m1300_MI);
		if (!L_9)
		{
			goto IL_0093;
		}
	}
	{
		bool L_10 = m58(__this, &m58_MI);
		if (!L_10)
		{
			goto IL_0093;
		}
	}
	{
		bool L_11 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (!L_11)
		{
			goto IL_0093;
		}
	}
	{
		t242 * L_12 = (__this->f18);
		m1014(L_12, __this, &m1014_MI);
	}

IL_0093:
	{
		return;
	}
}
 bool m58 (t39 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f20);
		return L_0;
	}
}
 void m1000 (t39 * __this, bool p0, MethodInfo* method){
	{
		m1001(__this, p0, &m1001_MI);
		return;
	}
}
 void m1001 (t39 * __this, bool p0, MethodInfo* method){
	{
		m1002(__this, p0, 1, &m1002_MI);
		return;
	}
}
 void m1002 (t39 * __this, bool p0, bool p1, MethodInfo* method){
	{
		bool L_0 = (__this->f20);
		if ((((uint32_t)L_0) != ((uint32_t)p0)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		__this->f20 = p0;
		t242 * L_1 = (__this->f18);
		bool L_2 = m1300(NULL, L_1, (t41 *)NULL, &m1300_MI);
		if (!L_2)
		{
			goto IL_006e;
		}
	}
	{
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (!L_3)
		{
			goto IL_006e;
		}
	}
	{
		bool L_4 = (__this->f20);
		if (L_4)
		{
			goto IL_005b;
		}
	}
	{
		t242 * L_5 = (__this->f18);
		bool L_6 = m1017(L_5, &m1017_MI);
		if (L_6)
		{
			goto IL_006e;
		}
	}
	{
		t242 * L_7 = (__this->f18);
		bool L_8 = m1011(L_7, &m1011_MI);
		if (L_8)
		{
			goto IL_006e;
		}
	}

IL_005b:
	{
		__this->f20 = 1;
		t242 * L_9 = (__this->f18);
		m1014(L_9, __this, &m1014_MI);
	}

IL_006e:
	{
		int32_t L_10 = (__this->f16);
		m1003(__this, ((((int32_t)L_10) == ((int32_t)0))? 1 : 0), &m1003_MI);
		if (!p1)
		{
			goto IL_0094;
		}
	}
	{
		t240 * L_11 = (__this->f19);
		bool L_12 = (__this->f20);
		m1931(L_11, L_12, &m1931_MI);
	}

IL_0094:
	{
		return;
	}
}
 void m1003 (t39 * __this, bool p0, MethodInfo* method){
	t155 * G_B4_0 = {0};
	t155 * G_B3_0 = {0};
	float G_B5_0 = 0.0f;
	t155 * G_B5_1 = {0};
	float G_B7_0 = 0.0f;
	t155 * G_B7_1 = {0};
	float G_B6_0 = 0.0f;
	t155 * G_B6_1 = {0};
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	t155 * G_B8_2 = {0};
	{
		t155 * L_0 = (__this->f17);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		t155 * L_2 = (__this->f17);
		bool L_3 = (__this->f20);
		G_B3_0 = L_2;
		if (!L_3)
		{
			G_B4_0 = L_2;
			goto IL_002d;
		}
	}
	{
		G_B5_0 = (1.0f);
		G_B5_1 = G_B3_0;
		goto IL_0032;
	}

IL_002d:
	{
		G_B5_0 = (0.0f);
		G_B5_1 = G_B4_0;
	}

IL_0032:
	{
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
		if (!p0)
		{
			G_B7_0 = G_B5_0;
			G_B7_1 = G_B5_1;
			goto IL_0042;
		}
	}
	{
		G_B8_0 = (0.0f);
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		goto IL_0047;
	}

IL_0042:
	{
		G_B8_0 = (0.1f);
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
	}

IL_0047:
	{
		m477(G_B8_2, G_B8_1, G_B8_0, 1, &m477_MI);
		return;
	}
}
extern MethodInfo m1004_MI;
 void m1004 (t39 * __this, MethodInfo* method){
	{
		m1003(__this, 1, &m1003_MI);
		return;
	}
}
 void m1005 (t39 * __this, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m859_MI, __this);
		if (L_1)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		return;
	}

IL_0017:
	{
		bool L_2 = m58(__this, &m58_MI);
		m1000(__this, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), &m1000_MI);
		return;
	}
}
extern MethodInfo m1006_MI;
 void m1006 (t39 * __this, t6 * p0, MethodInfo* method){
	{
		int32_t L_0 = m230(p0, &m230_MI);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		m1005(__this, &m1005_MI);
		return;
	}
}
extern MethodInfo m1007_MI;
 void m1007 (t39 * __this, t53 * p0, MethodInfo* method){
	{
		m1005(__this, &m1005_MI);
		return;
	}
}
extern MethodInfo m1008_MI;
 bool m1008 (t39 * __this, MethodInfo* method){
	{
		bool L_0 = m180(__this, &m180_MI);
		return L_0;
	}
}
extern MethodInfo m1009_MI;
 t25 * m1009 (t39 * __this, MethodInfo* method){
	{
		t25 * L_0 = m39(__this, &m39_MI);
		return L_0;
	}
}
// Metadata Definition UnityEngine.UI.Toggle
extern Il2CppType t239_0_0_6;
FieldInfo t39_f16_FieldInfo = 
{
	"toggleTransition", &t239_0_0_6, &t39_TI, offsetof(t39, f16), &EmptyCustomAttributesCache};
extern Il2CppType t155_0_0_6;
FieldInfo t39_f17_FieldInfo = 
{
	"graphic", &t155_0_0_6, &t39_TI, offsetof(t39, f17), &EmptyCustomAttributesCache};
extern Il2CppType t242_0_0_1;
extern CustomAttributesCache t39__CustomAttributeCache_m_Group;
FieldInfo t39_f18_FieldInfo = 
{
	"m_Group", &t242_0_0_1, &t39_TI, offsetof(t39, f18), &t39__CustomAttributeCache_m_Group};
extern Il2CppType t240_0_0_6;
FieldInfo t39_f19_FieldInfo = 
{
	"onValueChanged", &t240_0_0_6, &t39_TI, offsetof(t39, f19), &EmptyCustomAttributesCache};
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t39__CustomAttributeCache_m_IsOn;
FieldInfo t39_f20_FieldInfo = 
{
	"m_IsOn", &t40_0_0_1, &t39_TI, offsetof(t39, f20), &t39__CustomAttributeCache_m_IsOn};
static FieldInfo* t39_FIs[] =
{
	&t39_f16_FieldInfo,
	&t39_f17_FieldInfo,
	&t39_f18_FieldInfo,
	&t39_f19_FieldInfo,
	&t39_f20_FieldInfo,
	NULL
};
static PropertyInfo t39____group_PropertyInfo = 
{
	&t39_TI, "group", &m994_MI, &m995_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t39____isOn_PropertyInfo = 
{
	&t39_TI, "isOn", &m58_MI, &m1000_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t39_PIs[] =
{
	&t39____group_PropertyInfo,
	&t39____isOn_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m993_MI = 
{
	".ctor", (methodPointerType)&m993, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 975, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t242_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m994_MI = 
{
	"get_group", (methodPointerType)&m994, &t39_TI, &t242_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 976, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t242_0_0_0;
extern Il2CppType t242_0_0_0;
static ParameterInfo t39_m995_ParameterInfos[] = 
{
	{"value", 0, 134218302, &EmptyCustomAttributesCache, &t242_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m995_MI = 
{
	"set_group", (methodPointerType)&m995, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t39_m995_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 977, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t140_0_0_0;
static ParameterInfo t39_m996_ParameterInfos[] = 
{
	{"executing", 0, 134218303, &EmptyCustomAttributesCache, &t140_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m996_MI = 
{
	"Rebuild", (methodPointerType)&m996, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t39_m996_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 42, 1, false, false, 978, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m997_MI = 
{
	"OnEnable", (methodPointerType)&m997, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 5, 0, false, false, 979, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m998_MI = 
{
	"OnDisable", (methodPointerType)&m998, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 7, 0, false, false, 980, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t242_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t39_m999_ParameterInfos[] = 
{
	{"newGroup", 0, 134218304, &EmptyCustomAttributesCache, &t242_0_0_0},
	{"setMemberValue", 1, 134218305, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m999_MI = 
{
	"SetToggleGroup", (methodPointerType)&m999, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t297, t39_m999_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 981, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m58_MI = 
{
	"get_isOn", (methodPointerType)&m58, &t39_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 982, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t39_m1000_ParameterInfos[] = 
{
	{"value", 0, 134218306, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1000_MI = 
{
	"set_isOn", (methodPointerType)&m1000, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t39_m1000_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 983, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t39_m1001_ParameterInfos[] = 
{
	{"value", 0, 134218307, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1001_MI = 
{
	"Set", (methodPointerType)&m1001, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t39_m1001_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 984, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t39_m1002_ParameterInfos[] = 
{
	{"value", 0, 134218308, &EmptyCustomAttributesCache, &t40_0_0_0},
	{"sendCallback", 1, 134218309, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1002_MI = 
{
	"Set", (methodPointerType)&m1002, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21_t297_t297, t39_m1002_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 985, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t39_m1003_ParameterInfos[] = 
{
	{"instant", 0, 134218310, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1003_MI = 
{
	"PlayEffect", (methodPointerType)&m1003, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t39_m1003_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 986, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1004_MI = 
{
	"Start", (methodPointerType)&m1004, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 6, 0, false, false, 987, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1005_MI = 
{
	"InternalToggle", (methodPointerType)&m1005, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 988, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t6_0_0_0;
static ParameterInfo t39_m1006_ParameterInfos[] = 
{
	{"eventData", 0, 134218311, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1006_MI = 
{
	"OnPointerClick", (methodPointerType)&m1006, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t39_m1006_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 43, 1, false, false, 989, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t53_0_0_0;
static ParameterInfo t39_m1007_ParameterInfos[] = 
{
	{"eventData", 0, 134218312, &EmptyCustomAttributesCache, &t53_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1007_MI = 
{
	"OnSubmit", (methodPointerType)&m1007, &t39_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t39_m1007_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 44, 1, false, false, 990, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1008_MI = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed", (methodPointerType)&m1008, &t39_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 960, 0, 45, 0, false, false, 991, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1009_MI = 
{
	"UnityEngine.UI.ICanvasElement.get_transform", (methodPointerType)&m1009, &t39_TI, &t25_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 960, 0, 46, 0, false, false, 992, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t39_MIs[] =
{
	&m993_MI,
	&m994_MI,
	&m995_MI,
	&m996_MI,
	&m997_MI,
	&m998_MI,
	&m999_MI,
	&m58_MI,
	&m1000_MI,
	&m1001_MI,
	&m1002_MI,
	&m1003_MI,
	&m1004_MI,
	&m1005_MI,
	&m1006_MI,
	&m1007_MI,
	&m1008_MI,
	&m1009_MI,
	NULL
};
extern TypeInfo t239_TI;
extern TypeInfo t240_TI;
static TypeInfo* t39_TI__nestedTypes[3] =
{
	&t239_TI,
	&t240_TI,
	NULL
};
static MethodInfo* t39_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m857_MI,
	&m997_MI,
	&m1004_MI,
	&m998_MI,
	&m173_MI,
	&m174_MI,
	&m175_MI,
	&m176_MI,
	&m177_MI,
	&m860_MI,
	&m858_MI,
	&m886_MI,
	&m887_MI,
	&m884_MI,
	&m885_MI,
	&m888_MI,
	&m889_MI,
	&m874_MI,
	&m859_MI,
	&m865_MI,
	&m866_MI,
	&m870_MI,
	&m871_MI,
	&m872_MI,
	&m873_MI,
	&m874_MI,
	&m884_MI,
	&m885_MI,
	&m886_MI,
	&m887_MI,
	&m888_MI,
	&m889_MI,
	&m890_MI,
	&m1006_MI,
	&m1007_MI,
	&m996_MI,
	&m1009_MI,
	&m1008_MI,
	&m996_MI,
	&m1006_MI,
	&m1007_MI,
	&m1008_MI,
	&m1009_MI,
};
extern TypeInfo t91_TI;
extern TypeInfo t102_TI;
static TypeInfo* t39_ITIs[] = 
{
	&t31_TI,
	&t91_TI,
	&t102_TI,
	&t145_TI,
};
static Il2CppInterfaceOffsetPair t39_IOs[] = 
{
	{ &t31_TI, 15},
	{ &t30_TI, 15},
	{ &t32_TI, 16},
	{ &t89_TI, 17},
	{ &t90_TI, 18},
	{ &t99_TI, 19},
	{ &t100_TI, 20},
	{ &t101_TI, 21},
	{ &t91_TI, 37},
	{ &t102_TI, 38},
	{ &t145_TI, 39},
};
void t39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("UI/Toggle"), 35, &m1519_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t318 * tmp;
		tmp = (t318 *)il2cpp_codegen_object_new (&t318_TI);
		m1404(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2_TI)), &m1404_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t39_CustomAttributesCacheGenerator_m_Group(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t39_CustomAttributesCacheGenerator_m_IsOn(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t399 * tmp;
		tmp = (t399 *)il2cpp_codegen_object_new (&t399_TI);
		m1897(tmp, il2cpp_codegen_string_new_wrapper("Is the toggle currently on or off?"), &m1897_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("m_IsActive"), &m1318_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t39__CustomAttributeCache = {
2,
NULL,
&t39_CustomAttributesCacheGenerator
};
CustomAttributesCache t39__CustomAttributeCache_m_Group = {
1,
NULL,
&t39_CustomAttributesCacheGenerator_m_Group
};
CustomAttributesCache t39__CustomAttributeCache_m_IsOn = {
3,
NULL,
&t39_CustomAttributesCacheGenerator_m_IsOn
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t39_0_0_0;
extern Il2CppType t39_1_0_0;
struct t39;
extern CustomAttributesCache t39__CustomAttributeCache;
extern CustomAttributesCache t39__CustomAttributeCache_m_Group;
extern CustomAttributesCache t39__CustomAttributeCache_m_IsOn;
TypeInfo t39_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Toggle", "UnityEngine.UI", t39_MIs, t39_PIs, t39_FIs, NULL, &t139_TI, t39_TI__nestedTypes, NULL, &t39_TI, t39_ITIs, t39_VT, &t39__CustomAttributeCache, &t39_TI, &t39_0_0_0, &t39_1_0_0, t39_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t39), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 18, 2, 5, 0, 2, 47, 4, 11};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t242_TI;

#include "t243.h"
#include "t305.h"
#include "t244.h"
#include "t245.h"
extern TypeInfo t243_TI;
extern TypeInfo t305_TI;
extern TypeInfo t244_TI;
extern TypeInfo t245_TI;
#include "t243MD.h"
#include "t305MD.h"
#include "t244MD.h"
#include "t245MD.h"
#include "t406MD.h"
extern MethodInfo m1932_MI;
extern MethodInfo m1933_MI;
extern MethodInfo m1934_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m1013_MI;
extern MethodInfo m1936_MI;
extern MethodInfo m1937_MI;
extern MethodInfo m1938_MI;
extern MethodInfo m1939_MI;
extern MethodInfo m1020_MI;
extern MethodInfo m1940_MI;
extern MethodInfo m1941_MI;
extern MethodInfo m1021_MI;
extern MethodInfo m1942_MI;
extern MethodInfo m1943_MI;
struct t406;
#include "t406.h"
struct t406;
#include "t407.h"
 t29* m1944_gshared (t29 * __this, t29* p0, t407 * p1, MethodInfo* method);
#define m1944(__this, p0, p1, method) (t29*)m1944_gshared((t29 *)__this, (t29*)p0, (t407 *)p1, method)
#define m1943(__this, p0, p1, method) (t29*)m1944_gshared((t29 *)__this, (t29*)p0, (t407 *)p1, method)


extern MethodInfo m1010_MI;
 void m1010 (t242 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t243_TI));
		t243 * L_0 = (t243 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t243_TI));
		m1932(L_0, &m1932_MI);
		__this->f3 = L_0;
		m168(__this, &m168_MI);
		return;
	}
}
 bool m1011 (t242 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m1012_MI;
 void m1012 (t242 * __this, bool p0, MethodInfo* method){
	{
		__this->f2 = p0;
		return;
	}
}
 void m1013 (t242 * __this, t39 * p0, MethodInfo* method){
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		t243 * L_1 = (__this->f3);
		bool L_2 = (bool)VirtFuncInvoker1< bool, t39 * >::Invoke(&m1933_MI, L_1, p0);
		if (L_2)
		{
			goto IL_003b;
		}
	}

IL_001d:
	{
		t316* L_3 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 2));
		ArrayElementTypeCheck (L_3, p0);
		*((t29 **)(t29 **)SZArrayLdElema(L_3, 0)) = (t29 *)p0;
		t316* L_4 = L_3;
		ArrayElementTypeCheck (L_4, __this);
		*((t29 **)(t29 **)SZArrayLdElema(L_4, 1)) = (t29 *)__this;
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_5 = m1934(NULL, (t7*) &_stringLiteral62, L_4, &m1934_MI);
		t305 * L_6 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_6, L_5, &m1935_MI);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_003b:
	{
		return;
	}
}
 void m1014 (t242 * __this, t39 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		m1013(__this, p0, &m1013_MI);
		V_0 = 0;
		goto IL_0040;
	}

IL_000e:
	{
		t243 * L_0 = (__this->f3);
		t39 * L_1 = (t39 *)VirtFuncInvoker1< t39 *, int32_t >::Invoke(&m1936_MI, L_0, V_0);
		bool L_2 = m1297(NULL, L_1, p0, &m1297_MI);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		goto IL_003c;
	}

IL_002a:
	{
		t243 * L_3 = (__this->f3);
		t39 * L_4 = (t39 *)VirtFuncInvoker1< t39 *, int32_t >::Invoke(&m1936_MI, L_3, V_0);
		m1000(L_4, 0, &m1000_MI);
	}

IL_003c:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0040:
	{
		t243 * L_5 = (__this->f3);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1937_MI, L_5);
		if ((((int32_t)V_0) < ((int32_t)L_6)))
		{
			goto IL_000e;
		}
	}
	{
		return;
	}
}
 void m1015 (t242 * __this, t39 * p0, MethodInfo* method){
	{
		t243 * L_0 = (__this->f3);
		bool L_1 = (bool)VirtFuncInvoker1< bool, t39 * >::Invoke(&m1933_MI, L_0, p0);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		t243 * L_2 = (__this->f3);
		VirtFuncInvoker1< bool, t39 * >::Invoke(&m1938_MI, L_2, p0);
	}

IL_001e:
	{
		return;
	}
}
 void m1016 (t242 * __this, t39 * p0, MethodInfo* method){
	{
		t243 * L_0 = (__this->f3);
		bool L_1 = (bool)VirtFuncInvoker1< bool, t39 * >::Invoke(&m1933_MI, L_0, p0);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		t243 * L_2 = (__this->f3);
		VirtActionInvoker1< t39 * >::Invoke(&m1939_MI, L_2, p0);
	}

IL_001d:
	{
		return;
	}
}
 bool m1017 (t242 * __this, MethodInfo* method){
	t243 * G_B2_0 = {0};
	t243 * G_B1_0 = {0};
	{
		t243 * L_0 = (__this->f3);
		G_B1_0 = L_0;
		if ((((t242_SFs*)InitializedTypeInfo(&t242_TI)->static_fields)->f4))
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		t35 L_1 = { &m1020_MI };
		t244 * L_2 = (t244 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t244_TI));
		m1940(L_2, NULL, L_1, &m1940_MI);
		((t242_SFs*)InitializedTypeInfo(&t242_TI)->static_fields)->f4 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		t39 * L_3 = m1941(G_B2_0, (((t242_SFs*)InitializedTypeInfo(&t242_TI)->static_fields)->f4), &m1941_MI);
		bool L_4 = m1300(NULL, L_3, (t41 *)NULL, &m1300_MI);
		return L_4;
	}
}
extern MethodInfo m1018_MI;
 t29* m1018 (t242 * __this, MethodInfo* method){
	t243 * G_B2_0 = {0};
	t243 * G_B1_0 = {0};
	{
		t243 * L_0 = (__this->f3);
		G_B1_0 = L_0;
		if ((((t242_SFs*)InitializedTypeInfo(&t242_TI)->static_fields)->f5))
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		t35 L_1 = { &m1021_MI };
		t245 * L_2 = (t245 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t245_TI));
		m1942(L_2, NULL, L_1, &m1942_MI);
		((t242_SFs*)InitializedTypeInfo(&t242_TI)->static_fields)->f5 = L_2;
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		t29* L_3 = m1943(NULL, G_B2_0, (((t242_SFs*)InitializedTypeInfo(&t242_TI)->static_fields)->f5), &m1943_MI);
		return L_3;
	}
}
extern MethodInfo m1019_MI;
 void m1019 (t242 * __this, MethodInfo* method){
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		bool L_0 = (__this->f2);
		V_0 = L_0;
		__this->f2 = 1;
		V_1 = 0;
		goto IL_002b;
	}

IL_0015:
	{
		t243 * L_1 = (__this->f3);
		t39 * L_2 = (t39 *)VirtFuncInvoker1< t39 *, int32_t >::Invoke(&m1936_MI, L_1, V_1);
		m1000(L_2, 0, &m1000_MI);
		V_1 = ((int32_t)(V_1+1));
	}

IL_002b:
	{
		t243 * L_3 = (__this->f3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1937_MI, L_3);
		if ((((int32_t)V_1) < ((int32_t)L_4)))
		{
			goto IL_0015;
		}
	}
	{
		__this->f2 = V_0;
		return;
	}
}
 bool m1020 (t29 * __this, t39 * p0, MethodInfo* method){
	{
		bool L_0 = m58(p0, &m58_MI);
		return L_0;
	}
}
 bool m1021 (t29 * __this, t39 * p0, MethodInfo* method){
	{
		bool L_0 = m58(p0, &m58_MI);
		return L_0;
	}
}
// Metadata Definition UnityEngine.UI.ToggleGroup
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t242__CustomAttributeCache_m_AllowSwitchOff;
FieldInfo t242_f2_FieldInfo = 
{
	"m_AllowSwitchOff", &t40_0_0_1, &t242_TI, offsetof(t242, f2), &t242__CustomAttributeCache_m_AllowSwitchOff};
extern Il2CppType t243_0_0_1;
FieldInfo t242_f3_FieldInfo = 
{
	"m_Toggles", &t243_0_0_1, &t242_TI, offsetof(t242, f3), &EmptyCustomAttributesCache};
extern Il2CppType t244_0_0_17;
extern CustomAttributesCache t242__CustomAttributeCache_U3CU3Ef__am$cache2;
FieldInfo t242_f4_FieldInfo = 
{
	"<>f__am$cache2", &t244_0_0_17, &t242_TI, offsetof(t242_SFs, f4), &t242__CustomAttributeCache_U3CU3Ef__am$cache2};
extern Il2CppType t245_0_0_17;
extern CustomAttributesCache t242__CustomAttributeCache_U3CU3Ef__am$cache3;
FieldInfo t242_f5_FieldInfo = 
{
	"<>f__am$cache3", &t245_0_0_17, &t242_TI, offsetof(t242_SFs, f5), &t242__CustomAttributeCache_U3CU3Ef__am$cache3};
static FieldInfo* t242_FIs[] =
{
	&t242_f2_FieldInfo,
	&t242_f3_FieldInfo,
	&t242_f4_FieldInfo,
	&t242_f5_FieldInfo,
	NULL
};
static PropertyInfo t242____allowSwitchOff_PropertyInfo = 
{
	&t242_TI, "allowSwitchOff", &m1011_MI, &m1012_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t242_PIs[] =
{
	&t242____allowSwitchOff_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1010_MI = 
{
	".ctor", (methodPointerType)&m1010, &t242_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 994, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1011_MI = 
{
	"get_allowSwitchOff", (methodPointerType)&m1011, &t242_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 995, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t242_m1012_ParameterInfos[] = 
{
	{"value", 0, 134218313, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1012_MI = 
{
	"set_allowSwitchOff", (methodPointerType)&m1012, &t242_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t242_m1012_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 996, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t39_0_0_0;
static ParameterInfo t242_m1013_ParameterInfos[] = 
{
	{"toggle", 0, 134218314, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1013_MI = 
{
	"ValidateToggleIsInGroup", (methodPointerType)&m1013, &t242_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t242_m1013_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 997, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t39_0_0_0;
static ParameterInfo t242_m1014_ParameterInfos[] = 
{
	{"toggle", 0, 134218315, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1014_MI = 
{
	"NotifyToggleOn", (methodPointerType)&m1014, &t242_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t242_m1014_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 998, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t39_0_0_0;
static ParameterInfo t242_m1015_ParameterInfos[] = 
{
	{"toggle", 0, 134218316, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1015_MI = 
{
	"UnregisterToggle", (methodPointerType)&m1015, &t242_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t242_m1015_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 999, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t39_0_0_0;
static ParameterInfo t242_m1016_ParameterInfos[] = 
{
	{"toggle", 0, 134218317, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1016_MI = 
{
	"RegisterToggle", (methodPointerType)&m1016, &t242_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t242_m1016_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1000, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1017_MI = 
{
	"AnyTogglesOn", (methodPointerType)&m1017, &t242_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 1001, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t246_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1018_MI = 
{
	"ActiveToggles", (methodPointerType)&m1018, &t242_TI, &t246_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 1002, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1019_MI = 
{
	"SetAllTogglesOff", (methodPointerType)&m1019, &t242_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 1003, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t39_0_0_0;
static ParameterInfo t242_m1020_ParameterInfos[] = 
{
	{"x", 0, 134218318, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t242__CustomAttributeCache_m1020;
MethodInfo m1020_MI = 
{
	"<AnyTogglesOn>m__7", (methodPointerType)&m1020, &t242_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t242_m1020_ParameterInfos, &t242__CustomAttributeCache_m1020, 145, 0, 255, 1, false, false, 1004, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t39_0_0_0;
static ParameterInfo t242_m1021_ParameterInfos[] = 
{
	{"x", 0, 134218319, &EmptyCustomAttributesCache, &t39_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t242__CustomAttributeCache_m1021;
MethodInfo m1021_MI = 
{
	"<ActiveToggles>m__8", (methodPointerType)&m1021, &t242_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t242_m1021_ParameterInfos, &t242__CustomAttributeCache_m1021, 145, 0, 255, 1, false, false, 1005, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t242_MIs[] =
{
	&m1010_MI,
	&m1011_MI,
	&m1012_MI,
	&m1013_MI,
	&m1014_MI,
	&m1015_MI,
	&m1016_MI,
	&m1017_MI,
	&m1018_MI,
	&m1019_MI,
	&m1020_MI,
	&m1021_MI,
	NULL
};
static MethodInfo* t242_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m170_MI,
	&m171_MI,
	&m172_MI,
	&m173_MI,
	&m174_MI,
	&m175_MI,
	&m176_MI,
	&m177_MI,
	&m178_MI,
	&m179_MI,
};
void t242_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("UI/Toggle Group"), 36, &m1519_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t242_CustomAttributesCacheGenerator_m_AllowSwitchOff(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t242_CustomAttributesCacheGenerator_U3CU3Ef__am$cache2(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t242_CustomAttributesCacheGenerator_U3CU3Ef__am$cache3(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t242_CustomAttributesCacheGenerator_m1020(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t242_CustomAttributesCacheGenerator_m1021(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t242__CustomAttributeCache = {
1,
NULL,
&t242_CustomAttributesCacheGenerator
};
CustomAttributesCache t242__CustomAttributeCache_m_AllowSwitchOff = {
1,
NULL,
&t242_CustomAttributesCacheGenerator_m_AllowSwitchOff
};
CustomAttributesCache t242__CustomAttributeCache_U3CU3Ef__am$cache2 = {
1,
NULL,
&t242_CustomAttributesCacheGenerator_U3CU3Ef__am$cache2
};
CustomAttributesCache t242__CustomAttributeCache_U3CU3Ef__am$cache3 = {
1,
NULL,
&t242_CustomAttributesCacheGenerator_U3CU3Ef__am$cache3
};
CustomAttributesCache t242__CustomAttributeCache_m1020 = {
1,
NULL,
&t242_CustomAttributesCacheGenerator_m1020
};
CustomAttributesCache t242__CustomAttributeCache_m1021 = {
1,
NULL,
&t242_CustomAttributesCacheGenerator_m1021
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t242_1_0_0;
struct t242;
extern CustomAttributesCache t242__CustomAttributeCache;
extern CustomAttributesCache t242__CustomAttributeCache_m_AllowSwitchOff;
extern CustomAttributesCache t242__CustomAttributeCache_U3CU3Ef__am$cache2;
extern CustomAttributesCache t242__CustomAttributeCache_U3CU3Ef__am$cache3;
extern CustomAttributesCache t242__CustomAttributeCache_m1020;
extern CustomAttributesCache t242__CustomAttributeCache_m1021;
TypeInfo t242_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ToggleGroup", "UnityEngine.UI", t242_MIs, t242_PIs, t242_FIs, NULL, &t55_TI, NULL, NULL, &t242_TI, NULL, t242_VT, &t242__CustomAttributeCache, &t242_TI, &t242_0_0_0, &t242_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t242), 0, -1, sizeof(t242_SFs), 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 12, 1, 4, 0, 0, 15, 0, 0};
#include "t247.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t247_TI;
#include "t247MD.h"



// Metadata Definition UnityEngine.UI.AspectRatioFitter/AspectMode
extern Il2CppType t44_0_0_1542;
FieldInfo t247_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t247_TI, offsetof(t247, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t247_0_0_32854;
FieldInfo t247_f2_FieldInfo = 
{
	"None", &t247_0_0_32854, &t247_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t247_0_0_32854;
FieldInfo t247_f3_FieldInfo = 
{
	"WidthControlsHeight", &t247_0_0_32854, &t247_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t247_0_0_32854;
FieldInfo t247_f4_FieldInfo = 
{
	"HeightControlsWidth", &t247_0_0_32854, &t247_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t247_0_0_32854;
FieldInfo t247_f5_FieldInfo = 
{
	"FitInParent", &t247_0_0_32854, &t247_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t247_0_0_32854;
FieldInfo t247_f6_FieldInfo = 
{
	"EnvelopeParent", &t247_0_0_32854, &t247_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t247_FIs[] =
{
	&t247_f1_FieldInfo,
	&t247_f2_FieldInfo,
	&t247_f3_FieldInfo,
	&t247_f4_FieldInfo,
	&t247_f5_FieldInfo,
	&t247_f6_FieldInfo,
	NULL
};
static const int32_t t247_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t247_f2_DefaultValue = 
{
	&t247_f2_FieldInfo, { (char*)&t247_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t247_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t247_f3_DefaultValue = 
{
	&t247_f3_FieldInfo, { (char*)&t247_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t247_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t247_f4_DefaultValue = 
{
	&t247_f4_FieldInfo, { (char*)&t247_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t247_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t247_f5_DefaultValue = 
{
	&t247_f5_FieldInfo, { (char*)&t247_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t247_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t247_f6_DefaultValue = 
{
	&t247_f6_FieldInfo, { (char*)&t247_f6_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t247_FDVs[] = 
{
	&t247_f2_DefaultValue,
	&t247_f3_DefaultValue,
	&t247_f4_DefaultValue,
	&t247_f5_DefaultValue,
	&t247_f6_DefaultValue,
	NULL
};
static MethodInfo* t247_MIs[] =
{
	NULL
};
static MethodInfo* t247_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t247_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t247_0_0_0;
extern Il2CppType t247_1_0_0;
extern TypeInfo t248_TI;
TypeInfo t247_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "AspectMode", "", t247_MIs, NULL, t247_FIs, NULL, &t49_TI, NULL, &t248_TI, &t44_TI, NULL, t247_VT, &EmptyCustomAttributesCache, &t44_TI, &t247_0_0_0, &t247_1_0_0, t247_IOs, NULL, NULL, t247_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t247)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 258, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 6, 0, 0, 23, 0, 3};
#include "t248.h"
#ifndef _MSC_VER
#else
#endif
#include "t248MD.h"

#include "t408.h"
extern TypeInfo t265_TI;
#include "t265MD.h"
extern MethodInfo m1945_MI;
extern MethodInfo m1036_MI;
extern MethodInfo m1027_MI;
extern MethodInfo m1174_MI;
extern MethodInfo m1031_MI;
extern MethodInfo m1574_MI;
extern MethodInfo m1946_MI;
extern MethodInfo m1573_MI;
extern MethodInfo m1033_MI;
extern MethodInfo m1025_MI;
extern MethodInfo m1032_MI;
extern MethodInfo m63_MI;
extern MethodInfo m1793_MI;
extern MethodInfo m1662_MI;
struct t230;
 bool m1945 (t29 * __this, int32_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m1022_MI;
 void m1022 (t248 * __this, MethodInfo* method){
	{
		__this->f3 = (1.0f);
		m168(__this, &m168_MI);
		return;
	}
}
extern MethodInfo m1023_MI;
 int32_t m1023 (t248 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m1024_MI;
 void m1024 (t248 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t* L_0 = &(__this->f2);
		bool L_1 = m1945(NULL, L_0, p0, &m1945_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m1036(__this, &m1036_MI);
	}

IL_0017:
	{
		return;
	}
}
 float m1025 (t248 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m1026_MI;
 void m1026 (t248 * __this, float p0, MethodInfo* method){
	{
		float* L_0 = &(__this->f3);
		bool L_1 = m1644(NULL, L_0, p0, &m1644_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m1036(__this, &m1036_MI);
	}

IL_0017:
	{
		return;
	}
}
 t2 * m1027 (t248 * __this, MethodInfo* method){
	{
		t2 * L_0 = (__this->f4);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		t2 * L_2 = m60(__this, &m60_MI);
		__this->f4 = L_2;
	}

IL_001d:
	{
		t2 * L_3 = (__this->f4);
		return L_3;
	}
}
extern MethodInfo m1028_MI;
 void m1028 (t248 * __this, MethodInfo* method){
	{
		m170(__this, &m170_MI);
		m1036(__this, &m1036_MI);
		return;
	}
}
extern MethodInfo m1029_MI;
 void m1029 (t248 * __this, MethodInfo* method){
	{
		t218 * L_0 = &(__this->f5);
		m1831(L_0, &m1831_MI);
		t2 * L_1 = m1027(__this, &m1027_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1174(NULL, L_1, &m1174_MI);
		m172(__this, &m172_MI);
		return;
	}
}
extern MethodInfo m1030_MI;
 void m1030 (t248 * __this, MethodInfo* method){
	{
		m1031(__this, &m1031_MI);
		return;
	}
}
 void m1031 (t248 * __this, MethodInfo* method){
	t17  V_0 = {0};
	t17  V_1 = {0};
	int32_t V_2 = {0};
	t164  V_3 = {0};
	t164  V_4 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		t218 * L_1 = &(__this->f5);
		m1831(L_1, &m1831_MI);
		int32_t L_2 = (__this->f2);
		V_2 = L_2;
		if (((int32_t)(V_2-1)) == 0)
		{
			goto IL_007d;
		}
		if (((int32_t)(V_2-1)) == 1)
		{
			goto IL_003b;
		}
		if (((int32_t)(V_2-1)) == 2)
		{
			goto IL_00c0;
		}
		if (((int32_t)(V_2-1)) == 3)
		{
			goto IL_00c0;
		}
	}
	{
		goto IL_0188;
	}

IL_003b:
	{
		t218 * L_3 = &(__this->f5);
		t2 * L_4 = m1027(__this, &m1027_MI);
		m1833(L_3, __this, L_4, ((int32_t)4096), &m1833_MI);
		t2 * L_5 = m1027(__this, &m1027_MI);
		t2 * L_6 = m1027(__this, &m1027_MI);
		t164  L_7 = m1572(L_6, &m1572_MI);
		V_3 = L_7;
		float L_8 = m1574((&V_3), &m1574_MI);
		float L_9 = (__this->f3);
		m1946(L_5, 0, ((float)((float)L_8*(float)L_9)), &m1946_MI);
		goto IL_0188;
	}

IL_007d:
	{
		t218 * L_10 = &(__this->f5);
		t2 * L_11 = m1027(__this, &m1027_MI);
		m1833(L_10, __this, L_11, ((int32_t)8192), &m1833_MI);
		t2 * L_12 = m1027(__this, &m1027_MI);
		t2 * L_13 = m1027(__this, &m1027_MI);
		t164  L_14 = m1572(L_13, &m1572_MI);
		V_4 = L_14;
		float L_15 = m1573((&V_4), &m1573_MI);
		float L_16 = (__this->f3);
		m1946(L_12, 1, ((float)((float)L_15/(float)L_16)), &m1946_MI);
		goto IL_0188;
	}

IL_00c0:
	{
		t218 * L_17 = &(__this->f5);
		t2 * L_18 = m1027(__this, &m1027_MI);
		m1833(L_17, __this, L_18, ((int32_t)16134), &m1833_MI);
		t2 * L_19 = m1027(__this, &m1027_MI);
		t17  L_20 = m1394(NULL, &m1394_MI);
		m1796(L_19, L_20, &m1796_MI);
		t2 * L_21 = m1027(__this, &m1027_MI);
		t17  L_22 = m1667(NULL, &m1667_MI);
		m1663(L_21, L_22, &m1663_MI);
		t2 * L_23 = m1027(__this, &m1027_MI);
		t17  L_24 = m1394(NULL, &m1394_MI);
		m1797(L_23, L_24, &m1797_MI);
		t17  L_25 = m1394(NULL, &m1394_MI);
		V_0 = L_25;
		t17  L_26 = m1033(__this, &m1033_MI);
		V_1 = L_26;
		float L_27 = ((&V_1)->f2);
		float L_28 = m1025(__this, &m1025_MI);
		float L_29 = ((&V_1)->f1);
		int32_t L_30 = (__this->f2);
		if (!((int32_t)((int32_t)((((float)((float)((float)L_27*(float)L_28))) < ((float)L_29))? 1 : 0)^(int32_t)((((int32_t)L_30) == ((int32_t)3))? 1 : 0))))
		{
			goto IL_015b;
		}
	}
	{
		float L_31 = ((&V_1)->f1);
		float L_32 = m1025(__this, &m1025_MI);
		float L_33 = m1032(__this, ((float)((float)L_31/(float)L_32)), 1, &m1032_MI);
		(&V_0)->f2 = L_33;
		goto IL_0177;
	}

IL_015b:
	{
		float L_34 = ((&V_1)->f2);
		float L_35 = m1025(__this, &m1025_MI);
		float L_36 = m1032(__this, ((float)((float)L_34*(float)L_35)), 0, &m1032_MI);
		(&V_0)->f1 = L_36;
	}

IL_0177:
	{
		t2 * L_37 = m1027(__this, &m1027_MI);
		m63(L_37, V_0, &m63_MI);
		goto IL_0188;
	}

IL_0188:
	{
		return;
	}
}
 float m1032 (t248 * __this, float p0, int32_t p1, MethodInfo* method){
	t17  V_0 = {0};
	t17  V_1 = {0};
	t17  V_2 = {0};
	{
		t17  L_0 = m1033(__this, &m1033_MI);
		V_0 = L_0;
		float L_1 = m1672((&V_0), p1, &m1672_MI);
		t2 * L_2 = m1027(__this, &m1027_MI);
		t17  L_3 = m1793(L_2, &m1793_MI);
		V_1 = L_3;
		float L_4 = m1672((&V_1), p1, &m1672_MI);
		t2 * L_5 = m1027(__this, &m1027_MI);
		t17  L_6 = m1662(L_5, &m1662_MI);
		V_2 = L_6;
		float L_7 = m1672((&V_2), p1, &m1672_MI);
		return ((float)(p0-((float)((float)L_1*(float)((float)(L_4-L_7))))));
	}
}
 t17  m1033 (t248 * __this, MethodInfo* method){
	t2 * V_0 = {0};
	t164  V_1 = {0};
	{
		t2 * L_0 = m1027(__this, &m1027_MI);
		t25 * L_1 = m1365(L_0, &m1365_MI);
		V_0 = ((t2 *)IsInst(L_1, InitializedTypeInfo(&t2_TI)));
		bool L_2 = m1293(NULL, V_0, &m1293_MI);
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		t17  L_3 = m1394(NULL, &m1394_MI);
		return L_3;
	}

IL_0022:
	{
		t164  L_4 = m1572(V_0, &m1572_MI);
		V_1 = L_4;
		t17  L_5 = m1653((&V_1), &m1653_MI);
		return L_5;
	}
}
extern MethodInfo m1034_MI;
 void m1034 (t248 * __this, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m1035_MI;
 void m1035 (t248 * __this, MethodInfo* method){
	{
		return;
	}
}
 void m1036 (t248 * __this, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		m1031(__this, &m1031_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.AspectRatioFitter
extern Il2CppType t247_0_0_1;
extern CustomAttributesCache t248__CustomAttributeCache_m_AspectMode;
FieldInfo t248_f2_FieldInfo = 
{
	"m_AspectMode", &t247_0_0_1, &t248_TI, offsetof(t248, f2), &t248__CustomAttributeCache_m_AspectMode};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t248__CustomAttributeCache_m_AspectRatio;
FieldInfo t248_f3_FieldInfo = 
{
	"m_AspectRatio", &t22_0_0_1, &t248_TI, offsetof(t248, f3), &t248__CustomAttributeCache_m_AspectRatio};
extern Il2CppType t2_0_0_129;
FieldInfo t248_f4_FieldInfo = 
{
	"m_Rect", &t2_0_0_129, &t248_TI, offsetof(t248, f4), &EmptyCustomAttributesCache};
extern Il2CppType t218_0_0_1;
FieldInfo t248_f5_FieldInfo = 
{
	"m_Tracker", &t218_0_0_1, &t248_TI, offsetof(t248, f5), &EmptyCustomAttributesCache};
static FieldInfo* t248_FIs[] =
{
	&t248_f2_FieldInfo,
	&t248_f3_FieldInfo,
	&t248_f4_FieldInfo,
	&t248_f5_FieldInfo,
	NULL
};
static PropertyInfo t248____aspectMode_PropertyInfo = 
{
	&t248_TI, "aspectMode", &m1023_MI, &m1024_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t248____aspectRatio_PropertyInfo = 
{
	&t248_TI, "aspectRatio", &m1025_MI, &m1026_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t248____rectTransform_PropertyInfo = 
{
	&t248_TI, "rectTransform", &m1027_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t248_PIs[] =
{
	&t248____aspectMode_PropertyInfo,
	&t248____aspectRatio_PropertyInfo,
	&t248____rectTransform_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1022_MI = 
{
	".ctor", (methodPointerType)&m1022, &t248_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1006, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t247_0_0_0;
extern void* RuntimeInvoker_t247 (MethodInfo* method, void* obj, void** args);
MethodInfo m1023_MI = 
{
	"get_aspectMode", (methodPointerType)&m1023, &t248_TI, &t247_0_0_0, RuntimeInvoker_t247, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1007, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t247_0_0_0;
static ParameterInfo t248_m1024_ParameterInfos[] = 
{
	{"value", 0, 134218320, &EmptyCustomAttributesCache, &t247_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1024_MI = 
{
	"set_aspectMode", (methodPointerType)&m1024, &t248_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t248_m1024_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1008, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1025_MI = 
{
	"get_aspectRatio", (methodPointerType)&m1025, &t248_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1009, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t248_m1026_ParameterInfos[] = 
{
	{"value", 0, 134218321, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1026_MI = 
{
	"set_aspectRatio", (methodPointerType)&m1026, &t248_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t248_m1026_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1010, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1027_MI = 
{
	"get_rectTransform", (methodPointerType)&m1027, &t248_TI, &t2_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 1011, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1028_MI = 
{
	"OnEnable", (methodPointerType)&m1028, &t248_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 5, 0, false, false, 1012, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1029_MI = 
{
	"OnDisable", (methodPointerType)&m1029, &t248_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 7, 0, false, false, 1013, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1030_MI = 
{
	"OnRectTransformDimensionsChange", (methodPointerType)&m1030, &t248_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 10, 0, false, false, 1014, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1031_MI = 
{
	"UpdateRect", (methodPointerType)&m1031, &t248_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 1015, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t248_m1032_ParameterInfos[] = 
{
	{"size", 0, 134218322, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"axis", 1, 134218323, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t22_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1032_MI = 
{
	"GetSizeDeltaToProduceSize", (methodPointerType)&m1032, &t248_TI, &t22_0_0_0, RuntimeInvoker_t22_t22_t44, t248_m1032_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 1016, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m1033_MI = 
{
	"GetParentSize", (methodPointerType)&m1033, &t248_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 1017, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1034_MI = 
{
	"SetLayoutHorizontal", (methodPointerType)&m1034, &t248_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 17, 0, false, false, 1018, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1035_MI = 
{
	"SetLayoutVertical", (methodPointerType)&m1035, &t248_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 18, 0, false, false, 1019, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1036_MI = 
{
	"SetDirty", (methodPointerType)&m1036, &t248_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 132, 0, 255, 0, false, false, 1020, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t248_MIs[] =
{
	&m1022_MI,
	&m1023_MI,
	&m1024_MI,
	&m1025_MI,
	&m1026_MI,
	&m1027_MI,
	&m1028_MI,
	&m1029_MI,
	&m1030_MI,
	&m1031_MI,
	&m1032_MI,
	&m1033_MI,
	&m1034_MI,
	&m1035_MI,
	&m1036_MI,
	NULL
};
extern TypeInfo t247_TI;
static TypeInfo* t248_TI__nestedTypes[2] =
{
	&t247_TI,
	NULL
};
static MethodInfo* t248_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1028_MI,
	&m171_MI,
	&m1029_MI,
	&m173_MI,
	&m174_MI,
	&m1030_MI,
	&m176_MI,
	&m177_MI,
	&m178_MI,
	&m179_MI,
	&m1034_MI,
	&m1035_MI,
	&m1034_MI,
	&m1035_MI,
};
extern TypeInfo t409_TI;
extern TypeInfo t410_TI;
static TypeInfo* t248_ITIs[] = 
{
	&t409_TI,
	&t410_TI,
};
static Il2CppInterfaceOffsetPair t248_IOs[] = 
{
	{ &t409_TI, 15},
	{ &t410_TI, 17},
};
void t248_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t360 * tmp;
		tmp = (t360 *)il2cpp_codegen_object_new (&t360_TI);
		m1604(tmp, &m1604_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t318 * tmp;
		tmp = (t318 *)il2cpp_codegen_object_new (&t318_TI);
		m1404(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2_TI)), &m1404_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("Layout/Aspect Ratio Fitter"), 142, &m1519_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
void t248_CustomAttributesCacheGenerator_m_AspectMode(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t248_CustomAttributesCacheGenerator_m_AspectRatio(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t248__CustomAttributeCache = {
3,
NULL,
&t248_CustomAttributesCacheGenerator
};
CustomAttributesCache t248__CustomAttributeCache_m_AspectMode = {
1,
NULL,
&t248_CustomAttributesCacheGenerator_m_AspectMode
};
CustomAttributesCache t248__CustomAttributeCache_m_AspectRatio = {
1,
NULL,
&t248_CustomAttributesCacheGenerator_m_AspectRatio
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t248_0_0_0;
extern Il2CppType t248_1_0_0;
struct t248;
extern CustomAttributesCache t248__CustomAttributeCache;
extern CustomAttributesCache t248__CustomAttributeCache_m_AspectMode;
extern CustomAttributesCache t248__CustomAttributeCache_m_AspectRatio;
TypeInfo t248_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "AspectRatioFitter", "UnityEngine.UI", t248_MIs, t248_PIs, t248_FIs, NULL, &t55_TI, t248_TI__nestedTypes, NULL, &t248_TI, t248_ITIs, t248_VT, &t248__CustomAttributeCache, &t248_TI, &t248_0_0_0, &t248_1_0_0, t248_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t248), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 15, 3, 4, 0, 1, 19, 2, 2};
#include "t249.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t249_TI;
#include "t249MD.h"



// Metadata Definition UnityEngine.UI.CanvasScaler/ScaleMode
extern Il2CppType t44_0_0_1542;
FieldInfo t249_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t249_TI, offsetof(t249, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t249_0_0_32854;
FieldInfo t249_f2_FieldInfo = 
{
	"ConstantPixelSize", &t249_0_0_32854, &t249_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t249_0_0_32854;
FieldInfo t249_f3_FieldInfo = 
{
	"ScaleWithScreenSize", &t249_0_0_32854, &t249_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t249_0_0_32854;
FieldInfo t249_f4_FieldInfo = 
{
	"ConstantPhysicalSize", &t249_0_0_32854, &t249_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t249_FIs[] =
{
	&t249_f1_FieldInfo,
	&t249_f2_FieldInfo,
	&t249_f3_FieldInfo,
	&t249_f4_FieldInfo,
	NULL
};
static const int32_t t249_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t249_f2_DefaultValue = 
{
	&t249_f2_FieldInfo, { (char*)&t249_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t249_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t249_f3_DefaultValue = 
{
	&t249_f3_FieldInfo, { (char*)&t249_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t249_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t249_f4_DefaultValue = 
{
	&t249_f4_FieldInfo, { (char*)&t249_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t249_FDVs[] = 
{
	&t249_f2_DefaultValue,
	&t249_f3_DefaultValue,
	&t249_f4_DefaultValue,
	NULL
};
static MethodInfo* t249_MIs[] =
{
	NULL
};
static MethodInfo* t249_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t249_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t249_0_0_0;
extern Il2CppType t249_1_0_0;
extern TypeInfo t252_TI;
TypeInfo t249_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ScaleMode", "", t249_MIs, NULL, t249_FIs, NULL, &t49_TI, NULL, &t252_TI, &t44_TI, NULL, t249_VT, &EmptyCustomAttributesCache, &t44_TI, &t249_0_0_0, &t249_1_0_0, t249_IOs, NULL, NULL, t249_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t249)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 258, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#include "t250.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t250_TI;
#include "t250MD.h"



// Metadata Definition UnityEngine.UI.CanvasScaler/ScreenMatchMode
extern Il2CppType t44_0_0_1542;
FieldInfo t250_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t250_TI, offsetof(t250, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t250_0_0_32854;
FieldInfo t250_f2_FieldInfo = 
{
	"MatchWidthOrHeight", &t250_0_0_32854, &t250_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t250_0_0_32854;
FieldInfo t250_f3_FieldInfo = 
{
	"Expand", &t250_0_0_32854, &t250_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t250_0_0_32854;
FieldInfo t250_f4_FieldInfo = 
{
	"Shrink", &t250_0_0_32854, &t250_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t250_FIs[] =
{
	&t250_f1_FieldInfo,
	&t250_f2_FieldInfo,
	&t250_f3_FieldInfo,
	&t250_f4_FieldInfo,
	NULL
};
static const int32_t t250_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t250_f2_DefaultValue = 
{
	&t250_f2_FieldInfo, { (char*)&t250_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t250_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t250_f3_DefaultValue = 
{
	&t250_f3_FieldInfo, { (char*)&t250_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t250_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t250_f4_DefaultValue = 
{
	&t250_f4_FieldInfo, { (char*)&t250_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t250_FDVs[] = 
{
	&t250_f2_DefaultValue,
	&t250_f3_DefaultValue,
	&t250_f4_DefaultValue,
	NULL
};
static MethodInfo* t250_MIs[] =
{
	NULL
};
static MethodInfo* t250_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t250_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t250_0_0_0;
extern Il2CppType t250_1_0_0;
TypeInfo t250_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ScreenMatchMode", "", t250_MIs, NULL, t250_FIs, NULL, &t49_TI, NULL, &t252_TI, &t44_TI, NULL, t250_VT, &EmptyCustomAttributesCache, &t44_TI, &t250_0_0_0, &t250_1_0_0, t250_IOs, NULL, NULL, t250_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t250)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 258, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#include "t251.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t251_TI;
#include "t251MD.h"



// Metadata Definition UnityEngine.UI.CanvasScaler/Unit
extern Il2CppType t44_0_0_1542;
FieldInfo t251_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t251_TI, offsetof(t251, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t251_0_0_32854;
FieldInfo t251_f2_FieldInfo = 
{
	"Centimeters", &t251_0_0_32854, &t251_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t251_0_0_32854;
FieldInfo t251_f3_FieldInfo = 
{
	"Millimeters", &t251_0_0_32854, &t251_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t251_0_0_32854;
FieldInfo t251_f4_FieldInfo = 
{
	"Inches", &t251_0_0_32854, &t251_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t251_0_0_32854;
FieldInfo t251_f5_FieldInfo = 
{
	"Points", &t251_0_0_32854, &t251_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t251_0_0_32854;
FieldInfo t251_f6_FieldInfo = 
{
	"Picas", &t251_0_0_32854, &t251_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t251_FIs[] =
{
	&t251_f1_FieldInfo,
	&t251_f2_FieldInfo,
	&t251_f3_FieldInfo,
	&t251_f4_FieldInfo,
	&t251_f5_FieldInfo,
	&t251_f6_FieldInfo,
	NULL
};
static const int32_t t251_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t251_f2_DefaultValue = 
{
	&t251_f2_FieldInfo, { (char*)&t251_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t251_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t251_f3_DefaultValue = 
{
	&t251_f3_FieldInfo, { (char*)&t251_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t251_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t251_f4_DefaultValue = 
{
	&t251_f4_FieldInfo, { (char*)&t251_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t251_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t251_f5_DefaultValue = 
{
	&t251_f5_FieldInfo, { (char*)&t251_f5_DefaultValueData, &t44_0_0_0 }};
static const int32_t t251_f6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t251_f6_DefaultValue = 
{
	&t251_f6_FieldInfo, { (char*)&t251_f6_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t251_FDVs[] = 
{
	&t251_f2_DefaultValue,
	&t251_f3_DefaultValue,
	&t251_f4_DefaultValue,
	&t251_f5_DefaultValue,
	&t251_f6_DefaultValue,
	NULL
};
static MethodInfo* t251_MIs[] =
{
	NULL
};
static MethodInfo* t251_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t251_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t251_0_0_0;
extern Il2CppType t251_1_0_0;
TypeInfo t251_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Unit", "", t251_MIs, NULL, t251_FIs, NULL, &t49_TI, NULL, &t252_TI, &t44_TI, NULL, t251_VT, &EmptyCustomAttributesCache, &t44_TI, &t251_0_0_0, &t251_1_0_0, t251_IOs, NULL, NULL, t251_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t251)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 258, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 6, 0, 0, 23, 0, 3};
#include "t252.h"
#ifndef _MSC_VER
#else
#endif
#include "t252MD.h"

#include "t361.h"
#include "t45MD.h"
extern MethodInfo m1608_MI;
extern MethodInfo m1061_MI;
extern MethodInfo m1066_MI;
extern MethodInfo m1067_MI;
extern MethodInfo m1947_MI;
extern MethodInfo m1605_MI;
extern MethodInfo m1062_MI;
extern MethodInfo m1063_MI;
extern MethodInfo m1064_MI;
extern MethodInfo m1065_MI;
extern MethodInfo m1611_MI;
extern MethodInfo m1612_MI;
extern MethodInfo m1948_MI;
extern MethodInfo m1949_MI;
extern MethodInfo m1950_MI;
extern MethodInfo m1951_MI;
extern MethodInfo m1952_MI;
struct t28;
#define m1608(__this, method) (t3 *)m68_gshared((t28 *)__this, method)


extern MethodInfo m1037_MI;
 void m1037 (t252 * __this, MethodInfo* method){
	{
		__this->f4 = (100.0f);
		__this->f5 = (1.0f);
		t17  L_0 = {0};
		m62(&L_0, (800.0f), (600.0f), &m62_MI);
		__this->f6 = L_0;
		__this->f9 = 3;
		__this->f10 = (96.0f);
		__this->f11 = (96.0f);
		__this->f12 = (1.0f);
		__this->f14 = (1.0f);
		__this->f15 = (100.0f);
		m168(__this, &m168_MI);
		return;
	}
}
extern MethodInfo m1038_MI;
 int32_t m1038 (t252 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m1039_MI;
 void m1039 (t252 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f3 = p0;
		return;
	}
}
extern MethodInfo m1040_MI;
 float m1040 (t252 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m1041_MI;
 void m1041 (t252 * __this, float p0, MethodInfo* method){
	{
		__this->f4 = p0;
		return;
	}
}
extern MethodInfo m1042_MI;
 float m1042 (t252 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f5);
		return L_0;
	}
}
extern MethodInfo m1043_MI;
 void m1043 (t252 * __this, float p0, MethodInfo* method){
	{
		__this->f5 = p0;
		return;
	}
}
extern MethodInfo m1044_MI;
 t17  m1044 (t252 * __this, MethodInfo* method){
	{
		t17  L_0 = (__this->f6);
		return L_0;
	}
}
extern MethodInfo m1045_MI;
 void m1045 (t252 * __this, t17  p0, MethodInfo* method){
	{
		__this->f6 = p0;
		return;
	}
}
extern MethodInfo m1046_MI;
 int32_t m1046 (t252 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f7);
		return L_0;
	}
}
extern MethodInfo m1047_MI;
 void m1047 (t252 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f7 = p0;
		return;
	}
}
extern MethodInfo m1048_MI;
 float m1048 (t252 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f8);
		return L_0;
	}
}
extern MethodInfo m1049_MI;
 void m1049 (t252 * __this, float p0, MethodInfo* method){
	{
		__this->f8 = p0;
		return;
	}
}
extern MethodInfo m1050_MI;
 int32_t m1050 (t252 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f9);
		return L_0;
	}
}
extern MethodInfo m1051_MI;
 void m1051 (t252 * __this, int32_t p0, MethodInfo* method){
	{
		__this->f9 = p0;
		return;
	}
}
extern MethodInfo m1052_MI;
 float m1052 (t252 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f10);
		return L_0;
	}
}
extern MethodInfo m1053_MI;
 void m1053 (t252 * __this, float p0, MethodInfo* method){
	{
		__this->f10 = p0;
		return;
	}
}
extern MethodInfo m1054_MI;
 float m1054 (t252 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f11);
		return L_0;
	}
}
extern MethodInfo m1055_MI;
 void m1055 (t252 * __this, float p0, MethodInfo* method){
	{
		__this->f11 = p0;
		return;
	}
}
extern MethodInfo m1056_MI;
 float m1056 (t252 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f12);
		return L_0;
	}
}
extern MethodInfo m1057_MI;
 void m1057 (t252 * __this, float p0, MethodInfo* method){
	{
		__this->f12 = p0;
		return;
	}
}
extern MethodInfo m1058_MI;
 void m1058 (t252 * __this, MethodInfo* method){
	{
		m170(__this, &m170_MI);
		t3 * L_0 = m1608(__this, &m1608_MI);
		__this->f13 = L_0;
		VirtActionInvoker0::Invoke(&m1061_MI, __this);
		return;
	}
}
extern MethodInfo m1059_MI;
 void m1059 (t252 * __this, MethodInfo* method){
	{
		m1066(__this, (1.0f), &m1066_MI);
		m1067(__this, (100.0f), &m1067_MI);
		m172(__this, &m172_MI);
		return;
	}
}
extern MethodInfo m1060_MI;
 void m1060 (t252 * __this, MethodInfo* method){
	{
		VirtActionInvoker0::Invoke(&m1061_MI, __this);
		return;
	}
}
 void m1061 (t252 * __this, MethodInfo* method){
	int32_t V_0 = {0};
	{
		t3 * L_0 = (__this->f13);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		t3 * L_2 = (__this->f13);
		bool L_3 = m1947(L_2, &m1947_MI);
		if (L_3)
		{
			goto IL_0022;
		}
	}

IL_0021:
	{
		return;
	}

IL_0022:
	{
		t3 * L_4 = (__this->f13);
		int32_t L_5 = m1605(L_4, &m1605_MI);
		if ((((uint32_t)L_5) != ((uint32_t)2)))
		{
			goto IL_003a;
		}
	}
	{
		VirtActionInvoker0::Invoke(&m1062_MI, __this);
		return;
	}

IL_003a:
	{
		int32_t L_6 = (__this->f3);
		V_0 = L_6;
		if (V_0 == 0)
		{
			goto IL_0058;
		}
		if (V_0 == 1)
		{
			goto IL_0063;
		}
		if (V_0 == 2)
		{
			goto IL_006e;
		}
	}
	{
		goto IL_0079;
	}

IL_0058:
	{
		VirtActionInvoker0::Invoke(&m1063_MI, __this);
		goto IL_0079;
	}

IL_0063:
	{
		VirtActionInvoker0::Invoke(&m1064_MI, __this);
		goto IL_0079;
	}

IL_006e:
	{
		VirtActionInvoker0::Invoke(&m1065_MI, __this);
		goto IL_0079;
	}

IL_0079:
	{
		return;
	}
}
 void m1062 (t252 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f12);
		m1066(__this, L_0, &m1066_MI);
		float L_1 = (__this->f4);
		m1067(__this, L_1, &m1067_MI);
		return;
	}
}
 void m1063 (t252 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f5);
		m1066(__this, L_0, &m1066_MI);
		float L_1 = (__this->f4);
		m1067(__this, L_1, &m1067_MI);
		return;
	}
}
 void m1064 (t252 * __this, MethodInfo* method){
	t17  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = {0};
	{
		int32_t L_0 = m1611(NULL, &m1611_MI);
		int32_t L_1 = m1612(NULL, &m1612_MI);
		m62((&V_0), (((float)L_0)), (((float)L_1)), &m62_MI);
		V_1 = (0.0f);
		int32_t L_2 = (__this->f7);
		V_5 = L_2;
		if (V_5 == 0)
		{
			goto IL_0039;
		}
		if (V_5 == 1)
		{
			goto IL_0096;
		}
		if (V_5 == 2)
		{
			goto IL_00c7;
		}
	}
	{
		goto IL_00f8;
	}

IL_0039:
	{
		float L_3 = ((&V_0)->f1);
		t17 * L_4 = &(__this->f6);
		float L_5 = (L_4->f1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_6 = m1948(NULL, ((float)((float)L_3/(float)L_5)), (2.0f), &m1948_MI);
		V_2 = L_6;
		float L_7 = ((&V_0)->f2);
		t17 * L_8 = &(__this->f6);
		float L_9 = (L_8->f2);
		float L_10 = m1948(NULL, ((float)((float)L_7/(float)L_9)), (2.0f), &m1948_MI);
		V_3 = L_10;
		float L_11 = (__this->f8);
		float L_12 = m1674(NULL, V_2, V_3, L_11, &m1674_MI);
		V_4 = L_12;
		float L_13 = powf((2.0f), V_4);
		V_1 = L_13;
		goto IL_00f8;
	}

IL_0096:
	{
		float L_14 = ((&V_0)->f1);
		t17 * L_15 = &(__this->f6);
		float L_16 = (L_15->f1);
		float L_17 = ((&V_0)->f2);
		t17 * L_18 = &(__this->f6);
		float L_19 = (L_18->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_20 = m1949(NULL, ((float)((float)L_14/(float)L_16)), ((float)((float)L_17/(float)L_19)), &m1949_MI);
		V_1 = L_20;
		goto IL_00f8;
	}

IL_00c7:
	{
		float L_21 = ((&V_0)->f1);
		t17 * L_22 = &(__this->f6);
		float L_23 = (L_22->f1);
		float L_24 = ((&V_0)->f2);
		t17 * L_25 = &(__this->f6);
		float L_26 = (L_25->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_27 = m1889(NULL, ((float)((float)L_21/(float)L_23)), ((float)((float)L_24/(float)L_26)), &m1889_MI);
		V_1 = L_27;
		goto IL_00f8;
	}

IL_00f8:
	{
		m1066(__this, V_1, &m1066_MI);
		float L_28 = (__this->f4);
		m1067(__this, L_28, &m1067_MI);
		return;
	}
}
 void m1065 (t252 * __this, MethodInfo* method){
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = {0};
	float G_B3_0 = 0.0f;
	{
		float L_0 = m1950(NULL, &m1950_MI);
		V_0 = L_0;
		if ((((float)V_0) != ((float)(0.0f))))
		{
			goto IL_001c;
		}
	}
	{
		float L_1 = (__this->f10);
		G_B3_0 = L_1;
		goto IL_001d;
	}

IL_001c:
	{
		G_B3_0 = V_0;
	}

IL_001d:
	{
		V_1 = G_B3_0;
		V_2 = (1.0f);
		int32_t L_2 = (__this->f9);
		V_3 = L_2;
		if (V_3 == 0)
		{
			goto IL_004a;
		}
		if (V_3 == 1)
		{
			goto IL_0055;
		}
		if (V_3 == 2)
		{
			goto IL_0060;
		}
		if (V_3 == 3)
		{
			goto IL_006b;
		}
		if (V_3 == 4)
		{
			goto IL_0076;
		}
	}
	{
		goto IL_0081;
	}

IL_004a:
	{
		V_2 = (2.54f);
		goto IL_0081;
	}

IL_0055:
	{
		V_2 = (25.4f);
		goto IL_0081;
	}

IL_0060:
	{
		V_2 = (1.0f);
		goto IL_0081;
	}

IL_006b:
	{
		V_2 = (72.0f);
		goto IL_0081;
	}

IL_0076:
	{
		V_2 = (6.0f);
		goto IL_0081;
	}

IL_0081:
	{
		m1066(__this, ((float)((float)V_1/(float)V_2)), &m1066_MI);
		float L_3 = (__this->f4);
		float L_4 = (__this->f11);
		m1067(__this, ((float)((float)((float)((float)L_3*(float)V_2))/(float)L_4)), &m1067_MI);
		return;
	}
}
 void m1066 (t252 * __this, float p0, MethodInfo* method){
	{
		float L_0 = (__this->f14);
		if ((((float)p0) != ((float)L_0)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		t3 * L_1 = (__this->f13);
		m1951(L_1, p0, &m1951_MI);
		__this->f14 = p0;
		return;
	}
}
 void m1067 (t252 * __this, float p0, MethodInfo* method){
	{
		float L_0 = (__this->f15);
		if ((((float)p0) != ((float)L_0)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		t3 * L_1 = (__this->f13);
		m1952(L_1, p0, &m1952_MI);
		__this->f15 = p0;
		return;
	}
}
// Metadata Definition UnityEngine.UI.CanvasScaler
extern Il2CppType t22_0_0_32849;
FieldInfo t252_f2_FieldInfo = 
{
	"kLogBase", &t22_0_0_32849, &t252_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t249_0_0_1;
extern CustomAttributesCache t252__CustomAttributeCache_m_UiScaleMode;
FieldInfo t252_f3_FieldInfo = 
{
	"m_UiScaleMode", &t249_0_0_1, &t252_TI, offsetof(t252, f3), &t252__CustomAttributeCache_m_UiScaleMode};
extern Il2CppType t22_0_0_4;
extern CustomAttributesCache t252__CustomAttributeCache_m_ReferencePixelsPerUnit;
FieldInfo t252_f4_FieldInfo = 
{
	"m_ReferencePixelsPerUnit", &t22_0_0_4, &t252_TI, offsetof(t252, f4), &t252__CustomAttributeCache_m_ReferencePixelsPerUnit};
extern Il2CppType t22_0_0_4;
extern CustomAttributesCache t252__CustomAttributeCache_m_ScaleFactor;
FieldInfo t252_f5_FieldInfo = 
{
	"m_ScaleFactor", &t22_0_0_4, &t252_TI, offsetof(t252, f5), &t252__CustomAttributeCache_m_ScaleFactor};
extern Il2CppType t17_0_0_4;
extern CustomAttributesCache t252__CustomAttributeCache_m_ReferenceResolution;
FieldInfo t252_f6_FieldInfo = 
{
	"m_ReferenceResolution", &t17_0_0_4, &t252_TI, offsetof(t252, f6), &t252__CustomAttributeCache_m_ReferenceResolution};
extern Il2CppType t250_0_0_4;
extern CustomAttributesCache t252__CustomAttributeCache_m_ScreenMatchMode;
FieldInfo t252_f7_FieldInfo = 
{
	"m_ScreenMatchMode", &t250_0_0_4, &t252_TI, offsetof(t252, f7), &t252__CustomAttributeCache_m_ScreenMatchMode};
extern Il2CppType t22_0_0_4;
extern CustomAttributesCache t252__CustomAttributeCache_m_MatchWidthOrHeight;
FieldInfo t252_f8_FieldInfo = 
{
	"m_MatchWidthOrHeight", &t22_0_0_4, &t252_TI, offsetof(t252, f8), &t252__CustomAttributeCache_m_MatchWidthOrHeight};
extern Il2CppType t251_0_0_4;
extern CustomAttributesCache t252__CustomAttributeCache_m_PhysicalUnit;
FieldInfo t252_f9_FieldInfo = 
{
	"m_PhysicalUnit", &t251_0_0_4, &t252_TI, offsetof(t252, f9), &t252__CustomAttributeCache_m_PhysicalUnit};
extern Il2CppType t22_0_0_4;
extern CustomAttributesCache t252__CustomAttributeCache_m_FallbackScreenDPI;
FieldInfo t252_f10_FieldInfo = 
{
	"m_FallbackScreenDPI", &t22_0_0_4, &t252_TI, offsetof(t252, f10), &t252__CustomAttributeCache_m_FallbackScreenDPI};
extern Il2CppType t22_0_0_4;
extern CustomAttributesCache t252__CustomAttributeCache_m_DefaultSpriteDPI;
FieldInfo t252_f11_FieldInfo = 
{
	"m_DefaultSpriteDPI", &t22_0_0_4, &t252_TI, offsetof(t252, f11), &t252__CustomAttributeCache_m_DefaultSpriteDPI};
extern Il2CppType t22_0_0_4;
extern CustomAttributesCache t252__CustomAttributeCache_m_DynamicPixelsPerUnit;
FieldInfo t252_f12_FieldInfo = 
{
	"m_DynamicPixelsPerUnit", &t22_0_0_4, &t252_TI, offsetof(t252, f12), &t252__CustomAttributeCache_m_DynamicPixelsPerUnit};
extern Il2CppType t3_0_0_1;
FieldInfo t252_f13_FieldInfo = 
{
	"m_Canvas", &t3_0_0_1, &t252_TI, offsetof(t252, f13), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_129;
FieldInfo t252_f14_FieldInfo = 
{
	"m_PrevScaleFactor", &t22_0_0_129, &t252_TI, offsetof(t252, f14), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_129;
FieldInfo t252_f15_FieldInfo = 
{
	"m_PrevReferencePixelsPerUnit", &t22_0_0_129, &t252_TI, offsetof(t252, f15), &EmptyCustomAttributesCache};
static FieldInfo* t252_FIs[] =
{
	&t252_f2_FieldInfo,
	&t252_f3_FieldInfo,
	&t252_f4_FieldInfo,
	&t252_f5_FieldInfo,
	&t252_f6_FieldInfo,
	&t252_f7_FieldInfo,
	&t252_f8_FieldInfo,
	&t252_f9_FieldInfo,
	&t252_f10_FieldInfo,
	&t252_f11_FieldInfo,
	&t252_f12_FieldInfo,
	&t252_f13_FieldInfo,
	&t252_f14_FieldInfo,
	&t252_f15_FieldInfo,
	NULL
};
static const float t252_f2_DefaultValueData = 2.0f;
static Il2CppFieldDefaultValueEntry t252_f2_DefaultValue = 
{
	&t252_f2_FieldInfo, { (char*)&t252_f2_DefaultValueData, &t22_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t252_FDVs[] = 
{
	&t252_f2_DefaultValue,
	NULL
};
static PropertyInfo t252____uiScaleMode_PropertyInfo = 
{
	&t252_TI, "uiScaleMode", &m1038_MI, &m1039_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t252____referencePixelsPerUnit_PropertyInfo = 
{
	&t252_TI, "referencePixelsPerUnit", &m1040_MI, &m1041_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t252____scaleFactor_PropertyInfo = 
{
	&t252_TI, "scaleFactor", &m1042_MI, &m1043_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t252____referenceResolution_PropertyInfo = 
{
	&t252_TI, "referenceResolution", &m1044_MI, &m1045_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t252____screenMatchMode_PropertyInfo = 
{
	&t252_TI, "screenMatchMode", &m1046_MI, &m1047_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t252____matchWidthOrHeight_PropertyInfo = 
{
	&t252_TI, "matchWidthOrHeight", &m1048_MI, &m1049_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t252____physicalUnit_PropertyInfo = 
{
	&t252_TI, "physicalUnit", &m1050_MI, &m1051_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t252____fallbackScreenDPI_PropertyInfo = 
{
	&t252_TI, "fallbackScreenDPI", &m1052_MI, &m1053_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t252____defaultSpriteDPI_PropertyInfo = 
{
	&t252_TI, "defaultSpriteDPI", &m1054_MI, &m1055_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t252____dynamicPixelsPerUnit_PropertyInfo = 
{
	&t252_TI, "dynamicPixelsPerUnit", &m1056_MI, &m1057_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t252_PIs[] =
{
	&t252____uiScaleMode_PropertyInfo,
	&t252____referencePixelsPerUnit_PropertyInfo,
	&t252____scaleFactor_PropertyInfo,
	&t252____referenceResolution_PropertyInfo,
	&t252____screenMatchMode_PropertyInfo,
	&t252____matchWidthOrHeight_PropertyInfo,
	&t252____physicalUnit_PropertyInfo,
	&t252____fallbackScreenDPI_PropertyInfo,
	&t252____defaultSpriteDPI_PropertyInfo,
	&t252____dynamicPixelsPerUnit_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1037_MI = 
{
	".ctor", (methodPointerType)&m1037, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1021, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t249_0_0_0;
extern void* RuntimeInvoker_t249 (MethodInfo* method, void* obj, void** args);
MethodInfo m1038_MI = 
{
	"get_uiScaleMode", (methodPointerType)&m1038, &t252_TI, &t249_0_0_0, RuntimeInvoker_t249, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1022, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t249_0_0_0;
static ParameterInfo t252_m1039_ParameterInfos[] = 
{
	{"value", 0, 134218324, &EmptyCustomAttributesCache, &t249_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1039_MI = 
{
	"set_uiScaleMode", (methodPointerType)&m1039, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t252_m1039_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1023, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1040_MI = 
{
	"get_referencePixelsPerUnit", (methodPointerType)&m1040, &t252_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1024, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t252_m1041_ParameterInfos[] = 
{
	{"value", 0, 134218325, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1041_MI = 
{
	"set_referencePixelsPerUnit", (methodPointerType)&m1041, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t252_m1041_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1025, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1042_MI = 
{
	"get_scaleFactor", (methodPointerType)&m1042, &t252_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1026, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t252_m1043_ParameterInfos[] = 
{
	{"value", 0, 134218326, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1043_MI = 
{
	"set_scaleFactor", (methodPointerType)&m1043, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t252_m1043_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1027, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m1044_MI = 
{
	"get_referenceResolution", (methodPointerType)&m1044, &t252_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1028, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
static ParameterInfo t252_m1045_ParameterInfos[] = 
{
	{"value", 0, 134218327, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m1045_MI = 
{
	"set_referenceResolution", (methodPointerType)&m1045, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21_t17, t252_m1045_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1029, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t250_0_0_0;
extern void* RuntimeInvoker_t250 (MethodInfo* method, void* obj, void** args);
MethodInfo m1046_MI = 
{
	"get_screenMatchMode", (methodPointerType)&m1046, &t252_TI, &t250_0_0_0, RuntimeInvoker_t250, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1030, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t250_0_0_0;
static ParameterInfo t252_m1047_ParameterInfos[] = 
{
	{"value", 0, 134218328, &EmptyCustomAttributesCache, &t250_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1047_MI = 
{
	"set_screenMatchMode", (methodPointerType)&m1047, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t252_m1047_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1031, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1048_MI = 
{
	"get_matchWidthOrHeight", (methodPointerType)&m1048, &t252_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1032, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t252_m1049_ParameterInfos[] = 
{
	{"value", 0, 134218329, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1049_MI = 
{
	"set_matchWidthOrHeight", (methodPointerType)&m1049, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t252_m1049_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1033, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t251_0_0_0;
extern void* RuntimeInvoker_t251 (MethodInfo* method, void* obj, void** args);
MethodInfo m1050_MI = 
{
	"get_physicalUnit", (methodPointerType)&m1050, &t252_TI, &t251_0_0_0, RuntimeInvoker_t251, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1034, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t251_0_0_0;
static ParameterInfo t252_m1051_ParameterInfos[] = 
{
	{"value", 0, 134218330, &EmptyCustomAttributesCache, &t251_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1051_MI = 
{
	"set_physicalUnit", (methodPointerType)&m1051, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t252_m1051_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1035, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1052_MI = 
{
	"get_fallbackScreenDPI", (methodPointerType)&m1052, &t252_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1036, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t252_m1053_ParameterInfos[] = 
{
	{"value", 0, 134218331, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1053_MI = 
{
	"set_fallbackScreenDPI", (methodPointerType)&m1053, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t252_m1053_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1037, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1054_MI = 
{
	"get_defaultSpriteDPI", (methodPointerType)&m1054, &t252_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1038, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t252_m1055_ParameterInfos[] = 
{
	{"value", 0, 134218332, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1055_MI = 
{
	"set_defaultSpriteDPI", (methodPointerType)&m1055, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t252_m1055_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1039, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1056_MI = 
{
	"get_dynamicPixelsPerUnit", (methodPointerType)&m1056, &t252_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1040, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t252_m1057_ParameterInfos[] = 
{
	{"value", 0, 134218333, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1057_MI = 
{
	"set_dynamicPixelsPerUnit", (methodPointerType)&m1057, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t252_m1057_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1041, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1058_MI = 
{
	"OnEnable", (methodPointerType)&m1058, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 5, 0, false, false, 1042, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1059_MI = 
{
	"OnDisable", (methodPointerType)&m1059, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 7, 0, false, false, 1043, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1060_MI = 
{
	"Update", (methodPointerType)&m1060, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 15, 0, false, false, 1044, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1061_MI = 
{
	"Handle", (methodPointerType)&m1061, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 16, 0, false, false, 1045, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1062_MI = 
{
	"HandleWorldCanvas", (methodPointerType)&m1062, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 17, 0, false, false, 1046, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1063_MI = 
{
	"HandleConstantPixelSize", (methodPointerType)&m1063, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 18, 0, false, false, 1047, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1064_MI = 
{
	"HandleScaleWithScreenSize", (methodPointerType)&m1064, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 19, 0, false, false, 1048, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1065_MI = 
{
	"HandleConstantPhysicalSize", (methodPointerType)&m1065, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 20, 0, false, false, 1049, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t252_m1066_ParameterInfos[] = 
{
	{"scaleFactor", 0, 134218334, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1066_MI = 
{
	"SetScaleFactor", (methodPointerType)&m1066, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t252_m1066_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 1, false, false, 1050, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t252_m1067_ParameterInfos[] = 
{
	{"referencePixelsPerUnit", 0, 134218335, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1067_MI = 
{
	"SetReferencePixelsPerUnit", (methodPointerType)&m1067, &t252_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t252_m1067_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 1, false, false, 1051, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t252_MIs[] =
{
	&m1037_MI,
	&m1038_MI,
	&m1039_MI,
	&m1040_MI,
	&m1041_MI,
	&m1042_MI,
	&m1043_MI,
	&m1044_MI,
	&m1045_MI,
	&m1046_MI,
	&m1047_MI,
	&m1048_MI,
	&m1049_MI,
	&m1050_MI,
	&m1051_MI,
	&m1052_MI,
	&m1053_MI,
	&m1054_MI,
	&m1055_MI,
	&m1056_MI,
	&m1057_MI,
	&m1058_MI,
	&m1059_MI,
	&m1060_MI,
	&m1061_MI,
	&m1062_MI,
	&m1063_MI,
	&m1064_MI,
	&m1065_MI,
	&m1066_MI,
	&m1067_MI,
	NULL
};
extern TypeInfo t249_TI;
extern TypeInfo t250_TI;
extern TypeInfo t251_TI;
static TypeInfo* t252_TI__nestedTypes[4] =
{
	&t249_TI,
	&t250_TI,
	&t251_TI,
	NULL
};
static MethodInfo* t252_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1058_MI,
	&m171_MI,
	&m1059_MI,
	&m173_MI,
	&m174_MI,
	&m175_MI,
	&m176_MI,
	&m177_MI,
	&m178_MI,
	&m179_MI,
	&m1060_MI,
	&m1061_MI,
	&m1062_MI,
	&m1063_MI,
	&m1064_MI,
	&m1065_MI,
};
extern TypeInfo t3_TI;
void t252_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t318 * tmp;
		tmp = (t318 *)il2cpp_codegen_object_new (&t318_TI);
		m1404(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t3_TI)), &m1404_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("Layout/Canvas Scaler"), 101, &m1519_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t360 * tmp;
		tmp = (t360 *)il2cpp_codegen_object_new (&t360_TI);
		m1604(tmp, &m1604_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
void t252_CustomAttributesCacheGenerator_m_UiScaleMode(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t399 * tmp;
		tmp = (t399 *)il2cpp_codegen_object_new (&t399_TI);
		m1897(tmp, il2cpp_codegen_string_new_wrapper("Determines how UI elements in the Canvas are scaled."), &m1897_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t252_CustomAttributesCacheGenerator_m_ReferencePixelsPerUnit(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t399 * tmp;
		tmp = (t399 *)il2cpp_codegen_object_new (&t399_TI);
		m1897(tmp, il2cpp_codegen_string_new_wrapper("If a sprite has this 'Pixels Per Unit' setting, then one pixel in the sprite will cover one unit in the UI."), &m1897_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t252_CustomAttributesCacheGenerator_m_ScaleFactor(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t399 * tmp;
		tmp = (t399 *)il2cpp_codegen_object_new (&t399_TI);
		m1897(tmp, il2cpp_codegen_string_new_wrapper("Scales all UI elements in the Canvas by this factor."), &m1897_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t252_CustomAttributesCacheGenerator_m_ReferenceResolution(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t399 * tmp;
		tmp = (t399 *)il2cpp_codegen_object_new (&t399_TI);
		m1897(tmp, il2cpp_codegen_string_new_wrapper("The resolution the UI layout is designed for. If the screen resolution is larger, the UI will be scaled up, and if it's smaller, the UI will be scaled down. This is done in accordance with the Screen Match Mode."), &m1897_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t252_CustomAttributesCacheGenerator_m_ScreenMatchMode(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t399 * tmp;
		tmp = (t399 *)il2cpp_codegen_object_new (&t399_TI);
		m1897(tmp, il2cpp_codegen_string_new_wrapper("A mode used to scale the canvas area if the aspect ratio of the current resolution doesn't fit the reference resolution."), &m1897_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t349_TI;
#include "t349.h"
#include "t349MD.h"
extern MethodInfo m1539_MI;
void t252_CustomAttributesCacheGenerator_m_MatchWidthOrHeight(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t399 * tmp;
		tmp = (t399 *)il2cpp_codegen_object_new (&t399_TI);
		m1897(tmp, il2cpp_codegen_string_new_wrapper("Determines if the scaling is using the width or height as reference, or a mix in between."), &m1897_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t349 * tmp;
		tmp = (t349 *)il2cpp_codegen_object_new (&t349_TI);
		m1539(tmp, 0.0f, 1.0f, &m1539_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
void t252_CustomAttributesCacheGenerator_m_PhysicalUnit(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t399 * tmp;
		tmp = (t399 *)il2cpp_codegen_object_new (&t399_TI);
		m1897(tmp, il2cpp_codegen_string_new_wrapper("The physical unit to specify positions and sizes in."), &m1897_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t252_CustomAttributesCacheGenerator_m_FallbackScreenDPI(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t399 * tmp;
		tmp = (t399 *)il2cpp_codegen_object_new (&t399_TI);
		m1897(tmp, il2cpp_codegen_string_new_wrapper("The DPI to assume if the screen DPI is not known."), &m1897_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t252_CustomAttributesCacheGenerator_m_DefaultSpriteDPI(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t399 * tmp;
		tmp = (t399 *)il2cpp_codegen_object_new (&t399_TI);
		m1897(tmp, il2cpp_codegen_string_new_wrapper("The pixels per inch to use for sprites that have a 'Pixels Per Unit' setting that matches the 'Reference Pixels Per Unit' setting."), &m1897_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t252_CustomAttributesCacheGenerator_m_DynamicPixelsPerUnit(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t399 * tmp;
		tmp = (t399 *)il2cpp_codegen_object_new (&t399_TI);
		m1897(tmp, il2cpp_codegen_string_new_wrapper("The amount of pixels per unit to use for dynamically created bitmaps in the UI, such as Text."), &m1897_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t252__CustomAttributeCache = {
3,
NULL,
&t252_CustomAttributesCacheGenerator
};
CustomAttributesCache t252__CustomAttributeCache_m_UiScaleMode = {
2,
NULL,
&t252_CustomAttributesCacheGenerator_m_UiScaleMode
};
CustomAttributesCache t252__CustomAttributeCache_m_ReferencePixelsPerUnit = {
2,
NULL,
&t252_CustomAttributesCacheGenerator_m_ReferencePixelsPerUnit
};
CustomAttributesCache t252__CustomAttributeCache_m_ScaleFactor = {
2,
NULL,
&t252_CustomAttributesCacheGenerator_m_ScaleFactor
};
CustomAttributesCache t252__CustomAttributeCache_m_ReferenceResolution = {
2,
NULL,
&t252_CustomAttributesCacheGenerator_m_ReferenceResolution
};
CustomAttributesCache t252__CustomAttributeCache_m_ScreenMatchMode = {
2,
NULL,
&t252_CustomAttributesCacheGenerator_m_ScreenMatchMode
};
CustomAttributesCache t252__CustomAttributeCache_m_MatchWidthOrHeight = {
3,
NULL,
&t252_CustomAttributesCacheGenerator_m_MatchWidthOrHeight
};
CustomAttributesCache t252__CustomAttributeCache_m_PhysicalUnit = {
2,
NULL,
&t252_CustomAttributesCacheGenerator_m_PhysicalUnit
};
CustomAttributesCache t252__CustomAttributeCache_m_FallbackScreenDPI = {
2,
NULL,
&t252_CustomAttributesCacheGenerator_m_FallbackScreenDPI
};
CustomAttributesCache t252__CustomAttributeCache_m_DefaultSpriteDPI = {
2,
NULL,
&t252_CustomAttributesCacheGenerator_m_DefaultSpriteDPI
};
CustomAttributesCache t252__CustomAttributeCache_m_DynamicPixelsPerUnit = {
2,
NULL,
&t252_CustomAttributesCacheGenerator_m_DynamicPixelsPerUnit
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t252_0_0_0;
extern Il2CppType t252_1_0_0;
struct t252;
extern CustomAttributesCache t252__CustomAttributeCache;
extern CustomAttributesCache t252__CustomAttributeCache_m_UiScaleMode;
extern CustomAttributesCache t252__CustomAttributeCache_m_ReferencePixelsPerUnit;
extern CustomAttributesCache t252__CustomAttributeCache_m_ScaleFactor;
extern CustomAttributesCache t252__CustomAttributeCache_m_ReferenceResolution;
extern CustomAttributesCache t252__CustomAttributeCache_m_ScreenMatchMode;
extern CustomAttributesCache t252__CustomAttributeCache_m_MatchWidthOrHeight;
extern CustomAttributesCache t252__CustomAttributeCache_m_PhysicalUnit;
extern CustomAttributesCache t252__CustomAttributeCache_m_FallbackScreenDPI;
extern CustomAttributesCache t252__CustomAttributeCache_m_DefaultSpriteDPI;
extern CustomAttributesCache t252__CustomAttributeCache_m_DynamicPixelsPerUnit;
TypeInfo t252_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "CanvasScaler", "UnityEngine.UI", t252_MIs, t252_PIs, t252_FIs, NULL, &t55_TI, t252_TI__nestedTypes, NULL, &t252_TI, NULL, t252_VT, &t252__CustomAttributeCache, &t252_TI, &t252_0_0_0, &t252_1_0_0, NULL, NULL, NULL, t252_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t252), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 31, 10, 14, 0, 3, 21, 0, 0};
#include "t253.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t253_TI;
#include "t253MD.h"



// Metadata Definition UnityEngine.UI.ContentSizeFitter/FitMode
extern Il2CppType t44_0_0_1542;
FieldInfo t253_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t253_TI, offsetof(t253, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t253_0_0_32854;
FieldInfo t253_f2_FieldInfo = 
{
	"Unconstrained", &t253_0_0_32854, &t253_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t253_0_0_32854;
FieldInfo t253_f3_FieldInfo = 
{
	"MinSize", &t253_0_0_32854, &t253_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t253_0_0_32854;
FieldInfo t253_f4_FieldInfo = 
{
	"PreferredSize", &t253_0_0_32854, &t253_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t253_FIs[] =
{
	&t253_f1_FieldInfo,
	&t253_f2_FieldInfo,
	&t253_f3_FieldInfo,
	&t253_f4_FieldInfo,
	NULL
};
static const int32_t t253_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t253_f2_DefaultValue = 
{
	&t253_f2_FieldInfo, { (char*)&t253_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t253_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t253_f3_DefaultValue = 
{
	&t253_f3_FieldInfo, { (char*)&t253_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t253_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t253_f4_DefaultValue = 
{
	&t253_f4_FieldInfo, { (char*)&t253_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t253_FDVs[] = 
{
	&t253_f2_DefaultValue,
	&t253_f3_DefaultValue,
	&t253_f4_DefaultValue,
	NULL
};
static MethodInfo* t253_MIs[] =
{
	NULL
};
static MethodInfo* t253_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t253_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t253_0_0_0;
extern Il2CppType t253_1_0_0;
extern TypeInfo t254_TI;
TypeInfo t253_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "FitMode", "", t253_MIs, NULL, t253_FIs, NULL, &t49_TI, NULL, &t254_TI, &t44_TI, NULL, t253_VT, &EmptyCustomAttributesCache, &t44_TI, &t253_0_0_0, &t253_1_0_0, t253_IOs, NULL, NULL, t253_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t253)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 258, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#include "t254.h"
#ifndef _MSC_VER
#else
#endif
#include "t254MD.h"

#include "t269MD.h"
extern MethodInfo m1953_MI;
extern MethodInfo m1080_MI;
extern MethodInfo m1073_MI;
extern MethodInfo m1069_MI;
extern MethodInfo m1071_MI;
extern MethodInfo m1186_MI;
extern MethodInfo m1187_MI;
extern MethodInfo m1077_MI;
struct t230;
 bool m1953 (t29 * __this, int32_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m1068_MI;
 void m1068 (t254 * __this, MethodInfo* method){
	{
		m168(__this, &m168_MI);
		return;
	}
}
 int32_t m1069 (t254 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m1070_MI;
 void m1070 (t254 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t* L_0 = &(__this->f2);
		bool L_1 = m1953(NULL, L_0, p0, &m1953_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m1080(__this, &m1080_MI);
	}

IL_0017:
	{
		return;
	}
}
 int32_t m1071 (t254 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m1072_MI;
 void m1072 (t254 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t* L_0 = &(__this->f3);
		bool L_1 = m1953(NULL, L_0, p0, &m1953_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m1080(__this, &m1080_MI);
	}

IL_0017:
	{
		return;
	}
}
 t2 * m1073 (t254 * __this, MethodInfo* method){
	{
		t2 * L_0 = (__this->f4);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		t2 * L_2 = m60(__this, &m60_MI);
		__this->f4 = L_2;
	}

IL_001d:
	{
		t2 * L_3 = (__this->f4);
		return L_3;
	}
}
extern MethodInfo m1074_MI;
 void m1074 (t254 * __this, MethodInfo* method){
	{
		m170(__this, &m170_MI);
		m1080(__this, &m1080_MI);
		return;
	}
}
extern MethodInfo m1075_MI;
 void m1075 (t254 * __this, MethodInfo* method){
	{
		t218 * L_0 = &(__this->f5);
		m1831(L_0, &m1831_MI);
		t2 * L_1 = m1073(__this, &m1073_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1174(NULL, L_1, &m1174_MI);
		m172(__this, &m172_MI);
		return;
	}
}
extern MethodInfo m1076_MI;
 void m1076 (t254 * __this, MethodInfo* method){
	{
		m1080(__this, &m1080_MI);
		return;
	}
}
 void m1077 (t254 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	int32_t G_B3_0 = {0};
	t2 * G_B7_0 = {0};
	t254 * G_B7_1 = {0};
	t218 * G_B7_2 = {0};
	t2 * G_B6_0 = {0};
	t254 * G_B6_1 = {0};
	t218 * G_B6_2 = {0};
	int32_t G_B8_0 = 0;
	t2 * G_B8_1 = {0};
	t254 * G_B8_2 = {0};
	t218 * G_B8_3 = {0};
	{
		if (p0)
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_0 = m1069(__this, &m1069_MI);
		G_B3_0 = L_0;
		goto IL_0017;
	}

IL_0011:
	{
		int32_t L_1 = m1071(__this, &m1071_MI);
		G_B3_0 = L_1;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		if (V_0)
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		t218 * L_2 = &(__this->f5);
		t2 * L_3 = m1073(__this, &m1073_MI);
		G_B6_0 = L_3;
		G_B6_1 = __this;
		G_B6_2 = L_2;
		if (p0)
		{
			G_B7_0 = L_3;
			G_B7_1 = __this;
			G_B7_2 = L_2;
			goto IL_003c;
		}
	}
	{
		G_B8_0 = ((int32_t)4096);
		G_B8_1 = G_B6_0;
		G_B8_2 = G_B6_1;
		G_B8_3 = G_B6_2;
		goto IL_0041;
	}

IL_003c:
	{
		G_B8_0 = ((int32_t)8192);
		G_B8_1 = G_B7_0;
		G_B8_2 = G_B7_1;
		G_B8_3 = G_B7_2;
	}

IL_0041:
	{
		m1833(G_B8_3, G_B8_2, G_B8_1, G_B8_0, &m1833_MI);
		if ((((uint32_t)V_0) != ((uint32_t)1)))
		{
			goto IL_006a;
		}
	}
	{
		t2 * L_4 = m1073(__this, &m1073_MI);
		t2 * L_5 = (__this->f4);
		float L_6 = m1186(NULL, L_5, p0, &m1186_MI);
		m1946(L_4, p0, L_6, &m1946_MI);
		goto IL_0082;
	}

IL_006a:
	{
		t2 * L_7 = m1073(__this, &m1073_MI);
		t2 * L_8 = (__this->f4);
		float L_9 = m1187(NULL, L_8, p0, &m1187_MI);
		m1946(L_7, p0, L_9, &m1946_MI);
	}

IL_0082:
	{
		return;
	}
}
extern MethodInfo m1078_MI;
 void m1078 (t254 * __this, MethodInfo* method){
	{
		t218 * L_0 = &(__this->f5);
		m1831(L_0, &m1831_MI);
		m1077(__this, 0, &m1077_MI);
		return;
	}
}
extern MethodInfo m1079_MI;
 void m1079 (t254 * __this, MethodInfo* method){
	{
		m1077(__this, 1, &m1077_MI);
		return;
	}
}
 void m1080 (t254 * __this, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		t2 * L_1 = m1073(__this, &m1073_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1174(NULL, L_1, &m1174_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.ContentSizeFitter
extern Il2CppType t253_0_0_4;
extern CustomAttributesCache t254__CustomAttributeCache_m_HorizontalFit;
FieldInfo t254_f2_FieldInfo = 
{
	"m_HorizontalFit", &t253_0_0_4, &t254_TI, offsetof(t254, f2), &t254__CustomAttributeCache_m_HorizontalFit};
extern Il2CppType t253_0_0_4;
extern CustomAttributesCache t254__CustomAttributeCache_m_VerticalFit;
FieldInfo t254_f3_FieldInfo = 
{
	"m_VerticalFit", &t253_0_0_4, &t254_TI, offsetof(t254, f3), &t254__CustomAttributeCache_m_VerticalFit};
extern Il2CppType t2_0_0_129;
FieldInfo t254_f4_FieldInfo = 
{
	"m_Rect", &t2_0_0_129, &t254_TI, offsetof(t254, f4), &EmptyCustomAttributesCache};
extern Il2CppType t218_0_0_1;
FieldInfo t254_f5_FieldInfo = 
{
	"m_Tracker", &t218_0_0_1, &t254_TI, offsetof(t254, f5), &EmptyCustomAttributesCache};
static FieldInfo* t254_FIs[] =
{
	&t254_f2_FieldInfo,
	&t254_f3_FieldInfo,
	&t254_f4_FieldInfo,
	&t254_f5_FieldInfo,
	NULL
};
static PropertyInfo t254____horizontalFit_PropertyInfo = 
{
	&t254_TI, "horizontalFit", &m1069_MI, &m1070_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t254____verticalFit_PropertyInfo = 
{
	&t254_TI, "verticalFit", &m1071_MI, &m1072_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t254____rectTransform_PropertyInfo = 
{
	&t254_TI, "rectTransform", &m1073_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t254_PIs[] =
{
	&t254____horizontalFit_PropertyInfo,
	&t254____verticalFit_PropertyInfo,
	&t254____rectTransform_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1068_MI = 
{
	".ctor", (methodPointerType)&m1068, &t254_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1052, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t253_0_0_0;
extern void* RuntimeInvoker_t253 (MethodInfo* method, void* obj, void** args);
MethodInfo m1069_MI = 
{
	"get_horizontalFit", (methodPointerType)&m1069, &t254_TI, &t253_0_0_0, RuntimeInvoker_t253, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1053, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t253_0_0_0;
static ParameterInfo t254_m1070_ParameterInfos[] = 
{
	{"value", 0, 134218336, &EmptyCustomAttributesCache, &t253_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1070_MI = 
{
	"set_horizontalFit", (methodPointerType)&m1070, &t254_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t254_m1070_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1054, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t253_0_0_0;
extern void* RuntimeInvoker_t253 (MethodInfo* method, void* obj, void** args);
MethodInfo m1071_MI = 
{
	"get_verticalFit", (methodPointerType)&m1071, &t254_TI, &t253_0_0_0, RuntimeInvoker_t253, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1055, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t253_0_0_0;
static ParameterInfo t254_m1072_ParameterInfos[] = 
{
	{"value", 0, 134218337, &EmptyCustomAttributesCache, &t253_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1072_MI = 
{
	"set_verticalFit", (methodPointerType)&m1072, &t254_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t254_m1072_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1056, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1073_MI = 
{
	"get_rectTransform", (methodPointerType)&m1073, &t254_TI, &t2_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 1057, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1074_MI = 
{
	"OnEnable", (methodPointerType)&m1074, &t254_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 5, 0, false, false, 1058, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1075_MI = 
{
	"OnDisable", (methodPointerType)&m1075, &t254_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 7, 0, false, false, 1059, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1076_MI = 
{
	"OnRectTransformDimensionsChange", (methodPointerType)&m1076, &t254_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 10, 0, false, false, 1060, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t254_m1077_ParameterInfos[] = 
{
	{"axis", 0, 134218338, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1077_MI = 
{
	"HandleSelfFittingAlongAxis", (methodPointerType)&m1077, &t254_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t254_m1077_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 1061, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1078_MI = 
{
	"SetLayoutHorizontal", (methodPointerType)&m1078, &t254_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 17, 0, false, false, 1062, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1079_MI = 
{
	"SetLayoutVertical", (methodPointerType)&m1079, &t254_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 18, 0, false, false, 1063, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1080_MI = 
{
	"SetDirty", (methodPointerType)&m1080, &t254_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 132, 0, 255, 0, false, false, 1064, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t254_MIs[] =
{
	&m1068_MI,
	&m1069_MI,
	&m1070_MI,
	&m1071_MI,
	&m1072_MI,
	&m1073_MI,
	&m1074_MI,
	&m1075_MI,
	&m1076_MI,
	&m1077_MI,
	&m1078_MI,
	&m1079_MI,
	&m1080_MI,
	NULL
};
extern TypeInfo t253_TI;
static TypeInfo* t254_TI__nestedTypes[2] =
{
	&t253_TI,
	NULL
};
static MethodInfo* t254_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1074_MI,
	&m171_MI,
	&m1075_MI,
	&m173_MI,
	&m174_MI,
	&m1076_MI,
	&m176_MI,
	&m177_MI,
	&m178_MI,
	&m179_MI,
	&m1078_MI,
	&m1079_MI,
	&m1078_MI,
	&m1079_MI,
};
static TypeInfo* t254_ITIs[] = 
{
	&t409_TI,
	&t410_TI,
};
static Il2CppInterfaceOffsetPair t254_IOs[] = 
{
	{ &t409_TI, 15},
	{ &t410_TI, 17},
};
void t254_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t360 * tmp;
		tmp = (t360 *)il2cpp_codegen_object_new (&t360_TI);
		m1604(tmp, &m1604_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("Layout/Content Size Fitter"), 141, &m1519_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t318 * tmp;
		tmp = (t318 *)il2cpp_codegen_object_new (&t318_TI);
		m1404(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2_TI)), &m1404_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
void t254_CustomAttributesCacheGenerator_m_HorizontalFit(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t254_CustomAttributesCacheGenerator_m_VerticalFit(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t254__CustomAttributeCache = {
3,
NULL,
&t254_CustomAttributesCacheGenerator
};
CustomAttributesCache t254__CustomAttributeCache_m_HorizontalFit = {
1,
NULL,
&t254_CustomAttributesCacheGenerator_m_HorizontalFit
};
CustomAttributesCache t254__CustomAttributeCache_m_VerticalFit = {
1,
NULL,
&t254_CustomAttributesCacheGenerator_m_VerticalFit
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t254_0_0_0;
extern Il2CppType t254_1_0_0;
struct t254;
extern CustomAttributesCache t254__CustomAttributeCache;
extern CustomAttributesCache t254__CustomAttributeCache_m_HorizontalFit;
extern CustomAttributesCache t254__CustomAttributeCache_m_VerticalFit;
TypeInfo t254_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ContentSizeFitter", "UnityEngine.UI", t254_MIs, t254_PIs, t254_FIs, NULL, &t55_TI, t254_TI__nestedTypes, NULL, &t254_TI, t254_ITIs, t254_VT, &t254__CustomAttributeCache, &t254_TI, &t254_0_0_0, &t254_1_0_0, t254_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t254), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 13, 3, 4, 0, 1, 19, 2, 2};
#include "t255.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t255_TI;
#include "t255MD.h"



// Metadata Definition UnityEngine.UI.GridLayoutGroup/Corner
extern Il2CppType t44_0_0_1542;
FieldInfo t255_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t255_TI, offsetof(t255, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t255_0_0_32854;
FieldInfo t255_f2_FieldInfo = 
{
	"UpperLeft", &t255_0_0_32854, &t255_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t255_0_0_32854;
FieldInfo t255_f3_FieldInfo = 
{
	"UpperRight", &t255_0_0_32854, &t255_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t255_0_0_32854;
FieldInfo t255_f4_FieldInfo = 
{
	"LowerLeft", &t255_0_0_32854, &t255_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t255_0_0_32854;
FieldInfo t255_f5_FieldInfo = 
{
	"LowerRight", &t255_0_0_32854, &t255_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t255_FIs[] =
{
	&t255_f1_FieldInfo,
	&t255_f2_FieldInfo,
	&t255_f3_FieldInfo,
	&t255_f4_FieldInfo,
	&t255_f5_FieldInfo,
	NULL
};
static const int32_t t255_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t255_f2_DefaultValue = 
{
	&t255_f2_FieldInfo, { (char*)&t255_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t255_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t255_f3_DefaultValue = 
{
	&t255_f3_FieldInfo, { (char*)&t255_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t255_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t255_f4_DefaultValue = 
{
	&t255_f4_FieldInfo, { (char*)&t255_f4_DefaultValueData, &t44_0_0_0 }};
static const int32_t t255_f5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry t255_f5_DefaultValue = 
{
	&t255_f5_FieldInfo, { (char*)&t255_f5_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t255_FDVs[] = 
{
	&t255_f2_DefaultValue,
	&t255_f3_DefaultValue,
	&t255_f4_DefaultValue,
	&t255_f5_DefaultValue,
	NULL
};
static MethodInfo* t255_MIs[] =
{
	NULL
};
static MethodInfo* t255_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t255_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t255_0_0_0;
extern Il2CppType t255_1_0_0;
extern TypeInfo t258_TI;
TypeInfo t255_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Corner", "", t255_MIs, NULL, t255_FIs, NULL, &t49_TI, NULL, &t258_TI, &t44_TI, NULL, t255_VT, &EmptyCustomAttributesCache, &t44_TI, &t255_0_0_0, &t255_1_0_0, t255_IOs, NULL, NULL, t255_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t255)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 258, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 5, 0, 0, 23, 0, 3};
#include "t256.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t256_TI;
#include "t256MD.h"



// Metadata Definition UnityEngine.UI.GridLayoutGroup/Axis
extern Il2CppType t44_0_0_1542;
FieldInfo t256_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t256_TI, offsetof(t256, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t256_0_0_32854;
FieldInfo t256_f2_FieldInfo = 
{
	"Horizontal", &t256_0_0_32854, &t256_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t256_0_0_32854;
FieldInfo t256_f3_FieldInfo = 
{
	"Vertical", &t256_0_0_32854, &t256_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t256_FIs[] =
{
	&t256_f1_FieldInfo,
	&t256_f2_FieldInfo,
	&t256_f3_FieldInfo,
	NULL
};
static const int32_t t256_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t256_f2_DefaultValue = 
{
	&t256_f2_FieldInfo, { (char*)&t256_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t256_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t256_f3_DefaultValue = 
{
	&t256_f3_FieldInfo, { (char*)&t256_f3_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t256_FDVs[] = 
{
	&t256_f2_DefaultValue,
	&t256_f3_DefaultValue,
	NULL
};
static MethodInfo* t256_MIs[] =
{
	NULL
};
static MethodInfo* t256_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t256_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t256_0_0_0;
extern Il2CppType t256_1_0_0;
TypeInfo t256_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Axis", "", t256_MIs, NULL, t256_FIs, NULL, &t49_TI, NULL, &t258_TI, &t44_TI, NULL, t256_VT, &EmptyCustomAttributesCache, &t44_TI, &t256_0_0_0, &t256_1_0_0, t256_IOs, NULL, NULL, t256_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t256)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 258, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 3, 0, 0, 23, 0, 3};
#include "t257.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t257_TI;
#include "t257MD.h"



// Metadata Definition UnityEngine.UI.GridLayoutGroup/Constraint
extern Il2CppType t44_0_0_1542;
FieldInfo t257_f1_FieldInfo = 
{
	"value__", &t44_0_0_1542, &t257_TI, offsetof(t257, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t257_0_0_32854;
FieldInfo t257_f2_FieldInfo = 
{
	"Flexible", &t257_0_0_32854, &t257_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t257_0_0_32854;
FieldInfo t257_f3_FieldInfo = 
{
	"FixedColumnCount", &t257_0_0_32854, &t257_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t257_0_0_32854;
FieldInfo t257_f4_FieldInfo = 
{
	"FixedRowCount", &t257_0_0_32854, &t257_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
static FieldInfo* t257_FIs[] =
{
	&t257_f1_FieldInfo,
	&t257_f2_FieldInfo,
	&t257_f3_FieldInfo,
	&t257_f4_FieldInfo,
	NULL
};
static const int32_t t257_f2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry t257_f2_DefaultValue = 
{
	&t257_f2_FieldInfo, { (char*)&t257_f2_DefaultValueData, &t44_0_0_0 }};
static const int32_t t257_f3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry t257_f3_DefaultValue = 
{
	&t257_f3_FieldInfo, { (char*)&t257_f3_DefaultValueData, &t44_0_0_0 }};
static const int32_t t257_f4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry t257_f4_DefaultValue = 
{
	&t257_f4_FieldInfo, { (char*)&t257_f4_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t257_FDVs[] = 
{
	&t257_f2_DefaultValue,
	&t257_f3_DefaultValue,
	&t257_f4_DefaultValue,
	NULL
};
static MethodInfo* t257_MIs[] =
{
	NULL
};
static MethodInfo* t257_VT[] =
{
	&m1248_MI,
	&m46_MI,
	&m1249_MI,
	&m1250_MI,
	&m1251_MI,
	&m1252_MI,
	&m1253_MI,
	&m1254_MI,
	&m1255_MI,
	&m1256_MI,
	&m1257_MI,
	&m1258_MI,
	&m1259_MI,
	&m1260_MI,
	&m1261_MI,
	&m1262_MI,
	&m1263_MI,
	&m1264_MI,
	&m1265_MI,
	&m1266_MI,
	&m1267_MI,
	&m1268_MI,
	&m1269_MI,
};
static Il2CppInterfaceOffsetPair t257_IOs[] = 
{
	{ &t288_TI, 4},
	{ &t289_TI, 5},
	{ &t290_TI, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t257_0_0_0;
extern Il2CppType t257_1_0_0;
TypeInfo t257_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Constraint", "", t257_MIs, NULL, t257_FIs, NULL, &t49_TI, NULL, &t258_TI, &t44_TI, NULL, t257_VT, &EmptyCustomAttributesCache, &t44_TI, &t257_0_0_0, &t257_1_0_0, t257_IOs, NULL, NULL, t257_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t257)+ sizeof (Il2CppObject), sizeof (int32_t), sizeof(int32_t), 0, 0, -1, 258, 0, true, false, false, true, false, false, false, false, false, false, false, false, 0, 0, 4, 0, 0, 23, 0, 3};
#include "t258.h"
#ifndef _MSC_VER
#else
#endif
#include "t258MD.h"

#include "t259.h"
#include "t264.h"
#include "t263.h"
extern TypeInfo t259_TI;
extern TypeInfo t264_TI;
#include "t259MD.h"
#include "t264MD.h"
#include "t263MD.h"
extern MethodInfo m1954_MI;
extern MethodInfo m1955_MI;
extern MethodInfo m1956_MI;
extern MethodInfo m1957_MI;
extern MethodInfo m1958_MI;
extern MethodInfo m1137_MI;
extern MethodInfo m1144_MI;
extern MethodInfo m1143_MI;
extern MethodInfo m1959_MI;
extern MethodInfo m1960_MI;
extern MethodInfo m1961_MI;
extern MethodInfo m1138_MI;
extern MethodInfo m1962_MI;
extern MethodInfo m1086_MI;
extern MethodInfo m1088_MI;
extern MethodInfo m1159_MI;
extern MethodInfo m1142_MI;
extern MethodInfo m1763_MI;
extern MethodInfo m1963_MI;
extern MethodInfo m1098_MI;
extern MethodInfo m1964_MI;
extern MethodInfo m1965_MI;
extern MethodInfo m1082_MI;
extern MethodInfo m1084_MI;
extern MethodInfo m1540_MI;
extern MethodInfo m1158_MI;
extern MethodInfo m1160_MI;
struct t259;
 void m1954 (t259 * __this, int32_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t259;
 void m1955 (t259 * __this, int32_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t259;
 void m1956 (t259 * __this, t17 * p0, t17  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t259;
 void m1957 (t259 * __this, int32_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t259;
 void m1958 (t259 * __this, int32_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m1081_MI;
 void m1081 (t258 * __this, MethodInfo* method){
	{
		t17  L_0 = {0};
		m62(&L_0, (100.0f), (100.0f), &m62_MI);
		__this->f12 = L_0;
		t17  L_1 = m1394(NULL, &m1394_MI);
		__this->f13 = L_1;
		__this->f15 = 2;
		m1137(__this, &m1137_MI);
		return;
	}
}
 int32_t m1082 (t258 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f10);
		return L_0;
	}
}
extern MethodInfo m1083_MI;
 void m1083 (t258 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t* L_0 = &(__this->f10);
		m1954(__this, L_0, p0, &m1954_MI);
		return;
	}
}
 int32_t m1084 (t258 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f11);
		return L_0;
	}
}
extern MethodInfo m1085_MI;
 void m1085 (t258 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t* L_0 = &(__this->f11);
		m1955(__this, L_0, p0, &m1955_MI);
		return;
	}
}
 t17  m1086 (t258 * __this, MethodInfo* method){
	{
		t17  L_0 = (__this->f12);
		return L_0;
	}
}
extern MethodInfo m1087_MI;
 void m1087 (t258 * __this, t17  p0, MethodInfo* method){
	{
		t17 * L_0 = &(__this->f12);
		m1956(__this, L_0, p0, &m1956_MI);
		return;
	}
}
 t17  m1088 (t258 * __this, MethodInfo* method){
	{
		t17  L_0 = (__this->f13);
		return L_0;
	}
}
extern MethodInfo m1089_MI;
 void m1089 (t258 * __this, t17  p0, MethodInfo* method){
	{
		t17 * L_0 = &(__this->f13);
		m1956(__this, L_0, p0, &m1956_MI);
		return;
	}
}
extern MethodInfo m1090_MI;
 int32_t m1090 (t258 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f14);
		return L_0;
	}
}
extern MethodInfo m1091_MI;
 void m1091 (t258 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t* L_0 = &(__this->f14);
		m1957(__this, L_0, p0, &m1957_MI);
		return;
	}
}
extern MethodInfo m1092_MI;
 int32_t m1092 (t258 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f15);
		return L_0;
	}
}
extern MethodInfo m1093_MI;
 void m1093 (t258 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t* L_0 = &(__this->f15);
		m1958(__this, L_0, p0, &m1958_MI);
		return;
	}
}
extern MethodInfo m1094_MI;
 void m1094 (t258 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t17  V_2 = {0};
	t17  V_3 = {0};
	t17  V_4 = {0};
	t17  V_5 = {0};
	t17  V_6 = {0};
	t17  V_7 = {0};
	{
		m1144(__this, &m1144_MI);
		V_0 = 0;
		V_1 = 0;
		int32_t L_0 = (__this->f14);
		if ((((uint32_t)L_0) != ((uint32_t)1)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_1 = (__this->f15);
		int32_t L_2 = L_1;
		V_1 = L_2;
		V_0 = L_2;
		goto IL_0070;
	}

IL_0024:
	{
		int32_t L_3 = (__this->f14);
		if ((((uint32_t)L_3) != ((uint32_t)2)))
		{
			goto IL_0057;
		}
	}
	{
		t264 * L_4 = m1143(__this, &m1143_MI);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_4);
		int32_t L_6 = (__this->f15);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		int32_t L_7 = m1960(NULL, ((float)(((float)((float)(((float)L_5))/(float)(((float)L_6))))-(0.001f))), &m1960_MI);
		int32_t L_8 = L_7;
		V_1 = L_8;
		V_0 = L_8;
		goto IL_0070;
	}

IL_0057:
	{
		V_0 = 1;
		t264 * L_9 = m1143(__this, &m1143_MI);
		int32_t L_10 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_11 = sqrtf((((float)L_10)));
		int32_t L_12 = m1960(NULL, L_11, &m1960_MI);
		V_1 = L_12;
	}

IL_0070:
	{
		t263 * L_13 = m1138(__this, &m1138_MI);
		int32_t L_14 = m1962(L_13, &m1962_MI);
		t17  L_15 = m1086(__this, &m1086_MI);
		V_2 = L_15;
		float L_16 = ((&V_2)->f1);
		t17  L_17 = m1088(__this, &m1088_MI);
		V_3 = L_17;
		float L_18 = ((&V_3)->f1);
		t17  L_19 = m1088(__this, &m1088_MI);
		V_4 = L_19;
		float L_20 = ((&V_4)->f1);
		t263 * L_21 = m1138(__this, &m1138_MI);
		int32_t L_22 = m1962(L_21, &m1962_MI);
		t17  L_23 = m1086(__this, &m1086_MI);
		V_5 = L_23;
		float L_24 = ((&V_5)->f1);
		t17  L_25 = m1088(__this, &m1088_MI);
		V_6 = L_25;
		float L_26 = ((&V_6)->f1);
		t17  L_27 = m1088(__this, &m1088_MI);
		V_7 = L_27;
		float L_28 = ((&V_7)->f1);
		m1159(__this, ((float)(((float)((((float)L_14))+((float)((float)((float)(L_16+L_18))*(float)(((float)V_0))))))-L_20)), ((float)(((float)((((float)L_22))+((float)((float)((float)(L_24+L_26))*(float)(((float)V_1))))))-L_28)), (-1.0f), 0, &m1159_MI);
		return;
	}
}
extern MethodInfo m1095_MI;
 void m1095 (t258 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	t164  V_4 = {0};
	t17  V_5 = {0};
	t17  V_6 = {0};
	t17  V_7 = {0};
	t17  V_8 = {0};
	t17  V_9 = {0};
	t17  V_10 = {0};
	t17  V_11 = {0};
	{
		V_0 = 0;
		int32_t L_0 = (__this->f14);
		if ((((uint32_t)L_0) != ((uint32_t)1)))
		{
			goto IL_0033;
		}
	}
	{
		t264 * L_1 = m1143(__this, &m1143_MI);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_1);
		int32_t L_3 = (__this->f15);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		int32_t L_4 = m1960(NULL, ((float)(((float)((float)(((float)L_2))/(float)(((float)L_3))))-(0.001f))), &m1960_MI);
		V_0 = L_4;
		goto IL_00ce;
	}

IL_0033:
	{
		int32_t L_5 = (__this->f14);
		if ((((uint32_t)L_5) != ((uint32_t)2)))
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_6 = (__this->f15);
		V_0 = L_6;
		goto IL_00ce;
	}

IL_004b:
	{
		t2 * L_7 = m1142(__this, &m1142_MI);
		t164  L_8 = m1572(L_7, &m1572_MI);
		V_4 = L_8;
		t17  L_9 = m1653((&V_4), &m1653_MI);
		V_5 = L_9;
		float L_10 = ((&V_5)->f1);
		V_1 = L_10;
		t263 * L_11 = m1138(__this, &m1138_MI);
		int32_t L_12 = m1962(L_11, &m1962_MI);
		t17  L_13 = m1088(__this, &m1088_MI);
		V_6 = L_13;
		float L_14 = ((&V_6)->f1);
		t17  L_15 = m1086(__this, &m1086_MI);
		V_7 = L_15;
		float L_16 = ((&V_7)->f1);
		t17  L_17 = m1088(__this, &m1088_MI);
		V_8 = L_17;
		float L_18 = ((&V_8)->f1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		int32_t L_19 = m1922(NULL, ((float)((float)((float)(((float)(((float)(V_1-(((float)L_12))))+L_14))+(0.001f)))/(float)((float)(L_16+L_18)))), &m1922_MI);
		int32_t L_20 = m1763(NULL, 1, L_19, &m1763_MI);
		V_2 = L_20;
		t264 * L_21 = m1143(__this, &m1143_MI);
		int32_t L_22 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_21);
		int32_t L_23 = m1960(NULL, ((float)((float)(((float)L_22))/(float)(((float)V_2)))), &m1960_MI);
		V_0 = L_23;
	}

IL_00ce:
	{
		t263 * L_24 = m1138(__this, &m1138_MI);
		int32_t L_25 = m1963(L_24, &m1963_MI);
		t17  L_26 = m1086(__this, &m1086_MI);
		V_9 = L_26;
		float L_27 = ((&V_9)->f2);
		t17  L_28 = m1088(__this, &m1088_MI);
		V_10 = L_28;
		float L_29 = ((&V_10)->f2);
		t17  L_30 = m1088(__this, &m1088_MI);
		V_11 = L_30;
		float L_31 = ((&V_11)->f2);
		V_3 = ((float)(((float)((((float)L_25))+((float)((float)((float)(L_27+L_29))*(float)(((float)V_0))))))-L_31));
		m1159(__this, V_3, V_3, (-1.0f), 1, &m1159_MI);
		return;
	}
}
extern MethodInfo m1096_MI;
 void m1096 (t258 * __this, MethodInfo* method){
	{
		m1098(__this, 0, &m1098_MI);
		return;
	}
}
extern MethodInfo m1097_MI;
 void m1097 (t258 * __this, MethodInfo* method){
	{
		m1098(__this, 1, &m1098_MI);
		return;
	}
}
 void m1098 (t258 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	t2 * V_1 = {0};
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	t17  V_11 = {0};
	t17  V_12 = {0};
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	t164  V_16 = {0};
	t17  V_17 = {0};
	t164  V_18 = {0};
	t17  V_19 = {0};
	t17  V_20 = {0};
	t17  V_21 = {0};
	t17  V_22 = {0};
	t17  V_23 = {0};
	t17  V_24 = {0};
	t17  V_25 = {0};
	t17  V_26 = {0};
	t17  V_27 = {0};
	t17  V_28 = {0};
	t17  V_29 = {0};
	t17  V_30 = {0};
	t17  V_31 = {0};
	t17  V_32 = {0};
	t17  V_33 = {0};
	t17  V_34 = {0};
	t17  V_35 = {0};
	{
		if (p0)
		{
			goto IL_0064;
		}
	}
	{
		V_0 = 0;
		goto IL_0052;
	}

IL_000d:
	{
		t264 * L_0 = m1143(__this, &m1143_MI);
		t2 * L_1 = (t2 *)VirtFuncInvoker1< t2 *, int32_t >::Invoke(&m1964_MI, L_0, V_0);
		V_1 = L_1;
		t218 * L_2 = &(__this->f5);
		m1833(L_2, __this, V_1, ((int32_t)16134), &m1833_MI);
		t17  L_3 = m1965(NULL, &m1965_MI);
		m1796(V_1, L_3, &m1796_MI);
		t17  L_4 = m1965(NULL, &m1965_MI);
		m1663(V_1, L_4, &m1663_MI);
		t17  L_5 = m1086(__this, &m1086_MI);
		m63(V_1, L_5, &m63_MI);
		V_0 = ((int32_t)(V_0+1));
	}

IL_0052:
	{
		t264 * L_6 = m1143(__this, &m1143_MI);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_6);
		if ((((int32_t)V_0) < ((int32_t)L_7)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_0064:
	{
		t2 * L_8 = m1142(__this, &m1142_MI);
		t164  L_9 = m1572(L_8, &m1572_MI);
		V_16 = L_9;
		t17  L_10 = m1653((&V_16), &m1653_MI);
		V_17 = L_10;
		float L_11 = ((&V_17)->f1);
		V_2 = L_11;
		t2 * L_12 = m1142(__this, &m1142_MI);
		t164  L_13 = m1572(L_12, &m1572_MI);
		V_18 = L_13;
		t17  L_14 = m1653((&V_18), &m1653_MI);
		V_19 = L_14;
		float L_15 = ((&V_19)->f2);
		V_3 = L_15;
		V_4 = 1;
		V_5 = 1;
		int32_t L_16 = (__this->f14);
		if ((((uint32_t)L_16) != ((uint32_t)1)))
		{
			goto IL_00dc;
		}
	}
	{
		int32_t L_17 = (__this->f15);
		V_4 = L_17;
		t264 * L_18 = m1143(__this, &m1143_MI);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_18);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		int32_t L_20 = m1960(NULL, ((float)(((float)((float)(((float)L_19))/(float)(((float)V_4))))-(0.001f))), &m1960_MI);
		V_5 = L_20;
		goto IL_01b4;
	}

IL_00dc:
	{
		int32_t L_21 = (__this->f14);
		if ((((uint32_t)L_21) != ((uint32_t)2)))
		{
			goto IL_0112;
		}
	}
	{
		int32_t L_22 = (__this->f15);
		V_5 = L_22;
		t264 * L_23 = m1143(__this, &m1143_MI);
		int32_t L_24 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_23);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		int32_t L_25 = m1960(NULL, ((float)(((float)((float)(((float)L_24))/(float)(((float)V_5))))-(0.001f))), &m1960_MI);
		V_4 = L_25;
		goto IL_01b4;
	}

IL_0112:
	{
		t263 * L_26 = m1138(__this, &m1138_MI);
		int32_t L_27 = m1962(L_26, &m1962_MI);
		t17  L_28 = m1088(__this, &m1088_MI);
		V_20 = L_28;
		float L_29 = ((&V_20)->f1);
		t17  L_30 = m1086(__this, &m1086_MI);
		V_21 = L_30;
		float L_31 = ((&V_21)->f1);
		t17  L_32 = m1088(__this, &m1088_MI);
		V_22 = L_32;
		float L_33 = ((&V_22)->f1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		int32_t L_34 = m1922(NULL, ((float)((float)((float)(((float)(((float)(V_2-(((float)L_27))))+L_29))+(0.001f)))/(float)((float)(L_31+L_33)))), &m1922_MI);
		int32_t L_35 = m1763(NULL, 1, L_34, &m1763_MI);
		V_4 = L_35;
		t263 * L_36 = m1138(__this, &m1138_MI);
		int32_t L_37 = m1963(L_36, &m1963_MI);
		t17  L_38 = m1088(__this, &m1088_MI);
		V_23 = L_38;
		float L_39 = ((&V_23)->f2);
		t17  L_40 = m1086(__this, &m1086_MI);
		V_24 = L_40;
		float L_41 = ((&V_24)->f2);
		t17  L_42 = m1088(__this, &m1088_MI);
		V_25 = L_42;
		float L_43 = ((&V_25)->f2);
		int32_t L_44 = m1922(NULL, ((float)((float)((float)(((float)(((float)(V_3-(((float)L_37))))+L_39))+(0.001f)))/(float)((float)(L_41+L_43)))), &m1922_MI);
		int32_t L_45 = m1763(NULL, 1, L_44, &m1763_MI);
		V_5 = L_45;
	}

IL_01b4:
	{
		int32_t L_46 = m1082(__this, &m1082_MI);
		V_6 = ((int32_t)(L_46%2));
		int32_t L_47 = m1082(__this, &m1082_MI);
		V_7 = ((int32_t)((int32_t)L_47/(int32_t)2));
		int32_t L_48 = m1084(__this, &m1084_MI);
		if (L_48)
		{
			goto IL_0210;
		}
	}
	{
		V_8 = V_4;
		t264 * L_49 = m1143(__this, &m1143_MI);
		int32_t L_50 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_49);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		int32_t L_51 = m1540(NULL, V_4, 1, L_50, &m1540_MI);
		V_9 = L_51;
		t264 * L_52 = m1143(__this, &m1143_MI);
		int32_t L_53 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_52);
		int32_t L_54 = m1960(NULL, ((float)((float)(((float)L_53))/(float)(((float)V_8)))), &m1960_MI);
		int32_t L_55 = m1540(NULL, V_5, 1, L_54, &m1540_MI);
		V_10 = L_55;
		goto IL_0248;
	}

IL_0210:
	{
		V_8 = V_5;
		t264 * L_56 = m1143(__this, &m1143_MI);
		int32_t L_57 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_56);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		int32_t L_58 = m1540(NULL, V_5, 1, L_57, &m1540_MI);
		V_10 = L_58;
		t264 * L_59 = m1143(__this, &m1143_MI);
		int32_t L_60 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_59);
		int32_t L_61 = m1960(NULL, ((float)((float)(((float)L_60))/(float)(((float)V_8)))), &m1960_MI);
		int32_t L_62 = m1540(NULL, V_4, 1, L_61, &m1540_MI);
		V_9 = L_62;
	}

IL_0248:
	{
		t17  L_63 = m1086(__this, &m1086_MI);
		V_26 = L_63;
		float L_64 = ((&V_26)->f1);
		t17  L_65 = m1088(__this, &m1088_MI);
		V_27 = L_65;
		float L_66 = ((&V_27)->f1);
		t17  L_67 = m1086(__this, &m1086_MI);
		V_28 = L_67;
		float L_68 = ((&V_28)->f2);
		t17  L_69 = m1088(__this, &m1088_MI);
		V_29 = L_69;
		float L_70 = ((&V_29)->f2);
		m62((&V_11), ((float)(((float)((float)(((float)V_9))*(float)L_64))+((float)((float)(((float)((int32_t)(V_9-1))))*(float)L_66)))), ((float)(((float)((float)(((float)V_10))*(float)L_68))+((float)((float)(((float)((int32_t)(V_10-1))))*(float)L_70)))), &m62_MI);
		float L_71 = ((&V_11)->f1);
		float L_72 = m1158(__this, 0, L_71, &m1158_MI);
		float L_73 = ((&V_11)->f2);
		float L_74 = m1158(__this, 1, L_73, &m1158_MI);
		m62((&V_12), L_72, L_74, &m62_MI);
		V_13 = 0;
		goto IL_03c2;
	}

IL_02cc:
	{
		int32_t L_75 = m1084(__this, &m1084_MI);
		if (L_75)
		{
			goto IL_02ea;
		}
	}
	{
		V_14 = ((int32_t)(V_13%V_8));
		V_15 = ((int32_t)((int32_t)V_13/(int32_t)V_8));
		goto IL_02f8;
	}

IL_02ea:
	{
		V_14 = ((int32_t)((int32_t)V_13/(int32_t)V_8));
		V_15 = ((int32_t)(V_13%V_8));
	}

IL_02f8:
	{
		if ((((uint32_t)V_6) != ((uint32_t)1)))
		{
			goto IL_0309;
		}
	}
	{
		V_14 = ((int32_t)(((int32_t)(V_9-1))-V_14));
	}

IL_0309:
	{
		if ((((uint32_t)V_7) != ((uint32_t)1)))
		{
			goto IL_031a;
		}
	}
	{
		V_15 = ((int32_t)(((int32_t)(V_10-1))-V_15));
	}

IL_031a:
	{
		t264 * L_76 = m1143(__this, &m1143_MI);
		t2 * L_77 = (t2 *)VirtFuncInvoker1< t2 *, int32_t >::Invoke(&m1964_MI, L_76, V_13);
		float L_78 = ((&V_12)->f1);
		t17  L_79 = m1086(__this, &m1086_MI);
		V_30 = L_79;
		float L_80 = m1672((&V_30), 0, &m1672_MI);
		t17  L_81 = m1088(__this, &m1088_MI);
		V_31 = L_81;
		float L_82 = m1672((&V_31), 0, &m1672_MI);
		t17  L_83 = m1086(__this, &m1086_MI);
		V_32 = L_83;
		float L_84 = m1672((&V_32), 0, &m1672_MI);
		m1160(__this, L_77, 0, ((float)(L_78+((float)((float)((float)(L_80+L_82))*(float)(((float)V_14)))))), L_84, &m1160_MI);
		t264 * L_85 = m1143(__this, &m1143_MI);
		t2 * L_86 = (t2 *)VirtFuncInvoker1< t2 *, int32_t >::Invoke(&m1964_MI, L_85, V_13);
		float L_87 = ((&V_12)->f2);
		t17  L_88 = m1086(__this, &m1086_MI);
		V_33 = L_88;
		float L_89 = m1672((&V_33), 1, &m1672_MI);
		t17  L_90 = m1088(__this, &m1088_MI);
		V_34 = L_90;
		float L_91 = m1672((&V_34), 1, &m1672_MI);
		t17  L_92 = m1086(__this, &m1086_MI);
		V_35 = L_92;
		float L_93 = m1672((&V_35), 1, &m1672_MI);
		m1160(__this, L_86, 1, ((float)(L_87+((float)((float)((float)(L_89+L_91))*(float)(((float)V_15)))))), L_93, &m1160_MI);
		V_13 = ((int32_t)(V_13+1));
	}

IL_03c2:
	{
		t264 * L_94 = m1143(__this, &m1143_MI);
		int32_t L_95 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_94);
		if ((((int32_t)V_13) < ((int32_t)L_95)))
		{
			goto IL_02cc;
		}
	}
	{
		return;
	}
}
// Metadata Definition UnityEngine.UI.GridLayoutGroup
extern Il2CppType t255_0_0_4;
extern CustomAttributesCache t258__CustomAttributeCache_m_StartCorner;
FieldInfo t258_f10_FieldInfo = 
{
	"m_StartCorner", &t255_0_0_4, &t258_TI, offsetof(t258, f10), &t258__CustomAttributeCache_m_StartCorner};
extern Il2CppType t256_0_0_4;
extern CustomAttributesCache t258__CustomAttributeCache_m_StartAxis;
FieldInfo t258_f11_FieldInfo = 
{
	"m_StartAxis", &t256_0_0_4, &t258_TI, offsetof(t258, f11), &t258__CustomAttributeCache_m_StartAxis};
extern Il2CppType t17_0_0_4;
extern CustomAttributesCache t258__CustomAttributeCache_m_CellSize;
FieldInfo t258_f12_FieldInfo = 
{
	"m_CellSize", &t17_0_0_4, &t258_TI, offsetof(t258, f12), &t258__CustomAttributeCache_m_CellSize};
extern Il2CppType t17_0_0_4;
extern CustomAttributesCache t258__CustomAttributeCache_m_Spacing;
FieldInfo t258_f13_FieldInfo = 
{
	"m_Spacing", &t17_0_0_4, &t258_TI, offsetof(t258, f13), &t258__CustomAttributeCache_m_Spacing};
extern Il2CppType t257_0_0_4;
extern CustomAttributesCache t258__CustomAttributeCache_m_Constraint;
FieldInfo t258_f14_FieldInfo = 
{
	"m_Constraint", &t257_0_0_4, &t258_TI, offsetof(t258, f14), &t258__CustomAttributeCache_m_Constraint};
extern Il2CppType t44_0_0_4;
extern CustomAttributesCache t258__CustomAttributeCache_m_ConstraintCount;
FieldInfo t258_f15_FieldInfo = 
{
	"m_ConstraintCount", &t44_0_0_4, &t258_TI, offsetof(t258, f15), &t258__CustomAttributeCache_m_ConstraintCount};
static FieldInfo* t258_FIs[] =
{
	&t258_f10_FieldInfo,
	&t258_f11_FieldInfo,
	&t258_f12_FieldInfo,
	&t258_f13_FieldInfo,
	&t258_f14_FieldInfo,
	&t258_f15_FieldInfo,
	NULL
};
static PropertyInfo t258____startCorner_PropertyInfo = 
{
	&t258_TI, "startCorner", &m1082_MI, &m1083_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t258____startAxis_PropertyInfo = 
{
	&t258_TI, "startAxis", &m1084_MI, &m1085_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t258____cellSize_PropertyInfo = 
{
	&t258_TI, "cellSize", &m1086_MI, &m1087_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t258____spacing_PropertyInfo = 
{
	&t258_TI, "spacing", &m1088_MI, &m1089_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t258____constraint_PropertyInfo = 
{
	&t258_TI, "constraint", &m1090_MI, &m1091_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t258____constraintCount_PropertyInfo = 
{
	&t258_TI, "constraintCount", &m1092_MI, &m1093_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t258_PIs[] =
{
	&t258____startCorner_PropertyInfo,
	&t258____startAxis_PropertyInfo,
	&t258____cellSize_PropertyInfo,
	&t258____spacing_PropertyInfo,
	&t258____constraint_PropertyInfo,
	&t258____constraintCount_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1081_MI = 
{
	".ctor", (methodPointerType)&m1081, &t258_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1065, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t255_0_0_0;
extern void* RuntimeInvoker_t255 (MethodInfo* method, void* obj, void** args);
MethodInfo m1082_MI = 
{
	"get_startCorner", (methodPointerType)&m1082, &t258_TI, &t255_0_0_0, RuntimeInvoker_t255, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1066, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t255_0_0_0;
static ParameterInfo t258_m1083_ParameterInfos[] = 
{
	{"value", 0, 134218339, &EmptyCustomAttributesCache, &t255_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1083_MI = 
{
	"set_startCorner", (methodPointerType)&m1083, &t258_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t258_m1083_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1067, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t256_0_0_0;
extern void* RuntimeInvoker_t256 (MethodInfo* method, void* obj, void** args);
MethodInfo m1084_MI = 
{
	"get_startAxis", (methodPointerType)&m1084, &t258_TI, &t256_0_0_0, RuntimeInvoker_t256, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1068, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t256_0_0_0;
static ParameterInfo t258_m1085_ParameterInfos[] = 
{
	{"value", 0, 134218340, &EmptyCustomAttributesCache, &t256_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1085_MI = 
{
	"set_startAxis", (methodPointerType)&m1085, &t258_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t258_m1085_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1069, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m1086_MI = 
{
	"get_cellSize", (methodPointerType)&m1086, &t258_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1070, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
static ParameterInfo t258_m1087_ParameterInfos[] = 
{
	{"value", 0, 134218341, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m1087_MI = 
{
	"set_cellSize", (methodPointerType)&m1087, &t258_TI, &t21_0_0_0, RuntimeInvoker_t21_t17, t258_m1087_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1071, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m1088_MI = 
{
	"get_spacing", (methodPointerType)&m1088, &t258_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1072, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
static ParameterInfo t258_m1089_ParameterInfos[] = 
{
	{"value", 0, 134218342, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m1089_MI = 
{
	"set_spacing", (methodPointerType)&m1089, &t258_TI, &t21_0_0_0, RuntimeInvoker_t21_t17, t258_m1089_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1073, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t257_0_0_0;
extern void* RuntimeInvoker_t257 (MethodInfo* method, void* obj, void** args);
MethodInfo m1090_MI = 
{
	"get_constraint", (methodPointerType)&m1090, &t258_TI, &t257_0_0_0, RuntimeInvoker_t257, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1074, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t257_0_0_0;
static ParameterInfo t258_m1091_ParameterInfos[] = 
{
	{"value", 0, 134218343, &EmptyCustomAttributesCache, &t257_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1091_MI = 
{
	"set_constraint", (methodPointerType)&m1091, &t258_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t258_m1091_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1075, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1092_MI = 
{
	"get_constraintCount", (methodPointerType)&m1092, &t258_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1076, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t258_m1093_ParameterInfos[] = 
{
	{"value", 0, 134218344, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1093_MI = 
{
	"set_constraintCount", (methodPointerType)&m1093, &t258_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t258_m1093_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1077, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1094_MI = 
{
	"CalculateLayoutInputHorizontal", (methodPointerType)&m1094, &t258_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 26, 0, false, false, 1078, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1095_MI = 
{
	"CalculateLayoutInputVertical", (methodPointerType)&m1095, &t258_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 27, 0, false, false, 1079, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1096_MI = 
{
	"SetLayoutHorizontal", (methodPointerType)&m1096, &t258_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 35, 0, false, false, 1080, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1097_MI = 
{
	"SetLayoutVertical", (methodPointerType)&m1097, &t258_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 36, 0, false, false, 1081, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t258_m1098_ParameterInfos[] = 
{
	{"axis", 0, 134218345, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1098_MI = 
{
	"SetCellsAlongAxis", (methodPointerType)&m1098, &t258_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t258_m1098_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, false, 1082, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t258_MIs[] =
{
	&m1081_MI,
	&m1082_MI,
	&m1083_MI,
	&m1084_MI,
	&m1085_MI,
	&m1086_MI,
	&m1087_MI,
	&m1088_MI,
	&m1089_MI,
	&m1090_MI,
	&m1091_MI,
	&m1092_MI,
	&m1093_MI,
	&m1094_MI,
	&m1095_MI,
	&m1096_MI,
	&m1097_MI,
	&m1098_MI,
	NULL
};
extern TypeInfo t255_TI;
extern TypeInfo t256_TI;
extern TypeInfo t257_TI;
static TypeInfo* t258_TI__nestedTypes[4] =
{
	&t255_TI,
	&t256_TI,
	&t257_TI,
	NULL
};
extern MethodInfo m1152_MI;
extern MethodInfo m1153_MI;
extern MethodInfo m1162_MI;
extern MethodInfo m1154_MI;
extern MethodInfo m1145_MI;
extern MethodInfo m1146_MI;
extern MethodInfo m1147_MI;
extern MethodInfo m1148_MI;
extern MethodInfo m1149_MI;
extern MethodInfo m1150_MI;
extern MethodInfo m1151_MI;
extern MethodInfo m1163_MI;
static MethodInfo* t258_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1152_MI,
	&m171_MI,
	&m1153_MI,
	&m173_MI,
	&m174_MI,
	&m1162_MI,
	&m176_MI,
	&m177_MI,
	&m1154_MI,
	&m179_MI,
	&m1094_MI,
	&m1095_MI,
	&m1145_MI,
	&m1146_MI,
	&m1147_MI,
	&m1148_MI,
	&m1149_MI,
	&m1150_MI,
	&m1151_MI,
	&m1096_MI,
	&m1097_MI,
	&m1094_MI,
	&m1095_MI,
	&m1145_MI,
	&m1146_MI,
	&m1147_MI,
	&m1148_MI,
	&m1149_MI,
	&m1150_MI,
	&m1151_MI,
	&m1096_MI,
	&m1097_MI,
	&m1163_MI,
};
extern TypeInfo t411_TI;
static Il2CppInterfaceOffsetPair t258_IOs[] = 
{
	{ &t271_TI, 15},
	{ &t409_TI, 24},
	{ &t411_TI, 26},
};
void t258_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("Layout/Grid Layout Group"), 152, &m1519_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t258_CustomAttributesCacheGenerator_m_StartCorner(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t258_CustomAttributesCacheGenerator_m_StartAxis(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t258_CustomAttributesCacheGenerator_m_CellSize(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t258_CustomAttributesCacheGenerator_m_Spacing(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t258_CustomAttributesCacheGenerator_m_Constraint(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t258_CustomAttributesCacheGenerator_m_ConstraintCount(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t258__CustomAttributeCache = {
1,
NULL,
&t258_CustomAttributesCacheGenerator
};
CustomAttributesCache t258__CustomAttributeCache_m_StartCorner = {
1,
NULL,
&t258_CustomAttributesCacheGenerator_m_StartCorner
};
CustomAttributesCache t258__CustomAttributeCache_m_StartAxis = {
1,
NULL,
&t258_CustomAttributesCacheGenerator_m_StartAxis
};
CustomAttributesCache t258__CustomAttributeCache_m_CellSize = {
1,
NULL,
&t258_CustomAttributesCacheGenerator_m_CellSize
};
CustomAttributesCache t258__CustomAttributeCache_m_Spacing = {
1,
NULL,
&t258_CustomAttributesCacheGenerator_m_Spacing
};
CustomAttributesCache t258__CustomAttributeCache_m_Constraint = {
1,
NULL,
&t258_CustomAttributesCacheGenerator_m_Constraint
};
CustomAttributesCache t258__CustomAttributeCache_m_ConstraintCount = {
1,
NULL,
&t258_CustomAttributesCacheGenerator_m_ConstraintCount
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t258_0_0_0;
extern Il2CppType t258_1_0_0;
struct t258;
extern CustomAttributesCache t258__CustomAttributeCache;
extern CustomAttributesCache t258__CustomAttributeCache_m_StartCorner;
extern CustomAttributesCache t258__CustomAttributeCache_m_StartAxis;
extern CustomAttributesCache t258__CustomAttributeCache_m_CellSize;
extern CustomAttributesCache t258__CustomAttributeCache_m_Spacing;
extern CustomAttributesCache t258__CustomAttributeCache_m_Constraint;
extern CustomAttributesCache t258__CustomAttributeCache_m_ConstraintCount;
TypeInfo t258_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "GridLayoutGroup", "UnityEngine.UI", t258_MIs, t258_PIs, t258_FIs, NULL, &t259_TI, t258_TI__nestedTypes, NULL, &t258_TI, NULL, t258_VT, &t258__CustomAttributeCache, &t258_TI, &t258_0_0_0, &t258_1_0_0, t258_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t258), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 18, 6, 6, 0, 3, 38, 0, 3};
#include "t260.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t260_TI;
#include "t260MD.h"

#include "t261MD.h"
extern MethodInfo m1104_MI;
extern MethodInfo m1111_MI;
extern MethodInfo m1112_MI;


extern MethodInfo m1099_MI;
 void m1099 (t260 * __this, MethodInfo* method){
	{
		m1104(__this, &m1104_MI);
		return;
	}
}
extern MethodInfo m1100_MI;
 void m1100 (t260 * __this, MethodInfo* method){
	{
		m1144(__this, &m1144_MI);
		m1111(__this, 0, 0, &m1111_MI);
		return;
	}
}
extern MethodInfo m1101_MI;
 void m1101 (t260 * __this, MethodInfo* method){
	{
		m1111(__this, 1, 0, &m1111_MI);
		return;
	}
}
extern MethodInfo m1102_MI;
 void m1102 (t260 * __this, MethodInfo* method){
	{
		m1112(__this, 0, 0, &m1112_MI);
		return;
	}
}
extern MethodInfo m1103_MI;
 void m1103 (t260 * __this, MethodInfo* method){
	{
		m1112(__this, 1, 0, &m1112_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.HorizontalLayoutGroup
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1099_MI = 
{
	".ctor", (methodPointerType)&m1099, &t260_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1083, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1100_MI = 
{
	"CalculateLayoutInputHorizontal", (methodPointerType)&m1100, &t260_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 26, 0, false, false, 1084, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1101_MI = 
{
	"CalculateLayoutInputVertical", (methodPointerType)&m1101, &t260_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 27, 0, false, false, 1085, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1102_MI = 
{
	"SetLayoutHorizontal", (methodPointerType)&m1102, &t260_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 35, 0, false, false, 1086, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1103_MI = 
{
	"SetLayoutVertical", (methodPointerType)&m1103, &t260_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 36, 0, false, false, 1087, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t260_MIs[] =
{
	&m1099_MI,
	&m1100_MI,
	&m1101_MI,
	&m1102_MI,
	&m1103_MI,
	NULL
};
static MethodInfo* t260_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1152_MI,
	&m171_MI,
	&m1153_MI,
	&m173_MI,
	&m174_MI,
	&m1162_MI,
	&m176_MI,
	&m177_MI,
	&m1154_MI,
	&m179_MI,
	&m1100_MI,
	&m1101_MI,
	&m1145_MI,
	&m1146_MI,
	&m1147_MI,
	&m1148_MI,
	&m1149_MI,
	&m1150_MI,
	&m1151_MI,
	&m1102_MI,
	&m1103_MI,
	&m1100_MI,
	&m1101_MI,
	&m1145_MI,
	&m1146_MI,
	&m1147_MI,
	&m1148_MI,
	&m1149_MI,
	&m1150_MI,
	&m1151_MI,
	&m1102_MI,
	&m1103_MI,
	&m1163_MI,
};
static Il2CppInterfaceOffsetPair t260_IOs[] = 
{
	{ &t271_TI, 15},
	{ &t409_TI, 24},
	{ &t411_TI, 26},
};
void t260_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("Layout/Horizontal Layout Group"), 150, &m1519_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t260__CustomAttributeCache = {
1,
NULL,
&t260_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t260_0_0_0;
extern Il2CppType t260_1_0_0;
extern TypeInfo t261_TI;
struct t260;
extern CustomAttributesCache t260__CustomAttributeCache;
TypeInfo t260_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "HorizontalLayoutGroup", "UnityEngine.UI", t260_MIs, NULL, NULL, NULL, &t261_TI, NULL, NULL, &t260_TI, NULL, t260_VT, &t260__CustomAttributeCache, &t260_TI, &t260_0_0_0, &t260_1_0_0, t260_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t260), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 0, 0, 0, 0, 38, 0, 3};
#include "t261.h"
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m1966_MI;
extern MethodInfo m1967_MI;
extern MethodInfo m1188_MI;
extern MethodInfo m1107_MI;
extern MethodInfo m1109_MI;
extern MethodInfo m1105_MI;
extern MethodInfo m1968_MI;
extern MethodInfo m1969_MI;
extern MethodInfo m1157_MI;
extern MethodInfo m1156_MI;
extern MethodInfo m1155_MI;
struct t259;
 void m1966 (t259 * __this, float* p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t259;
 void m1967 (t259 * __this, bool* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m1104 (t261 * __this, MethodInfo* method){
	{
		__this->f11 = 1;
		__this->f12 = 1;
		m1137(__this, &m1137_MI);
		return;
	}
}
 float m1105 (t261 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f10);
		return L_0;
	}
}
extern MethodInfo m1106_MI;
 void m1106 (t261 * __this, float p0, MethodInfo* method){
	{
		float* L_0 = &(__this->f10);
		m1966(__this, L_0, p0, &m1966_MI);
		return;
	}
}
 bool m1107 (t261 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f11);
		return L_0;
	}
}
extern MethodInfo m1108_MI;
 void m1108 (t261 * __this, bool p0, MethodInfo* method){
	{
		bool* L_0 = &(__this->f11);
		m1967(__this, L_0, p0, &m1967_MI);
		return;
	}
}
 bool m1109 (t261 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f12);
		return L_0;
	}
}
extern MethodInfo m1110_MI;
 void m1110 (t261 * __this, bool p0, MethodInfo* method){
	{
		bool* L_0 = &(__this->f12);
		m1967(__this, L_0, p0, &m1967_MI);
		return;
	}
}
 void m1111 (t261 * __this, int32_t p0, bool p1, MethodInfo* method){
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	int32_t V_5 = 0;
	t2 * V_6 = {0};
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	int32_t G_B3_0 = 0;
	bool G_B7_0 = false;
	{
		if (p0)
		{
			goto IL_0016;
		}
	}
	{
		t263 * L_0 = m1138(__this, &m1138_MI);
		int32_t L_1 = m1962(L_0, &m1962_MI);
		G_B3_0 = L_1;
		goto IL_0021;
	}

IL_0016:
	{
		t263 * L_2 = m1138(__this, &m1138_MI);
		int32_t L_3 = m1963(L_2, &m1963_MI);
		G_B3_0 = L_3;
	}

IL_0021:
	{
		V_0 = (((float)G_B3_0));
		V_1 = V_0;
		V_2 = V_0;
		V_3 = (0.0f);
		V_4 = ((int32_t)((int32_t)p1^(int32_t)((((int32_t)p0) == ((int32_t)1))? 1 : 0)));
		V_5 = 0;
		goto IL_00e2;
	}

IL_003d:
	{
		t264 * L_4 = m1143(__this, &m1143_MI);
		t2 * L_5 = (t2 *)VirtFuncInvoker1< t2 *, int32_t >::Invoke(&m1964_MI, L_4, V_5);
		V_6 = L_5;
		float L_6 = m1186(NULL, V_6, p0, &m1186_MI);
		V_7 = L_6;
		float L_7 = m1187(NULL, V_6, p0, &m1187_MI);
		V_8 = L_7;
		float L_8 = m1188(NULL, V_6, p0, &m1188_MI);
		V_9 = L_8;
		if (p0)
		{
			goto IL_007b;
		}
	}
	{
		bool L_9 = m1107(__this, &m1107_MI);
		G_B7_0 = L_9;
		goto IL_0081;
	}

IL_007b:
	{
		bool L_10 = m1109(__this, &m1109_MI);
		G_B7_0 = L_10;
	}

IL_0081:
	{
		if (!G_B7_0)
		{
			goto IL_0094;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_11 = m1889(NULL, V_9, (1.0f), &m1889_MI);
		V_9 = L_11;
	}

IL_0094:
	{
		if (!V_4)
		{
			goto IL_00bf;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_12 = m1889(NULL, ((float)(V_7+V_0)), V_1, &m1889_MI);
		V_1 = L_12;
		float L_13 = m1889(NULL, ((float)(V_8+V_0)), V_2, &m1889_MI);
		V_2 = L_13;
		float L_14 = m1889(NULL, V_9, V_3, &m1889_MI);
		V_3 = L_14;
		goto IL_00dc;
	}

IL_00bf:
	{
		float L_15 = m1105(__this, &m1105_MI);
		V_1 = ((float)(V_1+((float)(V_7+L_15))));
		float L_16 = m1105(__this, &m1105_MI);
		V_2 = ((float)(V_2+((float)(V_8+L_16))));
		V_3 = ((float)(V_3+V_9));
	}

IL_00dc:
	{
		V_5 = ((int32_t)(V_5+1));
	}

IL_00e2:
	{
		t264 * L_17 = m1143(__this, &m1143_MI);
		int32_t L_18 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_17);
		if ((((int32_t)V_5) < ((int32_t)L_18)))
		{
			goto IL_003d;
		}
	}
	{
		if (V_4)
		{
			goto IL_011e;
		}
	}
	{
		t264 * L_19 = m1143(__this, &m1143_MI);
		int32_t L_20 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_19);
		if ((((int32_t)L_20) <= ((int32_t)0)))
		{
			goto IL_011e;
		}
	}
	{
		float L_21 = m1105(__this, &m1105_MI);
		V_1 = ((float)(V_1-L_21));
		float L_22 = m1105(__this, &m1105_MI);
		V_2 = ((float)(V_2-L_22));
	}

IL_011e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_23 = m1889(NULL, V_1, V_2, &m1889_MI);
		V_2 = L_23;
		m1159(__this, V_1, V_2, V_3, p0, &m1159_MI);
		return;
	}
}
 void m1112 (t261 * __this, int32_t p0, bool p1, MethodInfo* method){
	float V_0 = 0.0f;
	bool V_1 = false;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	t2 * V_4 = {0};
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	int32_t V_13 = 0;
	t2 * V_14 = {0};
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	t164  V_19 = {0};
	t17  V_20 = {0};
	float G_B3_0 = 0.0f;
	float G_B2_0 = 0.0f;
	int32_t G_B4_0 = 0;
	float G_B4_1 = 0.0f;
	bool G_B8_0 = false;
	float G_B12_0 = 0.0f;
	float G_B12_1 = 0.0f;
	float G_B11_0 = 0.0f;
	float G_B11_1 = 0.0f;
	float G_B13_0 = 0.0f;
	float G_B13_1 = 0.0f;
	float G_B13_2 = 0.0f;
	int32_t G_B19_0 = 0;
	float G_B23_0 = 0.0f;
	int32_t G_B23_1 = 0;
	t261 * G_B23_2 = {0};
	float G_B22_0 = 0.0f;
	int32_t G_B22_1 = 0;
	t261 * G_B22_2 = {0};
	int32_t G_B24_0 = 0;
	float G_B24_1 = 0.0f;
	int32_t G_B24_2 = 0;
	t261 * G_B24_3 = {0};
	bool G_B34_0 = false;
	{
		t2 * L_0 = m1142(__this, &m1142_MI);
		t164  L_1 = m1572(L_0, &m1572_MI);
		V_19 = L_1;
		t17  L_2 = m1653((&V_19), &m1653_MI);
		V_20 = L_2;
		float L_3 = m1672((&V_20), p0, &m1672_MI);
		V_0 = L_3;
		V_1 = ((int32_t)((int32_t)p1^(int32_t)((((int32_t)p0) == ((int32_t)1))? 1 : 0)));
		if (!V_1)
		{
			goto IL_00fe;
		}
	}
	{
		G_B2_0 = V_0;
		if (p0)
		{
			G_B3_0 = V_0;
			goto IL_0043;
		}
	}
	{
		t263 * L_4 = m1138(__this, &m1138_MI);
		int32_t L_5 = m1962(L_4, &m1962_MI);
		G_B4_0 = L_5;
		G_B4_1 = G_B2_0;
		goto IL_004e;
	}

IL_0043:
	{
		t263 * L_6 = m1138(__this, &m1138_MI);
		int32_t L_7 = m1963(L_6, &m1963_MI);
		G_B4_0 = L_7;
		G_B4_1 = G_B3_0;
	}

IL_004e:
	{
		V_2 = ((float)(G_B4_1-(((float)G_B4_0))));
		V_3 = 0;
		goto IL_00e8;
	}

IL_0058:
	{
		t264 * L_8 = m1143(__this, &m1143_MI);
		t2 * L_9 = (t2 *)VirtFuncInvoker1< t2 *, int32_t >::Invoke(&m1964_MI, L_8, V_3);
		V_4 = L_9;
		float L_10 = m1186(NULL, V_4, p0, &m1186_MI);
		V_5 = L_10;
		float L_11 = m1187(NULL, V_4, p0, &m1187_MI);
		V_6 = L_11;
		float L_12 = m1188(NULL, V_4, p0, &m1188_MI);
		V_7 = L_12;
		if (p0)
		{
			goto IL_0095;
		}
	}
	{
		bool L_13 = m1107(__this, &m1107_MI);
		G_B8_0 = L_13;
		goto IL_009b;
	}

IL_0095:
	{
		bool L_14 = m1109(__this, &m1109_MI);
		G_B8_0 = L_14;
	}

IL_009b:
	{
		if (!G_B8_0)
		{
			goto IL_00ae;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_15 = m1889(NULL, V_7, (1.0f), &m1889_MI);
		V_7 = L_15;
	}

IL_00ae:
	{
		G_B11_0 = V_5;
		G_B11_1 = V_2;
		if ((((float)V_7) <= ((float)(0.0f))))
		{
			G_B12_0 = V_5;
			G_B12_1 = V_2;
			goto IL_00c3;
		}
	}
	{
		G_B13_0 = V_0;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c5;
	}

IL_00c3:
	{
		G_B13_0 = V_6;
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_16 = m43(NULL, G_B13_2, G_B13_1, G_B13_0, &m43_MI);
		V_8 = L_16;
		float L_17 = m1158(__this, p0, V_8, &m1158_MI);
		V_9 = L_17;
		m1160(__this, V_4, p0, V_9, V_8, &m1160_MI);
		V_3 = ((int32_t)(V_3+1));
	}

IL_00e8:
	{
		t264 * L_18 = m1143(__this, &m1143_MI);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_18);
		if ((((int32_t)V_3) < ((int32_t)L_19)))
		{
			goto IL_0058;
		}
	}
	{
		goto IL_028e;
	}

IL_00fe:
	{
		if (p0)
		{
			goto IL_0114;
		}
	}
	{
		t263 * L_20 = m1138(__this, &m1138_MI);
		int32_t L_21 = m1968(L_20, &m1968_MI);
		G_B19_0 = L_21;
		goto IL_011f;
	}

IL_0114:
	{
		t263 * L_22 = m1138(__this, &m1138_MI);
		int32_t L_23 = m1969(L_22, &m1969_MI);
		G_B19_0 = L_23;
	}

IL_011f:
	{
		V_10 = (((float)G_B19_0));
		float L_24 = m1157(__this, p0, &m1157_MI);
		if ((((float)L_24) != ((float)(0.0f))))
		{
			goto IL_0173;
		}
	}
	{
		float L_25 = m1156(__this, p0, &m1156_MI);
		if ((((float)L_25) >= ((float)V_0)))
		{
			goto IL_0173;
		}
	}
	{
		float L_26 = m1156(__this, p0, &m1156_MI);
		G_B22_0 = L_26;
		G_B22_1 = p0;
		G_B22_2 = __this;
		if (p0)
		{
			G_B23_0 = L_26;
			G_B23_1 = p0;
			G_B23_2 = __this;
			goto IL_015f;
		}
	}
	{
		t263 * L_27 = m1138(__this, &m1138_MI);
		int32_t L_28 = m1962(L_27, &m1962_MI);
		G_B24_0 = L_28;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		goto IL_016a;
	}

IL_015f:
	{
		t263 * L_29 = m1138(__this, &m1138_MI);
		int32_t L_30 = m1963(L_29, &m1963_MI);
		G_B24_0 = L_30;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
	}

IL_016a:
	{
		float L_31 = m1158(G_B24_3, G_B24_2, ((float)(G_B24_1-(((float)G_B24_0)))), &m1158_MI);
		V_10 = L_31;
	}

IL_0173:
	{
		V_11 = (0.0f);
		float L_32 = m1155(__this, p0, &m1155_MI);
		float L_33 = m1156(__this, p0, &m1156_MI);
		if ((((float)L_32) == ((float)L_33)))
		{
			goto IL_01ad;
		}
	}
	{
		float L_34 = m1155(__this, p0, &m1155_MI);
		float L_35 = m1156(__this, p0, &m1156_MI);
		float L_36 = m1155(__this, p0, &m1155_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_37 = m1643(NULL, ((float)((float)((float)(V_0-L_34))/(float)((float)(L_35-L_36)))), &m1643_MI);
		V_11 = L_37;
	}

IL_01ad:
	{
		V_12 = (0.0f);
		float L_38 = m1156(__this, p0, &m1156_MI);
		if ((((float)V_0) <= ((float)L_38)))
		{
			goto IL_01e5;
		}
	}
	{
		float L_39 = m1157(__this, p0, &m1157_MI);
		if ((((float)L_39) <= ((float)(0.0f))))
		{
			goto IL_01e5;
		}
	}
	{
		float L_40 = m1156(__this, p0, &m1156_MI);
		float L_41 = m1157(__this, p0, &m1157_MI);
		V_12 = ((float)((float)((float)(V_0-L_40))/(float)L_41));
	}

IL_01e5:
	{
		V_13 = 0;
		goto IL_027c;
	}

IL_01ed:
	{
		t264 * L_42 = m1143(__this, &m1143_MI);
		t2 * L_43 = (t2 *)VirtFuncInvoker1< t2 *, int32_t >::Invoke(&m1964_MI, L_42, V_13);
		V_14 = L_43;
		float L_44 = m1186(NULL, V_14, p0, &m1186_MI);
		V_15 = L_44;
		float L_45 = m1187(NULL, V_14, p0, &m1187_MI);
		V_16 = L_45;
		float L_46 = m1188(NULL, V_14, p0, &m1188_MI);
		V_17 = L_46;
		if (p0)
		{
			goto IL_022b;
		}
	}
	{
		bool L_47 = m1107(__this, &m1107_MI);
		G_B34_0 = L_47;
		goto IL_0231;
	}

IL_022b:
	{
		bool L_48 = m1109(__this, &m1109_MI);
		G_B34_0 = L_48;
	}

IL_0231:
	{
		if (!G_B34_0)
		{
			goto IL_0244;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_49 = m1889(NULL, V_17, (1.0f), &m1889_MI);
		V_17 = L_49;
	}

IL_0244:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_50 = m1674(NULL, V_15, V_16, V_11, &m1674_MI);
		V_18 = L_50;
		V_18 = ((float)(V_18+((float)((float)V_17*(float)V_12))));
		m1160(__this, V_14, p0, V_10, V_18, &m1160_MI);
		float L_51 = m1105(__this, &m1105_MI);
		V_10 = ((float)(V_10+((float)(V_18+L_51))));
		V_13 = ((int32_t)(V_13+1));
	}

IL_027c:
	{
		t264 * L_52 = m1143(__this, &m1143_MI);
		int32_t L_53 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1959_MI, L_52);
		if ((((int32_t)V_13) < ((int32_t)L_53)))
		{
			goto IL_01ed;
		}
	}

IL_028e:
	{
		return;
	}
}
// Metadata Definition UnityEngine.UI.HorizontalOrVerticalLayoutGroup
extern Il2CppType t22_0_0_4;
extern CustomAttributesCache t261__CustomAttributeCache_m_Spacing;
FieldInfo t261_f10_FieldInfo = 
{
	"m_Spacing", &t22_0_0_4, &t261_TI, offsetof(t261, f10), &t261__CustomAttributeCache_m_Spacing};
extern Il2CppType t40_0_0_4;
extern CustomAttributesCache t261__CustomAttributeCache_m_ChildForceExpandWidth;
FieldInfo t261_f11_FieldInfo = 
{
	"m_ChildForceExpandWidth", &t40_0_0_4, &t261_TI, offsetof(t261, f11), &t261__CustomAttributeCache_m_ChildForceExpandWidth};
extern Il2CppType t40_0_0_4;
extern CustomAttributesCache t261__CustomAttributeCache_m_ChildForceExpandHeight;
FieldInfo t261_f12_FieldInfo = 
{
	"m_ChildForceExpandHeight", &t40_0_0_4, &t261_TI, offsetof(t261, f12), &t261__CustomAttributeCache_m_ChildForceExpandHeight};
static FieldInfo* t261_FIs[] =
{
	&t261_f10_FieldInfo,
	&t261_f11_FieldInfo,
	&t261_f12_FieldInfo,
	NULL
};
static PropertyInfo t261____spacing_PropertyInfo = 
{
	&t261_TI, "spacing", &m1105_MI, &m1106_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t261____childForceExpandWidth_PropertyInfo = 
{
	&t261_TI, "childForceExpandWidth", &m1107_MI, &m1108_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t261____childForceExpandHeight_PropertyInfo = 
{
	&t261_TI, "childForceExpandHeight", &m1109_MI, &m1110_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t261_PIs[] =
{
	&t261____spacing_PropertyInfo,
	&t261____childForceExpandWidth_PropertyInfo,
	&t261____childForceExpandHeight_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1104_MI = 
{
	".ctor", (methodPointerType)&m1104, &t261_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1088, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1105_MI = 
{
	"get_spacing", (methodPointerType)&m1105, &t261_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1089, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t261_m1106_ParameterInfos[] = 
{
	{"value", 0, 134218346, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1106_MI = 
{
	"set_spacing", (methodPointerType)&m1106, &t261_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t261_m1106_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1090, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1107_MI = 
{
	"get_childForceExpandWidth", (methodPointerType)&m1107, &t261_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1091, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t261_m1108_ParameterInfos[] = 
{
	{"value", 0, 134218347, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1108_MI = 
{
	"set_childForceExpandWidth", (methodPointerType)&m1108, &t261_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t261_m1108_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1092, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1109_MI = 
{
	"get_childForceExpandHeight", (methodPointerType)&m1109, &t261_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1093, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t261_m1110_ParameterInfos[] = 
{
	{"value", 0, 134218348, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1110_MI = 
{
	"set_childForceExpandHeight", (methodPointerType)&m1110, &t261_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t261_m1110_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1094, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t261_m1111_ParameterInfos[] = 
{
	{"axis", 0, 134218349, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"isVertical", 1, 134218350, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1111_MI = 
{
	"CalcAlongAxis", (methodPointerType)&m1111, &t261_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297, t261_m1111_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 2, false, false, 1095, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t40_0_0_0;
static ParameterInfo t261_m1112_ParameterInfos[] = 
{
	{"axis", 0, 134218351, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"isVertical", 1, 134218352, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1112_MI = 
{
	"SetChildrenAlongAxis", (methodPointerType)&m1112, &t261_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t297, t261_m1112_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 2, false, false, 1096, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t261_MIs[] =
{
	&m1104_MI,
	&m1105_MI,
	&m1106_MI,
	&m1107_MI,
	&m1108_MI,
	&m1109_MI,
	&m1110_MI,
	&m1111_MI,
	&m1112_MI,
	NULL
};
extern MethodInfo m1970_MI;
extern MethodInfo m1971_MI;
extern MethodInfo m1972_MI;
static MethodInfo* t261_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1152_MI,
	&m171_MI,
	&m1153_MI,
	&m173_MI,
	&m174_MI,
	&m1162_MI,
	&m176_MI,
	&m177_MI,
	&m1154_MI,
	&m179_MI,
	&m1144_MI,
	&m1970_MI,
	&m1145_MI,
	&m1146_MI,
	&m1147_MI,
	&m1148_MI,
	&m1149_MI,
	&m1150_MI,
	&m1151_MI,
	&m1971_MI,
	&m1972_MI,
	&m1144_MI,
	NULL,
	&m1145_MI,
	&m1146_MI,
	&m1147_MI,
	&m1148_MI,
	&m1149_MI,
	&m1150_MI,
	&m1151_MI,
	NULL,
	NULL,
	&m1163_MI,
};
static Il2CppInterfaceOffsetPair t261_IOs[] = 
{
	{ &t271_TI, 15},
	{ &t409_TI, 24},
	{ &t411_TI, 26},
};
void t261_CustomAttributesCacheGenerator_m_Spacing(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t261_CustomAttributesCacheGenerator_m_ChildForceExpandWidth(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t261_CustomAttributesCacheGenerator_m_ChildForceExpandHeight(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t261__CustomAttributeCache_m_Spacing = {
1,
NULL,
&t261_CustomAttributesCacheGenerator_m_Spacing
};
CustomAttributesCache t261__CustomAttributeCache_m_ChildForceExpandWidth = {
1,
NULL,
&t261_CustomAttributesCacheGenerator_m_ChildForceExpandWidth
};
CustomAttributesCache t261__CustomAttributeCache_m_ChildForceExpandHeight = {
1,
NULL,
&t261_CustomAttributesCacheGenerator_m_ChildForceExpandHeight
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t261_0_0_0;
extern Il2CppType t261_1_0_0;
struct t261;
extern CustomAttributesCache t261__CustomAttributeCache_m_Spacing;
extern CustomAttributesCache t261__CustomAttributeCache_m_ChildForceExpandWidth;
extern CustomAttributesCache t261__CustomAttributeCache_m_ChildForceExpandHeight;
TypeInfo t261_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "HorizontalOrVerticalLayoutGroup", "UnityEngine.UI", t261_MIs, t261_PIs, t261_FIs, NULL, &t259_TI, NULL, NULL, &t261_TI, NULL, t261_VT, &EmptyCustomAttributesCache, &t261_TI, &t261_0_0_0, &t261_1_0_0, t261_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t261), 0, -1, 0, 0, -1, 1048705, 0, false, false, false, false, false, false, false, false, false, false, false, false, 9, 3, 3, 0, 0, 38, 0, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.UI.ILayoutElement
extern MethodInfo m1973_MI;
static PropertyInfo t271____minWidth_PropertyInfo = 
{
	&t271_TI, "minWidth", &m1973_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1974_MI;
static PropertyInfo t271____preferredWidth_PropertyInfo = 
{
	&t271_TI, "preferredWidth", &m1974_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1975_MI;
static PropertyInfo t271____flexibleWidth_PropertyInfo = 
{
	&t271_TI, "flexibleWidth", &m1975_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1976_MI;
static PropertyInfo t271____minHeight_PropertyInfo = 
{
	&t271_TI, "minHeight", &m1976_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1977_MI;
static PropertyInfo t271____preferredHeight_PropertyInfo = 
{
	&t271_TI, "preferredHeight", &m1977_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1978_MI;
static PropertyInfo t271____flexibleHeight_PropertyInfo = 
{
	&t271_TI, "flexibleHeight", &m1978_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1979_MI;
static PropertyInfo t271____layoutPriority_PropertyInfo = 
{
	&t271_TI, "layoutPriority", &m1979_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t271_PIs[] =
{
	&t271____minWidth_PropertyInfo,
	&t271____preferredWidth_PropertyInfo,
	&t271____flexibleWidth_PropertyInfo,
	&t271____minHeight_PropertyInfo,
	&t271____preferredHeight_PropertyInfo,
	&t271____flexibleHeight_PropertyInfo,
	&t271____layoutPriority_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1980_MI = 
{
	"CalculateLayoutInputHorizontal", NULL, &t271_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, false, 1097, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1981_MI = 
{
	"CalculateLayoutInputVertical", NULL, &t271_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 1, 0, false, false, 1098, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1973_MI = 
{
	"get_minWidth", NULL, &t271_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 3526, 0, 2, 0, false, false, 1099, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1974_MI = 
{
	"get_preferredWidth", NULL, &t271_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 3526, 0, 3, 0, false, false, 1100, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1975_MI = 
{
	"get_flexibleWidth", NULL, &t271_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 3526, 0, 4, 0, false, false, 1101, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1976_MI = 
{
	"get_minHeight", NULL, &t271_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 3526, 0, 5, 0, false, false, 1102, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1977_MI = 
{
	"get_preferredHeight", NULL, &t271_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 3526, 0, 6, 0, false, false, 1103, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1978_MI = 
{
	"get_flexibleHeight", NULL, &t271_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 3526, 0, 7, 0, false, false, 1104, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1979_MI = 
{
	"get_layoutPriority", NULL, &t271_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 8, 0, false, false, 1105, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t271_MIs[] =
{
	&m1980_MI,
	&m1981_MI,
	&m1973_MI,
	&m1974_MI,
	&m1975_MI,
	&m1976_MI,
	&m1977_MI,
	&m1978_MI,
	&m1979_MI,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t271_0_0_0;
extern Il2CppType t271_1_0_0;
struct t271;
TypeInfo t271_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ILayoutElement", "UnityEngine.UI", t271_MIs, t271_PIs, NULL, NULL, NULL, NULL, NULL, &t271_TI, NULL, NULL, &EmptyCustomAttributesCache, &t271_TI, &t271_0_0_0, &t271_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 9, 7, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.UI.ILayoutController
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1982_MI = 
{
	"SetLayoutHorizontal", NULL, &t409_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, false, 1106, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1983_MI = 
{
	"SetLayoutVertical", NULL, &t409_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 1, 0, false, false, 1107, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t409_MIs[] =
{
	&m1982_MI,
	&m1983_MI,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t409_0_0_0;
extern Il2CppType t409_1_0_0;
struct t409;
TypeInfo t409_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ILayoutController", "UnityEngine.UI", t409_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t409_TI, NULL, NULL, &EmptyCustomAttributesCache, &t409_TI, &t409_0_0_0, &t409_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.UI.ILayoutGroup
static MethodInfo* t411_MIs[] =
{
	NULL
};
static TypeInfo* t411_ITIs[] = 
{
	&t409_TI,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t411_0_0_0;
extern Il2CppType t411_1_0_0;
struct t411;
TypeInfo t411_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ILayoutGroup", "UnityEngine.UI", t411_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t411_TI, t411_ITIs, NULL, &EmptyCustomAttributesCache, &t411_TI, &t411_0_0_0, &t411_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.UI.ILayoutSelfController
static MethodInfo* t410_MIs[] =
{
	NULL
};
static TypeInfo* t410_ITIs[] = 
{
	&t409_TI,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t410_0_0_0;
extern Il2CppType t410_1_0_0;
struct t410;
TypeInfo t410_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ILayoutSelfController", "UnityEngine.UI", t410_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t410_TI, t410_ITIs, NULL, &EmptyCustomAttributesCache, &t410_TI, &t410_0_0_0, &t410_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t412_TI;



// Metadata Definition UnityEngine.UI.ILayoutIgnorer
extern MethodInfo m1984_MI;
static PropertyInfo t412____ignoreLayout_PropertyInfo = 
{
	&t412_TI, "ignoreLayout", &m1984_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t412_PIs[] =
{
	&t412____ignoreLayout_PropertyInfo,
	NULL
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1984_MI = 
{
	"get_ignoreLayout", NULL, &t412_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, false, 1108, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t412_MIs[] =
{
	&m1984_MI,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t412_0_0_0;
extern Il2CppType t412_1_0_0;
struct t412;
TypeInfo t412_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ILayoutIgnorer", "UnityEngine.UI", t412_MIs, t412_PIs, NULL, NULL, NULL, NULL, NULL, &t412_TI, NULL, NULL, &EmptyCustomAttributesCache, &t412_TI, &t412_0_0_0, &t412_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 0, 0};
#include "t262.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t262_TI;
#include "t262MD.h"

extern MethodInfo m1136_MI;


extern MethodInfo m1113_MI;
 void m1113 (t262 * __this, MethodInfo* method){
	{
		__this->f3 = (-1.0f);
		__this->f4 = (-1.0f);
		__this->f5 = (-1.0f);
		__this->f6 = (-1.0f);
		__this->f7 = (-1.0f);
		__this->f8 = (-1.0f);
		m168(__this, &m168_MI);
		return;
	}
}
extern MethodInfo m1114_MI;
 bool m1114 (t262 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m1115_MI;
 void m1115 (t262 * __this, bool p0, MethodInfo* method){
	{
		bool* L_0 = &(__this->f2);
		bool L_1 = m1641(NULL, L_0, p0, &m1641_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m1136(__this, &m1136_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m1116_MI;
 void m1116 (t262 * __this, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m1117_MI;
 void m1117 (t262 * __this, MethodInfo* method){
	{
		return;
	}
}
extern MethodInfo m1118_MI;
 float m1118 (t262 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m1119_MI;
 void m1119 (t262 * __this, float p0, MethodInfo* method){
	{
		float* L_0 = &(__this->f3);
		bool L_1 = m1644(NULL, L_0, p0, &m1644_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m1136(__this, &m1136_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m1120_MI;
 float m1120 (t262 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m1121_MI;
 void m1121 (t262 * __this, float p0, MethodInfo* method){
	{
		float* L_0 = &(__this->f4);
		bool L_1 = m1644(NULL, L_0, p0, &m1644_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m1136(__this, &m1136_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m1122_MI;
 float m1122 (t262 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f5);
		return L_0;
	}
}
extern MethodInfo m1123_MI;
 void m1123 (t262 * __this, float p0, MethodInfo* method){
	{
		float* L_0 = &(__this->f5);
		bool L_1 = m1644(NULL, L_0, p0, &m1644_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m1136(__this, &m1136_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m1124_MI;
 float m1124 (t262 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f6);
		return L_0;
	}
}
extern MethodInfo m1125_MI;
 void m1125 (t262 * __this, float p0, MethodInfo* method){
	{
		float* L_0 = &(__this->f6);
		bool L_1 = m1644(NULL, L_0, p0, &m1644_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m1136(__this, &m1136_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m1126_MI;
 float m1126 (t262 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f7);
		return L_0;
	}
}
extern MethodInfo m1127_MI;
 void m1127 (t262 * __this, float p0, MethodInfo* method){
	{
		float* L_0 = &(__this->f7);
		bool L_1 = m1644(NULL, L_0, p0, &m1644_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m1136(__this, &m1136_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m1128_MI;
 float m1128 (t262 * __this, MethodInfo* method){
	{
		float L_0 = (__this->f8);
		return L_0;
	}
}
extern MethodInfo m1129_MI;
 void m1129 (t262 * __this, float p0, MethodInfo* method){
	{
		float* L_0 = &(__this->f8);
		bool L_1 = m1644(NULL, L_0, p0, &m1644_MI);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		m1136(__this, &m1136_MI);
	}

IL_0017:
	{
		return;
	}
}
extern MethodInfo m1130_MI;
 int32_t m1130 (t262 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m1131_MI;
 void m1131 (t262 * __this, MethodInfo* method){
	{
		m170(__this, &m170_MI);
		m1136(__this, &m1136_MI);
		return;
	}
}
extern MethodInfo m1132_MI;
 void m1132 (t262 * __this, MethodInfo* method){
	{
		m1136(__this, &m1136_MI);
		return;
	}
}
extern MethodInfo m1133_MI;
 void m1133 (t262 * __this, MethodInfo* method){
	{
		m1136(__this, &m1136_MI);
		m172(__this, &m172_MI);
		return;
	}
}
extern MethodInfo m1134_MI;
 void m1134 (t262 * __this, MethodInfo* method){
	{
		m1136(__this, &m1136_MI);
		return;
	}
}
extern MethodInfo m1135_MI;
 void m1135 (t262 * __this, MethodInfo* method){
	{
		m1136(__this, &m1136_MI);
		return;
	}
}
 void m1136 (t262 * __this, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		t25 * L_1 = m39(__this, &m39_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1174(NULL, ((t2 *)IsInst(L_1, InitializedTypeInfo(&t2_TI))), &m1174_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.LayoutElement
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t262__CustomAttributeCache_m_IgnoreLayout;
FieldInfo t262_f2_FieldInfo = 
{
	"m_IgnoreLayout", &t40_0_0_1, &t262_TI, offsetof(t262, f2), &t262__CustomAttributeCache_m_IgnoreLayout};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t262__CustomAttributeCache_m_MinWidth;
FieldInfo t262_f3_FieldInfo = 
{
	"m_MinWidth", &t22_0_0_1, &t262_TI, offsetof(t262, f3), &t262__CustomAttributeCache_m_MinWidth};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t262__CustomAttributeCache_m_MinHeight;
FieldInfo t262_f4_FieldInfo = 
{
	"m_MinHeight", &t22_0_0_1, &t262_TI, offsetof(t262, f4), &t262__CustomAttributeCache_m_MinHeight};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t262__CustomAttributeCache_m_PreferredWidth;
FieldInfo t262_f5_FieldInfo = 
{
	"m_PreferredWidth", &t22_0_0_1, &t262_TI, offsetof(t262, f5), &t262__CustomAttributeCache_m_PreferredWidth};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t262__CustomAttributeCache_m_PreferredHeight;
FieldInfo t262_f6_FieldInfo = 
{
	"m_PreferredHeight", &t22_0_0_1, &t262_TI, offsetof(t262, f6), &t262__CustomAttributeCache_m_PreferredHeight};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t262__CustomAttributeCache_m_FlexibleWidth;
FieldInfo t262_f7_FieldInfo = 
{
	"m_FlexibleWidth", &t22_0_0_1, &t262_TI, offsetof(t262, f7), &t262__CustomAttributeCache_m_FlexibleWidth};
extern Il2CppType t22_0_0_1;
extern CustomAttributesCache t262__CustomAttributeCache_m_FlexibleHeight;
FieldInfo t262_f8_FieldInfo = 
{
	"m_FlexibleHeight", &t22_0_0_1, &t262_TI, offsetof(t262, f8), &t262__CustomAttributeCache_m_FlexibleHeight};
static FieldInfo* t262_FIs[] =
{
	&t262_f2_FieldInfo,
	&t262_f3_FieldInfo,
	&t262_f4_FieldInfo,
	&t262_f5_FieldInfo,
	&t262_f6_FieldInfo,
	&t262_f7_FieldInfo,
	&t262_f8_FieldInfo,
	NULL
};
static PropertyInfo t262____ignoreLayout_PropertyInfo = 
{
	&t262_TI, "ignoreLayout", &m1114_MI, &m1115_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t262____minWidth_PropertyInfo = 
{
	&t262_TI, "minWidth", &m1118_MI, &m1119_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t262____minHeight_PropertyInfo = 
{
	&t262_TI, "minHeight", &m1120_MI, &m1121_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t262____preferredWidth_PropertyInfo = 
{
	&t262_TI, "preferredWidth", &m1122_MI, &m1123_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t262____preferredHeight_PropertyInfo = 
{
	&t262_TI, "preferredHeight", &m1124_MI, &m1125_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t262____flexibleWidth_PropertyInfo = 
{
	&t262_TI, "flexibleWidth", &m1126_MI, &m1127_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t262____flexibleHeight_PropertyInfo = 
{
	&t262_TI, "flexibleHeight", &m1128_MI, &m1129_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t262____layoutPriority_PropertyInfo = 
{
	&t262_TI, "layoutPriority", &m1130_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t262_PIs[] =
{
	&t262____ignoreLayout_PropertyInfo,
	&t262____minWidth_PropertyInfo,
	&t262____minHeight_PropertyInfo,
	&t262____preferredWidth_PropertyInfo,
	&t262____preferredHeight_PropertyInfo,
	&t262____flexibleWidth_PropertyInfo,
	&t262____flexibleHeight_PropertyInfo,
	&t262____layoutPriority_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1113_MI = 
{
	".ctor", (methodPointerType)&m1113, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1109, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1114_MI = 
{
	"get_ignoreLayout", (methodPointerType)&m1114, &t262_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2502, 0, 25, 0, false, false, 1110, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t262_m1115_ParameterInfos[] = 
{
	{"value", 0, 134218353, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1115_MI = 
{
	"set_ignoreLayout", (methodPointerType)&m1115, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t262_m1115_ParameterInfos, &EmptyCustomAttributesCache, 2502, 0, 26, 1, false, false, 1111, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1116_MI = 
{
	"CalculateLayoutInputHorizontal", (methodPointerType)&m1116, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 27, 0, false, false, 1112, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1117_MI = 
{
	"CalculateLayoutInputVertical", (methodPointerType)&m1117, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 28, 0, false, false, 1113, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1118_MI = 
{
	"get_minWidth", (methodPointerType)&m1118, &t262_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 29, 0, false, false, 1114, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t262_m1119_ParameterInfos[] = 
{
	{"value", 0, 134218354, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1119_MI = 
{
	"set_minWidth", (methodPointerType)&m1119, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t262_m1119_ParameterInfos, &EmptyCustomAttributesCache, 2502, 0, 30, 1, false, false, 1115, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1120_MI = 
{
	"get_minHeight", (methodPointerType)&m1120, &t262_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 31, 0, false, false, 1116, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t262_m1121_ParameterInfos[] = 
{
	{"value", 0, 134218355, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1121_MI = 
{
	"set_minHeight", (methodPointerType)&m1121, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t262_m1121_ParameterInfos, &EmptyCustomAttributesCache, 2502, 0, 32, 1, false, false, 1117, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1122_MI = 
{
	"get_preferredWidth", (methodPointerType)&m1122, &t262_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 33, 0, false, false, 1118, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t262_m1123_ParameterInfos[] = 
{
	{"value", 0, 134218356, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1123_MI = 
{
	"set_preferredWidth", (methodPointerType)&m1123, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t262_m1123_ParameterInfos, &EmptyCustomAttributesCache, 2502, 0, 34, 1, false, false, 1119, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1124_MI = 
{
	"get_preferredHeight", (methodPointerType)&m1124, &t262_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 35, 0, false, false, 1120, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t262_m1125_ParameterInfos[] = 
{
	{"value", 0, 134218357, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1125_MI = 
{
	"set_preferredHeight", (methodPointerType)&m1125, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t262_m1125_ParameterInfos, &EmptyCustomAttributesCache, 2502, 0, 36, 1, false, false, 1121, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1126_MI = 
{
	"get_flexibleWidth", (methodPointerType)&m1126, &t262_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 37, 0, false, false, 1122, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t262_m1127_ParameterInfos[] = 
{
	{"value", 0, 134218358, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1127_MI = 
{
	"set_flexibleWidth", (methodPointerType)&m1127, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t262_m1127_ParameterInfos, &EmptyCustomAttributesCache, 2502, 0, 38, 1, false, false, 1123, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1128_MI = 
{
	"get_flexibleHeight", (methodPointerType)&m1128, &t262_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 39, 0, false, false, 1124, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
static ParameterInfo t262_m1129_ParameterInfos[] = 
{
	{"value", 0, 134218359, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1129_MI = 
{
	"set_flexibleHeight", (methodPointerType)&m1129, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t262_m1129_ParameterInfos, &EmptyCustomAttributesCache, 2502, 0, 40, 1, false, false, 1125, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1130_MI = 
{
	"get_layoutPriority", (methodPointerType)&m1130, &t262_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2502, 0, 41, 0, false, false, 1126, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1131_MI = 
{
	"OnEnable", (methodPointerType)&m1131, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 5, 0, false, false, 1127, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1132_MI = 
{
	"OnTransformParentChanged", (methodPointerType)&m1132, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 12, 0, false, false, 1128, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1133_MI = 
{
	"OnDisable", (methodPointerType)&m1133, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 7, 0, false, false, 1129, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1134_MI = 
{
	"OnDidApplyAnimationProperties", (methodPointerType)&m1134, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 13, 0, false, false, 1130, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1135_MI = 
{
	"OnBeforeTransformParentChanged", (methodPointerType)&m1135, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 11, 0, false, false, 1131, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1136_MI = 
{
	"SetDirty", (methodPointerType)&m1136, &t262_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 132, 0, 255, 0, false, false, 1132, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t262_MIs[] =
{
	&m1113_MI,
	&m1114_MI,
	&m1115_MI,
	&m1116_MI,
	&m1117_MI,
	&m1118_MI,
	&m1119_MI,
	&m1120_MI,
	&m1121_MI,
	&m1122_MI,
	&m1123_MI,
	&m1124_MI,
	&m1125_MI,
	&m1126_MI,
	&m1127_MI,
	&m1128_MI,
	&m1129_MI,
	&m1130_MI,
	&m1131_MI,
	&m1132_MI,
	&m1133_MI,
	&m1134_MI,
	&m1135_MI,
	&m1136_MI,
	NULL
};
static MethodInfo* t262_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1131_MI,
	&m171_MI,
	&m1133_MI,
	&m173_MI,
	&m174_MI,
	&m175_MI,
	&m1135_MI,
	&m1132_MI,
	&m1134_MI,
	&m179_MI,
	&m1116_MI,
	&m1117_MI,
	&m1118_MI,
	&m1122_MI,
	&m1126_MI,
	&m1120_MI,
	&m1124_MI,
	&m1128_MI,
	&m1130_MI,
	&m1114_MI,
	&m1114_MI,
	&m1115_MI,
	&m1116_MI,
	&m1117_MI,
	&m1118_MI,
	&m1119_MI,
	&m1120_MI,
	&m1121_MI,
	&m1122_MI,
	&m1123_MI,
	&m1124_MI,
	&m1125_MI,
	&m1126_MI,
	&m1127_MI,
	&m1128_MI,
	&m1129_MI,
	&m1130_MI,
};
static TypeInfo* t262_ITIs[] = 
{
	&t271_TI,
	&t412_TI,
};
static Il2CppInterfaceOffsetPair t262_IOs[] = 
{
	{ &t271_TI, 15},
	{ &t412_TI, 24},
};
void t262_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("Layout/Layout Element"), 140, &m1519_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t360 * tmp;
		tmp = (t360 *)il2cpp_codegen_object_new (&t360_TI);
		m1604(tmp, &m1604_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t318 * tmp;
		tmp = (t318 *)il2cpp_codegen_object_new (&t318_TI);
		m1404(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2_TI)), &m1404_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
void t262_CustomAttributesCacheGenerator_m_IgnoreLayout(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t262_CustomAttributesCacheGenerator_m_MinWidth(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t262_CustomAttributesCacheGenerator_m_MinHeight(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t262_CustomAttributesCacheGenerator_m_PreferredWidth(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t262_CustomAttributesCacheGenerator_m_PreferredHeight(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t262_CustomAttributesCacheGenerator_m_FlexibleWidth(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t262_CustomAttributesCacheGenerator_m_FlexibleHeight(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t262__CustomAttributeCache = {
3,
NULL,
&t262_CustomAttributesCacheGenerator
};
CustomAttributesCache t262__CustomAttributeCache_m_IgnoreLayout = {
1,
NULL,
&t262_CustomAttributesCacheGenerator_m_IgnoreLayout
};
CustomAttributesCache t262__CustomAttributeCache_m_MinWidth = {
1,
NULL,
&t262_CustomAttributesCacheGenerator_m_MinWidth
};
CustomAttributesCache t262__CustomAttributeCache_m_MinHeight = {
1,
NULL,
&t262_CustomAttributesCacheGenerator_m_MinHeight
};
CustomAttributesCache t262__CustomAttributeCache_m_PreferredWidth = {
1,
NULL,
&t262_CustomAttributesCacheGenerator_m_PreferredWidth
};
CustomAttributesCache t262__CustomAttributeCache_m_PreferredHeight = {
1,
NULL,
&t262_CustomAttributesCacheGenerator_m_PreferredHeight
};
CustomAttributesCache t262__CustomAttributeCache_m_FlexibleWidth = {
1,
NULL,
&t262_CustomAttributesCacheGenerator_m_FlexibleWidth
};
CustomAttributesCache t262__CustomAttributeCache_m_FlexibleHeight = {
1,
NULL,
&t262_CustomAttributesCacheGenerator_m_FlexibleHeight
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t262_0_0_0;
extern Il2CppType t262_1_0_0;
struct t262;
extern CustomAttributesCache t262__CustomAttributeCache;
extern CustomAttributesCache t262__CustomAttributeCache_m_IgnoreLayout;
extern CustomAttributesCache t262__CustomAttributeCache_m_MinWidth;
extern CustomAttributesCache t262__CustomAttributeCache_m_MinHeight;
extern CustomAttributesCache t262__CustomAttributeCache_m_PreferredWidth;
extern CustomAttributesCache t262__CustomAttributeCache_m_PreferredHeight;
extern CustomAttributesCache t262__CustomAttributeCache_m_FlexibleWidth;
extern CustomAttributesCache t262__CustomAttributeCache_m_FlexibleHeight;
TypeInfo t262_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "LayoutElement", "UnityEngine.UI", t262_MIs, t262_PIs, t262_FIs, NULL, &t55_TI, NULL, NULL, &t262_TI, t262_ITIs, t262_VT, &t262__CustomAttributeCache, &t262_TI, &t262_0_0_0, &t262_1_0_0, t262_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t262), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 24, 8, 7, 0, 0, 42, 2, 2};
#ifndef _MSC_VER
#else
#endif

#include "t413.h"
extern TypeInfo t42_TI;
extern TypeInfo t263_TI;
#include "t42MD.h"
extern MethodInfo m1985_MI;
extern MethodInfo m1986_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m1987_MI;
extern MethodInfo m1988_MI;
extern MethodInfo m1989_MI;
extern MethodInfo m1990_MI;
extern MethodInfo m1991_MI;
extern MethodInfo m1992_MI;
extern MethodInfo m1993_MI;
extern MethodInfo m1164_MI;
extern MethodInfo m1140_MI;
extern MethodInfo m1994_MI;
extern MethodInfo m1161_MI;
struct t259;
struct t259;
 void m1995_gshared (t259 * __this, t29 ** p0, t29 * p1, MethodInfo* method);
#define m1995(__this, p0, p1, method) (void)m1995_gshared((t259 *)__this, (t29 **)p0, (t29 *)p1, method)
#define m1985(__this, p0, p1, method) (void)m1995_gshared((t259 *)__this, (t29 **)p0, (t29 *)p1, method)
struct t259;
 void m1986 (t259 * __this, int32_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m1137 (t259 * __this, MethodInfo* method){
	{
		t263 * L_0 = (t263 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t263_TI));
		m1988(L_0, &m1988_MI);
		__this->f2 = L_0;
		t17  L_1 = m1394(NULL, &m1394_MI);
		__this->f6 = L_1;
		t17  L_2 = m1394(NULL, &m1394_MI);
		__this->f7 = L_2;
		t17  L_3 = m1394(NULL, &m1394_MI);
		__this->f8 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t264_TI));
		t264 * L_4 = (t264 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t264_TI));
		m1989(L_4, &m1989_MI);
		__this->f9 = L_4;
		m168(__this, &m168_MI);
		t263 * L_5 = (__this->f2);
		if (L_5)
		{
			goto IL_0053;
		}
	}
	{
		t263 * L_6 = (t263 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t263_TI));
		m1988(L_6, &m1988_MI);
		__this->f2 = L_6;
	}

IL_0053:
	{
		return;
	}
}
 t263 * m1138 (t259 * __this, MethodInfo* method){
	{
		t263 * L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m1139_MI;
 void m1139 (t259 * __this, t263 * p0, MethodInfo* method){
	{
		t263 ** L_0 = &(__this->f2);
		m1985(__this, L_0, p0, &m1985_MI);
		return;
	}
}
 int32_t m1140 (t259 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m1141_MI;
 void m1141 (t259 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t* L_0 = &(__this->f3);
		m1986(__this, L_0, p0, &m1986_MI);
		return;
	}
}
 t2 * m1142 (t259 * __this, MethodInfo* method){
	{
		t2 * L_0 = (__this->f4);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		t2 * L_2 = m60(__this, &m60_MI);
		__this->f4 = L_2;
	}

IL_001d:
	{
		t2 * L_3 = (__this->f4);
		return L_3;
	}
}
 t264 * m1143 (t259 * __this, MethodInfo* method){
	{
		t264 * L_0 = (__this->f9);
		return L_0;
	}
}
 void m1144 (t259 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	t2 * V_1 = {0};
	t29 * V_2 = {0};
	{
		t264 * L_0 = (__this->f9);
		VirtActionInvoker0::Invoke(&m1990_MI, L_0);
		V_0 = 0;
		goto IL_007c;
	}

IL_0012:
	{
		t2 * L_1 = m1142(__this, &m1142_MI);
		t25 * L_2 = m1991(L_1, V_0, &m1991_MI);
		V_1 = ((t2 *)IsInst(L_2, InitializedTypeInfo(&t2_TI)));
		bool L_3 = m1297(NULL, V_1, (t41 *)NULL, &m1297_MI);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		goto IL_0078;
	}

IL_0035:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_4 = m1554(NULL, LoadTypeToken(&t412_0_0_0), &m1554_MI);
		t28 * L_5 = m1987(V_1, L_4, &m1987_MI);
		V_2 = ((t29 *)IsInst(L_5, InitializedTypeInfo(&t412_TI)));
		t16 * L_6 = m1392(V_1, &m1392_MI);
		bool L_7 = m1393(L_6, &m1393_MI);
		if (!L_7)
		{
			goto IL_0078;
		}
	}
	{
		if (!V_2)
		{
			goto IL_006c;
		}
	}
	{
		bool L_8 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m1984_MI, V_2);
		if (L_8)
		{
			goto IL_0078;
		}
	}

IL_006c:
	{
		t264 * L_9 = (__this->f9);
		VirtActionInvoker1< t2 * >::Invoke(&m1992_MI, L_9, V_1);
	}

IL_0078:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_007c:
	{
		t2 * L_10 = m1142(__this, &m1142_MI);
		int32_t L_11 = m1993(L_10, &m1993_MI);
		if ((((int32_t)V_0) < ((int32_t)L_11)))
		{
			goto IL_0012;
		}
	}
	{
		t218 * L_12 = &(__this->f5);
		m1831(L_12, &m1831_MI);
		return;
	}
}
 float m1145 (t259 * __this, MethodInfo* method){
	{
		float L_0 = m1155(__this, 0, &m1155_MI);
		return L_0;
	}
}
 float m1146 (t259 * __this, MethodInfo* method){
	{
		float L_0 = m1156(__this, 0, &m1156_MI);
		return L_0;
	}
}
 float m1147 (t259 * __this, MethodInfo* method){
	{
		float L_0 = m1157(__this, 0, &m1157_MI);
		return L_0;
	}
}
 float m1148 (t259 * __this, MethodInfo* method){
	{
		float L_0 = m1155(__this, 1, &m1155_MI);
		return L_0;
	}
}
 float m1149 (t259 * __this, MethodInfo* method){
	{
		float L_0 = m1156(__this, 1, &m1156_MI);
		return L_0;
	}
}
 float m1150 (t259 * __this, MethodInfo* method){
	{
		float L_0 = m1157(__this, 1, &m1157_MI);
		return L_0;
	}
}
 int32_t m1151 (t259 * __this, MethodInfo* method){
	{
		return 0;
	}
}
 void m1152 (t259 * __this, MethodInfo* method){
	{
		m170(__this, &m170_MI);
		m1164(__this, &m1164_MI);
		return;
	}
}
 void m1153 (t259 * __this, MethodInfo* method){
	{
		t218 * L_0 = &(__this->f5);
		m1831(L_0, &m1831_MI);
		t2 * L_1 = m1142(__this, &m1142_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1174(NULL, L_1, &m1174_MI);
		m172(__this, &m172_MI);
		return;
	}
}
 void m1154 (t259 * __this, MethodInfo* method){
	{
		m1164(__this, &m1164_MI);
		return;
	}
}
 float m1155 (t259 * __this, int32_t p0, MethodInfo* method){
	{
		t17 * L_0 = &(__this->f6);
		float L_1 = m1672(L_0, p0, &m1672_MI);
		return L_1;
	}
}
 float m1156 (t259 * __this, int32_t p0, MethodInfo* method){
	{
		t17 * L_0 = &(__this->f7);
		float L_1 = m1672(L_0, p0, &m1672_MI);
		return L_1;
	}
}
 float m1157 (t259 * __this, int32_t p0, MethodInfo* method){
	{
		t17 * L_0 = &(__this->f8);
		float L_1 = m1672(L_0, p0, &m1672_MI);
		return L_1;
	}
}
 float m1158 (t259 * __this, int32_t p0, float p1, MethodInfo* method){
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	t164  V_4 = {0};
	t17  V_5 = {0};
	float G_B2_0 = 0.0f;
	float G_B1_0 = 0.0f;
	int32_t G_B3_0 = 0;
	float G_B3_1 = 0.0f;
	int32_t G_B9_0 = 0;
	{
		G_B1_0 = p1;
		if (p0)
		{
			G_B2_0 = p1;
			goto IL_0017;
		}
	}
	{
		t263 * L_0 = m1138(__this, &m1138_MI);
		int32_t L_1 = m1962(L_0, &m1962_MI);
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0022;
	}

IL_0017:
	{
		t263 * L_2 = m1138(__this, &m1138_MI);
		int32_t L_3 = m1963(L_2, &m1963_MI);
		G_B3_0 = L_3;
		G_B3_1 = G_B2_0;
	}

IL_0022:
	{
		V_0 = ((float)(G_B3_1+(((float)G_B3_0))));
		t2 * L_4 = m1142(__this, &m1142_MI);
		t164  L_5 = m1572(L_4, &m1572_MI);
		V_4 = L_5;
		t17  L_6 = m1653((&V_4), &m1653_MI);
		V_5 = L_6;
		float L_7 = m1672((&V_5), p0, &m1672_MI);
		V_1 = L_7;
		V_2 = ((float)(V_1-V_0));
		V_3 = (0.0f);
		if (p0)
		{
			goto IL_0069;
		}
	}
	{
		int32_t L_8 = m1140(__this, &m1140_MI);
		V_3 = ((float)((float)(((float)((int32_t)(L_8%3))))*(float)(0.5f)));
		goto IL_0079;
	}

IL_0069:
	{
		int32_t L_9 = m1140(__this, &m1140_MI);
		V_3 = ((float)((float)(((float)((int32_t)((int32_t)L_9/(int32_t)3))))*(float)(0.5f)));
	}

IL_0079:
	{
		if (p0)
		{
			goto IL_008f;
		}
	}
	{
		t263 * L_10 = m1138(__this, &m1138_MI);
		int32_t L_11 = m1968(L_10, &m1968_MI);
		G_B9_0 = L_11;
		goto IL_009a;
	}

IL_008f:
	{
		t263 * L_12 = m1138(__this, &m1138_MI);
		int32_t L_13 = m1969(L_12, &m1969_MI);
		G_B9_0 = L_13;
	}

IL_009a:
	{
		return ((float)((((float)G_B9_0))+((float)((float)V_2*(float)V_3))));
	}
}
 void m1159 (t259 * __this, float p0, float p1, float p2, int32_t p3, MethodInfo* method){
	{
		t17 * L_0 = &(__this->f6);
		m1687(L_0, p3, p0, &m1687_MI);
		t17 * L_1 = &(__this->f7);
		m1687(L_1, p3, p1, &m1687_MI);
		t17 * L_2 = &(__this->f8);
		m1687(L_2, p3, p2, &m1687_MI);
		return;
	}
}
 void m1160 (t259 * __this, t2 * p0, int32_t p1, float p2, float p3, MethodInfo* method){
	t2 * G_B4_0 = {0};
	t2 * G_B3_0 = {0};
	int32_t G_B5_0 = 0;
	t2 * G_B5_1 = {0};
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		t218 * L_1 = &(__this->f5);
		m1833(L_1, __this, p0, ((int32_t)16134), &m1833_MI);
		G_B3_0 = p0;
		if (p1)
		{
			G_B4_0 = p0;
			goto IL_002c;
		}
	}
	{
		G_B5_0 = 0;
		G_B5_1 = G_B3_0;
		goto IL_002d;
	}

IL_002c:
	{
		G_B5_0 = 2;
		G_B5_1 = G_B4_0;
	}

IL_002d:
	{
		m1994(G_B5_1, G_B5_0, p2, p3, &m1994_MI);
		return;
	}
}
 bool m1161 (t259 * __this, MethodInfo* method){
	t25 * V_0 = {0};
	{
		t25 * L_0 = m39(__this, &m39_MI);
		t25 * L_1 = m1365(L_0, &m1365_MI);
		V_0 = L_1;
		bool L_2 = m1297(NULL, V_0, (t41 *)NULL, &m1297_MI);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		return 1;
	}

IL_001a:
	{
		t25 * L_3 = m39(__this, &m39_MI);
		t25 * L_4 = m1365(L_3, &m1365_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_5 = m1554(NULL, LoadTypeToken(&t411_0_0_0), &m1554_MI);
		t28 * L_6 = m1987(L_4, L_5, &m1987_MI);
		bool L_7 = m1297(NULL, L_6, (t41 *)NULL, &m1297_MI);
		return L_7;
	}
}
 void m1162 (t259 * __this, MethodInfo* method){
	{
		m175(__this, &m175_MI);
		bool L_0 = m1161(__this, &m1161_MI);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		m1164(__this, &m1164_MI);
	}

IL_0017:
	{
		return;
	}
}
 void m1163 (t259 * __this, MethodInfo* method){
	{
		m1164(__this, &m1164_MI);
		return;
	}
}
 void m1164 (t259 * __this, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		t2 * L_1 = m1142(__this, &m1142_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1174(NULL, L_1, &m1174_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.LayoutGroup
extern Il2CppType t263_0_0_4;
extern CustomAttributesCache t259__CustomAttributeCache_m_Padding;
FieldInfo t259_f2_FieldInfo = 
{
	"m_Padding", &t263_0_0_4, &t259_TI, offsetof(t259, f2), &t259__CustomAttributeCache_m_Padding};
extern Il2CppType t150_0_0_4;
extern CustomAttributesCache t259__CustomAttributeCache_m_ChildAlignment;
FieldInfo t259_f3_FieldInfo = 
{
	"m_ChildAlignment", &t150_0_0_4, &t259_TI, offsetof(t259, f3), &t259__CustomAttributeCache_m_ChildAlignment};
extern Il2CppType t2_0_0_129;
FieldInfo t259_f4_FieldInfo = 
{
	"m_Rect", &t2_0_0_129, &t259_TI, offsetof(t259, f4), &EmptyCustomAttributesCache};
extern Il2CppType t218_0_0_4;
FieldInfo t259_f5_FieldInfo = 
{
	"m_Tracker", &t218_0_0_4, &t259_TI, offsetof(t259, f5), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_1;
FieldInfo t259_f6_FieldInfo = 
{
	"m_TotalMinSize", &t17_0_0_1, &t259_TI, offsetof(t259, f6), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_1;
FieldInfo t259_f7_FieldInfo = 
{
	"m_TotalPreferredSize", &t17_0_0_1, &t259_TI, offsetof(t259, f7), &EmptyCustomAttributesCache};
extern Il2CppType t17_0_0_1;
FieldInfo t259_f8_FieldInfo = 
{
	"m_TotalFlexibleSize", &t17_0_0_1, &t259_TI, offsetof(t259, f8), &EmptyCustomAttributesCache};
extern Il2CppType t264_0_0_129;
FieldInfo t259_f9_FieldInfo = 
{
	"m_RectChildren", &t264_0_0_129, &t259_TI, offsetof(t259, f9), &EmptyCustomAttributesCache};
static FieldInfo* t259_FIs[] =
{
	&t259_f2_FieldInfo,
	&t259_f3_FieldInfo,
	&t259_f4_FieldInfo,
	&t259_f5_FieldInfo,
	&t259_f6_FieldInfo,
	&t259_f7_FieldInfo,
	&t259_f8_FieldInfo,
	&t259_f9_FieldInfo,
	NULL
};
static PropertyInfo t259____padding_PropertyInfo = 
{
	&t259_TI, "padding", &m1138_MI, &m1139_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t259____childAlignment_PropertyInfo = 
{
	&t259_TI, "childAlignment", &m1140_MI, &m1141_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t259____rectTransform_PropertyInfo = 
{
	&t259_TI, "rectTransform", &m1142_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t259____rectChildren_PropertyInfo = 
{
	&t259_TI, "rectChildren", &m1143_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t259____minWidth_PropertyInfo = 
{
	&t259_TI, "minWidth", &m1145_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t259____preferredWidth_PropertyInfo = 
{
	&t259_TI, "preferredWidth", &m1146_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t259____flexibleWidth_PropertyInfo = 
{
	&t259_TI, "flexibleWidth", &m1147_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t259____minHeight_PropertyInfo = 
{
	&t259_TI, "minHeight", &m1148_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t259____preferredHeight_PropertyInfo = 
{
	&t259_TI, "preferredHeight", &m1149_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t259____flexibleHeight_PropertyInfo = 
{
	&t259_TI, "flexibleHeight", &m1150_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t259____layoutPriority_PropertyInfo = 
{
	&t259_TI, "layoutPriority", &m1151_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t259____isRootLayoutGroup_PropertyInfo = 
{
	&t259_TI, "isRootLayoutGroup", &m1161_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t259_PIs[] =
{
	&t259____padding_PropertyInfo,
	&t259____childAlignment_PropertyInfo,
	&t259____rectTransform_PropertyInfo,
	&t259____rectChildren_PropertyInfo,
	&t259____minWidth_PropertyInfo,
	&t259____preferredWidth_PropertyInfo,
	&t259____flexibleWidth_PropertyInfo,
	&t259____minHeight_PropertyInfo,
	&t259____preferredHeight_PropertyInfo,
	&t259____flexibleHeight_PropertyInfo,
	&t259____layoutPriority_PropertyInfo,
	&t259____isRootLayoutGroup_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1137_MI = 
{
	".ctor", (methodPointerType)&m1137, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1133, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t263_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1138_MI = 
{
	"get_padding", (methodPointerType)&m1138, &t259_TI, &t263_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1134, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t263_0_0_0;
extern Il2CppType t263_0_0_0;
static ParameterInfo t259_m1139_ParameterInfos[] = 
{
	{"value", 0, 134218360, &EmptyCustomAttributesCache, &t263_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1139_MI = 
{
	"set_padding", (methodPointerType)&m1139, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t259_m1139_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1135, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t150_0_0_0;
extern void* RuntimeInvoker_t150 (MethodInfo* method, void* obj, void** args);
MethodInfo m1140_MI = 
{
	"get_childAlignment", (methodPointerType)&m1140, &t259_TI, &t150_0_0_0, RuntimeInvoker_t150, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1136, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t150_0_0_0;
static ParameterInfo t259_m1141_ParameterInfos[] = 
{
	{"value", 0, 134218361, &EmptyCustomAttributesCache, &t150_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1141_MI = 
{
	"set_childAlignment", (methodPointerType)&m1141, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t259_m1141_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1137, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1142_MI = 
{
	"get_rectTransform", (methodPointerType)&m1142, &t259_TI, &t2_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2180, 0, 255, 0, false, false, 1138, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t264_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1143_MI = 
{
	"get_rectChildren", (methodPointerType)&m1143, &t259_TI, &t264_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2180, 0, 255, 0, false, false, 1139, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1144_MI = 
{
	"CalculateLayoutInputHorizontal", (methodPointerType)&m1144, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 26, 0, false, false, 1140, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1970_MI = 
{
	"CalculateLayoutInputVertical", NULL, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 27, 0, false, false, 1141, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1145_MI = 
{
	"get_minWidth", (methodPointerType)&m1145, &t259_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 28, 0, false, false, 1142, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1146_MI = 
{
	"get_preferredWidth", (methodPointerType)&m1146, &t259_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 29, 0, false, false, 1143, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1147_MI = 
{
	"get_flexibleWidth", (methodPointerType)&m1147, &t259_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 30, 0, false, false, 1144, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1148_MI = 
{
	"get_minHeight", (methodPointerType)&m1148, &t259_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 31, 0, false, false, 1145, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1149_MI = 
{
	"get_preferredHeight", (methodPointerType)&m1149, &t259_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 32, 0, false, false, 1146, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1150_MI = 
{
	"get_flexibleHeight", (methodPointerType)&m1150, &t259_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2502, 0, 33, 0, false, false, 1147, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1151_MI = 
{
	"get_layoutPriority", (methodPointerType)&m1151, &t259_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2502, 0, 34, 0, false, false, 1148, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1971_MI = 
{
	"SetLayoutHorizontal", NULL, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 35, 0, false, false, 1149, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1972_MI = 
{
	"SetLayoutVertical", NULL, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 36, 0, false, false, 1150, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1152_MI = 
{
	"OnEnable", (methodPointerType)&m1152, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 5, 0, false, false, 1151, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1153_MI = 
{
	"OnDisable", (methodPointerType)&m1153, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 7, 0, false, false, 1152, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1154_MI = 
{
	"OnDidApplyAnimationProperties", (methodPointerType)&m1154, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 13, 0, false, false, 1153, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t259_m1155_ParameterInfos[] = 
{
	{"axis", 0, 134218362, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1155_MI = 
{
	"GetTotalMinSize", (methodPointerType)&m1155, &t259_TI, &t22_0_0_0, RuntimeInvoker_t22_t44, t259_m1155_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 1, false, false, 1154, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t259_m1156_ParameterInfos[] = 
{
	{"axis", 0, 134218363, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1156_MI = 
{
	"GetTotalPreferredSize", (methodPointerType)&m1156, &t259_TI, &t22_0_0_0, RuntimeInvoker_t22_t44, t259_m1156_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 1, false, false, 1155, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t259_m1157_ParameterInfos[] = 
{
	{"axis", 0, 134218364, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1157_MI = 
{
	"GetTotalFlexibleSize", (methodPointerType)&m1157, &t259_TI, &t22_0_0_0, RuntimeInvoker_t22_t44, t259_m1157_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 1, false, false, 1156, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t259_m1158_ParameterInfos[] = 
{
	{"axis", 0, 134218365, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"requiredSpaceWithoutPadding", 1, 134218366, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t44_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1158_MI = 
{
	"GetStartOffset", (methodPointerType)&m1158, &t259_TI, &t22_0_0_0, RuntimeInvoker_t22_t44_t22, t259_m1158_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 2, false, false, 1157, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t259_m1159_ParameterInfos[] = 
{
	{"totalMin", 0, 134218367, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"totalPreferred", 1, 134218368, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"totalFlexible", 2, 134218369, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"axis", 3, 134218370, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22_t22_t22_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1159_MI = 
{
	"SetLayoutInputForAxis", (methodPointerType)&m1159, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21_t22_t22_t22_t44, t259_m1159_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 4, false, false, 1158, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t259_m1160_ParameterInfos[] = 
{
	{"rect", 0, 134218371, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"axis", 1, 134218372, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"pos", 2, 134218373, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"size", 3, 134218374, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44_t22_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1160_MI = 
{
	"SetChildAlongAxis", (methodPointerType)&m1160, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t22_t22, t259_m1160_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 4, false, false, 1159, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1161_MI = 
{
	"get_isRootLayoutGroup", (methodPointerType)&m1161, &t259_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 1160, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1162_MI = 
{
	"OnRectTransformDimensionsChange", (methodPointerType)&m1162, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 10, 0, false, false, 1161, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1163_MI = 
{
	"OnTransformChildrenChanged", (methodPointerType)&m1163, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 37, 0, false, false, 1162, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType m1996_gp_0_1_0_0;
extern Il2CppType m1996_gp_0_1_0_0;
extern Il2CppType m1996_gp_0_0_0_0;
extern Il2CppType m1996_gp_0_0_0_0;
static ParameterInfo t259_m1996_ParameterInfos[] = 
{
	{"currentValue", 0, 134218375, &EmptyCustomAttributesCache, &m1996_gp_0_1_0_0},
	{"newValue", 1, 134218376, &EmptyCustomAttributesCache, &m1996_gp_0_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericContainer m1996_IGC;
extern TypeInfo m1996_gp_T_0_TI;
Il2CppGenericParamFull m1996_gp_T_0_TI_GenericParamFull = { { &m1996_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* m1996_IGPA[1] = 
{
	&m1996_gp_T_0_TI_GenericParamFull,
};
extern MethodInfo m1996_MI;
Il2CppGenericContainer m1996_IGC = { { NULL, NULL }, NULL, &m1996_MI, 1, 1, m1996_IGPA };
static Il2CppRGCTXDefinition m1996_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &m1996_gp_0_0_0_0 }/* Class Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
MethodInfo m1996_MI = 
{
	"SetProperty", NULL, &t259_TI, &t21_0_0_0, NULL, t259_m1996_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 2, true, false, 1163, m1996_RGCTXData, (methodPointerType)NULL, &m1996_IGC};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1164_MI = 
{
	"SetDirty", (methodPointerType)&m1164, &t259_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 132, 0, 255, 0, false, false, 1164, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t259_MIs[] =
{
	&m1137_MI,
	&m1138_MI,
	&m1139_MI,
	&m1140_MI,
	&m1141_MI,
	&m1142_MI,
	&m1143_MI,
	&m1144_MI,
	&m1970_MI,
	&m1145_MI,
	&m1146_MI,
	&m1147_MI,
	&m1148_MI,
	&m1149_MI,
	&m1150_MI,
	&m1151_MI,
	&m1971_MI,
	&m1972_MI,
	&m1152_MI,
	&m1153_MI,
	&m1154_MI,
	&m1155_MI,
	&m1156_MI,
	&m1157_MI,
	&m1158_MI,
	&m1159_MI,
	&m1160_MI,
	&m1161_MI,
	&m1162_MI,
	&m1163_MI,
	&m1996_MI,
	&m1164_MI,
	NULL
};
static MethodInfo* t259_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1152_MI,
	&m171_MI,
	&m1153_MI,
	&m173_MI,
	&m174_MI,
	&m1162_MI,
	&m176_MI,
	&m177_MI,
	&m1154_MI,
	&m179_MI,
	&m1144_MI,
	&m1970_MI,
	&m1145_MI,
	&m1146_MI,
	&m1147_MI,
	&m1148_MI,
	&m1149_MI,
	&m1150_MI,
	&m1151_MI,
	&m1971_MI,
	&m1972_MI,
	&m1144_MI,
	NULL,
	&m1145_MI,
	&m1146_MI,
	&m1147_MI,
	&m1148_MI,
	&m1149_MI,
	&m1150_MI,
	&m1151_MI,
	NULL,
	NULL,
	&m1163_MI,
};
static TypeInfo* t259_ITIs[] = 
{
	&t271_TI,
	&t409_TI,
	&t411_TI,
};
static Il2CppInterfaceOffsetPair t259_IOs[] = 
{
	{ &t271_TI, 15},
	{ &t409_TI, 24},
	{ &t411_TI, 26},
};
void t259_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t360 * tmp;
		tmp = (t360 *)il2cpp_codegen_object_new (&t360_TI);
		m1604(tmp, &m1604_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t318 * tmp;
		tmp = (t318 *)il2cpp_codegen_object_new (&t318_TI);
		m1404(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&t2_TI)), &m1404_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t259_CustomAttributesCacheGenerator_m_Padding(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t259_CustomAttributesCacheGenerator_m_ChildAlignment(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("m_Alignment"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t259__CustomAttributeCache = {
2,
NULL,
&t259_CustomAttributesCacheGenerator
};
CustomAttributesCache t259__CustomAttributeCache_m_Padding = {
1,
NULL,
&t259_CustomAttributesCacheGenerator_m_Padding
};
CustomAttributesCache t259__CustomAttributeCache_m_ChildAlignment = {
2,
NULL,
&t259_CustomAttributesCacheGenerator_m_ChildAlignment
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t259_0_0_0;
extern Il2CppType t259_1_0_0;
struct t259;
extern CustomAttributesCache t259__CustomAttributeCache;
extern CustomAttributesCache t259__CustomAttributeCache_m_Padding;
extern CustomAttributesCache t259__CustomAttributeCache_m_ChildAlignment;
TypeInfo t259_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "LayoutGroup", "UnityEngine.UI", t259_MIs, t259_PIs, t259_FIs, NULL, &t55_TI, NULL, NULL, &t259_TI, t259_ITIs, t259_VT, &t259__CustomAttributeCache, &t259_TI, &t259_0_0_0, &t259_1_0_0, t259_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t259), 0, -1, 0, 0, -1, 1048705, 0, false, false, false, false, false, false, false, false, false, false, false, false, 32, 12, 8, 0, 0, 38, 3, 3};
#include "t265.h"
#ifndef _MSC_VER
#else
#endif

#include "t415.h"
#include "t266.h"
#include "t268.h"
#include "t267.h"
#include "t317.h"
extern TypeInfo t41_TI;
extern TypeInfo t415_TI;
extern TypeInfo t266_TI;
extern TypeInfo t267_TI;
extern TypeInfo t279_TI;
extern TypeInfo t268_TI;
extern TypeInfo t28_TI;
extern TypeInfo t317_TI;
#include "t415MD.h"
#include "t266MD.h"
#include "t267MD.h"
#include "t268MD.h"
#include "t279MD.h"
extern MethodInfo m1168_MI;
extern MethodInfo m1997_MI;
extern MethodInfo m1998_MI;
extern MethodInfo m1181_MI;
extern MethodInfo m1999_MI;
extern MethodInfo m1173_MI;
extern MethodInfo m1182_MI;
extern MethodInfo m1172_MI;
extern MethodInfo m1183_MI;
extern MethodInfo m1184_MI;
extern MethodInfo m1185_MI;
extern MethodInfo m2000_MI;
extern MethodInfo m2001_MI;
extern MethodInfo m1228_MI;
extern MethodInfo m1555_MI;
extern MethodInfo m1171_MI;
extern MethodInfo m1558_MI;
extern MethodInfo m1556_MI;
extern MethodInfo m2002_MI;
extern MethodInfo m1229_MI;
extern MethodInfo m1175_MI;
extern MethodInfo m1176_MI;
extern MethodInfo m1177_MI;
extern MethodInfo m1165_MI;
extern MethodInfo m1312_MI;
extern MethodInfo m1980_MI;
extern MethodInfo m1982_MI;
extern MethodInfo m1981_MI;
extern MethodInfo m1983_MI;


 void m1165 (t265 * __this, t2 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		t2 * L_0 = (__this->f0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m47_MI, L_0);
		__this->f1 = L_1;
		return;
	}
}
extern MethodInfo m1166_MI;
 void m1166 (t29 * __this, MethodInfo* method){
	{
		t35 L_0 = { &m1168_MI };
		t415 * L_1 = (t415 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t415_TI));
		m1997(L_1, NULL, L_0, &m1997_MI);
		m1998(NULL, L_1, &m1998_MI);
		return;
	}
}
extern MethodInfo m1167_MI;
 void m1167 (t265 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	t2 * G_B4_0 = {0};
	t265 * G_B4_1 = {0};
	t2 * G_B3_0 = {0};
	t265 * G_B3_1 = {0};
	t2 * G_B6_0 = {0};
	t265 * G_B6_1 = {0};
	t2 * G_B5_0 = {0};
	t265 * G_B5_1 = {0};
	t2 * G_B8_0 = {0};
	t265 * G_B8_1 = {0};
	t2 * G_B7_0 = {0};
	t265 * G_B7_1 = {0};
	t2 * G_B10_0 = {0};
	t265 * G_B10_1 = {0};
	t2 * G_B9_0 = {0};
	t265 * G_B9_1 = {0};
	{
		V_0 = p0;
		if ((((int32_t)V_0) == ((int32_t)1)))
		{
			goto IL_000e;
		}
	}
	{
		goto IL_00b7;
	}

IL_000e:
	{
		t2 * L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		G_B3_0 = L_0;
		G_B3_1 = __this;
		if ((((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f2))
		{
			G_B4_0 = L_0;
			G_B4_1 = __this;
			goto IL_002d;
		}
	}
	{
		t35 L_1 = { &m1181_MI };
		t266 * L_2 = (t266 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t266_TI));
		m1999(L_2, NULL, L_1, &m1999_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f2 = L_2;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_002d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1173(G_B4_1, G_B4_0, (((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f2), &m1173_MI);
		t2 * L_3 = (__this->f0);
		G_B5_0 = L_3;
		G_B5_1 = __this;
		if ((((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f3))
		{
			G_B6_0 = L_3;
			G_B6_1 = __this;
			goto IL_0056;
		}
	}
	{
		t35 L_4 = { &m1182_MI };
		t266 * L_5 = (t266 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t266_TI));
		m1999(L_5, NULL, L_4, &m1999_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f3 = L_5;
		G_B6_0 = G_B5_0;
		G_B6_1 = G_B5_1;
	}

IL_0056:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1172(G_B6_1, G_B6_0, (((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f3), &m1172_MI);
		t2 * L_6 = (__this->f0);
		G_B7_0 = L_6;
		G_B7_1 = __this;
		if ((((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f4))
		{
			G_B8_0 = L_6;
			G_B8_1 = __this;
			goto IL_007f;
		}
	}
	{
		t35 L_7 = { &m1183_MI };
		t266 * L_8 = (t266 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t266_TI));
		m1999(L_8, NULL, L_7, &m1999_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f4 = L_8;
		G_B8_0 = G_B7_0;
		G_B8_1 = G_B7_1;
	}

IL_007f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1173(G_B8_1, G_B8_0, (((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f4), &m1173_MI);
		t2 * L_9 = (__this->f0);
		G_B9_0 = L_9;
		G_B9_1 = __this;
		if ((((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f5))
		{
			G_B10_0 = L_9;
			G_B10_1 = __this;
			goto IL_00a8;
		}
	}
	{
		t35 L_10 = { &m1184_MI };
		t266 * L_11 = (t266 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t266_TI));
		m1999(L_11, NULL, L_10, &m1999_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f5 = L_11;
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
	}

IL_00a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1172(G_B10_1, G_B10_0, (((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f5), &m1172_MI);
		goto IL_00b7;
	}

IL_00b7:
	{
		return;
	}
}
 void m1168 (t29 * __this, t2 * p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1174(NULL, p0, &m1174_MI);
		return;
	}
}
extern MethodInfo m1169_MI;
 t25 * m1169 (t265 * __this, MethodInfo* method){
	{
		t2 * L_0 = (__this->f0);
		return L_0;
	}
}
extern MethodInfo m1170_MI;
 bool m1170 (t265 * __this, MethodInfo* method){
	{
		t2 * L_0 = (__this->f0);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		return L_1;
	}
}
 void m1171 (t29 * __this, t268 * p0, MethodInfo* method){
	t268 * G_B2_0 = {0};
	t268 * G_B1_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		G_B1_0 = p0;
		if ((((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f6))
		{
			G_B2_0 = p0;
			goto IL_0019;
		}
	}
	{
		t35 L_0 = { &m1185_MI };
		t267 * L_1 = (t267 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t267_TI));
		m2000(L_1, NULL, L_0, &m2000_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f6 = L_1;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m2001(G_B2_0, (((t265_SFs*)InitializedTypeInfo(&t265_TI)->static_fields)->f6), &m2001_MI);
		return;
	}
}
 void m1172 (t265 * __this, t2 * p0, t266 * p1, MethodInfo* method){
	t268 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t279_TI));
		t268 * L_1 = m1228(NULL, &m1228_MI);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t409_0_0_0), &m1554_MI);
		m1555(p0, L_2, V_0, &m1555_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1171(NULL, V_0, &m1171_MI);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1558_MI, V_0);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_00ca;
		}
	}
	{
		V_1 = 0;
		goto IL_005f;
	}

IL_003d:
	{
		t28 * L_4 = (t28 *)VirtFuncInvoker1< t28 *, int32_t >::Invoke(&m1556_MI, V_0, V_1);
		if (!((t29 *)IsInst(L_4, InitializedTypeInfo(&t410_TI))))
		{
			goto IL_005b;
		}
	}
	{
		t28 * L_5 = (t28 *)VirtFuncInvoker1< t28 *, int32_t >::Invoke(&m1556_MI, V_0, V_1);
		VirtActionInvoker1< t28 * >::Invoke(&m2002_MI, p1, L_5);
	}

IL_005b:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005f:
	{
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1558_MI, V_0);
		if ((((int32_t)V_1) < ((int32_t)L_6)))
		{
			goto IL_003d;
		}
	}
	{
		V_2 = 0;
		goto IL_0094;
	}

IL_0072:
	{
		t28 * L_7 = (t28 *)VirtFuncInvoker1< t28 *, int32_t >::Invoke(&m1556_MI, V_0, V_2);
		if (((t29 *)IsInst(L_7, InitializedTypeInfo(&t410_TI))))
		{
			goto IL_0090;
		}
	}
	{
		t28 * L_8 = (t28 *)VirtFuncInvoker1< t28 *, int32_t >::Invoke(&m1556_MI, V_0, V_2);
		VirtActionInvoker1< t28 * >::Invoke(&m2002_MI, p1, L_8);
	}

IL_0090:
	{
		V_2 = ((int32_t)(V_2+1));
	}

IL_0094:
	{
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1558_MI, V_0);
		if ((((int32_t)V_2) < ((int32_t)L_9)))
		{
			goto IL_0072;
		}
	}
	{
		V_3 = 0;
		goto IL_00be;
	}

IL_00a7:
	{
		t25 * L_10 = m1991(p0, V_3, &m1991_MI);
		m1172(__this, ((t2 *)IsInst(L_10, InitializedTypeInfo(&t2_TI))), p1, &m1172_MI);
		V_3 = ((int32_t)(V_3+1));
	}

IL_00be:
	{
		int32_t L_11 = m1993(p0, &m1993_MI);
		if ((((int32_t)V_3) < ((int32_t)L_11)))
		{
			goto IL_00a7;
		}
	}

IL_00ca:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t279_TI));
		m1229(NULL, V_0, &m1229_MI);
		return;
	}
}
 void m1173 (t265 * __this, t2 * p0, t266 * p1, MethodInfo* method){
	t268 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t279_TI));
		t268 * L_1 = m1228(NULL, &m1228_MI);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t271_0_0_0), &m1554_MI);
		m1555(p0, L_2, V_0, &m1555_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1171(NULL, V_0, &m1171_MI);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1558_MI, V_0);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0084;
		}
	}
	{
		V_1 = 0;
		goto IL_0054;
	}

IL_003d:
	{
		t25 * L_4 = m1991(p0, V_1, &m1991_MI);
		m1173(__this, ((t2 *)IsInst(L_4, InitializedTypeInfo(&t2_TI))), p1, &m1173_MI);
		V_1 = ((int32_t)(V_1+1));
	}

IL_0054:
	{
		int32_t L_5 = m1993(p0, &m1993_MI);
		if ((((int32_t)V_1) < ((int32_t)L_5)))
		{
			goto IL_003d;
		}
	}
	{
		V_2 = 0;
		goto IL_0078;
	}

IL_0067:
	{
		t28 * L_6 = (t28 *)VirtFuncInvoker1< t28 *, int32_t >::Invoke(&m1556_MI, V_0, V_2);
		VirtActionInvoker1< t28 * >::Invoke(&m2002_MI, p1, L_6);
		V_2 = ((int32_t)(V_2+1));
	}

IL_0078:
	{
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1558_MI, V_0);
		if ((((int32_t)V_2) < ((int32_t)L_7)))
		{
			goto IL_0067;
		}
	}

IL_0084:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t279_TI));
		m1229(NULL, V_0, &m1229_MI);
		return;
	}
}
 void m1174 (t29 * __this, t2 * p0, MethodInfo* method){
	t2 * V_0 = {0};
	t2 * V_1 = {0};
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		V_0 = p0;
	}

IL_000f:
	{
		t25 * L_1 = m1365(V_0, &m1365_MI);
		V_1 = ((t2 *)IsInst(L_1, InitializedTypeInfo(&t2_TI)));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		bool L_2 = m1175(NULL, V_1, &m1175_MI);
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		goto IL_0032;
	}

IL_002b:
	{
		V_0 = V_1;
		goto IL_000f;
	}

IL_0032:
	{
		bool L_3 = m1297(NULL, V_0, p0, &m1297_MI);
		if (!L_3)
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		bool L_4 = m1176(NULL, V_0, &m1176_MI);
		if (L_4)
		{
			goto IL_004a;
		}
	}
	{
		return;
	}

IL_004a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1177(NULL, V_0, &m1177_MI);
		return;
	}
}
 bool m1175 (t29 * __this, t2 * p0, MethodInfo* method){
	t268 * V_0 = {0};
	bool V_1 = false;
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t279_TI));
		t268 * L_1 = m1228(NULL, &m1228_MI);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t411_0_0_0), &m1554_MI);
		m1555(p0, L_2, V_0, &m1555_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1171(NULL, V_0, &m1171_MI);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1558_MI, V_0);
		V_1 = ((((int32_t)L_3) > ((int32_t)0))? 1 : 0);
		m1229(NULL, V_0, &m1229_MI);
		return V_1;
	}
}
 bool m1176 (t29 * __this, t2 * p0, MethodInfo* method){
	t268 * V_0 = {0};
	bool V_1 = false;
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t279_TI));
		t268 * L_1 = m1228(NULL, &m1228_MI);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t409_0_0_0), &m1554_MI);
		m1555(p0, L_2, V_0, &m1555_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t265_TI));
		m1171(NULL, V_0, &m1171_MI);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1558_MI, V_0);
		V_1 = ((((int32_t)L_3) > ((int32_t)0))? 1 : 0);
		m1229(NULL, V_0, &m1229_MI);
		return V_1;
	}
}
 void m1177 (t29 * __this, t2 * p0, MethodInfo* method){
	{
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		t265  L_1 = {0};
		m1165(&L_1, p0, &m1165_MI);
		t265  L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t265_TI), &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t141_TI));
		m386(NULL, (t29 *)L_3, &m386_MI);
		return;
	}
}
extern MethodInfo m1178_MI;
 bool m1178 (t265 * __this, t265  p0, MethodInfo* method){
	{
		t2 * L_0 = (__this->f0);
		t2 * L_1 = ((&p0)->f0);
		bool L_2 = m1297(NULL, L_0, L_1, &m1297_MI);
		return L_2;
	}
}
extern MethodInfo m1179_MI;
 int32_t m1179 (t265 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m1180_MI;
 t7* m1180 (t265 * __this, MethodInfo* method){
	{
		t2 * L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_1 = m1312(NULL, (t7*) &_stringLiteral63, L_0, &m1312_MI);
		return L_1;
	}
}
 void m1181 (t29 * __this, t28 * p0, MethodInfo* method){
	{
		InterfaceActionInvoker0::Invoke(&m1980_MI, ((t29 *)IsInst(p0, InitializedTypeInfo(&t271_TI))));
		return;
	}
}
 void m1182 (t29 * __this, t28 * p0, MethodInfo* method){
	{
		InterfaceActionInvoker0::Invoke(&m1982_MI, ((t29 *)IsInst(p0, InitializedTypeInfo(&t409_TI))));
		return;
	}
}
 void m1183 (t29 * __this, t28 * p0, MethodInfo* method){
	{
		InterfaceActionInvoker0::Invoke(&m1981_MI, ((t29 *)IsInst(p0, InitializedTypeInfo(&t271_TI))));
		return;
	}
}
 void m1184 (t29 * __this, t28 * p0, MethodInfo* method){
	{
		InterfaceActionInvoker0::Invoke(&m1983_MI, ((t29 *)IsInst(p0, InitializedTypeInfo(&t409_TI))));
		return;
	}
}
 bool m1185 (t29 * __this, t28 * p0, MethodInfo* method){
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		if (!((t317 *)IsInst(p0, InitializedTypeInfo(&t317_TI))))
		{
			goto IL_002e;
		}
	}
	{
		bool L_0 = m1390(((t317 *)IsInst(p0, InitializedTypeInfo(&t317_TI))), &m1390_MI);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		bool L_1 = m1391(((t317 *)IsInst(p0, InitializedTypeInfo(&t317_TI))), &m1391_MI);
		G_B4_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B4_0 = 1;
	}

IL_002c:
	{
		G_B6_0 = G_B4_0;
		goto IL_002f;
	}

IL_002e:
	{
		G_B6_0 = 0;
	}

IL_002f:
	{
		return G_B6_0;
	}
}
// Metadata Definition UnityEngine.UI.LayoutRebuilder
extern Il2CppType t2_0_0_33;
FieldInfo t265_f0_FieldInfo = 
{
	"m_ToRebuild", &t2_0_0_33, &t265_TI, offsetof(t265, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_33;
FieldInfo t265_f1_FieldInfo = 
{
	"m_CachedHasFromTrasnform", &t44_0_0_33, &t265_TI, offsetof(t265, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t266_0_0_17;
extern CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache2;
FieldInfo t265_f2_FieldInfo = 
{
	"<>f__am$cache2", &t266_0_0_17, &t265_TI, offsetof(t265_SFs, f2), &t265__CustomAttributeCache_U3CU3Ef__am$cache2};
extern Il2CppType t266_0_0_17;
extern CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache3;
FieldInfo t265_f3_FieldInfo = 
{
	"<>f__am$cache3", &t266_0_0_17, &t265_TI, offsetof(t265_SFs, f3), &t265__CustomAttributeCache_U3CU3Ef__am$cache3};
extern Il2CppType t266_0_0_17;
extern CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache4;
FieldInfo t265_f4_FieldInfo = 
{
	"<>f__am$cache4", &t266_0_0_17, &t265_TI, offsetof(t265_SFs, f4), &t265__CustomAttributeCache_U3CU3Ef__am$cache4};
extern Il2CppType t266_0_0_17;
extern CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache5;
FieldInfo t265_f5_FieldInfo = 
{
	"<>f__am$cache5", &t266_0_0_17, &t265_TI, offsetof(t265_SFs, f5), &t265__CustomAttributeCache_U3CU3Ef__am$cache5};
extern Il2CppType t267_0_0_17;
extern CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache6;
FieldInfo t265_f6_FieldInfo = 
{
	"<>f__am$cache6", &t267_0_0_17, &t265_TI, offsetof(t265_SFs, f6), &t265__CustomAttributeCache_U3CU3Ef__am$cache6};
static FieldInfo* t265_FIs[] =
{
	&t265_f0_FieldInfo,
	&t265_f1_FieldInfo,
	&t265_f2_FieldInfo,
	&t265_f3_FieldInfo,
	&t265_f4_FieldInfo,
	&t265_f5_FieldInfo,
	&t265_f6_FieldInfo,
	NULL
};
static PropertyInfo t265____transform_PropertyInfo = 
{
	&t265_TI, "transform", &m1169_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t265_PIs[] =
{
	&t265____transform_PropertyInfo,
	NULL
};
extern Il2CppType t2_0_0_0;
static ParameterInfo t265_m1165_ParameterInfos[] = 
{
	{"controller", 0, 134218377, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1165_MI = 
{
	".ctor", (methodPointerType)&m1165, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t265_m1165_ParameterInfos, &EmptyCustomAttributesCache, 6273, 0, 255, 1, false, false, 1165, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1166_MI = 
{
	".cctor", (methodPointerType)&m1166, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 1166, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t140_0_0_0;
static ParameterInfo t265_m1167_ParameterInfos[] = 
{
	{"executing", 0, 134218378, &EmptyCustomAttributesCache, &t140_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1167_MI = 
{
	"UnityEngine.UI.ICanvasElement.Rebuild", (methodPointerType)&m1167, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t265_m1167_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 4, 1, false, false, 1167, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t265_m1168_ParameterInfos[] = 
{
	{"driven", 0, 134218379, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1168_MI = 
{
	"ReapplyDrivenProperties", (methodPointerType)&m1168, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t265_m1168_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1168, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t25_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1169_MI = 
{
	"get_transform", (methodPointerType)&m1169, &t265_TI, &t25_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 5, 0, false, false, 1169, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1170_MI = 
{
	"IsDestroyed", (methodPointerType)&m1170, &t265_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, false, 1170, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t268_0_0_0;
extern Il2CppType t268_0_0_0;
static ParameterInfo t265_m1171_ParameterInfos[] = 
{
	{"components", 0, 134218380, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1171_MI = 
{
	"StripDisabledBehavioursFromList", (methodPointerType)&m1171, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t265_m1171_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1171, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t266_0_0_0;
extern Il2CppType t266_0_0_0;
static ParameterInfo t265_m1172_ParameterInfos[] = 
{
	{"rect", 0, 134218381, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"action", 1, 134218382, &EmptyCustomAttributesCache, &t266_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1172_MI = 
{
	"PerformLayoutControl", (methodPointerType)&m1172, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t265_m1172_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 1172, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t266_0_0_0;
static ParameterInfo t265_m1173_ParameterInfos[] = 
{
	{"rect", 0, 134218383, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"action", 1, 134218384, &EmptyCustomAttributesCache, &t266_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1173_MI = 
{
	"PerformLayoutCalculation", (methodPointerType)&m1173, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t265_m1173_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, false, 1173, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t265_m1174_ParameterInfos[] = 
{
	{"rect", 0, 134218385, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1174_MI = 
{
	"MarkLayoutForRebuild", (methodPointerType)&m1174, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t265_m1174_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 1174, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t265_m1175_ParameterInfos[] = 
{
	{"parent", 0, 134218386, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1175_MI = 
{
	"ValidLayoutGroup", (methodPointerType)&m1175, &t265_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t265_m1175_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1175, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t265_m1176_ParameterInfos[] = 
{
	{"layoutRoot", 0, 134218387, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1176_MI = 
{
	"ValidController", (methodPointerType)&m1176, &t265_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t265_m1176_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1176, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t265_m1177_ParameterInfos[] = 
{
	{"controller", 0, 134218388, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1177_MI = 
{
	"MarkLayoutRootForRebuild", (methodPointerType)&m1177, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t265_m1177_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, false, 1177, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t265_0_0_0;
extern Il2CppType t265_0_0_0;
static ParameterInfo t265_m1178_ParameterInfos[] = 
{
	{"other", 0, 134218389, &EmptyCustomAttributesCache, &t265_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t265 (MethodInfo* method, void* obj, void** args);
MethodInfo m1178_MI = 
{
	"Equals", (methodPointerType)&m1178, &t265_TI, &t40_0_0_0, RuntimeInvoker_t40_t265, t265_m1178_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 7, 1, false, false, 1178, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1179_MI = 
{
	"GetHashCode", (methodPointerType)&m1179, &t265_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 198, 0, 2, 0, false, false, 1179, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t7_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1180_MI = 
{
	"ToString", (methodPointerType)&m1180, &t265_TI, &t7_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 198, 0, 3, 0, false, false, 1180, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t265_m1181_ParameterInfos[] = 
{
	{"e", 0, 134218390, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t265__CustomAttributeCache_m1181;
MethodInfo m1181_MI = 
{
	"<Rebuild>m__9", (methodPointerType)&m1181, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t265_m1181_ParameterInfos, &t265__CustomAttributeCache_m1181, 145, 0, 255, 1, false, false, 1181, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t28_0_0_0;
static ParameterInfo t265_m1182_ParameterInfos[] = 
{
	{"e", 0, 134218391, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t265__CustomAttributeCache_m1182;
MethodInfo m1182_MI = 
{
	"<Rebuild>m__A", (methodPointerType)&m1182, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t265_m1182_ParameterInfos, &t265__CustomAttributeCache_m1182, 145, 0, 255, 1, false, false, 1182, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t28_0_0_0;
static ParameterInfo t265_m1183_ParameterInfos[] = 
{
	{"e", 0, 134218392, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t265__CustomAttributeCache_m1183;
MethodInfo m1183_MI = 
{
	"<Rebuild>m__B", (methodPointerType)&m1183, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t265_m1183_ParameterInfos, &t265__CustomAttributeCache_m1183, 145, 0, 255, 1, false, false, 1183, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t28_0_0_0;
static ParameterInfo t265_m1184_ParameterInfos[] = 
{
	{"e", 0, 134218393, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t265__CustomAttributeCache_m1184;
MethodInfo m1184_MI = 
{
	"<Rebuild>m__C", (methodPointerType)&m1184, &t265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t265_m1184_ParameterInfos, &t265__CustomAttributeCache_m1184, 145, 0, 255, 1, false, false, 1184, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t28_0_0_0;
static ParameterInfo t265_m1185_ParameterInfos[] = 
{
	{"e", 0, 134218394, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t265__CustomAttributeCache_m1185;
MethodInfo m1185_MI = 
{
	"<StripDisabledBehavioursFromList>m__D", (methodPointerType)&m1185, &t265_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t265_m1185_ParameterInfos, &t265__CustomAttributeCache_m1185, 145, 0, 255, 1, false, false, 1185, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t265_MIs[] =
{
	&m1165_MI,
	&m1166_MI,
	&m1167_MI,
	&m1168_MI,
	&m1169_MI,
	&m1170_MI,
	&m1171_MI,
	&m1172_MI,
	&m1173_MI,
	&m1174_MI,
	&m1175_MI,
	&m1176_MI,
	&m1177_MI,
	&m1178_MI,
	&m1179_MI,
	&m1180_MI,
	&m1181_MI,
	&m1182_MI,
	&m1183_MI,
	&m1184_MI,
	&m1185_MI,
	NULL
};
static MethodInfo* t265_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1179_MI,
	&m1180_MI,
	&m1167_MI,
	&m1169_MI,
	&m1170_MI,
	&m1178_MI,
};
extern TypeInfo t416_TI;
static TypeInfo* t265_ITIs[] = 
{
	&t145_TI,
	&t416_TI,
};
static Il2CppInterfaceOffsetPair t265_IOs[] = 
{
	{ &t145_TI, 4},
	{ &t416_TI, 7},
};
void t265_CustomAttributesCacheGenerator_U3CU3Ef__am$cache2(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t265_CustomAttributesCacheGenerator_U3CU3Ef__am$cache3(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t265_CustomAttributesCacheGenerator_U3CU3Ef__am$cache4(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t265_CustomAttributesCacheGenerator_U3CU3Ef__am$cache5(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t265_CustomAttributesCacheGenerator_U3CU3Ef__am$cache6(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t265_CustomAttributesCacheGenerator_m1181(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t265_CustomAttributesCacheGenerator_m1182(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t265_CustomAttributesCacheGenerator_m1183(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t265_CustomAttributesCacheGenerator_m1184(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t265_CustomAttributesCacheGenerator_m1185(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache2 = {
1,
NULL,
&t265_CustomAttributesCacheGenerator_U3CU3Ef__am$cache2
};
CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache3 = {
1,
NULL,
&t265_CustomAttributesCacheGenerator_U3CU3Ef__am$cache3
};
CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache4 = {
1,
NULL,
&t265_CustomAttributesCacheGenerator_U3CU3Ef__am$cache4
};
CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache5 = {
1,
NULL,
&t265_CustomAttributesCacheGenerator_U3CU3Ef__am$cache5
};
CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache6 = {
1,
NULL,
&t265_CustomAttributesCacheGenerator_U3CU3Ef__am$cache6
};
CustomAttributesCache t265__CustomAttributeCache_m1181 = {
1,
NULL,
&t265_CustomAttributesCacheGenerator_m1181
};
CustomAttributesCache t265__CustomAttributeCache_m1182 = {
1,
NULL,
&t265_CustomAttributesCacheGenerator_m1182
};
CustomAttributesCache t265__CustomAttributeCache_m1183 = {
1,
NULL,
&t265_CustomAttributesCacheGenerator_m1183
};
CustomAttributesCache t265__CustomAttributeCache_m1184 = {
1,
NULL,
&t265_CustomAttributesCacheGenerator_m1184
};
CustomAttributesCache t265__CustomAttributeCache_m1185 = {
1,
NULL,
&t265_CustomAttributesCacheGenerator_m1185
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t265_1_0_0;
extern CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache2;
extern CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache3;
extern CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache4;
extern CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache5;
extern CustomAttributesCache t265__CustomAttributeCache_U3CU3Ef__am$cache6;
extern CustomAttributesCache t265__CustomAttributeCache_m1181;
extern CustomAttributesCache t265__CustomAttributeCache_m1182;
extern CustomAttributesCache t265__CustomAttributeCache_m1183;
extern CustomAttributesCache t265__CustomAttributeCache_m1184;
extern CustomAttributesCache t265__CustomAttributeCache_m1185;
TypeInfo t265_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "LayoutRebuilder", "UnityEngine.UI", t265_MIs, t265_PIs, t265_FIs, NULL, &t110_TI, NULL, NULL, &t265_TI, t265_ITIs, t265_VT, &EmptyCustomAttributesCache, &t265_TI, &t265_0_0_0, &t265_1_0_0, t265_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t265)+ sizeof (Il2CppObject), 0, -1, sizeof(t265_SFs), 0, -1, 265, 0, true, false, false, false, false, false, false, false, false, true, false, false, 21, 1, 7, 0, 0, 8, 2, 2};
#include "t269.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t269_TI;

#include "t270.h"
extern TypeInfo t270_TI;
extern TypeInfo t22_TI;
#include "t270MD.h"
extern MethodInfo m1189_MI;
extern MethodInfo m1192_MI;
extern MethodInfo m1190_MI;
extern MethodInfo m1193_MI;
extern MethodInfo m1191_MI;
extern MethodInfo m1194_MI;
extern MethodInfo m1197_MI;
extern MethodInfo m2003_MI;
extern MethodInfo m1195_MI;
extern MethodInfo m1198_MI;
extern MethodInfo m1199_MI;
extern MethodInfo m1200_MI;
extern MethodInfo m1201_MI;
extern MethodInfo m1202_MI;
extern MethodInfo m1203_MI;
extern MethodInfo m1204_MI;
extern MethodInfo m1196_MI;
extern MethodInfo m2004_MI;


 float m1186 (t29 * __this, t2 * p0, int32_t p1, MethodInfo* method){
	{
		if (p1)
		{
			goto IL_000d;
		}
	}
	{
		float L_0 = m1189(NULL, p0, &m1189_MI);
		return L_0;
	}

IL_000d:
	{
		float L_1 = m1192(NULL, p0, &m1192_MI);
		return L_1;
	}
}
 float m1187 (t29 * __this, t2 * p0, int32_t p1, MethodInfo* method){
	{
		if (p1)
		{
			goto IL_000d;
		}
	}
	{
		float L_0 = m1190(NULL, p0, &m1190_MI);
		return L_0;
	}

IL_000d:
	{
		float L_1 = m1193(NULL, p0, &m1193_MI);
		return L_1;
	}
}
 float m1188 (t29 * __this, t2 * p0, int32_t p1, MethodInfo* method){
	{
		if (p1)
		{
			goto IL_000d;
		}
	}
	{
		float L_0 = m1191(NULL, p0, &m1191_MI);
		return L_0;
	}

IL_000d:
	{
		float L_1 = m1194(NULL, p0, &m1194_MI);
		return L_1;
	}
}
 float m1189 (t29 * __this, t2 * p0, MethodInfo* method){
	t2 * G_B2_0 = {0};
	t2 * G_B1_0 = {0};
	{
		G_B1_0 = p0;
		if ((((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f0))
		{
			G_B2_0 = p0;
			goto IL_0019;
		}
	}
	{
		t35 L_0 = { &m1197_MI };
		t270 * L_1 = (t270 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t270_TI));
		m2003(L_1, NULL, L_0, &m2003_MI);
		((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f0 = L_1;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		float L_2 = m1195(NULL, G_B2_0, (((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f0), (0.0f), &m1195_MI);
		return L_2;
	}
}
 float m1190 (t29 * __this, t2 * p0, MethodInfo* method){
	t2 * G_B2_0 = {0};
	t2 * G_B1_0 = {0};
	t2 * G_B4_0 = {0};
	float G_B4_1 = 0.0f;
	t2 * G_B3_0 = {0};
	float G_B3_1 = 0.0f;
	{
		G_B1_0 = p0;
		if ((((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f1))
		{
			G_B2_0 = p0;
			goto IL_0019;
		}
	}
	{
		t35 L_0 = { &m1198_MI };
		t270 * L_1 = (t270 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t270_TI));
		m2003(L_1, NULL, L_0, &m2003_MI);
		((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f1 = L_1;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		float L_2 = m1195(NULL, G_B2_0, (((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f1), (0.0f), &m1195_MI);
		G_B3_0 = p0;
		G_B3_1 = L_2;
		if ((((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f2))
		{
			G_B4_0 = p0;
			G_B4_1 = L_2;
			goto IL_0041;
		}
	}
	{
		t35 L_3 = { &m1199_MI };
		t270 * L_4 = (t270 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t270_TI));
		m2003(L_4, NULL, L_3, &m2003_MI);
		((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f2 = L_4;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0041:
	{
		float L_5 = m1195(NULL, G_B4_0, (((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f2), (0.0f), &m1195_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_6 = m1889(NULL, G_B4_1, L_5, &m1889_MI);
		return L_6;
	}
}
 float m1191 (t29 * __this, t2 * p0, MethodInfo* method){
	t2 * G_B2_0 = {0};
	t2 * G_B1_0 = {0};
	{
		G_B1_0 = p0;
		if ((((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f3))
		{
			G_B2_0 = p0;
			goto IL_0019;
		}
	}
	{
		t35 L_0 = { &m1200_MI };
		t270 * L_1 = (t270 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t270_TI));
		m2003(L_1, NULL, L_0, &m2003_MI);
		((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f3 = L_1;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		float L_2 = m1195(NULL, G_B2_0, (((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f3), (0.0f), &m1195_MI);
		return L_2;
	}
}
 float m1192 (t29 * __this, t2 * p0, MethodInfo* method){
	t2 * G_B2_0 = {0};
	t2 * G_B1_0 = {0};
	{
		G_B1_0 = p0;
		if ((((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f4))
		{
			G_B2_0 = p0;
			goto IL_0019;
		}
	}
	{
		t35 L_0 = { &m1201_MI };
		t270 * L_1 = (t270 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t270_TI));
		m2003(L_1, NULL, L_0, &m2003_MI);
		((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f4 = L_1;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		float L_2 = m1195(NULL, G_B2_0, (((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f4), (0.0f), &m1195_MI);
		return L_2;
	}
}
 float m1193 (t29 * __this, t2 * p0, MethodInfo* method){
	t2 * G_B2_0 = {0};
	t2 * G_B1_0 = {0};
	t2 * G_B4_0 = {0};
	float G_B4_1 = 0.0f;
	t2 * G_B3_0 = {0};
	float G_B3_1 = 0.0f;
	{
		G_B1_0 = p0;
		if ((((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f5))
		{
			G_B2_0 = p0;
			goto IL_0019;
		}
	}
	{
		t35 L_0 = { &m1202_MI };
		t270 * L_1 = (t270 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t270_TI));
		m2003(L_1, NULL, L_0, &m2003_MI);
		((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f5 = L_1;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		float L_2 = m1195(NULL, G_B2_0, (((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f5), (0.0f), &m1195_MI);
		G_B3_0 = p0;
		G_B3_1 = L_2;
		if ((((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f6))
		{
			G_B4_0 = p0;
			G_B4_1 = L_2;
			goto IL_0041;
		}
	}
	{
		t35 L_3 = { &m1203_MI };
		t270 * L_4 = (t270 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t270_TI));
		m2003(L_4, NULL, L_3, &m2003_MI);
		((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f6 = L_4;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
	}

IL_0041:
	{
		float L_5 = m1195(NULL, G_B4_0, (((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f6), (0.0f), &m1195_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_6 = m1889(NULL, G_B4_1, L_5, &m1889_MI);
		return L_6;
	}
}
 float m1194 (t29 * __this, t2 * p0, MethodInfo* method){
	t2 * G_B2_0 = {0};
	t2 * G_B1_0 = {0};
	{
		G_B1_0 = p0;
		if ((((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f7))
		{
			G_B2_0 = p0;
			goto IL_0019;
		}
	}
	{
		t35 L_0 = { &m1204_MI };
		t270 * L_1 = (t270 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t270_TI));
		m2003(L_1, NULL, L_0, &m2003_MI);
		((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f7 = L_1;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		float L_2 = m1195(NULL, G_B2_0, (((t269_SFs*)InitializedTypeInfo(&t269_TI)->static_fields)->f7), (0.0f), &m1195_MI);
		return L_2;
	}
}
 float m1195 (t29 * __this, t2 * p0, t270 * p1, float p2, MethodInfo* method){
	t29 * V_0 = {0};
	{
		float L_0 = m1196(NULL, p0, p1, p2, (&V_0), &m1196_MI);
		return L_0;
	}
}
 float m1196 (t29 * __this, t2 * p0, t270 * p1, float p2, t29 ** p3, MethodInfo* method){
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	t268 * V_2 = {0};
	int32_t V_3 = 0;
	t29 * V_4 = {0};
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	{
		*((t29 **)(p3)) = (t29 *)NULL;
		bool L_0 = m1297(NULL, p0, (t41 *)NULL, &m1297_MI);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		return (0.0f);
	}

IL_0015:
	{
		V_0 = p2;
		V_1 = ((int32_t)-2147483648);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t279_TI));
		t268 * L_1 = m1228(NULL, &m1228_MI);
		V_2 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_2 = m1554(NULL, LoadTypeToken(&t271_0_0_0), &m1554_MI);
		m1555(p0, L_2, V_2, &m1555_MI);
		V_3 = 0;
		goto IL_00d7;
	}

IL_003b:
	{
		t28 * L_3 = (t28 *)VirtFuncInvoker1< t28 *, int32_t >::Invoke(&m1556_MI, V_2, V_3);
		V_4 = ((t29 *)IsInst(L_3, InitializedTypeInfo(&t271_TI)));
		if (!((t317 *)IsInst(V_4, InitializedTypeInfo(&t317_TI))))
		{
			goto IL_007c;
		}
	}
	{
		bool L_4 = m1390(((t317 *)IsInst(V_4, InitializedTypeInfo(&t317_TI))), &m1390_MI);
		if (!L_4)
		{
			goto IL_0077;
		}
	}
	{
		bool L_5 = m1391(((t317 *)IsInst(V_4, InitializedTypeInfo(&t317_TI))), &m1391_MI);
		if (L_5)
		{
			goto IL_007c;
		}
	}

IL_0077:
	{
		goto IL_00d3;
	}

IL_007c:
	{
		int32_t L_6 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1979_MI, V_4);
		V_5 = L_6;
		if ((((int32_t)V_5) >= ((int32_t)V_1)))
		{
			goto IL_0092;
		}
	}
	{
		goto IL_00d3;
	}

IL_0092:
	{
		float L_7 = (float)VirtFuncInvoker1< float, t29 * >::Invoke(&m2004_MI, p1, V_4);
		V_6 = L_7;
		if ((((float)V_6) >= ((float)(0.0f))))
		{
			goto IL_00ad;
		}
	}
	{
		goto IL_00d3;
	}

IL_00ad:
	{
		if ((((int32_t)V_5) <= ((int32_t)V_1)))
		{
			goto IL_00c4;
		}
	}
	{
		V_0 = V_6;
		V_1 = V_5;
		*((t29 **)(p3)) = (t29 *)V_4;
		goto IL_00d3;
	}

IL_00c4:
	{
		if ((((float)V_6) <= ((float)V_0)))
		{
			goto IL_00d3;
		}
	}
	{
		V_0 = V_6;
		*((t29 **)(p3)) = (t29 *)V_4;
	}

IL_00d3:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_00d7:
	{
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1558_MI, V_2);
		if ((((int32_t)V_3) < ((int32_t)L_8)))
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t279_TI));
		m1229(NULL, V_2, &m1229_MI);
		return V_0;
	}
}
 float m1197 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		float L_0 = (float)InterfaceFuncInvoker0< float >::Invoke(&m1973_MI, p0);
		return L_0;
	}
}
 float m1198 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		float L_0 = (float)InterfaceFuncInvoker0< float >::Invoke(&m1973_MI, p0);
		return L_0;
	}
}
 float m1199 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		float L_0 = (float)InterfaceFuncInvoker0< float >::Invoke(&m1974_MI, p0);
		return L_0;
	}
}
 float m1200 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		float L_0 = (float)InterfaceFuncInvoker0< float >::Invoke(&m1975_MI, p0);
		return L_0;
	}
}
 float m1201 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		float L_0 = (float)InterfaceFuncInvoker0< float >::Invoke(&m1976_MI, p0);
		return L_0;
	}
}
 float m1202 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		float L_0 = (float)InterfaceFuncInvoker0< float >::Invoke(&m1976_MI, p0);
		return L_0;
	}
}
 float m1203 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		float L_0 = (float)InterfaceFuncInvoker0< float >::Invoke(&m1977_MI, p0);
		return L_0;
	}
}
 float m1204 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		float L_0 = (float)InterfaceFuncInvoker0< float >::Invoke(&m1978_MI, p0);
		return L_0;
	}
}
// Metadata Definition UnityEngine.UI.LayoutUtility
extern Il2CppType t270_0_0_17;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache0;
FieldInfo t269_f0_FieldInfo = 
{
	"<>f__am$cache0", &t270_0_0_17, &t269_TI, offsetof(t269_SFs, f0), &t269__CustomAttributeCache_U3CU3Ef__am$cache0};
extern Il2CppType t270_0_0_17;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache1;
FieldInfo t269_f1_FieldInfo = 
{
	"<>f__am$cache1", &t270_0_0_17, &t269_TI, offsetof(t269_SFs, f1), &t269__CustomAttributeCache_U3CU3Ef__am$cache1};
extern Il2CppType t270_0_0_17;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache2;
FieldInfo t269_f2_FieldInfo = 
{
	"<>f__am$cache2", &t270_0_0_17, &t269_TI, offsetof(t269_SFs, f2), &t269__CustomAttributeCache_U3CU3Ef__am$cache2};
extern Il2CppType t270_0_0_17;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache3;
FieldInfo t269_f3_FieldInfo = 
{
	"<>f__am$cache3", &t270_0_0_17, &t269_TI, offsetof(t269_SFs, f3), &t269__CustomAttributeCache_U3CU3Ef__am$cache3};
extern Il2CppType t270_0_0_17;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache4;
FieldInfo t269_f4_FieldInfo = 
{
	"<>f__am$cache4", &t270_0_0_17, &t269_TI, offsetof(t269_SFs, f4), &t269__CustomAttributeCache_U3CU3Ef__am$cache4};
extern Il2CppType t270_0_0_17;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache5;
FieldInfo t269_f5_FieldInfo = 
{
	"<>f__am$cache5", &t270_0_0_17, &t269_TI, offsetof(t269_SFs, f5), &t269__CustomAttributeCache_U3CU3Ef__am$cache5};
extern Il2CppType t270_0_0_17;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache6;
FieldInfo t269_f6_FieldInfo = 
{
	"<>f__am$cache6", &t270_0_0_17, &t269_TI, offsetof(t269_SFs, f6), &t269__CustomAttributeCache_U3CU3Ef__am$cache6};
extern Il2CppType t270_0_0_17;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache7;
FieldInfo t269_f7_FieldInfo = 
{
	"<>f__am$cache7", &t270_0_0_17, &t269_TI, offsetof(t269_SFs, f7), &t269__CustomAttributeCache_U3CU3Ef__am$cache7};
static FieldInfo* t269_FIs[] =
{
	&t269_f0_FieldInfo,
	&t269_f1_FieldInfo,
	&t269_f2_FieldInfo,
	&t269_f3_FieldInfo,
	&t269_f4_FieldInfo,
	&t269_f5_FieldInfo,
	&t269_f6_FieldInfo,
	&t269_f7_FieldInfo,
	NULL
};
extern Il2CppType t2_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t269_m1186_ParameterInfos[] = 
{
	{"rect", 0, 134218395, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"axis", 1, 134218396, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1186_MI = 
{
	"GetMinSize", (methodPointerType)&m1186, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29_t44, t269_m1186_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 1186, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t269_m1187_ParameterInfos[] = 
{
	{"rect", 0, 134218397, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"axis", 1, 134218398, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1187_MI = 
{
	"GetPreferredSize", (methodPointerType)&m1187, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29_t44, t269_m1187_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 1187, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t269_m1188_ParameterInfos[] = 
{
	{"rect", 0, 134218399, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"axis", 1, 134218400, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m1188_MI = 
{
	"GetFlexibleSize", (methodPointerType)&m1188, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29_t44, t269_m1188_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 1188, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t269_m1189_ParameterInfos[] = 
{
	{"rect", 0, 134218401, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1189_MI = 
{
	"GetMinWidth", (methodPointerType)&m1189, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1189_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 1189, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t269_m1190_ParameterInfos[] = 
{
	{"rect", 0, 134218402, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1190_MI = 
{
	"GetPreferredWidth", (methodPointerType)&m1190, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1190_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 1190, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t269_m1191_ParameterInfos[] = 
{
	{"rect", 0, 134218403, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1191_MI = 
{
	"GetFlexibleWidth", (methodPointerType)&m1191, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1191_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 1191, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t269_m1192_ParameterInfos[] = 
{
	{"rect", 0, 134218404, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1192_MI = 
{
	"GetMinHeight", (methodPointerType)&m1192, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1192_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 1192, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t269_m1193_ParameterInfos[] = 
{
	{"rect", 0, 134218405, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1193_MI = 
{
	"GetPreferredHeight", (methodPointerType)&m1193, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1193_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 1193, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
static ParameterInfo t269_m1194_ParameterInfos[] = 
{
	{"rect", 0, 134218406, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1194_MI = 
{
	"GetFlexibleHeight", (methodPointerType)&m1194, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1194_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 1194, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t270_0_0_0;
extern Il2CppType t270_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t269_m1195_ParameterInfos[] = 
{
	{"rect", 0, 134218407, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"property", 1, 134218408, &EmptyCustomAttributesCache, &t270_0_0_0},
	{"defaultValue", 2, 134218409, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29_t29_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1195_MI = 
{
	"GetLayoutProperty", (methodPointerType)&m1195, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29_t29_t22, t269_m1195_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 3, false, false, 1195, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern Il2CppType t270_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t271_1_0_2;
static ParameterInfo t269_m1196_ParameterInfos[] = 
{
	{"rect", 0, 134218410, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"property", 1, 134218411, &EmptyCustomAttributesCache, &t270_0_0_0},
	{"defaultValue", 2, 134218412, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"source", 3, 134218413, &EmptyCustomAttributesCache, &t271_1_0_2},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29_t29_t22_t417 (MethodInfo* method, void* obj, void** args);
MethodInfo m1196_MI = 
{
	"GetLayoutProperty", (methodPointerType)&m1196, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29_t29_t22_t417, t269_m1196_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 4, false, false, 1196, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t271_0_0_0;
static ParameterInfo t269_m1197_ParameterInfos[] = 
{
	{"e", 0, 134218414, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t269__CustomAttributeCache_m1197;
MethodInfo m1197_MI = 
{
	"<GetMinWidth>m__E", (methodPointerType)&m1197, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1197_ParameterInfos, &t269__CustomAttributeCache_m1197, 145, 0, 255, 1, false, false, 1197, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t271_0_0_0;
static ParameterInfo t269_m1198_ParameterInfos[] = 
{
	{"e", 0, 134218415, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t269__CustomAttributeCache_m1198;
MethodInfo m1198_MI = 
{
	"<GetPreferredWidth>m__F", (methodPointerType)&m1198, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1198_ParameterInfos, &t269__CustomAttributeCache_m1198, 145, 0, 255, 1, false, false, 1198, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t271_0_0_0;
static ParameterInfo t269_m1199_ParameterInfos[] = 
{
	{"e", 0, 134218416, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t269__CustomAttributeCache_m1199;
MethodInfo m1199_MI = 
{
	"<GetPreferredWidth>m__10", (methodPointerType)&m1199, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1199_ParameterInfos, &t269__CustomAttributeCache_m1199, 145, 0, 255, 1, false, false, 1199, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t271_0_0_0;
static ParameterInfo t269_m1200_ParameterInfos[] = 
{
	{"e", 0, 134218417, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t269__CustomAttributeCache_m1200;
MethodInfo m1200_MI = 
{
	"<GetFlexibleWidth>m__11", (methodPointerType)&m1200, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1200_ParameterInfos, &t269__CustomAttributeCache_m1200, 145, 0, 255, 1, false, false, 1200, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t271_0_0_0;
static ParameterInfo t269_m1201_ParameterInfos[] = 
{
	{"e", 0, 134218418, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t269__CustomAttributeCache_m1201;
MethodInfo m1201_MI = 
{
	"<GetMinHeight>m__12", (methodPointerType)&m1201, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1201_ParameterInfos, &t269__CustomAttributeCache_m1201, 145, 0, 255, 1, false, false, 1201, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t271_0_0_0;
static ParameterInfo t269_m1202_ParameterInfos[] = 
{
	{"e", 0, 134218419, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t269__CustomAttributeCache_m1202;
MethodInfo m1202_MI = 
{
	"<GetPreferredHeight>m__13", (methodPointerType)&m1202, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1202_ParameterInfos, &t269__CustomAttributeCache_m1202, 145, 0, 255, 1, false, false, 1202, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t271_0_0_0;
static ParameterInfo t269_m1203_ParameterInfos[] = 
{
	{"e", 0, 134218420, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t269__CustomAttributeCache_m1203;
MethodInfo m1203_MI = 
{
	"<GetPreferredHeight>m__14", (methodPointerType)&m1203, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1203_ParameterInfos, &t269__CustomAttributeCache_m1203, 145, 0, 255, 1, false, false, 1203, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t271_0_0_0;
static ParameterInfo t269_m1204_ParameterInfos[] = 
{
	{"e", 0, 134218421, &EmptyCustomAttributesCache, &t271_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t269__CustomAttributeCache_m1204;
MethodInfo m1204_MI = 
{
	"<GetFlexibleHeight>m__15", (methodPointerType)&m1204, &t269_TI, &t22_0_0_0, RuntimeInvoker_t22_t29, t269_m1204_ParameterInfos, &t269__CustomAttributeCache_m1204, 145, 0, 255, 1, false, false, 1204, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t269_MIs[] =
{
	&m1186_MI,
	&m1187_MI,
	&m1188_MI,
	&m1189_MI,
	&m1190_MI,
	&m1191_MI,
	&m1192_MI,
	&m1193_MI,
	&m1194_MI,
	&m1195_MI,
	&m1196_MI,
	&m1197_MI,
	&m1198_MI,
	&m1199_MI,
	&m1200_MI,
	&m1201_MI,
	&m1202_MI,
	&m1203_MI,
	&m1204_MI,
	NULL
};
static MethodInfo* t269_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache0(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache1(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache2(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache3(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache4(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache5(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache6(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache7(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_m1197(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_m1198(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_m1199(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_m1200(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_m1201(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_m1202(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_m1203(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t269_CustomAttributesCacheGenerator_m1204(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache0 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache0
};
CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache1 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache1
};
CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache2 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache2
};
CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache3 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache3
};
CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache4 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache4
};
CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache5 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache5
};
CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache6 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache6
};
CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache7 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_U3CU3Ef__am$cache7
};
CustomAttributesCache t269__CustomAttributeCache_m1197 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_m1197
};
CustomAttributesCache t269__CustomAttributeCache_m1198 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_m1198
};
CustomAttributesCache t269__CustomAttributeCache_m1199 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_m1199
};
CustomAttributesCache t269__CustomAttributeCache_m1200 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_m1200
};
CustomAttributesCache t269__CustomAttributeCache_m1201 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_m1201
};
CustomAttributesCache t269__CustomAttributeCache_m1202 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_m1202
};
CustomAttributesCache t269__CustomAttributeCache_m1203 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_m1203
};
CustomAttributesCache t269__CustomAttributeCache_m1204 = {
1,
NULL,
&t269_CustomAttributesCacheGenerator_m1204
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t269_0_0_0;
extern Il2CppType t269_1_0_0;
struct t269;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache0;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache1;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache2;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache3;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache4;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache5;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache6;
extern CustomAttributesCache t269__CustomAttributeCache_U3CU3Ef__am$cache7;
extern CustomAttributesCache t269__CustomAttributeCache_m1197;
extern CustomAttributesCache t269__CustomAttributeCache_m1198;
extern CustomAttributesCache t269__CustomAttributeCache_m1199;
extern CustomAttributesCache t269__CustomAttributeCache_m1200;
extern CustomAttributesCache t269__CustomAttributeCache_m1201;
extern CustomAttributesCache t269__CustomAttributeCache_m1202;
extern CustomAttributesCache t269__CustomAttributeCache_m1203;
extern CustomAttributesCache t269__CustomAttributeCache_m1204;
TypeInfo t269_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "LayoutUtility", "UnityEngine.UI", t269_MIs, NULL, t269_FIs, NULL, &t29_TI, NULL, NULL, &t269_TI, NULL, t269_VT, &EmptyCustomAttributesCache, &t269_TI, &t269_0_0_0, &t269_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t269), 0, -1, sizeof(t269_SFs), 0, -1, 1048961, 0, false, false, false, false, false, false, false, false, false, false, false, false, 19, 0, 8, 0, 0, 4, 0, 0};
#include "t272.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t272_TI;
#include "t272MD.h"



extern MethodInfo m1205_MI;
 void m1205 (t272 * __this, MethodInfo* method){
	{
		m1104(__this, &m1104_MI);
		return;
	}
}
extern MethodInfo m1206_MI;
 void m1206 (t272 * __this, MethodInfo* method){
	{
		m1144(__this, &m1144_MI);
		m1111(__this, 0, 1, &m1111_MI);
		return;
	}
}
extern MethodInfo m1207_MI;
 void m1207 (t272 * __this, MethodInfo* method){
	{
		m1111(__this, 1, 1, &m1111_MI);
		return;
	}
}
extern MethodInfo m1208_MI;
 void m1208 (t272 * __this, MethodInfo* method){
	{
		m1112(__this, 0, 1, &m1112_MI);
		return;
	}
}
extern MethodInfo m1209_MI;
 void m1209 (t272 * __this, MethodInfo* method){
	{
		m1112(__this, 1, 1, &m1112_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.VerticalLayoutGroup
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1205_MI = 
{
	".ctor", (methodPointerType)&m1205, &t272_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1205, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1206_MI = 
{
	"CalculateLayoutInputHorizontal", (methodPointerType)&m1206, &t272_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 26, 0, false, false, 1206, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1207_MI = 
{
	"CalculateLayoutInputVertical", (methodPointerType)&m1207, &t272_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 27, 0, false, false, 1207, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1208_MI = 
{
	"SetLayoutHorizontal", (methodPointerType)&m1208, &t272_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 35, 0, false, false, 1208, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1209_MI = 
{
	"SetLayoutVertical", (methodPointerType)&m1209, &t272_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 198, 0, 36, 0, false, false, 1209, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t272_MIs[] =
{
	&m1205_MI,
	&m1206_MI,
	&m1207_MI,
	&m1208_MI,
	&m1209_MI,
	NULL
};
static MethodInfo* t272_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1152_MI,
	&m171_MI,
	&m1153_MI,
	&m173_MI,
	&m174_MI,
	&m1162_MI,
	&m176_MI,
	&m177_MI,
	&m1154_MI,
	&m179_MI,
	&m1206_MI,
	&m1207_MI,
	&m1145_MI,
	&m1146_MI,
	&m1147_MI,
	&m1148_MI,
	&m1149_MI,
	&m1150_MI,
	&m1151_MI,
	&m1208_MI,
	&m1209_MI,
	&m1206_MI,
	&m1207_MI,
	&m1145_MI,
	&m1146_MI,
	&m1147_MI,
	&m1148_MI,
	&m1149_MI,
	&m1150_MI,
	&m1151_MI,
	&m1208_MI,
	&m1209_MI,
	&m1163_MI,
};
static Il2CppInterfaceOffsetPair t272_IOs[] = 
{
	{ &t271_TI, 15},
	{ &t409_TI, 24},
	{ &t411_TI, 26},
};
void t272_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("Layout/Vertical Layout Group"), 151, &m1519_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t272__CustomAttributeCache = {
1,
NULL,
&t272_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t272_0_0_0;
extern Il2CppType t272_1_0_0;
struct t272;
extern CustomAttributesCache t272__CustomAttributeCache;
TypeInfo t272_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "VerticalLayoutGroup", "UnityEngine.UI", t272_MIs, NULL, NULL, NULL, &t261_TI, NULL, NULL, &t272_TI, NULL, t272_VT, &t272__CustomAttributeCache, &t272_TI, &t272_0_0_0, &t272_1_0_0, t272_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t272), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 0, 0, 0, 0, 38, 0, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t354_TI;



// Metadata Definition UnityEngine.UI.IMaterialModifier
extern Il2CppType t156_0_0_0;
static ParameterInfo t354_m1557_ParameterInfos[] = 
{
	{"baseMaterial", 0, 134218422, &EmptyCustomAttributesCache, &t156_0_0_0},
};
extern Il2CppType t156_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1557_MI = 
{
	"GetModifiedMaterial", NULL, &t354_TI, &t156_0_0_0, RuntimeInvoker_t29_t29, t354_m1557_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, false, 1210, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t354_MIs[] =
{
	&m1557_MI,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t354_0_0_0;
extern Il2CppType t354_1_0_0;
struct t354;
TypeInfo t354_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IMaterialModifier", "UnityEngine.UI", t354_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t354_TI, NULL, NULL, &EmptyCustomAttributesCache, &t354_TI, &t354_0_0_0, &t354_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t273.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t273_TI;
#include "t273MD.h"

#include "t159.h"
#include "t159MD.h"
extern MethodInfo m1211_MI;
extern MethodInfo m448_MI;
extern MethodInfo m1217_MI;
extern MethodInfo m456_MI;
extern MethodInfo m2005_MI;
extern MethodInfo m2006_MI;
extern MethodInfo m1692_MI;
extern MethodInfo m1218_MI;
extern MethodInfo m1214_MI;
extern MethodInfo m1295_MI;
struct t28;
struct t28;
 void m2007_gshared (t28 * __this, t294 * p0, MethodInfo* method);
#define m2007(__this, p0, method) (void)m2007_gshared((t28 *)__this, (t294 *)p0, method)
#define m2006(__this, p0, method) (void)m2007_gshared((t28 *)__this, (t294 *)p0, method)


extern MethodInfo m1210_MI;
 void m1210 (t273 * __this, MethodInfo* method){
	{
		__this->f2 = 1;
		m168(__this, &m168_MI);
		return;
	}
}
 t155 * m1211 (t273 * __this, MethodInfo* method){
	{
		t155 * L_0 = (__this->f4);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		t155 * L_2 = m1875(__this, &m1875_MI);
		__this->f4 = L_2;
	}

IL_001d:
	{
		t155 * L_3 = (__this->f4);
		return L_3;
	}
}
extern MethodInfo m1212_MI;
 bool m1212 (t273 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f2);
		return L_0;
	}
}
extern MethodInfo m1213_MI;
 void m1213 (t273 * __this, bool p0, MethodInfo* method){
	{
		bool L_0 = (__this->f2);
		if ((((uint32_t)L_0) != ((uint32_t)p0)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		__this->f2 = p0;
		t155 * L_1 = m1211(__this, &m1211_MI);
		bool L_2 = m1300(NULL, L_1, (t41 *)NULL, &m1300_MI);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		t155 * L_3 = m1211(__this, &m1211_MI);
		VirtActionInvoker0::Invoke(&m448_MI, L_3);
	}

IL_0030:
	{
		return;
	}
}
 t2 * m1214 (t273 * __this, MethodInfo* method){
	t2 * V_0 = {0};
	t2 * G_B2_0 = {0};
	t2 * G_B1_0 = {0};
	{
		t2 * L_0 = (__this->f5);
		t2 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001c;
		}
	}
	{
		t2 * L_2 = m60(__this, &m60_MI);
		t2 * L_3 = L_2;
		V_0 = L_3;
		__this->f5 = L_3;
		G_B2_0 = V_0;
	}

IL_001c:
	{
		return G_B2_0;
	}
}
extern MethodInfo m1215_MI;
 bool m1215 (t273 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		t155 * L_1 = m1211(__this, &m1211_MI);
		bool L_2 = m1300(NULL, L_1, (t41 *)NULL, &m1300_MI);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		return G_B3_0;
	}
}
extern MethodInfo m1216_MI;
 void m1216 (t273 * __this, MethodInfo* method){
	{
		m1217(__this, &m1217_MI);
		return;
	}
}
 void m1217 (t273 * __this, MethodInfo* method){
	t268 * V_0 = {0};
	int32_t V_1 = 0;
	t29 * V_2 = {0};
	{
		t155 * L_0 = m1211(__this, &m1211_MI);
		bool L_1 = m1300(NULL, L_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		t155 * L_2 = m1211(__this, &m1211_MI);
		t159 * L_3 = m456(L_2, &m456_MI);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		m2005(L_3, L_4, &m2005_MI);
		t155 * L_5 = m1211(__this, &m1211_MI);
		VirtActionInvoker0::Invoke(&m448_MI, L_5);
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t279_TI));
		t268 * L_6 = m1228(NULL, &m1228_MI);
		V_0 = L_6;
		m2006(__this, V_0, &m2006_MI);
		V_1 = 0;
		goto IL_0096;
	}

IL_0046:
	{
		t28 * L_7 = (t28 *)VirtFuncInvoker1< t28 *, int32_t >::Invoke(&m1556_MI, V_0, V_1);
		bool L_8 = m1297(NULL, L_7, (t41 *)NULL, &m1297_MI);
		if (L_8)
		{
			goto IL_0074;
		}
	}
	{
		t28 * L_9 = (t28 *)VirtFuncInvoker1< t28 *, int32_t >::Invoke(&m1556_MI, V_0, V_1);
		t16 * L_10 = m1392(L_9, &m1392_MI);
		t16 * L_11 = m1392(__this, &m1392_MI);
		bool L_12 = m1297(NULL, L_10, L_11, &m1297_MI);
		if (!L_12)
		{
			goto IL_0079;
		}
	}

IL_0074:
	{
		goto IL_0092;
	}

IL_0079:
	{
		t28 * L_13 = (t28 *)VirtFuncInvoker1< t28 *, int32_t >::Invoke(&m1556_MI, V_0, V_1);
		V_2 = ((t29 *)IsInst(L_13, InitializedTypeInfo(&t369_TI)));
		if (!V_2)
		{
			goto IL_0092;
		}
	}
	{
		InterfaceActionInvoker0::Invoke(&m1692_MI, V_2);
	}

IL_0092:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0096:
	{
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1558_MI, V_0);
		if ((((int32_t)V_1) < ((int32_t)L_14)))
		{
			goto IL_0046;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t279_TI));
		m1229(NULL, V_0, &m1229_MI);
		return;
	}
}
 void m1218 (t273 * __this, MethodInfo* method){
	{
		t156 * L_0 = (__this->f3);
		bool L_1 = m1300(NULL, L_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		t156 * L_2 = (__this->f3);
		m707(NULL, L_2, &m707_MI);
	}

IL_001c:
	{
		__this->f3 = (t156 *)NULL;
		return;
	}
}
extern MethodInfo m1219_MI;
 void m1219 (t273 * __this, MethodInfo* method){
	{
		m170(__this, &m170_MI);
		m1217(__this, &m1217_MI);
		return;
	}
}
extern MethodInfo m1220_MI;
 void m1220 (t273 * __this, MethodInfo* method){
	{
		m172(__this, &m172_MI);
		m1218(__this, &m1218_MI);
		m1217(__this, &m1217_MI);
		return;
	}
}
extern MethodInfo m1221_MI;
 bool m1221 (t273 * __this, t17  p0, t24 * p1, MethodInfo* method){
	{
		t2 * L_0 = m1214(__this, &m1214_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t358_TI));
		bool L_1 = m1628(NULL, L_0, p0, p1, &m1628_MI);
		return L_1;
	}
}
extern MethodInfo m1222_MI;
 t156 * m1222 (t273 * __this, t156 * p0, MethodInfo* method){
	t156 * V_0 = {0};
	t7* G_B5_0 = {0};
	t156 * G_B5_1 = {0};
	t7* G_B4_0 = {0};
	t156 * G_B4_1 = {0};
	int32_t G_B6_0 = 0;
	t7* G_B6_1 = {0};
	t156 * G_B6_2 = {0};
	{
		m1218(__this, &m1218_MI);
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		return p0;
	}

IL_0013:
	{
		t156 * L_1 = (t156 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t156_TI));
		m1907(L_1, p0, &m1907_MI);
		V_0 = L_1;
		t7* L_2 = m1385(p0, &m1385_MI);
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_3 = m1685(NULL, (t7*) &_stringLiteral64, L_2, (t7*) &_stringLiteral60, &m1685_MI);
		m1908(V_0, L_3, &m1908_MI);
		m1778(V_0, ((int32_t)61), &m1778_MI);
		__this->f3 = V_0;
		t156 * L_4 = (__this->f3);
		bool L_5 = m1903(L_4, (t7*) &_stringLiteral65, &m1903_MI);
		if (!L_5)
		{
			goto IL_0081;
		}
	}
	{
		t156 * L_6 = (__this->f3);
		bool L_7 = (__this->f2);
		G_B4_0 = (t7*) &_stringLiteral65;
		G_B4_1 = L_6;
		if (!L_7)
		{
			G_B5_0 = (t7*) &_stringLiteral65;
			G_B5_1 = L_6;
			goto IL_0076;
		}
	}
	{
		G_B6_0 = ((int32_t)15);
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_0077;
	}

IL_0076:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_0077:
	{
		m1909(G_B6_2, G_B6_1, G_B6_0, &m1909_MI);
		goto IL_0097;
	}

IL_0081:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&t7_TI));
		t7* L_8 = m1295(NULL, (t7*) &_stringLiteral56, p0, (t7*) &_stringLiteral66, &m1295_MI);
		m1904(NULL, L_8, p0, &m1904_MI);
	}

IL_0097:
	{
		t156 * L_9 = (__this->f3);
		return L_9;
	}
}
// Metadata Definition UnityEngine.UI.Mask
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t273__CustomAttributeCache_m_ShowMaskGraphic;
FieldInfo t273_f2_FieldInfo = 
{
	"m_ShowMaskGraphic", &t40_0_0_1, &t273_TI, offsetof(t273, f2), &t273__CustomAttributeCache_m_ShowMaskGraphic};
extern Il2CppType t156_0_0_1;
FieldInfo t273_f3_FieldInfo = 
{
	"m_RenderMaterial", &t156_0_0_1, &t273_TI, offsetof(t273, f3), &EmptyCustomAttributesCache};
extern Il2CppType t155_0_0_1;
FieldInfo t273_f4_FieldInfo = 
{
	"m_Graphic", &t155_0_0_1, &t273_TI, offsetof(t273, f4), &EmptyCustomAttributesCache};
extern Il2CppType t2_0_0_1;
FieldInfo t273_f5_FieldInfo = 
{
	"m_RectTransform", &t2_0_0_1, &t273_TI, offsetof(t273, f5), &EmptyCustomAttributesCache};
static FieldInfo* t273_FIs[] =
{
	&t273_f2_FieldInfo,
	&t273_f3_FieldInfo,
	&t273_f4_FieldInfo,
	&t273_f5_FieldInfo,
	NULL
};
static PropertyInfo t273____graphic_PropertyInfo = 
{
	&t273_TI, "graphic", &m1211_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t273____showMaskGraphic_PropertyInfo = 
{
	&t273_TI, "showMaskGraphic", &m1212_MI, &m1213_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t273____rectTransform_PropertyInfo = 
{
	&t273_TI, "rectTransform", &m1214_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t273_PIs[] =
{
	&t273____graphic_PropertyInfo,
	&t273____showMaskGraphic_PropertyInfo,
	&t273____rectTransform_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1210_MI = 
{
	".ctor", (methodPointerType)&m1210, &t273_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1211, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1211_MI = 
{
	"get_graphic", (methodPointerType)&m1211, &t273_TI, &t155_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2177, 0, 255, 0, false, false, 1212, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1212_MI = 
{
	"get_showMaskGraphic", (methodPointerType)&m1212, &t273_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1213, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t273_m1213_ParameterInfos[] = 
{
	{"value", 0, 134218423, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1213_MI = 
{
	"set_showMaskGraphic", (methodPointerType)&m1213, &t273_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t273_m1213_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1214, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1214_MI = 
{
	"get_rectTransform", (methodPointerType)&m1214, &t273_TI, &t2_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1215, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1215_MI = 
{
	"MaskEnabled", (methodPointerType)&m1215, &t273_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 454, 0, 19, 0, false, false, 1216, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1216_MI = 
{
	"OnSiblingGraphicEnabledDisabled", (methodPointerType)&m1216, &t273_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 454, 0, 20, 0, false, false, 1217, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1217_MI = 
{
	"NotifyMaskStateChanged", (methodPointerType)&m1217, &t273_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 1218, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1218_MI = 
{
	"ClearCachedMaterial", (methodPointerType)&m1218, &t273_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, false, 1219, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1219_MI = 
{
	"OnEnable", (methodPointerType)&m1219, &t273_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 5, 0, false, false, 1220, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1220_MI = 
{
	"OnDisable", (methodPointerType)&m1220, &t273_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 7, 0, false, false, 1221, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern Il2CppType t24_0_0_0;
static ParameterInfo t273_m1221_ParameterInfos[] = 
{
	{"sp", 0, 134218424, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"eventCamera", 1, 134218425, &EmptyCustomAttributesCache, &t24_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t17_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1221_MI = 
{
	"IsRaycastLocationValid", (methodPointerType)&m1221, &t273_TI, &t40_0_0_0, RuntimeInvoker_t40_t17_t29, t273_m1221_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 21, 2, false, false, 1222, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t156_0_0_0;
static ParameterInfo t273_m1222_ParameterInfos[] = 
{
	{"baseMaterial", 0, 134218426, &EmptyCustomAttributesCache, &t156_0_0_0},
};
extern Il2CppType t156_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1222_MI = 
{
	"GetModifiedMaterial", (methodPointerType)&m1222, &t273_TI, &t156_0_0_0, RuntimeInvoker_t29_t29, t273_m1222_ParameterInfos, &EmptyCustomAttributesCache, 454, 0, 22, 1, false, false, 1223, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t273_MIs[] =
{
	&m1210_MI,
	&m1211_MI,
	&m1212_MI,
	&m1213_MI,
	&m1214_MI,
	&m1215_MI,
	&m1216_MI,
	&m1217_MI,
	&m1218_MI,
	&m1219_MI,
	&m1220_MI,
	&m1221_MI,
	&m1222_MI,
	NULL
};
static MethodInfo* t273_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1219_MI,
	&m171_MI,
	&m1220_MI,
	&m173_MI,
	&m174_MI,
	&m175_MI,
	&m176_MI,
	&m177_MI,
	&m178_MI,
	&m179_MI,
	&m1216_MI,
	&m1215_MI,
	&m1221_MI,
	&m1222_MI,
	&m1215_MI,
	&m1216_MI,
	&m1221_MI,
	&m1222_MI,
};
extern TypeInfo t355_TI;
extern TypeInfo t370_TI;
extern TypeInfo t357_TI;
static TypeInfo* t273_ITIs[] = 
{
	&t355_TI,
	&t370_TI,
	&t357_TI,
	&t354_TI,
};
static Il2CppInterfaceOffsetPair t273_IOs[] = 
{
	{ &t355_TI, 15},
	{ &t370_TI, 16},
	{ &t357_TI, 17},
	{ &t354_TI, 18},
};
void t273_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t360 * tmp;
		tmp = (t360 *)il2cpp_codegen_object_new (&t360_TI);
		m1604(tmp, &m1604_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("UI/Mask"), 13, &m1519_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void t273_CustomAttributesCacheGenerator_m_ShowMaskGraphic(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t299 * tmp;
		tmp = (t299 *)il2cpp_codegen_object_new (&t299_TI);
		m1318(tmp, il2cpp_codegen_string_new_wrapper("m_ShowGraphic"), &m1318_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t273__CustomAttributeCache = {
2,
NULL,
&t273_CustomAttributesCacheGenerator
};
CustomAttributesCache t273__CustomAttributeCache_m_ShowMaskGraphic = {
2,
NULL,
&t273_CustomAttributesCacheGenerator_m_ShowMaskGraphic
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t273_0_0_0;
extern Il2CppType t273_1_0_0;
struct t273;
extern CustomAttributesCache t273__CustomAttributeCache;
extern CustomAttributesCache t273__CustomAttributeCache_m_ShowMaskGraphic;
TypeInfo t273_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Mask", "UnityEngine.UI", t273_MIs, t273_PIs, t273_FIs, NULL, &t55_TI, NULL, NULL, &t273_TI, t273_ITIs, t273_VT, &t273__CustomAttributeCache, &t273_TI, &t273_0_0_0, &t273_1_0_0, t273_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t273), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 13, 3, 4, 0, 0, 23, 4, 4};
#include "t274.h"
extern Il2CppGenericContainer t274_IGC;
extern TypeInfo t274_gp_T_0_TI;
Il2CppGenericParamFull t274_gp_T_0_TI_GenericParamFull = { { &t274_IGC, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* t274_IGPA[1] = 
{
	&t274_gp_T_0_TI_GenericParamFull,
};
extern TypeInfo t274_TI;
Il2CppGenericContainer t274_IGC = { { NULL, NULL }, NULL, &t274_TI, 1, 0, t274_IGPA };
extern Il2CppType t21_0_0_0;
MethodInfo m2008_MI = 
{
	".ctor", NULL, &t274_TI, &t21_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1224, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t136_0_0_0;
MethodInfo m2009_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", NULL, &t274_TI, &t136_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 481, 0, 17, 0, false, false, 1225, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t274_gp_0_0_0_0;
extern Il2CppType t274_gp_0_0_0_0;
static ParameterInfo t274_m2010_ParameterInfos[] = 
{
	{"item", 0, 134218427, &EmptyCustomAttributesCache, &t274_gp_0_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2010_MI = 
{
	"Add", NULL, &t274_TI, &t21_0_0_0, NULL, t274_m2010_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 11, 1, false, false, 1226, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t274_gp_0_0_0_0;
static ParameterInfo t274_m2011_ParameterInfos[] = 
{
	{"item", 0, 134218428, &EmptyCustomAttributesCache, &t274_gp_0_0_0_0},
};
extern Il2CppType t40_0_0_0;
MethodInfo m2011_MI = 
{
	"Remove", NULL, &t274_TI, &t40_0_0_0, NULL, t274_m2011_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 15, 1, false, false, 1227, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t419_0_0_0;
MethodInfo m2012_MI = 
{
	"GetEnumerator", NULL, &t274_TI, &t419_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 486, 0, 16, 0, false, false, 1228, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
MethodInfo m2013_MI = 
{
	"Clear", NULL, &t274_TI, &t21_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 486, 0, 12, 0, false, false, 1229, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t274_gp_0_0_0_0;
static ParameterInfo t274_m2014_ParameterInfos[] = 
{
	{"item", 0, 134218429, &EmptyCustomAttributesCache, &t274_gp_0_0_0_0},
};
extern Il2CppType t40_0_0_0;
MethodInfo m2014_MI = 
{
	"Contains", NULL, &t274_TI, &t40_0_0_0, NULL, t274_m2014_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 13, 1, false, false, 1230, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t420_0_0_0;
extern Il2CppType t420_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t274_m2015_ParameterInfos[] = 
{
	{"array", 0, 134218430, &EmptyCustomAttributesCache, &t420_0_0_0},
	{"arrayIndex", 1, 134218431, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2015_MI = 
{
	"CopyTo", NULL, &t274_TI, &t21_0_0_0, NULL, t274_m2015_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, false, 1231, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
MethodInfo m2016_MI = 
{
	"get_Count", NULL, &t274_TI, &t44_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, false, 1232, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
MethodInfo m2017_MI = 
{
	"get_IsReadOnly", NULL, &t274_TI, &t40_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 2534, 0, 10, 0, false, false, 1233, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t274_gp_0_0_0_0;
static ParameterInfo t274_m2018_ParameterInfos[] = 
{
	{"item", 0, 134218432, &EmptyCustomAttributesCache, &t274_gp_0_0_0_0},
};
extern Il2CppType t44_0_0_0;
MethodInfo m2018_MI = 
{
	"IndexOf", NULL, &t274_TI, &t44_0_0_0, NULL, t274_m2018_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 4, 1, false, false, 1234, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t274_gp_0_0_0_0;
static ParameterInfo t274_m2019_ParameterInfos[] = 
{
	{"index", 0, 134218433, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218434, &EmptyCustomAttributesCache, &t274_gp_0_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2019_MI = 
{
	"Insert", NULL, &t274_TI, &t21_0_0_0, NULL, t274_m2019_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 5, 2, false, false, 1235, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t274_m2020_ParameterInfos[] = 
{
	{"index", 0, 134218435, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2020_MI = 
{
	"RemoveAt", NULL, &t274_TI, &t21_0_0_0, NULL, t274_m2020_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 6, 1, false, false, 1236, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t274_m2021_ParameterInfos[] = 
{
	{"index", 0, 134218436, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t274_gp_0_0_0_0;
MethodInfo m2021_MI = 
{
	"get_Item", NULL, &t274_TI, &t274_gp_0_0_0_0, NULL, t274_m2021_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 7, 1, false, false, 1237, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern Il2CppType t274_gp_0_0_0_0;
static ParameterInfo t274_m2022_ParameterInfos[] = 
{
	{"index", 0, 134218437, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134218438, &EmptyCustomAttributesCache, &t274_gp_0_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2022_MI = 
{
	"set_Item", NULL, &t274_TI, &t21_0_0_0, NULL, t274_m2022_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 8, 2, false, false, 1238, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t421_0_0_0;
extern Il2CppType t421_0_0_0;
static ParameterInfo t274_m2023_ParameterInfos[] = 
{
	{"match", 0, 134218439, &EmptyCustomAttributesCache, &t421_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2023_MI = 
{
	"RemoveAll", NULL, &t274_TI, &t21_0_0_0, NULL, t274_m2023_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1239, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t422_0_0_0;
extern Il2CppType t422_0_0_0;
static ParameterInfo t274_m2024_ParameterInfos[] = 
{
	{"sortLayoutFunction", 0, 134218440, &EmptyCustomAttributesCache, &t422_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2024_MI = 
{
	"Sort", NULL, &t274_TI, &t21_0_0_0, NULL, t274_m2024_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1240, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t274_MIs[] =
{
	&m2008_MI,
	&m2009_MI,
	&m2010_MI,
	&m2011_MI,
	&m2012_MI,
	&m2013_MI,
	&m2014_MI,
	&m2015_MI,
	&m2016_MI,
	&m2017_MI,
	&m2018_MI,
	&m2019_MI,
	&m2020_MI,
	&m2021_MI,
	&m2022_MI,
	&m2023_MI,
	&m2024_MI,
	NULL
};
extern MethodInfo m2016_MI;
static PropertyInfo t274____Count_PropertyInfo = 
{
	&t274_TI, "Count", &m2016_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m2017_MI;
static PropertyInfo t274____IsReadOnly_PropertyInfo = 
{
	&t274_TI, "IsReadOnly", &m2017_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m2021_MI;
extern MethodInfo m2022_MI;
static PropertyInfo t274____Item_PropertyInfo = 
{
	&t274_TI, "Item", &m2021_MI, &m2022_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t274_PIs[] =
{
	&t274____Count_PropertyInfo,
	&t274____IsReadOnly_PropertyInfo,
	&t274____Item_PropertyInfo,
	NULL
};
extern Il2CppType t423_0_0_33;
FieldInfo t274_f0_FieldInfo = 
{
	"m_List", &t423_0_0_33, &t274_TI, 0, &EmptyCustomAttributesCache};
extern Il2CppType t424_0_0_1;
FieldInfo t274_f1_FieldInfo = 
{
	"m_Dictionary", &t424_0_0_1, &t274_TI, 0, &EmptyCustomAttributesCache};
static FieldInfo* t274_FIs[] =
{
	&t274_f0_FieldInfo,
	&t274_f1_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t274_0_0_0;
extern Il2CppType t274_1_0_0;
struct t274;
TypeInfo t274_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IndexedSet`1", "UnityEngine.UI.Collections", t274_MIs, t274_PIs, t274_FIs, NULL, NULL, NULL, NULL, &t274_TI, NULL, NULL, NULL, NULL, &t274_0_0_0, &t274_1_0_0, NULL, NULL, &t274_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 17, 3, 2, 0, 0, 0, 0, 0};
extern TypeInfo t425_TI;
#include "t425.h"
#include "t425MD.h"
extern MethodInfo m2025_MI;
void t274_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t425 * tmp;
		tmp = (t425 *)il2cpp_codegen_object_new (&t425_TI);
		m2025(tmp, il2cpp_codegen_string_new_wrapper("Item"), &m2025_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t274__CustomAttributeCache = {
1,
NULL,
&t274_CustomAttributesCacheGenerator
};
#include "t275.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t275_TI;
#include "t275MD.h"

#include "t277.h"
#include "t278.h"
#include "t276.h"
extern TypeInfo t277_TI;
extern TypeInfo t276_TI;
extern TypeInfo t278_TI;
#include "t277MD.h"
#include "t276MD.h"
#include "t278MD.h"
extern MethodInfo m1226_MI;
extern MethodInfo m2026_MI;
extern MethodInfo m2027_MI;
extern MethodInfo m2028_MI;
extern MethodInfo m2029_MI;
extern MethodInfo m2030_MI;


extern MethodInfo m1223_MI;
 void m1223 (t29 * __this, MethodInfo* method){
	t29 * G_B2_0 = {0};
	t29 * G_B1_0 = {0};
	{
		G_B1_0 = NULL;
		if ((((t275_SFs*)InitializedTypeInfo(&t275_TI)->static_fields)->f1))
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		t35 L_0 = { &m1226_MI };
		t277 * L_1 = (t277 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t277_TI));
		m2026(L_1, NULL, L_0, &m2026_MI);
		((t275_SFs*)InitializedTypeInfo(&t275_TI)->static_fields)->f1 = L_1;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		t276 * L_2 = (t276 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t276_TI));
		m2027(L_2, (t277 *)G_B2_0, (((t275_SFs*)InitializedTypeInfo(&t275_TI)->static_fields)->f1), &m2027_MI);
		((t275_SFs*)InitializedTypeInfo(&t275_TI)->static_fields)->f0 = L_2;
		return;
	}
}
extern MethodInfo m1224_MI;
 t278 * m1224 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t275_TI));
		t278 * L_0 = m2028((((t275_SFs*)InitializedTypeInfo(&t275_TI)->static_fields)->f0), &m2028_MI);
		return L_0;
	}
}
extern MethodInfo m1225_MI;
 void m1225 (t29 * __this, t278 * p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t275_TI));
		m2029((((t275_SFs*)InitializedTypeInfo(&t275_TI)->static_fields)->f0), p0, &m2029_MI);
		return;
	}
}
 void m1226 (t29 * __this, t278 * p0, MethodInfo* method){
	{
		VirtActionInvoker0::Invoke(&m2030_MI, p0);
		return;
	}
}
// Metadata Definition UnityEngine.UI.CanvasListPool
extern Il2CppType t276_0_0_49;
FieldInfo t275_f0_FieldInfo = 
{
	"s_CanvasListPool", &t276_0_0_49, &t275_TI, offsetof(t275_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t277_0_0_17;
extern CustomAttributesCache t275__CustomAttributeCache_U3CU3Ef__am$cache1;
FieldInfo t275_f1_FieldInfo = 
{
	"<>f__am$cache1", &t277_0_0_17, &t275_TI, offsetof(t275_SFs, f1), &t275__CustomAttributeCache_U3CU3Ef__am$cache1};
static FieldInfo* t275_FIs[] =
{
	&t275_f0_FieldInfo,
	&t275_f1_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1223_MI = 
{
	".cctor", (methodPointerType)&m1223, &t275_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 1241, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t278_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1224_MI = 
{
	"Get", (methodPointerType)&m1224, &t275_TI, &t278_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 150, 0, 255, 0, false, false, 1242, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t278_0_0_0;
extern Il2CppType t278_0_0_0;
static ParameterInfo t275_m1225_ParameterInfos[] = 
{
	{"toRelease", 0, 134218441, &EmptyCustomAttributesCache, &t278_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1225_MI = 
{
	"Release", (methodPointerType)&m1225, &t275_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t275_m1225_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 1243, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t278_0_0_0;
static ParameterInfo t275_m1226_ParameterInfos[] = 
{
	{"l", 0, 134218442, &EmptyCustomAttributesCache, &t278_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t275__CustomAttributeCache_m1226;
MethodInfo m1226_MI = 
{
	"<s_CanvasListPool>m__16", (methodPointerType)&m1226, &t275_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t275_m1226_ParameterInfos, &t275__CustomAttributeCache_m1226, 145, 0, 255, 1, false, false, 1244, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t275_MIs[] =
{
	&m1223_MI,
	&m1224_MI,
	&m1225_MI,
	&m1226_MI,
	NULL
};
static MethodInfo* t275_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t275_CustomAttributesCacheGenerator_U3CU3Ef__am$cache1(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t275_CustomAttributesCacheGenerator_m1226(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t275__CustomAttributeCache_U3CU3Ef__am$cache1 = {
1,
NULL,
&t275_CustomAttributesCacheGenerator_U3CU3Ef__am$cache1
};
CustomAttributesCache t275__CustomAttributeCache_m1226 = {
1,
NULL,
&t275_CustomAttributesCacheGenerator_m1226
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t275_0_0_0;
extern Il2CppType t275_1_0_0;
struct t275;
extern CustomAttributesCache t275__CustomAttributeCache_U3CU3Ef__am$cache1;
extern CustomAttributesCache t275__CustomAttributeCache_m1226;
TypeInfo t275_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "CanvasListPool", "UnityEngine.UI", t275_MIs, NULL, t275_FIs, NULL, &t29_TI, NULL, NULL, &t275_TI, NULL, t275_VT, &EmptyCustomAttributesCache, &t275_TI, &t275_0_0_0, &t275_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t275), 0, -1, sizeof(t275_SFs), 0, -1, 1048960, 0, false, false, false, false, false, false, false, false, false, true, false, false, 4, 0, 2, 0, 0, 4, 0, 0};
#include "t279.h"
#ifndef _MSC_VER
#else
#endif

#include "t281.h"
#include "t280.h"
extern TypeInfo t281_TI;
extern TypeInfo t280_TI;
#include "t281MD.h"
#include "t280MD.h"
extern MethodInfo m1230_MI;
extern MethodInfo m2031_MI;
extern MethodInfo m2032_MI;
extern MethodInfo m2033_MI;
extern MethodInfo m2034_MI;
extern MethodInfo m2035_MI;


extern MethodInfo m1227_MI;
 void m1227 (t29 * __this, MethodInfo* method){
	t29 * G_B2_0 = {0};
	t29 * G_B1_0 = {0};
	{
		G_B1_0 = NULL;
		if ((((t279_SFs*)InitializedTypeInfo(&t279_TI)->static_fields)->f1))
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		t35 L_0 = { &m1230_MI };
		t281 * L_1 = (t281 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t281_TI));
		m2031(L_1, NULL, L_0, &m2031_MI);
		((t279_SFs*)InitializedTypeInfo(&t279_TI)->static_fields)->f1 = L_1;
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		t280 * L_2 = (t280 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t280_TI));
		m2032(L_2, (t281 *)G_B2_0, (((t279_SFs*)InitializedTypeInfo(&t279_TI)->static_fields)->f1), &m2032_MI);
		((t279_SFs*)InitializedTypeInfo(&t279_TI)->static_fields)->f0 = L_2;
		return;
	}
}
 t268 * m1228 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t279_TI));
		t268 * L_0 = m2033((((t279_SFs*)InitializedTypeInfo(&t279_TI)->static_fields)->f0), &m2033_MI);
		return L_0;
	}
}
 void m1229 (t29 * __this, t268 * p0, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t279_TI));
		m2034((((t279_SFs*)InitializedTypeInfo(&t279_TI)->static_fields)->f0), p0, &m2034_MI);
		return;
	}
}
 void m1230 (t29 * __this, t268 * p0, MethodInfo* method){
	{
		VirtActionInvoker0::Invoke(&m2035_MI, p0);
		return;
	}
}
// Metadata Definition UnityEngine.UI.ComponentListPool
extern Il2CppType t280_0_0_49;
FieldInfo t279_f0_FieldInfo = 
{
	"s_ComponentListPool", &t280_0_0_49, &t279_TI, offsetof(t279_SFs, f0), &EmptyCustomAttributesCache};
extern Il2CppType t281_0_0_17;
extern CustomAttributesCache t279__CustomAttributeCache_U3CU3Ef__am$cache1;
FieldInfo t279_f1_FieldInfo = 
{
	"<>f__am$cache1", &t281_0_0_17, &t279_TI, offsetof(t279_SFs, f1), &t279__CustomAttributeCache_U3CU3Ef__am$cache1};
static FieldInfo* t279_FIs[] =
{
	&t279_f0_FieldInfo,
	&t279_f1_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1227_MI = 
{
	".cctor", (methodPointerType)&m1227, &t279_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, false, 1245, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t268_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1228_MI = 
{
	"Get", (methodPointerType)&m1228, &t279_TI, &t268_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 150, 0, 255, 0, false, false, 1246, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t268_0_0_0;
static ParameterInfo t279_m1229_ParameterInfos[] = 
{
	{"toRelease", 0, 134218443, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1229_MI = 
{
	"Release", (methodPointerType)&m1229, &t279_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t279_m1229_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 1, false, false, 1247, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t268_0_0_0;
static ParameterInfo t279_m1230_ParameterInfos[] = 
{
	{"l", 0, 134218444, &EmptyCustomAttributesCache, &t268_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t279__CustomAttributeCache_m1230;
MethodInfo m1230_MI = 
{
	"<s_ComponentListPool>m__17", (methodPointerType)&m1230, &t279_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t279_m1230_ParameterInfos, &t279__CustomAttributeCache_m1230, 145, 0, 255, 1, false, false, 1248, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t279_MIs[] =
{
	&m1227_MI,
	&m1228_MI,
	&m1229_MI,
	&m1230_MI,
	NULL
};
static MethodInfo* t279_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t279_CustomAttributesCacheGenerator_U3CU3Ef__am$cache1(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t279_CustomAttributesCacheGenerator_m1230(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t279__CustomAttributeCache_U3CU3Ef__am$cache1 = {
1,
NULL,
&t279_CustomAttributesCacheGenerator_U3CU3Ef__am$cache1
};
CustomAttributesCache t279__CustomAttributeCache_m1230 = {
1,
NULL,
&t279_CustomAttributesCacheGenerator_m1230
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t279_0_0_0;
extern Il2CppType t279_1_0_0;
struct t279;
extern CustomAttributesCache t279__CustomAttributeCache_U3CU3Ef__am$cache1;
extern CustomAttributesCache t279__CustomAttributeCache_m1230;
TypeInfo t279_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ComponentListPool", "UnityEngine.UI", t279_MIs, NULL, t279_FIs, NULL, &t29_TI, NULL, NULL, &t279_TI, NULL, t279_VT, &EmptyCustomAttributesCache, &t279_TI, &t279_0_0_0, &t279_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t279), 0, -1, sizeof(t279_SFs), 0, -1, 1048960, 0, false, false, false, false, false, false, false, false, false, true, false, false, 4, 0, 2, 0, 0, 4, 0, 0};
#include "t282.h"
extern Il2CppGenericContainer t282_IGC;
extern TypeInfo t282_gp_T_0_TI;
Il2CppGenericParamFull t282_gp_T_0_TI_GenericParamFull = { { &t282_IGC, 0}, {NULL, "T", 16, 0, NULL} };
static Il2CppGenericParamFull* t282_IGPA[1] = 
{
	&t282_gp_T_0_TI_GenericParamFull,
};
extern TypeInfo t282_TI;
Il2CppGenericContainer t282_IGC = { { NULL, NULL }, NULL, &t282_TI, 1, 0, t282_IGPA };
extern Il2CppType t427_0_0_0;
extern Il2CppType t427_0_0_0;
extern Il2CppType t427_0_0_0;
static ParameterInfo t282_m2036_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134218445, &EmptyCustomAttributesCache, &t427_0_0_0},
	{"actionOnRelease", 1, 134218446, &EmptyCustomAttributesCache, &t427_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2036_MI = 
{
	".ctor", NULL, &t282_TI, &t21_0_0_0, NULL, t282_m2036_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, false, 1249, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
extern CustomAttributesCache t282__CustomAttributeCache_m2037;
MethodInfo m2037_MI = 
{
	"get_countAll", NULL, &t282_TI, &t44_0_0_0, NULL, NULL, &t282__CustomAttributeCache_m2037, 2182, 0, 255, 0, false, false, 1250, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
static ParameterInfo t282_m2038_ParameterInfos[] = 
{
	{"value", 0, 134218447, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern CustomAttributesCache t282__CustomAttributeCache_m2038;
MethodInfo m2038_MI = 
{
	"set_countAll", NULL, &t282_TI, &t21_0_0_0, NULL, t282_m2038_ParameterInfos, &t282__CustomAttributeCache_m2038, 2177, 0, 255, 1, false, false, 1251, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
MethodInfo m2039_MI = 
{
	"get_countActive", NULL, &t282_TI, &t44_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1252, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t44_0_0_0;
MethodInfo m2040_MI = 
{
	"get_countInactive", NULL, &t282_TI, &t44_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1253, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t282_gp_0_0_0_0;
MethodInfo m2041_MI = 
{
	"Get", NULL, &t282_TI, &t282_gp_0_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, false, 1254, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t282_gp_0_0_0_0;
extern Il2CppType t282_gp_0_0_0_0;
static ParameterInfo t282_m2042_ParameterInfos[] = 
{
	{"element", 0, 134218448, &EmptyCustomAttributesCache, &t282_gp_0_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m2042_MI = 
{
	"Release", NULL, &t282_TI, &t21_0_0_0, NULL, t282_m2042_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, false, 1255, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t282_MIs[] =
{
	&m2036_MI,
	&m2037_MI,
	&m2038_MI,
	&m2039_MI,
	&m2040_MI,
	&m2041_MI,
	&m2042_MI,
	NULL
};
extern MethodInfo m2037_MI;
extern MethodInfo m2038_MI;
static PropertyInfo t282____countAll_PropertyInfo = 
{
	&t282_TI, "countAll", &m2037_MI, &m2038_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m2039_MI;
static PropertyInfo t282____countActive_PropertyInfo = 
{
	&t282_TI, "countActive", &m2039_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m2040_MI;
static PropertyInfo t282____countInactive_PropertyInfo = 
{
	&t282_TI, "countInactive", &m2040_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t282_PIs[] =
{
	&t282____countAll_PropertyInfo,
	&t282____countActive_PropertyInfo,
	&t282____countInactive_PropertyInfo,
	NULL
};
extern Il2CppType t428_0_0_33;
FieldInfo t282_f0_FieldInfo = 
{
	"m_Stack", &t428_0_0_33, &t282_TI, 0, &EmptyCustomAttributesCache};
extern Il2CppType t427_0_0_33;
FieldInfo t282_f1_FieldInfo = 
{
	"m_ActionOnGet", &t427_0_0_33, &t282_TI, 0, &EmptyCustomAttributesCache};
extern Il2CppType t427_0_0_33;
FieldInfo t282_f2_FieldInfo = 
{
	"m_ActionOnRelease", &t427_0_0_33, &t282_TI, 0, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
extern CustomAttributesCache t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
FieldInfo t282_f3_FieldInfo = 
{
	"<countAll>k__BackingField", &t44_0_0_1, &t282_TI, 0, &t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField};
static FieldInfo* t282_FIs[] =
{
	&t282_f0_FieldInfo,
	&t282_f1_FieldInfo,
	&t282_f2_FieldInfo,
	&t282_f3_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t282_0_0_0;
extern Il2CppType t282_1_0_0;
struct t282;
TypeInfo t282_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ObjectPool`1", "UnityEngine.UI", t282_MIs, t282_PIs, t282_FIs, NULL, NULL, NULL, NULL, &t282_TI, NULL, NULL, NULL, NULL, &t282_0_0_0, &t282_1_0_0, NULL, NULL, &t282_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 3, 4, 0, 0, 0, 0, 0};
void t282_CustomAttributesCacheGenerator_U3CcountAllU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t282_CustomAttributesCacheGenerator_m2037(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t282_CustomAttributesCacheGenerator_m2038(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField = {
1,
NULL,
&t282_CustomAttributesCacheGenerator_U3CcountAllU3Ek__BackingField
};
CustomAttributesCache t282__CustomAttributeCache_m2037 = {
1,
NULL,
&t282_CustomAttributesCacheGenerator_m2037
};
CustomAttributesCache t282__CustomAttributeCache_m2038 = {
1,
NULL,
&t282_CustomAttributesCacheGenerator_m2038
};
#include "t283.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t283_TI;
#include "t283MD.h"

extern MethodInfo m1232_MI;


extern MethodInfo m1231_MI;
 void m1231 (t283 * __this, MethodInfo* method){
	{
		m168(__this, &m168_MI);
		return;
	}
}
 t155 * m1232 (t283 * __this, MethodInfo* method){
	{
		t155 * L_0 = (__this->f2);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		t155 * L_2 = m1875(__this, &m1875_MI);
		__this->f2 = L_2;
	}

IL_001d:
	{
		t155 * L_3 = (__this->f2);
		return L_3;
	}
}
extern MethodInfo m1233_MI;
 void m1233 (t283 * __this, MethodInfo* method){
	{
		m170(__this, &m170_MI);
		t155 * L_0 = m1232(__this, &m1232_MI);
		bool L_1 = m1300(NULL, L_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		t155 * L_2 = m1232(__this, &m1232_MI);
		VirtActionInvoker0::Invoke(&m447_MI, L_2);
	}

IL_0022:
	{
		return;
	}
}
extern MethodInfo m1234_MI;
 void m1234 (t283 * __this, MethodInfo* method){
	{
		t155 * L_0 = m1232(__this, &m1232_MI);
		bool L_1 = m1300(NULL, L_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		t155 * L_2 = m1232(__this, &m1232_MI);
		VirtActionInvoker0::Invoke(&m447_MI, L_2);
	}

IL_001c:
	{
		m172(__this, &m172_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.BaseVertexEffect
extern Il2CppType t155_0_0_129;
FieldInfo t283_f2_FieldInfo = 
{
	"m_Graphic", &t155_0_0_129, &t283_TI, offsetof(t283, f2), &EmptyCustomAttributesCache};
static FieldInfo* t283_FIs[] =
{
	&t283_f2_FieldInfo,
	NULL
};
static PropertyInfo t283____graphic_PropertyInfo = 
{
	&t283_TI, "graphic", &m1232_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t283_PIs[] =
{
	&t283____graphic_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1231_MI = 
{
	".ctor", (methodPointerType)&m1231, &t283_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1256, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1232_MI = 
{
	"get_graphic", (methodPointerType)&m1232, &t283_TI, &t155_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2180, 0, 255, 0, false, false, 1257, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1233_MI = 
{
	"OnEnable", (methodPointerType)&m1233, &t283_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 5, 0, false, false, 1258, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1234_MI = 
{
	"OnDisable", (methodPointerType)&m1234, &t283_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 196, 0, 7, 0, false, false, 1259, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t163_0_0_0;
static ParameterInfo t283_m2043_ParameterInfos[] = 
{
	{"verts", 0, 134218449, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m2043_MI = 
{
	"ModifyVertices", NULL, &t283_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t283_m2043_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 16, 1, false, false, 1260, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t283_MIs[] =
{
	&m1231_MI,
	&m1232_MI,
	&m1233_MI,
	&m1234_MI,
	&m2043_MI,
	NULL
};
extern MethodInfo m2043_MI;
static MethodInfo* t283_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1233_MI,
	&m171_MI,
	&m1234_MI,
	&m173_MI,
	&m174_MI,
	&m175_MI,
	&m176_MI,
	&m177_MI,
	&m178_MI,
	&m179_MI,
	&m2043_MI,
	NULL,
};
extern TypeInfo t356_TI;
static TypeInfo* t283_ITIs[] = 
{
	&t356_TI,
};
static Il2CppInterfaceOffsetPair t283_IOs[] = 
{
	{ &t356_TI, 15},
};
void t283_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t360 * tmp;
		tmp = (t360 *)il2cpp_codegen_object_new (&t360_TI);
		m1604(tmp, &m1604_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t283__CustomAttributeCache = {
1,
NULL,
&t283_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t283_0_0_0;
extern Il2CppType t283_1_0_0;
struct t283;
extern CustomAttributesCache t283__CustomAttributeCache;
TypeInfo t283_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "BaseVertexEffect", "UnityEngine.UI", t283_MIs, t283_PIs, t283_FIs, NULL, &t55_TI, NULL, NULL, &t283_TI, t283_ITIs, t283_VT, &t283__CustomAttributeCache, &t283_TI, &t283_0_0_0, &t283_1_0_0, t283_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t283), 0, -1, 0, 0, -1, 1048705, 0, false, false, false, false, false, false, false, false, false, false, false, false, 5, 1, 1, 0, 0, 17, 1, 1};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.UI.IVertexModifier
extern Il2CppType t163_0_0_0;
static ParameterInfo t356_m1575_ParameterInfos[] = 
{
	{"verts", 0, 134218450, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1575_MI = 
{
	"ModifyVertices", NULL, &t356_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t356_m1575_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, false, 1261, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t356_MIs[] =
{
	&m1575_MI,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t356_0_0_0;
extern Il2CppType t356_1_0_0;
struct t356;
TypeInfo t356_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "IVertexModifier", "UnityEngine.UI", t356_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t356_TI, NULL, NULL, &EmptyCustomAttributesCache, &t356_TI, &t356_0_0_0, &t356_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t284.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t284_TI;
#include "t284MD.h"

#include "t287.h"
#include "t285MD.h"
#include "t287MD.h"
extern MethodInfo m1239_MI;
extern MethodInfo m1786_MI;
extern MethodInfo m1240_MI;
extern MethodInfo m1582_MI;
extern MethodInfo m1242_MI;
extern MethodInfo m1246_MI;


extern MethodInfo m1235_MI;
 void m1235 (t284 * __this, MethodInfo* method){
	{
		m1239(__this, &m1239_MI);
		return;
	}
}
extern MethodInfo m1236_MI;
 void m1236 (t284 * __this, t163 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t17  V_2 = {0};
	t17  V_3 = {0};
	t17  V_4 = {0};
	t17  V_5 = {0};
	t17  V_6 = {0};
	t17  V_7 = {0};
	t17  V_8 = {0};
	t17  V_9 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		V_0 = 0;
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1786_MI, p0);
		V_1 = L_1;
		t132  L_2 = m1240(__this, &m1240_MI);
		t287  L_3 = m1582(NULL, L_2, &m1582_MI);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1786_MI, p0);
		t17  L_5 = m1242(__this, &m1242_MI);
		V_2 = L_5;
		float L_6 = ((&V_2)->f1);
		t17  L_7 = m1242(__this, &m1242_MI);
		V_3 = L_7;
		float L_8 = ((&V_3)->f2);
		m1246(__this, p0, L_3, V_0, L_4, L_6, L_8, &m1246_MI);
		V_0 = V_1;
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1786_MI, p0);
		V_1 = L_9;
		t132  L_10 = m1240(__this, &m1240_MI);
		t287  L_11 = m1582(NULL, L_10, &m1582_MI);
		int32_t L_12 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1786_MI, p0);
		t17  L_13 = m1242(__this, &m1242_MI);
		V_4 = L_13;
		float L_14 = ((&V_4)->f1);
		t17  L_15 = m1242(__this, &m1242_MI);
		V_5 = L_15;
		float L_16 = ((&V_5)->f2);
		m1246(__this, p0, L_11, V_0, L_12, L_14, ((-L_16)), &m1246_MI);
		V_0 = V_1;
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1786_MI, p0);
		V_1 = L_17;
		t132  L_18 = m1240(__this, &m1240_MI);
		t287  L_19 = m1582(NULL, L_18, &m1582_MI);
		int32_t L_20 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1786_MI, p0);
		t17  L_21 = m1242(__this, &m1242_MI);
		V_6 = L_21;
		float L_22 = ((&V_6)->f1);
		t17  L_23 = m1242(__this, &m1242_MI);
		V_7 = L_23;
		float L_24 = ((&V_7)->f2);
		m1246(__this, p0, L_19, V_0, L_20, ((-L_22)), L_24, &m1246_MI);
		V_0 = V_1;
		int32_t L_25 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1786_MI, p0);
		V_1 = L_25;
		t132  L_26 = m1240(__this, &m1240_MI);
		t287  L_27 = m1582(NULL, L_26, &m1582_MI);
		int32_t L_28 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1786_MI, p0);
		t17  L_29 = m1242(__this, &m1242_MI);
		V_8 = L_29;
		float L_30 = ((&V_8)->f1);
		t17  L_31 = m1242(__this, &m1242_MI);
		V_9 = L_31;
		float L_32 = ((&V_9)->f2);
		m1246(__this, p0, L_27, V_0, L_28, ((-L_30)), ((-L_32)), &m1246_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.Outline
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1235_MI = 
{
	".ctor", (methodPointerType)&m1235, &t284_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1262, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t163_0_0_0;
static ParameterInfo t284_m1236_ParameterInfos[] = 
{
	{"verts", 0, 134218451, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1236_MI = 
{
	"ModifyVertices", (methodPointerType)&m1236, &t284_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t284_m1236_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 16, 1, false, false, 1263, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t284_MIs[] =
{
	&m1235_MI,
	&m1236_MI,
	NULL
};
static MethodInfo* t284_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1233_MI,
	&m171_MI,
	&m1234_MI,
	&m173_MI,
	&m174_MI,
	&m175_MI,
	&m176_MI,
	&m177_MI,
	&m178_MI,
	&m179_MI,
	&m1236_MI,
	&m1236_MI,
};
static Il2CppInterfaceOffsetPair t284_IOs[] = 
{
	{ &t356_TI, 15},
};
void t284_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Outline"), 15, &m1519_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t284__CustomAttributeCache = {
1,
NULL,
&t284_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t284_0_0_0;
extern Il2CppType t284_1_0_0;
extern TypeInfo t285_TI;
struct t284;
extern CustomAttributesCache t284__CustomAttributeCache;
TypeInfo t284_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Outline", "UnityEngine.UI", t284_MIs, NULL, NULL, NULL, &t285_TI, NULL, NULL, &t284_TI, NULL, t284_VT, &t284__CustomAttributeCache, &t284_TI, &t284_0_0_0, &t284_1_0_0, t284_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t284), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 17, 0, 1};
#include "t286.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t286_TI;
#include "t286MD.h"

extern MethodInfo m2044_MI;
extern MethodInfo m2045_MI;


extern MethodInfo m1237_MI;
 void m1237 (t286 * __this, MethodInfo* method){
	{
		m1231(__this, &m1231_MI);
		return;
	}
}
extern MethodInfo m1238_MI;
 void m1238 (t286 * __this, t163 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t184  V_1 = {0};
	t184  V_2 = {0};
	t184  V_3 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		V_0 = 0;
		goto IL_005b;
	}

IL_0013:
	{
		t184  L_1 = (t184 )VirtFuncInvoker1< t184 , int32_t >::Invoke(&m2044_MI, p0, V_0);
		V_1 = L_1;
		t184  L_2 = (t184 )VirtFuncInvoker1< t184 , int32_t >::Invoke(&m2044_MI, p0, V_0);
		V_2 = L_2;
		t23 * L_3 = &((&V_2)->f0);
		float L_4 = (L_3->f1);
		t184  L_5 = (t184 )VirtFuncInvoker1< t184 , int32_t >::Invoke(&m2044_MI, p0, V_0);
		V_3 = L_5;
		t23 * L_6 = &((&V_3)->f0);
		float L_7 = (L_6->f2);
		t17  L_8 = {0};
		m62(&L_8, L_4, L_7, &m62_MI);
		(&V_1)->f4 = L_8;
		VirtActionInvoker2< int32_t, t184  >::Invoke(&m2045_MI, p0, V_0, V_1);
		V_0 = ((int32_t)(V_0+1));
	}

IL_005b:
	{
		int32_t L_9 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1786_MI, p0);
		if ((((int32_t)V_0) < ((int32_t)L_9)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// Metadata Definition UnityEngine.UI.PositionAsUV1
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1237_MI = 
{
	".ctor", (methodPointerType)&m1237, &t286_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1264, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t163_0_0_0;
static ParameterInfo t286_m1238_ParameterInfos[] = 
{
	{"verts", 0, 134218452, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1238_MI = 
{
	"ModifyVertices", (methodPointerType)&m1238, &t286_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t286_m1238_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 16, 1, false, false, 1265, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t286_MIs[] =
{
	&m1237_MI,
	&m1238_MI,
	NULL
};
static MethodInfo* t286_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1233_MI,
	&m171_MI,
	&m1234_MI,
	&m173_MI,
	&m174_MI,
	&m175_MI,
	&m176_MI,
	&m177_MI,
	&m178_MI,
	&m179_MI,
	&m1238_MI,
	&m1238_MI,
};
static Il2CppInterfaceOffsetPair t286_IOs[] = 
{
	{ &t356_TI, 15},
};
void t286_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Position As UV1"), 16, &m1519_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t286__CustomAttributeCache = {
1,
NULL,
&t286_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t286_0_0_0;
extern Il2CppType t286_1_0_0;
struct t286;
extern CustomAttributesCache t286__CustomAttributeCache;
TypeInfo t286_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "PositionAsUV1", "UnityEngine.UI", t286_MIs, NULL, NULL, NULL, &t283_TI, NULL, NULL, &t286_TI, NULL, t286_VT, &t286__CustomAttributeCache, &t286_TI, &t286_0_0_0, &t286_1_0_0, t286_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t286), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 17, 0, 1};
#include "t285.h"
#ifndef _MSC_VER
#else
#endif

#include "t348.h"
extern TypeInfo t287_TI;
extern MethodInfo m2046_MI;
extern MethodInfo m1734_MI;
extern MethodInfo m1599_MI;
extern MethodInfo m1600_MI;


 void m1239 (t285 * __this, MethodInfo* method){
	{
		t132  L_0 = {0};
		m1734(&L_0, (0.0f), (0.0f), (0.0f), (0.5f), &m1734_MI);
		__this->f3 = L_0;
		t17  L_1 = {0};
		m62(&L_1, (1.0f), (-1.0f), &m62_MI);
		__this->f4 = L_1;
		__this->f5 = 1;
		m1231(__this, &m1231_MI);
		return;
	}
}
 t132  m1240 (t285 * __this, MethodInfo* method){
	{
		t132  L_0 = (__this->f3);
		return L_0;
	}
}
extern MethodInfo m1241_MI;
 void m1241 (t285 * __this, t132  p0, MethodInfo* method){
	{
		__this->f3 = p0;
		t155 * L_0 = m1232(__this, &m1232_MI);
		bool L_1 = m1300(NULL, L_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		t155 * L_2 = m1232(__this, &m1232_MI);
		VirtActionInvoker0::Invoke(&m447_MI, L_2);
	}

IL_0023:
	{
		return;
	}
}
 t17  m1242 (t285 * __this, MethodInfo* method){
	{
		t17  L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m1243_MI;
 void m1243 (t285 * __this, t17  p0, MethodInfo* method){
	{
		float L_0 = ((&p0)->f1);
		if ((((float)L_0) <= ((float)(600.0f))))
		{
			goto IL_001d;
		}
	}
	{
		(&p0)->f1 = (600.0f);
	}

IL_001d:
	{
		float L_1 = ((&p0)->f1);
		if ((((float)L_1) >= ((float)(-600.0f))))
		{
			goto IL_003a;
		}
	}
	{
		(&p0)->f1 = (-600.0f);
	}

IL_003a:
	{
		float L_2 = ((&p0)->f2);
		if ((((float)L_2) <= ((float)(600.0f))))
		{
			goto IL_0057;
		}
	}
	{
		(&p0)->f2 = (600.0f);
	}

IL_0057:
	{
		float L_3 = ((&p0)->f2);
		if ((((float)L_3) >= ((float)(-600.0f))))
		{
			goto IL_0074;
		}
	}
	{
		(&p0)->f2 = (-600.0f);
	}

IL_0074:
	{
		t17  L_4 = (__this->f4);
		bool L_5 = m2046(NULL, L_4, p0, &m2046_MI);
		if (!L_5)
		{
			goto IL_0086;
		}
	}
	{
		return;
	}

IL_0086:
	{
		__this->f4 = p0;
		t155 * L_6 = m1232(__this, &m1232_MI);
		bool L_7 = m1300(NULL, L_6, (t41 *)NULL, &m1300_MI);
		if (!L_7)
		{
			goto IL_00a9;
		}
	}
	{
		t155 * L_8 = m1232(__this, &m1232_MI);
		VirtActionInvoker0::Invoke(&m447_MI, L_8);
	}

IL_00a9:
	{
		return;
	}
}
extern MethodInfo m1244_MI;
 bool m1244 (t285 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->f5);
		return L_0;
	}
}
extern MethodInfo m1245_MI;
 void m1245 (t285 * __this, bool p0, MethodInfo* method){
	{
		__this->f5 = p0;
		t155 * L_0 = m1232(__this, &m1232_MI);
		bool L_1 = m1300(NULL, L_0, (t41 *)NULL, &m1300_MI);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		t155 * L_2 = m1232(__this, &m1232_MI);
		VirtActionInvoker0::Invoke(&m447_MI, L_2);
	}

IL_0023:
	{
		return;
	}
}
 void m1246 (t285 * __this, t163 * p0, t287  p1, int32_t p2, int32_t p3, float p4, float p5, MethodInfo* method){
	t184  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	t23  V_3 = {0};
	t287  V_4 = {0};
	t184  V_5 = {0};
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1786_MI, p0);
		V_1 = ((int32_t)((int32_t)L_0*(int32_t)2));
		int32_t L_1 = m1599(p0, &m1599_MI);
		if ((((int32_t)L_1) >= ((int32_t)V_1)))
		{
			goto IL_001c;
		}
	}
	{
		m1600(p0, V_1, &m1600_MI);
	}

IL_001c:
	{
		V_2 = p2;
		goto IL_00b0;
	}

IL_0023:
	{
		t184  L_2 = (t184 )VirtFuncInvoker1< t184 , int32_t >::Invoke(&m2044_MI, p0, V_2);
		V_0 = L_2;
		VirtActionInvoker1< t184  >::Invoke(&m1584_MI, p0, V_0);
		t23  L_3 = ((&V_0)->f0);
		V_3 = L_3;
		t23 * L_4 = (&V_3);
		float L_5 = (L_4->f1);
		L_4->f1 = ((float)(L_5+p4));
		t23 * L_6 = (&V_3);
		float L_7 = (L_6->f2);
		L_6->f2 = ((float)(L_7+p5));
		(&V_0)->f0 = V_3;
		V_4 = p1;
		bool L_8 = (__this->f5);
		if (!L_8)
		{
			goto IL_009b;
		}
	}
	{
		uint8_t L_9 = ((&V_4)->f3);
		t184  L_10 = (t184 )VirtFuncInvoker1< t184 , int32_t >::Invoke(&m2044_MI, p0, V_2);
		V_5 = L_10;
		t287 * L_11 = &((&V_5)->f2);
		uint8_t L_12 = (L_11->f3);
		(&V_4)->f3 = (((uint8_t)((int32_t)((int32_t)((int32_t)((int32_t)L_9*(int32_t)L_12))/(int32_t)((int32_t)255)))));
	}

IL_009b:
	{
		(&V_0)->f2 = V_4;
		VirtActionInvoker2< int32_t, t184  >::Invoke(&m2045_MI, p0, V_2, V_0);
		V_2 = ((int32_t)(V_2+1));
	}

IL_00b0:
	{
		if ((((int32_t)V_2) < ((int32_t)p3)))
		{
			goto IL_0023;
		}
	}
	{
		return;
	}
}
extern MethodInfo m1247_MI;
 void m1247 (t285 * __this, t163 * p0, MethodInfo* method){
	t17  V_0 = {0};
	t17  V_1 = {0};
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(&m174_MI, __this);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		t132  L_1 = m1240(__this, &m1240_MI);
		t287  L_2 = m1582(NULL, L_1, &m1582_MI);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1786_MI, p0);
		t17  L_4 = m1242(__this, &m1242_MI);
		V_0 = L_4;
		float L_5 = ((&V_0)->f1);
		t17  L_6 = m1242(__this, &m1242_MI);
		V_1 = L_6;
		float L_7 = ((&V_1)->f2);
		m1246(__this, p0, L_2, 0, L_3, L_5, L_7, &m1246_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.Shadow
extern Il2CppType t132_0_0_1;
extern CustomAttributesCache t285__CustomAttributeCache_m_EffectColor;
FieldInfo t285_f3_FieldInfo = 
{
	"m_EffectColor", &t132_0_0_1, &t285_TI, offsetof(t285, f3), &t285__CustomAttributeCache_m_EffectColor};
extern Il2CppType t17_0_0_1;
extern CustomAttributesCache t285__CustomAttributeCache_m_EffectDistance;
FieldInfo t285_f4_FieldInfo = 
{
	"m_EffectDistance", &t17_0_0_1, &t285_TI, offsetof(t285, f4), &t285__CustomAttributeCache_m_EffectDistance};
extern Il2CppType t40_0_0_1;
extern CustomAttributesCache t285__CustomAttributeCache_m_UseGraphicAlpha;
FieldInfo t285_f5_FieldInfo = 
{
	"m_UseGraphicAlpha", &t40_0_0_1, &t285_TI, offsetof(t285, f5), &t285__CustomAttributeCache_m_UseGraphicAlpha};
static FieldInfo* t285_FIs[] =
{
	&t285_f3_FieldInfo,
	&t285_f4_FieldInfo,
	&t285_f5_FieldInfo,
	NULL
};
static PropertyInfo t285____effectColor_PropertyInfo = 
{
	&t285_TI, "effectColor", &m1240_MI, &m1241_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t285____effectDistance_PropertyInfo = 
{
	&t285_TI, "effectDistance", &m1242_MI, &m1243_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t285____useGraphicAlpha_PropertyInfo = 
{
	&t285_TI, "useGraphicAlpha", &m1244_MI, &m1245_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t285_PIs[] =
{
	&t285____effectColor_PropertyInfo,
	&t285____effectDistance_PropertyInfo,
	&t285____useGraphicAlpha_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m1239_MI = 
{
	".ctor", (methodPointerType)&m1239, &t285_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, false, 1266, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t132_0_0_0;
extern void* RuntimeInvoker_t132 (MethodInfo* method, void* obj, void** args);
MethodInfo m1240_MI = 
{
	"get_effectColor", (methodPointerType)&m1240, &t285_TI, &t132_0_0_0, RuntimeInvoker_t132, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1267, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t132_0_0_0;
static ParameterInfo t285_m1241_ParameterInfos[] = 
{
	{"value", 0, 134218453, &EmptyCustomAttributesCache, &t132_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t132 (MethodInfo* method, void* obj, void** args);
MethodInfo m1241_MI = 
{
	"set_effectColor", (methodPointerType)&m1241, &t285_TI, &t21_0_0_0, RuntimeInvoker_t21_t132, t285_m1241_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1268, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
extern void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m1242_MI = 
{
	"get_effectDistance", (methodPointerType)&m1242, &t285_TI, &t17_0_0_0, RuntimeInvoker_t17, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1269, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t17_0_0_0;
static ParameterInfo t285_m1243_ParameterInfos[] = 
{
	{"value", 0, 134218454, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17 (MethodInfo* method, void* obj, void** args);
MethodInfo m1243_MI = 
{
	"set_effectDistance", (methodPointerType)&m1243, &t285_TI, &t21_0_0_0, RuntimeInvoker_t21_t17, t285_m1243_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1270, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
MethodInfo m1244_MI = 
{
	"get_useGraphicAlpha", (methodPointerType)&m1244, &t285_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, false, 1271, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
static ParameterInfo t285_m1245_ParameterInfos[] = 
{
	{"value", 0, 134218455, &EmptyCustomAttributesCache, &t40_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args);
MethodInfo m1245_MI = 
{
	"set_useGraphicAlpha", (methodPointerType)&m1245, &t285_TI, &t21_0_0_0, RuntimeInvoker_t21_t297, t285_m1245_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, false, 1272, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t163_0_0_0;
extern Il2CppType t287_0_0_0;
extern Il2CppType t287_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t285_m1246_ParameterInfos[] = 
{
	{"verts", 0, 134218456, &EmptyCustomAttributesCache, &t163_0_0_0},
	{"color", 1, 134218457, &EmptyCustomAttributesCache, &t287_0_0_0},
	{"start", 2, 134218458, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"end", 3, 134218459, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"x", 4, 134218460, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"y", 5, 134218461, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t287_t44_t44_t22_t22 (MethodInfo* method, void* obj, void** args);
MethodInfo m1246_MI = 
{
	"ApplyShadow", (methodPointerType)&m1246, &t285_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t287_t44_t44_t22_t22, t285_m1246_ParameterInfos, &EmptyCustomAttributesCache, 132, 0, 255, 6, false, false, 1273, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t163_0_0_0;
static ParameterInfo t285_m1247_ParameterInfos[] = 
{
	{"verts", 0, 134218462, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m1247_MI = 
{
	"ModifyVertices", (methodPointerType)&m1247, &t285_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t285_m1247_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 16, 1, false, false, 1274, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t285_MIs[] =
{
	&m1239_MI,
	&m1240_MI,
	&m1241_MI,
	&m1242_MI,
	&m1243_MI,
	&m1244_MI,
	&m1245_MI,
	&m1246_MI,
	&m1247_MI,
	NULL
};
static MethodInfo* t285_VT[] =
{
	&m45_MI,
	&m46_MI,
	&m47_MI,
	&m48_MI,
	&m169_MI,
	&m1233_MI,
	&m171_MI,
	&m1234_MI,
	&m173_MI,
	&m174_MI,
	&m175_MI,
	&m176_MI,
	&m177_MI,
	&m178_MI,
	&m179_MI,
	&m1247_MI,
	&m1247_MI,
};
static Il2CppInterfaceOffsetPair t285_IOs[] = 
{
	{ &t356_TI, 15},
};
void t285_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t298 * tmp;
		tmp = (t298 *)il2cpp_codegen_object_new (&t298_TI);
		m1519(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Shadow"), 14, &m1519_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t285_CustomAttributesCacheGenerator_m_EffectColor(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t285_CustomAttributesCacheGenerator_m_EffectDistance(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t285_CustomAttributesCacheGenerator_m_UseGraphicAlpha(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t300 * tmp;
		tmp = (t300 *)il2cpp_codegen_object_new (&t300_TI);
		m1319(tmp, &m1319_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t285__CustomAttributeCache = {
1,
NULL,
&t285_CustomAttributesCacheGenerator
};
CustomAttributesCache t285__CustomAttributeCache_m_EffectColor = {
1,
NULL,
&t285_CustomAttributesCacheGenerator_m_EffectColor
};
CustomAttributesCache t285__CustomAttributeCache_m_EffectDistance = {
1,
NULL,
&t285_CustomAttributesCacheGenerator_m_EffectDistance
};
CustomAttributesCache t285__CustomAttributeCache_m_UseGraphicAlpha = {
1,
NULL,
&t285_CustomAttributesCacheGenerator_m_UseGraphicAlpha
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t285_0_0_0;
extern Il2CppType t285_1_0_0;
struct t285;
extern CustomAttributesCache t285__CustomAttributeCache;
extern CustomAttributesCache t285__CustomAttributeCache_m_EffectColor;
extern CustomAttributesCache t285__CustomAttributeCache_m_EffectDistance;
extern CustomAttributesCache t285__CustomAttributeCache_m_UseGraphicAlpha;
TypeInfo t285_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "Shadow", "UnityEngine.UI", t285_MIs, t285_PIs, t285_FIs, NULL, &t283_TI, NULL, NULL, &t285_TI, NULL, t285_VT, &t285__CustomAttributeCache, &t285_TI, &t285_0_0_0, &t285_1_0_0, t285_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t285), 0, -1, 0, 0, -1, 1048577, 0, false, false, false, false, false, false, false, false, false, false, false, false, 9, 3, 3, 0, 0, 17, 0, 1};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
