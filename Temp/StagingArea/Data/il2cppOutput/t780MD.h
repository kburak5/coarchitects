﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t780;
struct t782;
struct t949;
struct t781;
struct t7;
struct t783;
struct t777;
struct t793;
struct t733;
#include "t465.h"
#include "t735.h"

 void m4102 (t780 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4494 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4495 (t780 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4496 (t780 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t782 * m4048 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4100 (t780 * __this, t782 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t949 * m4161 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4497 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4498 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4041 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4054 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4159 (t780 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4053 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t783 * m4042 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4099 (t780 * __this, t783 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4191 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4082 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4499 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4084 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4500 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m4081 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m4079 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4087 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4501 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4502 (t780 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4503 (t780 * __this, t782 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4504 (t780 * __this, t783 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4160 (t780 * __this, t777 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4505 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t793 * m4078 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t793 * m4085 (t780 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4506 (t780 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4507 (t29 * __this, t7* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
