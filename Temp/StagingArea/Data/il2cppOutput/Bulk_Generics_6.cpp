﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t2345.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t2345_TI;
#include "t2345MD.h"

#include "t21.h"
#include "t29.h"
#include "t35.h"
#include "t725.h"
#include "t44.h"
#include "t6.h"
#include "t67.h"

#include "t20.h"

extern MethodInfo m12147_MI;
 void m12147 (t2345 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12148_MI;
 t725  m12148 (t2345 * __this, int32_t p0, t6 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12148((t2345 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 *, t29 * __this, int32_t p0, t6 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	typedef t725  (*FunctionPointerType) (t29 * __this, int32_t p0, t6 * p1, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m12149_MI;
 t29 * m12149 (t2345 * __this, int32_t p0, t6 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t44_TI), &p0);
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12150_MI;
 t725  m12150 (t2345 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t725 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.EventSystems.PointerEventData,System.Collections.DictionaryEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2345_m12147_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12147_GM;
MethodInfo m12147_MI = 
{
	".ctor", (methodPointerType)&m12147, &t2345_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2345_m12147_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12147_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t2345_m12148_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12148_GM;
MethodInfo m12148_MI = 
{
	"Invoke", (methodPointerType)&m12148, &t2345_TI, &t725_0_0_0, RuntimeInvoker_t725_t44_t29, t2345_m12148_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12148_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2345_m12149_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12149_GM;
MethodInfo m12149_MI = 
{
	"BeginInvoke", (methodPointerType)&m12149, &t2345_TI, &t66_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29, t2345_m12149_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12149_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2345_m12150_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12150_GM;
MethodInfo m12150_MI = 
{
	"EndInvoke", (methodPointerType)&m12150, &t2345_TI, &t725_0_0_0, RuntimeInvoker_t725_t29, t2345_m12150_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12150_GM};
static MethodInfo* t2345_MIs[] =
{
	&m12147_MI,
	&m12148_MI,
	&m12149_MI,
	&m12150_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
static MethodInfo* t2345_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12148_MI,
	&m12149_MI,
	&m12150_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2345_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2345_0_0_0;
extern Il2CppType t2345_1_0_0;
extern TypeInfo t195_TI;
struct t2345;
extern Il2CppGenericClass t2345_GC;
extern TypeInfo t1254_TI;
TypeInfo t2345_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2345_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2345_TI, NULL, t2345_VT, &EmptyCustomAttributesCache, &t2345_TI, &t2345_0_0_0, &t2345_1_0_0, t2345_IOs, &t2345_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2345), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4103_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.DictionaryEntry>
extern MethodInfo m27411_MI;
static PropertyInfo t4103____Current_PropertyInfo = 
{
	&t4103_TI, "Current", &m27411_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4103_PIs[] =
{
	&t4103____Current_PropertyInfo,
	NULL
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27411_GM;
MethodInfo m27411_MI = 
{
	"get_Current", NULL, &t4103_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27411_GM};
static MethodInfo* t4103_MIs[] =
{
	&m27411_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4103_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4103_0_0_0;
extern Il2CppType t4103_1_0_0;
struct t4103;
extern Il2CppGenericClass t4103_GC;
TypeInfo t4103_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4103_MIs, t4103_PIs, NULL, NULL, NULL, NULL, NULL, &t4103_TI, t4103_ITIs, NULL, &EmptyCustomAttributesCache, &t4103_TI, &t4103_0_0_0, &t4103_1_0_0, NULL, &t4103_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2358.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2358_TI;
#include "t2358MD.h"

#include "t7.h"
#include "t914.h"
#include "t40.h"
extern TypeInfo t725_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m12155_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m20513_MI;
struct t20;
#include "t915.h"
 t725  m20513 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12151_MI;
 void m12151 (t2358 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12152_MI;
 t29 * m12152 (t2358 * __this, MethodInfo* method){
	{
		t725  L_0 = m12155(__this, &m12155_MI);
		t725  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t725_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12153_MI;
 void m12153 (t2358 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12154_MI;
 bool m12154 (t2358 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t725  m12155 (t2358 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t725  L_8 = m20513(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20513_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>
extern Il2CppType t20_0_0_1;
FieldInfo t2358_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2358_TI, offsetof(t2358, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2358_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2358_TI, offsetof(t2358, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2358_FIs[] =
{
	&t2358_f0_FieldInfo,
	&t2358_f1_FieldInfo,
	NULL
};
static PropertyInfo t2358____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2358_TI, "System.Collections.IEnumerator.Current", &m12152_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2358____Current_PropertyInfo = 
{
	&t2358_TI, "Current", &m12155_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2358_PIs[] =
{
	&t2358____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2358____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2358_m12151_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12151_GM;
MethodInfo m12151_MI = 
{
	".ctor", (methodPointerType)&m12151, &t2358_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2358_m12151_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12151_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12152_GM;
MethodInfo m12152_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12152, &t2358_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12152_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12153_GM;
MethodInfo m12153_MI = 
{
	"Dispose", (methodPointerType)&m12153, &t2358_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12153_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12154_GM;
MethodInfo m12154_MI = 
{
	"MoveNext", (methodPointerType)&m12154, &t2358_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12154_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12155_GM;
MethodInfo m12155_MI = 
{
	"get_Current", (methodPointerType)&m12155, &t2358_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12155_GM};
static MethodInfo* t2358_MIs[] =
{
	&m12151_MI,
	&m12152_MI,
	&m12153_MI,
	&m12154_MI,
	&m12155_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t2358_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12152_MI,
	&m12154_MI,
	&m12153_MI,
	&m12155_MI,
};
static TypeInfo* t2358_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4103_TI,
};
static Il2CppInterfaceOffsetPair t2358_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4103_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2358_0_0_0;
extern Il2CppType t2358_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2358_GC;
extern TypeInfo t20_TI;
TypeInfo t2358_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2358_MIs, t2358_PIs, t2358_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2358_TI, t2358_ITIs, t2358_VT, &EmptyCustomAttributesCache, &t2358_TI, &t2358_0_0_0, &t2358_1_0_0, t2358_IOs, &t2358_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2358)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5262_TI;

#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.DictionaryEntry>
extern MethodInfo m27412_MI;
static PropertyInfo t5262____Count_PropertyInfo = 
{
	&t5262_TI, "Count", &m27412_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27413_MI;
static PropertyInfo t5262____IsReadOnly_PropertyInfo = 
{
	&t5262_TI, "IsReadOnly", &m27413_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5262_PIs[] =
{
	&t5262____Count_PropertyInfo,
	&t5262____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27412_GM;
MethodInfo m27412_MI = 
{
	"get_Count", NULL, &t5262_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27412_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27413_GM;
MethodInfo m27413_MI = 
{
	"get_IsReadOnly", NULL, &t5262_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27413_GM};
extern Il2CppType t725_0_0_0;
extern Il2CppType t725_0_0_0;
static ParameterInfo t5262_m27414_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t725_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27414_GM;
MethodInfo m27414_MI = 
{
	"Add", NULL, &t5262_TI, &t21_0_0_0, RuntimeInvoker_t21_t725, t5262_m27414_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27414_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27415_GM;
MethodInfo m27415_MI = 
{
	"Clear", NULL, &t5262_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27415_GM};
extern Il2CppType t725_0_0_0;
static ParameterInfo t5262_m27416_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t725_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27416_GM;
MethodInfo m27416_MI = 
{
	"Contains", NULL, &t5262_TI, &t40_0_0_0, RuntimeInvoker_t40_t725, t5262_m27416_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27416_GM};
extern Il2CppType t3541_0_0_0;
extern Il2CppType t3541_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5262_m27417_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3541_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27417_GM;
MethodInfo m27417_MI = 
{
	"CopyTo", NULL, &t5262_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5262_m27417_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27417_GM};
extern Il2CppType t725_0_0_0;
static ParameterInfo t5262_m27418_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t725_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27418_GM;
MethodInfo m27418_MI = 
{
	"Remove", NULL, &t5262_TI, &t40_0_0_0, RuntimeInvoker_t40_t725, t5262_m27418_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27418_GM};
static MethodInfo* t5262_MIs[] =
{
	&m27412_MI,
	&m27413_MI,
	&m27414_MI,
	&m27415_MI,
	&m27416_MI,
	&m27417_MI,
	&m27418_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5264_TI;
static TypeInfo* t5262_ITIs[] = 
{
	&t603_TI,
	&t5264_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5262_0_0_0;
extern Il2CppType t5262_1_0_0;
struct t5262;
extern Il2CppGenericClass t5262_GC;
TypeInfo t5262_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5262_MIs, t5262_PIs, NULL, NULL, NULL, NULL, NULL, &t5262_TI, t5262_ITIs, NULL, &EmptyCustomAttributesCache, &t5262_TI, &t5262_0_0_0, &t5262_1_0_0, NULL, &t5262_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.DictionaryEntry>
extern Il2CppType t4103_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27419_GM;
MethodInfo m27419_MI = 
{
	"GetEnumerator", NULL, &t5264_TI, &t4103_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27419_GM};
static MethodInfo* t5264_MIs[] =
{
	&m27419_MI,
	NULL
};
static TypeInfo* t5264_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5264_0_0_0;
extern Il2CppType t5264_1_0_0;
struct t5264;
extern Il2CppGenericClass t5264_GC;
TypeInfo t5264_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5264_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5264_TI, t5264_ITIs, NULL, &EmptyCustomAttributesCache, &t5264_TI, &t5264_0_0_0, &t5264_1_0_0, NULL, &t5264_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5263_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.DictionaryEntry>
extern MethodInfo m27420_MI;
extern MethodInfo m27421_MI;
static PropertyInfo t5263____Item_PropertyInfo = 
{
	&t5263_TI, "Item", &m27420_MI, &m27421_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5263_PIs[] =
{
	&t5263____Item_PropertyInfo,
	NULL
};
extern Il2CppType t725_0_0_0;
static ParameterInfo t5263_m27422_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t725_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27422_GM;
MethodInfo m27422_MI = 
{
	"IndexOf", NULL, &t5263_TI, &t44_0_0_0, RuntimeInvoker_t44_t725, t5263_m27422_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27422_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t725_0_0_0;
static ParameterInfo t5263_m27423_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t725_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27423_GM;
MethodInfo m27423_MI = 
{
	"Insert", NULL, &t5263_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t725, t5263_m27423_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27423_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5263_m27424_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27424_GM;
MethodInfo m27424_MI = 
{
	"RemoveAt", NULL, &t5263_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5263_m27424_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27424_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5263_m27420_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27420_GM;
MethodInfo m27420_MI = 
{
	"get_Item", NULL, &t5263_TI, &t725_0_0_0, RuntimeInvoker_t725_t44, t5263_m27420_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27420_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t725_0_0_0;
static ParameterInfo t5263_m27421_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t725_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27421_GM;
MethodInfo m27421_MI = 
{
	"set_Item", NULL, &t5263_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t725, t5263_m27421_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27421_GM};
static MethodInfo* t5263_MIs[] =
{
	&m27422_MI,
	&m27423_MI,
	&m27424_MI,
	&m27420_MI,
	&m27421_MI,
	NULL
};
static TypeInfo* t5263_ITIs[] = 
{
	&t603_TI,
	&t5262_TI,
	&t5264_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5263_0_0_0;
extern Il2CppType t5263_1_0_0;
struct t5263;
extern Il2CppGenericClass t5263_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5263_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5263_MIs, t5263_PIs, NULL, NULL, NULL, NULL, NULL, &t5263_TI, t5263_ITIs, NULL, &t1908__CustomAttributeCache, &t5263_TI, &t5263_0_0_0, &t5263_1_0_0, NULL, &t5263_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2359.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2359_TI;
#include "t2359MD.h"

#include "t322.h"


extern MethodInfo m12156_MI;
 void m12156 (t2359 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12157_MI;
 t322  m12157 (t2359 * __this, int32_t p0, t6 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12157((t2359 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t322  (*FunctionPointerType) (t29 *, t29 * __this, int32_t p0, t6 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	typedef t322  (*FunctionPointerType) (t29 * __this, int32_t p0, t6 * p1, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m12158_MI;
 t29 * m12158 (t2359 * __this, int32_t p0, t6 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t44_TI), &p0);
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12159_MI;
 t322  m12159 (t2359 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t322 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.EventSystems.PointerEventData,System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2359_m12156_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12156_GM;
MethodInfo m12156_MI = 
{
	".ctor", (methodPointerType)&m12156, &t2359_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2359_m12156_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12156_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t2359_m12157_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t322_0_0_0;
extern void* RuntimeInvoker_t322_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12157_GM;
MethodInfo m12157_MI = 
{
	"Invoke", (methodPointerType)&m12157, &t2359_TI, &t322_0_0_0, RuntimeInvoker_t322_t44_t29, t2359_m12157_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12157_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t6_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2359_m12158_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12158_GM;
MethodInfo m12158_MI = 
{
	"BeginInvoke", (methodPointerType)&m12158, &t2359_TI, &t66_0_0_0, RuntimeInvoker_t29_t44_t29_t29_t29, t2359_m12158_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12158_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2359_m12159_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t322_0_0_0;
extern void* RuntimeInvoker_t322_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12159_GM;
MethodInfo m12159_MI = 
{
	"EndInvoke", (methodPointerType)&m12159, &t2359_TI, &t322_0_0_0, RuntimeInvoker_t322_t29, t2359_m12159_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12159_GM};
static MethodInfo* t2359_MIs[] =
{
	&m12156_MI,
	&m12157_MI,
	&m12158_MI,
	&m12159_MI,
	NULL
};
static MethodInfo* t2359_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12157_MI,
	&m12158_MI,
	&m12159_MI,
};
static Il2CppInterfaceOffsetPair t2359_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2359_0_0_0;
extern Il2CppType t2359_1_0_0;
struct t2359;
extern Il2CppGenericClass t2359_GC;
TypeInfo t2359_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2359_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2359_TI, NULL, t2359_VT, &EmptyCustomAttributesCache, &t2359_TI, &t2359_0_0_0, &t2359_1_0_0, t2359_IOs, &t2359_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2359), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2360.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2360_TI;
#include "t2360MD.h"

#include "t323.h"
#include "t118.h"
extern TypeInfo t323_TI;
extern TypeInfo t722_TI;
extern TypeInfo t322_TI;
extern TypeInfo t44_TI;
extern TypeInfo t6_TI;
extern TypeInfo t40_TI;
#include "t323MD.h"
#include "t322MD.h"
#include "t29MD.h"
#include "t118MD.h"
extern MethodInfo m10122_MI;
extern MethodInfo m1433_MI;
extern MethodInfo m1435_MI;
extern MethodInfo m1434_MI;
extern MethodInfo m12162_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m1432_MI;
extern MethodInfo m1436_MI;


extern MethodInfo m12160_MI;
 void m12160 (t2360 * __this, t118 * p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		t323  L_0 = m1432(p0, &m1432_MI);
		__this->f0 = L_0;
		return;
	}
}
extern MethodInfo m12161_MI;
 bool m12161 (t2360 * __this, MethodInfo* method){
	{
		t323 * L_0 = &(__this->f0);
		bool L_1 = m1436(L_0, &m1436_MI);
		return L_1;
	}
}
 t725  m12162 (t2360 * __this, MethodInfo* method){
	{
		t323  L_0 = (__this->f0);
		t323  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t323_TI), &L_1);
		t725  L_3 = (t725 )InterfaceFuncInvoker0< t725  >::Invoke(&m10122_MI, L_2);
		return L_3;
	}
}
extern MethodInfo m12163_MI;
 t29 * m12163 (t2360 * __this, MethodInfo* method){
	t322  V_0 = {0};
	{
		t323 * L_0 = &(__this->f0);
		t322  L_1 = m1433(L_0, &m1433_MI);
		V_0 = L_1;
		int32_t L_2 = m1435((&V_0), &m1435_MI);
		int32_t L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t44_TI), &L_3);
		return L_4;
	}
}
extern MethodInfo m12164_MI;
 t29 * m12164 (t2360 * __this, MethodInfo* method){
	t322  V_0 = {0};
	{
		t323 * L_0 = &(__this->f0);
		t322  L_1 = m1433(L_0, &m1433_MI);
		V_0 = L_1;
		t6 * L_2 = m1434((&V_0), &m1434_MI);
		t6 * L_3 = L_2;
		return ((t6 *)L_3);
	}
}
extern MethodInfo m12165_MI;
 t29 * m12165 (t2360 * __this, MethodInfo* method){
	{
		t725  L_0 = (t725 )VirtFuncInvoker0< t725  >::Invoke(&m12162_MI, __this);
		t725  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t725_TI), &L_1);
		return L_2;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t323_0_0_1;
FieldInfo t2360_f0_FieldInfo = 
{
	"host_enumerator", &t323_0_0_1, &t2360_TI, offsetof(t2360, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2360_FIs[] =
{
	&t2360_f0_FieldInfo,
	NULL
};
static PropertyInfo t2360____Entry_PropertyInfo = 
{
	&t2360_TI, "Entry", &m12162_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2360____Key_PropertyInfo = 
{
	&t2360_TI, "Key", &m12163_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2360____Value_PropertyInfo = 
{
	&t2360_TI, "Value", &m12164_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2360____Current_PropertyInfo = 
{
	&t2360_TI, "Current", &m12165_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2360_PIs[] =
{
	&t2360____Entry_PropertyInfo,
	&t2360____Key_PropertyInfo,
	&t2360____Value_PropertyInfo,
	&t2360____Current_PropertyInfo,
	NULL
};
extern Il2CppType t118_0_0_0;
extern Il2CppType t118_0_0_0;
static ParameterInfo t2360_m12160_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t118_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12160_GM;
MethodInfo m12160_MI = 
{
	".ctor", (methodPointerType)&m12160, &t2360_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2360_m12160_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12160_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12161_GM;
MethodInfo m12161_MI = 
{
	"MoveNext", (methodPointerType)&m12161, &t2360_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12161_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12162_GM;
MethodInfo m12162_MI = 
{
	"get_Entry", (methodPointerType)&m12162, &t2360_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12162_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12163_GM;
MethodInfo m12163_MI = 
{
	"get_Key", (methodPointerType)&m12163, &t2360_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12163_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12164_GM;
MethodInfo m12164_MI = 
{
	"get_Value", (methodPointerType)&m12164, &t2360_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12164_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12165_GM;
MethodInfo m12165_MI = 
{
	"get_Current", (methodPointerType)&m12165, &t2360_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12165_GM};
static MethodInfo* t2360_MIs[] =
{
	&m12160_MI,
	&m12161_MI,
	&m12162_MI,
	&m12163_MI,
	&m12164_MI,
	&m12165_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
static MethodInfo* t2360_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12165_MI,
	&m12161_MI,
	&m12162_MI,
	&m12163_MI,
	&m12164_MI,
};
static TypeInfo* t2360_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2360_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2360_0_0_0;
extern Il2CppType t2360_1_0_0;
extern TypeInfo t29_TI;
struct t2360;
extern Il2CppGenericClass t2360_GC;
TypeInfo t2360_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ShimEnumerator", "", t2360_MIs, t2360_PIs, t2360_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2360_TI, t2360_ITIs, t2360_VT, &EmptyCustomAttributesCache, &t2360_TI, &t2360_0_0_0, &t2360_1_0_0, t2360_IOs, &t2360_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2360), 0, -1, 0, 0, -1, 1056771, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 4, 1, 0, 0, 9, 2, 2};
#include "t2361.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2361_TI;
#include "t2361MD.h"

#include "t42.h"
#include "t43.h"
#include "t1257.h"
#include "t2363.h"
extern TypeInfo t1707_TI;
extern TypeInfo t42_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2363_TI;
#include "t42MD.h"
#include "t931MD.h"
#include "t2363MD.h"
extern Il2CppType t1707_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m1554_MI;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m12174_MI;
extern MethodInfo m27425_MI;
extern MethodInfo m27426_MI;


extern MethodInfo m12166_MI;
 void m12166 (t2361 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m12167_MI;
 void m12167 (t29 * __this, MethodInfo* method){
	t2363 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t2363 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t2363_TI));
	m12174(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m12174_MI);
	((t2361_SFs*)InitializedTypeInfo(&t2361_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m12168_MI;
 int32_t m12168 (t2361 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, int32_t >::Invoke(&m27425_MI, __this, ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI))))));
		return L_0;
	}
}
extern MethodInfo m12169_MI;
 bool m12169 (t2361 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(&m27426_MI, __this, ((*(int32_t*)((int32_t*)UnBox (p0, InitializedTypeInfo(&t44_TI))))), ((*(int32_t*)((int32_t*)UnBox (p1, InitializedTypeInfo(&t44_TI))))));
		return L_0;
	}
}
extern MethodInfo m12170_MI;
 t2361 * m12170 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2361_TI));
		return (((t2361_SFs*)InitializedTypeInfo(&t2361_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.Int32>
extern Il2CppType t2361_0_0_49;
FieldInfo t2361_f0_FieldInfo = 
{
	"_default", &t2361_0_0_49, &t2361_TI, offsetof(t2361_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2361_FIs[] =
{
	&t2361_f0_FieldInfo,
	NULL
};
static PropertyInfo t2361____Default_PropertyInfo = 
{
	&t2361_TI, "Default", &m12170_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2361_PIs[] =
{
	&t2361____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12166_GM;
MethodInfo m12166_MI = 
{
	".ctor", (methodPointerType)&m12166, &t2361_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12166_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12167_GM;
MethodInfo m12167_MI = 
{
	".cctor", (methodPointerType)&m12167, &t2361_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12167_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2361_m12168_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12168_GM;
MethodInfo m12168_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m12168, &t2361_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2361_m12168_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12168_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2361_m12169_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12169_GM;
MethodInfo m12169_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m12169, &t2361_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2361_m12169_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12169_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2361_m27425_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27425_GM;
MethodInfo m27425_MI = 
{
	"GetHashCode", NULL, &t2361_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t2361_m27425_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27425_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2361_m27426_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27426_GM;
MethodInfo m27426_MI = 
{
	"Equals", NULL, &t2361_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t44, t2361_m27426_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27426_GM};
extern Il2CppType t2361_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12170_GM;
MethodInfo m12170_MI = 
{
	"get_Default", (methodPointerType)&m12170, &t2361_TI, &t2361_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12170_GM};
static MethodInfo* t2361_MIs[] =
{
	&m12166_MI,
	&m12167_MI,
	&m12168_MI,
	&m12169_MI,
	&m27425_MI,
	&m27426_MI,
	&m12170_MI,
	NULL
};
static MethodInfo* t2361_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27426_MI,
	&m27425_MI,
	&m12169_MI,
	&m12168_MI,
	NULL,
	NULL,
};
extern TypeInfo t2344_TI;
extern TypeInfo t734_TI;
static TypeInfo* t2361_ITIs[] = 
{
	&t2344_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2361_IOs[] = 
{
	{ &t2344_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2361_0_0_0;
extern Il2CppType t2361_1_0_0;
struct t2361;
extern Il2CppGenericClass t2361_GC;
TypeInfo t2361_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2361_MIs, t2361_PIs, t2361_FIs, NULL, &t29_TI, NULL, NULL, &t2361_TI, t2361_ITIs, t2361_VT, &EmptyCustomAttributesCache, &t2361_TI, &t2361_0_0_0, &t2361_1_0_0, t2361_IOs, &t2361_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2361), 0, -1, sizeof(t2361_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#include "t2362.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2362_TI;
#include "t2362MD.h"

extern MethodInfo m26746_MI;


extern MethodInfo m12171_MI;
 void m12171 (t2362 * __this, MethodInfo* method){
	{
		m12166(__this, &m12166_MI);
		return;
	}
}
extern MethodInfo m12172_MI;
 int32_t m12172 (t2362 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t44_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m12173_MI;
 bool m12173 (t2362 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t44_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		bool L_4 = (bool)InterfaceFuncInvoker1< bool, int32_t >::Invoke(&m26746_MI, Box(InitializedTypeInfo(&t44_TI), &(*(&p0))), p1);
		return L_4;
	}
}
// Metadata Definition System.Collections.Generic.GenericEqualityComparer`1<System.Int32>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12171_GM;
MethodInfo m12171_MI = 
{
	".ctor", (methodPointerType)&m12171, &t2362_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12171_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2362_m12172_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12172_GM;
MethodInfo m12172_MI = 
{
	"GetHashCode", (methodPointerType)&m12172, &t2362_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t2362_m12172_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12172_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2362_m12173_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12173_GM;
MethodInfo m12173_MI = 
{
	"Equals", (methodPointerType)&m12173, &t2362_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t44, t2362_m12173_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12173_GM};
static MethodInfo* t2362_MIs[] =
{
	&m12171_MI,
	&m12172_MI,
	&m12173_MI,
	NULL
};
static MethodInfo* t2362_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12173_MI,
	&m12172_MI,
	&m12169_MI,
	&m12168_MI,
	&m12172_MI,
	&m12173_MI,
};
static Il2CppInterfaceOffsetPair t2362_IOs[] = 
{
	{ &t2344_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2362_0_0_0;
extern Il2CppType t2362_1_0_0;
struct t2362;
extern Il2CppGenericClass t2362_GC;
TypeInfo t2362_TI = 
{
	&g_mscorlib_dll_Image, NULL, "GenericEqualityComparer`1", "System.Collections.Generic", t2362_MIs, NULL, NULL, NULL, &t2361_TI, NULL, NULL, &t2362_TI, NULL, t2362_VT, &EmptyCustomAttributesCache, &t2362_TI, &t2362_0_0_0, &t2362_1_0_0, t2362_IOs, &t2362_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2362), 0, -1, 0, 0, -1, 1057024, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



 void m12174 (t2363 * __this, MethodInfo* method){
	{
		m12166(__this, &m12166_MI);
		return;
	}
}
extern MethodInfo m12175_MI;
 int32_t m12175 (t2363 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t44_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m12176_MI;
 bool m12176 (t2363 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		int32_t L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t44_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t44_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		int32_t L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t44_TI), &L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t44_TI), &(*(&p0))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12174_GM;
MethodInfo m12174_MI = 
{
	".ctor", (methodPointerType)&m12174, &t2363_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12174_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2363_m12175_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12175_GM;
MethodInfo m12175_MI = 
{
	"GetHashCode", (methodPointerType)&m12175, &t2363_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t2363_m12175_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12175_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2363_m12176_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12176_GM;
MethodInfo m12176_MI = 
{
	"Equals", (methodPointerType)&m12176, &t2363_TI, &t40_0_0_0, RuntimeInvoker_t40_t44_t44, t2363_m12176_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12176_GM};
static MethodInfo* t2363_MIs[] =
{
	&m12174_MI,
	&m12175_MI,
	&m12176_MI,
	NULL
};
static MethodInfo* t2363_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12176_MI,
	&m12175_MI,
	&m12169_MI,
	&m12168_MI,
	&m12175_MI,
	&m12176_MI,
};
static Il2CppInterfaceOffsetPair t2363_IOs[] = 
{
	{ &t2344_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2363_0_0_0;
extern Il2CppType t2363_1_0_0;
struct t2363;
extern Il2CppGenericClass t2363_GC;
extern TypeInfo t1256_TI;
TypeInfo t2363_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2363_MIs, NULL, NULL, NULL, &t2361_TI, NULL, &t1256_TI, &t2363_TI, NULL, t2363_VT, &EmptyCustomAttributesCache, &t2363_TI, &t2363_0_0_0, &t2363_1_0_0, t2363_IOs, &t2363_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2363), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6642_TI;



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t6_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t6642_m27291_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27291_GM;
MethodInfo m27291_MI = 
{
	"Equals", NULL, &t6642_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6642_m27291_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27291_GM};
extern Il2CppType t6_0_0_0;
static ParameterInfo t6642_m27427_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27427_GM;
MethodInfo m27427_MI = 
{
	"GetHashCode", NULL, &t6642_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6642_m27427_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27427_GM};
static MethodInfo* t6642_MIs[] =
{
	&m27291_MI,
	&m27427_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6642_0_0_0;
extern Il2CppType t6642_1_0_0;
struct t6642;
extern Il2CppGenericClass t6642_GC;
TypeInfo t6642_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6642_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6642_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6642_TI, &t6642_0_0_0, &t6642_1_0_0, NULL, &t6642_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#include "t2364.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2364_TI;
#include "t2364MD.h"

#include "t2365.h"
extern TypeInfo t6649_TI;
extern TypeInfo t2365_TI;
#include "t2365MD.h"
extern Il2CppType t6649_0_0_0;
extern MethodInfo m12182_MI;
extern MethodInfo m27428_MI;
extern MethodInfo m27292_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t2364_0_0_49;
FieldInfo t2364_f0_FieldInfo = 
{
	"_default", &t2364_0_0_49, &t2364_TI, offsetof(t2364_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2364_FIs[] =
{
	&t2364_f0_FieldInfo,
	NULL
};
extern MethodInfo m12181_MI;
static PropertyInfo t2364____Default_PropertyInfo = 
{
	&t2364_TI, "Default", &m12181_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2364_PIs[] =
{
	&t2364____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12177_GM;
MethodInfo m12177_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2364_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12177_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12178_GM;
MethodInfo m12178_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2364_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12178_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2364_m12179_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12179_GM;
MethodInfo m12179_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2364_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2364_m12179_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12179_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2364_m12180_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12180_GM;
MethodInfo m12180_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2364_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2364_m12180_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12180_GM};
extern Il2CppType t6_0_0_0;
static ParameterInfo t2364_m27428_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27428_GM;
MethodInfo m27428_MI = 
{
	"GetHashCode", NULL, &t2364_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2364_m27428_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27428_GM};
extern Il2CppType t6_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t2364_m27292_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27292_GM;
MethodInfo m27292_MI = 
{
	"Equals", NULL, &t2364_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2364_m27292_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27292_GM};
extern Il2CppType t2364_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12181_GM;
MethodInfo m12181_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2364_TI, &t2364_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12181_GM};
static MethodInfo* t2364_MIs[] =
{
	&m12177_MI,
	&m12178_MI,
	&m12179_MI,
	&m12180_MI,
	&m27428_MI,
	&m27292_MI,
	&m12181_MI,
	NULL
};
extern MethodInfo m12180_MI;
extern MethodInfo m12179_MI;
static MethodInfo* t2364_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27292_MI,
	&m27428_MI,
	&m12180_MI,
	&m12179_MI,
	NULL,
	NULL,
};
static TypeInfo* t2364_ITIs[] = 
{
	&t6642_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2364_IOs[] = 
{
	{ &t6642_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2364_TI;
extern TypeInfo t2364_TI;
extern TypeInfo t2365_TI;
extern TypeInfo t6_TI;
static Il2CppRGCTXData t2364_RGCTXData[9] = 
{
	&t6649_0_0_0/* Type Usage */,
	&t6_0_0_0/* Type Usage */,
	&t2364_TI/* Class Usage */,
	&t2364_TI/* Static Usage */,
	&t2365_TI/* Class Usage */,
	&m12182_MI/* Method Usage */,
	&t6_TI/* Class Usage */,
	&m27428_MI/* Method Usage */,
	&m27292_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2364_0_0_0;
extern Il2CppType t2364_1_0_0;
struct t2364;
extern Il2CppGenericClass t2364_GC;
TypeInfo t2364_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2364_MIs, t2364_PIs, t2364_FIs, NULL, &t29_TI, NULL, NULL, &t2364_TI, t2364_ITIs, t2364_VT, &EmptyCustomAttributesCache, &t2364_TI, &t2364_0_0_0, &t2364_1_0_0, t2364_IOs, &t2364_GC, NULL, NULL, NULL, t2364_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2364), 0, -1, sizeof(t2364_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t6_0_0_0;
static ParameterInfo t6649_m27429_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27429_GM;
MethodInfo m27429_MI = 
{
	"Equals", NULL, &t6649_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6649_m27429_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27429_GM};
static MethodInfo* t6649_MIs[] =
{
	&m27429_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6649_1_0_0;
struct t6649;
extern Il2CppGenericClass t6649_GC;
TypeInfo t6649_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6649_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6649_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6649_TI, &t6649_0_0_0, &t6649_1_0_0, NULL, &t6649_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12177_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.PointerEventData>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12182_GM;
MethodInfo m12182_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2365_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12182_GM};
extern Il2CppType t6_0_0_0;
static ParameterInfo t2365_m12183_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12183_GM;
MethodInfo m12183_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2365_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2365_m12183_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12183_GM};
extern Il2CppType t6_0_0_0;
extern Il2CppType t6_0_0_0;
static ParameterInfo t2365_m12184_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t6_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12184_GM;
MethodInfo m12184_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2365_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2365_m12184_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12184_GM};
static MethodInfo* t2365_MIs[] =
{
	&m12182_MI,
	&m12183_MI,
	&m12184_MI,
	NULL
};
extern MethodInfo m12184_MI;
extern MethodInfo m12183_MI;
static MethodInfo* t2365_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12184_MI,
	&m12183_MI,
	&m12180_MI,
	&m12179_MI,
	&m12183_MI,
	&m12184_MI,
};
static Il2CppInterfaceOffsetPair t2365_IOs[] = 
{
	{ &t6642_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2364_TI;
extern TypeInfo t2364_TI;
extern TypeInfo t2365_TI;
extern TypeInfo t6_TI;
extern TypeInfo t6_TI;
static Il2CppRGCTXData t2365_RGCTXData[11] = 
{
	&t6649_0_0_0/* Type Usage */,
	&t6_0_0_0/* Type Usage */,
	&t2364_TI/* Class Usage */,
	&t2364_TI/* Static Usage */,
	&t2365_TI/* Class Usage */,
	&m12182_MI/* Method Usage */,
	&t6_TI/* Class Usage */,
	&m27428_MI/* Method Usage */,
	&m27292_MI/* Method Usage */,
	&m12177_MI/* Method Usage */,
	&t6_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2365_0_0_0;
extern Il2CppType t2365_1_0_0;
struct t2365;
extern Il2CppGenericClass t2365_GC;
TypeInfo t2365_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2365_MIs, NULL, NULL, NULL, &t2364_TI, NULL, &t1256_TI, &t2365_TI, NULL, t2365_VT, &EmptyCustomAttributesCache, &t2365_TI, &t2365_0_0_0, &t2365_1_0_0, t2365_IOs, &t2365_GC, NULL, NULL, NULL, t2365_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2365), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t116.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t116_TI;
#include "t116MD.h"

#include "t113.h"
#include "t305.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "t2373.h"
#include "t2370.h"
#include "t2371.h"
#include "t338.h"
#include "t2379.h"
#include "t2372.h"
extern TypeInfo t113_TI;
extern TypeInfo t21_TI;
extern TypeInfo t305_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2366_TI;
extern TypeInfo t2373_TI;
extern TypeInfo t2368_TI;
extern TypeInfo t2369_TI;
extern TypeInfo t2367_TI;
extern TypeInfo t2370_TI;
extern TypeInfo t338_TI;
extern TypeInfo t2371_TI;
extern TypeInfo t2379_TI;
#include "t305MD.h"
#include "t915MD.h"
#include "t602MD.h"
#include "t2370MD.h"
#include "t338MD.h"
#include "t2371MD.h"
#include "t2373MD.h"
#include "t2379MD.h"
extern MethodInfo m1406_MI;
extern MethodInfo m12229_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m20546_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m12216_MI;
extern MethodInfo m12213_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m1408_MI;
extern MethodInfo m12208_MI;
extern MethodInfo m12214_MI;
extern MethodInfo m12217_MI;
extern MethodInfo m12219_MI;
extern MethodInfo m12202_MI;
extern MethodInfo m12227_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m12228_MI;
extern MethodInfo m27430_MI;
extern MethodInfo m27431_MI;
extern MethodInfo m27432_MI;
extern MethodInfo m27433_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m12218_MI;
extern MethodInfo m12203_MI;
extern MethodInfo m12204_MI;
extern MethodInfo m12241_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m20548_MI;
extern MethodInfo m12211_MI;
extern MethodInfo m12212_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m12316_MI;
extern MethodInfo m12235_MI;
extern MethodInfo m12215_MI;
extern MethodInfo m12221_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m12322_MI;
extern MethodInfo m20550_MI;
extern MethodInfo m20558_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m20546(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2377.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m20548(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m20550(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m20558(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2373  m12213 (t116 * __this, MethodInfo* method){
	{
		t2373  L_0 = {0};
		m12235(&L_0, __this, &m12235_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t44_0_0_32849;
FieldInfo t116_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t116_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2366_0_0_1;
FieldInfo t116_f1_FieldInfo = 
{
	"_items", &t2366_0_0_1, &t116_TI, offsetof(t116, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t116_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t116_TI, offsetof(t116, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t116_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t116_TI, offsetof(t116, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2366_0_0_49;
FieldInfo t116_f4_FieldInfo = 
{
	"EmptyArray", &t2366_0_0_49, &t116_TI, offsetof(t116_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t116_FIs[] =
{
	&t116_f0_FieldInfo,
	&t116_f1_FieldInfo,
	&t116_f2_FieldInfo,
	&t116_f3_FieldInfo,
	&t116_f4_FieldInfo,
	NULL
};
static const int32_t t116_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t116_f0_DefaultValue = 
{
	&t116_f0_FieldInfo, { (char*)&t116_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t116_FDVs[] = 
{
	&t116_f0_DefaultValue,
	NULL
};
extern MethodInfo m12195_MI;
static PropertyInfo t116____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t116_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m12195_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12196_MI;
static PropertyInfo t116____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t116_TI, "System.Collections.ICollection.IsSynchronized", &m12196_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12197_MI;
static PropertyInfo t116____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t116_TI, "System.Collections.ICollection.SyncRoot", &m12197_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12198_MI;
static PropertyInfo t116____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t116_TI, "System.Collections.IList.IsFixedSize", &m12198_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12199_MI;
static PropertyInfo t116____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t116_TI, "System.Collections.IList.IsReadOnly", &m12199_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12200_MI;
extern MethodInfo m12201_MI;
static PropertyInfo t116____System_Collections_IList_Item_PropertyInfo = 
{
	&t116_TI, "System.Collections.IList.Item", &m12200_MI, &m12201_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t116____Capacity_PropertyInfo = 
{
	&t116_TI, "Capacity", &m12227_MI, &m12228_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1407_MI;
static PropertyInfo t116____Count_PropertyInfo = 
{
	&t116_TI, "Count", &m1407_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t116____Item_PropertyInfo = 
{
	&t116_TI, "Item", &m1406_MI, &m12229_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t116_PIs[] =
{
	&t116____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t116____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t116____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t116____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t116____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t116____System_Collections_IList_Item_PropertyInfo,
	&t116____Capacity_PropertyInfo,
	&t116____Count_PropertyInfo,
	&t116____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1405_GM;
MethodInfo m1405_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1405_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t116_m12185_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12185_GM;
MethodInfo m12185_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t116_m12185_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12185_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12186_GM;
MethodInfo m12186_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12186_GM};
extern Il2CppType t2367_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12187_GM;
MethodInfo m12187_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t116_TI, &t2367_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12187_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t116_m12188_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12188_GM;
MethodInfo m12188_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t116_m12188_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12188_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12189_GM;
MethodInfo m12189_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t116_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12189_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t116_m12190_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12190_GM;
MethodInfo m12190_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t116_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t116_m12190_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12190_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t116_m12191_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12191_GM;
MethodInfo m12191_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t116_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t116_m12191_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12191_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t116_m12192_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12192_GM;
MethodInfo m12192_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t116_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t116_m12192_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12192_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t116_m12193_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12193_GM;
MethodInfo m12193_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t116_m12193_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12193_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t116_m12194_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12194_GM;
MethodInfo m12194_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t116_m12194_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12194_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12195_GM;
MethodInfo m12195_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t116_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12195_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12196_GM;
MethodInfo m12196_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t116_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12196_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12197_GM;
MethodInfo m12197_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t116_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12197_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12198_GM;
MethodInfo m12198_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t116_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12198_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12199_GM;
MethodInfo m12199_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t116_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12199_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t116_m12200_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12200_GM;
MethodInfo m12200_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t116_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t116_m12200_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12200_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t116_m12201_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12201_GM;
MethodInfo m12201_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t116_m12201_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12201_GM};
extern Il2CppType t113_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t116_m1408_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1408_GM;
MethodInfo m1408_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t116_m1408_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1408_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t116_m12202_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12202_GM;
MethodInfo m12202_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t116_m12202_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12202_GM};
extern Il2CppType t2368_0_0_0;
extern Il2CppType t2368_0_0_0;
static ParameterInfo t116_m12203_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2368_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12203_GM;
MethodInfo m12203_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t116_m12203_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12203_GM};
extern Il2CppType t2369_0_0_0;
extern Il2CppType t2369_0_0_0;
static ParameterInfo t116_m12204_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2369_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12204_GM;
MethodInfo m12204_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t116_m12204_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12204_GM};
extern Il2CppType t2369_0_0_0;
static ParameterInfo t116_m12205_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2369_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12205_GM;
MethodInfo m12205_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t116_m12205_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12205_GM};
extern Il2CppType t2370_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12206_GM;
MethodInfo m12206_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t116_TI, &t2370_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12206_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12207_GM;
MethodInfo m12207_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12207_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t116_m12208_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12208_GM;
MethodInfo m12208_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t116_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t116_m12208_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12208_GM};
extern Il2CppType t2366_0_0_0;
extern Il2CppType t2366_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t116_m12209_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2366_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12209_GM;
MethodInfo m12209_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t116_m12209_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12209_GM};
extern Il2CppType t2371_0_0_0;
extern Il2CppType t2371_0_0_0;
static ParameterInfo t116_m12210_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2371_0_0_0},
};
extern Il2CppType t113_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12210_GM;
MethodInfo m12210_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t116_TI, &t113_0_0_0, RuntimeInvoker_t29_t29, t116_m12210_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12210_GM};
extern Il2CppType t2371_0_0_0;
static ParameterInfo t116_m12211_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2371_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12211_GM;
MethodInfo m12211_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t116_m12211_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12211_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2371_0_0_0;
static ParameterInfo t116_m12212_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2371_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12212_GM;
MethodInfo m12212_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t116_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t116_m12212_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12212_GM};
extern Il2CppType t2373_0_0_0;
extern void* RuntimeInvoker_t2373 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12213_GM;
MethodInfo m12213_MI = 
{
	"GetEnumerator", (methodPointerType)&m12213, &t116_TI, &t2373_0_0_0, RuntimeInvoker_t2373, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12213_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t116_m12214_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12214_GM;
MethodInfo m12214_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t116_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t116_m12214_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12214_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t116_m12215_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12215_GM;
MethodInfo m12215_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t116_m12215_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12215_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t116_m12216_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12216_GM;
MethodInfo m12216_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t116_m12216_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12216_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t116_m12217_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12217_GM;
MethodInfo m12217_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t116_m12217_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12217_GM};
extern Il2CppType t2369_0_0_0;
static ParameterInfo t116_m12218_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2369_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12218_GM;
MethodInfo m12218_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t116_m12218_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12218_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t116_m12219_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12219_GM;
MethodInfo m12219_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t116_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t116_m12219_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12219_GM};
extern Il2CppType t2371_0_0_0;
static ParameterInfo t116_m12220_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2371_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12220_GM;
MethodInfo m12220_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t116_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t116_m12220_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12220_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t116_m12221_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12221_GM;
MethodInfo m12221_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t116_m12221_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12221_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12222_GM;
MethodInfo m12222_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12222_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12223_GM;
MethodInfo m12223_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12223_GM};
extern Il2CppType t2372_0_0_0;
extern Il2CppType t2372_0_0_0;
static ParameterInfo t116_m12224_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2372_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12224_GM;
MethodInfo m12224_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t116_m12224_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12224_GM};
extern Il2CppType t2366_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12225_GM;
MethodInfo m12225_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t116_TI, &t2366_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12225_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12226_GM;
MethodInfo m12226_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12226_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12227_GM;
MethodInfo m12227_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t116_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12227_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t116_m12228_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12228_GM;
MethodInfo m12228_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t116_m12228_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12228_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1407_GM;
MethodInfo m1407_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t116_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1407_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t116_m1406_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t113_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1406_GM;
MethodInfo m1406_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t116_TI, &t113_0_0_0, RuntimeInvoker_t29_t44, t116_m1406_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1406_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t116_m12229_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12229_GM;
MethodInfo m12229_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t116_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t116_m12229_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12229_GM};
static MethodInfo* t116_MIs[] =
{
	&m1405_MI,
	&m12185_MI,
	&m12186_MI,
	&m12187_MI,
	&m12188_MI,
	&m12189_MI,
	&m12190_MI,
	&m12191_MI,
	&m12192_MI,
	&m12193_MI,
	&m12194_MI,
	&m12195_MI,
	&m12196_MI,
	&m12197_MI,
	&m12198_MI,
	&m12199_MI,
	&m12200_MI,
	&m12201_MI,
	&m1408_MI,
	&m12202_MI,
	&m12203_MI,
	&m12204_MI,
	&m12205_MI,
	&m12206_MI,
	&m12207_MI,
	&m12208_MI,
	&m12209_MI,
	&m12210_MI,
	&m12211_MI,
	&m12212_MI,
	&m12213_MI,
	&m12214_MI,
	&m12215_MI,
	&m12216_MI,
	&m12217_MI,
	&m12218_MI,
	&m12219_MI,
	&m12220_MI,
	&m12221_MI,
	&m12222_MI,
	&m12223_MI,
	&m12224_MI,
	&m12225_MI,
	&m12226_MI,
	&m12227_MI,
	&m12228_MI,
	&m1407_MI,
	&m1406_MI,
	&m12229_MI,
	NULL
};
extern MethodInfo m12189_MI;
extern MethodInfo m12188_MI;
extern MethodInfo m12190_MI;
extern MethodInfo m12207_MI;
extern MethodInfo m12191_MI;
extern MethodInfo m12192_MI;
extern MethodInfo m12193_MI;
extern MethodInfo m12194_MI;
extern MethodInfo m12209_MI;
extern MethodInfo m12187_MI;
static MethodInfo* t116_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12189_MI,
	&m1407_MI,
	&m12196_MI,
	&m12197_MI,
	&m12188_MI,
	&m12198_MI,
	&m12199_MI,
	&m12200_MI,
	&m12201_MI,
	&m12190_MI,
	&m12207_MI,
	&m12191_MI,
	&m12192_MI,
	&m12193_MI,
	&m12194_MI,
	&m12221_MI,
	&m1407_MI,
	&m12195_MI,
	&m1408_MI,
	&m12207_MI,
	&m12208_MI,
	&m12209_MI,
	&m12219_MI,
	&m12187_MI,
	&m12214_MI,
	&m12217_MI,
	&m12221_MI,
	&m1406_MI,
	&m12229_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
extern TypeInfo t2375_TI;
static TypeInfo* t116_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2368_TI,
	&t2369_TI,
	&t2375_TI,
};
static Il2CppInterfaceOffsetPair t116_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2368_TI, 20},
	{ &t2369_TI, 27},
	{ &t2375_TI, 28},
};
extern TypeInfo t116_TI;
extern TypeInfo t2366_TI;
extern TypeInfo t2373_TI;
extern TypeInfo t113_TI;
extern TypeInfo t2368_TI;
extern TypeInfo t2370_TI;
static Il2CppRGCTXData t116_RGCTXData[37] = 
{
	&t116_TI/* Static Usage */,
	&t2366_TI/* Array Usage */,
	&m12213_MI/* Method Usage */,
	&t2373_TI/* Class Usage */,
	&t113_TI/* Class Usage */,
	&m1408_MI/* Method Usage */,
	&m12208_MI/* Method Usage */,
	&m12214_MI/* Method Usage */,
	&m12216_MI/* Method Usage */,
	&m12217_MI/* Method Usage */,
	&m12219_MI/* Method Usage */,
	&m1406_MI/* Method Usage */,
	&m12229_MI/* Method Usage */,
	&m12202_MI/* Method Usage */,
	&m12227_MI/* Method Usage */,
	&m12228_MI/* Method Usage */,
	&m27430_MI/* Method Usage */,
	&m27431_MI/* Method Usage */,
	&m27432_MI/* Method Usage */,
	&m27433_MI/* Method Usage */,
	&m12218_MI/* Method Usage */,
	&t2368_TI/* Class Usage */,
	&m12203_MI/* Method Usage */,
	&m12204_MI/* Method Usage */,
	&t2370_TI/* Class Usage */,
	&m12241_MI/* Method Usage */,
	&m20548_MI/* Method Usage */,
	&m12211_MI/* Method Usage */,
	&m12212_MI/* Method Usage */,
	&m12316_MI/* Method Usage */,
	&m12235_MI/* Method Usage */,
	&m12215_MI/* Method Usage */,
	&m12221_MI/* Method Usage */,
	&m12322_MI/* Method Usage */,
	&m20550_MI/* Method Usage */,
	&m20558_MI/* Method Usage */,
	&m20546_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t116_0_0_0;
extern Il2CppType t116_1_0_0;
struct t116;
extern Il2CppGenericClass t116_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t116_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t116_MIs, t116_PIs, t116_FIs, NULL, &t29_TI, NULL, NULL, &t116_TI, t116_ITIs, t116_VT, &t1261__CustomAttributeCache, &t116_TI, &t116_0_0_0, &t116_1_0_0, t116_IOs, &t116_GC, NULL, t116_FDVs, NULL, t116_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t116), 0, -1, sizeof(t116_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
static PropertyInfo t2368____Count_PropertyInfo = 
{
	&t2368_TI, "Count", &m27430_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27434_MI;
static PropertyInfo t2368____IsReadOnly_PropertyInfo = 
{
	&t2368_TI, "IsReadOnly", &m27434_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2368_PIs[] =
{
	&t2368____Count_PropertyInfo,
	&t2368____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27430_GM;
MethodInfo m27430_MI = 
{
	"get_Count", NULL, &t2368_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27430_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27434_GM;
MethodInfo m27434_MI = 
{
	"get_IsReadOnly", NULL, &t2368_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27434_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2368_m27435_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27435_GM;
MethodInfo m27435_MI = 
{
	"Add", NULL, &t2368_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2368_m27435_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27435_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27436_GM;
MethodInfo m27436_MI = 
{
	"Clear", NULL, &t2368_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27436_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2368_m27437_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27437_GM;
MethodInfo m27437_MI = 
{
	"Contains", NULL, &t2368_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2368_m27437_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27437_GM};
extern Il2CppType t2366_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2368_m27431_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2366_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27431_GM;
MethodInfo m27431_MI = 
{
	"CopyTo", NULL, &t2368_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2368_m27431_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27431_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2368_m27438_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27438_GM;
MethodInfo m27438_MI = 
{
	"Remove", NULL, &t2368_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2368_m27438_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27438_GM};
static MethodInfo* t2368_MIs[] =
{
	&m27430_MI,
	&m27434_MI,
	&m27435_MI,
	&m27436_MI,
	&m27437_MI,
	&m27431_MI,
	&m27438_MI,
	NULL
};
static TypeInfo* t2368_ITIs[] = 
{
	&t603_TI,
	&t2369_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2368_1_0_0;
struct t2368;
extern Il2CppGenericClass t2368_GC;
TypeInfo t2368_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2368_MIs, t2368_PIs, NULL, NULL, NULL, NULL, NULL, &t2368_TI, t2368_ITIs, NULL, &EmptyCustomAttributesCache, &t2368_TI, &t2368_0_0_0, &t2368_1_0_0, NULL, &t2368_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t2367_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27432_GM;
MethodInfo m27432_MI = 
{
	"GetEnumerator", NULL, &t2369_TI, &t2367_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27432_GM};
static MethodInfo* t2369_MIs[] =
{
	&m27432_MI,
	NULL
};
static TypeInfo* t2369_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2369_1_0_0;
struct t2369;
extern Il2CppGenericClass t2369_GC;
TypeInfo t2369_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2369_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2369_TI, t2369_ITIs, NULL, &EmptyCustomAttributesCache, &t2369_TI, &t2369_0_0_0, &t2369_1_0_0, NULL, &t2369_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
static PropertyInfo t2367____Current_PropertyInfo = 
{
	&t2367_TI, "Current", &m27433_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2367_PIs[] =
{
	&t2367____Current_PropertyInfo,
	NULL
};
extern Il2CppType t113_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27433_GM;
MethodInfo m27433_MI = 
{
	"get_Current", NULL, &t2367_TI, &t113_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27433_GM};
static MethodInfo* t2367_MIs[] =
{
	&m27433_MI,
	NULL
};
static TypeInfo* t2367_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2367_0_0_0;
extern Il2CppType t2367_1_0_0;
struct t2367;
extern Il2CppGenericClass t2367_GC;
TypeInfo t2367_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2367_MIs, t2367_PIs, NULL, NULL, NULL, NULL, NULL, &t2367_TI, t2367_ITIs, NULL, &EmptyCustomAttributesCache, &t2367_TI, &t2367_0_0_0, &t2367_1_0_0, NULL, &t2367_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2374.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2374_TI;
#include "t2374MD.h"

extern MethodInfo m12234_MI;
extern MethodInfo m20535_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m20535(__this, p0, method) (t113 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t20_0_0_1;
FieldInfo t2374_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2374_TI, offsetof(t2374, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2374_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2374_TI, offsetof(t2374, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2374_FIs[] =
{
	&t2374_f0_FieldInfo,
	&t2374_f1_FieldInfo,
	NULL
};
extern MethodInfo m12231_MI;
static PropertyInfo t2374____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2374_TI, "System.Collections.IEnumerator.Current", &m12231_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2374____Current_PropertyInfo = 
{
	&t2374_TI, "Current", &m12234_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2374_PIs[] =
{
	&t2374____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2374____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2374_m12230_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12230_GM;
MethodInfo m12230_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2374_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2374_m12230_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12230_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12231_GM;
MethodInfo m12231_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2374_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12231_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12232_GM;
MethodInfo m12232_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2374_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12232_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12233_GM;
MethodInfo m12233_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2374_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12233_GM};
extern Il2CppType t113_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12234_GM;
MethodInfo m12234_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2374_TI, &t113_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12234_GM};
static MethodInfo* t2374_MIs[] =
{
	&m12230_MI,
	&m12231_MI,
	&m12232_MI,
	&m12233_MI,
	&m12234_MI,
	NULL
};
extern MethodInfo m12233_MI;
extern MethodInfo m12232_MI;
static MethodInfo* t2374_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12231_MI,
	&m12233_MI,
	&m12232_MI,
	&m12234_MI,
};
static TypeInfo* t2374_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2367_TI,
};
static Il2CppInterfaceOffsetPair t2374_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2367_TI, 7},
};
extern TypeInfo t113_TI;
static Il2CppRGCTXData t2374_RGCTXData[3] = 
{
	&m12234_MI/* Method Usage */,
	&t113_TI/* Class Usage */,
	&m20535_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2374_0_0_0;
extern Il2CppType t2374_1_0_0;
extern Il2CppGenericClass t2374_GC;
TypeInfo t2374_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2374_MIs, t2374_PIs, t2374_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2374_TI, t2374_ITIs, t2374_VT, &EmptyCustomAttributesCache, &t2374_TI, &t2374_0_0_0, &t2374_1_0_0, t2374_IOs, &t2374_GC, NULL, NULL, NULL, t2374_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2374)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern MethodInfo m27439_MI;
extern MethodInfo m27440_MI;
static PropertyInfo t2375____Item_PropertyInfo = 
{
	&t2375_TI, "Item", &m27439_MI, &m27440_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2375_PIs[] =
{
	&t2375____Item_PropertyInfo,
	NULL
};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2375_m27441_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27441_GM;
MethodInfo m27441_MI = 
{
	"IndexOf", NULL, &t2375_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2375_m27441_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27441_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2375_m27442_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27442_GM;
MethodInfo m27442_MI = 
{
	"Insert", NULL, &t2375_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2375_m27442_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27442_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2375_m27443_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27443_GM;
MethodInfo m27443_MI = 
{
	"RemoveAt", NULL, &t2375_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2375_m27443_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27443_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2375_m27439_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t113_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27439_GM;
MethodInfo m27439_MI = 
{
	"get_Item", NULL, &t2375_TI, &t113_0_0_0, RuntimeInvoker_t29_t44, t2375_m27439_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27439_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2375_m27440_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27440_GM;
MethodInfo m27440_MI = 
{
	"set_Item", NULL, &t2375_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2375_m27440_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27440_GM};
static MethodInfo* t2375_MIs[] =
{
	&m27441_MI,
	&m27442_MI,
	&m27443_MI,
	&m27439_MI,
	&m27440_MI,
	NULL
};
static TypeInfo* t2375_ITIs[] = 
{
	&t603_TI,
	&t2368_TI,
	&t2369_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2375_0_0_0;
extern Il2CppType t2375_1_0_0;
struct t2375;
extern Il2CppGenericClass t2375_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2375_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2375_MIs, t2375_PIs, NULL, NULL, NULL, NULL, NULL, &t2375_TI, t2375_ITIs, NULL, &t1908__CustomAttributeCache, &t2375_TI, &t2375_0_0_0, &t2375_1_0_0, NULL, &t2375_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m12238_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t116_0_0_1;
FieldInfo t2373_f0_FieldInfo = 
{
	"l", &t116_0_0_1, &t2373_TI, offsetof(t2373, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2373_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2373_TI, offsetof(t2373, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2373_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2373_TI, offsetof(t2373, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t113_0_0_1;
FieldInfo t2373_f3_FieldInfo = 
{
	"current", &t113_0_0_1, &t2373_TI, offsetof(t2373, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2373_FIs[] =
{
	&t2373_f0_FieldInfo,
	&t2373_f1_FieldInfo,
	&t2373_f2_FieldInfo,
	&t2373_f3_FieldInfo,
	NULL
};
extern MethodInfo m12236_MI;
static PropertyInfo t2373____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2373_TI, "System.Collections.IEnumerator.Current", &m12236_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12240_MI;
static PropertyInfo t2373____Current_PropertyInfo = 
{
	&t2373_TI, "Current", &m12240_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2373_PIs[] =
{
	&t2373____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2373____Current_PropertyInfo,
	NULL
};
extern Il2CppType t116_0_0_0;
static ParameterInfo t2373_m12235_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t116_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12235_GM;
MethodInfo m12235_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2373_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2373_m12235_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12235_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12236_GM;
MethodInfo m12236_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2373_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12236_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12237_GM;
MethodInfo m12237_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2373_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12237_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12238_GM;
MethodInfo m12238_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2373_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12238_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12239_GM;
MethodInfo m12239_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2373_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12239_GM};
extern Il2CppType t113_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12240_GM;
MethodInfo m12240_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2373_TI, &t113_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12240_GM};
static MethodInfo* t2373_MIs[] =
{
	&m12235_MI,
	&m12236_MI,
	&m12237_MI,
	&m12238_MI,
	&m12239_MI,
	&m12240_MI,
	NULL
};
extern MethodInfo m12239_MI;
extern MethodInfo m12237_MI;
static MethodInfo* t2373_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12236_MI,
	&m12239_MI,
	&m12237_MI,
	&m12240_MI,
};
static TypeInfo* t2373_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2367_TI,
};
static Il2CppInterfaceOffsetPair t2373_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2367_TI, 7},
};
extern TypeInfo t113_TI;
extern TypeInfo t2373_TI;
static Il2CppRGCTXData t2373_RGCTXData[3] = 
{
	&m12238_MI/* Method Usage */,
	&t113_TI/* Class Usage */,
	&t2373_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2373_0_0_0;
extern Il2CppType t2373_1_0_0;
extern Il2CppGenericClass t2373_GC;
extern TypeInfo t1261_TI;
TypeInfo t2373_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2373_MIs, t2373_PIs, t2373_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2373_TI, t2373_ITIs, t2373_VT, &EmptyCustomAttributesCache, &t2373_TI, &t2373_0_0_0, &t2373_1_0_0, t2373_IOs, &t2373_GC, NULL, NULL, NULL, t2373_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2373)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t2376MD.h"
extern MethodInfo m12270_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m12302_MI;
extern MethodInfo m27437_MI;
extern MethodInfo m27441_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t2375_0_0_1;
FieldInfo t2370_f0_FieldInfo = 
{
	"list", &t2375_0_0_1, &t2370_TI, offsetof(t2370, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2370_FIs[] =
{
	&t2370_f0_FieldInfo,
	NULL
};
extern MethodInfo m12247_MI;
extern MethodInfo m12248_MI;
static PropertyInfo t2370____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2370_TI, "System.Collections.Generic.IList<T>.Item", &m12247_MI, &m12248_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12249_MI;
static PropertyInfo t2370____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2370_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m12249_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12259_MI;
static PropertyInfo t2370____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2370_TI, "System.Collections.ICollection.IsSynchronized", &m12259_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12260_MI;
static PropertyInfo t2370____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2370_TI, "System.Collections.ICollection.SyncRoot", &m12260_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12261_MI;
static PropertyInfo t2370____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2370_TI, "System.Collections.IList.IsFixedSize", &m12261_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12262_MI;
static PropertyInfo t2370____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2370_TI, "System.Collections.IList.IsReadOnly", &m12262_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12263_MI;
extern MethodInfo m12264_MI;
static PropertyInfo t2370____System_Collections_IList_Item_PropertyInfo = 
{
	&t2370_TI, "System.Collections.IList.Item", &m12263_MI, &m12264_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12269_MI;
static PropertyInfo t2370____Count_PropertyInfo = 
{
	&t2370_TI, "Count", &m12269_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2370____Item_PropertyInfo = 
{
	&t2370_TI, "Item", &m12270_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2370_PIs[] =
{
	&t2370____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2370____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2370____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2370____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2370____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2370____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2370____System_Collections_IList_Item_PropertyInfo,
	&t2370____Count_PropertyInfo,
	&t2370____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2375_0_0_0;
static ParameterInfo t2370_m12241_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2375_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12241_GM;
MethodInfo m12241_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2370_m12241_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12241_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2370_m12242_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12242_GM;
MethodInfo m12242_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2370_m12242_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12242_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12243_GM;
MethodInfo m12243_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12243_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2370_m12244_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12244_GM;
MethodInfo m12244_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2370_m12244_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12244_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2370_m12245_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12245_GM;
MethodInfo m12245_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2370_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2370_m12245_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12245_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2370_m12246_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12246_GM;
MethodInfo m12246_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2370_m12246_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12246_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2370_m12247_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t113_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12247_GM;
MethodInfo m12247_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2370_TI, &t113_0_0_0, RuntimeInvoker_t29_t44, t2370_m12247_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12247_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2370_m12248_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12248_GM;
MethodInfo m12248_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2370_m12248_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12248_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12249_GM;
MethodInfo m12249_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2370_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12249_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2370_m12250_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12250_GM;
MethodInfo m12250_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2370_m12250_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12250_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12251_GM;
MethodInfo m12251_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2370_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12251_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2370_m12252_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12252_GM;
MethodInfo m12252_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2370_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2370_m12252_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12252_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12253_GM;
MethodInfo m12253_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12253_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2370_m12254_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12254_GM;
MethodInfo m12254_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2370_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2370_m12254_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12254_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2370_m12255_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12255_GM;
MethodInfo m12255_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2370_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2370_m12255_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12255_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2370_m12256_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12256_GM;
MethodInfo m12256_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2370_m12256_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12256_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2370_m12257_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12257_GM;
MethodInfo m12257_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2370_m12257_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12257_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2370_m12258_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12258_GM;
MethodInfo m12258_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2370_m12258_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12258_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12259_GM;
MethodInfo m12259_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2370_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12259_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12260_GM;
MethodInfo m12260_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2370_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12260_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12261_GM;
MethodInfo m12261_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2370_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12261_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12262_GM;
MethodInfo m12262_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2370_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12262_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2370_m12263_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12263_GM;
MethodInfo m12263_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2370_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2370_m12263_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12263_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2370_m12264_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12264_GM;
MethodInfo m12264_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2370_m12264_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12264_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2370_m12265_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12265_GM;
MethodInfo m12265_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2370_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2370_m12265_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12265_GM};
extern Il2CppType t2366_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2370_m12266_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2366_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12266_GM;
MethodInfo m12266_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2370_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2370_m12266_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12266_GM};
extern Il2CppType t2367_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12267_GM;
MethodInfo m12267_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2370_TI, &t2367_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12267_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2370_m12268_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12268_GM;
MethodInfo m12268_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2370_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2370_m12268_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12268_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12269_GM;
MethodInfo m12269_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2370_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12269_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2370_m12270_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t113_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12270_GM;
MethodInfo m12270_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2370_TI, &t113_0_0_0, RuntimeInvoker_t29_t44, t2370_m12270_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12270_GM};
static MethodInfo* t2370_MIs[] =
{
	&m12241_MI,
	&m12242_MI,
	&m12243_MI,
	&m12244_MI,
	&m12245_MI,
	&m12246_MI,
	&m12247_MI,
	&m12248_MI,
	&m12249_MI,
	&m12250_MI,
	&m12251_MI,
	&m12252_MI,
	&m12253_MI,
	&m12254_MI,
	&m12255_MI,
	&m12256_MI,
	&m12257_MI,
	&m12258_MI,
	&m12259_MI,
	&m12260_MI,
	&m12261_MI,
	&m12262_MI,
	&m12263_MI,
	&m12264_MI,
	&m12265_MI,
	&m12266_MI,
	&m12267_MI,
	&m12268_MI,
	&m12269_MI,
	&m12270_MI,
	NULL
};
extern MethodInfo m12251_MI;
extern MethodInfo m12250_MI;
extern MethodInfo m12252_MI;
extern MethodInfo m12253_MI;
extern MethodInfo m12254_MI;
extern MethodInfo m12255_MI;
extern MethodInfo m12256_MI;
extern MethodInfo m12257_MI;
extern MethodInfo m12258_MI;
extern MethodInfo m12242_MI;
extern MethodInfo m12243_MI;
extern MethodInfo m12265_MI;
extern MethodInfo m12266_MI;
extern MethodInfo m12245_MI;
extern MethodInfo m12268_MI;
extern MethodInfo m12244_MI;
extern MethodInfo m12246_MI;
extern MethodInfo m12267_MI;
static MethodInfo* t2370_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12251_MI,
	&m12269_MI,
	&m12259_MI,
	&m12260_MI,
	&m12250_MI,
	&m12261_MI,
	&m12262_MI,
	&m12263_MI,
	&m12264_MI,
	&m12252_MI,
	&m12253_MI,
	&m12254_MI,
	&m12255_MI,
	&m12256_MI,
	&m12257_MI,
	&m12258_MI,
	&m12269_MI,
	&m12249_MI,
	&m12242_MI,
	&m12243_MI,
	&m12265_MI,
	&m12266_MI,
	&m12245_MI,
	&m12268_MI,
	&m12244_MI,
	&m12246_MI,
	&m12247_MI,
	&m12248_MI,
	&m12267_MI,
	&m12270_MI,
};
static TypeInfo* t2370_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2368_TI,
	&t2375_TI,
	&t2369_TI,
};
static Il2CppInterfaceOffsetPair t2370_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2368_TI, 20},
	{ &t2375_TI, 27},
	{ &t2369_TI, 32},
};
extern TypeInfo t113_TI;
static Il2CppRGCTXData t2370_RGCTXData[9] = 
{
	&m12270_MI/* Method Usage */,
	&m12302_MI/* Method Usage */,
	&t113_TI/* Class Usage */,
	&m27437_MI/* Method Usage */,
	&m27441_MI/* Method Usage */,
	&m27439_MI/* Method Usage */,
	&m27431_MI/* Method Usage */,
	&m27432_MI/* Method Usage */,
	&m27430_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2370_0_0_0;
extern Il2CppType t2370_1_0_0;
struct t2370;
extern Il2CppGenericClass t2370_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2370_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2370_MIs, t2370_PIs, t2370_FIs, NULL, &t29_TI, NULL, NULL, &t2370_TI, t2370_ITIs, t2370_VT, &t1263__CustomAttributeCache, &t2370_TI, &t2370_0_0_0, &t2370_1_0_0, t2370_IOs, &t2370_GC, NULL, NULL, NULL, t2370_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2370), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2376.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2376_TI;

extern MethodInfo m12305_MI;
extern MethodInfo m12306_MI;
extern MethodInfo m12303_MI;
extern MethodInfo m12301_MI;
extern MethodInfo m1405_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m12294_MI;
extern MethodInfo m12304_MI;
extern MethodInfo m12292_MI;
extern MethodInfo m12297_MI;
extern MethodInfo m12288_MI;
extern MethodInfo m27436_MI;
extern MethodInfo m27442_MI;
extern MethodInfo m27443_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t2375_0_0_1;
FieldInfo t2376_f0_FieldInfo = 
{
	"list", &t2375_0_0_1, &t2376_TI, offsetof(t2376, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2376_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2376_TI, offsetof(t2376, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2376_FIs[] =
{
	&t2376_f0_FieldInfo,
	&t2376_f1_FieldInfo,
	NULL
};
extern MethodInfo m12272_MI;
static PropertyInfo t2376____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2376_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m12272_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12280_MI;
static PropertyInfo t2376____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2376_TI, "System.Collections.ICollection.IsSynchronized", &m12280_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12281_MI;
static PropertyInfo t2376____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2376_TI, "System.Collections.ICollection.SyncRoot", &m12281_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12282_MI;
static PropertyInfo t2376____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2376_TI, "System.Collections.IList.IsFixedSize", &m12282_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12283_MI;
static PropertyInfo t2376____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2376_TI, "System.Collections.IList.IsReadOnly", &m12283_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12284_MI;
extern MethodInfo m12285_MI;
static PropertyInfo t2376____System_Collections_IList_Item_PropertyInfo = 
{
	&t2376_TI, "System.Collections.IList.Item", &m12284_MI, &m12285_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12298_MI;
static PropertyInfo t2376____Count_PropertyInfo = 
{
	&t2376_TI, "Count", &m12298_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m12299_MI;
extern MethodInfo m12300_MI;
static PropertyInfo t2376____Item_PropertyInfo = 
{
	&t2376_TI, "Item", &m12299_MI, &m12300_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2376_PIs[] =
{
	&t2376____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2376____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2376____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2376____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2376____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2376____System_Collections_IList_Item_PropertyInfo,
	&t2376____Count_PropertyInfo,
	&t2376____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12271_GM;
MethodInfo m12271_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12271_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12272_GM;
MethodInfo m12272_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2376_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12272_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2376_m12273_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12273_GM;
MethodInfo m12273_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2376_m12273_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12273_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12274_GM;
MethodInfo m12274_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2376_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12274_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2376_m12275_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12275_GM;
MethodInfo m12275_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2376_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2376_m12275_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12275_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2376_m12276_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12276_GM;
MethodInfo m12276_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2376_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2376_m12276_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12276_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2376_m12277_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12277_GM;
MethodInfo m12277_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2376_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2376_m12277_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12277_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2376_m12278_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12278_GM;
MethodInfo m12278_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2376_m12278_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12278_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2376_m12279_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12279_GM;
MethodInfo m12279_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2376_m12279_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12279_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12280_GM;
MethodInfo m12280_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2376_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12280_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12281_GM;
MethodInfo m12281_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2376_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12281_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12282_GM;
MethodInfo m12282_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2376_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12282_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12283_GM;
MethodInfo m12283_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2376_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12283_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2376_m12284_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12284_GM;
MethodInfo m12284_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2376_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2376_m12284_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12284_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2376_m12285_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12285_GM;
MethodInfo m12285_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2376_m12285_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12285_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2376_m12286_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12286_GM;
MethodInfo m12286_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2376_m12286_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12286_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12287_GM;
MethodInfo m12287_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12287_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12288_GM;
MethodInfo m12288_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12288_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2376_m12289_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12289_GM;
MethodInfo m12289_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2376_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2376_m12289_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12289_GM};
extern Il2CppType t2366_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2376_m12290_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2366_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12290_GM;
MethodInfo m12290_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2376_m12290_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12290_GM};
extern Il2CppType t2367_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12291_GM;
MethodInfo m12291_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2376_TI, &t2367_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12291_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2376_m12292_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12292_GM;
MethodInfo m12292_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2376_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2376_m12292_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12292_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2376_m12293_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12293_GM;
MethodInfo m12293_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2376_m12293_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12293_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2376_m12294_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12294_GM;
MethodInfo m12294_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2376_m12294_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12294_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2376_m12295_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12295_GM;
MethodInfo m12295_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2376_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2376_m12295_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12295_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2376_m12296_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12296_GM;
MethodInfo m12296_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2376_m12296_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12296_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2376_m12297_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12297_GM;
MethodInfo m12297_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2376_m12297_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12297_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12298_GM;
MethodInfo m12298_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2376_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12298_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2376_m12299_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t113_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12299_GM;
MethodInfo m12299_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2376_TI, &t113_0_0_0, RuntimeInvoker_t29_t44, t2376_m12299_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12299_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2376_m12300_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12300_GM;
MethodInfo m12300_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2376_m12300_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12300_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2376_m12301_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12301_GM;
MethodInfo m12301_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2376_m12301_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12301_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2376_m12302_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12302_GM;
MethodInfo m12302_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2376_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2376_m12302_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12302_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2376_m12303_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t113_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12303_GM;
MethodInfo m12303_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2376_TI, &t113_0_0_0, RuntimeInvoker_t29_t29, t2376_m12303_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12303_GM};
extern Il2CppType t2375_0_0_0;
static ParameterInfo t2376_m12304_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2375_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12304_GM;
MethodInfo m12304_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2376_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2376_m12304_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12304_GM};
extern Il2CppType t2375_0_0_0;
static ParameterInfo t2376_m12305_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2375_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12305_GM;
MethodInfo m12305_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2376_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2376_m12305_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12305_GM};
extern Il2CppType t2375_0_0_0;
static ParameterInfo t2376_m12306_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2375_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12306_GM;
MethodInfo m12306_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2376_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2376_m12306_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12306_GM};
static MethodInfo* t2376_MIs[] =
{
	&m12271_MI,
	&m12272_MI,
	&m12273_MI,
	&m12274_MI,
	&m12275_MI,
	&m12276_MI,
	&m12277_MI,
	&m12278_MI,
	&m12279_MI,
	&m12280_MI,
	&m12281_MI,
	&m12282_MI,
	&m12283_MI,
	&m12284_MI,
	&m12285_MI,
	&m12286_MI,
	&m12287_MI,
	&m12288_MI,
	&m12289_MI,
	&m12290_MI,
	&m12291_MI,
	&m12292_MI,
	&m12293_MI,
	&m12294_MI,
	&m12295_MI,
	&m12296_MI,
	&m12297_MI,
	&m12298_MI,
	&m12299_MI,
	&m12300_MI,
	&m12301_MI,
	&m12302_MI,
	&m12303_MI,
	&m12304_MI,
	&m12305_MI,
	&m12306_MI,
	NULL
};
extern MethodInfo m12274_MI;
extern MethodInfo m12273_MI;
extern MethodInfo m12275_MI;
extern MethodInfo m12287_MI;
extern MethodInfo m12276_MI;
extern MethodInfo m12277_MI;
extern MethodInfo m12278_MI;
extern MethodInfo m12279_MI;
extern MethodInfo m12296_MI;
extern MethodInfo m12286_MI;
extern MethodInfo m12289_MI;
extern MethodInfo m12290_MI;
extern MethodInfo m12295_MI;
extern MethodInfo m12293_MI;
extern MethodInfo m12291_MI;
static MethodInfo* t2376_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12274_MI,
	&m12298_MI,
	&m12280_MI,
	&m12281_MI,
	&m12273_MI,
	&m12282_MI,
	&m12283_MI,
	&m12284_MI,
	&m12285_MI,
	&m12275_MI,
	&m12287_MI,
	&m12276_MI,
	&m12277_MI,
	&m12278_MI,
	&m12279_MI,
	&m12296_MI,
	&m12298_MI,
	&m12272_MI,
	&m12286_MI,
	&m12287_MI,
	&m12289_MI,
	&m12290_MI,
	&m12295_MI,
	&m12292_MI,
	&m12293_MI,
	&m12296_MI,
	&m12299_MI,
	&m12300_MI,
	&m12291_MI,
	&m12288_MI,
	&m12294_MI,
	&m12297_MI,
	&m12301_MI,
};
static TypeInfo* t2376_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2368_TI,
	&t2375_TI,
	&t2369_TI,
};
static Il2CppInterfaceOffsetPair t2376_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2368_TI, 20},
	{ &t2375_TI, 27},
	{ &t2369_TI, 32},
};
extern TypeInfo t116_TI;
extern TypeInfo t113_TI;
static Il2CppRGCTXData t2376_RGCTXData[25] = 
{
	&t116_TI/* Class Usage */,
	&m1405_MI/* Method Usage */,
	&m27434_MI/* Method Usage */,
	&m27432_MI/* Method Usage */,
	&m27430_MI/* Method Usage */,
	&m12303_MI/* Method Usage */,
	&m12294_MI/* Method Usage */,
	&m12302_MI/* Method Usage */,
	&t113_TI/* Class Usage */,
	&m27437_MI/* Method Usage */,
	&m27441_MI/* Method Usage */,
	&m12304_MI/* Method Usage */,
	&m12292_MI/* Method Usage */,
	&m12297_MI/* Method Usage */,
	&m12305_MI/* Method Usage */,
	&m12306_MI/* Method Usage */,
	&m27439_MI/* Method Usage */,
	&m12301_MI/* Method Usage */,
	&m12288_MI/* Method Usage */,
	&m27436_MI/* Method Usage */,
	&m27431_MI/* Method Usage */,
	&m27442_MI/* Method Usage */,
	&m27443_MI/* Method Usage */,
	&m27440_MI/* Method Usage */,
	&t113_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2376_0_0_0;
extern Il2CppType t2376_1_0_0;
struct t2376;
extern Il2CppGenericClass t2376_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2376_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2376_MIs, t2376_PIs, t2376_FIs, NULL, &t29_TI, NULL, NULL, &t2376_TI, t2376_ITIs, t2376_VT, &t1262__CustomAttributeCache, &t2376_TI, &t2376_0_0_0, &t2376_1_0_0, t2376_IOs, &t2376_GC, NULL, NULL, NULL, t2376_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2376), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2377_TI;
#include "t2377MD.h"

#include "t2378.h"
extern TypeInfo t6650_TI;
extern TypeInfo t2378_TI;
#include "t2378MD.h"
extern Il2CppType t6650_0_0_0;
extern MethodInfo m12312_MI;
extern MethodInfo m27444_MI;
extern MethodInfo m20547_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t2377_0_0_49;
FieldInfo t2377_f0_FieldInfo = 
{
	"_default", &t2377_0_0_49, &t2377_TI, offsetof(t2377_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2377_FIs[] =
{
	&t2377_f0_FieldInfo,
	NULL
};
extern MethodInfo m12311_MI;
static PropertyInfo t2377____Default_PropertyInfo = 
{
	&t2377_TI, "Default", &m12311_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2377_PIs[] =
{
	&t2377____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12307_GM;
MethodInfo m12307_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2377_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12307_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12308_GM;
MethodInfo m12308_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2377_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12308_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2377_m12309_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12309_GM;
MethodInfo m12309_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2377_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2377_m12309_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12309_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2377_m12310_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12310_GM;
MethodInfo m12310_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2377_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2377_m12310_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12310_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2377_m27444_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27444_GM;
MethodInfo m27444_MI = 
{
	"GetHashCode", NULL, &t2377_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2377_m27444_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27444_GM};
extern Il2CppType t113_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2377_m20547_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20547_GM;
MethodInfo m20547_MI = 
{
	"Equals", NULL, &t2377_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2377_m20547_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20547_GM};
extern Il2CppType t2377_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12311_GM;
MethodInfo m12311_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2377_TI, &t2377_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12311_GM};
static MethodInfo* t2377_MIs[] =
{
	&m12307_MI,
	&m12308_MI,
	&m12309_MI,
	&m12310_MI,
	&m27444_MI,
	&m20547_MI,
	&m12311_MI,
	NULL
};
extern MethodInfo m12310_MI;
extern MethodInfo m12309_MI;
static MethodInfo* t2377_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m20547_MI,
	&m27444_MI,
	&m12310_MI,
	&m12309_MI,
	NULL,
	NULL,
};
extern TypeInfo t6651_TI;
static TypeInfo* t2377_ITIs[] = 
{
	&t6651_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2377_IOs[] = 
{
	{ &t6651_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2377_TI;
extern TypeInfo t2377_TI;
extern TypeInfo t2378_TI;
extern TypeInfo t113_TI;
static Il2CppRGCTXData t2377_RGCTXData[9] = 
{
	&t6650_0_0_0/* Type Usage */,
	&t113_0_0_0/* Type Usage */,
	&t2377_TI/* Class Usage */,
	&t2377_TI/* Static Usage */,
	&t2378_TI/* Class Usage */,
	&m12312_MI/* Method Usage */,
	&t113_TI/* Class Usage */,
	&m27444_MI/* Method Usage */,
	&m20547_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2377_0_0_0;
extern Il2CppType t2377_1_0_0;
struct t2377;
extern Il2CppGenericClass t2377_GC;
TypeInfo t2377_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2377_MIs, t2377_PIs, t2377_FIs, NULL, &t29_TI, NULL, NULL, &t2377_TI, t2377_ITIs, t2377_VT, &EmptyCustomAttributesCache, &t2377_TI, &t2377_0_0_0, &t2377_1_0_0, t2377_IOs, &t2377_GC, NULL, NULL, NULL, t2377_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2377), 0, -1, sizeof(t2377_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t113_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t6651_m27445_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27445_GM;
MethodInfo m27445_MI = 
{
	"Equals", NULL, &t6651_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6651_m27445_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27445_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t6651_m27446_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27446_GM;
MethodInfo m27446_MI = 
{
	"GetHashCode", NULL, &t6651_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6651_m27446_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27446_GM};
static MethodInfo* t6651_MIs[] =
{
	&m27445_MI,
	&m27446_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6651_0_0_0;
extern Il2CppType t6651_1_0_0;
struct t6651;
extern Il2CppGenericClass t6651_GC;
TypeInfo t6651_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6651_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6651_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6651_TI, &t6651_0_0_0, &t6651_1_0_0, NULL, &t6651_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t113_0_0_0;
static ParameterInfo t6650_m27447_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27447_GM;
MethodInfo m27447_MI = 
{
	"Equals", NULL, &t6650_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6650_m27447_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27447_GM};
static MethodInfo* t6650_MIs[] =
{
	&m27447_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6650_1_0_0;
struct t6650;
extern Il2CppGenericClass t6650_GC;
TypeInfo t6650_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6650_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6650_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6650_TI, &t6650_0_0_0, &t6650_1_0_0, NULL, &t6650_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m12307_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12312_GM;
MethodInfo m12312_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2378_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12312_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2378_m12313_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12313_GM;
MethodInfo m12313_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2378_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2378_m12313_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12313_GM};
extern Il2CppType t113_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2378_m12314_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12314_GM;
MethodInfo m12314_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2378_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2378_m12314_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12314_GM};
static MethodInfo* t2378_MIs[] =
{
	&m12312_MI,
	&m12313_MI,
	&m12314_MI,
	NULL
};
extern MethodInfo m12314_MI;
extern MethodInfo m12313_MI;
static MethodInfo* t2378_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12314_MI,
	&m12313_MI,
	&m12310_MI,
	&m12309_MI,
	&m12313_MI,
	&m12314_MI,
};
static Il2CppInterfaceOffsetPair t2378_IOs[] = 
{
	{ &t6651_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2377_TI;
extern TypeInfo t2377_TI;
extern TypeInfo t2378_TI;
extern TypeInfo t113_TI;
extern TypeInfo t113_TI;
static Il2CppRGCTXData t2378_RGCTXData[11] = 
{
	&t6650_0_0_0/* Type Usage */,
	&t113_0_0_0/* Type Usage */,
	&t2377_TI/* Class Usage */,
	&t2377_TI/* Static Usage */,
	&t2378_TI/* Class Usage */,
	&m12312_MI/* Method Usage */,
	&t113_TI/* Class Usage */,
	&m27444_MI/* Method Usage */,
	&m20547_MI/* Method Usage */,
	&m12307_MI/* Method Usage */,
	&t113_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2378_0_0_0;
extern Il2CppType t2378_1_0_0;
struct t2378;
extern Il2CppGenericClass t2378_GC;
TypeInfo t2378_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2378_MIs, NULL, NULL, NULL, &t2377_TI, NULL, &t1256_TI, &t2378_TI, NULL, t2378_VT, &EmptyCustomAttributesCache, &t2378_TI, &t2378_0_0_0, &t2378_1_0_0, t2378_IOs, &t2378_GC, NULL, NULL, NULL, t2378_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2378), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2371_m12315_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12315_GM;
MethodInfo m12315_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2371_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2371_m12315_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12315_GM};
extern Il2CppType t113_0_0_0;
static ParameterInfo t2371_m12316_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12316_GM;
MethodInfo m12316_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2371_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2371_m12316_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12316_GM};
extern Il2CppType t113_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2371_m12317_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12317_GM;
MethodInfo m12317_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2371_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2371_m12317_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12317_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2371_m12318_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12318_GM;
MethodInfo m12318_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2371_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2371_m12318_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12318_GM};
static MethodInfo* t2371_MIs[] =
{
	&m12315_MI,
	&m12316_MI,
	&m12317_MI,
	&m12318_MI,
	NULL
};
extern MethodInfo m12317_MI;
extern MethodInfo m12318_MI;
static MethodInfo* t2371_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12316_MI,
	&m12317_MI,
	&m12318_MI,
};
static Il2CppInterfaceOffsetPair t2371_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2371_1_0_0;
struct t2371;
extern Il2CppGenericClass t2371_GC;
TypeInfo t2371_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2371_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2371_TI, NULL, t2371_VT, &EmptyCustomAttributesCache, &t2371_TI, &t2371_0_0_0, &t2371_1_0_0, t2371_IOs, &t2371_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2371), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t2380.h"
extern TypeInfo t4107_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t2380_TI;
#include "t2380MD.h"
extern Il2CppType t4107_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m12323_MI;
extern MethodInfo m27448_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t2379_0_0_49;
FieldInfo t2379_f0_FieldInfo = 
{
	"_default", &t2379_0_0_49, &t2379_TI, offsetof(t2379_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2379_FIs[] =
{
	&t2379_f0_FieldInfo,
	NULL
};
static PropertyInfo t2379____Default_PropertyInfo = 
{
	&t2379_TI, "Default", &m12322_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2379_PIs[] =
{
	&t2379____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12319_GM;
MethodInfo m12319_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2379_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12319_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12320_GM;
MethodInfo m12320_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2379_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12320_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2379_m12321_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12321_GM;
MethodInfo m12321_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2379_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2379_m12321_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12321_GM};
extern Il2CppType t113_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2379_m27448_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27448_GM;
MethodInfo m27448_MI = 
{
	"Compare", NULL, &t2379_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2379_m27448_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27448_GM};
extern Il2CppType t2379_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12322_GM;
MethodInfo m12322_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2379_TI, &t2379_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12322_GM};
static MethodInfo* t2379_MIs[] =
{
	&m12319_MI,
	&m12320_MI,
	&m12321_MI,
	&m27448_MI,
	&m12322_MI,
	NULL
};
extern MethodInfo m12321_MI;
static MethodInfo* t2379_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27448_MI,
	&m12321_MI,
	NULL,
};
extern TypeInfo t4106_TI;
extern TypeInfo t726_TI;
static TypeInfo* t2379_ITIs[] = 
{
	&t4106_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2379_IOs[] = 
{
	{ &t4106_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2379_TI;
extern TypeInfo t2379_TI;
extern TypeInfo t2380_TI;
extern TypeInfo t113_TI;
static Il2CppRGCTXData t2379_RGCTXData[8] = 
{
	&t4107_0_0_0/* Type Usage */,
	&t113_0_0_0/* Type Usage */,
	&t2379_TI/* Class Usage */,
	&t2379_TI/* Static Usage */,
	&t2380_TI/* Class Usage */,
	&m12323_MI/* Method Usage */,
	&t113_TI/* Class Usage */,
	&m27448_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2379_0_0_0;
extern Il2CppType t2379_1_0_0;
struct t2379;
extern Il2CppGenericClass t2379_GC;
TypeInfo t2379_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2379_MIs, t2379_PIs, t2379_FIs, NULL, &t29_TI, NULL, NULL, &t2379_TI, t2379_ITIs, t2379_VT, &EmptyCustomAttributesCache, &t2379_TI, &t2379_0_0_0, &t2379_1_0_0, t2379_IOs, &t2379_GC, NULL, NULL, NULL, t2379_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2379), 0, -1, sizeof(t2379_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t113_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t4106_m20555_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20555_GM;
MethodInfo m20555_MI = 
{
	"Compare", NULL, &t4106_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4106_m20555_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20555_GM};
static MethodInfo* t4106_MIs[] =
{
	&m20555_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4106_0_0_0;
extern Il2CppType t4106_1_0_0;
struct t4106;
extern Il2CppGenericClass t4106_GC;
TypeInfo t4106_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4106_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4106_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4106_TI, &t4106_0_0_0, &t4106_1_0_0, NULL, &t4106_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t113_0_0_0;
static ParameterInfo t4107_m20556_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20556_GM;
MethodInfo m20556_MI = 
{
	"CompareTo", NULL, &t4107_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4107_m20556_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20556_GM};
static MethodInfo* t4107_MIs[] =
{
	&m20556_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4107_1_0_0;
struct t4107;
extern Il2CppGenericClass t4107_GC;
TypeInfo t4107_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4107_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4107_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4107_TI, &t4107_0_0_0, &t4107_1_0_0, NULL, &t4107_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m12319_MI;
extern MethodInfo m20556_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12323_GM;
MethodInfo m12323_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2380_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12323_GM};
extern Il2CppType t113_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2380_m12324_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12324_GM;
MethodInfo m12324_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2380_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2380_m12324_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12324_GM};
static MethodInfo* t2380_MIs[] =
{
	&m12323_MI,
	&m12324_MI,
	NULL
};
extern MethodInfo m12324_MI;
static MethodInfo* t2380_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12324_MI,
	&m12321_MI,
	&m12324_MI,
};
static Il2CppInterfaceOffsetPair t2380_IOs[] = 
{
	{ &t4106_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2379_TI;
extern TypeInfo t2379_TI;
extern TypeInfo t2380_TI;
extern TypeInfo t113_TI;
extern TypeInfo t113_TI;
extern TypeInfo t4107_TI;
static Il2CppRGCTXData t2380_RGCTXData[12] = 
{
	&t4107_0_0_0/* Type Usage */,
	&t113_0_0_0/* Type Usage */,
	&t2379_TI/* Class Usage */,
	&t2379_TI/* Static Usage */,
	&t2380_TI/* Class Usage */,
	&m12323_MI/* Method Usage */,
	&t113_TI/* Class Usage */,
	&m27448_MI/* Method Usage */,
	&m12319_MI/* Method Usage */,
	&t113_TI/* Class Usage */,
	&t4107_TI/* Class Usage */,
	&m20556_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2380_0_0_0;
extern Il2CppType t2380_1_0_0;
struct t2380;
extern Il2CppGenericClass t2380_GC;
extern TypeInfo t1246_TI;
TypeInfo t2380_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2380_MIs, NULL, NULL, NULL, &t2379_TI, NULL, &t1246_TI, &t2380_TI, NULL, t2380_VT, &EmptyCustomAttributesCache, &t2380_TI, &t2380_0_0_0, &t2380_1_0_0, t2380_IOs, &t2380_GC, NULL, NULL, NULL, t2380_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2380), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2372_TI;
#include "t2372MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2372_m12325_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12325_GM;
MethodInfo m12325_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2372_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2372_m12325_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12325_GM};
extern Il2CppType t113_0_0_0;
extern Il2CppType t113_0_0_0;
static ParameterInfo t2372_m12326_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12326_GM;
MethodInfo m12326_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2372_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2372_m12326_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12326_GM};
extern Il2CppType t113_0_0_0;
extern Il2CppType t113_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2372_m12327_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t113_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12327_GM;
MethodInfo m12327_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2372_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2372_m12327_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12327_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2372_m12328_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12328_GM;
MethodInfo m12328_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2372_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2372_m12328_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12328_GM};
static MethodInfo* t2372_MIs[] =
{
	&m12325_MI,
	&m12326_MI,
	&m12327_MI,
	&m12328_MI,
	NULL
};
extern MethodInfo m12326_MI;
extern MethodInfo m12327_MI;
extern MethodInfo m12328_MI;
static MethodInfo* t2372_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12326_MI,
	&m12327_MI,
	&m12328_MI,
};
static Il2CppInterfaceOffsetPair t2372_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2372_1_0_0;
struct t2372;
extern Il2CppGenericClass t2372_GC;
TypeInfo t2372_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2372_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2372_TI, NULL, t2372_VT, &EmptyCustomAttributesCache, &t2372_TI, &t2372_0_0_0, &t2372_1_0_0, t2372_IOs, &t2372_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2372), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4109_TI;

#include "t121.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.StandaloneInputModule>
extern MethodInfo m27449_MI;
static PropertyInfo t4109____Current_PropertyInfo = 
{
	&t4109_TI, "Current", &m27449_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4109_PIs[] =
{
	&t4109____Current_PropertyInfo,
	NULL
};
extern Il2CppType t121_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27449_GM;
MethodInfo m27449_MI = 
{
	"get_Current", NULL, &t4109_TI, &t121_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27449_GM};
static MethodInfo* t4109_MIs[] =
{
	&m27449_MI,
	NULL
};
static TypeInfo* t4109_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4109_0_0_0;
extern Il2CppType t4109_1_0_0;
struct t4109;
extern Il2CppGenericClass t4109_GC;
TypeInfo t4109_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4109_MIs, t4109_PIs, NULL, NULL, NULL, NULL, NULL, &t4109_TI, t4109_ITIs, NULL, &EmptyCustomAttributesCache, &t4109_TI, &t4109_0_0_0, &t4109_1_0_0, NULL, &t4109_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2381.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2381_TI;
#include "t2381MD.h"

extern TypeInfo t121_TI;
extern MethodInfo m12333_MI;
extern MethodInfo m20561_MI;
struct t20;
#define m20561(__this, p0, method) (t121 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.StandaloneInputModule>
extern Il2CppType t20_0_0_1;
FieldInfo t2381_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2381_TI, offsetof(t2381, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2381_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2381_TI, offsetof(t2381, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2381_FIs[] =
{
	&t2381_f0_FieldInfo,
	&t2381_f1_FieldInfo,
	NULL
};
extern MethodInfo m12330_MI;
static PropertyInfo t2381____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2381_TI, "System.Collections.IEnumerator.Current", &m12330_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2381____Current_PropertyInfo = 
{
	&t2381_TI, "Current", &m12333_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2381_PIs[] =
{
	&t2381____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2381____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2381_m12329_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12329_GM;
MethodInfo m12329_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2381_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2381_m12329_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12329_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12330_GM;
MethodInfo m12330_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2381_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12330_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12331_GM;
MethodInfo m12331_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2381_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12331_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12332_GM;
MethodInfo m12332_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2381_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12332_GM};
extern Il2CppType t121_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12333_GM;
MethodInfo m12333_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2381_TI, &t121_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12333_GM};
static MethodInfo* t2381_MIs[] =
{
	&m12329_MI,
	&m12330_MI,
	&m12331_MI,
	&m12332_MI,
	&m12333_MI,
	NULL
};
extern MethodInfo m12332_MI;
extern MethodInfo m12331_MI;
static MethodInfo* t2381_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12330_MI,
	&m12332_MI,
	&m12331_MI,
	&m12333_MI,
};
static TypeInfo* t2381_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4109_TI,
};
static Il2CppInterfaceOffsetPair t2381_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4109_TI, 7},
};
extern TypeInfo t121_TI;
static Il2CppRGCTXData t2381_RGCTXData[3] = 
{
	&m12333_MI/* Method Usage */,
	&t121_TI/* Class Usage */,
	&m20561_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2381_0_0_0;
extern Il2CppType t2381_1_0_0;
extern Il2CppGenericClass t2381_GC;
TypeInfo t2381_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2381_MIs, t2381_PIs, t2381_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2381_TI, t2381_ITIs, t2381_VT, &EmptyCustomAttributesCache, &t2381_TI, &t2381_0_0_0, &t2381_1_0_0, t2381_IOs, &t2381_GC, NULL, NULL, NULL, t2381_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2381)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5265_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.StandaloneInputModule>
extern MethodInfo m27450_MI;
static PropertyInfo t5265____Count_PropertyInfo = 
{
	&t5265_TI, "Count", &m27450_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27451_MI;
static PropertyInfo t5265____IsReadOnly_PropertyInfo = 
{
	&t5265_TI, "IsReadOnly", &m27451_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5265_PIs[] =
{
	&t5265____Count_PropertyInfo,
	&t5265____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27450_GM;
MethodInfo m27450_MI = 
{
	"get_Count", NULL, &t5265_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27450_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27451_GM;
MethodInfo m27451_MI = 
{
	"get_IsReadOnly", NULL, &t5265_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27451_GM};
extern Il2CppType t121_0_0_0;
extern Il2CppType t121_0_0_0;
static ParameterInfo t5265_m27452_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t121_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27452_GM;
MethodInfo m27452_MI = 
{
	"Add", NULL, &t5265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5265_m27452_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27452_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27453_GM;
MethodInfo m27453_MI = 
{
	"Clear", NULL, &t5265_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27453_GM};
extern Il2CppType t121_0_0_0;
static ParameterInfo t5265_m27454_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t121_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27454_GM;
MethodInfo m27454_MI = 
{
	"Contains", NULL, &t5265_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5265_m27454_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27454_GM};
extern Il2CppType t3825_0_0_0;
extern Il2CppType t3825_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5265_m27455_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3825_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27455_GM;
MethodInfo m27455_MI = 
{
	"CopyTo", NULL, &t5265_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5265_m27455_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27455_GM};
extern Il2CppType t121_0_0_0;
static ParameterInfo t5265_m27456_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t121_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27456_GM;
MethodInfo m27456_MI = 
{
	"Remove", NULL, &t5265_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5265_m27456_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27456_GM};
static MethodInfo* t5265_MIs[] =
{
	&m27450_MI,
	&m27451_MI,
	&m27452_MI,
	&m27453_MI,
	&m27454_MI,
	&m27455_MI,
	&m27456_MI,
	NULL
};
extern TypeInfo t5267_TI;
static TypeInfo* t5265_ITIs[] = 
{
	&t603_TI,
	&t5267_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5265_0_0_0;
extern Il2CppType t5265_1_0_0;
struct t5265;
extern Il2CppGenericClass t5265_GC;
TypeInfo t5265_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5265_MIs, t5265_PIs, NULL, NULL, NULL, NULL, NULL, &t5265_TI, t5265_ITIs, NULL, &EmptyCustomAttributesCache, &t5265_TI, &t5265_0_0_0, &t5265_1_0_0, NULL, &t5265_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.StandaloneInputModule>
extern Il2CppType t4109_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27457_GM;
MethodInfo m27457_MI = 
{
	"GetEnumerator", NULL, &t5267_TI, &t4109_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27457_GM};
static MethodInfo* t5267_MIs[] =
{
	&m27457_MI,
	NULL
};
static TypeInfo* t5267_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5267_0_0_0;
extern Il2CppType t5267_1_0_0;
struct t5267;
extern Il2CppGenericClass t5267_GC;
TypeInfo t5267_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5267_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5267_TI, t5267_ITIs, NULL, &EmptyCustomAttributesCache, &t5267_TI, &t5267_0_0_0, &t5267_1_0_0, NULL, &t5267_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5266_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.StandaloneInputModule>
extern MethodInfo m27458_MI;
extern MethodInfo m27459_MI;
static PropertyInfo t5266____Item_PropertyInfo = 
{
	&t5266_TI, "Item", &m27458_MI, &m27459_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5266_PIs[] =
{
	&t5266____Item_PropertyInfo,
	NULL
};
extern Il2CppType t121_0_0_0;
static ParameterInfo t5266_m27460_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t121_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27460_GM;
MethodInfo m27460_MI = 
{
	"IndexOf", NULL, &t5266_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5266_m27460_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27460_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t121_0_0_0;
static ParameterInfo t5266_m27461_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t121_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27461_GM;
MethodInfo m27461_MI = 
{
	"Insert", NULL, &t5266_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5266_m27461_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27461_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5266_m27462_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27462_GM;
MethodInfo m27462_MI = 
{
	"RemoveAt", NULL, &t5266_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5266_m27462_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27462_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5266_m27458_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t121_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27458_GM;
MethodInfo m27458_MI = 
{
	"get_Item", NULL, &t5266_TI, &t121_0_0_0, RuntimeInvoker_t29_t44, t5266_m27458_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27458_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t121_0_0_0;
static ParameterInfo t5266_m27459_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t121_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27459_GM;
MethodInfo m27459_MI = 
{
	"set_Item", NULL, &t5266_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5266_m27459_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27459_GM};
static MethodInfo* t5266_MIs[] =
{
	&m27460_MI,
	&m27461_MI,
	&m27462_MI,
	&m27458_MI,
	&m27459_MI,
	NULL
};
static TypeInfo* t5266_ITIs[] = 
{
	&t603_TI,
	&t5265_TI,
	&t5267_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5266_0_0_0;
extern Il2CppType t5266_1_0_0;
struct t5266;
extern Il2CppGenericClass t5266_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5266_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5266_MIs, t5266_PIs, NULL, NULL, NULL, NULL, NULL, &t5266_TI, t5266_ITIs, NULL, &t1908__CustomAttributeCache, &t5266_TI, &t5266_0_0_0, &t5266_1_0_0, NULL, &t5266_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2382.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2382_TI;
#include "t2382MD.h"

#include "t41.h"
#include "t557.h"
#include "t2383.h"
extern TypeInfo t316_TI;
extern TypeInfo t2383_TI;
#include "t2383MD.h"
extern MethodInfo m12336_MI;
extern MethodInfo m12338_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.StandaloneInputModule>
extern Il2CppType t316_0_0_33;
FieldInfo t2382_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2382_TI, offsetof(t2382, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2382_FIs[] =
{
	&t2382_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t121_0_0_0;
static ParameterInfo t2382_m12334_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t121_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12334_GM;
MethodInfo m12334_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2382_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2382_m12334_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12334_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2382_m12335_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12335_GM;
MethodInfo m12335_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2382_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2382_m12335_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12335_GM};
static MethodInfo* t2382_MIs[] =
{
	&m12334_MI,
	&m12335_MI,
	NULL
};
extern MethodInfo m12335_MI;
extern MethodInfo m12339_MI;
static MethodInfo* t2382_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12335_MI,
	&m12339_MI,
};
extern Il2CppType t2384_0_0_0;
extern TypeInfo t2384_TI;
extern MethodInfo m20571_MI;
extern TypeInfo t121_TI;
extern MethodInfo m12341_MI;
extern TypeInfo t121_TI;
static Il2CppRGCTXData t2382_RGCTXData[8] = 
{
	&t2384_0_0_0/* Type Usage */,
	&t2384_TI/* Class Usage */,
	&m20571_MI/* Method Usage */,
	&t121_TI/* Class Usage */,
	&m12341_MI/* Method Usage */,
	&m12336_MI/* Method Usage */,
	&t121_TI/* Class Usage */,
	&m12338_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2382_0_0_0;
extern Il2CppType t2382_1_0_0;
struct t2382;
extern Il2CppGenericClass t2382_GC;
TypeInfo t2382_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2382_MIs, NULL, t2382_FIs, NULL, &t2383_TI, NULL, NULL, &t2382_TI, NULL, t2382_VT, &EmptyCustomAttributesCache, &t2382_TI, &t2382_0_0_0, &t2382_1_0_0, NULL, &t2382_GC, NULL, NULL, NULL, t2382_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2382), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2384.h"
#include "t353.h"
extern TypeInfo t2384_TI;
#include "t556MD.h"
#include "t353MD.h"
#include "t2384MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m20571(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.StandaloneInputModule>
extern Il2CppType t2384_0_0_1;
FieldInfo t2383_f0_FieldInfo = 
{
	"Delegate", &t2384_0_0_1, &t2383_TI, offsetof(t2383, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2383_FIs[] =
{
	&t2383_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2383_m12336_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12336_GM;
MethodInfo m12336_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2383_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2383_m12336_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12336_GM};
extern Il2CppType t2384_0_0_0;
static ParameterInfo t2383_m12337_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2384_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12337_GM;
MethodInfo m12337_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2383_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2383_m12337_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12337_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2383_m12338_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12338_GM;
MethodInfo m12338_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2383_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2383_m12338_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12338_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2383_m12339_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12339_GM;
MethodInfo m12339_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2383_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2383_m12339_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12339_GM};
static MethodInfo* t2383_MIs[] =
{
	&m12336_MI,
	&m12337_MI,
	&m12338_MI,
	&m12339_MI,
	NULL
};
static MethodInfo* t2383_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12338_MI,
	&m12339_MI,
};
extern TypeInfo t2384_TI;
extern TypeInfo t121_TI;
static Il2CppRGCTXData t2383_RGCTXData[5] = 
{
	&t2384_0_0_0/* Type Usage */,
	&t2384_TI/* Class Usage */,
	&m20571_MI/* Method Usage */,
	&t121_TI/* Class Usage */,
	&m12341_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2383_0_0_0;
extern Il2CppType t2383_1_0_0;
extern TypeInfo t556_TI;
struct t2383;
extern Il2CppGenericClass t2383_GC;
TypeInfo t2383_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2383_MIs, NULL, t2383_FIs, NULL, &t556_TI, NULL, NULL, &t2383_TI, NULL, t2383_VT, &EmptyCustomAttributesCache, &t2383_TI, &t2383_0_0_0, &t2383_1_0_0, NULL, &t2383_GC, NULL, NULL, NULL, t2383_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2383), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.StandaloneInputModule>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2384_m12340_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12340_GM;
MethodInfo m12340_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2384_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2384_m12340_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12340_GM};
extern Il2CppType t121_0_0_0;
static ParameterInfo t2384_m12341_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t121_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12341_GM;
MethodInfo m12341_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2384_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2384_m12341_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12341_GM};
extern Il2CppType t121_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2384_m12342_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t121_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12342_GM;
MethodInfo m12342_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2384_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2384_m12342_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12342_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2384_m12343_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12343_GM;
MethodInfo m12343_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2384_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2384_m12343_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12343_GM};
static MethodInfo* t2384_MIs[] =
{
	&m12340_MI,
	&m12341_MI,
	&m12342_MI,
	&m12343_MI,
	NULL
};
extern MethodInfo m12342_MI;
extern MethodInfo m12343_MI;
static MethodInfo* t2384_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12341_MI,
	&m12342_MI,
	&m12343_MI,
};
static Il2CppInterfaceOffsetPair t2384_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2384_1_0_0;
struct t2384;
extern Il2CppGenericClass t2384_GC;
TypeInfo t2384_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2384_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2384_TI, NULL, t2384_VT, &EmptyCustomAttributesCache, &t2384_TI, &t2384_0_0_0, &t2384_1_0_0, t2384_IOs, &t2384_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2384), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4111_TI;

#include "t120.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.StandaloneInputModule/InputMode>
extern MethodInfo m27463_MI;
static PropertyInfo t4111____Current_PropertyInfo = 
{
	&t4111_TI, "Current", &m27463_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4111_PIs[] =
{
	&t4111____Current_PropertyInfo,
	NULL
};
extern Il2CppType t120_0_0_0;
extern void* RuntimeInvoker_t120 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27463_GM;
MethodInfo m27463_MI = 
{
	"get_Current", NULL, &t4111_TI, &t120_0_0_0, RuntimeInvoker_t120, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27463_GM};
static MethodInfo* t4111_MIs[] =
{
	&m27463_MI,
	NULL
};
static TypeInfo* t4111_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4111_0_0_0;
extern Il2CppType t4111_1_0_0;
struct t4111;
extern Il2CppGenericClass t4111_GC;
TypeInfo t4111_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4111_MIs, t4111_PIs, NULL, NULL, NULL, NULL, NULL, &t4111_TI, t4111_ITIs, NULL, &EmptyCustomAttributesCache, &t4111_TI, &t4111_0_0_0, &t4111_1_0_0, NULL, &t4111_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2385.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2385_TI;
#include "t2385MD.h"

extern TypeInfo t120_TI;
extern MethodInfo m12348_MI;
extern MethodInfo m20599_MI;
struct t20;
 int32_t m20599 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12344_MI;
 void m12344 (t2385 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12345_MI;
 t29 * m12345 (t2385 * __this, MethodInfo* method){
	{
		int32_t L_0 = m12348(__this, &m12348_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t120_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12346_MI;
 void m12346 (t2385 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12347_MI;
 bool m12347 (t2385 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m12348 (t2385 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m20599(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20599_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.StandaloneInputModule/InputMode>
extern Il2CppType t20_0_0_1;
FieldInfo t2385_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2385_TI, offsetof(t2385, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2385_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2385_TI, offsetof(t2385, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2385_FIs[] =
{
	&t2385_f0_FieldInfo,
	&t2385_f1_FieldInfo,
	NULL
};
static PropertyInfo t2385____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2385_TI, "System.Collections.IEnumerator.Current", &m12345_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2385____Current_PropertyInfo = 
{
	&t2385_TI, "Current", &m12348_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2385_PIs[] =
{
	&t2385____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2385____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2385_m12344_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12344_GM;
MethodInfo m12344_MI = 
{
	".ctor", (methodPointerType)&m12344, &t2385_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2385_m12344_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12344_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12345_GM;
MethodInfo m12345_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12345, &t2385_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12345_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12346_GM;
MethodInfo m12346_MI = 
{
	"Dispose", (methodPointerType)&m12346, &t2385_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12346_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12347_GM;
MethodInfo m12347_MI = 
{
	"MoveNext", (methodPointerType)&m12347, &t2385_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12347_GM};
extern Il2CppType t120_0_0_0;
extern void* RuntimeInvoker_t120 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12348_GM;
MethodInfo m12348_MI = 
{
	"get_Current", (methodPointerType)&m12348, &t2385_TI, &t120_0_0_0, RuntimeInvoker_t120, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12348_GM};
static MethodInfo* t2385_MIs[] =
{
	&m12344_MI,
	&m12345_MI,
	&m12346_MI,
	&m12347_MI,
	&m12348_MI,
	NULL
};
static MethodInfo* t2385_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12345_MI,
	&m12347_MI,
	&m12346_MI,
	&m12348_MI,
};
static TypeInfo* t2385_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4111_TI,
};
static Il2CppInterfaceOffsetPair t2385_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4111_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2385_0_0_0;
extern Il2CppType t2385_1_0_0;
extern Il2CppGenericClass t2385_GC;
TypeInfo t2385_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2385_MIs, t2385_PIs, t2385_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2385_TI, t2385_ITIs, t2385_VT, &EmptyCustomAttributesCache, &t2385_TI, &t2385_0_0_0, &t2385_1_0_0, t2385_IOs, &t2385_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2385)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5268_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.StandaloneInputModule/InputMode>
extern MethodInfo m27464_MI;
static PropertyInfo t5268____Count_PropertyInfo = 
{
	&t5268_TI, "Count", &m27464_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27465_MI;
static PropertyInfo t5268____IsReadOnly_PropertyInfo = 
{
	&t5268_TI, "IsReadOnly", &m27465_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5268_PIs[] =
{
	&t5268____Count_PropertyInfo,
	&t5268____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27464_GM;
MethodInfo m27464_MI = 
{
	"get_Count", NULL, &t5268_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27464_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27465_GM;
MethodInfo m27465_MI = 
{
	"get_IsReadOnly", NULL, &t5268_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27465_GM};
extern Il2CppType t120_0_0_0;
extern Il2CppType t120_0_0_0;
static ParameterInfo t5268_m27466_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t120_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27466_GM;
MethodInfo m27466_MI = 
{
	"Add", NULL, &t5268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5268_m27466_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27466_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27467_GM;
MethodInfo m27467_MI = 
{
	"Clear", NULL, &t5268_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27467_GM};
extern Il2CppType t120_0_0_0;
static ParameterInfo t5268_m27468_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t120_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27468_GM;
MethodInfo m27468_MI = 
{
	"Contains", NULL, &t5268_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5268_m27468_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27468_GM};
extern Il2CppType t3826_0_0_0;
extern Il2CppType t3826_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5268_m27469_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3826_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27469_GM;
MethodInfo m27469_MI = 
{
	"CopyTo", NULL, &t5268_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5268_m27469_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27469_GM};
extern Il2CppType t120_0_0_0;
static ParameterInfo t5268_m27470_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t120_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27470_GM;
MethodInfo m27470_MI = 
{
	"Remove", NULL, &t5268_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5268_m27470_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27470_GM};
static MethodInfo* t5268_MIs[] =
{
	&m27464_MI,
	&m27465_MI,
	&m27466_MI,
	&m27467_MI,
	&m27468_MI,
	&m27469_MI,
	&m27470_MI,
	NULL
};
extern TypeInfo t5270_TI;
static TypeInfo* t5268_ITIs[] = 
{
	&t603_TI,
	&t5270_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5268_0_0_0;
extern Il2CppType t5268_1_0_0;
struct t5268;
extern Il2CppGenericClass t5268_GC;
TypeInfo t5268_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5268_MIs, t5268_PIs, NULL, NULL, NULL, NULL, NULL, &t5268_TI, t5268_ITIs, NULL, &EmptyCustomAttributesCache, &t5268_TI, &t5268_0_0_0, &t5268_1_0_0, NULL, &t5268_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.StandaloneInputModule/InputMode>
extern Il2CppType t4111_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27471_GM;
MethodInfo m27471_MI = 
{
	"GetEnumerator", NULL, &t5270_TI, &t4111_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27471_GM};
static MethodInfo* t5270_MIs[] =
{
	&m27471_MI,
	NULL
};
static TypeInfo* t5270_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5270_0_0_0;
extern Il2CppType t5270_1_0_0;
struct t5270;
extern Il2CppGenericClass t5270_GC;
TypeInfo t5270_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5270_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5270_TI, t5270_ITIs, NULL, &EmptyCustomAttributesCache, &t5270_TI, &t5270_0_0_0, &t5270_1_0_0, NULL, &t5270_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5269_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.StandaloneInputModule/InputMode>
extern MethodInfo m27472_MI;
extern MethodInfo m27473_MI;
static PropertyInfo t5269____Item_PropertyInfo = 
{
	&t5269_TI, "Item", &m27472_MI, &m27473_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5269_PIs[] =
{
	&t5269____Item_PropertyInfo,
	NULL
};
extern Il2CppType t120_0_0_0;
static ParameterInfo t5269_m27474_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t120_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27474_GM;
MethodInfo m27474_MI = 
{
	"IndexOf", NULL, &t5269_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5269_m27474_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27474_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t120_0_0_0;
static ParameterInfo t5269_m27475_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t120_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27475_GM;
MethodInfo m27475_MI = 
{
	"Insert", NULL, &t5269_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5269_m27475_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27475_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5269_m27476_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27476_GM;
MethodInfo m27476_MI = 
{
	"RemoveAt", NULL, &t5269_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5269_m27476_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27476_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5269_m27472_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t120_0_0_0;
extern void* RuntimeInvoker_t120_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27472_GM;
MethodInfo m27472_MI = 
{
	"get_Item", NULL, &t5269_TI, &t120_0_0_0, RuntimeInvoker_t120_t44, t5269_m27472_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27472_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t120_0_0_0;
static ParameterInfo t5269_m27473_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t120_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27473_GM;
MethodInfo m27473_MI = 
{
	"set_Item", NULL, &t5269_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5269_m27473_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27473_GM};
static MethodInfo* t5269_MIs[] =
{
	&m27474_MI,
	&m27475_MI,
	&m27476_MI,
	&m27472_MI,
	&m27473_MI,
	NULL
};
static TypeInfo* t5269_ITIs[] = 
{
	&t603_TI,
	&t5268_TI,
	&t5270_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5269_0_0_0;
extern Il2CppType t5269_1_0_0;
struct t5269;
extern Il2CppGenericClass t5269_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5269_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5269_MIs, t5269_PIs, NULL, NULL, NULL, NULL, NULL, &t5269_TI, t5269_ITIs, NULL, &t1908__CustomAttributeCache, &t5269_TI, &t5269_0_0_0, &t5269_1_0_0, NULL, &t5269_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4113_TI;

#include "t122.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.TouchInputModule>
extern MethodInfo m27477_MI;
static PropertyInfo t4113____Current_PropertyInfo = 
{
	&t4113_TI, "Current", &m27477_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4113_PIs[] =
{
	&t4113____Current_PropertyInfo,
	NULL
};
extern Il2CppType t122_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27477_GM;
MethodInfo m27477_MI = 
{
	"get_Current", NULL, &t4113_TI, &t122_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27477_GM};
static MethodInfo* t4113_MIs[] =
{
	&m27477_MI,
	NULL
};
static TypeInfo* t4113_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4113_0_0_0;
extern Il2CppType t4113_1_0_0;
struct t4113;
extern Il2CppGenericClass t4113_GC;
TypeInfo t4113_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4113_MIs, t4113_PIs, NULL, NULL, NULL, NULL, NULL, &t4113_TI, t4113_ITIs, NULL, &EmptyCustomAttributesCache, &t4113_TI, &t4113_0_0_0, &t4113_1_0_0, NULL, &t4113_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2386.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2386_TI;
#include "t2386MD.h"

extern TypeInfo t122_TI;
extern MethodInfo m12353_MI;
extern MethodInfo m20610_MI;
struct t20;
#define m20610(__this, p0, method) (t122 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.TouchInputModule>
extern Il2CppType t20_0_0_1;
FieldInfo t2386_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2386_TI, offsetof(t2386, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2386_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2386_TI, offsetof(t2386, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2386_FIs[] =
{
	&t2386_f0_FieldInfo,
	&t2386_f1_FieldInfo,
	NULL
};
extern MethodInfo m12350_MI;
static PropertyInfo t2386____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2386_TI, "System.Collections.IEnumerator.Current", &m12350_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2386____Current_PropertyInfo = 
{
	&t2386_TI, "Current", &m12353_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2386_PIs[] =
{
	&t2386____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2386____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2386_m12349_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12349_GM;
MethodInfo m12349_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2386_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2386_m12349_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12349_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12350_GM;
MethodInfo m12350_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2386_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12350_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12351_GM;
MethodInfo m12351_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2386_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12351_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12352_GM;
MethodInfo m12352_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2386_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12352_GM};
extern Il2CppType t122_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12353_GM;
MethodInfo m12353_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2386_TI, &t122_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12353_GM};
static MethodInfo* t2386_MIs[] =
{
	&m12349_MI,
	&m12350_MI,
	&m12351_MI,
	&m12352_MI,
	&m12353_MI,
	NULL
};
extern MethodInfo m12352_MI;
extern MethodInfo m12351_MI;
static MethodInfo* t2386_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12350_MI,
	&m12352_MI,
	&m12351_MI,
	&m12353_MI,
};
static TypeInfo* t2386_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4113_TI,
};
static Il2CppInterfaceOffsetPair t2386_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4113_TI, 7},
};
extern TypeInfo t122_TI;
static Il2CppRGCTXData t2386_RGCTXData[3] = 
{
	&m12353_MI/* Method Usage */,
	&t122_TI/* Class Usage */,
	&m20610_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2386_0_0_0;
extern Il2CppType t2386_1_0_0;
extern Il2CppGenericClass t2386_GC;
TypeInfo t2386_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2386_MIs, t2386_PIs, t2386_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2386_TI, t2386_ITIs, t2386_VT, &EmptyCustomAttributesCache, &t2386_TI, &t2386_0_0_0, &t2386_1_0_0, t2386_IOs, &t2386_GC, NULL, NULL, NULL, t2386_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2386)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5271_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.TouchInputModule>
extern MethodInfo m27478_MI;
static PropertyInfo t5271____Count_PropertyInfo = 
{
	&t5271_TI, "Count", &m27478_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27479_MI;
static PropertyInfo t5271____IsReadOnly_PropertyInfo = 
{
	&t5271_TI, "IsReadOnly", &m27479_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5271_PIs[] =
{
	&t5271____Count_PropertyInfo,
	&t5271____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27478_GM;
MethodInfo m27478_MI = 
{
	"get_Count", NULL, &t5271_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27478_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27479_GM;
MethodInfo m27479_MI = 
{
	"get_IsReadOnly", NULL, &t5271_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27479_GM};
extern Il2CppType t122_0_0_0;
extern Il2CppType t122_0_0_0;
static ParameterInfo t5271_m27480_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t122_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27480_GM;
MethodInfo m27480_MI = 
{
	"Add", NULL, &t5271_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5271_m27480_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27480_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27481_GM;
MethodInfo m27481_MI = 
{
	"Clear", NULL, &t5271_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27481_GM};
extern Il2CppType t122_0_0_0;
static ParameterInfo t5271_m27482_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t122_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27482_GM;
MethodInfo m27482_MI = 
{
	"Contains", NULL, &t5271_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5271_m27482_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27482_GM};
extern Il2CppType t3827_0_0_0;
extern Il2CppType t3827_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5271_m27483_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3827_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27483_GM;
MethodInfo m27483_MI = 
{
	"CopyTo", NULL, &t5271_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5271_m27483_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27483_GM};
extern Il2CppType t122_0_0_0;
static ParameterInfo t5271_m27484_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t122_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27484_GM;
MethodInfo m27484_MI = 
{
	"Remove", NULL, &t5271_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5271_m27484_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27484_GM};
static MethodInfo* t5271_MIs[] =
{
	&m27478_MI,
	&m27479_MI,
	&m27480_MI,
	&m27481_MI,
	&m27482_MI,
	&m27483_MI,
	&m27484_MI,
	NULL
};
extern TypeInfo t5273_TI;
static TypeInfo* t5271_ITIs[] = 
{
	&t603_TI,
	&t5273_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5271_0_0_0;
extern Il2CppType t5271_1_0_0;
struct t5271;
extern Il2CppGenericClass t5271_GC;
TypeInfo t5271_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5271_MIs, t5271_PIs, NULL, NULL, NULL, NULL, NULL, &t5271_TI, t5271_ITIs, NULL, &EmptyCustomAttributesCache, &t5271_TI, &t5271_0_0_0, &t5271_1_0_0, NULL, &t5271_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.TouchInputModule>
extern Il2CppType t4113_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27485_GM;
MethodInfo m27485_MI = 
{
	"GetEnumerator", NULL, &t5273_TI, &t4113_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27485_GM};
static MethodInfo* t5273_MIs[] =
{
	&m27485_MI,
	NULL
};
static TypeInfo* t5273_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5273_0_0_0;
extern Il2CppType t5273_1_0_0;
struct t5273;
extern Il2CppGenericClass t5273_GC;
TypeInfo t5273_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5273_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5273_TI, t5273_ITIs, NULL, &EmptyCustomAttributesCache, &t5273_TI, &t5273_0_0_0, &t5273_1_0_0, NULL, &t5273_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5272_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.TouchInputModule>
extern MethodInfo m27486_MI;
extern MethodInfo m27487_MI;
static PropertyInfo t5272____Item_PropertyInfo = 
{
	&t5272_TI, "Item", &m27486_MI, &m27487_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5272_PIs[] =
{
	&t5272____Item_PropertyInfo,
	NULL
};
extern Il2CppType t122_0_0_0;
static ParameterInfo t5272_m27488_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t122_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27488_GM;
MethodInfo m27488_MI = 
{
	"IndexOf", NULL, &t5272_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5272_m27488_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27488_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t122_0_0_0;
static ParameterInfo t5272_m27489_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t122_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27489_GM;
MethodInfo m27489_MI = 
{
	"Insert", NULL, &t5272_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5272_m27489_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27489_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5272_m27490_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27490_GM;
MethodInfo m27490_MI = 
{
	"RemoveAt", NULL, &t5272_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5272_m27490_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27490_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5272_m27486_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t122_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27486_GM;
MethodInfo m27486_MI = 
{
	"get_Item", NULL, &t5272_TI, &t122_0_0_0, RuntimeInvoker_t29_t44, t5272_m27486_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27486_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t122_0_0_0;
static ParameterInfo t5272_m27487_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t122_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27487_GM;
MethodInfo m27487_MI = 
{
	"set_Item", NULL, &t5272_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5272_m27487_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27487_GM};
static MethodInfo* t5272_MIs[] =
{
	&m27488_MI,
	&m27489_MI,
	&m27490_MI,
	&m27486_MI,
	&m27487_MI,
	NULL
};
static TypeInfo* t5272_ITIs[] = 
{
	&t603_TI,
	&t5271_TI,
	&t5273_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5272_0_0_0;
extern Il2CppType t5272_1_0_0;
struct t5272;
extern Il2CppGenericClass t5272_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5272_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5272_MIs, t5272_PIs, NULL, NULL, NULL, NULL, NULL, &t5272_TI, t5272_ITIs, NULL, &t1908__CustomAttributeCache, &t5272_TI, &t5272_0_0_0, &t5272_1_0_0, NULL, &t5272_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2387.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2387_TI;
#include "t2387MD.h"

#include "t2388.h"
extern TypeInfo t2388_TI;
#include "t2388MD.h"
extern MethodInfo m12356_MI;
extern MethodInfo m12358_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.TouchInputModule>
extern Il2CppType t316_0_0_33;
FieldInfo t2387_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2387_TI, offsetof(t2387, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2387_FIs[] =
{
	&t2387_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t122_0_0_0;
static ParameterInfo t2387_m12354_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t122_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12354_GM;
MethodInfo m12354_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2387_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2387_m12354_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12354_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2387_m12355_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12355_GM;
MethodInfo m12355_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2387_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2387_m12355_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12355_GM};
static MethodInfo* t2387_MIs[] =
{
	&m12354_MI,
	&m12355_MI,
	NULL
};
extern MethodInfo m12355_MI;
extern MethodInfo m12359_MI;
static MethodInfo* t2387_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12355_MI,
	&m12359_MI,
};
extern Il2CppType t2389_0_0_0;
extern TypeInfo t2389_TI;
extern MethodInfo m20620_MI;
extern TypeInfo t122_TI;
extern MethodInfo m12361_MI;
extern TypeInfo t122_TI;
static Il2CppRGCTXData t2387_RGCTXData[8] = 
{
	&t2389_0_0_0/* Type Usage */,
	&t2389_TI/* Class Usage */,
	&m20620_MI/* Method Usage */,
	&t122_TI/* Class Usage */,
	&m12361_MI/* Method Usage */,
	&m12356_MI/* Method Usage */,
	&t122_TI/* Class Usage */,
	&m12358_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2387_0_0_0;
extern Il2CppType t2387_1_0_0;
struct t2387;
extern Il2CppGenericClass t2387_GC;
TypeInfo t2387_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2387_MIs, NULL, t2387_FIs, NULL, &t2388_TI, NULL, NULL, &t2387_TI, NULL, t2387_VT, &EmptyCustomAttributesCache, &t2387_TI, &t2387_0_0_0, &t2387_1_0_0, NULL, &t2387_GC, NULL, NULL, NULL, t2387_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2387), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2389.h"
extern TypeInfo t2389_TI;
#include "t2389MD.h"
struct t556;
#define m20620(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.TouchInputModule>
extern Il2CppType t2389_0_0_1;
FieldInfo t2388_f0_FieldInfo = 
{
	"Delegate", &t2389_0_0_1, &t2388_TI, offsetof(t2388, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2388_FIs[] =
{
	&t2388_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2388_m12356_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12356_GM;
MethodInfo m12356_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2388_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2388_m12356_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12356_GM};
extern Il2CppType t2389_0_0_0;
static ParameterInfo t2388_m12357_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2389_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12357_GM;
MethodInfo m12357_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2388_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2388_m12357_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12357_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2388_m12358_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12358_GM;
MethodInfo m12358_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2388_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2388_m12358_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12358_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2388_m12359_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12359_GM;
MethodInfo m12359_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2388_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2388_m12359_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12359_GM};
static MethodInfo* t2388_MIs[] =
{
	&m12356_MI,
	&m12357_MI,
	&m12358_MI,
	&m12359_MI,
	NULL
};
static MethodInfo* t2388_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12358_MI,
	&m12359_MI,
};
extern TypeInfo t2389_TI;
extern TypeInfo t122_TI;
static Il2CppRGCTXData t2388_RGCTXData[5] = 
{
	&t2389_0_0_0/* Type Usage */,
	&t2389_TI/* Class Usage */,
	&m20620_MI/* Method Usage */,
	&t122_TI/* Class Usage */,
	&m12361_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2388_0_0_0;
extern Il2CppType t2388_1_0_0;
struct t2388;
extern Il2CppGenericClass t2388_GC;
TypeInfo t2388_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2388_MIs, NULL, t2388_FIs, NULL, &t556_TI, NULL, NULL, &t2388_TI, NULL, t2388_VT, &EmptyCustomAttributesCache, &t2388_TI, &t2388_0_0_0, &t2388_1_0_0, NULL, &t2388_GC, NULL, NULL, NULL, t2388_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2388), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.TouchInputModule>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2389_m12360_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12360_GM;
MethodInfo m12360_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2389_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2389_m12360_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12360_GM};
extern Il2CppType t122_0_0_0;
static ParameterInfo t2389_m12361_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t122_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12361_GM;
MethodInfo m12361_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2389_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2389_m12361_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12361_GM};
extern Il2CppType t122_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2389_m12362_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t122_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12362_GM;
MethodInfo m12362_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2389_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2389_m12362_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12362_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2389_m12363_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12363_GM;
MethodInfo m12363_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2389_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2389_m12363_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12363_GM};
static MethodInfo* t2389_MIs[] =
{
	&m12360_MI,
	&m12361_MI,
	&m12362_MI,
	&m12363_MI,
	NULL
};
extern MethodInfo m12362_MI;
extern MethodInfo m12363_MI;
static MethodInfo* t2389_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12361_MI,
	&m12362_MI,
	&m12363_MI,
};
static Il2CppInterfaceOffsetPair t2389_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2389_1_0_0;
struct t2389;
extern Il2CppGenericClass t2389_GC;
TypeInfo t2389_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2389_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2389_TI, NULL, t2389_VT, &EmptyCustomAttributesCache, &t2389_TI, &t2389_0_0_0, &t2389_1_0_0, t2389_IOs, &t2389_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2389), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2390.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2390_TI;
#include "t2390MD.h"

#include "t109.h"
#include "t2391.h"
extern TypeInfo t109_TI;
extern TypeInfo t2391_TI;
#include "t2391MD.h"
extern MethodInfo m12366_MI;
extern MethodInfo m12368_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t316_0_0_33;
FieldInfo t2390_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2390_TI, offsetof(t2390, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2390_FIs[] =
{
	&t2390_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t109_0_0_0;
extern Il2CppType t109_0_0_0;
static ParameterInfo t2390_m12364_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12364_GM;
MethodInfo m12364_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2390_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2390_m12364_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12364_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2390_m12365_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12365_GM;
MethodInfo m12365_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2390_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2390_m12365_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12365_GM};
static MethodInfo* t2390_MIs[] =
{
	&m12364_MI,
	&m12365_MI,
	NULL
};
extern MethodInfo m12365_MI;
extern MethodInfo m12369_MI;
static MethodInfo* t2390_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12365_MI,
	&m12369_MI,
};
extern Il2CppType t2392_0_0_0;
extern TypeInfo t2392_TI;
extern MethodInfo m20621_MI;
extern TypeInfo t109_TI;
extern MethodInfo m12371_MI;
extern TypeInfo t109_TI;
static Il2CppRGCTXData t2390_RGCTXData[8] = 
{
	&t2392_0_0_0/* Type Usage */,
	&t2392_TI/* Class Usage */,
	&m20621_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
	&m12371_MI/* Method Usage */,
	&m12366_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
	&m12368_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2390_0_0_0;
extern Il2CppType t2390_1_0_0;
struct t2390;
extern Il2CppGenericClass t2390_GC;
TypeInfo t2390_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2390_MIs, NULL, t2390_FIs, NULL, &t2391_TI, NULL, NULL, &t2390_TI, NULL, t2390_VT, &EmptyCustomAttributesCache, &t2390_TI, &t2390_0_0_0, &t2390_1_0_0, NULL, &t2390_GC, NULL, NULL, NULL, t2390_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2390), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2392.h"
extern TypeInfo t2392_TI;
#include "t2392MD.h"
struct t556;
#define m20621(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t2392_0_0_1;
FieldInfo t2391_f0_FieldInfo = 
{
	"Delegate", &t2392_0_0_1, &t2391_TI, offsetof(t2391, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2391_FIs[] =
{
	&t2391_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2391_m12366_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12366_GM;
MethodInfo m12366_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2391_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2391_m12366_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12366_GM};
extern Il2CppType t2392_0_0_0;
static ParameterInfo t2391_m12367_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2392_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12367_GM;
MethodInfo m12367_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2391_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2391_m12367_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12367_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2391_m12368_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12368_GM;
MethodInfo m12368_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2391_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2391_m12368_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12368_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2391_m12369_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12369_GM;
MethodInfo m12369_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2391_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2391_m12369_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12369_GM};
static MethodInfo* t2391_MIs[] =
{
	&m12366_MI,
	&m12367_MI,
	&m12368_MI,
	&m12369_MI,
	NULL
};
static MethodInfo* t2391_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12368_MI,
	&m12369_MI,
};
extern TypeInfo t2392_TI;
extern TypeInfo t109_TI;
static Il2CppRGCTXData t2391_RGCTXData[5] = 
{
	&t2392_0_0_0/* Type Usage */,
	&t2392_TI/* Class Usage */,
	&m20621_MI/* Method Usage */,
	&t109_TI/* Class Usage */,
	&m12371_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2391_0_0_0;
extern Il2CppType t2391_1_0_0;
struct t2391;
extern Il2CppGenericClass t2391_GC;
TypeInfo t2391_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2391_MIs, NULL, t2391_FIs, NULL, &t556_TI, NULL, NULL, &t2391_TI, NULL, t2391_VT, &EmptyCustomAttributesCache, &t2391_TI, &t2391_0_0_0, &t2391_1_0_0, NULL, &t2391_GC, NULL, NULL, NULL, t2391_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2391), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseRaycaster>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2392_m12370_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12370_GM;
MethodInfo m12370_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2392_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2392_m12370_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12370_GM};
extern Il2CppType t109_0_0_0;
static ParameterInfo t2392_m12371_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12371_GM;
MethodInfo m12371_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2392_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2392_m12371_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12371_GM};
extern Il2CppType t109_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2392_m12372_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t109_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12372_GM;
MethodInfo m12372_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2392_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2392_m12372_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12372_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2392_m12373_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12373_GM;
MethodInfo m12373_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2392_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2392_m12373_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12373_GM};
static MethodInfo* t2392_MIs[] =
{
	&m12370_MI,
	&m12371_MI,
	&m12372_MI,
	&m12373_MI,
	NULL
};
extern MethodInfo m12372_MI;
extern MethodInfo m12373_MI;
static MethodInfo* t2392_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12371_MI,
	&m12372_MI,
	&m12373_MI,
};
static Il2CppInterfaceOffsetPair t2392_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2392_1_0_0;
struct t2392;
extern Il2CppGenericClass t2392_GC;
TypeInfo t2392_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2392_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2392_TI, NULL, t2392_VT, &EmptyCustomAttributesCache, &t2392_TI, &t2392_0_0_0, &t2392_1_0_0, t2392_IOs, &t2392_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2392), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4115_TI;

#include "t123.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.Physics2DRaycaster>
extern MethodInfo m27491_MI;
static PropertyInfo t4115____Current_PropertyInfo = 
{
	&t4115_TI, "Current", &m27491_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4115_PIs[] =
{
	&t4115____Current_PropertyInfo,
	NULL
};
extern Il2CppType t123_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27491_GM;
MethodInfo m27491_MI = 
{
	"get_Current", NULL, &t4115_TI, &t123_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27491_GM};
static MethodInfo* t4115_MIs[] =
{
	&m27491_MI,
	NULL
};
static TypeInfo* t4115_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4115_0_0_0;
extern Il2CppType t4115_1_0_0;
struct t4115;
extern Il2CppGenericClass t4115_GC;
TypeInfo t4115_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4115_MIs, t4115_PIs, NULL, NULL, NULL, NULL, NULL, &t4115_TI, t4115_ITIs, NULL, &EmptyCustomAttributesCache, &t4115_TI, &t4115_0_0_0, &t4115_1_0_0, NULL, &t4115_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2393.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2393_TI;
#include "t2393MD.h"

extern TypeInfo t123_TI;
extern MethodInfo m12378_MI;
extern MethodInfo m20623_MI;
struct t20;
#define m20623(__this, p0, method) (t123 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.Physics2DRaycaster>
extern Il2CppType t20_0_0_1;
FieldInfo t2393_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2393_TI, offsetof(t2393, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2393_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2393_TI, offsetof(t2393, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2393_FIs[] =
{
	&t2393_f0_FieldInfo,
	&t2393_f1_FieldInfo,
	NULL
};
extern MethodInfo m12375_MI;
static PropertyInfo t2393____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2393_TI, "System.Collections.IEnumerator.Current", &m12375_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2393____Current_PropertyInfo = 
{
	&t2393_TI, "Current", &m12378_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2393_PIs[] =
{
	&t2393____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2393____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2393_m12374_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12374_GM;
MethodInfo m12374_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2393_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2393_m12374_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12374_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12375_GM;
MethodInfo m12375_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2393_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12375_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12376_GM;
MethodInfo m12376_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2393_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12376_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12377_GM;
MethodInfo m12377_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2393_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12377_GM};
extern Il2CppType t123_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12378_GM;
MethodInfo m12378_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2393_TI, &t123_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12378_GM};
static MethodInfo* t2393_MIs[] =
{
	&m12374_MI,
	&m12375_MI,
	&m12376_MI,
	&m12377_MI,
	&m12378_MI,
	NULL
};
extern MethodInfo m12377_MI;
extern MethodInfo m12376_MI;
static MethodInfo* t2393_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12375_MI,
	&m12377_MI,
	&m12376_MI,
	&m12378_MI,
};
static TypeInfo* t2393_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4115_TI,
};
static Il2CppInterfaceOffsetPair t2393_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4115_TI, 7},
};
extern TypeInfo t123_TI;
static Il2CppRGCTXData t2393_RGCTXData[3] = 
{
	&m12378_MI/* Method Usage */,
	&t123_TI/* Class Usage */,
	&m20623_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2393_0_0_0;
extern Il2CppType t2393_1_0_0;
extern Il2CppGenericClass t2393_GC;
TypeInfo t2393_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2393_MIs, t2393_PIs, t2393_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2393_TI, t2393_ITIs, t2393_VT, &EmptyCustomAttributesCache, &t2393_TI, &t2393_0_0_0, &t2393_1_0_0, t2393_IOs, &t2393_GC, NULL, NULL, NULL, t2393_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2393)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5274_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.Physics2DRaycaster>
extern MethodInfo m27492_MI;
static PropertyInfo t5274____Count_PropertyInfo = 
{
	&t5274_TI, "Count", &m27492_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27493_MI;
static PropertyInfo t5274____IsReadOnly_PropertyInfo = 
{
	&t5274_TI, "IsReadOnly", &m27493_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5274_PIs[] =
{
	&t5274____Count_PropertyInfo,
	&t5274____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27492_GM;
MethodInfo m27492_MI = 
{
	"get_Count", NULL, &t5274_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27492_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27493_GM;
MethodInfo m27493_MI = 
{
	"get_IsReadOnly", NULL, &t5274_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27493_GM};
extern Il2CppType t123_0_0_0;
extern Il2CppType t123_0_0_0;
static ParameterInfo t5274_m27494_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t123_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27494_GM;
MethodInfo m27494_MI = 
{
	"Add", NULL, &t5274_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5274_m27494_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27494_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27495_GM;
MethodInfo m27495_MI = 
{
	"Clear", NULL, &t5274_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27495_GM};
extern Il2CppType t123_0_0_0;
static ParameterInfo t5274_m27496_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t123_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27496_GM;
MethodInfo m27496_MI = 
{
	"Contains", NULL, &t5274_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5274_m27496_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27496_GM};
extern Il2CppType t3828_0_0_0;
extern Il2CppType t3828_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5274_m27497_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3828_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27497_GM;
MethodInfo m27497_MI = 
{
	"CopyTo", NULL, &t5274_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5274_m27497_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27497_GM};
extern Il2CppType t123_0_0_0;
static ParameterInfo t5274_m27498_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t123_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27498_GM;
MethodInfo m27498_MI = 
{
	"Remove", NULL, &t5274_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5274_m27498_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27498_GM};
static MethodInfo* t5274_MIs[] =
{
	&m27492_MI,
	&m27493_MI,
	&m27494_MI,
	&m27495_MI,
	&m27496_MI,
	&m27497_MI,
	&m27498_MI,
	NULL
};
extern TypeInfo t5276_TI;
static TypeInfo* t5274_ITIs[] = 
{
	&t603_TI,
	&t5276_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5274_0_0_0;
extern Il2CppType t5274_1_0_0;
struct t5274;
extern Il2CppGenericClass t5274_GC;
TypeInfo t5274_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5274_MIs, t5274_PIs, NULL, NULL, NULL, NULL, NULL, &t5274_TI, t5274_ITIs, NULL, &EmptyCustomAttributesCache, &t5274_TI, &t5274_0_0_0, &t5274_1_0_0, NULL, &t5274_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.Physics2DRaycaster>
extern Il2CppType t4115_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27499_GM;
MethodInfo m27499_MI = 
{
	"GetEnumerator", NULL, &t5276_TI, &t4115_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27499_GM};
static MethodInfo* t5276_MIs[] =
{
	&m27499_MI,
	NULL
};
static TypeInfo* t5276_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5276_0_0_0;
extern Il2CppType t5276_1_0_0;
struct t5276;
extern Il2CppGenericClass t5276_GC;
TypeInfo t5276_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5276_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5276_TI, t5276_ITIs, NULL, &EmptyCustomAttributesCache, &t5276_TI, &t5276_0_0_0, &t5276_1_0_0, NULL, &t5276_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5275_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.Physics2DRaycaster>
extern MethodInfo m27500_MI;
extern MethodInfo m27501_MI;
static PropertyInfo t5275____Item_PropertyInfo = 
{
	&t5275_TI, "Item", &m27500_MI, &m27501_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5275_PIs[] =
{
	&t5275____Item_PropertyInfo,
	NULL
};
extern Il2CppType t123_0_0_0;
static ParameterInfo t5275_m27502_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t123_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27502_GM;
MethodInfo m27502_MI = 
{
	"IndexOf", NULL, &t5275_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5275_m27502_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27502_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t123_0_0_0;
static ParameterInfo t5275_m27503_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t123_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27503_GM;
MethodInfo m27503_MI = 
{
	"Insert", NULL, &t5275_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5275_m27503_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27503_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5275_m27504_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27504_GM;
MethodInfo m27504_MI = 
{
	"RemoveAt", NULL, &t5275_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5275_m27504_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27504_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5275_m27500_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t123_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27500_GM;
MethodInfo m27500_MI = 
{
	"get_Item", NULL, &t5275_TI, &t123_0_0_0, RuntimeInvoker_t29_t44, t5275_m27500_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27500_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t123_0_0_0;
static ParameterInfo t5275_m27501_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t123_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27501_GM;
MethodInfo m27501_MI = 
{
	"set_Item", NULL, &t5275_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5275_m27501_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27501_GM};
static MethodInfo* t5275_MIs[] =
{
	&m27502_MI,
	&m27503_MI,
	&m27504_MI,
	&m27500_MI,
	&m27501_MI,
	NULL
};
static TypeInfo* t5275_ITIs[] = 
{
	&t603_TI,
	&t5274_TI,
	&t5276_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5275_0_0_0;
extern Il2CppType t5275_1_0_0;
struct t5275;
extern Il2CppGenericClass t5275_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5275_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5275_MIs, t5275_PIs, NULL, NULL, NULL, NULL, NULL, &t5275_TI, t5275_ITIs, NULL, &t1908__CustomAttributeCache, &t5275_TI, &t5275_0_0_0, &t5275_1_0_0, NULL, &t5275_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5277_TI;

#include "t124.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.PhysicsRaycaster>
extern MethodInfo m27505_MI;
static PropertyInfo t5277____Count_PropertyInfo = 
{
	&t5277_TI, "Count", &m27505_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27506_MI;
static PropertyInfo t5277____IsReadOnly_PropertyInfo = 
{
	&t5277_TI, "IsReadOnly", &m27506_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5277_PIs[] =
{
	&t5277____Count_PropertyInfo,
	&t5277____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27505_GM;
MethodInfo m27505_MI = 
{
	"get_Count", NULL, &t5277_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27505_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27506_GM;
MethodInfo m27506_MI = 
{
	"get_IsReadOnly", NULL, &t5277_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27506_GM};
extern Il2CppType t124_0_0_0;
extern Il2CppType t124_0_0_0;
static ParameterInfo t5277_m27507_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t124_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27507_GM;
MethodInfo m27507_MI = 
{
	"Add", NULL, &t5277_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5277_m27507_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27507_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27508_GM;
MethodInfo m27508_MI = 
{
	"Clear", NULL, &t5277_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27508_GM};
extern Il2CppType t124_0_0_0;
static ParameterInfo t5277_m27509_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t124_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27509_GM;
MethodInfo m27509_MI = 
{
	"Contains", NULL, &t5277_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5277_m27509_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27509_GM};
extern Il2CppType t3829_0_0_0;
extern Il2CppType t3829_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5277_m27510_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3829_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27510_GM;
MethodInfo m27510_MI = 
{
	"CopyTo", NULL, &t5277_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5277_m27510_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27510_GM};
extern Il2CppType t124_0_0_0;
static ParameterInfo t5277_m27511_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t124_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27511_GM;
MethodInfo m27511_MI = 
{
	"Remove", NULL, &t5277_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5277_m27511_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27511_GM};
static MethodInfo* t5277_MIs[] =
{
	&m27505_MI,
	&m27506_MI,
	&m27507_MI,
	&m27508_MI,
	&m27509_MI,
	&m27510_MI,
	&m27511_MI,
	NULL
};
extern TypeInfo t5279_TI;
static TypeInfo* t5277_ITIs[] = 
{
	&t603_TI,
	&t5279_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5277_0_0_0;
extern Il2CppType t5277_1_0_0;
struct t5277;
extern Il2CppGenericClass t5277_GC;
TypeInfo t5277_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5277_MIs, t5277_PIs, NULL, NULL, NULL, NULL, NULL, &t5277_TI, t5277_ITIs, NULL, &EmptyCustomAttributesCache, &t5277_TI, &t5277_0_0_0, &t5277_1_0_0, NULL, &t5277_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.PhysicsRaycaster>
extern Il2CppType t4117_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27512_GM;
MethodInfo m27512_MI = 
{
	"GetEnumerator", NULL, &t5279_TI, &t4117_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27512_GM};
static MethodInfo* t5279_MIs[] =
{
	&m27512_MI,
	NULL
};
static TypeInfo* t5279_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5279_0_0_0;
extern Il2CppType t5279_1_0_0;
struct t5279;
extern Il2CppGenericClass t5279_GC;
TypeInfo t5279_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5279_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5279_TI, t5279_ITIs, NULL, &EmptyCustomAttributesCache, &t5279_TI, &t5279_0_0_0, &t5279_1_0_0, NULL, &t5279_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4117_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.PhysicsRaycaster>
extern MethodInfo m27513_MI;
static PropertyInfo t4117____Current_PropertyInfo = 
{
	&t4117_TI, "Current", &m27513_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4117_PIs[] =
{
	&t4117____Current_PropertyInfo,
	NULL
};
extern Il2CppType t124_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27513_GM;
MethodInfo m27513_MI = 
{
	"get_Current", NULL, &t4117_TI, &t124_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27513_GM};
static MethodInfo* t4117_MIs[] =
{
	&m27513_MI,
	NULL
};
static TypeInfo* t4117_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4117_0_0_0;
extern Il2CppType t4117_1_0_0;
struct t4117;
extern Il2CppGenericClass t4117_GC;
TypeInfo t4117_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4117_MIs, t4117_PIs, NULL, NULL, NULL, NULL, NULL, &t4117_TI, t4117_ITIs, NULL, &EmptyCustomAttributesCache, &t4117_TI, &t4117_0_0_0, &t4117_1_0_0, NULL, &t4117_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2394.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2394_TI;
#include "t2394MD.h"

extern TypeInfo t124_TI;
extern MethodInfo m12383_MI;
extern MethodInfo m20634_MI;
struct t20;
#define m20634(__this, p0, method) (t124 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.EventSystems.PhysicsRaycaster>
extern Il2CppType t20_0_0_1;
FieldInfo t2394_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2394_TI, offsetof(t2394, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2394_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2394_TI, offsetof(t2394, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2394_FIs[] =
{
	&t2394_f0_FieldInfo,
	&t2394_f1_FieldInfo,
	NULL
};
extern MethodInfo m12380_MI;
static PropertyInfo t2394____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2394_TI, "System.Collections.IEnumerator.Current", &m12380_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2394____Current_PropertyInfo = 
{
	&t2394_TI, "Current", &m12383_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2394_PIs[] =
{
	&t2394____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2394____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2394_m12379_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12379_GM;
MethodInfo m12379_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2394_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2394_m12379_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12379_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12380_GM;
MethodInfo m12380_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2394_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12380_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12381_GM;
MethodInfo m12381_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2394_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12381_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12382_GM;
MethodInfo m12382_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2394_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12382_GM};
extern Il2CppType t124_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12383_GM;
MethodInfo m12383_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2394_TI, &t124_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12383_GM};
static MethodInfo* t2394_MIs[] =
{
	&m12379_MI,
	&m12380_MI,
	&m12381_MI,
	&m12382_MI,
	&m12383_MI,
	NULL
};
extern MethodInfo m12382_MI;
extern MethodInfo m12381_MI;
static MethodInfo* t2394_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12380_MI,
	&m12382_MI,
	&m12381_MI,
	&m12383_MI,
};
static TypeInfo* t2394_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4117_TI,
};
static Il2CppInterfaceOffsetPair t2394_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4117_TI, 7},
};
extern TypeInfo t124_TI;
static Il2CppRGCTXData t2394_RGCTXData[3] = 
{
	&m12383_MI/* Method Usage */,
	&t124_TI/* Class Usage */,
	&m20634_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2394_0_0_0;
extern Il2CppType t2394_1_0_0;
extern Il2CppGenericClass t2394_GC;
TypeInfo t2394_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2394_MIs, t2394_PIs, t2394_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2394_TI, t2394_ITIs, t2394_VT, &EmptyCustomAttributesCache, &t2394_TI, &t2394_0_0_0, &t2394_1_0_0, t2394_IOs, &t2394_GC, NULL, NULL, NULL, t2394_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2394)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5278_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.EventSystems.PhysicsRaycaster>
extern MethodInfo m27514_MI;
extern MethodInfo m27515_MI;
static PropertyInfo t5278____Item_PropertyInfo = 
{
	&t5278_TI, "Item", &m27514_MI, &m27515_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5278_PIs[] =
{
	&t5278____Item_PropertyInfo,
	NULL
};
extern Il2CppType t124_0_0_0;
static ParameterInfo t5278_m27516_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t124_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27516_GM;
MethodInfo m27516_MI = 
{
	"IndexOf", NULL, &t5278_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5278_m27516_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27516_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t124_0_0_0;
static ParameterInfo t5278_m27517_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t124_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27517_GM;
MethodInfo m27517_MI = 
{
	"Insert", NULL, &t5278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5278_m27517_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27517_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5278_m27518_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27518_GM;
MethodInfo m27518_MI = 
{
	"RemoveAt", NULL, &t5278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5278_m27518_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27518_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5278_m27514_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t124_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27514_GM;
MethodInfo m27514_MI = 
{
	"get_Item", NULL, &t5278_TI, &t124_0_0_0, RuntimeInvoker_t29_t44, t5278_m27514_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27514_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t124_0_0_0;
static ParameterInfo t5278_m27515_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t124_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27515_GM;
MethodInfo m27515_MI = 
{
	"set_Item", NULL, &t5278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5278_m27515_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27515_GM};
static MethodInfo* t5278_MIs[] =
{
	&m27516_MI,
	&m27517_MI,
	&m27518_MI,
	&m27514_MI,
	&m27515_MI,
	NULL
};
static TypeInfo* t5278_ITIs[] = 
{
	&t603_TI,
	&t5277_TI,
	&t5279_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5278_0_0_0;
extern Il2CppType t5278_1_0_0;
struct t5278;
extern Il2CppGenericClass t5278_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5278_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5278_MIs, t5278_PIs, NULL, NULL, NULL, NULL, NULL, &t5278_TI, t5278_ITIs, NULL, &t1908__CustomAttributeCache, &t5278_TI, &t5278_0_0_0, &t5278_1_0_0, NULL, &t5278_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2395.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2395_TI;
#include "t2395MD.h"

#include "t2396.h"
extern TypeInfo t2396_TI;
#include "t2396MD.h"
extern MethodInfo m12386_MI;
extern MethodInfo m12388_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.Physics2DRaycaster>
extern Il2CppType t316_0_0_33;
FieldInfo t2395_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2395_TI, offsetof(t2395, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2395_FIs[] =
{
	&t2395_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t123_0_0_0;
static ParameterInfo t2395_m12384_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t123_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12384_GM;
MethodInfo m12384_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2395_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2395_m12384_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12384_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2395_m12385_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12385_GM;
MethodInfo m12385_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2395_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2395_m12385_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12385_GM};
static MethodInfo* t2395_MIs[] =
{
	&m12384_MI,
	&m12385_MI,
	NULL
};
extern MethodInfo m12385_MI;
extern MethodInfo m12389_MI;
static MethodInfo* t2395_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12385_MI,
	&m12389_MI,
};
extern Il2CppType t2397_0_0_0;
extern TypeInfo t2397_TI;
extern MethodInfo m20644_MI;
extern TypeInfo t123_TI;
extern MethodInfo m12391_MI;
extern TypeInfo t123_TI;
static Il2CppRGCTXData t2395_RGCTXData[8] = 
{
	&t2397_0_0_0/* Type Usage */,
	&t2397_TI/* Class Usage */,
	&m20644_MI/* Method Usage */,
	&t123_TI/* Class Usage */,
	&m12391_MI/* Method Usage */,
	&m12386_MI/* Method Usage */,
	&t123_TI/* Class Usage */,
	&m12388_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2395_0_0_0;
extern Il2CppType t2395_1_0_0;
struct t2395;
extern Il2CppGenericClass t2395_GC;
TypeInfo t2395_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2395_MIs, NULL, t2395_FIs, NULL, &t2396_TI, NULL, NULL, &t2395_TI, NULL, t2395_VT, &EmptyCustomAttributesCache, &t2395_TI, &t2395_0_0_0, &t2395_1_0_0, NULL, &t2395_GC, NULL, NULL, NULL, t2395_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2395), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2397.h"
extern TypeInfo t2397_TI;
#include "t2397MD.h"
struct t556;
#define m20644(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.Physics2DRaycaster>
extern Il2CppType t2397_0_0_1;
FieldInfo t2396_f0_FieldInfo = 
{
	"Delegate", &t2397_0_0_1, &t2396_TI, offsetof(t2396, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2396_FIs[] =
{
	&t2396_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2396_m12386_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12386_GM;
MethodInfo m12386_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2396_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2396_m12386_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12386_GM};
extern Il2CppType t2397_0_0_0;
static ParameterInfo t2396_m12387_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2397_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12387_GM;
MethodInfo m12387_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2396_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2396_m12387_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12387_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2396_m12388_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12388_GM;
MethodInfo m12388_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2396_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2396_m12388_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12388_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2396_m12389_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12389_GM;
MethodInfo m12389_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2396_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2396_m12389_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12389_GM};
static MethodInfo* t2396_MIs[] =
{
	&m12386_MI,
	&m12387_MI,
	&m12388_MI,
	&m12389_MI,
	NULL
};
static MethodInfo* t2396_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12388_MI,
	&m12389_MI,
};
extern TypeInfo t2397_TI;
extern TypeInfo t123_TI;
static Il2CppRGCTXData t2396_RGCTXData[5] = 
{
	&t2397_0_0_0/* Type Usage */,
	&t2397_TI/* Class Usage */,
	&m20644_MI/* Method Usage */,
	&t123_TI/* Class Usage */,
	&m12391_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2396_0_0_0;
extern Il2CppType t2396_1_0_0;
struct t2396;
extern Il2CppGenericClass t2396_GC;
TypeInfo t2396_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2396_MIs, NULL, t2396_FIs, NULL, &t556_TI, NULL, NULL, &t2396_TI, NULL, t2396_VT, &EmptyCustomAttributesCache, &t2396_TI, &t2396_0_0_0, &t2396_1_0_0, NULL, &t2396_GC, NULL, NULL, NULL, t2396_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2396), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.Physics2DRaycaster>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2397_m12390_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12390_GM;
MethodInfo m12390_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2397_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2397_m12390_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12390_GM};
extern Il2CppType t123_0_0_0;
static ParameterInfo t2397_m12391_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t123_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12391_GM;
MethodInfo m12391_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2397_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2397_m12391_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12391_GM};
extern Il2CppType t123_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2397_m12392_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t123_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12392_GM;
MethodInfo m12392_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2397_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2397_m12392_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12392_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2397_m12393_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12393_GM;
MethodInfo m12393_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2397_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2397_m12393_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12393_GM};
static MethodInfo* t2397_MIs[] =
{
	&m12390_MI,
	&m12391_MI,
	&m12392_MI,
	&m12393_MI,
	NULL
};
extern MethodInfo m12392_MI;
extern MethodInfo m12393_MI;
static MethodInfo* t2397_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12391_MI,
	&m12392_MI,
	&m12393_MI,
};
static Il2CppInterfaceOffsetPair t2397_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2397_1_0_0;
struct t2397;
extern Il2CppGenericClass t2397_GC;
TypeInfo t2397_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2397_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2397_TI, NULL, t2397_VT, &EmptyCustomAttributesCache, &t2397_TI, &t2397_0_0_0, &t2397_1_0_0, t2397_IOs, &t2397_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2397), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4118_TI;

#include "t330.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RaycastHit2D>
extern MethodInfo m27519_MI;
static PropertyInfo t4118____Current_PropertyInfo = 
{
	&t4118_TI, "Current", &m27519_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4118_PIs[] =
{
	&t4118____Current_PropertyInfo,
	NULL
};
extern Il2CppType t330_0_0_0;
extern void* RuntimeInvoker_t330 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27519_GM;
MethodInfo m27519_MI = 
{
	"get_Current", NULL, &t4118_TI, &t330_0_0_0, RuntimeInvoker_t330, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27519_GM};
static MethodInfo* t4118_MIs[] =
{
	&m27519_MI,
	NULL
};
static TypeInfo* t4118_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4118_0_0_0;
extern Il2CppType t4118_1_0_0;
struct t4118;
extern Il2CppGenericClass t4118_GC;
TypeInfo t4118_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4118_MIs, t4118_PIs, NULL, NULL, NULL, NULL, NULL, &t4118_TI, t4118_ITIs, NULL, &EmptyCustomAttributesCache, &t4118_TI, &t4118_0_0_0, &t4118_1_0_0, NULL, &t4118_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2398.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2398_TI;
#include "t2398MD.h"

extern TypeInfo t330_TI;
extern MethodInfo m12398_MI;
extern MethodInfo m20646_MI;
struct t20;
 t330  m20646 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12394_MI;
 void m12394 (t2398 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12395_MI;
 t29 * m12395 (t2398 * __this, MethodInfo* method){
	{
		t330  L_0 = m12398(__this, &m12398_MI);
		t330  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t330_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12396_MI;
 void m12396 (t2398 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12397_MI;
 bool m12397 (t2398 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t330  m12398 (t2398 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t330  L_8 = m20646(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20646_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>
extern Il2CppType t20_0_0_1;
FieldInfo t2398_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2398_TI, offsetof(t2398, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2398_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2398_TI, offsetof(t2398, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2398_FIs[] =
{
	&t2398_f0_FieldInfo,
	&t2398_f1_FieldInfo,
	NULL
};
static PropertyInfo t2398____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2398_TI, "System.Collections.IEnumerator.Current", &m12395_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2398____Current_PropertyInfo = 
{
	&t2398_TI, "Current", &m12398_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2398_PIs[] =
{
	&t2398____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2398____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2398_m12394_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12394_GM;
MethodInfo m12394_MI = 
{
	".ctor", (methodPointerType)&m12394, &t2398_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2398_m12394_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12394_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12395_GM;
MethodInfo m12395_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12395, &t2398_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12395_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12396_GM;
MethodInfo m12396_MI = 
{
	"Dispose", (methodPointerType)&m12396, &t2398_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12396_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12397_GM;
MethodInfo m12397_MI = 
{
	"MoveNext", (methodPointerType)&m12397, &t2398_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12397_GM};
extern Il2CppType t330_0_0_0;
extern void* RuntimeInvoker_t330 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12398_GM;
MethodInfo m12398_MI = 
{
	"get_Current", (methodPointerType)&m12398, &t2398_TI, &t330_0_0_0, RuntimeInvoker_t330, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12398_GM};
static MethodInfo* t2398_MIs[] =
{
	&m12394_MI,
	&m12395_MI,
	&m12396_MI,
	&m12397_MI,
	&m12398_MI,
	NULL
};
static MethodInfo* t2398_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12395_MI,
	&m12397_MI,
	&m12396_MI,
	&m12398_MI,
};
static TypeInfo* t2398_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4118_TI,
};
static Il2CppInterfaceOffsetPair t2398_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4118_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2398_0_0_0;
extern Il2CppType t2398_1_0_0;
extern Il2CppGenericClass t2398_GC;
TypeInfo t2398_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2398_MIs, t2398_PIs, t2398_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2398_TI, t2398_ITIs, t2398_VT, &EmptyCustomAttributesCache, &t2398_TI, &t2398_0_0_0, &t2398_1_0_0, t2398_IOs, &t2398_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2398)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5280_TI;

#include "UnityEngine_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RaycastHit2D>
extern MethodInfo m27520_MI;
static PropertyInfo t5280____Count_PropertyInfo = 
{
	&t5280_TI, "Count", &m27520_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27521_MI;
static PropertyInfo t5280____IsReadOnly_PropertyInfo = 
{
	&t5280_TI, "IsReadOnly", &m27521_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5280_PIs[] =
{
	&t5280____Count_PropertyInfo,
	&t5280____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27520_GM;
MethodInfo m27520_MI = 
{
	"get_Count", NULL, &t5280_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27520_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27521_GM;
MethodInfo m27521_MI = 
{
	"get_IsReadOnly", NULL, &t5280_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27521_GM};
extern Il2CppType t330_0_0_0;
extern Il2CppType t330_0_0_0;
static ParameterInfo t5280_m27522_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t330_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t330 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27522_GM;
MethodInfo m27522_MI = 
{
	"Add", NULL, &t5280_TI, &t21_0_0_0, RuntimeInvoker_t21_t330, t5280_m27522_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27522_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27523_GM;
MethodInfo m27523_MI = 
{
	"Clear", NULL, &t5280_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27523_GM};
extern Il2CppType t330_0_0_0;
static ParameterInfo t5280_m27524_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t330_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t330 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27524_GM;
MethodInfo m27524_MI = 
{
	"Contains", NULL, &t5280_TI, &t40_0_0_0, RuntimeInvoker_t40_t330, t5280_m27524_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27524_GM};
extern Il2CppType t335_0_0_0;
extern Il2CppType t335_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5280_m27525_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t335_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27525_GM;
MethodInfo m27525_MI = 
{
	"CopyTo", NULL, &t5280_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5280_m27525_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27525_GM};
extern Il2CppType t330_0_0_0;
static ParameterInfo t5280_m27526_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t330_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t330 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27526_GM;
MethodInfo m27526_MI = 
{
	"Remove", NULL, &t5280_TI, &t40_0_0_0, RuntimeInvoker_t40_t330, t5280_m27526_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27526_GM};
static MethodInfo* t5280_MIs[] =
{
	&m27520_MI,
	&m27521_MI,
	&m27522_MI,
	&m27523_MI,
	&m27524_MI,
	&m27525_MI,
	&m27526_MI,
	NULL
};
extern TypeInfo t5282_TI;
static TypeInfo* t5280_ITIs[] = 
{
	&t603_TI,
	&t5282_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5280_0_0_0;
extern Il2CppType t5280_1_0_0;
struct t5280;
extern Il2CppGenericClass t5280_GC;
TypeInfo t5280_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5280_MIs, t5280_PIs, NULL, NULL, NULL, NULL, NULL, &t5280_TI, t5280_ITIs, NULL, &EmptyCustomAttributesCache, &t5280_TI, &t5280_0_0_0, &t5280_1_0_0, NULL, &t5280_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RaycastHit2D>
extern Il2CppType t4118_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27527_GM;
MethodInfo m27527_MI = 
{
	"GetEnumerator", NULL, &t5282_TI, &t4118_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27527_GM};
static MethodInfo* t5282_MIs[] =
{
	&m27527_MI,
	NULL
};
static TypeInfo* t5282_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5282_0_0_0;
extern Il2CppType t5282_1_0_0;
struct t5282;
extern Il2CppGenericClass t5282_GC;
TypeInfo t5282_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5282_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5282_TI, t5282_ITIs, NULL, &EmptyCustomAttributesCache, &t5282_TI, &t5282_0_0_0, &t5282_1_0_0, NULL, &t5282_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5281_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RaycastHit2D>
extern MethodInfo m27528_MI;
extern MethodInfo m27529_MI;
static PropertyInfo t5281____Item_PropertyInfo = 
{
	&t5281_TI, "Item", &m27528_MI, &m27529_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5281_PIs[] =
{
	&t5281____Item_PropertyInfo,
	NULL
};
extern Il2CppType t330_0_0_0;
static ParameterInfo t5281_m27530_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t330_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t330 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27530_GM;
MethodInfo m27530_MI = 
{
	"IndexOf", NULL, &t5281_TI, &t44_0_0_0, RuntimeInvoker_t44_t330, t5281_m27530_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27530_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t330_0_0_0;
static ParameterInfo t5281_m27531_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t330_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t330 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27531_GM;
MethodInfo m27531_MI = 
{
	"Insert", NULL, &t5281_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t330, t5281_m27531_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27531_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5281_m27532_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27532_GM;
MethodInfo m27532_MI = 
{
	"RemoveAt", NULL, &t5281_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5281_m27532_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27532_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5281_m27528_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t330_0_0_0;
extern void* RuntimeInvoker_t330_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27528_GM;
MethodInfo m27528_MI = 
{
	"get_Item", NULL, &t5281_TI, &t330_0_0_0, RuntimeInvoker_t330_t44, t5281_m27528_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27528_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t330_0_0_0;
static ParameterInfo t5281_m27529_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t330_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t330 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27529_GM;
MethodInfo m27529_MI = 
{
	"set_Item", NULL, &t5281_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t330, t5281_m27529_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27529_GM};
static MethodInfo* t5281_MIs[] =
{
	&m27530_MI,
	&m27531_MI,
	&m27532_MI,
	&m27528_MI,
	&m27529_MI,
	NULL
};
static TypeInfo* t5281_ITIs[] = 
{
	&t603_TI,
	&t5280_TI,
	&t5282_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5281_0_0_0;
extern Il2CppType t5281_1_0_0;
struct t5281;
extern Il2CppGenericClass t5281_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5281_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5281_MIs, t5281_PIs, NULL, NULL, NULL, NULL, NULL, &t5281_TI, t5281_ITIs, NULL, &t1908__CustomAttributeCache, &t5281_TI, &t5281_0_0_0, &t5281_1_0_0, NULL, &t5281_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2399.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2399_TI;
#include "t2399MD.h"

#include "t2400.h"
extern TypeInfo t2400_TI;
#include "t2400MD.h"
extern MethodInfo m12401_MI;
extern MethodInfo m12403_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.PhysicsRaycaster>
extern Il2CppType t316_0_0_33;
FieldInfo t2399_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2399_TI, offsetof(t2399, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2399_FIs[] =
{
	&t2399_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t124_0_0_0;
static ParameterInfo t2399_m12399_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t124_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12399_GM;
MethodInfo m12399_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2399_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2399_m12399_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12399_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2399_m12400_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12400_GM;
MethodInfo m12400_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2399_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2399_m12400_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12400_GM};
static MethodInfo* t2399_MIs[] =
{
	&m12399_MI,
	&m12400_MI,
	NULL
};
extern MethodInfo m12400_MI;
extern MethodInfo m12404_MI;
static MethodInfo* t2399_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12400_MI,
	&m12404_MI,
};
extern Il2CppType t2401_0_0_0;
extern TypeInfo t2401_TI;
extern MethodInfo m20656_MI;
extern TypeInfo t124_TI;
extern MethodInfo m12406_MI;
extern TypeInfo t124_TI;
static Il2CppRGCTXData t2399_RGCTXData[8] = 
{
	&t2401_0_0_0/* Type Usage */,
	&t2401_TI/* Class Usage */,
	&m20656_MI/* Method Usage */,
	&t124_TI/* Class Usage */,
	&m12406_MI/* Method Usage */,
	&m12401_MI/* Method Usage */,
	&t124_TI/* Class Usage */,
	&m12403_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2399_0_0_0;
extern Il2CppType t2399_1_0_0;
struct t2399;
extern Il2CppGenericClass t2399_GC;
TypeInfo t2399_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2399_MIs, NULL, t2399_FIs, NULL, &t2400_TI, NULL, NULL, &t2399_TI, NULL, t2399_VT, &EmptyCustomAttributesCache, &t2399_TI, &t2399_0_0_0, &t2399_1_0_0, NULL, &t2399_GC, NULL, NULL, NULL, t2399_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2399), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2401.h"
extern TypeInfo t2401_TI;
#include "t2401MD.h"
struct t556;
#define m20656(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.PhysicsRaycaster>
extern Il2CppType t2401_0_0_1;
FieldInfo t2400_f0_FieldInfo = 
{
	"Delegate", &t2401_0_0_1, &t2400_TI, offsetof(t2400, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2400_FIs[] =
{
	&t2400_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2400_m12401_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12401_GM;
MethodInfo m12401_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2400_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2400_m12401_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12401_GM};
extern Il2CppType t2401_0_0_0;
static ParameterInfo t2400_m12402_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2401_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12402_GM;
MethodInfo m12402_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2400_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2400_m12402_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12402_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2400_m12403_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12403_GM;
MethodInfo m12403_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2400_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2400_m12403_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12403_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2400_m12404_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12404_GM;
MethodInfo m12404_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2400_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2400_m12404_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12404_GM};
static MethodInfo* t2400_MIs[] =
{
	&m12401_MI,
	&m12402_MI,
	&m12403_MI,
	&m12404_MI,
	NULL
};
static MethodInfo* t2400_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12403_MI,
	&m12404_MI,
};
extern TypeInfo t2401_TI;
extern TypeInfo t124_TI;
static Il2CppRGCTXData t2400_RGCTXData[5] = 
{
	&t2401_0_0_0/* Type Usage */,
	&t2401_TI/* Class Usage */,
	&m20656_MI/* Method Usage */,
	&t124_TI/* Class Usage */,
	&m12406_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2400_0_0_0;
extern Il2CppType t2400_1_0_0;
struct t2400;
extern Il2CppGenericClass t2400_GC;
TypeInfo t2400_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2400_MIs, NULL, t2400_FIs, NULL, &t556_TI, NULL, NULL, &t2400_TI, NULL, t2400_VT, &EmptyCustomAttributesCache, &t2400_TI, &t2400_0_0_0, &t2400_1_0_0, NULL, &t2400_GC, NULL, NULL, NULL, t2400_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2400), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.PhysicsRaycaster>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2401_m12405_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12405_GM;
MethodInfo m12405_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2401_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2401_m12405_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12405_GM};
extern Il2CppType t124_0_0_0;
static ParameterInfo t2401_m12406_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t124_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12406_GM;
MethodInfo m12406_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2401_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2401_m12406_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12406_GM};
extern Il2CppType t124_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2401_m12407_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t124_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12407_GM;
MethodInfo m12407_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2401_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2401_m12407_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12407_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2401_m12408_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12408_GM;
MethodInfo m12408_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2401_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2401_m12408_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12408_GM};
static MethodInfo* t2401_MIs[] =
{
	&m12405_MI,
	&m12406_MI,
	&m12407_MI,
	&m12408_MI,
	NULL
};
extern MethodInfo m12407_MI;
extern MethodInfo m12408_MI;
static MethodInfo* t2401_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12406_MI,
	&m12407_MI,
	&m12408_MI,
};
static Il2CppInterfaceOffsetPair t2401_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2401_1_0_0;
struct t2401;
extern Il2CppGenericClass t2401_GC;
TypeInfo t2401_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2401_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2401_TI, NULL, t2401_VT, &EmptyCustomAttributesCache, &t2401_TI, &t2401_0_0_0, &t2401_1_0_0, t2401_IOs, &t2401_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2401), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t125.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t125_TI;
#include "t125MD.h"

#include "t127.h"


extern MethodInfo m1484_MI;
 void m1484 (t125 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12409_MI;
 int32_t m12409 (t125 * __this, t127  p0, t127  p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12409((t125 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 *, t29 * __this, t127  p0, t127  p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	typedef int32_t (*FunctionPointerType) (t29 * __this, t127  p0, t127  p1, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m12410_MI;
 t29 * m12410 (t125 * __this, t127  p0, t127  p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t127_TI), &p0);
	__d_args[1] = Box(InitializedTypeInfo(&t127_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m12411_MI;
 int32_t m12411 (t125 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Comparison`1<UnityEngine.RaycastHit>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t125_m1484_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1484_GM;
MethodInfo m1484_MI = 
{
	".ctor", (methodPointerType)&m1484, &t125_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t125_m1484_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1484_GM};
extern Il2CppType t127_0_0_0;
extern Il2CppType t127_0_0_0;
extern Il2CppType t127_0_0_0;
static ParameterInfo t125_m12409_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t127_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t127_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t127_t127 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12409_GM;
MethodInfo m12409_MI = 
{
	"Invoke", (methodPointerType)&m12409, &t125_TI, &t44_0_0_0, RuntimeInvoker_t44_t127_t127, t125_m12409_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12409_GM};
extern Il2CppType t127_0_0_0;
extern Il2CppType t127_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t125_m12410_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t127_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t127_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t127_t127_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12410_GM;
MethodInfo m12410_MI = 
{
	"BeginInvoke", (methodPointerType)&m12410, &t125_TI, &t66_0_0_0, RuntimeInvoker_t29_t127_t127_t29_t29, t125_m12410_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m12410_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t125_m12411_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12411_GM;
MethodInfo m12411_MI = 
{
	"EndInvoke", (methodPointerType)&m12411, &t125_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t125_m12411_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12411_GM};
static MethodInfo* t125_MIs[] =
{
	&m1484_MI,
	&m12409_MI,
	&m12410_MI,
	&m12411_MI,
	NULL
};
static MethodInfo* t125_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12409_MI,
	&m12410_MI,
	&m12411_MI,
};
static Il2CppInterfaceOffsetPair t125_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t125_0_0_0;
extern Il2CppType t125_1_0_0;
struct t125;
extern Il2CppGenericClass t125_GC;
TypeInfo t125_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t125_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t125_TI, NULL, t125_VT, &EmptyCustomAttributesCache, &t125_TI, &t125_0_0_0, &t125_1_0_0, t125_IOs, &t125_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t125), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4119_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RaycastHit>
extern MethodInfo m27533_MI;
static PropertyInfo t4119____Current_PropertyInfo = 
{
	&t4119_TI, "Current", &m27533_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4119_PIs[] =
{
	&t4119____Current_PropertyInfo,
	NULL
};
extern Il2CppType t127_0_0_0;
extern void* RuntimeInvoker_t127 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27533_GM;
MethodInfo m27533_MI = 
{
	"get_Current", NULL, &t4119_TI, &t127_0_0_0, RuntimeInvoker_t127, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27533_GM};
static MethodInfo* t4119_MIs[] =
{
	&m27533_MI,
	NULL
};
static TypeInfo* t4119_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4119_0_0_0;
extern Il2CppType t4119_1_0_0;
struct t4119;
extern Il2CppGenericClass t4119_GC;
TypeInfo t4119_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4119_MIs, t4119_PIs, NULL, NULL, NULL, NULL, NULL, &t4119_TI, t4119_ITIs, NULL, &EmptyCustomAttributesCache, &t4119_TI, &t4119_0_0_0, &t4119_1_0_0, NULL, &t4119_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2402.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2402_TI;
#include "t2402MD.h"

extern TypeInfo t127_TI;
extern MethodInfo m12416_MI;
extern MethodInfo m20658_MI;
struct t20;
 t127  m20658 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12412_MI;
 void m12412 (t2402 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12413_MI;
 t29 * m12413 (t2402 * __this, MethodInfo* method){
	{
		t127  L_0 = m12416(__this, &m12416_MI);
		t127  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t127_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12414_MI;
 void m12414 (t2402 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12415_MI;
 bool m12415 (t2402 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t127  m12416 (t2402 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t127  L_8 = m20658(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20658_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>
extern Il2CppType t20_0_0_1;
FieldInfo t2402_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2402_TI, offsetof(t2402, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2402_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2402_TI, offsetof(t2402, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2402_FIs[] =
{
	&t2402_f0_FieldInfo,
	&t2402_f1_FieldInfo,
	NULL
};
static PropertyInfo t2402____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2402_TI, "System.Collections.IEnumerator.Current", &m12413_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2402____Current_PropertyInfo = 
{
	&t2402_TI, "Current", &m12416_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2402_PIs[] =
{
	&t2402____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2402____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2402_m12412_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12412_GM;
MethodInfo m12412_MI = 
{
	".ctor", (methodPointerType)&m12412, &t2402_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2402_m12412_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12412_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12413_GM;
MethodInfo m12413_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12413, &t2402_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12413_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12414_GM;
MethodInfo m12414_MI = 
{
	"Dispose", (methodPointerType)&m12414, &t2402_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12414_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12415_GM;
MethodInfo m12415_MI = 
{
	"MoveNext", (methodPointerType)&m12415, &t2402_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12415_GM};
extern Il2CppType t127_0_0_0;
extern void* RuntimeInvoker_t127 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12416_GM;
MethodInfo m12416_MI = 
{
	"get_Current", (methodPointerType)&m12416, &t2402_TI, &t127_0_0_0, RuntimeInvoker_t127, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12416_GM};
static MethodInfo* t2402_MIs[] =
{
	&m12412_MI,
	&m12413_MI,
	&m12414_MI,
	&m12415_MI,
	&m12416_MI,
	NULL
};
static MethodInfo* t2402_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12413_MI,
	&m12415_MI,
	&m12414_MI,
	&m12416_MI,
};
static TypeInfo* t2402_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4119_TI,
};
static Il2CppInterfaceOffsetPair t2402_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4119_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2402_0_0_0;
extern Il2CppType t2402_1_0_0;
extern Il2CppGenericClass t2402_GC;
TypeInfo t2402_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2402_MIs, t2402_PIs, t2402_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2402_TI, t2402_ITIs, t2402_VT, &EmptyCustomAttributesCache, &t2402_TI, &t2402_0_0_0, &t2402_1_0_0, t2402_IOs, &t2402_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2402)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5283_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RaycastHit>
extern MethodInfo m27534_MI;
static PropertyInfo t5283____Count_PropertyInfo = 
{
	&t5283_TI, "Count", &m27534_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27535_MI;
static PropertyInfo t5283____IsReadOnly_PropertyInfo = 
{
	&t5283_TI, "IsReadOnly", &m27535_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5283_PIs[] =
{
	&t5283____Count_PropertyInfo,
	&t5283____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27534_GM;
MethodInfo m27534_MI = 
{
	"get_Count", NULL, &t5283_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27534_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27535_GM;
MethodInfo m27535_MI = 
{
	"get_IsReadOnly", NULL, &t5283_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27535_GM};
extern Il2CppType t127_0_0_0;
static ParameterInfo t5283_m27536_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t127_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t127 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27536_GM;
MethodInfo m27536_MI = 
{
	"Add", NULL, &t5283_TI, &t21_0_0_0, RuntimeInvoker_t21_t127, t5283_m27536_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27536_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27537_GM;
MethodInfo m27537_MI = 
{
	"Clear", NULL, &t5283_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27537_GM};
extern Il2CppType t127_0_0_0;
static ParameterInfo t5283_m27538_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t127_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t127 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27538_GM;
MethodInfo m27538_MI = 
{
	"Contains", NULL, &t5283_TI, &t40_0_0_0, RuntimeInvoker_t40_t127, t5283_m27538_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27538_GM};
extern Il2CppType t339_0_0_0;
extern Il2CppType t339_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5283_m27539_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t339_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27539_GM;
MethodInfo m27539_MI = 
{
	"CopyTo", NULL, &t5283_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5283_m27539_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27539_GM};
extern Il2CppType t127_0_0_0;
static ParameterInfo t5283_m27540_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t127_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t127 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27540_GM;
MethodInfo m27540_MI = 
{
	"Remove", NULL, &t5283_TI, &t40_0_0_0, RuntimeInvoker_t40_t127, t5283_m27540_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27540_GM};
static MethodInfo* t5283_MIs[] =
{
	&m27534_MI,
	&m27535_MI,
	&m27536_MI,
	&m27537_MI,
	&m27538_MI,
	&m27539_MI,
	&m27540_MI,
	NULL
};
extern TypeInfo t5285_TI;
static TypeInfo* t5283_ITIs[] = 
{
	&t603_TI,
	&t5285_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5283_0_0_0;
extern Il2CppType t5283_1_0_0;
struct t5283;
extern Il2CppGenericClass t5283_GC;
TypeInfo t5283_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5283_MIs, t5283_PIs, NULL, NULL, NULL, NULL, NULL, &t5283_TI, t5283_ITIs, NULL, &EmptyCustomAttributesCache, &t5283_TI, &t5283_0_0_0, &t5283_1_0_0, NULL, &t5283_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RaycastHit>
extern Il2CppType t4119_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27541_GM;
MethodInfo m27541_MI = 
{
	"GetEnumerator", NULL, &t5285_TI, &t4119_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27541_GM};
static MethodInfo* t5285_MIs[] =
{
	&m27541_MI,
	NULL
};
static TypeInfo* t5285_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5285_0_0_0;
extern Il2CppType t5285_1_0_0;
struct t5285;
extern Il2CppGenericClass t5285_GC;
TypeInfo t5285_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5285_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5285_TI, t5285_ITIs, NULL, &EmptyCustomAttributesCache, &t5285_TI, &t5285_0_0_0, &t5285_1_0_0, NULL, &t5285_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5284_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RaycastHit>
extern MethodInfo m27542_MI;
extern MethodInfo m27543_MI;
static PropertyInfo t5284____Item_PropertyInfo = 
{
	&t5284_TI, "Item", &m27542_MI, &m27543_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5284_PIs[] =
{
	&t5284____Item_PropertyInfo,
	NULL
};
extern Il2CppType t127_0_0_0;
static ParameterInfo t5284_m27544_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t127_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t127 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27544_GM;
MethodInfo m27544_MI = 
{
	"IndexOf", NULL, &t5284_TI, &t44_0_0_0, RuntimeInvoker_t44_t127, t5284_m27544_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27544_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t127_0_0_0;
static ParameterInfo t5284_m27545_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t127_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t127 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27545_GM;
MethodInfo m27545_MI = 
{
	"Insert", NULL, &t5284_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t127, t5284_m27545_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27545_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5284_m27546_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27546_GM;
MethodInfo m27546_MI = 
{
	"RemoveAt", NULL, &t5284_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5284_m27546_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27546_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5284_m27542_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t127_0_0_0;
extern void* RuntimeInvoker_t127_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27542_GM;
MethodInfo m27542_MI = 
{
	"get_Item", NULL, &t5284_TI, &t127_0_0_0, RuntimeInvoker_t127_t44, t5284_m27542_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27542_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t127_0_0_0;
static ParameterInfo t5284_m27543_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t127_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t127 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27543_GM;
MethodInfo m27543_MI = 
{
	"set_Item", NULL, &t5284_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t127, t5284_m27543_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27543_GM};
static MethodInfo* t5284_MIs[] =
{
	&m27544_MI,
	&m27545_MI,
	&m27546_MI,
	&m27542_MI,
	&m27543_MI,
	NULL
};
static TypeInfo* t5284_ITIs[] = 
{
	&t603_TI,
	&t5283_TI,
	&t5285_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5284_0_0_0;
extern Il2CppType t5284_1_0_0;
struct t5284;
extern Il2CppGenericClass t5284_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5284_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5284_MIs, t5284_PIs, NULL, NULL, NULL, NULL, NULL, &t5284_TI, t5284_ITIs, NULL, &t1908__CustomAttributeCache, &t5284_TI, &t5284_0_0_0, &t5284_1_0_0, NULL, &t5284_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t130.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t130_TI;
#include "t130MD.h"

#include "t133.h"
#include "t132.h"
#include "t2403.h"
extern TypeInfo t132_TI;
extern TypeInfo t2403_TI;
#include "t566MD.h"
#include "t2403MD.h"
extern Il2CppType t132_0_0_0;
extern MethodInfo m2812_MI;
extern MethodInfo m12418_MI;
extern MethodInfo m2817_MI;
extern MethodInfo m2818_MI;
extern MethodInfo m2820_MI;
extern MethodInfo m12422_MI;
extern MethodInfo m12423_MI;
extern MethodInfo m2819_MI;


extern MethodInfo m1494_MI;
 void m1494 (t130 * __this, MethodInfo* method){
	{
		__this->f4 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		m2812(__this, &m2812_MI);
		return;
	}
}
extern MethodInfo m1499_MI;
 void m1499 (t130 * __this, t133 * p0, MethodInfo* method){
	{
		t556 * L_0 = m12418(NULL, p0, &m12418_MI);
		m2817(__this, L_0, &m2817_MI);
		return;
	}
}
extern MethodInfo m12417_MI;
 void m12417 (t130 * __this, t133 * p0, MethodInfo* method){
	{
		t29 * L_0 = m2953(p0, &m2953_MI);
		t557 * L_1 = m2951(p0, &m2951_MI);
		m2818(__this, L_0, L_1, &m2818_MI);
		return;
	}
}
extern MethodInfo m1495_MI;
 t557 * m1495 (t130 * __this, t7* p0, t29 * p1, MethodInfo* method){
	{
		t537* L_0 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t132_0_0_0), &m1554_MI);
		ArrayElementTypeCheck (L_0, L_1);
		*((t42 **)(t42 **)SZArrayLdElema(L_0, 0)) = (t42 *)L_1;
		t557 * L_2 = m2820(NULL, p1, p0, L_0, &m2820_MI);
		return L_2;
	}
}
extern MethodInfo m1496_MI;
 t556 * m1496 (t130 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		t2403 * L_0 = (t2403 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2403_TI));
		m12422(L_0, p0, p1, &m12422_MI);
		return L_0;
	}
}
 t556 * m12418 (t29 * __this, t133 * p0, MethodInfo* method){
	{
		t2403 * L_0 = (t2403 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2403_TI));
		m12423(L_0, p0, &m12423_MI);
		return L_0;
	}
}
extern MethodInfo m1498_MI;
 void m1498 (t130 * __this, t132  p0, MethodInfo* method){
	{
		t316* L_0 = (__this->f4);
		t132  L_1 = p0;
		t29 * L_2 = Box(InitializedTypeInfo(&t132_TI), &L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((t29 **)(t29 **)SZArrayLdElema(L_0, 0)) = (t29 *)L_2;
		t316* L_3 = (__this->f4);
		m2819(__this, L_3, &m2819_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
extern Il2CppType t316_0_0_33;
FieldInfo t130_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t130_TI, offsetof(t130, f4), &EmptyCustomAttributesCache};
static FieldInfo* t130_FIs[] =
{
	&t130_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1494_GM;
MethodInfo m1494_MI = 
{
	".ctor", (methodPointerType)&m1494, &t130_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1494_GM};
extern Il2CppType t133_0_0_0;
extern Il2CppType t133_0_0_0;
static ParameterInfo t130_m1499_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t133_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1499_GM;
MethodInfo m1499_MI = 
{
	"AddListener", (methodPointerType)&m1499, &t130_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t130_m1499_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1499_GM};
extern Il2CppType t133_0_0_0;
static ParameterInfo t130_m12417_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t133_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12417_GM;
MethodInfo m12417_MI = 
{
	"RemoveListener", (methodPointerType)&m12417, &t130_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t130_m12417_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12417_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t130_m1495_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1495_GM;
MethodInfo m1495_MI = 
{
	"FindMethod_Impl", (methodPointerType)&m1495, &t130_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29, t130_m1495_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1495_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t130_m1496_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1496_GM;
MethodInfo m1496_MI = 
{
	"GetDelegate", (methodPointerType)&m1496, &t130_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29, t130_m1496_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1496_GM};
extern Il2CppType t133_0_0_0;
static ParameterInfo t130_m12418_ParameterInfos[] = 
{
	{"action", 0, 134217728, &EmptyCustomAttributesCache, &t133_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12418_GM;
MethodInfo m12418_MI = 
{
	"GetDelegate", (methodPointerType)&m12418, &t130_TI, &t556_0_0_0, RuntimeInvoker_t29_t29, t130_m12418_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12418_GM};
extern Il2CppType t132_0_0_0;
static ParameterInfo t130_m1498_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t132_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t132 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1498_GM;
MethodInfo m1498_MI = 
{
	"Invoke", (methodPointerType)&m1498, &t130_TI, &t21_0_0_0, RuntimeInvoker_t21_t132, t130_m1498_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1498_GM};
static MethodInfo* t130_MIs[] =
{
	&m1494_MI,
	&m1499_MI,
	&m12417_MI,
	&m1495_MI,
	&m1496_MI,
	&m12418_MI,
	&m1498_MI,
	NULL
};
extern MethodInfo m1323_MI;
extern MethodInfo m1324_MI;
extern MethodInfo m1325_MI;
static MethodInfo* t130_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m1495_MI,
	&m1496_MI,
};
extern TypeInfo t301_TI;
static Il2CppInterfaceOffsetPair t130_IOs[] = 
{
	{ &t301_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t130_0_0_0;
extern Il2CppType t130_1_0_0;
extern TypeInfo t566_TI;
struct t130;
extern Il2CppGenericClass t130_GC;
TypeInfo t130_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`1", "UnityEngine.Events", t130_MIs, NULL, t130_FIs, NULL, &t566_TI, NULL, NULL, &t130_TI, NULL, t130_VT, &EmptyCustomAttributesCache, &t130_TI, &t130_0_0_0, &t130_1_0_0, t130_IOs, &t130_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t130), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 0, 1, 0, 0, 8, 0, 1};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t133_TI;
#include "t133MD.h"



extern MethodInfo m1594_MI;
 void m1594 (t133 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m12419_MI;
 void m12419 (t133 * __this, t132  p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m12419((t133 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t132  p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	typedef void (*FunctionPointerType) (t29 * __this, t132  p0, MethodInfo* method);
	((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m12420_MI;
 t29 * m12420 (t133 * __this, t132  p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t132_TI), &p0);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m12421_MI;
 void m12421 (t133 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Color>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t133_m1594_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1594_GM;
MethodInfo m1594_MI = 
{
	".ctor", (methodPointerType)&m1594, &t133_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t133_m1594_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1594_GM};
extern Il2CppType t132_0_0_0;
static ParameterInfo t133_m12419_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t132_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t132 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12419_GM;
MethodInfo m12419_MI = 
{
	"Invoke", (methodPointerType)&m12419, &t133_TI, &t21_0_0_0, RuntimeInvoker_t21_t132, t133_m12419_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12419_GM};
extern Il2CppType t132_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t133_m12420_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t132_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t132_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12420_GM;
MethodInfo m12420_MI = 
{
	"BeginInvoke", (methodPointerType)&m12420, &t133_TI, &t66_0_0_0, RuntimeInvoker_t29_t132_t29_t29, t133_m12420_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m12420_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t133_m12421_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12421_GM;
MethodInfo m12421_MI = 
{
	"EndInvoke", (methodPointerType)&m12421, &t133_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t133_m12421_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12421_GM};
static MethodInfo* t133_MIs[] =
{
	&m1594_MI,
	&m12419_MI,
	&m12420_MI,
	&m12421_MI,
	NULL
};
static MethodInfo* t133_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m12419_MI,
	&m12420_MI,
	&m12421_MI,
};
static Il2CppInterfaceOffsetPair t133_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t133_1_0_0;
struct t133;
extern Il2CppGenericClass t133_GC;
TypeInfo t133_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t133_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t133_TI, NULL, t133_VT, &EmptyCustomAttributesCache, &t133_TI, &t133_0_0_0, &t133_1_0_0, t133_IOs, &t133_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t133), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m20671_MI;
struct t556;
 void m20671 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m12422 (t2403 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		m2790(__this, p0, p1, &m2790_MI);
		t133 * L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t133_0_0_0), &m1554_MI);
		t353 * L_2 = m2957(NULL, L_1, p0, p1, &m2957_MI);
		t353 * L_3 = m1597(NULL, L_0, ((t133 *)IsInst(L_2, InitializedTypeInfo(&t133_TI))), &m1597_MI);
		__this->f0 = ((t133 *)Castclass(L_3, InitializedTypeInfo(&t133_TI)));
		return;
	}
}
 void m12423 (t2403 * __this, t133 * p0, MethodInfo* method){
	{
		m2789(__this, &m2789_MI);
		t133 * L_0 = (__this->f0);
		t353 * L_1 = m1597(NULL, L_0, p0, &m1597_MI);
		__this->f0 = ((t133 *)Castclass(L_1, InitializedTypeInfo(&t133_TI)));
		return;
	}
}
extern MethodInfo m12424_MI;
 void m12424 (t2403 * __this, t316* p0, MethodInfo* method){
	{
		if ((((int32_t)(((int32_t)(((t20 *)p0)->max_length)))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral192, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		int32_t L_1 = 0;
		m20671(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_1)), &m20671_MI);
		t133 * L_2 = (__this->f0);
		bool L_3 = m2791(NULL, L_2, &m2791_MI);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		t133 * L_4 = (__this->f0);
		int32_t L_5 = 0;
		VirtActionInvoker1< t132  >::Invoke(&m12419_MI, L_4, ((*(t132 *)((t132 *)UnBox ((*(t29 **)(t29 **)SZArrayLdElema(p0, L_5)), InitializedTypeInfo(&t132_TI))))));
	}

IL_003f:
	{
		return;
	}
}
extern MethodInfo m12425_MI;
 bool m12425 (t2403 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		t133 * L_0 = (__this->f0);
		t29 * L_1 = m2953(L_0, &m2953_MI);
		if ((((t29 *)L_1) != ((t29 *)p0)))
		{
			goto IL_0021;
		}
	}
	{
		t133 * L_2 = (__this->f0);
		t557 * L_3 = m2951(L_2, &m2951_MI);
		G_B3_0 = ((((t557 *)L_3) == ((t557 *)p1))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Color>
extern Il2CppType t133_0_0_1;
FieldInfo t2403_f0_FieldInfo = 
{
	"Delegate", &t133_0_0_1, &t2403_TI, offsetof(t2403, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2403_FIs[] =
{
	&t2403_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2403_m12422_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12422_GM;
MethodInfo m12422_MI = 
{
	".ctor", (methodPointerType)&m12422, &t2403_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2403_m12422_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12422_GM};
extern Il2CppType t133_0_0_0;
static ParameterInfo t2403_m12423_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t133_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12423_GM;
MethodInfo m12423_MI = 
{
	".ctor", (methodPointerType)&m12423, &t2403_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2403_m12423_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12423_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2403_m12424_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12424_GM;
MethodInfo m12424_MI = 
{
	"Invoke", (methodPointerType)&m12424, &t2403_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2403_m12424_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12424_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2403_m12425_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12425_GM;
MethodInfo m12425_MI = 
{
	"Find", (methodPointerType)&m12425, &t2403_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2403_m12425_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m12425_GM};
static MethodInfo* t2403_MIs[] =
{
	&m12422_MI,
	&m12423_MI,
	&m12424_MI,
	&m12425_MI,
	NULL
};
static MethodInfo* t2403_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m12424_MI,
	&m12425_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2403_0_0_0;
extern Il2CppType t2403_1_0_0;
struct t2403;
extern Il2CppGenericClass t2403_GC;
TypeInfo t2403_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2403_MIs, NULL, t2403_FIs, NULL, &t556_TI, NULL, NULL, &t2403_TI, NULL, t2403_VT, &EmptyCustomAttributesCache, &t2403_TI, &t2403_0_0_0, &t2403_1_0_0, NULL, &t2403_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2403), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4121_TI;

#include "t128.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode>
extern MethodInfo m27547_MI;
static PropertyInfo t4121____Current_PropertyInfo = 
{
	&t4121_TI, "Current", &m27547_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4121_PIs[] =
{
	&t4121____Current_PropertyInfo,
	NULL
};
extern Il2CppType t128_0_0_0;
extern void* RuntimeInvoker_t128 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27547_GM;
MethodInfo m27547_MI = 
{
	"get_Current", NULL, &t4121_TI, &t128_0_0_0, RuntimeInvoker_t128, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27547_GM};
static MethodInfo* t4121_MIs[] =
{
	&m27547_MI,
	NULL
};
static TypeInfo* t4121_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4121_0_0_0;
extern Il2CppType t4121_1_0_0;
struct t4121;
extern Il2CppGenericClass t4121_GC;
TypeInfo t4121_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4121_MIs, t4121_PIs, NULL, NULL, NULL, NULL, NULL, &t4121_TI, t4121_ITIs, NULL, &EmptyCustomAttributesCache, &t4121_TI, &t4121_0_0_0, &t4121_1_0_0, NULL, &t4121_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2404.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2404_TI;
#include "t2404MD.h"

extern TypeInfo t128_TI;
extern MethodInfo m12430_MI;
extern MethodInfo m20673_MI;
struct t20;
 int32_t m20673 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m12426_MI;
 void m12426 (t2404 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12427_MI;
 t29 * m12427 (t2404 * __this, MethodInfo* method){
	{
		int32_t L_0 = m12430(__this, &m12430_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t128_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m12428_MI;
 void m12428 (t2404 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m12429_MI;
 bool m12429 (t2404 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m12430 (t2404 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m20673(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20673_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode>
extern Il2CppType t20_0_0_1;
FieldInfo t2404_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2404_TI, offsetof(t2404, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2404_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2404_TI, offsetof(t2404, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2404_FIs[] =
{
	&t2404_f0_FieldInfo,
	&t2404_f1_FieldInfo,
	NULL
};
static PropertyInfo t2404____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2404_TI, "System.Collections.IEnumerator.Current", &m12427_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2404____Current_PropertyInfo = 
{
	&t2404_TI, "Current", &m12430_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2404_PIs[] =
{
	&t2404____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2404____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2404_m12426_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12426_GM;
MethodInfo m12426_MI = 
{
	".ctor", (methodPointerType)&m12426, &t2404_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2404_m12426_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12426_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12427_GM;
MethodInfo m12427_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12427, &t2404_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12427_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12428_GM;
MethodInfo m12428_MI = 
{
	"Dispose", (methodPointerType)&m12428, &t2404_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12428_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12429_GM;
MethodInfo m12429_MI = 
{
	"MoveNext", (methodPointerType)&m12429, &t2404_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12429_GM};
extern Il2CppType t128_0_0_0;
extern void* RuntimeInvoker_t128 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12430_GM;
MethodInfo m12430_MI = 
{
	"get_Current", (methodPointerType)&m12430, &t2404_TI, &t128_0_0_0, RuntimeInvoker_t128, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12430_GM};
static MethodInfo* t2404_MIs[] =
{
	&m12426_MI,
	&m12427_MI,
	&m12428_MI,
	&m12429_MI,
	&m12430_MI,
	NULL
};
static MethodInfo* t2404_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12427_MI,
	&m12429_MI,
	&m12428_MI,
	&m12430_MI,
};
static TypeInfo* t2404_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4121_TI,
};
static Il2CppInterfaceOffsetPair t2404_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4121_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2404_0_0_0;
extern Il2CppType t2404_1_0_0;
extern Il2CppGenericClass t2404_GC;
TypeInfo t2404_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2404_MIs, t2404_PIs, t2404_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2404_TI, t2404_ITIs, t2404_VT, &EmptyCustomAttributesCache, &t2404_TI, &t2404_0_0_0, &t2404_1_0_0, t2404_IOs, &t2404_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2404)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5286_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode>
extern MethodInfo m27548_MI;
static PropertyInfo t5286____Count_PropertyInfo = 
{
	&t5286_TI, "Count", &m27548_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27549_MI;
static PropertyInfo t5286____IsReadOnly_PropertyInfo = 
{
	&t5286_TI, "IsReadOnly", &m27549_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5286_PIs[] =
{
	&t5286____Count_PropertyInfo,
	&t5286____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27548_GM;
MethodInfo m27548_MI = 
{
	"get_Count", NULL, &t5286_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27548_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27549_GM;
MethodInfo m27549_MI = 
{
	"get_IsReadOnly", NULL, &t5286_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27549_GM};
extern Il2CppType t128_0_0_0;
extern Il2CppType t128_0_0_0;
static ParameterInfo t5286_m27550_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t128_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27550_GM;
MethodInfo m27550_MI = 
{
	"Add", NULL, &t5286_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5286_m27550_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27550_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27551_GM;
MethodInfo m27551_MI = 
{
	"Clear", NULL, &t5286_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27551_GM};
extern Il2CppType t128_0_0_0;
static ParameterInfo t5286_m27552_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t128_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27552_GM;
MethodInfo m27552_MI = 
{
	"Contains", NULL, &t5286_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5286_m27552_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27552_GM};
extern Il2CppType t3830_0_0_0;
extern Il2CppType t3830_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5286_m27553_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3830_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27553_GM;
MethodInfo m27553_MI = 
{
	"CopyTo", NULL, &t5286_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5286_m27553_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27553_GM};
extern Il2CppType t128_0_0_0;
static ParameterInfo t5286_m27554_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t128_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27554_GM;
MethodInfo m27554_MI = 
{
	"Remove", NULL, &t5286_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5286_m27554_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27554_GM};
static MethodInfo* t5286_MIs[] =
{
	&m27548_MI,
	&m27549_MI,
	&m27550_MI,
	&m27551_MI,
	&m27552_MI,
	&m27553_MI,
	&m27554_MI,
	NULL
};
extern TypeInfo t5288_TI;
static TypeInfo* t5286_ITIs[] = 
{
	&t603_TI,
	&t5288_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5286_0_0_0;
extern Il2CppType t5286_1_0_0;
struct t5286;
extern Il2CppGenericClass t5286_GC;
TypeInfo t5286_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5286_MIs, t5286_PIs, NULL, NULL, NULL, NULL, NULL, &t5286_TI, t5286_ITIs, NULL, &EmptyCustomAttributesCache, &t5286_TI, &t5286_0_0_0, &t5286_1_0_0, NULL, &t5286_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode>
extern Il2CppType t4121_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27555_GM;
MethodInfo m27555_MI = 
{
	"GetEnumerator", NULL, &t5288_TI, &t4121_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27555_GM};
static MethodInfo* t5288_MIs[] =
{
	&m27555_MI,
	NULL
};
static TypeInfo* t5288_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5288_0_0_0;
extern Il2CppType t5288_1_0_0;
struct t5288;
extern Il2CppGenericClass t5288_GC;
TypeInfo t5288_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5288_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5288_TI, t5288_ITIs, NULL, &EmptyCustomAttributesCache, &t5288_TI, &t5288_0_0_0, &t5288_1_0_0, NULL, &t5288_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5287_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode>
extern MethodInfo m27556_MI;
extern MethodInfo m27557_MI;
static PropertyInfo t5287____Item_PropertyInfo = 
{
	&t5287_TI, "Item", &m27556_MI, &m27557_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5287_PIs[] =
{
	&t5287____Item_PropertyInfo,
	NULL
};
extern Il2CppType t128_0_0_0;
static ParameterInfo t5287_m27558_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t128_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27558_GM;
MethodInfo m27558_MI = 
{
	"IndexOf", NULL, &t5287_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5287_m27558_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27558_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t128_0_0_0;
static ParameterInfo t5287_m27559_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t128_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27559_GM;
MethodInfo m27559_MI = 
{
	"Insert", NULL, &t5287_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5287_m27559_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27559_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5287_m27560_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27560_GM;
MethodInfo m27560_MI = 
{
	"RemoveAt", NULL, &t5287_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5287_m27560_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27560_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5287_m27556_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t128_0_0_0;
extern void* RuntimeInvoker_t128_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27556_GM;
MethodInfo m27556_MI = 
{
	"get_Item", NULL, &t5287_TI, &t128_0_0_0, RuntimeInvoker_t128_t44, t5287_m27556_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27556_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t128_0_0_0;
static ParameterInfo t5287_m27557_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t128_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27557_GM;
MethodInfo m27557_MI = 
{
	"set_Item", NULL, &t5287_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5287_m27557_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27557_GM};
static MethodInfo* t5287_MIs[] =
{
	&m27558_MI,
	&m27559_MI,
	&m27560_MI,
	&m27556_MI,
	&m27557_MI,
	NULL
};
static TypeInfo* t5287_ITIs[] = 
{
	&t603_TI,
	&t5286_TI,
	&t5288_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5287_0_0_0;
extern Il2CppType t5287_1_0_0;
struct t5287;
extern Il2CppGenericClass t5287_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5287_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5287_MIs, t5287_PIs, NULL, NULL, NULL, NULL, NULL, &t5287_TI, t5287_ITIs, NULL, &t1908__CustomAttributeCache, &t5287_TI, &t5287_0_0_0, &t5287_1_0_0, NULL, &t5287_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4123_TI;

#include "t9.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Button>
extern MethodInfo m27561_MI;
static PropertyInfo t4123____Current_PropertyInfo = 
{
	&t4123_TI, "Current", &m27561_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4123_PIs[] =
{
	&t4123____Current_PropertyInfo,
	NULL
};
extern Il2CppType t9_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27561_GM;
MethodInfo m27561_MI = 
{
	"get_Current", NULL, &t4123_TI, &t9_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27561_GM};
static MethodInfo* t4123_MIs[] =
{
	&m27561_MI,
	NULL
};
static TypeInfo* t4123_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4123_0_0_0;
extern Il2CppType t4123_1_0_0;
struct t4123;
extern Il2CppGenericClass t4123_GC;
TypeInfo t4123_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4123_MIs, t4123_PIs, NULL, NULL, NULL, NULL, NULL, &t4123_TI, t4123_ITIs, NULL, &EmptyCustomAttributesCache, &t4123_TI, &t4123_0_0_0, &t4123_1_0_0, NULL, &t4123_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2405.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2405_TI;
#include "t2405MD.h"

extern TypeInfo t9_TI;
extern MethodInfo m12435_MI;
extern MethodInfo m20684_MI;
struct t20;
#define m20684(__this, p0, method) (t9 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Button>
extern Il2CppType t20_0_0_1;
FieldInfo t2405_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2405_TI, offsetof(t2405, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2405_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2405_TI, offsetof(t2405, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2405_FIs[] =
{
	&t2405_f0_FieldInfo,
	&t2405_f1_FieldInfo,
	NULL
};
extern MethodInfo m12432_MI;
static PropertyInfo t2405____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2405_TI, "System.Collections.IEnumerator.Current", &m12432_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2405____Current_PropertyInfo = 
{
	&t2405_TI, "Current", &m12435_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2405_PIs[] =
{
	&t2405____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2405____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2405_m12431_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12431_GM;
MethodInfo m12431_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2405_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2405_m12431_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m12431_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12432_GM;
MethodInfo m12432_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2405_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12432_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12433_GM;
MethodInfo m12433_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2405_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12433_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12434_GM;
MethodInfo m12434_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2405_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12434_GM};
extern Il2CppType t9_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m12435_GM;
MethodInfo m12435_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2405_TI, &t9_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m12435_GM};
static MethodInfo* t2405_MIs[] =
{
	&m12431_MI,
	&m12432_MI,
	&m12433_MI,
	&m12434_MI,
	&m12435_MI,
	NULL
};
extern MethodInfo m12434_MI;
extern MethodInfo m12433_MI;
static MethodInfo* t2405_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m12432_MI,
	&m12434_MI,
	&m12433_MI,
	&m12435_MI,
};
static TypeInfo* t2405_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4123_TI,
};
static Il2CppInterfaceOffsetPair t2405_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4123_TI, 7},
};
extern TypeInfo t9_TI;
static Il2CppRGCTXData t2405_RGCTXData[3] = 
{
	&m12435_MI/* Method Usage */,
	&t9_TI/* Class Usage */,
	&m20684_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2405_0_0_0;
extern Il2CppType t2405_1_0_0;
extern Il2CppGenericClass t2405_GC;
TypeInfo t2405_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2405_MIs, t2405_PIs, t2405_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2405_TI, t2405_ITIs, t2405_VT, &EmptyCustomAttributesCache, &t2405_TI, &t2405_0_0_0, &t2405_1_0_0, t2405_IOs, &t2405_GC, NULL, NULL, NULL, t2405_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2405)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5289_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Button>
extern MethodInfo m27562_MI;
static PropertyInfo t5289____Count_PropertyInfo = 
{
	&t5289_TI, "Count", &m27562_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27563_MI;
static PropertyInfo t5289____IsReadOnly_PropertyInfo = 
{
	&t5289_TI, "IsReadOnly", &m27563_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5289_PIs[] =
{
	&t5289____Count_PropertyInfo,
	&t5289____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27562_GM;
MethodInfo m27562_MI = 
{
	"get_Count", NULL, &t5289_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27562_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27563_GM;
MethodInfo m27563_MI = 
{
	"get_IsReadOnly", NULL, &t5289_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27563_GM};
extern Il2CppType t9_0_0_0;
extern Il2CppType t9_0_0_0;
static ParameterInfo t5289_m27564_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t9_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27564_GM;
MethodInfo m27564_MI = 
{
	"Add", NULL, &t5289_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5289_m27564_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27564_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27565_GM;
MethodInfo m27565_MI = 
{
	"Clear", NULL, &t5289_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27565_GM};
extern Il2CppType t9_0_0_0;
static ParameterInfo t5289_m27566_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t9_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27566_GM;
MethodInfo m27566_MI = 
{
	"Contains", NULL, &t5289_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5289_m27566_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27566_GM};
extern Il2CppType t3831_0_0_0;
extern Il2CppType t3831_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5289_m27567_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3831_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27567_GM;
MethodInfo m27567_MI = 
{
	"CopyTo", NULL, &t5289_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5289_m27567_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27567_GM};
extern Il2CppType t9_0_0_0;
static ParameterInfo t5289_m27568_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t9_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27568_GM;
MethodInfo m27568_MI = 
{
	"Remove", NULL, &t5289_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5289_m27568_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27568_GM};
static MethodInfo* t5289_MIs[] =
{
	&m27562_MI,
	&m27563_MI,
	&m27564_MI,
	&m27565_MI,
	&m27566_MI,
	&m27567_MI,
	&m27568_MI,
	NULL
};
extern TypeInfo t5291_TI;
static TypeInfo* t5289_ITIs[] = 
{
	&t603_TI,
	&t5291_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5289_0_0_0;
extern Il2CppType t5289_1_0_0;
struct t5289;
extern Il2CppGenericClass t5289_GC;
TypeInfo t5289_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5289_MIs, t5289_PIs, NULL, NULL, NULL, NULL, NULL, &t5289_TI, t5289_ITIs, NULL, &EmptyCustomAttributesCache, &t5289_TI, &t5289_0_0_0, &t5289_1_0_0, NULL, &t5289_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Button>
extern Il2CppType t4123_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27569_GM;
MethodInfo m27569_MI = 
{
	"GetEnumerator", NULL, &t5291_TI, &t4123_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27569_GM};
static MethodInfo* t5291_MIs[] =
{
	&m27569_MI,
	NULL
};
static TypeInfo* t5291_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5291_0_0_0;
extern Il2CppType t5291_1_0_0;
struct t5291;
extern Il2CppGenericClass t5291_GC;
TypeInfo t5291_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5291_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5291_TI, t5291_ITIs, NULL, &EmptyCustomAttributesCache, &t5291_TI, &t5291_0_0_0, &t5291_1_0_0, NULL, &t5291_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5290_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Button>
extern MethodInfo m27570_MI;
extern MethodInfo m27571_MI;
static PropertyInfo t5290____Item_PropertyInfo = 
{
	&t5290_TI, "Item", &m27570_MI, &m27571_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5290_PIs[] =
{
	&t5290____Item_PropertyInfo,
	NULL
};
extern Il2CppType t9_0_0_0;
static ParameterInfo t5290_m27572_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t9_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27572_GM;
MethodInfo m27572_MI = 
{
	"IndexOf", NULL, &t5290_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5290_m27572_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27572_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t9_0_0_0;
static ParameterInfo t5290_m27573_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t9_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27573_GM;
MethodInfo m27573_MI = 
{
	"Insert", NULL, &t5290_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5290_m27573_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27573_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5290_m27574_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27574_GM;
MethodInfo m27574_MI = 
{
	"RemoveAt", NULL, &t5290_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5290_m27574_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27574_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5290_m27570_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t9_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27570_GM;
MethodInfo m27570_MI = 
{
	"get_Item", NULL, &t5290_TI, &t9_0_0_0, RuntimeInvoker_t29_t44, t5290_m27570_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27570_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t9_0_0_0;
static ParameterInfo t5290_m27571_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t9_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27571_GM;
MethodInfo m27571_MI = 
{
	"set_Item", NULL, &t5290_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5290_m27571_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27571_GM};
static MethodInfo* t5290_MIs[] =
{
	&m27572_MI,
	&m27573_MI,
	&m27574_MI,
	&m27570_MI,
	&m27571_MI,
	NULL
};
static TypeInfo* t5290_ITIs[] = 
{
	&t603_TI,
	&t5289_TI,
	&t5291_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5290_0_0_0;
extern Il2CppType t5290_1_0_0;
struct t5290;
extern Il2CppGenericClass t5290_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5290_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5290_MIs, t5290_PIs, NULL, NULL, NULL, NULL, NULL, &t5290_TI, t5290_ITIs, NULL, &t1908__CustomAttributeCache, &t5290_TI, &t5290_0_0_0, &t5290_1_0_0, NULL, &t5290_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2640_TI;

#include "t139.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Selectable>
extern MethodInfo m27575_MI;
static PropertyInfo t2640____Count_PropertyInfo = 
{
	&t2640_TI, "Count", &m27575_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27576_MI;
static PropertyInfo t2640____IsReadOnly_PropertyInfo = 
{
	&t2640_TI, "IsReadOnly", &m27576_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2640_PIs[] =
{
	&t2640____Count_PropertyInfo,
	&t2640____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27575_GM;
MethodInfo m27575_MI = 
{
	"get_Count", NULL, &t2640_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27575_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27576_GM;
MethodInfo m27576_MI = 
{
	"get_IsReadOnly", NULL, &t2640_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27576_GM};
extern Il2CppType t139_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2640_m27577_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27577_GM;
MethodInfo m27577_MI = 
{
	"Add", NULL, &t2640_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2640_m27577_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27577_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27578_GM;
MethodInfo m27578_MI = 
{
	"Clear", NULL, &t2640_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27578_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2640_m27579_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27579_GM;
MethodInfo m27579_MI = 
{
	"Contains", NULL, &t2640_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2640_m27579_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27579_GM};
extern Il2CppType t2638_0_0_0;
extern Il2CppType t2638_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2640_m27580_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2638_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27580_GM;
MethodInfo m27580_MI = 
{
	"CopyTo", NULL, &t2640_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2640_m27580_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27580_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2640_m27581_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27581_GM;
MethodInfo m27581_MI = 
{
	"Remove", NULL, &t2640_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2640_m27581_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27581_GM};
static MethodInfo* t2640_MIs[] =
{
	&m27575_MI,
	&m27576_MI,
	&m27577_MI,
	&m27578_MI,
	&m27579_MI,
	&m27580_MI,
	&m27581_MI,
	NULL
};
extern TypeInfo t2641_TI;
static TypeInfo* t2640_ITIs[] = 
{
	&t603_TI,
	&t2641_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2640_0_0_0;
extern Il2CppType t2640_1_0_0;
struct t2640;
extern Il2CppGenericClass t2640_GC;
TypeInfo t2640_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2640_MIs, t2640_PIs, NULL, NULL, NULL, NULL, NULL, &t2640_TI, t2640_ITIs, NULL, &EmptyCustomAttributesCache, &t2640_TI, &t2640_0_0_0, &t2640_1_0_0, NULL, &t2640_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Selectable>
extern Il2CppType t2639_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27582_GM;
MethodInfo m27582_MI = 
{
	"GetEnumerator", NULL, &t2641_TI, &t2639_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27582_GM};
static MethodInfo* t2641_MIs[] =
{
	&m27582_MI,
	NULL
};
static TypeInfo* t2641_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2641_0_0_0;
extern Il2CppType t2641_1_0_0;
struct t2641;
extern Il2CppGenericClass t2641_GC;
TypeInfo t2641_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2641_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2641_TI, t2641_ITIs, NULL, &EmptyCustomAttributesCache, &t2641_TI, &t2641_0_0_0, &t2641_1_0_0, NULL, &t2641_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
