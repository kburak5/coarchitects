﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3494;
struct t29;
#include "t1648.h"

 void m19416 (t3494 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19417 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19418 (t3494 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3494 * m19419 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
