﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3217;
struct t29;
struct t20;
#include "t794.h"

 void m17885 (t3217 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17886 (t3217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17887 (t3217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17888 (t3217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17889 (t3217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
