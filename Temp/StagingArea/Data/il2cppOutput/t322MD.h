﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t322;
struct t6;
struct t7;

 void m12074 (t322 * __this, int32_t p0, t6 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1435 (t322 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12075 (t322 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t6 * m1434 (t322 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12076 (t322 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1462 (t322 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
