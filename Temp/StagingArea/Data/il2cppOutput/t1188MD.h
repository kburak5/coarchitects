﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1188;
struct t1188_marshaled;

void t1188_marshal(const t1188& unmarshaled, t1188_marshaled& marshaled);
void t1188_marshal_back(const t1188_marshaled& marshaled, t1188& unmarshaled);
void t1188_marshal_cleanup(t1188_marshaled& marshaled);
