﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3437;
struct t29;
struct t20;
#include "t465.h"

 void m19055 (t3437 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19056 (t3437 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19057 (t3437 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19058 (t3437 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m19059 (t3437 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
