﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t221;
struct t2632;
struct t557;
struct t7;
struct t29;
struct t556;
#include "t17.h"

 void m1838 (t221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14152 (t221 * __this, t2632 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14153 (t221 * __this, t2632 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m1839 (t221 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t556 * m1840 (t221 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t556 * m14154 (t29 * __this, t2632 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1851 (t221 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
