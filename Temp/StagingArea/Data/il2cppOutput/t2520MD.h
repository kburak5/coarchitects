﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2520;
struct t29;
#include "t184.h"

 void m13373 (t2520 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13374 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13375 (t2520 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13376 (t2520 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2520 * m13377 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
