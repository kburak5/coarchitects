﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1052;
struct t777;
struct t781;
struct t7;

 void m4822 (t1052 * __this, t777 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4823 (t1052 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4824 (t1052 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4825 (t1052 * __this, t777 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
