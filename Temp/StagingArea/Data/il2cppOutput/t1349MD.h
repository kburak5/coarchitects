﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1349;
struct t42;
struct t7;
struct t1142;
struct t557;
struct t638;
struct t29;
struct t631;
struct t316;
struct t633;
struct t295;
struct t537;
#include "t1343.h"
#include "t1342.h"
#include "t1150.h"
#include "t630.h"

 bool m7379 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1343  m7380 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7381 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7382 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7383 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7384 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7385 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7386 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m7387 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t638* m7388 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7389 (t1349 * __this, t29 * p0, int32_t p1, t631 * p2, t316* p3, t633 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7390 (t1349 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7391 (t1349 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7392 (t1349 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7393 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7394 (t1349 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7395 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7396 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m7397 (t1349 * __this, t537* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7398 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7399 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7400 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1142 * m7401 (t1349 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
