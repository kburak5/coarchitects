﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t913;
struct t633;
struct t1191;
struct t29;

 void m6673 (t913 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6674 (t913 * __this, t633 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6675 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6676 (t29 * __this, t633 * p0, t633 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6677 (t29 * __this, t1191 * p0, t633 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t913 * m3957 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6678 (t913 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
