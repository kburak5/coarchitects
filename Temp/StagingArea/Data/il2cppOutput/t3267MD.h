﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3267;
struct t29;
struct t20;

 void m18131 (t3267 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18132 (t3267 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18133 (t3267 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18134 (t3267 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m18135 (t3267 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
