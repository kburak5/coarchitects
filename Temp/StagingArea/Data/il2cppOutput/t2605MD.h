﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2605;
struct t29;
struct t20;
#include "t380.h"

 void m14031 (t2605 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14032 (t2605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14033 (t2605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14034 (t2605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t380  m14035 (t2605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
