﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1326;
struct t200;
struct t7;

 void m7159 (t1326 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7160 (t1326 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7161 (t1326 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7162 (t1326 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7163 (t1326 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7164 (t1326 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
