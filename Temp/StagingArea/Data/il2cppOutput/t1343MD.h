﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1343;
struct t733;
struct t29;
#include "t35.h"
#include "t735.h"

 void m9497 (t1343 * __this, t35 p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9498 (t1343 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m9499 (t1343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9500 (t1343 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9501 (t1343 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9502 (t1343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
