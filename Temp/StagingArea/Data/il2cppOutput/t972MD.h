﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t972;
struct t976;
struct t781;
struct t7;
struct t29;
#include "t970.h"

 void m4315 (t972 * __this, int32_t p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4316 (t972 * __this, t972 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4317 (t972 * __this, t972 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4318 (t972 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4319 (t972 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4320 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t976 * m4321 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4322 (t29 * __this, int32_t p0, t976 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4323 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4324 (t972 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4325 (t972 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4326 (t972 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4327 (t972 * __this, uint32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4328 (t972 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4329 (t972 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4330 (t972 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4331 (t972 * __this, uint32_t p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4332 (t972 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4333 (t972 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4334 (t972 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4335 (t972 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4336 (t972 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4337 (t972 * __this, t972 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4338 (t972 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4339 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4340 (t972 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4341 (t29 * __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4342 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4343 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4344 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m4345 (t29 * __this, t972 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4346 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4347 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4348 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4349 (t29 * __this, t972 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t972 * m4350 (t29 * __this, t972 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4351 (t29 * __this, t972 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4352 (t29 * __this, t972 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4353 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4354 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4355 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4356 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4357 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4358 (t29 * __this, t972 * p0, t972 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
