﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2729;
struct t29;
struct t20;
#include "t249.h"

 void m14896 (t2729 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14897 (t2729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14898 (t2729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14899 (t2729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14900 (t2729 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
