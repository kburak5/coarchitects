﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t26;
struct t7;
#include "t23.h"
#include "t17.h"
#include "t386.h"
#include "t119.h"

 void m2542 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1441 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1440 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1460 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1417 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1418 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m37 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1420 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1439 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t119  m1458 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1459 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1457 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1815 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1727 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2543 (t29 * __this, t17 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1802 (t29 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
