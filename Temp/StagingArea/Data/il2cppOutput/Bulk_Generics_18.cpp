﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t5651_TI;


#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.ISerializable>
extern Il2CppType t4402_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29552_GM;
MethodInfo m29552_MI = 
{
	"GetEnumerator", NULL, &t5651_TI, &t4402_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29552_GM};
static MethodInfo* t5651_MIs[] =
{
	&m29552_MI,
	NULL
};
extern TypeInfo t603_TI;
static TypeInfo* t5651_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5651_0_0_0;
extern Il2CppType t5651_1_0_0;
struct t5651;
extern Il2CppGenericClass t5651_GC;
TypeInfo t5651_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5651_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5651_TI, t5651_ITIs, NULL, &EmptyCustomAttributesCache, &t5651_TI, &t5651_0_0_0, &t5651_1_0_0, NULL, &t5651_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4402_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.ISerializable>
extern MethodInfo m29553_MI;
static PropertyInfo t4402____Current_PropertyInfo = 
{
	&t4402_TI, "Current", &m29553_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4402_PIs[] =
{
	&t4402____Current_PropertyInfo,
	NULL
};
extern Il2CppType t374_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29553_GM;
MethodInfo m29553_MI = 
{
	"get_Current", NULL, &t4402_TI, &t374_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29553_GM};
static MethodInfo* t4402_MIs[] =
{
	&m29553_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4402_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4402_0_0_0;
extern Il2CppType t4402_1_0_0;
struct t4402;
extern Il2CppGenericClass t4402_GC;
TypeInfo t4402_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4402_MIs, t4402_PIs, NULL, NULL, NULL, NULL, NULL, &t4402_TI, t4402_ITIs, NULL, &EmptyCustomAttributesCache, &t4402_TI, &t4402_0_0_0, &t4402_1_0_0, NULL, &t4402_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2957.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2957_TI;
#include "t2957MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t40.h"
extern TypeInfo t374_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m16165_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m22438_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m22438(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>
extern Il2CppType t20_0_0_1;
FieldInfo t2957_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2957_TI, offsetof(t2957, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2957_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2957_TI, offsetof(t2957, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2957_FIs[] =
{
	&t2957_f0_FieldInfo,
	&t2957_f1_FieldInfo,
	NULL
};
extern MethodInfo m16162_MI;
static PropertyInfo t2957____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2957_TI, "System.Collections.IEnumerator.Current", &m16162_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2957____Current_PropertyInfo = 
{
	&t2957_TI, "Current", &m16165_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2957_PIs[] =
{
	&t2957____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2957____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2957_m16161_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16161_GM;
MethodInfo m16161_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2957_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2957_m16161_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16161_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16162_GM;
MethodInfo m16162_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2957_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16162_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16163_GM;
MethodInfo m16163_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2957_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16163_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16164_GM;
MethodInfo m16164_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2957_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16164_GM};
extern Il2CppType t374_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16165_GM;
MethodInfo m16165_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2957_TI, &t374_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16165_GM};
static MethodInfo* t2957_MIs[] =
{
	&m16161_MI,
	&m16162_MI,
	&m16163_MI,
	&m16164_MI,
	&m16165_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m16164_MI;
extern MethodInfo m16163_MI;
static MethodInfo* t2957_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16162_MI,
	&m16164_MI,
	&m16163_MI,
	&m16165_MI,
};
static TypeInfo* t2957_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4402_TI,
};
static Il2CppInterfaceOffsetPair t2957_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4402_TI, 7},
};
extern TypeInfo t374_TI;
static Il2CppRGCTXData t2957_RGCTXData[3] = 
{
	&m16165_MI/* Method Usage */,
	&t374_TI/* Class Usage */,
	&m22438_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2957_0_0_0;
extern Il2CppType t2957_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2957_GC;
extern TypeInfo t20_TI;
TypeInfo t2957_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2957_MIs, t2957_PIs, t2957_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2957_TI, t2957_ITIs, t2957_VT, &EmptyCustomAttributesCache, &t2957_TI, &t2957_0_0_0, &t2957_1_0_0, t2957_IOs, &t2957_GC, NULL, NULL, NULL, t2957_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2957)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5650_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>
extern MethodInfo m29554_MI;
extern MethodInfo m29555_MI;
static PropertyInfo t5650____Item_PropertyInfo = 
{
	&t5650_TI, "Item", &m29554_MI, &m29555_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5650_PIs[] =
{
	&t5650____Item_PropertyInfo,
	NULL
};
extern Il2CppType t374_0_0_0;
extern Il2CppType t374_0_0_0;
static ParameterInfo t5650_m29556_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t374_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29556_GM;
MethodInfo m29556_MI = 
{
	"IndexOf", NULL, &t5650_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5650_m29556_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29556_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t374_0_0_0;
static ParameterInfo t5650_m29557_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t374_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29557_GM;
MethodInfo m29557_MI = 
{
	"Insert", NULL, &t5650_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5650_m29557_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29557_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5650_m29558_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29558_GM;
MethodInfo m29558_MI = 
{
	"RemoveAt", NULL, &t5650_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5650_m29558_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29558_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5650_m29554_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t374_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29554_GM;
MethodInfo m29554_MI = 
{
	"get_Item", NULL, &t5650_TI, &t374_0_0_0, RuntimeInvoker_t29_t44, t5650_m29554_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29554_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t374_0_0_0;
static ParameterInfo t5650_m29555_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t374_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29555_GM;
MethodInfo m29555_MI = 
{
	"set_Item", NULL, &t5650_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5650_m29555_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29555_GM};
static MethodInfo* t5650_MIs[] =
{
	&m29556_MI,
	&m29557_MI,
	&m29558_MI,
	&m29554_MI,
	&m29555_MI,
	NULL
};
extern TypeInfo t5649_TI;
static TypeInfo* t5650_ITIs[] = 
{
	&t603_TI,
	&t5649_TI,
	&t5651_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5650_0_0_0;
extern Il2CppType t5650_1_0_0;
struct t5650;
extern Il2CppGenericClass t5650_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5650_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5650_MIs, t5650_PIs, NULL, NULL, NULL, NULL, NULL, &t5650_TI, t5650_ITIs, NULL, &t1908__CustomAttributeCache, &t5650_TI, &t5650_0_0_0, &t5650_1_0_0, NULL, &t5650_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2958.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2958_TI;
#include "t2958MD.h"

#include "t41.h"
#include "t557.h"
#include "t4.h"
#include "mscorlib_ArrayTypes.h"
#include "t2959.h"
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t4_TI;
extern TypeInfo t2959_TI;
extern TypeInfo t21_TI;
#include "t2959MD.h"
extern MethodInfo m16168_MI;
extern MethodInfo m16170_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>
extern Il2CppType t316_0_0_33;
FieldInfo t2958_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2958_TI, offsetof(t2958, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2958_FIs[] =
{
	&t2958_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t4_0_0_0;
extern Il2CppType t4_0_0_0;
static ParameterInfo t2958_m16166_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t4_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16166_GM;
MethodInfo m16166_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2958_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2958_m16166_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16166_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2958_m16167_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16167_GM;
MethodInfo m16167_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2958_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2958_m16167_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16167_GM};
static MethodInfo* t2958_MIs[] =
{
	&m16166_MI,
	&m16167_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m16167_MI;
extern MethodInfo m16171_MI;
static MethodInfo* t2958_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16167_MI,
	&m16171_MI,
};
extern Il2CppType t2960_0_0_0;
extern TypeInfo t2960_TI;
extern MethodInfo m22448_MI;
extern TypeInfo t4_TI;
extern MethodInfo m16173_MI;
extern TypeInfo t4_TI;
static Il2CppRGCTXData t2958_RGCTXData[8] = 
{
	&t2960_0_0_0/* Type Usage */,
	&t2960_TI/* Class Usage */,
	&m22448_MI/* Method Usage */,
	&t4_TI/* Class Usage */,
	&m16173_MI/* Method Usage */,
	&m16168_MI/* Method Usage */,
	&t4_TI/* Class Usage */,
	&m16170_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2958_0_0_0;
extern Il2CppType t2958_1_0_0;
struct t2958;
extern Il2CppGenericClass t2958_GC;
TypeInfo t2958_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2958_MIs, NULL, t2958_FIs, NULL, &t2959_TI, NULL, NULL, &t2958_TI, NULL, t2958_VT, &EmptyCustomAttributesCache, &t2958_TI, &t2958_0_0_0, &t2958_1_0_0, NULL, &t2958_GC, NULL, NULL, NULL, t2958_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2958), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2960.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "t305.h"
extern TypeInfo t2960_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t2960MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m22448(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>
extern Il2CppType t2960_0_0_1;
FieldInfo t2959_f0_FieldInfo = 
{
	"Delegate", &t2960_0_0_1, &t2959_TI, offsetof(t2959, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2959_FIs[] =
{
	&t2959_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2959_m16168_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16168_GM;
MethodInfo m16168_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2959_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2959_m16168_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16168_GM};
extern Il2CppType t2960_0_0_0;
static ParameterInfo t2959_m16169_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2960_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16169_GM;
MethodInfo m16169_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2959_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2959_m16169_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16169_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2959_m16170_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16170_GM;
MethodInfo m16170_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2959_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2959_m16170_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16170_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2959_m16171_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16171_GM;
MethodInfo m16171_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2959_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2959_m16171_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16171_GM};
static MethodInfo* t2959_MIs[] =
{
	&m16168_MI,
	&m16169_MI,
	&m16170_MI,
	&m16171_MI,
	NULL
};
static MethodInfo* t2959_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16170_MI,
	&m16171_MI,
};
extern TypeInfo t2960_TI;
extern TypeInfo t4_TI;
static Il2CppRGCTXData t2959_RGCTXData[5] = 
{
	&t2960_0_0_0/* Type Usage */,
	&t2960_TI/* Class Usage */,
	&m22448_MI/* Method Usage */,
	&t4_TI/* Class Usage */,
	&m16173_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2959_0_0_0;
extern Il2CppType t2959_1_0_0;
extern TypeInfo t556_TI;
struct t2959;
extern Il2CppGenericClass t2959_GC;
TypeInfo t2959_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2959_MIs, NULL, t2959_FIs, NULL, &t556_TI, NULL, NULL, &t2959_TI, NULL, t2959_VT, &EmptyCustomAttributesCache, &t2959_TI, &t2959_0_0_0, &t2959_1_0_0, NULL, &t2959_GC, NULL, NULL, NULL, t2959_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2959), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2960_m16172_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16172_GM;
MethodInfo m16172_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2960_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2960_m16172_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16172_GM};
extern Il2CppType t4_0_0_0;
static ParameterInfo t2960_m16173_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t4_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16173_GM;
MethodInfo m16173_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2960_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2960_m16173_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16173_GM};
extern Il2CppType t4_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2960_m16174_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t4_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16174_GM;
MethodInfo m16174_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2960_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2960_m16174_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16174_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2960_m16175_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16175_GM;
MethodInfo m16175_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2960_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2960_m16175_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16175_GM};
static MethodInfo* t2960_MIs[] =
{
	&m16172_MI,
	&m16173_MI,
	&m16174_MI,
	&m16175_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m16174_MI;
extern MethodInfo m16175_MI;
static MethodInfo* t2960_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16173_MI,
	&m16174_MI,
	&m16175_MI,
};
extern TypeInfo t373_TI;
static Il2CppInterfaceOffsetPair t2960_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2960_1_0_0;
extern TypeInfo t195_TI;
struct t2960;
extern Il2CppGenericClass t2960_GC;
TypeInfo t2960_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2960_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2960_TI, NULL, t2960_VT, &EmptyCustomAttributesCache, &t2960_TI, &t2960_0_0_0, &t2960_1_0_0, t2960_IOs, &t2960_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2960), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4404_TI;

#include "t319.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TouchPhase>
extern MethodInfo m29559_MI;
static PropertyInfo t4404____Current_PropertyInfo = 
{
	&t4404_TI, "Current", &m29559_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4404_PIs[] =
{
	&t4404____Current_PropertyInfo,
	NULL
};
extern Il2CppType t319_0_0_0;
extern void* RuntimeInvoker_t319 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29559_GM;
MethodInfo m29559_MI = 
{
	"get_Current", NULL, &t4404_TI, &t319_0_0_0, RuntimeInvoker_t319, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29559_GM};
static MethodInfo* t4404_MIs[] =
{
	&m29559_MI,
	NULL
};
static TypeInfo* t4404_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4404_0_0_0;
extern Il2CppType t4404_1_0_0;
struct t4404;
extern Il2CppGenericClass t4404_GC;
TypeInfo t4404_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4404_MIs, t4404_PIs, NULL, NULL, NULL, NULL, NULL, &t4404_TI, t4404_ITIs, NULL, &EmptyCustomAttributesCache, &t4404_TI, &t4404_0_0_0, &t4404_1_0_0, NULL, &t4404_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2961.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2961_TI;
#include "t2961MD.h"

extern TypeInfo t319_TI;
extern MethodInfo m16180_MI;
extern MethodInfo m22450_MI;
struct t20;
 int32_t m22450 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16176_MI;
 void m16176 (t2961 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16177_MI;
 t29 * m16177 (t2961 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16180(__this, &m16180_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t319_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16178_MI;
 void m16178 (t2961 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16179_MI;
 bool m16179 (t2961 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16180 (t2961 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22450(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22450_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>
extern Il2CppType t20_0_0_1;
FieldInfo t2961_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2961_TI, offsetof(t2961, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2961_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2961_TI, offsetof(t2961, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2961_FIs[] =
{
	&t2961_f0_FieldInfo,
	&t2961_f1_FieldInfo,
	NULL
};
static PropertyInfo t2961____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2961_TI, "System.Collections.IEnumerator.Current", &m16177_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2961____Current_PropertyInfo = 
{
	&t2961_TI, "Current", &m16180_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2961_PIs[] =
{
	&t2961____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2961____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2961_m16176_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16176_GM;
MethodInfo m16176_MI = 
{
	".ctor", (methodPointerType)&m16176, &t2961_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2961_m16176_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16176_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16177_GM;
MethodInfo m16177_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16177, &t2961_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16177_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16178_GM;
MethodInfo m16178_MI = 
{
	"Dispose", (methodPointerType)&m16178, &t2961_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16178_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16179_GM;
MethodInfo m16179_MI = 
{
	"MoveNext", (methodPointerType)&m16179, &t2961_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16179_GM};
extern Il2CppType t319_0_0_0;
extern void* RuntimeInvoker_t319 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16180_GM;
MethodInfo m16180_MI = 
{
	"get_Current", (methodPointerType)&m16180, &t2961_TI, &t319_0_0_0, RuntimeInvoker_t319, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16180_GM};
static MethodInfo* t2961_MIs[] =
{
	&m16176_MI,
	&m16177_MI,
	&m16178_MI,
	&m16179_MI,
	&m16180_MI,
	NULL
};
static MethodInfo* t2961_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16177_MI,
	&m16179_MI,
	&m16178_MI,
	&m16180_MI,
};
static TypeInfo* t2961_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4404_TI,
};
static Il2CppInterfaceOffsetPair t2961_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4404_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2961_0_0_0;
extern Il2CppType t2961_1_0_0;
extern Il2CppGenericClass t2961_GC;
TypeInfo t2961_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2961_MIs, t2961_PIs, t2961_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2961_TI, t2961_ITIs, t2961_VT, &EmptyCustomAttributesCache, &t2961_TI, &t2961_0_0_0, &t2961_1_0_0, t2961_IOs, &t2961_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2961)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5652_TI;

#include "UnityEngine_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>
extern MethodInfo m29560_MI;
static PropertyInfo t5652____Count_PropertyInfo = 
{
	&t5652_TI, "Count", &m29560_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29561_MI;
static PropertyInfo t5652____IsReadOnly_PropertyInfo = 
{
	&t5652_TI, "IsReadOnly", &m29561_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5652_PIs[] =
{
	&t5652____Count_PropertyInfo,
	&t5652____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29560_GM;
MethodInfo m29560_MI = 
{
	"get_Count", NULL, &t5652_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29560_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29561_GM;
MethodInfo m29561_MI = 
{
	"get_IsReadOnly", NULL, &t5652_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29561_GM};
extern Il2CppType t319_0_0_0;
extern Il2CppType t319_0_0_0;
static ParameterInfo t5652_m29562_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t319_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29562_GM;
MethodInfo m29562_MI = 
{
	"Add", NULL, &t5652_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5652_m29562_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29562_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29563_GM;
MethodInfo m29563_MI = 
{
	"Clear", NULL, &t5652_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29563_GM};
extern Il2CppType t319_0_0_0;
static ParameterInfo t5652_m29564_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t319_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29564_GM;
MethodInfo m29564_MI = 
{
	"Contains", NULL, &t5652_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5652_m29564_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29564_GM};
extern Il2CppType t3760_0_0_0;
extern Il2CppType t3760_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5652_m29565_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3760_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29565_GM;
MethodInfo m29565_MI = 
{
	"CopyTo", NULL, &t5652_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5652_m29565_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29565_GM};
extern Il2CppType t319_0_0_0;
static ParameterInfo t5652_m29566_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t319_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29566_GM;
MethodInfo m29566_MI = 
{
	"Remove", NULL, &t5652_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5652_m29566_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29566_GM};
static MethodInfo* t5652_MIs[] =
{
	&m29560_MI,
	&m29561_MI,
	&m29562_MI,
	&m29563_MI,
	&m29564_MI,
	&m29565_MI,
	&m29566_MI,
	NULL
};
extern TypeInfo t5654_TI;
static TypeInfo* t5652_ITIs[] = 
{
	&t603_TI,
	&t5654_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5652_0_0_0;
extern Il2CppType t5652_1_0_0;
struct t5652;
extern Il2CppGenericClass t5652_GC;
TypeInfo t5652_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5652_MIs, t5652_PIs, NULL, NULL, NULL, NULL, NULL, &t5652_TI, t5652_ITIs, NULL, &EmptyCustomAttributesCache, &t5652_TI, &t5652_0_0_0, &t5652_1_0_0, NULL, &t5652_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TouchPhase>
extern Il2CppType t4404_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29567_GM;
MethodInfo m29567_MI = 
{
	"GetEnumerator", NULL, &t5654_TI, &t4404_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29567_GM};
static MethodInfo* t5654_MIs[] =
{
	&m29567_MI,
	NULL
};
static TypeInfo* t5654_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5654_0_0_0;
extern Il2CppType t5654_1_0_0;
struct t5654;
extern Il2CppGenericClass t5654_GC;
TypeInfo t5654_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5654_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5654_TI, t5654_ITIs, NULL, &EmptyCustomAttributesCache, &t5654_TI, &t5654_0_0_0, &t5654_1_0_0, NULL, &t5654_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5653_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TouchPhase>
extern MethodInfo m29568_MI;
extern MethodInfo m29569_MI;
static PropertyInfo t5653____Item_PropertyInfo = 
{
	&t5653_TI, "Item", &m29568_MI, &m29569_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5653_PIs[] =
{
	&t5653____Item_PropertyInfo,
	NULL
};
extern Il2CppType t319_0_0_0;
static ParameterInfo t5653_m29570_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t319_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29570_GM;
MethodInfo m29570_MI = 
{
	"IndexOf", NULL, &t5653_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5653_m29570_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29570_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t319_0_0_0;
static ParameterInfo t5653_m29571_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t319_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29571_GM;
MethodInfo m29571_MI = 
{
	"Insert", NULL, &t5653_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5653_m29571_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29571_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5653_m29572_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29572_GM;
MethodInfo m29572_MI = 
{
	"RemoveAt", NULL, &t5653_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5653_m29572_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29572_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5653_m29568_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t319_0_0_0;
extern void* RuntimeInvoker_t319_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29568_GM;
MethodInfo m29568_MI = 
{
	"get_Item", NULL, &t5653_TI, &t319_0_0_0, RuntimeInvoker_t319_t44, t5653_m29568_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29568_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t319_0_0_0;
static ParameterInfo t5653_m29569_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t319_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29569_GM;
MethodInfo m29569_MI = 
{
	"set_Item", NULL, &t5653_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5653_m29569_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29569_GM};
static MethodInfo* t5653_MIs[] =
{
	&m29570_MI,
	&m29571_MI,
	&m29572_MI,
	&m29568_MI,
	&m29569_MI,
	NULL
};
static TypeInfo* t5653_ITIs[] = 
{
	&t603_TI,
	&t5652_TI,
	&t5654_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5653_0_0_0;
extern Il2CppType t5653_1_0_0;
struct t5653;
extern Il2CppGenericClass t5653_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5653_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5653_MIs, t5653_PIs, NULL, NULL, NULL, NULL, NULL, &t5653_TI, t5653_ITIs, NULL, &t1908__CustomAttributeCache, &t5653_TI, &t5653_0_0_0, &t5653_1_0_0, NULL, &t5653_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4406_TI;

#include "t386.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.IMECompositionMode>
extern MethodInfo m29573_MI;
static PropertyInfo t4406____Current_PropertyInfo = 
{
	&t4406_TI, "Current", &m29573_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4406_PIs[] =
{
	&t4406____Current_PropertyInfo,
	NULL
};
extern Il2CppType t386_0_0_0;
extern void* RuntimeInvoker_t386 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29573_GM;
MethodInfo m29573_MI = 
{
	"get_Current", NULL, &t4406_TI, &t386_0_0_0, RuntimeInvoker_t386, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29573_GM};
static MethodInfo* t4406_MIs[] =
{
	&m29573_MI,
	NULL
};
static TypeInfo* t4406_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4406_0_0_0;
extern Il2CppType t4406_1_0_0;
struct t4406;
extern Il2CppGenericClass t4406_GC;
TypeInfo t4406_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4406_MIs, t4406_PIs, NULL, NULL, NULL, NULL, NULL, &t4406_TI, t4406_ITIs, NULL, &EmptyCustomAttributesCache, &t4406_TI, &t4406_0_0_0, &t4406_1_0_0, NULL, &t4406_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2962.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2962_TI;
#include "t2962MD.h"

extern TypeInfo t386_TI;
extern MethodInfo m16185_MI;
extern MethodInfo m22461_MI;
struct t20;
 int32_t m22461 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16181_MI;
 void m16181 (t2962 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16182_MI;
 t29 * m16182 (t2962 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16185(__this, &m16185_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t386_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16183_MI;
 void m16183 (t2962 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16184_MI;
 bool m16184 (t2962 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16185 (t2962 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22461(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22461_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>
extern Il2CppType t20_0_0_1;
FieldInfo t2962_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2962_TI, offsetof(t2962, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2962_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2962_TI, offsetof(t2962, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2962_FIs[] =
{
	&t2962_f0_FieldInfo,
	&t2962_f1_FieldInfo,
	NULL
};
static PropertyInfo t2962____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2962_TI, "System.Collections.IEnumerator.Current", &m16182_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2962____Current_PropertyInfo = 
{
	&t2962_TI, "Current", &m16185_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2962_PIs[] =
{
	&t2962____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2962____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2962_m16181_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16181_GM;
MethodInfo m16181_MI = 
{
	".ctor", (methodPointerType)&m16181, &t2962_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2962_m16181_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16181_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16182_GM;
MethodInfo m16182_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16182, &t2962_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16182_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16183_GM;
MethodInfo m16183_MI = 
{
	"Dispose", (methodPointerType)&m16183, &t2962_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16183_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16184_GM;
MethodInfo m16184_MI = 
{
	"MoveNext", (methodPointerType)&m16184, &t2962_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16184_GM};
extern Il2CppType t386_0_0_0;
extern void* RuntimeInvoker_t386 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16185_GM;
MethodInfo m16185_MI = 
{
	"get_Current", (methodPointerType)&m16185, &t2962_TI, &t386_0_0_0, RuntimeInvoker_t386, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16185_GM};
static MethodInfo* t2962_MIs[] =
{
	&m16181_MI,
	&m16182_MI,
	&m16183_MI,
	&m16184_MI,
	&m16185_MI,
	NULL
};
static MethodInfo* t2962_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16182_MI,
	&m16184_MI,
	&m16183_MI,
	&m16185_MI,
};
static TypeInfo* t2962_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4406_TI,
};
static Il2CppInterfaceOffsetPair t2962_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4406_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2962_0_0_0;
extern Il2CppType t2962_1_0_0;
extern Il2CppGenericClass t2962_GC;
TypeInfo t2962_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2962_MIs, t2962_PIs, t2962_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2962_TI, t2962_ITIs, t2962_VT, &EmptyCustomAttributesCache, &t2962_TI, &t2962_0_0_0, &t2962_1_0_0, t2962_IOs, &t2962_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2962)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5655_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>
extern MethodInfo m29574_MI;
static PropertyInfo t5655____Count_PropertyInfo = 
{
	&t5655_TI, "Count", &m29574_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29575_MI;
static PropertyInfo t5655____IsReadOnly_PropertyInfo = 
{
	&t5655_TI, "IsReadOnly", &m29575_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5655_PIs[] =
{
	&t5655____Count_PropertyInfo,
	&t5655____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29574_GM;
MethodInfo m29574_MI = 
{
	"get_Count", NULL, &t5655_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29574_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29575_GM;
MethodInfo m29575_MI = 
{
	"get_IsReadOnly", NULL, &t5655_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29575_GM};
extern Il2CppType t386_0_0_0;
extern Il2CppType t386_0_0_0;
static ParameterInfo t5655_m29576_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t386_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29576_GM;
MethodInfo m29576_MI = 
{
	"Add", NULL, &t5655_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5655_m29576_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29576_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29577_GM;
MethodInfo m29577_MI = 
{
	"Clear", NULL, &t5655_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29577_GM};
extern Il2CppType t386_0_0_0;
static ParameterInfo t5655_m29578_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t386_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29578_GM;
MethodInfo m29578_MI = 
{
	"Contains", NULL, &t5655_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5655_m29578_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29578_GM};
extern Il2CppType t3761_0_0_0;
extern Il2CppType t3761_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5655_m29579_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3761_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29579_GM;
MethodInfo m29579_MI = 
{
	"CopyTo", NULL, &t5655_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5655_m29579_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29579_GM};
extern Il2CppType t386_0_0_0;
static ParameterInfo t5655_m29580_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t386_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29580_GM;
MethodInfo m29580_MI = 
{
	"Remove", NULL, &t5655_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5655_m29580_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29580_GM};
static MethodInfo* t5655_MIs[] =
{
	&m29574_MI,
	&m29575_MI,
	&m29576_MI,
	&m29577_MI,
	&m29578_MI,
	&m29579_MI,
	&m29580_MI,
	NULL
};
extern TypeInfo t5657_TI;
static TypeInfo* t5655_ITIs[] = 
{
	&t603_TI,
	&t5657_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5655_0_0_0;
extern Il2CppType t5655_1_0_0;
struct t5655;
extern Il2CppGenericClass t5655_GC;
TypeInfo t5655_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5655_MIs, t5655_PIs, NULL, NULL, NULL, NULL, NULL, &t5655_TI, t5655_ITIs, NULL, &EmptyCustomAttributesCache, &t5655_TI, &t5655_0_0_0, &t5655_1_0_0, NULL, &t5655_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.IMECompositionMode>
extern Il2CppType t4406_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29581_GM;
MethodInfo m29581_MI = 
{
	"GetEnumerator", NULL, &t5657_TI, &t4406_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29581_GM};
static MethodInfo* t5657_MIs[] =
{
	&m29581_MI,
	NULL
};
static TypeInfo* t5657_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5657_0_0_0;
extern Il2CppType t5657_1_0_0;
struct t5657;
extern Il2CppGenericClass t5657_GC;
TypeInfo t5657_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5657_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5657_TI, t5657_ITIs, NULL, &EmptyCustomAttributesCache, &t5657_TI, &t5657_0_0_0, &t5657_1_0_0, NULL, &t5657_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5656_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>
extern MethodInfo m29582_MI;
extern MethodInfo m29583_MI;
static PropertyInfo t5656____Item_PropertyInfo = 
{
	&t5656_TI, "Item", &m29582_MI, &m29583_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5656_PIs[] =
{
	&t5656____Item_PropertyInfo,
	NULL
};
extern Il2CppType t386_0_0_0;
static ParameterInfo t5656_m29584_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t386_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29584_GM;
MethodInfo m29584_MI = 
{
	"IndexOf", NULL, &t5656_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5656_m29584_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29584_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t386_0_0_0;
static ParameterInfo t5656_m29585_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t386_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29585_GM;
MethodInfo m29585_MI = 
{
	"Insert", NULL, &t5656_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5656_m29585_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29585_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5656_m29586_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29586_GM;
MethodInfo m29586_MI = 
{
	"RemoveAt", NULL, &t5656_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5656_m29586_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29586_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5656_m29582_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t386_0_0_0;
extern void* RuntimeInvoker_t386_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29582_GM;
MethodInfo m29582_MI = 
{
	"get_Item", NULL, &t5656_TI, &t386_0_0_0, RuntimeInvoker_t386_t44, t5656_m29582_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29582_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t386_0_0_0;
static ParameterInfo t5656_m29583_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t386_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29583_GM;
MethodInfo m29583_MI = 
{
	"set_Item", NULL, &t5656_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5656_m29583_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29583_GM};
static MethodInfo* t5656_MIs[] =
{
	&m29584_MI,
	&m29585_MI,
	&m29586_MI,
	&m29582_MI,
	&m29583_MI,
	NULL
};
static TypeInfo* t5656_ITIs[] = 
{
	&t603_TI,
	&t5655_TI,
	&t5657_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5656_0_0_0;
extern Il2CppType t5656_1_0_0;
struct t5656;
extern Il2CppGenericClass t5656_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5656_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5656_MIs, t5656_PIs, NULL, NULL, NULL, NULL, NULL, &t5656_TI, t5656_ITIs, NULL, &t1908__CustomAttributeCache, &t5656_TI, &t5656_0_0_0, &t5656_1_0_0, NULL, &t5656_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4408_TI;

#include "t385.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.HideFlags>
extern MethodInfo m29587_MI;
static PropertyInfo t4408____Current_PropertyInfo = 
{
	&t4408_TI, "Current", &m29587_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4408_PIs[] =
{
	&t4408____Current_PropertyInfo,
	NULL
};
extern Il2CppType t385_0_0_0;
extern void* RuntimeInvoker_t385 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29587_GM;
MethodInfo m29587_MI = 
{
	"get_Current", NULL, &t4408_TI, &t385_0_0_0, RuntimeInvoker_t385, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29587_GM};
static MethodInfo* t4408_MIs[] =
{
	&m29587_MI,
	NULL
};
static TypeInfo* t4408_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4408_0_0_0;
extern Il2CppType t4408_1_0_0;
struct t4408;
extern Il2CppGenericClass t4408_GC;
TypeInfo t4408_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4408_MIs, t4408_PIs, NULL, NULL, NULL, NULL, NULL, &t4408_TI, t4408_ITIs, NULL, &EmptyCustomAttributesCache, &t4408_TI, &t4408_0_0_0, &t4408_1_0_0, NULL, &t4408_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2963.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2963_TI;
#include "t2963MD.h"

extern TypeInfo t385_TI;
extern MethodInfo m16190_MI;
extern MethodInfo m22472_MI;
struct t20;
 int32_t m22472 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16186_MI;
 void m16186 (t2963 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16187_MI;
 t29 * m16187 (t2963 * __this, MethodInfo* method){
	{
		int32_t L_0 = m16190(__this, &m16190_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t385_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16188_MI;
 void m16188 (t2963 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16189_MI;
 bool m16189 (t2963 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m16190 (t2963 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m22472(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22472_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.HideFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t2963_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2963_TI, offsetof(t2963, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2963_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2963_TI, offsetof(t2963, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2963_FIs[] =
{
	&t2963_f0_FieldInfo,
	&t2963_f1_FieldInfo,
	NULL
};
static PropertyInfo t2963____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2963_TI, "System.Collections.IEnumerator.Current", &m16187_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2963____Current_PropertyInfo = 
{
	&t2963_TI, "Current", &m16190_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2963_PIs[] =
{
	&t2963____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2963____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2963_m16186_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16186_GM;
MethodInfo m16186_MI = 
{
	".ctor", (methodPointerType)&m16186, &t2963_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2963_m16186_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16186_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16187_GM;
MethodInfo m16187_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16187, &t2963_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16187_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16188_GM;
MethodInfo m16188_MI = 
{
	"Dispose", (methodPointerType)&m16188, &t2963_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16188_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16189_GM;
MethodInfo m16189_MI = 
{
	"MoveNext", (methodPointerType)&m16189, &t2963_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16189_GM};
extern Il2CppType t385_0_0_0;
extern void* RuntimeInvoker_t385 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16190_GM;
MethodInfo m16190_MI = 
{
	"get_Current", (methodPointerType)&m16190, &t2963_TI, &t385_0_0_0, RuntimeInvoker_t385, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16190_GM};
static MethodInfo* t2963_MIs[] =
{
	&m16186_MI,
	&m16187_MI,
	&m16188_MI,
	&m16189_MI,
	&m16190_MI,
	NULL
};
static MethodInfo* t2963_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16187_MI,
	&m16189_MI,
	&m16188_MI,
	&m16190_MI,
};
static TypeInfo* t2963_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4408_TI,
};
static Il2CppInterfaceOffsetPair t2963_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4408_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2963_0_0_0;
extern Il2CppType t2963_1_0_0;
extern Il2CppGenericClass t2963_GC;
TypeInfo t2963_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2963_MIs, t2963_PIs, t2963_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2963_TI, t2963_ITIs, t2963_VT, &EmptyCustomAttributesCache, &t2963_TI, &t2963_0_0_0, &t2963_1_0_0, t2963_IOs, &t2963_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2963)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5658_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>
extern MethodInfo m29588_MI;
static PropertyInfo t5658____Count_PropertyInfo = 
{
	&t5658_TI, "Count", &m29588_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29589_MI;
static PropertyInfo t5658____IsReadOnly_PropertyInfo = 
{
	&t5658_TI, "IsReadOnly", &m29589_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5658_PIs[] =
{
	&t5658____Count_PropertyInfo,
	&t5658____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29588_GM;
MethodInfo m29588_MI = 
{
	"get_Count", NULL, &t5658_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29588_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29589_GM;
MethodInfo m29589_MI = 
{
	"get_IsReadOnly", NULL, &t5658_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29589_GM};
extern Il2CppType t385_0_0_0;
extern Il2CppType t385_0_0_0;
static ParameterInfo t5658_m29590_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t385_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29590_GM;
MethodInfo m29590_MI = 
{
	"Add", NULL, &t5658_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5658_m29590_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29590_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29591_GM;
MethodInfo m29591_MI = 
{
	"Clear", NULL, &t5658_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29591_GM};
extern Il2CppType t385_0_0_0;
static ParameterInfo t5658_m29592_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t385_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29592_GM;
MethodInfo m29592_MI = 
{
	"Contains", NULL, &t5658_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5658_m29592_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29592_GM};
extern Il2CppType t3762_0_0_0;
extern Il2CppType t3762_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5658_m29593_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3762_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29593_GM;
MethodInfo m29593_MI = 
{
	"CopyTo", NULL, &t5658_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5658_m29593_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29593_GM};
extern Il2CppType t385_0_0_0;
static ParameterInfo t5658_m29594_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t385_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29594_GM;
MethodInfo m29594_MI = 
{
	"Remove", NULL, &t5658_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5658_m29594_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29594_GM};
static MethodInfo* t5658_MIs[] =
{
	&m29588_MI,
	&m29589_MI,
	&m29590_MI,
	&m29591_MI,
	&m29592_MI,
	&m29593_MI,
	&m29594_MI,
	NULL
};
extern TypeInfo t5660_TI;
static TypeInfo* t5658_ITIs[] = 
{
	&t603_TI,
	&t5660_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5658_0_0_0;
extern Il2CppType t5658_1_0_0;
struct t5658;
extern Il2CppGenericClass t5658_GC;
TypeInfo t5658_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5658_MIs, t5658_PIs, NULL, NULL, NULL, NULL, NULL, &t5658_TI, t5658_ITIs, NULL, &EmptyCustomAttributesCache, &t5658_TI, &t5658_0_0_0, &t5658_1_0_0, NULL, &t5658_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.HideFlags>
extern Il2CppType t4408_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29595_GM;
MethodInfo m29595_MI = 
{
	"GetEnumerator", NULL, &t5660_TI, &t4408_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29595_GM};
static MethodInfo* t5660_MIs[] =
{
	&m29595_MI,
	NULL
};
static TypeInfo* t5660_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5660_0_0_0;
extern Il2CppType t5660_1_0_0;
struct t5660;
extern Il2CppGenericClass t5660_GC;
TypeInfo t5660_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5660_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5660_TI, t5660_ITIs, NULL, &EmptyCustomAttributesCache, &t5660_TI, &t5660_0_0_0, &t5660_1_0_0, NULL, &t5660_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5659_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.HideFlags>
extern MethodInfo m29596_MI;
extern MethodInfo m29597_MI;
static PropertyInfo t5659____Item_PropertyInfo = 
{
	&t5659_TI, "Item", &m29596_MI, &m29597_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5659_PIs[] =
{
	&t5659____Item_PropertyInfo,
	NULL
};
extern Il2CppType t385_0_0_0;
static ParameterInfo t5659_m29598_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t385_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29598_GM;
MethodInfo m29598_MI = 
{
	"IndexOf", NULL, &t5659_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5659_m29598_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29598_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t385_0_0_0;
static ParameterInfo t5659_m29599_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t385_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29599_GM;
MethodInfo m29599_MI = 
{
	"Insert", NULL, &t5659_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5659_m29599_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29599_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5659_m29600_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29600_GM;
MethodInfo m29600_MI = 
{
	"RemoveAt", NULL, &t5659_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5659_m29600_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29600_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5659_m29596_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t385_0_0_0;
extern void* RuntimeInvoker_t385_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29596_GM;
MethodInfo m29596_MI = 
{
	"get_Item", NULL, &t5659_TI, &t385_0_0_0, RuntimeInvoker_t385_t44, t5659_m29596_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29596_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t385_0_0_0;
static ParameterInfo t5659_m29597_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t385_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29597_GM;
MethodInfo m29597_MI = 
{
	"set_Item", NULL, &t5659_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5659_m29597_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29597_GM};
static MethodInfo* t5659_MIs[] =
{
	&m29598_MI,
	&m29599_MI,
	&m29600_MI,
	&m29596_MI,
	&m29597_MI,
	NULL
};
static TypeInfo* t5659_ITIs[] = 
{
	&t603_TI,
	&t5658_TI,
	&t5660_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5659_0_0_0;
extern Il2CppType t5659_1_0_0;
struct t5659;
extern Il2CppGenericClass t5659_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5659_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5659_MIs, t5659_PIs, NULL, NULL, NULL, NULL, NULL, &t5659_TI, t5659_ITIs, NULL, &t1908__CustomAttributeCache, &t5659_TI, &t5659_0_0_0, &t5659_1_0_0, NULL, &t5659_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2964.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2964_TI;
#include "t2964MD.h"

#include "t2965.h"
extern TypeInfo t41_TI;
extern TypeInfo t2965_TI;
#include "t2965MD.h"
extern MethodInfo m16193_MI;
extern MethodInfo m16195_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Object>
extern Il2CppType t316_0_0_33;
FieldInfo t2964_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2964_TI, offsetof(t2964, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2964_FIs[] =
{
	&t2964_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t41_0_0_0;
static ParameterInfo t2964_m16191_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16191_GM;
MethodInfo m16191_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2964_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2964_m16191_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16191_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2964_m16192_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16192_GM;
MethodInfo m16192_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2964_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2964_m16192_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16192_GM};
static MethodInfo* t2964_MIs[] =
{
	&m16191_MI,
	&m16192_MI,
	NULL
};
extern MethodInfo m16192_MI;
extern MethodInfo m16196_MI;
static MethodInfo* t2964_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16192_MI,
	&m16196_MI,
};
extern Il2CppType t2966_0_0_0;
extern TypeInfo t2966_TI;
extern MethodInfo m22482_MI;
extern TypeInfo t41_TI;
extern MethodInfo m16198_MI;
extern TypeInfo t41_TI;
static Il2CppRGCTXData t2964_RGCTXData[8] = 
{
	&t2966_0_0_0/* Type Usage */,
	&t2966_TI/* Class Usage */,
	&m22482_MI/* Method Usage */,
	&t41_TI/* Class Usage */,
	&m16198_MI/* Method Usage */,
	&m16193_MI/* Method Usage */,
	&t41_TI/* Class Usage */,
	&m16195_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2964_0_0_0;
extern Il2CppType t2964_1_0_0;
struct t2964;
extern Il2CppGenericClass t2964_GC;
TypeInfo t2964_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2964_MIs, NULL, t2964_FIs, NULL, &t2965_TI, NULL, NULL, &t2964_TI, NULL, t2964_VT, &EmptyCustomAttributesCache, &t2964_TI, &t2964_0_0_0, &t2964_1_0_0, NULL, &t2964_GC, NULL, NULL, NULL, t2964_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2964), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2966.h"
extern TypeInfo t2966_TI;
#include "t2966MD.h"
struct t556;
#define m22482(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Object>
extern Il2CppType t2966_0_0_1;
FieldInfo t2965_f0_FieldInfo = 
{
	"Delegate", &t2966_0_0_1, &t2965_TI, offsetof(t2965, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2965_FIs[] =
{
	&t2965_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2965_m16193_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16193_GM;
MethodInfo m16193_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2965_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2965_m16193_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16193_GM};
extern Il2CppType t2966_0_0_0;
static ParameterInfo t2965_m16194_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2966_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16194_GM;
MethodInfo m16194_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2965_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2965_m16194_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16194_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2965_m16195_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16195_GM;
MethodInfo m16195_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2965_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2965_m16195_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16195_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2965_m16196_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16196_GM;
MethodInfo m16196_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2965_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2965_m16196_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16196_GM};
static MethodInfo* t2965_MIs[] =
{
	&m16193_MI,
	&m16194_MI,
	&m16195_MI,
	&m16196_MI,
	NULL
};
static MethodInfo* t2965_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16195_MI,
	&m16196_MI,
};
extern TypeInfo t2966_TI;
extern TypeInfo t41_TI;
static Il2CppRGCTXData t2965_RGCTXData[5] = 
{
	&t2966_0_0_0/* Type Usage */,
	&t2966_TI/* Class Usage */,
	&m22482_MI/* Method Usage */,
	&t41_TI/* Class Usage */,
	&m16198_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2965_0_0_0;
extern Il2CppType t2965_1_0_0;
struct t2965;
extern Il2CppGenericClass t2965_GC;
TypeInfo t2965_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2965_MIs, NULL, t2965_FIs, NULL, &t556_TI, NULL, NULL, &t2965_TI, NULL, t2965_VT, &EmptyCustomAttributesCache, &t2965_TI, &t2965_0_0_0, &t2965_1_0_0, NULL, &t2965_GC, NULL, NULL, NULL, t2965_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2965), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2966_m16197_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16197_GM;
MethodInfo m16197_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2966_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2966_m16197_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16197_GM};
extern Il2CppType t41_0_0_0;
static ParameterInfo t2966_m16198_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16198_GM;
MethodInfo m16198_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2966_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2966_m16198_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16198_GM};
extern Il2CppType t41_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2966_m16199_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16199_GM;
MethodInfo m16199_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2966_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2966_m16199_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16199_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2966_m16200_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16200_GM;
MethodInfo m16200_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2966_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2966_m16200_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16200_GM};
static MethodInfo* t2966_MIs[] =
{
	&m16197_MI,
	&m16198_MI,
	&m16199_MI,
	&m16200_MI,
	NULL
};
extern MethodInfo m16199_MI;
extern MethodInfo m16200_MI;
static MethodInfo* t2966_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16198_MI,
	&m16199_MI,
	&m16200_MI,
};
static Il2CppInterfaceOffsetPair t2966_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2966_1_0_0;
struct t2966;
extern Il2CppGenericClass t2966_GC;
TypeInfo t2966_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2966_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2966_TI, NULL, t2966_VT, &EmptyCustomAttributesCache, &t2966_TI, &t2966_0_0_0, &t2966_1_0_0, t2966_IOs, &t2966_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2966), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2967.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2967_TI;
#include "t2967MD.h"

#include "t28.h"
#include "t2968.h"
extern TypeInfo t28_TI;
extern TypeInfo t2968_TI;
#include "t2968MD.h"
extern MethodInfo m16203_MI;
extern MethodInfo m16205_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Component>
extern Il2CppType t316_0_0_33;
FieldInfo t2967_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2967_TI, offsetof(t2967, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2967_FIs[] =
{
	&t2967_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t28_0_0_0;
extern Il2CppType t28_0_0_0;
static ParameterInfo t2967_m16201_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t28_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16201_GM;
MethodInfo m16201_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2967_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2967_m16201_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16201_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2967_m16202_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16202_GM;
MethodInfo m16202_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2967_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2967_m16202_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16202_GM};
static MethodInfo* t2967_MIs[] =
{
	&m16201_MI,
	&m16202_MI,
	NULL
};
extern MethodInfo m16202_MI;
extern MethodInfo m16206_MI;
static MethodInfo* t2967_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16202_MI,
	&m16206_MI,
};
extern Il2CppType t266_0_0_0;
extern TypeInfo t266_TI;
extern MethodInfo m22483_MI;
extern TypeInfo t28_TI;
extern MethodInfo m2002_MI;
extern TypeInfo t28_TI;
static Il2CppRGCTXData t2967_RGCTXData[8] = 
{
	&t266_0_0_0/* Type Usage */,
	&t266_TI/* Class Usage */,
	&m22483_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
	&m2002_MI/* Method Usage */,
	&m16203_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
	&m16205_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2967_0_0_0;
extern Il2CppType t2967_1_0_0;
struct t2967;
extern Il2CppGenericClass t2967_GC;
TypeInfo t2967_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2967_MIs, NULL, t2967_FIs, NULL, &t2968_TI, NULL, NULL, &t2967_TI, NULL, t2967_VT, &EmptyCustomAttributesCache, &t2967_TI, &t2967_0_0_0, &t2967_1_0_0, NULL, &t2967_GC, NULL, NULL, NULL, t2967_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2967), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t266.h"
extern TypeInfo t266_TI;
#include "t266MD.h"
struct t556;
#define m22483(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Component>
extern Il2CppType t266_0_0_1;
FieldInfo t2968_f0_FieldInfo = 
{
	"Delegate", &t266_0_0_1, &t2968_TI, offsetof(t2968, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2968_FIs[] =
{
	&t2968_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2968_m16203_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16203_GM;
MethodInfo m16203_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2968_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2968_m16203_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16203_GM};
extern Il2CppType t266_0_0_0;
static ParameterInfo t2968_m16204_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t266_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16204_GM;
MethodInfo m16204_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2968_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2968_m16204_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16204_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2968_m16205_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16205_GM;
MethodInfo m16205_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2968_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2968_m16205_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16205_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2968_m16206_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16206_GM;
MethodInfo m16206_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2968_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2968_m16206_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16206_GM};
static MethodInfo* t2968_MIs[] =
{
	&m16203_MI,
	&m16204_MI,
	&m16205_MI,
	&m16206_MI,
	NULL
};
static MethodInfo* t2968_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16205_MI,
	&m16206_MI,
};
extern TypeInfo t266_TI;
extern TypeInfo t28_TI;
static Il2CppRGCTXData t2968_RGCTXData[5] = 
{
	&t266_0_0_0/* Type Usage */,
	&t266_TI/* Class Usage */,
	&m22483_MI/* Method Usage */,
	&t28_TI/* Class Usage */,
	&m2002_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2968_0_0_0;
extern Il2CppType t2968_1_0_0;
struct t2968;
extern Il2CppGenericClass t2968_GC;
TypeInfo t2968_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2968_MIs, NULL, t2968_FIs, NULL, &t556_TI, NULL, NULL, &t2968_TI, NULL, t2968_VT, &EmptyCustomAttributesCache, &t2968_TI, &t2968_0_0_0, &t2968_1_0_0, NULL, &t2968_GC, NULL, NULL, NULL, t2968_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2968), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4410_TI;

#include "t16.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject>
extern MethodInfo m29601_MI;
static PropertyInfo t4410____Current_PropertyInfo = 
{
	&t4410_TI, "Current", &m29601_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4410_PIs[] =
{
	&t4410____Current_PropertyInfo,
	NULL
};
extern Il2CppType t16_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29601_GM;
MethodInfo m29601_MI = 
{
	"get_Current", NULL, &t4410_TI, &t16_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29601_GM};
static MethodInfo* t4410_MIs[] =
{
	&m29601_MI,
	NULL
};
static TypeInfo* t4410_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4410_0_0_0;
extern Il2CppType t4410_1_0_0;
struct t4410;
extern Il2CppGenericClass t4410_GC;
TypeInfo t4410_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4410_MIs, t4410_PIs, NULL, NULL, NULL, NULL, NULL, &t4410_TI, t4410_ITIs, NULL, &EmptyCustomAttributesCache, &t4410_TI, &t4410_0_0_0, &t4410_1_0_0, NULL, &t4410_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2969.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2969_TI;
#include "t2969MD.h"

extern TypeInfo t16_TI;
extern MethodInfo m16211_MI;
extern MethodInfo m22485_MI;
struct t20;
#define m22485(__this, p0, method) (t16 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GameObject>
extern Il2CppType t20_0_0_1;
FieldInfo t2969_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2969_TI, offsetof(t2969, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2969_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2969_TI, offsetof(t2969, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2969_FIs[] =
{
	&t2969_f0_FieldInfo,
	&t2969_f1_FieldInfo,
	NULL
};
extern MethodInfo m16208_MI;
static PropertyInfo t2969____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2969_TI, "System.Collections.IEnumerator.Current", &m16208_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2969____Current_PropertyInfo = 
{
	&t2969_TI, "Current", &m16211_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2969_PIs[] =
{
	&t2969____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2969____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2969_m16207_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16207_GM;
MethodInfo m16207_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2969_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2969_m16207_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16207_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16208_GM;
MethodInfo m16208_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2969_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16208_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16209_GM;
MethodInfo m16209_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2969_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16209_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16210_GM;
MethodInfo m16210_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2969_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16210_GM};
extern Il2CppType t16_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16211_GM;
MethodInfo m16211_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2969_TI, &t16_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16211_GM};
static MethodInfo* t2969_MIs[] =
{
	&m16207_MI,
	&m16208_MI,
	&m16209_MI,
	&m16210_MI,
	&m16211_MI,
	NULL
};
extern MethodInfo m16210_MI;
extern MethodInfo m16209_MI;
static MethodInfo* t2969_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16208_MI,
	&m16210_MI,
	&m16209_MI,
	&m16211_MI,
};
static TypeInfo* t2969_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4410_TI,
};
static Il2CppInterfaceOffsetPair t2969_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4410_TI, 7},
};
extern TypeInfo t16_TI;
static Il2CppRGCTXData t2969_RGCTXData[3] = 
{
	&m16211_MI/* Method Usage */,
	&t16_TI/* Class Usage */,
	&m22485_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2969_0_0_0;
extern Il2CppType t2969_1_0_0;
extern Il2CppGenericClass t2969_GC;
TypeInfo t2969_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2969_MIs, t2969_PIs, t2969_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2969_TI, t2969_ITIs, t2969_VT, &EmptyCustomAttributesCache, &t2969_TI, &t2969_0_0_0, &t2969_1_0_0, t2969_IOs, &t2969_GC, NULL, NULL, NULL, t2969_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2969)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5661_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GameObject>
extern MethodInfo m29602_MI;
static PropertyInfo t5661____Count_PropertyInfo = 
{
	&t5661_TI, "Count", &m29602_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29603_MI;
static PropertyInfo t5661____IsReadOnly_PropertyInfo = 
{
	&t5661_TI, "IsReadOnly", &m29603_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5661_PIs[] =
{
	&t5661____Count_PropertyInfo,
	&t5661____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29602_GM;
MethodInfo m29602_MI = 
{
	"get_Count", NULL, &t5661_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29602_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29603_GM;
MethodInfo m29603_MI = 
{
	"get_IsReadOnly", NULL, &t5661_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29603_GM};
extern Il2CppType t16_0_0_0;
extern Il2CppType t16_0_0_0;
static ParameterInfo t5661_m29604_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t16_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29604_GM;
MethodInfo m29604_MI = 
{
	"Add", NULL, &t5661_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5661_m29604_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29604_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29605_GM;
MethodInfo m29605_MI = 
{
	"Clear", NULL, &t5661_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29605_GM};
extern Il2CppType t16_0_0_0;
static ParameterInfo t5661_m29606_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t16_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29606_GM;
MethodInfo m29606_MI = 
{
	"Contains", NULL, &t5661_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5661_m29606_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29606_GM};
extern Il2CppType t3763_0_0_0;
extern Il2CppType t3763_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5661_m29607_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3763_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29607_GM;
MethodInfo m29607_MI = 
{
	"CopyTo", NULL, &t5661_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5661_m29607_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29607_GM};
extern Il2CppType t16_0_0_0;
static ParameterInfo t5661_m29608_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t16_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29608_GM;
MethodInfo m29608_MI = 
{
	"Remove", NULL, &t5661_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5661_m29608_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29608_GM};
static MethodInfo* t5661_MIs[] =
{
	&m29602_MI,
	&m29603_MI,
	&m29604_MI,
	&m29605_MI,
	&m29606_MI,
	&m29607_MI,
	&m29608_MI,
	NULL
};
extern TypeInfo t5663_TI;
static TypeInfo* t5661_ITIs[] = 
{
	&t603_TI,
	&t5663_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5661_0_0_0;
extern Il2CppType t5661_1_0_0;
struct t5661;
extern Il2CppGenericClass t5661_GC;
TypeInfo t5661_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5661_MIs, t5661_PIs, NULL, NULL, NULL, NULL, NULL, &t5661_TI, t5661_ITIs, NULL, &EmptyCustomAttributesCache, &t5661_TI, &t5661_0_0_0, &t5661_1_0_0, NULL, &t5661_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>
extern Il2CppType t4410_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29609_GM;
MethodInfo m29609_MI = 
{
	"GetEnumerator", NULL, &t5663_TI, &t4410_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29609_GM};
static MethodInfo* t5663_MIs[] =
{
	&m29609_MI,
	NULL
};
static TypeInfo* t5663_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5663_0_0_0;
extern Il2CppType t5663_1_0_0;
struct t5663;
extern Il2CppGenericClass t5663_GC;
TypeInfo t5663_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5663_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5663_TI, t5663_ITIs, NULL, &EmptyCustomAttributesCache, &t5663_TI, &t5663_0_0_0, &t5663_1_0_0, NULL, &t5663_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5662_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GameObject>
extern MethodInfo m29610_MI;
extern MethodInfo m29611_MI;
static PropertyInfo t5662____Item_PropertyInfo = 
{
	&t5662_TI, "Item", &m29610_MI, &m29611_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5662_PIs[] =
{
	&t5662____Item_PropertyInfo,
	NULL
};
extern Il2CppType t16_0_0_0;
static ParameterInfo t5662_m29612_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t16_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29612_GM;
MethodInfo m29612_MI = 
{
	"IndexOf", NULL, &t5662_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5662_m29612_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29612_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t16_0_0_0;
static ParameterInfo t5662_m29613_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t16_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29613_GM;
MethodInfo m29613_MI = 
{
	"Insert", NULL, &t5662_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5662_m29613_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29613_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5662_m29614_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29614_GM;
MethodInfo m29614_MI = 
{
	"RemoveAt", NULL, &t5662_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5662_m29614_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29614_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5662_m29610_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t16_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29610_GM;
MethodInfo m29610_MI = 
{
	"get_Item", NULL, &t5662_TI, &t16_0_0_0, RuntimeInvoker_t29_t44, t5662_m29610_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29610_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t16_0_0_0;
static ParameterInfo t5662_m29611_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t16_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29611_GM;
MethodInfo m29611_MI = 
{
	"set_Item", NULL, &t5662_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5662_m29611_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29611_GM};
static MethodInfo* t5662_MIs[] =
{
	&m29612_MI,
	&m29613_MI,
	&m29614_MI,
	&m29610_MI,
	&m29611_MI,
	NULL
};
static TypeInfo* t5662_ITIs[] = 
{
	&t603_TI,
	&t5661_TI,
	&t5663_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5662_0_0_0;
extern Il2CppType t5662_1_0_0;
struct t5662;
extern Il2CppGenericClass t5662_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5662_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5662_MIs, t5662_PIs, NULL, NULL, NULL, NULL, NULL, &t5662_TI, t5662_ITIs, NULL, &t1908__CustomAttributeCache, &t5662_TI, &t5662_0_0_0, &t5662_1_0_0, NULL, &t5662_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2970.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2970_TI;
#include "t2970MD.h"

#include "t2971.h"
extern TypeInfo t2971_TI;
#include "t2971MD.h"
extern MethodInfo m16214_MI;
extern MethodInfo m16216_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GameObject>
extern Il2CppType t316_0_0_33;
FieldInfo t2970_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2970_TI, offsetof(t2970, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2970_FIs[] =
{
	&t2970_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t16_0_0_0;
static ParameterInfo t2970_m16212_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t16_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16212_GM;
MethodInfo m16212_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2970_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2970_m16212_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16212_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2970_m16213_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16213_GM;
MethodInfo m16213_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2970_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2970_m16213_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16213_GM};
static MethodInfo* t2970_MIs[] =
{
	&m16212_MI,
	&m16213_MI,
	NULL
};
extern MethodInfo m16213_MI;
extern MethodInfo m16217_MI;
static MethodInfo* t2970_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16213_MI,
	&m16217_MI,
};
extern Il2CppType t2972_0_0_0;
extern TypeInfo t2972_TI;
extern MethodInfo m22495_MI;
extern TypeInfo t16_TI;
extern MethodInfo m16219_MI;
extern TypeInfo t16_TI;
static Il2CppRGCTXData t2970_RGCTXData[8] = 
{
	&t2972_0_0_0/* Type Usage */,
	&t2972_TI/* Class Usage */,
	&m22495_MI/* Method Usage */,
	&t16_TI/* Class Usage */,
	&m16219_MI/* Method Usage */,
	&m16214_MI/* Method Usage */,
	&t16_TI/* Class Usage */,
	&m16216_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2970_0_0_0;
extern Il2CppType t2970_1_0_0;
struct t2970;
extern Il2CppGenericClass t2970_GC;
TypeInfo t2970_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2970_MIs, NULL, t2970_FIs, NULL, &t2971_TI, NULL, NULL, &t2970_TI, NULL, t2970_VT, &EmptyCustomAttributesCache, &t2970_TI, &t2970_0_0_0, &t2970_1_0_0, NULL, &t2970_GC, NULL, NULL, NULL, t2970_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2970), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2972.h"
extern TypeInfo t2972_TI;
#include "t2972MD.h"
struct t556;
#define m22495(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>
extern Il2CppType t2972_0_0_1;
FieldInfo t2971_f0_FieldInfo = 
{
	"Delegate", &t2972_0_0_1, &t2971_TI, offsetof(t2971, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2971_FIs[] =
{
	&t2971_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2971_m16214_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16214_GM;
MethodInfo m16214_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2971_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2971_m16214_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16214_GM};
extern Il2CppType t2972_0_0_0;
static ParameterInfo t2971_m16215_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2972_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16215_GM;
MethodInfo m16215_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2971_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2971_m16215_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16215_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2971_m16216_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16216_GM;
MethodInfo m16216_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2971_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2971_m16216_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16216_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2971_m16217_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16217_GM;
MethodInfo m16217_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2971_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2971_m16217_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16217_GM};
static MethodInfo* t2971_MIs[] =
{
	&m16214_MI,
	&m16215_MI,
	&m16216_MI,
	&m16217_MI,
	NULL
};
static MethodInfo* t2971_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16216_MI,
	&m16217_MI,
};
extern TypeInfo t2972_TI;
extern TypeInfo t16_TI;
static Il2CppRGCTXData t2971_RGCTXData[5] = 
{
	&t2972_0_0_0/* Type Usage */,
	&t2972_TI/* Class Usage */,
	&m22495_MI/* Method Usage */,
	&t16_TI/* Class Usage */,
	&m16219_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2971_0_0_0;
extern Il2CppType t2971_1_0_0;
struct t2971;
extern Il2CppGenericClass t2971_GC;
TypeInfo t2971_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2971_MIs, NULL, t2971_FIs, NULL, &t556_TI, NULL, NULL, &t2971_TI, NULL, t2971_VT, &EmptyCustomAttributesCache, &t2971_TI, &t2971_0_0_0, &t2971_1_0_0, NULL, &t2971_GC, NULL, NULL, NULL, t2971_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2971), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2972_m16218_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16218_GM;
MethodInfo m16218_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2972_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2972_m16218_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16218_GM};
extern Il2CppType t16_0_0_0;
static ParameterInfo t2972_m16219_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t16_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16219_GM;
MethodInfo m16219_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2972_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2972_m16219_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16219_GM};
extern Il2CppType t16_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2972_m16220_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t16_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16220_GM;
MethodInfo m16220_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2972_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2972_m16220_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16220_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2972_m16221_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16221_GM;
MethodInfo m16221_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2972_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2972_m16221_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16221_GM};
static MethodInfo* t2972_MIs[] =
{
	&m16218_MI,
	&m16219_MI,
	&m16220_MI,
	&m16221_MI,
	NULL
};
extern MethodInfo m16220_MI;
extern MethodInfo m16221_MI;
static MethodInfo* t2972_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16219_MI,
	&m16220_MI,
	&m16221_MI,
};
static Il2CppInterfaceOffsetPair t2972_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2972_1_0_0;
struct t2972;
extern Il2CppGenericClass t2972_GC;
TypeInfo t2972_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2972_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2972_TI, NULL, t2972_VT, &EmptyCustomAttributesCache, &t2972_TI, &t2972_0_0_0, &t2972_1_0_0, t2972_IOs, &t2972_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2972), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2973.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2973_TI;
#include "t2973MD.h"

#include "t25.h"
#include "t2974.h"
extern TypeInfo t25_TI;
extern TypeInfo t2974_TI;
#include "t2974MD.h"
extern MethodInfo m16224_MI;
extern MethodInfo m16226_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Transform>
extern Il2CppType t316_0_0_33;
FieldInfo t2973_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2973_TI, offsetof(t2973, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2973_FIs[] =
{
	&t2973_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t25_0_0_0;
extern Il2CppType t25_0_0_0;
static ParameterInfo t2973_m16222_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16222_GM;
MethodInfo m16222_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2973_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2973_m16222_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16222_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2973_m16223_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16223_GM;
MethodInfo m16223_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2973_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2973_m16223_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16223_GM};
static MethodInfo* t2973_MIs[] =
{
	&m16222_MI,
	&m16223_MI,
	NULL
};
extern MethodInfo m16223_MI;
extern MethodInfo m16227_MI;
static MethodInfo* t2973_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16223_MI,
	&m16227_MI,
};
extern Il2CppType t2975_0_0_0;
extern TypeInfo t2975_TI;
extern MethodInfo m22496_MI;
extern TypeInfo t25_TI;
extern MethodInfo m16229_MI;
extern TypeInfo t25_TI;
static Il2CppRGCTXData t2973_RGCTXData[8] = 
{
	&t2975_0_0_0/* Type Usage */,
	&t2975_TI/* Class Usage */,
	&m22496_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
	&m16229_MI/* Method Usage */,
	&m16224_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
	&m16226_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2973_0_0_0;
extern Il2CppType t2973_1_0_0;
struct t2973;
extern Il2CppGenericClass t2973_GC;
TypeInfo t2973_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2973_MIs, NULL, t2973_FIs, NULL, &t2974_TI, NULL, NULL, &t2973_TI, NULL, t2973_VT, &EmptyCustomAttributesCache, &t2973_TI, &t2973_0_0_0, &t2973_1_0_0, NULL, &t2973_GC, NULL, NULL, NULL, t2973_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2973), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2975.h"
extern TypeInfo t2975_TI;
#include "t2975MD.h"
struct t556;
#define m22496(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Transform>
extern Il2CppType t2975_0_0_1;
FieldInfo t2974_f0_FieldInfo = 
{
	"Delegate", &t2975_0_0_1, &t2974_TI, offsetof(t2974, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2974_FIs[] =
{
	&t2974_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2974_m16224_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16224_GM;
MethodInfo m16224_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2974_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2974_m16224_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16224_GM};
extern Il2CppType t2975_0_0_0;
static ParameterInfo t2974_m16225_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2975_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16225_GM;
MethodInfo m16225_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2974_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2974_m16225_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16225_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2974_m16226_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16226_GM;
MethodInfo m16226_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2974_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2974_m16226_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16226_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2974_m16227_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16227_GM;
MethodInfo m16227_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2974_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2974_m16227_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16227_GM};
static MethodInfo* t2974_MIs[] =
{
	&m16224_MI,
	&m16225_MI,
	&m16226_MI,
	&m16227_MI,
	NULL
};
static MethodInfo* t2974_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16226_MI,
	&m16227_MI,
};
extern TypeInfo t2975_TI;
extern TypeInfo t25_TI;
static Il2CppRGCTXData t2974_RGCTXData[5] = 
{
	&t2975_0_0_0/* Type Usage */,
	&t2975_TI/* Class Usage */,
	&m22496_MI/* Method Usage */,
	&t25_TI/* Class Usage */,
	&m16229_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2974_0_0_0;
extern Il2CppType t2974_1_0_0;
struct t2974;
extern Il2CppGenericClass t2974_GC;
TypeInfo t2974_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2974_MIs, NULL, t2974_FIs, NULL, &t556_TI, NULL, NULL, &t2974_TI, NULL, t2974_VT, &EmptyCustomAttributesCache, &t2974_TI, &t2974_0_0_0, &t2974_1_0_0, NULL, &t2974_GC, NULL, NULL, NULL, t2974_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2974), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Transform>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2975_m16228_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16228_GM;
MethodInfo m16228_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2975_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2975_m16228_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16228_GM};
extern Il2CppType t25_0_0_0;
static ParameterInfo t2975_m16229_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16229_GM;
MethodInfo m16229_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2975_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2975_m16229_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16229_GM};
extern Il2CppType t25_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2975_m16230_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t25_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16230_GM;
MethodInfo m16230_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2975_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2975_m16230_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16230_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2975_m16231_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16231_GM;
MethodInfo m16231_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2975_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2975_m16231_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16231_GM};
static MethodInfo* t2975_MIs[] =
{
	&m16228_MI,
	&m16229_MI,
	&m16230_MI,
	&m16231_MI,
	NULL
};
extern MethodInfo m16230_MI;
extern MethodInfo m16231_MI;
static MethodInfo* t2975_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16229_MI,
	&m16230_MI,
	&m16231_MI,
};
static Il2CppInterfaceOffsetPair t2975_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2975_1_0_0;
struct t2975;
extern Il2CppGenericClass t2975_GC;
TypeInfo t2975_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2975_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2975_TI, NULL, t2975_VT, &EmptyCustomAttributesCache, &t2975_TI, &t2975_0_0_0, &t2975_1_0_0, t2975_IOs, &t2975_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2975), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4412_TI;

#include "t336.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Collider>
extern MethodInfo m29615_MI;
static PropertyInfo t4412____Current_PropertyInfo = 
{
	&t4412_TI, "Current", &m29615_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4412_PIs[] =
{
	&t4412____Current_PropertyInfo,
	NULL
};
extern Il2CppType t336_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29615_GM;
MethodInfo m29615_MI = 
{
	"get_Current", NULL, &t4412_TI, &t336_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29615_GM};
static MethodInfo* t4412_MIs[] =
{
	&m29615_MI,
	NULL
};
static TypeInfo* t4412_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4412_0_0_0;
extern Il2CppType t4412_1_0_0;
struct t4412;
extern Il2CppGenericClass t4412_GC;
TypeInfo t4412_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4412_MIs, t4412_PIs, NULL, NULL, NULL, NULL, NULL, &t4412_TI, t4412_ITIs, NULL, &EmptyCustomAttributesCache, &t4412_TI, &t4412_0_0_0, &t4412_1_0_0, NULL, &t4412_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2976.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2976_TI;
#include "t2976MD.h"

extern TypeInfo t336_TI;
extern MethodInfo m16236_MI;
extern MethodInfo m22498_MI;
struct t20;
#define m22498(__this, p0, method) (t336 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Collider>
extern Il2CppType t20_0_0_1;
FieldInfo t2976_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2976_TI, offsetof(t2976, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2976_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2976_TI, offsetof(t2976, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2976_FIs[] =
{
	&t2976_f0_FieldInfo,
	&t2976_f1_FieldInfo,
	NULL
};
extern MethodInfo m16233_MI;
static PropertyInfo t2976____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2976_TI, "System.Collections.IEnumerator.Current", &m16233_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2976____Current_PropertyInfo = 
{
	&t2976_TI, "Current", &m16236_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2976_PIs[] =
{
	&t2976____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2976____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2976_m16232_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16232_GM;
MethodInfo m16232_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2976_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2976_m16232_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16232_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16233_GM;
MethodInfo m16233_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2976_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16233_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16234_GM;
MethodInfo m16234_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2976_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16234_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16235_GM;
MethodInfo m16235_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2976_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16235_GM};
extern Il2CppType t336_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16236_GM;
MethodInfo m16236_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2976_TI, &t336_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16236_GM};
static MethodInfo* t2976_MIs[] =
{
	&m16232_MI,
	&m16233_MI,
	&m16234_MI,
	&m16235_MI,
	&m16236_MI,
	NULL
};
extern MethodInfo m16235_MI;
extern MethodInfo m16234_MI;
static MethodInfo* t2976_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16233_MI,
	&m16235_MI,
	&m16234_MI,
	&m16236_MI,
};
static TypeInfo* t2976_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4412_TI,
};
static Il2CppInterfaceOffsetPair t2976_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4412_TI, 7},
};
extern TypeInfo t336_TI;
static Il2CppRGCTXData t2976_RGCTXData[3] = 
{
	&m16236_MI/* Method Usage */,
	&t336_TI/* Class Usage */,
	&m22498_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2976_0_0_0;
extern Il2CppType t2976_1_0_0;
extern Il2CppGenericClass t2976_GC;
TypeInfo t2976_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2976_MIs, t2976_PIs, t2976_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2976_TI, t2976_ITIs, t2976_VT, &EmptyCustomAttributesCache, &t2976_TI, &t2976_0_0_0, &t2976_1_0_0, t2976_IOs, &t2976_GC, NULL, NULL, NULL, t2976_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2976)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5664_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Collider>
extern MethodInfo m29616_MI;
static PropertyInfo t5664____Count_PropertyInfo = 
{
	&t5664_TI, "Count", &m29616_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29617_MI;
static PropertyInfo t5664____IsReadOnly_PropertyInfo = 
{
	&t5664_TI, "IsReadOnly", &m29617_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5664_PIs[] =
{
	&t5664____Count_PropertyInfo,
	&t5664____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29616_GM;
MethodInfo m29616_MI = 
{
	"get_Count", NULL, &t5664_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29616_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29617_GM;
MethodInfo m29617_MI = 
{
	"get_IsReadOnly", NULL, &t5664_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29617_GM};
extern Il2CppType t336_0_0_0;
extern Il2CppType t336_0_0_0;
static ParameterInfo t5664_m29618_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t336_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29618_GM;
MethodInfo m29618_MI = 
{
	"Add", NULL, &t5664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5664_m29618_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29618_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29619_GM;
MethodInfo m29619_MI = 
{
	"Clear", NULL, &t5664_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29619_GM};
extern Il2CppType t336_0_0_0;
static ParameterInfo t5664_m29620_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t336_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29620_GM;
MethodInfo m29620_MI = 
{
	"Contains", NULL, &t5664_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5664_m29620_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29620_GM};
extern Il2CppType t3764_0_0_0;
extern Il2CppType t3764_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5664_m29621_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3764_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29621_GM;
MethodInfo m29621_MI = 
{
	"CopyTo", NULL, &t5664_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5664_m29621_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29621_GM};
extern Il2CppType t336_0_0_0;
static ParameterInfo t5664_m29622_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t336_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29622_GM;
MethodInfo m29622_MI = 
{
	"Remove", NULL, &t5664_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5664_m29622_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29622_GM};
static MethodInfo* t5664_MIs[] =
{
	&m29616_MI,
	&m29617_MI,
	&m29618_MI,
	&m29619_MI,
	&m29620_MI,
	&m29621_MI,
	&m29622_MI,
	NULL
};
extern TypeInfo t5666_TI;
static TypeInfo* t5664_ITIs[] = 
{
	&t603_TI,
	&t5666_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5664_0_0_0;
extern Il2CppType t5664_1_0_0;
struct t5664;
extern Il2CppGenericClass t5664_GC;
TypeInfo t5664_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5664_MIs, t5664_PIs, NULL, NULL, NULL, NULL, NULL, &t5664_TI, t5664_ITIs, NULL, &EmptyCustomAttributesCache, &t5664_TI, &t5664_0_0_0, &t5664_1_0_0, NULL, &t5664_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Collider>
extern Il2CppType t4412_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29623_GM;
MethodInfo m29623_MI = 
{
	"GetEnumerator", NULL, &t5666_TI, &t4412_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29623_GM};
static MethodInfo* t5666_MIs[] =
{
	&m29623_MI,
	NULL
};
static TypeInfo* t5666_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5666_0_0_0;
extern Il2CppType t5666_1_0_0;
struct t5666;
extern Il2CppGenericClass t5666_GC;
TypeInfo t5666_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5666_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5666_TI, t5666_ITIs, NULL, &EmptyCustomAttributesCache, &t5666_TI, &t5666_0_0_0, &t5666_1_0_0, NULL, &t5666_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5665_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Collider>
extern MethodInfo m29624_MI;
extern MethodInfo m29625_MI;
static PropertyInfo t5665____Item_PropertyInfo = 
{
	&t5665_TI, "Item", &m29624_MI, &m29625_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5665_PIs[] =
{
	&t5665____Item_PropertyInfo,
	NULL
};
extern Il2CppType t336_0_0_0;
static ParameterInfo t5665_m29626_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t336_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29626_GM;
MethodInfo m29626_MI = 
{
	"IndexOf", NULL, &t5665_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5665_m29626_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29626_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t336_0_0_0;
static ParameterInfo t5665_m29627_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t336_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29627_GM;
MethodInfo m29627_MI = 
{
	"Insert", NULL, &t5665_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5665_m29627_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29627_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5665_m29628_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29628_GM;
MethodInfo m29628_MI = 
{
	"RemoveAt", NULL, &t5665_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5665_m29628_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29628_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5665_m29624_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t336_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29624_GM;
MethodInfo m29624_MI = 
{
	"get_Item", NULL, &t5665_TI, &t336_0_0_0, RuntimeInvoker_t29_t44, t5665_m29624_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29624_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t336_0_0_0;
static ParameterInfo t5665_m29625_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t336_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29625_GM;
MethodInfo m29625_MI = 
{
	"set_Item", NULL, &t5665_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5665_m29625_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29625_GM};
static MethodInfo* t5665_MIs[] =
{
	&m29626_MI,
	&m29627_MI,
	&m29628_MI,
	&m29624_MI,
	&m29625_MI,
	NULL
};
static TypeInfo* t5665_ITIs[] = 
{
	&t603_TI,
	&t5664_TI,
	&t5666_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5665_0_0_0;
extern Il2CppType t5665_1_0_0;
struct t5665;
extern Il2CppGenericClass t5665_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5665_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5665_MIs, t5665_PIs, NULL, NULL, NULL, NULL, NULL, &t5665_TI, t5665_ITIs, NULL, &t1908__CustomAttributeCache, &t5665_TI, &t5665_0_0_0, &t5665_1_0_0, NULL, &t5665_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2977.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2977_TI;
#include "t2977MD.h"

#include "t2978.h"
extern TypeInfo t2978_TI;
#include "t2978MD.h"
extern MethodInfo m16239_MI;
extern MethodInfo m16241_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider>
extern Il2CppType t316_0_0_33;
FieldInfo t2977_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2977_TI, offsetof(t2977, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2977_FIs[] =
{
	&t2977_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t336_0_0_0;
static ParameterInfo t2977_m16237_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t336_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16237_GM;
MethodInfo m16237_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2977_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2977_m16237_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16237_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2977_m16238_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16238_GM;
MethodInfo m16238_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2977_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2977_m16238_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16238_GM};
static MethodInfo* t2977_MIs[] =
{
	&m16237_MI,
	&m16238_MI,
	NULL
};
extern MethodInfo m16238_MI;
extern MethodInfo m16242_MI;
static MethodInfo* t2977_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16238_MI,
	&m16242_MI,
};
extern Il2CppType t2979_0_0_0;
extern TypeInfo t2979_TI;
extern MethodInfo m22508_MI;
extern TypeInfo t336_TI;
extern MethodInfo m16244_MI;
extern TypeInfo t336_TI;
static Il2CppRGCTXData t2977_RGCTXData[8] = 
{
	&t2979_0_0_0/* Type Usage */,
	&t2979_TI/* Class Usage */,
	&m22508_MI/* Method Usage */,
	&t336_TI/* Class Usage */,
	&m16244_MI/* Method Usage */,
	&m16239_MI/* Method Usage */,
	&t336_TI/* Class Usage */,
	&m16241_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2977_0_0_0;
extern Il2CppType t2977_1_0_0;
struct t2977;
extern Il2CppGenericClass t2977_GC;
TypeInfo t2977_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2977_MIs, NULL, t2977_FIs, NULL, &t2978_TI, NULL, NULL, &t2977_TI, NULL, t2977_VT, &EmptyCustomAttributesCache, &t2977_TI, &t2977_0_0_0, &t2977_1_0_0, NULL, &t2977_GC, NULL, NULL, NULL, t2977_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2977), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2979.h"
extern TypeInfo t2979_TI;
#include "t2979MD.h"
struct t556;
#define m22508(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>
extern Il2CppType t2979_0_0_1;
FieldInfo t2978_f0_FieldInfo = 
{
	"Delegate", &t2979_0_0_1, &t2978_TI, offsetof(t2978, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2978_FIs[] =
{
	&t2978_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2978_m16239_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16239_GM;
MethodInfo m16239_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2978_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2978_m16239_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16239_GM};
extern Il2CppType t2979_0_0_0;
static ParameterInfo t2978_m16240_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2979_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16240_GM;
MethodInfo m16240_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2978_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2978_m16240_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16240_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2978_m16241_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16241_GM;
MethodInfo m16241_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2978_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2978_m16241_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16241_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2978_m16242_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16242_GM;
MethodInfo m16242_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2978_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2978_m16242_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16242_GM};
static MethodInfo* t2978_MIs[] =
{
	&m16239_MI,
	&m16240_MI,
	&m16241_MI,
	&m16242_MI,
	NULL
};
static MethodInfo* t2978_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16241_MI,
	&m16242_MI,
};
extern TypeInfo t2979_TI;
extern TypeInfo t336_TI;
static Il2CppRGCTXData t2978_RGCTXData[5] = 
{
	&t2979_0_0_0/* Type Usage */,
	&t2979_TI/* Class Usage */,
	&m22508_MI/* Method Usage */,
	&t336_TI/* Class Usage */,
	&m16244_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2978_0_0_0;
extern Il2CppType t2978_1_0_0;
struct t2978;
extern Il2CppGenericClass t2978_GC;
TypeInfo t2978_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2978_MIs, NULL, t2978_FIs, NULL, &t556_TI, NULL, NULL, &t2978_TI, NULL, t2978_VT, &EmptyCustomAttributesCache, &t2978_TI, &t2978_0_0_0, &t2978_1_0_0, NULL, &t2978_GC, NULL, NULL, NULL, t2978_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2978), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Collider>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2979_m16243_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16243_GM;
MethodInfo m16243_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2979_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2979_m16243_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16243_GM};
extern Il2CppType t336_0_0_0;
static ParameterInfo t2979_m16244_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t336_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16244_GM;
MethodInfo m16244_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2979_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2979_m16244_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16244_GM};
extern Il2CppType t336_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2979_m16245_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t336_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16245_GM;
MethodInfo m16245_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2979_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2979_m16245_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16245_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2979_m16246_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16246_GM;
MethodInfo m16246_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2979_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2979_m16246_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16246_GM};
static MethodInfo* t2979_MIs[] =
{
	&m16243_MI,
	&m16244_MI,
	&m16245_MI,
	&m16246_MI,
	NULL
};
extern MethodInfo m16245_MI;
extern MethodInfo m16246_MI;
static MethodInfo* t2979_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16244_MI,
	&m16245_MI,
	&m16246_MI,
};
static Il2CppInterfaceOffsetPair t2979_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2979_1_0_0;
struct t2979;
extern Il2CppGenericClass t2979_GC;
TypeInfo t2979_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2979_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2979_TI, NULL, t2979_VT, &EmptyCustomAttributesCache, &t2979_TI, &t2979_0_0_0, &t2979_1_0_0, t2979_IOs, &t2979_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2979), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t506.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t506_TI;
#include "t506MD.h"

#include "t507.h"
#include "t2987.h"
#include "t2984.h"
#include "t2985.h"
#include "t338.h"
#include "t2993.h"
#include "t2986.h"
extern TypeInfo t507_TI;
extern TypeInfo t44_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2980_TI;
extern TypeInfo t2987_TI;
extern TypeInfo t40_TI;
extern TypeInfo t2982_TI;
extern TypeInfo t2983_TI;
extern TypeInfo t2981_TI;
extern TypeInfo t2984_TI;
extern TypeInfo t338_TI;
extern TypeInfo t2985_TI;
extern TypeInfo t2993_TI;
#include "t915MD.h"
#include "t29MD.h"
#include "t602MD.h"
#include "t2984MD.h"
#include "t338MD.h"
#include "t2985MD.h"
#include "t2987MD.h"
#include "t2993MD.h"
extern MethodInfo m16293_MI;
extern MethodInfo m16294_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m22521_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m16279_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m16276_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m16264_MI;
extern MethodInfo m16271_MI;
extern MethodInfo m16277_MI;
extern MethodInfo m16280_MI;
extern MethodInfo m16282_MI;
extern MethodInfo m16265_MI;
extern MethodInfo m16290_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m16291_MI;
extern MethodInfo m29629_MI;
extern MethodInfo m29630_MI;
extern MethodInfo m29631_MI;
extern MethodInfo m29632_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m16281_MI;
extern MethodInfo m16266_MI;
extern MethodInfo m16267_MI;
extern MethodInfo m16306_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m22523_MI;
extern MethodInfo m16274_MI;
extern MethodInfo m16275_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m16381_MI;
extern MethodInfo m16300_MI;
extern MethodInfo m16278_MI;
extern MethodInfo m16284_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m16387_MI;
extern MethodInfo m22525_MI;
extern MethodInfo m22533_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m22521(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2991.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m22523(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m22525(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m22533(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2987  m16276 (t506 * __this, MethodInfo* method){
	{
		t2987  L_0 = {0};
		m16300(&L_0, __this, &m16300_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
extern Il2CppType t44_0_0_32849;
FieldInfo t506_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t506_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2980_0_0_1;
FieldInfo t506_f1_FieldInfo = 
{
	"_items", &t2980_0_0_1, &t506_TI, offsetof(t506, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t506_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t506_TI, offsetof(t506, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t506_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t506_TI, offsetof(t506, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2980_0_0_49;
FieldInfo t506_f4_FieldInfo = 
{
	"EmptyArray", &t2980_0_0_49, &t506_TI, offsetof(t506_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t506_FIs[] =
{
	&t506_f0_FieldInfo,
	&t506_f1_FieldInfo,
	&t506_f2_FieldInfo,
	&t506_f3_FieldInfo,
	&t506_f4_FieldInfo,
	NULL
};
static const int32_t t506_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t506_f0_DefaultValue = 
{
	&t506_f0_FieldInfo, { (char*)&t506_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t506_FDVs[] = 
{
	&t506_f0_DefaultValue,
	NULL
};
extern MethodInfo m16257_MI;
static PropertyInfo t506____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t506_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m16257_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16258_MI;
static PropertyInfo t506____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t506_TI, "System.Collections.ICollection.IsSynchronized", &m16258_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16259_MI;
static PropertyInfo t506____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t506_TI, "System.Collections.ICollection.SyncRoot", &m16259_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16260_MI;
static PropertyInfo t506____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t506_TI, "System.Collections.IList.IsFixedSize", &m16260_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16261_MI;
static PropertyInfo t506____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t506_TI, "System.Collections.IList.IsReadOnly", &m16261_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16262_MI;
extern MethodInfo m16263_MI;
static PropertyInfo t506____System_Collections_IList_Item_PropertyInfo = 
{
	&t506_TI, "System.Collections.IList.Item", &m16262_MI, &m16263_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t506____Capacity_PropertyInfo = 
{
	&t506_TI, "Capacity", &m16290_MI, &m16291_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16292_MI;
static PropertyInfo t506____Count_PropertyInfo = 
{
	&t506_TI, "Count", &m16292_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t506____Item_PropertyInfo = 
{
	&t506_TI, "Item", &m16293_MI, &m16294_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t506_PIs[] =
{
	&t506____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t506____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t506____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t506____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t506____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t506____System_Collections_IList_Item_PropertyInfo,
	&t506____Capacity_PropertyInfo,
	&t506____Count_PropertyInfo,
	&t506____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2901_GM;
MethodInfo m2901_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2901_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t506_m16247_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16247_GM;
MethodInfo m16247_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t506_m16247_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16247_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16248_GM;
MethodInfo m16248_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16248_GM};
extern Il2CppType t2981_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16249_GM;
MethodInfo m16249_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t506_TI, &t2981_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16249_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t506_m16250_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16250_GM;
MethodInfo m16250_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t506_m16250_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16250_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16251_GM;
MethodInfo m16251_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t506_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16251_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t506_m16252_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16252_GM;
MethodInfo m16252_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t506_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t506_m16252_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16252_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t506_m16253_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16253_GM;
MethodInfo m16253_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t506_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t506_m16253_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16253_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t506_m16254_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16254_GM;
MethodInfo m16254_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t506_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t506_m16254_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16254_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t506_m16255_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16255_GM;
MethodInfo m16255_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t506_m16255_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16255_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t506_m16256_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16256_GM;
MethodInfo m16256_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t506_m16256_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16256_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16257_GM;
MethodInfo m16257_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t506_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16257_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16258_GM;
MethodInfo m16258_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t506_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16258_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16259_GM;
MethodInfo m16259_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t506_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16259_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16260_GM;
MethodInfo m16260_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t506_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16260_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16261_GM;
MethodInfo m16261_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t506_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16261_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t506_m16262_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16262_GM;
MethodInfo m16262_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t506_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t506_m16262_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16262_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t506_m16263_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16263_GM;
MethodInfo m16263_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t506_m16263_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16263_GM};
extern Il2CppType t507_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t506_m16264_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16264_GM;
MethodInfo m16264_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t506_m16264_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16264_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t506_m16265_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16265_GM;
MethodInfo m16265_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t506_m16265_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16265_GM};
extern Il2CppType t2982_0_0_0;
extern Il2CppType t2982_0_0_0;
static ParameterInfo t506_m16266_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2982_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16266_GM;
MethodInfo m16266_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t506_m16266_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16266_GM};
extern Il2CppType t2983_0_0_0;
extern Il2CppType t2983_0_0_0;
static ParameterInfo t506_m16267_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2983_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16267_GM;
MethodInfo m16267_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t506_m16267_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16267_GM};
extern Il2CppType t2983_0_0_0;
static ParameterInfo t506_m16268_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2983_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16268_GM;
MethodInfo m16268_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t506_m16268_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16268_GM};
extern Il2CppType t2984_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16269_GM;
MethodInfo m16269_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t506_TI, &t2984_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16269_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16270_GM;
MethodInfo m16270_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16270_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t506_m16271_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16271_GM;
MethodInfo m16271_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t506_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t506_m16271_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16271_GM};
extern Il2CppType t2980_0_0_0;
extern Il2CppType t2980_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t506_m16272_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2980_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16272_GM;
MethodInfo m16272_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t506_m16272_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16272_GM};
extern Il2CppType t2985_0_0_0;
extern Il2CppType t2985_0_0_0;
static ParameterInfo t506_m16273_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2985_0_0_0},
};
extern Il2CppType t507_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16273_GM;
MethodInfo m16273_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t506_TI, &t507_0_0_0, RuntimeInvoker_t29_t29, t506_m16273_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16273_GM};
extern Il2CppType t2985_0_0_0;
static ParameterInfo t506_m16274_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2985_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16274_GM;
MethodInfo m16274_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t506_m16274_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16274_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2985_0_0_0;
static ParameterInfo t506_m16275_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2985_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16275_GM;
MethodInfo m16275_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t506_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t506_m16275_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16275_GM};
extern Il2CppType t2987_0_0_0;
extern void* RuntimeInvoker_t2987 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16276_GM;
MethodInfo m16276_MI = 
{
	"GetEnumerator", (methodPointerType)&m16276, &t506_TI, &t2987_0_0_0, RuntimeInvoker_t2987, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16276_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t506_m16277_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16277_GM;
MethodInfo m16277_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t506_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t506_m16277_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16277_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t506_m16278_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16278_GM;
MethodInfo m16278_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t506_m16278_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16278_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t506_m16279_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16279_GM;
MethodInfo m16279_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t506_m16279_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16279_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t506_m16280_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16280_GM;
MethodInfo m16280_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t506_m16280_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16280_GM};
extern Il2CppType t2983_0_0_0;
static ParameterInfo t506_m16281_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2983_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16281_GM;
MethodInfo m16281_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t506_m16281_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16281_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t506_m16282_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16282_GM;
MethodInfo m16282_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t506_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t506_m16282_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16282_GM};
extern Il2CppType t2985_0_0_0;
static ParameterInfo t506_m16283_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2985_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16283_GM;
MethodInfo m16283_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t506_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t506_m16283_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16283_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t506_m16284_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16284_GM;
MethodInfo m16284_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t506_m16284_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16284_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16285_GM;
MethodInfo m16285_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16285_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16286_GM;
MethodInfo m16286_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16286_GM};
extern Il2CppType t2986_0_0_0;
extern Il2CppType t2986_0_0_0;
static ParameterInfo t506_m16287_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2986_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16287_GM;
MethodInfo m16287_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t506_m16287_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16287_GM};
extern Il2CppType t2980_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16288_GM;
MethodInfo m16288_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t506_TI, &t2980_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16288_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16289_GM;
MethodInfo m16289_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16289_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16290_GM;
MethodInfo m16290_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t506_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16290_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t506_m16291_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16291_GM;
MethodInfo m16291_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t506_m16291_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16291_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16292_GM;
MethodInfo m16292_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t506_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16292_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t506_m16293_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t507_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16293_GM;
MethodInfo m16293_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t506_TI, &t507_0_0_0, RuntimeInvoker_t29_t44, t506_m16293_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16293_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t506_m16294_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16294_GM;
MethodInfo m16294_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t506_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t506_m16294_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16294_GM};
static MethodInfo* t506_MIs[] =
{
	&m2901_MI,
	&m16247_MI,
	&m16248_MI,
	&m16249_MI,
	&m16250_MI,
	&m16251_MI,
	&m16252_MI,
	&m16253_MI,
	&m16254_MI,
	&m16255_MI,
	&m16256_MI,
	&m16257_MI,
	&m16258_MI,
	&m16259_MI,
	&m16260_MI,
	&m16261_MI,
	&m16262_MI,
	&m16263_MI,
	&m16264_MI,
	&m16265_MI,
	&m16266_MI,
	&m16267_MI,
	&m16268_MI,
	&m16269_MI,
	&m16270_MI,
	&m16271_MI,
	&m16272_MI,
	&m16273_MI,
	&m16274_MI,
	&m16275_MI,
	&m16276_MI,
	&m16277_MI,
	&m16278_MI,
	&m16279_MI,
	&m16280_MI,
	&m16281_MI,
	&m16282_MI,
	&m16283_MI,
	&m16284_MI,
	&m16285_MI,
	&m16286_MI,
	&m16287_MI,
	&m16288_MI,
	&m16289_MI,
	&m16290_MI,
	&m16291_MI,
	&m16292_MI,
	&m16293_MI,
	&m16294_MI,
	NULL
};
extern MethodInfo m16251_MI;
extern MethodInfo m16250_MI;
extern MethodInfo m16252_MI;
extern MethodInfo m16270_MI;
extern MethodInfo m16253_MI;
extern MethodInfo m16254_MI;
extern MethodInfo m16255_MI;
extern MethodInfo m16256_MI;
extern MethodInfo m16272_MI;
extern MethodInfo m16249_MI;
static MethodInfo* t506_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16251_MI,
	&m16292_MI,
	&m16258_MI,
	&m16259_MI,
	&m16250_MI,
	&m16260_MI,
	&m16261_MI,
	&m16262_MI,
	&m16263_MI,
	&m16252_MI,
	&m16270_MI,
	&m16253_MI,
	&m16254_MI,
	&m16255_MI,
	&m16256_MI,
	&m16284_MI,
	&m16292_MI,
	&m16257_MI,
	&m16264_MI,
	&m16270_MI,
	&m16271_MI,
	&m16272_MI,
	&m16282_MI,
	&m16249_MI,
	&m16277_MI,
	&m16280_MI,
	&m16284_MI,
	&m16293_MI,
	&m16294_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
extern TypeInfo t2989_TI;
static TypeInfo* t506_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2982_TI,
	&t2983_TI,
	&t2989_TI,
};
static Il2CppInterfaceOffsetPair t506_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2982_TI, 20},
	{ &t2983_TI, 27},
	{ &t2989_TI, 28},
};
extern TypeInfo t506_TI;
extern TypeInfo t2980_TI;
extern TypeInfo t2987_TI;
extern TypeInfo t507_TI;
extern TypeInfo t2982_TI;
extern TypeInfo t2984_TI;
static Il2CppRGCTXData t506_RGCTXData[37] = 
{
	&t506_TI/* Static Usage */,
	&t2980_TI/* Array Usage */,
	&m16276_MI/* Method Usage */,
	&t2987_TI/* Class Usage */,
	&t507_TI/* Class Usage */,
	&m16264_MI/* Method Usage */,
	&m16271_MI/* Method Usage */,
	&m16277_MI/* Method Usage */,
	&m16279_MI/* Method Usage */,
	&m16280_MI/* Method Usage */,
	&m16282_MI/* Method Usage */,
	&m16293_MI/* Method Usage */,
	&m16294_MI/* Method Usage */,
	&m16265_MI/* Method Usage */,
	&m16290_MI/* Method Usage */,
	&m16291_MI/* Method Usage */,
	&m29629_MI/* Method Usage */,
	&m29630_MI/* Method Usage */,
	&m29631_MI/* Method Usage */,
	&m29632_MI/* Method Usage */,
	&m16281_MI/* Method Usage */,
	&t2982_TI/* Class Usage */,
	&m16266_MI/* Method Usage */,
	&m16267_MI/* Method Usage */,
	&t2984_TI/* Class Usage */,
	&m16306_MI/* Method Usage */,
	&m22523_MI/* Method Usage */,
	&m16274_MI/* Method Usage */,
	&m16275_MI/* Method Usage */,
	&m16381_MI/* Method Usage */,
	&m16300_MI/* Method Usage */,
	&m16278_MI/* Method Usage */,
	&m16284_MI/* Method Usage */,
	&m16387_MI/* Method Usage */,
	&m22525_MI/* Method Usage */,
	&m22533_MI/* Method Usage */,
	&m22521_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t506_0_0_0;
extern Il2CppType t506_1_0_0;
struct t506;
extern Il2CppGenericClass t506_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t506_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t506_MIs, t506_PIs, t506_FIs, NULL, &t29_TI, NULL, NULL, &t506_TI, t506_ITIs, t506_VT, &t1261__CustomAttributeCache, &t506_TI, &t506_0_0_0, &t506_1_0_0, t506_IOs, &t506_GC, NULL, t506_FDVs, NULL, t506_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t506), 0, -1, sizeof(t506_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>
static PropertyInfo t2982____Count_PropertyInfo = 
{
	&t2982_TI, "Count", &m29629_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29633_MI;
static PropertyInfo t2982____IsReadOnly_PropertyInfo = 
{
	&t2982_TI, "IsReadOnly", &m29633_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2982_PIs[] =
{
	&t2982____Count_PropertyInfo,
	&t2982____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29629_GM;
MethodInfo m29629_MI = 
{
	"get_Count", NULL, &t2982_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29629_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29633_GM;
MethodInfo m29633_MI = 
{
	"get_IsReadOnly", NULL, &t2982_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29633_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2982_m29634_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29634_GM;
MethodInfo m29634_MI = 
{
	"Add", NULL, &t2982_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2982_m29634_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29634_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29635_GM;
MethodInfo m29635_MI = 
{
	"Clear", NULL, &t2982_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29635_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2982_m29636_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29636_GM;
MethodInfo m29636_MI = 
{
	"Contains", NULL, &t2982_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2982_m29636_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29636_GM};
extern Il2CppType t2980_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2982_m29630_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2980_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29630_GM;
MethodInfo m29630_MI = 
{
	"CopyTo", NULL, &t2982_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2982_m29630_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29630_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2982_m29637_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29637_GM;
MethodInfo m29637_MI = 
{
	"Remove", NULL, &t2982_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2982_m29637_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29637_GM};
static MethodInfo* t2982_MIs[] =
{
	&m29629_MI,
	&m29633_MI,
	&m29634_MI,
	&m29635_MI,
	&m29636_MI,
	&m29630_MI,
	&m29637_MI,
	NULL
};
static TypeInfo* t2982_ITIs[] = 
{
	&t603_TI,
	&t2983_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2982_1_0_0;
struct t2982;
extern Il2CppGenericClass t2982_GC;
TypeInfo t2982_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2982_MIs, t2982_PIs, NULL, NULL, NULL, NULL, NULL, &t2982_TI, t2982_ITIs, NULL, &EmptyCustomAttributesCache, &t2982_TI, &t2982_0_0_0, &t2982_1_0_0, NULL, &t2982_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Rigidbody2D>
extern Il2CppType t2981_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29631_GM;
MethodInfo m29631_MI = 
{
	"GetEnumerator", NULL, &t2983_TI, &t2981_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29631_GM};
static MethodInfo* t2983_MIs[] =
{
	&m29631_MI,
	NULL
};
static TypeInfo* t2983_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2983_1_0_0;
struct t2983;
extern Il2CppGenericClass t2983_GC;
TypeInfo t2983_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2983_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2983_TI, t2983_ITIs, NULL, &EmptyCustomAttributesCache, &t2983_TI, &t2983_0_0_0, &t2983_1_0_0, NULL, &t2983_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Rigidbody2D>
static PropertyInfo t2981____Current_PropertyInfo = 
{
	&t2981_TI, "Current", &m29632_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2981_PIs[] =
{
	&t2981____Current_PropertyInfo,
	NULL
};
extern Il2CppType t507_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29632_GM;
MethodInfo m29632_MI = 
{
	"get_Current", NULL, &t2981_TI, &t507_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29632_GM};
static MethodInfo* t2981_MIs[] =
{
	&m29632_MI,
	NULL
};
static TypeInfo* t2981_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2981_0_0_0;
extern Il2CppType t2981_1_0_0;
struct t2981;
extern Il2CppGenericClass t2981_GC;
TypeInfo t2981_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2981_MIs, t2981_PIs, NULL, NULL, NULL, NULL, NULL, &t2981_TI, t2981_ITIs, NULL, &EmptyCustomAttributesCache, &t2981_TI, &t2981_0_0_0, &t2981_1_0_0, NULL, &t2981_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2988.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2988_TI;
#include "t2988MD.h"

extern MethodInfo m16299_MI;
extern MethodInfo m22510_MI;
struct t20;
#define m22510(__this, p0, method) (t507 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>
extern Il2CppType t20_0_0_1;
FieldInfo t2988_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2988_TI, offsetof(t2988, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2988_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2988_TI, offsetof(t2988, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2988_FIs[] =
{
	&t2988_f0_FieldInfo,
	&t2988_f1_FieldInfo,
	NULL
};
extern MethodInfo m16296_MI;
static PropertyInfo t2988____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2988_TI, "System.Collections.IEnumerator.Current", &m16296_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2988____Current_PropertyInfo = 
{
	&t2988_TI, "Current", &m16299_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2988_PIs[] =
{
	&t2988____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2988____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2988_m16295_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16295_GM;
MethodInfo m16295_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2988_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2988_m16295_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16295_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16296_GM;
MethodInfo m16296_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2988_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16296_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16297_GM;
MethodInfo m16297_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2988_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16297_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16298_GM;
MethodInfo m16298_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2988_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16298_GM};
extern Il2CppType t507_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16299_GM;
MethodInfo m16299_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2988_TI, &t507_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16299_GM};
static MethodInfo* t2988_MIs[] =
{
	&m16295_MI,
	&m16296_MI,
	&m16297_MI,
	&m16298_MI,
	&m16299_MI,
	NULL
};
extern MethodInfo m16298_MI;
extern MethodInfo m16297_MI;
static MethodInfo* t2988_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16296_MI,
	&m16298_MI,
	&m16297_MI,
	&m16299_MI,
};
static TypeInfo* t2988_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2981_TI,
};
static Il2CppInterfaceOffsetPair t2988_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2981_TI, 7},
};
extern TypeInfo t507_TI;
static Il2CppRGCTXData t2988_RGCTXData[3] = 
{
	&m16299_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
	&m22510_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2988_0_0_0;
extern Il2CppType t2988_1_0_0;
extern Il2CppGenericClass t2988_GC;
TypeInfo t2988_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2988_MIs, t2988_PIs, t2988_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2988_TI, t2988_ITIs, t2988_VT, &EmptyCustomAttributesCache, &t2988_TI, &t2988_0_0_0, &t2988_1_0_0, t2988_IOs, &t2988_GC, NULL, NULL, NULL, t2988_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2988)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Rigidbody2D>
extern MethodInfo m29638_MI;
extern MethodInfo m29639_MI;
static PropertyInfo t2989____Item_PropertyInfo = 
{
	&t2989_TI, "Item", &m29638_MI, &m29639_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2989_PIs[] =
{
	&t2989____Item_PropertyInfo,
	NULL
};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2989_m29640_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29640_GM;
MethodInfo m29640_MI = 
{
	"IndexOf", NULL, &t2989_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2989_m29640_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29640_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2989_m29641_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29641_GM;
MethodInfo m29641_MI = 
{
	"Insert", NULL, &t2989_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2989_m29641_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29641_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2989_m29642_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29642_GM;
MethodInfo m29642_MI = 
{
	"RemoveAt", NULL, &t2989_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2989_m29642_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29642_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2989_m29638_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t507_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29638_GM;
MethodInfo m29638_MI = 
{
	"get_Item", NULL, &t2989_TI, &t507_0_0_0, RuntimeInvoker_t29_t44, t2989_m29638_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29638_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2989_m29639_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29639_GM;
MethodInfo m29639_MI = 
{
	"set_Item", NULL, &t2989_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2989_m29639_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29639_GM};
static MethodInfo* t2989_MIs[] =
{
	&m29640_MI,
	&m29641_MI,
	&m29642_MI,
	&m29638_MI,
	&m29639_MI,
	NULL
};
static TypeInfo* t2989_ITIs[] = 
{
	&t603_TI,
	&t2982_TI,
	&t2983_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2989_0_0_0;
extern Il2CppType t2989_1_0_0;
struct t2989;
extern Il2CppGenericClass t2989_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2989_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2989_MIs, t2989_PIs, NULL, NULL, NULL, NULL, NULL, &t2989_TI, t2989_ITIs, NULL, &t1908__CustomAttributeCache, &t2989_TI, &t2989_0_0_0, &t2989_1_0_0, NULL, &t2989_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m16303_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody2D>
extern Il2CppType t506_0_0_1;
FieldInfo t2987_f0_FieldInfo = 
{
	"l", &t506_0_0_1, &t2987_TI, offsetof(t2987, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2987_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2987_TI, offsetof(t2987, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2987_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2987_TI, offsetof(t2987, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t507_0_0_1;
FieldInfo t2987_f3_FieldInfo = 
{
	"current", &t507_0_0_1, &t2987_TI, offsetof(t2987, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2987_FIs[] =
{
	&t2987_f0_FieldInfo,
	&t2987_f1_FieldInfo,
	&t2987_f2_FieldInfo,
	&t2987_f3_FieldInfo,
	NULL
};
extern MethodInfo m16301_MI;
static PropertyInfo t2987____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2987_TI, "System.Collections.IEnumerator.Current", &m16301_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16305_MI;
static PropertyInfo t2987____Current_PropertyInfo = 
{
	&t2987_TI, "Current", &m16305_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2987_PIs[] =
{
	&t2987____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2987____Current_PropertyInfo,
	NULL
};
extern Il2CppType t506_0_0_0;
static ParameterInfo t2987_m16300_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t506_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16300_GM;
MethodInfo m16300_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2987_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2987_m16300_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16300_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16301_GM;
MethodInfo m16301_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2987_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16301_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16302_GM;
MethodInfo m16302_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2987_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16302_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16303_GM;
MethodInfo m16303_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2987_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16303_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16304_GM;
MethodInfo m16304_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2987_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16304_GM};
extern Il2CppType t507_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16305_GM;
MethodInfo m16305_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2987_TI, &t507_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16305_GM};
static MethodInfo* t2987_MIs[] =
{
	&m16300_MI,
	&m16301_MI,
	&m16302_MI,
	&m16303_MI,
	&m16304_MI,
	&m16305_MI,
	NULL
};
extern MethodInfo m16304_MI;
extern MethodInfo m16302_MI;
static MethodInfo* t2987_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16301_MI,
	&m16304_MI,
	&m16302_MI,
	&m16305_MI,
};
static TypeInfo* t2987_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2981_TI,
};
static Il2CppInterfaceOffsetPair t2987_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2981_TI, 7},
};
extern TypeInfo t507_TI;
extern TypeInfo t2987_TI;
static Il2CppRGCTXData t2987_RGCTXData[3] = 
{
	&m16303_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
	&t2987_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2987_0_0_0;
extern Il2CppType t2987_1_0_0;
extern Il2CppGenericClass t2987_GC;
extern TypeInfo t1261_TI;
TypeInfo t2987_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2987_MIs, t2987_PIs, t2987_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2987_TI, t2987_ITIs, t2987_VT, &EmptyCustomAttributesCache, &t2987_TI, &t2987_0_0_0, &t2987_1_0_0, t2987_IOs, &t2987_GC, NULL, NULL, NULL, t2987_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2987)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t2990MD.h"
extern MethodInfo m16335_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m16367_MI;
extern MethodInfo m29636_MI;
extern MethodInfo m29640_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody2D>
extern Il2CppType t2989_0_0_1;
FieldInfo t2984_f0_FieldInfo = 
{
	"list", &t2989_0_0_1, &t2984_TI, offsetof(t2984, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2984_FIs[] =
{
	&t2984_f0_FieldInfo,
	NULL
};
extern MethodInfo m16312_MI;
extern MethodInfo m16313_MI;
static PropertyInfo t2984____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2984_TI, "System.Collections.Generic.IList<T>.Item", &m16312_MI, &m16313_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16314_MI;
static PropertyInfo t2984____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2984_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m16314_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16324_MI;
static PropertyInfo t2984____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2984_TI, "System.Collections.ICollection.IsSynchronized", &m16324_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16325_MI;
static PropertyInfo t2984____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2984_TI, "System.Collections.ICollection.SyncRoot", &m16325_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16326_MI;
static PropertyInfo t2984____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2984_TI, "System.Collections.IList.IsFixedSize", &m16326_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16327_MI;
static PropertyInfo t2984____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2984_TI, "System.Collections.IList.IsReadOnly", &m16327_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16328_MI;
extern MethodInfo m16329_MI;
static PropertyInfo t2984____System_Collections_IList_Item_PropertyInfo = 
{
	&t2984_TI, "System.Collections.IList.Item", &m16328_MI, &m16329_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16334_MI;
static PropertyInfo t2984____Count_PropertyInfo = 
{
	&t2984_TI, "Count", &m16334_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2984____Item_PropertyInfo = 
{
	&t2984_TI, "Item", &m16335_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2984_PIs[] =
{
	&t2984____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2984____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2984____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2984____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2984____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2984____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2984____System_Collections_IList_Item_PropertyInfo,
	&t2984____Count_PropertyInfo,
	&t2984____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2989_0_0_0;
static ParameterInfo t2984_m16306_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2989_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16306_GM;
MethodInfo m16306_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2984_m16306_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16306_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2984_m16307_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16307_GM;
MethodInfo m16307_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2984_m16307_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16307_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16308_GM;
MethodInfo m16308_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16308_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2984_m16309_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16309_GM;
MethodInfo m16309_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2984_m16309_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16309_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2984_m16310_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16310_GM;
MethodInfo m16310_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2984_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2984_m16310_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16310_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2984_m16311_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16311_GM;
MethodInfo m16311_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2984_m16311_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16311_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2984_m16312_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t507_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16312_GM;
MethodInfo m16312_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2984_TI, &t507_0_0_0, RuntimeInvoker_t29_t44, t2984_m16312_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16312_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2984_m16313_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16313_GM;
MethodInfo m16313_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2984_m16313_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16313_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16314_GM;
MethodInfo m16314_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2984_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16314_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2984_m16315_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16315_GM;
MethodInfo m16315_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2984_m16315_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16315_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16316_GM;
MethodInfo m16316_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2984_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16316_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2984_m16317_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16317_GM;
MethodInfo m16317_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2984_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2984_m16317_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16317_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16318_GM;
MethodInfo m16318_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16318_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2984_m16319_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16319_GM;
MethodInfo m16319_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2984_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2984_m16319_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16319_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2984_m16320_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16320_GM;
MethodInfo m16320_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2984_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2984_m16320_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16320_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2984_m16321_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16321_GM;
MethodInfo m16321_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2984_m16321_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16321_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2984_m16322_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16322_GM;
MethodInfo m16322_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2984_m16322_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16322_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2984_m16323_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16323_GM;
MethodInfo m16323_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2984_m16323_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16323_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16324_GM;
MethodInfo m16324_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2984_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16324_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16325_GM;
MethodInfo m16325_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2984_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16325_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16326_GM;
MethodInfo m16326_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2984_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16326_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16327_GM;
MethodInfo m16327_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2984_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16327_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2984_m16328_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16328_GM;
MethodInfo m16328_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2984_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2984_m16328_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16328_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2984_m16329_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16329_GM;
MethodInfo m16329_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2984_m16329_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16329_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2984_m16330_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16330_GM;
MethodInfo m16330_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2984_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2984_m16330_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16330_GM};
extern Il2CppType t2980_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2984_m16331_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2980_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16331_GM;
MethodInfo m16331_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2984_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2984_m16331_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16331_GM};
extern Il2CppType t2981_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16332_GM;
MethodInfo m16332_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2984_TI, &t2981_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16332_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2984_m16333_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16333_GM;
MethodInfo m16333_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2984_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2984_m16333_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16333_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16334_GM;
MethodInfo m16334_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2984_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16334_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2984_m16335_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t507_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16335_GM;
MethodInfo m16335_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2984_TI, &t507_0_0_0, RuntimeInvoker_t29_t44, t2984_m16335_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16335_GM};
static MethodInfo* t2984_MIs[] =
{
	&m16306_MI,
	&m16307_MI,
	&m16308_MI,
	&m16309_MI,
	&m16310_MI,
	&m16311_MI,
	&m16312_MI,
	&m16313_MI,
	&m16314_MI,
	&m16315_MI,
	&m16316_MI,
	&m16317_MI,
	&m16318_MI,
	&m16319_MI,
	&m16320_MI,
	&m16321_MI,
	&m16322_MI,
	&m16323_MI,
	&m16324_MI,
	&m16325_MI,
	&m16326_MI,
	&m16327_MI,
	&m16328_MI,
	&m16329_MI,
	&m16330_MI,
	&m16331_MI,
	&m16332_MI,
	&m16333_MI,
	&m16334_MI,
	&m16335_MI,
	NULL
};
extern MethodInfo m16316_MI;
extern MethodInfo m16315_MI;
extern MethodInfo m16317_MI;
extern MethodInfo m16318_MI;
extern MethodInfo m16319_MI;
extern MethodInfo m16320_MI;
extern MethodInfo m16321_MI;
extern MethodInfo m16322_MI;
extern MethodInfo m16323_MI;
extern MethodInfo m16307_MI;
extern MethodInfo m16308_MI;
extern MethodInfo m16330_MI;
extern MethodInfo m16331_MI;
extern MethodInfo m16310_MI;
extern MethodInfo m16333_MI;
extern MethodInfo m16309_MI;
extern MethodInfo m16311_MI;
extern MethodInfo m16332_MI;
static MethodInfo* t2984_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16316_MI,
	&m16334_MI,
	&m16324_MI,
	&m16325_MI,
	&m16315_MI,
	&m16326_MI,
	&m16327_MI,
	&m16328_MI,
	&m16329_MI,
	&m16317_MI,
	&m16318_MI,
	&m16319_MI,
	&m16320_MI,
	&m16321_MI,
	&m16322_MI,
	&m16323_MI,
	&m16334_MI,
	&m16314_MI,
	&m16307_MI,
	&m16308_MI,
	&m16330_MI,
	&m16331_MI,
	&m16310_MI,
	&m16333_MI,
	&m16309_MI,
	&m16311_MI,
	&m16312_MI,
	&m16313_MI,
	&m16332_MI,
	&m16335_MI,
};
static TypeInfo* t2984_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2982_TI,
	&t2989_TI,
	&t2983_TI,
};
static Il2CppInterfaceOffsetPair t2984_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2982_TI, 20},
	{ &t2989_TI, 27},
	{ &t2983_TI, 32},
};
extern TypeInfo t507_TI;
static Il2CppRGCTXData t2984_RGCTXData[9] = 
{
	&m16335_MI/* Method Usage */,
	&m16367_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
	&m29636_MI/* Method Usage */,
	&m29640_MI/* Method Usage */,
	&m29638_MI/* Method Usage */,
	&m29630_MI/* Method Usage */,
	&m29631_MI/* Method Usage */,
	&m29629_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2984_0_0_0;
extern Il2CppType t2984_1_0_0;
struct t2984;
extern Il2CppGenericClass t2984_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2984_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2984_MIs, t2984_PIs, t2984_FIs, NULL, &t29_TI, NULL, NULL, &t2984_TI, t2984_ITIs, t2984_VT, &t1263__CustomAttributeCache, &t2984_TI, &t2984_0_0_0, &t2984_1_0_0, t2984_IOs, &t2984_GC, NULL, NULL, NULL, t2984_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2984), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2990.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2990_TI;

extern MethodInfo m16370_MI;
extern MethodInfo m16371_MI;
extern MethodInfo m16368_MI;
extern MethodInfo m16366_MI;
extern MethodInfo m2901_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m16359_MI;
extern MethodInfo m16369_MI;
extern MethodInfo m16357_MI;
extern MethodInfo m16362_MI;
extern MethodInfo m16353_MI;
extern MethodInfo m29635_MI;
extern MethodInfo m29641_MI;
extern MethodInfo m29642_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.Rigidbody2D>
extern Il2CppType t2989_0_0_1;
FieldInfo t2990_f0_FieldInfo = 
{
	"list", &t2989_0_0_1, &t2990_TI, offsetof(t2990, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2990_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2990_TI, offsetof(t2990, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2990_FIs[] =
{
	&t2990_f0_FieldInfo,
	&t2990_f1_FieldInfo,
	NULL
};
extern MethodInfo m16337_MI;
static PropertyInfo t2990____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2990_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m16337_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16345_MI;
static PropertyInfo t2990____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2990_TI, "System.Collections.ICollection.IsSynchronized", &m16345_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16346_MI;
static PropertyInfo t2990____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2990_TI, "System.Collections.ICollection.SyncRoot", &m16346_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16347_MI;
static PropertyInfo t2990____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2990_TI, "System.Collections.IList.IsFixedSize", &m16347_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16348_MI;
static PropertyInfo t2990____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2990_TI, "System.Collections.IList.IsReadOnly", &m16348_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16349_MI;
extern MethodInfo m16350_MI;
static PropertyInfo t2990____System_Collections_IList_Item_PropertyInfo = 
{
	&t2990_TI, "System.Collections.IList.Item", &m16349_MI, &m16350_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16363_MI;
static PropertyInfo t2990____Count_PropertyInfo = 
{
	&t2990_TI, "Count", &m16363_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m16364_MI;
extern MethodInfo m16365_MI;
static PropertyInfo t2990____Item_PropertyInfo = 
{
	&t2990_TI, "Item", &m16364_MI, &m16365_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2990_PIs[] =
{
	&t2990____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2990____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2990____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2990____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2990____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2990____System_Collections_IList_Item_PropertyInfo,
	&t2990____Count_PropertyInfo,
	&t2990____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16336_GM;
MethodInfo m16336_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16336_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16337_GM;
MethodInfo m16337_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2990_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16337_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2990_m16338_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16338_GM;
MethodInfo m16338_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2990_m16338_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16338_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16339_GM;
MethodInfo m16339_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2990_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16339_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2990_m16340_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16340_GM;
MethodInfo m16340_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2990_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2990_m16340_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16340_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2990_m16341_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16341_GM;
MethodInfo m16341_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2990_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2990_m16341_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16341_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2990_m16342_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16342_GM;
MethodInfo m16342_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2990_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2990_m16342_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16342_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2990_m16343_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16343_GM;
MethodInfo m16343_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2990_m16343_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16343_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2990_m16344_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16344_GM;
MethodInfo m16344_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2990_m16344_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16344_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16345_GM;
MethodInfo m16345_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2990_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16345_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16346_GM;
MethodInfo m16346_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2990_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16346_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16347_GM;
MethodInfo m16347_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2990_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16347_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16348_GM;
MethodInfo m16348_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2990_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16348_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2990_m16349_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16349_GM;
MethodInfo m16349_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2990_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2990_m16349_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16349_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2990_m16350_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16350_GM;
MethodInfo m16350_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2990_m16350_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16350_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2990_m16351_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16351_GM;
MethodInfo m16351_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2990_m16351_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16351_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16352_GM;
MethodInfo m16352_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16352_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16353_GM;
MethodInfo m16353_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16353_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2990_m16354_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16354_GM;
MethodInfo m16354_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2990_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2990_m16354_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16354_GM};
extern Il2CppType t2980_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2990_m16355_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2980_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16355_GM;
MethodInfo m16355_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2990_m16355_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16355_GM};
extern Il2CppType t2981_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16356_GM;
MethodInfo m16356_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2990_TI, &t2981_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16356_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2990_m16357_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16357_GM;
MethodInfo m16357_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2990_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2990_m16357_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16357_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2990_m16358_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16358_GM;
MethodInfo m16358_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2990_m16358_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16358_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2990_m16359_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16359_GM;
MethodInfo m16359_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2990_m16359_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16359_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2990_m16360_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16360_GM;
MethodInfo m16360_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2990_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2990_m16360_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16360_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2990_m16361_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16361_GM;
MethodInfo m16361_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2990_m16361_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16361_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2990_m16362_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16362_GM;
MethodInfo m16362_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2990_m16362_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16362_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16363_GM;
MethodInfo m16363_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2990_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16363_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2990_m16364_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t507_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16364_GM;
MethodInfo m16364_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2990_TI, &t507_0_0_0, RuntimeInvoker_t29_t44, t2990_m16364_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16364_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2990_m16365_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16365_GM;
MethodInfo m16365_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2990_m16365_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16365_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2990_m16366_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16366_GM;
MethodInfo m16366_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2990_m16366_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16366_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2990_m16367_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16367_GM;
MethodInfo m16367_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2990_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2990_m16367_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16367_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2990_m16368_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t507_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16368_GM;
MethodInfo m16368_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2990_TI, &t507_0_0_0, RuntimeInvoker_t29_t29, t2990_m16368_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16368_GM};
extern Il2CppType t2989_0_0_0;
static ParameterInfo t2990_m16369_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2989_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16369_GM;
MethodInfo m16369_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2990_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2990_m16369_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16369_GM};
extern Il2CppType t2989_0_0_0;
static ParameterInfo t2990_m16370_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2989_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16370_GM;
MethodInfo m16370_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2990_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2990_m16370_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16370_GM};
extern Il2CppType t2989_0_0_0;
static ParameterInfo t2990_m16371_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2989_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16371_GM;
MethodInfo m16371_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2990_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2990_m16371_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16371_GM};
static MethodInfo* t2990_MIs[] =
{
	&m16336_MI,
	&m16337_MI,
	&m16338_MI,
	&m16339_MI,
	&m16340_MI,
	&m16341_MI,
	&m16342_MI,
	&m16343_MI,
	&m16344_MI,
	&m16345_MI,
	&m16346_MI,
	&m16347_MI,
	&m16348_MI,
	&m16349_MI,
	&m16350_MI,
	&m16351_MI,
	&m16352_MI,
	&m16353_MI,
	&m16354_MI,
	&m16355_MI,
	&m16356_MI,
	&m16357_MI,
	&m16358_MI,
	&m16359_MI,
	&m16360_MI,
	&m16361_MI,
	&m16362_MI,
	&m16363_MI,
	&m16364_MI,
	&m16365_MI,
	&m16366_MI,
	&m16367_MI,
	&m16368_MI,
	&m16369_MI,
	&m16370_MI,
	&m16371_MI,
	NULL
};
extern MethodInfo m16339_MI;
extern MethodInfo m16338_MI;
extern MethodInfo m16340_MI;
extern MethodInfo m16352_MI;
extern MethodInfo m16341_MI;
extern MethodInfo m16342_MI;
extern MethodInfo m16343_MI;
extern MethodInfo m16344_MI;
extern MethodInfo m16361_MI;
extern MethodInfo m16351_MI;
extern MethodInfo m16354_MI;
extern MethodInfo m16355_MI;
extern MethodInfo m16360_MI;
extern MethodInfo m16358_MI;
extern MethodInfo m16356_MI;
static MethodInfo* t2990_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16339_MI,
	&m16363_MI,
	&m16345_MI,
	&m16346_MI,
	&m16338_MI,
	&m16347_MI,
	&m16348_MI,
	&m16349_MI,
	&m16350_MI,
	&m16340_MI,
	&m16352_MI,
	&m16341_MI,
	&m16342_MI,
	&m16343_MI,
	&m16344_MI,
	&m16361_MI,
	&m16363_MI,
	&m16337_MI,
	&m16351_MI,
	&m16352_MI,
	&m16354_MI,
	&m16355_MI,
	&m16360_MI,
	&m16357_MI,
	&m16358_MI,
	&m16361_MI,
	&m16364_MI,
	&m16365_MI,
	&m16356_MI,
	&m16353_MI,
	&m16359_MI,
	&m16362_MI,
	&m16366_MI,
};
static TypeInfo* t2990_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2982_TI,
	&t2989_TI,
	&t2983_TI,
};
static Il2CppInterfaceOffsetPair t2990_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2982_TI, 20},
	{ &t2989_TI, 27},
	{ &t2983_TI, 32},
};
extern TypeInfo t506_TI;
extern TypeInfo t507_TI;
static Il2CppRGCTXData t2990_RGCTXData[25] = 
{
	&t506_TI/* Class Usage */,
	&m2901_MI/* Method Usage */,
	&m29633_MI/* Method Usage */,
	&m29631_MI/* Method Usage */,
	&m29629_MI/* Method Usage */,
	&m16368_MI/* Method Usage */,
	&m16359_MI/* Method Usage */,
	&m16367_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
	&m29636_MI/* Method Usage */,
	&m29640_MI/* Method Usage */,
	&m16369_MI/* Method Usage */,
	&m16357_MI/* Method Usage */,
	&m16362_MI/* Method Usage */,
	&m16370_MI/* Method Usage */,
	&m16371_MI/* Method Usage */,
	&m29638_MI/* Method Usage */,
	&m16366_MI/* Method Usage */,
	&m16353_MI/* Method Usage */,
	&m29635_MI/* Method Usage */,
	&m29630_MI/* Method Usage */,
	&m29641_MI/* Method Usage */,
	&m29642_MI/* Method Usage */,
	&m29639_MI/* Method Usage */,
	&t507_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2990_0_0_0;
extern Il2CppType t2990_1_0_0;
struct t2990;
extern Il2CppGenericClass t2990_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2990_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2990_MIs, t2990_PIs, t2990_FIs, NULL, &t29_TI, NULL, NULL, &t2990_TI, t2990_ITIs, t2990_VT, &t1262__CustomAttributeCache, &t2990_TI, &t2990_0_0_0, &t2990_1_0_0, t2990_IOs, &t2990_GC, NULL, NULL, NULL, t2990_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2990), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2991_TI;
#include "t2991MD.h"

#include "t1257.h"
#include "t2992.h"
extern TypeInfo t6734_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2992_TI;
#include "t931MD.h"
#include "t2992MD.h"
extern Il2CppType t6734_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m16377_MI;
extern MethodInfo m29643_MI;
extern MethodInfo m22522_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.Rigidbody2D>
extern Il2CppType t2991_0_0_49;
FieldInfo t2991_f0_FieldInfo = 
{
	"_default", &t2991_0_0_49, &t2991_TI, offsetof(t2991_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2991_FIs[] =
{
	&t2991_f0_FieldInfo,
	NULL
};
extern MethodInfo m16376_MI;
static PropertyInfo t2991____Default_PropertyInfo = 
{
	&t2991_TI, "Default", &m16376_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2991_PIs[] =
{
	&t2991____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16372_GM;
MethodInfo m16372_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2991_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16372_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16373_GM;
MethodInfo m16373_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2991_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16373_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2991_m16374_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16374_GM;
MethodInfo m16374_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2991_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2991_m16374_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16374_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2991_m16375_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16375_GM;
MethodInfo m16375_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2991_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2991_m16375_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16375_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2991_m29643_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29643_GM;
MethodInfo m29643_MI = 
{
	"GetHashCode", NULL, &t2991_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2991_m29643_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29643_GM};
extern Il2CppType t507_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2991_m22522_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22522_GM;
MethodInfo m22522_MI = 
{
	"Equals", NULL, &t2991_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2991_m22522_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m22522_GM};
extern Il2CppType t2991_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16376_GM;
MethodInfo m16376_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2991_TI, &t2991_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16376_GM};
static MethodInfo* t2991_MIs[] =
{
	&m16372_MI,
	&m16373_MI,
	&m16374_MI,
	&m16375_MI,
	&m29643_MI,
	&m22522_MI,
	&m16376_MI,
	NULL
};
extern MethodInfo m16375_MI;
extern MethodInfo m16374_MI;
static MethodInfo* t2991_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m22522_MI,
	&m29643_MI,
	&m16375_MI,
	&m16374_MI,
	NULL,
	NULL,
};
extern TypeInfo t6735_TI;
extern TypeInfo t734_TI;
static TypeInfo* t2991_ITIs[] = 
{
	&t6735_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2991_IOs[] = 
{
	{ &t6735_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2991_TI;
extern TypeInfo t2991_TI;
extern TypeInfo t2992_TI;
extern TypeInfo t507_TI;
static Il2CppRGCTXData t2991_RGCTXData[9] = 
{
	&t6734_0_0_0/* Type Usage */,
	&t507_0_0_0/* Type Usage */,
	&t2991_TI/* Class Usage */,
	&t2991_TI/* Static Usage */,
	&t2992_TI/* Class Usage */,
	&m16377_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
	&m29643_MI/* Method Usage */,
	&m22522_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2991_0_0_0;
extern Il2CppType t2991_1_0_0;
struct t2991;
extern Il2CppGenericClass t2991_GC;
TypeInfo t2991_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2991_MIs, t2991_PIs, t2991_FIs, NULL, &t29_TI, NULL, NULL, &t2991_TI, t2991_ITIs, t2991_VT, &EmptyCustomAttributesCache, &t2991_TI, &t2991_0_0_0, &t2991_1_0_0, t2991_IOs, &t2991_GC, NULL, NULL, NULL, t2991_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2991), 0, -1, sizeof(t2991_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rigidbody2D>
extern Il2CppType t507_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t6735_m29644_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29644_GM;
MethodInfo m29644_MI = 
{
	"Equals", NULL, &t6735_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6735_m29644_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29644_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t6735_m29645_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29645_GM;
MethodInfo m29645_MI = 
{
	"GetHashCode", NULL, &t6735_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6735_m29645_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29645_GM};
static MethodInfo* t6735_MIs[] =
{
	&m29644_MI,
	&m29645_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6735_0_0_0;
extern Il2CppType t6735_1_0_0;
struct t6735;
extern Il2CppGenericClass t6735_GC;
TypeInfo t6735_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6735_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6735_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6735_TI, &t6735_0_0_0, &t6735_1_0_0, NULL, &t6735_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.Rigidbody2D>
extern Il2CppType t507_0_0_0;
static ParameterInfo t6734_m29646_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29646_GM;
MethodInfo m29646_MI = 
{
	"Equals", NULL, &t6734_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6734_m29646_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29646_GM};
static MethodInfo* t6734_MIs[] =
{
	&m29646_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6734_1_0_0;
struct t6734;
extern Il2CppGenericClass t6734_GC;
TypeInfo t6734_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6734_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6734_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6734_TI, &t6734_0_0_0, &t6734_1_0_0, NULL, &t6734_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m16372_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Rigidbody2D>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16377_GM;
MethodInfo m16377_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2992_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16377_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2992_m16378_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16378_GM;
MethodInfo m16378_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2992_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2992_m16378_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16378_GM};
extern Il2CppType t507_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2992_m16379_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16379_GM;
MethodInfo m16379_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2992_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2992_m16379_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16379_GM};
static MethodInfo* t2992_MIs[] =
{
	&m16377_MI,
	&m16378_MI,
	&m16379_MI,
	NULL
};
extern MethodInfo m16379_MI;
extern MethodInfo m16378_MI;
static MethodInfo* t2992_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16379_MI,
	&m16378_MI,
	&m16375_MI,
	&m16374_MI,
	&m16378_MI,
	&m16379_MI,
};
static Il2CppInterfaceOffsetPair t2992_IOs[] = 
{
	{ &t6735_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2991_TI;
extern TypeInfo t2991_TI;
extern TypeInfo t2992_TI;
extern TypeInfo t507_TI;
extern TypeInfo t507_TI;
static Il2CppRGCTXData t2992_RGCTXData[11] = 
{
	&t6734_0_0_0/* Type Usage */,
	&t507_0_0_0/* Type Usage */,
	&t2991_TI/* Class Usage */,
	&t2991_TI/* Static Usage */,
	&t2992_TI/* Class Usage */,
	&m16377_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
	&m29643_MI/* Method Usage */,
	&m22522_MI/* Method Usage */,
	&m16372_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2992_0_0_0;
extern Il2CppType t2992_1_0_0;
struct t2992;
extern Il2CppGenericClass t2992_GC;
extern TypeInfo t1256_TI;
TypeInfo t2992_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2992_MIs, NULL, NULL, NULL, &t2991_TI, NULL, &t1256_TI, &t2992_TI, NULL, t2992_VT, &EmptyCustomAttributesCache, &t2992_TI, &t2992_0_0_0, &t2992_1_0_0, t2992_IOs, &t2992_GC, NULL, NULL, NULL, t2992_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2992), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.Rigidbody2D>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2985_m16380_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16380_GM;
MethodInfo m16380_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2985_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2985_m16380_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16380_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2985_m16381_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16381_GM;
MethodInfo m16381_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2985_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2985_m16381_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16381_GM};
extern Il2CppType t507_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2985_m16382_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16382_GM;
MethodInfo m16382_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2985_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2985_m16382_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16382_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2985_m16383_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16383_GM;
MethodInfo m16383_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2985_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2985_m16383_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16383_GM};
static MethodInfo* t2985_MIs[] =
{
	&m16380_MI,
	&m16381_MI,
	&m16382_MI,
	&m16383_MI,
	NULL
};
extern MethodInfo m16382_MI;
extern MethodInfo m16383_MI;
static MethodInfo* t2985_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16381_MI,
	&m16382_MI,
	&m16383_MI,
};
static Il2CppInterfaceOffsetPair t2985_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2985_1_0_0;
struct t2985;
extern Il2CppGenericClass t2985_GC;
TypeInfo t2985_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2985_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2985_TI, NULL, t2985_VT, &EmptyCustomAttributesCache, &t2985_TI, &t2985_0_0_0, &t2985_1_0_0, t2985_IOs, &t2985_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2985), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t2994.h"
extern TypeInfo t4416_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t2994_TI;
#include "t2994MD.h"
extern Il2CppType t4416_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m16388_MI;
extern MethodInfo m29647_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.Rigidbody2D>
extern Il2CppType t2993_0_0_49;
FieldInfo t2993_f0_FieldInfo = 
{
	"_default", &t2993_0_0_49, &t2993_TI, offsetof(t2993_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2993_FIs[] =
{
	&t2993_f0_FieldInfo,
	NULL
};
static PropertyInfo t2993____Default_PropertyInfo = 
{
	&t2993_TI, "Default", &m16387_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2993_PIs[] =
{
	&t2993____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16384_GM;
MethodInfo m16384_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2993_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16384_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16385_GM;
MethodInfo m16385_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2993_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16385_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2993_m16386_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16386_GM;
MethodInfo m16386_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2993_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2993_m16386_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16386_GM};
extern Il2CppType t507_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2993_m29647_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29647_GM;
MethodInfo m29647_MI = 
{
	"Compare", NULL, &t2993_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2993_m29647_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29647_GM};
extern Il2CppType t2993_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16387_GM;
MethodInfo m16387_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2993_TI, &t2993_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16387_GM};
static MethodInfo* t2993_MIs[] =
{
	&m16384_MI,
	&m16385_MI,
	&m16386_MI,
	&m29647_MI,
	&m16387_MI,
	NULL
};
extern MethodInfo m16386_MI;
static MethodInfo* t2993_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m29647_MI,
	&m16386_MI,
	NULL,
};
extern TypeInfo t4415_TI;
extern TypeInfo t726_TI;
static TypeInfo* t2993_ITIs[] = 
{
	&t4415_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2993_IOs[] = 
{
	{ &t4415_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2993_TI;
extern TypeInfo t2993_TI;
extern TypeInfo t2994_TI;
extern TypeInfo t507_TI;
static Il2CppRGCTXData t2993_RGCTXData[8] = 
{
	&t4416_0_0_0/* Type Usage */,
	&t507_0_0_0/* Type Usage */,
	&t2993_TI/* Class Usage */,
	&t2993_TI/* Static Usage */,
	&t2994_TI/* Class Usage */,
	&m16388_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
	&m29647_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2993_0_0_0;
extern Il2CppType t2993_1_0_0;
struct t2993;
extern Il2CppGenericClass t2993_GC;
TypeInfo t2993_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2993_MIs, t2993_PIs, t2993_FIs, NULL, &t29_TI, NULL, NULL, &t2993_TI, t2993_ITIs, t2993_VT, &EmptyCustomAttributesCache, &t2993_TI, &t2993_0_0_0, &t2993_1_0_0, t2993_IOs, &t2993_GC, NULL, NULL, NULL, t2993_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2993), 0, -1, sizeof(t2993_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.Rigidbody2D>
extern Il2CppType t507_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t4415_m22530_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22530_GM;
MethodInfo m22530_MI = 
{
	"Compare", NULL, &t4415_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4415_m22530_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m22530_GM};
static MethodInfo* t4415_MIs[] =
{
	&m22530_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4415_0_0_0;
extern Il2CppType t4415_1_0_0;
struct t4415;
extern Il2CppGenericClass t4415_GC;
TypeInfo t4415_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4415_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4415_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4415_TI, &t4415_0_0_0, &t4415_1_0_0, NULL, &t4415_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.Rigidbody2D>
extern Il2CppType t507_0_0_0;
static ParameterInfo t4416_m22531_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m22531_GM;
MethodInfo m22531_MI = 
{
	"CompareTo", NULL, &t4416_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4416_m22531_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m22531_GM};
static MethodInfo* t4416_MIs[] =
{
	&m22531_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4416_1_0_0;
struct t4416;
extern Il2CppGenericClass t4416_GC;
TypeInfo t4416_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4416_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4416_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4416_TI, &t4416_0_0_0, &t4416_1_0_0, NULL, &t4416_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m16384_MI;
extern MethodInfo m22531_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Rigidbody2D>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16388_GM;
MethodInfo m16388_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2994_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16388_GM};
extern Il2CppType t507_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2994_m16389_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16389_GM;
MethodInfo m16389_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2994_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2994_m16389_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16389_GM};
static MethodInfo* t2994_MIs[] =
{
	&m16388_MI,
	&m16389_MI,
	NULL
};
extern MethodInfo m16389_MI;
static MethodInfo* t2994_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16389_MI,
	&m16386_MI,
	&m16389_MI,
};
static Il2CppInterfaceOffsetPair t2994_IOs[] = 
{
	{ &t4415_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2993_TI;
extern TypeInfo t2993_TI;
extern TypeInfo t2994_TI;
extern TypeInfo t507_TI;
extern TypeInfo t507_TI;
extern TypeInfo t4416_TI;
static Il2CppRGCTXData t2994_RGCTXData[12] = 
{
	&t4416_0_0_0/* Type Usage */,
	&t507_0_0_0/* Type Usage */,
	&t2993_TI/* Class Usage */,
	&t2993_TI/* Static Usage */,
	&t2994_TI/* Class Usage */,
	&m16388_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
	&m29647_MI/* Method Usage */,
	&m16384_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
	&t4416_TI/* Class Usage */,
	&m22531_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2994_0_0_0;
extern Il2CppType t2994_1_0_0;
struct t2994;
extern Il2CppGenericClass t2994_GC;
extern TypeInfo t1246_TI;
TypeInfo t2994_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2994_MIs, NULL, NULL, NULL, &t2993_TI, NULL, &t1246_TI, &t2994_TI, NULL, t2994_VT, &EmptyCustomAttributesCache, &t2994_TI, &t2994_0_0_0, &t2994_1_0_0, t2994_IOs, &t2994_GC, NULL, NULL, NULL, t2994_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2994), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2986_TI;
#include "t2986MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.Rigidbody2D>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2986_m16390_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16390_GM;
MethodInfo m16390_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2986_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2986_m16390_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16390_GM};
extern Il2CppType t507_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2986_m16391_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16391_GM;
MethodInfo m16391_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2986_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2986_m16391_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16391_GM};
extern Il2CppType t507_0_0_0;
extern Il2CppType t507_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2986_m16392_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16392_GM;
MethodInfo m16392_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2986_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2986_m16392_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m16392_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2986_m16393_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16393_GM;
MethodInfo m16393_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2986_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2986_m16393_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16393_GM};
static MethodInfo* t2986_MIs[] =
{
	&m16390_MI,
	&m16391_MI,
	&m16392_MI,
	&m16393_MI,
	NULL
};
extern MethodInfo m16391_MI;
extern MethodInfo m16392_MI;
extern MethodInfo m16393_MI;
static MethodInfo* t2986_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16391_MI,
	&m16392_MI,
	&m16393_MI,
};
static Il2CppInterfaceOffsetPair t2986_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2986_1_0_0;
struct t2986;
extern Il2CppGenericClass t2986_GC;
TypeInfo t2986_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2986_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2986_TI, NULL, t2986_VT, &EmptyCustomAttributesCache, &t2986_TI, &t2986_0_0_0, &t2986_1_0_0, t2986_IOs, &t2986_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2986), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2995.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2995_TI;
#include "t2995MD.h"

#include "t2996.h"
extern TypeInfo t2996_TI;
#include "t2996MD.h"
extern MethodInfo m16396_MI;
extern MethodInfo m16398_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Rigidbody2D>
extern Il2CppType t316_0_0_33;
FieldInfo t2995_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2995_TI, offsetof(t2995, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2995_FIs[] =
{
	&t2995_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t507_0_0_0;
static ParameterInfo t2995_m16394_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16394_GM;
MethodInfo m16394_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2995_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2995_m16394_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16394_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2995_m16395_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16395_GM;
MethodInfo m16395_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2995_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2995_m16395_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16395_GM};
static MethodInfo* t2995_MIs[] =
{
	&m16394_MI,
	&m16395_MI,
	NULL
};
extern MethodInfo m16395_MI;
extern MethodInfo m16399_MI;
static MethodInfo* t2995_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16395_MI,
	&m16399_MI,
};
extern Il2CppType t2997_0_0_0;
extern TypeInfo t2997_TI;
extern MethodInfo m22535_MI;
extern TypeInfo t507_TI;
extern MethodInfo m16401_MI;
extern TypeInfo t507_TI;
static Il2CppRGCTXData t2995_RGCTXData[8] = 
{
	&t2997_0_0_0/* Type Usage */,
	&t2997_TI/* Class Usage */,
	&m22535_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
	&m16401_MI/* Method Usage */,
	&m16396_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
	&m16398_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2995_0_0_0;
extern Il2CppType t2995_1_0_0;
struct t2995;
extern Il2CppGenericClass t2995_GC;
TypeInfo t2995_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2995_MIs, NULL, t2995_FIs, NULL, &t2996_TI, NULL, NULL, &t2995_TI, NULL, t2995_VT, &EmptyCustomAttributesCache, &t2995_TI, &t2995_0_0_0, &t2995_1_0_0, NULL, &t2995_GC, NULL, NULL, NULL, t2995_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2995), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2997.h"
extern TypeInfo t2997_TI;
#include "t2997MD.h"
struct t556;
#define m22535(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody2D>
extern Il2CppType t2997_0_0_1;
FieldInfo t2996_f0_FieldInfo = 
{
	"Delegate", &t2997_0_0_1, &t2996_TI, offsetof(t2996, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2996_FIs[] =
{
	&t2996_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2996_m16396_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16396_GM;
MethodInfo m16396_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2996_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2996_m16396_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16396_GM};
extern Il2CppType t2997_0_0_0;
static ParameterInfo t2996_m16397_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2997_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16397_GM;
MethodInfo m16397_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2996_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2996_m16397_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16397_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2996_m16398_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16398_GM;
MethodInfo m16398_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2996_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2996_m16398_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16398_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2996_m16399_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16399_GM;
MethodInfo m16399_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2996_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2996_m16399_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16399_GM};
static MethodInfo* t2996_MIs[] =
{
	&m16396_MI,
	&m16397_MI,
	&m16398_MI,
	&m16399_MI,
	NULL
};
static MethodInfo* t2996_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16398_MI,
	&m16399_MI,
};
extern TypeInfo t2997_TI;
extern TypeInfo t507_TI;
static Il2CppRGCTXData t2996_RGCTXData[5] = 
{
	&t2997_0_0_0/* Type Usage */,
	&t2997_TI/* Class Usage */,
	&m22535_MI/* Method Usage */,
	&t507_TI/* Class Usage */,
	&m16401_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2996_0_0_0;
extern Il2CppType t2996_1_0_0;
struct t2996;
extern Il2CppGenericClass t2996_GC;
TypeInfo t2996_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2996_MIs, NULL, t2996_FIs, NULL, &t556_TI, NULL, NULL, &t2996_TI, NULL, t2996_VT, &EmptyCustomAttributesCache, &t2996_TI, &t2996_0_0_0, &t2996_1_0_0, NULL, &t2996_GC, NULL, NULL, NULL, t2996_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2996), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody2D>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2997_m16400_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16400_GM;
MethodInfo m16400_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2997_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2997_m16400_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16400_GM};
extern Il2CppType t507_0_0_0;
static ParameterInfo t2997_m16401_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16401_GM;
MethodInfo m16401_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2997_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2997_m16401_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16401_GM};
extern Il2CppType t507_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2997_m16402_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t507_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16402_GM;
MethodInfo m16402_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2997_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2997_m16402_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16402_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2997_m16403_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16403_GM;
MethodInfo m16403_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2997_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2997_m16403_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16403_GM};
static MethodInfo* t2997_MIs[] =
{
	&m16400_MI,
	&m16401_MI,
	&m16402_MI,
	&m16403_MI,
	NULL
};
extern MethodInfo m16402_MI;
extern MethodInfo m16403_MI;
static MethodInfo* t2997_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16401_MI,
	&m16402_MI,
	&m16403_MI,
};
static Il2CppInterfaceOffsetPair t2997_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2997_1_0_0;
struct t2997;
extern Il2CppGenericClass t2997_GC;
TypeInfo t2997_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2997_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2997_TI, NULL, t2997_VT, &EmptyCustomAttributesCache, &t2997_TI, &t2997_0_0_0, &t2997_1_0_0, t2997_IOs, &t2997_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2997), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4418_TI;

#include "t332.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Collider2D>
extern MethodInfo m29648_MI;
static PropertyInfo t4418____Current_PropertyInfo = 
{
	&t4418_TI, "Current", &m29648_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4418_PIs[] =
{
	&t4418____Current_PropertyInfo,
	NULL
};
extern Il2CppType t332_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29648_GM;
MethodInfo m29648_MI = 
{
	"get_Current", NULL, &t4418_TI, &t332_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29648_GM};
static MethodInfo* t4418_MIs[] =
{
	&m29648_MI,
	NULL
};
static TypeInfo* t4418_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4418_0_0_0;
extern Il2CppType t4418_1_0_0;
struct t4418;
extern Il2CppGenericClass t4418_GC;
TypeInfo t4418_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4418_MIs, t4418_PIs, NULL, NULL, NULL, NULL, NULL, &t4418_TI, t4418_ITIs, NULL, &EmptyCustomAttributesCache, &t4418_TI, &t4418_0_0_0, &t4418_1_0_0, NULL, &t4418_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2998.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2998_TI;
#include "t2998MD.h"

extern TypeInfo t332_TI;
extern MethodInfo m16408_MI;
extern MethodInfo m22537_MI;
struct t20;
#define m22537(__this, p0, method) (t332 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Collider2D>
extern Il2CppType t20_0_0_1;
FieldInfo t2998_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2998_TI, offsetof(t2998, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2998_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2998_TI, offsetof(t2998, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2998_FIs[] =
{
	&t2998_f0_FieldInfo,
	&t2998_f1_FieldInfo,
	NULL
};
extern MethodInfo m16405_MI;
static PropertyInfo t2998____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2998_TI, "System.Collections.IEnumerator.Current", &m16405_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2998____Current_PropertyInfo = 
{
	&t2998_TI, "Current", &m16408_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2998_PIs[] =
{
	&t2998____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2998____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2998_m16404_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16404_GM;
MethodInfo m16404_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2998_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2998_m16404_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16404_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16405_GM;
MethodInfo m16405_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2998_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16405_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16406_GM;
MethodInfo m16406_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2998_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16406_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16407_GM;
MethodInfo m16407_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2998_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16407_GM};
extern Il2CppType t332_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16408_GM;
MethodInfo m16408_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2998_TI, &t332_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16408_GM};
static MethodInfo* t2998_MIs[] =
{
	&m16404_MI,
	&m16405_MI,
	&m16406_MI,
	&m16407_MI,
	&m16408_MI,
	NULL
};
extern MethodInfo m16407_MI;
extern MethodInfo m16406_MI;
static MethodInfo* t2998_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16405_MI,
	&m16407_MI,
	&m16406_MI,
	&m16408_MI,
};
static TypeInfo* t2998_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4418_TI,
};
static Il2CppInterfaceOffsetPair t2998_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4418_TI, 7},
};
extern TypeInfo t332_TI;
static Il2CppRGCTXData t2998_RGCTXData[3] = 
{
	&m16408_MI/* Method Usage */,
	&t332_TI/* Class Usage */,
	&m22537_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2998_0_0_0;
extern Il2CppType t2998_1_0_0;
extern Il2CppGenericClass t2998_GC;
TypeInfo t2998_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2998_MIs, t2998_PIs, t2998_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2998_TI, t2998_ITIs, t2998_VT, &EmptyCustomAttributesCache, &t2998_TI, &t2998_0_0_0, &t2998_1_0_0, t2998_IOs, &t2998_GC, NULL, NULL, NULL, t2998_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2998)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5667_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Collider2D>
extern MethodInfo m29649_MI;
static PropertyInfo t5667____Count_PropertyInfo = 
{
	&t5667_TI, "Count", &m29649_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29650_MI;
static PropertyInfo t5667____IsReadOnly_PropertyInfo = 
{
	&t5667_TI, "IsReadOnly", &m29650_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5667_PIs[] =
{
	&t5667____Count_PropertyInfo,
	&t5667____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29649_GM;
MethodInfo m29649_MI = 
{
	"get_Count", NULL, &t5667_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29649_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29650_GM;
MethodInfo m29650_MI = 
{
	"get_IsReadOnly", NULL, &t5667_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29650_GM};
extern Il2CppType t332_0_0_0;
extern Il2CppType t332_0_0_0;
static ParameterInfo t5667_m29651_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t332_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29651_GM;
MethodInfo m29651_MI = 
{
	"Add", NULL, &t5667_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5667_m29651_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29651_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29652_GM;
MethodInfo m29652_MI = 
{
	"Clear", NULL, &t5667_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29652_GM};
extern Il2CppType t332_0_0_0;
static ParameterInfo t5667_m29653_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t332_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29653_GM;
MethodInfo m29653_MI = 
{
	"Contains", NULL, &t5667_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5667_m29653_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29653_GM};
extern Il2CppType t3765_0_0_0;
extern Il2CppType t3765_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5667_m29654_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3765_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29654_GM;
MethodInfo m29654_MI = 
{
	"CopyTo", NULL, &t5667_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5667_m29654_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29654_GM};
extern Il2CppType t332_0_0_0;
static ParameterInfo t5667_m29655_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t332_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29655_GM;
MethodInfo m29655_MI = 
{
	"Remove", NULL, &t5667_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5667_m29655_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29655_GM};
static MethodInfo* t5667_MIs[] =
{
	&m29649_MI,
	&m29650_MI,
	&m29651_MI,
	&m29652_MI,
	&m29653_MI,
	&m29654_MI,
	&m29655_MI,
	NULL
};
extern TypeInfo t5669_TI;
static TypeInfo* t5667_ITIs[] = 
{
	&t603_TI,
	&t5669_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5667_0_0_0;
extern Il2CppType t5667_1_0_0;
struct t5667;
extern Il2CppGenericClass t5667_GC;
TypeInfo t5667_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5667_MIs, t5667_PIs, NULL, NULL, NULL, NULL, NULL, &t5667_TI, t5667_ITIs, NULL, &EmptyCustomAttributesCache, &t5667_TI, &t5667_0_0_0, &t5667_1_0_0, NULL, &t5667_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Collider2D>
extern Il2CppType t4418_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29656_GM;
MethodInfo m29656_MI = 
{
	"GetEnumerator", NULL, &t5669_TI, &t4418_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29656_GM};
static MethodInfo* t5669_MIs[] =
{
	&m29656_MI,
	NULL
};
static TypeInfo* t5669_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5669_0_0_0;
extern Il2CppType t5669_1_0_0;
struct t5669;
extern Il2CppGenericClass t5669_GC;
TypeInfo t5669_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5669_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5669_TI, t5669_ITIs, NULL, &EmptyCustomAttributesCache, &t5669_TI, &t5669_0_0_0, &t5669_1_0_0, NULL, &t5669_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5668_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Collider2D>
extern MethodInfo m29657_MI;
extern MethodInfo m29658_MI;
static PropertyInfo t5668____Item_PropertyInfo = 
{
	&t5668_TI, "Item", &m29657_MI, &m29658_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5668_PIs[] =
{
	&t5668____Item_PropertyInfo,
	NULL
};
extern Il2CppType t332_0_0_0;
static ParameterInfo t5668_m29659_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t332_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29659_GM;
MethodInfo m29659_MI = 
{
	"IndexOf", NULL, &t5668_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5668_m29659_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29659_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t332_0_0_0;
static ParameterInfo t5668_m29660_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t332_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29660_GM;
MethodInfo m29660_MI = 
{
	"Insert", NULL, &t5668_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5668_m29660_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29660_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5668_m29661_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29661_GM;
MethodInfo m29661_MI = 
{
	"RemoveAt", NULL, &t5668_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5668_m29661_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29661_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5668_m29657_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t332_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29657_GM;
MethodInfo m29657_MI = 
{
	"get_Item", NULL, &t5668_TI, &t332_0_0_0, RuntimeInvoker_t29_t44, t5668_m29657_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29657_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t332_0_0_0;
static ParameterInfo t5668_m29658_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t332_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29658_GM;
MethodInfo m29658_MI = 
{
	"set_Item", NULL, &t5668_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5668_m29658_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29658_GM};
static MethodInfo* t5668_MIs[] =
{
	&m29659_MI,
	&m29660_MI,
	&m29661_MI,
	&m29657_MI,
	&m29658_MI,
	NULL
};
static TypeInfo* t5668_ITIs[] = 
{
	&t603_TI,
	&t5667_TI,
	&t5669_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5668_0_0_0;
extern Il2CppType t5668_1_0_0;
struct t5668;
extern Il2CppGenericClass t5668_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5668_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5668_MIs, t5668_PIs, NULL, NULL, NULL, NULL, NULL, &t5668_TI, t5668_ITIs, NULL, &t1908__CustomAttributeCache, &t5668_TI, &t5668_0_0_0, &t5668_1_0_0, NULL, &t5668_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2999.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2999_TI;
#include "t2999MD.h"

#include "t3000.h"
extern TypeInfo t3000_TI;
#include "t3000MD.h"
extern MethodInfo m16411_MI;
extern MethodInfo m16413_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider2D>
extern Il2CppType t316_0_0_33;
FieldInfo t2999_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2999_TI, offsetof(t2999, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2999_FIs[] =
{
	&t2999_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t332_0_0_0;
static ParameterInfo t2999_m16409_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t332_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16409_GM;
MethodInfo m16409_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2999_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2999_m16409_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16409_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2999_m16410_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16410_GM;
MethodInfo m16410_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2999_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2999_m16410_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16410_GM};
static MethodInfo* t2999_MIs[] =
{
	&m16409_MI,
	&m16410_MI,
	NULL
};
extern MethodInfo m16410_MI;
extern MethodInfo m16414_MI;
static MethodInfo* t2999_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16410_MI,
	&m16414_MI,
};
extern Il2CppType t3001_0_0_0;
extern TypeInfo t3001_TI;
extern MethodInfo m22547_MI;
extern TypeInfo t332_TI;
extern MethodInfo m16416_MI;
extern TypeInfo t332_TI;
static Il2CppRGCTXData t2999_RGCTXData[8] = 
{
	&t3001_0_0_0/* Type Usage */,
	&t3001_TI/* Class Usage */,
	&m22547_MI/* Method Usage */,
	&t332_TI/* Class Usage */,
	&m16416_MI/* Method Usage */,
	&m16411_MI/* Method Usage */,
	&t332_TI/* Class Usage */,
	&m16413_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2999_0_0_0;
extern Il2CppType t2999_1_0_0;
struct t2999;
extern Il2CppGenericClass t2999_GC;
TypeInfo t2999_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2999_MIs, NULL, t2999_FIs, NULL, &t3000_TI, NULL, NULL, &t2999_TI, NULL, t2999_VT, &EmptyCustomAttributesCache, &t2999_TI, &t2999_0_0_0, &t2999_1_0_0, NULL, &t2999_GC, NULL, NULL, NULL, t2999_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2999), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3001.h"
extern TypeInfo t3001_TI;
#include "t3001MD.h"
struct t556;
#define m22547(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Collider2D>
extern Il2CppType t3001_0_0_1;
FieldInfo t3000_f0_FieldInfo = 
{
	"Delegate", &t3001_0_0_1, &t3000_TI, offsetof(t3000, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3000_FIs[] =
{
	&t3000_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3000_m16411_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16411_GM;
MethodInfo m16411_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t3000_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3000_m16411_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16411_GM};
extern Il2CppType t3001_0_0_0;
static ParameterInfo t3000_m16412_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t3001_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16412_GM;
MethodInfo m16412_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t3000_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3000_m16412_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16412_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3000_m16413_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16413_GM;
MethodInfo m16413_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t3000_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3000_m16413_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16413_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3000_m16414_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16414_GM;
MethodInfo m16414_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t3000_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3000_m16414_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16414_GM};
static MethodInfo* t3000_MIs[] =
{
	&m16411_MI,
	&m16412_MI,
	&m16413_MI,
	&m16414_MI,
	NULL
};
static MethodInfo* t3000_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16413_MI,
	&m16414_MI,
};
extern TypeInfo t3001_TI;
extern TypeInfo t332_TI;
static Il2CppRGCTXData t3000_RGCTXData[5] = 
{
	&t3001_0_0_0/* Type Usage */,
	&t3001_TI/* Class Usage */,
	&m22547_MI/* Method Usage */,
	&t332_TI/* Class Usage */,
	&m16416_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3000_0_0_0;
extern Il2CppType t3000_1_0_0;
struct t3000;
extern Il2CppGenericClass t3000_GC;
TypeInfo t3000_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t3000_MIs, NULL, t3000_FIs, NULL, &t556_TI, NULL, NULL, &t3000_TI, NULL, t3000_VT, &EmptyCustomAttributesCache, &t3000_TI, &t3000_0_0_0, &t3000_1_0_0, NULL, &t3000_GC, NULL, NULL, NULL, t3000_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3000), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Collider2D>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3001_m16415_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16415_GM;
MethodInfo m16415_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t3001_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3001_m16415_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16415_GM};
extern Il2CppType t332_0_0_0;
static ParameterInfo t3001_m16416_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t332_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16416_GM;
MethodInfo m16416_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t3001_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3001_m16416_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16416_GM};
extern Il2CppType t332_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3001_m16417_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t332_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16417_GM;
MethodInfo m16417_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t3001_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3001_m16417_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16417_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3001_m16418_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16418_GM;
MethodInfo m16418_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t3001_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3001_m16418_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16418_GM};
static MethodInfo* t3001_MIs[] =
{
	&m16415_MI,
	&m16416_MI,
	&m16417_MI,
	&m16418_MI,
	NULL
};
extern MethodInfo m16417_MI;
extern MethodInfo m16418_MI;
static MethodInfo* t3001_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16416_MI,
	&m16417_MI,
	&m16418_MI,
};
static Il2CppInterfaceOffsetPair t3001_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3001_1_0_0;
struct t3001;
extern Il2CppGenericClass t3001_GC;
TypeInfo t3001_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t3001_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3001_TI, NULL, t3001_VT, &EmptyCustomAttributesCache, &t3001_TI, &t3001_0_0_0, &t3001_1_0_0, t3001_IOs, &t3001_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3001), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4420_TI;

#include "t511.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.AudioClip>
extern MethodInfo m29662_MI;
static PropertyInfo t4420____Current_PropertyInfo = 
{
	&t4420_TI, "Current", &m29662_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4420_PIs[] =
{
	&t4420____Current_PropertyInfo,
	NULL
};
extern Il2CppType t511_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29662_GM;
MethodInfo m29662_MI = 
{
	"get_Current", NULL, &t4420_TI, &t511_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29662_GM};
static MethodInfo* t4420_MIs[] =
{
	&m29662_MI,
	NULL
};
static TypeInfo* t4420_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4420_0_0_0;
extern Il2CppType t4420_1_0_0;
struct t4420;
extern Il2CppGenericClass t4420_GC;
TypeInfo t4420_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4420_MIs, t4420_PIs, NULL, NULL, NULL, NULL, NULL, &t4420_TI, t4420_ITIs, NULL, &EmptyCustomAttributesCache, &t4420_TI, &t4420_0_0_0, &t4420_1_0_0, NULL, &t4420_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3002.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3002_TI;
#include "t3002MD.h"

extern TypeInfo t511_TI;
extern MethodInfo m16423_MI;
extern MethodInfo m22549_MI;
struct t20;
#define m22549(__this, p0, method) (t511 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.AudioClip>
extern Il2CppType t20_0_0_1;
FieldInfo t3002_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3002_TI, offsetof(t3002, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3002_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3002_TI, offsetof(t3002, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3002_FIs[] =
{
	&t3002_f0_FieldInfo,
	&t3002_f1_FieldInfo,
	NULL
};
extern MethodInfo m16420_MI;
static PropertyInfo t3002____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3002_TI, "System.Collections.IEnumerator.Current", &m16420_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3002____Current_PropertyInfo = 
{
	&t3002_TI, "Current", &m16423_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3002_PIs[] =
{
	&t3002____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3002____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3002_m16419_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16419_GM;
MethodInfo m16419_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3002_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3002_m16419_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16419_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16420_GM;
MethodInfo m16420_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3002_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16420_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16421_GM;
MethodInfo m16421_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3002_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16421_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16422_GM;
MethodInfo m16422_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3002_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16422_GM};
extern Il2CppType t511_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16423_GM;
MethodInfo m16423_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3002_TI, &t511_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16423_GM};
static MethodInfo* t3002_MIs[] =
{
	&m16419_MI,
	&m16420_MI,
	&m16421_MI,
	&m16422_MI,
	&m16423_MI,
	NULL
};
extern MethodInfo m16422_MI;
extern MethodInfo m16421_MI;
static MethodInfo* t3002_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16420_MI,
	&m16422_MI,
	&m16421_MI,
	&m16423_MI,
};
static TypeInfo* t3002_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4420_TI,
};
static Il2CppInterfaceOffsetPair t3002_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4420_TI, 7},
};
extern TypeInfo t511_TI;
static Il2CppRGCTXData t3002_RGCTXData[3] = 
{
	&m16423_MI/* Method Usage */,
	&t511_TI/* Class Usage */,
	&m22549_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3002_0_0_0;
extern Il2CppType t3002_1_0_0;
extern Il2CppGenericClass t3002_GC;
TypeInfo t3002_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3002_MIs, t3002_PIs, t3002_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3002_TI, t3002_ITIs, t3002_VT, &EmptyCustomAttributesCache, &t3002_TI, &t3002_0_0_0, &t3002_1_0_0, t3002_IOs, &t3002_GC, NULL, NULL, NULL, t3002_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3002)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5670_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.AudioClip>
extern MethodInfo m29663_MI;
static PropertyInfo t5670____Count_PropertyInfo = 
{
	&t5670_TI, "Count", &m29663_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29664_MI;
static PropertyInfo t5670____IsReadOnly_PropertyInfo = 
{
	&t5670_TI, "IsReadOnly", &m29664_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5670_PIs[] =
{
	&t5670____Count_PropertyInfo,
	&t5670____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29663_GM;
MethodInfo m29663_MI = 
{
	"get_Count", NULL, &t5670_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29663_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29664_GM;
MethodInfo m29664_MI = 
{
	"get_IsReadOnly", NULL, &t5670_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29664_GM};
extern Il2CppType t511_0_0_0;
extern Il2CppType t511_0_0_0;
static ParameterInfo t5670_m29665_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t511_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29665_GM;
MethodInfo m29665_MI = 
{
	"Add", NULL, &t5670_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5670_m29665_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29665_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29666_GM;
MethodInfo m29666_MI = 
{
	"Clear", NULL, &t5670_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29666_GM};
extern Il2CppType t511_0_0_0;
static ParameterInfo t5670_m29667_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t511_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29667_GM;
MethodInfo m29667_MI = 
{
	"Contains", NULL, &t5670_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5670_m29667_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29667_GM};
extern Il2CppType t3766_0_0_0;
extern Il2CppType t3766_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5670_m29668_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3766_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29668_GM;
MethodInfo m29668_MI = 
{
	"CopyTo", NULL, &t5670_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5670_m29668_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29668_GM};
extern Il2CppType t511_0_0_0;
static ParameterInfo t5670_m29669_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t511_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29669_GM;
MethodInfo m29669_MI = 
{
	"Remove", NULL, &t5670_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5670_m29669_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29669_GM};
static MethodInfo* t5670_MIs[] =
{
	&m29663_MI,
	&m29664_MI,
	&m29665_MI,
	&m29666_MI,
	&m29667_MI,
	&m29668_MI,
	&m29669_MI,
	NULL
};
extern TypeInfo t5672_TI;
static TypeInfo* t5670_ITIs[] = 
{
	&t603_TI,
	&t5672_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5670_0_0_0;
extern Il2CppType t5670_1_0_0;
struct t5670;
extern Il2CppGenericClass t5670_GC;
TypeInfo t5670_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5670_MIs, t5670_PIs, NULL, NULL, NULL, NULL, NULL, &t5670_TI, t5670_ITIs, NULL, &EmptyCustomAttributesCache, &t5670_TI, &t5670_0_0_0, &t5670_1_0_0, NULL, &t5670_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.AudioClip>
extern Il2CppType t4420_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29670_GM;
MethodInfo m29670_MI = 
{
	"GetEnumerator", NULL, &t5672_TI, &t4420_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29670_GM};
static MethodInfo* t5672_MIs[] =
{
	&m29670_MI,
	NULL
};
static TypeInfo* t5672_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5672_0_0_0;
extern Il2CppType t5672_1_0_0;
struct t5672;
extern Il2CppGenericClass t5672_GC;
TypeInfo t5672_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5672_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5672_TI, t5672_ITIs, NULL, &EmptyCustomAttributesCache, &t5672_TI, &t5672_0_0_0, &t5672_1_0_0, NULL, &t5672_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5671_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.AudioClip>
extern MethodInfo m29671_MI;
extern MethodInfo m29672_MI;
static PropertyInfo t5671____Item_PropertyInfo = 
{
	&t5671_TI, "Item", &m29671_MI, &m29672_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5671_PIs[] =
{
	&t5671____Item_PropertyInfo,
	NULL
};
extern Il2CppType t511_0_0_0;
static ParameterInfo t5671_m29673_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t511_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29673_GM;
MethodInfo m29673_MI = 
{
	"IndexOf", NULL, &t5671_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5671_m29673_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29673_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t511_0_0_0;
static ParameterInfo t5671_m29674_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t511_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29674_GM;
MethodInfo m29674_MI = 
{
	"Insert", NULL, &t5671_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5671_m29674_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29674_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5671_m29675_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29675_GM;
MethodInfo m29675_MI = 
{
	"RemoveAt", NULL, &t5671_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5671_m29675_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29675_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5671_m29671_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t511_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29671_GM;
MethodInfo m29671_MI = 
{
	"get_Item", NULL, &t5671_TI, &t511_0_0_0, RuntimeInvoker_t29_t44, t5671_m29671_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29671_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t511_0_0_0;
static ParameterInfo t5671_m29672_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t511_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29672_GM;
MethodInfo m29672_MI = 
{
	"set_Item", NULL, &t5671_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5671_m29672_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29672_GM};
static MethodInfo* t5671_MIs[] =
{
	&m29673_MI,
	&m29674_MI,
	&m29675_MI,
	&m29671_MI,
	&m29672_MI,
	NULL
};
static TypeInfo* t5671_ITIs[] = 
{
	&t603_TI,
	&t5670_TI,
	&t5672_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5671_0_0_0;
extern Il2CppType t5671_1_0_0;
struct t5671;
extern Il2CppGenericClass t5671_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5671_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5671_MIs, t5671_PIs, NULL, NULL, NULL, NULL, NULL, &t5671_TI, t5671_ITIs, NULL, &t1908__CustomAttributeCache, &t5671_TI, &t5671_0_0_0, &t5671_1_0_0, NULL, &t5671_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3003.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3003_TI;
#include "t3003MD.h"

#include "t3004.h"
extern TypeInfo t3004_TI;
#include "t3004MD.h"
extern MethodInfo m16426_MI;
extern MethodInfo m16428_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AudioClip>
extern Il2CppType t316_0_0_33;
FieldInfo t3003_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t3003_TI, offsetof(t3003, f1), &EmptyCustomAttributesCache};
static FieldInfo* t3003_FIs[] =
{
	&t3003_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t511_0_0_0;
static ParameterInfo t3003_m16424_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t511_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16424_GM;
MethodInfo m16424_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t3003_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t3003_m16424_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16424_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3003_m16425_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16425_GM;
MethodInfo m16425_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t3003_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3003_m16425_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16425_GM};
static MethodInfo* t3003_MIs[] =
{
	&m16424_MI,
	&m16425_MI,
	NULL
};
extern MethodInfo m16425_MI;
extern MethodInfo m16429_MI;
static MethodInfo* t3003_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16425_MI,
	&m16429_MI,
};
extern Il2CppType t3005_0_0_0;
extern TypeInfo t3005_TI;
extern MethodInfo m22559_MI;
extern TypeInfo t511_TI;
extern MethodInfo m16431_MI;
extern TypeInfo t511_TI;
static Il2CppRGCTXData t3003_RGCTXData[8] = 
{
	&t3005_0_0_0/* Type Usage */,
	&t3005_TI/* Class Usage */,
	&m22559_MI/* Method Usage */,
	&t511_TI/* Class Usage */,
	&m16431_MI/* Method Usage */,
	&m16426_MI/* Method Usage */,
	&t511_TI/* Class Usage */,
	&m16428_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3003_0_0_0;
extern Il2CppType t3003_1_0_0;
struct t3003;
extern Il2CppGenericClass t3003_GC;
TypeInfo t3003_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t3003_MIs, NULL, t3003_FIs, NULL, &t3004_TI, NULL, NULL, &t3003_TI, NULL, t3003_VT, &EmptyCustomAttributesCache, &t3003_TI, &t3003_0_0_0, &t3003_1_0_0, NULL, &t3003_GC, NULL, NULL, NULL, t3003_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3003), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t3005.h"
extern TypeInfo t3005_TI;
#include "t3005MD.h"
struct t556;
#define m22559(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>
extern Il2CppType t3005_0_0_1;
FieldInfo t3004_f0_FieldInfo = 
{
	"Delegate", &t3005_0_0_1, &t3004_TI, offsetof(t3004, f0), &EmptyCustomAttributesCache};
static FieldInfo* t3004_FIs[] =
{
	&t3004_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3004_m16426_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16426_GM;
MethodInfo m16426_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t3004_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t3004_m16426_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16426_GM};
extern Il2CppType t3005_0_0_0;
static ParameterInfo t3004_m16427_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t3005_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16427_GM;
MethodInfo m16427_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t3004_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3004_m16427_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16427_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t3004_m16428_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16428_GM;
MethodInfo m16428_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t3004_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3004_m16428_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16428_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t3004_m16429_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16429_GM;
MethodInfo m16429_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t3004_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t3004_m16429_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16429_GM};
static MethodInfo* t3004_MIs[] =
{
	&m16426_MI,
	&m16427_MI,
	&m16428_MI,
	&m16429_MI,
	NULL
};
static MethodInfo* t3004_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m16428_MI,
	&m16429_MI,
};
extern TypeInfo t3005_TI;
extern TypeInfo t511_TI;
static Il2CppRGCTXData t3004_RGCTXData[5] = 
{
	&t3005_0_0_0/* Type Usage */,
	&t3005_TI/* Class Usage */,
	&m22559_MI/* Method Usage */,
	&t511_TI/* Class Usage */,
	&m16431_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3004_0_0_0;
extern Il2CppType t3004_1_0_0;
struct t3004;
extern Il2CppGenericClass t3004_GC;
TypeInfo t3004_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t3004_MIs, NULL, t3004_FIs, NULL, &t556_TI, NULL, NULL, &t3004_TI, NULL, t3004_VT, &EmptyCustomAttributesCache, &t3004_TI, &t3004_0_0_0, &t3004_1_0_0, NULL, &t3004_GC, NULL, NULL, NULL, t3004_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3004), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.AudioClip>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3005_m16430_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16430_GM;
MethodInfo m16430_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t3005_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3005_m16430_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m16430_GM};
extern Il2CppType t511_0_0_0;
static ParameterInfo t3005_m16431_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t511_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16431_GM;
MethodInfo m16431_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t3005_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3005_m16431_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16431_GM};
extern Il2CppType t511_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3005_m16432_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t511_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16432_GM;
MethodInfo m16432_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t3005_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3005_m16432_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m16432_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3005_m16433_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16433_GM;
MethodInfo m16433_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t3005_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3005_m16433_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16433_GM};
static MethodInfo* t3005_MIs[] =
{
	&m16430_MI,
	&m16431_MI,
	&m16432_MI,
	&m16433_MI,
	NULL
};
extern MethodInfo m16432_MI;
extern MethodInfo m16433_MI;
static MethodInfo* t3005_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m16431_MI,
	&m16432_MI,
	&m16433_MI,
};
static Il2CppInterfaceOffsetPair t3005_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t3005_1_0_0;
struct t3005;
extern Il2CppGenericClass t3005_GC;
TypeInfo t3005_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t3005_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t3005_TI, NULL, t3005_VT, &EmptyCustomAttributesCache, &t3005_TI, &t3005_0_0_0, &t3005_1_0_0, t3005_IOs, &t3005_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3005), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4421_TI;

#include "t22.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Single>
extern MethodInfo m29676_MI;
static PropertyInfo t4421____Current_PropertyInfo = 
{
	&t4421_TI, "Current", &m29676_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4421_PIs[] =
{
	&t4421____Current_PropertyInfo,
	NULL
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29676_GM;
MethodInfo m29676_MI = 
{
	"get_Current", NULL, &t4421_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29676_GM};
static MethodInfo* t4421_MIs[] =
{
	&m29676_MI,
	NULL
};
static TypeInfo* t4421_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4421_0_0_0;
extern Il2CppType t4421_1_0_0;
struct t4421;
extern Il2CppGenericClass t4421_GC;
TypeInfo t4421_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4421_MIs, t4421_PIs, NULL, NULL, NULL, NULL, NULL, &t4421_TI, t4421_ITIs, NULL, &EmptyCustomAttributesCache, &t4421_TI, &t4421_0_0_0, &t4421_1_0_0, NULL, &t4421_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3006.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3006_TI;
#include "t3006MD.h"

extern TypeInfo t22_TI;
extern MethodInfo m16438_MI;
extern MethodInfo m22561_MI;
struct t20;
 float m22561 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m16434_MI;
 void m16434 (t3006 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16435_MI;
 t29 * m16435 (t3006 * __this, MethodInfo* method){
	{
		float L_0 = m16438(__this, &m16438_MI);
		float L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t22_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m16436_MI;
 void m16436 (t3006 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m16437_MI;
 bool m16437 (t3006 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 float m16438 (t3006 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		float L_8 = m22561(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m22561_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Single>
extern Il2CppType t20_0_0_1;
FieldInfo t3006_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3006_TI, offsetof(t3006, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3006_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3006_TI, offsetof(t3006, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3006_FIs[] =
{
	&t3006_f0_FieldInfo,
	&t3006_f1_FieldInfo,
	NULL
};
static PropertyInfo t3006____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3006_TI, "System.Collections.IEnumerator.Current", &m16435_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3006____Current_PropertyInfo = 
{
	&t3006_TI, "Current", &m16438_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3006_PIs[] =
{
	&t3006____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3006____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3006_m16434_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16434_GM;
MethodInfo m16434_MI = 
{
	".ctor", (methodPointerType)&m16434, &t3006_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3006_m16434_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16434_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16435_GM;
MethodInfo m16435_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m16435, &t3006_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16435_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16436_GM;
MethodInfo m16436_MI = 
{
	"Dispose", (methodPointerType)&m16436, &t3006_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16436_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16437_GM;
MethodInfo m16437_MI = 
{
	"MoveNext", (methodPointerType)&m16437, &t3006_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16437_GM};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16438_GM;
MethodInfo m16438_MI = 
{
	"get_Current", (methodPointerType)&m16438, &t3006_TI, &t22_0_0_0, RuntimeInvoker_t22, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16438_GM};
static MethodInfo* t3006_MIs[] =
{
	&m16434_MI,
	&m16435_MI,
	&m16436_MI,
	&m16437_MI,
	&m16438_MI,
	NULL
};
static MethodInfo* t3006_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16435_MI,
	&m16437_MI,
	&m16436_MI,
	&m16438_MI,
};
static TypeInfo* t3006_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4421_TI,
};
static Il2CppInterfaceOffsetPair t3006_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4421_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3006_0_0_0;
extern Il2CppType t3006_1_0_0;
extern Il2CppGenericClass t3006_GC;
TypeInfo t3006_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3006_MIs, t3006_PIs, t3006_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3006_TI, t3006_ITIs, t3006_VT, &EmptyCustomAttributesCache, &t3006_TI, &t3006_0_0_0, &t3006_1_0_0, t3006_IOs, &t3006_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3006)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5673_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Single>
extern MethodInfo m29677_MI;
static PropertyInfo t5673____Count_PropertyInfo = 
{
	&t5673_TI, "Count", &m29677_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29678_MI;
static PropertyInfo t5673____IsReadOnly_PropertyInfo = 
{
	&t5673_TI, "IsReadOnly", &m29678_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5673_PIs[] =
{
	&t5673____Count_PropertyInfo,
	&t5673____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29677_GM;
MethodInfo m29677_MI = 
{
	"get_Count", NULL, &t5673_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29677_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29678_GM;
MethodInfo m29678_MI = 
{
	"get_IsReadOnly", NULL, &t5673_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29678_GM};
extern Il2CppType t22_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t5673_m29679_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29679_GM;
MethodInfo m29679_MI = 
{
	"Add", NULL, &t5673_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t5673_m29679_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29679_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29680_GM;
MethodInfo m29680_MI = 
{
	"Clear", NULL, &t5673_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29680_GM};
extern Il2CppType t22_0_0_0;
static ParameterInfo t5673_m29681_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29681_GM;
MethodInfo m29681_MI = 
{
	"Contains", NULL, &t5673_TI, &t40_0_0_0, RuntimeInvoker_t40_t22, t5673_m29681_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29681_GM};
extern Il2CppType t509_0_0_0;
extern Il2CppType t509_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5673_m29682_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t509_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29682_GM;
MethodInfo m29682_MI = 
{
	"CopyTo", NULL, &t5673_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5673_m29682_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29682_GM};
extern Il2CppType t22_0_0_0;
static ParameterInfo t5673_m29683_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29683_GM;
MethodInfo m29683_MI = 
{
	"Remove", NULL, &t5673_TI, &t40_0_0_0, RuntimeInvoker_t40_t22, t5673_m29683_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29683_GM};
static MethodInfo* t5673_MIs[] =
{
	&m29677_MI,
	&m29678_MI,
	&m29679_MI,
	&m29680_MI,
	&m29681_MI,
	&m29682_MI,
	&m29683_MI,
	NULL
};
extern TypeInfo t5675_TI;
static TypeInfo* t5673_ITIs[] = 
{
	&t603_TI,
	&t5675_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5673_0_0_0;
extern Il2CppType t5673_1_0_0;
struct t5673;
extern Il2CppGenericClass t5673_GC;
TypeInfo t5673_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5673_MIs, t5673_PIs, NULL, NULL, NULL, NULL, NULL, &t5673_TI, t5673_ITIs, NULL, &EmptyCustomAttributesCache, &t5673_TI, &t5673_0_0_0, &t5673_1_0_0, NULL, &t5673_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Single>
extern Il2CppType t4421_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29684_GM;
MethodInfo m29684_MI = 
{
	"GetEnumerator", NULL, &t5675_TI, &t4421_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29684_GM};
static MethodInfo* t5675_MIs[] =
{
	&m29684_MI,
	NULL
};
static TypeInfo* t5675_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5675_0_0_0;
extern Il2CppType t5675_1_0_0;
struct t5675;
extern Il2CppGenericClass t5675_GC;
TypeInfo t5675_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5675_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5675_TI, t5675_ITIs, NULL, &EmptyCustomAttributesCache, &t5675_TI, &t5675_0_0_0, &t5675_1_0_0, NULL, &t5675_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5674_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Single>
extern MethodInfo m29685_MI;
extern MethodInfo m29686_MI;
static PropertyInfo t5674____Item_PropertyInfo = 
{
	&t5674_TI, "Item", &m29685_MI, &m29686_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5674_PIs[] =
{
	&t5674____Item_PropertyInfo,
	NULL
};
extern Il2CppType t22_0_0_0;
static ParameterInfo t5674_m29687_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29687_GM;
MethodInfo m29687_MI = 
{
	"IndexOf", NULL, &t5674_TI, &t44_0_0_0, RuntimeInvoker_t44_t22, t5674_m29687_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29687_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t5674_m29688_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29688_GM;
MethodInfo m29688_MI = 
{
	"Insert", NULL, &t5674_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t22, t5674_m29688_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29688_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5674_m29689_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29689_GM;
MethodInfo m29689_MI = 
{
	"RemoveAt", NULL, &t5674_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5674_m29689_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29689_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5674_m29685_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t22_0_0_0;
extern void* RuntimeInvoker_t22_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29685_GM;
MethodInfo m29685_MI = 
{
	"get_Item", NULL, &t5674_TI, &t22_0_0_0, RuntimeInvoker_t22_t44, t5674_m29685_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29685_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t22_0_0_0;
static ParameterInfo t5674_m29686_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29686_GM;
MethodInfo m29686_MI = 
{
	"set_Item", NULL, &t5674_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t22, t5674_m29686_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29686_GM};
static MethodInfo* t5674_MIs[] =
{
	&m29687_MI,
	&m29688_MI,
	&m29689_MI,
	&m29685_MI,
	&m29686_MI,
	NULL
};
static TypeInfo* t5674_ITIs[] = 
{
	&t603_TI,
	&t5673_TI,
	&t5675_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5674_0_0_0;
extern Il2CppType t5674_1_0_0;
struct t5674;
extern Il2CppGenericClass t5674_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5674_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5674_MIs, t5674_PIs, NULL, NULL, NULL, NULL, NULL, &t5674_TI, t5674_ITIs, NULL, &t1908__CustomAttributeCache, &t5674_TI, &t5674_0_0_0, &t5674_1_0_0, NULL, &t5674_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5676_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Single>>
extern MethodInfo m29690_MI;
static PropertyInfo t5676____Count_PropertyInfo = 
{
	&t5676_TI, "Count", &m29690_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29691_MI;
static PropertyInfo t5676____IsReadOnly_PropertyInfo = 
{
	&t5676_TI, "IsReadOnly", &m29691_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5676_PIs[] =
{
	&t5676____Count_PropertyInfo,
	&t5676____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29690_GM;
MethodInfo m29690_MI = 
{
	"get_Count", NULL, &t5676_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29690_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29691_GM;
MethodInfo m29691_MI = 
{
	"get_IsReadOnly", NULL, &t5676_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29691_GM};
extern Il2CppType t1751_0_0_0;
extern Il2CppType t1751_0_0_0;
static ParameterInfo t5676_m29692_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1751_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29692_GM;
MethodInfo m29692_MI = 
{
	"Add", NULL, &t5676_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5676_m29692_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29692_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29693_GM;
MethodInfo m29693_MI = 
{
	"Clear", NULL, &t5676_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29693_GM};
extern Il2CppType t1751_0_0_0;
static ParameterInfo t5676_m29694_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1751_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29694_GM;
MethodInfo m29694_MI = 
{
	"Contains", NULL, &t5676_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5676_m29694_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29694_GM};
extern Il2CppType t3545_0_0_0;
extern Il2CppType t3545_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5676_m29695_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3545_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29695_GM;
MethodInfo m29695_MI = 
{
	"CopyTo", NULL, &t5676_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5676_m29695_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29695_GM};
extern Il2CppType t1751_0_0_0;
static ParameterInfo t5676_m29696_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1751_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29696_GM;
MethodInfo m29696_MI = 
{
	"Remove", NULL, &t5676_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5676_m29696_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29696_GM};
static MethodInfo* t5676_MIs[] =
{
	&m29690_MI,
	&m29691_MI,
	&m29692_MI,
	&m29693_MI,
	&m29694_MI,
	&m29695_MI,
	&m29696_MI,
	NULL
};
extern TypeInfo t5678_TI;
static TypeInfo* t5676_ITIs[] = 
{
	&t603_TI,
	&t5678_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5676_0_0_0;
extern Il2CppType t5676_1_0_0;
struct t5676;
extern Il2CppGenericClass t5676_GC;
TypeInfo t5676_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5676_MIs, t5676_PIs, NULL, NULL, NULL, NULL, NULL, &t5676_TI, t5676_ITIs, NULL, &EmptyCustomAttributesCache, &t5676_TI, &t5676_0_0_0, &t5676_1_0_0, NULL, &t5676_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Single>>
extern Il2CppType t4423_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29697_GM;
MethodInfo m29697_MI = 
{
	"GetEnumerator", NULL, &t5678_TI, &t4423_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29697_GM};
static MethodInfo* t5678_MIs[] =
{
	&m29697_MI,
	NULL
};
static TypeInfo* t5678_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5678_0_0_0;
extern Il2CppType t5678_1_0_0;
struct t5678;
extern Il2CppGenericClass t5678_GC;
TypeInfo t5678_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5678_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5678_TI, t5678_ITIs, NULL, &EmptyCustomAttributesCache, &t5678_TI, &t5678_0_0_0, &t5678_1_0_0, NULL, &t5678_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4423_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Single>>
extern MethodInfo m29698_MI;
static PropertyInfo t4423____Current_PropertyInfo = 
{
	&t4423_TI, "Current", &m29698_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4423_PIs[] =
{
	&t4423____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1751_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29698_GM;
MethodInfo m29698_MI = 
{
	"get_Current", NULL, &t4423_TI, &t1751_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29698_GM};
static MethodInfo* t4423_MIs[] =
{
	&m29698_MI,
	NULL
};
static TypeInfo* t4423_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4423_0_0_0;
extern Il2CppType t4423_1_0_0;
struct t4423;
extern Il2CppGenericClass t4423_GC;
TypeInfo t4423_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4423_MIs, t4423_PIs, NULL, NULL, NULL, NULL, NULL, &t4423_TI, t4423_ITIs, NULL, &EmptyCustomAttributesCache, &t4423_TI, &t4423_0_0_0, &t4423_1_0_0, NULL, &t4423_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1751_TI;



// Metadata Definition System.IComparable`1<System.Single>
extern Il2CppType t22_0_0_0;
static ParameterInfo t1751_m29699_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29699_GM;
MethodInfo m29699_MI = 
{
	"CompareTo", NULL, &t1751_TI, &t44_0_0_0, RuntimeInvoker_t44_t22, t1751_m29699_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29699_GM};
static MethodInfo* t1751_MIs[] =
{
	&m29699_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1751_1_0_0;
struct t1751;
extern Il2CppGenericClass t1751_GC;
TypeInfo t1751_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1751_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1751_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1751_TI, &t1751_0_0_0, &t1751_1_0_0, NULL, &t1751_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3007.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3007_TI;
#include "t3007MD.h"

extern MethodInfo m16443_MI;
extern MethodInfo m22572_MI;
struct t20;
#define m22572(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Single>>
extern Il2CppType t20_0_0_1;
FieldInfo t3007_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3007_TI, offsetof(t3007, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3007_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3007_TI, offsetof(t3007, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3007_FIs[] =
{
	&t3007_f0_FieldInfo,
	&t3007_f1_FieldInfo,
	NULL
};
extern MethodInfo m16440_MI;
static PropertyInfo t3007____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3007_TI, "System.Collections.IEnumerator.Current", &m16440_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3007____Current_PropertyInfo = 
{
	&t3007_TI, "Current", &m16443_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3007_PIs[] =
{
	&t3007____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3007____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3007_m16439_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16439_GM;
MethodInfo m16439_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3007_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3007_m16439_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16439_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16440_GM;
MethodInfo m16440_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3007_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16440_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16441_GM;
MethodInfo m16441_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3007_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16441_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16442_GM;
MethodInfo m16442_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3007_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16442_GM};
extern Il2CppType t1751_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16443_GM;
MethodInfo m16443_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3007_TI, &t1751_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16443_GM};
static MethodInfo* t3007_MIs[] =
{
	&m16439_MI,
	&m16440_MI,
	&m16441_MI,
	&m16442_MI,
	&m16443_MI,
	NULL
};
extern MethodInfo m16442_MI;
extern MethodInfo m16441_MI;
static MethodInfo* t3007_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16440_MI,
	&m16442_MI,
	&m16441_MI,
	&m16443_MI,
};
static TypeInfo* t3007_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4423_TI,
};
static Il2CppInterfaceOffsetPair t3007_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4423_TI, 7},
};
extern TypeInfo t1751_TI;
static Il2CppRGCTXData t3007_RGCTXData[3] = 
{
	&m16443_MI/* Method Usage */,
	&t1751_TI/* Class Usage */,
	&m22572_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3007_0_0_0;
extern Il2CppType t3007_1_0_0;
extern Il2CppGenericClass t3007_GC;
TypeInfo t3007_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3007_MIs, t3007_PIs, t3007_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3007_TI, t3007_ITIs, t3007_VT, &EmptyCustomAttributesCache, &t3007_TI, &t3007_0_0_0, &t3007_1_0_0, t3007_IOs, &t3007_GC, NULL, NULL, NULL, t3007_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3007)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5677_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Single>>
extern MethodInfo m29700_MI;
extern MethodInfo m29701_MI;
static PropertyInfo t5677____Item_PropertyInfo = 
{
	&t5677_TI, "Item", &m29700_MI, &m29701_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5677_PIs[] =
{
	&t5677____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1751_0_0_0;
static ParameterInfo t5677_m29702_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1751_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29702_GM;
MethodInfo m29702_MI = 
{
	"IndexOf", NULL, &t5677_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5677_m29702_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29702_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1751_0_0_0;
static ParameterInfo t5677_m29703_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1751_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29703_GM;
MethodInfo m29703_MI = 
{
	"Insert", NULL, &t5677_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5677_m29703_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29703_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5677_m29704_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29704_GM;
MethodInfo m29704_MI = 
{
	"RemoveAt", NULL, &t5677_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5677_m29704_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29704_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5677_m29700_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1751_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29700_GM;
MethodInfo m29700_MI = 
{
	"get_Item", NULL, &t5677_TI, &t1751_0_0_0, RuntimeInvoker_t29_t44, t5677_m29700_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29700_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1751_0_0_0;
static ParameterInfo t5677_m29701_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1751_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29701_GM;
MethodInfo m29701_MI = 
{
	"set_Item", NULL, &t5677_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5677_m29701_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29701_GM};
static MethodInfo* t5677_MIs[] =
{
	&m29702_MI,
	&m29703_MI,
	&m29704_MI,
	&m29700_MI,
	&m29701_MI,
	NULL
};
static TypeInfo* t5677_ITIs[] = 
{
	&t603_TI,
	&t5676_TI,
	&t5678_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5677_0_0_0;
extern Il2CppType t5677_1_0_0;
struct t5677;
extern Il2CppGenericClass t5677_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5677_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5677_MIs, t5677_PIs, NULL, NULL, NULL, NULL, NULL, &t5677_TI, t5677_ITIs, NULL, &t1908__CustomAttributeCache, &t5677_TI, &t5677_0_0_0, &t5677_1_0_0, NULL, &t5677_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5679_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Single>>
extern MethodInfo m29705_MI;
static PropertyInfo t5679____Count_PropertyInfo = 
{
	&t5679_TI, "Count", &m29705_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m29706_MI;
static PropertyInfo t5679____IsReadOnly_PropertyInfo = 
{
	&t5679_TI, "IsReadOnly", &m29706_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5679_PIs[] =
{
	&t5679____Count_PropertyInfo,
	&t5679____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29705_GM;
MethodInfo m29705_MI = 
{
	"get_Count", NULL, &t5679_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29705_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29706_GM;
MethodInfo m29706_MI = 
{
	"get_IsReadOnly", NULL, &t5679_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29706_GM};
extern Il2CppType t1752_0_0_0;
extern Il2CppType t1752_0_0_0;
static ParameterInfo t5679_m29707_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1752_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29707_GM;
MethodInfo m29707_MI = 
{
	"Add", NULL, &t5679_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5679_m29707_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29707_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29708_GM;
MethodInfo m29708_MI = 
{
	"Clear", NULL, &t5679_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29708_GM};
extern Il2CppType t1752_0_0_0;
static ParameterInfo t5679_m29709_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1752_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29709_GM;
MethodInfo m29709_MI = 
{
	"Contains", NULL, &t5679_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5679_m29709_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29709_GM};
extern Il2CppType t3546_0_0_0;
extern Il2CppType t3546_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5679_m29710_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3546_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29710_GM;
MethodInfo m29710_MI = 
{
	"CopyTo", NULL, &t5679_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5679_m29710_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m29710_GM};
extern Il2CppType t1752_0_0_0;
static ParameterInfo t5679_m29711_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1752_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29711_GM;
MethodInfo m29711_MI = 
{
	"Remove", NULL, &t5679_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5679_m29711_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29711_GM};
static MethodInfo* t5679_MIs[] =
{
	&m29705_MI,
	&m29706_MI,
	&m29707_MI,
	&m29708_MI,
	&m29709_MI,
	&m29710_MI,
	&m29711_MI,
	NULL
};
extern TypeInfo t5681_TI;
static TypeInfo* t5679_ITIs[] = 
{
	&t603_TI,
	&t5681_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5679_0_0_0;
extern Il2CppType t5679_1_0_0;
struct t5679;
extern Il2CppGenericClass t5679_GC;
TypeInfo t5679_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5679_MIs, t5679_PIs, NULL, NULL, NULL, NULL, NULL, &t5679_TI, t5679_ITIs, NULL, &EmptyCustomAttributesCache, &t5679_TI, &t5679_0_0_0, &t5679_1_0_0, NULL, &t5679_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Single>>
extern Il2CppType t4425_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29712_GM;
MethodInfo m29712_MI = 
{
	"GetEnumerator", NULL, &t5681_TI, &t4425_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29712_GM};
static MethodInfo* t5681_MIs[] =
{
	&m29712_MI,
	NULL
};
static TypeInfo* t5681_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5681_0_0_0;
extern Il2CppType t5681_1_0_0;
struct t5681;
extern Il2CppGenericClass t5681_GC;
TypeInfo t5681_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5681_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5681_TI, t5681_ITIs, NULL, &EmptyCustomAttributesCache, &t5681_TI, &t5681_0_0_0, &t5681_1_0_0, NULL, &t5681_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4425_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Single>>
extern MethodInfo m29713_MI;
static PropertyInfo t4425____Current_PropertyInfo = 
{
	&t4425_TI, "Current", &m29713_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4425_PIs[] =
{
	&t4425____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1752_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29713_GM;
MethodInfo m29713_MI = 
{
	"get_Current", NULL, &t4425_TI, &t1752_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m29713_GM};
static MethodInfo* t4425_MIs[] =
{
	&m29713_MI,
	NULL
};
static TypeInfo* t4425_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4425_0_0_0;
extern Il2CppType t4425_1_0_0;
struct t4425;
extern Il2CppGenericClass t4425_GC;
TypeInfo t4425_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4425_MIs, t4425_PIs, NULL, NULL, NULL, NULL, NULL, &t4425_TI, t4425_ITIs, NULL, &EmptyCustomAttributesCache, &t4425_TI, &t4425_0_0_0, &t4425_1_0_0, NULL, &t4425_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1752_TI;



// Metadata Definition System.IEquatable`1<System.Single>
extern Il2CppType t22_0_0_0;
static ParameterInfo t1752_m29714_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m29714_GM;
MethodInfo m29714_MI = 
{
	"Equals", NULL, &t1752_TI, &t40_0_0_0, RuntimeInvoker_t40_t22, t1752_m29714_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m29714_GM};
static MethodInfo* t1752_MIs[] =
{
	&m29714_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1752_1_0_0;
struct t1752;
extern Il2CppGenericClass t1752_GC;
TypeInfo t1752_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1752_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1752_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1752_TI, &t1752_0_0_0, &t1752_1_0_0, NULL, &t1752_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3008.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3008_TI;
#include "t3008MD.h"

extern MethodInfo m16448_MI;
extern MethodInfo m22583_MI;
struct t20;
#define m22583(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Single>>
extern Il2CppType t20_0_0_1;
FieldInfo t3008_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3008_TI, offsetof(t3008, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3008_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3008_TI, offsetof(t3008, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3008_FIs[] =
{
	&t3008_f0_FieldInfo,
	&t3008_f1_FieldInfo,
	NULL
};
extern MethodInfo m16445_MI;
static PropertyInfo t3008____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3008_TI, "System.Collections.IEnumerator.Current", &m16445_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3008____Current_PropertyInfo = 
{
	&t3008_TI, "Current", &m16448_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3008_PIs[] =
{
	&t3008____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3008____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3008_m16444_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16444_GM;
MethodInfo m16444_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3008_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3008_m16444_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m16444_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16445_GM;
MethodInfo m16445_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3008_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16445_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16446_GM;
MethodInfo m16446_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3008_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16446_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16447_GM;
MethodInfo m16447_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3008_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16447_GM};
extern Il2CppType t1752_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m16448_GM;
MethodInfo m16448_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3008_TI, &t1752_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m16448_GM};
static MethodInfo* t3008_MIs[] =
{
	&m16444_MI,
	&m16445_MI,
	&m16446_MI,
	&m16447_MI,
	&m16448_MI,
	NULL
};
extern MethodInfo m16447_MI;
extern MethodInfo m16446_MI;
static MethodInfo* t3008_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m16445_MI,
	&m16447_MI,
	&m16446_MI,
	&m16448_MI,
};
static TypeInfo* t3008_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4425_TI,
};
static Il2CppInterfaceOffsetPair t3008_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4425_TI, 7},
};
extern TypeInfo t1752_TI;
static Il2CppRGCTXData t3008_RGCTXData[3] = 
{
	&m16448_MI/* Method Usage */,
	&t1752_TI/* Class Usage */,
	&m22583_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3008_0_0_0;
extern Il2CppType t3008_1_0_0;
extern Il2CppGenericClass t3008_GC;
TypeInfo t3008_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3008_MIs, t3008_PIs, t3008_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3008_TI, t3008_ITIs, t3008_VT, &EmptyCustomAttributesCache, &t3008_TI, &t3008_0_0_0, &t3008_1_0_0, t3008_IOs, &t3008_GC, NULL, NULL, NULL, t3008_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3008)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
