﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3353;
struct t29;
struct t20;
#include "t1064.h"

 void m18637 (t3353 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18638 (t3353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18639 (t3353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18640 (t3353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18641 (t3353 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
