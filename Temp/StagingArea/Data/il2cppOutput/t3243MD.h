﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3243;
struct t29;
struct t20;
#include "t977.h"

 void m18011 (t3243 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18012 (t3243 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18013 (t3243 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18014 (t3243 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18015 (t3243 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
