﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t396;
struct t29;
struct t7;
#include "t396.h"
#include "t183.h"
#include "t23.h"
#include "t362.h"

 float m2346 (t396 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2347 (t396 * __this, int32_t p0, int32_t p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2348 (t396 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2349 (t396 * __this, int32_t p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2350 (t396 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2351 (t396 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2352 (t29 * __this, t396  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2353 (t29 * __this, t396 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2354 (t29 * __this, t396  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2355 (t29 * __this, t396 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2356 (t29 * __this, t396  p0, t396 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2357 (t29 * __this, t396 * p0, t396 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2358 (t396 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2359 (t396 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2360 (t396 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t183  m2361 (t396 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t183  m2362 (t396 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2363 (t396 * __this, int32_t p0, t183  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2364 (t396 * __this, int32_t p0, t183  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2365 (t396 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m1861 (t396 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2366 (t396 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2367 (t29 * __this, t23  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2368 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2369 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2370 (t396 * __this, t23  p0, t362  p1, t23  p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2371 (t29 * __this, t23  p0, t362  p1, t23  p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2372 (t29 * __this, t23 * p0, t362 * p1, t23 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2373 (t396 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2374 (t396 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2375 (t29 * __this, float p0, float p1, float p2, float p3, float p4, float p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2376 (t29 * __this, float p0, float p1, float p2, float p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t396  m2377 (t29 * __this, t396  p0, t396  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t183  m2378 (t29 * __this, t396  p0, t183  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2379 (t29 * __this, t396  p0, t396  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2380 (t29 * __this, t396  p0, t396  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
