﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2180;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m10741_gshared (t2180 * __this, t29 * p0, t35 p1, MethodInfo* method);
#define m10741(__this, p0, p1, method) (void)m10741_gshared((t2180 *)__this, (t29 *)p0, (t35)p1, method)
 bool m10742_gshared (t2180 * __this, t29 * p0, MethodInfo* method);
#define m10742(__this, p0, method) (bool)m10742_gshared((t2180 *)__this, (t29 *)p0, method)
 t29 * m10743_gshared (t2180 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method);
#define m10743(__this, p0, p1, p2, method) (t29 *)m10743_gshared((t2180 *)__this, (t29 *)p0, (t67 *)p1, (t29 *)p2, method)
 bool m10744_gshared (t2180 * __this, t29 * p0, MethodInfo* method);
#define m10744(__this, p0, method) (bool)m10744_gshared((t2180 *)__this, (t29 *)p0, method)
