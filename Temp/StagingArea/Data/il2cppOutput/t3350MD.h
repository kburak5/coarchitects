﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3350;
struct t29;
struct t20;
#include "t1314.h"

 void m18622 (t3350 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18623 (t3350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18624 (t3350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18625 (t3350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18626 (t3350 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
