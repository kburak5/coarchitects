﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t887;
struct t877;
struct t875;
struct t878;

 void m3818 (t887 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t877 * m3819 (t887 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3820 (t887 * __this, t875 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3821 (t887 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3822 (t887 * __this, int32_t* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
