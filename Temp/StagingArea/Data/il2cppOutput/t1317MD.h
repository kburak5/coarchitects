﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1317;
struct t29;
struct t1050;
struct t67;
struct t66;

 void m7064 (t1317 * __this, t67 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7065 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7066 (t1317 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1050 * m7067 (t1317 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7068 (t1317 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
