﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1031;
struct t1034;
struct t762;
struct t745;
struct t1058;
struct t1046;
struct t1047;
struct t7;
struct t1059;
struct t66;
struct t67;
struct t29;
struct t1057;
struct t945;
struct t841;
struct t777;
#include "t1026.h"

 void m4840 (t1031 * __this, t1034 * p0, t7* p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4841 (t1031 * __this, t1034 * p0, t7* p1, t745 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4842 (t1031 * __this, t1034 * p0, t7* p1, t762 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4843 (t1031 * __this, t1034 * p0, t7* p1, bool p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4844 (t1031 * __this, t1034 * p0, t7* p1, bool p2, int32_t p3, t762 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4845 (t1031 * __this, t1058 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4846 (t1031 * __this, t1058 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4847 (t1031 * __this, t1046 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4848 (t1031 * __this, t1046 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4849 (t1031 * __this, t1047 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4850 (t1031 * __this, t1047 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4851 (t1031 * __this, t1059 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4852 (t1031 * __this, t1059 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1034 * m4853 (t1031 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t762 * m4854 (t1031 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m4855 (t1031 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1058 * m4856 (t1031 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4857 (t1031 * __this, t1058 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1046 * m4858 (t1031 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4859 (t1031 * __this, t1046 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1047 * m4860 (t1031 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4861 (t1031 * __this, t1047 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4862 (t1031 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4863 (t1031 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4864 (t1031 * __this, t67 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4865 (t1031 * __this, t1034 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4866 (t1031 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m4867 (t1031 * __this, t762 * p0, t745 * p1, t7* p2, t762 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4868 (t1031 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1057 * m4869 (t1031 * __this, t945 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4870 (t1031 * __this, t745 * p0, t841* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4871 (t1031 * __this, t745 * p0, t841* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1057 * m4872 (t1031 * __this, t945 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m4873 (t1031 * __this, t762 * p0, t745 * p1, t7* p2, t762 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t777 * m4874 (t1031 * __this, t745 * p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t777 * m4875 (t1031 * __this, t745 * p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
