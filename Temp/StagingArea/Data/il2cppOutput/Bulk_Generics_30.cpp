﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t6225_TI;

#include "t44.h"
#include "t40.h"
#include "t21.h"
#include "t1301.h"
#include "mscorlib_ArrayTypes.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>
extern MethodInfo m32352_MI;
static PropertyInfo t6225____Count_PropertyInfo = 
{
	&t6225_TI, "Count", &m32352_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32353_MI;
static PropertyInfo t6225____IsReadOnly_PropertyInfo = 
{
	&t6225_TI, "IsReadOnly", &m32353_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6225_PIs[] =
{
	&t6225____Count_PropertyInfo,
	&t6225____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32352_GM;
MethodInfo m32352_MI = 
{
	"get_Count", NULL, &t6225_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32352_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32353_GM;
MethodInfo m32353_MI = 
{
	"get_IsReadOnly", NULL, &t6225_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32353_GM};
extern Il2CppType t1301_0_0_0;
extern Il2CppType t1301_0_0_0;
static ParameterInfo t6225_m32354_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1301_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32354_GM;
MethodInfo m32354_MI = 
{
	"Add", NULL, &t6225_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6225_m32354_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32354_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32355_GM;
MethodInfo m32355_MI = 
{
	"Clear", NULL, &t6225_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32355_GM};
extern Il2CppType t1301_0_0_0;
static ParameterInfo t6225_m32356_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1301_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32356_GM;
MethodInfo m32356_MI = 
{
	"Contains", NULL, &t6225_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6225_m32356_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32356_GM};
extern Il2CppType t3609_0_0_0;
extern Il2CppType t3609_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6225_m32357_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3609_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32357_GM;
MethodInfo m32357_MI = 
{
	"CopyTo", NULL, &t6225_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6225_m32357_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32357_GM};
extern Il2CppType t1301_0_0_0;
static ParameterInfo t6225_m32358_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1301_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32358_GM;
MethodInfo m32358_MI = 
{
	"Remove", NULL, &t6225_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6225_m32358_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32358_GM};
static MethodInfo* t6225_MIs[] =
{
	&m32352_MI,
	&m32353_MI,
	&m32354_MI,
	&m32355_MI,
	&m32356_MI,
	&m32357_MI,
	&m32358_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t6227_TI;
static TypeInfo* t6225_ITIs[] = 
{
	&t603_TI,
	&t6227_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6225_0_0_0;
extern Il2CppType t6225_1_0_0;
struct t6225;
extern Il2CppGenericClass t6225_GC;
TypeInfo t6225_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6225_MIs, t6225_PIs, NULL, NULL, NULL, NULL, NULL, &t6225_TI, t6225_ITIs, NULL, &EmptyCustomAttributesCache, &t6225_TI, &t6225_0_0_0, &t6225_1_0_0, NULL, &t6225_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.GregorianCalendarTypes>
extern Il2CppType t4789_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32359_GM;
MethodInfo m32359_MI = 
{
	"GetEnumerator", NULL, &t6227_TI, &t4789_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32359_GM};
static MethodInfo* t6227_MIs[] =
{
	&m32359_MI,
	NULL
};
static TypeInfo* t6227_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6227_0_0_0;
extern Il2CppType t6227_1_0_0;
struct t6227;
extern Il2CppGenericClass t6227_GC;
TypeInfo t6227_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6227_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6227_TI, t6227_ITIs, NULL, &EmptyCustomAttributesCache, &t6227_TI, &t6227_0_0_0, &t6227_1_0_0, NULL, &t6227_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6226_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>
extern MethodInfo m32360_MI;
extern MethodInfo m32361_MI;
static PropertyInfo t6226____Item_PropertyInfo = 
{
	&t6226_TI, "Item", &m32360_MI, &m32361_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6226_PIs[] =
{
	&t6226____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1301_0_0_0;
static ParameterInfo t6226_m32362_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1301_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32362_GM;
MethodInfo m32362_MI = 
{
	"IndexOf", NULL, &t6226_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6226_m32362_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32362_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1301_0_0_0;
static ParameterInfo t6226_m32363_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1301_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32363_GM;
MethodInfo m32363_MI = 
{
	"Insert", NULL, &t6226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6226_m32363_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32363_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6226_m32364_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32364_GM;
MethodInfo m32364_MI = 
{
	"RemoveAt", NULL, &t6226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6226_m32364_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32364_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6226_m32360_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1301_0_0_0;
extern void* RuntimeInvoker_t1301_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32360_GM;
MethodInfo m32360_MI = 
{
	"get_Item", NULL, &t6226_TI, &t1301_0_0_0, RuntimeInvoker_t1301_t44, t6226_m32360_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32360_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1301_0_0_0;
static ParameterInfo t6226_m32361_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1301_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32361_GM;
MethodInfo m32361_MI = 
{
	"set_Item", NULL, &t6226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6226_m32361_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32361_GM};
static MethodInfo* t6226_MIs[] =
{
	&m32362_MI,
	&m32363_MI,
	&m32364_MI,
	&m32360_MI,
	&m32361_MI,
	NULL
};
static TypeInfo* t6226_ITIs[] = 
{
	&t603_TI,
	&t6225_TI,
	&t6227_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6226_0_0_0;
extern Il2CppType t6226_1_0_0;
struct t6226;
extern Il2CppGenericClass t6226_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6226_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6226_MIs, t6226_PIs, NULL, NULL, NULL, NULL, NULL, &t6226_TI, t6226_ITIs, NULL, &t1908__CustomAttributeCache, &t6226_TI, &t6226_0_0_0, &t6226_1_0_0, NULL, &t6226_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4791_TI;

#include "t923.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.NumberStyles>
extern MethodInfo m32365_MI;
static PropertyInfo t4791____Current_PropertyInfo = 
{
	&t4791_TI, "Current", &m32365_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4791_PIs[] =
{
	&t4791____Current_PropertyInfo,
	NULL
};
extern Il2CppType t923_0_0_0;
extern void* RuntimeInvoker_t923 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32365_GM;
MethodInfo m32365_MI = 
{
	"get_Current", NULL, &t4791_TI, &t923_0_0_0, RuntimeInvoker_t923, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32365_GM};
static MethodInfo* t4791_MIs[] =
{
	&m32365_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4791_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4791_0_0_0;
extern Il2CppType t4791_1_0_0;
struct t4791;
extern Il2CppGenericClass t4791_GC;
TypeInfo t4791_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4791_MIs, t4791_PIs, NULL, NULL, NULL, NULL, NULL, &t4791_TI, t4791_ITIs, NULL, &EmptyCustomAttributesCache, &t4791_TI, &t4791_0_0_0, &t4791_1_0_0, NULL, &t4791_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3345.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3345_TI;
#include "t3345MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
extern TypeInfo t923_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m18601_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m24762_MI;
struct t20;
#include "t915.h"
 int32_t m24762 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18597_MI;
 void m18597 (t3345 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18598_MI;
 t29 * m18598 (t3345 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18601(__this, &m18601_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t923_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18599_MI;
 void m18599 (t3345 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18600_MI;
 bool m18600 (t3345 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18601 (t3345 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24762(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24762_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>
extern Il2CppType t20_0_0_1;
FieldInfo t3345_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3345_TI, offsetof(t3345, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3345_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3345_TI, offsetof(t3345, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3345_FIs[] =
{
	&t3345_f0_FieldInfo,
	&t3345_f1_FieldInfo,
	NULL
};
static PropertyInfo t3345____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3345_TI, "System.Collections.IEnumerator.Current", &m18598_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3345____Current_PropertyInfo = 
{
	&t3345_TI, "Current", &m18601_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3345_PIs[] =
{
	&t3345____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3345____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3345_m18597_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18597_GM;
MethodInfo m18597_MI = 
{
	".ctor", (methodPointerType)&m18597, &t3345_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3345_m18597_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18597_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18598_GM;
MethodInfo m18598_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18598, &t3345_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18598_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18599_GM;
MethodInfo m18599_MI = 
{
	"Dispose", (methodPointerType)&m18599, &t3345_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18599_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18600_GM;
MethodInfo m18600_MI = 
{
	"MoveNext", (methodPointerType)&m18600, &t3345_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18600_GM};
extern Il2CppType t923_0_0_0;
extern void* RuntimeInvoker_t923 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18601_GM;
MethodInfo m18601_MI = 
{
	"get_Current", (methodPointerType)&m18601, &t3345_TI, &t923_0_0_0, RuntimeInvoker_t923, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18601_GM};
static MethodInfo* t3345_MIs[] =
{
	&m18597_MI,
	&m18598_MI,
	&m18599_MI,
	&m18600_MI,
	&m18601_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t3345_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18598_MI,
	&m18600_MI,
	&m18599_MI,
	&m18601_MI,
};
static TypeInfo* t3345_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4791_TI,
};
static Il2CppInterfaceOffsetPair t3345_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4791_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3345_0_0_0;
extern Il2CppType t3345_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3345_GC;
extern TypeInfo t20_TI;
TypeInfo t3345_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3345_MIs, t3345_PIs, t3345_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3345_TI, t3345_ITIs, t3345_VT, &EmptyCustomAttributesCache, &t3345_TI, &t3345_0_0_0, &t3345_1_0_0, t3345_IOs, &t3345_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3345)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6228_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>
extern MethodInfo m32366_MI;
static PropertyInfo t6228____Count_PropertyInfo = 
{
	&t6228_TI, "Count", &m32366_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32367_MI;
static PropertyInfo t6228____IsReadOnly_PropertyInfo = 
{
	&t6228_TI, "IsReadOnly", &m32367_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6228_PIs[] =
{
	&t6228____Count_PropertyInfo,
	&t6228____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32366_GM;
MethodInfo m32366_MI = 
{
	"get_Count", NULL, &t6228_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32366_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32367_GM;
MethodInfo m32367_MI = 
{
	"get_IsReadOnly", NULL, &t6228_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32367_GM};
extern Il2CppType t923_0_0_0;
extern Il2CppType t923_0_0_0;
static ParameterInfo t6228_m32368_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t923_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32368_GM;
MethodInfo m32368_MI = 
{
	"Add", NULL, &t6228_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6228_m32368_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32368_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32369_GM;
MethodInfo m32369_MI = 
{
	"Clear", NULL, &t6228_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32369_GM};
extern Il2CppType t923_0_0_0;
static ParameterInfo t6228_m32370_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t923_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32370_GM;
MethodInfo m32370_MI = 
{
	"Contains", NULL, &t6228_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6228_m32370_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32370_GM};
extern Il2CppType t3610_0_0_0;
extern Il2CppType t3610_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6228_m32371_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3610_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32371_GM;
MethodInfo m32371_MI = 
{
	"CopyTo", NULL, &t6228_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6228_m32371_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32371_GM};
extern Il2CppType t923_0_0_0;
static ParameterInfo t6228_m32372_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t923_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32372_GM;
MethodInfo m32372_MI = 
{
	"Remove", NULL, &t6228_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6228_m32372_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32372_GM};
static MethodInfo* t6228_MIs[] =
{
	&m32366_MI,
	&m32367_MI,
	&m32368_MI,
	&m32369_MI,
	&m32370_MI,
	&m32371_MI,
	&m32372_MI,
	NULL
};
extern TypeInfo t6230_TI;
static TypeInfo* t6228_ITIs[] = 
{
	&t603_TI,
	&t6230_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6228_0_0_0;
extern Il2CppType t6228_1_0_0;
struct t6228;
extern Il2CppGenericClass t6228_GC;
TypeInfo t6228_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6228_MIs, t6228_PIs, NULL, NULL, NULL, NULL, NULL, &t6228_TI, t6228_ITIs, NULL, &EmptyCustomAttributesCache, &t6228_TI, &t6228_0_0_0, &t6228_1_0_0, NULL, &t6228_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.NumberStyles>
extern Il2CppType t4791_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32373_GM;
MethodInfo m32373_MI = 
{
	"GetEnumerator", NULL, &t6230_TI, &t4791_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32373_GM};
static MethodInfo* t6230_MIs[] =
{
	&m32373_MI,
	NULL
};
static TypeInfo* t6230_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6230_0_0_0;
extern Il2CppType t6230_1_0_0;
struct t6230;
extern Il2CppGenericClass t6230_GC;
TypeInfo t6230_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6230_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6230_TI, t6230_ITIs, NULL, &EmptyCustomAttributesCache, &t6230_TI, &t6230_0_0_0, &t6230_1_0_0, NULL, &t6230_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6229_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.NumberStyles>
extern MethodInfo m32374_MI;
extern MethodInfo m32375_MI;
static PropertyInfo t6229____Item_PropertyInfo = 
{
	&t6229_TI, "Item", &m32374_MI, &m32375_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6229_PIs[] =
{
	&t6229____Item_PropertyInfo,
	NULL
};
extern Il2CppType t923_0_0_0;
static ParameterInfo t6229_m32376_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t923_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32376_GM;
MethodInfo m32376_MI = 
{
	"IndexOf", NULL, &t6229_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6229_m32376_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32376_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t923_0_0_0;
static ParameterInfo t6229_m32377_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t923_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32377_GM;
MethodInfo m32377_MI = 
{
	"Insert", NULL, &t6229_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6229_m32377_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32377_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6229_m32378_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32378_GM;
MethodInfo m32378_MI = 
{
	"RemoveAt", NULL, &t6229_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6229_m32378_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32378_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6229_m32374_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t923_0_0_0;
extern void* RuntimeInvoker_t923_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32374_GM;
MethodInfo m32374_MI = 
{
	"get_Item", NULL, &t6229_TI, &t923_0_0_0, RuntimeInvoker_t923_t44, t6229_m32374_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32374_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t923_0_0_0;
static ParameterInfo t6229_m32375_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t923_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32375_GM;
MethodInfo m32375_MI = 
{
	"set_Item", NULL, &t6229_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6229_m32375_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32375_GM};
static MethodInfo* t6229_MIs[] =
{
	&m32376_MI,
	&m32377_MI,
	&m32378_MI,
	&m32374_MI,
	&m32375_MI,
	NULL
};
static TypeInfo* t6229_ITIs[] = 
{
	&t603_TI,
	&t6228_TI,
	&t6230_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6229_0_0_0;
extern Il2CppType t6229_1_0_0;
struct t6229;
extern Il2CppGenericClass t6229_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6229_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6229_MIs, t6229_PIs, NULL, NULL, NULL, NULL, NULL, &t6229_TI, t6229_ITIs, NULL, &t1908__CustomAttributeCache, &t6229_TI, &t6229_0_0_0, &t6229_1_0_0, NULL, &t6229_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4793_TI;

#include "t851.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.UnicodeCategory>
extern MethodInfo m32379_MI;
static PropertyInfo t4793____Current_PropertyInfo = 
{
	&t4793_TI, "Current", &m32379_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4793_PIs[] =
{
	&t4793____Current_PropertyInfo,
	NULL
};
extern Il2CppType t851_0_0_0;
extern void* RuntimeInvoker_t851 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32379_GM;
MethodInfo m32379_MI = 
{
	"get_Current", NULL, &t4793_TI, &t851_0_0_0, RuntimeInvoker_t851, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32379_GM};
static MethodInfo* t4793_MIs[] =
{
	&m32379_MI,
	NULL
};
static TypeInfo* t4793_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4793_0_0_0;
extern Il2CppType t4793_1_0_0;
struct t4793;
extern Il2CppGenericClass t4793_GC;
TypeInfo t4793_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4793_MIs, t4793_PIs, NULL, NULL, NULL, NULL, NULL, &t4793_TI, t4793_ITIs, NULL, &EmptyCustomAttributesCache, &t4793_TI, &t4793_0_0_0, &t4793_1_0_0, NULL, &t4793_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3346.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3346_TI;
#include "t3346MD.h"

extern TypeInfo t851_TI;
extern MethodInfo m18606_MI;
extern MethodInfo m24773_MI;
struct t20;
 int32_t m24773 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18602_MI;
 void m18602 (t3346 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18603_MI;
 t29 * m18603 (t3346 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18606(__this, &m18606_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t851_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18604_MI;
 void m18604 (t3346 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18605_MI;
 bool m18605 (t3346 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18606 (t3346 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24773(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24773_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>
extern Il2CppType t20_0_0_1;
FieldInfo t3346_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3346_TI, offsetof(t3346, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3346_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3346_TI, offsetof(t3346, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3346_FIs[] =
{
	&t3346_f0_FieldInfo,
	&t3346_f1_FieldInfo,
	NULL
};
static PropertyInfo t3346____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3346_TI, "System.Collections.IEnumerator.Current", &m18603_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3346____Current_PropertyInfo = 
{
	&t3346_TI, "Current", &m18606_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3346_PIs[] =
{
	&t3346____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3346____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3346_m18602_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18602_GM;
MethodInfo m18602_MI = 
{
	".ctor", (methodPointerType)&m18602, &t3346_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3346_m18602_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18602_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18603_GM;
MethodInfo m18603_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18603, &t3346_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18603_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18604_GM;
MethodInfo m18604_MI = 
{
	"Dispose", (methodPointerType)&m18604, &t3346_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18604_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18605_GM;
MethodInfo m18605_MI = 
{
	"MoveNext", (methodPointerType)&m18605, &t3346_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18605_GM};
extern Il2CppType t851_0_0_0;
extern void* RuntimeInvoker_t851 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18606_GM;
MethodInfo m18606_MI = 
{
	"get_Current", (methodPointerType)&m18606, &t3346_TI, &t851_0_0_0, RuntimeInvoker_t851, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18606_GM};
static MethodInfo* t3346_MIs[] =
{
	&m18602_MI,
	&m18603_MI,
	&m18604_MI,
	&m18605_MI,
	&m18606_MI,
	NULL
};
static MethodInfo* t3346_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18603_MI,
	&m18605_MI,
	&m18604_MI,
	&m18606_MI,
};
static TypeInfo* t3346_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4793_TI,
};
static Il2CppInterfaceOffsetPair t3346_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4793_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3346_0_0_0;
extern Il2CppType t3346_1_0_0;
extern Il2CppGenericClass t3346_GC;
TypeInfo t3346_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3346_MIs, t3346_PIs, t3346_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3346_TI, t3346_ITIs, t3346_VT, &EmptyCustomAttributesCache, &t3346_TI, &t3346_0_0_0, &t3346_1_0_0, t3346_IOs, &t3346_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3346)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6231_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>
extern MethodInfo m32380_MI;
static PropertyInfo t6231____Count_PropertyInfo = 
{
	&t6231_TI, "Count", &m32380_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32381_MI;
static PropertyInfo t6231____IsReadOnly_PropertyInfo = 
{
	&t6231_TI, "IsReadOnly", &m32381_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6231_PIs[] =
{
	&t6231____Count_PropertyInfo,
	&t6231____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32380_GM;
MethodInfo m32380_MI = 
{
	"get_Count", NULL, &t6231_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32380_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32381_GM;
MethodInfo m32381_MI = 
{
	"get_IsReadOnly", NULL, &t6231_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32381_GM};
extern Il2CppType t851_0_0_0;
extern Il2CppType t851_0_0_0;
static ParameterInfo t6231_m32382_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t851_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32382_GM;
MethodInfo m32382_MI = 
{
	"Add", NULL, &t6231_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6231_m32382_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32382_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32383_GM;
MethodInfo m32383_MI = 
{
	"Clear", NULL, &t6231_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32383_GM};
extern Il2CppType t851_0_0_0;
static ParameterInfo t6231_m32384_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t851_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32384_GM;
MethodInfo m32384_MI = 
{
	"Contains", NULL, &t6231_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6231_m32384_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32384_GM};
extern Il2CppType t3611_0_0_0;
extern Il2CppType t3611_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6231_m32385_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3611_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32385_GM;
MethodInfo m32385_MI = 
{
	"CopyTo", NULL, &t6231_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6231_m32385_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32385_GM};
extern Il2CppType t851_0_0_0;
static ParameterInfo t6231_m32386_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t851_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32386_GM;
MethodInfo m32386_MI = 
{
	"Remove", NULL, &t6231_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6231_m32386_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32386_GM};
static MethodInfo* t6231_MIs[] =
{
	&m32380_MI,
	&m32381_MI,
	&m32382_MI,
	&m32383_MI,
	&m32384_MI,
	&m32385_MI,
	&m32386_MI,
	NULL
};
extern TypeInfo t6233_TI;
static TypeInfo* t6231_ITIs[] = 
{
	&t603_TI,
	&t6233_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6231_0_0_0;
extern Il2CppType t6231_1_0_0;
struct t6231;
extern Il2CppGenericClass t6231_GC;
TypeInfo t6231_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6231_MIs, t6231_PIs, NULL, NULL, NULL, NULL, NULL, &t6231_TI, t6231_ITIs, NULL, &EmptyCustomAttributesCache, &t6231_TI, &t6231_0_0_0, &t6231_1_0_0, NULL, &t6231_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.UnicodeCategory>
extern Il2CppType t4793_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32387_GM;
MethodInfo m32387_MI = 
{
	"GetEnumerator", NULL, &t6233_TI, &t4793_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32387_GM};
static MethodInfo* t6233_MIs[] =
{
	&m32387_MI,
	NULL
};
static TypeInfo* t6233_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6233_0_0_0;
extern Il2CppType t6233_1_0_0;
struct t6233;
extern Il2CppGenericClass t6233_GC;
TypeInfo t6233_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6233_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6233_TI, t6233_ITIs, NULL, &EmptyCustomAttributesCache, &t6233_TI, &t6233_0_0_0, &t6233_1_0_0, NULL, &t6233_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6232_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>
extern MethodInfo m32388_MI;
extern MethodInfo m32389_MI;
static PropertyInfo t6232____Item_PropertyInfo = 
{
	&t6232_TI, "Item", &m32388_MI, &m32389_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6232_PIs[] =
{
	&t6232____Item_PropertyInfo,
	NULL
};
extern Il2CppType t851_0_0_0;
static ParameterInfo t6232_m32390_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t851_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32390_GM;
MethodInfo m32390_MI = 
{
	"IndexOf", NULL, &t6232_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6232_m32390_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32390_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t851_0_0_0;
static ParameterInfo t6232_m32391_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t851_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32391_GM;
MethodInfo m32391_MI = 
{
	"Insert", NULL, &t6232_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6232_m32391_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32391_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6232_m32392_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32392_GM;
MethodInfo m32392_MI = 
{
	"RemoveAt", NULL, &t6232_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6232_m32392_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32392_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6232_m32388_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t851_0_0_0;
extern void* RuntimeInvoker_t851_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32388_GM;
MethodInfo m32388_MI = 
{
	"get_Item", NULL, &t6232_TI, &t851_0_0_0, RuntimeInvoker_t851_t44, t6232_m32388_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32388_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t851_0_0_0;
static ParameterInfo t6232_m32389_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t851_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32389_GM;
MethodInfo m32389_MI = 
{
	"set_Item", NULL, &t6232_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6232_m32389_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32389_GM};
static MethodInfo* t6232_MIs[] =
{
	&m32390_MI,
	&m32391_MI,
	&m32392_MI,
	&m32388_MI,
	&m32389_MI,
	NULL
};
static TypeInfo* t6232_ITIs[] = 
{
	&t603_TI,
	&t6231_TI,
	&t6233_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6232_0_0_0;
extern Il2CppType t6232_1_0_0;
struct t6232;
extern Il2CppGenericClass t6232_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6232_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6232_MIs, t6232_PIs, NULL, NULL, NULL, NULL, NULL, &t6232_TI, t6232_ITIs, NULL, &t1908__CustomAttributeCache, &t6232_TI, &t6232_0_0_0, &t6232_1_0_0, NULL, &t6232_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4795_TI;

#include "t1306.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileAttributes>
extern MethodInfo m32393_MI;
static PropertyInfo t4795____Current_PropertyInfo = 
{
	&t4795_TI, "Current", &m32393_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4795_PIs[] =
{
	&t4795____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1306_0_0_0;
extern void* RuntimeInvoker_t1306 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32393_GM;
MethodInfo m32393_MI = 
{
	"get_Current", NULL, &t4795_TI, &t1306_0_0_0, RuntimeInvoker_t1306, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32393_GM};
static MethodInfo* t4795_MIs[] =
{
	&m32393_MI,
	NULL
};
static TypeInfo* t4795_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4795_0_0_0;
extern Il2CppType t4795_1_0_0;
struct t4795;
extern Il2CppGenericClass t4795_GC;
TypeInfo t4795_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4795_MIs, t4795_PIs, NULL, NULL, NULL, NULL, NULL, &t4795_TI, t4795_ITIs, NULL, &EmptyCustomAttributesCache, &t4795_TI, &t4795_0_0_0, &t4795_1_0_0, NULL, &t4795_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3347.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3347_TI;
#include "t3347MD.h"

extern TypeInfo t1306_TI;
extern MethodInfo m18611_MI;
extern MethodInfo m24784_MI;
struct t20;
 int32_t m24784 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18607_MI;
 void m18607 (t3347 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18608_MI;
 t29 * m18608 (t3347 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18611(__this, &m18611_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1306_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18609_MI;
 void m18609 (t3347 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18610_MI;
 bool m18610 (t3347 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18611 (t3347 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24784(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24784_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileAttributes>
extern Il2CppType t20_0_0_1;
FieldInfo t3347_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3347_TI, offsetof(t3347, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3347_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3347_TI, offsetof(t3347, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3347_FIs[] =
{
	&t3347_f0_FieldInfo,
	&t3347_f1_FieldInfo,
	NULL
};
static PropertyInfo t3347____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3347_TI, "System.Collections.IEnumerator.Current", &m18608_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3347____Current_PropertyInfo = 
{
	&t3347_TI, "Current", &m18611_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3347_PIs[] =
{
	&t3347____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3347____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3347_m18607_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18607_GM;
MethodInfo m18607_MI = 
{
	".ctor", (methodPointerType)&m18607, &t3347_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3347_m18607_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18607_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18608_GM;
MethodInfo m18608_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18608, &t3347_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18608_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18609_GM;
MethodInfo m18609_MI = 
{
	"Dispose", (methodPointerType)&m18609, &t3347_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18609_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18610_GM;
MethodInfo m18610_MI = 
{
	"MoveNext", (methodPointerType)&m18610, &t3347_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18610_GM};
extern Il2CppType t1306_0_0_0;
extern void* RuntimeInvoker_t1306 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18611_GM;
MethodInfo m18611_MI = 
{
	"get_Current", (methodPointerType)&m18611, &t3347_TI, &t1306_0_0_0, RuntimeInvoker_t1306, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18611_GM};
static MethodInfo* t3347_MIs[] =
{
	&m18607_MI,
	&m18608_MI,
	&m18609_MI,
	&m18610_MI,
	&m18611_MI,
	NULL
};
static MethodInfo* t3347_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18608_MI,
	&m18610_MI,
	&m18609_MI,
	&m18611_MI,
};
static TypeInfo* t3347_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4795_TI,
};
static Il2CppInterfaceOffsetPair t3347_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4795_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3347_0_0_0;
extern Il2CppType t3347_1_0_0;
extern Il2CppGenericClass t3347_GC;
TypeInfo t3347_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3347_MIs, t3347_PIs, t3347_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3347_TI, t3347_ITIs, t3347_VT, &EmptyCustomAttributesCache, &t3347_TI, &t3347_0_0_0, &t3347_1_0_0, t3347_IOs, &t3347_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3347)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6234_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileAttributes>
extern MethodInfo m32394_MI;
static PropertyInfo t6234____Count_PropertyInfo = 
{
	&t6234_TI, "Count", &m32394_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32395_MI;
static PropertyInfo t6234____IsReadOnly_PropertyInfo = 
{
	&t6234_TI, "IsReadOnly", &m32395_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6234_PIs[] =
{
	&t6234____Count_PropertyInfo,
	&t6234____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32394_GM;
MethodInfo m32394_MI = 
{
	"get_Count", NULL, &t6234_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32394_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32395_GM;
MethodInfo m32395_MI = 
{
	"get_IsReadOnly", NULL, &t6234_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32395_GM};
extern Il2CppType t1306_0_0_0;
extern Il2CppType t1306_0_0_0;
static ParameterInfo t6234_m32396_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1306_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32396_GM;
MethodInfo m32396_MI = 
{
	"Add", NULL, &t6234_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6234_m32396_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32396_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32397_GM;
MethodInfo m32397_MI = 
{
	"Clear", NULL, &t6234_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32397_GM};
extern Il2CppType t1306_0_0_0;
static ParameterInfo t6234_m32398_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1306_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32398_GM;
MethodInfo m32398_MI = 
{
	"Contains", NULL, &t6234_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6234_m32398_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32398_GM};
extern Il2CppType t3612_0_0_0;
extern Il2CppType t3612_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6234_m32399_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3612_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32399_GM;
MethodInfo m32399_MI = 
{
	"CopyTo", NULL, &t6234_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6234_m32399_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32399_GM};
extern Il2CppType t1306_0_0_0;
static ParameterInfo t6234_m32400_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1306_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32400_GM;
MethodInfo m32400_MI = 
{
	"Remove", NULL, &t6234_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6234_m32400_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32400_GM};
static MethodInfo* t6234_MIs[] =
{
	&m32394_MI,
	&m32395_MI,
	&m32396_MI,
	&m32397_MI,
	&m32398_MI,
	&m32399_MI,
	&m32400_MI,
	NULL
};
extern TypeInfo t6236_TI;
static TypeInfo* t6234_ITIs[] = 
{
	&t603_TI,
	&t6236_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6234_0_0_0;
extern Il2CppType t6234_1_0_0;
struct t6234;
extern Il2CppGenericClass t6234_GC;
TypeInfo t6234_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6234_MIs, t6234_PIs, NULL, NULL, NULL, NULL, NULL, &t6234_TI, t6234_ITIs, NULL, &EmptyCustomAttributesCache, &t6234_TI, &t6234_0_0_0, &t6234_1_0_0, NULL, &t6234_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileAttributes>
extern Il2CppType t4795_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32401_GM;
MethodInfo m32401_MI = 
{
	"GetEnumerator", NULL, &t6236_TI, &t4795_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32401_GM};
static MethodInfo* t6236_MIs[] =
{
	&m32401_MI,
	NULL
};
static TypeInfo* t6236_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6236_0_0_0;
extern Il2CppType t6236_1_0_0;
struct t6236;
extern Il2CppGenericClass t6236_GC;
TypeInfo t6236_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6236_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6236_TI, t6236_ITIs, NULL, &EmptyCustomAttributesCache, &t6236_TI, &t6236_0_0_0, &t6236_1_0_0, NULL, &t6236_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6235_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileAttributes>
extern MethodInfo m32402_MI;
extern MethodInfo m32403_MI;
static PropertyInfo t6235____Item_PropertyInfo = 
{
	&t6235_TI, "Item", &m32402_MI, &m32403_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6235_PIs[] =
{
	&t6235____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1306_0_0_0;
static ParameterInfo t6235_m32404_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1306_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32404_GM;
MethodInfo m32404_MI = 
{
	"IndexOf", NULL, &t6235_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6235_m32404_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32404_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1306_0_0_0;
static ParameterInfo t6235_m32405_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1306_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32405_GM;
MethodInfo m32405_MI = 
{
	"Insert", NULL, &t6235_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6235_m32405_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32405_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6235_m32406_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32406_GM;
MethodInfo m32406_MI = 
{
	"RemoveAt", NULL, &t6235_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6235_m32406_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32406_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6235_m32402_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1306_0_0_0;
extern void* RuntimeInvoker_t1306_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32402_GM;
MethodInfo m32402_MI = 
{
	"get_Item", NULL, &t6235_TI, &t1306_0_0_0, RuntimeInvoker_t1306_t44, t6235_m32402_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32402_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1306_0_0_0;
static ParameterInfo t6235_m32403_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1306_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32403_GM;
MethodInfo m32403_MI = 
{
	"set_Item", NULL, &t6235_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6235_m32403_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32403_GM};
static MethodInfo* t6235_MIs[] =
{
	&m32404_MI,
	&m32405_MI,
	&m32406_MI,
	&m32402_MI,
	&m32403_MI,
	NULL
};
static TypeInfo* t6235_ITIs[] = 
{
	&t603_TI,
	&t6234_TI,
	&t6236_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6235_0_0_0;
extern Il2CppType t6235_1_0_0;
struct t6235;
extern Il2CppGenericClass t6235_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6235_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6235_MIs, t6235_PIs, NULL, NULL, NULL, NULL, NULL, &t6235_TI, t6235_ITIs, NULL, &t1908__CustomAttributeCache, &t6235_TI, &t6235_0_0_0, &t6235_1_0_0, NULL, &t6235_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4797_TI;

#include "t1311.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileMode>
extern MethodInfo m32407_MI;
static PropertyInfo t4797____Current_PropertyInfo = 
{
	&t4797_TI, "Current", &m32407_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4797_PIs[] =
{
	&t4797____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1311_0_0_0;
extern void* RuntimeInvoker_t1311 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32407_GM;
MethodInfo m32407_MI = 
{
	"get_Current", NULL, &t4797_TI, &t1311_0_0_0, RuntimeInvoker_t1311, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32407_GM};
static MethodInfo* t4797_MIs[] =
{
	&m32407_MI,
	NULL
};
static TypeInfo* t4797_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4797_0_0_0;
extern Il2CppType t4797_1_0_0;
struct t4797;
extern Il2CppGenericClass t4797_GC;
TypeInfo t4797_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4797_MIs, t4797_PIs, NULL, NULL, NULL, NULL, NULL, &t4797_TI, t4797_ITIs, NULL, &EmptyCustomAttributesCache, &t4797_TI, &t4797_0_0_0, &t4797_1_0_0, NULL, &t4797_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3348.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3348_TI;
#include "t3348MD.h"

extern TypeInfo t1311_TI;
extern MethodInfo m18616_MI;
extern MethodInfo m24795_MI;
struct t20;
 int32_t m24795 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18612_MI;
 void m18612 (t3348 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18613_MI;
 t29 * m18613 (t3348 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18616(__this, &m18616_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1311_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18614_MI;
 void m18614 (t3348 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18615_MI;
 bool m18615 (t3348 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18616 (t3348 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24795(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24795_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileMode>
extern Il2CppType t20_0_0_1;
FieldInfo t3348_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3348_TI, offsetof(t3348, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3348_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3348_TI, offsetof(t3348, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3348_FIs[] =
{
	&t3348_f0_FieldInfo,
	&t3348_f1_FieldInfo,
	NULL
};
static PropertyInfo t3348____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3348_TI, "System.Collections.IEnumerator.Current", &m18613_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3348____Current_PropertyInfo = 
{
	&t3348_TI, "Current", &m18616_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3348_PIs[] =
{
	&t3348____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3348____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3348_m18612_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18612_GM;
MethodInfo m18612_MI = 
{
	".ctor", (methodPointerType)&m18612, &t3348_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3348_m18612_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18612_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18613_GM;
MethodInfo m18613_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18613, &t3348_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18613_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18614_GM;
MethodInfo m18614_MI = 
{
	"Dispose", (methodPointerType)&m18614, &t3348_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18614_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18615_GM;
MethodInfo m18615_MI = 
{
	"MoveNext", (methodPointerType)&m18615, &t3348_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18615_GM};
extern Il2CppType t1311_0_0_0;
extern void* RuntimeInvoker_t1311 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18616_GM;
MethodInfo m18616_MI = 
{
	"get_Current", (methodPointerType)&m18616, &t3348_TI, &t1311_0_0_0, RuntimeInvoker_t1311, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18616_GM};
static MethodInfo* t3348_MIs[] =
{
	&m18612_MI,
	&m18613_MI,
	&m18614_MI,
	&m18615_MI,
	&m18616_MI,
	NULL
};
static MethodInfo* t3348_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18613_MI,
	&m18615_MI,
	&m18614_MI,
	&m18616_MI,
};
static TypeInfo* t3348_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4797_TI,
};
static Il2CppInterfaceOffsetPair t3348_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4797_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3348_0_0_0;
extern Il2CppType t3348_1_0_0;
extern Il2CppGenericClass t3348_GC;
TypeInfo t3348_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3348_MIs, t3348_PIs, t3348_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3348_TI, t3348_ITIs, t3348_VT, &EmptyCustomAttributesCache, &t3348_TI, &t3348_0_0_0, &t3348_1_0_0, t3348_IOs, &t3348_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3348)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6237_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileMode>
extern MethodInfo m32408_MI;
static PropertyInfo t6237____Count_PropertyInfo = 
{
	&t6237_TI, "Count", &m32408_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32409_MI;
static PropertyInfo t6237____IsReadOnly_PropertyInfo = 
{
	&t6237_TI, "IsReadOnly", &m32409_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6237_PIs[] =
{
	&t6237____Count_PropertyInfo,
	&t6237____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32408_GM;
MethodInfo m32408_MI = 
{
	"get_Count", NULL, &t6237_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32408_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32409_GM;
MethodInfo m32409_MI = 
{
	"get_IsReadOnly", NULL, &t6237_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32409_GM};
extern Il2CppType t1311_0_0_0;
extern Il2CppType t1311_0_0_0;
static ParameterInfo t6237_m32410_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1311_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32410_GM;
MethodInfo m32410_MI = 
{
	"Add", NULL, &t6237_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6237_m32410_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32410_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32411_GM;
MethodInfo m32411_MI = 
{
	"Clear", NULL, &t6237_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32411_GM};
extern Il2CppType t1311_0_0_0;
static ParameterInfo t6237_m32412_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1311_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32412_GM;
MethodInfo m32412_MI = 
{
	"Contains", NULL, &t6237_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6237_m32412_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32412_GM};
extern Il2CppType t3613_0_0_0;
extern Il2CppType t3613_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6237_m32413_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3613_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32413_GM;
MethodInfo m32413_MI = 
{
	"CopyTo", NULL, &t6237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6237_m32413_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32413_GM};
extern Il2CppType t1311_0_0_0;
static ParameterInfo t6237_m32414_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1311_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32414_GM;
MethodInfo m32414_MI = 
{
	"Remove", NULL, &t6237_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6237_m32414_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32414_GM};
static MethodInfo* t6237_MIs[] =
{
	&m32408_MI,
	&m32409_MI,
	&m32410_MI,
	&m32411_MI,
	&m32412_MI,
	&m32413_MI,
	&m32414_MI,
	NULL
};
extern TypeInfo t6239_TI;
static TypeInfo* t6237_ITIs[] = 
{
	&t603_TI,
	&t6239_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6237_0_0_0;
extern Il2CppType t6237_1_0_0;
struct t6237;
extern Il2CppGenericClass t6237_GC;
TypeInfo t6237_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6237_MIs, t6237_PIs, NULL, NULL, NULL, NULL, NULL, &t6237_TI, t6237_ITIs, NULL, &EmptyCustomAttributesCache, &t6237_TI, &t6237_0_0_0, &t6237_1_0_0, NULL, &t6237_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileMode>
extern Il2CppType t4797_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32415_GM;
MethodInfo m32415_MI = 
{
	"GetEnumerator", NULL, &t6239_TI, &t4797_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32415_GM};
static MethodInfo* t6239_MIs[] =
{
	&m32415_MI,
	NULL
};
static TypeInfo* t6239_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6239_0_0_0;
extern Il2CppType t6239_1_0_0;
struct t6239;
extern Il2CppGenericClass t6239_GC;
TypeInfo t6239_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6239_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6239_TI, t6239_ITIs, NULL, &EmptyCustomAttributesCache, &t6239_TI, &t6239_0_0_0, &t6239_1_0_0, NULL, &t6239_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6238_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileMode>
extern MethodInfo m32416_MI;
extern MethodInfo m32417_MI;
static PropertyInfo t6238____Item_PropertyInfo = 
{
	&t6238_TI, "Item", &m32416_MI, &m32417_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6238_PIs[] =
{
	&t6238____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1311_0_0_0;
static ParameterInfo t6238_m32418_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1311_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32418_GM;
MethodInfo m32418_MI = 
{
	"IndexOf", NULL, &t6238_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6238_m32418_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32418_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1311_0_0_0;
static ParameterInfo t6238_m32419_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1311_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32419_GM;
MethodInfo m32419_MI = 
{
	"Insert", NULL, &t6238_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6238_m32419_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32419_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6238_m32420_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32420_GM;
MethodInfo m32420_MI = 
{
	"RemoveAt", NULL, &t6238_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6238_m32420_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32420_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6238_m32416_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1311_0_0_0;
extern void* RuntimeInvoker_t1311_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32416_GM;
MethodInfo m32416_MI = 
{
	"get_Item", NULL, &t6238_TI, &t1311_0_0_0, RuntimeInvoker_t1311_t44, t6238_m32416_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32416_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1311_0_0_0;
static ParameterInfo t6238_m32417_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1311_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32417_GM;
MethodInfo m32417_MI = 
{
	"set_Item", NULL, &t6238_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6238_m32417_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32417_GM};
static MethodInfo* t6238_MIs[] =
{
	&m32418_MI,
	&m32419_MI,
	&m32420_MI,
	&m32416_MI,
	&m32417_MI,
	NULL
};
static TypeInfo* t6238_ITIs[] = 
{
	&t603_TI,
	&t6237_TI,
	&t6239_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6238_0_0_0;
extern Il2CppType t6238_1_0_0;
struct t6238;
extern Il2CppGenericClass t6238_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6238_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6238_MIs, t6238_PIs, NULL, NULL, NULL, NULL, NULL, &t6238_TI, t6238_ITIs, NULL, &t1908__CustomAttributeCache, &t6238_TI, &t6238_0_0_0, &t6238_1_0_0, NULL, &t6238_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4799_TI;

#include "t1313.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileOptions>
extern MethodInfo m32421_MI;
static PropertyInfo t4799____Current_PropertyInfo = 
{
	&t4799_TI, "Current", &m32421_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4799_PIs[] =
{
	&t4799____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1313_0_0_0;
extern void* RuntimeInvoker_t1313 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32421_GM;
MethodInfo m32421_MI = 
{
	"get_Current", NULL, &t4799_TI, &t1313_0_0_0, RuntimeInvoker_t1313, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32421_GM};
static MethodInfo* t4799_MIs[] =
{
	&m32421_MI,
	NULL
};
static TypeInfo* t4799_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4799_0_0_0;
extern Il2CppType t4799_1_0_0;
struct t4799;
extern Il2CppGenericClass t4799_GC;
TypeInfo t4799_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4799_MIs, t4799_PIs, NULL, NULL, NULL, NULL, NULL, &t4799_TI, t4799_ITIs, NULL, &EmptyCustomAttributesCache, &t4799_TI, &t4799_0_0_0, &t4799_1_0_0, NULL, &t4799_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3349.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3349_TI;
#include "t3349MD.h"

extern TypeInfo t1313_TI;
extern MethodInfo m18621_MI;
extern MethodInfo m24806_MI;
struct t20;
 int32_t m24806 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18617_MI;
 void m18617 (t3349 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18618_MI;
 t29 * m18618 (t3349 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18621(__this, &m18621_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1313_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18619_MI;
 void m18619 (t3349 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18620_MI;
 bool m18620 (t3349 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18621 (t3349 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24806(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24806_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileOptions>
extern Il2CppType t20_0_0_1;
FieldInfo t3349_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3349_TI, offsetof(t3349, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3349_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3349_TI, offsetof(t3349, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3349_FIs[] =
{
	&t3349_f0_FieldInfo,
	&t3349_f1_FieldInfo,
	NULL
};
static PropertyInfo t3349____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3349_TI, "System.Collections.IEnumerator.Current", &m18618_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3349____Current_PropertyInfo = 
{
	&t3349_TI, "Current", &m18621_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3349_PIs[] =
{
	&t3349____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3349____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3349_m18617_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18617_GM;
MethodInfo m18617_MI = 
{
	".ctor", (methodPointerType)&m18617, &t3349_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3349_m18617_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18617_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18618_GM;
MethodInfo m18618_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18618, &t3349_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18618_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18619_GM;
MethodInfo m18619_MI = 
{
	"Dispose", (methodPointerType)&m18619, &t3349_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18619_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18620_GM;
MethodInfo m18620_MI = 
{
	"MoveNext", (methodPointerType)&m18620, &t3349_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18620_GM};
extern Il2CppType t1313_0_0_0;
extern void* RuntimeInvoker_t1313 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18621_GM;
MethodInfo m18621_MI = 
{
	"get_Current", (methodPointerType)&m18621, &t3349_TI, &t1313_0_0_0, RuntimeInvoker_t1313, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18621_GM};
static MethodInfo* t3349_MIs[] =
{
	&m18617_MI,
	&m18618_MI,
	&m18619_MI,
	&m18620_MI,
	&m18621_MI,
	NULL
};
static MethodInfo* t3349_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18618_MI,
	&m18620_MI,
	&m18619_MI,
	&m18621_MI,
};
static TypeInfo* t3349_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4799_TI,
};
static Il2CppInterfaceOffsetPair t3349_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4799_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3349_0_0_0;
extern Il2CppType t3349_1_0_0;
extern Il2CppGenericClass t3349_GC;
TypeInfo t3349_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3349_MIs, t3349_PIs, t3349_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3349_TI, t3349_ITIs, t3349_VT, &EmptyCustomAttributesCache, &t3349_TI, &t3349_0_0_0, &t3349_1_0_0, t3349_IOs, &t3349_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3349)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6240_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileOptions>
extern MethodInfo m32422_MI;
static PropertyInfo t6240____Count_PropertyInfo = 
{
	&t6240_TI, "Count", &m32422_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32423_MI;
static PropertyInfo t6240____IsReadOnly_PropertyInfo = 
{
	&t6240_TI, "IsReadOnly", &m32423_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6240_PIs[] =
{
	&t6240____Count_PropertyInfo,
	&t6240____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32422_GM;
MethodInfo m32422_MI = 
{
	"get_Count", NULL, &t6240_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32422_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32423_GM;
MethodInfo m32423_MI = 
{
	"get_IsReadOnly", NULL, &t6240_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32423_GM};
extern Il2CppType t1313_0_0_0;
extern Il2CppType t1313_0_0_0;
static ParameterInfo t6240_m32424_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1313_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32424_GM;
MethodInfo m32424_MI = 
{
	"Add", NULL, &t6240_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6240_m32424_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32424_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32425_GM;
MethodInfo m32425_MI = 
{
	"Clear", NULL, &t6240_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32425_GM};
extern Il2CppType t1313_0_0_0;
static ParameterInfo t6240_m32426_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1313_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32426_GM;
MethodInfo m32426_MI = 
{
	"Contains", NULL, &t6240_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6240_m32426_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32426_GM};
extern Il2CppType t3614_0_0_0;
extern Il2CppType t3614_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6240_m32427_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3614_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32427_GM;
MethodInfo m32427_MI = 
{
	"CopyTo", NULL, &t6240_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6240_m32427_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32427_GM};
extern Il2CppType t1313_0_0_0;
static ParameterInfo t6240_m32428_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1313_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32428_GM;
MethodInfo m32428_MI = 
{
	"Remove", NULL, &t6240_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6240_m32428_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32428_GM};
static MethodInfo* t6240_MIs[] =
{
	&m32422_MI,
	&m32423_MI,
	&m32424_MI,
	&m32425_MI,
	&m32426_MI,
	&m32427_MI,
	&m32428_MI,
	NULL
};
extern TypeInfo t6242_TI;
static TypeInfo* t6240_ITIs[] = 
{
	&t603_TI,
	&t6242_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6240_0_0_0;
extern Il2CppType t6240_1_0_0;
struct t6240;
extern Il2CppGenericClass t6240_GC;
TypeInfo t6240_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6240_MIs, t6240_PIs, NULL, NULL, NULL, NULL, NULL, &t6240_TI, t6240_ITIs, NULL, &EmptyCustomAttributesCache, &t6240_TI, &t6240_0_0_0, &t6240_1_0_0, NULL, &t6240_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileOptions>
extern Il2CppType t4799_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32429_GM;
MethodInfo m32429_MI = 
{
	"GetEnumerator", NULL, &t6242_TI, &t4799_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32429_GM};
static MethodInfo* t6242_MIs[] =
{
	&m32429_MI,
	NULL
};
static TypeInfo* t6242_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6242_0_0_0;
extern Il2CppType t6242_1_0_0;
struct t6242;
extern Il2CppGenericClass t6242_GC;
TypeInfo t6242_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6242_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6242_TI, t6242_ITIs, NULL, &EmptyCustomAttributesCache, &t6242_TI, &t6242_0_0_0, &t6242_1_0_0, NULL, &t6242_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6241_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileOptions>
extern MethodInfo m32430_MI;
extern MethodInfo m32431_MI;
static PropertyInfo t6241____Item_PropertyInfo = 
{
	&t6241_TI, "Item", &m32430_MI, &m32431_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6241_PIs[] =
{
	&t6241____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1313_0_0_0;
static ParameterInfo t6241_m32432_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1313_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32432_GM;
MethodInfo m32432_MI = 
{
	"IndexOf", NULL, &t6241_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6241_m32432_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32432_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1313_0_0_0;
static ParameterInfo t6241_m32433_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1313_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32433_GM;
MethodInfo m32433_MI = 
{
	"Insert", NULL, &t6241_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6241_m32433_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32433_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6241_m32434_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32434_GM;
MethodInfo m32434_MI = 
{
	"RemoveAt", NULL, &t6241_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6241_m32434_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32434_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6241_m32430_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1313_0_0_0;
extern void* RuntimeInvoker_t1313_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32430_GM;
MethodInfo m32430_MI = 
{
	"get_Item", NULL, &t6241_TI, &t1313_0_0_0, RuntimeInvoker_t1313_t44, t6241_m32430_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32430_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1313_0_0_0;
static ParameterInfo t6241_m32431_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1313_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32431_GM;
MethodInfo m32431_MI = 
{
	"set_Item", NULL, &t6241_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6241_m32431_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32431_GM};
static MethodInfo* t6241_MIs[] =
{
	&m32432_MI,
	&m32433_MI,
	&m32434_MI,
	&m32430_MI,
	&m32431_MI,
	NULL
};
static TypeInfo* t6241_ITIs[] = 
{
	&t603_TI,
	&t6240_TI,
	&t6242_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6241_0_0_0;
extern Il2CppType t6241_1_0_0;
struct t6241;
extern Il2CppGenericClass t6241_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6241_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6241_MIs, t6241_PIs, NULL, NULL, NULL, NULL, NULL, &t6241_TI, t6241_ITIs, NULL, &t1908__CustomAttributeCache, &t6241_TI, &t6241_0_0_0, &t6241_1_0_0, NULL, &t6241_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4801_TI;

#include "t1314.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileShare>
extern MethodInfo m32435_MI;
static PropertyInfo t4801____Current_PropertyInfo = 
{
	&t4801_TI, "Current", &m32435_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4801_PIs[] =
{
	&t4801____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1314_0_0_0;
extern void* RuntimeInvoker_t1314 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32435_GM;
MethodInfo m32435_MI = 
{
	"get_Current", NULL, &t4801_TI, &t1314_0_0_0, RuntimeInvoker_t1314, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32435_GM};
static MethodInfo* t4801_MIs[] =
{
	&m32435_MI,
	NULL
};
static TypeInfo* t4801_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4801_0_0_0;
extern Il2CppType t4801_1_0_0;
struct t4801;
extern Il2CppGenericClass t4801_GC;
TypeInfo t4801_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4801_MIs, t4801_PIs, NULL, NULL, NULL, NULL, NULL, &t4801_TI, t4801_ITIs, NULL, &EmptyCustomAttributesCache, &t4801_TI, &t4801_0_0_0, &t4801_1_0_0, NULL, &t4801_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3350.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3350_TI;
#include "t3350MD.h"

extern TypeInfo t1314_TI;
extern MethodInfo m18626_MI;
extern MethodInfo m24817_MI;
struct t20;
 int32_t m24817 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18622_MI;
 void m18622 (t3350 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18623_MI;
 t29 * m18623 (t3350 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18626(__this, &m18626_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1314_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18624_MI;
 void m18624 (t3350 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18625_MI;
 bool m18625 (t3350 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18626 (t3350 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24817(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24817_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileShare>
extern Il2CppType t20_0_0_1;
FieldInfo t3350_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3350_TI, offsetof(t3350, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3350_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3350_TI, offsetof(t3350, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3350_FIs[] =
{
	&t3350_f0_FieldInfo,
	&t3350_f1_FieldInfo,
	NULL
};
static PropertyInfo t3350____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3350_TI, "System.Collections.IEnumerator.Current", &m18623_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3350____Current_PropertyInfo = 
{
	&t3350_TI, "Current", &m18626_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3350_PIs[] =
{
	&t3350____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3350____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3350_m18622_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18622_GM;
MethodInfo m18622_MI = 
{
	".ctor", (methodPointerType)&m18622, &t3350_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3350_m18622_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18622_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18623_GM;
MethodInfo m18623_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18623, &t3350_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18623_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18624_GM;
MethodInfo m18624_MI = 
{
	"Dispose", (methodPointerType)&m18624, &t3350_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18624_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18625_GM;
MethodInfo m18625_MI = 
{
	"MoveNext", (methodPointerType)&m18625, &t3350_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18625_GM};
extern Il2CppType t1314_0_0_0;
extern void* RuntimeInvoker_t1314 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18626_GM;
MethodInfo m18626_MI = 
{
	"get_Current", (methodPointerType)&m18626, &t3350_TI, &t1314_0_0_0, RuntimeInvoker_t1314, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18626_GM};
static MethodInfo* t3350_MIs[] =
{
	&m18622_MI,
	&m18623_MI,
	&m18624_MI,
	&m18625_MI,
	&m18626_MI,
	NULL
};
static MethodInfo* t3350_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18623_MI,
	&m18625_MI,
	&m18624_MI,
	&m18626_MI,
};
static TypeInfo* t3350_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4801_TI,
};
static Il2CppInterfaceOffsetPair t3350_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4801_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3350_0_0_0;
extern Il2CppType t3350_1_0_0;
extern Il2CppGenericClass t3350_GC;
TypeInfo t3350_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3350_MIs, t3350_PIs, t3350_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3350_TI, t3350_ITIs, t3350_VT, &EmptyCustomAttributesCache, &t3350_TI, &t3350_0_0_0, &t3350_1_0_0, t3350_IOs, &t3350_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3350)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6243_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileShare>
extern MethodInfo m32436_MI;
static PropertyInfo t6243____Count_PropertyInfo = 
{
	&t6243_TI, "Count", &m32436_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32437_MI;
static PropertyInfo t6243____IsReadOnly_PropertyInfo = 
{
	&t6243_TI, "IsReadOnly", &m32437_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6243_PIs[] =
{
	&t6243____Count_PropertyInfo,
	&t6243____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32436_GM;
MethodInfo m32436_MI = 
{
	"get_Count", NULL, &t6243_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32436_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32437_GM;
MethodInfo m32437_MI = 
{
	"get_IsReadOnly", NULL, &t6243_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32437_GM};
extern Il2CppType t1314_0_0_0;
extern Il2CppType t1314_0_0_0;
static ParameterInfo t6243_m32438_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1314_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32438_GM;
MethodInfo m32438_MI = 
{
	"Add", NULL, &t6243_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6243_m32438_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32438_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32439_GM;
MethodInfo m32439_MI = 
{
	"Clear", NULL, &t6243_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32439_GM};
extern Il2CppType t1314_0_0_0;
static ParameterInfo t6243_m32440_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1314_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32440_GM;
MethodInfo m32440_MI = 
{
	"Contains", NULL, &t6243_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6243_m32440_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32440_GM};
extern Il2CppType t3615_0_0_0;
extern Il2CppType t3615_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6243_m32441_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3615_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32441_GM;
MethodInfo m32441_MI = 
{
	"CopyTo", NULL, &t6243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6243_m32441_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32441_GM};
extern Il2CppType t1314_0_0_0;
static ParameterInfo t6243_m32442_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1314_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32442_GM;
MethodInfo m32442_MI = 
{
	"Remove", NULL, &t6243_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6243_m32442_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32442_GM};
static MethodInfo* t6243_MIs[] =
{
	&m32436_MI,
	&m32437_MI,
	&m32438_MI,
	&m32439_MI,
	&m32440_MI,
	&m32441_MI,
	&m32442_MI,
	NULL
};
extern TypeInfo t6245_TI;
static TypeInfo* t6243_ITIs[] = 
{
	&t603_TI,
	&t6245_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6243_0_0_0;
extern Il2CppType t6243_1_0_0;
struct t6243;
extern Il2CppGenericClass t6243_GC;
TypeInfo t6243_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6243_MIs, t6243_PIs, NULL, NULL, NULL, NULL, NULL, &t6243_TI, t6243_ITIs, NULL, &EmptyCustomAttributesCache, &t6243_TI, &t6243_0_0_0, &t6243_1_0_0, NULL, &t6243_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileShare>
extern Il2CppType t4801_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32443_GM;
MethodInfo m32443_MI = 
{
	"GetEnumerator", NULL, &t6245_TI, &t4801_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32443_GM};
static MethodInfo* t6245_MIs[] =
{
	&m32443_MI,
	NULL
};
static TypeInfo* t6245_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6245_0_0_0;
extern Il2CppType t6245_1_0_0;
struct t6245;
extern Il2CppGenericClass t6245_GC;
TypeInfo t6245_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6245_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6245_TI, t6245_ITIs, NULL, &EmptyCustomAttributesCache, &t6245_TI, &t6245_0_0_0, &t6245_1_0_0, NULL, &t6245_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6244_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileShare>
extern MethodInfo m32444_MI;
extern MethodInfo m32445_MI;
static PropertyInfo t6244____Item_PropertyInfo = 
{
	&t6244_TI, "Item", &m32444_MI, &m32445_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6244_PIs[] =
{
	&t6244____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1314_0_0_0;
static ParameterInfo t6244_m32446_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1314_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32446_GM;
MethodInfo m32446_MI = 
{
	"IndexOf", NULL, &t6244_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6244_m32446_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32446_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1314_0_0_0;
static ParameterInfo t6244_m32447_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1314_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32447_GM;
MethodInfo m32447_MI = 
{
	"Insert", NULL, &t6244_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6244_m32447_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32447_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6244_m32448_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32448_GM;
MethodInfo m32448_MI = 
{
	"RemoveAt", NULL, &t6244_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6244_m32448_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32448_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6244_m32444_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1314_0_0_0;
extern void* RuntimeInvoker_t1314_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32444_GM;
MethodInfo m32444_MI = 
{
	"get_Item", NULL, &t6244_TI, &t1314_0_0_0, RuntimeInvoker_t1314_t44, t6244_m32444_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32444_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1314_0_0_0;
static ParameterInfo t6244_m32445_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1314_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32445_GM;
MethodInfo m32445_MI = 
{
	"set_Item", NULL, &t6244_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6244_m32445_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32445_GM};
static MethodInfo* t6244_MIs[] =
{
	&m32446_MI,
	&m32447_MI,
	&m32448_MI,
	&m32444_MI,
	&m32445_MI,
	NULL
};
static TypeInfo* t6244_ITIs[] = 
{
	&t603_TI,
	&t6243_TI,
	&t6245_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6244_0_0_0;
extern Il2CppType t6244_1_0_0;
struct t6244;
extern Il2CppGenericClass t6244_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6244_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6244_MIs, t6244_PIs, NULL, NULL, NULL, NULL, NULL, &t6244_TI, t6244_ITIs, NULL, &t1908__CustomAttributeCache, &t6244_TI, &t6244_0_0_0, &t6244_1_0_0, NULL, &t6244_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4803_TI;

#include "t1319.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.MonoFileType>
extern MethodInfo m32449_MI;
static PropertyInfo t4803____Current_PropertyInfo = 
{
	&t4803_TI, "Current", &m32449_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4803_PIs[] =
{
	&t4803____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1319_0_0_0;
extern void* RuntimeInvoker_t1319 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32449_GM;
MethodInfo m32449_MI = 
{
	"get_Current", NULL, &t4803_TI, &t1319_0_0_0, RuntimeInvoker_t1319, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32449_GM};
static MethodInfo* t4803_MIs[] =
{
	&m32449_MI,
	NULL
};
static TypeInfo* t4803_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4803_0_0_0;
extern Il2CppType t4803_1_0_0;
struct t4803;
extern Il2CppGenericClass t4803_GC;
TypeInfo t4803_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4803_MIs, t4803_PIs, NULL, NULL, NULL, NULL, NULL, &t4803_TI, t4803_ITIs, NULL, &EmptyCustomAttributesCache, &t4803_TI, &t4803_0_0_0, &t4803_1_0_0, NULL, &t4803_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3351.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3351_TI;
#include "t3351MD.h"

extern TypeInfo t1319_TI;
extern MethodInfo m18631_MI;
extern MethodInfo m24828_MI;
struct t20;
 int32_t m24828 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18627_MI;
 void m18627 (t3351 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18628_MI;
 t29 * m18628 (t3351 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18631(__this, &m18631_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1319_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18629_MI;
 void m18629 (t3351 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18630_MI;
 bool m18630 (t3351 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18631 (t3351 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24828(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24828_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.MonoFileType>
extern Il2CppType t20_0_0_1;
FieldInfo t3351_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3351_TI, offsetof(t3351, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3351_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3351_TI, offsetof(t3351, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3351_FIs[] =
{
	&t3351_f0_FieldInfo,
	&t3351_f1_FieldInfo,
	NULL
};
static PropertyInfo t3351____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3351_TI, "System.Collections.IEnumerator.Current", &m18628_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3351____Current_PropertyInfo = 
{
	&t3351_TI, "Current", &m18631_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3351_PIs[] =
{
	&t3351____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3351____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3351_m18627_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18627_GM;
MethodInfo m18627_MI = 
{
	".ctor", (methodPointerType)&m18627, &t3351_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3351_m18627_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18627_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18628_GM;
MethodInfo m18628_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18628, &t3351_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18628_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18629_GM;
MethodInfo m18629_MI = 
{
	"Dispose", (methodPointerType)&m18629, &t3351_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18629_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18630_GM;
MethodInfo m18630_MI = 
{
	"MoveNext", (methodPointerType)&m18630, &t3351_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18630_GM};
extern Il2CppType t1319_0_0_0;
extern void* RuntimeInvoker_t1319 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18631_GM;
MethodInfo m18631_MI = 
{
	"get_Current", (methodPointerType)&m18631, &t3351_TI, &t1319_0_0_0, RuntimeInvoker_t1319, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18631_GM};
static MethodInfo* t3351_MIs[] =
{
	&m18627_MI,
	&m18628_MI,
	&m18629_MI,
	&m18630_MI,
	&m18631_MI,
	NULL
};
static MethodInfo* t3351_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18628_MI,
	&m18630_MI,
	&m18629_MI,
	&m18631_MI,
};
static TypeInfo* t3351_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4803_TI,
};
static Il2CppInterfaceOffsetPair t3351_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4803_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3351_0_0_0;
extern Il2CppType t3351_1_0_0;
extern Il2CppGenericClass t3351_GC;
TypeInfo t3351_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3351_MIs, t3351_PIs, t3351_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3351_TI, t3351_ITIs, t3351_VT, &EmptyCustomAttributesCache, &t3351_TI, &t3351_0_0_0, &t3351_1_0_0, t3351_IOs, &t3351_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3351)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6246_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.MonoFileType>
extern MethodInfo m32450_MI;
static PropertyInfo t6246____Count_PropertyInfo = 
{
	&t6246_TI, "Count", &m32450_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32451_MI;
static PropertyInfo t6246____IsReadOnly_PropertyInfo = 
{
	&t6246_TI, "IsReadOnly", &m32451_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6246_PIs[] =
{
	&t6246____Count_PropertyInfo,
	&t6246____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32450_GM;
MethodInfo m32450_MI = 
{
	"get_Count", NULL, &t6246_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32450_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32451_GM;
MethodInfo m32451_MI = 
{
	"get_IsReadOnly", NULL, &t6246_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32451_GM};
extern Il2CppType t1319_0_0_0;
extern Il2CppType t1319_0_0_0;
static ParameterInfo t6246_m32452_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1319_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32452_GM;
MethodInfo m32452_MI = 
{
	"Add", NULL, &t6246_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6246_m32452_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32452_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32453_GM;
MethodInfo m32453_MI = 
{
	"Clear", NULL, &t6246_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32453_GM};
extern Il2CppType t1319_0_0_0;
static ParameterInfo t6246_m32454_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1319_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32454_GM;
MethodInfo m32454_MI = 
{
	"Contains", NULL, &t6246_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6246_m32454_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32454_GM};
extern Il2CppType t3616_0_0_0;
extern Il2CppType t3616_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6246_m32455_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3616_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32455_GM;
MethodInfo m32455_MI = 
{
	"CopyTo", NULL, &t6246_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6246_m32455_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32455_GM};
extern Il2CppType t1319_0_0_0;
static ParameterInfo t6246_m32456_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1319_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32456_GM;
MethodInfo m32456_MI = 
{
	"Remove", NULL, &t6246_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6246_m32456_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32456_GM};
static MethodInfo* t6246_MIs[] =
{
	&m32450_MI,
	&m32451_MI,
	&m32452_MI,
	&m32453_MI,
	&m32454_MI,
	&m32455_MI,
	&m32456_MI,
	NULL
};
extern TypeInfo t6248_TI;
static TypeInfo* t6246_ITIs[] = 
{
	&t603_TI,
	&t6248_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6246_0_0_0;
extern Il2CppType t6246_1_0_0;
struct t6246;
extern Il2CppGenericClass t6246_GC;
TypeInfo t6246_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6246_MIs, t6246_PIs, NULL, NULL, NULL, NULL, NULL, &t6246_TI, t6246_ITIs, NULL, &EmptyCustomAttributesCache, &t6246_TI, &t6246_0_0_0, &t6246_1_0_0, NULL, &t6246_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.MonoFileType>
extern Il2CppType t4803_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32457_GM;
MethodInfo m32457_MI = 
{
	"GetEnumerator", NULL, &t6248_TI, &t4803_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32457_GM};
static MethodInfo* t6248_MIs[] =
{
	&m32457_MI,
	NULL
};
static TypeInfo* t6248_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6248_0_0_0;
extern Il2CppType t6248_1_0_0;
struct t6248;
extern Il2CppGenericClass t6248_GC;
TypeInfo t6248_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6248_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6248_TI, t6248_ITIs, NULL, &EmptyCustomAttributesCache, &t6248_TI, &t6248_0_0_0, &t6248_1_0_0, NULL, &t6248_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6247_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IO.MonoFileType>
extern MethodInfo m32458_MI;
extern MethodInfo m32459_MI;
static PropertyInfo t6247____Item_PropertyInfo = 
{
	&t6247_TI, "Item", &m32458_MI, &m32459_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6247_PIs[] =
{
	&t6247____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1319_0_0_0;
static ParameterInfo t6247_m32460_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1319_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32460_GM;
MethodInfo m32460_MI = 
{
	"IndexOf", NULL, &t6247_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6247_m32460_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32460_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1319_0_0_0;
static ParameterInfo t6247_m32461_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1319_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32461_GM;
MethodInfo m32461_MI = 
{
	"Insert", NULL, &t6247_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6247_m32461_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32461_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6247_m32462_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32462_GM;
MethodInfo m32462_MI = 
{
	"RemoveAt", NULL, &t6247_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6247_m32462_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32462_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6247_m32458_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1319_0_0_0;
extern void* RuntimeInvoker_t1319_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32458_GM;
MethodInfo m32458_MI = 
{
	"get_Item", NULL, &t6247_TI, &t1319_0_0_0, RuntimeInvoker_t1319_t44, t6247_m32458_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32458_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1319_0_0_0;
static ParameterInfo t6247_m32459_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1319_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32459_GM;
MethodInfo m32459_MI = 
{
	"set_Item", NULL, &t6247_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6247_m32459_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32459_GM};
static MethodInfo* t6247_MIs[] =
{
	&m32460_MI,
	&m32461_MI,
	&m32462_MI,
	&m32458_MI,
	&m32459_MI,
	NULL
};
static TypeInfo* t6247_ITIs[] = 
{
	&t603_TI,
	&t6246_TI,
	&t6248_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6247_0_0_0;
extern Il2CppType t6247_1_0_0;
struct t6247;
extern Il2CppGenericClass t6247_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6247_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6247_MIs, t6247_PIs, NULL, NULL, NULL, NULL, NULL, &t6247_TI, t6247_ITIs, NULL, &t1908__CustomAttributeCache, &t6247_TI, &t6247_0_0_0, &t6247_1_0_0, NULL, &t6247_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4804_TI;

#include "t1321.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.MonoIOError>
extern MethodInfo m32463_MI;
static PropertyInfo t4804____Current_PropertyInfo = 
{
	&t4804_TI, "Current", &m32463_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4804_PIs[] =
{
	&t4804____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1321_0_0_0;
extern void* RuntimeInvoker_t1321 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32463_GM;
MethodInfo m32463_MI = 
{
	"get_Current", NULL, &t4804_TI, &t1321_0_0_0, RuntimeInvoker_t1321, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32463_GM};
static MethodInfo* t4804_MIs[] =
{
	&m32463_MI,
	NULL
};
static TypeInfo* t4804_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4804_0_0_0;
extern Il2CppType t4804_1_0_0;
struct t4804;
extern Il2CppGenericClass t4804_GC;
TypeInfo t4804_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4804_MIs, t4804_PIs, NULL, NULL, NULL, NULL, NULL, &t4804_TI, t4804_ITIs, NULL, &EmptyCustomAttributesCache, &t4804_TI, &t4804_0_0_0, &t4804_1_0_0, NULL, &t4804_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3352.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3352_TI;
#include "t3352MD.h"

extern TypeInfo t1321_TI;
extern MethodInfo m18636_MI;
extern MethodInfo m24839_MI;
struct t20;
 int32_t m24839 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18632_MI;
 void m18632 (t3352 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18633_MI;
 t29 * m18633 (t3352 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18636(__this, &m18636_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1321_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18634_MI;
 void m18634 (t3352 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18635_MI;
 bool m18635 (t3352 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18636 (t3352 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24839(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24839_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.MonoIOError>
extern Il2CppType t20_0_0_1;
FieldInfo t3352_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3352_TI, offsetof(t3352, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3352_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3352_TI, offsetof(t3352, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3352_FIs[] =
{
	&t3352_f0_FieldInfo,
	&t3352_f1_FieldInfo,
	NULL
};
static PropertyInfo t3352____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3352_TI, "System.Collections.IEnumerator.Current", &m18633_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3352____Current_PropertyInfo = 
{
	&t3352_TI, "Current", &m18636_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3352_PIs[] =
{
	&t3352____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3352____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3352_m18632_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18632_GM;
MethodInfo m18632_MI = 
{
	".ctor", (methodPointerType)&m18632, &t3352_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3352_m18632_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18632_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18633_GM;
MethodInfo m18633_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18633, &t3352_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18633_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18634_GM;
MethodInfo m18634_MI = 
{
	"Dispose", (methodPointerType)&m18634, &t3352_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18634_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18635_GM;
MethodInfo m18635_MI = 
{
	"MoveNext", (methodPointerType)&m18635, &t3352_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18635_GM};
extern Il2CppType t1321_0_0_0;
extern void* RuntimeInvoker_t1321 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18636_GM;
MethodInfo m18636_MI = 
{
	"get_Current", (methodPointerType)&m18636, &t3352_TI, &t1321_0_0_0, RuntimeInvoker_t1321, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18636_GM};
static MethodInfo* t3352_MIs[] =
{
	&m18632_MI,
	&m18633_MI,
	&m18634_MI,
	&m18635_MI,
	&m18636_MI,
	NULL
};
static MethodInfo* t3352_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18633_MI,
	&m18635_MI,
	&m18634_MI,
	&m18636_MI,
};
static TypeInfo* t3352_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4804_TI,
};
static Il2CppInterfaceOffsetPair t3352_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4804_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3352_0_0_0;
extern Il2CppType t3352_1_0_0;
extern Il2CppGenericClass t3352_GC;
TypeInfo t3352_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3352_MIs, t3352_PIs, t3352_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3352_TI, t3352_ITIs, t3352_VT, &EmptyCustomAttributesCache, &t3352_TI, &t3352_0_0_0, &t3352_1_0_0, t3352_IOs, &t3352_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3352)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6249_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.MonoIOError>
extern MethodInfo m32464_MI;
static PropertyInfo t6249____Count_PropertyInfo = 
{
	&t6249_TI, "Count", &m32464_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32465_MI;
static PropertyInfo t6249____IsReadOnly_PropertyInfo = 
{
	&t6249_TI, "IsReadOnly", &m32465_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6249_PIs[] =
{
	&t6249____Count_PropertyInfo,
	&t6249____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32464_GM;
MethodInfo m32464_MI = 
{
	"get_Count", NULL, &t6249_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32464_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32465_GM;
MethodInfo m32465_MI = 
{
	"get_IsReadOnly", NULL, &t6249_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32465_GM};
extern Il2CppType t1321_0_0_0;
extern Il2CppType t1321_0_0_0;
static ParameterInfo t6249_m32466_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1321_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32466_GM;
MethodInfo m32466_MI = 
{
	"Add", NULL, &t6249_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6249_m32466_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32466_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32467_GM;
MethodInfo m32467_MI = 
{
	"Clear", NULL, &t6249_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32467_GM};
extern Il2CppType t1321_0_0_0;
static ParameterInfo t6249_m32468_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1321_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32468_GM;
MethodInfo m32468_MI = 
{
	"Contains", NULL, &t6249_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6249_m32468_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32468_GM};
extern Il2CppType t3617_0_0_0;
extern Il2CppType t3617_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6249_m32469_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3617_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32469_GM;
MethodInfo m32469_MI = 
{
	"CopyTo", NULL, &t6249_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6249_m32469_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32469_GM};
extern Il2CppType t1321_0_0_0;
static ParameterInfo t6249_m32470_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1321_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32470_GM;
MethodInfo m32470_MI = 
{
	"Remove", NULL, &t6249_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6249_m32470_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32470_GM};
static MethodInfo* t6249_MIs[] =
{
	&m32464_MI,
	&m32465_MI,
	&m32466_MI,
	&m32467_MI,
	&m32468_MI,
	&m32469_MI,
	&m32470_MI,
	NULL
};
extern TypeInfo t6251_TI;
static TypeInfo* t6249_ITIs[] = 
{
	&t603_TI,
	&t6251_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6249_0_0_0;
extern Il2CppType t6249_1_0_0;
struct t6249;
extern Il2CppGenericClass t6249_GC;
TypeInfo t6249_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6249_MIs, t6249_PIs, NULL, NULL, NULL, NULL, NULL, &t6249_TI, t6249_ITIs, NULL, &EmptyCustomAttributesCache, &t6249_TI, &t6249_0_0_0, &t6249_1_0_0, NULL, &t6249_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.MonoIOError>
extern Il2CppType t4804_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32471_GM;
MethodInfo m32471_MI = 
{
	"GetEnumerator", NULL, &t6251_TI, &t4804_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32471_GM};
static MethodInfo* t6251_MIs[] =
{
	&m32471_MI,
	NULL
};
static TypeInfo* t6251_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6251_0_0_0;
extern Il2CppType t6251_1_0_0;
struct t6251;
extern Il2CppGenericClass t6251_GC;
TypeInfo t6251_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6251_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6251_TI, t6251_ITIs, NULL, &EmptyCustomAttributesCache, &t6251_TI, &t6251_0_0_0, &t6251_1_0_0, NULL, &t6251_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6250_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IO.MonoIOError>
extern MethodInfo m32472_MI;
extern MethodInfo m32473_MI;
static PropertyInfo t6250____Item_PropertyInfo = 
{
	&t6250_TI, "Item", &m32472_MI, &m32473_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6250_PIs[] =
{
	&t6250____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1321_0_0_0;
static ParameterInfo t6250_m32474_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1321_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32474_GM;
MethodInfo m32474_MI = 
{
	"IndexOf", NULL, &t6250_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6250_m32474_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32474_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1321_0_0_0;
static ParameterInfo t6250_m32475_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1321_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32475_GM;
MethodInfo m32475_MI = 
{
	"Insert", NULL, &t6250_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6250_m32475_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32475_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6250_m32476_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32476_GM;
MethodInfo m32476_MI = 
{
	"RemoveAt", NULL, &t6250_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6250_m32476_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32476_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6250_m32472_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1321_0_0_0;
extern void* RuntimeInvoker_t1321_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32472_GM;
MethodInfo m32472_MI = 
{
	"get_Item", NULL, &t6250_TI, &t1321_0_0_0, RuntimeInvoker_t1321_t44, t6250_m32472_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32472_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1321_0_0_0;
static ParameterInfo t6250_m32473_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1321_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32473_GM;
MethodInfo m32473_MI = 
{
	"set_Item", NULL, &t6250_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6250_m32473_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32473_GM};
static MethodInfo* t6250_MIs[] =
{
	&m32474_MI,
	&m32475_MI,
	&m32476_MI,
	&m32472_MI,
	&m32473_MI,
	NULL
};
static TypeInfo* t6250_ITIs[] = 
{
	&t603_TI,
	&t6249_TI,
	&t6251_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6250_0_0_0;
extern Il2CppType t6250_1_0_0;
struct t6250;
extern Il2CppGenericClass t6250_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6250_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6250_MIs, t6250_PIs, NULL, NULL, NULL, NULL, NULL, &t6250_TI, t6250_ITIs, NULL, &t1908__CustomAttributeCache, &t6250_TI, &t6250_0_0_0, &t6250_1_0_0, NULL, &t6250_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4806_TI;

#include "t1064.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.SeekOrigin>
extern MethodInfo m32477_MI;
static PropertyInfo t4806____Current_PropertyInfo = 
{
	&t4806_TI, "Current", &m32477_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4806_PIs[] =
{
	&t4806____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1064_0_0_0;
extern void* RuntimeInvoker_t1064 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32477_GM;
MethodInfo m32477_MI = 
{
	"get_Current", NULL, &t4806_TI, &t1064_0_0_0, RuntimeInvoker_t1064, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32477_GM};
static MethodInfo* t4806_MIs[] =
{
	&m32477_MI,
	NULL
};
static TypeInfo* t4806_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4806_0_0_0;
extern Il2CppType t4806_1_0_0;
struct t4806;
extern Il2CppGenericClass t4806_GC;
TypeInfo t4806_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4806_MIs, t4806_PIs, NULL, NULL, NULL, NULL, NULL, &t4806_TI, t4806_ITIs, NULL, &EmptyCustomAttributesCache, &t4806_TI, &t4806_0_0_0, &t4806_1_0_0, NULL, &t4806_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3353.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3353_TI;
#include "t3353MD.h"

extern TypeInfo t1064_TI;
extern MethodInfo m18641_MI;
extern MethodInfo m24850_MI;
struct t20;
 int32_t m24850 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18637_MI;
 void m18637 (t3353 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18638_MI;
 t29 * m18638 (t3353 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18641(__this, &m18641_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1064_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18639_MI;
 void m18639 (t3353 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18640_MI;
 bool m18640 (t3353 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18641 (t3353 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m24850(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m24850_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.SeekOrigin>
extern Il2CppType t20_0_0_1;
FieldInfo t3353_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3353_TI, offsetof(t3353, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3353_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3353_TI, offsetof(t3353, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3353_FIs[] =
{
	&t3353_f0_FieldInfo,
	&t3353_f1_FieldInfo,
	NULL
};
static PropertyInfo t3353____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3353_TI, "System.Collections.IEnumerator.Current", &m18638_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3353____Current_PropertyInfo = 
{
	&t3353_TI, "Current", &m18641_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3353_PIs[] =
{
	&t3353____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3353____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3353_m18637_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18637_GM;
MethodInfo m18637_MI = 
{
	".ctor", (methodPointerType)&m18637, &t3353_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3353_m18637_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18637_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18638_GM;
MethodInfo m18638_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18638, &t3353_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18638_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18639_GM;
MethodInfo m18639_MI = 
{
	"Dispose", (methodPointerType)&m18639, &t3353_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18639_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18640_GM;
MethodInfo m18640_MI = 
{
	"MoveNext", (methodPointerType)&m18640, &t3353_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18640_GM};
extern Il2CppType t1064_0_0_0;
extern void* RuntimeInvoker_t1064 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18641_GM;
MethodInfo m18641_MI = 
{
	"get_Current", (methodPointerType)&m18641, &t3353_TI, &t1064_0_0_0, RuntimeInvoker_t1064, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18641_GM};
static MethodInfo* t3353_MIs[] =
{
	&m18637_MI,
	&m18638_MI,
	&m18639_MI,
	&m18640_MI,
	&m18641_MI,
	NULL
};
static MethodInfo* t3353_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18638_MI,
	&m18640_MI,
	&m18639_MI,
	&m18641_MI,
};
static TypeInfo* t3353_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4806_TI,
};
static Il2CppInterfaceOffsetPair t3353_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4806_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3353_0_0_0;
extern Il2CppType t3353_1_0_0;
extern Il2CppGenericClass t3353_GC;
TypeInfo t3353_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3353_MIs, t3353_PIs, t3353_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3353_TI, t3353_ITIs, t3353_VT, &EmptyCustomAttributesCache, &t3353_TI, &t3353_0_0_0, &t3353_1_0_0, t3353_IOs, &t3353_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3353)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6252_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>
extern MethodInfo m32478_MI;
static PropertyInfo t6252____Count_PropertyInfo = 
{
	&t6252_TI, "Count", &m32478_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32479_MI;
static PropertyInfo t6252____IsReadOnly_PropertyInfo = 
{
	&t6252_TI, "IsReadOnly", &m32479_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6252_PIs[] =
{
	&t6252____Count_PropertyInfo,
	&t6252____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32478_GM;
MethodInfo m32478_MI = 
{
	"get_Count", NULL, &t6252_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32478_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32479_GM;
MethodInfo m32479_MI = 
{
	"get_IsReadOnly", NULL, &t6252_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32479_GM};
extern Il2CppType t1064_0_0_0;
extern Il2CppType t1064_0_0_0;
static ParameterInfo t6252_m32480_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1064_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32480_GM;
MethodInfo m32480_MI = 
{
	"Add", NULL, &t6252_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6252_m32480_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32480_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32481_GM;
MethodInfo m32481_MI = 
{
	"Clear", NULL, &t6252_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32481_GM};
extern Il2CppType t1064_0_0_0;
static ParameterInfo t6252_m32482_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1064_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32482_GM;
MethodInfo m32482_MI = 
{
	"Contains", NULL, &t6252_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6252_m32482_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32482_GM};
extern Il2CppType t3618_0_0_0;
extern Il2CppType t3618_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6252_m32483_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3618_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32483_GM;
MethodInfo m32483_MI = 
{
	"CopyTo", NULL, &t6252_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6252_m32483_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32483_GM};
extern Il2CppType t1064_0_0_0;
static ParameterInfo t6252_m32484_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1064_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32484_GM;
MethodInfo m32484_MI = 
{
	"Remove", NULL, &t6252_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6252_m32484_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32484_GM};
static MethodInfo* t6252_MIs[] =
{
	&m32478_MI,
	&m32479_MI,
	&m32480_MI,
	&m32481_MI,
	&m32482_MI,
	&m32483_MI,
	&m32484_MI,
	NULL
};
extern TypeInfo t6254_TI;
static TypeInfo* t6252_ITIs[] = 
{
	&t603_TI,
	&t6254_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6252_0_0_0;
extern Il2CppType t6252_1_0_0;
struct t6252;
extern Il2CppGenericClass t6252_GC;
TypeInfo t6252_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6252_MIs, t6252_PIs, NULL, NULL, NULL, NULL, NULL, &t6252_TI, t6252_ITIs, NULL, &EmptyCustomAttributesCache, &t6252_TI, &t6252_0_0_0, &t6252_1_0_0, NULL, &t6252_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.SeekOrigin>
extern Il2CppType t4806_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32485_GM;
MethodInfo m32485_MI = 
{
	"GetEnumerator", NULL, &t6254_TI, &t4806_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32485_GM};
static MethodInfo* t6254_MIs[] =
{
	&m32485_MI,
	NULL
};
static TypeInfo* t6254_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6254_0_0_0;
extern Il2CppType t6254_1_0_0;
struct t6254;
extern Il2CppGenericClass t6254_GC;
TypeInfo t6254_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6254_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6254_TI, t6254_ITIs, NULL, &EmptyCustomAttributesCache, &t6254_TI, &t6254_0_0_0, &t6254_1_0_0, NULL, &t6254_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6253_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IO.SeekOrigin>
extern MethodInfo m32486_MI;
extern MethodInfo m32487_MI;
static PropertyInfo t6253____Item_PropertyInfo = 
{
	&t6253_TI, "Item", &m32486_MI, &m32487_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6253_PIs[] =
{
	&t6253____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1064_0_0_0;
static ParameterInfo t6253_m32488_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1064_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32488_GM;
MethodInfo m32488_MI = 
{
	"IndexOf", NULL, &t6253_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6253_m32488_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32488_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1064_0_0_0;
static ParameterInfo t6253_m32489_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1064_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32489_GM;
MethodInfo m32489_MI = 
{
	"Insert", NULL, &t6253_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6253_m32489_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32489_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6253_m32490_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32490_GM;
MethodInfo m32490_MI = 
{
	"RemoveAt", NULL, &t6253_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6253_m32490_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32490_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6253_m32486_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1064_0_0_0;
extern void* RuntimeInvoker_t1064_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32486_GM;
MethodInfo m32486_MI = 
{
	"get_Item", NULL, &t6253_TI, &t1064_0_0_0, RuntimeInvoker_t1064_t44, t6253_m32486_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32486_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1064_0_0_0;
static ParameterInfo t6253_m32487_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1064_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32487_GM;
MethodInfo m32487_MI = 
{
	"set_Item", NULL, &t6253_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6253_m32487_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32487_GM};
static MethodInfo* t6253_MIs[] =
{
	&m32488_MI,
	&m32489_MI,
	&m32490_MI,
	&m32486_MI,
	&m32487_MI,
	NULL
};
static TypeInfo* t6253_ITIs[] = 
{
	&t603_TI,
	&t6252_TI,
	&t6254_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6253_0_0_0;
extern Il2CppType t6253_1_0_0;
struct t6253;
extern Il2CppGenericClass t6253_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6253_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6253_MIs, t6253_PIs, NULL, NULL, NULL, NULL, NULL, &t6253_TI, t6253_ITIs, NULL, &t1908__CustomAttributeCache, &t6253_TI, &t6253_0_0_0, &t6253_1_0_0, NULL, &t6253_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4808_TI;

#include "t1351.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ModuleBuilder>
extern MethodInfo m32491_MI;
static PropertyInfo t4808____Current_PropertyInfo = 
{
	&t4808_TI, "Current", &m32491_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4808_PIs[] =
{
	&t4808____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1351_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32491_GM;
MethodInfo m32491_MI = 
{
	"get_Current", NULL, &t4808_TI, &t1351_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32491_GM};
static MethodInfo* t4808_MIs[] =
{
	&m32491_MI,
	NULL
};
static TypeInfo* t4808_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4808_0_0_0;
extern Il2CppType t4808_1_0_0;
struct t4808;
extern Il2CppGenericClass t4808_GC;
TypeInfo t4808_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4808_MIs, t4808_PIs, NULL, NULL, NULL, NULL, NULL, &t4808_TI, t4808_ITIs, NULL, &EmptyCustomAttributesCache, &t4808_TI, &t4808_0_0_0, &t4808_1_0_0, NULL, &t4808_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3354.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3354_TI;
#include "t3354MD.h"

extern TypeInfo t1351_TI;
extern MethodInfo m18646_MI;
extern MethodInfo m24861_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m24861(__this, p0, method) (t1351 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>
extern Il2CppType t20_0_0_1;
FieldInfo t3354_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3354_TI, offsetof(t3354, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3354_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3354_TI, offsetof(t3354, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3354_FIs[] =
{
	&t3354_f0_FieldInfo,
	&t3354_f1_FieldInfo,
	NULL
};
extern MethodInfo m18643_MI;
static PropertyInfo t3354____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3354_TI, "System.Collections.IEnumerator.Current", &m18643_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3354____Current_PropertyInfo = 
{
	&t3354_TI, "Current", &m18646_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3354_PIs[] =
{
	&t3354____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3354____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3354_m18642_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18642_GM;
MethodInfo m18642_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3354_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3354_m18642_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18642_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18643_GM;
MethodInfo m18643_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3354_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18643_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18644_GM;
MethodInfo m18644_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3354_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18644_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18645_GM;
MethodInfo m18645_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3354_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18645_GM};
extern Il2CppType t1351_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18646_GM;
MethodInfo m18646_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3354_TI, &t1351_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18646_GM};
static MethodInfo* t3354_MIs[] =
{
	&m18642_MI,
	&m18643_MI,
	&m18644_MI,
	&m18645_MI,
	&m18646_MI,
	NULL
};
extern MethodInfo m18645_MI;
extern MethodInfo m18644_MI;
static MethodInfo* t3354_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18643_MI,
	&m18645_MI,
	&m18644_MI,
	&m18646_MI,
};
static TypeInfo* t3354_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4808_TI,
};
static Il2CppInterfaceOffsetPair t3354_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4808_TI, 7},
};
extern TypeInfo t1351_TI;
static Il2CppRGCTXData t3354_RGCTXData[3] = 
{
	&m18646_MI/* Method Usage */,
	&t1351_TI/* Class Usage */,
	&m24861_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3354_0_0_0;
extern Il2CppType t3354_1_0_0;
extern Il2CppGenericClass t3354_GC;
TypeInfo t3354_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3354_MIs, t3354_PIs, t3354_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3354_TI, t3354_ITIs, t3354_VT, &EmptyCustomAttributesCache, &t3354_TI, &t3354_0_0_0, &t3354_1_0_0, t3354_IOs, &t3354_GC, NULL, NULL, NULL, t3354_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3354)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6255_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>
extern MethodInfo m32492_MI;
static PropertyInfo t6255____Count_PropertyInfo = 
{
	&t6255_TI, "Count", &m32492_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32493_MI;
static PropertyInfo t6255____IsReadOnly_PropertyInfo = 
{
	&t6255_TI, "IsReadOnly", &m32493_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6255_PIs[] =
{
	&t6255____Count_PropertyInfo,
	&t6255____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32492_GM;
MethodInfo m32492_MI = 
{
	"get_Count", NULL, &t6255_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32492_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32493_GM;
MethodInfo m32493_MI = 
{
	"get_IsReadOnly", NULL, &t6255_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32493_GM};
extern Il2CppType t1351_0_0_0;
extern Il2CppType t1351_0_0_0;
static ParameterInfo t6255_m32494_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1351_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32494_GM;
MethodInfo m32494_MI = 
{
	"Add", NULL, &t6255_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6255_m32494_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32494_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32495_GM;
MethodInfo m32495_MI = 
{
	"Clear", NULL, &t6255_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32495_GM};
extern Il2CppType t1351_0_0_0;
static ParameterInfo t6255_m32496_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1351_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32496_GM;
MethodInfo m32496_MI = 
{
	"Contains", NULL, &t6255_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6255_m32496_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32496_GM};
extern Il2CppType t1336_0_0_0;
extern Il2CppType t1336_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6255_m32497_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1336_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32497_GM;
MethodInfo m32497_MI = 
{
	"CopyTo", NULL, &t6255_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6255_m32497_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32497_GM};
extern Il2CppType t1351_0_0_0;
static ParameterInfo t6255_m32498_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1351_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32498_GM;
MethodInfo m32498_MI = 
{
	"Remove", NULL, &t6255_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6255_m32498_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32498_GM};
static MethodInfo* t6255_MIs[] =
{
	&m32492_MI,
	&m32493_MI,
	&m32494_MI,
	&m32495_MI,
	&m32496_MI,
	&m32497_MI,
	&m32498_MI,
	NULL
};
extern TypeInfo t6257_TI;
static TypeInfo* t6255_ITIs[] = 
{
	&t603_TI,
	&t6257_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6255_0_0_0;
extern Il2CppType t6255_1_0_0;
struct t6255;
extern Il2CppGenericClass t6255_GC;
TypeInfo t6255_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6255_MIs, t6255_PIs, NULL, NULL, NULL, NULL, NULL, &t6255_TI, t6255_ITIs, NULL, &EmptyCustomAttributesCache, &t6255_TI, &t6255_0_0_0, &t6255_1_0_0, NULL, &t6255_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ModuleBuilder>
extern Il2CppType t4808_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32499_GM;
MethodInfo m32499_MI = 
{
	"GetEnumerator", NULL, &t6257_TI, &t4808_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32499_GM};
static MethodInfo* t6257_MIs[] =
{
	&m32499_MI,
	NULL
};
static TypeInfo* t6257_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6257_0_0_0;
extern Il2CppType t6257_1_0_0;
struct t6257;
extern Il2CppGenericClass t6257_GC;
TypeInfo t6257_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6257_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6257_TI, t6257_ITIs, NULL, &EmptyCustomAttributesCache, &t6257_TI, &t6257_0_0_0, &t6257_1_0_0, NULL, &t6257_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6256_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>
extern MethodInfo m32500_MI;
extern MethodInfo m32501_MI;
static PropertyInfo t6256____Item_PropertyInfo = 
{
	&t6256_TI, "Item", &m32500_MI, &m32501_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6256_PIs[] =
{
	&t6256____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1351_0_0_0;
static ParameterInfo t6256_m32502_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1351_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32502_GM;
MethodInfo m32502_MI = 
{
	"IndexOf", NULL, &t6256_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6256_m32502_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32502_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1351_0_0_0;
static ParameterInfo t6256_m32503_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1351_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32503_GM;
MethodInfo m32503_MI = 
{
	"Insert", NULL, &t6256_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6256_m32503_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32503_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6256_m32504_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32504_GM;
MethodInfo m32504_MI = 
{
	"RemoveAt", NULL, &t6256_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6256_m32504_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32504_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6256_m32500_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1351_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32500_GM;
MethodInfo m32500_MI = 
{
	"get_Item", NULL, &t6256_TI, &t1351_0_0_0, RuntimeInvoker_t29_t44, t6256_m32500_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32500_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1351_0_0_0;
static ParameterInfo t6256_m32501_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1351_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32501_GM;
MethodInfo m32501_MI = 
{
	"set_Item", NULL, &t6256_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6256_m32501_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32501_GM};
static MethodInfo* t6256_MIs[] =
{
	&m32502_MI,
	&m32503_MI,
	&m32504_MI,
	&m32500_MI,
	&m32501_MI,
	NULL
};
static TypeInfo* t6256_ITIs[] = 
{
	&t603_TI,
	&t6255_TI,
	&t6257_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6256_0_0_0;
extern Il2CppType t6256_1_0_0;
struct t6256;
extern Il2CppGenericClass t6256_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6256_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6256_MIs, t6256_PIs, NULL, NULL, NULL, NULL, NULL, &t6256_TI, t6256_ITIs, NULL, &t1908__CustomAttributeCache, &t6256_TI, &t6256_0_0_0, &t6256_1_0_0, NULL, &t6256_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6258_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>
extern MethodInfo m32505_MI;
static PropertyInfo t6258____Count_PropertyInfo = 
{
	&t6258_TI, "Count", &m32505_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32506_MI;
static PropertyInfo t6258____IsReadOnly_PropertyInfo = 
{
	&t6258_TI, "IsReadOnly", &m32506_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6258_PIs[] =
{
	&t6258____Count_PropertyInfo,
	&t6258____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32505_GM;
MethodInfo m32505_MI = 
{
	"get_Count", NULL, &t6258_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32505_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32506_GM;
MethodInfo m32506_MI = 
{
	"get_IsReadOnly", NULL, &t6258_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32506_GM};
extern Il2CppType t2016_0_0_0;
extern Il2CppType t2016_0_0_0;
static ParameterInfo t6258_m32507_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2016_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32507_GM;
MethodInfo m32507_MI = 
{
	"Add", NULL, &t6258_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6258_m32507_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32507_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32508_GM;
MethodInfo m32508_MI = 
{
	"Clear", NULL, &t6258_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32508_GM};
extern Il2CppType t2016_0_0_0;
static ParameterInfo t6258_m32509_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2016_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32509_GM;
MethodInfo m32509_MI = 
{
	"Contains", NULL, &t6258_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6258_m32509_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32509_GM};
extern Il2CppType t3619_0_0_0;
extern Il2CppType t3619_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6258_m32510_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3619_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32510_GM;
MethodInfo m32510_MI = 
{
	"CopyTo", NULL, &t6258_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6258_m32510_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32510_GM};
extern Il2CppType t2016_0_0_0;
static ParameterInfo t6258_m32511_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2016_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32511_GM;
MethodInfo m32511_MI = 
{
	"Remove", NULL, &t6258_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6258_m32511_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32511_GM};
static MethodInfo* t6258_MIs[] =
{
	&m32505_MI,
	&m32506_MI,
	&m32507_MI,
	&m32508_MI,
	&m32509_MI,
	&m32510_MI,
	&m32511_MI,
	NULL
};
extern TypeInfo t6260_TI;
static TypeInfo* t6258_ITIs[] = 
{
	&t603_TI,
	&t6260_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6258_0_0_0;
extern Il2CppType t6258_1_0_0;
struct t6258;
extern Il2CppGenericClass t6258_GC;
TypeInfo t6258_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6258_MIs, t6258_PIs, NULL, NULL, NULL, NULL, NULL, &t6258_TI, t6258_ITIs, NULL, &EmptyCustomAttributesCache, &t6258_TI, &t6258_0_0_0, &t6258_1_0_0, NULL, &t6258_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ModuleBuilder>
extern Il2CppType t4810_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32512_GM;
MethodInfo m32512_MI = 
{
	"GetEnumerator", NULL, &t6260_TI, &t4810_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32512_GM};
static MethodInfo* t6260_MIs[] =
{
	&m32512_MI,
	NULL
};
static TypeInfo* t6260_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6260_0_0_0;
extern Il2CppType t6260_1_0_0;
struct t6260;
extern Il2CppGenericClass t6260_GC;
TypeInfo t6260_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6260_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6260_TI, t6260_ITIs, NULL, &EmptyCustomAttributesCache, &t6260_TI, &t6260_0_0_0, &t6260_1_0_0, NULL, &t6260_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4810_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>
extern MethodInfo m32513_MI;
static PropertyInfo t4810____Current_PropertyInfo = 
{
	&t4810_TI, "Current", &m32513_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4810_PIs[] =
{
	&t4810____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2016_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32513_GM;
MethodInfo m32513_MI = 
{
	"get_Current", NULL, &t4810_TI, &t2016_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32513_GM};
static MethodInfo* t4810_MIs[] =
{
	&m32513_MI,
	NULL
};
static TypeInfo* t4810_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4810_0_0_0;
extern Il2CppType t4810_1_0_0;
struct t4810;
extern Il2CppGenericClass t4810_GC;
TypeInfo t4810_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4810_MIs, t4810_PIs, NULL, NULL, NULL, NULL, NULL, &t4810_TI, t4810_ITIs, NULL, &EmptyCustomAttributesCache, &t4810_TI, &t4810_0_0_0, &t4810_1_0_0, NULL, &t4810_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3355.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3355_TI;
#include "t3355MD.h"

extern TypeInfo t2016_TI;
extern MethodInfo m18651_MI;
extern MethodInfo m24872_MI;
struct t20;
#define m24872(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>
extern Il2CppType t20_0_0_1;
FieldInfo t3355_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3355_TI, offsetof(t3355, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3355_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3355_TI, offsetof(t3355, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3355_FIs[] =
{
	&t3355_f0_FieldInfo,
	&t3355_f1_FieldInfo,
	NULL
};
extern MethodInfo m18648_MI;
static PropertyInfo t3355____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3355_TI, "System.Collections.IEnumerator.Current", &m18648_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3355____Current_PropertyInfo = 
{
	&t3355_TI, "Current", &m18651_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3355_PIs[] =
{
	&t3355____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3355____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3355_m18647_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18647_GM;
MethodInfo m18647_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3355_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3355_m18647_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18647_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18648_GM;
MethodInfo m18648_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3355_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18648_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18649_GM;
MethodInfo m18649_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3355_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18649_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18650_GM;
MethodInfo m18650_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3355_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18650_GM};
extern Il2CppType t2016_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18651_GM;
MethodInfo m18651_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3355_TI, &t2016_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18651_GM};
static MethodInfo* t3355_MIs[] =
{
	&m18647_MI,
	&m18648_MI,
	&m18649_MI,
	&m18650_MI,
	&m18651_MI,
	NULL
};
extern MethodInfo m18650_MI;
extern MethodInfo m18649_MI;
static MethodInfo* t3355_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18648_MI,
	&m18650_MI,
	&m18649_MI,
	&m18651_MI,
};
static TypeInfo* t3355_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4810_TI,
};
static Il2CppInterfaceOffsetPair t3355_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4810_TI, 7},
};
extern TypeInfo t2016_TI;
static Il2CppRGCTXData t3355_RGCTXData[3] = 
{
	&m18651_MI/* Method Usage */,
	&t2016_TI/* Class Usage */,
	&m24872_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3355_0_0_0;
extern Il2CppType t3355_1_0_0;
extern Il2CppGenericClass t3355_GC;
TypeInfo t3355_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3355_MIs, t3355_PIs, t3355_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3355_TI, t3355_ITIs, t3355_VT, &EmptyCustomAttributesCache, &t3355_TI, &t3355_0_0_0, &t3355_1_0_0, t3355_IOs, &t3355_GC, NULL, NULL, NULL, t3355_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3355)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6259_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>
extern MethodInfo m32514_MI;
extern MethodInfo m32515_MI;
static PropertyInfo t6259____Item_PropertyInfo = 
{
	&t6259_TI, "Item", &m32514_MI, &m32515_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6259_PIs[] =
{
	&t6259____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2016_0_0_0;
static ParameterInfo t6259_m32516_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2016_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32516_GM;
MethodInfo m32516_MI = 
{
	"IndexOf", NULL, &t6259_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6259_m32516_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32516_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2016_0_0_0;
static ParameterInfo t6259_m32517_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2016_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32517_GM;
MethodInfo m32517_MI = 
{
	"Insert", NULL, &t6259_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6259_m32517_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32517_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6259_m32518_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32518_GM;
MethodInfo m32518_MI = 
{
	"RemoveAt", NULL, &t6259_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6259_m32518_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32518_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6259_m32514_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2016_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32514_GM;
MethodInfo m32514_MI = 
{
	"get_Item", NULL, &t6259_TI, &t2016_0_0_0, RuntimeInvoker_t29_t44, t6259_m32514_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32514_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2016_0_0_0;
static ParameterInfo t6259_m32515_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2016_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32515_GM;
MethodInfo m32515_MI = 
{
	"set_Item", NULL, &t6259_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6259_m32515_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32515_GM};
static MethodInfo* t6259_MIs[] =
{
	&m32516_MI,
	&m32517_MI,
	&m32518_MI,
	&m32514_MI,
	&m32515_MI,
	NULL
};
static TypeInfo* t6259_ITIs[] = 
{
	&t603_TI,
	&t6258_TI,
	&t6260_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6259_0_0_0;
extern Il2CppType t6259_1_0_0;
struct t6259;
extern Il2CppGenericClass t6259_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6259_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6259_MIs, t6259_PIs, NULL, NULL, NULL, NULL, NULL, &t6259_TI, t6259_ITIs, NULL, &t1908__CustomAttributeCache, &t6259_TI, &t6259_0_0_0, &t6259_1_0_0, NULL, &t6259_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6261_TI;

#include "t1142.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Module>
extern MethodInfo m32519_MI;
static PropertyInfo t6261____Count_PropertyInfo = 
{
	&t6261_TI, "Count", &m32519_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32520_MI;
static PropertyInfo t6261____IsReadOnly_PropertyInfo = 
{
	&t6261_TI, "IsReadOnly", &m32520_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6261_PIs[] =
{
	&t6261____Count_PropertyInfo,
	&t6261____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32519_GM;
MethodInfo m32519_MI = 
{
	"get_Count", NULL, &t6261_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32519_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32520_GM;
MethodInfo m32520_MI = 
{
	"get_IsReadOnly", NULL, &t6261_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32520_GM};
extern Il2CppType t1142_0_0_0;
extern Il2CppType t1142_0_0_0;
static ParameterInfo t6261_m32521_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1142_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32521_GM;
MethodInfo m32521_MI = 
{
	"Add", NULL, &t6261_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6261_m32521_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32521_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32522_GM;
MethodInfo m32522_MI = 
{
	"Clear", NULL, &t6261_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32522_GM};
extern Il2CppType t1142_0_0_0;
static ParameterInfo t6261_m32523_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1142_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32523_GM;
MethodInfo m32523_MI = 
{
	"Contains", NULL, &t6261_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6261_m32523_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32523_GM};
extern Il2CppType t1337_0_0_0;
extern Il2CppType t1337_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6261_m32524_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1337_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32524_GM;
MethodInfo m32524_MI = 
{
	"CopyTo", NULL, &t6261_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6261_m32524_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32524_GM};
extern Il2CppType t1142_0_0_0;
static ParameterInfo t6261_m32525_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1142_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32525_GM;
MethodInfo m32525_MI = 
{
	"Remove", NULL, &t6261_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6261_m32525_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32525_GM};
static MethodInfo* t6261_MIs[] =
{
	&m32519_MI,
	&m32520_MI,
	&m32521_MI,
	&m32522_MI,
	&m32523_MI,
	&m32524_MI,
	&m32525_MI,
	NULL
};
extern TypeInfo t6263_TI;
static TypeInfo* t6261_ITIs[] = 
{
	&t603_TI,
	&t6263_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6261_0_0_0;
extern Il2CppType t6261_1_0_0;
struct t6261;
extern Il2CppGenericClass t6261_GC;
TypeInfo t6261_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6261_MIs, t6261_PIs, NULL, NULL, NULL, NULL, NULL, &t6261_TI, t6261_ITIs, NULL, &EmptyCustomAttributesCache, &t6261_TI, &t6261_0_0_0, &t6261_1_0_0, NULL, &t6261_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Module>
extern Il2CppType t4811_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32526_GM;
MethodInfo m32526_MI = 
{
	"GetEnumerator", NULL, &t6263_TI, &t4811_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32526_GM};
static MethodInfo* t6263_MIs[] =
{
	&m32526_MI,
	NULL
};
static TypeInfo* t6263_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6263_0_0_0;
extern Il2CppType t6263_1_0_0;
struct t6263;
extern Il2CppGenericClass t6263_GC;
TypeInfo t6263_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6263_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6263_TI, t6263_ITIs, NULL, &EmptyCustomAttributesCache, &t6263_TI, &t6263_0_0_0, &t6263_1_0_0, NULL, &t6263_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4811_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Module>
extern MethodInfo m32527_MI;
static PropertyInfo t4811____Current_PropertyInfo = 
{
	&t4811_TI, "Current", &m32527_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4811_PIs[] =
{
	&t4811____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1142_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32527_GM;
MethodInfo m32527_MI = 
{
	"get_Current", NULL, &t4811_TI, &t1142_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32527_GM};
static MethodInfo* t4811_MIs[] =
{
	&m32527_MI,
	NULL
};
static TypeInfo* t4811_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4811_0_0_0;
extern Il2CppType t4811_1_0_0;
struct t4811;
extern Il2CppGenericClass t4811_GC;
TypeInfo t4811_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4811_MIs, t4811_PIs, NULL, NULL, NULL, NULL, NULL, &t4811_TI, t4811_ITIs, NULL, &EmptyCustomAttributesCache, &t4811_TI, &t4811_0_0_0, &t4811_1_0_0, NULL, &t4811_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3356.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3356_TI;
#include "t3356MD.h"

extern TypeInfo t1142_TI;
extern MethodInfo m18656_MI;
extern MethodInfo m24883_MI;
struct t20;
#define m24883(__this, p0, method) (t1142 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Module>
extern Il2CppType t20_0_0_1;
FieldInfo t3356_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3356_TI, offsetof(t3356, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3356_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3356_TI, offsetof(t3356, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3356_FIs[] =
{
	&t3356_f0_FieldInfo,
	&t3356_f1_FieldInfo,
	NULL
};
extern MethodInfo m18653_MI;
static PropertyInfo t3356____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3356_TI, "System.Collections.IEnumerator.Current", &m18653_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3356____Current_PropertyInfo = 
{
	&t3356_TI, "Current", &m18656_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3356_PIs[] =
{
	&t3356____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3356____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3356_m18652_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18652_GM;
MethodInfo m18652_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3356_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3356_m18652_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18652_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18653_GM;
MethodInfo m18653_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3356_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18653_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18654_GM;
MethodInfo m18654_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3356_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18654_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18655_GM;
MethodInfo m18655_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3356_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18655_GM};
extern Il2CppType t1142_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18656_GM;
MethodInfo m18656_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3356_TI, &t1142_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18656_GM};
static MethodInfo* t3356_MIs[] =
{
	&m18652_MI,
	&m18653_MI,
	&m18654_MI,
	&m18655_MI,
	&m18656_MI,
	NULL
};
extern MethodInfo m18655_MI;
extern MethodInfo m18654_MI;
static MethodInfo* t3356_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18653_MI,
	&m18655_MI,
	&m18654_MI,
	&m18656_MI,
};
static TypeInfo* t3356_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4811_TI,
};
static Il2CppInterfaceOffsetPair t3356_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4811_TI, 7},
};
extern TypeInfo t1142_TI;
static Il2CppRGCTXData t3356_RGCTXData[3] = 
{
	&m18656_MI/* Method Usage */,
	&t1142_TI/* Class Usage */,
	&m24883_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3356_0_0_0;
extern Il2CppType t3356_1_0_0;
extern Il2CppGenericClass t3356_GC;
TypeInfo t3356_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3356_MIs, t3356_PIs, t3356_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3356_TI, t3356_ITIs, t3356_VT, &EmptyCustomAttributesCache, &t3356_TI, &t3356_0_0_0, &t3356_1_0_0, t3356_IOs, &t3356_GC, NULL, NULL, NULL, t3356_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3356)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6262_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Module>
extern MethodInfo m32528_MI;
extern MethodInfo m32529_MI;
static PropertyInfo t6262____Item_PropertyInfo = 
{
	&t6262_TI, "Item", &m32528_MI, &m32529_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6262_PIs[] =
{
	&t6262____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1142_0_0_0;
static ParameterInfo t6262_m32530_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1142_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32530_GM;
MethodInfo m32530_MI = 
{
	"IndexOf", NULL, &t6262_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6262_m32530_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32530_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1142_0_0_0;
static ParameterInfo t6262_m32531_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1142_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32531_GM;
MethodInfo m32531_MI = 
{
	"Insert", NULL, &t6262_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6262_m32531_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32531_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6262_m32532_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32532_GM;
MethodInfo m32532_MI = 
{
	"RemoveAt", NULL, &t6262_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6262_m32532_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32532_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6262_m32528_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1142_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32528_GM;
MethodInfo m32528_MI = 
{
	"get_Item", NULL, &t6262_TI, &t1142_0_0_0, RuntimeInvoker_t29_t44, t6262_m32528_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32528_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1142_0_0_0;
static ParameterInfo t6262_m32529_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1142_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32529_GM;
MethodInfo m32529_MI = 
{
	"set_Item", NULL, &t6262_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6262_m32529_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32529_GM};
static MethodInfo* t6262_MIs[] =
{
	&m32530_MI,
	&m32531_MI,
	&m32532_MI,
	&m32528_MI,
	&m32529_MI,
	NULL
};
static TypeInfo* t6262_ITIs[] = 
{
	&t603_TI,
	&t6261_TI,
	&t6263_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6262_0_0_0;
extern Il2CppType t6262_1_0_0;
struct t6262;
extern Il2CppGenericClass t6262_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6262_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6262_MIs, t6262_PIs, NULL, NULL, NULL, NULL, NULL, &t6262_TI, t6262_ITIs, NULL, &t1908__CustomAttributeCache, &t6262_TI, &t6262_0_0_0, &t6262_1_0_0, NULL, &t6262_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6264_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>
extern MethodInfo m32533_MI;
static PropertyInfo t6264____Count_PropertyInfo = 
{
	&t6264_TI, "Count", &m32533_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32534_MI;
static PropertyInfo t6264____IsReadOnly_PropertyInfo = 
{
	&t6264_TI, "IsReadOnly", &m32534_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6264_PIs[] =
{
	&t6264____Count_PropertyInfo,
	&t6264____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32533_GM;
MethodInfo m32533_MI = 
{
	"get_Count", NULL, &t6264_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32533_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32534_GM;
MethodInfo m32534_MI = 
{
	"get_IsReadOnly", NULL, &t6264_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32534_GM};
extern Il2CppType t2017_0_0_0;
extern Il2CppType t2017_0_0_0;
static ParameterInfo t6264_m32535_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2017_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32535_GM;
MethodInfo m32535_MI = 
{
	"Add", NULL, &t6264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6264_m32535_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32535_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32536_GM;
MethodInfo m32536_MI = 
{
	"Clear", NULL, &t6264_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32536_GM};
extern Il2CppType t2017_0_0_0;
static ParameterInfo t6264_m32537_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2017_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32537_GM;
MethodInfo m32537_MI = 
{
	"Contains", NULL, &t6264_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6264_m32537_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32537_GM};
extern Il2CppType t3620_0_0_0;
extern Il2CppType t3620_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6264_m32538_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3620_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32538_GM;
MethodInfo m32538_MI = 
{
	"CopyTo", NULL, &t6264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6264_m32538_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32538_GM};
extern Il2CppType t2017_0_0_0;
static ParameterInfo t6264_m32539_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2017_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32539_GM;
MethodInfo m32539_MI = 
{
	"Remove", NULL, &t6264_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6264_m32539_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32539_GM};
static MethodInfo* t6264_MIs[] =
{
	&m32533_MI,
	&m32534_MI,
	&m32535_MI,
	&m32536_MI,
	&m32537_MI,
	&m32538_MI,
	&m32539_MI,
	NULL
};
extern TypeInfo t6266_TI;
static TypeInfo* t6264_ITIs[] = 
{
	&t603_TI,
	&t6266_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6264_0_0_0;
extern Il2CppType t6264_1_0_0;
struct t6264;
extern Il2CppGenericClass t6264_GC;
TypeInfo t6264_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6264_MIs, t6264_PIs, NULL, NULL, NULL, NULL, NULL, &t6264_TI, t6264_ITIs, NULL, &EmptyCustomAttributesCache, &t6264_TI, &t6264_0_0_0, &t6264_1_0_0, NULL, &t6264_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._Module>
extern Il2CppType t4813_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32540_GM;
MethodInfo m32540_MI = 
{
	"GetEnumerator", NULL, &t6266_TI, &t4813_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32540_GM};
static MethodInfo* t6266_MIs[] =
{
	&m32540_MI,
	NULL
};
static TypeInfo* t6266_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6266_0_0_0;
extern Il2CppType t6266_1_0_0;
struct t6266;
extern Il2CppGenericClass t6266_GC;
TypeInfo t6266_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6266_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6266_TI, t6266_ITIs, NULL, &EmptyCustomAttributesCache, &t6266_TI, &t6266_0_0_0, &t6266_1_0_0, NULL, &t6266_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4813_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._Module>
extern MethodInfo m32541_MI;
static PropertyInfo t4813____Current_PropertyInfo = 
{
	&t4813_TI, "Current", &m32541_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4813_PIs[] =
{
	&t4813____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2017_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32541_GM;
MethodInfo m32541_MI = 
{
	"get_Current", NULL, &t4813_TI, &t2017_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32541_GM};
static MethodInfo* t4813_MIs[] =
{
	&m32541_MI,
	NULL
};
static TypeInfo* t4813_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4813_0_0_0;
extern Il2CppType t4813_1_0_0;
struct t4813;
extern Il2CppGenericClass t4813_GC;
TypeInfo t4813_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4813_MIs, t4813_PIs, NULL, NULL, NULL, NULL, NULL, &t4813_TI, t4813_ITIs, NULL, &EmptyCustomAttributesCache, &t4813_TI, &t4813_0_0_0, &t4813_1_0_0, NULL, &t4813_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3357.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3357_TI;
#include "t3357MD.h"

extern TypeInfo t2017_TI;
extern MethodInfo m18661_MI;
extern MethodInfo m24894_MI;
struct t20;
#define m24894(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>
extern Il2CppType t20_0_0_1;
FieldInfo t3357_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3357_TI, offsetof(t3357, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3357_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3357_TI, offsetof(t3357, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3357_FIs[] =
{
	&t3357_f0_FieldInfo,
	&t3357_f1_FieldInfo,
	NULL
};
extern MethodInfo m18658_MI;
static PropertyInfo t3357____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3357_TI, "System.Collections.IEnumerator.Current", &m18658_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3357____Current_PropertyInfo = 
{
	&t3357_TI, "Current", &m18661_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3357_PIs[] =
{
	&t3357____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3357____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3357_m18657_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18657_GM;
MethodInfo m18657_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3357_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3357_m18657_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18657_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18658_GM;
MethodInfo m18658_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3357_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18658_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18659_GM;
MethodInfo m18659_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3357_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18659_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18660_GM;
MethodInfo m18660_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3357_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18660_GM};
extern Il2CppType t2017_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18661_GM;
MethodInfo m18661_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3357_TI, &t2017_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18661_GM};
static MethodInfo* t3357_MIs[] =
{
	&m18657_MI,
	&m18658_MI,
	&m18659_MI,
	&m18660_MI,
	&m18661_MI,
	NULL
};
extern MethodInfo m18660_MI;
extern MethodInfo m18659_MI;
static MethodInfo* t3357_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18658_MI,
	&m18660_MI,
	&m18659_MI,
	&m18661_MI,
};
static TypeInfo* t3357_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4813_TI,
};
static Il2CppInterfaceOffsetPair t3357_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4813_TI, 7},
};
extern TypeInfo t2017_TI;
static Il2CppRGCTXData t3357_RGCTXData[3] = 
{
	&m18661_MI/* Method Usage */,
	&t2017_TI/* Class Usage */,
	&m24894_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3357_0_0_0;
extern Il2CppType t3357_1_0_0;
extern Il2CppGenericClass t3357_GC;
TypeInfo t3357_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3357_MIs, t3357_PIs, t3357_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3357_TI, t3357_ITIs, t3357_VT, &EmptyCustomAttributesCache, &t3357_TI, &t3357_0_0_0, &t3357_1_0_0, t3357_IOs, &t3357_GC, NULL, NULL, NULL, t3357_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3357)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6265_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>
extern MethodInfo m32542_MI;
extern MethodInfo m32543_MI;
static PropertyInfo t6265____Item_PropertyInfo = 
{
	&t6265_TI, "Item", &m32542_MI, &m32543_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6265_PIs[] =
{
	&t6265____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2017_0_0_0;
static ParameterInfo t6265_m32544_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2017_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32544_GM;
MethodInfo m32544_MI = 
{
	"IndexOf", NULL, &t6265_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6265_m32544_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32544_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2017_0_0_0;
static ParameterInfo t6265_m32545_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2017_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32545_GM;
MethodInfo m32545_MI = 
{
	"Insert", NULL, &t6265_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6265_m32545_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32545_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6265_m32546_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32546_GM;
MethodInfo m32546_MI = 
{
	"RemoveAt", NULL, &t6265_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6265_m32546_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32546_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6265_m32542_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2017_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32542_GM;
MethodInfo m32542_MI = 
{
	"get_Item", NULL, &t6265_TI, &t2017_0_0_0, RuntimeInvoker_t29_t44, t6265_m32542_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32542_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2017_0_0_0;
static ParameterInfo t6265_m32543_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2017_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32543_GM;
MethodInfo m32543_MI = 
{
	"set_Item", NULL, &t6265_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6265_m32543_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32543_GM};
static MethodInfo* t6265_MIs[] =
{
	&m32544_MI,
	&m32545_MI,
	&m32546_MI,
	&m32542_MI,
	&m32543_MI,
	NULL
};
static TypeInfo* t6265_ITIs[] = 
{
	&t603_TI,
	&t6264_TI,
	&t6266_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6265_0_0_0;
extern Il2CppType t6265_1_0_0;
struct t6265;
extern Il2CppGenericClass t6265_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6265_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6265_MIs, t6265_PIs, NULL, NULL, NULL, NULL, NULL, &t6265_TI, t6265_ITIs, NULL, &t1908__CustomAttributeCache, &t6265_TI, &t6265_0_0_0, &t6265_1_0_0, NULL, &t6265_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4815_TI;

#include "t1352.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ParameterBuilder>
extern MethodInfo m32547_MI;
static PropertyInfo t4815____Current_PropertyInfo = 
{
	&t4815_TI, "Current", &m32547_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4815_PIs[] =
{
	&t4815____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1352_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32547_GM;
MethodInfo m32547_MI = 
{
	"get_Current", NULL, &t4815_TI, &t1352_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32547_GM};
static MethodInfo* t4815_MIs[] =
{
	&m32547_MI,
	NULL
};
static TypeInfo* t4815_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4815_0_0_0;
extern Il2CppType t4815_1_0_0;
struct t4815;
extern Il2CppGenericClass t4815_GC;
TypeInfo t4815_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4815_MIs, t4815_PIs, NULL, NULL, NULL, NULL, NULL, &t4815_TI, t4815_ITIs, NULL, &EmptyCustomAttributesCache, &t4815_TI, &t4815_0_0_0, &t4815_1_0_0, NULL, &t4815_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3358.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3358_TI;
#include "t3358MD.h"

extern TypeInfo t1352_TI;
extern MethodInfo m18666_MI;
extern MethodInfo m24905_MI;
struct t20;
#define m24905(__this, p0, method) (t1352 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>
extern Il2CppType t20_0_0_1;
FieldInfo t3358_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3358_TI, offsetof(t3358, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3358_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3358_TI, offsetof(t3358, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3358_FIs[] =
{
	&t3358_f0_FieldInfo,
	&t3358_f1_FieldInfo,
	NULL
};
extern MethodInfo m18663_MI;
static PropertyInfo t3358____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3358_TI, "System.Collections.IEnumerator.Current", &m18663_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3358____Current_PropertyInfo = 
{
	&t3358_TI, "Current", &m18666_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3358_PIs[] =
{
	&t3358____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3358____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3358_m18662_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18662_GM;
MethodInfo m18662_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3358_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3358_m18662_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18662_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18663_GM;
MethodInfo m18663_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3358_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18663_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18664_GM;
MethodInfo m18664_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3358_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18664_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18665_GM;
MethodInfo m18665_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3358_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18665_GM};
extern Il2CppType t1352_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18666_GM;
MethodInfo m18666_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3358_TI, &t1352_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18666_GM};
static MethodInfo* t3358_MIs[] =
{
	&m18662_MI,
	&m18663_MI,
	&m18664_MI,
	&m18665_MI,
	&m18666_MI,
	NULL
};
extern MethodInfo m18665_MI;
extern MethodInfo m18664_MI;
static MethodInfo* t3358_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18663_MI,
	&m18665_MI,
	&m18664_MI,
	&m18666_MI,
};
static TypeInfo* t3358_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4815_TI,
};
static Il2CppInterfaceOffsetPair t3358_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4815_TI, 7},
};
extern TypeInfo t1352_TI;
static Il2CppRGCTXData t3358_RGCTXData[3] = 
{
	&m18666_MI/* Method Usage */,
	&t1352_TI/* Class Usage */,
	&m24905_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3358_0_0_0;
extern Il2CppType t3358_1_0_0;
extern Il2CppGenericClass t3358_GC;
TypeInfo t3358_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3358_MIs, t3358_PIs, t3358_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3358_TI, t3358_ITIs, t3358_VT, &EmptyCustomAttributesCache, &t3358_TI, &t3358_0_0_0, &t3358_1_0_0, t3358_IOs, &t3358_GC, NULL, NULL, NULL, t3358_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3358)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6267_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>
extern MethodInfo m32548_MI;
static PropertyInfo t6267____Count_PropertyInfo = 
{
	&t6267_TI, "Count", &m32548_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32549_MI;
static PropertyInfo t6267____IsReadOnly_PropertyInfo = 
{
	&t6267_TI, "IsReadOnly", &m32549_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6267_PIs[] =
{
	&t6267____Count_PropertyInfo,
	&t6267____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32548_GM;
MethodInfo m32548_MI = 
{
	"get_Count", NULL, &t6267_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32548_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32549_GM;
MethodInfo m32549_MI = 
{
	"get_IsReadOnly", NULL, &t6267_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32549_GM};
extern Il2CppType t1352_0_0_0;
extern Il2CppType t1352_0_0_0;
static ParameterInfo t6267_m32550_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32550_GM;
MethodInfo m32550_MI = 
{
	"Add", NULL, &t6267_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6267_m32550_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32550_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32551_GM;
MethodInfo m32551_MI = 
{
	"Clear", NULL, &t6267_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32551_GM};
extern Il2CppType t1352_0_0_0;
static ParameterInfo t6267_m32552_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32552_GM;
MethodInfo m32552_MI = 
{
	"Contains", NULL, &t6267_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6267_m32552_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32552_GM};
extern Il2CppType t1341_0_0_0;
extern Il2CppType t1341_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6267_m32553_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1341_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32553_GM;
MethodInfo m32553_MI = 
{
	"CopyTo", NULL, &t6267_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6267_m32553_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32553_GM};
extern Il2CppType t1352_0_0_0;
static ParameterInfo t6267_m32554_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32554_GM;
MethodInfo m32554_MI = 
{
	"Remove", NULL, &t6267_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6267_m32554_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32554_GM};
static MethodInfo* t6267_MIs[] =
{
	&m32548_MI,
	&m32549_MI,
	&m32550_MI,
	&m32551_MI,
	&m32552_MI,
	&m32553_MI,
	&m32554_MI,
	NULL
};
extern TypeInfo t6269_TI;
static TypeInfo* t6267_ITIs[] = 
{
	&t603_TI,
	&t6269_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6267_0_0_0;
extern Il2CppType t6267_1_0_0;
struct t6267;
extern Il2CppGenericClass t6267_GC;
TypeInfo t6267_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6267_MIs, t6267_PIs, NULL, NULL, NULL, NULL, NULL, &t6267_TI, t6267_ITIs, NULL, &EmptyCustomAttributesCache, &t6267_TI, &t6267_0_0_0, &t6267_1_0_0, NULL, &t6267_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ParameterBuilder>
extern Il2CppType t4815_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32555_GM;
MethodInfo m32555_MI = 
{
	"GetEnumerator", NULL, &t6269_TI, &t4815_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32555_GM};
static MethodInfo* t6269_MIs[] =
{
	&m32555_MI,
	NULL
};
static TypeInfo* t6269_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6269_0_0_0;
extern Il2CppType t6269_1_0_0;
struct t6269;
extern Il2CppGenericClass t6269_GC;
TypeInfo t6269_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6269_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6269_TI, t6269_ITIs, NULL, &EmptyCustomAttributesCache, &t6269_TI, &t6269_0_0_0, &t6269_1_0_0, NULL, &t6269_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6268_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.ParameterBuilder>
extern MethodInfo m32556_MI;
extern MethodInfo m32557_MI;
static PropertyInfo t6268____Item_PropertyInfo = 
{
	&t6268_TI, "Item", &m32556_MI, &m32557_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6268_PIs[] =
{
	&t6268____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1352_0_0_0;
static ParameterInfo t6268_m32558_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32558_GM;
MethodInfo m32558_MI = 
{
	"IndexOf", NULL, &t6268_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6268_m32558_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32558_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1352_0_0_0;
static ParameterInfo t6268_m32559_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32559_GM;
MethodInfo m32559_MI = 
{
	"Insert", NULL, &t6268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6268_m32559_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32559_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6268_m32560_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32560_GM;
MethodInfo m32560_MI = 
{
	"RemoveAt", NULL, &t6268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6268_m32560_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32560_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6268_m32556_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1352_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32556_GM;
MethodInfo m32556_MI = 
{
	"get_Item", NULL, &t6268_TI, &t1352_0_0_0, RuntimeInvoker_t29_t44, t6268_m32556_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32556_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1352_0_0_0;
static ParameterInfo t6268_m32557_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32557_GM;
MethodInfo m32557_MI = 
{
	"set_Item", NULL, &t6268_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6268_m32557_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32557_GM};
static MethodInfo* t6268_MIs[] =
{
	&m32558_MI,
	&m32559_MI,
	&m32560_MI,
	&m32556_MI,
	&m32557_MI,
	NULL
};
static TypeInfo* t6268_ITIs[] = 
{
	&t603_TI,
	&t6267_TI,
	&t6269_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6268_0_0_0;
extern Il2CppType t6268_1_0_0;
struct t6268;
extern Il2CppGenericClass t6268_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6268_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6268_MIs, t6268_PIs, NULL, NULL, NULL, NULL, NULL, &t6268_TI, t6268_ITIs, NULL, &t1908__CustomAttributeCache, &t6268_TI, &t6268_0_0_0, &t6268_1_0_0, NULL, &t6268_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6270_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>
extern MethodInfo m32561_MI;
static PropertyInfo t6270____Count_PropertyInfo = 
{
	&t6270_TI, "Count", &m32561_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32562_MI;
static PropertyInfo t6270____IsReadOnly_PropertyInfo = 
{
	&t6270_TI, "IsReadOnly", &m32562_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6270_PIs[] =
{
	&t6270____Count_PropertyInfo,
	&t6270____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32561_GM;
MethodInfo m32561_MI = 
{
	"get_Count", NULL, &t6270_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32561_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32562_GM;
MethodInfo m32562_MI = 
{
	"get_IsReadOnly", NULL, &t6270_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32562_GM};
extern Il2CppType t2018_0_0_0;
extern Il2CppType t2018_0_0_0;
static ParameterInfo t6270_m32563_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2018_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32563_GM;
MethodInfo m32563_MI = 
{
	"Add", NULL, &t6270_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6270_m32563_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32563_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32564_GM;
MethodInfo m32564_MI = 
{
	"Clear", NULL, &t6270_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32564_GM};
extern Il2CppType t2018_0_0_0;
static ParameterInfo t6270_m32565_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2018_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32565_GM;
MethodInfo m32565_MI = 
{
	"Contains", NULL, &t6270_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6270_m32565_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32565_GM};
extern Il2CppType t3621_0_0_0;
extern Il2CppType t3621_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6270_m32566_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3621_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32566_GM;
MethodInfo m32566_MI = 
{
	"CopyTo", NULL, &t6270_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6270_m32566_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32566_GM};
extern Il2CppType t2018_0_0_0;
static ParameterInfo t6270_m32567_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2018_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32567_GM;
MethodInfo m32567_MI = 
{
	"Remove", NULL, &t6270_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6270_m32567_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32567_GM};
static MethodInfo* t6270_MIs[] =
{
	&m32561_MI,
	&m32562_MI,
	&m32563_MI,
	&m32564_MI,
	&m32565_MI,
	&m32566_MI,
	&m32567_MI,
	NULL
};
extern TypeInfo t6272_TI;
static TypeInfo* t6270_ITIs[] = 
{
	&t603_TI,
	&t6272_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6270_0_0_0;
extern Il2CppType t6270_1_0_0;
struct t6270;
extern Il2CppGenericClass t6270_GC;
TypeInfo t6270_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6270_MIs, t6270_PIs, NULL, NULL, NULL, NULL, NULL, &t6270_TI, t6270_ITIs, NULL, &EmptyCustomAttributesCache, &t6270_TI, &t6270_0_0_0, &t6270_1_0_0, NULL, &t6270_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ParameterBuilder>
extern Il2CppType t4817_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32568_GM;
MethodInfo m32568_MI = 
{
	"GetEnumerator", NULL, &t6272_TI, &t4817_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32568_GM};
static MethodInfo* t6272_MIs[] =
{
	&m32568_MI,
	NULL
};
static TypeInfo* t6272_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6272_0_0_0;
extern Il2CppType t6272_1_0_0;
struct t6272;
extern Il2CppGenericClass t6272_GC;
TypeInfo t6272_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6272_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6272_TI, t6272_ITIs, NULL, &EmptyCustomAttributesCache, &t6272_TI, &t6272_0_0_0, &t6272_1_0_0, NULL, &t6272_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4817_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>
extern MethodInfo m32569_MI;
static PropertyInfo t4817____Current_PropertyInfo = 
{
	&t4817_TI, "Current", &m32569_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4817_PIs[] =
{
	&t4817____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2018_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32569_GM;
MethodInfo m32569_MI = 
{
	"get_Current", NULL, &t4817_TI, &t2018_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32569_GM};
static MethodInfo* t4817_MIs[] =
{
	&m32569_MI,
	NULL
};
static TypeInfo* t4817_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4817_0_0_0;
extern Il2CppType t4817_1_0_0;
struct t4817;
extern Il2CppGenericClass t4817_GC;
TypeInfo t4817_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4817_MIs, t4817_PIs, NULL, NULL, NULL, NULL, NULL, &t4817_TI, t4817_ITIs, NULL, &EmptyCustomAttributesCache, &t4817_TI, &t4817_0_0_0, &t4817_1_0_0, NULL, &t4817_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3359.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3359_TI;
#include "t3359MD.h"

extern TypeInfo t2018_TI;
extern MethodInfo m18671_MI;
extern MethodInfo m24916_MI;
struct t20;
#define m24916(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>
extern Il2CppType t20_0_0_1;
FieldInfo t3359_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3359_TI, offsetof(t3359, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3359_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3359_TI, offsetof(t3359, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3359_FIs[] =
{
	&t3359_f0_FieldInfo,
	&t3359_f1_FieldInfo,
	NULL
};
extern MethodInfo m18668_MI;
static PropertyInfo t3359____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3359_TI, "System.Collections.IEnumerator.Current", &m18668_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3359____Current_PropertyInfo = 
{
	&t3359_TI, "Current", &m18671_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3359_PIs[] =
{
	&t3359____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3359____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3359_m18667_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18667_GM;
MethodInfo m18667_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3359_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3359_m18667_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18667_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18668_GM;
MethodInfo m18668_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3359_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18668_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18669_GM;
MethodInfo m18669_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3359_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18669_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18670_GM;
MethodInfo m18670_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3359_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18670_GM};
extern Il2CppType t2018_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18671_GM;
MethodInfo m18671_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3359_TI, &t2018_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18671_GM};
static MethodInfo* t3359_MIs[] =
{
	&m18667_MI,
	&m18668_MI,
	&m18669_MI,
	&m18670_MI,
	&m18671_MI,
	NULL
};
extern MethodInfo m18670_MI;
extern MethodInfo m18669_MI;
static MethodInfo* t3359_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18668_MI,
	&m18670_MI,
	&m18669_MI,
	&m18671_MI,
};
static TypeInfo* t3359_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4817_TI,
};
static Il2CppInterfaceOffsetPair t3359_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4817_TI, 7},
};
extern TypeInfo t2018_TI;
static Il2CppRGCTXData t3359_RGCTXData[3] = 
{
	&m18671_MI/* Method Usage */,
	&t2018_TI/* Class Usage */,
	&m24916_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3359_0_0_0;
extern Il2CppType t3359_1_0_0;
extern Il2CppGenericClass t3359_GC;
TypeInfo t3359_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3359_MIs, t3359_PIs, t3359_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3359_TI, t3359_ITIs, t3359_VT, &EmptyCustomAttributesCache, &t3359_TI, &t3359_0_0_0, &t3359_1_0_0, t3359_IOs, &t3359_GC, NULL, NULL, NULL, t3359_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3359)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6271_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterBuilder>
extern MethodInfo m32570_MI;
extern MethodInfo m32571_MI;
static PropertyInfo t6271____Item_PropertyInfo = 
{
	&t6271_TI, "Item", &m32570_MI, &m32571_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6271_PIs[] =
{
	&t6271____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2018_0_0_0;
static ParameterInfo t6271_m32572_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2018_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32572_GM;
MethodInfo m32572_MI = 
{
	"IndexOf", NULL, &t6271_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6271_m32572_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32572_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2018_0_0_0;
static ParameterInfo t6271_m32573_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2018_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32573_GM;
MethodInfo m32573_MI = 
{
	"Insert", NULL, &t6271_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6271_m32573_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32573_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6271_m32574_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32574_GM;
MethodInfo m32574_MI = 
{
	"RemoveAt", NULL, &t6271_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6271_m32574_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32574_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6271_m32570_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2018_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32570_GM;
MethodInfo m32570_MI = 
{
	"get_Item", NULL, &t6271_TI, &t2018_0_0_0, RuntimeInvoker_t29_t44, t6271_m32570_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32570_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2018_0_0_0;
static ParameterInfo t6271_m32571_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2018_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32571_GM;
MethodInfo m32571_MI = 
{
	"set_Item", NULL, &t6271_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6271_m32571_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32571_GM};
static MethodInfo* t6271_MIs[] =
{
	&m32572_MI,
	&m32573_MI,
	&m32574_MI,
	&m32570_MI,
	&m32571_MI,
	NULL
};
static TypeInfo* t6271_ITIs[] = 
{
	&t603_TI,
	&t6270_TI,
	&t6272_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6271_0_0_0;
extern Il2CppType t6271_1_0_0;
struct t6271;
extern Il2CppGenericClass t6271_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6271_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6271_MIs, t6271_PIs, NULL, NULL, NULL, NULL, NULL, &t6271_TI, t6271_ITIs, NULL, &t1908__CustomAttributeCache, &t6271_TI, &t6271_0_0_0, &t6271_1_0_0, NULL, &t6271_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4819_TI;

#include "t1348.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>
extern MethodInfo m32575_MI;
static PropertyInfo t4819____Current_PropertyInfo = 
{
	&t4819_TI, "Current", &m32575_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4819_PIs[] =
{
	&t4819____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1348_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32575_GM;
MethodInfo m32575_MI = 
{
	"get_Current", NULL, &t4819_TI, &t1348_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32575_GM};
static MethodInfo* t4819_MIs[] =
{
	&m32575_MI,
	NULL
};
static TypeInfo* t4819_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4819_0_0_0;
extern Il2CppType t4819_1_0_0;
struct t4819;
extern Il2CppGenericClass t4819_GC;
TypeInfo t4819_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4819_MIs, t4819_PIs, NULL, NULL, NULL, NULL, NULL, &t4819_TI, t4819_ITIs, NULL, &EmptyCustomAttributesCache, &t4819_TI, &t4819_0_0_0, &t4819_1_0_0, NULL, &t4819_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3360.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3360_TI;
#include "t3360MD.h"

extern TypeInfo t1348_TI;
extern MethodInfo m18676_MI;
extern MethodInfo m24927_MI;
struct t20;
#define m24927(__this, p0, method) (t1348 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>
extern Il2CppType t20_0_0_1;
FieldInfo t3360_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3360_TI, offsetof(t3360, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3360_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3360_TI, offsetof(t3360, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3360_FIs[] =
{
	&t3360_f0_FieldInfo,
	&t3360_f1_FieldInfo,
	NULL
};
extern MethodInfo m18673_MI;
static PropertyInfo t3360____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3360_TI, "System.Collections.IEnumerator.Current", &m18673_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3360____Current_PropertyInfo = 
{
	&t3360_TI, "Current", &m18676_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3360_PIs[] =
{
	&t3360____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3360____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3360_m18672_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18672_GM;
MethodInfo m18672_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3360_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3360_m18672_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18672_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18673_GM;
MethodInfo m18673_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3360_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18673_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18674_GM;
MethodInfo m18674_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3360_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18674_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18675_GM;
MethodInfo m18675_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3360_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18675_GM};
extern Il2CppType t1348_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18676_GM;
MethodInfo m18676_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3360_TI, &t1348_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18676_GM};
static MethodInfo* t3360_MIs[] =
{
	&m18672_MI,
	&m18673_MI,
	&m18674_MI,
	&m18675_MI,
	&m18676_MI,
	NULL
};
extern MethodInfo m18675_MI;
extern MethodInfo m18674_MI;
static MethodInfo* t3360_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18673_MI,
	&m18675_MI,
	&m18674_MI,
	&m18676_MI,
};
static TypeInfo* t3360_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4819_TI,
};
static Il2CppInterfaceOffsetPair t3360_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4819_TI, 7},
};
extern TypeInfo t1348_TI;
static Il2CppRGCTXData t3360_RGCTXData[3] = 
{
	&m18676_MI/* Method Usage */,
	&t1348_TI/* Class Usage */,
	&m24927_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3360_0_0_0;
extern Il2CppType t3360_1_0_0;
extern Il2CppGenericClass t3360_GC;
TypeInfo t3360_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3360_MIs, t3360_PIs, t3360_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3360_TI, t3360_ITIs, t3360_VT, &EmptyCustomAttributesCache, &t3360_TI, &t3360_0_0_0, &t3360_1_0_0, t3360_IOs, &t3360_GC, NULL, NULL, NULL, t3360_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3360)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6273_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>
extern MethodInfo m32576_MI;
static PropertyInfo t6273____Count_PropertyInfo = 
{
	&t6273_TI, "Count", &m32576_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32577_MI;
static PropertyInfo t6273____IsReadOnly_PropertyInfo = 
{
	&t6273_TI, "IsReadOnly", &m32577_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6273_PIs[] =
{
	&t6273____Count_PropertyInfo,
	&t6273____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32576_GM;
MethodInfo m32576_MI = 
{
	"get_Count", NULL, &t6273_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32576_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32577_GM;
MethodInfo m32577_MI = 
{
	"get_IsReadOnly", NULL, &t6273_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32577_GM};
extern Il2CppType t1348_0_0_0;
extern Il2CppType t1348_0_0_0;
static ParameterInfo t6273_m32578_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1348_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32578_GM;
MethodInfo m32578_MI = 
{
	"Add", NULL, &t6273_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6273_m32578_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32578_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32579_GM;
MethodInfo m32579_MI = 
{
	"Clear", NULL, &t6273_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32579_GM};
extern Il2CppType t1348_0_0_0;
static ParameterInfo t6273_m32580_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1348_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32580_GM;
MethodInfo m32580_MI = 
{
	"Contains", NULL, &t6273_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6273_m32580_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32580_GM};
extern Il2CppType t1350_0_0_0;
extern Il2CppType t1350_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6273_m32581_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1350_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32581_GM;
MethodInfo m32581_MI = 
{
	"CopyTo", NULL, &t6273_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6273_m32581_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32581_GM};
extern Il2CppType t1348_0_0_0;
static ParameterInfo t6273_m32582_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1348_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32582_GM;
MethodInfo m32582_MI = 
{
	"Remove", NULL, &t6273_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6273_m32582_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32582_GM};
static MethodInfo* t6273_MIs[] =
{
	&m32576_MI,
	&m32577_MI,
	&m32578_MI,
	&m32579_MI,
	&m32580_MI,
	&m32581_MI,
	&m32582_MI,
	NULL
};
extern TypeInfo t6275_TI;
static TypeInfo* t6273_ITIs[] = 
{
	&t603_TI,
	&t6275_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6273_0_0_0;
extern Il2CppType t6273_1_0_0;
struct t6273;
extern Il2CppGenericClass t6273_GC;
TypeInfo t6273_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6273_MIs, t6273_PIs, NULL, NULL, NULL, NULL, NULL, &t6273_TI, t6273_ITIs, NULL, &EmptyCustomAttributesCache, &t6273_TI, &t6273_0_0_0, &t6273_1_0_0, NULL, &t6273_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.GenericTypeParameterBuilder>
extern Il2CppType t4819_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32583_GM;
MethodInfo m32583_MI = 
{
	"GetEnumerator", NULL, &t6275_TI, &t4819_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32583_GM};
static MethodInfo* t6275_MIs[] =
{
	&m32583_MI,
	NULL
};
static TypeInfo* t6275_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6275_0_0_0;
extern Il2CppType t6275_1_0_0;
struct t6275;
extern Il2CppGenericClass t6275_GC;
TypeInfo t6275_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6275_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6275_TI, t6275_ITIs, NULL, &EmptyCustomAttributesCache, &t6275_TI, &t6275_0_0_0, &t6275_1_0_0, NULL, &t6275_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6274_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.GenericTypeParameterBuilder>
extern MethodInfo m32584_MI;
extern MethodInfo m32585_MI;
static PropertyInfo t6274____Item_PropertyInfo = 
{
	&t6274_TI, "Item", &m32584_MI, &m32585_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6274_PIs[] =
{
	&t6274____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1348_0_0_0;
static ParameterInfo t6274_m32586_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1348_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32586_GM;
MethodInfo m32586_MI = 
{
	"IndexOf", NULL, &t6274_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6274_m32586_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32586_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1348_0_0_0;
static ParameterInfo t6274_m32587_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1348_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32587_GM;
MethodInfo m32587_MI = 
{
	"Insert", NULL, &t6274_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6274_m32587_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32587_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6274_m32588_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32588_GM;
MethodInfo m32588_MI = 
{
	"RemoveAt", NULL, &t6274_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6274_m32588_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32588_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6274_m32584_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1348_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32584_GM;
MethodInfo m32584_MI = 
{
	"get_Item", NULL, &t6274_TI, &t1348_0_0_0, RuntimeInvoker_t29_t44, t6274_m32584_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32584_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1348_0_0_0;
static ParameterInfo t6274_m32585_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1348_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32585_GM;
MethodInfo m32585_MI = 
{
	"set_Item", NULL, &t6274_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6274_m32585_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32585_GM};
static MethodInfo* t6274_MIs[] =
{
	&m32586_MI,
	&m32587_MI,
	&m32588_MI,
	&m32584_MI,
	&m32585_MI,
	NULL
};
static TypeInfo* t6274_ITIs[] = 
{
	&t603_TI,
	&t6273_TI,
	&t6275_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6274_0_0_0;
extern Il2CppType t6274_1_0_0;
struct t6274;
extern Il2CppGenericClass t6274_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6274_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6274_MIs, t6274_PIs, NULL, NULL, NULL, NULL, NULL, &t6274_TI, t6274_ITIs, NULL, &t1908__CustomAttributeCache, &t6274_TI, &t6274_0_0_0, &t6274_1_0_0, NULL, &t6274_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4821_TI;

#include "t1349.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.MethodBuilder>
extern MethodInfo m32589_MI;
static PropertyInfo t4821____Current_PropertyInfo = 
{
	&t4821_TI, "Current", &m32589_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4821_PIs[] =
{
	&t4821____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1349_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32589_GM;
MethodInfo m32589_MI = 
{
	"get_Current", NULL, &t4821_TI, &t1349_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32589_GM};
static MethodInfo* t4821_MIs[] =
{
	&m32589_MI,
	NULL
};
static TypeInfo* t4821_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4821_0_0_0;
extern Il2CppType t4821_1_0_0;
struct t4821;
extern Il2CppGenericClass t4821_GC;
TypeInfo t4821_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4821_MIs, t4821_PIs, NULL, NULL, NULL, NULL, NULL, &t4821_TI, t4821_ITIs, NULL, &EmptyCustomAttributesCache, &t4821_TI, &t4821_0_0_0, &t4821_1_0_0, NULL, &t4821_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3361.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3361_TI;
#include "t3361MD.h"

extern TypeInfo t1349_TI;
extern MethodInfo m18681_MI;
extern MethodInfo m24938_MI;
struct t20;
#define m24938(__this, p0, method) (t1349 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>
extern Il2CppType t20_0_0_1;
FieldInfo t3361_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3361_TI, offsetof(t3361, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3361_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3361_TI, offsetof(t3361, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3361_FIs[] =
{
	&t3361_f0_FieldInfo,
	&t3361_f1_FieldInfo,
	NULL
};
extern MethodInfo m18678_MI;
static PropertyInfo t3361____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3361_TI, "System.Collections.IEnumerator.Current", &m18678_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3361____Current_PropertyInfo = 
{
	&t3361_TI, "Current", &m18681_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3361_PIs[] =
{
	&t3361____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3361____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3361_m18677_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18677_GM;
MethodInfo m18677_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3361_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3361_m18677_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18677_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18678_GM;
MethodInfo m18678_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3361_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18678_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18679_GM;
MethodInfo m18679_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3361_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18679_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18680_GM;
MethodInfo m18680_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3361_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18680_GM};
extern Il2CppType t1349_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18681_GM;
MethodInfo m18681_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3361_TI, &t1349_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18681_GM};
static MethodInfo* t3361_MIs[] =
{
	&m18677_MI,
	&m18678_MI,
	&m18679_MI,
	&m18680_MI,
	&m18681_MI,
	NULL
};
extern MethodInfo m18680_MI;
extern MethodInfo m18679_MI;
static MethodInfo* t3361_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18678_MI,
	&m18680_MI,
	&m18679_MI,
	&m18681_MI,
};
static TypeInfo* t3361_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4821_TI,
};
static Il2CppInterfaceOffsetPair t3361_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4821_TI, 7},
};
extern TypeInfo t1349_TI;
static Il2CppRGCTXData t3361_RGCTXData[3] = 
{
	&m18681_MI/* Method Usage */,
	&t1349_TI/* Class Usage */,
	&m24938_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3361_0_0_0;
extern Il2CppType t3361_1_0_0;
extern Il2CppGenericClass t3361_GC;
TypeInfo t3361_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3361_MIs, t3361_PIs, t3361_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3361_TI, t3361_ITIs, t3361_VT, &EmptyCustomAttributesCache, &t3361_TI, &t3361_0_0_0, &t3361_1_0_0, t3361_IOs, &t3361_GC, NULL, NULL, NULL, t3361_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3361)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6276_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>
extern MethodInfo m32590_MI;
static PropertyInfo t6276____Count_PropertyInfo = 
{
	&t6276_TI, "Count", &m32590_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32591_MI;
static PropertyInfo t6276____IsReadOnly_PropertyInfo = 
{
	&t6276_TI, "IsReadOnly", &m32591_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6276_PIs[] =
{
	&t6276____Count_PropertyInfo,
	&t6276____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32590_GM;
MethodInfo m32590_MI = 
{
	"get_Count", NULL, &t6276_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32590_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32591_GM;
MethodInfo m32591_MI = 
{
	"get_IsReadOnly", NULL, &t6276_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32591_GM};
extern Il2CppType t1349_0_0_0;
extern Il2CppType t1349_0_0_0;
static ParameterInfo t6276_m32592_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1349_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32592_GM;
MethodInfo m32592_MI = 
{
	"Add", NULL, &t6276_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6276_m32592_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32592_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32593_GM;
MethodInfo m32593_MI = 
{
	"Clear", NULL, &t6276_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32593_GM};
extern Il2CppType t1349_0_0_0;
static ParameterInfo t6276_m32594_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1349_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32594_GM;
MethodInfo m32594_MI = 
{
	"Contains", NULL, &t6276_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6276_m32594_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32594_GM};
extern Il2CppType t1354_0_0_0;
extern Il2CppType t1354_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6276_m32595_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1354_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32595_GM;
MethodInfo m32595_MI = 
{
	"CopyTo", NULL, &t6276_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6276_m32595_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32595_GM};
extern Il2CppType t1349_0_0_0;
static ParameterInfo t6276_m32596_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1349_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32596_GM;
MethodInfo m32596_MI = 
{
	"Remove", NULL, &t6276_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6276_m32596_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32596_GM};
static MethodInfo* t6276_MIs[] =
{
	&m32590_MI,
	&m32591_MI,
	&m32592_MI,
	&m32593_MI,
	&m32594_MI,
	&m32595_MI,
	&m32596_MI,
	NULL
};
extern TypeInfo t6278_TI;
static TypeInfo* t6276_ITIs[] = 
{
	&t603_TI,
	&t6278_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6276_0_0_0;
extern Il2CppType t6276_1_0_0;
struct t6276;
extern Il2CppGenericClass t6276_GC;
TypeInfo t6276_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6276_MIs, t6276_PIs, NULL, NULL, NULL, NULL, NULL, &t6276_TI, t6276_ITIs, NULL, &EmptyCustomAttributesCache, &t6276_TI, &t6276_0_0_0, &t6276_1_0_0, NULL, &t6276_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.MethodBuilder>
extern Il2CppType t4821_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32597_GM;
MethodInfo m32597_MI = 
{
	"GetEnumerator", NULL, &t6278_TI, &t4821_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32597_GM};
static MethodInfo* t6278_MIs[] =
{
	&m32597_MI,
	NULL
};
static TypeInfo* t6278_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6278_0_0_0;
extern Il2CppType t6278_1_0_0;
struct t6278;
extern Il2CppGenericClass t6278_GC;
TypeInfo t6278_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6278_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6278_TI, t6278_ITIs, NULL, &EmptyCustomAttributesCache, &t6278_TI, &t6278_0_0_0, &t6278_1_0_0, NULL, &t6278_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6277_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.MethodBuilder>
extern MethodInfo m32598_MI;
extern MethodInfo m32599_MI;
static PropertyInfo t6277____Item_PropertyInfo = 
{
	&t6277_TI, "Item", &m32598_MI, &m32599_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6277_PIs[] =
{
	&t6277____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1349_0_0_0;
static ParameterInfo t6277_m32600_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1349_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32600_GM;
MethodInfo m32600_MI = 
{
	"IndexOf", NULL, &t6277_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6277_m32600_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32600_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1349_0_0_0;
static ParameterInfo t6277_m32601_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1349_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32601_GM;
MethodInfo m32601_MI = 
{
	"Insert", NULL, &t6277_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6277_m32601_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32601_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6277_m32602_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32602_GM;
MethodInfo m32602_MI = 
{
	"RemoveAt", NULL, &t6277_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6277_m32602_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32602_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6277_m32598_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1349_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32598_GM;
MethodInfo m32598_MI = 
{
	"get_Item", NULL, &t6277_TI, &t1349_0_0_0, RuntimeInvoker_t29_t44, t6277_m32598_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32598_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1349_0_0_0;
static ParameterInfo t6277_m32599_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1349_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32599_GM;
MethodInfo m32599_MI = 
{
	"set_Item", NULL, &t6277_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6277_m32599_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32599_GM};
static MethodInfo* t6277_MIs[] =
{
	&m32600_MI,
	&m32601_MI,
	&m32602_MI,
	&m32598_MI,
	&m32599_MI,
	NULL
};
static TypeInfo* t6277_ITIs[] = 
{
	&t603_TI,
	&t6276_TI,
	&t6278_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6277_0_0_0;
extern Il2CppType t6277_1_0_0;
struct t6277;
extern Il2CppGenericClass t6277_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6277_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6277_MIs, t6277_PIs, NULL, NULL, NULL, NULL, NULL, &t6277_TI, t6277_ITIs, NULL, &t1908__CustomAttributeCache, &t6277_TI, &t6277_0_0_0, &t6277_1_0_0, NULL, &t6277_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6279_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>
extern MethodInfo m32603_MI;
static PropertyInfo t6279____Count_PropertyInfo = 
{
	&t6279_TI, "Count", &m32603_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32604_MI;
static PropertyInfo t6279____IsReadOnly_PropertyInfo = 
{
	&t6279_TI, "IsReadOnly", &m32604_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6279_PIs[] =
{
	&t6279____Count_PropertyInfo,
	&t6279____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32603_GM;
MethodInfo m32603_MI = 
{
	"get_Count", NULL, &t6279_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32603_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32604_GM;
MethodInfo m32604_MI = 
{
	"get_IsReadOnly", NULL, &t6279_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32604_GM};
extern Il2CppType t2014_0_0_0;
extern Il2CppType t2014_0_0_0;
static ParameterInfo t6279_m32605_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2014_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32605_GM;
MethodInfo m32605_MI = 
{
	"Add", NULL, &t6279_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6279_m32605_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32605_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32606_GM;
MethodInfo m32606_MI = 
{
	"Clear", NULL, &t6279_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32606_GM};
extern Il2CppType t2014_0_0_0;
static ParameterInfo t6279_m32607_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2014_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32607_GM;
MethodInfo m32607_MI = 
{
	"Contains", NULL, &t6279_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6279_m32607_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32607_GM};
extern Il2CppType t3622_0_0_0;
extern Il2CppType t3622_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6279_m32608_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3622_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32608_GM;
MethodInfo m32608_MI = 
{
	"CopyTo", NULL, &t6279_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6279_m32608_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32608_GM};
extern Il2CppType t2014_0_0_0;
static ParameterInfo t6279_m32609_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2014_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32609_GM;
MethodInfo m32609_MI = 
{
	"Remove", NULL, &t6279_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6279_m32609_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32609_GM};
static MethodInfo* t6279_MIs[] =
{
	&m32603_MI,
	&m32604_MI,
	&m32605_MI,
	&m32606_MI,
	&m32607_MI,
	&m32608_MI,
	&m32609_MI,
	NULL
};
extern TypeInfo t6281_TI;
static TypeInfo* t6279_ITIs[] = 
{
	&t603_TI,
	&t6281_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6279_0_0_0;
extern Il2CppType t6279_1_0_0;
struct t6279;
extern Il2CppGenericClass t6279_GC;
TypeInfo t6279_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6279_MIs, t6279_PIs, NULL, NULL, NULL, NULL, NULL, &t6279_TI, t6279_ITIs, NULL, &EmptyCustomAttributesCache, &t6279_TI, &t6279_0_0_0, &t6279_1_0_0, NULL, &t6279_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MethodBuilder>
extern Il2CppType t4823_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32610_GM;
MethodInfo m32610_MI = 
{
	"GetEnumerator", NULL, &t6281_TI, &t4823_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32610_GM};
static MethodInfo* t6281_MIs[] =
{
	&m32610_MI,
	NULL
};
static TypeInfo* t6281_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6281_0_0_0;
extern Il2CppType t6281_1_0_0;
struct t6281;
extern Il2CppGenericClass t6281_GC;
TypeInfo t6281_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6281_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6281_TI, t6281_ITIs, NULL, &EmptyCustomAttributesCache, &t6281_TI, &t6281_0_0_0, &t6281_1_0_0, NULL, &t6281_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4823_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MethodBuilder>
extern MethodInfo m32611_MI;
static PropertyInfo t4823____Current_PropertyInfo = 
{
	&t4823_TI, "Current", &m32611_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4823_PIs[] =
{
	&t4823____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2014_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32611_GM;
MethodInfo m32611_MI = 
{
	"get_Current", NULL, &t4823_TI, &t2014_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32611_GM};
static MethodInfo* t4823_MIs[] =
{
	&m32611_MI,
	NULL
};
static TypeInfo* t4823_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4823_0_0_0;
extern Il2CppType t4823_1_0_0;
struct t4823;
extern Il2CppGenericClass t4823_GC;
TypeInfo t4823_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4823_MIs, t4823_PIs, NULL, NULL, NULL, NULL, NULL, &t4823_TI, t4823_ITIs, NULL, &EmptyCustomAttributesCache, &t4823_TI, &t4823_0_0_0, &t4823_1_0_0, NULL, &t4823_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3362.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3362_TI;
#include "t3362MD.h"

extern TypeInfo t2014_TI;
extern MethodInfo m18686_MI;
extern MethodInfo m24949_MI;
struct t20;
#define m24949(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>
extern Il2CppType t20_0_0_1;
FieldInfo t3362_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3362_TI, offsetof(t3362, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3362_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3362_TI, offsetof(t3362, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3362_FIs[] =
{
	&t3362_f0_FieldInfo,
	&t3362_f1_FieldInfo,
	NULL
};
extern MethodInfo m18683_MI;
static PropertyInfo t3362____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3362_TI, "System.Collections.IEnumerator.Current", &m18683_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3362____Current_PropertyInfo = 
{
	&t3362_TI, "Current", &m18686_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3362_PIs[] =
{
	&t3362____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3362____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3362_m18682_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18682_GM;
MethodInfo m18682_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3362_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3362_m18682_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18682_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18683_GM;
MethodInfo m18683_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3362_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18683_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18684_GM;
MethodInfo m18684_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3362_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18684_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18685_GM;
MethodInfo m18685_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3362_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18685_GM};
extern Il2CppType t2014_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18686_GM;
MethodInfo m18686_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3362_TI, &t2014_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18686_GM};
static MethodInfo* t3362_MIs[] =
{
	&m18682_MI,
	&m18683_MI,
	&m18684_MI,
	&m18685_MI,
	&m18686_MI,
	NULL
};
extern MethodInfo m18685_MI;
extern MethodInfo m18684_MI;
static MethodInfo* t3362_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18683_MI,
	&m18685_MI,
	&m18684_MI,
	&m18686_MI,
};
static TypeInfo* t3362_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4823_TI,
};
static Il2CppInterfaceOffsetPair t3362_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4823_TI, 7},
};
extern TypeInfo t2014_TI;
static Il2CppRGCTXData t3362_RGCTXData[3] = 
{
	&m18686_MI/* Method Usage */,
	&t2014_TI/* Class Usage */,
	&m24949_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3362_0_0_0;
extern Il2CppType t3362_1_0_0;
extern Il2CppGenericClass t3362_GC;
TypeInfo t3362_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3362_MIs, t3362_PIs, t3362_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3362_TI, t3362_ITIs, t3362_VT, &EmptyCustomAttributesCache, &t3362_TI, &t3362_0_0_0, &t3362_1_0_0, t3362_IOs, &t3362_GC, NULL, NULL, NULL, t3362_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3362)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6280_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBuilder>
extern MethodInfo m32612_MI;
extern MethodInfo m32613_MI;
static PropertyInfo t6280____Item_PropertyInfo = 
{
	&t6280_TI, "Item", &m32612_MI, &m32613_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6280_PIs[] =
{
	&t6280____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2014_0_0_0;
static ParameterInfo t6280_m32614_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2014_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32614_GM;
MethodInfo m32614_MI = 
{
	"IndexOf", NULL, &t6280_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6280_m32614_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32614_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2014_0_0_0;
static ParameterInfo t6280_m32615_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2014_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32615_GM;
MethodInfo m32615_MI = 
{
	"Insert", NULL, &t6280_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6280_m32615_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32615_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6280_m32616_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32616_GM;
MethodInfo m32616_MI = 
{
	"RemoveAt", NULL, &t6280_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6280_m32616_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32616_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6280_m32612_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2014_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32612_GM;
MethodInfo m32612_MI = 
{
	"get_Item", NULL, &t6280_TI, &t2014_0_0_0, RuntimeInvoker_t29_t44, t6280_m32612_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32612_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2014_0_0_0;
static ParameterInfo t6280_m32613_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2014_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32613_GM;
MethodInfo m32613_MI = 
{
	"set_Item", NULL, &t6280_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6280_m32613_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32613_GM};
static MethodInfo* t6280_MIs[] =
{
	&m32614_MI,
	&m32615_MI,
	&m32616_MI,
	&m32612_MI,
	&m32613_MI,
	NULL
};
static TypeInfo* t6280_ITIs[] = 
{
	&t603_TI,
	&t6279_TI,
	&t6281_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6280_0_0_0;
extern Il2CppType t6280_1_0_0;
struct t6280;
extern Il2CppGenericClass t6280_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6280_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6280_MIs, t6280_PIs, NULL, NULL, NULL, NULL, NULL, &t6280_TI, t6280_ITIs, NULL, &t1908__CustomAttributeCache, &t6280_TI, &t6280_0_0_0, &t6280_1_0_0, NULL, &t6280_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4825_TI;

#include "t1339.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ConstructorBuilder>
extern MethodInfo m32617_MI;
static PropertyInfo t4825____Current_PropertyInfo = 
{
	&t4825_TI, "Current", &m32617_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4825_PIs[] =
{
	&t4825____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1339_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32617_GM;
MethodInfo m32617_MI = 
{
	"get_Current", NULL, &t4825_TI, &t1339_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32617_GM};
static MethodInfo* t4825_MIs[] =
{
	&m32617_MI,
	NULL
};
static TypeInfo* t4825_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4825_0_0_0;
extern Il2CppType t4825_1_0_0;
struct t4825;
extern Il2CppGenericClass t4825_GC;
TypeInfo t4825_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4825_MIs, t4825_PIs, NULL, NULL, NULL, NULL, NULL, &t4825_TI, t4825_ITIs, NULL, &EmptyCustomAttributesCache, &t4825_TI, &t4825_0_0_0, &t4825_1_0_0, NULL, &t4825_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3363.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3363_TI;
#include "t3363MD.h"

extern TypeInfo t1339_TI;
extern MethodInfo m18691_MI;
extern MethodInfo m24960_MI;
struct t20;
#define m24960(__this, p0, method) (t1339 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>
extern Il2CppType t20_0_0_1;
FieldInfo t3363_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3363_TI, offsetof(t3363, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3363_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3363_TI, offsetof(t3363, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3363_FIs[] =
{
	&t3363_f0_FieldInfo,
	&t3363_f1_FieldInfo,
	NULL
};
extern MethodInfo m18688_MI;
static PropertyInfo t3363____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3363_TI, "System.Collections.IEnumerator.Current", &m18688_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3363____Current_PropertyInfo = 
{
	&t3363_TI, "Current", &m18691_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3363_PIs[] =
{
	&t3363____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3363____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3363_m18687_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18687_GM;
MethodInfo m18687_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3363_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3363_m18687_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18687_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18688_GM;
MethodInfo m18688_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3363_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18688_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18689_GM;
MethodInfo m18689_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3363_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18689_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18690_GM;
MethodInfo m18690_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3363_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18690_GM};
extern Il2CppType t1339_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18691_GM;
MethodInfo m18691_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3363_TI, &t1339_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18691_GM};
static MethodInfo* t3363_MIs[] =
{
	&m18687_MI,
	&m18688_MI,
	&m18689_MI,
	&m18690_MI,
	&m18691_MI,
	NULL
};
extern MethodInfo m18690_MI;
extern MethodInfo m18689_MI;
static MethodInfo* t3363_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18688_MI,
	&m18690_MI,
	&m18689_MI,
	&m18691_MI,
};
static TypeInfo* t3363_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4825_TI,
};
static Il2CppInterfaceOffsetPair t3363_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4825_TI, 7},
};
extern TypeInfo t1339_TI;
static Il2CppRGCTXData t3363_RGCTXData[3] = 
{
	&m18691_MI/* Method Usage */,
	&t1339_TI/* Class Usage */,
	&m24960_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3363_0_0_0;
extern Il2CppType t3363_1_0_0;
extern Il2CppGenericClass t3363_GC;
TypeInfo t3363_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3363_MIs, t3363_PIs, t3363_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3363_TI, t3363_ITIs, t3363_VT, &EmptyCustomAttributesCache, &t3363_TI, &t3363_0_0_0, &t3363_1_0_0, t3363_IOs, &t3363_GC, NULL, NULL, NULL, t3363_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3363)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6282_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>
extern MethodInfo m32618_MI;
static PropertyInfo t6282____Count_PropertyInfo = 
{
	&t6282_TI, "Count", &m32618_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32619_MI;
static PropertyInfo t6282____IsReadOnly_PropertyInfo = 
{
	&t6282_TI, "IsReadOnly", &m32619_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6282_PIs[] =
{
	&t6282____Count_PropertyInfo,
	&t6282____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32618_GM;
MethodInfo m32618_MI = 
{
	"get_Count", NULL, &t6282_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32618_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32619_GM;
MethodInfo m32619_MI = 
{
	"get_IsReadOnly", NULL, &t6282_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32619_GM};
extern Il2CppType t1339_0_0_0;
extern Il2CppType t1339_0_0_0;
static ParameterInfo t6282_m32620_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1339_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32620_GM;
MethodInfo m32620_MI = 
{
	"Add", NULL, &t6282_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6282_m32620_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32620_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32621_GM;
MethodInfo m32621_MI = 
{
	"Clear", NULL, &t6282_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32621_GM};
extern Il2CppType t1339_0_0_0;
static ParameterInfo t6282_m32622_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1339_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32622_GM;
MethodInfo m32622_MI = 
{
	"Contains", NULL, &t6282_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6282_m32622_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32622_GM};
extern Il2CppType t1355_0_0_0;
extern Il2CppType t1355_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6282_m32623_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1355_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32623_GM;
MethodInfo m32623_MI = 
{
	"CopyTo", NULL, &t6282_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6282_m32623_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32623_GM};
extern Il2CppType t1339_0_0_0;
static ParameterInfo t6282_m32624_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1339_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32624_GM;
MethodInfo m32624_MI = 
{
	"Remove", NULL, &t6282_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6282_m32624_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32624_GM};
static MethodInfo* t6282_MIs[] =
{
	&m32618_MI,
	&m32619_MI,
	&m32620_MI,
	&m32621_MI,
	&m32622_MI,
	&m32623_MI,
	&m32624_MI,
	NULL
};
extern TypeInfo t6284_TI;
static TypeInfo* t6282_ITIs[] = 
{
	&t603_TI,
	&t6284_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6282_0_0_0;
extern Il2CppType t6282_1_0_0;
struct t6282;
extern Il2CppGenericClass t6282_GC;
TypeInfo t6282_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6282_MIs, t6282_PIs, NULL, NULL, NULL, NULL, NULL, &t6282_TI, t6282_ITIs, NULL, &EmptyCustomAttributesCache, &t6282_TI, &t6282_0_0_0, &t6282_1_0_0, NULL, &t6282_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ConstructorBuilder>
extern Il2CppType t4825_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32625_GM;
MethodInfo m32625_MI = 
{
	"GetEnumerator", NULL, &t6284_TI, &t4825_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32625_GM};
static MethodInfo* t6284_MIs[] =
{
	&m32625_MI,
	NULL
};
static TypeInfo* t6284_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6284_0_0_0;
extern Il2CppType t6284_1_0_0;
struct t6284;
extern Il2CppGenericClass t6284_GC;
TypeInfo t6284_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6284_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6284_TI, t6284_ITIs, NULL, &EmptyCustomAttributesCache, &t6284_TI, &t6284_0_0_0, &t6284_1_0_0, NULL, &t6284_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6283_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.ConstructorBuilder>
extern MethodInfo m32626_MI;
extern MethodInfo m32627_MI;
static PropertyInfo t6283____Item_PropertyInfo = 
{
	&t6283_TI, "Item", &m32626_MI, &m32627_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6283_PIs[] =
{
	&t6283____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1339_0_0_0;
static ParameterInfo t6283_m32628_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1339_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32628_GM;
MethodInfo m32628_MI = 
{
	"IndexOf", NULL, &t6283_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6283_m32628_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32628_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1339_0_0_0;
static ParameterInfo t6283_m32629_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1339_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32629_GM;
MethodInfo m32629_MI = 
{
	"Insert", NULL, &t6283_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6283_m32629_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32629_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6283_m32630_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32630_GM;
MethodInfo m32630_MI = 
{
	"RemoveAt", NULL, &t6283_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6283_m32630_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32630_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6283_m32626_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1339_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32626_GM;
MethodInfo m32626_MI = 
{
	"get_Item", NULL, &t6283_TI, &t1339_0_0_0, RuntimeInvoker_t29_t44, t6283_m32626_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32626_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1339_0_0_0;
static ParameterInfo t6283_m32627_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1339_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32627_GM;
MethodInfo m32627_MI = 
{
	"set_Item", NULL, &t6283_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6283_m32627_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32627_GM};
static MethodInfo* t6283_MIs[] =
{
	&m32628_MI,
	&m32629_MI,
	&m32630_MI,
	&m32626_MI,
	&m32627_MI,
	NULL
};
static TypeInfo* t6283_ITIs[] = 
{
	&t603_TI,
	&t6282_TI,
	&t6284_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6283_0_0_0;
extern Il2CppType t6283_1_0_0;
struct t6283;
extern Il2CppGenericClass t6283_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6283_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6283_MIs, t6283_PIs, NULL, NULL, NULL, NULL, NULL, &t6283_TI, t6283_ITIs, NULL, &t1908__CustomAttributeCache, &t6283_TI, &t6283_0_0_0, &t6283_1_0_0, NULL, &t6283_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6285_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>
extern MethodInfo m32631_MI;
static PropertyInfo t6285____Count_PropertyInfo = 
{
	&t6285_TI, "Count", &m32631_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32632_MI;
static PropertyInfo t6285____IsReadOnly_PropertyInfo = 
{
	&t6285_TI, "IsReadOnly", &m32632_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6285_PIs[] =
{
	&t6285____Count_PropertyInfo,
	&t6285____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32631_GM;
MethodInfo m32631_MI = 
{
	"get_Count", NULL, &t6285_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32631_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32632_GM;
MethodInfo m32632_MI = 
{
	"get_IsReadOnly", NULL, &t6285_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32632_GM};
extern Il2CppType t2008_0_0_0;
extern Il2CppType t2008_0_0_0;
static ParameterInfo t6285_m32633_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2008_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32633_GM;
MethodInfo m32633_MI = 
{
	"Add", NULL, &t6285_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6285_m32633_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32633_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32634_GM;
MethodInfo m32634_MI = 
{
	"Clear", NULL, &t6285_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32634_GM};
extern Il2CppType t2008_0_0_0;
static ParameterInfo t6285_m32635_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2008_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32635_GM;
MethodInfo m32635_MI = 
{
	"Contains", NULL, &t6285_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6285_m32635_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32635_GM};
extern Il2CppType t3623_0_0_0;
extern Il2CppType t3623_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6285_m32636_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3623_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32636_GM;
MethodInfo m32636_MI = 
{
	"CopyTo", NULL, &t6285_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6285_m32636_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32636_GM};
extern Il2CppType t2008_0_0_0;
static ParameterInfo t6285_m32637_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2008_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32637_GM;
MethodInfo m32637_MI = 
{
	"Remove", NULL, &t6285_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6285_m32637_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32637_GM};
static MethodInfo* t6285_MIs[] =
{
	&m32631_MI,
	&m32632_MI,
	&m32633_MI,
	&m32634_MI,
	&m32635_MI,
	&m32636_MI,
	&m32637_MI,
	NULL
};
extern TypeInfo t6287_TI;
static TypeInfo* t6285_ITIs[] = 
{
	&t603_TI,
	&t6287_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6285_0_0_0;
extern Il2CppType t6285_1_0_0;
struct t6285;
extern Il2CppGenericClass t6285_GC;
TypeInfo t6285_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6285_MIs, t6285_PIs, NULL, NULL, NULL, NULL, NULL, &t6285_TI, t6285_ITIs, NULL, &EmptyCustomAttributesCache, &t6285_TI, &t6285_0_0_0, &t6285_1_0_0, NULL, &t6285_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ConstructorBuilder>
extern Il2CppType t4827_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32638_GM;
MethodInfo m32638_MI = 
{
	"GetEnumerator", NULL, &t6287_TI, &t4827_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32638_GM};
static MethodInfo* t6287_MIs[] =
{
	&m32638_MI,
	NULL
};
static TypeInfo* t6287_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6287_0_0_0;
extern Il2CppType t6287_1_0_0;
struct t6287;
extern Il2CppGenericClass t6287_GC;
TypeInfo t6287_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6287_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6287_TI, t6287_ITIs, NULL, &EmptyCustomAttributesCache, &t6287_TI, &t6287_0_0_0, &t6287_1_0_0, NULL, &t6287_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
