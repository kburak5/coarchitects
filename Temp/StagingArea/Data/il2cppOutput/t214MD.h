﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t214;
struct t395;
struct t557;
struct t7;
struct t29;
struct t556;

 void m1824 (t214 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1843 (t214 * __this, t395 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1842 (t214 * __this, t395 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m1825 (t214 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t556 * m1826 (t214 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t556 * m14114 (t29 * __this, t395 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1832 (t214 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
