﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3173;
struct t29;
struct t20;
#include "t578.h"

 void m17629 (t3173 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17630 (t3173 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17631 (t3173 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17632 (t3173 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17633 (t3173 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
