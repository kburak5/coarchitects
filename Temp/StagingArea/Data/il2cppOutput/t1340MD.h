﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1340;
struct t929;
struct t7;
struct t42;
struct t1142;
struct t660;
struct t631;
struct t537;
struct t634;
struct t316;
struct t1147;
struct t1143;
struct t1144;
struct t1145;
struct t557;
struct t1146;
struct t29;
struct t633;
struct t446;
struct t295;
#include "t43.h"
#include "t1148.h"
#include "t630.h"
#include "t1150.h"

 int32_t m7406 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t929 * m7407 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7408 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7409 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7410 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7411 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7412 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1142 * m7413 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7414 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7415 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7416 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t660 * m7417 (t1340 * __this, int32_t p0, t631 * p1, int32_t p2, t537* p3, t634* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7418 (t1340 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7419 (t1340 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7420 (t1340 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1147* m7421 (t1340 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1147* m7422 (t1340 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7423 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1143 * m7424 (t1340 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1144 * m7425 (t1340 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7426 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1145* m7427 (t1340 * __this, t7* p0, int32_t p1, bool p2, t42 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1145* m7428 (t1340 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m7429 (t1340 * __this, t7* p0, int32_t p1, t631 * p2, int32_t p3, t537* p4, t634* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1146 * m7430 (t1340 * __this, t7* p0, int32_t p1, t631 * p2, t42 * p3, t537* p4, t634* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7431 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7432 (t1340 * __this, t7* p0, int32_t p1, t631 * p2, t29 * p3, t316* p4, t634* p5, t633 * p6, t446* p7, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7433 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7434 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7435 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7436 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7437 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7438 (t1340 * __this, t537* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t43  m7439 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7440 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7441 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m7442 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7443 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7444 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7445 (t1340 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7446 (t1340 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7447 (t1340 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7448 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7449 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7450 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7451 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7452 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7453 (t1340 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
