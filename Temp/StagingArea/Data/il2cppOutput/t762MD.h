﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t762;
struct t745;
struct t801;
struct t799;

 void m3345 (t762 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3346 (t762 * __this, t801* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m3347 (t762 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3348 (t762 * __this, t801* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t799 * m3349 (t762 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3350 (t762 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
