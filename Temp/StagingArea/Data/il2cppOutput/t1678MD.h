﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1678;
struct t733;
struct t42;
struct t1626;
struct t1142;
struct t29;
#include "t735.h"

 void m9584 (t1678 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9585 (t29 * __this, t42 * p0, t733 * p1, t735  p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9586 (t29 * __this, t1626 * p0, t733 * p1, t735  p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9587 (t29 * __this, t1142 * p0, t733 * p1, t735  p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9588 (t1678 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9589 (t1678 * __this, t735  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
