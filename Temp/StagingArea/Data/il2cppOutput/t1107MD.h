﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1107;
struct t777;
struct t7;
struct t781;

 void m8344 (t1107 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5175 (t1107 * __this, t777 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5176 (t1107 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8345 (t1107 * __this, t777 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5177 (t1107 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
