﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t772;
struct t748;
struct t446;
struct t773;
struct t733;
#include "t735.h"

 void m3260 (t772 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3261 (t772 * __this, t748 * p0, bool p1, t446* p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3262 (t772 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3263 (t772 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3264 (t772 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t748 * m3265 (t772 * __this, t748 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3266 (t772 * __this, t748 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3267 (t772 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3268 (t772 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
