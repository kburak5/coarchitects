﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t44;
struct t1094;
struct t29;
struct t42;
struct t7;
struct t295;
struct t1125;
#include "t465.h"
#include "t1126.h"
#include "t923.h"
#include "t1127.h"

 bool m5296 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5297 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5298 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5299 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5300 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5301 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5302 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5303 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5304 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5305 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5306 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5307 (int32_t* __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5308 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5309 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5310 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5311 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5312 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2840 (int32_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1303 (int32_t* __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2842 (int32_t* __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5313 (t29 * __this, bool p0, t7* p1, int32_t p2, t295 ** p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5314 (t29 * __this, t7* p0, bool p1, int32_t* p2, t295 ** p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5315 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5316 (t29 * __this, int32_t p0, bool p1, t295 ** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5317 (t29 * __this, int32_t* p0, t7* p1, bool p2, bool p3, t295 ** p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5318 (t29 * __this, int32_t* p0, t7* p1, t1125 * p2, bool* p3, bool* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5319 (t29 * __this, int32_t* p0, t7* p1, t1125 * p2, bool* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5320 (t29 * __this, int32_t* p0, t7* p1, int32_t* p2, bool p3, t295 ** p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5321 (t29 * __this, int32_t* p0, t7* p1, t7* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5322 (t29 * __this, uint16_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m5323 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5324 (t29 * __this, t7* p0, int32_t p1, t29 * p2, bool p3, int32_t* p4, t295 ** p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4203 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5325 (t29 * __this, t7* p0, int32_t p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5326 (t29 * __this, t7* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4010 (t29 * __this, t7* p0, int32_t p1, t29 * p2, int32_t* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2943 (int32_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5190 (int32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4197 (int32_t* __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5193 (int32_t* __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5327 (int32_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
