﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3385;
struct t29;
struct t20;
#include "t1150.h"

 void m18797 (t3385 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18798 (t3385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18799 (t3385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18800 (t3385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18801 (t3385 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
