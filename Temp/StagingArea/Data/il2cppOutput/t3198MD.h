﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3198;
struct t29;
struct t7;
struct t66;
struct t67;
#include "t35.h"

 void m17791 (t3198 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17792 (t3198 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17793 (t3198 * __this, t7* p0, bool p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17794 (t3198 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
