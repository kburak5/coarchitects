﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t521;
struct t521_marshaled;
struct t7;

 bool m2654 (t521 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2655 (t521 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2656 (t521 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2657 (t521 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2658 (t521 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2659 (t521 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2660 (t521 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2661 (t521 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2662 (t521 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t521_marshal(const t521& unmarshaled, t521_marshaled& marshaled);
void t521_marshal_back(const t521_marshaled& marshaled, t521& unmarshaled);
void t521_marshal_cleanup(t521_marshaled& marshaled);
