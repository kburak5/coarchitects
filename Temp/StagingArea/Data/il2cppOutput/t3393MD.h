﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3393;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m18836_gshared (t3393 * __this, t29 * p0, t35 p1, MethodInfo* method);
#define m18836(__this, p0, p1, method) (void)m18836_gshared((t3393 *)__this, (t29 *)p0, (t35)p1, method)
 t29 * m18837_gshared (t3393 * __this, MethodInfo* method);
#define m18837(__this, method) (t29 *)m18837_gshared((t3393 *)__this, method)
 t29 * m18838_gshared (t3393 * __this, t67 * p0, t29 * p1, MethodInfo* method);
#define m18838(__this, p0, p1, method) (t29 *)m18838_gshared((t3393 *)__this, (t67 *)p0, (t29 *)p1, method)
 t29 * m18839_gshared (t3393 * __this, t29 * p0, MethodInfo* method);
#define m18839(__this, p0, method) (t29 *)m18839_gshared((t3393 *)__this, (t29 *)p0, method)
