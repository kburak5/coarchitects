﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t520;
struct t520_marshaled;
struct t29;

 bool m2779 (t520 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2780 (t520 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2781 (t29 * __this, t520 * p0, t520 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
void t520_marshal(const t520& unmarshaled, t520_marshaled& marshaled);
void t520_marshal_back(const t520_marshaled& marshaled, t520& unmarshaled);
void t520_marshal_cleanup(t520_marshaled& marshaled);
