﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1636;
struct t7;
struct t733;
#include "t735.h"

 void m9576 (t1636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9577 (t1636 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9578 (t1636 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9579 (t1636 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9580 (t1636 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
