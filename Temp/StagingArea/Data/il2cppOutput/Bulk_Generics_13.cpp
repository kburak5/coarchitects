﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t5455_TI;

#include "t250.h"
#include "t44.h"
#include "t21.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.CanvasScaler/ScreenMatchMode>
extern MethodInfo m28577_MI;
extern MethodInfo m28578_MI;
static PropertyInfo t5455____Item_PropertyInfo = 
{
	&t5455_TI, "Item", &m28577_MI, &m28578_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5455_PIs[] =
{
	&t5455____Item_PropertyInfo,
	NULL
};
extern Il2CppType t250_0_0_0;
extern Il2CppType t250_0_0_0;
static ParameterInfo t5455_m28579_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t250_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28579_GM;
MethodInfo m28579_MI = 
{
	"IndexOf", NULL, &t5455_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5455_m28579_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28579_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t250_0_0_0;
static ParameterInfo t5455_m28580_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t250_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28580_GM;
MethodInfo m28580_MI = 
{
	"Insert", NULL, &t5455_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5455_m28580_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28580_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5455_m28581_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28581_GM;
MethodInfo m28581_MI = 
{
	"RemoveAt", NULL, &t5455_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5455_m28581_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28581_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5455_m28577_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t250_0_0_0;
extern void* RuntimeInvoker_t250_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28577_GM;
MethodInfo m28577_MI = 
{
	"get_Item", NULL, &t5455_TI, &t250_0_0_0, RuntimeInvoker_t250_t44, t5455_m28577_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28577_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t250_0_0_0;
static ParameterInfo t5455_m28578_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t250_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28578_GM;
MethodInfo m28578_MI = 
{
	"set_Item", NULL, &t5455_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5455_m28578_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28578_GM};
static MethodInfo* t5455_MIs[] =
{
	&m28579_MI,
	&m28580_MI,
	&m28581_MI,
	&m28577_MI,
	&m28578_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5454_TI;
extern TypeInfo t5456_TI;
static TypeInfo* t5455_ITIs[] = 
{
	&t603_TI,
	&t5454_TI,
	&t5456_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5455_0_0_0;
extern Il2CppType t5455_1_0_0;
struct t5455;
extern Il2CppGenericClass t5455_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5455_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5455_MIs, t5455_PIs, NULL, NULL, NULL, NULL, NULL, &t5455_TI, t5455_ITIs, NULL, &t1908__CustomAttributeCache, &t5455_TI, &t5455_0_0_0, &t5455_1_0_0, NULL, &t5455_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4272_TI;

#include "t251.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.CanvasScaler/Unit>
extern MethodInfo m28582_MI;
static PropertyInfo t4272____Current_PropertyInfo = 
{
	&t4272_TI, "Current", &m28582_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4272_PIs[] =
{
	&t4272____Current_PropertyInfo,
	NULL
};
extern Il2CppType t251_0_0_0;
extern void* RuntimeInvoker_t251 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28582_GM;
MethodInfo m28582_MI = 
{
	"get_Current", NULL, &t4272_TI, &t251_0_0_0, RuntimeInvoker_t251, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28582_GM};
static MethodInfo* t4272_MIs[] =
{
	&m28582_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4272_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4272_0_0_0;
extern Il2CppType t4272_1_0_0;
struct t4272;
extern Il2CppGenericClass t4272_GC;
TypeInfo t4272_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4272_MIs, t4272_PIs, NULL, NULL, NULL, NULL, NULL, &t4272_TI, t4272_ITIs, NULL, &EmptyCustomAttributesCache, &t4272_TI, &t4272_0_0_0, &t4272_1_0_0, NULL, &t4272_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2731.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2731_TI;
#include "t2731MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
#include "t40.h"
extern TypeInfo t251_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m14910_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m21622_MI;
struct t20;
#include "t915.h"
 int32_t m21622 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14906_MI;
 void m14906 (t2731 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14907_MI;
 t29 * m14907 (t2731 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14910(__this, &m14910_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t251_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14908_MI;
 void m14908 (t2731 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14909_MI;
 bool m14909 (t2731 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14910 (t2731 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21622(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21622_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.CanvasScaler/Unit>
extern Il2CppType t20_0_0_1;
FieldInfo t2731_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2731_TI, offsetof(t2731, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2731_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2731_TI, offsetof(t2731, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2731_FIs[] =
{
	&t2731_f0_FieldInfo,
	&t2731_f1_FieldInfo,
	NULL
};
static PropertyInfo t2731____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2731_TI, "System.Collections.IEnumerator.Current", &m14907_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2731____Current_PropertyInfo = 
{
	&t2731_TI, "Current", &m14910_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2731_PIs[] =
{
	&t2731____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2731____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2731_m14906_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14906_GM;
MethodInfo m14906_MI = 
{
	".ctor", (methodPointerType)&m14906, &t2731_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2731_m14906_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14906_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14907_GM;
MethodInfo m14907_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14907, &t2731_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14907_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14908_GM;
MethodInfo m14908_MI = 
{
	"Dispose", (methodPointerType)&m14908, &t2731_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14908_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14909_GM;
MethodInfo m14909_MI = 
{
	"MoveNext", (methodPointerType)&m14909, &t2731_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14909_GM};
extern Il2CppType t251_0_0_0;
extern void* RuntimeInvoker_t251 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14910_GM;
MethodInfo m14910_MI = 
{
	"get_Current", (methodPointerType)&m14910, &t2731_TI, &t251_0_0_0, RuntimeInvoker_t251, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14910_GM};
static MethodInfo* t2731_MIs[] =
{
	&m14906_MI,
	&m14907_MI,
	&m14908_MI,
	&m14909_MI,
	&m14910_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t2731_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14907_MI,
	&m14909_MI,
	&m14908_MI,
	&m14910_MI,
};
static TypeInfo* t2731_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4272_TI,
};
static Il2CppInterfaceOffsetPair t2731_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4272_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2731_0_0_0;
extern Il2CppType t2731_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2731_GC;
extern TypeInfo t20_TI;
TypeInfo t2731_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2731_MIs, t2731_PIs, t2731_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2731_TI, t2731_ITIs, t2731_VT, &EmptyCustomAttributesCache, &t2731_TI, &t2731_0_0_0, &t2731_1_0_0, t2731_IOs, &t2731_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2731)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5457_TI;

#include "UnityEngine.UI_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.CanvasScaler/Unit>
extern MethodInfo m28583_MI;
static PropertyInfo t5457____Count_PropertyInfo = 
{
	&t5457_TI, "Count", &m28583_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28584_MI;
static PropertyInfo t5457____IsReadOnly_PropertyInfo = 
{
	&t5457_TI, "IsReadOnly", &m28584_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5457_PIs[] =
{
	&t5457____Count_PropertyInfo,
	&t5457____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28583_GM;
MethodInfo m28583_MI = 
{
	"get_Count", NULL, &t5457_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28583_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28584_GM;
MethodInfo m28584_MI = 
{
	"get_IsReadOnly", NULL, &t5457_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28584_GM};
extern Il2CppType t251_0_0_0;
extern Il2CppType t251_0_0_0;
static ParameterInfo t5457_m28585_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t251_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28585_GM;
MethodInfo m28585_MI = 
{
	"Add", NULL, &t5457_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5457_m28585_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28585_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28586_GM;
MethodInfo m28586_MI = 
{
	"Clear", NULL, &t5457_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28586_GM};
extern Il2CppType t251_0_0_0;
static ParameterInfo t5457_m28587_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t251_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28587_GM;
MethodInfo m28587_MI = 
{
	"Contains", NULL, &t5457_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5457_m28587_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28587_GM};
extern Il2CppType t3872_0_0_0;
extern Il2CppType t3872_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5457_m28588_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3872_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28588_GM;
MethodInfo m28588_MI = 
{
	"CopyTo", NULL, &t5457_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5457_m28588_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28588_GM};
extern Il2CppType t251_0_0_0;
static ParameterInfo t5457_m28589_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t251_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28589_GM;
MethodInfo m28589_MI = 
{
	"Remove", NULL, &t5457_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5457_m28589_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28589_GM};
static MethodInfo* t5457_MIs[] =
{
	&m28583_MI,
	&m28584_MI,
	&m28585_MI,
	&m28586_MI,
	&m28587_MI,
	&m28588_MI,
	&m28589_MI,
	NULL
};
extern TypeInfo t5459_TI;
static TypeInfo* t5457_ITIs[] = 
{
	&t603_TI,
	&t5459_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5457_0_0_0;
extern Il2CppType t5457_1_0_0;
struct t5457;
extern Il2CppGenericClass t5457_GC;
TypeInfo t5457_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5457_MIs, t5457_PIs, NULL, NULL, NULL, NULL, NULL, &t5457_TI, t5457_ITIs, NULL, &EmptyCustomAttributesCache, &t5457_TI, &t5457_0_0_0, &t5457_1_0_0, NULL, &t5457_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.CanvasScaler/Unit>
extern Il2CppType t4272_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28590_GM;
MethodInfo m28590_MI = 
{
	"GetEnumerator", NULL, &t5459_TI, &t4272_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28590_GM};
static MethodInfo* t5459_MIs[] =
{
	&m28590_MI,
	NULL
};
static TypeInfo* t5459_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5459_0_0_0;
extern Il2CppType t5459_1_0_0;
struct t5459;
extern Il2CppGenericClass t5459_GC;
TypeInfo t5459_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5459_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5459_TI, t5459_ITIs, NULL, &EmptyCustomAttributesCache, &t5459_TI, &t5459_0_0_0, &t5459_1_0_0, NULL, &t5459_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5458_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.CanvasScaler/Unit>
extern MethodInfo m28591_MI;
extern MethodInfo m28592_MI;
static PropertyInfo t5458____Item_PropertyInfo = 
{
	&t5458_TI, "Item", &m28591_MI, &m28592_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5458_PIs[] =
{
	&t5458____Item_PropertyInfo,
	NULL
};
extern Il2CppType t251_0_0_0;
static ParameterInfo t5458_m28593_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t251_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28593_GM;
MethodInfo m28593_MI = 
{
	"IndexOf", NULL, &t5458_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5458_m28593_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28593_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t251_0_0_0;
static ParameterInfo t5458_m28594_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t251_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28594_GM;
MethodInfo m28594_MI = 
{
	"Insert", NULL, &t5458_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5458_m28594_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28594_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5458_m28595_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28595_GM;
MethodInfo m28595_MI = 
{
	"RemoveAt", NULL, &t5458_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5458_m28595_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28595_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5458_m28591_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t251_0_0_0;
extern void* RuntimeInvoker_t251_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28591_GM;
MethodInfo m28591_MI = 
{
	"get_Item", NULL, &t5458_TI, &t251_0_0_0, RuntimeInvoker_t251_t44, t5458_m28591_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28591_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t251_0_0_0;
static ParameterInfo t5458_m28592_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t251_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28592_GM;
MethodInfo m28592_MI = 
{
	"set_Item", NULL, &t5458_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5458_m28592_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28592_GM};
static MethodInfo* t5458_MIs[] =
{
	&m28593_MI,
	&m28594_MI,
	&m28595_MI,
	&m28591_MI,
	&m28592_MI,
	NULL
};
static TypeInfo* t5458_ITIs[] = 
{
	&t603_TI,
	&t5457_TI,
	&t5459_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5458_0_0_0;
extern Il2CppType t5458_1_0_0;
struct t5458;
extern Il2CppGenericClass t5458_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5458_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5458_MIs, t5458_PIs, NULL, NULL, NULL, NULL, NULL, &t5458_TI, t5458_ITIs, NULL, &t1908__CustomAttributeCache, &t5458_TI, &t5458_0_0_0, &t5458_1_0_0, NULL, &t5458_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4274_TI;

#include "t254.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ContentSizeFitter>
extern MethodInfo m28596_MI;
static PropertyInfo t4274____Current_PropertyInfo = 
{
	&t4274_TI, "Current", &m28596_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4274_PIs[] =
{
	&t4274____Current_PropertyInfo,
	NULL
};
extern Il2CppType t254_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28596_GM;
MethodInfo m28596_MI = 
{
	"get_Current", NULL, &t4274_TI, &t254_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28596_GM};
static MethodInfo* t4274_MIs[] =
{
	&m28596_MI,
	NULL
};
static TypeInfo* t4274_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4274_0_0_0;
extern Il2CppType t4274_1_0_0;
struct t4274;
extern Il2CppGenericClass t4274_GC;
TypeInfo t4274_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4274_MIs, t4274_PIs, NULL, NULL, NULL, NULL, NULL, &t4274_TI, t4274_ITIs, NULL, &EmptyCustomAttributesCache, &t4274_TI, &t4274_0_0_0, &t4274_1_0_0, NULL, &t4274_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2732.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2732_TI;
#include "t2732MD.h"

extern TypeInfo t254_TI;
extern MethodInfo m14915_MI;
extern MethodInfo m21633_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m21633(__this, p0, method) (t254 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.ContentSizeFitter>
extern Il2CppType t20_0_0_1;
FieldInfo t2732_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2732_TI, offsetof(t2732, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2732_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2732_TI, offsetof(t2732, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2732_FIs[] =
{
	&t2732_f0_FieldInfo,
	&t2732_f1_FieldInfo,
	NULL
};
extern MethodInfo m14912_MI;
static PropertyInfo t2732____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2732_TI, "System.Collections.IEnumerator.Current", &m14912_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2732____Current_PropertyInfo = 
{
	&t2732_TI, "Current", &m14915_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2732_PIs[] =
{
	&t2732____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2732____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2732_m14911_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14911_GM;
MethodInfo m14911_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2732_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2732_m14911_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14911_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14912_GM;
MethodInfo m14912_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2732_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14912_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14913_GM;
MethodInfo m14913_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2732_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14913_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14914_GM;
MethodInfo m14914_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2732_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14914_GM};
extern Il2CppType t254_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14915_GM;
MethodInfo m14915_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2732_TI, &t254_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14915_GM};
static MethodInfo* t2732_MIs[] =
{
	&m14911_MI,
	&m14912_MI,
	&m14913_MI,
	&m14914_MI,
	&m14915_MI,
	NULL
};
extern MethodInfo m14914_MI;
extern MethodInfo m14913_MI;
static MethodInfo* t2732_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14912_MI,
	&m14914_MI,
	&m14913_MI,
	&m14915_MI,
};
static TypeInfo* t2732_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4274_TI,
};
static Il2CppInterfaceOffsetPair t2732_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4274_TI, 7},
};
extern TypeInfo t254_TI;
static Il2CppRGCTXData t2732_RGCTXData[3] = 
{
	&m14915_MI/* Method Usage */,
	&t254_TI/* Class Usage */,
	&m21633_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2732_0_0_0;
extern Il2CppType t2732_1_0_0;
extern Il2CppGenericClass t2732_GC;
TypeInfo t2732_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2732_MIs, t2732_PIs, t2732_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2732_TI, t2732_ITIs, t2732_VT, &EmptyCustomAttributesCache, &t2732_TI, &t2732_0_0_0, &t2732_1_0_0, t2732_IOs, &t2732_GC, NULL, NULL, NULL, t2732_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2732)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5460_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.ContentSizeFitter>
extern MethodInfo m28597_MI;
static PropertyInfo t5460____Count_PropertyInfo = 
{
	&t5460_TI, "Count", &m28597_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28598_MI;
static PropertyInfo t5460____IsReadOnly_PropertyInfo = 
{
	&t5460_TI, "IsReadOnly", &m28598_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5460_PIs[] =
{
	&t5460____Count_PropertyInfo,
	&t5460____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28597_GM;
MethodInfo m28597_MI = 
{
	"get_Count", NULL, &t5460_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28597_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28598_GM;
MethodInfo m28598_MI = 
{
	"get_IsReadOnly", NULL, &t5460_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28598_GM};
extern Il2CppType t254_0_0_0;
extern Il2CppType t254_0_0_0;
static ParameterInfo t5460_m28599_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t254_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28599_GM;
MethodInfo m28599_MI = 
{
	"Add", NULL, &t5460_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5460_m28599_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28599_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28600_GM;
MethodInfo m28600_MI = 
{
	"Clear", NULL, &t5460_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28600_GM};
extern Il2CppType t254_0_0_0;
static ParameterInfo t5460_m28601_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t254_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28601_GM;
MethodInfo m28601_MI = 
{
	"Contains", NULL, &t5460_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5460_m28601_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28601_GM};
extern Il2CppType t3873_0_0_0;
extern Il2CppType t3873_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5460_m28602_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3873_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28602_GM;
MethodInfo m28602_MI = 
{
	"CopyTo", NULL, &t5460_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5460_m28602_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28602_GM};
extern Il2CppType t254_0_0_0;
static ParameterInfo t5460_m28603_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t254_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28603_GM;
MethodInfo m28603_MI = 
{
	"Remove", NULL, &t5460_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5460_m28603_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28603_GM};
static MethodInfo* t5460_MIs[] =
{
	&m28597_MI,
	&m28598_MI,
	&m28599_MI,
	&m28600_MI,
	&m28601_MI,
	&m28602_MI,
	&m28603_MI,
	NULL
};
extern TypeInfo t5462_TI;
static TypeInfo* t5460_ITIs[] = 
{
	&t603_TI,
	&t5462_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5460_0_0_0;
extern Il2CppType t5460_1_0_0;
struct t5460;
extern Il2CppGenericClass t5460_GC;
TypeInfo t5460_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5460_MIs, t5460_PIs, NULL, NULL, NULL, NULL, NULL, &t5460_TI, t5460_ITIs, NULL, &EmptyCustomAttributesCache, &t5460_TI, &t5460_0_0_0, &t5460_1_0_0, NULL, &t5460_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.ContentSizeFitter>
extern Il2CppType t4274_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28604_GM;
MethodInfo m28604_MI = 
{
	"GetEnumerator", NULL, &t5462_TI, &t4274_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28604_GM};
static MethodInfo* t5462_MIs[] =
{
	&m28604_MI,
	NULL
};
static TypeInfo* t5462_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5462_0_0_0;
extern Il2CppType t5462_1_0_0;
struct t5462;
extern Il2CppGenericClass t5462_GC;
TypeInfo t5462_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5462_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5462_TI, t5462_ITIs, NULL, &EmptyCustomAttributesCache, &t5462_TI, &t5462_0_0_0, &t5462_1_0_0, NULL, &t5462_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5461_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.ContentSizeFitter>
extern MethodInfo m28605_MI;
extern MethodInfo m28606_MI;
static PropertyInfo t5461____Item_PropertyInfo = 
{
	&t5461_TI, "Item", &m28605_MI, &m28606_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5461_PIs[] =
{
	&t5461____Item_PropertyInfo,
	NULL
};
extern Il2CppType t254_0_0_0;
static ParameterInfo t5461_m28607_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t254_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28607_GM;
MethodInfo m28607_MI = 
{
	"IndexOf", NULL, &t5461_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5461_m28607_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28607_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t254_0_0_0;
static ParameterInfo t5461_m28608_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t254_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28608_GM;
MethodInfo m28608_MI = 
{
	"Insert", NULL, &t5461_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5461_m28608_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28608_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5461_m28609_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28609_GM;
MethodInfo m28609_MI = 
{
	"RemoveAt", NULL, &t5461_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5461_m28609_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28609_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5461_m28605_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t254_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28605_GM;
MethodInfo m28605_MI = 
{
	"get_Item", NULL, &t5461_TI, &t254_0_0_0, RuntimeInvoker_t29_t44, t5461_m28605_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28605_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t254_0_0_0;
static ParameterInfo t5461_m28606_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t254_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28606_GM;
MethodInfo m28606_MI = 
{
	"set_Item", NULL, &t5461_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5461_m28606_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28606_GM};
static MethodInfo* t5461_MIs[] =
{
	&m28607_MI,
	&m28608_MI,
	&m28609_MI,
	&m28605_MI,
	&m28606_MI,
	NULL
};
static TypeInfo* t5461_ITIs[] = 
{
	&t603_TI,
	&t5460_TI,
	&t5462_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5461_0_0_0;
extern Il2CppType t5461_1_0_0;
struct t5461;
extern Il2CppGenericClass t5461_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5461_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5461_MIs, t5461_PIs, NULL, NULL, NULL, NULL, NULL, &t5461_TI, t5461_ITIs, NULL, &t1908__CustomAttributeCache, &t5461_TI, &t5461_0_0_0, &t5461_1_0_0, NULL, &t5461_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2733.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2733_TI;
#include "t2733MD.h"

#include "t41.h"
#include "t557.h"
#include "mscorlib_ArrayTypes.h"
#include "t2734.h"
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t2734_TI;
extern TypeInfo t21_TI;
#include "t2734MD.h"
extern MethodInfo m14918_MI;
extern MethodInfo m14920_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ContentSizeFitter>
extern Il2CppType t316_0_0_33;
FieldInfo t2733_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2733_TI, offsetof(t2733, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2733_FIs[] =
{
	&t2733_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t254_0_0_0;
static ParameterInfo t2733_m14916_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t254_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14916_GM;
MethodInfo m14916_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2733_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2733_m14916_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14916_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2733_m14917_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14917_GM;
MethodInfo m14917_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2733_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2733_m14917_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14917_GM};
static MethodInfo* t2733_MIs[] =
{
	&m14916_MI,
	&m14917_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m14917_MI;
extern MethodInfo m14921_MI;
static MethodInfo* t2733_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14917_MI,
	&m14921_MI,
};
extern Il2CppType t2735_0_0_0;
extern TypeInfo t2735_TI;
extern MethodInfo m21643_MI;
extern TypeInfo t254_TI;
extern MethodInfo m14923_MI;
extern TypeInfo t254_TI;
static Il2CppRGCTXData t2733_RGCTXData[8] = 
{
	&t2735_0_0_0/* Type Usage */,
	&t2735_TI/* Class Usage */,
	&m21643_MI/* Method Usage */,
	&t254_TI/* Class Usage */,
	&m14923_MI/* Method Usage */,
	&m14918_MI/* Method Usage */,
	&t254_TI/* Class Usage */,
	&m14920_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2733_0_0_0;
extern Il2CppType t2733_1_0_0;
struct t2733;
extern Il2CppGenericClass t2733_GC;
TypeInfo t2733_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2733_MIs, NULL, t2733_FIs, NULL, &t2734_TI, NULL, NULL, &t2733_TI, NULL, t2733_VT, &EmptyCustomAttributesCache, &t2733_TI, &t2733_0_0_0, &t2733_1_0_0, NULL, &t2733_GC, NULL, NULL, NULL, t2733_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2733), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2735.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "t305.h"
extern TypeInfo t2735_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t2735MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m21643(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.ContentSizeFitter>
extern Il2CppType t2735_0_0_1;
FieldInfo t2734_f0_FieldInfo = 
{
	"Delegate", &t2735_0_0_1, &t2734_TI, offsetof(t2734, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2734_FIs[] =
{
	&t2734_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2734_m14918_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14918_GM;
MethodInfo m14918_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2734_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2734_m14918_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14918_GM};
extern Il2CppType t2735_0_0_0;
static ParameterInfo t2734_m14919_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2735_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14919_GM;
MethodInfo m14919_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2734_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2734_m14919_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14919_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2734_m14920_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14920_GM;
MethodInfo m14920_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2734_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2734_m14920_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14920_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2734_m14921_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14921_GM;
MethodInfo m14921_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2734_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2734_m14921_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14921_GM};
static MethodInfo* t2734_MIs[] =
{
	&m14918_MI,
	&m14919_MI,
	&m14920_MI,
	&m14921_MI,
	NULL
};
static MethodInfo* t2734_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14920_MI,
	&m14921_MI,
};
extern TypeInfo t2735_TI;
extern TypeInfo t254_TI;
static Il2CppRGCTXData t2734_RGCTXData[5] = 
{
	&t2735_0_0_0/* Type Usage */,
	&t2735_TI/* Class Usage */,
	&m21643_MI/* Method Usage */,
	&t254_TI/* Class Usage */,
	&m14923_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2734_0_0_0;
extern Il2CppType t2734_1_0_0;
extern TypeInfo t556_TI;
struct t2734;
extern Il2CppGenericClass t2734_GC;
TypeInfo t2734_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2734_MIs, NULL, t2734_FIs, NULL, &t556_TI, NULL, NULL, &t2734_TI, NULL, t2734_VT, &EmptyCustomAttributesCache, &t2734_TI, &t2734_0_0_0, &t2734_1_0_0, NULL, &t2734_GC, NULL, NULL, NULL, t2734_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2734), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.ContentSizeFitter>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2735_m14922_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14922_GM;
MethodInfo m14922_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2735_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2735_m14922_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14922_GM};
extern Il2CppType t254_0_0_0;
static ParameterInfo t2735_m14923_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t254_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14923_GM;
MethodInfo m14923_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2735_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2735_m14923_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14923_GM};
extern Il2CppType t254_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2735_m14924_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t254_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14924_GM;
MethodInfo m14924_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2735_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2735_m14924_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14924_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2735_m14925_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14925_GM;
MethodInfo m14925_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2735_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2735_m14925_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14925_GM};
static MethodInfo* t2735_MIs[] =
{
	&m14922_MI,
	&m14923_MI,
	&m14924_MI,
	&m14925_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m14924_MI;
extern MethodInfo m14925_MI;
static MethodInfo* t2735_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14923_MI,
	&m14924_MI,
	&m14925_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2735_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2735_1_0_0;
extern TypeInfo t195_TI;
struct t2735;
extern Il2CppGenericClass t2735_GC;
TypeInfo t2735_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2735_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2735_TI, NULL, t2735_VT, &EmptyCustomAttributesCache, &t2735_TI, &t2735_0_0_0, &t2735_1_0_0, t2735_IOs, &t2735_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2735), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4276_TI;

#include "t253.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ContentSizeFitter/FitMode>
extern MethodInfo m28610_MI;
static PropertyInfo t4276____Current_PropertyInfo = 
{
	&t4276_TI, "Current", &m28610_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4276_PIs[] =
{
	&t4276____Current_PropertyInfo,
	NULL
};
extern Il2CppType t253_0_0_0;
extern void* RuntimeInvoker_t253 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28610_GM;
MethodInfo m28610_MI = 
{
	"get_Current", NULL, &t4276_TI, &t253_0_0_0, RuntimeInvoker_t253, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28610_GM};
static MethodInfo* t4276_MIs[] =
{
	&m28610_MI,
	NULL
};
static TypeInfo* t4276_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4276_0_0_0;
extern Il2CppType t4276_1_0_0;
struct t4276;
extern Il2CppGenericClass t4276_GC;
TypeInfo t4276_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4276_MIs, t4276_PIs, NULL, NULL, NULL, NULL, NULL, &t4276_TI, t4276_ITIs, NULL, &EmptyCustomAttributesCache, &t4276_TI, &t4276_0_0_0, &t4276_1_0_0, NULL, &t4276_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2736.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2736_TI;
#include "t2736MD.h"

extern TypeInfo t253_TI;
extern MethodInfo m14930_MI;
extern MethodInfo m21645_MI;
struct t20;
 int32_t m21645 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14926_MI;
 void m14926 (t2736 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14927_MI;
 t29 * m14927 (t2736 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14930(__this, &m14930_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t253_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14928_MI;
 void m14928 (t2736 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14929_MI;
 bool m14929 (t2736 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14930 (t2736 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21645(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21645_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.ContentSizeFitter/FitMode>
extern Il2CppType t20_0_0_1;
FieldInfo t2736_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2736_TI, offsetof(t2736, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2736_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2736_TI, offsetof(t2736, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2736_FIs[] =
{
	&t2736_f0_FieldInfo,
	&t2736_f1_FieldInfo,
	NULL
};
static PropertyInfo t2736____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2736_TI, "System.Collections.IEnumerator.Current", &m14927_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2736____Current_PropertyInfo = 
{
	&t2736_TI, "Current", &m14930_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2736_PIs[] =
{
	&t2736____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2736____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2736_m14926_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14926_GM;
MethodInfo m14926_MI = 
{
	".ctor", (methodPointerType)&m14926, &t2736_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2736_m14926_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14926_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14927_GM;
MethodInfo m14927_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14927, &t2736_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14927_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14928_GM;
MethodInfo m14928_MI = 
{
	"Dispose", (methodPointerType)&m14928, &t2736_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14928_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14929_GM;
MethodInfo m14929_MI = 
{
	"MoveNext", (methodPointerType)&m14929, &t2736_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14929_GM};
extern Il2CppType t253_0_0_0;
extern void* RuntimeInvoker_t253 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14930_GM;
MethodInfo m14930_MI = 
{
	"get_Current", (methodPointerType)&m14930, &t2736_TI, &t253_0_0_0, RuntimeInvoker_t253, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14930_GM};
static MethodInfo* t2736_MIs[] =
{
	&m14926_MI,
	&m14927_MI,
	&m14928_MI,
	&m14929_MI,
	&m14930_MI,
	NULL
};
static MethodInfo* t2736_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14927_MI,
	&m14929_MI,
	&m14928_MI,
	&m14930_MI,
};
static TypeInfo* t2736_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4276_TI,
};
static Il2CppInterfaceOffsetPair t2736_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4276_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2736_0_0_0;
extern Il2CppType t2736_1_0_0;
extern Il2CppGenericClass t2736_GC;
TypeInfo t2736_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2736_MIs, t2736_PIs, t2736_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2736_TI, t2736_ITIs, t2736_VT, &EmptyCustomAttributesCache, &t2736_TI, &t2736_0_0_0, &t2736_1_0_0, t2736_IOs, &t2736_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2736)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5463_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.ContentSizeFitter/FitMode>
extern MethodInfo m28611_MI;
static PropertyInfo t5463____Count_PropertyInfo = 
{
	&t5463_TI, "Count", &m28611_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28612_MI;
static PropertyInfo t5463____IsReadOnly_PropertyInfo = 
{
	&t5463_TI, "IsReadOnly", &m28612_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5463_PIs[] =
{
	&t5463____Count_PropertyInfo,
	&t5463____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28611_GM;
MethodInfo m28611_MI = 
{
	"get_Count", NULL, &t5463_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28611_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28612_GM;
MethodInfo m28612_MI = 
{
	"get_IsReadOnly", NULL, &t5463_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28612_GM};
extern Il2CppType t253_0_0_0;
extern Il2CppType t253_0_0_0;
static ParameterInfo t5463_m28613_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t253_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28613_GM;
MethodInfo m28613_MI = 
{
	"Add", NULL, &t5463_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5463_m28613_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28613_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28614_GM;
MethodInfo m28614_MI = 
{
	"Clear", NULL, &t5463_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28614_GM};
extern Il2CppType t253_0_0_0;
static ParameterInfo t5463_m28615_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t253_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28615_GM;
MethodInfo m28615_MI = 
{
	"Contains", NULL, &t5463_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5463_m28615_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28615_GM};
extern Il2CppType t3874_0_0_0;
extern Il2CppType t3874_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5463_m28616_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3874_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28616_GM;
MethodInfo m28616_MI = 
{
	"CopyTo", NULL, &t5463_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5463_m28616_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28616_GM};
extern Il2CppType t253_0_0_0;
static ParameterInfo t5463_m28617_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t253_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28617_GM;
MethodInfo m28617_MI = 
{
	"Remove", NULL, &t5463_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5463_m28617_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28617_GM};
static MethodInfo* t5463_MIs[] =
{
	&m28611_MI,
	&m28612_MI,
	&m28613_MI,
	&m28614_MI,
	&m28615_MI,
	&m28616_MI,
	&m28617_MI,
	NULL
};
extern TypeInfo t5465_TI;
static TypeInfo* t5463_ITIs[] = 
{
	&t603_TI,
	&t5465_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5463_0_0_0;
extern Il2CppType t5463_1_0_0;
struct t5463;
extern Il2CppGenericClass t5463_GC;
TypeInfo t5463_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5463_MIs, t5463_PIs, NULL, NULL, NULL, NULL, NULL, &t5463_TI, t5463_ITIs, NULL, &EmptyCustomAttributesCache, &t5463_TI, &t5463_0_0_0, &t5463_1_0_0, NULL, &t5463_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.ContentSizeFitter/FitMode>
extern Il2CppType t4276_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28618_GM;
MethodInfo m28618_MI = 
{
	"GetEnumerator", NULL, &t5465_TI, &t4276_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28618_GM};
static MethodInfo* t5465_MIs[] =
{
	&m28618_MI,
	NULL
};
static TypeInfo* t5465_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5465_0_0_0;
extern Il2CppType t5465_1_0_0;
struct t5465;
extern Il2CppGenericClass t5465_GC;
TypeInfo t5465_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5465_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5465_TI, t5465_ITIs, NULL, &EmptyCustomAttributesCache, &t5465_TI, &t5465_0_0_0, &t5465_1_0_0, NULL, &t5465_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5464_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.ContentSizeFitter/FitMode>
extern MethodInfo m28619_MI;
extern MethodInfo m28620_MI;
static PropertyInfo t5464____Item_PropertyInfo = 
{
	&t5464_TI, "Item", &m28619_MI, &m28620_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5464_PIs[] =
{
	&t5464____Item_PropertyInfo,
	NULL
};
extern Il2CppType t253_0_0_0;
static ParameterInfo t5464_m28621_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t253_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28621_GM;
MethodInfo m28621_MI = 
{
	"IndexOf", NULL, &t5464_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5464_m28621_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28621_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t253_0_0_0;
static ParameterInfo t5464_m28622_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t253_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28622_GM;
MethodInfo m28622_MI = 
{
	"Insert", NULL, &t5464_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5464_m28622_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28622_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5464_m28623_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28623_GM;
MethodInfo m28623_MI = 
{
	"RemoveAt", NULL, &t5464_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5464_m28623_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28623_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5464_m28619_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t253_0_0_0;
extern void* RuntimeInvoker_t253_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28619_GM;
MethodInfo m28619_MI = 
{
	"get_Item", NULL, &t5464_TI, &t253_0_0_0, RuntimeInvoker_t253_t44, t5464_m28619_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28619_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t253_0_0_0;
static ParameterInfo t5464_m28620_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t253_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28620_GM;
MethodInfo m28620_MI = 
{
	"set_Item", NULL, &t5464_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5464_m28620_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28620_GM};
static MethodInfo* t5464_MIs[] =
{
	&m28621_MI,
	&m28622_MI,
	&m28623_MI,
	&m28619_MI,
	&m28620_MI,
	NULL
};
static TypeInfo* t5464_ITIs[] = 
{
	&t603_TI,
	&t5463_TI,
	&t5465_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5464_0_0_0;
extern Il2CppType t5464_1_0_0;
struct t5464;
extern Il2CppGenericClass t5464_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5464_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5464_MIs, t5464_PIs, NULL, NULL, NULL, NULL, NULL, &t5464_TI, t5464_ITIs, NULL, &t1908__CustomAttributeCache, &t5464_TI, &t5464_0_0_0, &t5464_1_0_0, NULL, &t5464_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4278_TI;

#include "t258.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.GridLayoutGroup>
extern MethodInfo m28624_MI;
static PropertyInfo t4278____Current_PropertyInfo = 
{
	&t4278_TI, "Current", &m28624_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4278_PIs[] =
{
	&t4278____Current_PropertyInfo,
	NULL
};
extern Il2CppType t258_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28624_GM;
MethodInfo m28624_MI = 
{
	"get_Current", NULL, &t4278_TI, &t258_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28624_GM};
static MethodInfo* t4278_MIs[] =
{
	&m28624_MI,
	NULL
};
static TypeInfo* t4278_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4278_0_0_0;
extern Il2CppType t4278_1_0_0;
struct t4278;
extern Il2CppGenericClass t4278_GC;
TypeInfo t4278_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4278_MIs, t4278_PIs, NULL, NULL, NULL, NULL, NULL, &t4278_TI, t4278_ITIs, NULL, &EmptyCustomAttributesCache, &t4278_TI, &t4278_0_0_0, &t4278_1_0_0, NULL, &t4278_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2737.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2737_TI;
#include "t2737MD.h"

extern TypeInfo t258_TI;
extern MethodInfo m14935_MI;
extern MethodInfo m21656_MI;
struct t20;
#define m21656(__this, p0, method) (t258 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.GridLayoutGroup>
extern Il2CppType t20_0_0_1;
FieldInfo t2737_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2737_TI, offsetof(t2737, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2737_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2737_TI, offsetof(t2737, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2737_FIs[] =
{
	&t2737_f0_FieldInfo,
	&t2737_f1_FieldInfo,
	NULL
};
extern MethodInfo m14932_MI;
static PropertyInfo t2737____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2737_TI, "System.Collections.IEnumerator.Current", &m14932_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2737____Current_PropertyInfo = 
{
	&t2737_TI, "Current", &m14935_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2737_PIs[] =
{
	&t2737____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2737____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2737_m14931_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14931_GM;
MethodInfo m14931_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2737_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2737_m14931_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14931_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14932_GM;
MethodInfo m14932_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2737_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14932_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14933_GM;
MethodInfo m14933_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2737_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14933_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14934_GM;
MethodInfo m14934_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2737_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14934_GM};
extern Il2CppType t258_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14935_GM;
MethodInfo m14935_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2737_TI, &t258_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14935_GM};
static MethodInfo* t2737_MIs[] =
{
	&m14931_MI,
	&m14932_MI,
	&m14933_MI,
	&m14934_MI,
	&m14935_MI,
	NULL
};
extern MethodInfo m14934_MI;
extern MethodInfo m14933_MI;
static MethodInfo* t2737_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14932_MI,
	&m14934_MI,
	&m14933_MI,
	&m14935_MI,
};
static TypeInfo* t2737_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4278_TI,
};
static Il2CppInterfaceOffsetPair t2737_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4278_TI, 7},
};
extern TypeInfo t258_TI;
static Il2CppRGCTXData t2737_RGCTXData[3] = 
{
	&m14935_MI/* Method Usage */,
	&t258_TI/* Class Usage */,
	&m21656_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2737_0_0_0;
extern Il2CppType t2737_1_0_0;
extern Il2CppGenericClass t2737_GC;
TypeInfo t2737_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2737_MIs, t2737_PIs, t2737_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2737_TI, t2737_ITIs, t2737_VT, &EmptyCustomAttributesCache, &t2737_TI, &t2737_0_0_0, &t2737_1_0_0, t2737_IOs, &t2737_GC, NULL, NULL, NULL, t2737_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2737)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5466_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.GridLayoutGroup>
extern MethodInfo m28625_MI;
static PropertyInfo t5466____Count_PropertyInfo = 
{
	&t5466_TI, "Count", &m28625_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28626_MI;
static PropertyInfo t5466____IsReadOnly_PropertyInfo = 
{
	&t5466_TI, "IsReadOnly", &m28626_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5466_PIs[] =
{
	&t5466____Count_PropertyInfo,
	&t5466____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28625_GM;
MethodInfo m28625_MI = 
{
	"get_Count", NULL, &t5466_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28625_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28626_GM;
MethodInfo m28626_MI = 
{
	"get_IsReadOnly", NULL, &t5466_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28626_GM};
extern Il2CppType t258_0_0_0;
extern Il2CppType t258_0_0_0;
static ParameterInfo t5466_m28627_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t258_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28627_GM;
MethodInfo m28627_MI = 
{
	"Add", NULL, &t5466_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5466_m28627_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28627_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28628_GM;
MethodInfo m28628_MI = 
{
	"Clear", NULL, &t5466_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28628_GM};
extern Il2CppType t258_0_0_0;
static ParameterInfo t5466_m28629_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t258_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28629_GM;
MethodInfo m28629_MI = 
{
	"Contains", NULL, &t5466_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5466_m28629_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28629_GM};
extern Il2CppType t3875_0_0_0;
extern Il2CppType t3875_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5466_m28630_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3875_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28630_GM;
MethodInfo m28630_MI = 
{
	"CopyTo", NULL, &t5466_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5466_m28630_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28630_GM};
extern Il2CppType t258_0_0_0;
static ParameterInfo t5466_m28631_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t258_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28631_GM;
MethodInfo m28631_MI = 
{
	"Remove", NULL, &t5466_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5466_m28631_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28631_GM};
static MethodInfo* t5466_MIs[] =
{
	&m28625_MI,
	&m28626_MI,
	&m28627_MI,
	&m28628_MI,
	&m28629_MI,
	&m28630_MI,
	&m28631_MI,
	NULL
};
extern TypeInfo t5468_TI;
static TypeInfo* t5466_ITIs[] = 
{
	&t603_TI,
	&t5468_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5466_0_0_0;
extern Il2CppType t5466_1_0_0;
struct t5466;
extern Il2CppGenericClass t5466_GC;
TypeInfo t5466_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5466_MIs, t5466_PIs, NULL, NULL, NULL, NULL, NULL, &t5466_TI, t5466_ITIs, NULL, &EmptyCustomAttributesCache, &t5466_TI, &t5466_0_0_0, &t5466_1_0_0, NULL, &t5466_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.GridLayoutGroup>
extern Il2CppType t4278_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28632_GM;
MethodInfo m28632_MI = 
{
	"GetEnumerator", NULL, &t5468_TI, &t4278_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28632_GM};
static MethodInfo* t5468_MIs[] =
{
	&m28632_MI,
	NULL
};
static TypeInfo* t5468_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5468_0_0_0;
extern Il2CppType t5468_1_0_0;
struct t5468;
extern Il2CppGenericClass t5468_GC;
TypeInfo t5468_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5468_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5468_TI, t5468_ITIs, NULL, &EmptyCustomAttributesCache, &t5468_TI, &t5468_0_0_0, &t5468_1_0_0, NULL, &t5468_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5467_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.GridLayoutGroup>
extern MethodInfo m28633_MI;
extern MethodInfo m28634_MI;
static PropertyInfo t5467____Item_PropertyInfo = 
{
	&t5467_TI, "Item", &m28633_MI, &m28634_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5467_PIs[] =
{
	&t5467____Item_PropertyInfo,
	NULL
};
extern Il2CppType t258_0_0_0;
static ParameterInfo t5467_m28635_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t258_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28635_GM;
MethodInfo m28635_MI = 
{
	"IndexOf", NULL, &t5467_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5467_m28635_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28635_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t258_0_0_0;
static ParameterInfo t5467_m28636_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t258_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28636_GM;
MethodInfo m28636_MI = 
{
	"Insert", NULL, &t5467_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5467_m28636_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28636_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5467_m28637_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28637_GM;
MethodInfo m28637_MI = 
{
	"RemoveAt", NULL, &t5467_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5467_m28637_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28637_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5467_m28633_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t258_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28633_GM;
MethodInfo m28633_MI = 
{
	"get_Item", NULL, &t5467_TI, &t258_0_0_0, RuntimeInvoker_t29_t44, t5467_m28633_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28633_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t258_0_0_0;
static ParameterInfo t5467_m28634_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t258_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28634_GM;
MethodInfo m28634_MI = 
{
	"set_Item", NULL, &t5467_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5467_m28634_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28634_GM};
static MethodInfo* t5467_MIs[] =
{
	&m28635_MI,
	&m28636_MI,
	&m28637_MI,
	&m28633_MI,
	&m28634_MI,
	NULL
};
static TypeInfo* t5467_ITIs[] = 
{
	&t603_TI,
	&t5466_TI,
	&t5468_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5467_0_0_0;
extern Il2CppType t5467_1_0_0;
struct t5467;
extern Il2CppGenericClass t5467_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5467_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5467_MIs, t5467_PIs, NULL, NULL, NULL, NULL, NULL, &t5467_TI, t5467_ITIs, NULL, &t1908__CustomAttributeCache, &t5467_TI, &t5467_0_0_0, &t5467_1_0_0, NULL, &t5467_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5469_TI;

#include "t259.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.LayoutGroup>
extern MethodInfo m28638_MI;
static PropertyInfo t5469____Count_PropertyInfo = 
{
	&t5469_TI, "Count", &m28638_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28639_MI;
static PropertyInfo t5469____IsReadOnly_PropertyInfo = 
{
	&t5469_TI, "IsReadOnly", &m28639_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5469_PIs[] =
{
	&t5469____Count_PropertyInfo,
	&t5469____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28638_GM;
MethodInfo m28638_MI = 
{
	"get_Count", NULL, &t5469_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28638_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28639_GM;
MethodInfo m28639_MI = 
{
	"get_IsReadOnly", NULL, &t5469_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28639_GM};
extern Il2CppType t259_0_0_0;
extern Il2CppType t259_0_0_0;
static ParameterInfo t5469_m28640_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t259_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28640_GM;
MethodInfo m28640_MI = 
{
	"Add", NULL, &t5469_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5469_m28640_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28640_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28641_GM;
MethodInfo m28641_MI = 
{
	"Clear", NULL, &t5469_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28641_GM};
extern Il2CppType t259_0_0_0;
static ParameterInfo t5469_m28642_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t259_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28642_GM;
MethodInfo m28642_MI = 
{
	"Contains", NULL, &t5469_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5469_m28642_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28642_GM};
extern Il2CppType t3876_0_0_0;
extern Il2CppType t3876_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5469_m28643_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3876_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28643_GM;
MethodInfo m28643_MI = 
{
	"CopyTo", NULL, &t5469_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5469_m28643_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28643_GM};
extern Il2CppType t259_0_0_0;
static ParameterInfo t5469_m28644_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t259_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28644_GM;
MethodInfo m28644_MI = 
{
	"Remove", NULL, &t5469_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5469_m28644_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28644_GM};
static MethodInfo* t5469_MIs[] =
{
	&m28638_MI,
	&m28639_MI,
	&m28640_MI,
	&m28641_MI,
	&m28642_MI,
	&m28643_MI,
	&m28644_MI,
	NULL
};
extern TypeInfo t5471_TI;
static TypeInfo* t5469_ITIs[] = 
{
	&t603_TI,
	&t5471_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5469_0_0_0;
extern Il2CppType t5469_1_0_0;
struct t5469;
extern Il2CppGenericClass t5469_GC;
TypeInfo t5469_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5469_MIs, t5469_PIs, NULL, NULL, NULL, NULL, NULL, &t5469_TI, t5469_ITIs, NULL, &EmptyCustomAttributesCache, &t5469_TI, &t5469_0_0_0, &t5469_1_0_0, NULL, &t5469_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.LayoutGroup>
extern Il2CppType t4280_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28645_GM;
MethodInfo m28645_MI = 
{
	"GetEnumerator", NULL, &t5471_TI, &t4280_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28645_GM};
static MethodInfo* t5471_MIs[] =
{
	&m28645_MI,
	NULL
};
static TypeInfo* t5471_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5471_0_0_0;
extern Il2CppType t5471_1_0_0;
struct t5471;
extern Il2CppGenericClass t5471_GC;
TypeInfo t5471_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5471_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5471_TI, t5471_ITIs, NULL, &EmptyCustomAttributesCache, &t5471_TI, &t5471_0_0_0, &t5471_1_0_0, NULL, &t5471_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4280_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.LayoutGroup>
extern MethodInfo m28646_MI;
static PropertyInfo t4280____Current_PropertyInfo = 
{
	&t4280_TI, "Current", &m28646_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4280_PIs[] =
{
	&t4280____Current_PropertyInfo,
	NULL
};
extern Il2CppType t259_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28646_GM;
MethodInfo m28646_MI = 
{
	"get_Current", NULL, &t4280_TI, &t259_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28646_GM};
static MethodInfo* t4280_MIs[] =
{
	&m28646_MI,
	NULL
};
static TypeInfo* t4280_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4280_0_0_0;
extern Il2CppType t4280_1_0_0;
struct t4280;
extern Il2CppGenericClass t4280_GC;
TypeInfo t4280_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4280_MIs, t4280_PIs, NULL, NULL, NULL, NULL, NULL, &t4280_TI, t4280_ITIs, NULL, &EmptyCustomAttributesCache, &t4280_TI, &t4280_0_0_0, &t4280_1_0_0, NULL, &t4280_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2738.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2738_TI;
#include "t2738MD.h"

extern TypeInfo t259_TI;
extern MethodInfo m14940_MI;
extern MethodInfo m21667_MI;
struct t20;
#define m21667(__this, p0, method) (t259 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.LayoutGroup>
extern Il2CppType t20_0_0_1;
FieldInfo t2738_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2738_TI, offsetof(t2738, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2738_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2738_TI, offsetof(t2738, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2738_FIs[] =
{
	&t2738_f0_FieldInfo,
	&t2738_f1_FieldInfo,
	NULL
};
extern MethodInfo m14937_MI;
static PropertyInfo t2738____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2738_TI, "System.Collections.IEnumerator.Current", &m14937_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2738____Current_PropertyInfo = 
{
	&t2738_TI, "Current", &m14940_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2738_PIs[] =
{
	&t2738____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2738____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2738_m14936_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14936_GM;
MethodInfo m14936_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2738_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2738_m14936_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14936_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14937_GM;
MethodInfo m14937_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2738_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14937_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14938_GM;
MethodInfo m14938_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2738_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14938_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14939_GM;
MethodInfo m14939_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2738_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14939_GM};
extern Il2CppType t259_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14940_GM;
MethodInfo m14940_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2738_TI, &t259_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14940_GM};
static MethodInfo* t2738_MIs[] =
{
	&m14936_MI,
	&m14937_MI,
	&m14938_MI,
	&m14939_MI,
	&m14940_MI,
	NULL
};
extern MethodInfo m14939_MI;
extern MethodInfo m14938_MI;
static MethodInfo* t2738_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14937_MI,
	&m14939_MI,
	&m14938_MI,
	&m14940_MI,
};
static TypeInfo* t2738_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4280_TI,
};
static Il2CppInterfaceOffsetPair t2738_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4280_TI, 7},
};
extern TypeInfo t259_TI;
static Il2CppRGCTXData t2738_RGCTXData[3] = 
{
	&m14940_MI/* Method Usage */,
	&t259_TI/* Class Usage */,
	&m21667_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2738_0_0_0;
extern Il2CppType t2738_1_0_0;
extern Il2CppGenericClass t2738_GC;
TypeInfo t2738_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2738_MIs, t2738_PIs, t2738_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2738_TI, t2738_ITIs, t2738_VT, &EmptyCustomAttributesCache, &t2738_TI, &t2738_0_0_0, &t2738_1_0_0, t2738_IOs, &t2738_GC, NULL, NULL, NULL, t2738_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2738)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5470_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.LayoutGroup>
extern MethodInfo m28647_MI;
extern MethodInfo m28648_MI;
static PropertyInfo t5470____Item_PropertyInfo = 
{
	&t5470_TI, "Item", &m28647_MI, &m28648_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5470_PIs[] =
{
	&t5470____Item_PropertyInfo,
	NULL
};
extern Il2CppType t259_0_0_0;
static ParameterInfo t5470_m28649_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t259_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28649_GM;
MethodInfo m28649_MI = 
{
	"IndexOf", NULL, &t5470_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5470_m28649_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28649_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t259_0_0_0;
static ParameterInfo t5470_m28650_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t259_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28650_GM;
MethodInfo m28650_MI = 
{
	"Insert", NULL, &t5470_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5470_m28650_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28650_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5470_m28651_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28651_GM;
MethodInfo m28651_MI = 
{
	"RemoveAt", NULL, &t5470_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5470_m28651_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28651_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5470_m28647_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t259_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28647_GM;
MethodInfo m28647_MI = 
{
	"get_Item", NULL, &t5470_TI, &t259_0_0_0, RuntimeInvoker_t29_t44, t5470_m28647_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28647_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t259_0_0_0;
static ParameterInfo t5470_m28648_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t259_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28648_GM;
MethodInfo m28648_MI = 
{
	"set_Item", NULL, &t5470_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5470_m28648_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28648_GM};
static MethodInfo* t5470_MIs[] =
{
	&m28649_MI,
	&m28650_MI,
	&m28651_MI,
	&m28647_MI,
	&m28648_MI,
	NULL
};
static TypeInfo* t5470_ITIs[] = 
{
	&t603_TI,
	&t5469_TI,
	&t5471_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5470_0_0_0;
extern Il2CppType t5470_1_0_0;
struct t5470;
extern Il2CppGenericClass t5470_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5470_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5470_MIs, t5470_PIs, NULL, NULL, NULL, NULL, NULL, &t5470_TI, t5470_ITIs, NULL, &t1908__CustomAttributeCache, &t5470_TI, &t5470_0_0_0, &t5470_1_0_0, NULL, &t5470_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5472_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.ILayoutGroup>
extern MethodInfo m28652_MI;
static PropertyInfo t5472____Count_PropertyInfo = 
{
	&t5472_TI, "Count", &m28652_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28653_MI;
static PropertyInfo t5472____IsReadOnly_PropertyInfo = 
{
	&t5472_TI, "IsReadOnly", &m28653_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5472_PIs[] =
{
	&t5472____Count_PropertyInfo,
	&t5472____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28652_GM;
MethodInfo m28652_MI = 
{
	"get_Count", NULL, &t5472_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28652_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28653_GM;
MethodInfo m28653_MI = 
{
	"get_IsReadOnly", NULL, &t5472_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28653_GM};
extern Il2CppType t411_0_0_0;
extern Il2CppType t411_0_0_0;
static ParameterInfo t5472_m28654_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t411_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28654_GM;
MethodInfo m28654_MI = 
{
	"Add", NULL, &t5472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5472_m28654_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28654_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28655_GM;
MethodInfo m28655_MI = 
{
	"Clear", NULL, &t5472_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28655_GM};
extern Il2CppType t411_0_0_0;
static ParameterInfo t5472_m28656_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t411_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28656_GM;
MethodInfo m28656_MI = 
{
	"Contains", NULL, &t5472_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5472_m28656_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28656_GM};
extern Il2CppType t3877_0_0_0;
extern Il2CppType t3877_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5472_m28657_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3877_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28657_GM;
MethodInfo m28657_MI = 
{
	"CopyTo", NULL, &t5472_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5472_m28657_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28657_GM};
extern Il2CppType t411_0_0_0;
static ParameterInfo t5472_m28658_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t411_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28658_GM;
MethodInfo m28658_MI = 
{
	"Remove", NULL, &t5472_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5472_m28658_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28658_GM};
static MethodInfo* t5472_MIs[] =
{
	&m28652_MI,
	&m28653_MI,
	&m28654_MI,
	&m28655_MI,
	&m28656_MI,
	&m28657_MI,
	&m28658_MI,
	NULL
};
extern TypeInfo t5474_TI;
static TypeInfo* t5472_ITIs[] = 
{
	&t603_TI,
	&t5474_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5472_0_0_0;
extern Il2CppType t5472_1_0_0;
struct t5472;
extern Il2CppGenericClass t5472_GC;
TypeInfo t5472_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5472_MIs, t5472_PIs, NULL, NULL, NULL, NULL, NULL, &t5472_TI, t5472_ITIs, NULL, &EmptyCustomAttributesCache, &t5472_TI, &t5472_0_0_0, &t5472_1_0_0, NULL, &t5472_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.ILayoutGroup>
extern Il2CppType t4282_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28659_GM;
MethodInfo m28659_MI = 
{
	"GetEnumerator", NULL, &t5474_TI, &t4282_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28659_GM};
static MethodInfo* t5474_MIs[] =
{
	&m28659_MI,
	NULL
};
static TypeInfo* t5474_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5474_0_0_0;
extern Il2CppType t5474_1_0_0;
struct t5474;
extern Il2CppGenericClass t5474_GC;
TypeInfo t5474_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5474_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5474_TI, t5474_ITIs, NULL, &EmptyCustomAttributesCache, &t5474_TI, &t5474_0_0_0, &t5474_1_0_0, NULL, &t5474_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4282_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ILayoutGroup>
extern MethodInfo m28660_MI;
static PropertyInfo t4282____Current_PropertyInfo = 
{
	&t4282_TI, "Current", &m28660_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4282_PIs[] =
{
	&t4282____Current_PropertyInfo,
	NULL
};
extern Il2CppType t411_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28660_GM;
MethodInfo m28660_MI = 
{
	"get_Current", NULL, &t4282_TI, &t411_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28660_GM};
static MethodInfo* t4282_MIs[] =
{
	&m28660_MI,
	NULL
};
static TypeInfo* t4282_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4282_0_0_0;
extern Il2CppType t4282_1_0_0;
struct t4282;
extern Il2CppGenericClass t4282_GC;
TypeInfo t4282_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4282_MIs, t4282_PIs, NULL, NULL, NULL, NULL, NULL, &t4282_TI, t4282_ITIs, NULL, &EmptyCustomAttributesCache, &t4282_TI, &t4282_0_0_0, &t4282_1_0_0, NULL, &t4282_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2739.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2739_TI;
#include "t2739MD.h"

extern TypeInfo t411_TI;
extern MethodInfo m14945_MI;
extern MethodInfo m21678_MI;
struct t20;
#define m21678(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutGroup>
extern Il2CppType t20_0_0_1;
FieldInfo t2739_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2739_TI, offsetof(t2739, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2739_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2739_TI, offsetof(t2739, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2739_FIs[] =
{
	&t2739_f0_FieldInfo,
	&t2739_f1_FieldInfo,
	NULL
};
extern MethodInfo m14942_MI;
static PropertyInfo t2739____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2739_TI, "System.Collections.IEnumerator.Current", &m14942_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2739____Current_PropertyInfo = 
{
	&t2739_TI, "Current", &m14945_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2739_PIs[] =
{
	&t2739____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2739____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2739_m14941_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14941_GM;
MethodInfo m14941_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2739_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2739_m14941_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14941_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14942_GM;
MethodInfo m14942_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2739_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14942_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14943_GM;
MethodInfo m14943_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2739_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14943_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14944_GM;
MethodInfo m14944_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2739_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14944_GM};
extern Il2CppType t411_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14945_GM;
MethodInfo m14945_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2739_TI, &t411_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14945_GM};
static MethodInfo* t2739_MIs[] =
{
	&m14941_MI,
	&m14942_MI,
	&m14943_MI,
	&m14944_MI,
	&m14945_MI,
	NULL
};
extern MethodInfo m14944_MI;
extern MethodInfo m14943_MI;
static MethodInfo* t2739_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14942_MI,
	&m14944_MI,
	&m14943_MI,
	&m14945_MI,
};
static TypeInfo* t2739_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4282_TI,
};
static Il2CppInterfaceOffsetPair t2739_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4282_TI, 7},
};
extern TypeInfo t411_TI;
static Il2CppRGCTXData t2739_RGCTXData[3] = 
{
	&m14945_MI/* Method Usage */,
	&t411_TI/* Class Usage */,
	&m21678_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2739_0_0_0;
extern Il2CppType t2739_1_0_0;
extern Il2CppGenericClass t2739_GC;
TypeInfo t2739_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2739_MIs, t2739_PIs, t2739_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2739_TI, t2739_ITIs, t2739_VT, &EmptyCustomAttributesCache, &t2739_TI, &t2739_0_0_0, &t2739_1_0_0, t2739_IOs, &t2739_GC, NULL, NULL, NULL, t2739_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2739)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5473_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutGroup>
extern MethodInfo m28661_MI;
extern MethodInfo m28662_MI;
static PropertyInfo t5473____Item_PropertyInfo = 
{
	&t5473_TI, "Item", &m28661_MI, &m28662_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5473_PIs[] =
{
	&t5473____Item_PropertyInfo,
	NULL
};
extern Il2CppType t411_0_0_0;
static ParameterInfo t5473_m28663_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t411_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28663_GM;
MethodInfo m28663_MI = 
{
	"IndexOf", NULL, &t5473_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5473_m28663_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28663_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t411_0_0_0;
static ParameterInfo t5473_m28664_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t411_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28664_GM;
MethodInfo m28664_MI = 
{
	"Insert", NULL, &t5473_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5473_m28664_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28664_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5473_m28665_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28665_GM;
MethodInfo m28665_MI = 
{
	"RemoveAt", NULL, &t5473_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5473_m28665_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28665_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5473_m28661_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t411_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28661_GM;
MethodInfo m28661_MI = 
{
	"get_Item", NULL, &t5473_TI, &t411_0_0_0, RuntimeInvoker_t29_t44, t5473_m28661_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28661_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t411_0_0_0;
static ParameterInfo t5473_m28662_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t411_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28662_GM;
MethodInfo m28662_MI = 
{
	"set_Item", NULL, &t5473_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5473_m28662_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28662_GM};
static MethodInfo* t5473_MIs[] =
{
	&m28663_MI,
	&m28664_MI,
	&m28665_MI,
	&m28661_MI,
	&m28662_MI,
	NULL
};
static TypeInfo* t5473_ITIs[] = 
{
	&t603_TI,
	&t5472_TI,
	&t5474_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5473_0_0_0;
extern Il2CppType t5473_1_0_0;
struct t5473;
extern Il2CppGenericClass t5473_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5473_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5473_MIs, t5473_PIs, NULL, NULL, NULL, NULL, NULL, &t5473_TI, t5473_ITIs, NULL, &t1908__CustomAttributeCache, &t5473_TI, &t5473_0_0_0, &t5473_1_0_0, NULL, &t5473_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2740.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2740_TI;
#include "t2740MD.h"

#include "t2741.h"
extern TypeInfo t2741_TI;
#include "t2741MD.h"
extern MethodInfo m14948_MI;
extern MethodInfo m14950_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.GridLayoutGroup>
extern Il2CppType t316_0_0_33;
FieldInfo t2740_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2740_TI, offsetof(t2740, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2740_FIs[] =
{
	&t2740_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t258_0_0_0;
static ParameterInfo t2740_m14946_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t258_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14946_GM;
MethodInfo m14946_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2740_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2740_m14946_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14946_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2740_m14947_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14947_GM;
MethodInfo m14947_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2740_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2740_m14947_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14947_GM};
static MethodInfo* t2740_MIs[] =
{
	&m14946_MI,
	&m14947_MI,
	NULL
};
extern MethodInfo m14947_MI;
extern MethodInfo m14951_MI;
static MethodInfo* t2740_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14947_MI,
	&m14951_MI,
};
extern Il2CppType t2742_0_0_0;
extern TypeInfo t2742_TI;
extern MethodInfo m21688_MI;
extern TypeInfo t258_TI;
extern MethodInfo m14953_MI;
extern TypeInfo t258_TI;
static Il2CppRGCTXData t2740_RGCTXData[8] = 
{
	&t2742_0_0_0/* Type Usage */,
	&t2742_TI/* Class Usage */,
	&m21688_MI/* Method Usage */,
	&t258_TI/* Class Usage */,
	&m14953_MI/* Method Usage */,
	&m14948_MI/* Method Usage */,
	&t258_TI/* Class Usage */,
	&m14950_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2740_0_0_0;
extern Il2CppType t2740_1_0_0;
struct t2740;
extern Il2CppGenericClass t2740_GC;
TypeInfo t2740_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2740_MIs, NULL, t2740_FIs, NULL, &t2741_TI, NULL, NULL, &t2740_TI, NULL, t2740_VT, &EmptyCustomAttributesCache, &t2740_TI, &t2740_0_0_0, &t2740_1_0_0, NULL, &t2740_GC, NULL, NULL, NULL, t2740_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2740), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2742.h"
extern TypeInfo t2742_TI;
#include "t2742MD.h"
struct t556;
#define m21688(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.GridLayoutGroup>
extern Il2CppType t2742_0_0_1;
FieldInfo t2741_f0_FieldInfo = 
{
	"Delegate", &t2742_0_0_1, &t2741_TI, offsetof(t2741, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2741_FIs[] =
{
	&t2741_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2741_m14948_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14948_GM;
MethodInfo m14948_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2741_m14948_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14948_GM};
extern Il2CppType t2742_0_0_0;
static ParameterInfo t2741_m14949_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2742_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14949_GM;
MethodInfo m14949_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2741_m14949_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14949_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2741_m14950_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14950_GM;
MethodInfo m14950_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2741_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2741_m14950_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14950_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2741_m14951_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14951_GM;
MethodInfo m14951_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2741_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2741_m14951_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14951_GM};
static MethodInfo* t2741_MIs[] =
{
	&m14948_MI,
	&m14949_MI,
	&m14950_MI,
	&m14951_MI,
	NULL
};
static MethodInfo* t2741_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14950_MI,
	&m14951_MI,
};
extern TypeInfo t2742_TI;
extern TypeInfo t258_TI;
static Il2CppRGCTXData t2741_RGCTXData[5] = 
{
	&t2742_0_0_0/* Type Usage */,
	&t2742_TI/* Class Usage */,
	&m21688_MI/* Method Usage */,
	&t258_TI/* Class Usage */,
	&m14953_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2741_0_0_0;
extern Il2CppType t2741_1_0_0;
struct t2741;
extern Il2CppGenericClass t2741_GC;
TypeInfo t2741_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2741_MIs, NULL, t2741_FIs, NULL, &t556_TI, NULL, NULL, &t2741_TI, NULL, t2741_VT, &EmptyCustomAttributesCache, &t2741_TI, &t2741_0_0_0, &t2741_1_0_0, NULL, &t2741_GC, NULL, NULL, NULL, t2741_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2741), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.GridLayoutGroup>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2742_m14952_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14952_GM;
MethodInfo m14952_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2742_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2742_m14952_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14952_GM};
extern Il2CppType t258_0_0_0;
static ParameterInfo t2742_m14953_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t258_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14953_GM;
MethodInfo m14953_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2742_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2742_m14953_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14953_GM};
extern Il2CppType t258_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2742_m14954_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t258_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14954_GM;
MethodInfo m14954_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2742_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2742_m14954_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14954_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2742_m14955_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14955_GM;
MethodInfo m14955_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2742_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2742_m14955_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14955_GM};
static MethodInfo* t2742_MIs[] =
{
	&m14952_MI,
	&m14953_MI,
	&m14954_MI,
	&m14955_MI,
	NULL
};
extern MethodInfo m14954_MI;
extern MethodInfo m14955_MI;
static MethodInfo* t2742_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14953_MI,
	&m14954_MI,
	&m14955_MI,
};
static Il2CppInterfaceOffsetPair t2742_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2742_1_0_0;
struct t2742;
extern Il2CppGenericClass t2742_GC;
TypeInfo t2742_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2742_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2742_TI, NULL, t2742_VT, &EmptyCustomAttributesCache, &t2742_TI, &t2742_0_0_0, &t2742_1_0_0, t2742_IOs, &t2742_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2742), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t264.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t264_TI;
#include "t264MD.h"

#include "t2.h"
#include "UnityEngine_ArrayTypes.h"
#include "t2750.h"
#include "t2747.h"
#include "t2748.h"
#include "t338.h"
#include "t2756.h"
#include "t2749.h"
extern TypeInfo t2_TI;
extern TypeInfo t44_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2743_TI;
extern TypeInfo t2750_TI;
extern TypeInfo t40_TI;
extern TypeInfo t2745_TI;
extern TypeInfo t2746_TI;
extern TypeInfo t2744_TI;
extern TypeInfo t2747_TI;
extern TypeInfo t338_TI;
extern TypeInfo t2748_TI;
extern TypeInfo t2756_TI;
#include "t915MD.h"
#include "t29MD.h"
#include "t602MD.h"
#include "t2747MD.h"
#include "t338MD.h"
#include "t2748MD.h"
#include "t2750MD.h"
#include "t2756MD.h"
extern MethodInfo m1964_MI;
extern MethodInfo m14999_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m21701_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m14986_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m14983_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m1992_MI;
extern MethodInfo m14978_MI;
extern MethodInfo m14984_MI;
extern MethodInfo m14987_MI;
extern MethodInfo m14989_MI;
extern MethodInfo m14973_MI;
extern MethodInfo m14997_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m14998_MI;
extern MethodInfo m28666_MI;
extern MethodInfo m28667_MI;
extern MethodInfo m28668_MI;
extern MethodInfo m28669_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m14988_MI;
extern MethodInfo m14974_MI;
extern MethodInfo m14975_MI;
extern MethodInfo m15011_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m21703_MI;
extern MethodInfo m14981_MI;
extern MethodInfo m14982_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m15086_MI;
extern MethodInfo m15005_MI;
extern MethodInfo m14985_MI;
extern MethodInfo m14991_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m15092_MI;
extern MethodInfo m21705_MI;
extern MethodInfo m21713_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m21701(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2754.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m21703(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m21705(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m21713(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2750  m14983 (t264 * __this, MethodInfo* method){
	{
		t2750  L_0 = {0};
		m15005(&L_0, __this, &m15005_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.RectTransform>
extern Il2CppType t44_0_0_32849;
FieldInfo t264_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t264_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2743_0_0_1;
FieldInfo t264_f1_FieldInfo = 
{
	"_items", &t2743_0_0_1, &t264_TI, offsetof(t264, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t264_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t264_TI, offsetof(t264, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t264_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t264_TI, offsetof(t264, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2743_0_0_49;
FieldInfo t264_f4_FieldInfo = 
{
	"EmptyArray", &t2743_0_0_49, &t264_TI, offsetof(t264_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t264_FIs[] =
{
	&t264_f0_FieldInfo,
	&t264_f1_FieldInfo,
	&t264_f2_FieldInfo,
	&t264_f3_FieldInfo,
	&t264_f4_FieldInfo,
	NULL
};
static const int32_t t264_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t264_f0_DefaultValue = 
{
	&t264_f0_FieldInfo, { (char*)&t264_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t264_FDVs[] = 
{
	&t264_f0_DefaultValue,
	NULL
};
extern MethodInfo m14966_MI;
static PropertyInfo t264____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t264_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14966_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14967_MI;
static PropertyInfo t264____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t264_TI, "System.Collections.ICollection.IsSynchronized", &m14967_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14968_MI;
static PropertyInfo t264____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t264_TI, "System.Collections.ICollection.SyncRoot", &m14968_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14969_MI;
static PropertyInfo t264____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t264_TI, "System.Collections.IList.IsFixedSize", &m14969_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14970_MI;
static PropertyInfo t264____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t264_TI, "System.Collections.IList.IsReadOnly", &m14970_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14971_MI;
extern MethodInfo m14972_MI;
static PropertyInfo t264____System_Collections_IList_Item_PropertyInfo = 
{
	&t264_TI, "System.Collections.IList.Item", &m14971_MI, &m14972_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t264____Capacity_PropertyInfo = 
{
	&t264_TI, "Capacity", &m14997_MI, &m14998_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1959_MI;
static PropertyInfo t264____Count_PropertyInfo = 
{
	&t264_TI, "Count", &m1959_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t264____Item_PropertyInfo = 
{
	&t264_TI, "Item", &m1964_MI, &m14999_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t264_PIs[] =
{
	&t264____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t264____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t264____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t264____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t264____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t264____System_Collections_IList_Item_PropertyInfo,
	&t264____Capacity_PropertyInfo,
	&t264____Count_PropertyInfo,
	&t264____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1989_GM;
MethodInfo m1989_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1989_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t264_m14956_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14956_GM;
MethodInfo m14956_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t264_m14956_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14956_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14957_GM;
MethodInfo m14957_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14957_GM};
extern Il2CppType t2744_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14958_GM;
MethodInfo m14958_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t264_TI, &t2744_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14958_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t264_m14959_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14959_GM;
MethodInfo m14959_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t264_m14959_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14959_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14960_GM;
MethodInfo m14960_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t264_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14960_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t264_m14961_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14961_GM;
MethodInfo m14961_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t264_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t264_m14961_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14961_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t264_m14962_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14962_GM;
MethodInfo m14962_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t264_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t264_m14962_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14962_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t264_m14963_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14963_GM;
MethodInfo m14963_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t264_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t264_m14963_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14963_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t264_m14964_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14964_GM;
MethodInfo m14964_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t264_m14964_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14964_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t264_m14965_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14965_GM;
MethodInfo m14965_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t264_m14965_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14965_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14966_GM;
MethodInfo m14966_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t264_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14966_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14967_GM;
MethodInfo m14967_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t264_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14967_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14968_GM;
MethodInfo m14968_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t264_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14968_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14969_GM;
MethodInfo m14969_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t264_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14969_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14970_GM;
MethodInfo m14970_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t264_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14970_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t264_m14971_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14971_GM;
MethodInfo m14971_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t264_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t264_m14971_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14971_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t264_m14972_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14972_GM;
MethodInfo m14972_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t264_m14972_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14972_GM};
extern Il2CppType t2_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t264_m1992_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1992_GM;
MethodInfo m1992_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t264_m1992_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1992_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t264_m14973_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14973_GM;
MethodInfo m14973_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t264_m14973_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14973_GM};
extern Il2CppType t2745_0_0_0;
extern Il2CppType t2745_0_0_0;
static ParameterInfo t264_m14974_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2745_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14974_GM;
MethodInfo m14974_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t264_m14974_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14974_GM};
extern Il2CppType t2746_0_0_0;
extern Il2CppType t2746_0_0_0;
static ParameterInfo t264_m14975_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2746_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14975_GM;
MethodInfo m14975_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t264_m14975_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14975_GM};
extern Il2CppType t2746_0_0_0;
static ParameterInfo t264_m14976_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2746_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14976_GM;
MethodInfo m14976_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t264_m14976_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14976_GM};
extern Il2CppType t2747_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14977_GM;
MethodInfo m14977_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t264_TI, &t2747_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14977_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1990_GM;
MethodInfo m1990_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1990_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t264_m14978_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14978_GM;
MethodInfo m14978_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t264_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t264_m14978_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14978_GM};
extern Il2CppType t2743_0_0_0;
extern Il2CppType t2743_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t264_m14979_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2743_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14979_GM;
MethodInfo m14979_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t264_m14979_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14979_GM};
extern Il2CppType t2748_0_0_0;
extern Il2CppType t2748_0_0_0;
static ParameterInfo t264_m14980_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2748_0_0_0},
};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14980_GM;
MethodInfo m14980_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t264_TI, &t2_0_0_0, RuntimeInvoker_t29_t29, t264_m14980_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14980_GM};
extern Il2CppType t2748_0_0_0;
static ParameterInfo t264_m14981_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2748_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14981_GM;
MethodInfo m14981_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t264_m14981_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14981_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2748_0_0_0;
static ParameterInfo t264_m14982_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2748_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14982_GM;
MethodInfo m14982_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t264_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t264_m14982_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14982_GM};
extern Il2CppType t2750_0_0_0;
extern void* RuntimeInvoker_t2750 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14983_GM;
MethodInfo m14983_MI = 
{
	"GetEnumerator", (methodPointerType)&m14983, &t264_TI, &t2750_0_0_0, RuntimeInvoker_t2750, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14983_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t264_m14984_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14984_GM;
MethodInfo m14984_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t264_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t264_m14984_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14984_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t264_m14985_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14985_GM;
MethodInfo m14985_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t264_m14985_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14985_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t264_m14986_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14986_GM;
MethodInfo m14986_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t264_m14986_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14986_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t264_m14987_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14987_GM;
MethodInfo m14987_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t264_m14987_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14987_GM};
extern Il2CppType t2746_0_0_0;
static ParameterInfo t264_m14988_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2746_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14988_GM;
MethodInfo m14988_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t264_m14988_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14988_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t264_m14989_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14989_GM;
MethodInfo m14989_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t264_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t264_m14989_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14989_GM};
extern Il2CppType t2748_0_0_0;
static ParameterInfo t264_m14990_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2748_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14990_GM;
MethodInfo m14990_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t264_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t264_m14990_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14990_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t264_m14991_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14991_GM;
MethodInfo m14991_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t264_m14991_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14991_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14992_GM;
MethodInfo m14992_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14992_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14993_GM;
MethodInfo m14993_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14993_GM};
extern Il2CppType t2749_0_0_0;
extern Il2CppType t2749_0_0_0;
static ParameterInfo t264_m14994_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2749_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14994_GM;
MethodInfo m14994_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t264_m14994_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14994_GM};
extern Il2CppType t2743_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14995_GM;
MethodInfo m14995_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t264_TI, &t2743_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14995_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14996_GM;
MethodInfo m14996_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14996_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14997_GM;
MethodInfo m14997_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t264_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14997_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t264_m14998_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14998_GM;
MethodInfo m14998_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t264_m14998_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14998_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1959_GM;
MethodInfo m1959_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t264_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1959_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t264_m1964_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1964_GM;
MethodInfo m1964_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t264_TI, &t2_0_0_0, RuntimeInvoker_t29_t44, t264_m1964_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1964_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t264_m14999_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14999_GM;
MethodInfo m14999_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t264_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t264_m14999_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14999_GM};
static MethodInfo* t264_MIs[] =
{
	&m1989_MI,
	&m14956_MI,
	&m14957_MI,
	&m14958_MI,
	&m14959_MI,
	&m14960_MI,
	&m14961_MI,
	&m14962_MI,
	&m14963_MI,
	&m14964_MI,
	&m14965_MI,
	&m14966_MI,
	&m14967_MI,
	&m14968_MI,
	&m14969_MI,
	&m14970_MI,
	&m14971_MI,
	&m14972_MI,
	&m1992_MI,
	&m14973_MI,
	&m14974_MI,
	&m14975_MI,
	&m14976_MI,
	&m14977_MI,
	&m1990_MI,
	&m14978_MI,
	&m14979_MI,
	&m14980_MI,
	&m14981_MI,
	&m14982_MI,
	&m14983_MI,
	&m14984_MI,
	&m14985_MI,
	&m14986_MI,
	&m14987_MI,
	&m14988_MI,
	&m14989_MI,
	&m14990_MI,
	&m14991_MI,
	&m14992_MI,
	&m14993_MI,
	&m14994_MI,
	&m14995_MI,
	&m14996_MI,
	&m14997_MI,
	&m14998_MI,
	&m1959_MI,
	&m1964_MI,
	&m14999_MI,
	NULL
};
extern MethodInfo m14960_MI;
extern MethodInfo m14959_MI;
extern MethodInfo m14961_MI;
extern MethodInfo m1990_MI;
extern MethodInfo m14962_MI;
extern MethodInfo m14963_MI;
extern MethodInfo m14964_MI;
extern MethodInfo m14965_MI;
extern MethodInfo m14979_MI;
extern MethodInfo m14958_MI;
static MethodInfo* t264_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14960_MI,
	&m1959_MI,
	&m14967_MI,
	&m14968_MI,
	&m14959_MI,
	&m14969_MI,
	&m14970_MI,
	&m14971_MI,
	&m14972_MI,
	&m14961_MI,
	&m1990_MI,
	&m14962_MI,
	&m14963_MI,
	&m14964_MI,
	&m14965_MI,
	&m14991_MI,
	&m1959_MI,
	&m14966_MI,
	&m1992_MI,
	&m1990_MI,
	&m14978_MI,
	&m14979_MI,
	&m14989_MI,
	&m14958_MI,
	&m14984_MI,
	&m14987_MI,
	&m14991_MI,
	&m1964_MI,
	&m14999_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
extern TypeInfo t2752_TI;
static TypeInfo* t264_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2745_TI,
	&t2746_TI,
	&t2752_TI,
};
static Il2CppInterfaceOffsetPair t264_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2745_TI, 20},
	{ &t2746_TI, 27},
	{ &t2752_TI, 28},
};
extern TypeInfo t264_TI;
extern TypeInfo t2743_TI;
extern TypeInfo t2750_TI;
extern TypeInfo t2_TI;
extern TypeInfo t2745_TI;
extern TypeInfo t2747_TI;
static Il2CppRGCTXData t264_RGCTXData[37] = 
{
	&t264_TI/* Static Usage */,
	&t2743_TI/* Array Usage */,
	&m14983_MI/* Method Usage */,
	&t2750_TI/* Class Usage */,
	&t2_TI/* Class Usage */,
	&m1992_MI/* Method Usage */,
	&m14978_MI/* Method Usage */,
	&m14984_MI/* Method Usage */,
	&m14986_MI/* Method Usage */,
	&m14987_MI/* Method Usage */,
	&m14989_MI/* Method Usage */,
	&m1964_MI/* Method Usage */,
	&m14999_MI/* Method Usage */,
	&m14973_MI/* Method Usage */,
	&m14997_MI/* Method Usage */,
	&m14998_MI/* Method Usage */,
	&m28666_MI/* Method Usage */,
	&m28667_MI/* Method Usage */,
	&m28668_MI/* Method Usage */,
	&m28669_MI/* Method Usage */,
	&m14988_MI/* Method Usage */,
	&t2745_TI/* Class Usage */,
	&m14974_MI/* Method Usage */,
	&m14975_MI/* Method Usage */,
	&t2747_TI/* Class Usage */,
	&m15011_MI/* Method Usage */,
	&m21703_MI/* Method Usage */,
	&m14981_MI/* Method Usage */,
	&m14982_MI/* Method Usage */,
	&m15086_MI/* Method Usage */,
	&m15005_MI/* Method Usage */,
	&m14985_MI/* Method Usage */,
	&m14991_MI/* Method Usage */,
	&m15092_MI/* Method Usage */,
	&m21705_MI/* Method Usage */,
	&m21713_MI/* Method Usage */,
	&m21701_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t264_0_0_0;
extern Il2CppType t264_1_0_0;
struct t264;
extern Il2CppGenericClass t264_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t264_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t264_MIs, t264_PIs, t264_FIs, NULL, &t29_TI, NULL, NULL, &t264_TI, t264_ITIs, t264_VT, &t1261__CustomAttributeCache, &t264_TI, &t264_0_0_0, &t264_1_0_0, t264_IOs, &t264_GC, NULL, t264_FDVs, NULL, t264_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t264), 0, -1, sizeof(t264_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.RectTransform>
static PropertyInfo t2745____Count_PropertyInfo = 
{
	&t2745_TI, "Count", &m28666_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28670_MI;
static PropertyInfo t2745____IsReadOnly_PropertyInfo = 
{
	&t2745_TI, "IsReadOnly", &m28670_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2745_PIs[] =
{
	&t2745____Count_PropertyInfo,
	&t2745____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28666_GM;
MethodInfo m28666_MI = 
{
	"get_Count", NULL, &t2745_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28666_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28670_GM;
MethodInfo m28670_MI = 
{
	"get_IsReadOnly", NULL, &t2745_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28670_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2745_m28671_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28671_GM;
MethodInfo m28671_MI = 
{
	"Add", NULL, &t2745_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2745_m28671_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28671_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28672_GM;
MethodInfo m28672_MI = 
{
	"Clear", NULL, &t2745_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28672_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2745_m28673_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28673_GM;
MethodInfo m28673_MI = 
{
	"Contains", NULL, &t2745_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2745_m28673_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28673_GM};
extern Il2CppType t2743_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2745_m28667_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2743_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28667_GM;
MethodInfo m28667_MI = 
{
	"CopyTo", NULL, &t2745_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2745_m28667_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28667_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2745_m28674_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28674_GM;
MethodInfo m28674_MI = 
{
	"Remove", NULL, &t2745_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2745_m28674_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28674_GM};
static MethodInfo* t2745_MIs[] =
{
	&m28666_MI,
	&m28670_MI,
	&m28671_MI,
	&m28672_MI,
	&m28673_MI,
	&m28667_MI,
	&m28674_MI,
	NULL
};
static TypeInfo* t2745_ITIs[] = 
{
	&t603_TI,
	&t2746_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2745_1_0_0;
struct t2745;
extern Il2CppGenericClass t2745_GC;
TypeInfo t2745_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2745_MIs, t2745_PIs, NULL, NULL, NULL, NULL, NULL, &t2745_TI, t2745_ITIs, NULL, &EmptyCustomAttributesCache, &t2745_TI, &t2745_0_0_0, &t2745_1_0_0, NULL, &t2745_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.RectTransform>
extern Il2CppType t2744_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28668_GM;
MethodInfo m28668_MI = 
{
	"GetEnumerator", NULL, &t2746_TI, &t2744_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28668_GM};
static MethodInfo* t2746_MIs[] =
{
	&m28668_MI,
	NULL
};
static TypeInfo* t2746_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2746_1_0_0;
struct t2746;
extern Il2CppGenericClass t2746_GC;
TypeInfo t2746_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2746_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2746_TI, t2746_ITIs, NULL, &EmptyCustomAttributesCache, &t2746_TI, &t2746_0_0_0, &t2746_1_0_0, NULL, &t2746_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.RectTransform>
static PropertyInfo t2744____Current_PropertyInfo = 
{
	&t2744_TI, "Current", &m28669_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2744_PIs[] =
{
	&t2744____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28669_GM;
MethodInfo m28669_MI = 
{
	"get_Current", NULL, &t2744_TI, &t2_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28669_GM};
static MethodInfo* t2744_MIs[] =
{
	&m28669_MI,
	NULL
};
static TypeInfo* t2744_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2744_0_0_0;
extern Il2CppType t2744_1_0_0;
struct t2744;
extern Il2CppGenericClass t2744_GC;
TypeInfo t2744_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2744_MIs, t2744_PIs, NULL, NULL, NULL, NULL, NULL, &t2744_TI, t2744_ITIs, NULL, &EmptyCustomAttributesCache, &t2744_TI, &t2744_0_0_0, &t2744_1_0_0, NULL, &t2744_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2751.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2751_TI;
#include "t2751MD.h"

extern MethodInfo m15004_MI;
extern MethodInfo m21690_MI;
struct t20;
#define m21690(__this, p0, method) (t2 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.RectTransform>
extern Il2CppType t20_0_0_1;
FieldInfo t2751_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2751_TI, offsetof(t2751, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2751_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2751_TI, offsetof(t2751, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2751_FIs[] =
{
	&t2751_f0_FieldInfo,
	&t2751_f1_FieldInfo,
	NULL
};
extern MethodInfo m15001_MI;
static PropertyInfo t2751____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2751_TI, "System.Collections.IEnumerator.Current", &m15001_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2751____Current_PropertyInfo = 
{
	&t2751_TI, "Current", &m15004_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2751_PIs[] =
{
	&t2751____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2751____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2751_m15000_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15000_GM;
MethodInfo m15000_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2751_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2751_m15000_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15000_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15001_GM;
MethodInfo m15001_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2751_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15001_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15002_GM;
MethodInfo m15002_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2751_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15002_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15003_GM;
MethodInfo m15003_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2751_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15003_GM};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15004_GM;
MethodInfo m15004_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2751_TI, &t2_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15004_GM};
static MethodInfo* t2751_MIs[] =
{
	&m15000_MI,
	&m15001_MI,
	&m15002_MI,
	&m15003_MI,
	&m15004_MI,
	NULL
};
extern MethodInfo m15003_MI;
extern MethodInfo m15002_MI;
static MethodInfo* t2751_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15001_MI,
	&m15003_MI,
	&m15002_MI,
	&m15004_MI,
};
static TypeInfo* t2751_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2744_TI,
};
static Il2CppInterfaceOffsetPair t2751_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2744_TI, 7},
};
extern TypeInfo t2_TI;
static Il2CppRGCTXData t2751_RGCTXData[3] = 
{
	&m15004_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
	&m21690_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2751_0_0_0;
extern Il2CppType t2751_1_0_0;
extern Il2CppGenericClass t2751_GC;
TypeInfo t2751_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2751_MIs, t2751_PIs, t2751_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2751_TI, t2751_ITIs, t2751_VT, &EmptyCustomAttributesCache, &t2751_TI, &t2751_0_0_0, &t2751_1_0_0, t2751_IOs, &t2751_GC, NULL, NULL, NULL, t2751_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2751)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.RectTransform>
extern MethodInfo m28675_MI;
extern MethodInfo m28676_MI;
static PropertyInfo t2752____Item_PropertyInfo = 
{
	&t2752_TI, "Item", &m28675_MI, &m28676_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2752_PIs[] =
{
	&t2752____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2752_m28677_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28677_GM;
MethodInfo m28677_MI = 
{
	"IndexOf", NULL, &t2752_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2752_m28677_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28677_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2752_m28678_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28678_GM;
MethodInfo m28678_MI = 
{
	"Insert", NULL, &t2752_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2752_m28678_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28678_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2752_m28679_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28679_GM;
MethodInfo m28679_MI = 
{
	"RemoveAt", NULL, &t2752_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2752_m28679_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28679_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2752_m28675_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28675_GM;
MethodInfo m28675_MI = 
{
	"get_Item", NULL, &t2752_TI, &t2_0_0_0, RuntimeInvoker_t29_t44, t2752_m28675_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28675_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2752_m28676_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28676_GM;
MethodInfo m28676_MI = 
{
	"set_Item", NULL, &t2752_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2752_m28676_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28676_GM};
static MethodInfo* t2752_MIs[] =
{
	&m28677_MI,
	&m28678_MI,
	&m28679_MI,
	&m28675_MI,
	&m28676_MI,
	NULL
};
static TypeInfo* t2752_ITIs[] = 
{
	&t603_TI,
	&t2745_TI,
	&t2746_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2752_0_0_0;
extern Il2CppType t2752_1_0_0;
struct t2752;
extern Il2CppGenericClass t2752_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2752_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2752_MIs, t2752_PIs, NULL, NULL, NULL, NULL, NULL, &t2752_TI, t2752_ITIs, NULL, &t1908__CustomAttributeCache, &t2752_TI, &t2752_0_0_0, &t2752_1_0_0, NULL, &t2752_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m15008_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>
extern Il2CppType t264_0_0_1;
FieldInfo t2750_f0_FieldInfo = 
{
	"l", &t264_0_0_1, &t2750_TI, offsetof(t2750, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2750_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2750_TI, offsetof(t2750, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2750_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2750_TI, offsetof(t2750, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t2_0_0_1;
FieldInfo t2750_f3_FieldInfo = 
{
	"current", &t2_0_0_1, &t2750_TI, offsetof(t2750, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2750_FIs[] =
{
	&t2750_f0_FieldInfo,
	&t2750_f1_FieldInfo,
	&t2750_f2_FieldInfo,
	&t2750_f3_FieldInfo,
	NULL
};
extern MethodInfo m15006_MI;
static PropertyInfo t2750____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2750_TI, "System.Collections.IEnumerator.Current", &m15006_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15010_MI;
static PropertyInfo t2750____Current_PropertyInfo = 
{
	&t2750_TI, "Current", &m15010_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2750_PIs[] =
{
	&t2750____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2750____Current_PropertyInfo,
	NULL
};
extern Il2CppType t264_0_0_0;
static ParameterInfo t2750_m15005_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t264_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15005_GM;
MethodInfo m15005_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2750_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2750_m15005_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15005_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15006_GM;
MethodInfo m15006_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2750_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15006_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15007_GM;
MethodInfo m15007_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2750_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15007_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15008_GM;
MethodInfo m15008_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2750_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15008_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15009_GM;
MethodInfo m15009_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2750_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15009_GM};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15010_GM;
MethodInfo m15010_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2750_TI, &t2_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15010_GM};
static MethodInfo* t2750_MIs[] =
{
	&m15005_MI,
	&m15006_MI,
	&m15007_MI,
	&m15008_MI,
	&m15009_MI,
	&m15010_MI,
	NULL
};
extern MethodInfo m15009_MI;
extern MethodInfo m15007_MI;
static MethodInfo* t2750_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15006_MI,
	&m15009_MI,
	&m15007_MI,
	&m15010_MI,
};
static TypeInfo* t2750_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2744_TI,
};
static Il2CppInterfaceOffsetPair t2750_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2744_TI, 7},
};
extern TypeInfo t2_TI;
extern TypeInfo t2750_TI;
static Il2CppRGCTXData t2750_RGCTXData[3] = 
{
	&m15008_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
	&t2750_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2750_0_0_0;
extern Il2CppType t2750_1_0_0;
extern Il2CppGenericClass t2750_GC;
extern TypeInfo t1261_TI;
TypeInfo t2750_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2750_MIs, t2750_PIs, t2750_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2750_TI, t2750_ITIs, t2750_VT, &EmptyCustomAttributesCache, &t2750_TI, &t2750_0_0_0, &t2750_1_0_0, t2750_IOs, &t2750_GC, NULL, NULL, NULL, t2750_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2750)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t2753MD.h"
extern MethodInfo m15040_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m15072_MI;
extern MethodInfo m28673_MI;
extern MethodInfo m28677_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.RectTransform>
extern Il2CppType t2752_0_0_1;
FieldInfo t2747_f0_FieldInfo = 
{
	"list", &t2752_0_0_1, &t2747_TI, offsetof(t2747, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2747_FIs[] =
{
	&t2747_f0_FieldInfo,
	NULL
};
extern MethodInfo m15017_MI;
extern MethodInfo m15018_MI;
static PropertyInfo t2747____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2747_TI, "System.Collections.Generic.IList<T>.Item", &m15017_MI, &m15018_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15019_MI;
static PropertyInfo t2747____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2747_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m15019_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15029_MI;
static PropertyInfo t2747____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2747_TI, "System.Collections.ICollection.IsSynchronized", &m15029_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15030_MI;
static PropertyInfo t2747____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2747_TI, "System.Collections.ICollection.SyncRoot", &m15030_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15031_MI;
static PropertyInfo t2747____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2747_TI, "System.Collections.IList.IsFixedSize", &m15031_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15032_MI;
static PropertyInfo t2747____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2747_TI, "System.Collections.IList.IsReadOnly", &m15032_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15033_MI;
extern MethodInfo m15034_MI;
static PropertyInfo t2747____System_Collections_IList_Item_PropertyInfo = 
{
	&t2747_TI, "System.Collections.IList.Item", &m15033_MI, &m15034_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15039_MI;
static PropertyInfo t2747____Count_PropertyInfo = 
{
	&t2747_TI, "Count", &m15039_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2747____Item_PropertyInfo = 
{
	&t2747_TI, "Item", &m15040_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2747_PIs[] =
{
	&t2747____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2747____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2747____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2747____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2747____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2747____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2747____System_Collections_IList_Item_PropertyInfo,
	&t2747____Count_PropertyInfo,
	&t2747____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2752_0_0_0;
static ParameterInfo t2747_m15011_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2752_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15011_GM;
MethodInfo m15011_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2747_m15011_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15011_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2747_m15012_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15012_GM;
MethodInfo m15012_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2747_m15012_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15012_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15013_GM;
MethodInfo m15013_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15013_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2747_m15014_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15014_GM;
MethodInfo m15014_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2747_m15014_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15014_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2747_m15015_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15015_GM;
MethodInfo m15015_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2747_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2747_m15015_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15015_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2747_m15016_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15016_GM;
MethodInfo m15016_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2747_m15016_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15016_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2747_m15017_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15017_GM;
MethodInfo m15017_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2747_TI, &t2_0_0_0, RuntimeInvoker_t29_t44, t2747_m15017_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15017_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2747_m15018_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15018_GM;
MethodInfo m15018_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2747_m15018_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15018_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15019_GM;
MethodInfo m15019_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2747_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15019_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2747_m15020_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15020_GM;
MethodInfo m15020_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2747_m15020_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15020_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15021_GM;
MethodInfo m15021_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2747_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15021_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2747_m15022_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15022_GM;
MethodInfo m15022_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2747_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2747_m15022_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15022_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15023_GM;
MethodInfo m15023_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15023_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2747_m15024_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15024_GM;
MethodInfo m15024_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2747_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2747_m15024_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15024_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2747_m15025_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15025_GM;
MethodInfo m15025_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2747_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2747_m15025_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15025_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2747_m15026_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15026_GM;
MethodInfo m15026_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2747_m15026_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15026_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2747_m15027_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15027_GM;
MethodInfo m15027_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2747_m15027_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15027_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2747_m15028_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15028_GM;
MethodInfo m15028_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2747_m15028_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15028_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15029_GM;
MethodInfo m15029_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2747_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15029_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15030_GM;
MethodInfo m15030_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2747_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15030_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15031_GM;
MethodInfo m15031_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2747_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15031_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15032_GM;
MethodInfo m15032_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2747_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15032_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2747_m15033_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15033_GM;
MethodInfo m15033_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2747_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2747_m15033_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15033_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2747_m15034_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15034_GM;
MethodInfo m15034_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2747_m15034_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15034_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2747_m15035_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15035_GM;
MethodInfo m15035_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2747_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2747_m15035_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15035_GM};
extern Il2CppType t2743_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2747_m15036_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2743_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15036_GM;
MethodInfo m15036_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2747_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2747_m15036_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15036_GM};
extern Il2CppType t2744_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15037_GM;
MethodInfo m15037_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2747_TI, &t2744_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15037_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2747_m15038_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15038_GM;
MethodInfo m15038_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2747_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2747_m15038_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15038_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15039_GM;
MethodInfo m15039_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2747_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15039_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2747_m15040_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15040_GM;
MethodInfo m15040_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2747_TI, &t2_0_0_0, RuntimeInvoker_t29_t44, t2747_m15040_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15040_GM};
static MethodInfo* t2747_MIs[] =
{
	&m15011_MI,
	&m15012_MI,
	&m15013_MI,
	&m15014_MI,
	&m15015_MI,
	&m15016_MI,
	&m15017_MI,
	&m15018_MI,
	&m15019_MI,
	&m15020_MI,
	&m15021_MI,
	&m15022_MI,
	&m15023_MI,
	&m15024_MI,
	&m15025_MI,
	&m15026_MI,
	&m15027_MI,
	&m15028_MI,
	&m15029_MI,
	&m15030_MI,
	&m15031_MI,
	&m15032_MI,
	&m15033_MI,
	&m15034_MI,
	&m15035_MI,
	&m15036_MI,
	&m15037_MI,
	&m15038_MI,
	&m15039_MI,
	&m15040_MI,
	NULL
};
extern MethodInfo m15021_MI;
extern MethodInfo m15020_MI;
extern MethodInfo m15022_MI;
extern MethodInfo m15023_MI;
extern MethodInfo m15024_MI;
extern MethodInfo m15025_MI;
extern MethodInfo m15026_MI;
extern MethodInfo m15027_MI;
extern MethodInfo m15028_MI;
extern MethodInfo m15012_MI;
extern MethodInfo m15013_MI;
extern MethodInfo m15035_MI;
extern MethodInfo m15036_MI;
extern MethodInfo m15015_MI;
extern MethodInfo m15038_MI;
extern MethodInfo m15014_MI;
extern MethodInfo m15016_MI;
extern MethodInfo m15037_MI;
static MethodInfo* t2747_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15021_MI,
	&m15039_MI,
	&m15029_MI,
	&m15030_MI,
	&m15020_MI,
	&m15031_MI,
	&m15032_MI,
	&m15033_MI,
	&m15034_MI,
	&m15022_MI,
	&m15023_MI,
	&m15024_MI,
	&m15025_MI,
	&m15026_MI,
	&m15027_MI,
	&m15028_MI,
	&m15039_MI,
	&m15019_MI,
	&m15012_MI,
	&m15013_MI,
	&m15035_MI,
	&m15036_MI,
	&m15015_MI,
	&m15038_MI,
	&m15014_MI,
	&m15016_MI,
	&m15017_MI,
	&m15018_MI,
	&m15037_MI,
	&m15040_MI,
};
static TypeInfo* t2747_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2745_TI,
	&t2752_TI,
	&t2746_TI,
};
static Il2CppInterfaceOffsetPair t2747_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2745_TI, 20},
	{ &t2752_TI, 27},
	{ &t2746_TI, 32},
};
extern TypeInfo t2_TI;
static Il2CppRGCTXData t2747_RGCTXData[9] = 
{
	&m15040_MI/* Method Usage */,
	&m15072_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
	&m28673_MI/* Method Usage */,
	&m28677_MI/* Method Usage */,
	&m28675_MI/* Method Usage */,
	&m28667_MI/* Method Usage */,
	&m28668_MI/* Method Usage */,
	&m28666_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2747_0_0_0;
extern Il2CppType t2747_1_0_0;
struct t2747;
extern Il2CppGenericClass t2747_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2747_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2747_MIs, t2747_PIs, t2747_FIs, NULL, &t29_TI, NULL, NULL, &t2747_TI, t2747_ITIs, t2747_VT, &t1263__CustomAttributeCache, &t2747_TI, &t2747_0_0_0, &t2747_1_0_0, t2747_IOs, &t2747_GC, NULL, NULL, NULL, t2747_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2747), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2753.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2753_TI;

extern MethodInfo m15075_MI;
extern MethodInfo m15076_MI;
extern MethodInfo m15073_MI;
extern MethodInfo m15071_MI;
extern MethodInfo m1989_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m15064_MI;
extern MethodInfo m15074_MI;
extern MethodInfo m15062_MI;
extern MethodInfo m15067_MI;
extern MethodInfo m15058_MI;
extern MethodInfo m28672_MI;
extern MethodInfo m28678_MI;
extern MethodInfo m28679_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.RectTransform>
extern Il2CppType t2752_0_0_1;
FieldInfo t2753_f0_FieldInfo = 
{
	"list", &t2752_0_0_1, &t2753_TI, offsetof(t2753, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2753_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2753_TI, offsetof(t2753, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2753_FIs[] =
{
	&t2753_f0_FieldInfo,
	&t2753_f1_FieldInfo,
	NULL
};
extern MethodInfo m15042_MI;
static PropertyInfo t2753____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2753_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m15042_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15050_MI;
static PropertyInfo t2753____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2753_TI, "System.Collections.ICollection.IsSynchronized", &m15050_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15051_MI;
static PropertyInfo t2753____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2753_TI, "System.Collections.ICollection.SyncRoot", &m15051_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15052_MI;
static PropertyInfo t2753____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2753_TI, "System.Collections.IList.IsFixedSize", &m15052_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15053_MI;
static PropertyInfo t2753____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2753_TI, "System.Collections.IList.IsReadOnly", &m15053_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15054_MI;
extern MethodInfo m15055_MI;
static PropertyInfo t2753____System_Collections_IList_Item_PropertyInfo = 
{
	&t2753_TI, "System.Collections.IList.Item", &m15054_MI, &m15055_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15068_MI;
static PropertyInfo t2753____Count_PropertyInfo = 
{
	&t2753_TI, "Count", &m15068_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m15069_MI;
extern MethodInfo m15070_MI;
static PropertyInfo t2753____Item_PropertyInfo = 
{
	&t2753_TI, "Item", &m15069_MI, &m15070_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2753_PIs[] =
{
	&t2753____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2753____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2753____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2753____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2753____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2753____System_Collections_IList_Item_PropertyInfo,
	&t2753____Count_PropertyInfo,
	&t2753____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15041_GM;
MethodInfo m15041_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15041_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15042_GM;
MethodInfo m15042_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2753_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15042_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2753_m15043_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15043_GM;
MethodInfo m15043_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2753_m15043_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15043_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15044_GM;
MethodInfo m15044_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2753_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15044_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2753_m15045_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15045_GM;
MethodInfo m15045_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2753_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2753_m15045_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15045_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2753_m15046_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15046_GM;
MethodInfo m15046_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2753_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2753_m15046_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15046_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2753_m15047_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15047_GM;
MethodInfo m15047_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2753_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2753_m15047_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15047_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2753_m15048_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15048_GM;
MethodInfo m15048_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2753_m15048_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15048_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2753_m15049_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15049_GM;
MethodInfo m15049_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2753_m15049_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15049_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15050_GM;
MethodInfo m15050_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2753_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15050_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15051_GM;
MethodInfo m15051_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2753_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15051_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15052_GM;
MethodInfo m15052_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2753_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15052_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15053_GM;
MethodInfo m15053_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2753_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15053_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2753_m15054_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15054_GM;
MethodInfo m15054_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2753_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2753_m15054_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15054_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2753_m15055_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15055_GM;
MethodInfo m15055_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2753_m15055_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15055_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2753_m15056_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15056_GM;
MethodInfo m15056_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2753_m15056_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15056_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15057_GM;
MethodInfo m15057_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15057_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15058_GM;
MethodInfo m15058_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15058_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2753_m15059_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15059_GM;
MethodInfo m15059_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2753_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2753_m15059_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15059_GM};
extern Il2CppType t2743_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2753_m15060_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2743_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15060_GM;
MethodInfo m15060_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2753_m15060_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15060_GM};
extern Il2CppType t2744_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15061_GM;
MethodInfo m15061_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2753_TI, &t2744_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15061_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2753_m15062_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15062_GM;
MethodInfo m15062_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2753_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2753_m15062_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15062_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2753_m15063_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15063_GM;
MethodInfo m15063_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2753_m15063_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15063_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2753_m15064_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15064_GM;
MethodInfo m15064_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2753_m15064_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15064_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2753_m15065_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15065_GM;
MethodInfo m15065_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2753_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2753_m15065_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15065_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2753_m15066_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15066_GM;
MethodInfo m15066_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2753_m15066_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15066_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2753_m15067_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15067_GM;
MethodInfo m15067_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2753_m15067_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15067_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15068_GM;
MethodInfo m15068_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2753_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15068_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2753_m15069_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15069_GM;
MethodInfo m15069_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2753_TI, &t2_0_0_0, RuntimeInvoker_t29_t44, t2753_m15069_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15069_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2753_m15070_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15070_GM;
MethodInfo m15070_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2753_m15070_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15070_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2753_m15071_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15071_GM;
MethodInfo m15071_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2753_m15071_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15071_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2753_m15072_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15072_GM;
MethodInfo m15072_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2753_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2753_m15072_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15072_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2753_m15073_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t2_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15073_GM;
MethodInfo m15073_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2753_TI, &t2_0_0_0, RuntimeInvoker_t29_t29, t2753_m15073_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15073_GM};
extern Il2CppType t2752_0_0_0;
static ParameterInfo t2753_m15074_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2752_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15074_GM;
MethodInfo m15074_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2753_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2753_m15074_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15074_GM};
extern Il2CppType t2752_0_0_0;
static ParameterInfo t2753_m15075_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2752_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15075_GM;
MethodInfo m15075_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2753_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2753_m15075_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15075_GM};
extern Il2CppType t2752_0_0_0;
static ParameterInfo t2753_m15076_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2752_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15076_GM;
MethodInfo m15076_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2753_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2753_m15076_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15076_GM};
static MethodInfo* t2753_MIs[] =
{
	&m15041_MI,
	&m15042_MI,
	&m15043_MI,
	&m15044_MI,
	&m15045_MI,
	&m15046_MI,
	&m15047_MI,
	&m15048_MI,
	&m15049_MI,
	&m15050_MI,
	&m15051_MI,
	&m15052_MI,
	&m15053_MI,
	&m15054_MI,
	&m15055_MI,
	&m15056_MI,
	&m15057_MI,
	&m15058_MI,
	&m15059_MI,
	&m15060_MI,
	&m15061_MI,
	&m15062_MI,
	&m15063_MI,
	&m15064_MI,
	&m15065_MI,
	&m15066_MI,
	&m15067_MI,
	&m15068_MI,
	&m15069_MI,
	&m15070_MI,
	&m15071_MI,
	&m15072_MI,
	&m15073_MI,
	&m15074_MI,
	&m15075_MI,
	&m15076_MI,
	NULL
};
extern MethodInfo m15044_MI;
extern MethodInfo m15043_MI;
extern MethodInfo m15045_MI;
extern MethodInfo m15057_MI;
extern MethodInfo m15046_MI;
extern MethodInfo m15047_MI;
extern MethodInfo m15048_MI;
extern MethodInfo m15049_MI;
extern MethodInfo m15066_MI;
extern MethodInfo m15056_MI;
extern MethodInfo m15059_MI;
extern MethodInfo m15060_MI;
extern MethodInfo m15065_MI;
extern MethodInfo m15063_MI;
extern MethodInfo m15061_MI;
static MethodInfo* t2753_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15044_MI,
	&m15068_MI,
	&m15050_MI,
	&m15051_MI,
	&m15043_MI,
	&m15052_MI,
	&m15053_MI,
	&m15054_MI,
	&m15055_MI,
	&m15045_MI,
	&m15057_MI,
	&m15046_MI,
	&m15047_MI,
	&m15048_MI,
	&m15049_MI,
	&m15066_MI,
	&m15068_MI,
	&m15042_MI,
	&m15056_MI,
	&m15057_MI,
	&m15059_MI,
	&m15060_MI,
	&m15065_MI,
	&m15062_MI,
	&m15063_MI,
	&m15066_MI,
	&m15069_MI,
	&m15070_MI,
	&m15061_MI,
	&m15058_MI,
	&m15064_MI,
	&m15067_MI,
	&m15071_MI,
};
static TypeInfo* t2753_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2745_TI,
	&t2752_TI,
	&t2746_TI,
};
static Il2CppInterfaceOffsetPair t2753_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2745_TI, 20},
	{ &t2752_TI, 27},
	{ &t2746_TI, 32},
};
extern TypeInfo t264_TI;
extern TypeInfo t2_TI;
static Il2CppRGCTXData t2753_RGCTXData[25] = 
{
	&t264_TI/* Class Usage */,
	&m1989_MI/* Method Usage */,
	&m28670_MI/* Method Usage */,
	&m28668_MI/* Method Usage */,
	&m28666_MI/* Method Usage */,
	&m15073_MI/* Method Usage */,
	&m15064_MI/* Method Usage */,
	&m15072_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
	&m28673_MI/* Method Usage */,
	&m28677_MI/* Method Usage */,
	&m15074_MI/* Method Usage */,
	&m15062_MI/* Method Usage */,
	&m15067_MI/* Method Usage */,
	&m15075_MI/* Method Usage */,
	&m15076_MI/* Method Usage */,
	&m28675_MI/* Method Usage */,
	&m15071_MI/* Method Usage */,
	&m15058_MI/* Method Usage */,
	&m28672_MI/* Method Usage */,
	&m28667_MI/* Method Usage */,
	&m28678_MI/* Method Usage */,
	&m28679_MI/* Method Usage */,
	&m28676_MI/* Method Usage */,
	&t2_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2753_0_0_0;
extern Il2CppType t2753_1_0_0;
struct t2753;
extern Il2CppGenericClass t2753_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2753_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2753_MIs, t2753_PIs, t2753_FIs, NULL, &t29_TI, NULL, NULL, &t2753_TI, t2753_ITIs, t2753_VT, &t1262__CustomAttributeCache, &t2753_TI, &t2753_0_0_0, &t2753_1_0_0, t2753_IOs, &t2753_GC, NULL, NULL, NULL, t2753_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2753), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2754_TI;
#include "t2754MD.h"

#include "t1257.h"
#include "t2755.h"
extern TypeInfo t6708_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2755_TI;
#include "t931MD.h"
#include "t2755MD.h"
extern Il2CppType t6708_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m15082_MI;
extern MethodInfo m28680_MI;
extern MethodInfo m21702_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.RectTransform>
extern Il2CppType t2754_0_0_49;
FieldInfo t2754_f0_FieldInfo = 
{
	"_default", &t2754_0_0_49, &t2754_TI, offsetof(t2754_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2754_FIs[] =
{
	&t2754_f0_FieldInfo,
	NULL
};
extern MethodInfo m15081_MI;
static PropertyInfo t2754____Default_PropertyInfo = 
{
	&t2754_TI, "Default", &m15081_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2754_PIs[] =
{
	&t2754____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15077_GM;
MethodInfo m15077_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2754_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15077_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15078_GM;
MethodInfo m15078_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2754_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15078_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2754_m15079_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15079_GM;
MethodInfo m15079_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2754_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2754_m15079_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15079_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2754_m15080_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15080_GM;
MethodInfo m15080_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2754_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2754_m15080_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15080_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2754_m28680_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28680_GM;
MethodInfo m28680_MI = 
{
	"GetHashCode", NULL, &t2754_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2754_m28680_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28680_GM};
extern Il2CppType t2_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2754_m21702_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21702_GM;
MethodInfo m21702_MI = 
{
	"Equals", NULL, &t2754_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2754_m21702_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m21702_GM};
extern Il2CppType t2754_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15081_GM;
MethodInfo m15081_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2754_TI, &t2754_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15081_GM};
static MethodInfo* t2754_MIs[] =
{
	&m15077_MI,
	&m15078_MI,
	&m15079_MI,
	&m15080_MI,
	&m28680_MI,
	&m21702_MI,
	&m15081_MI,
	NULL
};
extern MethodInfo m15080_MI;
extern MethodInfo m15079_MI;
static MethodInfo* t2754_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m21702_MI,
	&m28680_MI,
	&m15080_MI,
	&m15079_MI,
	NULL,
	NULL,
};
extern TypeInfo t6709_TI;
extern TypeInfo t734_TI;
static TypeInfo* t2754_ITIs[] = 
{
	&t6709_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2754_IOs[] = 
{
	{ &t6709_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2754_TI;
extern TypeInfo t2754_TI;
extern TypeInfo t2755_TI;
extern TypeInfo t2_TI;
static Il2CppRGCTXData t2754_RGCTXData[9] = 
{
	&t6708_0_0_0/* Type Usage */,
	&t2_0_0_0/* Type Usage */,
	&t2754_TI/* Class Usage */,
	&t2754_TI/* Static Usage */,
	&t2755_TI/* Class Usage */,
	&m15082_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
	&m28680_MI/* Method Usage */,
	&m21702_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2754_0_0_0;
extern Il2CppType t2754_1_0_0;
struct t2754;
extern Il2CppGenericClass t2754_GC;
TypeInfo t2754_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2754_MIs, t2754_PIs, t2754_FIs, NULL, &t29_TI, NULL, NULL, &t2754_TI, t2754_ITIs, t2754_VT, &EmptyCustomAttributesCache, &t2754_TI, &t2754_0_0_0, &t2754_1_0_0, t2754_IOs, &t2754_GC, NULL, NULL, NULL, t2754_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2754), 0, -1, sizeof(t2754_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.RectTransform>
extern Il2CppType t2_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t6709_m28681_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28681_GM;
MethodInfo m28681_MI = 
{
	"Equals", NULL, &t6709_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6709_m28681_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28681_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t6709_m28682_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28682_GM;
MethodInfo m28682_MI = 
{
	"GetHashCode", NULL, &t6709_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6709_m28682_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28682_GM};
static MethodInfo* t6709_MIs[] =
{
	&m28681_MI,
	&m28682_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6709_0_0_0;
extern Il2CppType t6709_1_0_0;
struct t6709;
extern Il2CppGenericClass t6709_GC;
TypeInfo t6709_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6709_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6709_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6709_TI, &t6709_0_0_0, &t6709_1_0_0, NULL, &t6709_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.RectTransform>
extern Il2CppType t2_0_0_0;
static ParameterInfo t6708_m28683_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28683_GM;
MethodInfo m28683_MI = 
{
	"Equals", NULL, &t6708_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6708_m28683_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28683_GM};
static MethodInfo* t6708_MIs[] =
{
	&m28683_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6708_1_0_0;
struct t6708;
extern Il2CppGenericClass t6708_GC;
TypeInfo t6708_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6708_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6708_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6708_TI, &t6708_0_0_0, &t6708_1_0_0, NULL, &t6708_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m15077_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RectTransform>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15082_GM;
MethodInfo m15082_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2755_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15082_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2755_m15083_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15083_GM;
MethodInfo m15083_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2755_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2755_m15083_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15083_GM};
extern Il2CppType t2_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2755_m15084_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15084_GM;
MethodInfo m15084_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2755_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2755_m15084_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15084_GM};
static MethodInfo* t2755_MIs[] =
{
	&m15082_MI,
	&m15083_MI,
	&m15084_MI,
	NULL
};
extern MethodInfo m15084_MI;
extern MethodInfo m15083_MI;
static MethodInfo* t2755_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15084_MI,
	&m15083_MI,
	&m15080_MI,
	&m15079_MI,
	&m15083_MI,
	&m15084_MI,
};
static Il2CppInterfaceOffsetPair t2755_IOs[] = 
{
	{ &t6709_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2754_TI;
extern TypeInfo t2754_TI;
extern TypeInfo t2755_TI;
extern TypeInfo t2_TI;
extern TypeInfo t2_TI;
static Il2CppRGCTXData t2755_RGCTXData[11] = 
{
	&t6708_0_0_0/* Type Usage */,
	&t2_0_0_0/* Type Usage */,
	&t2754_TI/* Class Usage */,
	&t2754_TI/* Static Usage */,
	&t2755_TI/* Class Usage */,
	&m15082_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
	&m28680_MI/* Method Usage */,
	&m21702_MI/* Method Usage */,
	&m15077_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2755_0_0_0;
extern Il2CppType t2755_1_0_0;
struct t2755;
extern Il2CppGenericClass t2755_GC;
extern TypeInfo t1256_TI;
TypeInfo t2755_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2755_MIs, NULL, NULL, NULL, &t2754_TI, NULL, &t1256_TI, &t2755_TI, NULL, t2755_VT, &EmptyCustomAttributesCache, &t2755_TI, &t2755_0_0_0, &t2755_1_0_0, t2755_IOs, &t2755_GC, NULL, NULL, NULL, t2755_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2755), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.RectTransform>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2748_m15085_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15085_GM;
MethodInfo m15085_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2748_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2748_m15085_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15085_GM};
extern Il2CppType t2_0_0_0;
static ParameterInfo t2748_m15086_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15086_GM;
MethodInfo m15086_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2748_m15086_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15086_GM};
extern Il2CppType t2_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2748_m15087_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15087_GM;
MethodInfo m15087_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2748_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2748_m15087_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15087_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2748_m15088_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15088_GM;
MethodInfo m15088_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2748_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2748_m15088_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15088_GM};
static MethodInfo* t2748_MIs[] =
{
	&m15085_MI,
	&m15086_MI,
	&m15087_MI,
	&m15088_MI,
	NULL
};
extern MethodInfo m15087_MI;
extern MethodInfo m15088_MI;
static MethodInfo* t2748_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15086_MI,
	&m15087_MI,
	&m15088_MI,
};
static Il2CppInterfaceOffsetPair t2748_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2748_1_0_0;
struct t2748;
extern Il2CppGenericClass t2748_GC;
TypeInfo t2748_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2748_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2748_TI, NULL, t2748_VT, &EmptyCustomAttributesCache, &t2748_TI, &t2748_0_0_0, &t2748_1_0_0, t2748_IOs, &t2748_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2748), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t2757.h"
extern TypeInfo t4288_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t2757_TI;
#include "t2757MD.h"
extern Il2CppType t4288_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m15093_MI;
extern MethodInfo m28684_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.RectTransform>
extern Il2CppType t2756_0_0_49;
FieldInfo t2756_f0_FieldInfo = 
{
	"_default", &t2756_0_0_49, &t2756_TI, offsetof(t2756_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2756_FIs[] =
{
	&t2756_f0_FieldInfo,
	NULL
};
static PropertyInfo t2756____Default_PropertyInfo = 
{
	&t2756_TI, "Default", &m15092_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2756_PIs[] =
{
	&t2756____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15089_GM;
MethodInfo m15089_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2756_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15089_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15090_GM;
MethodInfo m15090_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2756_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15090_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2756_m15091_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15091_GM;
MethodInfo m15091_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2756_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2756_m15091_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15091_GM};
extern Il2CppType t2_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2756_m28684_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28684_GM;
MethodInfo m28684_MI = 
{
	"Compare", NULL, &t2756_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2756_m28684_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28684_GM};
extern Il2CppType t2756_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15092_GM;
MethodInfo m15092_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2756_TI, &t2756_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15092_GM};
static MethodInfo* t2756_MIs[] =
{
	&m15089_MI,
	&m15090_MI,
	&m15091_MI,
	&m28684_MI,
	&m15092_MI,
	NULL
};
extern MethodInfo m15091_MI;
static MethodInfo* t2756_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m28684_MI,
	&m15091_MI,
	NULL,
};
extern TypeInfo t4287_TI;
extern TypeInfo t726_TI;
static TypeInfo* t2756_ITIs[] = 
{
	&t4287_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2756_IOs[] = 
{
	{ &t4287_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2756_TI;
extern TypeInfo t2756_TI;
extern TypeInfo t2757_TI;
extern TypeInfo t2_TI;
static Il2CppRGCTXData t2756_RGCTXData[8] = 
{
	&t4288_0_0_0/* Type Usage */,
	&t2_0_0_0/* Type Usage */,
	&t2756_TI/* Class Usage */,
	&t2756_TI/* Static Usage */,
	&t2757_TI/* Class Usage */,
	&m15093_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
	&m28684_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2756_0_0_0;
extern Il2CppType t2756_1_0_0;
struct t2756;
extern Il2CppGenericClass t2756_GC;
TypeInfo t2756_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2756_MIs, t2756_PIs, t2756_FIs, NULL, &t29_TI, NULL, NULL, &t2756_TI, t2756_ITIs, t2756_VT, &EmptyCustomAttributesCache, &t2756_TI, &t2756_0_0_0, &t2756_1_0_0, t2756_IOs, &t2756_GC, NULL, NULL, NULL, t2756_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2756), 0, -1, sizeof(t2756_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.RectTransform>
extern Il2CppType t2_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t4287_m21710_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21710_GM;
MethodInfo m21710_MI = 
{
	"Compare", NULL, &t4287_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4287_m21710_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m21710_GM};
static MethodInfo* t4287_MIs[] =
{
	&m21710_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4287_0_0_0;
extern Il2CppType t4287_1_0_0;
struct t4287;
extern Il2CppGenericClass t4287_GC;
TypeInfo t4287_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4287_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4287_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4287_TI, &t4287_0_0_0, &t4287_1_0_0, NULL, &t4287_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.RectTransform>
extern Il2CppType t2_0_0_0;
static ParameterInfo t4288_m21711_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21711_GM;
MethodInfo m21711_MI = 
{
	"CompareTo", NULL, &t4288_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4288_m21711_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m21711_GM};
static MethodInfo* t4288_MIs[] =
{
	&m21711_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4288_1_0_0;
struct t4288;
extern Il2CppGenericClass t4288_GC;
TypeInfo t4288_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4288_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4288_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4288_TI, &t4288_0_0_0, &t4288_1_0_0, NULL, &t4288_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m15089_MI;
extern MethodInfo m21711_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.RectTransform>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15093_GM;
MethodInfo m15093_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2757_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15093_GM};
extern Il2CppType t2_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2757_m15094_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15094_GM;
MethodInfo m15094_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2757_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2757_m15094_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15094_GM};
static MethodInfo* t2757_MIs[] =
{
	&m15093_MI,
	&m15094_MI,
	NULL
};
extern MethodInfo m15094_MI;
static MethodInfo* t2757_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15094_MI,
	&m15091_MI,
	&m15094_MI,
};
static Il2CppInterfaceOffsetPair t2757_IOs[] = 
{
	{ &t4287_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2756_TI;
extern TypeInfo t2756_TI;
extern TypeInfo t2757_TI;
extern TypeInfo t2_TI;
extern TypeInfo t2_TI;
extern TypeInfo t4288_TI;
static Il2CppRGCTXData t2757_RGCTXData[12] = 
{
	&t4288_0_0_0/* Type Usage */,
	&t2_0_0_0/* Type Usage */,
	&t2756_TI/* Class Usage */,
	&t2756_TI/* Static Usage */,
	&t2757_TI/* Class Usage */,
	&m15093_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
	&m28684_MI/* Method Usage */,
	&m15089_MI/* Method Usage */,
	&t2_TI/* Class Usage */,
	&t4288_TI/* Class Usage */,
	&m21711_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2757_0_0_0;
extern Il2CppType t2757_1_0_0;
struct t2757;
extern Il2CppGenericClass t2757_GC;
extern TypeInfo t1246_TI;
TypeInfo t2757_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2757_MIs, NULL, NULL, NULL, &t2756_TI, NULL, &t1246_TI, &t2757_TI, NULL, t2757_VT, &EmptyCustomAttributesCache, &t2757_TI, &t2757_0_0_0, &t2757_1_0_0, t2757_IOs, &t2757_GC, NULL, NULL, NULL, t2757_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2757), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2749_TI;
#include "t2749MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.RectTransform>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2749_m15095_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15095_GM;
MethodInfo m15095_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2749_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2749_m15095_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15095_GM};
extern Il2CppType t2_0_0_0;
extern Il2CppType t2_0_0_0;
static ParameterInfo t2749_m15096_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15096_GM;
MethodInfo m15096_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2749_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2749_m15096_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15096_GM};
extern Il2CppType t2_0_0_0;
extern Il2CppType t2_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2749_m15097_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t2_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15097_GM;
MethodInfo m15097_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2749_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2749_m15097_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m15097_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2749_m15098_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15098_GM;
MethodInfo m15098_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2749_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2749_m15098_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15098_GM};
static MethodInfo* t2749_MIs[] =
{
	&m15095_MI,
	&m15096_MI,
	&m15097_MI,
	&m15098_MI,
	NULL
};
extern MethodInfo m15096_MI;
extern MethodInfo m15097_MI;
extern MethodInfo m15098_MI;
static MethodInfo* t2749_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15096_MI,
	&m15097_MI,
	&m15098_MI,
};
static Il2CppInterfaceOffsetPair t2749_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2749_1_0_0;
struct t2749;
extern Il2CppGenericClass t2749_GC;
TypeInfo t2749_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2749_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2749_TI, NULL, t2749_VT, &EmptyCustomAttributesCache, &t2749_TI, &t2749_0_0_0, &t2749_1_0_0, t2749_IOs, &t2749_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2749), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4289_TI;

#include "t255.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.GridLayoutGroup/Corner>
extern MethodInfo m28685_MI;
static PropertyInfo t4289____Current_PropertyInfo = 
{
	&t4289_TI, "Current", &m28685_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4289_PIs[] =
{
	&t4289____Current_PropertyInfo,
	NULL
};
extern Il2CppType t255_0_0_0;
extern void* RuntimeInvoker_t255 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28685_GM;
MethodInfo m28685_MI = 
{
	"get_Current", NULL, &t4289_TI, &t255_0_0_0, RuntimeInvoker_t255, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28685_GM};
static MethodInfo* t4289_MIs[] =
{
	&m28685_MI,
	NULL
};
static TypeInfo* t4289_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4289_0_0_0;
extern Il2CppType t4289_1_0_0;
struct t4289;
extern Il2CppGenericClass t4289_GC;
TypeInfo t4289_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4289_MIs, t4289_PIs, NULL, NULL, NULL, NULL, NULL, &t4289_TI, t4289_ITIs, NULL, &EmptyCustomAttributesCache, &t4289_TI, &t4289_0_0_0, &t4289_1_0_0, NULL, &t4289_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2758.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2758_TI;
#include "t2758MD.h"

extern TypeInfo t255_TI;
extern MethodInfo m15103_MI;
extern MethodInfo m21716_MI;
struct t20;
 int32_t m21716 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15099_MI;
 void m15099 (t2758 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15100_MI;
 t29 * m15100 (t2758 * __this, MethodInfo* method){
	{
		int32_t L_0 = m15103(__this, &m15103_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t255_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15101_MI;
 void m15101 (t2758 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15102_MI;
 bool m15102 (t2758 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m15103 (t2758 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21716(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21716_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.GridLayoutGroup/Corner>
extern Il2CppType t20_0_0_1;
FieldInfo t2758_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2758_TI, offsetof(t2758, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2758_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2758_TI, offsetof(t2758, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2758_FIs[] =
{
	&t2758_f0_FieldInfo,
	&t2758_f1_FieldInfo,
	NULL
};
static PropertyInfo t2758____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2758_TI, "System.Collections.IEnumerator.Current", &m15100_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2758____Current_PropertyInfo = 
{
	&t2758_TI, "Current", &m15103_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2758_PIs[] =
{
	&t2758____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2758____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2758_m15099_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15099_GM;
MethodInfo m15099_MI = 
{
	".ctor", (methodPointerType)&m15099, &t2758_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2758_m15099_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15099_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15100_GM;
MethodInfo m15100_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15100, &t2758_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15100_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15101_GM;
MethodInfo m15101_MI = 
{
	"Dispose", (methodPointerType)&m15101, &t2758_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15101_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15102_GM;
MethodInfo m15102_MI = 
{
	"MoveNext", (methodPointerType)&m15102, &t2758_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15102_GM};
extern Il2CppType t255_0_0_0;
extern void* RuntimeInvoker_t255 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15103_GM;
MethodInfo m15103_MI = 
{
	"get_Current", (methodPointerType)&m15103, &t2758_TI, &t255_0_0_0, RuntimeInvoker_t255, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15103_GM};
static MethodInfo* t2758_MIs[] =
{
	&m15099_MI,
	&m15100_MI,
	&m15101_MI,
	&m15102_MI,
	&m15103_MI,
	NULL
};
static MethodInfo* t2758_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15100_MI,
	&m15102_MI,
	&m15101_MI,
	&m15103_MI,
};
static TypeInfo* t2758_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4289_TI,
};
static Il2CppInterfaceOffsetPair t2758_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4289_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2758_0_0_0;
extern Il2CppType t2758_1_0_0;
extern Il2CppGenericClass t2758_GC;
TypeInfo t2758_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2758_MIs, t2758_PIs, t2758_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2758_TI, t2758_ITIs, t2758_VT, &EmptyCustomAttributesCache, &t2758_TI, &t2758_0_0_0, &t2758_1_0_0, t2758_IOs, &t2758_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2758)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5475_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.GridLayoutGroup/Corner>
extern MethodInfo m28686_MI;
static PropertyInfo t5475____Count_PropertyInfo = 
{
	&t5475_TI, "Count", &m28686_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28687_MI;
static PropertyInfo t5475____IsReadOnly_PropertyInfo = 
{
	&t5475_TI, "IsReadOnly", &m28687_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5475_PIs[] =
{
	&t5475____Count_PropertyInfo,
	&t5475____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28686_GM;
MethodInfo m28686_MI = 
{
	"get_Count", NULL, &t5475_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28686_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28687_GM;
MethodInfo m28687_MI = 
{
	"get_IsReadOnly", NULL, &t5475_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28687_GM};
extern Il2CppType t255_0_0_0;
extern Il2CppType t255_0_0_0;
static ParameterInfo t5475_m28688_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t255_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28688_GM;
MethodInfo m28688_MI = 
{
	"Add", NULL, &t5475_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5475_m28688_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28688_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28689_GM;
MethodInfo m28689_MI = 
{
	"Clear", NULL, &t5475_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28689_GM};
extern Il2CppType t255_0_0_0;
static ParameterInfo t5475_m28690_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t255_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28690_GM;
MethodInfo m28690_MI = 
{
	"Contains", NULL, &t5475_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5475_m28690_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28690_GM};
extern Il2CppType t3878_0_0_0;
extern Il2CppType t3878_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5475_m28691_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3878_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28691_GM;
MethodInfo m28691_MI = 
{
	"CopyTo", NULL, &t5475_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5475_m28691_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28691_GM};
extern Il2CppType t255_0_0_0;
static ParameterInfo t5475_m28692_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t255_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28692_GM;
MethodInfo m28692_MI = 
{
	"Remove", NULL, &t5475_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5475_m28692_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28692_GM};
static MethodInfo* t5475_MIs[] =
{
	&m28686_MI,
	&m28687_MI,
	&m28688_MI,
	&m28689_MI,
	&m28690_MI,
	&m28691_MI,
	&m28692_MI,
	NULL
};
extern TypeInfo t5477_TI;
static TypeInfo* t5475_ITIs[] = 
{
	&t603_TI,
	&t5477_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5475_0_0_0;
extern Il2CppType t5475_1_0_0;
struct t5475;
extern Il2CppGenericClass t5475_GC;
TypeInfo t5475_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5475_MIs, t5475_PIs, NULL, NULL, NULL, NULL, NULL, &t5475_TI, t5475_ITIs, NULL, &EmptyCustomAttributesCache, &t5475_TI, &t5475_0_0_0, &t5475_1_0_0, NULL, &t5475_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.GridLayoutGroup/Corner>
extern Il2CppType t4289_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28693_GM;
MethodInfo m28693_MI = 
{
	"GetEnumerator", NULL, &t5477_TI, &t4289_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28693_GM};
static MethodInfo* t5477_MIs[] =
{
	&m28693_MI,
	NULL
};
static TypeInfo* t5477_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5477_0_0_0;
extern Il2CppType t5477_1_0_0;
struct t5477;
extern Il2CppGenericClass t5477_GC;
TypeInfo t5477_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5477_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5477_TI, t5477_ITIs, NULL, &EmptyCustomAttributesCache, &t5477_TI, &t5477_0_0_0, &t5477_1_0_0, NULL, &t5477_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5476_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.GridLayoutGroup/Corner>
extern MethodInfo m28694_MI;
extern MethodInfo m28695_MI;
static PropertyInfo t5476____Item_PropertyInfo = 
{
	&t5476_TI, "Item", &m28694_MI, &m28695_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5476_PIs[] =
{
	&t5476____Item_PropertyInfo,
	NULL
};
extern Il2CppType t255_0_0_0;
static ParameterInfo t5476_m28696_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t255_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28696_GM;
MethodInfo m28696_MI = 
{
	"IndexOf", NULL, &t5476_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5476_m28696_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28696_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t255_0_0_0;
static ParameterInfo t5476_m28697_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t255_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28697_GM;
MethodInfo m28697_MI = 
{
	"Insert", NULL, &t5476_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5476_m28697_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28697_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5476_m28698_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28698_GM;
MethodInfo m28698_MI = 
{
	"RemoveAt", NULL, &t5476_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5476_m28698_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28698_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5476_m28694_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t255_0_0_0;
extern void* RuntimeInvoker_t255_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28694_GM;
MethodInfo m28694_MI = 
{
	"get_Item", NULL, &t5476_TI, &t255_0_0_0, RuntimeInvoker_t255_t44, t5476_m28694_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28694_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t255_0_0_0;
static ParameterInfo t5476_m28695_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t255_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28695_GM;
MethodInfo m28695_MI = 
{
	"set_Item", NULL, &t5476_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5476_m28695_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28695_GM};
static MethodInfo* t5476_MIs[] =
{
	&m28696_MI,
	&m28697_MI,
	&m28698_MI,
	&m28694_MI,
	&m28695_MI,
	NULL
};
static TypeInfo* t5476_ITIs[] = 
{
	&t603_TI,
	&t5475_TI,
	&t5477_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5476_0_0_0;
extern Il2CppType t5476_1_0_0;
struct t5476;
extern Il2CppGenericClass t5476_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5476_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5476_MIs, t5476_PIs, NULL, NULL, NULL, NULL, NULL, &t5476_TI, t5476_ITIs, NULL, &t1908__CustomAttributeCache, &t5476_TI, &t5476_0_0_0, &t5476_1_0_0, NULL, &t5476_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4290_TI;

#include "t256.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.GridLayoutGroup/Axis>
extern MethodInfo m28699_MI;
static PropertyInfo t4290____Current_PropertyInfo = 
{
	&t4290_TI, "Current", &m28699_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4290_PIs[] =
{
	&t4290____Current_PropertyInfo,
	NULL
};
extern Il2CppType t256_0_0_0;
extern void* RuntimeInvoker_t256 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28699_GM;
MethodInfo m28699_MI = 
{
	"get_Current", NULL, &t4290_TI, &t256_0_0_0, RuntimeInvoker_t256, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28699_GM};
static MethodInfo* t4290_MIs[] =
{
	&m28699_MI,
	NULL
};
static TypeInfo* t4290_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4290_0_0_0;
extern Il2CppType t4290_1_0_0;
struct t4290;
extern Il2CppGenericClass t4290_GC;
TypeInfo t4290_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4290_MIs, t4290_PIs, NULL, NULL, NULL, NULL, NULL, &t4290_TI, t4290_ITIs, NULL, &EmptyCustomAttributesCache, &t4290_TI, &t4290_0_0_0, &t4290_1_0_0, NULL, &t4290_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2759.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2759_TI;
#include "t2759MD.h"

extern TypeInfo t256_TI;
extern MethodInfo m15108_MI;
extern MethodInfo m21727_MI;
struct t20;
 int32_t m21727 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15104_MI;
 void m15104 (t2759 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15105_MI;
 t29 * m15105 (t2759 * __this, MethodInfo* method){
	{
		int32_t L_0 = m15108(__this, &m15108_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t256_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15106_MI;
 void m15106 (t2759 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15107_MI;
 bool m15107 (t2759 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m15108 (t2759 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21727(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21727_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.GridLayoutGroup/Axis>
extern Il2CppType t20_0_0_1;
FieldInfo t2759_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2759_TI, offsetof(t2759, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2759_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2759_TI, offsetof(t2759, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2759_FIs[] =
{
	&t2759_f0_FieldInfo,
	&t2759_f1_FieldInfo,
	NULL
};
static PropertyInfo t2759____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2759_TI, "System.Collections.IEnumerator.Current", &m15105_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2759____Current_PropertyInfo = 
{
	&t2759_TI, "Current", &m15108_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2759_PIs[] =
{
	&t2759____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2759____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2759_m15104_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15104_GM;
MethodInfo m15104_MI = 
{
	".ctor", (methodPointerType)&m15104, &t2759_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2759_m15104_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15104_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15105_GM;
MethodInfo m15105_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15105, &t2759_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15105_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15106_GM;
MethodInfo m15106_MI = 
{
	"Dispose", (methodPointerType)&m15106, &t2759_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15106_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15107_GM;
MethodInfo m15107_MI = 
{
	"MoveNext", (methodPointerType)&m15107, &t2759_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15107_GM};
extern Il2CppType t256_0_0_0;
extern void* RuntimeInvoker_t256 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15108_GM;
MethodInfo m15108_MI = 
{
	"get_Current", (methodPointerType)&m15108, &t2759_TI, &t256_0_0_0, RuntimeInvoker_t256, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15108_GM};
static MethodInfo* t2759_MIs[] =
{
	&m15104_MI,
	&m15105_MI,
	&m15106_MI,
	&m15107_MI,
	&m15108_MI,
	NULL
};
static MethodInfo* t2759_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15105_MI,
	&m15107_MI,
	&m15106_MI,
	&m15108_MI,
};
static TypeInfo* t2759_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4290_TI,
};
static Il2CppInterfaceOffsetPair t2759_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4290_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2759_0_0_0;
extern Il2CppType t2759_1_0_0;
extern Il2CppGenericClass t2759_GC;
TypeInfo t2759_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2759_MIs, t2759_PIs, t2759_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2759_TI, t2759_ITIs, t2759_VT, &EmptyCustomAttributesCache, &t2759_TI, &t2759_0_0_0, &t2759_1_0_0, t2759_IOs, &t2759_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2759)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5478_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.GridLayoutGroup/Axis>
extern MethodInfo m28700_MI;
static PropertyInfo t5478____Count_PropertyInfo = 
{
	&t5478_TI, "Count", &m28700_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28701_MI;
static PropertyInfo t5478____IsReadOnly_PropertyInfo = 
{
	&t5478_TI, "IsReadOnly", &m28701_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5478_PIs[] =
{
	&t5478____Count_PropertyInfo,
	&t5478____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28700_GM;
MethodInfo m28700_MI = 
{
	"get_Count", NULL, &t5478_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28700_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28701_GM;
MethodInfo m28701_MI = 
{
	"get_IsReadOnly", NULL, &t5478_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28701_GM};
extern Il2CppType t256_0_0_0;
extern Il2CppType t256_0_0_0;
static ParameterInfo t5478_m28702_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t256_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28702_GM;
MethodInfo m28702_MI = 
{
	"Add", NULL, &t5478_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5478_m28702_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28702_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28703_GM;
MethodInfo m28703_MI = 
{
	"Clear", NULL, &t5478_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28703_GM};
extern Il2CppType t256_0_0_0;
static ParameterInfo t5478_m28704_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t256_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28704_GM;
MethodInfo m28704_MI = 
{
	"Contains", NULL, &t5478_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5478_m28704_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28704_GM};
extern Il2CppType t3879_0_0_0;
extern Il2CppType t3879_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5478_m28705_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3879_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28705_GM;
MethodInfo m28705_MI = 
{
	"CopyTo", NULL, &t5478_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5478_m28705_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28705_GM};
extern Il2CppType t256_0_0_0;
static ParameterInfo t5478_m28706_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t256_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28706_GM;
MethodInfo m28706_MI = 
{
	"Remove", NULL, &t5478_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5478_m28706_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28706_GM};
static MethodInfo* t5478_MIs[] =
{
	&m28700_MI,
	&m28701_MI,
	&m28702_MI,
	&m28703_MI,
	&m28704_MI,
	&m28705_MI,
	&m28706_MI,
	NULL
};
extern TypeInfo t5480_TI;
static TypeInfo* t5478_ITIs[] = 
{
	&t603_TI,
	&t5480_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5478_0_0_0;
extern Il2CppType t5478_1_0_0;
struct t5478;
extern Il2CppGenericClass t5478_GC;
TypeInfo t5478_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5478_MIs, t5478_PIs, NULL, NULL, NULL, NULL, NULL, &t5478_TI, t5478_ITIs, NULL, &EmptyCustomAttributesCache, &t5478_TI, &t5478_0_0_0, &t5478_1_0_0, NULL, &t5478_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.GridLayoutGroup/Axis>
extern Il2CppType t4290_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28707_GM;
MethodInfo m28707_MI = 
{
	"GetEnumerator", NULL, &t5480_TI, &t4290_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28707_GM};
static MethodInfo* t5480_MIs[] =
{
	&m28707_MI,
	NULL
};
static TypeInfo* t5480_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5480_0_0_0;
extern Il2CppType t5480_1_0_0;
struct t5480;
extern Il2CppGenericClass t5480_GC;
TypeInfo t5480_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5480_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5480_TI, t5480_ITIs, NULL, &EmptyCustomAttributesCache, &t5480_TI, &t5480_0_0_0, &t5480_1_0_0, NULL, &t5480_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5479_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.GridLayoutGroup/Axis>
extern MethodInfo m28708_MI;
extern MethodInfo m28709_MI;
static PropertyInfo t5479____Item_PropertyInfo = 
{
	&t5479_TI, "Item", &m28708_MI, &m28709_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5479_PIs[] =
{
	&t5479____Item_PropertyInfo,
	NULL
};
extern Il2CppType t256_0_0_0;
static ParameterInfo t5479_m28710_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t256_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28710_GM;
MethodInfo m28710_MI = 
{
	"IndexOf", NULL, &t5479_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5479_m28710_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28710_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t256_0_0_0;
static ParameterInfo t5479_m28711_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t256_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28711_GM;
MethodInfo m28711_MI = 
{
	"Insert", NULL, &t5479_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5479_m28711_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28711_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5479_m28712_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28712_GM;
MethodInfo m28712_MI = 
{
	"RemoveAt", NULL, &t5479_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5479_m28712_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28712_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5479_m28708_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t256_0_0_0;
extern void* RuntimeInvoker_t256_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28708_GM;
MethodInfo m28708_MI = 
{
	"get_Item", NULL, &t5479_TI, &t256_0_0_0, RuntimeInvoker_t256_t44, t5479_m28708_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28708_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t256_0_0_0;
static ParameterInfo t5479_m28709_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t256_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28709_GM;
MethodInfo m28709_MI = 
{
	"set_Item", NULL, &t5479_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5479_m28709_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28709_GM};
static MethodInfo* t5479_MIs[] =
{
	&m28710_MI,
	&m28711_MI,
	&m28712_MI,
	&m28708_MI,
	&m28709_MI,
	NULL
};
static TypeInfo* t5479_ITIs[] = 
{
	&t603_TI,
	&t5478_TI,
	&t5480_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5479_0_0_0;
extern Il2CppType t5479_1_0_0;
struct t5479;
extern Il2CppGenericClass t5479_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5479_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5479_MIs, t5479_PIs, NULL, NULL, NULL, NULL, NULL, &t5479_TI, t5479_ITIs, NULL, &t1908__CustomAttributeCache, &t5479_TI, &t5479_0_0_0, &t5479_1_0_0, NULL, &t5479_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4291_TI;

#include "t257.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.GridLayoutGroup/Constraint>
extern MethodInfo m28713_MI;
static PropertyInfo t4291____Current_PropertyInfo = 
{
	&t4291_TI, "Current", &m28713_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4291_PIs[] =
{
	&t4291____Current_PropertyInfo,
	NULL
};
extern Il2CppType t257_0_0_0;
extern void* RuntimeInvoker_t257 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28713_GM;
MethodInfo m28713_MI = 
{
	"get_Current", NULL, &t4291_TI, &t257_0_0_0, RuntimeInvoker_t257, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28713_GM};
static MethodInfo* t4291_MIs[] =
{
	&m28713_MI,
	NULL
};
static TypeInfo* t4291_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4291_0_0_0;
extern Il2CppType t4291_1_0_0;
struct t4291;
extern Il2CppGenericClass t4291_GC;
TypeInfo t4291_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4291_MIs, t4291_PIs, NULL, NULL, NULL, NULL, NULL, &t4291_TI, t4291_ITIs, NULL, &EmptyCustomAttributesCache, &t4291_TI, &t4291_0_0_0, &t4291_1_0_0, NULL, &t4291_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2760.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2760_TI;
#include "t2760MD.h"

extern TypeInfo t257_TI;
extern MethodInfo m15113_MI;
extern MethodInfo m21738_MI;
struct t20;
 int32_t m21738 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m15109_MI;
 void m15109 (t2760 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15110_MI;
 t29 * m15110 (t2760 * __this, MethodInfo* method){
	{
		int32_t L_0 = m15113(__this, &m15113_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t257_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m15111_MI;
 void m15111 (t2760 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m15112_MI;
 bool m15112 (t2760 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m15113 (t2760 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21738(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21738_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.GridLayoutGroup/Constraint>
extern Il2CppType t20_0_0_1;
FieldInfo t2760_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2760_TI, offsetof(t2760, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2760_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2760_TI, offsetof(t2760, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2760_FIs[] =
{
	&t2760_f0_FieldInfo,
	&t2760_f1_FieldInfo,
	NULL
};
static PropertyInfo t2760____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2760_TI, "System.Collections.IEnumerator.Current", &m15110_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2760____Current_PropertyInfo = 
{
	&t2760_TI, "Current", &m15113_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2760_PIs[] =
{
	&t2760____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2760____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2760_m15109_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15109_GM;
MethodInfo m15109_MI = 
{
	".ctor", (methodPointerType)&m15109, &t2760_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2760_m15109_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15109_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15110_GM;
MethodInfo m15110_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m15110, &t2760_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15110_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15111_GM;
MethodInfo m15111_MI = 
{
	"Dispose", (methodPointerType)&m15111, &t2760_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15111_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15112_GM;
MethodInfo m15112_MI = 
{
	"MoveNext", (methodPointerType)&m15112, &t2760_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15112_GM};
extern Il2CppType t257_0_0_0;
extern void* RuntimeInvoker_t257 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15113_GM;
MethodInfo m15113_MI = 
{
	"get_Current", (methodPointerType)&m15113, &t2760_TI, &t257_0_0_0, RuntimeInvoker_t257, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15113_GM};
static MethodInfo* t2760_MIs[] =
{
	&m15109_MI,
	&m15110_MI,
	&m15111_MI,
	&m15112_MI,
	&m15113_MI,
	NULL
};
static MethodInfo* t2760_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15110_MI,
	&m15112_MI,
	&m15111_MI,
	&m15113_MI,
};
static TypeInfo* t2760_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4291_TI,
};
static Il2CppInterfaceOffsetPair t2760_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4291_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2760_0_0_0;
extern Il2CppType t2760_1_0_0;
extern Il2CppGenericClass t2760_GC;
TypeInfo t2760_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2760_MIs, t2760_PIs, t2760_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2760_TI, t2760_ITIs, t2760_VT, &EmptyCustomAttributesCache, &t2760_TI, &t2760_0_0_0, &t2760_1_0_0, t2760_IOs, &t2760_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2760)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5481_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.GridLayoutGroup/Constraint>
extern MethodInfo m28714_MI;
static PropertyInfo t5481____Count_PropertyInfo = 
{
	&t5481_TI, "Count", &m28714_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28715_MI;
static PropertyInfo t5481____IsReadOnly_PropertyInfo = 
{
	&t5481_TI, "IsReadOnly", &m28715_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5481_PIs[] =
{
	&t5481____Count_PropertyInfo,
	&t5481____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28714_GM;
MethodInfo m28714_MI = 
{
	"get_Count", NULL, &t5481_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28714_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28715_GM;
MethodInfo m28715_MI = 
{
	"get_IsReadOnly", NULL, &t5481_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28715_GM};
extern Il2CppType t257_0_0_0;
extern Il2CppType t257_0_0_0;
static ParameterInfo t5481_m28716_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t257_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28716_GM;
MethodInfo m28716_MI = 
{
	"Add", NULL, &t5481_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5481_m28716_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28716_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28717_GM;
MethodInfo m28717_MI = 
{
	"Clear", NULL, &t5481_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28717_GM};
extern Il2CppType t257_0_0_0;
static ParameterInfo t5481_m28718_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t257_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28718_GM;
MethodInfo m28718_MI = 
{
	"Contains", NULL, &t5481_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5481_m28718_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28718_GM};
extern Il2CppType t3880_0_0_0;
extern Il2CppType t3880_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5481_m28719_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3880_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28719_GM;
MethodInfo m28719_MI = 
{
	"CopyTo", NULL, &t5481_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5481_m28719_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28719_GM};
extern Il2CppType t257_0_0_0;
static ParameterInfo t5481_m28720_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t257_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28720_GM;
MethodInfo m28720_MI = 
{
	"Remove", NULL, &t5481_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5481_m28720_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28720_GM};
static MethodInfo* t5481_MIs[] =
{
	&m28714_MI,
	&m28715_MI,
	&m28716_MI,
	&m28717_MI,
	&m28718_MI,
	&m28719_MI,
	&m28720_MI,
	NULL
};
extern TypeInfo t5483_TI;
static TypeInfo* t5481_ITIs[] = 
{
	&t603_TI,
	&t5483_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5481_0_0_0;
extern Il2CppType t5481_1_0_0;
struct t5481;
extern Il2CppGenericClass t5481_GC;
TypeInfo t5481_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5481_MIs, t5481_PIs, NULL, NULL, NULL, NULL, NULL, &t5481_TI, t5481_ITIs, NULL, &EmptyCustomAttributesCache, &t5481_TI, &t5481_0_0_0, &t5481_1_0_0, NULL, &t5481_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.GridLayoutGroup/Constraint>
extern Il2CppType t4291_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28721_GM;
MethodInfo m28721_MI = 
{
	"GetEnumerator", NULL, &t5483_TI, &t4291_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28721_GM};
static MethodInfo* t5483_MIs[] =
{
	&m28721_MI,
	NULL
};
static TypeInfo* t5483_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5483_0_0_0;
extern Il2CppType t5483_1_0_0;
struct t5483;
extern Il2CppGenericClass t5483_GC;
TypeInfo t5483_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5483_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5483_TI, t5483_ITIs, NULL, &EmptyCustomAttributesCache, &t5483_TI, &t5483_0_0_0, &t5483_1_0_0, NULL, &t5483_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5482_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.GridLayoutGroup/Constraint>
extern MethodInfo m28722_MI;
extern MethodInfo m28723_MI;
static PropertyInfo t5482____Item_PropertyInfo = 
{
	&t5482_TI, "Item", &m28722_MI, &m28723_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5482_PIs[] =
{
	&t5482____Item_PropertyInfo,
	NULL
};
extern Il2CppType t257_0_0_0;
static ParameterInfo t5482_m28724_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t257_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28724_GM;
MethodInfo m28724_MI = 
{
	"IndexOf", NULL, &t5482_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5482_m28724_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28724_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t257_0_0_0;
static ParameterInfo t5482_m28725_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t257_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28725_GM;
MethodInfo m28725_MI = 
{
	"Insert", NULL, &t5482_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5482_m28725_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28725_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5482_m28726_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28726_GM;
MethodInfo m28726_MI = 
{
	"RemoveAt", NULL, &t5482_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5482_m28726_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28726_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5482_m28722_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t257_0_0_0;
extern void* RuntimeInvoker_t257_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28722_GM;
MethodInfo m28722_MI = 
{
	"get_Item", NULL, &t5482_TI, &t257_0_0_0, RuntimeInvoker_t257_t44, t5482_m28722_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28722_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t257_0_0_0;
static ParameterInfo t5482_m28723_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t257_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28723_GM;
MethodInfo m28723_MI = 
{
	"set_Item", NULL, &t5482_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5482_m28723_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28723_GM};
static MethodInfo* t5482_MIs[] =
{
	&m28724_MI,
	&m28725_MI,
	&m28726_MI,
	&m28722_MI,
	&m28723_MI,
	NULL
};
static TypeInfo* t5482_ITIs[] = 
{
	&t603_TI,
	&t5481_TI,
	&t5483_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5482_0_0_0;
extern Il2CppType t5482_1_0_0;
struct t5482;
extern Il2CppGenericClass t5482_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5482_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5482_MIs, t5482_PIs, NULL, NULL, NULL, NULL, NULL, &t5482_TI, t5482_ITIs, NULL, &t1908__CustomAttributeCache, &t5482_TI, &t5482_0_0_0, &t5482_1_0_0, NULL, &t5482_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4293_TI;

#include "t260.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.HorizontalLayoutGroup>
extern MethodInfo m28727_MI;
static PropertyInfo t4293____Current_PropertyInfo = 
{
	&t4293_TI, "Current", &m28727_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4293_PIs[] =
{
	&t4293____Current_PropertyInfo,
	NULL
};
extern Il2CppType t260_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28727_GM;
MethodInfo m28727_MI = 
{
	"get_Current", NULL, &t4293_TI, &t260_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28727_GM};
static MethodInfo* t4293_MIs[] =
{
	&m28727_MI,
	NULL
};
static TypeInfo* t4293_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4293_0_0_0;
extern Il2CppType t4293_1_0_0;
struct t4293;
extern Il2CppGenericClass t4293_GC;
TypeInfo t4293_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4293_MIs, t4293_PIs, NULL, NULL, NULL, NULL, NULL, &t4293_TI, t4293_ITIs, NULL, &EmptyCustomAttributesCache, &t4293_TI, &t4293_0_0_0, &t4293_1_0_0, NULL, &t4293_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2761.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2761_TI;
#include "t2761MD.h"

extern TypeInfo t260_TI;
extern MethodInfo m15118_MI;
extern MethodInfo m21749_MI;
struct t20;
#define m21749(__this, p0, method) (t260 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.HorizontalLayoutGroup>
extern Il2CppType t20_0_0_1;
FieldInfo t2761_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2761_TI, offsetof(t2761, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2761_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2761_TI, offsetof(t2761, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2761_FIs[] =
{
	&t2761_f0_FieldInfo,
	&t2761_f1_FieldInfo,
	NULL
};
extern MethodInfo m15115_MI;
static PropertyInfo t2761____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2761_TI, "System.Collections.IEnumerator.Current", &m15115_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2761____Current_PropertyInfo = 
{
	&t2761_TI, "Current", &m15118_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2761_PIs[] =
{
	&t2761____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2761____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2761_m15114_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15114_GM;
MethodInfo m15114_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2761_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2761_m15114_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15114_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15115_GM;
MethodInfo m15115_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2761_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15115_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15116_GM;
MethodInfo m15116_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2761_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15116_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15117_GM;
MethodInfo m15117_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2761_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15117_GM};
extern Il2CppType t260_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15118_GM;
MethodInfo m15118_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2761_TI, &t260_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15118_GM};
static MethodInfo* t2761_MIs[] =
{
	&m15114_MI,
	&m15115_MI,
	&m15116_MI,
	&m15117_MI,
	&m15118_MI,
	NULL
};
extern MethodInfo m15117_MI;
extern MethodInfo m15116_MI;
static MethodInfo* t2761_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15115_MI,
	&m15117_MI,
	&m15116_MI,
	&m15118_MI,
};
static TypeInfo* t2761_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4293_TI,
};
static Il2CppInterfaceOffsetPair t2761_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4293_TI, 7},
};
extern TypeInfo t260_TI;
static Il2CppRGCTXData t2761_RGCTXData[3] = 
{
	&m15118_MI/* Method Usage */,
	&t260_TI/* Class Usage */,
	&m21749_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2761_0_0_0;
extern Il2CppType t2761_1_0_0;
extern Il2CppGenericClass t2761_GC;
TypeInfo t2761_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2761_MIs, t2761_PIs, t2761_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2761_TI, t2761_ITIs, t2761_VT, &EmptyCustomAttributesCache, &t2761_TI, &t2761_0_0_0, &t2761_1_0_0, t2761_IOs, &t2761_GC, NULL, NULL, NULL, t2761_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2761)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5484_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.HorizontalLayoutGroup>
extern MethodInfo m28728_MI;
static PropertyInfo t5484____Count_PropertyInfo = 
{
	&t5484_TI, "Count", &m28728_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28729_MI;
static PropertyInfo t5484____IsReadOnly_PropertyInfo = 
{
	&t5484_TI, "IsReadOnly", &m28729_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5484_PIs[] =
{
	&t5484____Count_PropertyInfo,
	&t5484____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28728_GM;
MethodInfo m28728_MI = 
{
	"get_Count", NULL, &t5484_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28728_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28729_GM;
MethodInfo m28729_MI = 
{
	"get_IsReadOnly", NULL, &t5484_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28729_GM};
extern Il2CppType t260_0_0_0;
extern Il2CppType t260_0_0_0;
static ParameterInfo t5484_m28730_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t260_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28730_GM;
MethodInfo m28730_MI = 
{
	"Add", NULL, &t5484_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5484_m28730_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28730_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28731_GM;
MethodInfo m28731_MI = 
{
	"Clear", NULL, &t5484_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28731_GM};
extern Il2CppType t260_0_0_0;
static ParameterInfo t5484_m28732_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t260_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28732_GM;
MethodInfo m28732_MI = 
{
	"Contains", NULL, &t5484_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5484_m28732_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28732_GM};
extern Il2CppType t3881_0_0_0;
extern Il2CppType t3881_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5484_m28733_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3881_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28733_GM;
MethodInfo m28733_MI = 
{
	"CopyTo", NULL, &t5484_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5484_m28733_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28733_GM};
extern Il2CppType t260_0_0_0;
static ParameterInfo t5484_m28734_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t260_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28734_GM;
MethodInfo m28734_MI = 
{
	"Remove", NULL, &t5484_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5484_m28734_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28734_GM};
static MethodInfo* t5484_MIs[] =
{
	&m28728_MI,
	&m28729_MI,
	&m28730_MI,
	&m28731_MI,
	&m28732_MI,
	&m28733_MI,
	&m28734_MI,
	NULL
};
extern TypeInfo t5486_TI;
static TypeInfo* t5484_ITIs[] = 
{
	&t603_TI,
	&t5486_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5484_0_0_0;
extern Il2CppType t5484_1_0_0;
struct t5484;
extern Il2CppGenericClass t5484_GC;
TypeInfo t5484_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5484_MIs, t5484_PIs, NULL, NULL, NULL, NULL, NULL, &t5484_TI, t5484_ITIs, NULL, &EmptyCustomAttributesCache, &t5484_TI, &t5484_0_0_0, &t5484_1_0_0, NULL, &t5484_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.HorizontalLayoutGroup>
extern Il2CppType t4293_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28735_GM;
MethodInfo m28735_MI = 
{
	"GetEnumerator", NULL, &t5486_TI, &t4293_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28735_GM};
static MethodInfo* t5486_MIs[] =
{
	&m28735_MI,
	NULL
};
static TypeInfo* t5486_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5486_0_0_0;
extern Il2CppType t5486_1_0_0;
struct t5486;
extern Il2CppGenericClass t5486_GC;
TypeInfo t5486_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5486_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5486_TI, t5486_ITIs, NULL, &EmptyCustomAttributesCache, &t5486_TI, &t5486_0_0_0, &t5486_1_0_0, NULL, &t5486_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5485_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.HorizontalLayoutGroup>
extern MethodInfo m28736_MI;
extern MethodInfo m28737_MI;
static PropertyInfo t5485____Item_PropertyInfo = 
{
	&t5485_TI, "Item", &m28736_MI, &m28737_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5485_PIs[] =
{
	&t5485____Item_PropertyInfo,
	NULL
};
extern Il2CppType t260_0_0_0;
static ParameterInfo t5485_m28738_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t260_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28738_GM;
MethodInfo m28738_MI = 
{
	"IndexOf", NULL, &t5485_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5485_m28738_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28738_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t260_0_0_0;
static ParameterInfo t5485_m28739_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t260_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28739_GM;
MethodInfo m28739_MI = 
{
	"Insert", NULL, &t5485_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5485_m28739_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28739_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5485_m28740_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28740_GM;
MethodInfo m28740_MI = 
{
	"RemoveAt", NULL, &t5485_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5485_m28740_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28740_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5485_m28736_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t260_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28736_GM;
MethodInfo m28736_MI = 
{
	"get_Item", NULL, &t5485_TI, &t260_0_0_0, RuntimeInvoker_t29_t44, t5485_m28736_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28736_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t260_0_0_0;
static ParameterInfo t5485_m28737_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t260_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28737_GM;
MethodInfo m28737_MI = 
{
	"set_Item", NULL, &t5485_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5485_m28737_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28737_GM};
static MethodInfo* t5485_MIs[] =
{
	&m28738_MI,
	&m28739_MI,
	&m28740_MI,
	&m28736_MI,
	&m28737_MI,
	NULL
};
static TypeInfo* t5485_ITIs[] = 
{
	&t603_TI,
	&t5484_TI,
	&t5486_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5485_0_0_0;
extern Il2CppType t5485_1_0_0;
struct t5485;
extern Il2CppGenericClass t5485_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5485_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5485_MIs, t5485_PIs, NULL, NULL, NULL, NULL, NULL, &t5485_TI, t5485_ITIs, NULL, &t1908__CustomAttributeCache, &t5485_TI, &t5485_0_0_0, &t5485_1_0_0, NULL, &t5485_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5487_TI;

#include "t261.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>
extern MethodInfo m28741_MI;
static PropertyInfo t5487____Count_PropertyInfo = 
{
	&t5487_TI, "Count", &m28741_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28742_MI;
static PropertyInfo t5487____IsReadOnly_PropertyInfo = 
{
	&t5487_TI, "IsReadOnly", &m28742_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5487_PIs[] =
{
	&t5487____Count_PropertyInfo,
	&t5487____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28741_GM;
MethodInfo m28741_MI = 
{
	"get_Count", NULL, &t5487_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28741_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28742_GM;
MethodInfo m28742_MI = 
{
	"get_IsReadOnly", NULL, &t5487_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28742_GM};
extern Il2CppType t261_0_0_0;
extern Il2CppType t261_0_0_0;
static ParameterInfo t5487_m28743_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t261_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28743_GM;
MethodInfo m28743_MI = 
{
	"Add", NULL, &t5487_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5487_m28743_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28743_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28744_GM;
MethodInfo m28744_MI = 
{
	"Clear", NULL, &t5487_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28744_GM};
extern Il2CppType t261_0_0_0;
static ParameterInfo t5487_m28745_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t261_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28745_GM;
MethodInfo m28745_MI = 
{
	"Contains", NULL, &t5487_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5487_m28745_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28745_GM};
extern Il2CppType t3882_0_0_0;
extern Il2CppType t3882_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5487_m28746_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3882_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28746_GM;
MethodInfo m28746_MI = 
{
	"CopyTo", NULL, &t5487_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5487_m28746_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28746_GM};
extern Il2CppType t261_0_0_0;
static ParameterInfo t5487_m28747_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t261_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28747_GM;
MethodInfo m28747_MI = 
{
	"Remove", NULL, &t5487_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5487_m28747_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28747_GM};
static MethodInfo* t5487_MIs[] =
{
	&m28741_MI,
	&m28742_MI,
	&m28743_MI,
	&m28744_MI,
	&m28745_MI,
	&m28746_MI,
	&m28747_MI,
	NULL
};
extern TypeInfo t5489_TI;
static TypeInfo* t5487_ITIs[] = 
{
	&t603_TI,
	&t5489_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5487_0_0_0;
extern Il2CppType t5487_1_0_0;
struct t5487;
extern Il2CppGenericClass t5487_GC;
TypeInfo t5487_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5487_MIs, t5487_PIs, NULL, NULL, NULL, NULL, NULL, &t5487_TI, t5487_ITIs, NULL, &EmptyCustomAttributesCache, &t5487_TI, &t5487_0_0_0, &t5487_1_0_0, NULL, &t5487_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>
extern Il2CppType t4295_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28748_GM;
MethodInfo m28748_MI = 
{
	"GetEnumerator", NULL, &t5489_TI, &t4295_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28748_GM};
static MethodInfo* t5489_MIs[] =
{
	&m28748_MI,
	NULL
};
static TypeInfo* t5489_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5489_0_0_0;
extern Il2CppType t5489_1_0_0;
struct t5489;
extern Il2CppGenericClass t5489_GC;
TypeInfo t5489_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5489_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5489_TI, t5489_ITIs, NULL, &EmptyCustomAttributesCache, &t5489_TI, &t5489_0_0_0, &t5489_1_0_0, NULL, &t5489_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4295_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>
extern MethodInfo m28749_MI;
static PropertyInfo t4295____Current_PropertyInfo = 
{
	&t4295_TI, "Current", &m28749_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4295_PIs[] =
{
	&t4295____Current_PropertyInfo,
	NULL
};
extern Il2CppType t261_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28749_GM;
MethodInfo m28749_MI = 
{
	"get_Current", NULL, &t4295_TI, &t261_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28749_GM};
static MethodInfo* t4295_MIs[] =
{
	&m28749_MI,
	NULL
};
static TypeInfo* t4295_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4295_0_0_0;
extern Il2CppType t4295_1_0_0;
struct t4295;
extern Il2CppGenericClass t4295_GC;
TypeInfo t4295_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4295_MIs, t4295_PIs, NULL, NULL, NULL, NULL, NULL, &t4295_TI, t4295_ITIs, NULL, &EmptyCustomAttributesCache, &t4295_TI, &t4295_0_0_0, &t4295_1_0_0, NULL, &t4295_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2762.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2762_TI;
#include "t2762MD.h"

extern TypeInfo t261_TI;
extern MethodInfo m15123_MI;
extern MethodInfo m21760_MI;
struct t20;
#define m21760(__this, p0, method) (t261 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>
extern Il2CppType t20_0_0_1;
FieldInfo t2762_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2762_TI, offsetof(t2762, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2762_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2762_TI, offsetof(t2762, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2762_FIs[] =
{
	&t2762_f0_FieldInfo,
	&t2762_f1_FieldInfo,
	NULL
};
extern MethodInfo m15120_MI;
static PropertyInfo t2762____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2762_TI, "System.Collections.IEnumerator.Current", &m15120_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2762____Current_PropertyInfo = 
{
	&t2762_TI, "Current", &m15123_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2762_PIs[] =
{
	&t2762____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2762____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2762_m15119_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15119_GM;
MethodInfo m15119_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2762_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2762_m15119_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15119_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15120_GM;
MethodInfo m15120_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2762_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15120_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15121_GM;
MethodInfo m15121_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2762_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15121_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15122_GM;
MethodInfo m15122_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2762_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15122_GM};
extern Il2CppType t261_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15123_GM;
MethodInfo m15123_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2762_TI, &t261_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15123_GM};
static MethodInfo* t2762_MIs[] =
{
	&m15119_MI,
	&m15120_MI,
	&m15121_MI,
	&m15122_MI,
	&m15123_MI,
	NULL
};
extern MethodInfo m15122_MI;
extern MethodInfo m15121_MI;
static MethodInfo* t2762_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15120_MI,
	&m15122_MI,
	&m15121_MI,
	&m15123_MI,
};
static TypeInfo* t2762_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4295_TI,
};
static Il2CppInterfaceOffsetPair t2762_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4295_TI, 7},
};
extern TypeInfo t261_TI;
static Il2CppRGCTXData t2762_RGCTXData[3] = 
{
	&m15123_MI/* Method Usage */,
	&t261_TI/* Class Usage */,
	&m21760_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2762_0_0_0;
extern Il2CppType t2762_1_0_0;
extern Il2CppGenericClass t2762_GC;
TypeInfo t2762_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2762_MIs, t2762_PIs, t2762_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2762_TI, t2762_ITIs, t2762_VT, &EmptyCustomAttributesCache, &t2762_TI, &t2762_0_0_0, &t2762_1_0_0, t2762_IOs, &t2762_GC, NULL, NULL, NULL, t2762_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2762)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5488_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>
extern MethodInfo m28750_MI;
extern MethodInfo m28751_MI;
static PropertyInfo t5488____Item_PropertyInfo = 
{
	&t5488_TI, "Item", &m28750_MI, &m28751_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5488_PIs[] =
{
	&t5488____Item_PropertyInfo,
	NULL
};
extern Il2CppType t261_0_0_0;
static ParameterInfo t5488_m28752_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t261_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28752_GM;
MethodInfo m28752_MI = 
{
	"IndexOf", NULL, &t5488_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5488_m28752_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28752_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t261_0_0_0;
static ParameterInfo t5488_m28753_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t261_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28753_GM;
MethodInfo m28753_MI = 
{
	"Insert", NULL, &t5488_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5488_m28753_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28753_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5488_m28754_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28754_GM;
MethodInfo m28754_MI = 
{
	"RemoveAt", NULL, &t5488_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5488_m28754_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28754_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5488_m28750_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t261_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28750_GM;
MethodInfo m28750_MI = 
{
	"get_Item", NULL, &t5488_TI, &t261_0_0_0, RuntimeInvoker_t29_t44, t5488_m28750_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28750_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t261_0_0_0;
static ParameterInfo t5488_m28751_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t261_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28751_GM;
MethodInfo m28751_MI = 
{
	"set_Item", NULL, &t5488_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5488_m28751_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28751_GM};
static MethodInfo* t5488_MIs[] =
{
	&m28752_MI,
	&m28753_MI,
	&m28754_MI,
	&m28750_MI,
	&m28751_MI,
	NULL
};
static TypeInfo* t5488_ITIs[] = 
{
	&t603_TI,
	&t5487_TI,
	&t5489_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5488_0_0_0;
extern Il2CppType t5488_1_0_0;
struct t5488;
extern Il2CppGenericClass t5488_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5488_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5488_MIs, t5488_PIs, NULL, NULL, NULL, NULL, NULL, &t5488_TI, t5488_ITIs, NULL, &t1908__CustomAttributeCache, &t5488_TI, &t5488_0_0_0, &t5488_1_0_0, NULL, &t5488_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2763.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2763_TI;
#include "t2763MD.h"

#include "t2764.h"
extern TypeInfo t2764_TI;
#include "t2764MD.h"
extern MethodInfo m15126_MI;
extern MethodInfo m15128_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.HorizontalLayoutGroup>
extern Il2CppType t316_0_0_33;
FieldInfo t2763_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2763_TI, offsetof(t2763, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2763_FIs[] =
{
	&t2763_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t260_0_0_0;
static ParameterInfo t2763_m15124_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t260_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15124_GM;
MethodInfo m15124_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2763_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2763_m15124_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15124_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2763_m15125_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15125_GM;
MethodInfo m15125_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2763_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2763_m15125_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15125_GM};
static MethodInfo* t2763_MIs[] =
{
	&m15124_MI,
	&m15125_MI,
	NULL
};
extern MethodInfo m15125_MI;
extern MethodInfo m15129_MI;
static MethodInfo* t2763_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15125_MI,
	&m15129_MI,
};
extern Il2CppType t2765_0_0_0;
extern TypeInfo t2765_TI;
extern MethodInfo m21770_MI;
extern TypeInfo t260_TI;
extern MethodInfo m15131_MI;
extern TypeInfo t260_TI;
static Il2CppRGCTXData t2763_RGCTXData[8] = 
{
	&t2765_0_0_0/* Type Usage */,
	&t2765_TI/* Class Usage */,
	&m21770_MI/* Method Usage */,
	&t260_TI/* Class Usage */,
	&m15131_MI/* Method Usage */,
	&m15126_MI/* Method Usage */,
	&t260_TI/* Class Usage */,
	&m15128_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2763_0_0_0;
extern Il2CppType t2763_1_0_0;
struct t2763;
extern Il2CppGenericClass t2763_GC;
TypeInfo t2763_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2763_MIs, NULL, t2763_FIs, NULL, &t2764_TI, NULL, NULL, &t2763_TI, NULL, t2763_VT, &EmptyCustomAttributesCache, &t2763_TI, &t2763_0_0_0, &t2763_1_0_0, NULL, &t2763_GC, NULL, NULL, NULL, t2763_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2763), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2765.h"
extern TypeInfo t2765_TI;
#include "t2765MD.h"
struct t556;
#define m21770(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.HorizontalLayoutGroup>
extern Il2CppType t2765_0_0_1;
FieldInfo t2764_f0_FieldInfo = 
{
	"Delegate", &t2765_0_0_1, &t2764_TI, offsetof(t2764, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2764_FIs[] =
{
	&t2764_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2764_m15126_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15126_GM;
MethodInfo m15126_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2764_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2764_m15126_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15126_GM};
extern Il2CppType t2765_0_0_0;
static ParameterInfo t2764_m15127_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2765_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15127_GM;
MethodInfo m15127_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2764_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2764_m15127_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15127_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2764_m15128_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15128_GM;
MethodInfo m15128_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2764_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2764_m15128_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15128_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2764_m15129_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15129_GM;
MethodInfo m15129_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2764_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2764_m15129_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15129_GM};
static MethodInfo* t2764_MIs[] =
{
	&m15126_MI,
	&m15127_MI,
	&m15128_MI,
	&m15129_MI,
	NULL
};
static MethodInfo* t2764_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15128_MI,
	&m15129_MI,
};
extern TypeInfo t2765_TI;
extern TypeInfo t260_TI;
static Il2CppRGCTXData t2764_RGCTXData[5] = 
{
	&t2765_0_0_0/* Type Usage */,
	&t2765_TI/* Class Usage */,
	&m21770_MI/* Method Usage */,
	&t260_TI/* Class Usage */,
	&m15131_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2764_0_0_0;
extern Il2CppType t2764_1_0_0;
struct t2764;
extern Il2CppGenericClass t2764_GC;
TypeInfo t2764_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2764_MIs, NULL, t2764_FIs, NULL, &t556_TI, NULL, NULL, &t2764_TI, NULL, t2764_VT, &EmptyCustomAttributesCache, &t2764_TI, &t2764_0_0_0, &t2764_1_0_0, NULL, &t2764_GC, NULL, NULL, NULL, t2764_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2764), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.HorizontalLayoutGroup>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2765_m15130_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15130_GM;
MethodInfo m15130_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2765_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2765_m15130_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15130_GM};
extern Il2CppType t260_0_0_0;
static ParameterInfo t2765_m15131_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t260_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15131_GM;
MethodInfo m15131_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2765_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2765_m15131_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15131_GM};
extern Il2CppType t260_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2765_m15132_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t260_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15132_GM;
MethodInfo m15132_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2765_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2765_m15132_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15132_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2765_m15133_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15133_GM;
MethodInfo m15133_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2765_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2765_m15133_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15133_GM};
static MethodInfo* t2765_MIs[] =
{
	&m15130_MI,
	&m15131_MI,
	&m15132_MI,
	&m15133_MI,
	NULL
};
extern MethodInfo m15132_MI;
extern MethodInfo m15133_MI;
static MethodInfo* t2765_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15131_MI,
	&m15132_MI,
	&m15133_MI,
};
static Il2CppInterfaceOffsetPair t2765_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2765_1_0_0;
struct t2765;
extern Il2CppGenericClass t2765_GC;
TypeInfo t2765_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2765_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2765_TI, NULL, t2765_VT, &EmptyCustomAttributesCache, &t2765_TI, &t2765_0_0_0, &t2765_1_0_0, t2765_IOs, &t2765_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2765), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2766.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2766_TI;
#include "t2766MD.h"

#include "t2767.h"
extern TypeInfo t2767_TI;
#include "t2767MD.h"
extern MethodInfo m15136_MI;
extern MethodInfo m15138_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>
extern Il2CppType t316_0_0_33;
FieldInfo t2766_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2766_TI, offsetof(t2766, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2766_FIs[] =
{
	&t2766_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t261_0_0_0;
static ParameterInfo t2766_m15134_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t261_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15134_GM;
MethodInfo m15134_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2766_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2766_m15134_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15134_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2766_m15135_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15135_GM;
MethodInfo m15135_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2766_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2766_m15135_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15135_GM};
static MethodInfo* t2766_MIs[] =
{
	&m15134_MI,
	&m15135_MI,
	NULL
};
extern MethodInfo m15135_MI;
extern MethodInfo m15139_MI;
static MethodInfo* t2766_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15135_MI,
	&m15139_MI,
};
extern Il2CppType t2768_0_0_0;
extern TypeInfo t2768_TI;
extern MethodInfo m21771_MI;
extern TypeInfo t261_TI;
extern MethodInfo m15141_MI;
extern TypeInfo t261_TI;
static Il2CppRGCTXData t2766_RGCTXData[8] = 
{
	&t2768_0_0_0/* Type Usage */,
	&t2768_TI/* Class Usage */,
	&m21771_MI/* Method Usage */,
	&t261_TI/* Class Usage */,
	&m15141_MI/* Method Usage */,
	&m15136_MI/* Method Usage */,
	&t261_TI/* Class Usage */,
	&m15138_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2766_0_0_0;
extern Il2CppType t2766_1_0_0;
struct t2766;
extern Il2CppGenericClass t2766_GC;
TypeInfo t2766_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2766_MIs, NULL, t2766_FIs, NULL, &t2767_TI, NULL, NULL, &t2766_TI, NULL, t2766_VT, &EmptyCustomAttributesCache, &t2766_TI, &t2766_0_0_0, &t2766_1_0_0, NULL, &t2766_GC, NULL, NULL, NULL, t2766_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2766), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2768.h"
extern TypeInfo t2768_TI;
#include "t2768MD.h"
struct t556;
#define m21771(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>
extern Il2CppType t2768_0_0_1;
FieldInfo t2767_f0_FieldInfo = 
{
	"Delegate", &t2768_0_0_1, &t2767_TI, offsetof(t2767, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2767_FIs[] =
{
	&t2767_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2767_m15136_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15136_GM;
MethodInfo m15136_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2767_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2767_m15136_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15136_GM};
extern Il2CppType t2768_0_0_0;
static ParameterInfo t2767_m15137_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2768_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15137_GM;
MethodInfo m15137_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2767_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2767_m15137_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15137_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2767_m15138_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15138_GM;
MethodInfo m15138_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2767_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2767_m15138_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15138_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2767_m15139_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15139_GM;
MethodInfo m15139_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2767_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2767_m15139_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15139_GM};
static MethodInfo* t2767_MIs[] =
{
	&m15136_MI,
	&m15137_MI,
	&m15138_MI,
	&m15139_MI,
	NULL
};
static MethodInfo* t2767_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15138_MI,
	&m15139_MI,
};
extern TypeInfo t2768_TI;
extern TypeInfo t261_TI;
static Il2CppRGCTXData t2767_RGCTXData[5] = 
{
	&t2768_0_0_0/* Type Usage */,
	&t2768_TI/* Class Usage */,
	&m21771_MI/* Method Usage */,
	&t261_TI/* Class Usage */,
	&m15141_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2767_0_0_0;
extern Il2CppType t2767_1_0_0;
struct t2767;
extern Il2CppGenericClass t2767_GC;
TypeInfo t2767_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2767_MIs, NULL, t2767_FIs, NULL, &t556_TI, NULL, NULL, &t2767_TI, NULL, t2767_VT, &EmptyCustomAttributesCache, &t2767_TI, &t2767_0_0_0, &t2767_1_0_0, NULL, &t2767_GC, NULL, NULL, NULL, t2767_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2767), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.HorizontalOrVerticalLayoutGroup>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2768_m15140_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15140_GM;
MethodInfo m15140_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2768_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2768_m15140_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15140_GM};
extern Il2CppType t261_0_0_0;
static ParameterInfo t2768_m15141_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t261_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15141_GM;
MethodInfo m15141_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2768_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2768_m15141_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15141_GM};
extern Il2CppType t261_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2768_m15142_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t261_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15142_GM;
MethodInfo m15142_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2768_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2768_m15142_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15142_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2768_m15143_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15143_GM;
MethodInfo m15143_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2768_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2768_m15143_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15143_GM};
static MethodInfo* t2768_MIs[] =
{
	&m15140_MI,
	&m15141_MI,
	&m15142_MI,
	&m15143_MI,
	NULL
};
extern MethodInfo m15142_MI;
extern MethodInfo m15143_MI;
static MethodInfo* t2768_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15141_MI,
	&m15142_MI,
	&m15143_MI,
};
static Il2CppInterfaceOffsetPair t2768_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2768_1_0_0;
struct t2768;
extern Il2CppGenericClass t2768_GC;
TypeInfo t2768_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2768_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2768_TI, NULL, t2768_VT, &EmptyCustomAttributesCache, &t2768_TI, &t2768_0_0_0, &t2768_1_0_0, t2768_IOs, &t2768_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2768), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4297_TI;

#include "t262.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.LayoutElement>
extern MethodInfo m28755_MI;
static PropertyInfo t4297____Current_PropertyInfo = 
{
	&t4297_TI, "Current", &m28755_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4297_PIs[] =
{
	&t4297____Current_PropertyInfo,
	NULL
};
extern Il2CppType t262_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28755_GM;
MethodInfo m28755_MI = 
{
	"get_Current", NULL, &t4297_TI, &t262_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28755_GM};
static MethodInfo* t4297_MIs[] =
{
	&m28755_MI,
	NULL
};
static TypeInfo* t4297_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4297_0_0_0;
extern Il2CppType t4297_1_0_0;
struct t4297;
extern Il2CppGenericClass t4297_GC;
TypeInfo t4297_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4297_MIs, t4297_PIs, NULL, NULL, NULL, NULL, NULL, &t4297_TI, t4297_ITIs, NULL, &EmptyCustomAttributesCache, &t4297_TI, &t4297_0_0_0, &t4297_1_0_0, NULL, &t4297_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2769.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2769_TI;
#include "t2769MD.h"

extern TypeInfo t262_TI;
extern MethodInfo m15148_MI;
extern MethodInfo m21773_MI;
struct t20;
#define m21773(__this, p0, method) (t262 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.LayoutElement>
extern Il2CppType t20_0_0_1;
FieldInfo t2769_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2769_TI, offsetof(t2769, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2769_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2769_TI, offsetof(t2769, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2769_FIs[] =
{
	&t2769_f0_FieldInfo,
	&t2769_f1_FieldInfo,
	NULL
};
extern MethodInfo m15145_MI;
static PropertyInfo t2769____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2769_TI, "System.Collections.IEnumerator.Current", &m15145_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2769____Current_PropertyInfo = 
{
	&t2769_TI, "Current", &m15148_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2769_PIs[] =
{
	&t2769____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2769____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2769_m15144_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15144_GM;
MethodInfo m15144_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2769_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2769_m15144_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15144_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15145_GM;
MethodInfo m15145_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2769_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15145_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15146_GM;
MethodInfo m15146_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2769_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15146_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15147_GM;
MethodInfo m15147_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2769_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15147_GM};
extern Il2CppType t262_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15148_GM;
MethodInfo m15148_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2769_TI, &t262_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15148_GM};
static MethodInfo* t2769_MIs[] =
{
	&m15144_MI,
	&m15145_MI,
	&m15146_MI,
	&m15147_MI,
	&m15148_MI,
	NULL
};
extern MethodInfo m15147_MI;
extern MethodInfo m15146_MI;
static MethodInfo* t2769_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15145_MI,
	&m15147_MI,
	&m15146_MI,
	&m15148_MI,
};
static TypeInfo* t2769_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4297_TI,
};
static Il2CppInterfaceOffsetPair t2769_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4297_TI, 7},
};
extern TypeInfo t262_TI;
static Il2CppRGCTXData t2769_RGCTXData[3] = 
{
	&m15148_MI/* Method Usage */,
	&t262_TI/* Class Usage */,
	&m21773_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2769_0_0_0;
extern Il2CppType t2769_1_0_0;
extern Il2CppGenericClass t2769_GC;
TypeInfo t2769_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2769_MIs, t2769_PIs, t2769_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2769_TI, t2769_ITIs, t2769_VT, &EmptyCustomAttributesCache, &t2769_TI, &t2769_0_0_0, &t2769_1_0_0, t2769_IOs, &t2769_GC, NULL, NULL, NULL, t2769_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2769)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5490_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.LayoutElement>
extern MethodInfo m28756_MI;
static PropertyInfo t5490____Count_PropertyInfo = 
{
	&t5490_TI, "Count", &m28756_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28757_MI;
static PropertyInfo t5490____IsReadOnly_PropertyInfo = 
{
	&t5490_TI, "IsReadOnly", &m28757_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5490_PIs[] =
{
	&t5490____Count_PropertyInfo,
	&t5490____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28756_GM;
MethodInfo m28756_MI = 
{
	"get_Count", NULL, &t5490_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28756_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28757_GM;
MethodInfo m28757_MI = 
{
	"get_IsReadOnly", NULL, &t5490_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28757_GM};
extern Il2CppType t262_0_0_0;
extern Il2CppType t262_0_0_0;
static ParameterInfo t5490_m28758_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t262_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28758_GM;
MethodInfo m28758_MI = 
{
	"Add", NULL, &t5490_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5490_m28758_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28758_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28759_GM;
MethodInfo m28759_MI = 
{
	"Clear", NULL, &t5490_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28759_GM};
extern Il2CppType t262_0_0_0;
static ParameterInfo t5490_m28760_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t262_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28760_GM;
MethodInfo m28760_MI = 
{
	"Contains", NULL, &t5490_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5490_m28760_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28760_GM};
extern Il2CppType t3883_0_0_0;
extern Il2CppType t3883_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5490_m28761_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3883_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28761_GM;
MethodInfo m28761_MI = 
{
	"CopyTo", NULL, &t5490_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5490_m28761_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28761_GM};
extern Il2CppType t262_0_0_0;
static ParameterInfo t5490_m28762_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t262_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28762_GM;
MethodInfo m28762_MI = 
{
	"Remove", NULL, &t5490_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5490_m28762_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28762_GM};
static MethodInfo* t5490_MIs[] =
{
	&m28756_MI,
	&m28757_MI,
	&m28758_MI,
	&m28759_MI,
	&m28760_MI,
	&m28761_MI,
	&m28762_MI,
	NULL
};
extern TypeInfo t5492_TI;
static TypeInfo* t5490_ITIs[] = 
{
	&t603_TI,
	&t5492_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5490_0_0_0;
extern Il2CppType t5490_1_0_0;
struct t5490;
extern Il2CppGenericClass t5490_GC;
TypeInfo t5490_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5490_MIs, t5490_PIs, NULL, NULL, NULL, NULL, NULL, &t5490_TI, t5490_ITIs, NULL, &EmptyCustomAttributesCache, &t5490_TI, &t5490_0_0_0, &t5490_1_0_0, NULL, &t5490_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.LayoutElement>
extern Il2CppType t4297_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28763_GM;
MethodInfo m28763_MI = 
{
	"GetEnumerator", NULL, &t5492_TI, &t4297_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28763_GM};
static MethodInfo* t5492_MIs[] =
{
	&m28763_MI,
	NULL
};
static TypeInfo* t5492_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5492_0_0_0;
extern Il2CppType t5492_1_0_0;
struct t5492;
extern Il2CppGenericClass t5492_GC;
TypeInfo t5492_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5492_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5492_TI, t5492_ITIs, NULL, &EmptyCustomAttributesCache, &t5492_TI, &t5492_0_0_0, &t5492_1_0_0, NULL, &t5492_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5491_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.LayoutElement>
extern MethodInfo m28764_MI;
extern MethodInfo m28765_MI;
static PropertyInfo t5491____Item_PropertyInfo = 
{
	&t5491_TI, "Item", &m28764_MI, &m28765_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5491_PIs[] =
{
	&t5491____Item_PropertyInfo,
	NULL
};
extern Il2CppType t262_0_0_0;
static ParameterInfo t5491_m28766_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t262_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28766_GM;
MethodInfo m28766_MI = 
{
	"IndexOf", NULL, &t5491_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5491_m28766_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28766_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t262_0_0_0;
static ParameterInfo t5491_m28767_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t262_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28767_GM;
MethodInfo m28767_MI = 
{
	"Insert", NULL, &t5491_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5491_m28767_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28767_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5491_m28768_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28768_GM;
MethodInfo m28768_MI = 
{
	"RemoveAt", NULL, &t5491_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5491_m28768_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28768_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5491_m28764_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t262_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28764_GM;
MethodInfo m28764_MI = 
{
	"get_Item", NULL, &t5491_TI, &t262_0_0_0, RuntimeInvoker_t29_t44, t5491_m28764_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28764_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t262_0_0_0;
static ParameterInfo t5491_m28765_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t262_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28765_GM;
MethodInfo m28765_MI = 
{
	"set_Item", NULL, &t5491_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5491_m28765_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28765_GM};
static MethodInfo* t5491_MIs[] =
{
	&m28766_MI,
	&m28767_MI,
	&m28768_MI,
	&m28764_MI,
	&m28765_MI,
	NULL
};
static TypeInfo* t5491_ITIs[] = 
{
	&t603_TI,
	&t5490_TI,
	&t5492_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5491_0_0_0;
extern Il2CppType t5491_1_0_0;
struct t5491;
extern Il2CppGenericClass t5491_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5491_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5491_MIs, t5491_PIs, NULL, NULL, NULL, NULL, NULL, &t5491_TI, t5491_ITIs, NULL, &t1908__CustomAttributeCache, &t5491_TI, &t5491_0_0_0, &t5491_1_0_0, NULL, &t5491_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5493_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.ILayoutIgnorer>
extern MethodInfo m28769_MI;
static PropertyInfo t5493____Count_PropertyInfo = 
{
	&t5493_TI, "Count", &m28769_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28770_MI;
static PropertyInfo t5493____IsReadOnly_PropertyInfo = 
{
	&t5493_TI, "IsReadOnly", &m28770_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5493_PIs[] =
{
	&t5493____Count_PropertyInfo,
	&t5493____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28769_GM;
MethodInfo m28769_MI = 
{
	"get_Count", NULL, &t5493_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28769_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28770_GM;
MethodInfo m28770_MI = 
{
	"get_IsReadOnly", NULL, &t5493_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28770_GM};
extern Il2CppType t412_0_0_0;
extern Il2CppType t412_0_0_0;
static ParameterInfo t5493_m28771_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t412_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28771_GM;
MethodInfo m28771_MI = 
{
	"Add", NULL, &t5493_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5493_m28771_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28771_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28772_GM;
MethodInfo m28772_MI = 
{
	"Clear", NULL, &t5493_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28772_GM};
extern Il2CppType t412_0_0_0;
static ParameterInfo t5493_m28773_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t412_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28773_GM;
MethodInfo m28773_MI = 
{
	"Contains", NULL, &t5493_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5493_m28773_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28773_GM};
extern Il2CppType t3884_0_0_0;
extern Il2CppType t3884_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5493_m28774_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3884_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28774_GM;
MethodInfo m28774_MI = 
{
	"CopyTo", NULL, &t5493_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5493_m28774_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28774_GM};
extern Il2CppType t412_0_0_0;
static ParameterInfo t5493_m28775_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t412_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28775_GM;
MethodInfo m28775_MI = 
{
	"Remove", NULL, &t5493_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5493_m28775_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28775_GM};
static MethodInfo* t5493_MIs[] =
{
	&m28769_MI,
	&m28770_MI,
	&m28771_MI,
	&m28772_MI,
	&m28773_MI,
	&m28774_MI,
	&m28775_MI,
	NULL
};
extern TypeInfo t5495_TI;
static TypeInfo* t5493_ITIs[] = 
{
	&t603_TI,
	&t5495_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5493_0_0_0;
extern Il2CppType t5493_1_0_0;
struct t5493;
extern Il2CppGenericClass t5493_GC;
TypeInfo t5493_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5493_MIs, t5493_PIs, NULL, NULL, NULL, NULL, NULL, &t5493_TI, t5493_ITIs, NULL, &EmptyCustomAttributesCache, &t5493_TI, &t5493_0_0_0, &t5493_1_0_0, NULL, &t5493_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.ILayoutIgnorer>
extern Il2CppType t4299_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28776_GM;
MethodInfo m28776_MI = 
{
	"GetEnumerator", NULL, &t5495_TI, &t4299_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28776_GM};
static MethodInfo* t5495_MIs[] =
{
	&m28776_MI,
	NULL
};
static TypeInfo* t5495_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5495_0_0_0;
extern Il2CppType t5495_1_0_0;
struct t5495;
extern Il2CppGenericClass t5495_GC;
TypeInfo t5495_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5495_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5495_TI, t5495_ITIs, NULL, &EmptyCustomAttributesCache, &t5495_TI, &t5495_0_0_0, &t5495_1_0_0, NULL, &t5495_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4299_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ILayoutIgnorer>
extern MethodInfo m28777_MI;
static PropertyInfo t4299____Current_PropertyInfo = 
{
	&t4299_TI, "Current", &m28777_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4299_PIs[] =
{
	&t4299____Current_PropertyInfo,
	NULL
};
extern Il2CppType t412_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28777_GM;
MethodInfo m28777_MI = 
{
	"get_Current", NULL, &t4299_TI, &t412_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28777_GM};
static MethodInfo* t4299_MIs[] =
{
	&m28777_MI,
	NULL
};
static TypeInfo* t4299_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4299_0_0_0;
extern Il2CppType t4299_1_0_0;
struct t4299;
extern Il2CppGenericClass t4299_GC;
TypeInfo t4299_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4299_MIs, t4299_PIs, NULL, NULL, NULL, NULL, NULL, &t4299_TI, t4299_ITIs, NULL, &EmptyCustomAttributesCache, &t4299_TI, &t4299_0_0_0, &t4299_1_0_0, NULL, &t4299_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2770.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2770_TI;
#include "t2770MD.h"

extern TypeInfo t412_TI;
extern MethodInfo m15153_MI;
extern MethodInfo m21784_MI;
struct t20;
#define m21784(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>
extern Il2CppType t20_0_0_1;
FieldInfo t2770_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2770_TI, offsetof(t2770, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2770_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2770_TI, offsetof(t2770, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2770_FIs[] =
{
	&t2770_f0_FieldInfo,
	&t2770_f1_FieldInfo,
	NULL
};
extern MethodInfo m15150_MI;
static PropertyInfo t2770____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2770_TI, "System.Collections.IEnumerator.Current", &m15150_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2770____Current_PropertyInfo = 
{
	&t2770_TI, "Current", &m15153_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2770_PIs[] =
{
	&t2770____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2770____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2770_m15149_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15149_GM;
MethodInfo m15149_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2770_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2770_m15149_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15149_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15150_GM;
MethodInfo m15150_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2770_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15150_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15151_GM;
MethodInfo m15151_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2770_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15151_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15152_GM;
MethodInfo m15152_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2770_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15152_GM};
extern Il2CppType t412_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15153_GM;
MethodInfo m15153_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2770_TI, &t412_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m15153_GM};
static MethodInfo* t2770_MIs[] =
{
	&m15149_MI,
	&m15150_MI,
	&m15151_MI,
	&m15152_MI,
	&m15153_MI,
	NULL
};
extern MethodInfo m15152_MI;
extern MethodInfo m15151_MI;
static MethodInfo* t2770_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m15150_MI,
	&m15152_MI,
	&m15151_MI,
	&m15153_MI,
};
static TypeInfo* t2770_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4299_TI,
};
static Il2CppInterfaceOffsetPair t2770_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4299_TI, 7},
};
extern TypeInfo t412_TI;
static Il2CppRGCTXData t2770_RGCTXData[3] = 
{
	&m15153_MI/* Method Usage */,
	&t412_TI/* Class Usage */,
	&m21784_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2770_0_0_0;
extern Il2CppType t2770_1_0_0;
extern Il2CppGenericClass t2770_GC;
TypeInfo t2770_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2770_MIs, t2770_PIs, t2770_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2770_TI, t2770_ITIs, t2770_VT, &EmptyCustomAttributesCache, &t2770_TI, &t2770_0_0_0, &t2770_1_0_0, t2770_IOs, &t2770_GC, NULL, NULL, NULL, t2770_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2770)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5494_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutIgnorer>
extern MethodInfo m28778_MI;
extern MethodInfo m28779_MI;
static PropertyInfo t5494____Item_PropertyInfo = 
{
	&t5494_TI, "Item", &m28778_MI, &m28779_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5494_PIs[] =
{
	&t5494____Item_PropertyInfo,
	NULL
};
extern Il2CppType t412_0_0_0;
static ParameterInfo t5494_m28780_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t412_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28780_GM;
MethodInfo m28780_MI = 
{
	"IndexOf", NULL, &t5494_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5494_m28780_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28780_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t412_0_0_0;
static ParameterInfo t5494_m28781_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t412_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28781_GM;
MethodInfo m28781_MI = 
{
	"Insert", NULL, &t5494_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5494_m28781_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28781_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5494_m28782_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28782_GM;
MethodInfo m28782_MI = 
{
	"RemoveAt", NULL, &t5494_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5494_m28782_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28782_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5494_m28778_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t412_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28778_GM;
MethodInfo m28778_MI = 
{
	"get_Item", NULL, &t5494_TI, &t412_0_0_0, RuntimeInvoker_t29_t44, t5494_m28778_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28778_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t412_0_0_0;
static ParameterInfo t5494_m28779_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t412_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28779_GM;
MethodInfo m28779_MI = 
{
	"set_Item", NULL, &t5494_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5494_m28779_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28779_GM};
static MethodInfo* t5494_MIs[] =
{
	&m28780_MI,
	&m28781_MI,
	&m28782_MI,
	&m28778_MI,
	&m28779_MI,
	NULL
};
static TypeInfo* t5494_ITIs[] = 
{
	&t603_TI,
	&t5493_TI,
	&t5495_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5494_0_0_0;
extern Il2CppType t5494_1_0_0;
struct t5494;
extern Il2CppGenericClass t5494_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5494_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5494_MIs, t5494_PIs, NULL, NULL, NULL, NULL, NULL, &t5494_TI, t5494_ITIs, NULL, &t1908__CustomAttributeCache, &t5494_TI, &t5494_0_0_0, &t5494_1_0_0, NULL, &t5494_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2771.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2771_TI;
#include "t2771MD.h"

#include "t2772.h"
extern TypeInfo t2772_TI;
#include "t2772MD.h"
extern MethodInfo m15156_MI;
extern MethodInfo m15158_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutElement>
extern Il2CppType t316_0_0_33;
FieldInfo t2771_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2771_TI, offsetof(t2771, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2771_FIs[] =
{
	&t2771_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t262_0_0_0;
static ParameterInfo t2771_m15154_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t262_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15154_GM;
MethodInfo m15154_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2771_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2771_m15154_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15154_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2771_m15155_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15155_GM;
MethodInfo m15155_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2771_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2771_m15155_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15155_GM};
static MethodInfo* t2771_MIs[] =
{
	&m15154_MI,
	&m15155_MI,
	NULL
};
extern MethodInfo m15155_MI;
extern MethodInfo m15159_MI;
static MethodInfo* t2771_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15155_MI,
	&m15159_MI,
};
extern Il2CppType t2773_0_0_0;
extern TypeInfo t2773_TI;
extern MethodInfo m21794_MI;
extern TypeInfo t262_TI;
extern MethodInfo m15161_MI;
extern TypeInfo t262_TI;
static Il2CppRGCTXData t2771_RGCTXData[8] = 
{
	&t2773_0_0_0/* Type Usage */,
	&t2773_TI/* Class Usage */,
	&m21794_MI/* Method Usage */,
	&t262_TI/* Class Usage */,
	&m15161_MI/* Method Usage */,
	&m15156_MI/* Method Usage */,
	&t262_TI/* Class Usage */,
	&m15158_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2771_0_0_0;
extern Il2CppType t2771_1_0_0;
struct t2771;
extern Il2CppGenericClass t2771_GC;
TypeInfo t2771_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2771_MIs, NULL, t2771_FIs, NULL, &t2772_TI, NULL, NULL, &t2771_TI, NULL, t2771_VT, &EmptyCustomAttributesCache, &t2771_TI, &t2771_0_0_0, &t2771_1_0_0, NULL, &t2771_GC, NULL, NULL, NULL, t2771_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2771), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2773.h"
extern TypeInfo t2773_TI;
#include "t2773MD.h"
struct t556;
#define m21794(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>
extern Il2CppType t2773_0_0_1;
FieldInfo t2772_f0_FieldInfo = 
{
	"Delegate", &t2773_0_0_1, &t2772_TI, offsetof(t2772, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2772_FIs[] =
{
	&t2772_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2772_m15156_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15156_GM;
MethodInfo m15156_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2772_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2772_m15156_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15156_GM};
extern Il2CppType t2773_0_0_0;
static ParameterInfo t2772_m15157_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2773_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15157_GM;
MethodInfo m15157_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2772_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2772_m15157_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15157_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2772_m15158_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15158_GM;
MethodInfo m15158_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2772_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2772_m15158_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15158_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2772_m15159_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15159_GM;
MethodInfo m15159_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2772_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2772_m15159_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15159_GM};
static MethodInfo* t2772_MIs[] =
{
	&m15156_MI,
	&m15157_MI,
	&m15158_MI,
	&m15159_MI,
	NULL
};
static MethodInfo* t2772_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m15158_MI,
	&m15159_MI,
};
extern TypeInfo t2773_TI;
extern TypeInfo t262_TI;
static Il2CppRGCTXData t2772_RGCTXData[5] = 
{
	&t2773_0_0_0/* Type Usage */,
	&t2773_TI/* Class Usage */,
	&m21794_MI/* Method Usage */,
	&t262_TI/* Class Usage */,
	&m15161_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2772_0_0_0;
extern Il2CppType t2772_1_0_0;
struct t2772;
extern Il2CppGenericClass t2772_GC;
TypeInfo t2772_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2772_MIs, NULL, t2772_FIs, NULL, &t556_TI, NULL, NULL, &t2772_TI, NULL, t2772_VT, &EmptyCustomAttributesCache, &t2772_TI, &t2772_0_0_0, &t2772_1_0_0, NULL, &t2772_GC, NULL, NULL, NULL, t2772_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2772), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutElement>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2773_m15160_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15160_GM;
MethodInfo m15160_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2773_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2773_m15160_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m15160_GM};
extern Il2CppType t262_0_0_0;
static ParameterInfo t2773_m15161_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t262_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15161_GM;
MethodInfo m15161_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2773_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2773_m15161_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15161_GM};
extern Il2CppType t262_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2773_m15162_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t262_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15162_GM;
MethodInfo m15162_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2773_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2773_m15162_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m15162_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2773_m15163_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m15163_GM;
MethodInfo m15163_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2773_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2773_m15163_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m15163_GM};
static MethodInfo* t2773_MIs[] =
{
	&m15160_MI,
	&m15161_MI,
	&m15162_MI,
	&m15163_MI,
	NULL
};
extern MethodInfo m15162_MI;
extern MethodInfo m15163_MI;
static MethodInfo* t2773_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m15161_MI,
	&m15162_MI,
	&m15163_MI,
};
static Il2CppInterfaceOffsetPair t2773_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2773_1_0_0;
struct t2773;
extern Il2CppGenericClass t2773_GC;
TypeInfo t2773_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2773_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2773_TI, NULL, t2773_VT, &EmptyCustomAttributesCache, &t2773_TI, &t2773_0_0_0, &t2773_1_0_0, t2773_IOs, &t2773_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2773), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
