﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1267;
struct t7;
struct t731;
struct t29;
struct t674;

 void m6629 (t1267 * __this, t731 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6630 (t1267 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6631 (t1267 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6632 (t1267 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6633 (t1267 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6634 (t1267 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6635 (t1267 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6636 (t1267 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6637 (t1267 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6638 (t1267 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
