﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3033;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t381.h"

 void m16657 (t3033 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16658 (t3033 * __this, t381  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16659 (t3033 * __this, t381  p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16660 (t3033 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
