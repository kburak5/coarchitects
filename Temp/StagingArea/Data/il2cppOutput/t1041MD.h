﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1041;
struct t7;
struct t762;
struct t745;

 void m4957 (t1041 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4958 (t1041 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4959 (t1041 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t762 * m4960 (t1041 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4961 (t1041 * __this, t762 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t745 * m4962 (t1041 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4963 (t1041 * __this, t745 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4964 (t1041 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
