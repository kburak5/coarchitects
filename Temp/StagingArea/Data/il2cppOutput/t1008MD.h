﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1008;
struct t809;
struct t7;
#include "t1007.h"

 void m4579 (t1008 * __this, t809 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4580 (t1008 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4581 (t1008 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4582 (t1008 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4583 (t1008 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
