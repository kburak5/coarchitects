﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2522;
struct t29;
#include "t184.h"

 void m13385 (t2522 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13386 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13387 (t2522 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2522 * m13388 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
