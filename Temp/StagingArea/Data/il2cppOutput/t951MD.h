﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t951;
struct t29;
struct t780;
struct t945;

 void m4508 (t951 * __this, t945 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4509 (t951 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4510 (t951 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t780 * m4190 (t951 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4192 (t951 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
