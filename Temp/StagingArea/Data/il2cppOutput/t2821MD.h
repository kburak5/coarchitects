﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2821;
struct t29;
struct t20;
#include "t447.h"

 void m15369 (t2821 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15370 (t2821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15371 (t2821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15372 (t2821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15373 (t2821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
