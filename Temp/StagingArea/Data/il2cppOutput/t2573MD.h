﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2573;
struct t29;
struct t364;

 void m13844 (t2573 * __this, t364 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13845 (t2573 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13846 (t2573 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13847 (t2573 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13848 (t2573 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
