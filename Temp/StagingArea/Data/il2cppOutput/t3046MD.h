﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3046;
struct t29;
struct t529;
#include "t380.h"

 void m16719 (t3046 * __this, t529 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16720 (t3046 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16721 (t3046 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16722 (t3046 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16723 (t3046 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t380  m16724 (t3046 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
