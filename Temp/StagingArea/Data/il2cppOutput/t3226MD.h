﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3226;
struct t29;

 void m17930 (t3226 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17931 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17932 (t3226 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3226 * m17933 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
