﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1038;
struct t7;
struct t781;
struct t1020;

 void m4686 (t1038 * __this, t7* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4687 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4688 (t1038 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4689 (t1038 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4690 (t1038 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4691 (t1038 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4692 (t1038 * __this, t1020 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4693 (t1038 * __this, t1020 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4694 (t1038 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4695 (t1038 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4696 (t1038 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4697 (t1038 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
