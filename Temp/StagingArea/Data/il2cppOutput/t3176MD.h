﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3176;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m17644_gshared (t3176 * __this, t29 * p0, t35 p1, MethodInfo* method);
#define m17644(__this, p0, p1, method) (void)m17644_gshared((t3176 *)__this, (t29 *)p0, (t35)p1, method)
 t29 * m17645_gshared (t3176 * __this, t29 * p0, MethodInfo* method);
#define m17645(__this, p0, method) (t29 *)m17645_gshared((t3176 *)__this, (t29 *)p0, method)
 t29 * m17646_gshared (t3176 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method);
#define m17646(__this, p0, p1, p2, method) (t29 *)m17646_gshared((t3176 *)__this, (t29 *)p0, (t67 *)p1, (t29 *)p2, method)
 t29 * m17647_gshared (t3176 * __this, t29 * p0, MethodInfo* method);
#define m17647(__this, p0, method) (t29 *)m17647_gshared((t3176 *)__this, (t29 *)p0, method)
