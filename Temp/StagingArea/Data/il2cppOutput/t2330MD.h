﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2330;
struct t29;
struct t20;
#include "t106.h"

 void m11988 (t2330 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m11989 (t2330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m11990 (t2330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m11991 (t2330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m11992 (t2330 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
