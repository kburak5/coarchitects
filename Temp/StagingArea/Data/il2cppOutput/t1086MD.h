﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1086;
struct t1086_marshaled;

void t1086_marshal(const t1086& unmarshaled, t1086_marshaled& marshaled);
void t1086_marshal_back(const t1086_marshaled& marshaled, t1086& unmarshaled);
void t1086_marshal_cleanup(t1086_marshaled& marshaled);
