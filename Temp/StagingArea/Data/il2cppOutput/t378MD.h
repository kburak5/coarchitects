﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t378;
struct t7;

 void m2245 (t378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1731 (t378 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2246 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1730 (t378 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2247 (t378 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2248 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
