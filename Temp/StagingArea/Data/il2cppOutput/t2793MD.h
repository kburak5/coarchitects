﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2793;
struct t29;
struct t20;
struct t2795;
struct t136;
struct t268;
#include "t2796.h"

#include "t2229MD.h"
#define m15255(__this, method) (void)m11064_gshared((t2229 *)__this, method)
#define m15256(__this, method) (bool)m11065_gshared((t2229 *)__this, method)
#define m15257(__this, method) (t29 *)m11066_gshared((t2229 *)__this, method)
#define m15258(__this, p0, p1, method) (void)m11067_gshared((t2229 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m15259(__this, method) (t29*)m11068_gshared((t2229 *)__this, method)
#define m15260(__this, method) (t29 *)m11069_gshared((t2229 *)__this, method)
#define m15261(__this, method) (t268 *)m11070_gshared((t2229 *)__this, method)
#define m15262(__this, method) (t268 *)m11071_gshared((t2229 *)__this, method)
#define m15263(__this, p0, method) (void)m11072_gshared((t2229 *)__this, (t29 *)p0, method)
#define m15264(__this, method) (int32_t)m11073_gshared((t2229 *)__this, method)
 t2796  m15265 (t2793 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
