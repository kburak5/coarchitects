﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2411;
struct t29;
struct t145;
struct t2412;
struct t20;
struct t136;
struct t2430;
struct t2431;
struct t2432;
struct t2413;
struct t144;
struct t143;
#include "t2433.h"

#include "t294MD.h"
#define m12590(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m12591(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m12592(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m12593(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m12594(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m12595(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m12596(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m12597(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m12598(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m12599(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m12600(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m12601(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m12602(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m12603(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m12604(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m12605(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m12606(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m12607(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m12608(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m12609(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m12610(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m12611(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m12612(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m12613(__this, method) (t2432 *)m10583_gshared((t294 *)__this, method)
#define m12614(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m12615(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m12616(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m12617(__this, p0, method) (t29 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m12618(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m12619(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2433  m12620 (t2411 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m12621(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m12622(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m12623(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m12624(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m12625(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m12626(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m12627(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m12628(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m12629(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m12630(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m12631(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m12632(__this, method) (t2413*)m10619_gshared((t294 *)__this, method)
#define m12633(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m12634(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m12635(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m12636(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m12637(__this, p0, method) (t29 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m12638(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
