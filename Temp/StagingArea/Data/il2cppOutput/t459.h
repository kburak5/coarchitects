﻿#pragma once
#include <stdint.h>
#include "t29.h"
#include "t35.h"
struct t459  : public t29
{
	t35 f0;
};
// Native definition for marshalling of: UnityEngine.Gradient
struct t459_marshaled
{
	t35 f0;
};
