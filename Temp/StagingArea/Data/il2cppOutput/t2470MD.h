﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2470;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m12959_gshared (t2470 * __this, t29 * p0, t35 p1, MethodInfo* method);
#define m12959(__this, p0, p1, method) (void)m12959_gshared((t2470 *)__this, (t29 *)p0, (t35)p1, method)
 t29 * m12960_gshared (t2470 * __this, t29 * p0, t29 * p1, MethodInfo* method);
#define m12960(__this, p0, p1, method) (t29 *)m12960_gshared((t2470 *)__this, (t29 *)p0, (t29 *)p1, method)
 t29 * m12961_gshared (t2470 * __this, t29 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method);
#define m12961(__this, p0, p1, p2, p3, method) (t29 *)m12961_gshared((t2470 *)__this, (t29 *)p0, (t29 *)p1, (t67 *)p2, (t29 *)p3, method)
 t29 * m12962_gshared (t2470 * __this, t29 * p0, MethodInfo* method);
#define m12962(__this, p0, method) (t29 *)m12962_gshared((t2470 *)__this, (t29 *)p0, method)
