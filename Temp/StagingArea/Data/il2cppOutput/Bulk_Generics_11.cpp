﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t2624.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t2624_TI;
#include "t2624MD.h"

#include "t21.h"
#include "t29.h"
#include "t557.h"
#include "t395.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "mscorlib_ArrayTypes.h"
#include "t7.h"
#include "t305.h"
#include "t40.h"
#include "t22.h"
extern TypeInfo t395_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
extern TypeInfo t22_TI;
extern TypeInfo t21_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t395MD.h"
extern Il2CppType t395_0_0_0;
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m21306_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m14115_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
 void m21306 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;

#include "t20.h"

extern MethodInfo m14118_MI;
 void m14118 (t2624 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		m2790(__this, p0, p1, &m2790_MI);
		t395 * L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t395_0_0_0), &m1554_MI);
		t353 * L_2 = m2957(NULL, L_1, p0, p1, &m2957_MI);
		t353 * L_3 = m1597(NULL, L_0, ((t395 *)IsInst(L_2, InitializedTypeInfo(&t395_TI))), &m1597_MI);
		__this->f0 = ((t395 *)Castclass(L_3, InitializedTypeInfo(&t395_TI)));
		return;
	}
}
extern MethodInfo m14119_MI;
 void m14119 (t2624 * __this, t395 * p0, MethodInfo* method){
	{
		m2789(__this, &m2789_MI);
		t395 * L_0 = (__this->f0);
		t353 * L_1 = m1597(NULL, L_0, p0, &m1597_MI);
		__this->f0 = ((t395 *)Castclass(L_1, InitializedTypeInfo(&t395_TI)));
		return;
	}
}
extern MethodInfo m14120_MI;
 void m14120 (t2624 * __this, t316* p0, MethodInfo* method){
	{
		if ((((int32_t)(((int32_t)(((t20 *)p0)->max_length)))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral192, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		int32_t L_1 = 0;
		m21306(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_1)), &m21306_MI);
		t395 * L_2 = (__this->f0);
		bool L_3 = m2791(NULL, L_2, &m2791_MI);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		t395 * L_4 = (__this->f0);
		int32_t L_5 = 0;
		VirtActionInvoker1< float >::Invoke(&m14115_MI, L_4, ((*(float*)((float*)UnBox ((*(t29 **)(t29 **)SZArrayLdElema(p0, L_5)), InitializedTypeInfo(&t22_TI))))));
	}

IL_003f:
	{
		return;
	}
}
extern MethodInfo m14121_MI;
 bool m14121 (t2624 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		t395 * L_0 = (__this->f0);
		t29 * L_1 = m2953(L_0, &m2953_MI);
		if ((((t29 *)L_1) != ((t29 *)p0)))
		{
			goto IL_0021;
		}
	}
	{
		t395 * L_2 = (__this->f0);
		t557 * L_3 = m2951(L_2, &m2951_MI);
		G_B3_0 = ((((t557 *)L_3) == ((t557 *)p1))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall`1<System.Single>
extern Il2CppType t395_0_0_1;
FieldInfo t2624_f0_FieldInfo = 
{
	"Delegate", &t395_0_0_1, &t2624_TI, offsetof(t2624, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2624_FIs[] =
{
	&t2624_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2624_m14118_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14118_GM;
MethodInfo m14118_MI = 
{
	".ctor", (methodPointerType)&m14118, &t2624_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2624_m14118_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14118_GM};
extern Il2CppType t395_0_0_0;
static ParameterInfo t2624_m14119_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t395_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14119_GM;
MethodInfo m14119_MI = 
{
	".ctor", (methodPointerType)&m14119, &t2624_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2624_m14119_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14119_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2624_m14120_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14120_GM;
MethodInfo m14120_MI = 
{
	"Invoke", (methodPointerType)&m14120, &t2624_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2624_m14120_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14120_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2624_m14121_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14121_GM;
MethodInfo m14121_MI = 
{
	"Find", (methodPointerType)&m14121, &t2624_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2624_m14121_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14121_GM};
static MethodInfo* t2624_MIs[] =
{
	&m14118_MI,
	&m14119_MI,
	&m14120_MI,
	&m14121_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
static MethodInfo* t2624_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14120_MI,
	&m14121_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2624_0_0_0;
extern Il2CppType t2624_1_0_0;
extern TypeInfo t556_TI;
struct t2624;
extern Il2CppGenericClass t2624_GC;
TypeInfo t2624_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2624_MIs, NULL, t2624_FIs, NULL, &t556_TI, NULL, NULL, &t2624_TI, NULL, t2624_VT, &EmptyCustomAttributesCache, &t2624_TI, &t2624_0_0_0, &t2624_1_0_0, NULL, &t2624_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2624), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4216_TI;

#include "t212.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Scrollbar/Direction>
extern MethodInfo m28254_MI;
static PropertyInfo t4216____Current_PropertyInfo = 
{
	&t4216_TI, "Current", &m28254_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4216_PIs[] =
{
	&t4216____Current_PropertyInfo,
	NULL
};
extern Il2CppType t212_0_0_0;
extern void* RuntimeInvoker_t212 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28254_GM;
MethodInfo m28254_MI = 
{
	"get_Current", NULL, &t4216_TI, &t212_0_0_0, RuntimeInvoker_t212, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28254_GM};
static MethodInfo* t4216_MIs[] =
{
	&m28254_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4216_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4216_0_0_0;
extern Il2CppType t4216_1_0_0;
struct t4216;
extern Il2CppGenericClass t4216_GC;
TypeInfo t4216_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4216_MIs, t4216_PIs, NULL, NULL, NULL, NULL, NULL, &t4216_TI, t4216_ITIs, NULL, &EmptyCustomAttributesCache, &t4216_TI, &t4216_0_0_0, &t4216_1_0_0, NULL, &t4216_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2625.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2625_TI;
#include "t2625MD.h"

#include "t44.h"
#include "t914.h"
extern TypeInfo t212_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m14126_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m21308_MI;
struct t20;
#include "t915.h"
 int32_t m21308 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14122_MI;
 void m14122 (t2625 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14123_MI;
 t29 * m14123 (t2625 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14126(__this, &m14126_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t212_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14124_MI;
 void m14124 (t2625 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14125_MI;
 bool m14125 (t2625 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14126 (t2625 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21308(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21308_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar/Direction>
extern Il2CppType t20_0_0_1;
FieldInfo t2625_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2625_TI, offsetof(t2625, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2625_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2625_TI, offsetof(t2625, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2625_FIs[] =
{
	&t2625_f0_FieldInfo,
	&t2625_f1_FieldInfo,
	NULL
};
static PropertyInfo t2625____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2625_TI, "System.Collections.IEnumerator.Current", &m14123_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2625____Current_PropertyInfo = 
{
	&t2625_TI, "Current", &m14126_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2625_PIs[] =
{
	&t2625____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2625____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2625_m14122_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14122_GM;
MethodInfo m14122_MI = 
{
	".ctor", (methodPointerType)&m14122, &t2625_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2625_m14122_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14122_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14123_GM;
MethodInfo m14123_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14123, &t2625_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14123_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14124_GM;
MethodInfo m14124_MI = 
{
	"Dispose", (methodPointerType)&m14124, &t2625_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14124_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14125_GM;
MethodInfo m14125_MI = 
{
	"MoveNext", (methodPointerType)&m14125, &t2625_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14125_GM};
extern Il2CppType t212_0_0_0;
extern void* RuntimeInvoker_t212 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14126_GM;
MethodInfo m14126_MI = 
{
	"get_Current", (methodPointerType)&m14126, &t2625_TI, &t212_0_0_0, RuntimeInvoker_t212, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14126_GM};
static MethodInfo* t2625_MIs[] =
{
	&m14122_MI,
	&m14123_MI,
	&m14124_MI,
	&m14125_MI,
	&m14126_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t2625_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14123_MI,
	&m14125_MI,
	&m14124_MI,
	&m14126_MI,
};
static TypeInfo* t2625_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4216_TI,
};
static Il2CppInterfaceOffsetPair t2625_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4216_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2625_0_0_0;
extern Il2CppType t2625_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2625_GC;
extern TypeInfo t20_TI;
TypeInfo t2625_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2625_MIs, t2625_PIs, t2625_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2625_TI, t2625_ITIs, t2625_VT, &EmptyCustomAttributesCache, &t2625_TI, &t2625_0_0_0, &t2625_1_0_0, t2625_IOs, &t2625_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2625)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5400_TI;

#include "UnityEngine.UI_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Scrollbar/Direction>
extern MethodInfo m28255_MI;
static PropertyInfo t5400____Count_PropertyInfo = 
{
	&t5400_TI, "Count", &m28255_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28256_MI;
static PropertyInfo t5400____IsReadOnly_PropertyInfo = 
{
	&t5400_TI, "IsReadOnly", &m28256_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5400_PIs[] =
{
	&t5400____Count_PropertyInfo,
	&t5400____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28255_GM;
MethodInfo m28255_MI = 
{
	"get_Count", NULL, &t5400_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28255_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28256_GM;
MethodInfo m28256_MI = 
{
	"get_IsReadOnly", NULL, &t5400_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28256_GM};
extern Il2CppType t212_0_0_0;
extern Il2CppType t212_0_0_0;
static ParameterInfo t5400_m28257_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t212_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28257_GM;
MethodInfo m28257_MI = 
{
	"Add", NULL, &t5400_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5400_m28257_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28257_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28258_GM;
MethodInfo m28258_MI = 
{
	"Clear", NULL, &t5400_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28258_GM};
extern Il2CppType t212_0_0_0;
static ParameterInfo t5400_m28259_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t212_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28259_GM;
MethodInfo m28259_MI = 
{
	"Contains", NULL, &t5400_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5400_m28259_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28259_GM};
extern Il2CppType t3854_0_0_0;
extern Il2CppType t3854_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5400_m28260_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3854_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28260_GM;
MethodInfo m28260_MI = 
{
	"CopyTo", NULL, &t5400_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5400_m28260_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28260_GM};
extern Il2CppType t212_0_0_0;
static ParameterInfo t5400_m28261_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t212_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28261_GM;
MethodInfo m28261_MI = 
{
	"Remove", NULL, &t5400_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5400_m28261_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28261_GM};
static MethodInfo* t5400_MIs[] =
{
	&m28255_MI,
	&m28256_MI,
	&m28257_MI,
	&m28258_MI,
	&m28259_MI,
	&m28260_MI,
	&m28261_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5402_TI;
static TypeInfo* t5400_ITIs[] = 
{
	&t603_TI,
	&t5402_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5400_0_0_0;
extern Il2CppType t5400_1_0_0;
struct t5400;
extern Il2CppGenericClass t5400_GC;
TypeInfo t5400_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5400_MIs, t5400_PIs, NULL, NULL, NULL, NULL, NULL, &t5400_TI, t5400_ITIs, NULL, &EmptyCustomAttributesCache, &t5400_TI, &t5400_0_0_0, &t5400_1_0_0, NULL, &t5400_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Scrollbar/Direction>
extern Il2CppType t4216_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28262_GM;
MethodInfo m28262_MI = 
{
	"GetEnumerator", NULL, &t5402_TI, &t4216_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28262_GM};
static MethodInfo* t5402_MIs[] =
{
	&m28262_MI,
	NULL
};
static TypeInfo* t5402_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5402_0_0_0;
extern Il2CppType t5402_1_0_0;
struct t5402;
extern Il2CppGenericClass t5402_GC;
TypeInfo t5402_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5402_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5402_TI, t5402_ITIs, NULL, &EmptyCustomAttributesCache, &t5402_TI, &t5402_0_0_0, &t5402_1_0_0, NULL, &t5402_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5401_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Scrollbar/Direction>
extern MethodInfo m28263_MI;
extern MethodInfo m28264_MI;
static PropertyInfo t5401____Item_PropertyInfo = 
{
	&t5401_TI, "Item", &m28263_MI, &m28264_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5401_PIs[] =
{
	&t5401____Item_PropertyInfo,
	NULL
};
extern Il2CppType t212_0_0_0;
static ParameterInfo t5401_m28265_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t212_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28265_GM;
MethodInfo m28265_MI = 
{
	"IndexOf", NULL, &t5401_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5401_m28265_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28265_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t212_0_0_0;
static ParameterInfo t5401_m28266_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t212_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28266_GM;
MethodInfo m28266_MI = 
{
	"Insert", NULL, &t5401_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5401_m28266_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28266_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5401_m28267_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28267_GM;
MethodInfo m28267_MI = 
{
	"RemoveAt", NULL, &t5401_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5401_m28267_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28267_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5401_m28263_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t212_0_0_0;
extern void* RuntimeInvoker_t212_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28263_GM;
MethodInfo m28263_MI = 
{
	"get_Item", NULL, &t5401_TI, &t212_0_0_0, RuntimeInvoker_t212_t44, t5401_m28263_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28263_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t212_0_0_0;
static ParameterInfo t5401_m28264_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t212_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28264_GM;
MethodInfo m28264_MI = 
{
	"set_Item", NULL, &t5401_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5401_m28264_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28264_GM};
static MethodInfo* t5401_MIs[] =
{
	&m28265_MI,
	&m28266_MI,
	&m28267_MI,
	&m28263_MI,
	&m28264_MI,
	NULL
};
static TypeInfo* t5401_ITIs[] = 
{
	&t603_TI,
	&t5400_TI,
	&t5402_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5401_0_0_0;
extern Il2CppType t5401_1_0_0;
struct t5401;
extern Il2CppGenericClass t5401_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5401_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5401_MIs, t5401_PIs, NULL, NULL, NULL, NULL, NULL, &t5401_TI, t5401_ITIs, NULL, &t1908__CustomAttributeCache, &t5401_TI, &t5401_0_0_0, &t5401_1_0_0, NULL, &t5401_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4218_TI;

#include "t215.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Scrollbar/Axis>
extern MethodInfo m28268_MI;
static PropertyInfo t4218____Current_PropertyInfo = 
{
	&t4218_TI, "Current", &m28268_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4218_PIs[] =
{
	&t4218____Current_PropertyInfo,
	NULL
};
extern Il2CppType t215_0_0_0;
extern void* RuntimeInvoker_t215 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28268_GM;
MethodInfo m28268_MI = 
{
	"get_Current", NULL, &t4218_TI, &t215_0_0_0, RuntimeInvoker_t215, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28268_GM};
static MethodInfo* t4218_MIs[] =
{
	&m28268_MI,
	NULL
};
static TypeInfo* t4218_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4218_0_0_0;
extern Il2CppType t4218_1_0_0;
struct t4218;
extern Il2CppGenericClass t4218_GC;
TypeInfo t4218_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4218_MIs, t4218_PIs, NULL, NULL, NULL, NULL, NULL, &t4218_TI, t4218_ITIs, NULL, &EmptyCustomAttributesCache, &t4218_TI, &t4218_0_0_0, &t4218_1_0_0, NULL, &t4218_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2626.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2626_TI;
#include "t2626MD.h"

extern TypeInfo t215_TI;
extern MethodInfo m14131_MI;
extern MethodInfo m21319_MI;
struct t20;
 int32_t m21319 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14127_MI;
 void m14127 (t2626 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14128_MI;
 t29 * m14128 (t2626 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14131(__this, &m14131_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t215_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14129_MI;
 void m14129 (t2626 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14130_MI;
 bool m14130 (t2626 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14131 (t2626 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21319(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21319_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar/Axis>
extern Il2CppType t20_0_0_1;
FieldInfo t2626_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2626_TI, offsetof(t2626, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2626_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2626_TI, offsetof(t2626, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2626_FIs[] =
{
	&t2626_f0_FieldInfo,
	&t2626_f1_FieldInfo,
	NULL
};
static PropertyInfo t2626____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2626_TI, "System.Collections.IEnumerator.Current", &m14128_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2626____Current_PropertyInfo = 
{
	&t2626_TI, "Current", &m14131_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2626_PIs[] =
{
	&t2626____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2626____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2626_m14127_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14127_GM;
MethodInfo m14127_MI = 
{
	".ctor", (methodPointerType)&m14127, &t2626_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2626_m14127_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14127_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14128_GM;
MethodInfo m14128_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14128, &t2626_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14128_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14129_GM;
MethodInfo m14129_MI = 
{
	"Dispose", (methodPointerType)&m14129, &t2626_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14129_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14130_GM;
MethodInfo m14130_MI = 
{
	"MoveNext", (methodPointerType)&m14130, &t2626_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14130_GM};
extern Il2CppType t215_0_0_0;
extern void* RuntimeInvoker_t215 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14131_GM;
MethodInfo m14131_MI = 
{
	"get_Current", (methodPointerType)&m14131, &t2626_TI, &t215_0_0_0, RuntimeInvoker_t215, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14131_GM};
static MethodInfo* t2626_MIs[] =
{
	&m14127_MI,
	&m14128_MI,
	&m14129_MI,
	&m14130_MI,
	&m14131_MI,
	NULL
};
static MethodInfo* t2626_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14128_MI,
	&m14130_MI,
	&m14129_MI,
	&m14131_MI,
};
static TypeInfo* t2626_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4218_TI,
};
static Il2CppInterfaceOffsetPair t2626_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4218_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2626_0_0_0;
extern Il2CppType t2626_1_0_0;
extern Il2CppGenericClass t2626_GC;
TypeInfo t2626_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2626_MIs, t2626_PIs, t2626_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2626_TI, t2626_ITIs, t2626_VT, &EmptyCustomAttributesCache, &t2626_TI, &t2626_0_0_0, &t2626_1_0_0, t2626_IOs, &t2626_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2626)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5403_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Scrollbar/Axis>
extern MethodInfo m28269_MI;
static PropertyInfo t5403____Count_PropertyInfo = 
{
	&t5403_TI, "Count", &m28269_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28270_MI;
static PropertyInfo t5403____IsReadOnly_PropertyInfo = 
{
	&t5403_TI, "IsReadOnly", &m28270_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5403_PIs[] =
{
	&t5403____Count_PropertyInfo,
	&t5403____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28269_GM;
MethodInfo m28269_MI = 
{
	"get_Count", NULL, &t5403_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28269_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28270_GM;
MethodInfo m28270_MI = 
{
	"get_IsReadOnly", NULL, &t5403_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28270_GM};
extern Il2CppType t215_0_0_0;
extern Il2CppType t215_0_0_0;
static ParameterInfo t5403_m28271_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t215_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28271_GM;
MethodInfo m28271_MI = 
{
	"Add", NULL, &t5403_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5403_m28271_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28271_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28272_GM;
MethodInfo m28272_MI = 
{
	"Clear", NULL, &t5403_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28272_GM};
extern Il2CppType t215_0_0_0;
static ParameterInfo t5403_m28273_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t215_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28273_GM;
MethodInfo m28273_MI = 
{
	"Contains", NULL, &t5403_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5403_m28273_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28273_GM};
extern Il2CppType t3855_0_0_0;
extern Il2CppType t3855_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5403_m28274_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3855_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28274_GM;
MethodInfo m28274_MI = 
{
	"CopyTo", NULL, &t5403_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5403_m28274_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28274_GM};
extern Il2CppType t215_0_0_0;
static ParameterInfo t5403_m28275_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t215_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28275_GM;
MethodInfo m28275_MI = 
{
	"Remove", NULL, &t5403_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5403_m28275_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28275_GM};
static MethodInfo* t5403_MIs[] =
{
	&m28269_MI,
	&m28270_MI,
	&m28271_MI,
	&m28272_MI,
	&m28273_MI,
	&m28274_MI,
	&m28275_MI,
	NULL
};
extern TypeInfo t5405_TI;
static TypeInfo* t5403_ITIs[] = 
{
	&t603_TI,
	&t5405_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5403_0_0_0;
extern Il2CppType t5403_1_0_0;
struct t5403;
extern Il2CppGenericClass t5403_GC;
TypeInfo t5403_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5403_MIs, t5403_PIs, NULL, NULL, NULL, NULL, NULL, &t5403_TI, t5403_ITIs, NULL, &EmptyCustomAttributesCache, &t5403_TI, &t5403_0_0_0, &t5403_1_0_0, NULL, &t5403_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Scrollbar/Axis>
extern Il2CppType t4218_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28276_GM;
MethodInfo m28276_MI = 
{
	"GetEnumerator", NULL, &t5405_TI, &t4218_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28276_GM};
static MethodInfo* t5405_MIs[] =
{
	&m28276_MI,
	NULL
};
static TypeInfo* t5405_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5405_0_0_0;
extern Il2CppType t5405_1_0_0;
struct t5405;
extern Il2CppGenericClass t5405_GC;
TypeInfo t5405_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5405_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5405_TI, t5405_ITIs, NULL, &EmptyCustomAttributesCache, &t5405_TI, &t5405_0_0_0, &t5405_1_0_0, NULL, &t5405_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5404_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Scrollbar/Axis>
extern MethodInfo m28277_MI;
extern MethodInfo m28278_MI;
static PropertyInfo t5404____Item_PropertyInfo = 
{
	&t5404_TI, "Item", &m28277_MI, &m28278_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5404_PIs[] =
{
	&t5404____Item_PropertyInfo,
	NULL
};
extern Il2CppType t215_0_0_0;
static ParameterInfo t5404_m28279_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t215_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28279_GM;
MethodInfo m28279_MI = 
{
	"IndexOf", NULL, &t5404_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5404_m28279_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28279_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t215_0_0_0;
static ParameterInfo t5404_m28280_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t215_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28280_GM;
MethodInfo m28280_MI = 
{
	"Insert", NULL, &t5404_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5404_m28280_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28280_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5404_m28281_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28281_GM;
MethodInfo m28281_MI = 
{
	"RemoveAt", NULL, &t5404_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5404_m28281_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28281_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5404_m28277_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t215_0_0_0;
extern void* RuntimeInvoker_t215_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28277_GM;
MethodInfo m28277_MI = 
{
	"get_Item", NULL, &t5404_TI, &t215_0_0_0, RuntimeInvoker_t215_t44, t5404_m28277_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28277_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t215_0_0_0;
static ParameterInfo t5404_m28278_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t215_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28278_GM;
MethodInfo m28278_MI = 
{
	"set_Item", NULL, &t5404_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5404_m28278_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28278_GM};
static MethodInfo* t5404_MIs[] =
{
	&m28279_MI,
	&m28280_MI,
	&m28281_MI,
	&m28277_MI,
	&m28278_MI,
	NULL
};
static TypeInfo* t5404_ITIs[] = 
{
	&t603_TI,
	&t5403_TI,
	&t5405_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5404_0_0_0;
extern Il2CppType t5404_1_0_0;
struct t5404;
extern Il2CppGenericClass t5404_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5404_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5404_MIs, t5404_PIs, NULL, NULL, NULL, NULL, NULL, &t5404_TI, t5404_ITIs, NULL, &t1908__CustomAttributeCache, &t5404_TI, &t5404_0_0_0, &t5404_1_0_0, NULL, &t5404_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4220_TI;

#include "t222.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ScrollRect>
extern MethodInfo m28282_MI;
static PropertyInfo t4220____Current_PropertyInfo = 
{
	&t4220_TI, "Current", &m28282_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4220_PIs[] =
{
	&t4220____Current_PropertyInfo,
	NULL
};
extern Il2CppType t222_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28282_GM;
MethodInfo m28282_MI = 
{
	"get_Current", NULL, &t4220_TI, &t222_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28282_GM};
static MethodInfo* t4220_MIs[] =
{
	&m28282_MI,
	NULL
};
static TypeInfo* t4220_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4220_0_0_0;
extern Il2CppType t4220_1_0_0;
struct t4220;
extern Il2CppGenericClass t4220_GC;
TypeInfo t4220_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4220_MIs, t4220_PIs, NULL, NULL, NULL, NULL, NULL, &t4220_TI, t4220_ITIs, NULL, &EmptyCustomAttributesCache, &t4220_TI, &t4220_0_0_0, &t4220_1_0_0, NULL, &t4220_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2627.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2627_TI;
#include "t2627MD.h"

extern TypeInfo t222_TI;
extern MethodInfo m14136_MI;
extern MethodInfo m21330_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m21330(__this, p0, method) (t222 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.ScrollRect>
extern Il2CppType t20_0_0_1;
FieldInfo t2627_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2627_TI, offsetof(t2627, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2627_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2627_TI, offsetof(t2627, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2627_FIs[] =
{
	&t2627_f0_FieldInfo,
	&t2627_f1_FieldInfo,
	NULL
};
extern MethodInfo m14133_MI;
static PropertyInfo t2627____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2627_TI, "System.Collections.IEnumerator.Current", &m14133_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2627____Current_PropertyInfo = 
{
	&t2627_TI, "Current", &m14136_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2627_PIs[] =
{
	&t2627____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2627____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2627_m14132_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14132_GM;
MethodInfo m14132_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2627_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2627_m14132_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14132_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14133_GM;
MethodInfo m14133_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2627_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14133_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14134_GM;
MethodInfo m14134_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2627_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14134_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14135_GM;
MethodInfo m14135_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2627_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14135_GM};
extern Il2CppType t222_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14136_GM;
MethodInfo m14136_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2627_TI, &t222_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14136_GM};
static MethodInfo* t2627_MIs[] =
{
	&m14132_MI,
	&m14133_MI,
	&m14134_MI,
	&m14135_MI,
	&m14136_MI,
	NULL
};
extern MethodInfo m14135_MI;
extern MethodInfo m14134_MI;
static MethodInfo* t2627_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14133_MI,
	&m14135_MI,
	&m14134_MI,
	&m14136_MI,
};
static TypeInfo* t2627_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4220_TI,
};
static Il2CppInterfaceOffsetPair t2627_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4220_TI, 7},
};
extern TypeInfo t222_TI;
static Il2CppRGCTXData t2627_RGCTXData[3] = 
{
	&m14136_MI/* Method Usage */,
	&t222_TI/* Class Usage */,
	&m21330_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2627_0_0_0;
extern Il2CppType t2627_1_0_0;
extern Il2CppGenericClass t2627_GC;
TypeInfo t2627_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2627_MIs, t2627_PIs, t2627_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2627_TI, t2627_ITIs, t2627_VT, &EmptyCustomAttributesCache, &t2627_TI, &t2627_0_0_0, &t2627_1_0_0, t2627_IOs, &t2627_GC, NULL, NULL, NULL, t2627_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2627)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5406_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.ScrollRect>
extern MethodInfo m28283_MI;
static PropertyInfo t5406____Count_PropertyInfo = 
{
	&t5406_TI, "Count", &m28283_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28284_MI;
static PropertyInfo t5406____IsReadOnly_PropertyInfo = 
{
	&t5406_TI, "IsReadOnly", &m28284_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5406_PIs[] =
{
	&t5406____Count_PropertyInfo,
	&t5406____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28283_GM;
MethodInfo m28283_MI = 
{
	"get_Count", NULL, &t5406_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28283_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28284_GM;
MethodInfo m28284_MI = 
{
	"get_IsReadOnly", NULL, &t5406_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28284_GM};
extern Il2CppType t222_0_0_0;
extern Il2CppType t222_0_0_0;
static ParameterInfo t5406_m28285_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t222_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28285_GM;
MethodInfo m28285_MI = 
{
	"Add", NULL, &t5406_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5406_m28285_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28285_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28286_GM;
MethodInfo m28286_MI = 
{
	"Clear", NULL, &t5406_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28286_GM};
extern Il2CppType t222_0_0_0;
static ParameterInfo t5406_m28287_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t222_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28287_GM;
MethodInfo m28287_MI = 
{
	"Contains", NULL, &t5406_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5406_m28287_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28287_GM};
extern Il2CppType t3856_0_0_0;
extern Il2CppType t3856_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5406_m28288_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3856_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28288_GM;
MethodInfo m28288_MI = 
{
	"CopyTo", NULL, &t5406_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5406_m28288_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28288_GM};
extern Il2CppType t222_0_0_0;
static ParameterInfo t5406_m28289_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t222_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28289_GM;
MethodInfo m28289_MI = 
{
	"Remove", NULL, &t5406_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5406_m28289_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28289_GM};
static MethodInfo* t5406_MIs[] =
{
	&m28283_MI,
	&m28284_MI,
	&m28285_MI,
	&m28286_MI,
	&m28287_MI,
	&m28288_MI,
	&m28289_MI,
	NULL
};
extern TypeInfo t5408_TI;
static TypeInfo* t5406_ITIs[] = 
{
	&t603_TI,
	&t5408_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5406_0_0_0;
extern Il2CppType t5406_1_0_0;
struct t5406;
extern Il2CppGenericClass t5406_GC;
TypeInfo t5406_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5406_MIs, t5406_PIs, NULL, NULL, NULL, NULL, NULL, &t5406_TI, t5406_ITIs, NULL, &EmptyCustomAttributesCache, &t5406_TI, &t5406_0_0_0, &t5406_1_0_0, NULL, &t5406_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.ScrollRect>
extern Il2CppType t4220_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28290_GM;
MethodInfo m28290_MI = 
{
	"GetEnumerator", NULL, &t5408_TI, &t4220_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28290_GM};
static MethodInfo* t5408_MIs[] =
{
	&m28290_MI,
	NULL
};
static TypeInfo* t5408_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5408_0_0_0;
extern Il2CppType t5408_1_0_0;
struct t5408;
extern Il2CppGenericClass t5408_GC;
TypeInfo t5408_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5408_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5408_TI, t5408_ITIs, NULL, &EmptyCustomAttributesCache, &t5408_TI, &t5408_0_0_0, &t5408_1_0_0, NULL, &t5408_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5407_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.ScrollRect>
extern MethodInfo m28291_MI;
extern MethodInfo m28292_MI;
static PropertyInfo t5407____Item_PropertyInfo = 
{
	&t5407_TI, "Item", &m28291_MI, &m28292_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5407_PIs[] =
{
	&t5407____Item_PropertyInfo,
	NULL
};
extern Il2CppType t222_0_0_0;
static ParameterInfo t5407_m28293_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t222_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28293_GM;
MethodInfo m28293_MI = 
{
	"IndexOf", NULL, &t5407_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5407_m28293_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28293_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t222_0_0_0;
static ParameterInfo t5407_m28294_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t222_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28294_GM;
MethodInfo m28294_MI = 
{
	"Insert", NULL, &t5407_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5407_m28294_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28294_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5407_m28295_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28295_GM;
MethodInfo m28295_MI = 
{
	"RemoveAt", NULL, &t5407_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5407_m28295_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28295_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5407_m28291_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t222_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28291_GM;
MethodInfo m28291_MI = 
{
	"get_Item", NULL, &t5407_TI, &t222_0_0_0, RuntimeInvoker_t29_t44, t5407_m28291_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28291_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t222_0_0_0;
static ParameterInfo t5407_m28292_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t222_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28292_GM;
MethodInfo m28292_MI = 
{
	"set_Item", NULL, &t5407_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5407_m28292_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28292_GM};
static MethodInfo* t5407_MIs[] =
{
	&m28293_MI,
	&m28294_MI,
	&m28295_MI,
	&m28291_MI,
	&m28292_MI,
	NULL
};
static TypeInfo* t5407_ITIs[] = 
{
	&t603_TI,
	&t5406_TI,
	&t5408_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5407_0_0_0;
extern Il2CppType t5407_1_0_0;
struct t5407;
extern Il2CppGenericClass t5407_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5407_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5407_MIs, t5407_PIs, NULL, NULL, NULL, NULL, NULL, &t5407_TI, t5407_ITIs, NULL, &t1908__CustomAttributeCache, &t5407_TI, &t5407_0_0_0, &t5407_1_0_0, NULL, &t5407_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2628.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2628_TI;
#include "t2628MD.h"

#include "t41.h"
#include "t2629.h"
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t2629_TI;
#include "t2629MD.h"
extern MethodInfo m14139_MI;
extern MethodInfo m14141_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ScrollRect>
extern Il2CppType t316_0_0_33;
FieldInfo t2628_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2628_TI, offsetof(t2628, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2628_FIs[] =
{
	&t2628_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t222_0_0_0;
static ParameterInfo t2628_m14137_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t222_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14137_GM;
MethodInfo m14137_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2628_m14137_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14137_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2628_m14138_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14138_GM;
MethodInfo m14138_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2628_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2628_m14138_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14138_GM};
static MethodInfo* t2628_MIs[] =
{
	&m14137_MI,
	&m14138_MI,
	NULL
};
extern MethodInfo m14138_MI;
extern MethodInfo m14142_MI;
static MethodInfo* t2628_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14138_MI,
	&m14142_MI,
};
extern Il2CppType t2630_0_0_0;
extern TypeInfo t2630_TI;
extern MethodInfo m21340_MI;
extern TypeInfo t222_TI;
extern MethodInfo m14144_MI;
extern TypeInfo t222_TI;
static Il2CppRGCTXData t2628_RGCTXData[8] = 
{
	&t2630_0_0_0/* Type Usage */,
	&t2630_TI/* Class Usage */,
	&m21340_MI/* Method Usage */,
	&t222_TI/* Class Usage */,
	&m14144_MI/* Method Usage */,
	&m14139_MI/* Method Usage */,
	&t222_TI/* Class Usage */,
	&m14141_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2628_0_0_0;
extern Il2CppType t2628_1_0_0;
struct t2628;
extern Il2CppGenericClass t2628_GC;
TypeInfo t2628_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2628_MIs, NULL, t2628_FIs, NULL, &t2629_TI, NULL, NULL, &t2628_TI, NULL, t2628_VT, &EmptyCustomAttributesCache, &t2628_TI, &t2628_0_0_0, &t2628_1_0_0, NULL, &t2628_GC, NULL, NULL, NULL, t2628_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2628), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2630.h"
extern TypeInfo t2630_TI;
#include "t2630MD.h"
struct t556;
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m21340(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.ScrollRect>
extern Il2CppType t2630_0_0_1;
FieldInfo t2629_f0_FieldInfo = 
{
	"Delegate", &t2630_0_0_1, &t2629_TI, offsetof(t2629, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2629_FIs[] =
{
	&t2629_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2629_m14139_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14139_GM;
MethodInfo m14139_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2629_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2629_m14139_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14139_GM};
extern Il2CppType t2630_0_0_0;
static ParameterInfo t2629_m14140_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2630_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14140_GM;
MethodInfo m14140_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2629_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2629_m14140_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14140_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2629_m14141_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14141_GM;
MethodInfo m14141_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2629_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2629_m14141_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14141_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2629_m14142_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14142_GM;
MethodInfo m14142_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2629_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2629_m14142_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14142_GM};
static MethodInfo* t2629_MIs[] =
{
	&m14139_MI,
	&m14140_MI,
	&m14141_MI,
	&m14142_MI,
	NULL
};
static MethodInfo* t2629_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14141_MI,
	&m14142_MI,
};
extern TypeInfo t2630_TI;
extern TypeInfo t222_TI;
static Il2CppRGCTXData t2629_RGCTXData[5] = 
{
	&t2630_0_0_0/* Type Usage */,
	&t2630_TI/* Class Usage */,
	&m21340_MI/* Method Usage */,
	&t222_TI/* Class Usage */,
	&m14144_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2629_0_0_0;
extern Il2CppType t2629_1_0_0;
struct t2629;
extern Il2CppGenericClass t2629_GC;
TypeInfo t2629_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2629_MIs, NULL, t2629_FIs, NULL, &t556_TI, NULL, NULL, &t2629_TI, NULL, t2629_VT, &EmptyCustomAttributesCache, &t2629_TI, &t2629_0_0_0, &t2629_1_0_0, NULL, &t2629_GC, NULL, NULL, NULL, t2629_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2629), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.ScrollRect>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2630_m14143_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14143_GM;
MethodInfo m14143_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2630_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2630_m14143_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14143_GM};
extern Il2CppType t222_0_0_0;
static ParameterInfo t2630_m14144_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t222_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14144_GM;
MethodInfo m14144_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2630_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2630_m14144_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14144_GM};
extern Il2CppType t222_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2630_m14145_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t222_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14145_GM;
MethodInfo m14145_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2630_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2630_m14145_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14145_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2630_m14146_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14146_GM;
MethodInfo m14146_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2630_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2630_m14146_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14146_GM};
static MethodInfo* t2630_MIs[] =
{
	&m14143_MI,
	&m14144_MI,
	&m14145_MI,
	&m14146_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m14145_MI;
extern MethodInfo m14146_MI;
static MethodInfo* t2630_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14144_MI,
	&m14145_MI,
	&m14146_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2630_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2630_1_0_0;
extern TypeInfo t195_TI;
struct t2630;
extern Il2CppGenericClass t2630_GC;
TypeInfo t2630_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2630_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2630_TI, NULL, t2630_VT, &EmptyCustomAttributesCache, &t2630_TI, &t2630_0_0_0, &t2630_1_0_0, t2630_IOs, &t2630_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2630), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4221_TI;

#include "t23.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
extern MethodInfo m28296_MI;
static PropertyInfo t4221____Current_PropertyInfo = 
{
	&t4221_TI, "Current", &m28296_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4221_PIs[] =
{
	&t4221____Current_PropertyInfo,
	NULL
};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28296_GM;
MethodInfo m28296_MI = 
{
	"get_Current", NULL, &t4221_TI, &t23_0_0_0, RuntimeInvoker_t23, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28296_GM};
static MethodInfo* t4221_MIs[] =
{
	&m28296_MI,
	NULL
};
static TypeInfo* t4221_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4221_0_0_0;
extern Il2CppType t4221_1_0_0;
struct t4221;
extern Il2CppGenericClass t4221_GC;
TypeInfo t4221_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4221_MIs, t4221_PIs, NULL, NULL, NULL, NULL, NULL, &t4221_TI, t4221_ITIs, NULL, &EmptyCustomAttributesCache, &t4221_TI, &t4221_0_0_0, &t4221_1_0_0, NULL, &t4221_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2631.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2631_TI;
#include "t2631MD.h"

extern TypeInfo t23_TI;
extern MethodInfo m14151_MI;
extern MethodInfo m21342_MI;
struct t20;
 t23  m21342 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14147_MI;
 void m14147 (t2631 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14148_MI;
 t29 * m14148 (t2631 * __this, MethodInfo* method){
	{
		t23  L_0 = m14151(__this, &m14151_MI);
		t23  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t23_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14149_MI;
 void m14149 (t2631 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14150_MI;
 bool m14150 (t2631 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t23  m14151 (t2631 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t23  L_8 = m21342(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21342_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Vector3>
extern Il2CppType t20_0_0_1;
FieldInfo t2631_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2631_TI, offsetof(t2631, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2631_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2631_TI, offsetof(t2631, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2631_FIs[] =
{
	&t2631_f0_FieldInfo,
	&t2631_f1_FieldInfo,
	NULL
};
static PropertyInfo t2631____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2631_TI, "System.Collections.IEnumerator.Current", &m14148_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2631____Current_PropertyInfo = 
{
	&t2631_TI, "Current", &m14151_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2631_PIs[] =
{
	&t2631____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2631____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2631_m14147_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14147_GM;
MethodInfo m14147_MI = 
{
	".ctor", (methodPointerType)&m14147, &t2631_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2631_m14147_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14147_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14148_GM;
MethodInfo m14148_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14148, &t2631_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14148_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14149_GM;
MethodInfo m14149_MI = 
{
	"Dispose", (methodPointerType)&m14149, &t2631_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14149_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14150_GM;
MethodInfo m14150_MI = 
{
	"MoveNext", (methodPointerType)&m14150, &t2631_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14150_GM};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14151_GM;
MethodInfo m14151_MI = 
{
	"get_Current", (methodPointerType)&m14151, &t2631_TI, &t23_0_0_0, RuntimeInvoker_t23, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14151_GM};
static MethodInfo* t2631_MIs[] =
{
	&m14147_MI,
	&m14148_MI,
	&m14149_MI,
	&m14150_MI,
	&m14151_MI,
	NULL
};
static MethodInfo* t2631_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14148_MI,
	&m14150_MI,
	&m14149_MI,
	&m14151_MI,
};
static TypeInfo* t2631_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4221_TI,
};
static Il2CppInterfaceOffsetPair t2631_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4221_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2631_0_0_0;
extern Il2CppType t2631_1_0_0;
extern Il2CppGenericClass t2631_GC;
TypeInfo t2631_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2631_MIs, t2631_PIs, t2631_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2631_TI, t2631_ITIs, t2631_VT, &EmptyCustomAttributesCache, &t2631_TI, &t2631_0_0_0, &t2631_1_0_0, t2631_IOs, &t2631_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2631)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5409_TI;

#include "UnityEngine_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Vector3>
extern MethodInfo m28297_MI;
static PropertyInfo t5409____Count_PropertyInfo = 
{
	&t5409_TI, "Count", &m28297_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28298_MI;
static PropertyInfo t5409____IsReadOnly_PropertyInfo = 
{
	&t5409_TI, "IsReadOnly", &m28298_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5409_PIs[] =
{
	&t5409____Count_PropertyInfo,
	&t5409____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28297_GM;
MethodInfo m28297_MI = 
{
	"get_Count", NULL, &t5409_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28297_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28298_GM;
MethodInfo m28298_MI = 
{
	"get_IsReadOnly", NULL, &t5409_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28298_GM};
extern Il2CppType t23_0_0_0;
extern Il2CppType t23_0_0_0;
static ParameterInfo t5409_m28299_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t23 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28299_GM;
MethodInfo m28299_MI = 
{
	"Add", NULL, &t5409_TI, &t21_0_0_0, RuntimeInvoker_t21_t23, t5409_m28299_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28299_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28300_GM;
MethodInfo m28300_MI = 
{
	"Clear", NULL, &t5409_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28300_GM};
extern Il2CppType t23_0_0_0;
static ParameterInfo t5409_m28301_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t23 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28301_GM;
MethodInfo m28301_MI = 
{
	"Contains", NULL, &t5409_TI, &t40_0_0_0, RuntimeInvoker_t40_t23, t5409_m28301_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28301_GM};
extern Il2CppType t223_0_0_0;
extern Il2CppType t223_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5409_m28302_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t223_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28302_GM;
MethodInfo m28302_MI = 
{
	"CopyTo", NULL, &t5409_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5409_m28302_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28302_GM};
extern Il2CppType t23_0_0_0;
static ParameterInfo t5409_m28303_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t23 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28303_GM;
MethodInfo m28303_MI = 
{
	"Remove", NULL, &t5409_TI, &t40_0_0_0, RuntimeInvoker_t40_t23, t5409_m28303_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28303_GM};
static MethodInfo* t5409_MIs[] =
{
	&m28297_MI,
	&m28298_MI,
	&m28299_MI,
	&m28300_MI,
	&m28301_MI,
	&m28302_MI,
	&m28303_MI,
	NULL
};
extern TypeInfo t5411_TI;
static TypeInfo* t5409_ITIs[] = 
{
	&t603_TI,
	&t5411_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5409_0_0_0;
extern Il2CppType t5409_1_0_0;
struct t5409;
extern Il2CppGenericClass t5409_GC;
TypeInfo t5409_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5409_MIs, t5409_PIs, NULL, NULL, NULL, NULL, NULL, &t5409_TI, t5409_ITIs, NULL, &EmptyCustomAttributesCache, &t5409_TI, &t5409_0_0_0, &t5409_1_0_0, NULL, &t5409_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
extern Il2CppType t4221_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28304_GM;
MethodInfo m28304_MI = 
{
	"GetEnumerator", NULL, &t5411_TI, &t4221_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28304_GM};
static MethodInfo* t5411_MIs[] =
{
	&m28304_MI,
	NULL
};
static TypeInfo* t5411_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5411_0_0_0;
extern Il2CppType t5411_1_0_0;
struct t5411;
extern Il2CppGenericClass t5411_GC;
TypeInfo t5411_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5411_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5411_TI, t5411_ITIs, NULL, &EmptyCustomAttributesCache, &t5411_TI, &t5411_0_0_0, &t5411_1_0_0, NULL, &t5411_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5410_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Vector3>
extern MethodInfo m28305_MI;
extern MethodInfo m28306_MI;
static PropertyInfo t5410____Item_PropertyInfo = 
{
	&t5410_TI, "Item", &m28305_MI, &m28306_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5410_PIs[] =
{
	&t5410____Item_PropertyInfo,
	NULL
};
extern Il2CppType t23_0_0_0;
static ParameterInfo t5410_m28307_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t23 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28307_GM;
MethodInfo m28307_MI = 
{
	"IndexOf", NULL, &t5410_TI, &t44_0_0_0, RuntimeInvoker_t44_t23, t5410_m28307_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28307_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t23_0_0_0;
static ParameterInfo t5410_m28308_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t23 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28308_GM;
MethodInfo m28308_MI = 
{
	"Insert", NULL, &t5410_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t23, t5410_m28308_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28308_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5410_m28309_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28309_GM;
MethodInfo m28309_MI = 
{
	"RemoveAt", NULL, &t5410_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5410_m28309_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28309_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5410_m28305_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t23_0_0_0;
extern void* RuntimeInvoker_t23_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28305_GM;
MethodInfo m28305_MI = 
{
	"get_Item", NULL, &t5410_TI, &t23_0_0_0, RuntimeInvoker_t23_t44, t5410_m28305_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28305_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t23_0_0_0;
static ParameterInfo t5410_m28306_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t23_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t23 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28306_GM;
MethodInfo m28306_MI = 
{
	"set_Item", NULL, &t5410_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t23, t5410_m28306_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28306_GM};
static MethodInfo* t5410_MIs[] =
{
	&m28307_MI,
	&m28308_MI,
	&m28309_MI,
	&m28305_MI,
	&m28306_MI,
	NULL
};
static TypeInfo* t5410_ITIs[] = 
{
	&t603_TI,
	&t5409_TI,
	&t5411_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5410_0_0_0;
extern Il2CppType t5410_1_0_0;
struct t5410;
extern Il2CppGenericClass t5410_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5410_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5410_MIs, t5410_PIs, NULL, NULL, NULL, NULL, NULL, &t5410_TI, t5410_ITIs, NULL, &t1908__CustomAttributeCache, &t5410_TI, &t5410_0_0_0, &t5410_1_0_0, NULL, &t5410_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t221.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t221_TI;
#include "t221MD.h"

#include "t2632.h"
#include "t17.h"
#include "t2633.h"
extern TypeInfo t537_TI;
extern TypeInfo t17_TI;
extern TypeInfo t2633_TI;
#include "t566MD.h"
#include "t2633MD.h"
extern Il2CppType t17_0_0_0;
extern MethodInfo m2812_MI;
extern MethodInfo m14154_MI;
extern MethodInfo m2817_MI;
extern MethodInfo m2818_MI;
extern MethodInfo m2820_MI;
extern MethodInfo m14159_MI;
extern MethodInfo m14160_MI;
extern MethodInfo m2819_MI;


extern MethodInfo m1838_MI;
 void m1838 (t221 * __this, MethodInfo* method){
	{
		__this->f4 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		m2812(__this, &m2812_MI);
		return;
	}
}
extern MethodInfo m14152_MI;
 void m14152 (t221 * __this, t2632 * p0, MethodInfo* method){
	{
		t556 * L_0 = m14154(NULL, p0, &m14154_MI);
		m2817(__this, L_0, &m2817_MI);
		return;
	}
}
extern MethodInfo m14153_MI;
 void m14153 (t221 * __this, t2632 * p0, MethodInfo* method){
	{
		t29 * L_0 = m2953(p0, &m2953_MI);
		t557 * L_1 = m2951(p0, &m2951_MI);
		m2818(__this, L_0, L_1, &m2818_MI);
		return;
	}
}
extern MethodInfo m1839_MI;
 t557 * m1839 (t221 * __this, t7* p0, t29 * p1, MethodInfo* method){
	{
		t537* L_0 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t17_0_0_0), &m1554_MI);
		ArrayElementTypeCheck (L_0, L_1);
		*((t42 **)(t42 **)SZArrayLdElema(L_0, 0)) = (t42 *)L_1;
		t557 * L_2 = m2820(NULL, p1, p0, L_0, &m2820_MI);
		return L_2;
	}
}
extern MethodInfo m1840_MI;
 t556 * m1840 (t221 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		t2633 * L_0 = (t2633 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2633_TI));
		m14159(L_0, p0, p1, &m14159_MI);
		return L_0;
	}
}
 t556 * m14154 (t29 * __this, t2632 * p0, MethodInfo* method){
	{
		t2633 * L_0 = (t2633 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2633_TI));
		m14160(L_0, p0, &m14160_MI);
		return L_0;
	}
}
extern MethodInfo m1851_MI;
 void m1851 (t221 * __this, t17  p0, MethodInfo* method){
	{
		t316* L_0 = (__this->f4);
		t17  L_1 = p0;
		t29 * L_2 = Box(InitializedTypeInfo(&t17_TI), &L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((t29 **)(t29 **)SZArrayLdElema(L_0, 0)) = (t29 *)L_2;
		t316* L_3 = (__this->f4);
		m2819(__this, L_3, &m2819_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
extern Il2CppType t316_0_0_33;
FieldInfo t221_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t221_TI, offsetof(t221, f4), &EmptyCustomAttributesCache};
static FieldInfo* t221_FIs[] =
{
	&t221_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1838_GM;
MethodInfo m1838_MI = 
{
	".ctor", (methodPointerType)&m1838, &t221_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1838_GM};
extern Il2CppType t2632_0_0_0;
extern Il2CppType t2632_0_0_0;
static ParameterInfo t221_m14152_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t2632_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14152_GM;
MethodInfo m14152_MI = 
{
	"AddListener", (methodPointerType)&m14152, &t221_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t221_m14152_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14152_GM};
extern Il2CppType t2632_0_0_0;
static ParameterInfo t221_m14153_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t2632_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14153_GM;
MethodInfo m14153_MI = 
{
	"RemoveListener", (methodPointerType)&m14153, &t221_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t221_m14153_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14153_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t221_m1839_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1839_GM;
MethodInfo m1839_MI = 
{
	"FindMethod_Impl", (methodPointerType)&m1839, &t221_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29, t221_m1839_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1839_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t221_m1840_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1840_GM;
MethodInfo m1840_MI = 
{
	"GetDelegate", (methodPointerType)&m1840, &t221_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29, t221_m1840_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1840_GM};
extern Il2CppType t2632_0_0_0;
static ParameterInfo t221_m14154_ParameterInfos[] = 
{
	{"action", 0, 134217728, &EmptyCustomAttributesCache, &t2632_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14154_GM;
MethodInfo m14154_MI = 
{
	"GetDelegate", (methodPointerType)&m14154, &t221_TI, &t556_0_0_0, RuntimeInvoker_t29_t29, t221_m14154_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14154_GM};
extern Il2CppType t17_0_0_0;
static ParameterInfo t221_m1851_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1851_GM;
MethodInfo m1851_MI = 
{
	"Invoke", (methodPointerType)&m1851, &t221_TI, &t21_0_0_0, RuntimeInvoker_t21_t17, t221_m1851_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1851_GM};
static MethodInfo* t221_MIs[] =
{
	&m1838_MI,
	&m14152_MI,
	&m14153_MI,
	&m1839_MI,
	&m1840_MI,
	&m14154_MI,
	&m1851_MI,
	NULL
};
extern MethodInfo m1323_MI;
extern MethodInfo m1324_MI;
extern MethodInfo m1325_MI;
static MethodInfo* t221_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m1839_MI,
	&m1840_MI,
};
extern TypeInfo t301_TI;
static Il2CppInterfaceOffsetPair t221_IOs[] = 
{
	{ &t301_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t221_0_0_0;
extern Il2CppType t221_1_0_0;
extern TypeInfo t566_TI;
struct t221;
extern Il2CppGenericClass t221_GC;
TypeInfo t221_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`1", "UnityEngine.Events", t221_MIs, NULL, t221_FIs, NULL, &t566_TI, NULL, NULL, &t221_TI, NULL, t221_VT, &EmptyCustomAttributesCache, &t221_TI, &t221_0_0_0, &t221_1_0_0, t221_IOs, &t221_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t221), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 0, 1, 0, 0, 8, 0, 1};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2632_TI;
#include "t2632MD.h"



extern MethodInfo m14155_MI;
 void m14155 (t2632 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m14156_MI;
 void m14156 (t2632 * __this, t17  p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m14156((t2632 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t17  p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	typedef void (*FunctionPointerType) (t29 * __this, t17  p0, MethodInfo* method);
	((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m14157_MI;
 t29 * m14157 (t2632 * __this, t17  p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t17_TI), &p0);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m14158_MI;
 void m14158 (t2632 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2632_m14155_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14155_GM;
MethodInfo m14155_MI = 
{
	".ctor", (methodPointerType)&m14155, &t2632_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2632_m14155_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14155_GM};
extern Il2CppType t17_0_0_0;
static ParameterInfo t2632_m14156_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t17_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t17 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14156_GM;
MethodInfo m14156_MI = 
{
	"Invoke", (methodPointerType)&m14156, &t2632_TI, &t21_0_0_0, RuntimeInvoker_t21_t17, t2632_m14156_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14156_GM};
extern Il2CppType t17_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2632_m14157_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t17_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t17_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14157_GM;
MethodInfo m14157_MI = 
{
	"BeginInvoke", (methodPointerType)&m14157, &t2632_TI, &t66_0_0_0, RuntimeInvoker_t29_t17_t29_t29, t2632_m14157_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14157_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2632_m14158_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14158_GM;
MethodInfo m14158_MI = 
{
	"EndInvoke", (methodPointerType)&m14158, &t2632_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2632_m14158_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14158_GM};
static MethodInfo* t2632_MIs[] =
{
	&m14155_MI,
	&m14156_MI,
	&m14157_MI,
	&m14158_MI,
	NULL
};
static MethodInfo* t2632_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14156_MI,
	&m14157_MI,
	&m14158_MI,
};
static Il2CppInterfaceOffsetPair t2632_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2632_1_0_0;
struct t2632;
extern Il2CppGenericClass t2632_GC;
TypeInfo t2632_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2632_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2632_TI, NULL, t2632_VT, &EmptyCustomAttributesCache, &t2632_TI, &t2632_0_0_0, &t2632_1_0_0, t2632_IOs, &t2632_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2632), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m21352_MI;
struct t556;
 void m21352 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


 void m14159 (t2633 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		m2790(__this, p0, p1, &m2790_MI);
		t2632 * L_0 = (__this->f0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t2632_0_0_0), &m1554_MI);
		t353 * L_2 = m2957(NULL, L_1, p0, p1, &m2957_MI);
		t353 * L_3 = m1597(NULL, L_0, ((t2632 *)IsInst(L_2, InitializedTypeInfo(&t2632_TI))), &m1597_MI);
		__this->f0 = ((t2632 *)Castclass(L_3, InitializedTypeInfo(&t2632_TI)));
		return;
	}
}
 void m14160 (t2633 * __this, t2632 * p0, MethodInfo* method){
	{
		m2789(__this, &m2789_MI);
		t2632 * L_0 = (__this->f0);
		t353 * L_1 = m1597(NULL, L_0, p0, &m1597_MI);
		__this->f0 = ((t2632 *)Castclass(L_1, InitializedTypeInfo(&t2632_TI)));
		return;
	}
}
extern MethodInfo m14161_MI;
 void m14161 (t2633 * __this, t316* p0, MethodInfo* method){
	{
		if ((((int32_t)(((int32_t)(((t20 *)p0)->max_length)))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral192, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		int32_t L_1 = 0;
		m21352(NULL, (*(t29 **)(t29 **)SZArrayLdElema(p0, L_1)), &m21352_MI);
		t2632 * L_2 = (__this->f0);
		bool L_3 = m2791(NULL, L_2, &m2791_MI);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		t2632 * L_4 = (__this->f0);
		int32_t L_5 = 0;
		VirtActionInvoker1< t17  >::Invoke(&m14156_MI, L_4, ((*(t17 *)((t17 *)UnBox ((*(t29 **)(t29 **)SZArrayLdElema(p0, L_5)), InitializedTypeInfo(&t17_TI))))));
	}

IL_003f:
	{
		return;
	}
}
extern MethodInfo m14162_MI;
 bool m14162 (t2633 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		t2632 * L_0 = (__this->f0);
		t29 * L_1 = m2953(L_0, &m2953_MI);
		if ((((t29 *)L_1) != ((t29 *)p0)))
		{
			goto IL_0021;
		}
	}
	{
		t2632 * L_2 = (__this->f0);
		t557 * L_3 = m2951(L_2, &m2951_MI);
		G_B3_0 = ((((t557 *)L_3) == ((t557 *)p1))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>
extern Il2CppType t2632_0_0_1;
FieldInfo t2633_f0_FieldInfo = 
{
	"Delegate", &t2632_0_0_1, &t2633_TI, offsetof(t2633, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2633_FIs[] =
{
	&t2633_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2633_m14159_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14159_GM;
MethodInfo m14159_MI = 
{
	".ctor", (methodPointerType)&m14159, &t2633_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2633_m14159_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14159_GM};
extern Il2CppType t2632_0_0_0;
static ParameterInfo t2633_m14160_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2632_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14160_GM;
MethodInfo m14160_MI = 
{
	".ctor", (methodPointerType)&m14160, &t2633_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2633_m14160_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14160_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2633_m14161_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14161_GM;
MethodInfo m14161_MI = 
{
	"Invoke", (methodPointerType)&m14161, &t2633_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2633_m14161_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14161_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2633_m14162_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14162_GM;
MethodInfo m14162_MI = 
{
	"Find", (methodPointerType)&m14162, &t2633_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2633_m14162_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14162_GM};
static MethodInfo* t2633_MIs[] =
{
	&m14159_MI,
	&m14160_MI,
	&m14161_MI,
	&m14162_MI,
	NULL
};
static MethodInfo* t2633_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14161_MI,
	&m14162_MI,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2633_0_0_0;
extern Il2CppType t2633_1_0_0;
struct t2633;
extern Il2CppGenericClass t2633_GC;
TypeInfo t2633_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2633_MIs, NULL, t2633_FIs, NULL, &t556_TI, NULL, NULL, &t2633_TI, NULL, t2633_VT, &EmptyCustomAttributesCache, &t2633_TI, &t2633_0_0_0, &t2633_1_0_0, NULL, &t2633_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2633), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4223_TI;

#include "t219.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ScrollRect/MovementType>
extern MethodInfo m28310_MI;
static PropertyInfo t4223____Current_PropertyInfo = 
{
	&t4223_TI, "Current", &m28310_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4223_PIs[] =
{
	&t4223____Current_PropertyInfo,
	NULL
};
extern Il2CppType t219_0_0_0;
extern void* RuntimeInvoker_t219 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28310_GM;
MethodInfo m28310_MI = 
{
	"get_Current", NULL, &t4223_TI, &t219_0_0_0, RuntimeInvoker_t219, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28310_GM};
static MethodInfo* t4223_MIs[] =
{
	&m28310_MI,
	NULL
};
static TypeInfo* t4223_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4223_0_0_0;
extern Il2CppType t4223_1_0_0;
struct t4223;
extern Il2CppGenericClass t4223_GC;
TypeInfo t4223_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4223_MIs, t4223_PIs, NULL, NULL, NULL, NULL, NULL, &t4223_TI, t4223_ITIs, NULL, &EmptyCustomAttributesCache, &t4223_TI, &t4223_0_0_0, &t4223_1_0_0, NULL, &t4223_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2634.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2634_TI;
#include "t2634MD.h"

extern TypeInfo t219_TI;
extern MethodInfo m14167_MI;
extern MethodInfo m21354_MI;
struct t20;
 int32_t m21354 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14163_MI;
 void m14163 (t2634 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14164_MI;
 t29 * m14164 (t2634 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14167(__this, &m14167_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t219_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14165_MI;
 void m14165 (t2634 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14166_MI;
 bool m14166 (t2634 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14167 (t2634 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21354(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21354_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.ScrollRect/MovementType>
extern Il2CppType t20_0_0_1;
FieldInfo t2634_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2634_TI, offsetof(t2634, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2634_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2634_TI, offsetof(t2634, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2634_FIs[] =
{
	&t2634_f0_FieldInfo,
	&t2634_f1_FieldInfo,
	NULL
};
static PropertyInfo t2634____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2634_TI, "System.Collections.IEnumerator.Current", &m14164_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2634____Current_PropertyInfo = 
{
	&t2634_TI, "Current", &m14167_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2634_PIs[] =
{
	&t2634____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2634____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2634_m14163_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14163_GM;
MethodInfo m14163_MI = 
{
	".ctor", (methodPointerType)&m14163, &t2634_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2634_m14163_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14163_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14164_GM;
MethodInfo m14164_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14164, &t2634_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14164_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14165_GM;
MethodInfo m14165_MI = 
{
	"Dispose", (methodPointerType)&m14165, &t2634_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14165_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14166_GM;
MethodInfo m14166_MI = 
{
	"MoveNext", (methodPointerType)&m14166, &t2634_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14166_GM};
extern Il2CppType t219_0_0_0;
extern void* RuntimeInvoker_t219 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14167_GM;
MethodInfo m14167_MI = 
{
	"get_Current", (methodPointerType)&m14167, &t2634_TI, &t219_0_0_0, RuntimeInvoker_t219, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14167_GM};
static MethodInfo* t2634_MIs[] =
{
	&m14163_MI,
	&m14164_MI,
	&m14165_MI,
	&m14166_MI,
	&m14167_MI,
	NULL
};
static MethodInfo* t2634_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14164_MI,
	&m14166_MI,
	&m14165_MI,
	&m14167_MI,
};
static TypeInfo* t2634_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4223_TI,
};
static Il2CppInterfaceOffsetPair t2634_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4223_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2634_0_0_0;
extern Il2CppType t2634_1_0_0;
extern Il2CppGenericClass t2634_GC;
TypeInfo t2634_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2634_MIs, t2634_PIs, t2634_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2634_TI, t2634_ITIs, t2634_VT, &EmptyCustomAttributesCache, &t2634_TI, &t2634_0_0_0, &t2634_1_0_0, t2634_IOs, &t2634_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2634)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5412_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.ScrollRect/MovementType>
extern MethodInfo m28311_MI;
static PropertyInfo t5412____Count_PropertyInfo = 
{
	&t5412_TI, "Count", &m28311_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28312_MI;
static PropertyInfo t5412____IsReadOnly_PropertyInfo = 
{
	&t5412_TI, "IsReadOnly", &m28312_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5412_PIs[] =
{
	&t5412____Count_PropertyInfo,
	&t5412____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28311_GM;
MethodInfo m28311_MI = 
{
	"get_Count", NULL, &t5412_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28311_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28312_GM;
MethodInfo m28312_MI = 
{
	"get_IsReadOnly", NULL, &t5412_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28312_GM};
extern Il2CppType t219_0_0_0;
extern Il2CppType t219_0_0_0;
static ParameterInfo t5412_m28313_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t219_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28313_GM;
MethodInfo m28313_MI = 
{
	"Add", NULL, &t5412_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5412_m28313_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28313_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28314_GM;
MethodInfo m28314_MI = 
{
	"Clear", NULL, &t5412_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28314_GM};
extern Il2CppType t219_0_0_0;
static ParameterInfo t5412_m28315_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t219_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28315_GM;
MethodInfo m28315_MI = 
{
	"Contains", NULL, &t5412_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5412_m28315_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28315_GM};
extern Il2CppType t3857_0_0_0;
extern Il2CppType t3857_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5412_m28316_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3857_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28316_GM;
MethodInfo m28316_MI = 
{
	"CopyTo", NULL, &t5412_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5412_m28316_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28316_GM};
extern Il2CppType t219_0_0_0;
static ParameterInfo t5412_m28317_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t219_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28317_GM;
MethodInfo m28317_MI = 
{
	"Remove", NULL, &t5412_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5412_m28317_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28317_GM};
static MethodInfo* t5412_MIs[] =
{
	&m28311_MI,
	&m28312_MI,
	&m28313_MI,
	&m28314_MI,
	&m28315_MI,
	&m28316_MI,
	&m28317_MI,
	NULL
};
extern TypeInfo t5414_TI;
static TypeInfo* t5412_ITIs[] = 
{
	&t603_TI,
	&t5414_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5412_0_0_0;
extern Il2CppType t5412_1_0_0;
struct t5412;
extern Il2CppGenericClass t5412_GC;
TypeInfo t5412_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5412_MIs, t5412_PIs, NULL, NULL, NULL, NULL, NULL, &t5412_TI, t5412_ITIs, NULL, &EmptyCustomAttributesCache, &t5412_TI, &t5412_0_0_0, &t5412_1_0_0, NULL, &t5412_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.ScrollRect/MovementType>
extern Il2CppType t4223_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28318_GM;
MethodInfo m28318_MI = 
{
	"GetEnumerator", NULL, &t5414_TI, &t4223_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28318_GM};
static MethodInfo* t5414_MIs[] =
{
	&m28318_MI,
	NULL
};
static TypeInfo* t5414_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5414_0_0_0;
extern Il2CppType t5414_1_0_0;
struct t5414;
extern Il2CppGenericClass t5414_GC;
TypeInfo t5414_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5414_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5414_TI, t5414_ITIs, NULL, &EmptyCustomAttributesCache, &t5414_TI, &t5414_0_0_0, &t5414_1_0_0, NULL, &t5414_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5413_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.ScrollRect/MovementType>
extern MethodInfo m28319_MI;
extern MethodInfo m28320_MI;
static PropertyInfo t5413____Item_PropertyInfo = 
{
	&t5413_TI, "Item", &m28319_MI, &m28320_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5413_PIs[] =
{
	&t5413____Item_PropertyInfo,
	NULL
};
extern Il2CppType t219_0_0_0;
static ParameterInfo t5413_m28321_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t219_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28321_GM;
MethodInfo m28321_MI = 
{
	"IndexOf", NULL, &t5413_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5413_m28321_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28321_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t219_0_0_0;
static ParameterInfo t5413_m28322_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t219_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28322_GM;
MethodInfo m28322_MI = 
{
	"Insert", NULL, &t5413_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5413_m28322_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28322_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5413_m28323_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28323_GM;
MethodInfo m28323_MI = 
{
	"RemoveAt", NULL, &t5413_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5413_m28323_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28323_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5413_m28319_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t219_0_0_0;
extern void* RuntimeInvoker_t219_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28319_GM;
MethodInfo m28319_MI = 
{
	"get_Item", NULL, &t5413_TI, &t219_0_0_0, RuntimeInvoker_t219_t44, t5413_m28319_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28319_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t219_0_0_0;
static ParameterInfo t5413_m28320_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t219_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28320_GM;
MethodInfo m28320_MI = 
{
	"set_Item", NULL, &t5413_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5413_m28320_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28320_GM};
static MethodInfo* t5413_MIs[] =
{
	&m28321_MI,
	&m28322_MI,
	&m28323_MI,
	&m28319_MI,
	&m28320_MI,
	NULL
};
static TypeInfo* t5413_ITIs[] = 
{
	&t603_TI,
	&t5412_TI,
	&t5414_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5413_0_0_0;
extern Il2CppType t5413_1_0_0;
struct t5413;
extern Il2CppGenericClass t5413_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5413_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5413_MIs, t5413_PIs, NULL, NULL, NULL, NULL, NULL, &t5413_TI, t5413_ITIs, NULL, &t1908__CustomAttributeCache, &t5413_TI, &t5413_0_0_0, &t5413_1_0_0, NULL, &t5413_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2635.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2635_TI;
#include "t2635MD.h"

#include "t139.h"
#include "t2636.h"
extern TypeInfo t139_TI;
extern TypeInfo t2636_TI;
#include "t2636MD.h"
extern MethodInfo m14170_MI;
extern MethodInfo m14172_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Selectable>
extern Il2CppType t316_0_0_33;
FieldInfo t2635_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2635_TI, offsetof(t2635, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2635_FIs[] =
{
	&t2635_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t139_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2635_m14168_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14168_GM;
MethodInfo m14168_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2635_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2635_m14168_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14168_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2635_m14169_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14169_GM;
MethodInfo m14169_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2635_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2635_m14169_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14169_GM};
static MethodInfo* t2635_MIs[] =
{
	&m14168_MI,
	&m14169_MI,
	NULL
};
extern MethodInfo m14169_MI;
extern MethodInfo m14173_MI;
static MethodInfo* t2635_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14169_MI,
	&m14173_MI,
};
extern Il2CppType t2637_0_0_0;
extern TypeInfo t2637_TI;
extern MethodInfo m21364_MI;
extern TypeInfo t139_TI;
extern MethodInfo m14175_MI;
extern TypeInfo t139_TI;
static Il2CppRGCTXData t2635_RGCTXData[8] = 
{
	&t2637_0_0_0/* Type Usage */,
	&t2637_TI/* Class Usage */,
	&m21364_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
	&m14175_MI/* Method Usage */,
	&m14170_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
	&m14172_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2635_0_0_0;
extern Il2CppType t2635_1_0_0;
struct t2635;
extern Il2CppGenericClass t2635_GC;
TypeInfo t2635_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2635_MIs, NULL, t2635_FIs, NULL, &t2636_TI, NULL, NULL, &t2635_TI, NULL, t2635_VT, &EmptyCustomAttributesCache, &t2635_TI, &t2635_0_0_0, &t2635_1_0_0, NULL, &t2635_GC, NULL, NULL, NULL, t2635_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2635), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2637.h"
extern TypeInfo t2637_TI;
#include "t2637MD.h"
struct t556;
#define m21364(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Selectable>
extern Il2CppType t2637_0_0_1;
FieldInfo t2636_f0_FieldInfo = 
{
	"Delegate", &t2637_0_0_1, &t2636_TI, offsetof(t2636, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2636_FIs[] =
{
	&t2636_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2636_m14170_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14170_GM;
MethodInfo m14170_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2636_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2636_m14170_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14170_GM};
extern Il2CppType t2637_0_0_0;
static ParameterInfo t2636_m14171_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2637_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14171_GM;
MethodInfo m14171_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2636_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2636_m14171_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14171_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2636_m14172_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14172_GM;
MethodInfo m14172_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2636_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2636_m14172_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14172_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2636_m14173_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14173_GM;
MethodInfo m14173_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2636_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2636_m14173_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14173_GM};
static MethodInfo* t2636_MIs[] =
{
	&m14170_MI,
	&m14171_MI,
	&m14172_MI,
	&m14173_MI,
	NULL
};
static MethodInfo* t2636_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14172_MI,
	&m14173_MI,
};
extern TypeInfo t2637_TI;
extern TypeInfo t139_TI;
static Il2CppRGCTXData t2636_RGCTXData[5] = 
{
	&t2637_0_0_0/* Type Usage */,
	&t2637_TI/* Class Usage */,
	&m21364_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
	&m14175_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2636_0_0_0;
extern Il2CppType t2636_1_0_0;
struct t2636;
extern Il2CppGenericClass t2636_GC;
TypeInfo t2636_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2636_MIs, NULL, t2636_FIs, NULL, &t556_TI, NULL, NULL, &t2636_TI, NULL, t2636_VT, &EmptyCustomAttributesCache, &t2636_TI, &t2636_0_0_0, &t2636_1_0_0, NULL, &t2636_GC, NULL, NULL, NULL, t2636_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2636), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Selectable>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2637_m14174_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14174_GM;
MethodInfo m14174_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2637_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2637_m14174_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14174_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2637_m14175_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14175_GM;
MethodInfo m14175_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2637_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2637_m14175_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14175_GM};
extern Il2CppType t139_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2637_m14176_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14176_GM;
MethodInfo m14176_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2637_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2637_m14176_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14176_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2637_m14177_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14177_GM;
MethodInfo m14177_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2637_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2637_m14177_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14177_GM};
static MethodInfo* t2637_MIs[] =
{
	&m14174_MI,
	&m14175_MI,
	&m14176_MI,
	&m14177_MI,
	NULL
};
extern MethodInfo m14176_MI;
extern MethodInfo m14177_MI;
static MethodInfo* t2637_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14175_MI,
	&m14176_MI,
	&m14177_MI,
};
static Il2CppInterfaceOffsetPair t2637_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2637_1_0_0;
struct t2637;
extern Il2CppGenericClass t2637_GC;
TypeInfo t2637_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2637_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2637_TI, NULL, t2637_VT, &EmptyCustomAttributesCache, &t2637_TI, &t2637_0_0_0, &t2637_1_0_0, t2637_IOs, &t2637_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2637), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t226.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t226_TI;
#include "t226MD.h"

#include "t2645.h"
#include "t2642.h"
#include "t2643.h"
#include "t338.h"
#include "t2650.h"
#include "t2644.h"
extern TypeInfo t44_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t2638_TI;
extern TypeInfo t2645_TI;
extern TypeInfo t40_TI;
extern TypeInfo t2640_TI;
extern TypeInfo t2641_TI;
extern TypeInfo t2639_TI;
extern TypeInfo t2642_TI;
extern TypeInfo t338_TI;
extern TypeInfo t2643_TI;
extern TypeInfo t2650_TI;
#include "t915MD.h"
#include "t29MD.h"
#include "t602MD.h"
#include "t2642MD.h"
#include "t338MD.h"
#include "t2643MD.h"
#include "t2645MD.h"
#include "t2650MD.h"
extern MethodInfo m1886_MI;
extern MethodInfo m14221_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m21366_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m14209_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m14206_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m1880_MI;
extern MethodInfo m14201_MI;
extern MethodInfo m14207_MI;
extern MethodInfo m14210_MI;
extern MethodInfo m1881_MI;
extern MethodInfo m14195_MI;
extern MethodInfo m14219_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m14220_MI;
extern MethodInfo m27575_MI;
extern MethodInfo m27580_MI;
extern MethodInfo m27582_MI;
extern MethodInfo m27583_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m14211_MI;
extern MethodInfo m14196_MI;
extern MethodInfo m14197_MI;
extern MethodInfo m14228_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m21368_MI;
extern MethodInfo m14204_MI;
extern MethodInfo m14205_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m14303_MI;
extern MethodInfo m14222_MI;
extern MethodInfo m14208_MI;
extern MethodInfo m14213_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m14309_MI;
extern MethodInfo m21370_MI;
extern MethodInfo m21378_MI;
extern MethodInfo m5951_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m21366(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2648.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m21368(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m21370(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#include "t295.h"
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m21378(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2645  m14206 (t226 * __this, MethodInfo* method){
	{
		t2645  L_0 = {0};
		m14222(&L_0, __this, &m14222_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
extern Il2CppType t44_0_0_32849;
FieldInfo t226_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t226_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2638_0_0_1;
FieldInfo t226_f1_FieldInfo = 
{
	"_items", &t2638_0_0_1, &t226_TI, offsetof(t226, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t226_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t226_TI, offsetof(t226, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t226_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t226_TI, offsetof(t226, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2638_0_0_49;
FieldInfo t226_f4_FieldInfo = 
{
	"EmptyArray", &t2638_0_0_49, &t226_TI, offsetof(t226_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t226_FIs[] =
{
	&t226_f0_FieldInfo,
	&t226_f1_FieldInfo,
	&t226_f2_FieldInfo,
	&t226_f3_FieldInfo,
	&t226_f4_FieldInfo,
	NULL
};
static const int32_t t226_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t226_f0_DefaultValue = 
{
	&t226_f0_FieldInfo, { (char*)&t226_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t226_FDVs[] = 
{
	&t226_f0_DefaultValue,
	NULL
};
extern MethodInfo m14188_MI;
static PropertyInfo t226____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t226_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14188_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14189_MI;
static PropertyInfo t226____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t226_TI, "System.Collections.ICollection.IsSynchronized", &m14189_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14190_MI;
static PropertyInfo t226____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t226_TI, "System.Collections.ICollection.SyncRoot", &m14190_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14191_MI;
static PropertyInfo t226____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t226_TI, "System.Collections.IList.IsFixedSize", &m14191_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14192_MI;
static PropertyInfo t226____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t226_TI, "System.Collections.IList.IsReadOnly", &m14192_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14193_MI;
extern MethodInfo m14194_MI;
static PropertyInfo t226____System_Collections_IList_Item_PropertyInfo = 
{
	&t226_TI, "System.Collections.IList.Item", &m14193_MI, &m14194_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t226____Capacity_PropertyInfo = 
{
	&t226_TI, "Capacity", &m14219_MI, &m14220_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1888_MI;
static PropertyInfo t226____Count_PropertyInfo = 
{
	&t226_TI, "Count", &m1888_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t226____Item_PropertyInfo = 
{
	&t226_TI, "Item", &m1886_MI, &m14221_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t226_PIs[] =
{
	&t226____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t226____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t226____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t226____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t226____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t226____System_Collections_IList_Item_PropertyInfo,
	&t226____Capacity_PropertyInfo,
	&t226____Count_PropertyInfo,
	&t226____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1874_GM;
MethodInfo m1874_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1874_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t226_m14178_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14178_GM;
MethodInfo m14178_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t226_m14178_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14178_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14179_GM;
MethodInfo m14179_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14179_GM};
extern Il2CppType t2639_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14180_GM;
MethodInfo m14180_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t226_TI, &t2639_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14180_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t226_m14181_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14181_GM;
MethodInfo m14181_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t226_m14181_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14181_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14182_GM;
MethodInfo m14182_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t226_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14182_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t226_m14183_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14183_GM;
MethodInfo m14183_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t226_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t226_m14183_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14183_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t226_m14184_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14184_GM;
MethodInfo m14184_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t226_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t226_m14184_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14184_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t226_m14185_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14185_GM;
MethodInfo m14185_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t226_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t226_m14185_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14185_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t226_m14186_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14186_GM;
MethodInfo m14186_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t226_m14186_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14186_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t226_m14187_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14187_GM;
MethodInfo m14187_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t226_m14187_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14187_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14188_GM;
MethodInfo m14188_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t226_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14188_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14189_GM;
MethodInfo m14189_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t226_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14189_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14190_GM;
MethodInfo m14190_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t226_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14190_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14191_GM;
MethodInfo m14191_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t226_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14191_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14192_GM;
MethodInfo m14192_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t226_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14192_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t226_m14193_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14193_GM;
MethodInfo m14193_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t226_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t226_m14193_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14193_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t226_m14194_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14194_GM;
MethodInfo m14194_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t226_m14194_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14194_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t226_m1880_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1880_GM;
MethodInfo m1880_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t226_m1880_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1880_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t226_m14195_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14195_GM;
MethodInfo m14195_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t226_m14195_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14195_GM};
extern Il2CppType t2640_0_0_0;
extern Il2CppType t2640_0_0_0;
static ParameterInfo t226_m14196_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2640_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14196_GM;
MethodInfo m14196_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t226_m14196_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14196_GM};
extern Il2CppType t2641_0_0_0;
extern Il2CppType t2641_0_0_0;
static ParameterInfo t226_m14197_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2641_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14197_GM;
MethodInfo m14197_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t226_m14197_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14197_GM};
extern Il2CppType t2641_0_0_0;
static ParameterInfo t226_m14198_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2641_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14198_GM;
MethodInfo m14198_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t226_m14198_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14198_GM};
extern Il2CppType t2642_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14199_GM;
MethodInfo m14199_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t226_TI, &t2642_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14199_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14200_GM;
MethodInfo m14200_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14200_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t226_m14201_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14201_GM;
MethodInfo m14201_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t226_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t226_m14201_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14201_GM};
extern Il2CppType t2638_0_0_0;
extern Il2CppType t2638_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t226_m14202_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2638_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14202_GM;
MethodInfo m14202_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t226_m14202_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14202_GM};
extern Il2CppType t2643_0_0_0;
extern Il2CppType t2643_0_0_0;
static ParameterInfo t226_m14203_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2643_0_0_0},
};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14203_GM;
MethodInfo m14203_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t226_TI, &t139_0_0_0, RuntimeInvoker_t29_t29, t226_m14203_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14203_GM};
extern Il2CppType t2643_0_0_0;
static ParameterInfo t226_m14204_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2643_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14204_GM;
MethodInfo m14204_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t226_m14204_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14204_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2643_0_0_0;
static ParameterInfo t226_m14205_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2643_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14205_GM;
MethodInfo m14205_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t226_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t226_m14205_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14205_GM};
extern Il2CppType t2645_0_0_0;
extern void* RuntimeInvoker_t2645 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14206_GM;
MethodInfo m14206_MI = 
{
	"GetEnumerator", (methodPointerType)&m14206, &t226_TI, &t2645_0_0_0, RuntimeInvoker_t2645, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14206_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t226_m14207_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14207_GM;
MethodInfo m14207_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t226_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t226_m14207_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14207_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t226_m14208_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14208_GM;
MethodInfo m14208_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t226_m14208_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14208_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t226_m14209_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14209_GM;
MethodInfo m14209_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t226_m14209_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14209_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t226_m14210_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14210_GM;
MethodInfo m14210_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t226_m14210_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14210_GM};
extern Il2CppType t2641_0_0_0;
static ParameterInfo t226_m14211_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2641_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14211_GM;
MethodInfo m14211_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t226_m14211_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14211_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t226_m1881_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1881_GM;
MethodInfo m1881_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t226_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t226_m1881_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1881_GM};
extern Il2CppType t2643_0_0_0;
static ParameterInfo t226_m14212_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2643_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14212_GM;
MethodInfo m14212_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t226_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t226_m14212_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14212_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t226_m14213_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14213_GM;
MethodInfo m14213_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t226_m14213_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14213_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14214_GM;
MethodInfo m14214_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14214_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14215_GM;
MethodInfo m14215_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14215_GM};
extern Il2CppType t2644_0_0_0;
extern Il2CppType t2644_0_0_0;
static ParameterInfo t226_m14216_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2644_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14216_GM;
MethodInfo m14216_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t226_m14216_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14216_GM};
extern Il2CppType t2638_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14217_GM;
MethodInfo m14217_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t226_TI, &t2638_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14217_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14218_GM;
MethodInfo m14218_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14218_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14219_GM;
MethodInfo m14219_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t226_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14219_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t226_m14220_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14220_GM;
MethodInfo m14220_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t226_m14220_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14220_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1888_GM;
MethodInfo m1888_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t226_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1888_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t226_m1886_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1886_GM;
MethodInfo m1886_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t226_TI, &t139_0_0_0, RuntimeInvoker_t29_t44, t226_m1886_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1886_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t226_m14221_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14221_GM;
MethodInfo m14221_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t226_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t226_m14221_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14221_GM};
static MethodInfo* t226_MIs[] =
{
	&m1874_MI,
	&m14178_MI,
	&m14179_MI,
	&m14180_MI,
	&m14181_MI,
	&m14182_MI,
	&m14183_MI,
	&m14184_MI,
	&m14185_MI,
	&m14186_MI,
	&m14187_MI,
	&m14188_MI,
	&m14189_MI,
	&m14190_MI,
	&m14191_MI,
	&m14192_MI,
	&m14193_MI,
	&m14194_MI,
	&m1880_MI,
	&m14195_MI,
	&m14196_MI,
	&m14197_MI,
	&m14198_MI,
	&m14199_MI,
	&m14200_MI,
	&m14201_MI,
	&m14202_MI,
	&m14203_MI,
	&m14204_MI,
	&m14205_MI,
	&m14206_MI,
	&m14207_MI,
	&m14208_MI,
	&m14209_MI,
	&m14210_MI,
	&m14211_MI,
	&m1881_MI,
	&m14212_MI,
	&m14213_MI,
	&m14214_MI,
	&m14215_MI,
	&m14216_MI,
	&m14217_MI,
	&m14218_MI,
	&m14219_MI,
	&m14220_MI,
	&m1888_MI,
	&m1886_MI,
	&m14221_MI,
	NULL
};
extern MethodInfo m14182_MI;
extern MethodInfo m14181_MI;
extern MethodInfo m14183_MI;
extern MethodInfo m14200_MI;
extern MethodInfo m14184_MI;
extern MethodInfo m14185_MI;
extern MethodInfo m14186_MI;
extern MethodInfo m14187_MI;
extern MethodInfo m14202_MI;
extern MethodInfo m14180_MI;
static MethodInfo* t226_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14182_MI,
	&m1888_MI,
	&m14189_MI,
	&m14190_MI,
	&m14181_MI,
	&m14191_MI,
	&m14192_MI,
	&m14193_MI,
	&m14194_MI,
	&m14183_MI,
	&m14200_MI,
	&m14184_MI,
	&m14185_MI,
	&m14186_MI,
	&m14187_MI,
	&m14213_MI,
	&m1888_MI,
	&m14188_MI,
	&m1880_MI,
	&m14200_MI,
	&m14201_MI,
	&m14202_MI,
	&m1881_MI,
	&m14180_MI,
	&m14207_MI,
	&m14210_MI,
	&m14213_MI,
	&m1886_MI,
	&m14221_MI,
};
extern TypeInfo t674_TI;
extern TypeInfo t868_TI;
extern TypeInfo t2646_TI;
static TypeInfo* t226_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2640_TI,
	&t2641_TI,
	&t2646_TI,
};
static Il2CppInterfaceOffsetPair t226_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2640_TI, 20},
	{ &t2641_TI, 27},
	{ &t2646_TI, 28},
};
extern TypeInfo t226_TI;
extern TypeInfo t2638_TI;
extern TypeInfo t2645_TI;
extern TypeInfo t139_TI;
extern TypeInfo t2640_TI;
extern TypeInfo t2642_TI;
static Il2CppRGCTXData t226_RGCTXData[37] = 
{
	&t226_TI/* Static Usage */,
	&t2638_TI/* Array Usage */,
	&m14206_MI/* Method Usage */,
	&t2645_TI/* Class Usage */,
	&t139_TI/* Class Usage */,
	&m1880_MI/* Method Usage */,
	&m14201_MI/* Method Usage */,
	&m14207_MI/* Method Usage */,
	&m14209_MI/* Method Usage */,
	&m14210_MI/* Method Usage */,
	&m1881_MI/* Method Usage */,
	&m1886_MI/* Method Usage */,
	&m14221_MI/* Method Usage */,
	&m14195_MI/* Method Usage */,
	&m14219_MI/* Method Usage */,
	&m14220_MI/* Method Usage */,
	&m27575_MI/* Method Usage */,
	&m27580_MI/* Method Usage */,
	&m27582_MI/* Method Usage */,
	&m27583_MI/* Method Usage */,
	&m14211_MI/* Method Usage */,
	&t2640_TI/* Class Usage */,
	&m14196_MI/* Method Usage */,
	&m14197_MI/* Method Usage */,
	&t2642_TI/* Class Usage */,
	&m14228_MI/* Method Usage */,
	&m21368_MI/* Method Usage */,
	&m14204_MI/* Method Usage */,
	&m14205_MI/* Method Usage */,
	&m14303_MI/* Method Usage */,
	&m14222_MI/* Method Usage */,
	&m14208_MI/* Method Usage */,
	&m14213_MI/* Method Usage */,
	&m14309_MI/* Method Usage */,
	&m21370_MI/* Method Usage */,
	&m21378_MI/* Method Usage */,
	&m21366_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t226_0_0_0;
extern Il2CppType t226_1_0_0;
struct t226;
extern Il2CppGenericClass t226_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t226_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t226_MIs, t226_PIs, t226_FIs, NULL, &t29_TI, NULL, NULL, &t226_TI, t226_ITIs, t226_VT, &t1261__CustomAttributeCache, &t226_TI, &t226_0_0_0, &t226_1_0_0, t226_IOs, &t226_GC, NULL, t226_FDVs, NULL, t226_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t226), 0, -1, sizeof(t226_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif

#include "t1101.h"
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t1101MD.h"
extern MethodInfo m14225_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>
extern Il2CppType t226_0_0_1;
FieldInfo t2645_f0_FieldInfo = 
{
	"l", &t226_0_0_1, &t2645_TI, offsetof(t2645, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2645_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2645_TI, offsetof(t2645, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2645_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2645_TI, offsetof(t2645, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t139_0_0_1;
FieldInfo t2645_f3_FieldInfo = 
{
	"current", &t139_0_0_1, &t2645_TI, offsetof(t2645, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2645_FIs[] =
{
	&t2645_f0_FieldInfo,
	&t2645_f1_FieldInfo,
	&t2645_f2_FieldInfo,
	&t2645_f3_FieldInfo,
	NULL
};
extern MethodInfo m14223_MI;
static PropertyInfo t2645____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2645_TI, "System.Collections.IEnumerator.Current", &m14223_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14227_MI;
static PropertyInfo t2645____Current_PropertyInfo = 
{
	&t2645_TI, "Current", &m14227_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2645_PIs[] =
{
	&t2645____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2645____Current_PropertyInfo,
	NULL
};
extern Il2CppType t226_0_0_0;
static ParameterInfo t2645_m14222_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t226_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14222_GM;
MethodInfo m14222_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2645_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2645_m14222_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14222_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14223_GM;
MethodInfo m14223_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2645_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14223_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14224_GM;
MethodInfo m14224_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2645_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14224_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14225_GM;
MethodInfo m14225_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2645_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14225_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14226_GM;
MethodInfo m14226_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2645_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14226_GM};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14227_GM;
MethodInfo m14227_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2645_TI, &t139_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14227_GM};
static MethodInfo* t2645_MIs[] =
{
	&m14222_MI,
	&m14223_MI,
	&m14224_MI,
	&m14225_MI,
	&m14226_MI,
	&m14227_MI,
	NULL
};
extern MethodInfo m14226_MI;
extern MethodInfo m14224_MI;
static MethodInfo* t2645_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14223_MI,
	&m14226_MI,
	&m14224_MI,
	&m14227_MI,
};
static TypeInfo* t2645_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2639_TI,
};
static Il2CppInterfaceOffsetPair t2645_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2639_TI, 7},
};
extern TypeInfo t139_TI;
extern TypeInfo t2645_TI;
static Il2CppRGCTXData t2645_RGCTXData[3] = 
{
	&m14225_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
	&t2645_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2645_0_0_0;
extern Il2CppType t2645_1_0_0;
extern Il2CppGenericClass t2645_GC;
extern TypeInfo t1261_TI;
TypeInfo t2645_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2645_MIs, t2645_PIs, t2645_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2645_TI, t2645_ITIs, t2645_VT, &EmptyCustomAttributesCache, &t2645_TI, &t2645_0_0_0, &t2645_1_0_0, t2645_IOs, &t2645_GC, NULL, NULL, NULL, t2645_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2645)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
#include "t2647MD.h"
extern MethodInfo m14257_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m27584_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m14289_MI;
extern MethodInfo m27579_MI;
extern MethodInfo m27586_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.Selectable>
extern Il2CppType t2646_0_0_1;
FieldInfo t2642_f0_FieldInfo = 
{
	"list", &t2646_0_0_1, &t2642_TI, offsetof(t2642, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2642_FIs[] =
{
	&t2642_f0_FieldInfo,
	NULL
};
extern MethodInfo m14234_MI;
extern MethodInfo m14235_MI;
static PropertyInfo t2642____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2642_TI, "System.Collections.Generic.IList<T>.Item", &m14234_MI, &m14235_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14236_MI;
static PropertyInfo t2642____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2642_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14236_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14246_MI;
static PropertyInfo t2642____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2642_TI, "System.Collections.ICollection.IsSynchronized", &m14246_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14247_MI;
static PropertyInfo t2642____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2642_TI, "System.Collections.ICollection.SyncRoot", &m14247_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14248_MI;
static PropertyInfo t2642____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2642_TI, "System.Collections.IList.IsFixedSize", &m14248_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14249_MI;
static PropertyInfo t2642____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2642_TI, "System.Collections.IList.IsReadOnly", &m14249_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14250_MI;
extern MethodInfo m14251_MI;
static PropertyInfo t2642____System_Collections_IList_Item_PropertyInfo = 
{
	&t2642_TI, "System.Collections.IList.Item", &m14250_MI, &m14251_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14256_MI;
static PropertyInfo t2642____Count_PropertyInfo = 
{
	&t2642_TI, "Count", &m14256_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2642____Item_PropertyInfo = 
{
	&t2642_TI, "Item", &m14257_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2642_PIs[] =
{
	&t2642____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2642____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2642____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2642____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2642____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2642____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2642____System_Collections_IList_Item_PropertyInfo,
	&t2642____Count_PropertyInfo,
	&t2642____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2646_0_0_0;
extern Il2CppType t2646_0_0_0;
static ParameterInfo t2642_m14228_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2646_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14228_GM;
MethodInfo m14228_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2642_m14228_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14228_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2642_m14229_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14229_GM;
MethodInfo m14229_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2642_m14229_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14229_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14230_GM;
MethodInfo m14230_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14230_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2642_m14231_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14231_GM;
MethodInfo m14231_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2642_m14231_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14231_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2642_m14232_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14232_GM;
MethodInfo m14232_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2642_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2642_m14232_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14232_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2642_m14233_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14233_GM;
MethodInfo m14233_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2642_m14233_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14233_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2642_m14234_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14234_GM;
MethodInfo m14234_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2642_TI, &t139_0_0_0, RuntimeInvoker_t29_t44, t2642_m14234_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14234_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2642_m14235_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14235_GM;
MethodInfo m14235_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2642_m14235_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14235_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14236_GM;
MethodInfo m14236_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2642_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14236_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2642_m14237_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14237_GM;
MethodInfo m14237_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2642_m14237_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14237_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14238_GM;
MethodInfo m14238_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2642_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14238_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2642_m14239_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14239_GM;
MethodInfo m14239_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2642_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2642_m14239_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14239_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14240_GM;
MethodInfo m14240_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14240_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2642_m14241_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14241_GM;
MethodInfo m14241_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2642_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2642_m14241_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14241_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2642_m14242_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14242_GM;
MethodInfo m14242_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2642_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2642_m14242_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14242_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2642_m14243_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14243_GM;
MethodInfo m14243_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2642_m14243_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14243_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2642_m14244_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14244_GM;
MethodInfo m14244_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2642_m14244_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14244_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2642_m14245_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14245_GM;
MethodInfo m14245_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2642_m14245_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14245_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14246_GM;
MethodInfo m14246_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2642_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14246_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14247_GM;
MethodInfo m14247_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2642_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14247_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14248_GM;
MethodInfo m14248_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2642_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14248_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14249_GM;
MethodInfo m14249_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2642_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14249_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2642_m14250_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14250_GM;
MethodInfo m14250_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2642_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2642_m14250_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14250_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2642_m14251_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14251_GM;
MethodInfo m14251_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2642_m14251_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14251_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2642_m14252_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14252_GM;
MethodInfo m14252_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2642_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2642_m14252_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14252_GM};
extern Il2CppType t2638_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2642_m14253_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2638_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14253_GM;
MethodInfo m14253_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2642_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2642_m14253_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14253_GM};
extern Il2CppType t2639_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14254_GM;
MethodInfo m14254_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2642_TI, &t2639_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14254_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2642_m14255_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14255_GM;
MethodInfo m14255_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2642_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2642_m14255_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14255_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14256_GM;
MethodInfo m14256_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2642_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14256_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2642_m14257_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14257_GM;
MethodInfo m14257_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2642_TI, &t139_0_0_0, RuntimeInvoker_t29_t44, t2642_m14257_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14257_GM};
static MethodInfo* t2642_MIs[] =
{
	&m14228_MI,
	&m14229_MI,
	&m14230_MI,
	&m14231_MI,
	&m14232_MI,
	&m14233_MI,
	&m14234_MI,
	&m14235_MI,
	&m14236_MI,
	&m14237_MI,
	&m14238_MI,
	&m14239_MI,
	&m14240_MI,
	&m14241_MI,
	&m14242_MI,
	&m14243_MI,
	&m14244_MI,
	&m14245_MI,
	&m14246_MI,
	&m14247_MI,
	&m14248_MI,
	&m14249_MI,
	&m14250_MI,
	&m14251_MI,
	&m14252_MI,
	&m14253_MI,
	&m14254_MI,
	&m14255_MI,
	&m14256_MI,
	&m14257_MI,
	NULL
};
extern MethodInfo m14238_MI;
extern MethodInfo m14237_MI;
extern MethodInfo m14239_MI;
extern MethodInfo m14240_MI;
extern MethodInfo m14241_MI;
extern MethodInfo m14242_MI;
extern MethodInfo m14243_MI;
extern MethodInfo m14244_MI;
extern MethodInfo m14245_MI;
extern MethodInfo m14229_MI;
extern MethodInfo m14230_MI;
extern MethodInfo m14252_MI;
extern MethodInfo m14253_MI;
extern MethodInfo m14232_MI;
extern MethodInfo m14255_MI;
extern MethodInfo m14231_MI;
extern MethodInfo m14233_MI;
extern MethodInfo m14254_MI;
static MethodInfo* t2642_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14238_MI,
	&m14256_MI,
	&m14246_MI,
	&m14247_MI,
	&m14237_MI,
	&m14248_MI,
	&m14249_MI,
	&m14250_MI,
	&m14251_MI,
	&m14239_MI,
	&m14240_MI,
	&m14241_MI,
	&m14242_MI,
	&m14243_MI,
	&m14244_MI,
	&m14245_MI,
	&m14256_MI,
	&m14236_MI,
	&m14229_MI,
	&m14230_MI,
	&m14252_MI,
	&m14253_MI,
	&m14232_MI,
	&m14255_MI,
	&m14231_MI,
	&m14233_MI,
	&m14234_MI,
	&m14235_MI,
	&m14254_MI,
	&m14257_MI,
};
static TypeInfo* t2642_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2640_TI,
	&t2646_TI,
	&t2641_TI,
};
static Il2CppInterfaceOffsetPair t2642_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2640_TI, 20},
	{ &t2646_TI, 27},
	{ &t2641_TI, 32},
};
extern TypeInfo t139_TI;
static Il2CppRGCTXData t2642_RGCTXData[9] = 
{
	&m14257_MI/* Method Usage */,
	&m14289_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
	&m27579_MI/* Method Usage */,
	&m27586_MI/* Method Usage */,
	&m27584_MI/* Method Usage */,
	&m27580_MI/* Method Usage */,
	&m27582_MI/* Method Usage */,
	&m27575_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2642_0_0_0;
extern Il2CppType t2642_1_0_0;
struct t2642;
extern Il2CppGenericClass t2642_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2642_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2642_MIs, t2642_PIs, t2642_FIs, NULL, &t29_TI, NULL, NULL, &t2642_TI, t2642_ITIs, t2642_VT, &t1263__CustomAttributeCache, &t2642_TI, &t2642_0_0_0, &t2642_1_0_0, t2642_IOs, &t2642_GC, NULL, NULL, NULL, t2642_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2642), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2647.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2647_TI;

extern MethodInfo m27576_MI;
extern MethodInfo m14292_MI;
extern MethodInfo m14293_MI;
extern MethodInfo m14290_MI;
extern MethodInfo m14288_MI;
extern MethodInfo m1874_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m14281_MI;
extern MethodInfo m14291_MI;
extern MethodInfo m14279_MI;
extern MethodInfo m14284_MI;
extern MethodInfo m14275_MI;
extern MethodInfo m27578_MI;
extern MethodInfo m27587_MI;
extern MethodInfo m27588_MI;
extern MethodInfo m27585_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.UI.Selectable>
extern Il2CppType t2646_0_0_1;
FieldInfo t2647_f0_FieldInfo = 
{
	"list", &t2646_0_0_1, &t2647_TI, offsetof(t2647, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2647_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2647_TI, offsetof(t2647, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2647_FIs[] =
{
	&t2647_f0_FieldInfo,
	&t2647_f1_FieldInfo,
	NULL
};
extern MethodInfo m14259_MI;
static PropertyInfo t2647____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2647_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14259_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14267_MI;
static PropertyInfo t2647____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2647_TI, "System.Collections.ICollection.IsSynchronized", &m14267_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14268_MI;
static PropertyInfo t2647____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2647_TI, "System.Collections.ICollection.SyncRoot", &m14268_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14269_MI;
static PropertyInfo t2647____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2647_TI, "System.Collections.IList.IsFixedSize", &m14269_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14270_MI;
static PropertyInfo t2647____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2647_TI, "System.Collections.IList.IsReadOnly", &m14270_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14271_MI;
extern MethodInfo m14272_MI;
static PropertyInfo t2647____System_Collections_IList_Item_PropertyInfo = 
{
	&t2647_TI, "System.Collections.IList.Item", &m14271_MI, &m14272_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14285_MI;
static PropertyInfo t2647____Count_PropertyInfo = 
{
	&t2647_TI, "Count", &m14285_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14286_MI;
extern MethodInfo m14287_MI;
static PropertyInfo t2647____Item_PropertyInfo = 
{
	&t2647_TI, "Item", &m14286_MI, &m14287_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2647_PIs[] =
{
	&t2647____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2647____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2647____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2647____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2647____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2647____System_Collections_IList_Item_PropertyInfo,
	&t2647____Count_PropertyInfo,
	&t2647____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14258_GM;
MethodInfo m14258_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14258_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14259_GM;
MethodInfo m14259_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2647_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14259_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2647_m14260_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14260_GM;
MethodInfo m14260_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2647_m14260_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14260_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14261_GM;
MethodInfo m14261_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2647_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14261_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2647_m14262_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14262_GM;
MethodInfo m14262_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2647_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2647_m14262_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14262_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2647_m14263_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14263_GM;
MethodInfo m14263_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2647_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2647_m14263_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14263_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2647_m14264_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14264_GM;
MethodInfo m14264_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2647_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2647_m14264_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14264_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2647_m14265_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14265_GM;
MethodInfo m14265_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2647_m14265_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14265_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2647_m14266_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14266_GM;
MethodInfo m14266_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2647_m14266_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14266_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14267_GM;
MethodInfo m14267_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2647_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14267_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14268_GM;
MethodInfo m14268_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2647_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14268_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14269_GM;
MethodInfo m14269_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2647_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14269_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14270_GM;
MethodInfo m14270_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2647_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14270_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2647_m14271_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14271_GM;
MethodInfo m14271_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2647_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2647_m14271_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14271_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2647_m14272_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14272_GM;
MethodInfo m14272_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2647_m14272_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14272_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2647_m14273_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14273_GM;
MethodInfo m14273_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2647_m14273_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14273_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14274_GM;
MethodInfo m14274_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14274_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14275_GM;
MethodInfo m14275_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14275_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2647_m14276_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14276_GM;
MethodInfo m14276_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2647_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2647_m14276_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14276_GM};
extern Il2CppType t2638_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2647_m14277_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2638_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14277_GM;
MethodInfo m14277_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2647_m14277_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14277_GM};
extern Il2CppType t2639_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14278_GM;
MethodInfo m14278_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2647_TI, &t2639_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14278_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2647_m14279_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14279_GM;
MethodInfo m14279_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2647_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2647_m14279_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14279_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2647_m14280_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14280_GM;
MethodInfo m14280_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2647_m14280_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14280_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2647_m14281_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14281_GM;
MethodInfo m14281_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2647_m14281_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14281_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2647_m14282_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14282_GM;
MethodInfo m14282_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2647_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2647_m14282_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14282_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2647_m14283_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14283_GM;
MethodInfo m14283_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2647_m14283_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14283_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2647_m14284_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14284_GM;
MethodInfo m14284_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2647_m14284_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14284_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14285_GM;
MethodInfo m14285_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2647_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14285_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2647_m14286_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14286_GM;
MethodInfo m14286_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2647_TI, &t139_0_0_0, RuntimeInvoker_t29_t44, t2647_m14286_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14286_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2647_m14287_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14287_GM;
MethodInfo m14287_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2647_m14287_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14287_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2647_m14288_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14288_GM;
MethodInfo m14288_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2647_m14288_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14288_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2647_m14289_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14289_GM;
MethodInfo m14289_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2647_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2647_m14289_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14289_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2647_m14290_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t139_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14290_GM;
MethodInfo m14290_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2647_TI, &t139_0_0_0, RuntimeInvoker_t29_t29, t2647_m14290_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14290_GM};
extern Il2CppType t2646_0_0_0;
static ParameterInfo t2647_m14291_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2646_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14291_GM;
MethodInfo m14291_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2647_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2647_m14291_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14291_GM};
extern Il2CppType t2646_0_0_0;
static ParameterInfo t2647_m14292_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2646_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14292_GM;
MethodInfo m14292_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2647_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2647_m14292_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14292_GM};
extern Il2CppType t2646_0_0_0;
static ParameterInfo t2647_m14293_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2646_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14293_GM;
MethodInfo m14293_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2647_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2647_m14293_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14293_GM};
static MethodInfo* t2647_MIs[] =
{
	&m14258_MI,
	&m14259_MI,
	&m14260_MI,
	&m14261_MI,
	&m14262_MI,
	&m14263_MI,
	&m14264_MI,
	&m14265_MI,
	&m14266_MI,
	&m14267_MI,
	&m14268_MI,
	&m14269_MI,
	&m14270_MI,
	&m14271_MI,
	&m14272_MI,
	&m14273_MI,
	&m14274_MI,
	&m14275_MI,
	&m14276_MI,
	&m14277_MI,
	&m14278_MI,
	&m14279_MI,
	&m14280_MI,
	&m14281_MI,
	&m14282_MI,
	&m14283_MI,
	&m14284_MI,
	&m14285_MI,
	&m14286_MI,
	&m14287_MI,
	&m14288_MI,
	&m14289_MI,
	&m14290_MI,
	&m14291_MI,
	&m14292_MI,
	&m14293_MI,
	NULL
};
extern MethodInfo m14261_MI;
extern MethodInfo m14260_MI;
extern MethodInfo m14262_MI;
extern MethodInfo m14274_MI;
extern MethodInfo m14263_MI;
extern MethodInfo m14264_MI;
extern MethodInfo m14265_MI;
extern MethodInfo m14266_MI;
extern MethodInfo m14283_MI;
extern MethodInfo m14273_MI;
extern MethodInfo m14276_MI;
extern MethodInfo m14277_MI;
extern MethodInfo m14282_MI;
extern MethodInfo m14280_MI;
extern MethodInfo m14278_MI;
static MethodInfo* t2647_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14261_MI,
	&m14285_MI,
	&m14267_MI,
	&m14268_MI,
	&m14260_MI,
	&m14269_MI,
	&m14270_MI,
	&m14271_MI,
	&m14272_MI,
	&m14262_MI,
	&m14274_MI,
	&m14263_MI,
	&m14264_MI,
	&m14265_MI,
	&m14266_MI,
	&m14283_MI,
	&m14285_MI,
	&m14259_MI,
	&m14273_MI,
	&m14274_MI,
	&m14276_MI,
	&m14277_MI,
	&m14282_MI,
	&m14279_MI,
	&m14280_MI,
	&m14283_MI,
	&m14286_MI,
	&m14287_MI,
	&m14278_MI,
	&m14275_MI,
	&m14281_MI,
	&m14284_MI,
	&m14288_MI,
};
static TypeInfo* t2647_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2640_TI,
	&t2646_TI,
	&t2641_TI,
};
static Il2CppInterfaceOffsetPair t2647_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2640_TI, 20},
	{ &t2646_TI, 27},
	{ &t2641_TI, 32},
};
extern TypeInfo t226_TI;
extern TypeInfo t139_TI;
static Il2CppRGCTXData t2647_RGCTXData[25] = 
{
	&t226_TI/* Class Usage */,
	&m1874_MI/* Method Usage */,
	&m27576_MI/* Method Usage */,
	&m27582_MI/* Method Usage */,
	&m27575_MI/* Method Usage */,
	&m14290_MI/* Method Usage */,
	&m14281_MI/* Method Usage */,
	&m14289_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
	&m27579_MI/* Method Usage */,
	&m27586_MI/* Method Usage */,
	&m14291_MI/* Method Usage */,
	&m14279_MI/* Method Usage */,
	&m14284_MI/* Method Usage */,
	&m14292_MI/* Method Usage */,
	&m14293_MI/* Method Usage */,
	&m27584_MI/* Method Usage */,
	&m14288_MI/* Method Usage */,
	&m14275_MI/* Method Usage */,
	&m27578_MI/* Method Usage */,
	&m27580_MI/* Method Usage */,
	&m27587_MI/* Method Usage */,
	&m27588_MI/* Method Usage */,
	&m27585_MI/* Method Usage */,
	&t139_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2647_0_0_0;
extern Il2CppType t2647_1_0_0;
struct t2647;
extern Il2CppGenericClass t2647_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2647_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2647_MIs, t2647_PIs, t2647_FIs, NULL, &t29_TI, NULL, NULL, &t2647_TI, t2647_ITIs, t2647_VT, &t1262__CustomAttributeCache, &t2647_TI, &t2647_0_0_0, &t2647_1_0_0, t2647_IOs, &t2647_GC, NULL, NULL, NULL, t2647_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2647), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2648_TI;
#include "t2648MD.h"

#include "t1257.h"
#include "t2649.h"
extern TypeInfo t6700_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t2649_TI;
#include "t931MD.h"
#include "t2649MD.h"
extern Il2CppType t6700_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m14299_MI;
extern MethodInfo m28324_MI;
extern MethodInfo m21367_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Selectable>
extern Il2CppType t2648_0_0_49;
FieldInfo t2648_f0_FieldInfo = 
{
	"_default", &t2648_0_0_49, &t2648_TI, offsetof(t2648_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2648_FIs[] =
{
	&t2648_f0_FieldInfo,
	NULL
};
extern MethodInfo m14298_MI;
static PropertyInfo t2648____Default_PropertyInfo = 
{
	&t2648_TI, "Default", &m14298_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2648_PIs[] =
{
	&t2648____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14294_GM;
MethodInfo m14294_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2648_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14294_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14295_GM;
MethodInfo m14295_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2648_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14295_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2648_m14296_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14296_GM;
MethodInfo m14296_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2648_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2648_m14296_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14296_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2648_m14297_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14297_GM;
MethodInfo m14297_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2648_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2648_m14297_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14297_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2648_m28324_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28324_GM;
MethodInfo m28324_MI = 
{
	"GetHashCode", NULL, &t2648_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2648_m28324_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28324_GM};
extern Il2CppType t139_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2648_m21367_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21367_GM;
MethodInfo m21367_MI = 
{
	"Equals", NULL, &t2648_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2648_m21367_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m21367_GM};
extern Il2CppType t2648_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14298_GM;
MethodInfo m14298_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2648_TI, &t2648_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14298_GM};
static MethodInfo* t2648_MIs[] =
{
	&m14294_MI,
	&m14295_MI,
	&m14296_MI,
	&m14297_MI,
	&m28324_MI,
	&m21367_MI,
	&m14298_MI,
	NULL
};
extern MethodInfo m14297_MI;
extern MethodInfo m14296_MI;
static MethodInfo* t2648_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m21367_MI,
	&m28324_MI,
	&m14297_MI,
	&m14296_MI,
	NULL,
	NULL,
};
extern TypeInfo t6701_TI;
extern TypeInfo t734_TI;
static TypeInfo* t2648_ITIs[] = 
{
	&t6701_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2648_IOs[] = 
{
	{ &t6701_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2648_TI;
extern TypeInfo t2648_TI;
extern TypeInfo t2649_TI;
extern TypeInfo t139_TI;
static Il2CppRGCTXData t2648_RGCTXData[9] = 
{
	&t6700_0_0_0/* Type Usage */,
	&t139_0_0_0/* Type Usage */,
	&t2648_TI/* Class Usage */,
	&t2648_TI/* Static Usage */,
	&t2649_TI/* Class Usage */,
	&m14299_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
	&m28324_MI/* Method Usage */,
	&m21367_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2648_0_0_0;
extern Il2CppType t2648_1_0_0;
struct t2648;
extern Il2CppGenericClass t2648_GC;
TypeInfo t2648_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2648_MIs, t2648_PIs, t2648_FIs, NULL, &t29_TI, NULL, NULL, &t2648_TI, t2648_ITIs, t2648_VT, &EmptyCustomAttributesCache, &t2648_TI, &t2648_0_0_0, &t2648_1_0_0, t2648_IOs, &t2648_GC, NULL, NULL, NULL, t2648_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2648), 0, -1, sizeof(t2648_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.UI.Selectable>
extern Il2CppType t139_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t6701_m28325_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28325_GM;
MethodInfo m28325_MI = 
{
	"Equals", NULL, &t6701_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6701_m28325_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28325_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t6701_m28326_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28326_GM;
MethodInfo m28326_MI = 
{
	"GetHashCode", NULL, &t6701_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6701_m28326_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28326_GM};
static MethodInfo* t6701_MIs[] =
{
	&m28325_MI,
	&m28326_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6701_0_0_0;
extern Il2CppType t6701_1_0_0;
struct t6701;
extern Il2CppGenericClass t6701_GC;
TypeInfo t6701_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6701_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6701_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6701_TI, &t6701_0_0_0, &t6701_1_0_0, NULL, &t6701_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.UI.Selectable>
extern Il2CppType t139_0_0_0;
static ParameterInfo t6700_m28327_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28327_GM;
MethodInfo m28327_MI = 
{
	"Equals", NULL, &t6700_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6700_m28327_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28327_GM};
static MethodInfo* t6700_MIs[] =
{
	&m28327_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6700_1_0_0;
struct t6700;
extern Il2CppGenericClass t6700_GC;
TypeInfo t6700_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6700_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6700_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6700_TI, &t6700_0_0_0, &t6700_1_0_0, NULL, &t6700_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m14294_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Selectable>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14299_GM;
MethodInfo m14299_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2649_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14299_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2649_m14300_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14300_GM;
MethodInfo m14300_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2649_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2649_m14300_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14300_GM};
extern Il2CppType t139_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2649_m14301_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14301_GM;
MethodInfo m14301_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2649_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2649_m14301_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14301_GM};
static MethodInfo* t2649_MIs[] =
{
	&m14299_MI,
	&m14300_MI,
	&m14301_MI,
	NULL
};
extern MethodInfo m14301_MI;
extern MethodInfo m14300_MI;
static MethodInfo* t2649_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14301_MI,
	&m14300_MI,
	&m14297_MI,
	&m14296_MI,
	&m14300_MI,
	&m14301_MI,
};
static Il2CppInterfaceOffsetPair t2649_IOs[] = 
{
	{ &t6701_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2648_TI;
extern TypeInfo t2648_TI;
extern TypeInfo t2649_TI;
extern TypeInfo t139_TI;
extern TypeInfo t139_TI;
static Il2CppRGCTXData t2649_RGCTXData[11] = 
{
	&t6700_0_0_0/* Type Usage */,
	&t139_0_0_0/* Type Usage */,
	&t2648_TI/* Class Usage */,
	&t2648_TI/* Static Usage */,
	&t2649_TI/* Class Usage */,
	&m14299_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
	&m28324_MI/* Method Usage */,
	&m21367_MI/* Method Usage */,
	&m14294_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2649_0_0_0;
extern Il2CppType t2649_1_0_0;
struct t2649;
extern Il2CppGenericClass t2649_GC;
extern TypeInfo t1256_TI;
TypeInfo t2649_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2649_MIs, NULL, NULL, NULL, &t2648_TI, NULL, &t1256_TI, &t2649_TI, NULL, t2649_VT, &EmptyCustomAttributesCache, &t2649_TI, &t2649_0_0_0, &t2649_1_0_0, t2649_IOs, &t2649_GC, NULL, NULL, NULL, t2649_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2649), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.UI.Selectable>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2643_m14302_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14302_GM;
MethodInfo m14302_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2643_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2643_m14302_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14302_GM};
extern Il2CppType t139_0_0_0;
static ParameterInfo t2643_m14303_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14303_GM;
MethodInfo m14303_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2643_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2643_m14303_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14303_GM};
extern Il2CppType t139_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2643_m14304_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14304_GM;
MethodInfo m14304_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2643_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2643_m14304_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14304_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2643_m14305_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14305_GM;
MethodInfo m14305_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2643_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2643_m14305_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14305_GM};
static MethodInfo* t2643_MIs[] =
{
	&m14302_MI,
	&m14303_MI,
	&m14304_MI,
	&m14305_MI,
	NULL
};
extern MethodInfo m14304_MI;
extern MethodInfo m14305_MI;
static MethodInfo* t2643_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14303_MI,
	&m14304_MI,
	&m14305_MI,
};
static Il2CppInterfaceOffsetPair t2643_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2643_1_0_0;
struct t2643;
extern Il2CppGenericClass t2643_GC;
TypeInfo t2643_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2643_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2643_TI, NULL, t2643_VT, &EmptyCustomAttributesCache, &t2643_TI, &t2643_0_0_0, &t2643_1_0_0, t2643_IOs, &t2643_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2643), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t1247.h"
#include "t2651.h"
extern TypeInfo t4226_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t2651_TI;
#include "t2651MD.h"
extern Il2CppType t4226_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m14310_MI;
extern MethodInfo m28328_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.UI.Selectable>
extern Il2CppType t2650_0_0_49;
FieldInfo t2650_f0_FieldInfo = 
{
	"_default", &t2650_0_0_49, &t2650_TI, offsetof(t2650_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2650_FIs[] =
{
	&t2650_f0_FieldInfo,
	NULL
};
static PropertyInfo t2650____Default_PropertyInfo = 
{
	&t2650_TI, "Default", &m14309_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2650_PIs[] =
{
	&t2650____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14306_GM;
MethodInfo m14306_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2650_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14306_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14307_GM;
MethodInfo m14307_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2650_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14307_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2650_m14308_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14308_GM;
MethodInfo m14308_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2650_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2650_m14308_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14308_GM};
extern Il2CppType t139_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2650_m28328_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28328_GM;
MethodInfo m28328_MI = 
{
	"Compare", NULL, &t2650_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2650_m28328_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28328_GM};
extern Il2CppType t2650_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14309_GM;
MethodInfo m14309_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2650_TI, &t2650_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14309_GM};
static MethodInfo* t2650_MIs[] =
{
	&m14306_MI,
	&m14307_MI,
	&m14308_MI,
	&m28328_MI,
	&m14309_MI,
	NULL
};
extern MethodInfo m14308_MI;
static MethodInfo* t2650_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m28328_MI,
	&m14308_MI,
	NULL,
};
extern TypeInfo t4225_TI;
extern TypeInfo t726_TI;
static TypeInfo* t2650_ITIs[] = 
{
	&t4225_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2650_IOs[] = 
{
	{ &t4225_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2650_TI;
extern TypeInfo t2650_TI;
extern TypeInfo t2651_TI;
extern TypeInfo t139_TI;
static Il2CppRGCTXData t2650_RGCTXData[8] = 
{
	&t4226_0_0_0/* Type Usage */,
	&t139_0_0_0/* Type Usage */,
	&t2650_TI/* Class Usage */,
	&t2650_TI/* Static Usage */,
	&t2651_TI/* Class Usage */,
	&m14310_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
	&m28328_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2650_0_0_0;
extern Il2CppType t2650_1_0_0;
struct t2650;
extern Il2CppGenericClass t2650_GC;
TypeInfo t2650_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2650_MIs, t2650_PIs, t2650_FIs, NULL, &t29_TI, NULL, NULL, &t2650_TI, t2650_ITIs, t2650_VT, &EmptyCustomAttributesCache, &t2650_TI, &t2650_0_0_0, &t2650_1_0_0, t2650_IOs, &t2650_GC, NULL, NULL, NULL, t2650_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2650), 0, -1, sizeof(t2650_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.UI.Selectable>
extern Il2CppType t139_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t4225_m21375_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21375_GM;
MethodInfo m21375_MI = 
{
	"Compare", NULL, &t4225_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4225_m21375_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m21375_GM};
static MethodInfo* t4225_MIs[] =
{
	&m21375_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4225_0_0_0;
extern Il2CppType t4225_1_0_0;
struct t4225;
extern Il2CppGenericClass t4225_GC;
TypeInfo t4225_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4225_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4225_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4225_TI, &t4225_0_0_0, &t4225_1_0_0, NULL, &t4225_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.UI.Selectable>
extern Il2CppType t139_0_0_0;
static ParameterInfo t4226_m21376_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21376_GM;
MethodInfo m21376_MI = 
{
	"CompareTo", NULL, &t4226_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4226_m21376_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m21376_GM};
static MethodInfo* t4226_MIs[] =
{
	&m21376_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4226_1_0_0;
struct t4226;
extern Il2CppGenericClass t4226_GC;
TypeInfo t4226_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4226_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4226_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4226_TI, &t4226_0_0_0, &t4226_1_0_0, NULL, &t4226_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m14306_MI;
extern MethodInfo m21376_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UI.Selectable>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14310_GM;
MethodInfo m14310_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2651_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14310_GM};
extern Il2CppType t139_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2651_m14311_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14311_GM;
MethodInfo m14311_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2651_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2651_m14311_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14311_GM};
static MethodInfo* t2651_MIs[] =
{
	&m14310_MI,
	&m14311_MI,
	NULL
};
extern MethodInfo m14311_MI;
static MethodInfo* t2651_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14311_MI,
	&m14308_MI,
	&m14311_MI,
};
static Il2CppInterfaceOffsetPair t2651_IOs[] = 
{
	{ &t4225_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2650_TI;
extern TypeInfo t2650_TI;
extern TypeInfo t2651_TI;
extern TypeInfo t139_TI;
extern TypeInfo t139_TI;
extern TypeInfo t4226_TI;
static Il2CppRGCTXData t2651_RGCTXData[12] = 
{
	&t4226_0_0_0/* Type Usage */,
	&t139_0_0_0/* Type Usage */,
	&t2650_TI/* Class Usage */,
	&t2650_TI/* Static Usage */,
	&t2651_TI/* Class Usage */,
	&m14310_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
	&m28328_MI/* Method Usage */,
	&m14306_MI/* Method Usage */,
	&t139_TI/* Class Usage */,
	&t4226_TI/* Class Usage */,
	&m21376_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2651_0_0_0;
extern Il2CppType t2651_1_0_0;
struct t2651;
extern Il2CppGenericClass t2651_GC;
extern TypeInfo t1246_TI;
TypeInfo t2651_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2651_MIs, NULL, NULL, NULL, &t2650_TI, NULL, &t1246_TI, &t2651_TI, NULL, t2651_VT, &EmptyCustomAttributesCache, &t2651_TI, &t2651_0_0_0, &t2651_1_0_0, t2651_IOs, &t2651_GC, NULL, NULL, NULL, t2651_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2651), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2644_TI;
#include "t2644MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.UI.Selectable>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2644_m14312_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14312_GM;
MethodInfo m14312_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2644_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2644_m14312_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14312_GM};
extern Il2CppType t139_0_0_0;
extern Il2CppType t139_0_0_0;
static ParameterInfo t2644_m14313_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14313_GM;
MethodInfo m14313_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2644_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2644_m14313_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14313_GM};
extern Il2CppType t139_0_0_0;
extern Il2CppType t139_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2644_m14314_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t139_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14314_GM;
MethodInfo m14314_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2644_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2644_m14314_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m14314_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2644_m14315_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14315_GM;
MethodInfo m14315_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2644_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2644_m14315_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14315_GM};
static MethodInfo* t2644_MIs[] =
{
	&m14312_MI,
	&m14313_MI,
	&m14314_MI,
	&m14315_MI,
	NULL
};
extern MethodInfo m14313_MI;
extern MethodInfo m14314_MI;
extern MethodInfo m14315_MI;
static MethodInfo* t2644_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14313_MI,
	&m14314_MI,
	&m14315_MI,
};
static Il2CppInterfaceOffsetPair t2644_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2644_1_0_0;
struct t2644;
extern Il2CppGenericClass t2644_GC;
TypeInfo t2644_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2644_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2644_TI, NULL, t2644_VT, &EmptyCustomAttributesCache, &t2644_TI, &t2644_0_0_0, &t2644_1_0_0, t2644_IOs, &t2644_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2644), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t227.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t227_TI;
#include "t227MD.h"

#include "t352.h"
#include "t2659.h"
#include "t2656.h"
#include "t2657.h"
#include "t2665.h"
#include "t2658.h"
extern TypeInfo t352_TI;
extern TypeInfo t2652_TI;
extern TypeInfo t2659_TI;
extern TypeInfo t2654_TI;
extern TypeInfo t2655_TI;
extern TypeInfo t2653_TI;
extern TypeInfo t2656_TI;
extern TypeInfo t2657_TI;
extern TypeInfo t2665_TI;
#include "t2656MD.h"
#include "t2657MD.h"
#include "t2659MD.h"
#include "t2665MD.h"
extern MethodInfo m1877_MI;
extern MethodInfo m14361_MI;
extern MethodInfo m21392_MI;
extern MethodInfo m14348_MI;
extern MethodInfo m14345_MI;
extern MethodInfo m14333_MI;
extern MethodInfo m14340_MI;
extern MethodInfo m14346_MI;
extern MethodInfo m14349_MI;
extern MethodInfo m14351_MI;
extern MethodInfo m14334_MI;
extern MethodInfo m14359_MI;
extern MethodInfo m14360_MI;
extern MethodInfo m28329_MI;
extern MethodInfo m28330_MI;
extern MethodInfo m28331_MI;
extern MethodInfo m28332_MI;
extern MethodInfo m14350_MI;
extern MethodInfo m14335_MI;
extern MethodInfo m14336_MI;
extern MethodInfo m14373_MI;
extern MethodInfo m21394_MI;
extern MethodInfo m14343_MI;
extern MethodInfo m14344_MI;
extern MethodInfo m14448_MI;
extern MethodInfo m14367_MI;
extern MethodInfo m14347_MI;
extern MethodInfo m14353_MI;
extern MethodInfo m14454_MI;
extern MethodInfo m21396_MI;
extern MethodInfo m21404_MI;
struct t20;
#define m21392(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2663.h"
#define m21394(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
#define m21396(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#define m21404(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2659  m14345 (t227 * __this, MethodInfo* method){
	{
		t2659  L_0 = {0};
		m14367(&L_0, __this, &m14367_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
extern Il2CppType t44_0_0_32849;
FieldInfo t227_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t227_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2652_0_0_1;
FieldInfo t227_f1_FieldInfo = 
{
	"_items", &t2652_0_0_1, &t227_TI, offsetof(t227, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t227_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t227_TI, offsetof(t227, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t227_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t227_TI, offsetof(t227, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2652_0_0_49;
FieldInfo t227_f4_FieldInfo = 
{
	"EmptyArray", &t2652_0_0_49, &t227_TI, offsetof(t227_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t227_FIs[] =
{
	&t227_f0_FieldInfo,
	&t227_f1_FieldInfo,
	&t227_f2_FieldInfo,
	&t227_f3_FieldInfo,
	&t227_f4_FieldInfo,
	NULL
};
static const int32_t t227_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t227_f0_DefaultValue = 
{
	&t227_f0_FieldInfo, { (char*)&t227_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t227_FDVs[] = 
{
	&t227_f0_DefaultValue,
	NULL
};
extern MethodInfo m14326_MI;
static PropertyInfo t227____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t227_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14326_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14327_MI;
static PropertyInfo t227____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t227_TI, "System.Collections.ICollection.IsSynchronized", &m14327_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14328_MI;
static PropertyInfo t227____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t227_TI, "System.Collections.ICollection.SyncRoot", &m14328_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14329_MI;
static PropertyInfo t227____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t227_TI, "System.Collections.IList.IsFixedSize", &m14329_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14330_MI;
static PropertyInfo t227____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t227_TI, "System.Collections.IList.IsReadOnly", &m14330_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14331_MI;
extern MethodInfo m14332_MI;
static PropertyInfo t227____System_Collections_IList_Item_PropertyInfo = 
{
	&t227_TI, "System.Collections.IList.Item", &m14331_MI, &m14332_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t227____Capacity_PropertyInfo = 
{
	&t227_TI, "Capacity", &m14359_MI, &m14360_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1879_MI;
static PropertyInfo t227____Count_PropertyInfo = 
{
	&t227_TI, "Count", &m1879_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t227____Item_PropertyInfo = 
{
	&t227_TI, "Item", &m1877_MI, &m14361_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t227_PIs[] =
{
	&t227____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t227____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t227____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t227____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t227____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t227____System_Collections_IList_Item_PropertyInfo,
	&t227____Capacity_PropertyInfo,
	&t227____Count_PropertyInfo,
	&t227____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1873_GM;
MethodInfo m1873_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1873_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t227_m14316_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14316_GM;
MethodInfo m14316_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t227_m14316_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14316_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14317_GM;
MethodInfo m14317_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14317_GM};
extern Il2CppType t2653_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14318_GM;
MethodInfo m14318_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t227_TI, &t2653_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14318_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t227_m14319_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14319_GM;
MethodInfo m14319_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t227_m14319_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14319_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14320_GM;
MethodInfo m14320_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t227_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14320_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t227_m14321_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14321_GM;
MethodInfo m14321_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t227_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t227_m14321_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14321_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t227_m14322_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14322_GM;
MethodInfo m14322_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t227_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t227_m14322_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14322_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t227_m14323_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14323_GM;
MethodInfo m14323_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t227_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t227_m14323_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14323_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t227_m14324_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14324_GM;
MethodInfo m14324_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t227_m14324_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14324_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t227_m14325_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14325_GM;
MethodInfo m14325_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t227_m14325_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14325_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14326_GM;
MethodInfo m14326_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t227_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14326_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14327_GM;
MethodInfo m14327_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t227_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14327_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14328_GM;
MethodInfo m14328_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t227_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14328_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14329_GM;
MethodInfo m14329_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t227_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14329_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14330_GM;
MethodInfo m14330_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t227_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14330_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t227_m14331_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14331_GM;
MethodInfo m14331_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t227_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t227_m14331_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14331_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t227_m14332_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14332_GM;
MethodInfo m14332_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t227_m14332_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14332_GM};
extern Il2CppType t352_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t227_m14333_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14333_GM;
MethodInfo m14333_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t227_m14333_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14333_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t227_m14334_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14334_GM;
MethodInfo m14334_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t227_m14334_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14334_GM};
extern Il2CppType t2654_0_0_0;
extern Il2CppType t2654_0_0_0;
static ParameterInfo t227_m14335_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2654_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14335_GM;
MethodInfo m14335_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t227_m14335_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14335_GM};
extern Il2CppType t2655_0_0_0;
extern Il2CppType t2655_0_0_0;
static ParameterInfo t227_m14336_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2655_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14336_GM;
MethodInfo m14336_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t227_m14336_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14336_GM};
extern Il2CppType t2655_0_0_0;
static ParameterInfo t227_m14337_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2655_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14337_GM;
MethodInfo m14337_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t227_m14337_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14337_GM};
extern Il2CppType t2656_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14338_GM;
MethodInfo m14338_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t227_TI, &t2656_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14338_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14339_GM;
MethodInfo m14339_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14339_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t227_m14340_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14340_GM;
MethodInfo m14340_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t227_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t227_m14340_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14340_GM};
extern Il2CppType t2652_0_0_0;
extern Il2CppType t2652_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t227_m14341_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2652_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14341_GM;
MethodInfo m14341_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t227_m14341_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14341_GM};
extern Il2CppType t2657_0_0_0;
extern Il2CppType t2657_0_0_0;
static ParameterInfo t227_m14342_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2657_0_0_0},
};
extern Il2CppType t352_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14342_GM;
MethodInfo m14342_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t227_TI, &t352_0_0_0, RuntimeInvoker_t29_t29, t227_m14342_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14342_GM};
extern Il2CppType t2657_0_0_0;
static ParameterInfo t227_m14343_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2657_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14343_GM;
MethodInfo m14343_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t227_m14343_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14343_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2657_0_0_0;
static ParameterInfo t227_m14344_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2657_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14344_GM;
MethodInfo m14344_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t227_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t227_m14344_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14344_GM};
extern Il2CppType t2659_0_0_0;
extern void* RuntimeInvoker_t2659 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14345_GM;
MethodInfo m14345_MI = 
{
	"GetEnumerator", (methodPointerType)&m14345, &t227_TI, &t2659_0_0_0, RuntimeInvoker_t2659, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14345_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t227_m14346_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14346_GM;
MethodInfo m14346_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t227_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t227_m14346_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14346_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t227_m14347_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14347_GM;
MethodInfo m14347_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t227_m14347_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14347_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t227_m14348_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14348_GM;
MethodInfo m14348_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t227_m14348_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14348_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t227_m14349_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14349_GM;
MethodInfo m14349_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t227_m14349_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14349_GM};
extern Il2CppType t2655_0_0_0;
static ParameterInfo t227_m14350_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2655_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14350_GM;
MethodInfo m14350_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t227_m14350_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14350_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t227_m14351_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14351_GM;
MethodInfo m14351_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t227_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t227_m14351_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14351_GM};
extern Il2CppType t2657_0_0_0;
static ParameterInfo t227_m14352_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2657_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14352_GM;
MethodInfo m14352_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t227_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t227_m14352_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14352_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t227_m14353_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14353_GM;
MethodInfo m14353_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t227_m14353_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14353_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14354_GM;
MethodInfo m14354_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14354_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14355_GM;
MethodInfo m14355_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14355_GM};
extern Il2CppType t2658_0_0_0;
extern Il2CppType t2658_0_0_0;
static ParameterInfo t227_m14356_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2658_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14356_GM;
MethodInfo m14356_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t227_m14356_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14356_GM};
extern Il2CppType t2652_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14357_GM;
MethodInfo m14357_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t227_TI, &t2652_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14357_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14358_GM;
MethodInfo m14358_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14358_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14359_GM;
MethodInfo m14359_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t227_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14359_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t227_m14360_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14360_GM;
MethodInfo m14360_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t227_m14360_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14360_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1879_GM;
MethodInfo m1879_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t227_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1879_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t227_m1877_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t352_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1877_GM;
MethodInfo m1877_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t227_TI, &t352_0_0_0, RuntimeInvoker_t29_t44, t227_m1877_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1877_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t227_m14361_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14361_GM;
MethodInfo m14361_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t227_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t227_m14361_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14361_GM};
static MethodInfo* t227_MIs[] =
{
	&m1873_MI,
	&m14316_MI,
	&m14317_MI,
	&m14318_MI,
	&m14319_MI,
	&m14320_MI,
	&m14321_MI,
	&m14322_MI,
	&m14323_MI,
	&m14324_MI,
	&m14325_MI,
	&m14326_MI,
	&m14327_MI,
	&m14328_MI,
	&m14329_MI,
	&m14330_MI,
	&m14331_MI,
	&m14332_MI,
	&m14333_MI,
	&m14334_MI,
	&m14335_MI,
	&m14336_MI,
	&m14337_MI,
	&m14338_MI,
	&m14339_MI,
	&m14340_MI,
	&m14341_MI,
	&m14342_MI,
	&m14343_MI,
	&m14344_MI,
	&m14345_MI,
	&m14346_MI,
	&m14347_MI,
	&m14348_MI,
	&m14349_MI,
	&m14350_MI,
	&m14351_MI,
	&m14352_MI,
	&m14353_MI,
	&m14354_MI,
	&m14355_MI,
	&m14356_MI,
	&m14357_MI,
	&m14358_MI,
	&m14359_MI,
	&m14360_MI,
	&m1879_MI,
	&m1877_MI,
	&m14361_MI,
	NULL
};
extern MethodInfo m14320_MI;
extern MethodInfo m14319_MI;
extern MethodInfo m14321_MI;
extern MethodInfo m14339_MI;
extern MethodInfo m14322_MI;
extern MethodInfo m14323_MI;
extern MethodInfo m14324_MI;
extern MethodInfo m14325_MI;
extern MethodInfo m14341_MI;
extern MethodInfo m14318_MI;
static MethodInfo* t227_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14320_MI,
	&m1879_MI,
	&m14327_MI,
	&m14328_MI,
	&m14319_MI,
	&m14329_MI,
	&m14330_MI,
	&m14331_MI,
	&m14332_MI,
	&m14321_MI,
	&m14339_MI,
	&m14322_MI,
	&m14323_MI,
	&m14324_MI,
	&m14325_MI,
	&m14353_MI,
	&m1879_MI,
	&m14326_MI,
	&m14333_MI,
	&m14339_MI,
	&m14340_MI,
	&m14341_MI,
	&m14351_MI,
	&m14318_MI,
	&m14346_MI,
	&m14349_MI,
	&m14353_MI,
	&m1877_MI,
	&m14361_MI,
};
extern TypeInfo t2661_TI;
static TypeInfo* t227_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2654_TI,
	&t2655_TI,
	&t2661_TI,
};
static Il2CppInterfaceOffsetPair t227_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2654_TI, 20},
	{ &t2655_TI, 27},
	{ &t2661_TI, 28},
};
extern TypeInfo t227_TI;
extern TypeInfo t2652_TI;
extern TypeInfo t2659_TI;
extern TypeInfo t352_TI;
extern TypeInfo t2654_TI;
extern TypeInfo t2656_TI;
static Il2CppRGCTXData t227_RGCTXData[37] = 
{
	&t227_TI/* Static Usage */,
	&t2652_TI/* Array Usage */,
	&m14345_MI/* Method Usage */,
	&t2659_TI/* Class Usage */,
	&t352_TI/* Class Usage */,
	&m14333_MI/* Method Usage */,
	&m14340_MI/* Method Usage */,
	&m14346_MI/* Method Usage */,
	&m14348_MI/* Method Usage */,
	&m14349_MI/* Method Usage */,
	&m14351_MI/* Method Usage */,
	&m1877_MI/* Method Usage */,
	&m14361_MI/* Method Usage */,
	&m14334_MI/* Method Usage */,
	&m14359_MI/* Method Usage */,
	&m14360_MI/* Method Usage */,
	&m28329_MI/* Method Usage */,
	&m28330_MI/* Method Usage */,
	&m28331_MI/* Method Usage */,
	&m28332_MI/* Method Usage */,
	&m14350_MI/* Method Usage */,
	&t2654_TI/* Class Usage */,
	&m14335_MI/* Method Usage */,
	&m14336_MI/* Method Usage */,
	&t2656_TI/* Class Usage */,
	&m14373_MI/* Method Usage */,
	&m21394_MI/* Method Usage */,
	&m14343_MI/* Method Usage */,
	&m14344_MI/* Method Usage */,
	&m14448_MI/* Method Usage */,
	&m14367_MI/* Method Usage */,
	&m14347_MI/* Method Usage */,
	&m14353_MI/* Method Usage */,
	&m14454_MI/* Method Usage */,
	&m21396_MI/* Method Usage */,
	&m21404_MI/* Method Usage */,
	&m21392_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t227_0_0_0;
extern Il2CppType t227_1_0_0;
struct t227;
extern Il2CppGenericClass t227_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t227_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t227_MIs, t227_PIs, t227_FIs, NULL, &t29_TI, NULL, NULL, &t227_TI, t227_ITIs, t227_VT, &t1261__CustomAttributeCache, &t227_TI, &t227_0_0_0, &t227_1_0_0, t227_IOs, &t227_GC, NULL, t227_FDVs, NULL, t227_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t227), 0, -1, sizeof(t227_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.CanvasGroup>
static PropertyInfo t2654____Count_PropertyInfo = 
{
	&t2654_TI, "Count", &m28329_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28333_MI;
static PropertyInfo t2654____IsReadOnly_PropertyInfo = 
{
	&t2654_TI, "IsReadOnly", &m28333_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2654_PIs[] =
{
	&t2654____Count_PropertyInfo,
	&t2654____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28329_GM;
MethodInfo m28329_MI = 
{
	"get_Count", NULL, &t2654_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28329_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28333_GM;
MethodInfo m28333_MI = 
{
	"get_IsReadOnly", NULL, &t2654_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28333_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2654_m28334_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28334_GM;
MethodInfo m28334_MI = 
{
	"Add", NULL, &t2654_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2654_m28334_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28334_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28335_GM;
MethodInfo m28335_MI = 
{
	"Clear", NULL, &t2654_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28335_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2654_m28336_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28336_GM;
MethodInfo m28336_MI = 
{
	"Contains", NULL, &t2654_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2654_m28336_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28336_GM};
extern Il2CppType t2652_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2654_m28330_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2652_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28330_GM;
MethodInfo m28330_MI = 
{
	"CopyTo", NULL, &t2654_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2654_m28330_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28330_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2654_m28337_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28337_GM;
MethodInfo m28337_MI = 
{
	"Remove", NULL, &t2654_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2654_m28337_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28337_GM};
static MethodInfo* t2654_MIs[] =
{
	&m28329_MI,
	&m28333_MI,
	&m28334_MI,
	&m28335_MI,
	&m28336_MI,
	&m28330_MI,
	&m28337_MI,
	NULL
};
static TypeInfo* t2654_ITIs[] = 
{
	&t603_TI,
	&t2655_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2654_1_0_0;
struct t2654;
extern Il2CppGenericClass t2654_GC;
TypeInfo t2654_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2654_MIs, t2654_PIs, NULL, NULL, NULL, NULL, NULL, &t2654_TI, t2654_ITIs, NULL, &EmptyCustomAttributesCache, &t2654_TI, &t2654_0_0_0, &t2654_1_0_0, NULL, &t2654_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.CanvasGroup>
extern Il2CppType t2653_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28331_GM;
MethodInfo m28331_MI = 
{
	"GetEnumerator", NULL, &t2655_TI, &t2653_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28331_GM};
static MethodInfo* t2655_MIs[] =
{
	&m28331_MI,
	NULL
};
static TypeInfo* t2655_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2655_1_0_0;
struct t2655;
extern Il2CppGenericClass t2655_GC;
TypeInfo t2655_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2655_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2655_TI, t2655_ITIs, NULL, &EmptyCustomAttributesCache, &t2655_TI, &t2655_0_0_0, &t2655_1_0_0, NULL, &t2655_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.CanvasGroup>
static PropertyInfo t2653____Current_PropertyInfo = 
{
	&t2653_TI, "Current", &m28332_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2653_PIs[] =
{
	&t2653____Current_PropertyInfo,
	NULL
};
extern Il2CppType t352_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28332_GM;
MethodInfo m28332_MI = 
{
	"get_Current", NULL, &t2653_TI, &t352_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28332_GM};
static MethodInfo* t2653_MIs[] =
{
	&m28332_MI,
	NULL
};
static TypeInfo* t2653_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2653_0_0_0;
extern Il2CppType t2653_1_0_0;
struct t2653;
extern Il2CppGenericClass t2653_GC;
TypeInfo t2653_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2653_MIs, t2653_PIs, NULL, NULL, NULL, NULL, NULL, &t2653_TI, t2653_ITIs, NULL, &EmptyCustomAttributesCache, &t2653_TI, &t2653_0_0_0, &t2653_1_0_0, NULL, &t2653_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2660.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2660_TI;
#include "t2660MD.h"

extern MethodInfo m14366_MI;
extern MethodInfo m21381_MI;
struct t20;
#define m21381(__this, p0, method) (t352 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.CanvasGroup>
extern Il2CppType t20_0_0_1;
FieldInfo t2660_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2660_TI, offsetof(t2660, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2660_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2660_TI, offsetof(t2660, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2660_FIs[] =
{
	&t2660_f0_FieldInfo,
	&t2660_f1_FieldInfo,
	NULL
};
extern MethodInfo m14363_MI;
static PropertyInfo t2660____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2660_TI, "System.Collections.IEnumerator.Current", &m14363_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2660____Current_PropertyInfo = 
{
	&t2660_TI, "Current", &m14366_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2660_PIs[] =
{
	&t2660____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2660____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2660_m14362_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14362_GM;
MethodInfo m14362_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2660_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2660_m14362_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14362_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14363_GM;
MethodInfo m14363_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2660_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14363_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14364_GM;
MethodInfo m14364_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2660_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14364_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14365_GM;
MethodInfo m14365_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2660_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14365_GM};
extern Il2CppType t352_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14366_GM;
MethodInfo m14366_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2660_TI, &t352_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14366_GM};
static MethodInfo* t2660_MIs[] =
{
	&m14362_MI,
	&m14363_MI,
	&m14364_MI,
	&m14365_MI,
	&m14366_MI,
	NULL
};
extern MethodInfo m14365_MI;
extern MethodInfo m14364_MI;
static MethodInfo* t2660_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14363_MI,
	&m14365_MI,
	&m14364_MI,
	&m14366_MI,
};
static TypeInfo* t2660_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2653_TI,
};
static Il2CppInterfaceOffsetPair t2660_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2653_TI, 7},
};
extern TypeInfo t352_TI;
static Il2CppRGCTXData t2660_RGCTXData[3] = 
{
	&m14366_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
	&m21381_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2660_0_0_0;
extern Il2CppType t2660_1_0_0;
extern Il2CppGenericClass t2660_GC;
TypeInfo t2660_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2660_MIs, t2660_PIs, t2660_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2660_TI, t2660_ITIs, t2660_VT, &EmptyCustomAttributesCache, &t2660_TI, &t2660_0_0_0, &t2660_1_0_0, t2660_IOs, &t2660_GC, NULL, NULL, NULL, t2660_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2660)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.CanvasGroup>
extern MethodInfo m28338_MI;
extern MethodInfo m28339_MI;
static PropertyInfo t2661____Item_PropertyInfo = 
{
	&t2661_TI, "Item", &m28338_MI, &m28339_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2661_PIs[] =
{
	&t2661____Item_PropertyInfo,
	NULL
};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2661_m28340_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28340_GM;
MethodInfo m28340_MI = 
{
	"IndexOf", NULL, &t2661_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2661_m28340_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28340_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2661_m28341_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28341_GM;
MethodInfo m28341_MI = 
{
	"Insert", NULL, &t2661_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2661_m28341_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28341_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2661_m28342_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28342_GM;
MethodInfo m28342_MI = 
{
	"RemoveAt", NULL, &t2661_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2661_m28342_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28342_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2661_m28338_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t352_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28338_GM;
MethodInfo m28338_MI = 
{
	"get_Item", NULL, &t2661_TI, &t352_0_0_0, RuntimeInvoker_t29_t44, t2661_m28338_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28338_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2661_m28339_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28339_GM;
MethodInfo m28339_MI = 
{
	"set_Item", NULL, &t2661_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2661_m28339_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28339_GM};
static MethodInfo* t2661_MIs[] =
{
	&m28340_MI,
	&m28341_MI,
	&m28342_MI,
	&m28338_MI,
	&m28339_MI,
	NULL
};
static TypeInfo* t2661_ITIs[] = 
{
	&t603_TI,
	&t2654_TI,
	&t2655_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2661_0_0_0;
extern Il2CppType t2661_1_0_0;
struct t2661;
extern Il2CppGenericClass t2661_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2661_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2661_MIs, t2661_PIs, NULL, NULL, NULL, NULL, NULL, &t2661_TI, t2661_ITIs, NULL, &t1908__CustomAttributeCache, &t2661_TI, &t2661_0_0_0, &t2661_1_0_0, NULL, &t2661_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m14370_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>
extern Il2CppType t227_0_0_1;
FieldInfo t2659_f0_FieldInfo = 
{
	"l", &t227_0_0_1, &t2659_TI, offsetof(t2659, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2659_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2659_TI, offsetof(t2659, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2659_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2659_TI, offsetof(t2659, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t352_0_0_1;
FieldInfo t2659_f3_FieldInfo = 
{
	"current", &t352_0_0_1, &t2659_TI, offsetof(t2659, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2659_FIs[] =
{
	&t2659_f0_FieldInfo,
	&t2659_f1_FieldInfo,
	&t2659_f2_FieldInfo,
	&t2659_f3_FieldInfo,
	NULL
};
extern MethodInfo m14368_MI;
static PropertyInfo t2659____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2659_TI, "System.Collections.IEnumerator.Current", &m14368_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14372_MI;
static PropertyInfo t2659____Current_PropertyInfo = 
{
	&t2659_TI, "Current", &m14372_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2659_PIs[] =
{
	&t2659____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2659____Current_PropertyInfo,
	NULL
};
extern Il2CppType t227_0_0_0;
static ParameterInfo t2659_m14367_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t227_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14367_GM;
MethodInfo m14367_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2659_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2659_m14367_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14367_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14368_GM;
MethodInfo m14368_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2659_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14368_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14369_GM;
MethodInfo m14369_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2659_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14369_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14370_GM;
MethodInfo m14370_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2659_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14370_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14371_GM;
MethodInfo m14371_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2659_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14371_GM};
extern Il2CppType t352_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14372_GM;
MethodInfo m14372_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2659_TI, &t352_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14372_GM};
static MethodInfo* t2659_MIs[] =
{
	&m14367_MI,
	&m14368_MI,
	&m14369_MI,
	&m14370_MI,
	&m14371_MI,
	&m14372_MI,
	NULL
};
extern MethodInfo m14371_MI;
extern MethodInfo m14369_MI;
static MethodInfo* t2659_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14368_MI,
	&m14371_MI,
	&m14369_MI,
	&m14372_MI,
};
static TypeInfo* t2659_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2653_TI,
};
static Il2CppInterfaceOffsetPair t2659_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2653_TI, 7},
};
extern TypeInfo t352_TI;
extern TypeInfo t2659_TI;
static Il2CppRGCTXData t2659_RGCTXData[3] = 
{
	&m14370_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
	&t2659_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2659_0_0_0;
extern Il2CppType t2659_1_0_0;
extern Il2CppGenericClass t2659_GC;
TypeInfo t2659_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2659_MIs, t2659_PIs, t2659_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2659_TI, t2659_ITIs, t2659_VT, &EmptyCustomAttributesCache, &t2659_TI, &t2659_0_0_0, &t2659_1_0_0, t2659_IOs, &t2659_GC, NULL, NULL, NULL, t2659_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2659)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t2662MD.h"
extern MethodInfo m14402_MI;
extern MethodInfo m14434_MI;
extern MethodInfo m28336_MI;
extern MethodInfo m28340_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.CanvasGroup>
extern Il2CppType t2661_0_0_1;
FieldInfo t2656_f0_FieldInfo = 
{
	"list", &t2661_0_0_1, &t2656_TI, offsetof(t2656, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2656_FIs[] =
{
	&t2656_f0_FieldInfo,
	NULL
};
extern MethodInfo m14379_MI;
extern MethodInfo m14380_MI;
static PropertyInfo t2656____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2656_TI, "System.Collections.Generic.IList<T>.Item", &m14379_MI, &m14380_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14381_MI;
static PropertyInfo t2656____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2656_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14381_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14391_MI;
static PropertyInfo t2656____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2656_TI, "System.Collections.ICollection.IsSynchronized", &m14391_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14392_MI;
static PropertyInfo t2656____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2656_TI, "System.Collections.ICollection.SyncRoot", &m14392_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14393_MI;
static PropertyInfo t2656____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2656_TI, "System.Collections.IList.IsFixedSize", &m14393_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14394_MI;
static PropertyInfo t2656____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2656_TI, "System.Collections.IList.IsReadOnly", &m14394_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14395_MI;
extern MethodInfo m14396_MI;
static PropertyInfo t2656____System_Collections_IList_Item_PropertyInfo = 
{
	&t2656_TI, "System.Collections.IList.Item", &m14395_MI, &m14396_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14401_MI;
static PropertyInfo t2656____Count_PropertyInfo = 
{
	&t2656_TI, "Count", &m14401_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2656____Item_PropertyInfo = 
{
	&t2656_TI, "Item", &m14402_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2656_PIs[] =
{
	&t2656____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2656____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2656____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2656____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2656____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2656____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2656____System_Collections_IList_Item_PropertyInfo,
	&t2656____Count_PropertyInfo,
	&t2656____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2661_0_0_0;
static ParameterInfo t2656_m14373_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2661_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14373_GM;
MethodInfo m14373_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2656_m14373_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14373_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2656_m14374_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14374_GM;
MethodInfo m14374_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2656_m14374_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14374_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14375_GM;
MethodInfo m14375_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14375_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2656_m14376_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14376_GM;
MethodInfo m14376_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2656_m14376_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14376_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2656_m14377_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14377_GM;
MethodInfo m14377_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2656_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2656_m14377_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14377_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2656_m14378_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14378_GM;
MethodInfo m14378_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2656_m14378_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14378_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2656_m14379_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t352_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14379_GM;
MethodInfo m14379_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2656_TI, &t352_0_0_0, RuntimeInvoker_t29_t44, t2656_m14379_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14379_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2656_m14380_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14380_GM;
MethodInfo m14380_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2656_m14380_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14380_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14381_GM;
MethodInfo m14381_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2656_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14381_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2656_m14382_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14382_GM;
MethodInfo m14382_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2656_m14382_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14382_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14383_GM;
MethodInfo m14383_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2656_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14383_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2656_m14384_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14384_GM;
MethodInfo m14384_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2656_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2656_m14384_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14384_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14385_GM;
MethodInfo m14385_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14385_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2656_m14386_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14386_GM;
MethodInfo m14386_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2656_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2656_m14386_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14386_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2656_m14387_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14387_GM;
MethodInfo m14387_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2656_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2656_m14387_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14387_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2656_m14388_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14388_GM;
MethodInfo m14388_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2656_m14388_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14388_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2656_m14389_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14389_GM;
MethodInfo m14389_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2656_m14389_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14389_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2656_m14390_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14390_GM;
MethodInfo m14390_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2656_m14390_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14390_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14391_GM;
MethodInfo m14391_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2656_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14391_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14392_GM;
MethodInfo m14392_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2656_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14392_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14393_GM;
MethodInfo m14393_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2656_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14393_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14394_GM;
MethodInfo m14394_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2656_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14394_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2656_m14395_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14395_GM;
MethodInfo m14395_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2656_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2656_m14395_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14395_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2656_m14396_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14396_GM;
MethodInfo m14396_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2656_m14396_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14396_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2656_m14397_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14397_GM;
MethodInfo m14397_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2656_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2656_m14397_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14397_GM};
extern Il2CppType t2652_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2656_m14398_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2652_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14398_GM;
MethodInfo m14398_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2656_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2656_m14398_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14398_GM};
extern Il2CppType t2653_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14399_GM;
MethodInfo m14399_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2656_TI, &t2653_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14399_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2656_m14400_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14400_GM;
MethodInfo m14400_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2656_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2656_m14400_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14400_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14401_GM;
MethodInfo m14401_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2656_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14401_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2656_m14402_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t352_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14402_GM;
MethodInfo m14402_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2656_TI, &t352_0_0_0, RuntimeInvoker_t29_t44, t2656_m14402_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14402_GM};
static MethodInfo* t2656_MIs[] =
{
	&m14373_MI,
	&m14374_MI,
	&m14375_MI,
	&m14376_MI,
	&m14377_MI,
	&m14378_MI,
	&m14379_MI,
	&m14380_MI,
	&m14381_MI,
	&m14382_MI,
	&m14383_MI,
	&m14384_MI,
	&m14385_MI,
	&m14386_MI,
	&m14387_MI,
	&m14388_MI,
	&m14389_MI,
	&m14390_MI,
	&m14391_MI,
	&m14392_MI,
	&m14393_MI,
	&m14394_MI,
	&m14395_MI,
	&m14396_MI,
	&m14397_MI,
	&m14398_MI,
	&m14399_MI,
	&m14400_MI,
	&m14401_MI,
	&m14402_MI,
	NULL
};
extern MethodInfo m14383_MI;
extern MethodInfo m14382_MI;
extern MethodInfo m14384_MI;
extern MethodInfo m14385_MI;
extern MethodInfo m14386_MI;
extern MethodInfo m14387_MI;
extern MethodInfo m14388_MI;
extern MethodInfo m14389_MI;
extern MethodInfo m14390_MI;
extern MethodInfo m14374_MI;
extern MethodInfo m14375_MI;
extern MethodInfo m14397_MI;
extern MethodInfo m14398_MI;
extern MethodInfo m14377_MI;
extern MethodInfo m14400_MI;
extern MethodInfo m14376_MI;
extern MethodInfo m14378_MI;
extern MethodInfo m14399_MI;
static MethodInfo* t2656_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14383_MI,
	&m14401_MI,
	&m14391_MI,
	&m14392_MI,
	&m14382_MI,
	&m14393_MI,
	&m14394_MI,
	&m14395_MI,
	&m14396_MI,
	&m14384_MI,
	&m14385_MI,
	&m14386_MI,
	&m14387_MI,
	&m14388_MI,
	&m14389_MI,
	&m14390_MI,
	&m14401_MI,
	&m14381_MI,
	&m14374_MI,
	&m14375_MI,
	&m14397_MI,
	&m14398_MI,
	&m14377_MI,
	&m14400_MI,
	&m14376_MI,
	&m14378_MI,
	&m14379_MI,
	&m14380_MI,
	&m14399_MI,
	&m14402_MI,
};
static TypeInfo* t2656_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2654_TI,
	&t2661_TI,
	&t2655_TI,
};
static Il2CppInterfaceOffsetPair t2656_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2654_TI, 20},
	{ &t2661_TI, 27},
	{ &t2655_TI, 32},
};
extern TypeInfo t352_TI;
static Il2CppRGCTXData t2656_RGCTXData[9] = 
{
	&m14402_MI/* Method Usage */,
	&m14434_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
	&m28336_MI/* Method Usage */,
	&m28340_MI/* Method Usage */,
	&m28338_MI/* Method Usage */,
	&m28330_MI/* Method Usage */,
	&m28331_MI/* Method Usage */,
	&m28329_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2656_0_0_0;
extern Il2CppType t2656_1_0_0;
struct t2656;
extern Il2CppGenericClass t2656_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2656_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2656_MIs, t2656_PIs, t2656_FIs, NULL, &t29_TI, NULL, NULL, &t2656_TI, t2656_ITIs, t2656_VT, &t1263__CustomAttributeCache, &t2656_TI, &t2656_0_0_0, &t2656_1_0_0, t2656_IOs, &t2656_GC, NULL, NULL, NULL, t2656_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2656), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2662.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2662_TI;

extern MethodInfo m14437_MI;
extern MethodInfo m14438_MI;
extern MethodInfo m14435_MI;
extern MethodInfo m14433_MI;
extern MethodInfo m1873_MI;
extern MethodInfo m14426_MI;
extern MethodInfo m14436_MI;
extern MethodInfo m14424_MI;
extern MethodInfo m14429_MI;
extern MethodInfo m14420_MI;
extern MethodInfo m28335_MI;
extern MethodInfo m28341_MI;
extern MethodInfo m28342_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.CanvasGroup>
extern Il2CppType t2661_0_0_1;
FieldInfo t2662_f0_FieldInfo = 
{
	"list", &t2661_0_0_1, &t2662_TI, offsetof(t2662, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2662_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2662_TI, offsetof(t2662, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2662_FIs[] =
{
	&t2662_f0_FieldInfo,
	&t2662_f1_FieldInfo,
	NULL
};
extern MethodInfo m14404_MI;
static PropertyInfo t2662____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2662_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14404_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14412_MI;
static PropertyInfo t2662____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2662_TI, "System.Collections.ICollection.IsSynchronized", &m14412_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14413_MI;
static PropertyInfo t2662____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2662_TI, "System.Collections.ICollection.SyncRoot", &m14413_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14414_MI;
static PropertyInfo t2662____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2662_TI, "System.Collections.IList.IsFixedSize", &m14414_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14415_MI;
static PropertyInfo t2662____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2662_TI, "System.Collections.IList.IsReadOnly", &m14415_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14416_MI;
extern MethodInfo m14417_MI;
static PropertyInfo t2662____System_Collections_IList_Item_PropertyInfo = 
{
	&t2662_TI, "System.Collections.IList.Item", &m14416_MI, &m14417_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14430_MI;
static PropertyInfo t2662____Count_PropertyInfo = 
{
	&t2662_TI, "Count", &m14430_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14431_MI;
extern MethodInfo m14432_MI;
static PropertyInfo t2662____Item_PropertyInfo = 
{
	&t2662_TI, "Item", &m14431_MI, &m14432_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2662_PIs[] =
{
	&t2662____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2662____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2662____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2662____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2662____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2662____System_Collections_IList_Item_PropertyInfo,
	&t2662____Count_PropertyInfo,
	&t2662____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14403_GM;
MethodInfo m14403_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14403_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14404_GM;
MethodInfo m14404_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2662_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14404_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2662_m14405_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14405_GM;
MethodInfo m14405_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2662_m14405_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14405_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14406_GM;
MethodInfo m14406_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2662_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14406_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2662_m14407_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14407_GM;
MethodInfo m14407_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2662_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2662_m14407_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14407_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2662_m14408_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14408_GM;
MethodInfo m14408_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2662_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2662_m14408_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14408_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2662_m14409_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14409_GM;
MethodInfo m14409_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2662_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2662_m14409_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14409_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2662_m14410_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14410_GM;
MethodInfo m14410_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2662_m14410_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14410_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2662_m14411_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14411_GM;
MethodInfo m14411_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2662_m14411_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14411_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14412_GM;
MethodInfo m14412_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2662_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14412_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14413_GM;
MethodInfo m14413_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2662_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14413_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14414_GM;
MethodInfo m14414_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2662_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14414_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14415_GM;
MethodInfo m14415_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2662_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14415_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2662_m14416_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14416_GM;
MethodInfo m14416_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2662_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2662_m14416_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14416_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2662_m14417_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14417_GM;
MethodInfo m14417_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2662_m14417_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14417_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2662_m14418_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14418_GM;
MethodInfo m14418_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2662_m14418_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14418_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14419_GM;
MethodInfo m14419_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14419_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14420_GM;
MethodInfo m14420_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14420_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2662_m14421_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14421_GM;
MethodInfo m14421_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2662_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2662_m14421_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14421_GM};
extern Il2CppType t2652_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2662_m14422_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2652_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14422_GM;
MethodInfo m14422_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2662_m14422_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14422_GM};
extern Il2CppType t2653_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14423_GM;
MethodInfo m14423_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2662_TI, &t2653_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14423_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2662_m14424_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14424_GM;
MethodInfo m14424_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2662_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2662_m14424_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14424_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2662_m14425_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14425_GM;
MethodInfo m14425_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2662_m14425_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14425_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2662_m14426_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14426_GM;
MethodInfo m14426_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2662_m14426_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14426_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2662_m14427_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14427_GM;
MethodInfo m14427_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2662_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2662_m14427_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14427_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2662_m14428_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14428_GM;
MethodInfo m14428_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2662_m14428_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14428_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2662_m14429_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14429_GM;
MethodInfo m14429_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2662_m14429_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14429_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14430_GM;
MethodInfo m14430_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2662_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14430_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2662_m14431_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t352_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14431_GM;
MethodInfo m14431_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2662_TI, &t352_0_0_0, RuntimeInvoker_t29_t44, t2662_m14431_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14431_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2662_m14432_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14432_GM;
MethodInfo m14432_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2662_m14432_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14432_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2662_m14433_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14433_GM;
MethodInfo m14433_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2662_m14433_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14433_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2662_m14434_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14434_GM;
MethodInfo m14434_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2662_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2662_m14434_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14434_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2662_m14435_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t352_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14435_GM;
MethodInfo m14435_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2662_TI, &t352_0_0_0, RuntimeInvoker_t29_t29, t2662_m14435_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14435_GM};
extern Il2CppType t2661_0_0_0;
static ParameterInfo t2662_m14436_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2661_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14436_GM;
MethodInfo m14436_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2662_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2662_m14436_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14436_GM};
extern Il2CppType t2661_0_0_0;
static ParameterInfo t2662_m14437_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2661_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14437_GM;
MethodInfo m14437_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2662_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2662_m14437_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14437_GM};
extern Il2CppType t2661_0_0_0;
static ParameterInfo t2662_m14438_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2661_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14438_GM;
MethodInfo m14438_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2662_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2662_m14438_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14438_GM};
static MethodInfo* t2662_MIs[] =
{
	&m14403_MI,
	&m14404_MI,
	&m14405_MI,
	&m14406_MI,
	&m14407_MI,
	&m14408_MI,
	&m14409_MI,
	&m14410_MI,
	&m14411_MI,
	&m14412_MI,
	&m14413_MI,
	&m14414_MI,
	&m14415_MI,
	&m14416_MI,
	&m14417_MI,
	&m14418_MI,
	&m14419_MI,
	&m14420_MI,
	&m14421_MI,
	&m14422_MI,
	&m14423_MI,
	&m14424_MI,
	&m14425_MI,
	&m14426_MI,
	&m14427_MI,
	&m14428_MI,
	&m14429_MI,
	&m14430_MI,
	&m14431_MI,
	&m14432_MI,
	&m14433_MI,
	&m14434_MI,
	&m14435_MI,
	&m14436_MI,
	&m14437_MI,
	&m14438_MI,
	NULL
};
extern MethodInfo m14406_MI;
extern MethodInfo m14405_MI;
extern MethodInfo m14407_MI;
extern MethodInfo m14419_MI;
extern MethodInfo m14408_MI;
extern MethodInfo m14409_MI;
extern MethodInfo m14410_MI;
extern MethodInfo m14411_MI;
extern MethodInfo m14428_MI;
extern MethodInfo m14418_MI;
extern MethodInfo m14421_MI;
extern MethodInfo m14422_MI;
extern MethodInfo m14427_MI;
extern MethodInfo m14425_MI;
extern MethodInfo m14423_MI;
static MethodInfo* t2662_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14406_MI,
	&m14430_MI,
	&m14412_MI,
	&m14413_MI,
	&m14405_MI,
	&m14414_MI,
	&m14415_MI,
	&m14416_MI,
	&m14417_MI,
	&m14407_MI,
	&m14419_MI,
	&m14408_MI,
	&m14409_MI,
	&m14410_MI,
	&m14411_MI,
	&m14428_MI,
	&m14430_MI,
	&m14404_MI,
	&m14418_MI,
	&m14419_MI,
	&m14421_MI,
	&m14422_MI,
	&m14427_MI,
	&m14424_MI,
	&m14425_MI,
	&m14428_MI,
	&m14431_MI,
	&m14432_MI,
	&m14423_MI,
	&m14420_MI,
	&m14426_MI,
	&m14429_MI,
	&m14433_MI,
};
static TypeInfo* t2662_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2654_TI,
	&t2661_TI,
	&t2655_TI,
};
static Il2CppInterfaceOffsetPair t2662_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2654_TI, 20},
	{ &t2661_TI, 27},
	{ &t2655_TI, 32},
};
extern TypeInfo t227_TI;
extern TypeInfo t352_TI;
static Il2CppRGCTXData t2662_RGCTXData[25] = 
{
	&t227_TI/* Class Usage */,
	&m1873_MI/* Method Usage */,
	&m28333_MI/* Method Usage */,
	&m28331_MI/* Method Usage */,
	&m28329_MI/* Method Usage */,
	&m14435_MI/* Method Usage */,
	&m14426_MI/* Method Usage */,
	&m14434_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
	&m28336_MI/* Method Usage */,
	&m28340_MI/* Method Usage */,
	&m14436_MI/* Method Usage */,
	&m14424_MI/* Method Usage */,
	&m14429_MI/* Method Usage */,
	&m14437_MI/* Method Usage */,
	&m14438_MI/* Method Usage */,
	&m28338_MI/* Method Usage */,
	&m14433_MI/* Method Usage */,
	&m14420_MI/* Method Usage */,
	&m28335_MI/* Method Usage */,
	&m28330_MI/* Method Usage */,
	&m28341_MI/* Method Usage */,
	&m28342_MI/* Method Usage */,
	&m28339_MI/* Method Usage */,
	&t352_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2662_0_0_0;
extern Il2CppType t2662_1_0_0;
struct t2662;
extern Il2CppGenericClass t2662_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2662_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2662_MIs, t2662_PIs, t2662_FIs, NULL, &t29_TI, NULL, NULL, &t2662_TI, t2662_ITIs, t2662_VT, &t1262__CustomAttributeCache, &t2662_TI, &t2662_0_0_0, &t2662_1_0_0, t2662_IOs, &t2662_GC, NULL, NULL, NULL, t2662_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2662), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2663_TI;
#include "t2663MD.h"

#include "t2664.h"
extern TypeInfo t6702_TI;
extern TypeInfo t2664_TI;
#include "t2664MD.h"
extern Il2CppType t6702_0_0_0;
extern MethodInfo m14444_MI;
extern MethodInfo m28343_MI;
extern MethodInfo m21393_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.CanvasGroup>
extern Il2CppType t2663_0_0_49;
FieldInfo t2663_f0_FieldInfo = 
{
	"_default", &t2663_0_0_49, &t2663_TI, offsetof(t2663_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2663_FIs[] =
{
	&t2663_f0_FieldInfo,
	NULL
};
extern MethodInfo m14443_MI;
static PropertyInfo t2663____Default_PropertyInfo = 
{
	&t2663_TI, "Default", &m14443_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2663_PIs[] =
{
	&t2663____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14439_GM;
MethodInfo m14439_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2663_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14439_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14440_GM;
MethodInfo m14440_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2663_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14440_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2663_m14441_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14441_GM;
MethodInfo m14441_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2663_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2663_m14441_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14441_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2663_m14442_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14442_GM;
MethodInfo m14442_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2663_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2663_m14442_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14442_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2663_m28343_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28343_GM;
MethodInfo m28343_MI = 
{
	"GetHashCode", NULL, &t2663_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2663_m28343_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28343_GM};
extern Il2CppType t352_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2663_m21393_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21393_GM;
MethodInfo m21393_MI = 
{
	"Equals", NULL, &t2663_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2663_m21393_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m21393_GM};
extern Il2CppType t2663_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14443_GM;
MethodInfo m14443_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2663_TI, &t2663_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14443_GM};
static MethodInfo* t2663_MIs[] =
{
	&m14439_MI,
	&m14440_MI,
	&m14441_MI,
	&m14442_MI,
	&m28343_MI,
	&m21393_MI,
	&m14443_MI,
	NULL
};
extern MethodInfo m14442_MI;
extern MethodInfo m14441_MI;
static MethodInfo* t2663_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m21393_MI,
	&m28343_MI,
	&m14442_MI,
	&m14441_MI,
	NULL,
	NULL,
};
extern TypeInfo t6703_TI;
static TypeInfo* t2663_ITIs[] = 
{
	&t6703_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2663_IOs[] = 
{
	{ &t6703_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2663_TI;
extern TypeInfo t2663_TI;
extern TypeInfo t2664_TI;
extern TypeInfo t352_TI;
static Il2CppRGCTXData t2663_RGCTXData[9] = 
{
	&t6702_0_0_0/* Type Usage */,
	&t352_0_0_0/* Type Usage */,
	&t2663_TI/* Class Usage */,
	&t2663_TI/* Static Usage */,
	&t2664_TI/* Class Usage */,
	&m14444_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
	&m28343_MI/* Method Usage */,
	&m21393_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2663_0_0_0;
extern Il2CppType t2663_1_0_0;
struct t2663;
extern Il2CppGenericClass t2663_GC;
TypeInfo t2663_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2663_MIs, t2663_PIs, t2663_FIs, NULL, &t29_TI, NULL, NULL, &t2663_TI, t2663_ITIs, t2663_VT, &EmptyCustomAttributesCache, &t2663_TI, &t2663_0_0_0, &t2663_1_0_0, t2663_IOs, &t2663_GC, NULL, NULL, NULL, t2663_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2663), 0, -1, sizeof(t2663_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.CanvasGroup>
extern Il2CppType t352_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t6703_m28344_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28344_GM;
MethodInfo m28344_MI = 
{
	"Equals", NULL, &t6703_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6703_m28344_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28344_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t6703_m28345_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28345_GM;
MethodInfo m28345_MI = 
{
	"GetHashCode", NULL, &t6703_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6703_m28345_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28345_GM};
static MethodInfo* t6703_MIs[] =
{
	&m28344_MI,
	&m28345_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6703_0_0_0;
extern Il2CppType t6703_1_0_0;
struct t6703;
extern Il2CppGenericClass t6703_GC;
TypeInfo t6703_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6703_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6703_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6703_TI, &t6703_0_0_0, &t6703_1_0_0, NULL, &t6703_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.CanvasGroup>
extern Il2CppType t352_0_0_0;
static ParameterInfo t6702_m28346_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28346_GM;
MethodInfo m28346_MI = 
{
	"Equals", NULL, &t6702_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6702_m28346_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28346_GM};
static MethodInfo* t6702_MIs[] =
{
	&m28346_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6702_1_0_0;
struct t6702;
extern Il2CppGenericClass t6702_GC;
TypeInfo t6702_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6702_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6702_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6702_TI, &t6702_0_0_0, &t6702_1_0_0, NULL, &t6702_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m14439_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.CanvasGroup>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14444_GM;
MethodInfo m14444_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2664_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14444_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2664_m14445_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14445_GM;
MethodInfo m14445_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2664_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2664_m14445_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14445_GM};
extern Il2CppType t352_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2664_m14446_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14446_GM;
MethodInfo m14446_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2664_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2664_m14446_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14446_GM};
static MethodInfo* t2664_MIs[] =
{
	&m14444_MI,
	&m14445_MI,
	&m14446_MI,
	NULL
};
extern MethodInfo m14446_MI;
extern MethodInfo m14445_MI;
static MethodInfo* t2664_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14446_MI,
	&m14445_MI,
	&m14442_MI,
	&m14441_MI,
	&m14445_MI,
	&m14446_MI,
};
static Il2CppInterfaceOffsetPair t2664_IOs[] = 
{
	{ &t6703_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2663_TI;
extern TypeInfo t2663_TI;
extern TypeInfo t2664_TI;
extern TypeInfo t352_TI;
extern TypeInfo t352_TI;
static Il2CppRGCTXData t2664_RGCTXData[11] = 
{
	&t6702_0_0_0/* Type Usage */,
	&t352_0_0_0/* Type Usage */,
	&t2663_TI/* Class Usage */,
	&t2663_TI/* Static Usage */,
	&t2664_TI/* Class Usage */,
	&m14444_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
	&m28343_MI/* Method Usage */,
	&m21393_MI/* Method Usage */,
	&m14439_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2664_0_0_0;
extern Il2CppType t2664_1_0_0;
struct t2664;
extern Il2CppGenericClass t2664_GC;
TypeInfo t2664_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2664_MIs, NULL, NULL, NULL, &t2663_TI, NULL, &t1256_TI, &t2664_TI, NULL, t2664_VT, &EmptyCustomAttributesCache, &t2664_TI, &t2664_0_0_0, &t2664_1_0_0, t2664_IOs, &t2664_GC, NULL, NULL, NULL, t2664_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2664), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.CanvasGroup>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2657_m14447_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14447_GM;
MethodInfo m14447_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2657_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2657_m14447_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14447_GM};
extern Il2CppType t352_0_0_0;
static ParameterInfo t2657_m14448_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14448_GM;
MethodInfo m14448_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2657_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2657_m14448_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14448_GM};
extern Il2CppType t352_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2657_m14449_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14449_GM;
MethodInfo m14449_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2657_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2657_m14449_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14449_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2657_m14450_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14450_GM;
MethodInfo m14450_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2657_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2657_m14450_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14450_GM};
static MethodInfo* t2657_MIs[] =
{
	&m14447_MI,
	&m14448_MI,
	&m14449_MI,
	&m14450_MI,
	NULL
};
extern MethodInfo m14449_MI;
extern MethodInfo m14450_MI;
static MethodInfo* t2657_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14448_MI,
	&m14449_MI,
	&m14450_MI,
};
static Il2CppInterfaceOffsetPair t2657_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2657_1_0_0;
struct t2657;
extern Il2CppGenericClass t2657_GC;
TypeInfo t2657_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2657_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2657_TI, NULL, t2657_VT, &EmptyCustomAttributesCache, &t2657_TI, &t2657_0_0_0, &t2657_1_0_0, t2657_IOs, &t2657_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2657), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t2666.h"
extern TypeInfo t4235_TI;
extern TypeInfo t2666_TI;
#include "t2666MD.h"
extern Il2CppType t4235_0_0_0;
extern MethodInfo m14455_MI;
extern MethodInfo m28347_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.CanvasGroup>
extern Il2CppType t2665_0_0_49;
FieldInfo t2665_f0_FieldInfo = 
{
	"_default", &t2665_0_0_49, &t2665_TI, offsetof(t2665_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2665_FIs[] =
{
	&t2665_f0_FieldInfo,
	NULL
};
static PropertyInfo t2665____Default_PropertyInfo = 
{
	&t2665_TI, "Default", &m14454_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2665_PIs[] =
{
	&t2665____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14451_GM;
MethodInfo m14451_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2665_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14451_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14452_GM;
MethodInfo m14452_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2665_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14452_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2665_m14453_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14453_GM;
MethodInfo m14453_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2665_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2665_m14453_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14453_GM};
extern Il2CppType t352_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2665_m28347_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28347_GM;
MethodInfo m28347_MI = 
{
	"Compare", NULL, &t2665_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2665_m28347_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28347_GM};
extern Il2CppType t2665_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14454_GM;
MethodInfo m14454_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2665_TI, &t2665_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14454_GM};
static MethodInfo* t2665_MIs[] =
{
	&m14451_MI,
	&m14452_MI,
	&m14453_MI,
	&m28347_MI,
	&m14454_MI,
	NULL
};
extern MethodInfo m14453_MI;
static MethodInfo* t2665_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m28347_MI,
	&m14453_MI,
	NULL,
};
extern TypeInfo t4234_TI;
static TypeInfo* t2665_ITIs[] = 
{
	&t4234_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2665_IOs[] = 
{
	{ &t4234_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2665_TI;
extern TypeInfo t2665_TI;
extern TypeInfo t2666_TI;
extern TypeInfo t352_TI;
static Il2CppRGCTXData t2665_RGCTXData[8] = 
{
	&t4235_0_0_0/* Type Usage */,
	&t352_0_0_0/* Type Usage */,
	&t2665_TI/* Class Usage */,
	&t2665_TI/* Static Usage */,
	&t2666_TI/* Class Usage */,
	&m14455_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
	&m28347_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2665_0_0_0;
extern Il2CppType t2665_1_0_0;
struct t2665;
extern Il2CppGenericClass t2665_GC;
TypeInfo t2665_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2665_MIs, t2665_PIs, t2665_FIs, NULL, &t29_TI, NULL, NULL, &t2665_TI, t2665_ITIs, t2665_VT, &EmptyCustomAttributesCache, &t2665_TI, &t2665_0_0_0, &t2665_1_0_0, t2665_IOs, &t2665_GC, NULL, NULL, NULL, t2665_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2665), 0, -1, sizeof(t2665_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.CanvasGroup>
extern Il2CppType t352_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t4234_m21401_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21401_GM;
MethodInfo m21401_MI = 
{
	"Compare", NULL, &t4234_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4234_m21401_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m21401_GM};
static MethodInfo* t4234_MIs[] =
{
	&m21401_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4234_0_0_0;
extern Il2CppType t4234_1_0_0;
struct t4234;
extern Il2CppGenericClass t4234_GC;
TypeInfo t4234_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4234_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4234_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4234_TI, &t4234_0_0_0, &t4234_1_0_0, NULL, &t4234_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.CanvasGroup>
extern Il2CppType t352_0_0_0;
static ParameterInfo t4235_m21402_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m21402_GM;
MethodInfo m21402_MI = 
{
	"CompareTo", NULL, &t4235_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4235_m21402_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m21402_GM};
static MethodInfo* t4235_MIs[] =
{
	&m21402_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4235_1_0_0;
struct t4235;
extern Il2CppGenericClass t4235_GC;
TypeInfo t4235_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4235_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4235_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4235_TI, &t4235_0_0_0, &t4235_1_0_0, NULL, &t4235_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m14451_MI;
extern MethodInfo m21402_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.CanvasGroup>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14455_GM;
MethodInfo m14455_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2666_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14455_GM};
extern Il2CppType t352_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2666_m14456_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14456_GM;
MethodInfo m14456_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2666_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2666_m14456_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14456_GM};
static MethodInfo* t2666_MIs[] =
{
	&m14455_MI,
	&m14456_MI,
	NULL
};
extern MethodInfo m14456_MI;
static MethodInfo* t2666_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14456_MI,
	&m14453_MI,
	&m14456_MI,
};
static Il2CppInterfaceOffsetPair t2666_IOs[] = 
{
	{ &t4234_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2665_TI;
extern TypeInfo t2665_TI;
extern TypeInfo t2666_TI;
extern TypeInfo t352_TI;
extern TypeInfo t352_TI;
extern TypeInfo t4235_TI;
static Il2CppRGCTXData t2666_RGCTXData[12] = 
{
	&t4235_0_0_0/* Type Usage */,
	&t352_0_0_0/* Type Usage */,
	&t2665_TI/* Class Usage */,
	&t2665_TI/* Static Usage */,
	&t2666_TI/* Class Usage */,
	&m14455_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
	&m28347_MI/* Method Usage */,
	&m14451_MI/* Method Usage */,
	&t352_TI/* Class Usage */,
	&t4235_TI/* Class Usage */,
	&m21402_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2666_0_0_0;
extern Il2CppType t2666_1_0_0;
struct t2666;
extern Il2CppGenericClass t2666_GC;
TypeInfo t2666_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2666_MIs, NULL, NULL, NULL, &t2665_TI, NULL, &t1246_TI, &t2666_TI, NULL, t2666_VT, &EmptyCustomAttributesCache, &t2666_TI, &t2666_0_0_0, &t2666_1_0_0, t2666_IOs, &t2666_GC, NULL, NULL, NULL, t2666_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2666), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2658_TI;
#include "t2658MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.CanvasGroup>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2658_m14457_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14457_GM;
MethodInfo m14457_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2658_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2658_m14457_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14457_GM};
extern Il2CppType t352_0_0_0;
extern Il2CppType t352_0_0_0;
static ParameterInfo t2658_m14458_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14458_GM;
MethodInfo m14458_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2658_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2658_m14458_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14458_GM};
extern Il2CppType t352_0_0_0;
extern Il2CppType t352_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2658_m14459_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t352_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14459_GM;
MethodInfo m14459_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2658_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2658_m14459_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m14459_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2658_m14460_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14460_GM;
MethodInfo m14460_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2658_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2658_m14460_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14460_GM};
static MethodInfo* t2658_MIs[] =
{
	&m14457_MI,
	&m14458_MI,
	&m14459_MI,
	&m14460_MI,
	NULL
};
extern MethodInfo m14458_MI;
extern MethodInfo m14459_MI;
extern MethodInfo m14460_MI;
static MethodInfo* t2658_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14458_MI,
	&m14459_MI,
	&m14460_MI,
};
static Il2CppInterfaceOffsetPair t2658_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2658_1_0_0;
struct t2658;
extern Il2CppGenericClass t2658_GC;
TypeInfo t2658_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2658_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2658_TI, NULL, t2658_VT, &EmptyCustomAttributesCache, &t2658_TI, &t2658_0_0_0, &t2658_1_0_0, t2658_IOs, &t2658_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2658), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4236_TI;

#include "t225.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Selectable/Transition>
extern MethodInfo m28348_MI;
static PropertyInfo t4236____Current_PropertyInfo = 
{
	&t4236_TI, "Current", &m28348_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4236_PIs[] =
{
	&t4236____Current_PropertyInfo,
	NULL
};
extern Il2CppType t225_0_0_0;
extern void* RuntimeInvoker_t225 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28348_GM;
MethodInfo m28348_MI = 
{
	"get_Current", NULL, &t4236_TI, &t225_0_0_0, RuntimeInvoker_t225, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28348_GM};
static MethodInfo* t4236_MIs[] =
{
	&m28348_MI,
	NULL
};
static TypeInfo* t4236_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4236_0_0_0;
extern Il2CppType t4236_1_0_0;
struct t4236;
extern Il2CppGenericClass t4236_GC;
TypeInfo t4236_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4236_MIs, t4236_PIs, NULL, NULL, NULL, NULL, NULL, &t4236_TI, t4236_ITIs, NULL, &EmptyCustomAttributesCache, &t4236_TI, &t4236_0_0_0, &t4236_1_0_0, NULL, &t4236_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2667.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2667_TI;
#include "t2667MD.h"

extern TypeInfo t225_TI;
extern MethodInfo m14465_MI;
extern MethodInfo m21407_MI;
struct t20;
 int32_t m21407 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14461_MI;
 void m14461 (t2667 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14462_MI;
 t29 * m14462 (t2667 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14465(__this, &m14465_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t225_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14463_MI;
 void m14463 (t2667 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14464_MI;
 bool m14464 (t2667 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14465 (t2667 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21407(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21407_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Selectable/Transition>
extern Il2CppType t20_0_0_1;
FieldInfo t2667_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2667_TI, offsetof(t2667, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2667_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2667_TI, offsetof(t2667, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2667_FIs[] =
{
	&t2667_f0_FieldInfo,
	&t2667_f1_FieldInfo,
	NULL
};
static PropertyInfo t2667____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2667_TI, "System.Collections.IEnumerator.Current", &m14462_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2667____Current_PropertyInfo = 
{
	&t2667_TI, "Current", &m14465_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2667_PIs[] =
{
	&t2667____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2667____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2667_m14461_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14461_GM;
MethodInfo m14461_MI = 
{
	".ctor", (methodPointerType)&m14461, &t2667_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2667_m14461_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14461_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14462_GM;
MethodInfo m14462_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14462, &t2667_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14462_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14463_GM;
MethodInfo m14463_MI = 
{
	"Dispose", (methodPointerType)&m14463, &t2667_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14463_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14464_GM;
MethodInfo m14464_MI = 
{
	"MoveNext", (methodPointerType)&m14464, &t2667_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14464_GM};
extern Il2CppType t225_0_0_0;
extern void* RuntimeInvoker_t225 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14465_GM;
MethodInfo m14465_MI = 
{
	"get_Current", (methodPointerType)&m14465, &t2667_TI, &t225_0_0_0, RuntimeInvoker_t225, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14465_GM};
static MethodInfo* t2667_MIs[] =
{
	&m14461_MI,
	&m14462_MI,
	&m14463_MI,
	&m14464_MI,
	&m14465_MI,
	NULL
};
static MethodInfo* t2667_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14462_MI,
	&m14464_MI,
	&m14463_MI,
	&m14465_MI,
};
static TypeInfo* t2667_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4236_TI,
};
static Il2CppInterfaceOffsetPair t2667_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4236_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2667_0_0_0;
extern Il2CppType t2667_1_0_0;
extern Il2CppGenericClass t2667_GC;
TypeInfo t2667_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2667_MIs, t2667_PIs, t2667_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2667_TI, t2667_ITIs, t2667_VT, &EmptyCustomAttributesCache, &t2667_TI, &t2667_0_0_0, &t2667_1_0_0, t2667_IOs, &t2667_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2667)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5415_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Selectable/Transition>
extern MethodInfo m28349_MI;
static PropertyInfo t5415____Count_PropertyInfo = 
{
	&t5415_TI, "Count", &m28349_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28350_MI;
static PropertyInfo t5415____IsReadOnly_PropertyInfo = 
{
	&t5415_TI, "IsReadOnly", &m28350_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5415_PIs[] =
{
	&t5415____Count_PropertyInfo,
	&t5415____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28349_GM;
MethodInfo m28349_MI = 
{
	"get_Count", NULL, &t5415_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28349_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28350_GM;
MethodInfo m28350_MI = 
{
	"get_IsReadOnly", NULL, &t5415_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28350_GM};
extern Il2CppType t225_0_0_0;
extern Il2CppType t225_0_0_0;
static ParameterInfo t5415_m28351_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t225_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28351_GM;
MethodInfo m28351_MI = 
{
	"Add", NULL, &t5415_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5415_m28351_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28351_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28352_GM;
MethodInfo m28352_MI = 
{
	"Clear", NULL, &t5415_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28352_GM};
extern Il2CppType t225_0_0_0;
static ParameterInfo t5415_m28353_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t225_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28353_GM;
MethodInfo m28353_MI = 
{
	"Contains", NULL, &t5415_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5415_m28353_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28353_GM};
extern Il2CppType t3858_0_0_0;
extern Il2CppType t3858_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5415_m28354_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3858_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28354_GM;
MethodInfo m28354_MI = 
{
	"CopyTo", NULL, &t5415_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5415_m28354_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28354_GM};
extern Il2CppType t225_0_0_0;
static ParameterInfo t5415_m28355_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t225_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28355_GM;
MethodInfo m28355_MI = 
{
	"Remove", NULL, &t5415_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5415_m28355_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28355_GM};
static MethodInfo* t5415_MIs[] =
{
	&m28349_MI,
	&m28350_MI,
	&m28351_MI,
	&m28352_MI,
	&m28353_MI,
	&m28354_MI,
	&m28355_MI,
	NULL
};
extern TypeInfo t5417_TI;
static TypeInfo* t5415_ITIs[] = 
{
	&t603_TI,
	&t5417_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5415_0_0_0;
extern Il2CppType t5415_1_0_0;
struct t5415;
extern Il2CppGenericClass t5415_GC;
TypeInfo t5415_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5415_MIs, t5415_PIs, NULL, NULL, NULL, NULL, NULL, &t5415_TI, t5415_ITIs, NULL, &EmptyCustomAttributesCache, &t5415_TI, &t5415_0_0_0, &t5415_1_0_0, NULL, &t5415_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Selectable/Transition>
extern Il2CppType t4236_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28356_GM;
MethodInfo m28356_MI = 
{
	"GetEnumerator", NULL, &t5417_TI, &t4236_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28356_GM};
static MethodInfo* t5417_MIs[] =
{
	&m28356_MI,
	NULL
};
static TypeInfo* t5417_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5417_0_0_0;
extern Il2CppType t5417_1_0_0;
struct t5417;
extern Il2CppGenericClass t5417_GC;
TypeInfo t5417_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5417_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5417_TI, t5417_ITIs, NULL, &EmptyCustomAttributesCache, &t5417_TI, &t5417_0_0_0, &t5417_1_0_0, NULL, &t5417_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5416_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Selectable/Transition>
extern MethodInfo m28357_MI;
extern MethodInfo m28358_MI;
static PropertyInfo t5416____Item_PropertyInfo = 
{
	&t5416_TI, "Item", &m28357_MI, &m28358_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5416_PIs[] =
{
	&t5416____Item_PropertyInfo,
	NULL
};
extern Il2CppType t225_0_0_0;
static ParameterInfo t5416_m28359_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t225_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28359_GM;
MethodInfo m28359_MI = 
{
	"IndexOf", NULL, &t5416_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5416_m28359_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28359_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t225_0_0_0;
static ParameterInfo t5416_m28360_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t225_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28360_GM;
MethodInfo m28360_MI = 
{
	"Insert", NULL, &t5416_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5416_m28360_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28360_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5416_m28361_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28361_GM;
MethodInfo m28361_MI = 
{
	"RemoveAt", NULL, &t5416_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5416_m28361_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28361_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5416_m28357_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t225_0_0_0;
extern void* RuntimeInvoker_t225_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28357_GM;
MethodInfo m28357_MI = 
{
	"get_Item", NULL, &t5416_TI, &t225_0_0_0, RuntimeInvoker_t225_t44, t5416_m28357_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28357_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t225_0_0_0;
static ParameterInfo t5416_m28358_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t225_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28358_GM;
MethodInfo m28358_MI = 
{
	"set_Item", NULL, &t5416_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5416_m28358_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28358_GM};
static MethodInfo* t5416_MIs[] =
{
	&m28359_MI,
	&m28360_MI,
	&m28361_MI,
	&m28357_MI,
	&m28358_MI,
	NULL
};
static TypeInfo* t5416_ITIs[] = 
{
	&t603_TI,
	&t5415_TI,
	&t5417_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5416_0_0_0;
extern Il2CppType t5416_1_0_0;
struct t5416;
extern Il2CppGenericClass t5416_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5416_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5416_MIs, t5416_PIs, NULL, NULL, NULL, NULL, NULL, &t5416_TI, t5416_ITIs, NULL, &t1908__CustomAttributeCache, &t5416_TI, &t5416_0_0_0, &t5416_1_0_0, NULL, &t5416_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4238_TI;

#include "t207.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Selectable/SelectionState>
extern MethodInfo m28362_MI;
static PropertyInfo t4238____Current_PropertyInfo = 
{
	&t4238_TI, "Current", &m28362_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4238_PIs[] =
{
	&t4238____Current_PropertyInfo,
	NULL
};
extern Il2CppType t207_0_0_0;
extern void* RuntimeInvoker_t207 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28362_GM;
MethodInfo m28362_MI = 
{
	"get_Current", NULL, &t4238_TI, &t207_0_0_0, RuntimeInvoker_t207, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28362_GM};
static MethodInfo* t4238_MIs[] =
{
	&m28362_MI,
	NULL
};
static TypeInfo* t4238_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4238_0_0_0;
extern Il2CppType t4238_1_0_0;
struct t4238;
extern Il2CppGenericClass t4238_GC;
TypeInfo t4238_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4238_MIs, t4238_PIs, NULL, NULL, NULL, NULL, NULL, &t4238_TI, t4238_ITIs, NULL, &EmptyCustomAttributesCache, &t4238_TI, &t4238_0_0_0, &t4238_1_0_0, NULL, &t4238_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2668.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2668_TI;
#include "t2668MD.h"

extern TypeInfo t207_TI;
extern MethodInfo m14470_MI;
extern MethodInfo m21418_MI;
struct t20;
 int32_t m21418 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14466_MI;
 void m14466 (t2668 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14467_MI;
 t29 * m14467 (t2668 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14470(__this, &m14470_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t207_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14468_MI;
 void m14468 (t2668 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14469_MI;
 bool m14469 (t2668 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14470 (t2668 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21418(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21418_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Selectable/SelectionState>
extern Il2CppType t20_0_0_1;
FieldInfo t2668_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2668_TI, offsetof(t2668, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2668_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2668_TI, offsetof(t2668, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2668_FIs[] =
{
	&t2668_f0_FieldInfo,
	&t2668_f1_FieldInfo,
	NULL
};
static PropertyInfo t2668____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2668_TI, "System.Collections.IEnumerator.Current", &m14467_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2668____Current_PropertyInfo = 
{
	&t2668_TI, "Current", &m14470_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2668_PIs[] =
{
	&t2668____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2668____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2668_m14466_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14466_GM;
MethodInfo m14466_MI = 
{
	".ctor", (methodPointerType)&m14466, &t2668_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2668_m14466_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14466_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14467_GM;
MethodInfo m14467_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14467, &t2668_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14467_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14468_GM;
MethodInfo m14468_MI = 
{
	"Dispose", (methodPointerType)&m14468, &t2668_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14468_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14469_GM;
MethodInfo m14469_MI = 
{
	"MoveNext", (methodPointerType)&m14469, &t2668_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14469_GM};
extern Il2CppType t207_0_0_0;
extern void* RuntimeInvoker_t207 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14470_GM;
MethodInfo m14470_MI = 
{
	"get_Current", (methodPointerType)&m14470, &t2668_TI, &t207_0_0_0, RuntimeInvoker_t207, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14470_GM};
static MethodInfo* t2668_MIs[] =
{
	&m14466_MI,
	&m14467_MI,
	&m14468_MI,
	&m14469_MI,
	&m14470_MI,
	NULL
};
static MethodInfo* t2668_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14467_MI,
	&m14469_MI,
	&m14468_MI,
	&m14470_MI,
};
static TypeInfo* t2668_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4238_TI,
};
static Il2CppInterfaceOffsetPair t2668_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4238_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2668_0_0_0;
extern Il2CppType t2668_1_0_0;
extern Il2CppGenericClass t2668_GC;
TypeInfo t2668_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2668_MIs, t2668_PIs, t2668_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2668_TI, t2668_ITIs, t2668_VT, &EmptyCustomAttributesCache, &t2668_TI, &t2668_0_0_0, &t2668_1_0_0, t2668_IOs, &t2668_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2668)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5418_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Selectable/SelectionState>
extern MethodInfo m28363_MI;
static PropertyInfo t5418____Count_PropertyInfo = 
{
	&t5418_TI, "Count", &m28363_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28364_MI;
static PropertyInfo t5418____IsReadOnly_PropertyInfo = 
{
	&t5418_TI, "IsReadOnly", &m28364_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5418_PIs[] =
{
	&t5418____Count_PropertyInfo,
	&t5418____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28363_GM;
MethodInfo m28363_MI = 
{
	"get_Count", NULL, &t5418_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28363_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28364_GM;
MethodInfo m28364_MI = 
{
	"get_IsReadOnly", NULL, &t5418_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28364_GM};
extern Il2CppType t207_0_0_0;
extern Il2CppType t207_0_0_0;
static ParameterInfo t5418_m28365_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t207_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28365_GM;
MethodInfo m28365_MI = 
{
	"Add", NULL, &t5418_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5418_m28365_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28365_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28366_GM;
MethodInfo m28366_MI = 
{
	"Clear", NULL, &t5418_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28366_GM};
extern Il2CppType t207_0_0_0;
static ParameterInfo t5418_m28367_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t207_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28367_GM;
MethodInfo m28367_MI = 
{
	"Contains", NULL, &t5418_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5418_m28367_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28367_GM};
extern Il2CppType t3859_0_0_0;
extern Il2CppType t3859_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5418_m28368_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3859_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28368_GM;
MethodInfo m28368_MI = 
{
	"CopyTo", NULL, &t5418_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5418_m28368_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28368_GM};
extern Il2CppType t207_0_0_0;
static ParameterInfo t5418_m28369_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t207_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28369_GM;
MethodInfo m28369_MI = 
{
	"Remove", NULL, &t5418_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5418_m28369_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28369_GM};
static MethodInfo* t5418_MIs[] =
{
	&m28363_MI,
	&m28364_MI,
	&m28365_MI,
	&m28366_MI,
	&m28367_MI,
	&m28368_MI,
	&m28369_MI,
	NULL
};
extern TypeInfo t5420_TI;
static TypeInfo* t5418_ITIs[] = 
{
	&t603_TI,
	&t5420_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5418_0_0_0;
extern Il2CppType t5418_1_0_0;
struct t5418;
extern Il2CppGenericClass t5418_GC;
TypeInfo t5418_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5418_MIs, t5418_PIs, NULL, NULL, NULL, NULL, NULL, &t5418_TI, t5418_ITIs, NULL, &EmptyCustomAttributesCache, &t5418_TI, &t5418_0_0_0, &t5418_1_0_0, NULL, &t5418_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Selectable/SelectionState>
extern Il2CppType t4238_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28370_GM;
MethodInfo m28370_MI = 
{
	"GetEnumerator", NULL, &t5420_TI, &t4238_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28370_GM};
static MethodInfo* t5420_MIs[] =
{
	&m28370_MI,
	NULL
};
static TypeInfo* t5420_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5420_0_0_0;
extern Il2CppType t5420_1_0_0;
struct t5420;
extern Il2CppGenericClass t5420_GC;
TypeInfo t5420_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5420_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5420_TI, t5420_ITIs, NULL, &EmptyCustomAttributesCache, &t5420_TI, &t5420_0_0_0, &t5420_1_0_0, NULL, &t5420_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5419_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Selectable/SelectionState>
extern MethodInfo m28371_MI;
extern MethodInfo m28372_MI;
static PropertyInfo t5419____Item_PropertyInfo = 
{
	&t5419_TI, "Item", &m28371_MI, &m28372_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5419_PIs[] =
{
	&t5419____Item_PropertyInfo,
	NULL
};
extern Il2CppType t207_0_0_0;
static ParameterInfo t5419_m28373_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t207_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28373_GM;
MethodInfo m28373_MI = 
{
	"IndexOf", NULL, &t5419_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5419_m28373_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28373_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t207_0_0_0;
static ParameterInfo t5419_m28374_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t207_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28374_GM;
MethodInfo m28374_MI = 
{
	"Insert", NULL, &t5419_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5419_m28374_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28374_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5419_m28375_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28375_GM;
MethodInfo m28375_MI = 
{
	"RemoveAt", NULL, &t5419_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5419_m28375_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28375_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5419_m28371_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t207_0_0_0;
extern void* RuntimeInvoker_t207_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28371_GM;
MethodInfo m28371_MI = 
{
	"get_Item", NULL, &t5419_TI, &t207_0_0_0, RuntimeInvoker_t207_t44, t5419_m28371_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28371_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t207_0_0_0;
static ParameterInfo t5419_m28372_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t207_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28372_GM;
MethodInfo m28372_MI = 
{
	"set_Item", NULL, &t5419_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5419_m28372_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28372_GM};
static MethodInfo* t5419_MIs[] =
{
	&m28373_MI,
	&m28374_MI,
	&m28375_MI,
	&m28371_MI,
	&m28372_MI,
	NULL
};
static TypeInfo* t5419_ITIs[] = 
{
	&t603_TI,
	&t5418_TI,
	&t5420_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5419_0_0_0;
extern Il2CppType t5419_1_0_0;
struct t5419;
extern Il2CppGenericClass t5419_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5419_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5419_MIs, t5419_PIs, NULL, NULL, NULL, NULL, NULL, &t5419_TI, t5419_ITIs, NULL, &t1908__CustomAttributeCache, &t5419_TI, &t5419_0_0_0, &t5419_1_0_0, NULL, &t5419_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4240_TI;

#include "t234.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Slider>
extern MethodInfo m28376_MI;
static PropertyInfo t4240____Current_PropertyInfo = 
{
	&t4240_TI, "Current", &m28376_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4240_PIs[] =
{
	&t4240____Current_PropertyInfo,
	NULL
};
extern Il2CppType t234_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28376_GM;
MethodInfo m28376_MI = 
{
	"get_Current", NULL, &t4240_TI, &t234_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28376_GM};
static MethodInfo* t4240_MIs[] =
{
	&m28376_MI,
	NULL
};
static TypeInfo* t4240_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4240_0_0_0;
extern Il2CppType t4240_1_0_0;
struct t4240;
extern Il2CppGenericClass t4240_GC;
TypeInfo t4240_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4240_MIs, t4240_PIs, NULL, NULL, NULL, NULL, NULL, &t4240_TI, t4240_ITIs, NULL, &EmptyCustomAttributesCache, &t4240_TI, &t4240_0_0_0, &t4240_1_0_0, NULL, &t4240_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2669.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2669_TI;
#include "t2669MD.h"

extern TypeInfo t234_TI;
extern MethodInfo m14475_MI;
extern MethodInfo m21429_MI;
struct t20;
#define m21429(__this, p0, method) (t234 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Slider>
extern Il2CppType t20_0_0_1;
FieldInfo t2669_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2669_TI, offsetof(t2669, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2669_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2669_TI, offsetof(t2669, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2669_FIs[] =
{
	&t2669_f0_FieldInfo,
	&t2669_f1_FieldInfo,
	NULL
};
extern MethodInfo m14472_MI;
static PropertyInfo t2669____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2669_TI, "System.Collections.IEnumerator.Current", &m14472_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2669____Current_PropertyInfo = 
{
	&t2669_TI, "Current", &m14475_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2669_PIs[] =
{
	&t2669____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2669____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2669_m14471_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14471_GM;
MethodInfo m14471_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2669_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2669_m14471_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14471_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14472_GM;
MethodInfo m14472_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2669_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14472_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14473_GM;
MethodInfo m14473_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2669_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14473_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14474_GM;
MethodInfo m14474_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2669_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14474_GM};
extern Il2CppType t234_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14475_GM;
MethodInfo m14475_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2669_TI, &t234_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14475_GM};
static MethodInfo* t2669_MIs[] =
{
	&m14471_MI,
	&m14472_MI,
	&m14473_MI,
	&m14474_MI,
	&m14475_MI,
	NULL
};
extern MethodInfo m14474_MI;
extern MethodInfo m14473_MI;
static MethodInfo* t2669_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14472_MI,
	&m14474_MI,
	&m14473_MI,
	&m14475_MI,
};
static TypeInfo* t2669_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4240_TI,
};
static Il2CppInterfaceOffsetPair t2669_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4240_TI, 7},
};
extern TypeInfo t234_TI;
static Il2CppRGCTXData t2669_RGCTXData[3] = 
{
	&m14475_MI/* Method Usage */,
	&t234_TI/* Class Usage */,
	&m21429_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2669_0_0_0;
extern Il2CppType t2669_1_0_0;
extern Il2CppGenericClass t2669_GC;
TypeInfo t2669_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2669_MIs, t2669_PIs, t2669_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2669_TI, t2669_ITIs, t2669_VT, &EmptyCustomAttributesCache, &t2669_TI, &t2669_0_0_0, &t2669_1_0_0, t2669_IOs, &t2669_GC, NULL, NULL, NULL, t2669_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2669)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5421_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Slider>
extern MethodInfo m28377_MI;
static PropertyInfo t5421____Count_PropertyInfo = 
{
	&t5421_TI, "Count", &m28377_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28378_MI;
static PropertyInfo t5421____IsReadOnly_PropertyInfo = 
{
	&t5421_TI, "IsReadOnly", &m28378_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5421_PIs[] =
{
	&t5421____Count_PropertyInfo,
	&t5421____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28377_GM;
MethodInfo m28377_MI = 
{
	"get_Count", NULL, &t5421_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28377_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28378_GM;
MethodInfo m28378_MI = 
{
	"get_IsReadOnly", NULL, &t5421_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28378_GM};
extern Il2CppType t234_0_0_0;
extern Il2CppType t234_0_0_0;
static ParameterInfo t5421_m28379_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t234_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28379_GM;
MethodInfo m28379_MI = 
{
	"Add", NULL, &t5421_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5421_m28379_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28379_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28380_GM;
MethodInfo m28380_MI = 
{
	"Clear", NULL, &t5421_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28380_GM};
extern Il2CppType t234_0_0_0;
static ParameterInfo t5421_m28381_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t234_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28381_GM;
MethodInfo m28381_MI = 
{
	"Contains", NULL, &t5421_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5421_m28381_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28381_GM};
extern Il2CppType t3860_0_0_0;
extern Il2CppType t3860_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5421_m28382_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3860_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28382_GM;
MethodInfo m28382_MI = 
{
	"CopyTo", NULL, &t5421_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5421_m28382_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28382_GM};
extern Il2CppType t234_0_0_0;
static ParameterInfo t5421_m28383_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t234_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28383_GM;
MethodInfo m28383_MI = 
{
	"Remove", NULL, &t5421_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5421_m28383_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28383_GM};
static MethodInfo* t5421_MIs[] =
{
	&m28377_MI,
	&m28378_MI,
	&m28379_MI,
	&m28380_MI,
	&m28381_MI,
	&m28382_MI,
	&m28383_MI,
	NULL
};
extern TypeInfo t5423_TI;
static TypeInfo* t5421_ITIs[] = 
{
	&t603_TI,
	&t5423_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5421_0_0_0;
extern Il2CppType t5421_1_0_0;
struct t5421;
extern Il2CppGenericClass t5421_GC;
TypeInfo t5421_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5421_MIs, t5421_PIs, NULL, NULL, NULL, NULL, NULL, &t5421_TI, t5421_ITIs, NULL, &EmptyCustomAttributesCache, &t5421_TI, &t5421_0_0_0, &t5421_1_0_0, NULL, &t5421_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Slider>
extern Il2CppType t4240_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28384_GM;
MethodInfo m28384_MI = 
{
	"GetEnumerator", NULL, &t5423_TI, &t4240_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28384_GM};
static MethodInfo* t5423_MIs[] =
{
	&m28384_MI,
	NULL
};
static TypeInfo* t5423_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5423_0_0_0;
extern Il2CppType t5423_1_0_0;
struct t5423;
extern Il2CppGenericClass t5423_GC;
TypeInfo t5423_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5423_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5423_TI, t5423_ITIs, NULL, &EmptyCustomAttributesCache, &t5423_TI, &t5423_0_0_0, &t5423_1_0_0, NULL, &t5423_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5422_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Slider>
extern MethodInfo m28385_MI;
extern MethodInfo m28386_MI;
static PropertyInfo t5422____Item_PropertyInfo = 
{
	&t5422_TI, "Item", &m28385_MI, &m28386_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5422_PIs[] =
{
	&t5422____Item_PropertyInfo,
	NULL
};
extern Il2CppType t234_0_0_0;
static ParameterInfo t5422_m28387_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t234_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28387_GM;
MethodInfo m28387_MI = 
{
	"IndexOf", NULL, &t5422_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5422_m28387_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28387_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t234_0_0_0;
static ParameterInfo t5422_m28388_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t234_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28388_GM;
MethodInfo m28388_MI = 
{
	"Insert", NULL, &t5422_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5422_m28388_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28388_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5422_m28389_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28389_GM;
MethodInfo m28389_MI = 
{
	"RemoveAt", NULL, &t5422_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5422_m28389_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28389_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5422_m28385_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t234_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28385_GM;
MethodInfo m28385_MI = 
{
	"get_Item", NULL, &t5422_TI, &t234_0_0_0, RuntimeInvoker_t29_t44, t5422_m28385_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28385_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t234_0_0_0;
static ParameterInfo t5422_m28386_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t234_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28386_GM;
MethodInfo m28386_MI = 
{
	"set_Item", NULL, &t5422_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5422_m28386_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28386_GM};
static MethodInfo* t5422_MIs[] =
{
	&m28387_MI,
	&m28388_MI,
	&m28389_MI,
	&m28385_MI,
	&m28386_MI,
	NULL
};
static TypeInfo* t5422_ITIs[] = 
{
	&t603_TI,
	&t5421_TI,
	&t5423_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5422_0_0_0;
extern Il2CppType t5422_1_0_0;
struct t5422;
extern Il2CppGenericClass t5422_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5422_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5422_MIs, t5422_PIs, NULL, NULL, NULL, NULL, NULL, &t5422_TI, t5422_ITIs, NULL, &t1908__CustomAttributeCache, &t5422_TI, &t5422_0_0_0, &t5422_1_0_0, NULL, &t5422_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2670.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2670_TI;
#include "t2670MD.h"

#include "t2671.h"
extern TypeInfo t2671_TI;
#include "t2671MD.h"
extern MethodInfo m14478_MI;
extern MethodInfo m14480_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Slider>
extern Il2CppType t316_0_0_33;
FieldInfo t2670_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2670_TI, offsetof(t2670, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2670_FIs[] =
{
	&t2670_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t234_0_0_0;
static ParameterInfo t2670_m14476_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t234_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14476_GM;
MethodInfo m14476_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2670_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2670_m14476_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14476_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2670_m14477_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14477_GM;
MethodInfo m14477_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2670_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2670_m14477_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14477_GM};
static MethodInfo* t2670_MIs[] =
{
	&m14476_MI,
	&m14477_MI,
	NULL
};
extern MethodInfo m14477_MI;
extern MethodInfo m14481_MI;
static MethodInfo* t2670_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14477_MI,
	&m14481_MI,
};
extern Il2CppType t2672_0_0_0;
extern TypeInfo t2672_TI;
extern MethodInfo m21439_MI;
extern TypeInfo t234_TI;
extern MethodInfo m14483_MI;
extern TypeInfo t234_TI;
static Il2CppRGCTXData t2670_RGCTXData[8] = 
{
	&t2672_0_0_0/* Type Usage */,
	&t2672_TI/* Class Usage */,
	&m21439_MI/* Method Usage */,
	&t234_TI/* Class Usage */,
	&m14483_MI/* Method Usage */,
	&m14478_MI/* Method Usage */,
	&t234_TI/* Class Usage */,
	&m14480_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2670_0_0_0;
extern Il2CppType t2670_1_0_0;
struct t2670;
extern Il2CppGenericClass t2670_GC;
TypeInfo t2670_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2670_MIs, NULL, t2670_FIs, NULL, &t2671_TI, NULL, NULL, &t2670_TI, NULL, t2670_VT, &EmptyCustomAttributesCache, &t2670_TI, &t2670_0_0_0, &t2670_1_0_0, NULL, &t2670_GC, NULL, NULL, NULL, t2670_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2670), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2672.h"
extern TypeInfo t2672_TI;
#include "t2672MD.h"
struct t556;
#define m21439(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Slider>
extern Il2CppType t2672_0_0_1;
FieldInfo t2671_f0_FieldInfo = 
{
	"Delegate", &t2672_0_0_1, &t2671_TI, offsetof(t2671, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2671_FIs[] =
{
	&t2671_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2671_m14478_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14478_GM;
MethodInfo m14478_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2671_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2671_m14478_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14478_GM};
extern Il2CppType t2672_0_0_0;
static ParameterInfo t2671_m14479_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2672_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14479_GM;
MethodInfo m14479_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2671_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2671_m14479_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14479_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2671_m14480_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14480_GM;
MethodInfo m14480_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2671_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2671_m14480_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14480_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2671_m14481_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14481_GM;
MethodInfo m14481_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2671_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2671_m14481_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14481_GM};
static MethodInfo* t2671_MIs[] =
{
	&m14478_MI,
	&m14479_MI,
	&m14480_MI,
	&m14481_MI,
	NULL
};
static MethodInfo* t2671_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14480_MI,
	&m14481_MI,
};
extern TypeInfo t2672_TI;
extern TypeInfo t234_TI;
static Il2CppRGCTXData t2671_RGCTXData[5] = 
{
	&t2672_0_0_0/* Type Usage */,
	&t2672_TI/* Class Usage */,
	&m21439_MI/* Method Usage */,
	&t234_TI/* Class Usage */,
	&m14483_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2671_0_0_0;
extern Il2CppType t2671_1_0_0;
struct t2671;
extern Il2CppGenericClass t2671_GC;
TypeInfo t2671_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2671_MIs, NULL, t2671_FIs, NULL, &t556_TI, NULL, NULL, &t2671_TI, NULL, t2671_VT, &EmptyCustomAttributesCache, &t2671_TI, &t2671_0_0_0, &t2671_1_0_0, NULL, &t2671_GC, NULL, NULL, NULL, t2671_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2671), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Slider>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2672_m14482_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14482_GM;
MethodInfo m14482_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2672_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2672_m14482_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14482_GM};
extern Il2CppType t234_0_0_0;
static ParameterInfo t2672_m14483_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t234_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14483_GM;
MethodInfo m14483_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2672_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2672_m14483_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14483_GM};
extern Il2CppType t234_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2672_m14484_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t234_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14484_GM;
MethodInfo m14484_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2672_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2672_m14484_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14484_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2672_m14485_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14485_GM;
MethodInfo m14485_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2672_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2672_m14485_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14485_GM};
static MethodInfo* t2672_MIs[] =
{
	&m14482_MI,
	&m14483_MI,
	&m14484_MI,
	&m14485_MI,
	NULL
};
extern MethodInfo m14484_MI;
extern MethodInfo m14485_MI;
static MethodInfo* t2672_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14483_MI,
	&m14484_MI,
	&m14485_MI,
};
static Il2CppInterfaceOffsetPair t2672_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2672_1_0_0;
struct t2672;
extern Il2CppGenericClass t2672_GC;
TypeInfo t2672_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2672_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2672_TI, NULL, t2672_VT, &EmptyCustomAttributesCache, &t2672_TI, &t2672_0_0_0, &t2672_1_0_0, t2672_IOs, &t2672_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2672), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4242_TI;

#include "t231.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Slider/Direction>
extern MethodInfo m28390_MI;
static PropertyInfo t4242____Current_PropertyInfo = 
{
	&t4242_TI, "Current", &m28390_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4242_PIs[] =
{
	&t4242____Current_PropertyInfo,
	NULL
};
extern Il2CppType t231_0_0_0;
extern void* RuntimeInvoker_t231 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28390_GM;
MethodInfo m28390_MI = 
{
	"get_Current", NULL, &t4242_TI, &t231_0_0_0, RuntimeInvoker_t231, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28390_GM};
static MethodInfo* t4242_MIs[] =
{
	&m28390_MI,
	NULL
};
static TypeInfo* t4242_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4242_0_0_0;
extern Il2CppType t4242_1_0_0;
struct t4242;
extern Il2CppGenericClass t4242_GC;
TypeInfo t4242_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4242_MIs, t4242_PIs, NULL, NULL, NULL, NULL, NULL, &t4242_TI, t4242_ITIs, NULL, &EmptyCustomAttributesCache, &t4242_TI, &t4242_0_0_0, &t4242_1_0_0, NULL, &t4242_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2673.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2673_TI;
#include "t2673MD.h"

extern TypeInfo t231_TI;
extern MethodInfo m14490_MI;
extern MethodInfo m21441_MI;
struct t20;
 int32_t m21441 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14486_MI;
 void m14486 (t2673 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14487_MI;
 t29 * m14487 (t2673 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14490(__this, &m14490_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t231_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14488_MI;
 void m14488 (t2673 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14489_MI;
 bool m14489 (t2673 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14490 (t2673 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21441(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21441_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Slider/Direction>
extern Il2CppType t20_0_0_1;
FieldInfo t2673_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2673_TI, offsetof(t2673, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2673_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2673_TI, offsetof(t2673, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2673_FIs[] =
{
	&t2673_f0_FieldInfo,
	&t2673_f1_FieldInfo,
	NULL
};
static PropertyInfo t2673____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2673_TI, "System.Collections.IEnumerator.Current", &m14487_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2673____Current_PropertyInfo = 
{
	&t2673_TI, "Current", &m14490_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2673_PIs[] =
{
	&t2673____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2673____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2673_m14486_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14486_GM;
MethodInfo m14486_MI = 
{
	".ctor", (methodPointerType)&m14486, &t2673_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2673_m14486_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14486_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14487_GM;
MethodInfo m14487_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14487, &t2673_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14487_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14488_GM;
MethodInfo m14488_MI = 
{
	"Dispose", (methodPointerType)&m14488, &t2673_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14488_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14489_GM;
MethodInfo m14489_MI = 
{
	"MoveNext", (methodPointerType)&m14489, &t2673_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14489_GM};
extern Il2CppType t231_0_0_0;
extern void* RuntimeInvoker_t231 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14490_GM;
MethodInfo m14490_MI = 
{
	"get_Current", (methodPointerType)&m14490, &t2673_TI, &t231_0_0_0, RuntimeInvoker_t231, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14490_GM};
static MethodInfo* t2673_MIs[] =
{
	&m14486_MI,
	&m14487_MI,
	&m14488_MI,
	&m14489_MI,
	&m14490_MI,
	NULL
};
static MethodInfo* t2673_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14487_MI,
	&m14489_MI,
	&m14488_MI,
	&m14490_MI,
};
static TypeInfo* t2673_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4242_TI,
};
static Il2CppInterfaceOffsetPair t2673_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4242_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2673_0_0_0;
extern Il2CppType t2673_1_0_0;
extern Il2CppGenericClass t2673_GC;
TypeInfo t2673_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2673_MIs, t2673_PIs, t2673_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2673_TI, t2673_ITIs, t2673_VT, &EmptyCustomAttributesCache, &t2673_TI, &t2673_0_0_0, &t2673_1_0_0, t2673_IOs, &t2673_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2673)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5424_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Slider/Direction>
extern MethodInfo m28391_MI;
static PropertyInfo t5424____Count_PropertyInfo = 
{
	&t5424_TI, "Count", &m28391_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28392_MI;
static PropertyInfo t5424____IsReadOnly_PropertyInfo = 
{
	&t5424_TI, "IsReadOnly", &m28392_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5424_PIs[] =
{
	&t5424____Count_PropertyInfo,
	&t5424____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28391_GM;
MethodInfo m28391_MI = 
{
	"get_Count", NULL, &t5424_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28391_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28392_GM;
MethodInfo m28392_MI = 
{
	"get_IsReadOnly", NULL, &t5424_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28392_GM};
extern Il2CppType t231_0_0_0;
extern Il2CppType t231_0_0_0;
static ParameterInfo t5424_m28393_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t231_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28393_GM;
MethodInfo m28393_MI = 
{
	"Add", NULL, &t5424_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5424_m28393_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28393_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28394_GM;
MethodInfo m28394_MI = 
{
	"Clear", NULL, &t5424_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28394_GM};
extern Il2CppType t231_0_0_0;
static ParameterInfo t5424_m28395_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t231_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28395_GM;
MethodInfo m28395_MI = 
{
	"Contains", NULL, &t5424_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5424_m28395_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28395_GM};
extern Il2CppType t3861_0_0_0;
extern Il2CppType t3861_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5424_m28396_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3861_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28396_GM;
MethodInfo m28396_MI = 
{
	"CopyTo", NULL, &t5424_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5424_m28396_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28396_GM};
extern Il2CppType t231_0_0_0;
static ParameterInfo t5424_m28397_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t231_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28397_GM;
MethodInfo m28397_MI = 
{
	"Remove", NULL, &t5424_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5424_m28397_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28397_GM};
static MethodInfo* t5424_MIs[] =
{
	&m28391_MI,
	&m28392_MI,
	&m28393_MI,
	&m28394_MI,
	&m28395_MI,
	&m28396_MI,
	&m28397_MI,
	NULL
};
extern TypeInfo t5426_TI;
static TypeInfo* t5424_ITIs[] = 
{
	&t603_TI,
	&t5426_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5424_0_0_0;
extern Il2CppType t5424_1_0_0;
struct t5424;
extern Il2CppGenericClass t5424_GC;
TypeInfo t5424_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5424_MIs, t5424_PIs, NULL, NULL, NULL, NULL, NULL, &t5424_TI, t5424_ITIs, NULL, &EmptyCustomAttributesCache, &t5424_TI, &t5424_0_0_0, &t5424_1_0_0, NULL, &t5424_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Slider/Direction>
extern Il2CppType t4242_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28398_GM;
MethodInfo m28398_MI = 
{
	"GetEnumerator", NULL, &t5426_TI, &t4242_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28398_GM};
static MethodInfo* t5426_MIs[] =
{
	&m28398_MI,
	NULL
};
static TypeInfo* t5426_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5426_0_0_0;
extern Il2CppType t5426_1_0_0;
struct t5426;
extern Il2CppGenericClass t5426_GC;
TypeInfo t5426_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5426_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5426_TI, t5426_ITIs, NULL, &EmptyCustomAttributesCache, &t5426_TI, &t5426_0_0_0, &t5426_1_0_0, NULL, &t5426_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5425_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Slider/Direction>
extern MethodInfo m28399_MI;
extern MethodInfo m28400_MI;
static PropertyInfo t5425____Item_PropertyInfo = 
{
	&t5425_TI, "Item", &m28399_MI, &m28400_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5425_PIs[] =
{
	&t5425____Item_PropertyInfo,
	NULL
};
extern Il2CppType t231_0_0_0;
static ParameterInfo t5425_m28401_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t231_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28401_GM;
MethodInfo m28401_MI = 
{
	"IndexOf", NULL, &t5425_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5425_m28401_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28401_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t231_0_0_0;
static ParameterInfo t5425_m28402_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t231_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28402_GM;
MethodInfo m28402_MI = 
{
	"Insert", NULL, &t5425_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5425_m28402_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28402_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5425_m28403_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28403_GM;
MethodInfo m28403_MI = 
{
	"RemoveAt", NULL, &t5425_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5425_m28403_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28403_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5425_m28399_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t231_0_0_0;
extern void* RuntimeInvoker_t231_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28399_GM;
MethodInfo m28399_MI = 
{
	"get_Item", NULL, &t5425_TI, &t231_0_0_0, RuntimeInvoker_t231_t44, t5425_m28399_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28399_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t231_0_0_0;
static ParameterInfo t5425_m28400_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t231_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28400_GM;
MethodInfo m28400_MI = 
{
	"set_Item", NULL, &t5425_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5425_m28400_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28400_GM};
static MethodInfo* t5425_MIs[] =
{
	&m28401_MI,
	&m28402_MI,
	&m28403_MI,
	&m28399_MI,
	&m28400_MI,
	NULL
};
static TypeInfo* t5425_ITIs[] = 
{
	&t603_TI,
	&t5424_TI,
	&t5426_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5425_0_0_0;
extern Il2CppType t5425_1_0_0;
struct t5425;
extern Il2CppGenericClass t5425_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5425_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5425_MIs, t5425_PIs, NULL, NULL, NULL, NULL, NULL, &t5425_TI, t5425_ITIs, NULL, &t1908__CustomAttributeCache, &t5425_TI, &t5425_0_0_0, &t5425_1_0_0, NULL, &t5425_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4244_TI;

#include "t233.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Slider/Axis>
extern MethodInfo m28404_MI;
static PropertyInfo t4244____Current_PropertyInfo = 
{
	&t4244_TI, "Current", &m28404_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4244_PIs[] =
{
	&t4244____Current_PropertyInfo,
	NULL
};
extern Il2CppType t233_0_0_0;
extern void* RuntimeInvoker_t233 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28404_GM;
MethodInfo m28404_MI = 
{
	"get_Current", NULL, &t4244_TI, &t233_0_0_0, RuntimeInvoker_t233, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28404_GM};
static MethodInfo* t4244_MIs[] =
{
	&m28404_MI,
	NULL
};
static TypeInfo* t4244_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4244_0_0_0;
extern Il2CppType t4244_1_0_0;
struct t4244;
extern Il2CppGenericClass t4244_GC;
TypeInfo t4244_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4244_MIs, t4244_PIs, NULL, NULL, NULL, NULL, NULL, &t4244_TI, t4244_ITIs, NULL, &EmptyCustomAttributesCache, &t4244_TI, &t4244_0_0_0, &t4244_1_0_0, NULL, &t4244_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2674.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2674_TI;
#include "t2674MD.h"

extern TypeInfo t233_TI;
extern MethodInfo m14495_MI;
extern MethodInfo m21452_MI;
struct t20;
 int32_t m21452 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14491_MI;
 void m14491 (t2674 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14492_MI;
 t29 * m14492 (t2674 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14495(__this, &m14495_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t233_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14493_MI;
 void m14493 (t2674 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14494_MI;
 bool m14494 (t2674 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14495 (t2674 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21452(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21452_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Slider/Axis>
extern Il2CppType t20_0_0_1;
FieldInfo t2674_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2674_TI, offsetof(t2674, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2674_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2674_TI, offsetof(t2674, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2674_FIs[] =
{
	&t2674_f0_FieldInfo,
	&t2674_f1_FieldInfo,
	NULL
};
static PropertyInfo t2674____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2674_TI, "System.Collections.IEnumerator.Current", &m14492_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2674____Current_PropertyInfo = 
{
	&t2674_TI, "Current", &m14495_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2674_PIs[] =
{
	&t2674____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2674____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2674_m14491_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14491_GM;
MethodInfo m14491_MI = 
{
	".ctor", (methodPointerType)&m14491, &t2674_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2674_m14491_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14491_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14492_GM;
MethodInfo m14492_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14492, &t2674_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14492_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14493_GM;
MethodInfo m14493_MI = 
{
	"Dispose", (methodPointerType)&m14493, &t2674_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14493_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14494_GM;
MethodInfo m14494_MI = 
{
	"MoveNext", (methodPointerType)&m14494, &t2674_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14494_GM};
extern Il2CppType t233_0_0_0;
extern void* RuntimeInvoker_t233 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14495_GM;
MethodInfo m14495_MI = 
{
	"get_Current", (methodPointerType)&m14495, &t2674_TI, &t233_0_0_0, RuntimeInvoker_t233, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14495_GM};
static MethodInfo* t2674_MIs[] =
{
	&m14491_MI,
	&m14492_MI,
	&m14493_MI,
	&m14494_MI,
	&m14495_MI,
	NULL
};
static MethodInfo* t2674_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14492_MI,
	&m14494_MI,
	&m14493_MI,
	&m14495_MI,
};
static TypeInfo* t2674_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4244_TI,
};
static Il2CppInterfaceOffsetPair t2674_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4244_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2674_0_0_0;
extern Il2CppType t2674_1_0_0;
extern Il2CppGenericClass t2674_GC;
TypeInfo t2674_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2674_MIs, t2674_PIs, t2674_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2674_TI, t2674_ITIs, t2674_VT, &EmptyCustomAttributesCache, &t2674_TI, &t2674_0_0_0, &t2674_1_0_0, t2674_IOs, &t2674_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2674)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5427_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Slider/Axis>
extern MethodInfo m28405_MI;
static PropertyInfo t5427____Count_PropertyInfo = 
{
	&t5427_TI, "Count", &m28405_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28406_MI;
static PropertyInfo t5427____IsReadOnly_PropertyInfo = 
{
	&t5427_TI, "IsReadOnly", &m28406_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5427_PIs[] =
{
	&t5427____Count_PropertyInfo,
	&t5427____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28405_GM;
MethodInfo m28405_MI = 
{
	"get_Count", NULL, &t5427_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28405_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28406_GM;
MethodInfo m28406_MI = 
{
	"get_IsReadOnly", NULL, &t5427_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28406_GM};
extern Il2CppType t233_0_0_0;
extern Il2CppType t233_0_0_0;
static ParameterInfo t5427_m28407_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t233_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28407_GM;
MethodInfo m28407_MI = 
{
	"Add", NULL, &t5427_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5427_m28407_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28407_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28408_GM;
MethodInfo m28408_MI = 
{
	"Clear", NULL, &t5427_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28408_GM};
extern Il2CppType t233_0_0_0;
static ParameterInfo t5427_m28409_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t233_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28409_GM;
MethodInfo m28409_MI = 
{
	"Contains", NULL, &t5427_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5427_m28409_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28409_GM};
extern Il2CppType t3862_0_0_0;
extern Il2CppType t3862_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5427_m28410_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3862_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28410_GM;
MethodInfo m28410_MI = 
{
	"CopyTo", NULL, &t5427_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5427_m28410_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28410_GM};
extern Il2CppType t233_0_0_0;
static ParameterInfo t5427_m28411_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t233_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28411_GM;
MethodInfo m28411_MI = 
{
	"Remove", NULL, &t5427_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5427_m28411_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28411_GM};
static MethodInfo* t5427_MIs[] =
{
	&m28405_MI,
	&m28406_MI,
	&m28407_MI,
	&m28408_MI,
	&m28409_MI,
	&m28410_MI,
	&m28411_MI,
	NULL
};
extern TypeInfo t5429_TI;
static TypeInfo* t5427_ITIs[] = 
{
	&t603_TI,
	&t5429_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5427_0_0_0;
extern Il2CppType t5427_1_0_0;
struct t5427;
extern Il2CppGenericClass t5427_GC;
TypeInfo t5427_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5427_MIs, t5427_PIs, NULL, NULL, NULL, NULL, NULL, &t5427_TI, t5427_ITIs, NULL, &EmptyCustomAttributesCache, &t5427_TI, &t5427_0_0_0, &t5427_1_0_0, NULL, &t5427_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Slider/Axis>
extern Il2CppType t4244_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28412_GM;
MethodInfo m28412_MI = 
{
	"GetEnumerator", NULL, &t5429_TI, &t4244_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28412_GM};
static MethodInfo* t5429_MIs[] =
{
	&m28412_MI,
	NULL
};
static TypeInfo* t5429_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5429_0_0_0;
extern Il2CppType t5429_1_0_0;
struct t5429;
extern Il2CppGenericClass t5429_GC;
TypeInfo t5429_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5429_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5429_TI, t5429_ITIs, NULL, &EmptyCustomAttributesCache, &t5429_TI, &t5429_0_0_0, &t5429_1_0_0, NULL, &t5429_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5428_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Slider/Axis>
extern MethodInfo m28413_MI;
extern MethodInfo m28414_MI;
static PropertyInfo t5428____Item_PropertyInfo = 
{
	&t5428_TI, "Item", &m28413_MI, &m28414_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5428_PIs[] =
{
	&t5428____Item_PropertyInfo,
	NULL
};
extern Il2CppType t233_0_0_0;
static ParameterInfo t5428_m28415_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t233_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28415_GM;
MethodInfo m28415_MI = 
{
	"IndexOf", NULL, &t5428_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5428_m28415_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28415_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t233_0_0_0;
static ParameterInfo t5428_m28416_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t233_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28416_GM;
MethodInfo m28416_MI = 
{
	"Insert", NULL, &t5428_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5428_m28416_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28416_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5428_m28417_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28417_GM;
MethodInfo m28417_MI = 
{
	"RemoveAt", NULL, &t5428_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5428_m28417_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28417_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5428_m28413_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t233_0_0_0;
extern void* RuntimeInvoker_t233_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28413_GM;
MethodInfo m28413_MI = 
{
	"get_Item", NULL, &t5428_TI, &t233_0_0_0, RuntimeInvoker_t233_t44, t5428_m28413_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28413_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t233_0_0_0;
static ParameterInfo t5428_m28414_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t233_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28414_GM;
MethodInfo m28414_MI = 
{
	"set_Item", NULL, &t5428_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5428_m28414_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28414_GM};
static MethodInfo* t5428_MIs[] =
{
	&m28415_MI,
	&m28416_MI,
	&m28417_MI,
	&m28413_MI,
	&m28414_MI,
	NULL
};
static TypeInfo* t5428_ITIs[] = 
{
	&t603_TI,
	&t5427_TI,
	&t5429_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5428_0_0_0;
extern Il2CppType t5428_1_0_0;
struct t5428;
extern Il2CppGenericClass t5428_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5428_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5428_MIs, t5428_PIs, NULL, NULL, NULL, NULL, NULL, &t5428_TI, t5428_ITIs, NULL, &t1908__CustomAttributeCache, &t5428_TI, &t5428_0_0_0, &t5428_1_0_0, NULL, &t5428_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t237.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t237_TI;
#include "t237MD.h"

#include "t235.h"
#include "t2682.h"
#include "t2679.h"
#include "t2680.h"
#include "t2688.h"
#include "t2681.h"
extern TypeInfo t235_TI;
extern TypeInfo t2675_TI;
extern TypeInfo t2682_TI;
extern TypeInfo t2677_TI;
extern TypeInfo t2678_TI;
extern TypeInfo t2676_TI;
extern TypeInfo t2679_TI;
extern TypeInfo t2680_TI;
extern TypeInfo t2688_TI;
#include "t2679MD.h"
#include "t2680MD.h"
#include "t2682MD.h"
#include "t2688MD.h"
extern MethodInfo m1905_MI;
extern MethodInfo m14539_MI;
extern MethodInfo m21474_MI;
extern MethodInfo m14527_MI;
extern MethodInfo m14524_MI;
extern MethodInfo m1910_MI;
extern MethodInfo m14519_MI;
extern MethodInfo m14525_MI;
extern MethodInfo m14528_MI;
extern MethodInfo m14530_MI;
extern MethodInfo m14513_MI;
extern MethodInfo m14537_MI;
extern MethodInfo m14538_MI;
extern MethodInfo m28418_MI;
extern MethodInfo m28419_MI;
extern MethodInfo m28420_MI;
extern MethodInfo m28421_MI;
extern MethodInfo m14529_MI;
extern MethodInfo m14514_MI;
extern MethodInfo m14515_MI;
extern MethodInfo m14551_MI;
extern MethodInfo m21476_MI;
extern MethodInfo m14522_MI;
extern MethodInfo m14523_MI;
extern MethodInfo m14626_MI;
extern MethodInfo m14545_MI;
extern MethodInfo m14526_MI;
extern MethodInfo m1911_MI;
extern MethodInfo m14632_MI;
extern MethodInfo m21478_MI;
extern MethodInfo m21486_MI;
struct t20;
#define m21474(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2686.h"
#define m21476(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
#define m21478(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
#define m21486(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2682  m14524 (t237 * __this, MethodInfo* method){
	{
		t2682  L_0 = {0};
		m14545(&L_0, __this, &m14545_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t44_0_0_32849;
FieldInfo t237_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t237_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2675_0_0_1;
FieldInfo t237_f1_FieldInfo = 
{
	"_items", &t2675_0_0_1, &t237_TI, offsetof(t237, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t237_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t237_TI, offsetof(t237, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t237_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t237_TI, offsetof(t237, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2675_0_0_49;
FieldInfo t237_f4_FieldInfo = 
{
	"EmptyArray", &t2675_0_0_49, &t237_TI, offsetof(t237_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t237_FIs[] =
{
	&t237_f0_FieldInfo,
	&t237_f1_FieldInfo,
	&t237_f2_FieldInfo,
	&t237_f3_FieldInfo,
	&t237_f4_FieldInfo,
	NULL
};
static const int32_t t237_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t237_f0_DefaultValue = 
{
	&t237_f0_FieldInfo, { (char*)&t237_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t237_FDVs[] = 
{
	&t237_f0_DefaultValue,
	NULL
};
extern MethodInfo m14506_MI;
static PropertyInfo t237____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t237_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m14506_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14507_MI;
static PropertyInfo t237____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t237_TI, "System.Collections.ICollection.IsSynchronized", &m14507_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14508_MI;
static PropertyInfo t237____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t237_TI, "System.Collections.ICollection.SyncRoot", &m14508_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14509_MI;
static PropertyInfo t237____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t237_TI, "System.Collections.IList.IsFixedSize", &m14509_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14510_MI;
static PropertyInfo t237____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t237_TI, "System.Collections.IList.IsReadOnly", &m14510_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m14511_MI;
extern MethodInfo m14512_MI;
static PropertyInfo t237____System_Collections_IList_Item_PropertyInfo = 
{
	&t237_TI, "System.Collections.IList.Item", &m14511_MI, &m14512_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t237____Capacity_PropertyInfo = 
{
	&t237_TI, "Capacity", &m14537_MI, &m14538_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1906_MI;
static PropertyInfo t237____Count_PropertyInfo = 
{
	&t237_TI, "Count", &m1906_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t237____Item_PropertyInfo = 
{
	&t237_TI, "Item", &m1905_MI, &m14539_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t237_PIs[] =
{
	&t237____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t237____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t237____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t237____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t237____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t237____System_Collections_IList_Item_PropertyInfo,
	&t237____Capacity_PropertyInfo,
	&t237____Count_PropertyInfo,
	&t237____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1902_GM;
MethodInfo m1902_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1902_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t237_m14496_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14496_GM;
MethodInfo m14496_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t237_m14496_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14496_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14497_GM;
MethodInfo m14497_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14497_GM};
extern Il2CppType t2676_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14498_GM;
MethodInfo m14498_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t237_TI, &t2676_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14498_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t237_m14499_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14499_GM;
MethodInfo m14499_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t237_m14499_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14499_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14500_GM;
MethodInfo m14500_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t237_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14500_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t237_m14501_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14501_GM;
MethodInfo m14501_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t237_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t237_m14501_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14501_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t237_m14502_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14502_GM;
MethodInfo m14502_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t237_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t237_m14502_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14502_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t237_m14503_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14503_GM;
MethodInfo m14503_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t237_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t237_m14503_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14503_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t237_m14504_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14504_GM;
MethodInfo m14504_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t237_m14504_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14504_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t237_m14505_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14505_GM;
MethodInfo m14505_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t237_m14505_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14505_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14506_GM;
MethodInfo m14506_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t237_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14506_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14507_GM;
MethodInfo m14507_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t237_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14507_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14508_GM;
MethodInfo m14508_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t237_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14508_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14509_GM;
MethodInfo m14509_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t237_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14509_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14510_GM;
MethodInfo m14510_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t237_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14510_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t237_m14511_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14511_GM;
MethodInfo m14511_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t237_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t237_m14511_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14511_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t237_m14512_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14512_GM;
MethodInfo m14512_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t237_m14512_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14512_GM};
extern Il2CppType t235_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t237_m1910_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1910_GM;
MethodInfo m1910_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t237_m1910_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1910_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t237_m14513_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14513_GM;
MethodInfo m14513_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t237_m14513_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14513_GM};
extern Il2CppType t2677_0_0_0;
extern Il2CppType t2677_0_0_0;
static ParameterInfo t237_m14514_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2677_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14514_GM;
MethodInfo m14514_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t237_m14514_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14514_GM};
extern Il2CppType t2678_0_0_0;
extern Il2CppType t2678_0_0_0;
static ParameterInfo t237_m14515_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2678_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14515_GM;
MethodInfo m14515_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t237_m14515_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14515_GM};
extern Il2CppType t2678_0_0_0;
static ParameterInfo t237_m14516_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2678_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14516_GM;
MethodInfo m14516_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t237_m14516_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14516_GM};
extern Il2CppType t2679_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14517_GM;
MethodInfo m14517_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t237_TI, &t2679_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14517_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14518_GM;
MethodInfo m14518_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14518_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t237_m14519_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14519_GM;
MethodInfo m14519_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t237_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t237_m14519_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14519_GM};
extern Il2CppType t2675_0_0_0;
extern Il2CppType t2675_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t237_m14520_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2675_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14520_GM;
MethodInfo m14520_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t237_m14520_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14520_GM};
extern Il2CppType t2680_0_0_0;
extern Il2CppType t2680_0_0_0;
static ParameterInfo t237_m14521_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2680_0_0_0},
};
extern Il2CppType t235_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14521_GM;
MethodInfo m14521_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t237_TI, &t235_0_0_0, RuntimeInvoker_t29_t29, t237_m14521_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14521_GM};
extern Il2CppType t2680_0_0_0;
static ParameterInfo t237_m14522_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2680_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14522_GM;
MethodInfo m14522_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t237_m14522_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14522_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2680_0_0_0;
static ParameterInfo t237_m14523_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2680_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14523_GM;
MethodInfo m14523_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t237_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t237_m14523_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14523_GM};
extern Il2CppType t2682_0_0_0;
extern void* RuntimeInvoker_t2682 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14524_GM;
MethodInfo m14524_MI = 
{
	"GetEnumerator", (methodPointerType)&m14524, &t237_TI, &t2682_0_0_0, RuntimeInvoker_t2682, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14524_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t237_m14525_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14525_GM;
MethodInfo m14525_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t237_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t237_m14525_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14525_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t237_m14526_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14526_GM;
MethodInfo m14526_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t237_m14526_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14526_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t237_m14527_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14527_GM;
MethodInfo m14527_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t237_m14527_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14527_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t237_m14528_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14528_GM;
MethodInfo m14528_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t237_m14528_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14528_GM};
extern Il2CppType t2678_0_0_0;
static ParameterInfo t237_m14529_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2678_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14529_GM;
MethodInfo m14529_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t237_m14529_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14529_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t237_m14530_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14530_GM;
MethodInfo m14530_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t237_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t237_m14530_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14530_GM};
extern Il2CppType t2680_0_0_0;
static ParameterInfo t237_m14531_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2680_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14531_GM;
MethodInfo m14531_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t237_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t237_m14531_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14531_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t237_m1911_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1911_GM;
MethodInfo m1911_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t237_m1911_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1911_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14532_GM;
MethodInfo m14532_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14532_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14533_GM;
MethodInfo m14533_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14533_GM};
extern Il2CppType t2681_0_0_0;
extern Il2CppType t2681_0_0_0;
static ParameterInfo t237_m14534_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2681_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14534_GM;
MethodInfo m14534_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t237_m14534_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14534_GM};
extern Il2CppType t2675_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14535_GM;
MethodInfo m14535_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t237_TI, &t2675_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14535_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14536_GM;
MethodInfo m14536_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14536_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14537_GM;
MethodInfo m14537_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t237_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14537_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t237_m14538_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14538_GM;
MethodInfo m14538_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t237_m14538_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14538_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1906_GM;
MethodInfo m1906_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t237_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1906_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t237_m1905_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t235_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1905_GM;
MethodInfo m1905_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t237_TI, &t235_0_0_0, RuntimeInvoker_t29_t44, t237_m1905_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1905_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t235_0_0_0;
static ParameterInfo t237_m14539_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14539_GM;
MethodInfo m14539_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t237_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t237_m14539_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14539_GM};
static MethodInfo* t237_MIs[] =
{
	&m1902_MI,
	&m14496_MI,
	&m14497_MI,
	&m14498_MI,
	&m14499_MI,
	&m14500_MI,
	&m14501_MI,
	&m14502_MI,
	&m14503_MI,
	&m14504_MI,
	&m14505_MI,
	&m14506_MI,
	&m14507_MI,
	&m14508_MI,
	&m14509_MI,
	&m14510_MI,
	&m14511_MI,
	&m14512_MI,
	&m1910_MI,
	&m14513_MI,
	&m14514_MI,
	&m14515_MI,
	&m14516_MI,
	&m14517_MI,
	&m14518_MI,
	&m14519_MI,
	&m14520_MI,
	&m14521_MI,
	&m14522_MI,
	&m14523_MI,
	&m14524_MI,
	&m14525_MI,
	&m14526_MI,
	&m14527_MI,
	&m14528_MI,
	&m14529_MI,
	&m14530_MI,
	&m14531_MI,
	&m1911_MI,
	&m14532_MI,
	&m14533_MI,
	&m14534_MI,
	&m14535_MI,
	&m14536_MI,
	&m14537_MI,
	&m14538_MI,
	&m1906_MI,
	&m1905_MI,
	&m14539_MI,
	NULL
};
extern MethodInfo m14500_MI;
extern MethodInfo m14499_MI;
extern MethodInfo m14501_MI;
extern MethodInfo m14518_MI;
extern MethodInfo m14502_MI;
extern MethodInfo m14503_MI;
extern MethodInfo m14504_MI;
extern MethodInfo m14505_MI;
extern MethodInfo m14520_MI;
extern MethodInfo m14498_MI;
static MethodInfo* t237_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14500_MI,
	&m1906_MI,
	&m14507_MI,
	&m14508_MI,
	&m14499_MI,
	&m14509_MI,
	&m14510_MI,
	&m14511_MI,
	&m14512_MI,
	&m14501_MI,
	&m14518_MI,
	&m14502_MI,
	&m14503_MI,
	&m14504_MI,
	&m14505_MI,
	&m1911_MI,
	&m1906_MI,
	&m14506_MI,
	&m1910_MI,
	&m14518_MI,
	&m14519_MI,
	&m14520_MI,
	&m14530_MI,
	&m14498_MI,
	&m14525_MI,
	&m14528_MI,
	&m1911_MI,
	&m1905_MI,
	&m14539_MI,
};
extern TypeInfo t2684_TI;
static TypeInfo* t237_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2677_TI,
	&t2678_TI,
	&t2684_TI,
};
static Il2CppInterfaceOffsetPair t237_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2677_TI, 20},
	{ &t2678_TI, 27},
	{ &t2684_TI, 28},
};
extern TypeInfo t237_TI;
extern TypeInfo t2675_TI;
extern TypeInfo t2682_TI;
extern TypeInfo t235_TI;
extern TypeInfo t2677_TI;
extern TypeInfo t2679_TI;
static Il2CppRGCTXData t237_RGCTXData[37] = 
{
	&t237_TI/* Static Usage */,
	&t2675_TI/* Array Usage */,
	&m14524_MI/* Method Usage */,
	&t2682_TI/* Class Usage */,
	&t235_TI/* Class Usage */,
	&m1910_MI/* Method Usage */,
	&m14519_MI/* Method Usage */,
	&m14525_MI/* Method Usage */,
	&m14527_MI/* Method Usage */,
	&m14528_MI/* Method Usage */,
	&m14530_MI/* Method Usage */,
	&m1905_MI/* Method Usage */,
	&m14539_MI/* Method Usage */,
	&m14513_MI/* Method Usage */,
	&m14537_MI/* Method Usage */,
	&m14538_MI/* Method Usage */,
	&m28418_MI/* Method Usage */,
	&m28419_MI/* Method Usage */,
	&m28420_MI/* Method Usage */,
	&m28421_MI/* Method Usage */,
	&m14529_MI/* Method Usage */,
	&t2677_TI/* Class Usage */,
	&m14514_MI/* Method Usage */,
	&m14515_MI/* Method Usage */,
	&t2679_TI/* Class Usage */,
	&m14551_MI/* Method Usage */,
	&m21476_MI/* Method Usage */,
	&m14522_MI/* Method Usage */,
	&m14523_MI/* Method Usage */,
	&m14626_MI/* Method Usage */,
	&m14545_MI/* Method Usage */,
	&m14526_MI/* Method Usage */,
	&m1911_MI/* Method Usage */,
	&m14632_MI/* Method Usage */,
	&m21478_MI/* Method Usage */,
	&m21486_MI/* Method Usage */,
	&m21474_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t237_0_0_0;
extern Il2CppType t237_1_0_0;
struct t237;
extern Il2CppGenericClass t237_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t237_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t237_MIs, t237_PIs, t237_FIs, NULL, &t29_TI, NULL, NULL, &t237_TI, t237_ITIs, t237_VT, &t1261__CustomAttributeCache, &t237_TI, &t237_0_0_0, &t237_1_0_0, t237_IOs, &t237_GC, NULL, t237_FDVs, NULL, t237_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t237), 0, -1, sizeof(t237_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.StencilMaterial/MatEntry>
static PropertyInfo t2677____Count_PropertyInfo = 
{
	&t2677_TI, "Count", &m28418_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28422_MI;
static PropertyInfo t2677____IsReadOnly_PropertyInfo = 
{
	&t2677_TI, "IsReadOnly", &m28422_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2677_PIs[] =
{
	&t2677____Count_PropertyInfo,
	&t2677____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28418_GM;
MethodInfo m28418_MI = 
{
	"get_Count", NULL, &t2677_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28418_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28422_GM;
MethodInfo m28422_MI = 
{
	"get_IsReadOnly", NULL, &t2677_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28422_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2677_m28423_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28423_GM;
MethodInfo m28423_MI = 
{
	"Add", NULL, &t2677_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2677_m28423_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28423_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28424_GM;
MethodInfo m28424_MI = 
{
	"Clear", NULL, &t2677_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28424_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2677_m28425_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28425_GM;
MethodInfo m28425_MI = 
{
	"Contains", NULL, &t2677_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2677_m28425_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28425_GM};
extern Il2CppType t2675_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2677_m28419_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2675_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28419_GM;
MethodInfo m28419_MI = 
{
	"CopyTo", NULL, &t2677_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2677_m28419_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28419_GM};
extern Il2CppType t235_0_0_0;
static ParameterInfo t2677_m28426_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t235_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28426_GM;
MethodInfo m28426_MI = 
{
	"Remove", NULL, &t2677_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2677_m28426_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28426_GM};
static MethodInfo* t2677_MIs[] =
{
	&m28418_MI,
	&m28422_MI,
	&m28423_MI,
	&m28424_MI,
	&m28425_MI,
	&m28419_MI,
	&m28426_MI,
	NULL
};
static TypeInfo* t2677_ITIs[] = 
{
	&t603_TI,
	&t2678_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2677_1_0_0;
struct t2677;
extern Il2CppGenericClass t2677_GC;
TypeInfo t2677_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2677_MIs, t2677_PIs, NULL, NULL, NULL, NULL, NULL, &t2677_TI, t2677_ITIs, NULL, &EmptyCustomAttributesCache, &t2677_TI, &t2677_0_0_0, &t2677_1_0_0, NULL, &t2677_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.StencilMaterial/MatEntry>
extern Il2CppType t2676_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28420_GM;
MethodInfo m28420_MI = 
{
	"GetEnumerator", NULL, &t2678_TI, &t2676_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28420_GM};
static MethodInfo* t2678_MIs[] =
{
	&m28420_MI,
	NULL
};
static TypeInfo* t2678_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2678_1_0_0;
struct t2678;
extern Il2CppGenericClass t2678_GC;
TypeInfo t2678_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2678_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2678_TI, t2678_ITIs, NULL, &EmptyCustomAttributesCache, &t2678_TI, &t2678_0_0_0, &t2678_1_0_0, NULL, &t2678_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.StencilMaterial/MatEntry>
static PropertyInfo t2676____Current_PropertyInfo = 
{
	&t2676_TI, "Current", &m28421_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2676_PIs[] =
{
	&t2676____Current_PropertyInfo,
	NULL
};
extern Il2CppType t235_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28421_GM;
MethodInfo m28421_MI = 
{
	"get_Current", NULL, &t2676_TI, &t235_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28421_GM};
static MethodInfo* t2676_MIs[] =
{
	&m28421_MI,
	NULL
};
static TypeInfo* t2676_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2676_0_0_0;
extern Il2CppType t2676_1_0_0;
struct t2676;
extern Il2CppGenericClass t2676_GC;
TypeInfo t2676_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2676_MIs, t2676_PIs, NULL, NULL, NULL, NULL, NULL, &t2676_TI, t2676_ITIs, NULL, &EmptyCustomAttributesCache, &t2676_TI, &t2676_0_0_0, &t2676_1_0_0, NULL, &t2676_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
