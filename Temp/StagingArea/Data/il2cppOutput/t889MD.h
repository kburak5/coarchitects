﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t889;
struct t881;
struct t878;

 void m3834 (t889 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t881 * m3835 (t889 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3836 (t889 * __this, t881 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3837 (t889 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3838 (t889 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3839 (t889 * __this, int32_t* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3840 (t889 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
