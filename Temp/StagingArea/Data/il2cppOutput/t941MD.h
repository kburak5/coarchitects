﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t941;
struct t7;
struct t1643;
#include "t1644.h"
#include "t1112.h"

 bool m9237 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4066 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9238 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9239 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1643 * m9240 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9241 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5218 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9242 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5189 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9243 (t29 * __this, t7* p0, t7* p1, t7* p2, t7* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9244 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9245 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9246 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
