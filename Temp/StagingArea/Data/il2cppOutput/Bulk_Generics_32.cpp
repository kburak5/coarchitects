﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t6345_TI;

#include "t44.h"
#include "t40.h"
#include "t21.h"
#include "t630.h"
#include "mscorlib_ArrayTypes.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.BindingFlags>
extern MethodInfo m32912_MI;
static PropertyInfo t6345____Count_PropertyInfo = 
{
	&t6345_TI, "Count", &m32912_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32913_MI;
static PropertyInfo t6345____IsReadOnly_PropertyInfo = 
{
	&t6345_TI, "IsReadOnly", &m32913_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6345_PIs[] =
{
	&t6345____Count_PropertyInfo,
	&t6345____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32912_GM;
MethodInfo m32912_MI = 
{
	"get_Count", NULL, &t6345_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32912_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32913_GM;
MethodInfo m32913_MI = 
{
	"get_IsReadOnly", NULL, &t6345_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32913_GM};
extern Il2CppType t630_0_0_0;
extern Il2CppType t630_0_0_0;
static ParameterInfo t6345_m32914_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t630_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32914_GM;
MethodInfo m32914_MI = 
{
	"Add", NULL, &t6345_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6345_m32914_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32914_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32915_GM;
MethodInfo m32915_MI = 
{
	"Clear", NULL, &t6345_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32915_GM};
extern Il2CppType t630_0_0_0;
static ParameterInfo t6345_m32916_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t630_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32916_GM;
MethodInfo m32916_MI = 
{
	"Contains", NULL, &t6345_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6345_m32916_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32916_GM};
extern Il2CppType t3641_0_0_0;
extern Il2CppType t3641_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6345_m32917_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3641_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32917_GM;
MethodInfo m32917_MI = 
{
	"CopyTo", NULL, &t6345_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6345_m32917_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32917_GM};
extern Il2CppType t630_0_0_0;
static ParameterInfo t6345_m32918_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t630_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32918_GM;
MethodInfo m32918_MI = 
{
	"Remove", NULL, &t6345_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6345_m32918_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32918_GM};
static MethodInfo* t6345_MIs[] =
{
	&m32912_MI,
	&m32913_MI,
	&m32914_MI,
	&m32915_MI,
	&m32916_MI,
	&m32917_MI,
	&m32918_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t6347_TI;
static TypeInfo* t6345_ITIs[] = 
{
	&t603_TI,
	&t6347_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6345_0_0_0;
extern Il2CppType t6345_1_0_0;
struct t6345;
extern Il2CppGenericClass t6345_GC;
TypeInfo t6345_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6345_MIs, t6345_PIs, NULL, NULL, NULL, NULL, NULL, &t6345_TI, t6345_ITIs, NULL, &EmptyCustomAttributesCache, &t6345_TI, &t6345_0_0_0, &t6345_1_0_0, NULL, &t6345_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.BindingFlags>
extern Il2CppType t4867_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32919_GM;
MethodInfo m32919_MI = 
{
	"GetEnumerator", NULL, &t6347_TI, &t4867_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32919_GM};
static MethodInfo* t6347_MIs[] =
{
	&m32919_MI,
	NULL
};
static TypeInfo* t6347_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6347_0_0_0;
extern Il2CppType t6347_1_0_0;
struct t6347;
extern Il2CppGenericClass t6347_GC;
TypeInfo t6347_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6347_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6347_TI, t6347_ITIs, NULL, &EmptyCustomAttributesCache, &t6347_TI, &t6347_0_0_0, &t6347_1_0_0, NULL, &t6347_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6346_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.BindingFlags>
extern MethodInfo m32920_MI;
extern MethodInfo m32921_MI;
static PropertyInfo t6346____Item_PropertyInfo = 
{
	&t6346_TI, "Item", &m32920_MI, &m32921_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6346_PIs[] =
{
	&t6346____Item_PropertyInfo,
	NULL
};
extern Il2CppType t630_0_0_0;
static ParameterInfo t6346_m32922_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t630_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32922_GM;
MethodInfo m32922_MI = 
{
	"IndexOf", NULL, &t6346_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6346_m32922_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32922_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t630_0_0_0;
static ParameterInfo t6346_m32923_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t630_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32923_GM;
MethodInfo m32923_MI = 
{
	"Insert", NULL, &t6346_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6346_m32923_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32923_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6346_m32924_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32924_GM;
MethodInfo m32924_MI = 
{
	"RemoveAt", NULL, &t6346_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6346_m32924_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32924_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6346_m32920_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t630_0_0_0;
extern void* RuntimeInvoker_t630_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32920_GM;
MethodInfo m32920_MI = 
{
	"get_Item", NULL, &t6346_TI, &t630_0_0_0, RuntimeInvoker_t630_t44, t6346_m32920_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32920_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t630_0_0_0;
static ParameterInfo t6346_m32921_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t630_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32921_GM;
MethodInfo m32921_MI = 
{
	"set_Item", NULL, &t6346_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6346_m32921_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32921_GM};
static MethodInfo* t6346_MIs[] =
{
	&m32922_MI,
	&m32923_MI,
	&m32924_MI,
	&m32920_MI,
	&m32921_MI,
	NULL
};
static TypeInfo* t6346_ITIs[] = 
{
	&t603_TI,
	&t6345_TI,
	&t6347_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6346_0_0_0;
extern Il2CppType t6346_1_0_0;
struct t6346;
extern Il2CppGenericClass t6346_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6346_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6346_MIs, t6346_PIs, NULL, NULL, NULL, NULL, NULL, &t6346_TI, t6346_ITIs, NULL, &t1908__CustomAttributeCache, &t6346_TI, &t6346_0_0_0, &t6346_1_0_0, NULL, &t6346_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4869_TI;

#include "t1150.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.CallingConventions>
extern MethodInfo m32925_MI;
static PropertyInfo t4869____Current_PropertyInfo = 
{
	&t4869_TI, "Current", &m32925_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4869_PIs[] =
{
	&t4869____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1150_0_0_0;
extern void* RuntimeInvoker_t1150 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32925_GM;
MethodInfo m32925_MI = 
{
	"get_Current", NULL, &t4869_TI, &t1150_0_0_0, RuntimeInvoker_t1150, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32925_GM};
static MethodInfo* t4869_MIs[] =
{
	&m32925_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4869_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4869_0_0_0;
extern Il2CppType t4869_1_0_0;
struct t4869;
extern Il2CppGenericClass t4869_GC;
TypeInfo t4869_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4869_MIs, t4869_PIs, NULL, NULL, NULL, NULL, NULL, &t4869_TI, t4869_ITIs, NULL, &EmptyCustomAttributesCache, &t4869_TI, &t4869_0_0_0, &t4869_1_0_0, NULL, &t4869_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3385.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3385_TI;
#include "t3385MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
extern TypeInfo t1150_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m18801_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m25202_MI;
struct t20;
#include "t915.h"
 int32_t m25202 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18797_MI;
 void m18797 (t3385 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18798_MI;
 t29 * m18798 (t3385 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18801(__this, &m18801_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1150_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18799_MI;
 void m18799 (t3385 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18800_MI;
 bool m18800 (t3385 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18801 (t3385 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25202(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25202_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>
extern Il2CppType t20_0_0_1;
FieldInfo t3385_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3385_TI, offsetof(t3385, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3385_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3385_TI, offsetof(t3385, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3385_FIs[] =
{
	&t3385_f0_FieldInfo,
	&t3385_f1_FieldInfo,
	NULL
};
static PropertyInfo t3385____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3385_TI, "System.Collections.IEnumerator.Current", &m18798_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3385____Current_PropertyInfo = 
{
	&t3385_TI, "Current", &m18801_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3385_PIs[] =
{
	&t3385____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3385____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3385_m18797_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18797_GM;
MethodInfo m18797_MI = 
{
	".ctor", (methodPointerType)&m18797, &t3385_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3385_m18797_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18797_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18798_GM;
MethodInfo m18798_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18798, &t3385_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18798_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18799_GM;
MethodInfo m18799_MI = 
{
	"Dispose", (methodPointerType)&m18799, &t3385_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18799_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18800_GM;
MethodInfo m18800_MI = 
{
	"MoveNext", (methodPointerType)&m18800, &t3385_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18800_GM};
extern Il2CppType t1150_0_0_0;
extern void* RuntimeInvoker_t1150 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18801_GM;
MethodInfo m18801_MI = 
{
	"get_Current", (methodPointerType)&m18801, &t3385_TI, &t1150_0_0_0, RuntimeInvoker_t1150, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18801_GM};
static MethodInfo* t3385_MIs[] =
{
	&m18797_MI,
	&m18798_MI,
	&m18799_MI,
	&m18800_MI,
	&m18801_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t3385_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18798_MI,
	&m18800_MI,
	&m18799_MI,
	&m18801_MI,
};
static TypeInfo* t3385_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4869_TI,
};
static Il2CppInterfaceOffsetPair t3385_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4869_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3385_0_0_0;
extern Il2CppType t3385_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3385_GC;
extern TypeInfo t20_TI;
TypeInfo t3385_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3385_MIs, t3385_PIs, t3385_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3385_TI, t3385_ITIs, t3385_VT, &EmptyCustomAttributesCache, &t3385_TI, &t3385_0_0_0, &t3385_1_0_0, t3385_IOs, &t3385_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3385)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6348_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.CallingConventions>
extern MethodInfo m32926_MI;
static PropertyInfo t6348____Count_PropertyInfo = 
{
	&t6348_TI, "Count", &m32926_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32927_MI;
static PropertyInfo t6348____IsReadOnly_PropertyInfo = 
{
	&t6348_TI, "IsReadOnly", &m32927_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6348_PIs[] =
{
	&t6348____Count_PropertyInfo,
	&t6348____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32926_GM;
MethodInfo m32926_MI = 
{
	"get_Count", NULL, &t6348_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32926_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32927_GM;
MethodInfo m32927_MI = 
{
	"get_IsReadOnly", NULL, &t6348_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32927_GM};
extern Il2CppType t1150_0_0_0;
extern Il2CppType t1150_0_0_0;
static ParameterInfo t6348_m32928_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1150_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32928_GM;
MethodInfo m32928_MI = 
{
	"Add", NULL, &t6348_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6348_m32928_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32928_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32929_GM;
MethodInfo m32929_MI = 
{
	"Clear", NULL, &t6348_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32929_GM};
extern Il2CppType t1150_0_0_0;
static ParameterInfo t6348_m32930_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1150_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32930_GM;
MethodInfo m32930_MI = 
{
	"Contains", NULL, &t6348_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6348_m32930_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32930_GM};
extern Il2CppType t3642_0_0_0;
extern Il2CppType t3642_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6348_m32931_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3642_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32931_GM;
MethodInfo m32931_MI = 
{
	"CopyTo", NULL, &t6348_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6348_m32931_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32931_GM};
extern Il2CppType t1150_0_0_0;
static ParameterInfo t6348_m32932_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1150_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32932_GM;
MethodInfo m32932_MI = 
{
	"Remove", NULL, &t6348_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6348_m32932_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32932_GM};
static MethodInfo* t6348_MIs[] =
{
	&m32926_MI,
	&m32927_MI,
	&m32928_MI,
	&m32929_MI,
	&m32930_MI,
	&m32931_MI,
	&m32932_MI,
	NULL
};
extern TypeInfo t6350_TI;
static TypeInfo* t6348_ITIs[] = 
{
	&t603_TI,
	&t6350_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6348_0_0_0;
extern Il2CppType t6348_1_0_0;
struct t6348;
extern Il2CppGenericClass t6348_GC;
TypeInfo t6348_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6348_MIs, t6348_PIs, NULL, NULL, NULL, NULL, NULL, &t6348_TI, t6348_ITIs, NULL, &EmptyCustomAttributesCache, &t6348_TI, &t6348_0_0_0, &t6348_1_0_0, NULL, &t6348_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.CallingConventions>
extern Il2CppType t4869_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32933_GM;
MethodInfo m32933_MI = 
{
	"GetEnumerator", NULL, &t6350_TI, &t4869_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32933_GM};
static MethodInfo* t6350_MIs[] =
{
	&m32933_MI,
	NULL
};
static TypeInfo* t6350_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6350_0_0_0;
extern Il2CppType t6350_1_0_0;
struct t6350;
extern Il2CppGenericClass t6350_GC;
TypeInfo t6350_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6350_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6350_TI, t6350_ITIs, NULL, &EmptyCustomAttributesCache, &t6350_TI, &t6350_0_0_0, &t6350_1_0_0, NULL, &t6350_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6349_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.CallingConventions>
extern MethodInfo m32934_MI;
extern MethodInfo m32935_MI;
static PropertyInfo t6349____Item_PropertyInfo = 
{
	&t6349_TI, "Item", &m32934_MI, &m32935_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6349_PIs[] =
{
	&t6349____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1150_0_0_0;
static ParameterInfo t6349_m32936_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1150_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32936_GM;
MethodInfo m32936_MI = 
{
	"IndexOf", NULL, &t6349_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6349_m32936_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32936_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1150_0_0_0;
static ParameterInfo t6349_m32937_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1150_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32937_GM;
MethodInfo m32937_MI = 
{
	"Insert", NULL, &t6349_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6349_m32937_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32937_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6349_m32938_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32938_GM;
MethodInfo m32938_MI = 
{
	"RemoveAt", NULL, &t6349_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6349_m32938_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32938_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6349_m32934_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1150_0_0_0;
extern void* RuntimeInvoker_t1150_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32934_GM;
MethodInfo m32934_MI = 
{
	"get_Item", NULL, &t6349_TI, &t1150_0_0_0, RuntimeInvoker_t1150_t44, t6349_m32934_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32934_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1150_0_0_0;
static ParameterInfo t6349_m32935_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1150_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32935_GM;
MethodInfo m32935_MI = 
{
	"set_Item", NULL, &t6349_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6349_m32935_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32935_GM};
static MethodInfo* t6349_MIs[] =
{
	&m32936_MI,
	&m32937_MI,
	&m32938_MI,
	&m32934_MI,
	&m32935_MI,
	NULL
};
static TypeInfo* t6349_ITIs[] = 
{
	&t603_TI,
	&t6348_TI,
	&t6350_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6349_0_0_0;
extern Il2CppType t6349_1_0_0;
struct t6349;
extern Il2CppGenericClass t6349_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6349_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6349_MIs, t6349_PIs, NULL, NULL, NULL, NULL, NULL, &t6349_TI, t6349_ITIs, NULL, &t1908__CustomAttributeCache, &t6349_TI, &t6349_0_0_0, &t6349_1_0_0, NULL, &t6349_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4871_TI;

#include "t1367.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.EventAttributes>
extern MethodInfo m32939_MI;
static PropertyInfo t4871____Current_PropertyInfo = 
{
	&t4871_TI, "Current", &m32939_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4871_PIs[] =
{
	&t4871____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1367_0_0_0;
extern void* RuntimeInvoker_t1367 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32939_GM;
MethodInfo m32939_MI = 
{
	"get_Current", NULL, &t4871_TI, &t1367_0_0_0, RuntimeInvoker_t1367, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32939_GM};
static MethodInfo* t4871_MIs[] =
{
	&m32939_MI,
	NULL
};
static TypeInfo* t4871_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4871_0_0_0;
extern Il2CppType t4871_1_0_0;
struct t4871;
extern Il2CppGenericClass t4871_GC;
TypeInfo t4871_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4871_MIs, t4871_PIs, NULL, NULL, NULL, NULL, NULL, &t4871_TI, t4871_ITIs, NULL, &EmptyCustomAttributesCache, &t4871_TI, &t4871_0_0_0, &t4871_1_0_0, NULL, &t4871_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3386.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3386_TI;
#include "t3386MD.h"

extern TypeInfo t1367_TI;
extern MethodInfo m18806_MI;
extern MethodInfo m25213_MI;
struct t20;
 int32_t m25213 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18802_MI;
 void m18802 (t3386 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18803_MI;
 t29 * m18803 (t3386 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18806(__this, &m18806_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1367_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18804_MI;
 void m18804 (t3386 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18805_MI;
 bool m18805 (t3386 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18806 (t3386 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25213(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25213_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.EventAttributes>
extern Il2CppType t20_0_0_1;
FieldInfo t3386_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3386_TI, offsetof(t3386, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3386_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3386_TI, offsetof(t3386, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3386_FIs[] =
{
	&t3386_f0_FieldInfo,
	&t3386_f1_FieldInfo,
	NULL
};
static PropertyInfo t3386____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3386_TI, "System.Collections.IEnumerator.Current", &m18803_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3386____Current_PropertyInfo = 
{
	&t3386_TI, "Current", &m18806_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3386_PIs[] =
{
	&t3386____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3386____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3386_m18802_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18802_GM;
MethodInfo m18802_MI = 
{
	".ctor", (methodPointerType)&m18802, &t3386_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3386_m18802_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18802_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18803_GM;
MethodInfo m18803_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18803, &t3386_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18803_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18804_GM;
MethodInfo m18804_MI = 
{
	"Dispose", (methodPointerType)&m18804, &t3386_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18804_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18805_GM;
MethodInfo m18805_MI = 
{
	"MoveNext", (methodPointerType)&m18805, &t3386_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18805_GM};
extern Il2CppType t1367_0_0_0;
extern void* RuntimeInvoker_t1367 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18806_GM;
MethodInfo m18806_MI = 
{
	"get_Current", (methodPointerType)&m18806, &t3386_TI, &t1367_0_0_0, RuntimeInvoker_t1367, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18806_GM};
static MethodInfo* t3386_MIs[] =
{
	&m18802_MI,
	&m18803_MI,
	&m18804_MI,
	&m18805_MI,
	&m18806_MI,
	NULL
};
static MethodInfo* t3386_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18803_MI,
	&m18805_MI,
	&m18804_MI,
	&m18806_MI,
};
static TypeInfo* t3386_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4871_TI,
};
static Il2CppInterfaceOffsetPair t3386_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4871_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3386_0_0_0;
extern Il2CppType t3386_1_0_0;
extern Il2CppGenericClass t3386_GC;
TypeInfo t3386_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3386_MIs, t3386_PIs, t3386_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3386_TI, t3386_ITIs, t3386_VT, &EmptyCustomAttributesCache, &t3386_TI, &t3386_0_0_0, &t3386_1_0_0, t3386_IOs, &t3386_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3386)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6351_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.EventAttributes>
extern MethodInfo m32940_MI;
static PropertyInfo t6351____Count_PropertyInfo = 
{
	&t6351_TI, "Count", &m32940_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32941_MI;
static PropertyInfo t6351____IsReadOnly_PropertyInfo = 
{
	&t6351_TI, "IsReadOnly", &m32941_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6351_PIs[] =
{
	&t6351____Count_PropertyInfo,
	&t6351____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32940_GM;
MethodInfo m32940_MI = 
{
	"get_Count", NULL, &t6351_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32940_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32941_GM;
MethodInfo m32941_MI = 
{
	"get_IsReadOnly", NULL, &t6351_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32941_GM};
extern Il2CppType t1367_0_0_0;
extern Il2CppType t1367_0_0_0;
static ParameterInfo t6351_m32942_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1367_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32942_GM;
MethodInfo m32942_MI = 
{
	"Add", NULL, &t6351_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6351_m32942_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32942_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32943_GM;
MethodInfo m32943_MI = 
{
	"Clear", NULL, &t6351_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32943_GM};
extern Il2CppType t1367_0_0_0;
static ParameterInfo t6351_m32944_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1367_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32944_GM;
MethodInfo m32944_MI = 
{
	"Contains", NULL, &t6351_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6351_m32944_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32944_GM};
extern Il2CppType t3643_0_0_0;
extern Il2CppType t3643_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6351_m32945_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3643_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32945_GM;
MethodInfo m32945_MI = 
{
	"CopyTo", NULL, &t6351_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6351_m32945_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32945_GM};
extern Il2CppType t1367_0_0_0;
static ParameterInfo t6351_m32946_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1367_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32946_GM;
MethodInfo m32946_MI = 
{
	"Remove", NULL, &t6351_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6351_m32946_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32946_GM};
static MethodInfo* t6351_MIs[] =
{
	&m32940_MI,
	&m32941_MI,
	&m32942_MI,
	&m32943_MI,
	&m32944_MI,
	&m32945_MI,
	&m32946_MI,
	NULL
};
extern TypeInfo t6353_TI;
static TypeInfo* t6351_ITIs[] = 
{
	&t603_TI,
	&t6353_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6351_0_0_0;
extern Il2CppType t6351_1_0_0;
struct t6351;
extern Il2CppGenericClass t6351_GC;
TypeInfo t6351_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6351_MIs, t6351_PIs, NULL, NULL, NULL, NULL, NULL, &t6351_TI, t6351_ITIs, NULL, &EmptyCustomAttributesCache, &t6351_TI, &t6351_0_0_0, &t6351_1_0_0, NULL, &t6351_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.EventAttributes>
extern Il2CppType t4871_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32947_GM;
MethodInfo m32947_MI = 
{
	"GetEnumerator", NULL, &t6353_TI, &t4871_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32947_GM};
static MethodInfo* t6353_MIs[] =
{
	&m32947_MI,
	NULL
};
static TypeInfo* t6353_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6353_0_0_0;
extern Il2CppType t6353_1_0_0;
struct t6353;
extern Il2CppGenericClass t6353_GC;
TypeInfo t6353_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6353_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6353_TI, t6353_ITIs, NULL, &EmptyCustomAttributesCache, &t6353_TI, &t6353_0_0_0, &t6353_1_0_0, NULL, &t6353_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6352_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.EventAttributes>
extern MethodInfo m32948_MI;
extern MethodInfo m32949_MI;
static PropertyInfo t6352____Item_PropertyInfo = 
{
	&t6352_TI, "Item", &m32948_MI, &m32949_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6352_PIs[] =
{
	&t6352____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1367_0_0_0;
static ParameterInfo t6352_m32950_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1367_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32950_GM;
MethodInfo m32950_MI = 
{
	"IndexOf", NULL, &t6352_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6352_m32950_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32950_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1367_0_0_0;
static ParameterInfo t6352_m32951_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1367_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32951_GM;
MethodInfo m32951_MI = 
{
	"Insert", NULL, &t6352_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6352_m32951_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32951_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6352_m32952_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32952_GM;
MethodInfo m32952_MI = 
{
	"RemoveAt", NULL, &t6352_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6352_m32952_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32952_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6352_m32948_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1367_0_0_0;
extern void* RuntimeInvoker_t1367_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32948_GM;
MethodInfo m32948_MI = 
{
	"get_Item", NULL, &t6352_TI, &t1367_0_0_0, RuntimeInvoker_t1367_t44, t6352_m32948_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32948_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1367_0_0_0;
static ParameterInfo t6352_m32949_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1367_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32949_GM;
MethodInfo m32949_MI = 
{
	"set_Item", NULL, &t6352_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6352_m32949_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32949_GM};
static MethodInfo* t6352_MIs[] =
{
	&m32950_MI,
	&m32951_MI,
	&m32952_MI,
	&m32948_MI,
	&m32949_MI,
	NULL
};
static TypeInfo* t6352_ITIs[] = 
{
	&t603_TI,
	&t6351_TI,
	&t6353_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6352_0_0_0;
extern Il2CppType t6352_1_0_0;
struct t6352;
extern Il2CppGenericClass t6352_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6352_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6352_MIs, t6352_PIs, NULL, NULL, NULL, NULL, NULL, &t6352_TI, t6352_ITIs, NULL, &t1908__CustomAttributeCache, &t6352_TI, &t6352_0_0_0, &t6352_1_0_0, NULL, &t6352_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4873_TI;

#include "t1347.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.FieldAttributes>
extern MethodInfo m32953_MI;
static PropertyInfo t4873____Current_PropertyInfo = 
{
	&t4873_TI, "Current", &m32953_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4873_PIs[] =
{
	&t4873____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1347_0_0_0;
extern void* RuntimeInvoker_t1347 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32953_GM;
MethodInfo m32953_MI = 
{
	"get_Current", NULL, &t4873_TI, &t1347_0_0_0, RuntimeInvoker_t1347, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32953_GM};
static MethodInfo* t4873_MIs[] =
{
	&m32953_MI,
	NULL
};
static TypeInfo* t4873_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4873_0_0_0;
extern Il2CppType t4873_1_0_0;
struct t4873;
extern Il2CppGenericClass t4873_GC;
TypeInfo t4873_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4873_MIs, t4873_PIs, NULL, NULL, NULL, NULL, NULL, &t4873_TI, t4873_ITIs, NULL, &EmptyCustomAttributesCache, &t4873_TI, &t4873_0_0_0, &t4873_1_0_0, NULL, &t4873_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3387.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3387_TI;
#include "t3387MD.h"

extern TypeInfo t1347_TI;
extern MethodInfo m18811_MI;
extern MethodInfo m25224_MI;
struct t20;
 int32_t m25224 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18807_MI;
 void m18807 (t3387 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18808_MI;
 t29 * m18808 (t3387 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18811(__this, &m18811_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1347_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18809_MI;
 void m18809 (t3387 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18810_MI;
 bool m18810 (t3387 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18811 (t3387 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25224(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25224_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.FieldAttributes>
extern Il2CppType t20_0_0_1;
FieldInfo t3387_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3387_TI, offsetof(t3387, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3387_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3387_TI, offsetof(t3387, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3387_FIs[] =
{
	&t3387_f0_FieldInfo,
	&t3387_f1_FieldInfo,
	NULL
};
static PropertyInfo t3387____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3387_TI, "System.Collections.IEnumerator.Current", &m18808_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3387____Current_PropertyInfo = 
{
	&t3387_TI, "Current", &m18811_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3387_PIs[] =
{
	&t3387____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3387____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3387_m18807_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18807_GM;
MethodInfo m18807_MI = 
{
	".ctor", (methodPointerType)&m18807, &t3387_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3387_m18807_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18807_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18808_GM;
MethodInfo m18808_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18808, &t3387_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18808_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18809_GM;
MethodInfo m18809_MI = 
{
	"Dispose", (methodPointerType)&m18809, &t3387_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18809_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18810_GM;
MethodInfo m18810_MI = 
{
	"MoveNext", (methodPointerType)&m18810, &t3387_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18810_GM};
extern Il2CppType t1347_0_0_0;
extern void* RuntimeInvoker_t1347 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18811_GM;
MethodInfo m18811_MI = 
{
	"get_Current", (methodPointerType)&m18811, &t3387_TI, &t1347_0_0_0, RuntimeInvoker_t1347, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18811_GM};
static MethodInfo* t3387_MIs[] =
{
	&m18807_MI,
	&m18808_MI,
	&m18809_MI,
	&m18810_MI,
	&m18811_MI,
	NULL
};
static MethodInfo* t3387_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18808_MI,
	&m18810_MI,
	&m18809_MI,
	&m18811_MI,
};
static TypeInfo* t3387_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4873_TI,
};
static Il2CppInterfaceOffsetPair t3387_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4873_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3387_0_0_0;
extern Il2CppType t3387_1_0_0;
extern Il2CppGenericClass t3387_GC;
TypeInfo t3387_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3387_MIs, t3387_PIs, t3387_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3387_TI, t3387_ITIs, t3387_VT, &EmptyCustomAttributesCache, &t3387_TI, &t3387_0_0_0, &t3387_1_0_0, t3387_IOs, &t3387_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3387)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6354_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.FieldAttributes>
extern MethodInfo m32954_MI;
static PropertyInfo t6354____Count_PropertyInfo = 
{
	&t6354_TI, "Count", &m32954_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32955_MI;
static PropertyInfo t6354____IsReadOnly_PropertyInfo = 
{
	&t6354_TI, "IsReadOnly", &m32955_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6354_PIs[] =
{
	&t6354____Count_PropertyInfo,
	&t6354____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32954_GM;
MethodInfo m32954_MI = 
{
	"get_Count", NULL, &t6354_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32954_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32955_GM;
MethodInfo m32955_MI = 
{
	"get_IsReadOnly", NULL, &t6354_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32955_GM};
extern Il2CppType t1347_0_0_0;
extern Il2CppType t1347_0_0_0;
static ParameterInfo t6354_m32956_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1347_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32956_GM;
MethodInfo m32956_MI = 
{
	"Add", NULL, &t6354_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6354_m32956_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32956_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32957_GM;
MethodInfo m32957_MI = 
{
	"Clear", NULL, &t6354_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32957_GM};
extern Il2CppType t1347_0_0_0;
static ParameterInfo t6354_m32958_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1347_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32958_GM;
MethodInfo m32958_MI = 
{
	"Contains", NULL, &t6354_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6354_m32958_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32958_GM};
extern Il2CppType t3644_0_0_0;
extern Il2CppType t3644_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6354_m32959_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3644_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32959_GM;
MethodInfo m32959_MI = 
{
	"CopyTo", NULL, &t6354_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6354_m32959_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32959_GM};
extern Il2CppType t1347_0_0_0;
static ParameterInfo t6354_m32960_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1347_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32960_GM;
MethodInfo m32960_MI = 
{
	"Remove", NULL, &t6354_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6354_m32960_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32960_GM};
static MethodInfo* t6354_MIs[] =
{
	&m32954_MI,
	&m32955_MI,
	&m32956_MI,
	&m32957_MI,
	&m32958_MI,
	&m32959_MI,
	&m32960_MI,
	NULL
};
extern TypeInfo t6356_TI;
static TypeInfo* t6354_ITIs[] = 
{
	&t603_TI,
	&t6356_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6354_0_0_0;
extern Il2CppType t6354_1_0_0;
struct t6354;
extern Il2CppGenericClass t6354_GC;
TypeInfo t6354_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6354_MIs, t6354_PIs, NULL, NULL, NULL, NULL, NULL, &t6354_TI, t6354_ITIs, NULL, &EmptyCustomAttributesCache, &t6354_TI, &t6354_0_0_0, &t6354_1_0_0, NULL, &t6354_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.FieldAttributes>
extern Il2CppType t4873_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32961_GM;
MethodInfo m32961_MI = 
{
	"GetEnumerator", NULL, &t6356_TI, &t4873_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32961_GM};
static MethodInfo* t6356_MIs[] =
{
	&m32961_MI,
	NULL
};
static TypeInfo* t6356_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6356_0_0_0;
extern Il2CppType t6356_1_0_0;
struct t6356;
extern Il2CppGenericClass t6356_GC;
TypeInfo t6356_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6356_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6356_TI, t6356_ITIs, NULL, &EmptyCustomAttributesCache, &t6356_TI, &t6356_0_0_0, &t6356_1_0_0, NULL, &t6356_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6355_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.FieldAttributes>
extern MethodInfo m32962_MI;
extern MethodInfo m32963_MI;
static PropertyInfo t6355____Item_PropertyInfo = 
{
	&t6355_TI, "Item", &m32962_MI, &m32963_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6355_PIs[] =
{
	&t6355____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1347_0_0_0;
static ParameterInfo t6355_m32964_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1347_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32964_GM;
MethodInfo m32964_MI = 
{
	"IndexOf", NULL, &t6355_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6355_m32964_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32964_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1347_0_0_0;
static ParameterInfo t6355_m32965_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1347_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32965_GM;
MethodInfo m32965_MI = 
{
	"Insert", NULL, &t6355_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6355_m32965_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32965_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6355_m32966_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32966_GM;
MethodInfo m32966_MI = 
{
	"RemoveAt", NULL, &t6355_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6355_m32966_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32966_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6355_m32962_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1347_0_0_0;
extern void* RuntimeInvoker_t1347_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32962_GM;
MethodInfo m32962_MI = 
{
	"get_Item", NULL, &t6355_TI, &t1347_0_0_0, RuntimeInvoker_t1347_t44, t6355_m32962_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32962_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1347_0_0_0;
static ParameterInfo t6355_m32963_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1347_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32963_GM;
MethodInfo m32963_MI = 
{
	"set_Item", NULL, &t6355_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6355_m32963_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32963_GM};
static MethodInfo* t6355_MIs[] =
{
	&m32964_MI,
	&m32965_MI,
	&m32966_MI,
	&m32962_MI,
	&m32963_MI,
	NULL
};
static TypeInfo* t6355_ITIs[] = 
{
	&t603_TI,
	&t6354_TI,
	&t6356_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6355_0_0_0;
extern Il2CppType t6355_1_0_0;
struct t6355;
extern Il2CppGenericClass t6355_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6355_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6355_MIs, t6355_PIs, NULL, NULL, NULL, NULL, NULL, &t6355_TI, t6355_ITIs, NULL, &t1908__CustomAttributeCache, &t6355_TI, &t6355_0_0_0, &t6355_1_0_0, NULL, &t6355_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4875_TI;

#include "t1149.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.MemberTypes>
extern MethodInfo m32967_MI;
static PropertyInfo t4875____Current_PropertyInfo = 
{
	&t4875_TI, "Current", &m32967_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4875_PIs[] =
{
	&t4875____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1149_0_0_0;
extern void* RuntimeInvoker_t1149 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32967_GM;
MethodInfo m32967_MI = 
{
	"get_Current", NULL, &t4875_TI, &t1149_0_0_0, RuntimeInvoker_t1149, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32967_GM};
static MethodInfo* t4875_MIs[] =
{
	&m32967_MI,
	NULL
};
static TypeInfo* t4875_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4875_0_0_0;
extern Il2CppType t4875_1_0_0;
struct t4875;
extern Il2CppGenericClass t4875_GC;
TypeInfo t4875_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4875_MIs, t4875_PIs, NULL, NULL, NULL, NULL, NULL, &t4875_TI, t4875_ITIs, NULL, &EmptyCustomAttributesCache, &t4875_TI, &t4875_0_0_0, &t4875_1_0_0, NULL, &t4875_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3388.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3388_TI;
#include "t3388MD.h"

extern TypeInfo t1149_TI;
extern MethodInfo m18816_MI;
extern MethodInfo m25235_MI;
struct t20;
 int32_t m25235 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18812_MI;
 void m18812 (t3388 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18813_MI;
 t29 * m18813 (t3388 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18816(__this, &m18816_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1149_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18814_MI;
 void m18814 (t3388 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18815_MI;
 bool m18815 (t3388 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18816 (t3388 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25235(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25235_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.MemberTypes>
extern Il2CppType t20_0_0_1;
FieldInfo t3388_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3388_TI, offsetof(t3388, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3388_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3388_TI, offsetof(t3388, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3388_FIs[] =
{
	&t3388_f0_FieldInfo,
	&t3388_f1_FieldInfo,
	NULL
};
static PropertyInfo t3388____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3388_TI, "System.Collections.IEnumerator.Current", &m18813_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3388____Current_PropertyInfo = 
{
	&t3388_TI, "Current", &m18816_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3388_PIs[] =
{
	&t3388____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3388____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3388_m18812_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18812_GM;
MethodInfo m18812_MI = 
{
	".ctor", (methodPointerType)&m18812, &t3388_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3388_m18812_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18812_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18813_GM;
MethodInfo m18813_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18813, &t3388_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18813_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18814_GM;
MethodInfo m18814_MI = 
{
	"Dispose", (methodPointerType)&m18814, &t3388_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18814_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18815_GM;
MethodInfo m18815_MI = 
{
	"MoveNext", (methodPointerType)&m18815, &t3388_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18815_GM};
extern Il2CppType t1149_0_0_0;
extern void* RuntimeInvoker_t1149 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18816_GM;
MethodInfo m18816_MI = 
{
	"get_Current", (methodPointerType)&m18816, &t3388_TI, &t1149_0_0_0, RuntimeInvoker_t1149, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18816_GM};
static MethodInfo* t3388_MIs[] =
{
	&m18812_MI,
	&m18813_MI,
	&m18814_MI,
	&m18815_MI,
	&m18816_MI,
	NULL
};
static MethodInfo* t3388_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18813_MI,
	&m18815_MI,
	&m18814_MI,
	&m18816_MI,
};
static TypeInfo* t3388_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4875_TI,
};
static Il2CppInterfaceOffsetPair t3388_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4875_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3388_0_0_0;
extern Il2CppType t3388_1_0_0;
extern Il2CppGenericClass t3388_GC;
TypeInfo t3388_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3388_MIs, t3388_PIs, t3388_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3388_TI, t3388_ITIs, t3388_VT, &EmptyCustomAttributesCache, &t3388_TI, &t3388_0_0_0, &t3388_1_0_0, t3388_IOs, &t3388_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3388)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6357_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.MemberTypes>
extern MethodInfo m32968_MI;
static PropertyInfo t6357____Count_PropertyInfo = 
{
	&t6357_TI, "Count", &m32968_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32969_MI;
static PropertyInfo t6357____IsReadOnly_PropertyInfo = 
{
	&t6357_TI, "IsReadOnly", &m32969_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6357_PIs[] =
{
	&t6357____Count_PropertyInfo,
	&t6357____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32968_GM;
MethodInfo m32968_MI = 
{
	"get_Count", NULL, &t6357_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32968_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32969_GM;
MethodInfo m32969_MI = 
{
	"get_IsReadOnly", NULL, &t6357_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32969_GM};
extern Il2CppType t1149_0_0_0;
extern Il2CppType t1149_0_0_0;
static ParameterInfo t6357_m32970_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1149_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32970_GM;
MethodInfo m32970_MI = 
{
	"Add", NULL, &t6357_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6357_m32970_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32970_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32971_GM;
MethodInfo m32971_MI = 
{
	"Clear", NULL, &t6357_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32971_GM};
extern Il2CppType t1149_0_0_0;
static ParameterInfo t6357_m32972_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1149_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32972_GM;
MethodInfo m32972_MI = 
{
	"Contains", NULL, &t6357_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6357_m32972_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32972_GM};
extern Il2CppType t3645_0_0_0;
extern Il2CppType t3645_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6357_m32973_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3645_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32973_GM;
MethodInfo m32973_MI = 
{
	"CopyTo", NULL, &t6357_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6357_m32973_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32973_GM};
extern Il2CppType t1149_0_0_0;
static ParameterInfo t6357_m32974_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1149_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32974_GM;
MethodInfo m32974_MI = 
{
	"Remove", NULL, &t6357_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6357_m32974_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32974_GM};
static MethodInfo* t6357_MIs[] =
{
	&m32968_MI,
	&m32969_MI,
	&m32970_MI,
	&m32971_MI,
	&m32972_MI,
	&m32973_MI,
	&m32974_MI,
	NULL
};
extern TypeInfo t6359_TI;
static TypeInfo* t6357_ITIs[] = 
{
	&t603_TI,
	&t6359_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6357_0_0_0;
extern Il2CppType t6357_1_0_0;
struct t6357;
extern Il2CppGenericClass t6357_GC;
TypeInfo t6357_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6357_MIs, t6357_PIs, NULL, NULL, NULL, NULL, NULL, &t6357_TI, t6357_ITIs, NULL, &EmptyCustomAttributesCache, &t6357_TI, &t6357_0_0_0, &t6357_1_0_0, NULL, &t6357_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.MemberTypes>
extern Il2CppType t4875_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32975_GM;
MethodInfo m32975_MI = 
{
	"GetEnumerator", NULL, &t6359_TI, &t4875_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32975_GM};
static MethodInfo* t6359_MIs[] =
{
	&m32975_MI,
	NULL
};
static TypeInfo* t6359_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6359_0_0_0;
extern Il2CppType t6359_1_0_0;
struct t6359;
extern Il2CppGenericClass t6359_GC;
TypeInfo t6359_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6359_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6359_TI, t6359_ITIs, NULL, &EmptyCustomAttributesCache, &t6359_TI, &t6359_0_0_0, &t6359_1_0_0, NULL, &t6359_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6358_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.MemberTypes>
extern MethodInfo m32976_MI;
extern MethodInfo m32977_MI;
static PropertyInfo t6358____Item_PropertyInfo = 
{
	&t6358_TI, "Item", &m32976_MI, &m32977_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6358_PIs[] =
{
	&t6358____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1149_0_0_0;
static ParameterInfo t6358_m32978_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1149_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32978_GM;
MethodInfo m32978_MI = 
{
	"IndexOf", NULL, &t6358_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6358_m32978_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32978_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1149_0_0_0;
static ParameterInfo t6358_m32979_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1149_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32979_GM;
MethodInfo m32979_MI = 
{
	"Insert", NULL, &t6358_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6358_m32979_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32979_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6358_m32980_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32980_GM;
MethodInfo m32980_MI = 
{
	"RemoveAt", NULL, &t6358_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6358_m32980_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32980_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6358_m32976_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1149_0_0_0;
extern void* RuntimeInvoker_t1149_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32976_GM;
MethodInfo m32976_MI = 
{
	"get_Item", NULL, &t6358_TI, &t1149_0_0_0, RuntimeInvoker_t1149_t44, t6358_m32976_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32976_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1149_0_0_0;
static ParameterInfo t6358_m32977_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1149_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32977_GM;
MethodInfo m32977_MI = 
{
	"set_Item", NULL, &t6358_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6358_m32977_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32977_GM};
static MethodInfo* t6358_MIs[] =
{
	&m32978_MI,
	&m32979_MI,
	&m32980_MI,
	&m32976_MI,
	&m32977_MI,
	NULL
};
static TypeInfo* t6358_ITIs[] = 
{
	&t603_TI,
	&t6357_TI,
	&t6359_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6358_0_0_0;
extern Il2CppType t6358_1_0_0;
struct t6358;
extern Il2CppGenericClass t6358_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6358_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6358_MIs, t6358_PIs, NULL, NULL, NULL, NULL, NULL, &t6358_TI, t6358_ITIs, NULL, &t1908__CustomAttributeCache, &t6358_TI, &t6358_0_0_0, &t6358_1_0_0, NULL, &t6358_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4877_TI;

#include "t1342.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.MethodAttributes>
extern MethodInfo m32981_MI;
static PropertyInfo t4877____Current_PropertyInfo = 
{
	&t4877_TI, "Current", &m32981_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4877_PIs[] =
{
	&t4877____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1342_0_0_0;
extern void* RuntimeInvoker_t1342 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32981_GM;
MethodInfo m32981_MI = 
{
	"get_Current", NULL, &t4877_TI, &t1342_0_0_0, RuntimeInvoker_t1342, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32981_GM};
static MethodInfo* t4877_MIs[] =
{
	&m32981_MI,
	NULL
};
static TypeInfo* t4877_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4877_0_0_0;
extern Il2CppType t4877_1_0_0;
struct t4877;
extern Il2CppGenericClass t4877_GC;
TypeInfo t4877_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4877_MIs, t4877_PIs, NULL, NULL, NULL, NULL, NULL, &t4877_TI, t4877_ITIs, NULL, &EmptyCustomAttributesCache, &t4877_TI, &t4877_0_0_0, &t4877_1_0_0, NULL, &t4877_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3389.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3389_TI;
#include "t3389MD.h"

extern TypeInfo t1342_TI;
extern MethodInfo m18821_MI;
extern MethodInfo m25246_MI;
struct t20;
 int32_t m25246 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18817_MI;
 void m18817 (t3389 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18818_MI;
 t29 * m18818 (t3389 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18821(__this, &m18821_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1342_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18819_MI;
 void m18819 (t3389 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18820_MI;
 bool m18820 (t3389 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18821 (t3389 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25246(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25246_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.MethodAttributes>
extern Il2CppType t20_0_0_1;
FieldInfo t3389_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3389_TI, offsetof(t3389, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3389_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3389_TI, offsetof(t3389, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3389_FIs[] =
{
	&t3389_f0_FieldInfo,
	&t3389_f1_FieldInfo,
	NULL
};
static PropertyInfo t3389____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3389_TI, "System.Collections.IEnumerator.Current", &m18818_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3389____Current_PropertyInfo = 
{
	&t3389_TI, "Current", &m18821_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3389_PIs[] =
{
	&t3389____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3389____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3389_m18817_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18817_GM;
MethodInfo m18817_MI = 
{
	".ctor", (methodPointerType)&m18817, &t3389_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3389_m18817_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18817_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18818_GM;
MethodInfo m18818_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18818, &t3389_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18818_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18819_GM;
MethodInfo m18819_MI = 
{
	"Dispose", (methodPointerType)&m18819, &t3389_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18819_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18820_GM;
MethodInfo m18820_MI = 
{
	"MoveNext", (methodPointerType)&m18820, &t3389_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18820_GM};
extern Il2CppType t1342_0_0_0;
extern void* RuntimeInvoker_t1342 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18821_GM;
MethodInfo m18821_MI = 
{
	"get_Current", (methodPointerType)&m18821, &t3389_TI, &t1342_0_0_0, RuntimeInvoker_t1342, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18821_GM};
static MethodInfo* t3389_MIs[] =
{
	&m18817_MI,
	&m18818_MI,
	&m18819_MI,
	&m18820_MI,
	&m18821_MI,
	NULL
};
static MethodInfo* t3389_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18818_MI,
	&m18820_MI,
	&m18819_MI,
	&m18821_MI,
};
static TypeInfo* t3389_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4877_TI,
};
static Il2CppInterfaceOffsetPair t3389_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4877_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3389_0_0_0;
extern Il2CppType t3389_1_0_0;
extern Il2CppGenericClass t3389_GC;
TypeInfo t3389_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3389_MIs, t3389_PIs, t3389_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3389_TI, t3389_ITIs, t3389_VT, &EmptyCustomAttributesCache, &t3389_TI, &t3389_0_0_0, &t3389_1_0_0, t3389_IOs, &t3389_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3389)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6360_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.MethodAttributes>
extern MethodInfo m32982_MI;
static PropertyInfo t6360____Count_PropertyInfo = 
{
	&t6360_TI, "Count", &m32982_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32983_MI;
static PropertyInfo t6360____IsReadOnly_PropertyInfo = 
{
	&t6360_TI, "IsReadOnly", &m32983_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6360_PIs[] =
{
	&t6360____Count_PropertyInfo,
	&t6360____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32982_GM;
MethodInfo m32982_MI = 
{
	"get_Count", NULL, &t6360_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32982_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32983_GM;
MethodInfo m32983_MI = 
{
	"get_IsReadOnly", NULL, &t6360_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32983_GM};
extern Il2CppType t1342_0_0_0;
extern Il2CppType t1342_0_0_0;
static ParameterInfo t6360_m32984_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1342_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32984_GM;
MethodInfo m32984_MI = 
{
	"Add", NULL, &t6360_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6360_m32984_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32984_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32985_GM;
MethodInfo m32985_MI = 
{
	"Clear", NULL, &t6360_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32985_GM};
extern Il2CppType t1342_0_0_0;
static ParameterInfo t6360_m32986_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1342_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32986_GM;
MethodInfo m32986_MI = 
{
	"Contains", NULL, &t6360_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6360_m32986_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32986_GM};
extern Il2CppType t3646_0_0_0;
extern Il2CppType t3646_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6360_m32987_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3646_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32987_GM;
MethodInfo m32987_MI = 
{
	"CopyTo", NULL, &t6360_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6360_m32987_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32987_GM};
extern Il2CppType t1342_0_0_0;
static ParameterInfo t6360_m32988_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1342_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32988_GM;
MethodInfo m32988_MI = 
{
	"Remove", NULL, &t6360_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6360_m32988_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32988_GM};
static MethodInfo* t6360_MIs[] =
{
	&m32982_MI,
	&m32983_MI,
	&m32984_MI,
	&m32985_MI,
	&m32986_MI,
	&m32987_MI,
	&m32988_MI,
	NULL
};
extern TypeInfo t6362_TI;
static TypeInfo* t6360_ITIs[] = 
{
	&t603_TI,
	&t6362_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6360_0_0_0;
extern Il2CppType t6360_1_0_0;
struct t6360;
extern Il2CppGenericClass t6360_GC;
TypeInfo t6360_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6360_MIs, t6360_PIs, NULL, NULL, NULL, NULL, NULL, &t6360_TI, t6360_ITIs, NULL, &EmptyCustomAttributesCache, &t6360_TI, &t6360_0_0_0, &t6360_1_0_0, NULL, &t6360_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.MethodAttributes>
extern Il2CppType t4877_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32989_GM;
MethodInfo m32989_MI = 
{
	"GetEnumerator", NULL, &t6362_TI, &t4877_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32989_GM};
static MethodInfo* t6362_MIs[] =
{
	&m32989_MI,
	NULL
};
static TypeInfo* t6362_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6362_0_0_0;
extern Il2CppType t6362_1_0_0;
struct t6362;
extern Il2CppGenericClass t6362_GC;
TypeInfo t6362_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6362_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6362_TI, t6362_ITIs, NULL, &EmptyCustomAttributesCache, &t6362_TI, &t6362_0_0_0, &t6362_1_0_0, NULL, &t6362_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6361_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.MethodAttributes>
extern MethodInfo m32990_MI;
extern MethodInfo m32991_MI;
static PropertyInfo t6361____Item_PropertyInfo = 
{
	&t6361_TI, "Item", &m32990_MI, &m32991_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6361_PIs[] =
{
	&t6361____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1342_0_0_0;
static ParameterInfo t6361_m32992_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1342_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32992_GM;
MethodInfo m32992_MI = 
{
	"IndexOf", NULL, &t6361_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6361_m32992_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32992_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1342_0_0_0;
static ParameterInfo t6361_m32993_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1342_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32993_GM;
MethodInfo m32993_MI = 
{
	"Insert", NULL, &t6361_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6361_m32993_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32993_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6361_m32994_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32994_GM;
MethodInfo m32994_MI = 
{
	"RemoveAt", NULL, &t6361_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6361_m32994_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32994_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6361_m32990_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1342_0_0_0;
extern void* RuntimeInvoker_t1342_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32990_GM;
MethodInfo m32990_MI = 
{
	"get_Item", NULL, &t6361_TI, &t1342_0_0_0, RuntimeInvoker_t1342_t44, t6361_m32990_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32990_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1342_0_0_0;
static ParameterInfo t6361_m32991_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1342_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32991_GM;
MethodInfo m32991_MI = 
{
	"set_Item", NULL, &t6361_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6361_m32991_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32991_GM};
static MethodInfo* t6361_MIs[] =
{
	&m32992_MI,
	&m32993_MI,
	&m32994_MI,
	&m32990_MI,
	&m32991_MI,
	NULL
};
static TypeInfo* t6361_ITIs[] = 
{
	&t603_TI,
	&t6360_TI,
	&t6362_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6361_0_0_0;
extern Il2CppType t6361_1_0_0;
struct t6361;
extern Il2CppGenericClass t6361_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6361_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6361_MIs, t6361_PIs, NULL, NULL, NULL, NULL, NULL, &t6361_TI, t6361_ITIs, NULL, &t1908__CustomAttributeCache, &t6361_TI, &t6361_0_0_0, &t6361_1_0_0, NULL, &t6361_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4879_TI;

#include "t1370.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.MethodImplAttributes>
extern MethodInfo m32995_MI;
static PropertyInfo t4879____Current_PropertyInfo = 
{
	&t4879_TI, "Current", &m32995_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4879_PIs[] =
{
	&t4879____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1370_0_0_0;
extern void* RuntimeInvoker_t1370 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32995_GM;
MethodInfo m32995_MI = 
{
	"get_Current", NULL, &t4879_TI, &t1370_0_0_0, RuntimeInvoker_t1370, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32995_GM};
static MethodInfo* t4879_MIs[] =
{
	&m32995_MI,
	NULL
};
static TypeInfo* t4879_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4879_0_0_0;
extern Il2CppType t4879_1_0_0;
struct t4879;
extern Il2CppGenericClass t4879_GC;
TypeInfo t4879_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4879_MIs, t4879_PIs, NULL, NULL, NULL, NULL, NULL, &t4879_TI, t4879_ITIs, NULL, &EmptyCustomAttributesCache, &t4879_TI, &t4879_0_0_0, &t4879_1_0_0, NULL, &t4879_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3390.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3390_TI;
#include "t3390MD.h"

extern TypeInfo t1370_TI;
extern MethodInfo m18826_MI;
extern MethodInfo m25257_MI;
struct t20;
 int32_t m25257 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18822_MI;
 void m18822 (t3390 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18823_MI;
 t29 * m18823 (t3390 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18826(__this, &m18826_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1370_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18824_MI;
 void m18824 (t3390 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18825_MI;
 bool m18825 (t3390 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18826 (t3390 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25257(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25257_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.MethodImplAttributes>
extern Il2CppType t20_0_0_1;
FieldInfo t3390_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3390_TI, offsetof(t3390, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3390_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3390_TI, offsetof(t3390, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3390_FIs[] =
{
	&t3390_f0_FieldInfo,
	&t3390_f1_FieldInfo,
	NULL
};
static PropertyInfo t3390____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3390_TI, "System.Collections.IEnumerator.Current", &m18823_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3390____Current_PropertyInfo = 
{
	&t3390_TI, "Current", &m18826_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3390_PIs[] =
{
	&t3390____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3390____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3390_m18822_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18822_GM;
MethodInfo m18822_MI = 
{
	".ctor", (methodPointerType)&m18822, &t3390_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3390_m18822_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18822_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18823_GM;
MethodInfo m18823_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18823, &t3390_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18823_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18824_GM;
MethodInfo m18824_MI = 
{
	"Dispose", (methodPointerType)&m18824, &t3390_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18824_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18825_GM;
MethodInfo m18825_MI = 
{
	"MoveNext", (methodPointerType)&m18825, &t3390_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18825_GM};
extern Il2CppType t1370_0_0_0;
extern void* RuntimeInvoker_t1370 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18826_GM;
MethodInfo m18826_MI = 
{
	"get_Current", (methodPointerType)&m18826, &t3390_TI, &t1370_0_0_0, RuntimeInvoker_t1370, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18826_GM};
static MethodInfo* t3390_MIs[] =
{
	&m18822_MI,
	&m18823_MI,
	&m18824_MI,
	&m18825_MI,
	&m18826_MI,
	NULL
};
static MethodInfo* t3390_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18823_MI,
	&m18825_MI,
	&m18824_MI,
	&m18826_MI,
};
static TypeInfo* t3390_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4879_TI,
};
static Il2CppInterfaceOffsetPair t3390_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4879_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3390_0_0_0;
extern Il2CppType t3390_1_0_0;
extern Il2CppGenericClass t3390_GC;
TypeInfo t3390_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3390_MIs, t3390_PIs, t3390_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3390_TI, t3390_ITIs, t3390_VT, &EmptyCustomAttributesCache, &t3390_TI, &t3390_0_0_0, &t3390_1_0_0, t3390_IOs, &t3390_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3390)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6363_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.MethodImplAttributes>
extern MethodInfo m32996_MI;
static PropertyInfo t6363____Count_PropertyInfo = 
{
	&t6363_TI, "Count", &m32996_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32997_MI;
static PropertyInfo t6363____IsReadOnly_PropertyInfo = 
{
	&t6363_TI, "IsReadOnly", &m32997_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6363_PIs[] =
{
	&t6363____Count_PropertyInfo,
	&t6363____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32996_GM;
MethodInfo m32996_MI = 
{
	"get_Count", NULL, &t6363_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32996_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32997_GM;
MethodInfo m32997_MI = 
{
	"get_IsReadOnly", NULL, &t6363_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32997_GM};
extern Il2CppType t1370_0_0_0;
extern Il2CppType t1370_0_0_0;
static ParameterInfo t6363_m32998_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1370_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32998_GM;
MethodInfo m32998_MI = 
{
	"Add", NULL, &t6363_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6363_m32998_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32998_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32999_GM;
MethodInfo m32999_MI = 
{
	"Clear", NULL, &t6363_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32999_GM};
extern Il2CppType t1370_0_0_0;
static ParameterInfo t6363_m33000_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1370_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33000_GM;
MethodInfo m33000_MI = 
{
	"Contains", NULL, &t6363_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6363_m33000_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33000_GM};
extern Il2CppType t3647_0_0_0;
extern Il2CppType t3647_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6363_m33001_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3647_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33001_GM;
MethodInfo m33001_MI = 
{
	"CopyTo", NULL, &t6363_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6363_m33001_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33001_GM};
extern Il2CppType t1370_0_0_0;
static ParameterInfo t6363_m33002_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1370_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33002_GM;
MethodInfo m33002_MI = 
{
	"Remove", NULL, &t6363_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6363_m33002_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33002_GM};
static MethodInfo* t6363_MIs[] =
{
	&m32996_MI,
	&m32997_MI,
	&m32998_MI,
	&m32999_MI,
	&m33000_MI,
	&m33001_MI,
	&m33002_MI,
	NULL
};
extern TypeInfo t6365_TI;
static TypeInfo* t6363_ITIs[] = 
{
	&t603_TI,
	&t6365_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6363_0_0_0;
extern Il2CppType t6363_1_0_0;
struct t6363;
extern Il2CppGenericClass t6363_GC;
TypeInfo t6363_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6363_MIs, t6363_PIs, NULL, NULL, NULL, NULL, NULL, &t6363_TI, t6363_ITIs, NULL, &EmptyCustomAttributesCache, &t6363_TI, &t6363_0_0_0, &t6363_1_0_0, NULL, &t6363_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.MethodImplAttributes>
extern Il2CppType t4879_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33003_GM;
MethodInfo m33003_MI = 
{
	"GetEnumerator", NULL, &t6365_TI, &t4879_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33003_GM};
static MethodInfo* t6365_MIs[] =
{
	&m33003_MI,
	NULL
};
static TypeInfo* t6365_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6365_0_0_0;
extern Il2CppType t6365_1_0_0;
struct t6365;
extern Il2CppGenericClass t6365_GC;
TypeInfo t6365_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6365_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6365_TI, t6365_ITIs, NULL, &EmptyCustomAttributesCache, &t6365_TI, &t6365_0_0_0, &t6365_1_0_0, NULL, &t6365_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6364_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.MethodImplAttributes>
extern MethodInfo m33004_MI;
extern MethodInfo m33005_MI;
static PropertyInfo t6364____Item_PropertyInfo = 
{
	&t6364_TI, "Item", &m33004_MI, &m33005_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6364_PIs[] =
{
	&t6364____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1370_0_0_0;
static ParameterInfo t6364_m33006_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1370_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33006_GM;
MethodInfo m33006_MI = 
{
	"IndexOf", NULL, &t6364_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6364_m33006_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33006_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1370_0_0_0;
static ParameterInfo t6364_m33007_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1370_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33007_GM;
MethodInfo m33007_MI = 
{
	"Insert", NULL, &t6364_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6364_m33007_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33007_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6364_m33008_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33008_GM;
MethodInfo m33008_MI = 
{
	"RemoveAt", NULL, &t6364_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6364_m33008_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33008_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6364_m33004_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1370_0_0_0;
extern void* RuntimeInvoker_t1370_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33004_GM;
MethodInfo m33004_MI = 
{
	"get_Item", NULL, &t6364_TI, &t1370_0_0_0, RuntimeInvoker_t1370_t44, t6364_m33004_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33004_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1370_0_0_0;
static ParameterInfo t6364_m33005_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1370_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33005_GM;
MethodInfo m33005_MI = 
{
	"set_Item", NULL, &t6364_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6364_m33005_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33005_GM};
static MethodInfo* t6364_MIs[] =
{
	&m33006_MI,
	&m33007_MI,
	&m33008_MI,
	&m33004_MI,
	&m33005_MI,
	NULL
};
static TypeInfo* t6364_ITIs[] = 
{
	&t603_TI,
	&t6363_TI,
	&t6365_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6364_0_0_0;
extern Il2CppType t6364_1_0_0;
struct t6364;
extern Il2CppGenericClass t6364_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6364_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6364_MIs, t6364_PIs, NULL, NULL, NULL, NULL, NULL, &t6364_TI, t6364_ITIs, NULL, &t1908__CustomAttributeCache, &t6364_TI, &t6364_0_0_0, &t6364_1_0_0, NULL, &t6364_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4881_TI;

#include "t1384.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.PInfo>
extern MethodInfo m33009_MI;
static PropertyInfo t4881____Current_PropertyInfo = 
{
	&t4881_TI, "Current", &m33009_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4881_PIs[] =
{
	&t4881____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1384_0_0_0;
extern void* RuntimeInvoker_t1384 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33009_GM;
MethodInfo m33009_MI = 
{
	"get_Current", NULL, &t4881_TI, &t1384_0_0_0, RuntimeInvoker_t1384, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33009_GM};
static MethodInfo* t4881_MIs[] =
{
	&m33009_MI,
	NULL
};
static TypeInfo* t4881_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4881_0_0_0;
extern Il2CppType t4881_1_0_0;
struct t4881;
extern Il2CppGenericClass t4881_GC;
TypeInfo t4881_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4881_MIs, t4881_PIs, NULL, NULL, NULL, NULL, NULL, &t4881_TI, t4881_ITIs, NULL, &EmptyCustomAttributesCache, &t4881_TI, &t4881_0_0_0, &t4881_1_0_0, NULL, &t4881_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3391.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3391_TI;
#include "t3391MD.h"

extern TypeInfo t1384_TI;
extern MethodInfo m18831_MI;
extern MethodInfo m25268_MI;
struct t20;
 int32_t m25268 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18827_MI;
 void m18827 (t3391 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18828_MI;
 t29 * m18828 (t3391 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18831(__this, &m18831_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1384_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18829_MI;
 void m18829 (t3391 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18830_MI;
 bool m18830 (t3391 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18831 (t3391 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25268(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25268_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.PInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3391_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3391_TI, offsetof(t3391, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3391_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3391_TI, offsetof(t3391, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3391_FIs[] =
{
	&t3391_f0_FieldInfo,
	&t3391_f1_FieldInfo,
	NULL
};
static PropertyInfo t3391____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3391_TI, "System.Collections.IEnumerator.Current", &m18828_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3391____Current_PropertyInfo = 
{
	&t3391_TI, "Current", &m18831_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3391_PIs[] =
{
	&t3391____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3391____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3391_m18827_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18827_GM;
MethodInfo m18827_MI = 
{
	".ctor", (methodPointerType)&m18827, &t3391_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3391_m18827_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18827_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18828_GM;
MethodInfo m18828_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18828, &t3391_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18828_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18829_GM;
MethodInfo m18829_MI = 
{
	"Dispose", (methodPointerType)&m18829, &t3391_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18829_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18830_GM;
MethodInfo m18830_MI = 
{
	"MoveNext", (methodPointerType)&m18830, &t3391_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18830_GM};
extern Il2CppType t1384_0_0_0;
extern void* RuntimeInvoker_t1384 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18831_GM;
MethodInfo m18831_MI = 
{
	"get_Current", (methodPointerType)&m18831, &t3391_TI, &t1384_0_0_0, RuntimeInvoker_t1384, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18831_GM};
static MethodInfo* t3391_MIs[] =
{
	&m18827_MI,
	&m18828_MI,
	&m18829_MI,
	&m18830_MI,
	&m18831_MI,
	NULL
};
static MethodInfo* t3391_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18828_MI,
	&m18830_MI,
	&m18829_MI,
	&m18831_MI,
};
static TypeInfo* t3391_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4881_TI,
};
static Il2CppInterfaceOffsetPair t3391_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4881_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3391_0_0_0;
extern Il2CppType t3391_1_0_0;
extern Il2CppGenericClass t3391_GC;
TypeInfo t3391_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3391_MIs, t3391_PIs, t3391_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3391_TI, t3391_ITIs, t3391_VT, &EmptyCustomAttributesCache, &t3391_TI, &t3391_0_0_0, &t3391_1_0_0, t3391_IOs, &t3391_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3391)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6366_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.PInfo>
extern MethodInfo m33010_MI;
static PropertyInfo t6366____Count_PropertyInfo = 
{
	&t6366_TI, "Count", &m33010_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33011_MI;
static PropertyInfo t6366____IsReadOnly_PropertyInfo = 
{
	&t6366_TI, "IsReadOnly", &m33011_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6366_PIs[] =
{
	&t6366____Count_PropertyInfo,
	&t6366____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33010_GM;
MethodInfo m33010_MI = 
{
	"get_Count", NULL, &t6366_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33010_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33011_GM;
MethodInfo m33011_MI = 
{
	"get_IsReadOnly", NULL, &t6366_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33011_GM};
extern Il2CppType t1384_0_0_0;
extern Il2CppType t1384_0_0_0;
static ParameterInfo t6366_m33012_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1384_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33012_GM;
MethodInfo m33012_MI = 
{
	"Add", NULL, &t6366_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6366_m33012_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33012_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33013_GM;
MethodInfo m33013_MI = 
{
	"Clear", NULL, &t6366_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33013_GM};
extern Il2CppType t1384_0_0_0;
static ParameterInfo t6366_m33014_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1384_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33014_GM;
MethodInfo m33014_MI = 
{
	"Contains", NULL, &t6366_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6366_m33014_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33014_GM};
extern Il2CppType t3648_0_0_0;
extern Il2CppType t3648_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6366_m33015_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3648_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33015_GM;
MethodInfo m33015_MI = 
{
	"CopyTo", NULL, &t6366_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6366_m33015_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33015_GM};
extern Il2CppType t1384_0_0_0;
static ParameterInfo t6366_m33016_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1384_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33016_GM;
MethodInfo m33016_MI = 
{
	"Remove", NULL, &t6366_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6366_m33016_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33016_GM};
static MethodInfo* t6366_MIs[] =
{
	&m33010_MI,
	&m33011_MI,
	&m33012_MI,
	&m33013_MI,
	&m33014_MI,
	&m33015_MI,
	&m33016_MI,
	NULL
};
extern TypeInfo t6368_TI;
static TypeInfo* t6366_ITIs[] = 
{
	&t603_TI,
	&t6368_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6366_0_0_0;
extern Il2CppType t6366_1_0_0;
struct t6366;
extern Il2CppGenericClass t6366_GC;
TypeInfo t6366_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6366_MIs, t6366_PIs, NULL, NULL, NULL, NULL, NULL, &t6366_TI, t6366_ITIs, NULL, &EmptyCustomAttributesCache, &t6366_TI, &t6366_0_0_0, &t6366_1_0_0, NULL, &t6366_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.PInfo>
extern Il2CppType t4881_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33017_GM;
MethodInfo m33017_MI = 
{
	"GetEnumerator", NULL, &t6368_TI, &t4881_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33017_GM};
static MethodInfo* t6368_MIs[] =
{
	&m33017_MI,
	NULL
};
static TypeInfo* t6368_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6368_0_0_0;
extern Il2CppType t6368_1_0_0;
struct t6368;
extern Il2CppGenericClass t6368_GC;
TypeInfo t6368_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6368_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6368_TI, t6368_ITIs, NULL, &EmptyCustomAttributesCache, &t6368_TI, &t6368_0_0_0, &t6368_1_0_0, NULL, &t6368_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6367_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.PInfo>
extern MethodInfo m33018_MI;
extern MethodInfo m33019_MI;
static PropertyInfo t6367____Item_PropertyInfo = 
{
	&t6367_TI, "Item", &m33018_MI, &m33019_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6367_PIs[] =
{
	&t6367____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1384_0_0_0;
static ParameterInfo t6367_m33020_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1384_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33020_GM;
MethodInfo m33020_MI = 
{
	"IndexOf", NULL, &t6367_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6367_m33020_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33020_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1384_0_0_0;
static ParameterInfo t6367_m33021_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1384_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33021_GM;
MethodInfo m33021_MI = 
{
	"Insert", NULL, &t6367_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6367_m33021_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33021_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6367_m33022_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33022_GM;
MethodInfo m33022_MI = 
{
	"RemoveAt", NULL, &t6367_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6367_m33022_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33022_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6367_m33018_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1384_0_0_0;
extern void* RuntimeInvoker_t1384_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33018_GM;
MethodInfo m33018_MI = 
{
	"get_Item", NULL, &t6367_TI, &t1384_0_0_0, RuntimeInvoker_t1384_t44, t6367_m33018_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33018_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1384_0_0_0;
static ParameterInfo t6367_m33019_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1384_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33019_GM;
MethodInfo m33019_MI = 
{
	"set_Item", NULL, &t6367_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6367_m33019_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33019_GM};
static MethodInfo* t6367_MIs[] =
{
	&m33020_MI,
	&m33021_MI,
	&m33022_MI,
	&m33018_MI,
	&m33019_MI,
	NULL
};
static TypeInfo* t6367_ITIs[] = 
{
	&t603_TI,
	&t6366_TI,
	&t6368_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6367_0_0_0;
extern Il2CppType t6367_1_0_0;
struct t6367;
extern Il2CppGenericClass t6367_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6367_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6367_MIs, t6367_PIs, NULL, NULL, NULL, NULL, NULL, &t6367_TI, t6367_ITIs, NULL, &t1908__CustomAttributeCache, &t6367_TI, &t6367_0_0_0, &t6367_1_0_0, NULL, &t6367_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t3392.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3392_TI;
#include "t3392MD.h"

#include "t35.h"
#include "t67.h"


extern MethodInfo m18832_MI;
 void m18832_gshared (t3392 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m18833_MI;
 t29 * m18833_gshared (t3392 * __this, t29 * p0, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m18833((t3392 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 * __this, t29 * p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t29 * (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m18834_MI;
 t29 * m18834_gshared (t3392 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m18835_MI;
 t29 * m18835_gshared (t3392 * __this, t29 * p0, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t29 *)__result;
}
// Metadata Definition System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3392_m18832_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18832_GM;
MethodInfo m18832_MI = 
{
	".ctor", (methodPointerType)&m18832_gshared, &t3392_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3392_m18832_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18832_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t3392_m18833_ParameterInfos[] = 
{
	{"_this", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18833_GM;
MethodInfo m18833_MI = 
{
	"Invoke", (methodPointerType)&m18833_gshared, &t3392_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t3392_m18833_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18833_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3392_m18834_ParameterInfos[] = 
{
	{"_this", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18834_GM;
MethodInfo m18834_MI = 
{
	"BeginInvoke", (methodPointerType)&m18834_gshared, &t3392_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t3392_m18834_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m18834_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t3392_m18835_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18835_GM;
MethodInfo m18835_MI = 
{
	"EndInvoke", (methodPointerType)&m18835_gshared, &t3392_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t3392_m18835_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18835_GM};
static MethodInfo* t3392_MIs[] =
{
	&m18832_MI,
	&m18833_MI,
	&m18834_MI,
	&m18835_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
static MethodInfo* t3392_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m18833_MI,
	&m18834_MI,
	&m18835_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t3392_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3392_0_0_0;
extern Il2CppType t3392_1_0_0;
extern TypeInfo t195_TI;
struct t3392;
extern Il2CppGenericClass t3392_GC;
extern TypeInfo t1383_TI;
TypeInfo t3392_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Getter`2", "", t3392_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1383_TI, &t3392_TI, NULL, t3392_VT, &EmptyCustomAttributesCache, &t3392_TI, &t3392_0_0_0, &t3392_1_0_0, t3392_IOs, &t3392_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3392), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t3393.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3393_TI;
#include "t3393MD.h"



extern MethodInfo m18836_MI;
 void m18836_gshared (t3393 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m18837_MI;
 t29 * m18837_gshared (t3393 * __this, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m18837((t3393 *)__this->f9, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t29 * (*FunctionPointerType) (t29 *, t29 * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,(MethodInfo*)(__this->f3.f0));
	}
	typedef t29 * (*FunctionPointerType) (t29 * __this, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m18838_MI;
 t29 * m18838_gshared (t3393 * __this, t67 * p0, t29 * p1, MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p0, (Il2CppObject*)p1);
}
extern MethodInfo m18839_MI;
 t29 * m18839_gshared (t3393 * __this, t29 * p0, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return (t29 *)__result;
}
// Metadata Definition System.Reflection.MonoProperty/StaticGetter`1<System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t3393_m18836_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18836_GM;
MethodInfo m18836_MI = 
{
	".ctor", (methodPointerType)&m18836_gshared, &t3393_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t3393_m18836_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18836_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18837_GM;
MethodInfo m18837_MI = 
{
	"Invoke", (methodPointerType)&m18837_gshared, &t3393_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 454, 3, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18837_GM};
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t3393_m18838_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18838_GM;
MethodInfo m18838_MI = 
{
	"BeginInvoke", (methodPointerType)&m18838_gshared, &t3393_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29, t3393_m18838_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 2, false, true, 0, NULL, (methodPointerType)NULL, &m18838_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t3393_m18839_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18839_GM;
MethodInfo m18839_MI = 
{
	"EndInvoke", (methodPointerType)&m18839_gshared, &t3393_TI, &t29_0_0_0, RuntimeInvoker_t29_t29, t3393_m18839_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18839_GM};
static MethodInfo* t3393_MIs[] =
{
	&m18836_MI,
	&m18837_MI,
	&m18838_MI,
	&m18839_MI,
	NULL
};
static MethodInfo* t3393_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m18837_MI,
	&m18838_MI,
	&m18839_MI,
};
static Il2CppInterfaceOffsetPair t3393_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3393_0_0_0;
extern Il2CppType t3393_1_0_0;
struct t3393;
extern Il2CppGenericClass t3393_GC;
TypeInfo t3393_TI = 
{
	&g_mscorlib_dll_Image, NULL, "StaticGetter`1", "", t3393_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1383_TI, &t3393_TI, NULL, t3393_VT, &EmptyCustomAttributesCache, &t3393_TI, &t3393_0_0_0, &t3393_1_0_0, t3393_IOs, &t3393_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3393), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4883_TI;

#include "t1353.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.ParameterAttributes>
extern MethodInfo m33023_MI;
static PropertyInfo t4883____Current_PropertyInfo = 
{
	&t4883_TI, "Current", &m33023_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4883_PIs[] =
{
	&t4883____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1353_0_0_0;
extern void* RuntimeInvoker_t1353 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33023_GM;
MethodInfo m33023_MI = 
{
	"get_Current", NULL, &t4883_TI, &t1353_0_0_0, RuntimeInvoker_t1353, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33023_GM};
static MethodInfo* t4883_MIs[] =
{
	&m33023_MI,
	NULL
};
static TypeInfo* t4883_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4883_0_0_0;
extern Il2CppType t4883_1_0_0;
struct t4883;
extern Il2CppGenericClass t4883_GC;
TypeInfo t4883_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4883_MIs, t4883_PIs, NULL, NULL, NULL, NULL, NULL, &t4883_TI, t4883_ITIs, NULL, &EmptyCustomAttributesCache, &t4883_TI, &t4883_0_0_0, &t4883_1_0_0, NULL, &t4883_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3394.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3394_TI;
#include "t3394MD.h"

extern TypeInfo t1353_TI;
extern MethodInfo m18844_MI;
extern MethodInfo m25281_MI;
struct t20;
 int32_t m25281 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18840_MI;
 void m18840 (t3394 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18841_MI;
 t29 * m18841 (t3394 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18844(__this, &m18844_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1353_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18842_MI;
 void m18842 (t3394 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18843_MI;
 bool m18843 (t3394 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18844 (t3394 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25281(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25281_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.ParameterAttributes>
extern Il2CppType t20_0_0_1;
FieldInfo t3394_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3394_TI, offsetof(t3394, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3394_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3394_TI, offsetof(t3394, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3394_FIs[] =
{
	&t3394_f0_FieldInfo,
	&t3394_f1_FieldInfo,
	NULL
};
static PropertyInfo t3394____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3394_TI, "System.Collections.IEnumerator.Current", &m18841_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3394____Current_PropertyInfo = 
{
	&t3394_TI, "Current", &m18844_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3394_PIs[] =
{
	&t3394____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3394____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3394_m18840_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18840_GM;
MethodInfo m18840_MI = 
{
	".ctor", (methodPointerType)&m18840, &t3394_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3394_m18840_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18840_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18841_GM;
MethodInfo m18841_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18841, &t3394_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18841_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18842_GM;
MethodInfo m18842_MI = 
{
	"Dispose", (methodPointerType)&m18842, &t3394_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18842_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18843_GM;
MethodInfo m18843_MI = 
{
	"MoveNext", (methodPointerType)&m18843, &t3394_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18843_GM};
extern Il2CppType t1353_0_0_0;
extern void* RuntimeInvoker_t1353 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18844_GM;
MethodInfo m18844_MI = 
{
	"get_Current", (methodPointerType)&m18844, &t3394_TI, &t1353_0_0_0, RuntimeInvoker_t1353, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18844_GM};
static MethodInfo* t3394_MIs[] =
{
	&m18840_MI,
	&m18841_MI,
	&m18842_MI,
	&m18843_MI,
	&m18844_MI,
	NULL
};
static MethodInfo* t3394_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18841_MI,
	&m18843_MI,
	&m18842_MI,
	&m18844_MI,
};
static TypeInfo* t3394_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4883_TI,
};
static Il2CppInterfaceOffsetPair t3394_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4883_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3394_0_0_0;
extern Il2CppType t3394_1_0_0;
extern Il2CppGenericClass t3394_GC;
TypeInfo t3394_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3394_MIs, t3394_PIs, t3394_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3394_TI, t3394_ITIs, t3394_VT, &EmptyCustomAttributesCache, &t3394_TI, &t3394_0_0_0, &t3394_1_0_0, t3394_IOs, &t3394_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3394)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6369_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.ParameterAttributes>
extern MethodInfo m33024_MI;
static PropertyInfo t6369____Count_PropertyInfo = 
{
	&t6369_TI, "Count", &m33024_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33025_MI;
static PropertyInfo t6369____IsReadOnly_PropertyInfo = 
{
	&t6369_TI, "IsReadOnly", &m33025_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6369_PIs[] =
{
	&t6369____Count_PropertyInfo,
	&t6369____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33024_GM;
MethodInfo m33024_MI = 
{
	"get_Count", NULL, &t6369_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33024_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33025_GM;
MethodInfo m33025_MI = 
{
	"get_IsReadOnly", NULL, &t6369_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33025_GM};
extern Il2CppType t1353_0_0_0;
extern Il2CppType t1353_0_0_0;
static ParameterInfo t6369_m33026_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1353_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33026_GM;
MethodInfo m33026_MI = 
{
	"Add", NULL, &t6369_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6369_m33026_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33026_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33027_GM;
MethodInfo m33027_MI = 
{
	"Clear", NULL, &t6369_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33027_GM};
extern Il2CppType t1353_0_0_0;
static ParameterInfo t6369_m33028_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1353_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33028_GM;
MethodInfo m33028_MI = 
{
	"Contains", NULL, &t6369_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6369_m33028_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33028_GM};
extern Il2CppType t3649_0_0_0;
extern Il2CppType t3649_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6369_m33029_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3649_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33029_GM;
MethodInfo m33029_MI = 
{
	"CopyTo", NULL, &t6369_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6369_m33029_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33029_GM};
extern Il2CppType t1353_0_0_0;
static ParameterInfo t6369_m33030_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1353_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33030_GM;
MethodInfo m33030_MI = 
{
	"Remove", NULL, &t6369_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6369_m33030_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33030_GM};
static MethodInfo* t6369_MIs[] =
{
	&m33024_MI,
	&m33025_MI,
	&m33026_MI,
	&m33027_MI,
	&m33028_MI,
	&m33029_MI,
	&m33030_MI,
	NULL
};
extern TypeInfo t6371_TI;
static TypeInfo* t6369_ITIs[] = 
{
	&t603_TI,
	&t6371_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6369_0_0_0;
extern Il2CppType t6369_1_0_0;
struct t6369;
extern Il2CppGenericClass t6369_GC;
TypeInfo t6369_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6369_MIs, t6369_PIs, NULL, NULL, NULL, NULL, NULL, &t6369_TI, t6369_ITIs, NULL, &EmptyCustomAttributesCache, &t6369_TI, &t6369_0_0_0, &t6369_1_0_0, NULL, &t6369_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.ParameterAttributes>
extern Il2CppType t4883_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33031_GM;
MethodInfo m33031_MI = 
{
	"GetEnumerator", NULL, &t6371_TI, &t4883_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33031_GM};
static MethodInfo* t6371_MIs[] =
{
	&m33031_MI,
	NULL
};
static TypeInfo* t6371_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6371_0_0_0;
extern Il2CppType t6371_1_0_0;
struct t6371;
extern Il2CppGenericClass t6371_GC;
TypeInfo t6371_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6371_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6371_TI, t6371_ITIs, NULL, &EmptyCustomAttributesCache, &t6371_TI, &t6371_0_0_0, &t6371_1_0_0, NULL, &t6371_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6370_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.ParameterAttributes>
extern MethodInfo m33032_MI;
extern MethodInfo m33033_MI;
static PropertyInfo t6370____Item_PropertyInfo = 
{
	&t6370_TI, "Item", &m33032_MI, &m33033_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6370_PIs[] =
{
	&t6370____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1353_0_0_0;
static ParameterInfo t6370_m33034_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1353_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33034_GM;
MethodInfo m33034_MI = 
{
	"IndexOf", NULL, &t6370_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6370_m33034_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33034_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1353_0_0_0;
static ParameterInfo t6370_m33035_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1353_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33035_GM;
MethodInfo m33035_MI = 
{
	"Insert", NULL, &t6370_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6370_m33035_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33035_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6370_m33036_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33036_GM;
MethodInfo m33036_MI = 
{
	"RemoveAt", NULL, &t6370_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6370_m33036_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33036_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6370_m33032_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1353_0_0_0;
extern void* RuntimeInvoker_t1353_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33032_GM;
MethodInfo m33032_MI = 
{
	"get_Item", NULL, &t6370_TI, &t1353_0_0_0, RuntimeInvoker_t1353_t44, t6370_m33032_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33032_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1353_0_0_0;
static ParameterInfo t6370_m33033_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1353_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33033_GM;
MethodInfo m33033_MI = 
{
	"set_Item", NULL, &t6370_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6370_m33033_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33033_GM};
static MethodInfo* t6370_MIs[] =
{
	&m33034_MI,
	&m33035_MI,
	&m33036_MI,
	&m33032_MI,
	&m33033_MI,
	NULL
};
static TypeInfo* t6370_ITIs[] = 
{
	&t603_TI,
	&t6369_TI,
	&t6371_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6370_0_0_0;
extern Il2CppType t6370_1_0_0;
struct t6370;
extern Il2CppGenericClass t6370_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6370_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6370_MIs, t6370_PIs, NULL, NULL, NULL, NULL, NULL, &t6370_TI, t6370_ITIs, NULL, &t1908__CustomAttributeCache, &t6370_TI, &t6370_0_0_0, &t6370_1_0_0, NULL, &t6370_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4885_TI;

#include "t1363.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.ProcessorArchitecture>
extern MethodInfo m33037_MI;
static PropertyInfo t4885____Current_PropertyInfo = 
{
	&t4885_TI, "Current", &m33037_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4885_PIs[] =
{
	&t4885____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1363_0_0_0;
extern void* RuntimeInvoker_t1363 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33037_GM;
MethodInfo m33037_MI = 
{
	"get_Current", NULL, &t4885_TI, &t1363_0_0_0, RuntimeInvoker_t1363, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33037_GM};
static MethodInfo* t4885_MIs[] =
{
	&m33037_MI,
	NULL
};
static TypeInfo* t4885_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4885_0_0_0;
extern Il2CppType t4885_1_0_0;
struct t4885;
extern Il2CppGenericClass t4885_GC;
TypeInfo t4885_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4885_MIs, t4885_PIs, NULL, NULL, NULL, NULL, NULL, &t4885_TI, t4885_ITIs, NULL, &EmptyCustomAttributesCache, &t4885_TI, &t4885_0_0_0, &t4885_1_0_0, NULL, &t4885_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3395.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3395_TI;
#include "t3395MD.h"

extern TypeInfo t1363_TI;
extern MethodInfo m18849_MI;
extern MethodInfo m25292_MI;
struct t20;
 int32_t m25292 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18845_MI;
 void m18845 (t3395 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18846_MI;
 t29 * m18846 (t3395 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18849(__this, &m18849_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1363_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18847_MI;
 void m18847 (t3395 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18848_MI;
 bool m18848 (t3395 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18849 (t3395 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25292(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25292_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.ProcessorArchitecture>
extern Il2CppType t20_0_0_1;
FieldInfo t3395_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3395_TI, offsetof(t3395, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3395_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3395_TI, offsetof(t3395, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3395_FIs[] =
{
	&t3395_f0_FieldInfo,
	&t3395_f1_FieldInfo,
	NULL
};
static PropertyInfo t3395____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3395_TI, "System.Collections.IEnumerator.Current", &m18846_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3395____Current_PropertyInfo = 
{
	&t3395_TI, "Current", &m18849_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3395_PIs[] =
{
	&t3395____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3395____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3395_m18845_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18845_GM;
MethodInfo m18845_MI = 
{
	".ctor", (methodPointerType)&m18845, &t3395_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3395_m18845_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18845_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18846_GM;
MethodInfo m18846_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18846, &t3395_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18846_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18847_GM;
MethodInfo m18847_MI = 
{
	"Dispose", (methodPointerType)&m18847, &t3395_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18847_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18848_GM;
MethodInfo m18848_MI = 
{
	"MoveNext", (methodPointerType)&m18848, &t3395_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18848_GM};
extern Il2CppType t1363_0_0_0;
extern void* RuntimeInvoker_t1363 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18849_GM;
MethodInfo m18849_MI = 
{
	"get_Current", (methodPointerType)&m18849, &t3395_TI, &t1363_0_0_0, RuntimeInvoker_t1363, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18849_GM};
static MethodInfo* t3395_MIs[] =
{
	&m18845_MI,
	&m18846_MI,
	&m18847_MI,
	&m18848_MI,
	&m18849_MI,
	NULL
};
static MethodInfo* t3395_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18846_MI,
	&m18848_MI,
	&m18847_MI,
	&m18849_MI,
};
static TypeInfo* t3395_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4885_TI,
};
static Il2CppInterfaceOffsetPair t3395_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4885_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3395_0_0_0;
extern Il2CppType t3395_1_0_0;
extern Il2CppGenericClass t3395_GC;
TypeInfo t3395_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3395_MIs, t3395_PIs, t3395_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3395_TI, t3395_ITIs, t3395_VT, &EmptyCustomAttributesCache, &t3395_TI, &t3395_0_0_0, &t3395_1_0_0, t3395_IOs, &t3395_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3395)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6372_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.ProcessorArchitecture>
extern MethodInfo m33038_MI;
static PropertyInfo t6372____Count_PropertyInfo = 
{
	&t6372_TI, "Count", &m33038_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33039_MI;
static PropertyInfo t6372____IsReadOnly_PropertyInfo = 
{
	&t6372_TI, "IsReadOnly", &m33039_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6372_PIs[] =
{
	&t6372____Count_PropertyInfo,
	&t6372____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33038_GM;
MethodInfo m33038_MI = 
{
	"get_Count", NULL, &t6372_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33038_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33039_GM;
MethodInfo m33039_MI = 
{
	"get_IsReadOnly", NULL, &t6372_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33039_GM};
extern Il2CppType t1363_0_0_0;
extern Il2CppType t1363_0_0_0;
static ParameterInfo t6372_m33040_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1363_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33040_GM;
MethodInfo m33040_MI = 
{
	"Add", NULL, &t6372_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6372_m33040_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33040_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33041_GM;
MethodInfo m33041_MI = 
{
	"Clear", NULL, &t6372_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33041_GM};
extern Il2CppType t1363_0_0_0;
static ParameterInfo t6372_m33042_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1363_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33042_GM;
MethodInfo m33042_MI = 
{
	"Contains", NULL, &t6372_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6372_m33042_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33042_GM};
extern Il2CppType t3650_0_0_0;
extern Il2CppType t3650_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6372_m33043_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3650_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33043_GM;
MethodInfo m33043_MI = 
{
	"CopyTo", NULL, &t6372_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6372_m33043_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33043_GM};
extern Il2CppType t1363_0_0_0;
static ParameterInfo t6372_m33044_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1363_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33044_GM;
MethodInfo m33044_MI = 
{
	"Remove", NULL, &t6372_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6372_m33044_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33044_GM};
static MethodInfo* t6372_MIs[] =
{
	&m33038_MI,
	&m33039_MI,
	&m33040_MI,
	&m33041_MI,
	&m33042_MI,
	&m33043_MI,
	&m33044_MI,
	NULL
};
extern TypeInfo t6374_TI;
static TypeInfo* t6372_ITIs[] = 
{
	&t603_TI,
	&t6374_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6372_0_0_0;
extern Il2CppType t6372_1_0_0;
struct t6372;
extern Il2CppGenericClass t6372_GC;
TypeInfo t6372_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6372_MIs, t6372_PIs, NULL, NULL, NULL, NULL, NULL, &t6372_TI, t6372_ITIs, NULL, &EmptyCustomAttributesCache, &t6372_TI, &t6372_0_0_0, &t6372_1_0_0, NULL, &t6372_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.ProcessorArchitecture>
extern Il2CppType t4885_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33045_GM;
MethodInfo m33045_MI = 
{
	"GetEnumerator", NULL, &t6374_TI, &t4885_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33045_GM};
static MethodInfo* t6374_MIs[] =
{
	&m33045_MI,
	NULL
};
static TypeInfo* t6374_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6374_0_0_0;
extern Il2CppType t6374_1_0_0;
struct t6374;
extern Il2CppGenericClass t6374_GC;
TypeInfo t6374_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6374_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6374_TI, t6374_ITIs, NULL, &EmptyCustomAttributesCache, &t6374_TI, &t6374_0_0_0, &t6374_1_0_0, NULL, &t6374_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6373_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.ProcessorArchitecture>
extern MethodInfo m33046_MI;
extern MethodInfo m33047_MI;
static PropertyInfo t6373____Item_PropertyInfo = 
{
	&t6373_TI, "Item", &m33046_MI, &m33047_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6373_PIs[] =
{
	&t6373____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1363_0_0_0;
static ParameterInfo t6373_m33048_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1363_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33048_GM;
MethodInfo m33048_MI = 
{
	"IndexOf", NULL, &t6373_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6373_m33048_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33048_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1363_0_0_0;
static ParameterInfo t6373_m33049_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1363_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33049_GM;
MethodInfo m33049_MI = 
{
	"Insert", NULL, &t6373_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6373_m33049_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33049_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6373_m33050_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33050_GM;
MethodInfo m33050_MI = 
{
	"RemoveAt", NULL, &t6373_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6373_m33050_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33050_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6373_m33046_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1363_0_0_0;
extern void* RuntimeInvoker_t1363_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33046_GM;
MethodInfo m33046_MI = 
{
	"get_Item", NULL, &t6373_TI, &t1363_0_0_0, RuntimeInvoker_t1363_t44, t6373_m33046_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33046_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1363_0_0_0;
static ParameterInfo t6373_m33047_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1363_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33047_GM;
MethodInfo m33047_MI = 
{
	"set_Item", NULL, &t6373_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6373_m33047_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33047_GM};
static MethodInfo* t6373_MIs[] =
{
	&m33048_MI,
	&m33049_MI,
	&m33050_MI,
	&m33046_MI,
	&m33047_MI,
	NULL
};
static TypeInfo* t6373_ITIs[] = 
{
	&t603_TI,
	&t6372_TI,
	&t6374_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6373_0_0_0;
extern Il2CppType t6373_1_0_0;
struct t6373;
extern Il2CppGenericClass t6373_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6373_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6373_MIs, t6373_PIs, NULL, NULL, NULL, NULL, NULL, &t6373_TI, t6373_ITIs, NULL, &t1908__CustomAttributeCache, &t6373_TI, &t6373_0_0_0, &t6373_1_0_0, NULL, &t6373_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4887_TI;

#include "t1382.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyAttributes>
extern MethodInfo m33051_MI;
static PropertyInfo t4887____Current_PropertyInfo = 
{
	&t4887_TI, "Current", &m33051_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4887_PIs[] =
{
	&t4887____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1382_0_0_0;
extern void* RuntimeInvoker_t1382 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33051_GM;
MethodInfo m33051_MI = 
{
	"get_Current", NULL, &t4887_TI, &t1382_0_0_0, RuntimeInvoker_t1382, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33051_GM};
static MethodInfo* t4887_MIs[] =
{
	&m33051_MI,
	NULL
};
static TypeInfo* t4887_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4887_0_0_0;
extern Il2CppType t4887_1_0_0;
struct t4887;
extern Il2CppGenericClass t4887_GC;
TypeInfo t4887_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4887_MIs, t4887_PIs, NULL, NULL, NULL, NULL, NULL, &t4887_TI, t4887_ITIs, NULL, &EmptyCustomAttributesCache, &t4887_TI, &t4887_0_0_0, &t4887_1_0_0, NULL, &t4887_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3396.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3396_TI;
#include "t3396MD.h"

extern TypeInfo t1382_TI;
extern MethodInfo m18854_MI;
extern MethodInfo m25303_MI;
struct t20;
 int32_t m25303 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18850_MI;
 void m18850 (t3396 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18851_MI;
 t29 * m18851 (t3396 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18854(__this, &m18854_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1382_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18852_MI;
 void m18852 (t3396 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18853_MI;
 bool m18853 (t3396 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18854 (t3396 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25303(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25303_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>
extern Il2CppType t20_0_0_1;
FieldInfo t3396_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3396_TI, offsetof(t3396, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3396_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3396_TI, offsetof(t3396, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3396_FIs[] =
{
	&t3396_f0_FieldInfo,
	&t3396_f1_FieldInfo,
	NULL
};
static PropertyInfo t3396____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3396_TI, "System.Collections.IEnumerator.Current", &m18851_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3396____Current_PropertyInfo = 
{
	&t3396_TI, "Current", &m18854_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3396_PIs[] =
{
	&t3396____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3396____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3396_m18850_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18850_GM;
MethodInfo m18850_MI = 
{
	".ctor", (methodPointerType)&m18850, &t3396_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3396_m18850_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18850_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18851_GM;
MethodInfo m18851_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18851, &t3396_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18851_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18852_GM;
MethodInfo m18852_MI = 
{
	"Dispose", (methodPointerType)&m18852, &t3396_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18852_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18853_GM;
MethodInfo m18853_MI = 
{
	"MoveNext", (methodPointerType)&m18853, &t3396_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18853_GM};
extern Il2CppType t1382_0_0_0;
extern void* RuntimeInvoker_t1382 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18854_GM;
MethodInfo m18854_MI = 
{
	"get_Current", (methodPointerType)&m18854, &t3396_TI, &t1382_0_0_0, RuntimeInvoker_t1382, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18854_GM};
static MethodInfo* t3396_MIs[] =
{
	&m18850_MI,
	&m18851_MI,
	&m18852_MI,
	&m18853_MI,
	&m18854_MI,
	NULL
};
static MethodInfo* t3396_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18851_MI,
	&m18853_MI,
	&m18852_MI,
	&m18854_MI,
};
static TypeInfo* t3396_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4887_TI,
};
static Il2CppInterfaceOffsetPair t3396_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4887_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3396_0_0_0;
extern Il2CppType t3396_1_0_0;
extern Il2CppGenericClass t3396_GC;
TypeInfo t3396_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3396_MIs, t3396_PIs, t3396_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3396_TI, t3396_ITIs, t3396_VT, &EmptyCustomAttributesCache, &t3396_TI, &t3396_0_0_0, &t3396_1_0_0, t3396_IOs, &t3396_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3396)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6375_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.PropertyAttributes>
extern MethodInfo m33052_MI;
static PropertyInfo t6375____Count_PropertyInfo = 
{
	&t6375_TI, "Count", &m33052_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33053_MI;
static PropertyInfo t6375____IsReadOnly_PropertyInfo = 
{
	&t6375_TI, "IsReadOnly", &m33053_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6375_PIs[] =
{
	&t6375____Count_PropertyInfo,
	&t6375____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33052_GM;
MethodInfo m33052_MI = 
{
	"get_Count", NULL, &t6375_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33052_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33053_GM;
MethodInfo m33053_MI = 
{
	"get_IsReadOnly", NULL, &t6375_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33053_GM};
extern Il2CppType t1382_0_0_0;
extern Il2CppType t1382_0_0_0;
static ParameterInfo t6375_m33054_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1382_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33054_GM;
MethodInfo m33054_MI = 
{
	"Add", NULL, &t6375_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6375_m33054_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33054_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33055_GM;
MethodInfo m33055_MI = 
{
	"Clear", NULL, &t6375_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33055_GM};
extern Il2CppType t1382_0_0_0;
static ParameterInfo t6375_m33056_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1382_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33056_GM;
MethodInfo m33056_MI = 
{
	"Contains", NULL, &t6375_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6375_m33056_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33056_GM};
extern Il2CppType t3651_0_0_0;
extern Il2CppType t3651_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6375_m33057_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3651_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33057_GM;
MethodInfo m33057_MI = 
{
	"CopyTo", NULL, &t6375_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6375_m33057_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33057_GM};
extern Il2CppType t1382_0_0_0;
static ParameterInfo t6375_m33058_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1382_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33058_GM;
MethodInfo m33058_MI = 
{
	"Remove", NULL, &t6375_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6375_m33058_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33058_GM};
static MethodInfo* t6375_MIs[] =
{
	&m33052_MI,
	&m33053_MI,
	&m33054_MI,
	&m33055_MI,
	&m33056_MI,
	&m33057_MI,
	&m33058_MI,
	NULL
};
extern TypeInfo t6377_TI;
static TypeInfo* t6375_ITIs[] = 
{
	&t603_TI,
	&t6377_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6375_0_0_0;
extern Il2CppType t6375_1_0_0;
struct t6375;
extern Il2CppGenericClass t6375_GC;
TypeInfo t6375_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6375_MIs, t6375_PIs, NULL, NULL, NULL, NULL, NULL, &t6375_TI, t6375_ITIs, NULL, &EmptyCustomAttributesCache, &t6375_TI, &t6375_0_0_0, &t6375_1_0_0, NULL, &t6375_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyAttributes>
extern Il2CppType t4887_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33059_GM;
MethodInfo m33059_MI = 
{
	"GetEnumerator", NULL, &t6377_TI, &t4887_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33059_GM};
static MethodInfo* t6377_MIs[] =
{
	&m33059_MI,
	NULL
};
static TypeInfo* t6377_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6377_0_0_0;
extern Il2CppType t6377_1_0_0;
struct t6377;
extern Il2CppGenericClass t6377_GC;
TypeInfo t6377_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6377_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6377_TI, t6377_ITIs, NULL, &EmptyCustomAttributesCache, &t6377_TI, &t6377_0_0_0, &t6377_1_0_0, NULL, &t6377_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6376_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.PropertyAttributes>
extern MethodInfo m33060_MI;
extern MethodInfo m33061_MI;
static PropertyInfo t6376____Item_PropertyInfo = 
{
	&t6376_TI, "Item", &m33060_MI, &m33061_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6376_PIs[] =
{
	&t6376____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1382_0_0_0;
static ParameterInfo t6376_m33062_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1382_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33062_GM;
MethodInfo m33062_MI = 
{
	"IndexOf", NULL, &t6376_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6376_m33062_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33062_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1382_0_0_0;
static ParameterInfo t6376_m33063_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1382_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33063_GM;
MethodInfo m33063_MI = 
{
	"Insert", NULL, &t6376_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6376_m33063_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33063_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6376_m33064_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33064_GM;
MethodInfo m33064_MI = 
{
	"RemoveAt", NULL, &t6376_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6376_m33064_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33064_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6376_m33060_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1382_0_0_0;
extern void* RuntimeInvoker_t1382_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33060_GM;
MethodInfo m33060_MI = 
{
	"get_Item", NULL, &t6376_TI, &t1382_0_0_0, RuntimeInvoker_t1382_t44, t6376_m33060_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33060_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1382_0_0_0;
static ParameterInfo t6376_m33061_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1382_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33061_GM;
MethodInfo m33061_MI = 
{
	"set_Item", NULL, &t6376_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6376_m33061_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33061_GM};
static MethodInfo* t6376_MIs[] =
{
	&m33062_MI,
	&m33063_MI,
	&m33064_MI,
	&m33060_MI,
	&m33061_MI,
	NULL
};
static TypeInfo* t6376_ITIs[] = 
{
	&t603_TI,
	&t6375_TI,
	&t6377_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6376_0_0_0;
extern Il2CppType t6376_1_0_0;
struct t6376;
extern Il2CppGenericClass t6376_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6376_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6376_MIs, t6376_PIs, NULL, NULL, NULL, NULL, NULL, &t6376_TI, t6376_ITIs, NULL, &t1908__CustomAttributeCache, &t6376_TI, &t6376_0_0_0, &t6376_1_0_0, NULL, &t6376_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4889_TI;

#include "t1148.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.TypeAttributes>
extern MethodInfo m33065_MI;
static PropertyInfo t4889____Current_PropertyInfo = 
{
	&t4889_TI, "Current", &m33065_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4889_PIs[] =
{
	&t4889____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1148_0_0_0;
extern void* RuntimeInvoker_t1148 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33065_GM;
MethodInfo m33065_MI = 
{
	"get_Current", NULL, &t4889_TI, &t1148_0_0_0, RuntimeInvoker_t1148, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33065_GM};
static MethodInfo* t4889_MIs[] =
{
	&m33065_MI,
	NULL
};
static TypeInfo* t4889_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4889_0_0_0;
extern Il2CppType t4889_1_0_0;
struct t4889;
extern Il2CppGenericClass t4889_GC;
TypeInfo t4889_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4889_MIs, t4889_PIs, NULL, NULL, NULL, NULL, NULL, &t4889_TI, t4889_ITIs, NULL, &EmptyCustomAttributesCache, &t4889_TI, &t4889_0_0_0, &t4889_1_0_0, NULL, &t4889_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3397.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3397_TI;
#include "t3397MD.h"

extern TypeInfo t1148_TI;
extern MethodInfo m18859_MI;
extern MethodInfo m25314_MI;
struct t20;
 int32_t m25314 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18855_MI;
 void m18855 (t3397 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18856_MI;
 t29 * m18856 (t3397 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18859(__this, &m18859_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1148_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18857_MI;
 void m18857 (t3397 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18858_MI;
 bool m18858 (t3397 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18859 (t3397 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25314(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25314_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>
extern Il2CppType t20_0_0_1;
FieldInfo t3397_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3397_TI, offsetof(t3397, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3397_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3397_TI, offsetof(t3397, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3397_FIs[] =
{
	&t3397_f0_FieldInfo,
	&t3397_f1_FieldInfo,
	NULL
};
static PropertyInfo t3397____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3397_TI, "System.Collections.IEnumerator.Current", &m18856_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3397____Current_PropertyInfo = 
{
	&t3397_TI, "Current", &m18859_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3397_PIs[] =
{
	&t3397____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3397____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3397_m18855_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18855_GM;
MethodInfo m18855_MI = 
{
	".ctor", (methodPointerType)&m18855, &t3397_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3397_m18855_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18855_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18856_GM;
MethodInfo m18856_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18856, &t3397_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18856_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18857_GM;
MethodInfo m18857_MI = 
{
	"Dispose", (methodPointerType)&m18857, &t3397_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18857_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18858_GM;
MethodInfo m18858_MI = 
{
	"MoveNext", (methodPointerType)&m18858, &t3397_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18858_GM};
extern Il2CppType t1148_0_0_0;
extern void* RuntimeInvoker_t1148 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18859_GM;
MethodInfo m18859_MI = 
{
	"get_Current", (methodPointerType)&m18859, &t3397_TI, &t1148_0_0_0, RuntimeInvoker_t1148, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18859_GM};
static MethodInfo* t3397_MIs[] =
{
	&m18855_MI,
	&m18856_MI,
	&m18857_MI,
	&m18858_MI,
	&m18859_MI,
	NULL
};
static MethodInfo* t3397_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18856_MI,
	&m18858_MI,
	&m18857_MI,
	&m18859_MI,
};
static TypeInfo* t3397_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4889_TI,
};
static Il2CppInterfaceOffsetPair t3397_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4889_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3397_0_0_0;
extern Il2CppType t3397_1_0_0;
extern Il2CppGenericClass t3397_GC;
TypeInfo t3397_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3397_MIs, t3397_PIs, t3397_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3397_TI, t3397_ITIs, t3397_VT, &EmptyCustomAttributesCache, &t3397_TI, &t3397_0_0_0, &t3397_1_0_0, t3397_IOs, &t3397_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3397)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6378_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.TypeAttributes>
extern MethodInfo m33066_MI;
static PropertyInfo t6378____Count_PropertyInfo = 
{
	&t6378_TI, "Count", &m33066_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33067_MI;
static PropertyInfo t6378____IsReadOnly_PropertyInfo = 
{
	&t6378_TI, "IsReadOnly", &m33067_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6378_PIs[] =
{
	&t6378____Count_PropertyInfo,
	&t6378____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33066_GM;
MethodInfo m33066_MI = 
{
	"get_Count", NULL, &t6378_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33066_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33067_GM;
MethodInfo m33067_MI = 
{
	"get_IsReadOnly", NULL, &t6378_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33067_GM};
extern Il2CppType t1148_0_0_0;
extern Il2CppType t1148_0_0_0;
static ParameterInfo t6378_m33068_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33068_GM;
MethodInfo m33068_MI = 
{
	"Add", NULL, &t6378_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6378_m33068_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33068_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33069_GM;
MethodInfo m33069_MI = 
{
	"Clear", NULL, &t6378_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33069_GM};
extern Il2CppType t1148_0_0_0;
static ParameterInfo t6378_m33070_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1148_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33070_GM;
MethodInfo m33070_MI = 
{
	"Contains", NULL, &t6378_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6378_m33070_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33070_GM};
extern Il2CppType t3652_0_0_0;
extern Il2CppType t3652_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6378_m33071_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3652_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33071_GM;
MethodInfo m33071_MI = 
{
	"CopyTo", NULL, &t6378_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6378_m33071_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33071_GM};
extern Il2CppType t1148_0_0_0;
static ParameterInfo t6378_m33072_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1148_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33072_GM;
MethodInfo m33072_MI = 
{
	"Remove", NULL, &t6378_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6378_m33072_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33072_GM};
static MethodInfo* t6378_MIs[] =
{
	&m33066_MI,
	&m33067_MI,
	&m33068_MI,
	&m33069_MI,
	&m33070_MI,
	&m33071_MI,
	&m33072_MI,
	NULL
};
extern TypeInfo t6380_TI;
static TypeInfo* t6378_ITIs[] = 
{
	&t603_TI,
	&t6380_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6378_0_0_0;
extern Il2CppType t6378_1_0_0;
struct t6378;
extern Il2CppGenericClass t6378_GC;
TypeInfo t6378_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6378_MIs, t6378_PIs, NULL, NULL, NULL, NULL, NULL, &t6378_TI, t6378_ITIs, NULL, &EmptyCustomAttributesCache, &t6378_TI, &t6378_0_0_0, &t6378_1_0_0, NULL, &t6378_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.TypeAttributes>
extern Il2CppType t4889_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33073_GM;
MethodInfo m33073_MI = 
{
	"GetEnumerator", NULL, &t6380_TI, &t4889_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33073_GM};
static MethodInfo* t6380_MIs[] =
{
	&m33073_MI,
	NULL
};
static TypeInfo* t6380_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6380_0_0_0;
extern Il2CppType t6380_1_0_0;
struct t6380;
extern Il2CppGenericClass t6380_GC;
TypeInfo t6380_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6380_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6380_TI, t6380_ITIs, NULL, &EmptyCustomAttributesCache, &t6380_TI, &t6380_0_0_0, &t6380_1_0_0, NULL, &t6380_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6379_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.TypeAttributes>
extern MethodInfo m33074_MI;
extern MethodInfo m33075_MI;
static PropertyInfo t6379____Item_PropertyInfo = 
{
	&t6379_TI, "Item", &m33074_MI, &m33075_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6379_PIs[] =
{
	&t6379____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1148_0_0_0;
static ParameterInfo t6379_m33076_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1148_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33076_GM;
MethodInfo m33076_MI = 
{
	"IndexOf", NULL, &t6379_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6379_m33076_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33076_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1148_0_0_0;
static ParameterInfo t6379_m33077_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33077_GM;
MethodInfo m33077_MI = 
{
	"Insert", NULL, &t6379_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6379_m33077_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33077_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6379_m33078_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33078_GM;
MethodInfo m33078_MI = 
{
	"RemoveAt", NULL, &t6379_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6379_m33078_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33078_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6379_m33074_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1148_0_0_0;
extern void* RuntimeInvoker_t1148_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33074_GM;
MethodInfo m33074_MI = 
{
	"get_Item", NULL, &t6379_TI, &t1148_0_0_0, RuntimeInvoker_t1148_t44, t6379_m33074_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33074_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1148_0_0_0;
static ParameterInfo t6379_m33075_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33075_GM;
MethodInfo m33075_MI = 
{
	"set_Item", NULL, &t6379_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6379_m33075_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33075_GM};
static MethodInfo* t6379_MIs[] =
{
	&m33076_MI,
	&m33077_MI,
	&m33078_MI,
	&m33074_MI,
	&m33075_MI,
	NULL
};
static TypeInfo* t6379_ITIs[] = 
{
	&t603_TI,
	&t6378_TI,
	&t6380_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6379_0_0_0;
extern Il2CppType t6379_1_0_0;
struct t6379;
extern Il2CppGenericClass t6379_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6379_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6379_MIs, t6379_PIs, NULL, NULL, NULL, NULL, NULL, &t6379_TI, t6379_ITIs, NULL, &t1908__CustomAttributeCache, &t6379_TI, &t6379_0_0_0, &t6379_1_0_0, NULL, &t6379_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4891_TI;

#include "t706.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>
extern MethodInfo m33079_MI;
static PropertyInfo t4891____Current_PropertyInfo = 
{
	&t4891_TI, "Current", &m33079_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4891_PIs[] =
{
	&t4891____Current_PropertyInfo,
	NULL
};
extern Il2CppType t706_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33079_GM;
MethodInfo m33079_MI = 
{
	"get_Current", NULL, &t4891_TI, &t706_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33079_GM};
static MethodInfo* t4891_MIs[] =
{
	&m33079_MI,
	NULL
};
static TypeInfo* t4891_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4891_0_0_0;
extern Il2CppType t4891_1_0_0;
struct t4891;
extern Il2CppGenericClass t4891_GC;
TypeInfo t4891_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4891_MIs, t4891_PIs, NULL, NULL, NULL, NULL, NULL, &t4891_TI, t4891_ITIs, NULL, &EmptyCustomAttributesCache, &t4891_TI, &t4891_0_0_0, &t4891_1_0_0, NULL, &t4891_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3398.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3398_TI;
#include "t3398MD.h"

extern TypeInfo t706_TI;
extern MethodInfo m18864_MI;
extern MethodInfo m25325_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m25325(__this, p0, method) (t706 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Resources.NeutralResourcesLanguageAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3398_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3398_TI, offsetof(t3398, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3398_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3398_TI, offsetof(t3398, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3398_FIs[] =
{
	&t3398_f0_FieldInfo,
	&t3398_f1_FieldInfo,
	NULL
};
extern MethodInfo m18861_MI;
static PropertyInfo t3398____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3398_TI, "System.Collections.IEnumerator.Current", &m18861_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3398____Current_PropertyInfo = 
{
	&t3398_TI, "Current", &m18864_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3398_PIs[] =
{
	&t3398____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3398____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3398_m18860_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18860_GM;
MethodInfo m18860_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3398_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3398_m18860_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18860_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18861_GM;
MethodInfo m18861_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3398_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18861_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18862_GM;
MethodInfo m18862_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3398_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18862_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18863_GM;
MethodInfo m18863_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3398_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18863_GM};
extern Il2CppType t706_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18864_GM;
MethodInfo m18864_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3398_TI, &t706_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18864_GM};
static MethodInfo* t3398_MIs[] =
{
	&m18860_MI,
	&m18861_MI,
	&m18862_MI,
	&m18863_MI,
	&m18864_MI,
	NULL
};
extern MethodInfo m18863_MI;
extern MethodInfo m18862_MI;
static MethodInfo* t3398_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18861_MI,
	&m18863_MI,
	&m18862_MI,
	&m18864_MI,
};
static TypeInfo* t3398_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4891_TI,
};
static Il2CppInterfaceOffsetPair t3398_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4891_TI, 7},
};
extern TypeInfo t706_TI;
static Il2CppRGCTXData t3398_RGCTXData[3] = 
{
	&m18864_MI/* Method Usage */,
	&t706_TI/* Class Usage */,
	&m25325_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3398_0_0_0;
extern Il2CppType t3398_1_0_0;
extern Il2CppGenericClass t3398_GC;
TypeInfo t3398_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3398_MIs, t3398_PIs, t3398_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3398_TI, t3398_ITIs, t3398_VT, &EmptyCustomAttributesCache, &t3398_TI, &t3398_0_0_0, &t3398_1_0_0, t3398_IOs, &t3398_GC, NULL, NULL, NULL, t3398_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3398)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6381_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Resources.NeutralResourcesLanguageAttribute>
extern MethodInfo m33080_MI;
static PropertyInfo t6381____Count_PropertyInfo = 
{
	&t6381_TI, "Count", &m33080_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33081_MI;
static PropertyInfo t6381____IsReadOnly_PropertyInfo = 
{
	&t6381_TI, "IsReadOnly", &m33081_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6381_PIs[] =
{
	&t6381____Count_PropertyInfo,
	&t6381____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33080_GM;
MethodInfo m33080_MI = 
{
	"get_Count", NULL, &t6381_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33080_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33081_GM;
MethodInfo m33081_MI = 
{
	"get_IsReadOnly", NULL, &t6381_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33081_GM};
extern Il2CppType t706_0_0_0;
extern Il2CppType t706_0_0_0;
static ParameterInfo t6381_m33082_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t706_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33082_GM;
MethodInfo m33082_MI = 
{
	"Add", NULL, &t6381_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6381_m33082_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33082_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33083_GM;
MethodInfo m33083_MI = 
{
	"Clear", NULL, &t6381_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33083_GM};
extern Il2CppType t706_0_0_0;
static ParameterInfo t6381_m33084_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t706_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33084_GM;
MethodInfo m33084_MI = 
{
	"Contains", NULL, &t6381_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6381_m33084_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33084_GM};
extern Il2CppType t3653_0_0_0;
extern Il2CppType t3653_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6381_m33085_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3653_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33085_GM;
MethodInfo m33085_MI = 
{
	"CopyTo", NULL, &t6381_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6381_m33085_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33085_GM};
extern Il2CppType t706_0_0_0;
static ParameterInfo t6381_m33086_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t706_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33086_GM;
MethodInfo m33086_MI = 
{
	"Remove", NULL, &t6381_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6381_m33086_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33086_GM};
static MethodInfo* t6381_MIs[] =
{
	&m33080_MI,
	&m33081_MI,
	&m33082_MI,
	&m33083_MI,
	&m33084_MI,
	&m33085_MI,
	&m33086_MI,
	NULL
};
extern TypeInfo t6383_TI;
static TypeInfo* t6381_ITIs[] = 
{
	&t603_TI,
	&t6383_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6381_0_0_0;
extern Il2CppType t6381_1_0_0;
struct t6381;
extern Il2CppGenericClass t6381_GC;
TypeInfo t6381_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6381_MIs, t6381_PIs, NULL, NULL, NULL, NULL, NULL, &t6381_TI, t6381_ITIs, NULL, &EmptyCustomAttributesCache, &t6381_TI, &t6381_0_0_0, &t6381_1_0_0, NULL, &t6381_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Resources.NeutralResourcesLanguageAttribute>
extern Il2CppType t4891_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33087_GM;
MethodInfo m33087_MI = 
{
	"GetEnumerator", NULL, &t6383_TI, &t4891_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33087_GM};
static MethodInfo* t6383_MIs[] =
{
	&m33087_MI,
	NULL
};
static TypeInfo* t6383_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6383_0_0_0;
extern Il2CppType t6383_1_0_0;
struct t6383;
extern Il2CppGenericClass t6383_GC;
TypeInfo t6383_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6383_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6383_TI, t6383_ITIs, NULL, &EmptyCustomAttributesCache, &t6383_TI, &t6383_0_0_0, &t6383_1_0_0, NULL, &t6383_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6382_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Resources.NeutralResourcesLanguageAttribute>
extern MethodInfo m33088_MI;
extern MethodInfo m33089_MI;
static PropertyInfo t6382____Item_PropertyInfo = 
{
	&t6382_TI, "Item", &m33088_MI, &m33089_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6382_PIs[] =
{
	&t6382____Item_PropertyInfo,
	NULL
};
extern Il2CppType t706_0_0_0;
static ParameterInfo t6382_m33090_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t706_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33090_GM;
MethodInfo m33090_MI = 
{
	"IndexOf", NULL, &t6382_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6382_m33090_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33090_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t706_0_0_0;
static ParameterInfo t6382_m33091_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t706_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33091_GM;
MethodInfo m33091_MI = 
{
	"Insert", NULL, &t6382_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6382_m33091_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33091_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6382_m33092_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33092_GM;
MethodInfo m33092_MI = 
{
	"RemoveAt", NULL, &t6382_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6382_m33092_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33092_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6382_m33088_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t706_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33088_GM;
MethodInfo m33088_MI = 
{
	"get_Item", NULL, &t6382_TI, &t706_0_0_0, RuntimeInvoker_t29_t44, t6382_m33088_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33088_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t706_0_0_0;
static ParameterInfo t6382_m33089_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t706_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33089_GM;
MethodInfo m33089_MI = 
{
	"set_Item", NULL, &t6382_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6382_m33089_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33089_GM};
static MethodInfo* t6382_MIs[] =
{
	&m33090_MI,
	&m33091_MI,
	&m33092_MI,
	&m33088_MI,
	&m33089_MI,
	NULL
};
static TypeInfo* t6382_ITIs[] = 
{
	&t603_TI,
	&t6381_TI,
	&t6383_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6382_0_0_0;
extern Il2CppType t6382_1_0_0;
struct t6382;
extern Il2CppGenericClass t6382_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6382_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6382_MIs, t6382_PIs, NULL, NULL, NULL, NULL, NULL, &t6382_TI, t6382_ITIs, NULL, &t1908__CustomAttributeCache, &t6382_TI, &t6382_0_0_0, &t6382_1_0_0, NULL, &t6382_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4893_TI;

#include "t704.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Resources.SatelliteContractVersionAttribute>
extern MethodInfo m33093_MI;
static PropertyInfo t4893____Current_PropertyInfo = 
{
	&t4893_TI, "Current", &m33093_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4893_PIs[] =
{
	&t4893____Current_PropertyInfo,
	NULL
};
extern Il2CppType t704_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33093_GM;
MethodInfo m33093_MI = 
{
	"get_Current", NULL, &t4893_TI, &t704_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33093_GM};
static MethodInfo* t4893_MIs[] =
{
	&m33093_MI,
	NULL
};
static TypeInfo* t4893_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4893_0_0_0;
extern Il2CppType t4893_1_0_0;
struct t4893;
extern Il2CppGenericClass t4893_GC;
TypeInfo t4893_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4893_MIs, t4893_PIs, NULL, NULL, NULL, NULL, NULL, &t4893_TI, t4893_ITIs, NULL, &EmptyCustomAttributesCache, &t4893_TI, &t4893_0_0_0, &t4893_1_0_0, NULL, &t4893_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3399.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3399_TI;
#include "t3399MD.h"

extern TypeInfo t704_TI;
extern MethodInfo m18869_MI;
extern MethodInfo m25336_MI;
struct t20;
#define m25336(__this, p0, method) (t704 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Resources.SatelliteContractVersionAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3399_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3399_TI, offsetof(t3399, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3399_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3399_TI, offsetof(t3399, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3399_FIs[] =
{
	&t3399_f0_FieldInfo,
	&t3399_f1_FieldInfo,
	NULL
};
extern MethodInfo m18866_MI;
static PropertyInfo t3399____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3399_TI, "System.Collections.IEnumerator.Current", &m18866_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3399____Current_PropertyInfo = 
{
	&t3399_TI, "Current", &m18869_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3399_PIs[] =
{
	&t3399____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3399____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3399_m18865_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18865_GM;
MethodInfo m18865_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3399_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3399_m18865_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18865_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18866_GM;
MethodInfo m18866_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3399_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18866_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18867_GM;
MethodInfo m18867_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3399_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18867_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18868_GM;
MethodInfo m18868_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3399_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18868_GM};
extern Il2CppType t704_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18869_GM;
MethodInfo m18869_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3399_TI, &t704_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18869_GM};
static MethodInfo* t3399_MIs[] =
{
	&m18865_MI,
	&m18866_MI,
	&m18867_MI,
	&m18868_MI,
	&m18869_MI,
	NULL
};
extern MethodInfo m18868_MI;
extern MethodInfo m18867_MI;
static MethodInfo* t3399_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18866_MI,
	&m18868_MI,
	&m18867_MI,
	&m18869_MI,
};
static TypeInfo* t3399_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4893_TI,
};
static Il2CppInterfaceOffsetPair t3399_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4893_TI, 7},
};
extern TypeInfo t704_TI;
static Il2CppRGCTXData t3399_RGCTXData[3] = 
{
	&m18869_MI/* Method Usage */,
	&t704_TI/* Class Usage */,
	&m25336_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3399_0_0_0;
extern Il2CppType t3399_1_0_0;
extern Il2CppGenericClass t3399_GC;
TypeInfo t3399_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3399_MIs, t3399_PIs, t3399_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3399_TI, t3399_ITIs, t3399_VT, &EmptyCustomAttributesCache, &t3399_TI, &t3399_0_0_0, &t3399_1_0_0, t3399_IOs, &t3399_GC, NULL, NULL, NULL, t3399_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3399)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6384_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Resources.SatelliteContractVersionAttribute>
extern MethodInfo m33094_MI;
static PropertyInfo t6384____Count_PropertyInfo = 
{
	&t6384_TI, "Count", &m33094_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33095_MI;
static PropertyInfo t6384____IsReadOnly_PropertyInfo = 
{
	&t6384_TI, "IsReadOnly", &m33095_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6384_PIs[] =
{
	&t6384____Count_PropertyInfo,
	&t6384____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33094_GM;
MethodInfo m33094_MI = 
{
	"get_Count", NULL, &t6384_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33094_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33095_GM;
MethodInfo m33095_MI = 
{
	"get_IsReadOnly", NULL, &t6384_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33095_GM};
extern Il2CppType t704_0_0_0;
extern Il2CppType t704_0_0_0;
static ParameterInfo t6384_m33096_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t704_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33096_GM;
MethodInfo m33096_MI = 
{
	"Add", NULL, &t6384_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6384_m33096_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33096_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33097_GM;
MethodInfo m33097_MI = 
{
	"Clear", NULL, &t6384_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33097_GM};
extern Il2CppType t704_0_0_0;
static ParameterInfo t6384_m33098_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t704_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33098_GM;
MethodInfo m33098_MI = 
{
	"Contains", NULL, &t6384_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6384_m33098_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33098_GM};
extern Il2CppType t3654_0_0_0;
extern Il2CppType t3654_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6384_m33099_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3654_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33099_GM;
MethodInfo m33099_MI = 
{
	"CopyTo", NULL, &t6384_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6384_m33099_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33099_GM};
extern Il2CppType t704_0_0_0;
static ParameterInfo t6384_m33100_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t704_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33100_GM;
MethodInfo m33100_MI = 
{
	"Remove", NULL, &t6384_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6384_m33100_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33100_GM};
static MethodInfo* t6384_MIs[] =
{
	&m33094_MI,
	&m33095_MI,
	&m33096_MI,
	&m33097_MI,
	&m33098_MI,
	&m33099_MI,
	&m33100_MI,
	NULL
};
extern TypeInfo t6386_TI;
static TypeInfo* t6384_ITIs[] = 
{
	&t603_TI,
	&t6386_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6384_0_0_0;
extern Il2CppType t6384_1_0_0;
struct t6384;
extern Il2CppGenericClass t6384_GC;
TypeInfo t6384_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6384_MIs, t6384_PIs, NULL, NULL, NULL, NULL, NULL, &t6384_TI, t6384_ITIs, NULL, &EmptyCustomAttributesCache, &t6384_TI, &t6384_0_0_0, &t6384_1_0_0, NULL, &t6384_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Resources.SatelliteContractVersionAttribute>
extern Il2CppType t4893_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33101_GM;
MethodInfo m33101_MI = 
{
	"GetEnumerator", NULL, &t6386_TI, &t4893_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33101_GM};
static MethodInfo* t6386_MIs[] =
{
	&m33101_MI,
	NULL
};
static TypeInfo* t6386_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6386_0_0_0;
extern Il2CppType t6386_1_0_0;
struct t6386;
extern Il2CppGenericClass t6386_GC;
TypeInfo t6386_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6386_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6386_TI, t6386_ITIs, NULL, &EmptyCustomAttributesCache, &t6386_TI, &t6386_0_0_0, &t6386_1_0_0, NULL, &t6386_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6385_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Resources.SatelliteContractVersionAttribute>
extern MethodInfo m33102_MI;
extern MethodInfo m33103_MI;
static PropertyInfo t6385____Item_PropertyInfo = 
{
	&t6385_TI, "Item", &m33102_MI, &m33103_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6385_PIs[] =
{
	&t6385____Item_PropertyInfo,
	NULL
};
extern Il2CppType t704_0_0_0;
static ParameterInfo t6385_m33104_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t704_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33104_GM;
MethodInfo m33104_MI = 
{
	"IndexOf", NULL, &t6385_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6385_m33104_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33104_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t704_0_0_0;
static ParameterInfo t6385_m33105_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t704_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33105_GM;
MethodInfo m33105_MI = 
{
	"Insert", NULL, &t6385_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6385_m33105_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33105_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6385_m33106_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33106_GM;
MethodInfo m33106_MI = 
{
	"RemoveAt", NULL, &t6385_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6385_m33106_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33106_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6385_m33102_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t704_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33102_GM;
MethodInfo m33102_MI = 
{
	"get_Item", NULL, &t6385_TI, &t704_0_0_0, RuntimeInvoker_t29_t44, t6385_m33102_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33102_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t704_0_0_0;
static ParameterInfo t6385_m33103_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t704_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33103_GM;
MethodInfo m33103_MI = 
{
	"set_Item", NULL, &t6385_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6385_m33103_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33103_GM};
static MethodInfo* t6385_MIs[] =
{
	&m33104_MI,
	&m33105_MI,
	&m33106_MI,
	&m33102_MI,
	&m33103_MI,
	NULL
};
static TypeInfo* t6385_ITIs[] = 
{
	&t603_TI,
	&t6384_TI,
	&t6386_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6385_0_0_0;
extern Il2CppType t6385_1_0_0;
struct t6385;
extern Il2CppGenericClass t6385_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6385_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6385_MIs, t6385_PIs, NULL, NULL, NULL, NULL, NULL, &t6385_TI, t6385_ITIs, NULL, &t1908__CustomAttributeCache, &t6385_TI, &t6385_0_0_0, &t6385_1_0_0, NULL, &t6385_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4895_TI;

#include "t1392.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>
extern MethodInfo m33107_MI;
static PropertyInfo t4895____Current_PropertyInfo = 
{
	&t4895_TI, "Current", &m33107_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4895_PIs[] =
{
	&t4895____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1392_0_0_0;
extern void* RuntimeInvoker_t1392 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33107_GM;
MethodInfo m33107_MI = 
{
	"get_Current", NULL, &t4895_TI, &t1392_0_0_0, RuntimeInvoker_t1392, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33107_GM};
static MethodInfo* t4895_MIs[] =
{
	&m33107_MI,
	NULL
};
static TypeInfo* t4895_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4895_0_0_0;
extern Il2CppType t4895_1_0_0;
struct t4895;
extern Il2CppGenericClass t4895_GC;
TypeInfo t4895_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4895_MIs, t4895_PIs, NULL, NULL, NULL, NULL, NULL, &t4895_TI, t4895_ITIs, NULL, &EmptyCustomAttributesCache, &t4895_TI, &t4895_0_0_0, &t4895_1_0_0, NULL, &t4895_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3400.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3400_TI;
#include "t3400MD.h"

extern TypeInfo t1392_TI;
extern MethodInfo m18874_MI;
extern MethodInfo m25347_MI;
struct t20;
 int32_t m25347 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18870_MI;
 void m18870 (t3400 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18871_MI;
 t29 * m18871 (t3400 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18874(__this, &m18874_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1392_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18872_MI;
 void m18872 (t3400 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18873_MI;
 bool m18873 (t3400 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18874 (t3400 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25347(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25347_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxations>
extern Il2CppType t20_0_0_1;
FieldInfo t3400_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3400_TI, offsetof(t3400, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3400_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3400_TI, offsetof(t3400, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3400_FIs[] =
{
	&t3400_f0_FieldInfo,
	&t3400_f1_FieldInfo,
	NULL
};
static PropertyInfo t3400____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3400_TI, "System.Collections.IEnumerator.Current", &m18871_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3400____Current_PropertyInfo = 
{
	&t3400_TI, "Current", &m18874_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3400_PIs[] =
{
	&t3400____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3400____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3400_m18870_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18870_GM;
MethodInfo m18870_MI = 
{
	".ctor", (methodPointerType)&m18870, &t3400_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3400_m18870_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18870_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18871_GM;
MethodInfo m18871_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18871, &t3400_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18871_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18872_GM;
MethodInfo m18872_MI = 
{
	"Dispose", (methodPointerType)&m18872, &t3400_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18872_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18873_GM;
MethodInfo m18873_MI = 
{
	"MoveNext", (methodPointerType)&m18873, &t3400_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18873_GM};
extern Il2CppType t1392_0_0_0;
extern void* RuntimeInvoker_t1392 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18874_GM;
MethodInfo m18874_MI = 
{
	"get_Current", (methodPointerType)&m18874, &t3400_TI, &t1392_0_0_0, RuntimeInvoker_t1392, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18874_GM};
static MethodInfo* t3400_MIs[] =
{
	&m18870_MI,
	&m18871_MI,
	&m18872_MI,
	&m18873_MI,
	&m18874_MI,
	NULL
};
static MethodInfo* t3400_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18871_MI,
	&m18873_MI,
	&m18872_MI,
	&m18874_MI,
};
static TypeInfo* t3400_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4895_TI,
};
static Il2CppInterfaceOffsetPair t3400_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4895_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3400_0_0_0;
extern Il2CppType t3400_1_0_0;
extern Il2CppGenericClass t3400_GC;
TypeInfo t3400_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3400_MIs, t3400_PIs, t3400_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3400_TI, t3400_ITIs, t3400_VT, &EmptyCustomAttributesCache, &t3400_TI, &t3400_0_0_0, &t3400_1_0_0, t3400_IOs, &t3400_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3400)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6387_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxations>
extern MethodInfo m33108_MI;
static PropertyInfo t6387____Count_PropertyInfo = 
{
	&t6387_TI, "Count", &m33108_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33109_MI;
static PropertyInfo t6387____IsReadOnly_PropertyInfo = 
{
	&t6387_TI, "IsReadOnly", &m33109_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6387_PIs[] =
{
	&t6387____Count_PropertyInfo,
	&t6387____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33108_GM;
MethodInfo m33108_MI = 
{
	"get_Count", NULL, &t6387_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33108_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33109_GM;
MethodInfo m33109_MI = 
{
	"get_IsReadOnly", NULL, &t6387_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33109_GM};
extern Il2CppType t1392_0_0_0;
extern Il2CppType t1392_0_0_0;
static ParameterInfo t6387_m33110_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1392_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33110_GM;
MethodInfo m33110_MI = 
{
	"Add", NULL, &t6387_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6387_m33110_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33110_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33111_GM;
MethodInfo m33111_MI = 
{
	"Clear", NULL, &t6387_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33111_GM};
extern Il2CppType t1392_0_0_0;
static ParameterInfo t6387_m33112_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1392_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33112_GM;
MethodInfo m33112_MI = 
{
	"Contains", NULL, &t6387_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6387_m33112_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33112_GM};
extern Il2CppType t3655_0_0_0;
extern Il2CppType t3655_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6387_m33113_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3655_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33113_GM;
MethodInfo m33113_MI = 
{
	"CopyTo", NULL, &t6387_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6387_m33113_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33113_GM};
extern Il2CppType t1392_0_0_0;
static ParameterInfo t6387_m33114_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1392_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33114_GM;
MethodInfo m33114_MI = 
{
	"Remove", NULL, &t6387_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6387_m33114_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33114_GM};
static MethodInfo* t6387_MIs[] =
{
	&m33108_MI,
	&m33109_MI,
	&m33110_MI,
	&m33111_MI,
	&m33112_MI,
	&m33113_MI,
	&m33114_MI,
	NULL
};
extern TypeInfo t6389_TI;
static TypeInfo* t6387_ITIs[] = 
{
	&t603_TI,
	&t6389_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6387_0_0_0;
extern Il2CppType t6387_1_0_0;
struct t6387;
extern Il2CppGenericClass t6387_GC;
TypeInfo t6387_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6387_MIs, t6387_PIs, NULL, NULL, NULL, NULL, NULL, &t6387_TI, t6387_ITIs, NULL, &EmptyCustomAttributesCache, &t6387_TI, &t6387_0_0_0, &t6387_1_0_0, NULL, &t6387_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.CompilationRelaxations>
extern Il2CppType t4895_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33115_GM;
MethodInfo m33115_MI = 
{
	"GetEnumerator", NULL, &t6389_TI, &t4895_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33115_GM};
static MethodInfo* t6389_MIs[] =
{
	&m33115_MI,
	NULL
};
static TypeInfo* t6389_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6389_0_0_0;
extern Il2CppType t6389_1_0_0;
struct t6389;
extern Il2CppGenericClass t6389_GC;
TypeInfo t6389_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6389_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6389_TI, t6389_ITIs, NULL, &EmptyCustomAttributesCache, &t6389_TI, &t6389_0_0_0, &t6389_1_0_0, NULL, &t6389_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6388_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxations>
extern MethodInfo m33116_MI;
extern MethodInfo m33117_MI;
static PropertyInfo t6388____Item_PropertyInfo = 
{
	&t6388_TI, "Item", &m33116_MI, &m33117_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6388_PIs[] =
{
	&t6388____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1392_0_0_0;
static ParameterInfo t6388_m33118_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1392_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33118_GM;
MethodInfo m33118_MI = 
{
	"IndexOf", NULL, &t6388_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6388_m33118_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33118_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1392_0_0_0;
static ParameterInfo t6388_m33119_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1392_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33119_GM;
MethodInfo m33119_MI = 
{
	"Insert", NULL, &t6388_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6388_m33119_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33119_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6388_m33120_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33120_GM;
MethodInfo m33120_MI = 
{
	"RemoveAt", NULL, &t6388_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6388_m33120_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33120_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6388_m33116_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1392_0_0_0;
extern void* RuntimeInvoker_t1392_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33116_GM;
MethodInfo m33116_MI = 
{
	"get_Item", NULL, &t6388_TI, &t1392_0_0_0, RuntimeInvoker_t1392_t44, t6388_m33116_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33116_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1392_0_0_0;
static ParameterInfo t6388_m33117_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1392_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33117_GM;
MethodInfo m33117_MI = 
{
	"set_Item", NULL, &t6388_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6388_m33117_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33117_GM};
static MethodInfo* t6388_MIs[] =
{
	&m33118_MI,
	&m33119_MI,
	&m33120_MI,
	&m33116_MI,
	&m33117_MI,
	NULL
};
static TypeInfo* t6388_ITIs[] = 
{
	&t603_TI,
	&t6387_TI,
	&t6389_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6388_0_0_0;
extern Il2CppType t6388_1_0_0;
struct t6388;
extern Il2CppGenericClass t6388_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6388_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6388_MIs, t6388_PIs, NULL, NULL, NULL, NULL, NULL, &t6388_TI, t6388_ITIs, NULL, &t1908__CustomAttributeCache, &t6388_TI, &t6388_0_0_0, &t6388_1_0_0, NULL, &t6388_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4897_TI;

#include "t709.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>
extern MethodInfo m33121_MI;
static PropertyInfo t4897____Current_PropertyInfo = 
{
	&t4897_TI, "Current", &m33121_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4897_PIs[] =
{
	&t4897____Current_PropertyInfo,
	NULL
};
extern Il2CppType t709_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33121_GM;
MethodInfo m33121_MI = 
{
	"get_Current", NULL, &t4897_TI, &t709_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33121_GM};
static MethodInfo* t4897_MIs[] =
{
	&m33121_MI,
	NULL
};
static TypeInfo* t4897_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4897_0_0_0;
extern Il2CppType t4897_1_0_0;
struct t4897;
extern Il2CppGenericClass t4897_GC;
TypeInfo t4897_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4897_MIs, t4897_PIs, NULL, NULL, NULL, NULL, NULL, &t4897_TI, t4897_ITIs, NULL, &EmptyCustomAttributesCache, &t4897_TI, &t4897_0_0_0, &t4897_1_0_0, NULL, &t4897_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3401.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3401_TI;
#include "t3401MD.h"

extern TypeInfo t709_TI;
extern MethodInfo m18879_MI;
extern MethodInfo m25358_MI;
struct t20;
#define m25358(__this, p0, method) (t709 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3401_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3401_TI, offsetof(t3401, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3401_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3401_TI, offsetof(t3401, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3401_FIs[] =
{
	&t3401_f0_FieldInfo,
	&t3401_f1_FieldInfo,
	NULL
};
extern MethodInfo m18876_MI;
static PropertyInfo t3401____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3401_TI, "System.Collections.IEnumerator.Current", &m18876_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3401____Current_PropertyInfo = 
{
	&t3401_TI, "Current", &m18879_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3401_PIs[] =
{
	&t3401____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3401____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3401_m18875_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18875_GM;
MethodInfo m18875_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3401_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3401_m18875_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18875_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18876_GM;
MethodInfo m18876_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3401_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18876_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18877_GM;
MethodInfo m18877_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3401_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18877_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18878_GM;
MethodInfo m18878_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3401_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18878_GM};
extern Il2CppType t709_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18879_GM;
MethodInfo m18879_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3401_TI, &t709_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18879_GM};
static MethodInfo* t3401_MIs[] =
{
	&m18875_MI,
	&m18876_MI,
	&m18877_MI,
	&m18878_MI,
	&m18879_MI,
	NULL
};
extern MethodInfo m18878_MI;
extern MethodInfo m18877_MI;
static MethodInfo* t3401_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18876_MI,
	&m18878_MI,
	&m18877_MI,
	&m18879_MI,
};
static TypeInfo* t3401_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4897_TI,
};
static Il2CppInterfaceOffsetPair t3401_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4897_TI, 7},
};
extern TypeInfo t709_TI;
static Il2CppRGCTXData t3401_RGCTXData[3] = 
{
	&m18879_MI/* Method Usage */,
	&t709_TI/* Class Usage */,
	&m25358_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3401_0_0_0;
extern Il2CppType t3401_1_0_0;
extern Il2CppGenericClass t3401_GC;
TypeInfo t3401_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3401_MIs, t3401_PIs, t3401_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3401_TI, t3401_ITIs, t3401_VT, &EmptyCustomAttributesCache, &t3401_TI, &t3401_0_0_0, &t3401_1_0_0, t3401_IOs, &t3401_GC, NULL, NULL, NULL, t3401_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3401)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6390_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>
extern MethodInfo m33122_MI;
static PropertyInfo t6390____Count_PropertyInfo = 
{
	&t6390_TI, "Count", &m33122_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33123_MI;
static PropertyInfo t6390____IsReadOnly_PropertyInfo = 
{
	&t6390_TI, "IsReadOnly", &m33123_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6390_PIs[] =
{
	&t6390____Count_PropertyInfo,
	&t6390____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33122_GM;
MethodInfo m33122_MI = 
{
	"get_Count", NULL, &t6390_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33122_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33123_GM;
MethodInfo m33123_MI = 
{
	"get_IsReadOnly", NULL, &t6390_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33123_GM};
extern Il2CppType t709_0_0_0;
extern Il2CppType t709_0_0_0;
static ParameterInfo t6390_m33124_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t709_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33124_GM;
MethodInfo m33124_MI = 
{
	"Add", NULL, &t6390_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6390_m33124_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33124_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33125_GM;
MethodInfo m33125_MI = 
{
	"Clear", NULL, &t6390_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33125_GM};
extern Il2CppType t709_0_0_0;
static ParameterInfo t6390_m33126_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t709_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33126_GM;
MethodInfo m33126_MI = 
{
	"Contains", NULL, &t6390_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6390_m33126_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33126_GM};
extern Il2CppType t3656_0_0_0;
extern Il2CppType t3656_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6390_m33127_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3656_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33127_GM;
MethodInfo m33127_MI = 
{
	"CopyTo", NULL, &t6390_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6390_m33127_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33127_GM};
extern Il2CppType t709_0_0_0;
static ParameterInfo t6390_m33128_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t709_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33128_GM;
MethodInfo m33128_MI = 
{
	"Remove", NULL, &t6390_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6390_m33128_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33128_GM};
static MethodInfo* t6390_MIs[] =
{
	&m33122_MI,
	&m33123_MI,
	&m33124_MI,
	&m33125_MI,
	&m33126_MI,
	&m33127_MI,
	&m33128_MI,
	NULL
};
extern TypeInfo t6392_TI;
static TypeInfo* t6390_ITIs[] = 
{
	&t603_TI,
	&t6392_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6390_0_0_0;
extern Il2CppType t6390_1_0_0;
struct t6390;
extern Il2CppGenericClass t6390_GC;
TypeInfo t6390_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6390_MIs, t6390_PIs, NULL, NULL, NULL, NULL, NULL, &t6390_TI, t6390_ITIs, NULL, &EmptyCustomAttributesCache, &t6390_TI, &t6390_0_0_0, &t6390_1_0_0, NULL, &t6390_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>
extern Il2CppType t4897_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33129_GM;
MethodInfo m33129_MI = 
{
	"GetEnumerator", NULL, &t6392_TI, &t4897_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33129_GM};
static MethodInfo* t6392_MIs[] =
{
	&m33129_MI,
	NULL
};
static TypeInfo* t6392_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6392_0_0_0;
extern Il2CppType t6392_1_0_0;
struct t6392;
extern Il2CppGenericClass t6392_GC;
TypeInfo t6392_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6392_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6392_TI, t6392_ITIs, NULL, &EmptyCustomAttributesCache, &t6392_TI, &t6392_0_0_0, &t6392_1_0_0, NULL, &t6392_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6391_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.CompilationRelaxationsAttribute>
extern MethodInfo m33130_MI;
extern MethodInfo m33131_MI;
static PropertyInfo t6391____Item_PropertyInfo = 
{
	&t6391_TI, "Item", &m33130_MI, &m33131_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6391_PIs[] =
{
	&t6391____Item_PropertyInfo,
	NULL
};
extern Il2CppType t709_0_0_0;
static ParameterInfo t6391_m33132_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t709_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33132_GM;
MethodInfo m33132_MI = 
{
	"IndexOf", NULL, &t6391_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6391_m33132_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33132_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t709_0_0_0;
static ParameterInfo t6391_m33133_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t709_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33133_GM;
MethodInfo m33133_MI = 
{
	"Insert", NULL, &t6391_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6391_m33133_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33133_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6391_m33134_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33134_GM;
MethodInfo m33134_MI = 
{
	"RemoveAt", NULL, &t6391_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6391_m33134_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33134_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6391_m33130_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t709_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33130_GM;
MethodInfo m33130_MI = 
{
	"get_Item", NULL, &t6391_TI, &t709_0_0_0, RuntimeInvoker_t29_t44, t6391_m33130_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33130_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t709_0_0_0;
static ParameterInfo t6391_m33131_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t709_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33131_GM;
MethodInfo m33131_MI = 
{
	"set_Item", NULL, &t6391_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6391_m33131_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33131_GM};
static MethodInfo* t6391_MIs[] =
{
	&m33132_MI,
	&m33133_MI,
	&m33134_MI,
	&m33130_MI,
	&m33131_MI,
	NULL
};
static TypeInfo* t6391_ITIs[] = 
{
	&t603_TI,
	&t6390_TI,
	&t6392_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6391_0_0_0;
extern Il2CppType t6391_1_0_0;
struct t6391;
extern Il2CppGenericClass t6391_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6391_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6391_MIs, t6391_PIs, NULL, NULL, NULL, NULL, NULL, &t6391_TI, t6391_ITIs, NULL, &t1908__CustomAttributeCache, &t6391_TI, &t6391_0_0_0, &t6391_1_0_0, NULL, &t6391_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4899_TI;

#include "t1393.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>
extern MethodInfo m33135_MI;
static PropertyInfo t4899____Current_PropertyInfo = 
{
	&t4899_TI, "Current", &m33135_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4899_PIs[] =
{
	&t4899____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1393_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33135_GM;
MethodInfo m33135_MI = 
{
	"get_Current", NULL, &t4899_TI, &t1393_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33135_GM};
static MethodInfo* t4899_MIs[] =
{
	&m33135_MI,
	NULL
};
static TypeInfo* t4899_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4899_0_0_0;
extern Il2CppType t4899_1_0_0;
struct t4899;
extern Il2CppGenericClass t4899_GC;
TypeInfo t4899_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4899_MIs, t4899_PIs, NULL, NULL, NULL, NULL, NULL, &t4899_TI, t4899_ITIs, NULL, &EmptyCustomAttributesCache, &t4899_TI, &t4899_0_0_0, &t4899_1_0_0, NULL, &t4899_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3402.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3402_TI;
#include "t3402MD.h"

extern TypeInfo t1393_TI;
extern MethodInfo m18884_MI;
extern MethodInfo m25369_MI;
struct t20;
#define m25369(__this, p0, method) (t1393 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3402_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3402_TI, offsetof(t3402, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3402_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3402_TI, offsetof(t3402, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3402_FIs[] =
{
	&t3402_f0_FieldInfo,
	&t3402_f1_FieldInfo,
	NULL
};
extern MethodInfo m18881_MI;
static PropertyInfo t3402____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3402_TI, "System.Collections.IEnumerator.Current", &m18881_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3402____Current_PropertyInfo = 
{
	&t3402_TI, "Current", &m18884_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3402_PIs[] =
{
	&t3402____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3402____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3402_m18880_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18880_GM;
MethodInfo m18880_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3402_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3402_m18880_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18880_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18881_GM;
MethodInfo m18881_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3402_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18881_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18882_GM;
MethodInfo m18882_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3402_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18882_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18883_GM;
MethodInfo m18883_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3402_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18883_GM};
extern Il2CppType t1393_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18884_GM;
MethodInfo m18884_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3402_TI, &t1393_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18884_GM};
static MethodInfo* t3402_MIs[] =
{
	&m18880_MI,
	&m18881_MI,
	&m18882_MI,
	&m18883_MI,
	&m18884_MI,
	NULL
};
extern MethodInfo m18883_MI;
extern MethodInfo m18882_MI;
static MethodInfo* t3402_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18881_MI,
	&m18883_MI,
	&m18882_MI,
	&m18884_MI,
};
static TypeInfo* t3402_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4899_TI,
};
static Il2CppInterfaceOffsetPair t3402_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4899_TI, 7},
};
extern TypeInfo t1393_TI;
static Il2CppRGCTXData t3402_RGCTXData[3] = 
{
	&m18884_MI/* Method Usage */,
	&t1393_TI/* Class Usage */,
	&m25369_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3402_0_0_0;
extern Il2CppType t3402_1_0_0;
extern Il2CppGenericClass t3402_GC;
TypeInfo t3402_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3402_MIs, t3402_PIs, t3402_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3402_TI, t3402_ITIs, t3402_VT, &EmptyCustomAttributesCache, &t3402_TI, &t3402_0_0_0, &t3402_1_0_0, t3402_IOs, &t3402_GC, NULL, NULL, NULL, t3402_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3402)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6393_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>
extern MethodInfo m33136_MI;
static PropertyInfo t6393____Count_PropertyInfo = 
{
	&t6393_TI, "Count", &m33136_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33137_MI;
static PropertyInfo t6393____IsReadOnly_PropertyInfo = 
{
	&t6393_TI, "IsReadOnly", &m33137_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6393_PIs[] =
{
	&t6393____Count_PropertyInfo,
	&t6393____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33136_GM;
MethodInfo m33136_MI = 
{
	"get_Count", NULL, &t6393_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33136_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33137_GM;
MethodInfo m33137_MI = 
{
	"get_IsReadOnly", NULL, &t6393_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33137_GM};
extern Il2CppType t1393_0_0_0;
extern Il2CppType t1393_0_0_0;
static ParameterInfo t6393_m33138_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1393_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33138_GM;
MethodInfo m33138_MI = 
{
	"Add", NULL, &t6393_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6393_m33138_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33138_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33139_GM;
MethodInfo m33139_MI = 
{
	"Clear", NULL, &t6393_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33139_GM};
extern Il2CppType t1393_0_0_0;
static ParameterInfo t6393_m33140_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1393_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33140_GM;
MethodInfo m33140_MI = 
{
	"Contains", NULL, &t6393_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6393_m33140_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33140_GM};
extern Il2CppType t3657_0_0_0;
extern Il2CppType t3657_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6393_m33141_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3657_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33141_GM;
MethodInfo m33141_MI = 
{
	"CopyTo", NULL, &t6393_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6393_m33141_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33141_GM};
extern Il2CppType t1393_0_0_0;
static ParameterInfo t6393_m33142_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1393_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33142_GM;
MethodInfo m33142_MI = 
{
	"Remove", NULL, &t6393_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6393_m33142_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33142_GM};
static MethodInfo* t6393_MIs[] =
{
	&m33136_MI,
	&m33137_MI,
	&m33138_MI,
	&m33139_MI,
	&m33140_MI,
	&m33141_MI,
	&m33142_MI,
	NULL
};
extern TypeInfo t6395_TI;
static TypeInfo* t6393_ITIs[] = 
{
	&t603_TI,
	&t6395_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6393_0_0_0;
extern Il2CppType t6393_1_0_0;
struct t6393;
extern Il2CppGenericClass t6393_GC;
TypeInfo t6393_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6393_MIs, t6393_PIs, NULL, NULL, NULL, NULL, NULL, &t6393_TI, t6393_ITIs, NULL, &EmptyCustomAttributesCache, &t6393_TI, &t6393_0_0_0, &t6393_1_0_0, NULL, &t6393_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>
extern Il2CppType t4899_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33143_GM;
MethodInfo m33143_MI = 
{
	"GetEnumerator", NULL, &t6395_TI, &t4899_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33143_GM};
static MethodInfo* t6395_MIs[] =
{
	&m33143_MI,
	NULL
};
static TypeInfo* t6395_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6395_0_0_0;
extern Il2CppType t6395_1_0_0;
struct t6395;
extern Il2CppGenericClass t6395_GC;
TypeInfo t6395_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6395_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6395_TI, t6395_ITIs, NULL, &EmptyCustomAttributesCache, &t6395_TI, &t6395_0_0_0, &t6395_1_0_0, NULL, &t6395_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6394_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.DefaultDependencyAttribute>
extern MethodInfo m33144_MI;
extern MethodInfo m33145_MI;
static PropertyInfo t6394____Item_PropertyInfo = 
{
	&t6394_TI, "Item", &m33144_MI, &m33145_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6394_PIs[] =
{
	&t6394____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1393_0_0_0;
static ParameterInfo t6394_m33146_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1393_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33146_GM;
MethodInfo m33146_MI = 
{
	"IndexOf", NULL, &t6394_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6394_m33146_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33146_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1393_0_0_0;
static ParameterInfo t6394_m33147_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1393_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33147_GM;
MethodInfo m33147_MI = 
{
	"Insert", NULL, &t6394_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6394_m33147_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33147_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6394_m33148_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33148_GM;
MethodInfo m33148_MI = 
{
	"RemoveAt", NULL, &t6394_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6394_m33148_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33148_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6394_m33144_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1393_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33144_GM;
MethodInfo m33144_MI = 
{
	"get_Item", NULL, &t6394_TI, &t1393_0_0_0, RuntimeInvoker_t29_t44, t6394_m33144_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33144_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1393_0_0_0;
static ParameterInfo t6394_m33145_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1393_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33145_GM;
MethodInfo m33145_MI = 
{
	"set_Item", NULL, &t6394_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6394_m33145_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33145_GM};
static MethodInfo* t6394_MIs[] =
{
	&m33146_MI,
	&m33147_MI,
	&m33148_MI,
	&m33144_MI,
	&m33145_MI,
	NULL
};
static TypeInfo* t6394_ITIs[] = 
{
	&t603_TI,
	&t6393_TI,
	&t6395_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6394_0_0_0;
extern Il2CppType t6394_1_0_0;
struct t6394;
extern Il2CppGenericClass t6394_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6394_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6394_MIs, t6394_PIs, NULL, NULL, NULL, NULL, NULL, &t6394_TI, t6394_ITIs, NULL, &t1908__CustomAttributeCache, &t6394_TI, &t6394_0_0_0, &t6394_1_0_0, NULL, &t6394_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4901_TI;

#include "t1394.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.LoadHint>
extern MethodInfo m33149_MI;
static PropertyInfo t4901____Current_PropertyInfo = 
{
	&t4901_TI, "Current", &m33149_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4901_PIs[] =
{
	&t4901____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1394_0_0_0;
extern void* RuntimeInvoker_t1394 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33149_GM;
MethodInfo m33149_MI = 
{
	"get_Current", NULL, &t4901_TI, &t1394_0_0_0, RuntimeInvoker_t1394, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33149_GM};
static MethodInfo* t4901_MIs[] =
{
	&m33149_MI,
	NULL
};
static TypeInfo* t4901_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4901_0_0_0;
extern Il2CppType t4901_1_0_0;
struct t4901;
extern Il2CppGenericClass t4901_GC;
TypeInfo t4901_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4901_MIs, t4901_PIs, NULL, NULL, NULL, NULL, NULL, &t4901_TI, t4901_ITIs, NULL, &EmptyCustomAttributesCache, &t4901_TI, &t4901_0_0_0, &t4901_1_0_0, NULL, &t4901_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3403.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3403_TI;
#include "t3403MD.h"

extern TypeInfo t1394_TI;
extern MethodInfo m18889_MI;
extern MethodInfo m25380_MI;
struct t20;
 int32_t m25380 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18885_MI;
 void m18885 (t3403 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18886_MI;
 t29 * m18886 (t3403 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18889(__this, &m18889_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1394_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18887_MI;
 void m18887 (t3403 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18888_MI;
 bool m18888 (t3403 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18889 (t3403 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25380(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25380_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.LoadHint>
extern Il2CppType t20_0_0_1;
FieldInfo t3403_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3403_TI, offsetof(t3403, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3403_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3403_TI, offsetof(t3403, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3403_FIs[] =
{
	&t3403_f0_FieldInfo,
	&t3403_f1_FieldInfo,
	NULL
};
static PropertyInfo t3403____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3403_TI, "System.Collections.IEnumerator.Current", &m18886_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3403____Current_PropertyInfo = 
{
	&t3403_TI, "Current", &m18889_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3403_PIs[] =
{
	&t3403____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3403____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3403_m18885_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18885_GM;
MethodInfo m18885_MI = 
{
	".ctor", (methodPointerType)&m18885, &t3403_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3403_m18885_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18885_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18886_GM;
MethodInfo m18886_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18886, &t3403_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18886_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18887_GM;
MethodInfo m18887_MI = 
{
	"Dispose", (methodPointerType)&m18887, &t3403_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18887_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18888_GM;
MethodInfo m18888_MI = 
{
	"MoveNext", (methodPointerType)&m18888, &t3403_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18888_GM};
extern Il2CppType t1394_0_0_0;
extern void* RuntimeInvoker_t1394 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18889_GM;
MethodInfo m18889_MI = 
{
	"get_Current", (methodPointerType)&m18889, &t3403_TI, &t1394_0_0_0, RuntimeInvoker_t1394, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18889_GM};
static MethodInfo* t3403_MIs[] =
{
	&m18885_MI,
	&m18886_MI,
	&m18887_MI,
	&m18888_MI,
	&m18889_MI,
	NULL
};
static MethodInfo* t3403_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18886_MI,
	&m18888_MI,
	&m18887_MI,
	&m18889_MI,
};
static TypeInfo* t3403_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4901_TI,
};
static Il2CppInterfaceOffsetPair t3403_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4901_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3403_0_0_0;
extern Il2CppType t3403_1_0_0;
extern Il2CppGenericClass t3403_GC;
TypeInfo t3403_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3403_MIs, t3403_PIs, t3403_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3403_TI, t3403_ITIs, t3403_VT, &EmptyCustomAttributesCache, &t3403_TI, &t3403_0_0_0, &t3403_1_0_0, t3403_IOs, &t3403_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3403)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6396_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.LoadHint>
extern MethodInfo m33150_MI;
static PropertyInfo t6396____Count_PropertyInfo = 
{
	&t6396_TI, "Count", &m33150_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33151_MI;
static PropertyInfo t6396____IsReadOnly_PropertyInfo = 
{
	&t6396_TI, "IsReadOnly", &m33151_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6396_PIs[] =
{
	&t6396____Count_PropertyInfo,
	&t6396____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33150_GM;
MethodInfo m33150_MI = 
{
	"get_Count", NULL, &t6396_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33150_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33151_GM;
MethodInfo m33151_MI = 
{
	"get_IsReadOnly", NULL, &t6396_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33151_GM};
extern Il2CppType t1394_0_0_0;
extern Il2CppType t1394_0_0_0;
static ParameterInfo t6396_m33152_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1394_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33152_GM;
MethodInfo m33152_MI = 
{
	"Add", NULL, &t6396_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6396_m33152_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33152_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33153_GM;
MethodInfo m33153_MI = 
{
	"Clear", NULL, &t6396_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33153_GM};
extern Il2CppType t1394_0_0_0;
static ParameterInfo t6396_m33154_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1394_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33154_GM;
MethodInfo m33154_MI = 
{
	"Contains", NULL, &t6396_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6396_m33154_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33154_GM};
extern Il2CppType t3658_0_0_0;
extern Il2CppType t3658_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6396_m33155_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3658_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33155_GM;
MethodInfo m33155_MI = 
{
	"CopyTo", NULL, &t6396_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6396_m33155_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33155_GM};
extern Il2CppType t1394_0_0_0;
static ParameterInfo t6396_m33156_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1394_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33156_GM;
MethodInfo m33156_MI = 
{
	"Remove", NULL, &t6396_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6396_m33156_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33156_GM};
static MethodInfo* t6396_MIs[] =
{
	&m33150_MI,
	&m33151_MI,
	&m33152_MI,
	&m33153_MI,
	&m33154_MI,
	&m33155_MI,
	&m33156_MI,
	NULL
};
extern TypeInfo t6398_TI;
static TypeInfo* t6396_ITIs[] = 
{
	&t603_TI,
	&t6398_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6396_0_0_0;
extern Il2CppType t6396_1_0_0;
struct t6396;
extern Il2CppGenericClass t6396_GC;
TypeInfo t6396_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6396_MIs, t6396_PIs, NULL, NULL, NULL, NULL, NULL, &t6396_TI, t6396_ITIs, NULL, &EmptyCustomAttributesCache, &t6396_TI, &t6396_0_0_0, &t6396_1_0_0, NULL, &t6396_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.LoadHint>
extern Il2CppType t4901_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33157_GM;
MethodInfo m33157_MI = 
{
	"GetEnumerator", NULL, &t6398_TI, &t4901_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33157_GM};
static MethodInfo* t6398_MIs[] =
{
	&m33157_MI,
	NULL
};
static TypeInfo* t6398_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6398_0_0_0;
extern Il2CppType t6398_1_0_0;
struct t6398;
extern Il2CppGenericClass t6398_GC;
TypeInfo t6398_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6398_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6398_TI, t6398_ITIs, NULL, &EmptyCustomAttributesCache, &t6398_TI, &t6398_0_0_0, &t6398_1_0_0, NULL, &t6398_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6397_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.LoadHint>
extern MethodInfo m33158_MI;
extern MethodInfo m33159_MI;
static PropertyInfo t6397____Item_PropertyInfo = 
{
	&t6397_TI, "Item", &m33158_MI, &m33159_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6397_PIs[] =
{
	&t6397____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1394_0_0_0;
static ParameterInfo t6397_m33160_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1394_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33160_GM;
MethodInfo m33160_MI = 
{
	"IndexOf", NULL, &t6397_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6397_m33160_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33160_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1394_0_0_0;
static ParameterInfo t6397_m33161_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1394_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33161_GM;
MethodInfo m33161_MI = 
{
	"Insert", NULL, &t6397_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6397_m33161_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33161_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6397_m33162_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33162_GM;
MethodInfo m33162_MI = 
{
	"RemoveAt", NULL, &t6397_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6397_m33162_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33162_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6397_m33158_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1394_0_0_0;
extern void* RuntimeInvoker_t1394_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33158_GM;
MethodInfo m33158_MI = 
{
	"get_Item", NULL, &t6397_TI, &t1394_0_0_0, RuntimeInvoker_t1394_t44, t6397_m33158_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33158_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1394_0_0_0;
static ParameterInfo t6397_m33159_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1394_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33159_GM;
MethodInfo m33159_MI = 
{
	"set_Item", NULL, &t6397_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6397_m33159_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33159_GM};
static MethodInfo* t6397_MIs[] =
{
	&m33160_MI,
	&m33161_MI,
	&m33162_MI,
	&m33158_MI,
	&m33159_MI,
	NULL
};
static TypeInfo* t6397_ITIs[] = 
{
	&t603_TI,
	&t6396_TI,
	&t6398_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6397_0_0_0;
extern Il2CppType t6397_1_0_0;
struct t6397;
extern Il2CppGenericClass t6397_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6397_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6397_MIs, t6397_PIs, NULL, NULL, NULL, NULL, NULL, &t6397_TI, t6397_ITIs, NULL, &t1908__CustomAttributeCache, &t6397_TI, &t6397_0_0_0, &t6397_1_0_0, NULL, &t6397_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4903_TI;

#include "t1396.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>
extern MethodInfo m33163_MI;
static PropertyInfo t4903____Current_PropertyInfo = 
{
	&t4903_TI, "Current", &m33163_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4903_PIs[] =
{
	&t4903____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1396_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33163_GM;
MethodInfo m33163_MI = 
{
	"get_Current", NULL, &t4903_TI, &t1396_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33163_GM};
static MethodInfo* t4903_MIs[] =
{
	&m33163_MI,
	NULL
};
static TypeInfo* t4903_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4903_0_0_0;
extern Il2CppType t4903_1_0_0;
struct t4903;
extern Il2CppGenericClass t4903_GC;
TypeInfo t4903_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4903_MIs, t4903_PIs, NULL, NULL, NULL, NULL, NULL, &t4903_TI, t4903_ITIs, NULL, &EmptyCustomAttributesCache, &t4903_TI, &t4903_0_0_0, &t4903_1_0_0, NULL, &t4903_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3404.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3404_TI;
#include "t3404MD.h"

extern TypeInfo t1396_TI;
extern MethodInfo m18894_MI;
extern MethodInfo m25391_MI;
struct t20;
#define m25391(__this, p0, method) (t1396 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.StringFreezingAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3404_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3404_TI, offsetof(t3404, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3404_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3404_TI, offsetof(t3404, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3404_FIs[] =
{
	&t3404_f0_FieldInfo,
	&t3404_f1_FieldInfo,
	NULL
};
extern MethodInfo m18891_MI;
static PropertyInfo t3404____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3404_TI, "System.Collections.IEnumerator.Current", &m18891_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3404____Current_PropertyInfo = 
{
	&t3404_TI, "Current", &m18894_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3404_PIs[] =
{
	&t3404____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3404____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3404_m18890_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18890_GM;
MethodInfo m18890_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3404_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3404_m18890_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18890_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18891_GM;
MethodInfo m18891_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3404_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18891_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18892_GM;
MethodInfo m18892_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3404_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18892_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18893_GM;
MethodInfo m18893_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3404_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18893_GM};
extern Il2CppType t1396_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18894_GM;
MethodInfo m18894_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3404_TI, &t1396_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18894_GM};
static MethodInfo* t3404_MIs[] =
{
	&m18890_MI,
	&m18891_MI,
	&m18892_MI,
	&m18893_MI,
	&m18894_MI,
	NULL
};
extern MethodInfo m18893_MI;
extern MethodInfo m18892_MI;
static MethodInfo* t3404_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18891_MI,
	&m18893_MI,
	&m18892_MI,
	&m18894_MI,
};
static TypeInfo* t3404_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4903_TI,
};
static Il2CppInterfaceOffsetPair t3404_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4903_TI, 7},
};
extern TypeInfo t1396_TI;
static Il2CppRGCTXData t3404_RGCTXData[3] = 
{
	&m18894_MI/* Method Usage */,
	&t1396_TI/* Class Usage */,
	&m25391_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3404_0_0_0;
extern Il2CppType t3404_1_0_0;
extern Il2CppGenericClass t3404_GC;
TypeInfo t3404_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3404_MIs, t3404_PIs, t3404_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3404_TI, t3404_ITIs, t3404_VT, &EmptyCustomAttributesCache, &t3404_TI, &t3404_0_0_0, &t3404_1_0_0, t3404_IOs, &t3404_GC, NULL, NULL, NULL, t3404_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3404)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6399_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.StringFreezingAttribute>
extern MethodInfo m33164_MI;
static PropertyInfo t6399____Count_PropertyInfo = 
{
	&t6399_TI, "Count", &m33164_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33165_MI;
static PropertyInfo t6399____IsReadOnly_PropertyInfo = 
{
	&t6399_TI, "IsReadOnly", &m33165_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6399_PIs[] =
{
	&t6399____Count_PropertyInfo,
	&t6399____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33164_GM;
MethodInfo m33164_MI = 
{
	"get_Count", NULL, &t6399_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33164_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33165_GM;
MethodInfo m33165_MI = 
{
	"get_IsReadOnly", NULL, &t6399_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33165_GM};
extern Il2CppType t1396_0_0_0;
extern Il2CppType t1396_0_0_0;
static ParameterInfo t6399_m33166_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1396_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33166_GM;
MethodInfo m33166_MI = 
{
	"Add", NULL, &t6399_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6399_m33166_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33166_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33167_GM;
MethodInfo m33167_MI = 
{
	"Clear", NULL, &t6399_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33167_GM};
extern Il2CppType t1396_0_0_0;
static ParameterInfo t6399_m33168_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1396_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33168_GM;
MethodInfo m33168_MI = 
{
	"Contains", NULL, &t6399_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6399_m33168_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33168_GM};
extern Il2CppType t3659_0_0_0;
extern Il2CppType t3659_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6399_m33169_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3659_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33169_GM;
MethodInfo m33169_MI = 
{
	"CopyTo", NULL, &t6399_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6399_m33169_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33169_GM};
extern Il2CppType t1396_0_0_0;
static ParameterInfo t6399_m33170_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1396_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33170_GM;
MethodInfo m33170_MI = 
{
	"Remove", NULL, &t6399_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6399_m33170_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33170_GM};
static MethodInfo* t6399_MIs[] =
{
	&m33164_MI,
	&m33165_MI,
	&m33166_MI,
	&m33167_MI,
	&m33168_MI,
	&m33169_MI,
	&m33170_MI,
	NULL
};
extern TypeInfo t6401_TI;
static TypeInfo* t6399_ITIs[] = 
{
	&t603_TI,
	&t6401_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6399_0_0_0;
extern Il2CppType t6399_1_0_0;
struct t6399;
extern Il2CppGenericClass t6399_GC;
TypeInfo t6399_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6399_MIs, t6399_PIs, NULL, NULL, NULL, NULL, NULL, &t6399_TI, t6399_ITIs, NULL, &EmptyCustomAttributesCache, &t6399_TI, &t6399_0_0_0, &t6399_1_0_0, NULL, &t6399_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.StringFreezingAttribute>
extern Il2CppType t4903_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33171_GM;
MethodInfo m33171_MI = 
{
	"GetEnumerator", NULL, &t6401_TI, &t4903_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33171_GM};
static MethodInfo* t6401_MIs[] =
{
	&m33171_MI,
	NULL
};
static TypeInfo* t6401_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6401_0_0_0;
extern Il2CppType t6401_1_0_0;
struct t6401;
extern Il2CppGenericClass t6401_GC;
TypeInfo t6401_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6401_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6401_TI, t6401_ITIs, NULL, &EmptyCustomAttributesCache, &t6401_TI, &t6401_0_0_0, &t6401_1_0_0, NULL, &t6401_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6400_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.StringFreezingAttribute>
extern MethodInfo m33172_MI;
extern MethodInfo m33173_MI;
static PropertyInfo t6400____Item_PropertyInfo = 
{
	&t6400_TI, "Item", &m33172_MI, &m33173_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6400_PIs[] =
{
	&t6400____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1396_0_0_0;
static ParameterInfo t6400_m33174_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1396_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33174_GM;
MethodInfo m33174_MI = 
{
	"IndexOf", NULL, &t6400_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6400_m33174_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33174_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1396_0_0_0;
static ParameterInfo t6400_m33175_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1396_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33175_GM;
MethodInfo m33175_MI = 
{
	"Insert", NULL, &t6400_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6400_m33175_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33175_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6400_m33176_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33176_GM;
MethodInfo m33176_MI = 
{
	"RemoveAt", NULL, &t6400_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6400_m33176_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33176_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6400_m33172_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1396_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33172_GM;
MethodInfo m33172_MI = 
{
	"get_Item", NULL, &t6400_TI, &t1396_0_0_0, RuntimeInvoker_t29_t44, t6400_m33172_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33172_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1396_0_0_0;
static ParameterInfo t6400_m33173_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1396_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33173_GM;
MethodInfo m33173_MI = 
{
	"set_Item", NULL, &t6400_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6400_m33173_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33173_GM};
static MethodInfo* t6400_MIs[] =
{
	&m33174_MI,
	&m33175_MI,
	&m33176_MI,
	&m33172_MI,
	&m33173_MI,
	NULL
};
static TypeInfo* t6400_ITIs[] = 
{
	&t603_TI,
	&t6399_TI,
	&t6401_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6400_0_0_0;
extern Il2CppType t6400_1_0_0;
struct t6400;
extern Il2CppGenericClass t6400_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6400_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6400_MIs, t6400_PIs, NULL, NULL, NULL, NULL, NULL, &t6400_TI, t6400_ITIs, NULL, &t1908__CustomAttributeCache, &t6400_TI, &t6400_0_0_0, &t6400_1_0_0, NULL, &t6400_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4905_TI;

#include "t1397.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.ConstrainedExecution.Cer>
extern MethodInfo m33177_MI;
static PropertyInfo t4905____Current_PropertyInfo = 
{
	&t4905_TI, "Current", &m33177_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4905_PIs[] =
{
	&t4905____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1397_0_0_0;
extern void* RuntimeInvoker_t1397 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33177_GM;
MethodInfo m33177_MI = 
{
	"get_Current", NULL, &t4905_TI, &t1397_0_0_0, RuntimeInvoker_t1397, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33177_GM};
static MethodInfo* t4905_MIs[] =
{
	&m33177_MI,
	NULL
};
static TypeInfo* t4905_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4905_0_0_0;
extern Il2CppType t4905_1_0_0;
struct t4905;
extern Il2CppGenericClass t4905_GC;
TypeInfo t4905_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4905_MIs, t4905_PIs, NULL, NULL, NULL, NULL, NULL, &t4905_TI, t4905_ITIs, NULL, &EmptyCustomAttributesCache, &t4905_TI, &t4905_0_0_0, &t4905_1_0_0, NULL, &t4905_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3405.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3405_TI;
#include "t3405MD.h"

extern TypeInfo t1397_TI;
extern MethodInfo m18899_MI;
extern MethodInfo m25402_MI;
struct t20;
 int32_t m25402 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18895_MI;
 void m18895 (t3405 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18896_MI;
 t29 * m18896 (t3405 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18899(__this, &m18899_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1397_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18897_MI;
 void m18897 (t3405 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18898_MI;
 bool m18898 (t3405 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18899 (t3405 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25402(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25402_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>
extern Il2CppType t20_0_0_1;
FieldInfo t3405_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3405_TI, offsetof(t3405, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3405_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3405_TI, offsetof(t3405, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3405_FIs[] =
{
	&t3405_f0_FieldInfo,
	&t3405_f1_FieldInfo,
	NULL
};
static PropertyInfo t3405____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3405_TI, "System.Collections.IEnumerator.Current", &m18896_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3405____Current_PropertyInfo = 
{
	&t3405_TI, "Current", &m18899_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3405_PIs[] =
{
	&t3405____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3405____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3405_m18895_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18895_GM;
MethodInfo m18895_MI = 
{
	".ctor", (methodPointerType)&m18895, &t3405_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3405_m18895_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18895_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18896_GM;
MethodInfo m18896_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18896, &t3405_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18896_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18897_GM;
MethodInfo m18897_MI = 
{
	"Dispose", (methodPointerType)&m18897, &t3405_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18897_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18898_GM;
MethodInfo m18898_MI = 
{
	"MoveNext", (methodPointerType)&m18898, &t3405_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18898_GM};
extern Il2CppType t1397_0_0_0;
extern void* RuntimeInvoker_t1397 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18899_GM;
MethodInfo m18899_MI = 
{
	"get_Current", (methodPointerType)&m18899, &t3405_TI, &t1397_0_0_0, RuntimeInvoker_t1397, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18899_GM};
static MethodInfo* t3405_MIs[] =
{
	&m18895_MI,
	&m18896_MI,
	&m18897_MI,
	&m18898_MI,
	&m18899_MI,
	NULL
};
static MethodInfo* t3405_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18896_MI,
	&m18898_MI,
	&m18897_MI,
	&m18899_MI,
};
static TypeInfo* t3405_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4905_TI,
};
static Il2CppInterfaceOffsetPair t3405_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4905_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3405_0_0_0;
extern Il2CppType t3405_1_0_0;
extern Il2CppGenericClass t3405_GC;
TypeInfo t3405_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3405_MIs, t3405_PIs, t3405_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3405_TI, t3405_ITIs, t3405_VT, &EmptyCustomAttributesCache, &t3405_TI, &t3405_0_0_0, &t3405_1_0_0, t3405_IOs, &t3405_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3405)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6402_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.ConstrainedExecution.Cer>
extern MethodInfo m33178_MI;
static PropertyInfo t6402____Count_PropertyInfo = 
{
	&t6402_TI, "Count", &m33178_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m33179_MI;
static PropertyInfo t6402____IsReadOnly_PropertyInfo = 
{
	&t6402_TI, "IsReadOnly", &m33179_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6402_PIs[] =
{
	&t6402____Count_PropertyInfo,
	&t6402____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33178_GM;
MethodInfo m33178_MI = 
{
	"get_Count", NULL, &t6402_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33178_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33179_GM;
MethodInfo m33179_MI = 
{
	"get_IsReadOnly", NULL, &t6402_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33179_GM};
extern Il2CppType t1397_0_0_0;
extern Il2CppType t1397_0_0_0;
static ParameterInfo t6402_m33180_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1397_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33180_GM;
MethodInfo m33180_MI = 
{
	"Add", NULL, &t6402_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6402_m33180_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33180_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33181_GM;
MethodInfo m33181_MI = 
{
	"Clear", NULL, &t6402_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33181_GM};
extern Il2CppType t1397_0_0_0;
static ParameterInfo t6402_m33182_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1397_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33182_GM;
MethodInfo m33182_MI = 
{
	"Contains", NULL, &t6402_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6402_m33182_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33182_GM};
extern Il2CppType t3660_0_0_0;
extern Il2CppType t3660_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6402_m33183_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3660_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33183_GM;
MethodInfo m33183_MI = 
{
	"CopyTo", NULL, &t6402_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6402_m33183_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33183_GM};
extern Il2CppType t1397_0_0_0;
static ParameterInfo t6402_m33184_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1397_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33184_GM;
MethodInfo m33184_MI = 
{
	"Remove", NULL, &t6402_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6402_m33184_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33184_GM};
static MethodInfo* t6402_MIs[] =
{
	&m33178_MI,
	&m33179_MI,
	&m33180_MI,
	&m33181_MI,
	&m33182_MI,
	&m33183_MI,
	&m33184_MI,
	NULL
};
extern TypeInfo t6404_TI;
static TypeInfo* t6402_ITIs[] = 
{
	&t603_TI,
	&t6404_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6402_0_0_0;
extern Il2CppType t6402_1_0_0;
struct t6402;
extern Il2CppGenericClass t6402_GC;
TypeInfo t6402_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6402_MIs, t6402_PIs, NULL, NULL, NULL, NULL, NULL, &t6402_TI, t6402_ITIs, NULL, &EmptyCustomAttributesCache, &t6402_TI, &t6402_0_0_0, &t6402_1_0_0, NULL, &t6402_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.ConstrainedExecution.Cer>
extern Il2CppType t4905_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33185_GM;
MethodInfo m33185_MI = 
{
	"GetEnumerator", NULL, &t6404_TI, &t4905_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m33185_GM};
static MethodInfo* t6404_MIs[] =
{
	&m33185_MI,
	NULL
};
static TypeInfo* t6404_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6404_0_0_0;
extern Il2CppType t6404_1_0_0;
struct t6404;
extern Il2CppGenericClass t6404_GC;
TypeInfo t6404_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6404_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6404_TI, t6404_ITIs, NULL, &EmptyCustomAttributesCache, &t6404_TI, &t6404_0_0_0, &t6404_1_0_0, NULL, &t6404_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6403_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.ConstrainedExecution.Cer>
extern MethodInfo m33186_MI;
extern MethodInfo m33187_MI;
static PropertyInfo t6403____Item_PropertyInfo = 
{
	&t6403_TI, "Item", &m33186_MI, &m33187_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6403_PIs[] =
{
	&t6403____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1397_0_0_0;
static ParameterInfo t6403_m33188_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1397_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33188_GM;
MethodInfo m33188_MI = 
{
	"IndexOf", NULL, &t6403_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6403_m33188_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33188_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1397_0_0_0;
static ParameterInfo t6403_m33189_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1397_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33189_GM;
MethodInfo m33189_MI = 
{
	"Insert", NULL, &t6403_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6403_m33189_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33189_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6403_m33190_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33190_GM;
MethodInfo m33190_MI = 
{
	"RemoveAt", NULL, &t6403_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6403_m33190_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33190_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6403_m33186_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1397_0_0_0;
extern void* RuntimeInvoker_t1397_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33186_GM;
MethodInfo m33186_MI = 
{
	"get_Item", NULL, &t6403_TI, &t1397_0_0_0, RuntimeInvoker_t1397_t44, t6403_m33186_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m33186_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1397_0_0_0;
static ParameterInfo t6403_m33187_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1397_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m33187_GM;
MethodInfo m33187_MI = 
{
	"set_Item", NULL, &t6403_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6403_m33187_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m33187_GM};
static MethodInfo* t6403_MIs[] =
{
	&m33188_MI,
	&m33189_MI,
	&m33190_MI,
	&m33186_MI,
	&m33187_MI,
	NULL
};
static TypeInfo* t6403_ITIs[] = 
{
	&t603_TI,
	&t6402_TI,
	&t6404_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6403_0_0_0;
extern Il2CppType t6403_1_0_0;
struct t6403;
extern Il2CppGenericClass t6403_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6403_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6403_MIs, t6403_PIs, NULL, NULL, NULL, NULL, NULL, &t6403_TI, t6403_ITIs, NULL, &t1908__CustomAttributeCache, &t6403_TI, &t6403_0_0_0, &t6403_1_0_0, NULL, &t6403_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
