﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t54;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t57.h"

 void m1289 (t54 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m10899 (t54 * __this, t57  p0, t57  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m10900 (t54 * __this, t57  p0, t57  p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m10901 (t54 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
