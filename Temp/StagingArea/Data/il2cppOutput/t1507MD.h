﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1507;
struct t1504;
struct t296;
struct t29;
struct t841;
struct t7;
struct t1508;
struct t1458;
#include "t735.h"

 void m8112 (t1507 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8113 (t1507 * __this, t1504 * p0, t296 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8114 (t1507 * __this, t1504 * p0, t29 * p1, t841* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8115 (t1507 * __this, t1504 * p0, t7* p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8116 (t1507 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8117 (t1507 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8118 (t1507 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8119 (t1507 * __this, bool p0, t1504 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8120 (t1507 * __this, t1508 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8121 (t1507 * __this, t1508 * p0, t1508 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8122 (t1507 * __this, t1508 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8123 (t1507 * __this, t1504 * p0, t29 * p1, t735  p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8124 (t1507 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
