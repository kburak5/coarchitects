﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1206;
struct t988;
struct t781;

 void m6289 (t1206 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6290 (t1206 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6291 (t1206 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6292 (t1206 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6293 (t1206 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6294 (t1206 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
