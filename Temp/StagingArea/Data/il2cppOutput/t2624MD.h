﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2624;
struct t29;
struct t557;
struct t395;
struct t316;

 void m14118 (t2624 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14119 (t2624 * __this, t395 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14120 (t2624 * __this, t316* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14121 (t2624 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
