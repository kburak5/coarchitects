﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2921;
struct t29;
struct t20;
#include "t382.h"

 void m16016 (t2921 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16017 (t2921 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16018 (t2921 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16019 (t2921 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16020 (t2921 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
