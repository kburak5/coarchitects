﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t194;
struct t29;
struct t42;
struct t1094;
struct t7;
struct t633;
#include "t465.h"
#include "t1126.h"
#include "t851.h"
#include "t1127.h"

 void m5525 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5526 (uint16_t* __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5527 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5528 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5529 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5530 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5531 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5532 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5533 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5534 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5535 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5536 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5537 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5538 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5539 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5540 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5541 (t29 * __this, uint8_t** p0, uint8_t** p1, double** p2, uint16_t** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5542 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5543 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5544 (uint16_t* __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5545 (uint16_t* __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5546 (uint16_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4224 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4222 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1805 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4221 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1806 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5547 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1808 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4223 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4067 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5548 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5549 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m1809 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5550 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5551 (t29 * __this, uint16_t p0, t633 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m1807 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m4069 (t29 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1767 (uint16_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5552 (uint16_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5553 (uint16_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
