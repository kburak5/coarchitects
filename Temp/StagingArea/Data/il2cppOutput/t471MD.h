﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t471;
struct t466;
struct t469;
struct t473;
struct t470;
#include "t164.h"

 void m2135 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t469 * m2136 (t29 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2137 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2138 (t29 * __this, int32_t p0, t466 * p1, t473* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2139 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2140 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2141 (t29 * __this, t470 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2142 (t29 * __this, t470 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t164  m2143 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2144 (t29 * __this, int32_t p0, t164  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2145 (t29 * __this, int32_t p0, t164 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t466 * m2146 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
