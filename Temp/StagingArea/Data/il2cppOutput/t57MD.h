﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t57;
struct t16;
struct t7;

 t16 * m163 (t57 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m164 (t57 * __this, t16 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m165 (t57 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m166 (t57 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m167 (t57 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
