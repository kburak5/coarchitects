﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2515;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t184.h"

 void m13381 (t2515 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13382 (t2515 * __this, t184  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13383 (t2515 * __this, t184  p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13384 (t2515 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
