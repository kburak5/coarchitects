﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t873;
struct t875;
struct t878;
struct t879;

 void m3770 (t873 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3771 (t873 * __this, t875 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3772 (t873 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3773 (t873 * __this, int32_t* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t879 * m3774 (t873 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
