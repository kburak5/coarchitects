﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3213;
struct t29;
struct t20;
#include "t805.h"

 void m17865 (t3213 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17866 (t3213 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17867 (t3213 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17868 (t3213 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t805  m17869 (t3213 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
