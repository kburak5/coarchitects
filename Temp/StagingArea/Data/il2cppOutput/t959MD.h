﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t959;
struct t958;
struct t943;
struct t1034;
#include "t35.h"
#include "t751.h"

 void m8886 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8887 (t29 * __this, t943 * p0, t943 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t958 * m4274 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1034 * m8888 (t29 * __this, t35 p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1034 * m8889 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1034 * m8890 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1034 * m8891 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
