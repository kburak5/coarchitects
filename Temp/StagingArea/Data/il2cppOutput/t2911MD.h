﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2911;
struct t7;

 void m15946 (t2911 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m15947 (t2911 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15948 (t2911 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15949 (t2911 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15950 (t2911 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m15951 (t2911 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
