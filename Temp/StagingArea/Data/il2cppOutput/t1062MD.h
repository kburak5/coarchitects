﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1062;
struct t781;
struct t29;
struct t295;
struct t1050;
struct t67;

 void m4888 (t1062 * __this, t67 * p0, t29 * p1, t781* p2, int32_t p3, int32_t p4, bool p5, bool p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4889 (t1062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4890 (t1062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4891 (t1062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4892 (t1062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4893 (t1062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4894 (t1062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4895 (t1062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m4896 (t1062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4897 (t1062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1050 * m4898 (t1062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4899 (t1062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4900 (t1062 * __this, t295 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4901 (t1062 * __this, t295 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4902 (t1062 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4903 (t1062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
