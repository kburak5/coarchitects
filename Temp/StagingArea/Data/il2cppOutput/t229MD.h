﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t229;
struct t398;
struct t7;

 void m1896 (t229 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1895 (t229 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t398 * m1894 (t229 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2663 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2664 (t229 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2665 (t229 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
