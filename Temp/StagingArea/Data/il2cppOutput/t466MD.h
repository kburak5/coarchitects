﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t466;
struct t7;
struct t484;
struct t263;
struct t148;
struct t378;
#include "t35.h"
#include "t17.h"
#include "t164.h"

 void m2271 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2272 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2273 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2274 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2275 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2276 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2277 (t466 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t484 * m2278 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m2279 (t466 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t263 * m2280 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t263 * m2281 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m2282 (t466 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2283 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2284 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2285 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2286 (t466 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2287 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2288 (t466 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2289 (t29 * __this, t35 p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2290 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2291 (t29 * __this, t148 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t466 * m2292 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2293 (t466 * __this, t164  p0, t378 * p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2294 (t29 * __this, t35 p0, t164  p1, t378 * p2, int32_t p3, t17 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2295 (t29 * __this, t35 p0, t164 * p1, t378 * p2, int32_t p3, t17 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2296 (t466 * __this, t378 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2297 (t29 * __this, t35 p0, t378 * p1, t17 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2298 (t466 * __this, t378 * p0, float p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2299 (t29 * __this, t35 p0, t378 * p1, float p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2300 (t466 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
