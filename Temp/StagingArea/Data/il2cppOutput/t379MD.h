﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t379;
#include "t23.h"
#include "t329.h"

 void m1745 (t379 * __this, t23  p0, t23  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t23  m2417 (t379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2418 (t379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1746 (t379 * __this, t329  p0, float* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
