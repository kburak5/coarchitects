﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1004;
struct t809;
struct t7;

 void m4565 (t1004 * __this, t809 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4566 (t1004 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4567 (t1004 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4568 (t1004 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4569 (t1004 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
