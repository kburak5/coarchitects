﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t407;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m14833 (t407 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14834 (t407 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14835 (t407 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14836 (t407 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
