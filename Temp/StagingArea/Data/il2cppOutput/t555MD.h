﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t555;
struct t41;
struct t41_marshaled;
struct t7;

 void m2782 (t555 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t41 * m2783 (t555 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2784 (t555 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2785 (t555 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m2786 (t555 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2787 (t555 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2788 (t555 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
