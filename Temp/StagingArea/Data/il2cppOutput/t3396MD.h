﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3396;
struct t29;
struct t20;
#include "t1382.h"

 void m18850 (t3396 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18851 (t3396 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18852 (t3396 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18853 (t3396 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18854 (t3396 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
