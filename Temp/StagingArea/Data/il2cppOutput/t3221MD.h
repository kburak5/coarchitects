﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3221;
struct t29;
struct t20;
#include "t817.h"

 void m17905 (t3221 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17906 (t3221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17907 (t3221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17908 (t3221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17909 (t3221 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
