﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2961;
struct t29;
struct t20;
#include "t319.h"

 void m16176 (t2961 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16177 (t2961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16178 (t2961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16179 (t2961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16180 (t2961 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
