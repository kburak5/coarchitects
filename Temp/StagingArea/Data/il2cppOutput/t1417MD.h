﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1417;
struct t42;
struct t7;
struct t1416;
struct t316;
struct t868;
struct t721;
struct t733;
struct t29;
#include "t735.h"

 void m7832 (t1417 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7833 (t1417 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7834 (t1417 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7835 (t1417 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7836 (t1417 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7837 (t1417 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7838 (t1417 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7839 (t1417 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7840 (t1417 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7841 (t1417 * __this, t316* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7842 (t1417 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7843 (t1417 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7844 (t1417 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7845 (t1417 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
