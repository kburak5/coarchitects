﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t997;
struct t781;
struct t996;
struct t988;
struct t7;
#include "t1023.h"
#include "t1117.h"

 void m5143 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5115 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5111 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5163 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5145 (t997 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5116 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5117 (t997 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5118 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8444 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5161 (t997 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m5146 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5147 (t997 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5119 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5120 (t997 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t996* m5121 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5122 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5123 (t997 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5124 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5125 (t997 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5126 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5114 (t997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t997 * m5160 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
