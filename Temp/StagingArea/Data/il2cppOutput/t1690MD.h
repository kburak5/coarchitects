﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1690;
struct t1690_marshaled;

void t1690_marshal(const t1690& unmarshaled, t1690_marshaled& marshaled);
void t1690_marshal_back(const t1690_marshaled& marshaled, t1690& unmarshaled);
void t1690_marshal_cleanup(t1690_marshaled& marshaled);
