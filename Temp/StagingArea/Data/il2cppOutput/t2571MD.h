﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2571;
struct t29;
struct t155;
struct t364;
#include "t725.h"
#include "t2569.h"

 void m13849 (t2571 * __this, t364 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13850 (t2571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m13851 (t2571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13852 (t2571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13853 (t2571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13854 (t2571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2569  m13855 (t2571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t155 * m13856 (t2571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13857 (t2571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13858 (t2571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13859 (t2571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13860 (t2571 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
