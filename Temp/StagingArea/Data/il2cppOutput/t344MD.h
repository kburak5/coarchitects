﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t344;
struct t1094;
struct t29;
struct t42;
struct t7;
struct t295;
#include "t465.h"
#include "t1126.h"
#include "t923.h"

 bool m5360 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m5361 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5362 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5363 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m5364 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m5365 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m5366 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5367 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5368 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m5369 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m5370 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5371 (uint32_t* __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m5372 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5373 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5374 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5375 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5376 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5377 (uint32_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5378 (uint32_t* __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5379 (uint32_t* __this, uint32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5380 (t29 * __this, t7* p0, bool p1, uint32_t* p2, t295 ** p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5381 (t29 * __this, t7* p0, int32_t p1, t29 * p2, bool p3, uint32_t* p4, t295 ** p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5382 (t29 * __this, t7* p0, int32_t p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m5383 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4278 (t29 * __this, t7* p0, uint32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5384 (t29 * __this, t7* p0, int32_t p1, t29 * p2, uint32_t* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5385 (uint32_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5386 (uint32_t* __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5387 (uint32_t* __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5388 (uint32_t* __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
