﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t632;
struct t632_marshaled;

void t632_marshal(const t632& unmarshaled, t632_marshaled& marshaled);
void t632_marshal_back(const t632_marshaled& marshaled, t632& unmarshaled);
void t632_marshal_cleanup(t632_marshaled& marshaled);
