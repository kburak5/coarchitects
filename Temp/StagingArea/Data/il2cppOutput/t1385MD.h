﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1385;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m7666 (t1385 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7667 (t1385 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7668 (t1385 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7669 (t1385 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
