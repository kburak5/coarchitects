﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1141;
struct t29;
struct t296;
struct t66;
struct t67;
#include "t35.h"

 void m9620 (t1141 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9621 (t1141 * __this, t296 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9622 (t1141 * __this, t296 * p0, t29 * p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9623 (t1141 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
