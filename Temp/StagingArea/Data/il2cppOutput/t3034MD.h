﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3034;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t381.h"

 void m16667 (t3034 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16668 (t3034 * __this, t381  p0, t381  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16669 (t3034 * __this, t381  p0, t381  p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16670 (t3034 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
