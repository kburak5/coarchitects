﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t912;
struct t29;

 void m6669 (t912 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6670 (t912 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6671 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t912 * m3956 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6672 (t912 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
