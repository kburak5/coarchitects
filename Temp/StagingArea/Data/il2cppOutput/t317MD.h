﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t317;

 void m2479 (t317 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1390 (t317 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1773 (t317 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1391 (t317 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
