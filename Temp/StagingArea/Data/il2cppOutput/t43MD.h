﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t43;
struct t733;
struct t29;
#include "t35.h"
#include "t735.h"

 void m6056 (t43 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t35 m6057 (t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6058 (t43 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6059 (t43 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6060 (t43 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
