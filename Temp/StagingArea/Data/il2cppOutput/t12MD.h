﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t12;
struct t7;

 void m18 (t12 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19 (t12 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m20 (t12 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m21 (t12 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
