﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1482;
struct t636;
struct t42;
struct t29;
struct t7;
struct t1466;
struct t1465;
struct t1453;
struct t537;
struct t1472;
struct t316;
struct t1468;
struct t1469;
struct t1165;
#include "t1484.h"

 void m8001 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m8002 (t29 * __this, t42 * p0, t636 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8003 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m8004 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8005 (t29 * __this, t1466 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8006 (t29 * __this, t1466 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1465 * m8007 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m8008 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m8009 (t29 * __this, t42 * p0, t7* p1, t537* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m8010 (t29 * __this, t42 * p0, t7* p1, t537* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8011 (t29 * __this, t1472 * p0, t316* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8012 (t29 * __this, t42 * p0, t316* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1468 * m8013 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8014 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1469 * m8015 (t29 * __this, t1466 * p0, t42 * p1, t29 ** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1165 * m8016 (t29 * __this, t42 * p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8017 (t29 * __this, t1165 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8018 (t29 * __this, t1466 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8019 (t29 * __this, t1466 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8020 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8021 (t29 * __this, t1468 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8022 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
