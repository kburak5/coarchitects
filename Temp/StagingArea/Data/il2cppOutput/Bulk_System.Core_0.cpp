﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "t687.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t687_TI;
#include "t687MD.h"


#include "t20.h"

// Metadata Definition <Module>
static MethodInfo* t687_MIs[] =
{
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t687_0_0_0;
extern Il2CppType t687_1_0_0;
struct t687;
TypeInfo t687_TI = 
{
	&g_System_Core_dll_Image, NULL, "<Module>", "", t687_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t687_TI, NULL, NULL, &EmptyCustomAttributesCache, &t687_TI, &t687_0_0_0, &t687_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t687), 0, -1, 0, 0, -1, 0, 0, false, false, false, false, false, false, false, false, false, false, false, false, 0, 0, 0, 0, 0, 0, 0, 0};
#include "t686.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t686_TI;
#include "t686MD.h"

#include "t21.h"
#include "t490MD.h"
extern MethodInfo m2881_MI;


extern MethodInfo m3042_MI;
 void m3042 (t686 * __this, MethodInfo* method){
	{
		m2881(__this, &m2881_MI);
		return;
	}
}
// Metadata Definition System.Runtime.CompilerServices.ExtensionAttribute
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
MethodInfo m3042_MI = 
{
	".ctor", (methodPointerType)&m3042, &t686_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 1, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t686_MIs[] =
{
	&m3042_MI,
	NULL
};
extern MethodInfo m2882_MI;
extern MethodInfo m46_MI;
extern MethodInfo m2883_MI;
extern MethodInfo m1332_MI;
static MethodInfo* t686_VT[] =
{
	&m2882_MI,
	&m46_MI,
	&m2883_MI,
	&m1332_MI,
};
extern TypeInfo t604_TI;
static Il2CppInterfaceOffsetPair t686_IOs[] = 
{
	{ &t604_TI, 4},
};
extern TypeInfo t629_TI;
#include "t629.h"
#include "t629MD.h"
extern MethodInfo m2915_MI;
void t686_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t629 * tmp;
		tmp = (t629 *)il2cpp_codegen_object_new (&t629_TI);
		m2915(tmp, 69, &m2915_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t686__CustomAttributeCache = {
1,
NULL,
&t686_CustomAttributesCacheGenerator
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t686_0_0_0;
extern Il2CppType t686_1_0_0;
extern TypeInfo t490_TI;
struct t686;
extern CustomAttributesCache t686__CustomAttributeCache;
TypeInfo t686_TI = 
{
	&g_System_Core_dll_Image, NULL, "ExtensionAttribute", "System.Runtime.CompilerServices", t686_MIs, NULL, NULL, NULL, &t490_TI, NULL, NULL, &t686_TI, NULL, t686_VT, &t686__CustomAttributeCache, &t686_TI, &t686_0_0_0, &t686_1_0_0, t686_IOs, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t686), 0, -1, 0, 0, -1, 1048833, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 1};
#include "t688.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t688_TI;
#include "t688MD.h"

#include "t29.h"
#include "t7.h"
#include "t338.h"
extern TypeInfo t338_TI;
#include "t338MD.h"
extern MethodInfo m2950_MI;


extern MethodInfo m3043_MI;
 void m3043 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_0011;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral193, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0011:
	{
		if (p1)
		{
			goto IL_0022;
		}
	}
	{
		t338 * L_1 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_1, (t7*) &_stringLiteral194, &m2950_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0022:
	{
		return;
	}
}
// Metadata Definition System.Linq.Check
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t688_m3043_ParameterInfos[] = 
{
	{"source", 0, 134217729, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"predicate", 1, 134217730, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m3043_MI = 
{
	"SourceAndPredicate", (methodPointerType)&m3043, &t688_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t688_m3043_ParameterInfos, &EmptyCustomAttributesCache, 150, 0, 255, 2, false, false, 2, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t688_MIs[] =
{
	&m3043_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
static MethodInfo* t688_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t688_0_0_0;
extern Il2CppType t688_1_0_0;
extern TypeInfo t29_TI;
struct t688;
TypeInfo t688_TI = 
{
	&g_System_Core_dll_Image, NULL, "Check", "System.Linq", t688_MIs, NULL, NULL, NULL, &t29_TI, NULL, NULL, &t688_TI, NULL, t688_VT, &EmptyCustomAttributesCache, &t688_TI, &t688_0_0_0, &t688_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t688), 0, -1, 0, 0, -1, 1048960, 0, false, false, false, false, false, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 4, 0, 0};
#include "t689.h"
extern Il2CppGenericContainer t689_IGC;
extern TypeInfo t689_gp_TSource_0_TI;
Il2CppGenericParamFull t689_gp_TSource_0_TI_GenericParamFull = { { &t689_IGC, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* t689_IGPA[1] = 
{
	&t689_gp_TSource_0_TI_GenericParamFull,
};
extern TypeInfo t689_TI;
Il2CppGenericContainer t689_IGC = { { NULL, NULL }, NULL, &t689_TI, 1, 0, t689_IGPA };
extern Il2CppType t21_0_0_0;
MethodInfo m3044_MI = 
{
	".ctor", NULL, &t689_TI, &t21_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, false, 5, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t689_gp_0_0_0_0;
extern CustomAttributesCache t689__CustomAttributeCache_m3045;
MethodInfo m3045_MI = 
{
	"System.Collections.Generic.IEnumerator<TSource>.get_Current", NULL, &t689_TI, &t689_gp_0_0_0_0, NULL, NULL, &t689__CustomAttributeCache_m3045, 2529, 0, 9, 0, false, false, 6, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t29_0_0_0;
extern CustomAttributesCache t689__CustomAttributeCache_m3046;
MethodInfo m3046_MI = 
{
	"System.Collections.IEnumerator.get_Current", NULL, &t689_TI, &t29_0_0_0, NULL, NULL, &t689__CustomAttributeCache_m3046, 2529, 0, 4, 0, false, false, 7, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t136_0_0_0;
extern CustomAttributesCache t689__CustomAttributeCache_m3047;
MethodInfo m3047_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", NULL, &t689_TI, &t136_0_0_0, NULL, NULL, &t689__CustomAttributeCache_m3047, 481, 0, 7, 0, false, false, 8, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t692_0_0_0;
extern CustomAttributesCache t689__CustomAttributeCache_m3048;
MethodInfo m3048_MI = 
{
	"System.Collections.Generic.IEnumerable<TSource>.GetEnumerator", NULL, &t689_TI, &t692_0_0_0, NULL, NULL, &t689__CustomAttributeCache_m3048, 481, 0, 8, 0, false, false, 9, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t40_0_0_0;
MethodInfo m3049_MI = 
{
	"MoveNext", NULL, &t689_TI, &t40_0_0_0, NULL, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, false, 10, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t21_0_0_0;
extern CustomAttributesCache t689__CustomAttributeCache_m3050;
MethodInfo m3050_MI = 
{
	"Dispose", NULL, &t689_TI, &t21_0_0_0, NULL, NULL, &t689__CustomAttributeCache_m3050, 486, 0, 6, 0, false, false, 11, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t689_MIs[] =
{
	&m3044_MI,
	&m3045_MI,
	&m3046_MI,
	&m3047_MI,
	&m3048_MI,
	&m3049_MI,
	&m3050_MI,
	NULL
};
extern MethodInfo m3045_MI;
static PropertyInfo t689____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo = 
{
	&t689_TI, "System.Collections.Generic.IEnumerator<TSource>.Current", &m3045_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m3046_MI;
static PropertyInfo t689____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t689_TI, "System.Collections.IEnumerator.Current", &m3046_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t689_PIs[] =
{
	&t689____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo,
	&t689____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern Il2CppType t693_0_0_3;
FieldInfo t689_f0_FieldInfo = 
{
	"source", &t693_0_0_3, &t689_TI, 0, &EmptyCustomAttributesCache};
extern Il2CppType t692_0_0_3;
FieldInfo t689_f1_FieldInfo = 
{
	"<$s_97>__0", &t692_0_0_3, &t689_TI, 0, &EmptyCustomAttributesCache};
extern Il2CppType t689_gp_0_0_0_3;
FieldInfo t689_f2_FieldInfo = 
{
	"<element>__1", &t689_gp_0_0_0_3, &t689_TI, 0, &EmptyCustomAttributesCache};
extern Il2CppType t694_0_0_3;
FieldInfo t689_f3_FieldInfo = 
{
	"predicate", &t694_0_0_3, &t689_TI, 0, &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_3;
FieldInfo t689_f4_FieldInfo = 
{
	"$PC", &t44_0_0_3, &t689_TI, 0, &EmptyCustomAttributesCache};
extern Il2CppType t689_gp_0_0_0_3;
FieldInfo t689_f5_FieldInfo = 
{
	"$current", &t689_gp_0_0_0_3, &t689_TI, 0, &EmptyCustomAttributesCache};
extern Il2CppType t693_0_0_3;
FieldInfo t689_f6_FieldInfo = 
{
	"<$>source", &t693_0_0_3, &t689_TI, 0, &EmptyCustomAttributesCache};
extern Il2CppType t694_0_0_3;
FieldInfo t689_f7_FieldInfo = 
{
	"<$>predicate", &t694_0_0_3, &t689_TI, 0, &EmptyCustomAttributesCache};
static FieldInfo* t689_FIs[] =
{
	&t689_f0_FieldInfo,
	&t689_f1_FieldInfo,
	&t689_f2_FieldInfo,
	&t689_f3_FieldInfo,
	&t689_f4_FieldInfo,
	&t689_f5_FieldInfo,
	&t689_f6_FieldInfo,
	&t689_f7_FieldInfo,
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t689_0_0_0;
extern Il2CppType t689_1_0_0;
struct t689;
extern TypeInfo t406_TI;
TypeInfo t689_TI = 
{
	&g_System_Core_dll_Image, NULL, "<CreateWhereIterator>c__Iterator1D`1", "", t689_MIs, t689_PIs, t689_FIs, NULL, NULL, NULL, &t406_TI, &t689_TI, NULL, NULL, NULL, NULL, &t689_0_0_0, &t689_1_0_0, NULL, NULL, &t689_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 2, 8, 0, 0, 0, 0, 0};
extern TypeInfo t38_TI;
#include "t38.h"
#include "t38MD.h"
extern MethodInfo m55_MI;
void t689_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t38 * tmp;
		tmp = (t38 *)il2cpp_codegen_object_new (&t38_TI);
		m55(tmp, &m55_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo t342_TI;
#include "t342.h"
#include "t342MD.h"
extern MethodInfo m1507_MI;
void t689_CustomAttributesCacheGenerator_m3045(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t342 * tmp;
		tmp = (t342 *)il2cpp_codegen_object_new (&t342_TI);
		m1507(tmp, &m1507_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t689_CustomAttributesCacheGenerator_m3046(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t342 * tmp;
		tmp = (t342 *)il2cpp_codegen_object_new (&t342_TI);
		m1507(tmp, &m1507_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t689_CustomAttributesCacheGenerator_m3047(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t342 * tmp;
		tmp = (t342 *)il2cpp_codegen_object_new (&t342_TI);
		m1507(tmp, &m1507_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t689_CustomAttributesCacheGenerator_m3048(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t342 * tmp;
		tmp = (t342 *)il2cpp_codegen_object_new (&t342_TI);
		m1507(tmp, &m1507_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t689_CustomAttributesCacheGenerator_m3050(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t342 * tmp;
		tmp = (t342 *)il2cpp_codegen_object_new (&t342_TI);
		m1507(tmp, &m1507_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t689__CustomAttributeCache = {
1,
NULL,
&t689_CustomAttributesCacheGenerator
};
CustomAttributesCache t689__CustomAttributeCache_m3045 = {
1,
NULL,
&t689_CustomAttributesCacheGenerator_m3045
};
CustomAttributesCache t689__CustomAttributeCache_m3046 = {
1,
NULL,
&t689_CustomAttributesCacheGenerator_m3046
};
CustomAttributesCache t689__CustomAttributeCache_m3047 = {
1,
NULL,
&t689_CustomAttributesCacheGenerator_m3047
};
CustomAttributesCache t689__CustomAttributeCache_m3048 = {
1,
NULL,
&t689_CustomAttributesCacheGenerator_m3048
};
CustomAttributesCache t689__CustomAttributeCache_m3050 = {
1,
NULL,
&t689_CustomAttributesCacheGenerator_m3050
};
#include "t406.h"
#ifndef _MSC_VER
#else
#endif
#include "t406MD.h"



// Metadata Definition System.Linq.Enumerable
extern Il2CppType t695_0_0_0;
extern Il2CppType t695_0_0_0;
extern Il2CppType t696_0_0_0;
extern Il2CppType t696_0_0_0;
static ParameterInfo t406_m3051_ParameterInfos[] = 
{
	{"source", 0, 134217731, &EmptyCustomAttributesCache, &t695_0_0_0},
	{"predicate", 1, 134217732, &EmptyCustomAttributesCache, &t696_0_0_0},
};
extern Il2CppType t695_0_0_0;
extern Il2CppGenericContainer m3051_IGC;
extern TypeInfo m3051_gp_TSource_0_TI;
Il2CppGenericParamFull m3051_gp_TSource_0_TI_GenericParamFull = { { &m3051_IGC, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* m3051_IGPA[1] = 
{
	&m3051_gp_TSource_0_TI_GenericParamFull,
};
extern MethodInfo m3051_MI;
Il2CppGenericContainer m3051_IGC = { { NULL, NULL }, NULL, &m3051_MI, 1, 1, m3051_IGPA };
extern Il2CppGenericMethod m3052_GM;
static Il2CppRGCTXDefinition m3051_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &m3052_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache t406__CustomAttributeCache_m3051;
MethodInfo m3051_MI = 
{
	"Where", NULL, &t406_TI, &t695_0_0_0, NULL, t406_m3051_ParameterInfos, &t406__CustomAttributeCache_m3051, 150, 0, 255, 2, true, false, 3, m3051_RGCTXData, (methodPointerType)NULL, &m3051_IGC};
extern Il2CppType t698_0_0_0;
extern Il2CppType t698_0_0_0;
extern Il2CppType t699_0_0_0;
extern Il2CppType t699_0_0_0;
static ParameterInfo t406_m3053_ParameterInfos[] = 
{
	{"source", 0, 134217733, &EmptyCustomAttributesCache, &t698_0_0_0},
	{"predicate", 1, 134217734, &EmptyCustomAttributesCache, &t699_0_0_0},
};
extern Il2CppType t698_0_0_0;
extern Il2CppGenericContainer m3053_IGC;
extern TypeInfo m3053_gp_TSource_0_TI;
Il2CppGenericParamFull m3053_gp_TSource_0_TI_GenericParamFull = { { &m3053_IGC, 0}, {NULL, "TSource", 0, 0, NULL} };
static Il2CppGenericParamFull* m3053_IGPA[1] = 
{
	&m3053_gp_TSource_0_TI_GenericParamFull,
};
extern MethodInfo m3053_MI;
Il2CppGenericContainer m3053_IGC = { { NULL, NULL }, NULL, &m3053_MI, 1, 1, m3053_IGPA };
extern Il2CppType t701_0_0_0;
extern Il2CppGenericMethod m3054_GM;
static Il2CppRGCTXDefinition m3053_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &t701_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_METHOD, &m3054_GM }/* Method Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern CustomAttributesCache t406__CustomAttributeCache_m3053;
MethodInfo m3053_MI = 
{
	"CreateWhereIterator", NULL, &t406_TI, &t698_0_0_0, NULL, t406_m3053_ParameterInfos, &t406__CustomAttributeCache_m3053, 145, 0, 255, 2, true, false, 4, m3053_RGCTXData, (methodPointerType)NULL, &m3053_IGC};
static MethodInfo* t406_MIs[] =
{
	&m3051_MI,
	&m3053_MI,
	NULL
};
extern TypeInfo t689_TI;
static TypeInfo* t406_TI__nestedTypes[2] =
{
	&t689_TI,
	NULL
};
static MethodInfo* t406_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
void t406_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t686 * tmp;
		tmp = (t686 *)il2cpp_codegen_object_new (&t686_TI);
		m3042(tmp, &m3042_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t406_CustomAttributesCacheGenerator_m3051(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t686 * tmp;
		tmp = (t686 *)il2cpp_codegen_object_new (&t686_TI);
		m3042(tmp, &m3042_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void t406_CustomAttributesCacheGenerator_m3053(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t342 * tmp;
		tmp = (t342 *)il2cpp_codegen_object_new (&t342_TI);
		m1507(tmp, &m1507_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache t406__CustomAttributeCache = {
1,
NULL,
&t406_CustomAttributesCacheGenerator
};
CustomAttributesCache t406__CustomAttributeCache_m3051 = {
1,
NULL,
&t406_CustomAttributesCacheGenerator_m3051
};
CustomAttributesCache t406__CustomAttributeCache_m3053 = {
1,
NULL,
&t406_CustomAttributesCacheGenerator_m3053
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t406_0_0_0;
extern Il2CppType t406_1_0_0;
struct t406;
extern CustomAttributesCache t406__CustomAttributeCache;
extern CustomAttributesCache t406__CustomAttributeCache_m3051;
extern CustomAttributesCache t406__CustomAttributeCache_m3053;
TypeInfo t406_TI = 
{
	&g_System_Core_dll_Image, NULL, "Enumerable", "System.Linq", t406_MIs, NULL, NULL, NULL, &t29_TI, t406_TI__nestedTypes, NULL, &t406_TI, NULL, t406_VT, &t406__CustomAttributeCache, &t406_TI, &t406_0_0_0, &t406_1_0_0, NULL, NULL, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t406), 0, -1, 0, 0, -1, 1048961, 0, false, false, false, false, false, false, false, false, false, false, false, false, 2, 0, 0, 0, 1, 4, 0, 0};
#include "t690.h"
extern Il2CppGenericContainer t690_IGC;
extern TypeInfo t690_gp_T_0_TI;
Il2CppGenericParamFull t690_gp_T_0_TI_GenericParamFull = { { &t690_IGC, 0}, {NULL, "T", 0, 0, NULL} };
extern TypeInfo t690_gp_TResult_1_TI;
Il2CppGenericParamFull t690_gp_TResult_1_TI_GenericParamFull = { { &t690_IGC, 1}, {NULL, "TResult", 0, 0, NULL} };
static Il2CppGenericParamFull* t690_IGPA[2] = 
{
	&t690_gp_T_0_TI_GenericParamFull,
	&t690_gp_TResult_1_TI_GenericParamFull,
};
extern TypeInfo t690_TI;
Il2CppGenericContainer t690_IGC = { { NULL, NULL }, NULL, &t690_TI, 2, 0, t690_IGPA };
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t690_m3055_ParameterInfos[] = 
{
	{"object", 0, 134217735, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217736, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
MethodInfo m3055_MI = 
{
	".ctor", NULL, &t690_TI, &t21_0_0_0, NULL, t690_m3055_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, false, 12, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t690_gp_0_0_0_0;
extern Il2CppType t690_gp_0_0_0_0;
static ParameterInfo t690_m3056_ParameterInfos[] = 
{
	{"arg1", 0, 134217737, &EmptyCustomAttributesCache, &t690_gp_0_0_0_0},
};
extern Il2CppType t690_gp_1_0_0_0;
MethodInfo m3056_MI = 
{
	"Invoke", NULL, &t690_TI, &t690_gp_1_0_0_0, NULL, t690_m3056_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, false, 13, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t690_gp_0_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t690_m3057_ParameterInfos[] = 
{
	{"arg1", 0, 134217738, &EmptyCustomAttributesCache, &t690_gp_0_0_0_0},
	{"callback", 1, 134217739, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217740, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
MethodInfo m3057_MI = 
{
	"BeginInvoke", NULL, &t690_TI, &t66_0_0_0, NULL, t690_m3057_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, false, 14, NULL, (methodPointerType)NULL, NULL};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t690_m3058_ParameterInfos[] = 
{
	{"result", 0, 134217741, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t690_gp_1_0_0_0;
MethodInfo m3058_MI = 
{
	"EndInvoke", NULL, &t690_TI, &t690_gp_1_0_0_0, NULL, t690_m3058_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, false, 15, NULL, (methodPointerType)NULL, NULL};
static MethodInfo* t690_MIs[] =
{
	&m3055_MI,
	&m3056_MI,
	&m3057_MI,
	&m3058_MI,
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType t690_0_0_0;
extern Il2CppType t690_1_0_0;
struct t690;
TypeInfo t690_TI = 
{
	&g_System_Core_dll_Image, NULL, "Func`2", "System", t690_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t690_TI, NULL, NULL, NULL, NULL, &t690_0_0_0, &t690_1_0_0, NULL, NULL, &t690_IGC, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 0, 0, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
