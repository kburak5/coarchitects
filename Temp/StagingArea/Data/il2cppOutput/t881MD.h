﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t881;
struct t7;
struct t878;
struct t29;

 void m3778 (t881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3779 (t881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3780 (t881 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3781 (t881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3782 (t881 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3783 (t881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3784 (t881 * __this, t29 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3785 (t881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3786 (t881 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
