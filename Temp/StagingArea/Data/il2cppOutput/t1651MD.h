﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1651;
struct t7;
struct t733;
#include "t735.h"

 void m9293 (t1651 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9294 (t1651 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9295 (t1651 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9296 (t1651 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
