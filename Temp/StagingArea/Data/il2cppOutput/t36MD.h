﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t36;
struct t34;
struct t557;
struct t7;
struct t29;
struct t556;

 void m1512 (t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m53 (t36 * __this, t34 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m1513 (t36 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t556 * m1514 (t36 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t556 * m2821 (t29 * __this, t34 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1517 (t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
