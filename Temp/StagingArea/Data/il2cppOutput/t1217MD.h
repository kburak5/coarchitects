﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1217;
struct t997;
struct t781;
#include "t1117.h"

 void m6394 (t1217 * __this, t997 * p0, bool p1, t781* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6395 (t1217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6396 (t1217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6397 (t1217 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6398 (t1217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6399 (t1217 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6400 (t1217 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6401 (t1217 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6402 (t1217 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6403 (t1217 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6404 (t1217 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6405 (t1217 * __this, t781* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6406 (t1217 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6407 (t1217 * __this, t781* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6408 (t1217 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6409 (t1217 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6410 (t1217 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6411 (t1217 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6412 (t1217 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
