﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1567;
struct t7;
struct t1562;
struct t760;
struct t29;

 t7* m8490 (t1567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1562 * m8491 (t1567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t760 * m8492 (t1567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8493 (t1567 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8494 (t1567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8495 (t1567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
