﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t946;
struct t29;
struct t7;
struct t316;
struct t781;

 void m8170 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8171 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4110 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4183 (t29 * __this, t7* p0, t316* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5138 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4125 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8172 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
