﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t870;
struct t29;
struct t869;
struct t20;
struct t136;
#include "t866.h"

 void m3709 (t870 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t866  m3710 (t870 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3711 (t870 * __this, t866  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3712 (t870 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t870 * m3713 (t870 * __this, t869 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3714 (t870 * __this, int32_t p0, int32_t p1, t870 * p2, t869 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3715 (t870 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3716 (t870 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3717 (t870 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3718 (t870 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3719 (t870 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
