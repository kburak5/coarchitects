﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2228;
struct t2120;
struct t29;

 void m11051_gshared (t2228 * __this, t2120 * p0, t2120 * p1, MethodInfo* method);
#define m11051(__this, p0, p1, method) (void)m11051_gshared((t2228 *)__this, (t2120 *)p0, (t2120 *)p1, method)
 int32_t m11053_gshared (t2228 * __this, MethodInfo* method);
#define m11053(__this, method) (int32_t)m11053_gshared((t2228 *)__this, method)
 void m11055_gshared (t2228 * __this, int32_t p0, MethodInfo* method);
#define m11055(__this, p0, method) (void)m11055_gshared((t2228 *)__this, (int32_t)p0, method)
 int32_t m11057_gshared (t2228 * __this, MethodInfo* method);
#define m11057(__this, method) (int32_t)m11057_gshared((t2228 *)__this, method)
 int32_t m11059_gshared (t2228 * __this, MethodInfo* method);
#define m11059(__this, method) (int32_t)m11059_gshared((t2228 *)__this, method)
 t29 * m11061_gshared (t2228 * __this, MethodInfo* method);
#define m11061(__this, method) (t29 *)m11061_gshared((t2228 *)__this, method)
 void m11063_gshared (t2228 * __this, t29 * p0, MethodInfo* method);
#define m11063(__this, p0, method) (void)m11063_gshared((t2228 *)__this, (t29 *)p0, method)
