﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2161;
struct t29;
struct t20;
#include "t48.h"

 void m10487 (t2161 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m10488 (t2161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m10489 (t2161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m10490 (t2161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m10491 (t2161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
