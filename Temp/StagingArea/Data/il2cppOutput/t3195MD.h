﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3195;
struct t29;
struct t20;
#include "t3192.h"

 void m17755 (t3195 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17756 (t3195 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17757 (t3195 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17758 (t3195 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3192  m17759 (t3195 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
