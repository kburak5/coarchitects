﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2569;
struct t155;
struct t7;

 void m13819 (t2569 * __this, t155 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t155 * m13820 (t2569 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13821 (t2569 * __this, t155 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m13822 (t2569 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13823 (t2569 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m13824 (t2569 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
