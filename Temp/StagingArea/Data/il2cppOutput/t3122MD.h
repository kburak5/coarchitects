﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3122;
struct t29;
struct t20;
#include "t554.h"

 void m17249 (t3122 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17250 (t3122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17251 (t3122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17252 (t3122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17253 (t3122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
