﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t875;
struct t878;
struct t879;

 void m3763 (t875 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3764 (t875 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t879 * m3765 (t875 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
