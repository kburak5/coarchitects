﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3446;
struct t29;
struct t20;
#include "t1495.h"

 void m19100 (t3446 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19101 (t3446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19102 (t3446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19103 (t3446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19104 (t3446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
