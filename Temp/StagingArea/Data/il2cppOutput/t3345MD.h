﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3345;
struct t29;
struct t20;
#include "t923.h"

 void m18597 (t3345 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18598 (t3345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18599 (t3345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18600 (t3345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18601 (t3345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
