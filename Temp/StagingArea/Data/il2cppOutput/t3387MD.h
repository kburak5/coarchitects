﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3387;
struct t29;
struct t20;
#include "t1347.h"

 void m18807 (t3387 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18808 (t3387 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18809 (t3387 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18810 (t3387 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18811 (t3387 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
