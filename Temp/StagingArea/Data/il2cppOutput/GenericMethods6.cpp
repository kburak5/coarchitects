﻿#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"
#include "codegen/il2cpp-codegen.h"

#include "t20.h"
#include "t3411.h"
#include "t21.h"
extern TypeInfo t3411_TI;
#include "t3411MD.h"
extern MethodInfo m18925_MI;
extern MethodInfo m25477_MI;
struct t20;
 t29* m25477 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25477 (t20 * __this, MethodInfo* method){
	{
		t3411  L_0 = {0};
		m18925(&L_0, __this, &m18925_MI);
		t3411  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3411_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.InteropServices.ClassInterfaceType>()
extern TypeInfo t20_TI;
extern TypeInfo t4917_TI;
extern Il2CppGenericInst GenInst_t1403_0_0_0;
extern Il2CppType t4917_0_0_0;
extern TypeInfo t20_TI;
extern Il2CppGenericMethod m25477_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25477_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25477, &t20_TI, &t4917_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25477_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1404.h"
#include "t44.h"
#include "t7.h"
#include "t915.h"
extern TypeInfo t915_TI;
#include "t20MD.h"
#include "t915MD.h"
extern MethodInfo m3969_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m25478_MI;
struct t20;
struct t20;
#include "t29.h"
extern MethodInfo m25479_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m25479(__this, p0, method) (t1404 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1404_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25479_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1404_0_0_0;
extern Il2CppType t1404_0_0_0;
extern Il2CppGenericMethod m25479_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25479_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1404_0_0_0, RuntimeInvoker_t29_t44, t20_m25479_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25479_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern TypeInfo t21_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1404_1_0_2;
extern Il2CppType t1404_1_0_0;
static ParameterInfo t20_m25478_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1404_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25478_GM;
extern void* RuntimeInvoker_t21_t44_t4918 (MethodInfo* method, void* obj, void** args);
MethodInfo m25478_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4918, t20_m25478_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25478_GM};
#ifndef _MSC_VER
#else
#endif

#include "t345.h"
extern TypeInfo t345_TI;
#include "t345MD.h"
extern MethodInfo m3988_MI;
extern MethodInfo m25480_MI;
struct t20;
struct t20;
 void m19494_gshared (t20 * __this, t29 * p0, MethodInfo* method);
#define m19494(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
#define m25480(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1404_0_0_0;
extern Il2CppType t1404_0_0_0;
static ParameterInfo t20_m25480_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1404_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25480_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25480_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25480_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25480_GM};
#ifndef _MSC_VER
#else
#endif

#include "t40.h"
#include "t1667.h"
extern TypeInfo t1667_TI;
extern TypeInfo t29_TI;
extern TypeInfo t40_TI;
#include "t1167MD.h"
#include "t1667MD.h"
#include "t29MD.h"
extern MethodInfo m3977_MI;
extern MethodInfo m6079_MI;
extern MethodInfo m9495_MI;
extern MethodInfo m1321_MI;
extern MethodInfo m25481_MI;
struct t20;
struct t20;
 bool m19496_gshared (t20 * __this, t29 * p0, MethodInfo* method);
#define m19496(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
#define m25481(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1404_0_0_0;
static ParameterInfo t20_m25481_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1404_0_0_0},
};
extern TypeInfo t1404_TI;
static Il2CppRGCTXData m25481_RGCTXData[1] = 
{
	&t1404_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25481_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25481_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25481_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25481_RGCTXData, NULL, &m25481_GM};
#ifndef _MSC_VER
#else
#endif

#include "mscorlib_ArrayTypes.h"
#include "t338.h"
#include "t305.h"
extern TypeInfo t338_TI;
extern TypeInfo t305_TI;
#include "t338MD.h"
#include "t305MD.h"
extern MethodInfo m2950_MI;
extern MethodInfo m5911_MI;
extern MethodInfo m5913_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m3968_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m25482_MI;
struct t20;
struct t20;
 void m19498_gshared (t20 * __this, t316* p0, int32_t p1, MethodInfo* method);
#define m19498(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
#define m25482(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3667_0_0_0;
extern Il2CppType t3667_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25482_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3667_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25482_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25482_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25482_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25482_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25483_MI;
struct t20;
struct t20;
 bool m19499_gshared (t20 * __this, t29 * p0, MethodInfo* method);
#define m19499(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
#define m25483(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1404_0_0_0;
static ParameterInfo t20_m25483_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1404_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25483_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25483_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25483_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25483_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25484_MI;
struct t20;
struct t20;
 int32_t m19500_gshared (t20 * __this, t29 * p0, MethodInfo* method);
#define m19500(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
#define m25484(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>(T)
extern TypeInfo t20_TI;
extern TypeInfo t44_TI;
extern Il2CppType t1404_0_0_0;
static ParameterInfo t20_m25484_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1404_0_0_0},
};
extern TypeInfo t1404_TI;
static Il2CppRGCTXData m25484_RGCTXData[1] = 
{
	&t1404_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25484_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25484_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25484_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25484_RGCTXData, NULL, &m25484_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25485_MI;
struct t20;
struct t20;
 void m19501_gshared (t20 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m19501(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m25485(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1404_0_0_0;
static ParameterInfo t20_m25485_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1404_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25485_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25485_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25485_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25485_GM};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t316_TI;
extern MethodInfo m25486_MI;
struct t20;
struct t20;
extern MethodInfo m25487_MI;
struct t20;
struct t20;
 void m19503_gshared (t20 * __this, int32_t p0, t29 * p1, MethodInfo* method);
#define m19503(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m25487(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1404_0_0_0;
static ParameterInfo t20_m25487_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1404_0_0_0},
};
extern TypeInfo t1404_TI;
static Il2CppRGCTXData m25487_RGCTXData[1] = 
{
	&t1404_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25487_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25487_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25487_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25487_RGCTXData, NULL, &m25487_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1404_1_0_0;
static ParameterInfo t20_m25486_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1404_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25486_GM;
extern void* RuntimeInvoker_t21_t44_t4918 (MethodInfo* method, void* obj, void** args);
MethodInfo m25486_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4918, t20_m25486_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25486_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3412.h"
extern TypeInfo t3412_TI;
#include "t3412MD.h"
extern MethodInfo m18930_MI;
extern MethodInfo m25488_MI;
struct t20;
struct t20;
#include "t2111.h"
 t29* m19504_gshared (t20 * __this, MethodInfo* method);
#define m19504(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
#define m25488(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.InteropServices.ComDefaultInterfaceAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4919_TI;
extern TypeInfo t3412_TI;
static Il2CppRGCTXData m25488_RGCTXData[2] = 
{
	&t3412_TI/* Class Usage */,
	&m18930_MI/* Method Usage */,
};
extern Il2CppType t4919_0_0_0;
extern Il2CppGenericMethod m25488_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25488_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4919_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25488_RGCTXData, NULL, &m25488_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1405.h"
extern MethodInfo m25489_MI;
struct t20;
extern MethodInfo m25490_MI;
struct t20;
 int32_t m25490 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25490 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.InteropServices.ComInterfaceType>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1405_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25490_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1405_0_0_0;
extern Il2CppType t1405_0_0_0;
extern Il2CppGenericMethod m25490_GM;
extern void* RuntimeInvoker_t1405_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25490_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25490, &t20_TI, &t1405_0_0_0, RuntimeInvoker_t1405_t44, t20_m25490_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25490_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.InteropServices.ComInterfaceType>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1405_1_0_2;
extern Il2CppType t1405_1_0_0;
static ParameterInfo t20_m25489_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1405_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25489_GM;
extern void* RuntimeInvoker_t21_t44_t4920 (MethodInfo* method, void* obj, void** args);
MethodInfo m25489_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4920, t20_m25489_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25489_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25491_MI;
struct t20;
 void m25491 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25491 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.InteropServices.ComInterfaceType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1405_0_0_0;
extern Il2CppType t1405_0_0_0;
static ParameterInfo t20_m25491_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25491_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25491_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25491, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25491_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25491_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25492_MI;
struct t20;
 bool m25492 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25492 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1405_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1405_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1405_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1405_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.InteropServices.ComInterfaceType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1405_0_0_0;
static ParameterInfo t20_m25492_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25492_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25492_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25492, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25492_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25492_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25493_MI;
struct t20;
 void m25493 (t20 * __this, t3668* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25493 (t20 * __this, t3668* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.InteropServices.ComInterfaceType>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3668_0_0_0;
extern Il2CppType t3668_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25493_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3668_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25493_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25493_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25493, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25493_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25493_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25494_MI;
struct t20;
 bool m25494 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25494 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.InteropServices.ComInterfaceType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1405_0_0_0;
static ParameterInfo t20_m25494_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25494_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25494_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25494, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25494_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25494_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25495_MI;
struct t20;
 int32_t m25495 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25495 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1405_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1405_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1405_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1405_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.InteropServices.ComInterfaceType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1405_0_0_0;
static ParameterInfo t20_m25495_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25495_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25495_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25495, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25495_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25495_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25496_MI;
struct t20;
 void m25496 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25496 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.InteropServices.ComInterfaceType>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1405_0_0_0;
static ParameterInfo t20_m25496_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25496_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25496_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25496, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25496_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25496_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25497_MI;
struct t20;
extern MethodInfo m25498_MI;
struct t20;
 void m25498 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25498 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1405_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.InteropServices.ComInterfaceType>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1405_0_0_0;
static ParameterInfo t20_m25498_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1405_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25498_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25498_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25498, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25498_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25498_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.InteropServices.ComInterfaceType>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1405_1_0_0;
static ParameterInfo t20_m25497_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1405_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25497_GM;
extern void* RuntimeInvoker_t21_t44_t4920 (MethodInfo* method, void* obj, void** args);
MethodInfo m25497_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4920, t20_m25497_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25497_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3413.h"
extern TypeInfo t3413_TI;
#include "t3413MD.h"
extern MethodInfo m18935_MI;
extern MethodInfo m25499_MI;
struct t20;
 t29* m25499 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25499 (t20 * __this, MethodInfo* method){
	{
		t3413  L_0 = {0};
		m18935(&L_0, __this, &m18935_MI);
		t3413  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3413_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.InteropServices.ComInterfaceType>()
extern TypeInfo t20_TI;
extern TypeInfo t4921_TI;
extern Il2CppType t4921_0_0_0;
extern Il2CppGenericMethod m25499_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25499_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25499, &t20_TI, &t4921_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25499_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1406.h"
extern MethodInfo m25500_MI;
struct t20;
extern MethodInfo m25501_MI;
struct t20;
#define m25501(__this, p0, method) (t1406 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.InteropServices.DispIdAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1406_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25501_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1406_0_0_0;
extern Il2CppType t1406_0_0_0;
extern Il2CppGenericMethod m25501_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25501_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1406_0_0_0, RuntimeInvoker_t29_t44, t20_m25501_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25501_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.InteropServices.DispIdAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1406_1_0_2;
extern Il2CppType t1406_1_0_0;
static ParameterInfo t20_m25500_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1406_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25500_GM;
extern void* RuntimeInvoker_t21_t44_t4922 (MethodInfo* method, void* obj, void** args);
MethodInfo m25500_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4922, t20_m25500_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25500_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25502_MI;
struct t20;
#define m25502(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.InteropServices.DispIdAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1406_0_0_0;
extern Il2CppType t1406_0_0_0;
static ParameterInfo t20_m25502_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1406_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25502_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25502_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25502_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25502_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25503_MI;
struct t20;
#define m25503(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.InteropServices.DispIdAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1406_0_0_0;
static ParameterInfo t20_m25503_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1406_0_0_0},
};
extern TypeInfo t1406_TI;
static Il2CppRGCTXData m25503_RGCTXData[1] = 
{
	&t1406_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25503_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25503_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25503_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25503_RGCTXData, NULL, &m25503_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25504_MI;
struct t20;
#define m25504(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.InteropServices.DispIdAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3669_0_0_0;
extern Il2CppType t3669_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25504_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3669_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25504_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25504_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25504_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25504_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25505_MI;
struct t20;
#define m25505(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.InteropServices.DispIdAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1406_0_0_0;
static ParameterInfo t20_m25505_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1406_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25505_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25505_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25505_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25505_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25506_MI;
struct t20;
#define m25506(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.InteropServices.DispIdAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1406_0_0_0;
static ParameterInfo t20_m25506_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1406_0_0_0},
};
extern TypeInfo t1406_TI;
static Il2CppRGCTXData m25506_RGCTXData[1] = 
{
	&t1406_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25506_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25506_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25506_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25506_RGCTXData, NULL, &m25506_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25507_MI;
struct t20;
#define m25507(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.InteropServices.DispIdAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1406_0_0_0;
static ParameterInfo t20_m25507_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1406_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25507_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25507_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25507_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25507_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25508_MI;
struct t20;
extern MethodInfo m25509_MI;
struct t20;
#define m25509(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.InteropServices.DispIdAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1406_0_0_0;
static ParameterInfo t20_m25509_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1406_0_0_0},
};
extern TypeInfo t1406_TI;
static Il2CppRGCTXData m25509_RGCTXData[1] = 
{
	&t1406_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25509_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25509_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25509_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25509_RGCTXData, NULL, &m25509_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.InteropServices.DispIdAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1406_1_0_0;
static ParameterInfo t20_m25508_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1406_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25508_GM;
extern void* RuntimeInvoker_t21_t44_t4922 (MethodInfo* method, void* obj, void** args);
MethodInfo m25508_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4922, t20_m25508_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25508_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3414.h"
extern TypeInfo t3414_TI;
#include "t3414MD.h"
extern MethodInfo m18940_MI;
extern MethodInfo m25510_MI;
struct t20;
#define m25510(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.InteropServices.DispIdAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4923_TI;
extern TypeInfo t3414_TI;
static Il2CppRGCTXData m25510_RGCTXData[2] = 
{
	&t3414_TI/* Class Usage */,
	&m18940_MI/* Method Usage */,
};
extern Il2CppType t4923_0_0_0;
extern Il2CppGenericMethod m25510_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25510_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4923_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25510_RGCTXData, NULL, &m25510_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1408.h"
extern MethodInfo m25511_MI;
struct t20;
extern MethodInfo m25512_MI;
struct t20;
 int32_t m25512 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25512 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.InteropServices.GCHandleType>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1408_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25512_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1408_0_0_0;
extern Il2CppType t1408_0_0_0;
extern Il2CppGenericMethod m25512_GM;
extern void* RuntimeInvoker_t1408_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25512_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25512, &t20_TI, &t1408_0_0_0, RuntimeInvoker_t1408_t44, t20_m25512_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25512_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.InteropServices.GCHandleType>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1408_1_0_2;
extern Il2CppType t1408_1_0_0;
static ParameterInfo t20_m25511_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1408_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25511_GM;
extern void* RuntimeInvoker_t21_t44_t4924 (MethodInfo* method, void* obj, void** args);
MethodInfo m25511_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4924, t20_m25511_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25511_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25513_MI;
struct t20;
 void m25513 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25513 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.InteropServices.GCHandleType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1408_0_0_0;
extern Il2CppType t1408_0_0_0;
static ParameterInfo t20_m25513_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25513_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25513_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25513, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25513_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25513_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25514_MI;
struct t20;
 bool m25514 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25514 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1408_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1408_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1408_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1408_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.InteropServices.GCHandleType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1408_0_0_0;
static ParameterInfo t20_m25514_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25514_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25514_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25514, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25514_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25514_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25515_MI;
struct t20;
 void m25515 (t20 * __this, t3670* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25515 (t20 * __this, t3670* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.InteropServices.GCHandleType>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3670_0_0_0;
extern Il2CppType t3670_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25515_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3670_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25515_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25515_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25515, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25515_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25515_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25516_MI;
struct t20;
 bool m25516 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25516 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.InteropServices.GCHandleType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1408_0_0_0;
static ParameterInfo t20_m25516_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25516_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25516_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25516, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25516_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25516_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25517_MI;
struct t20;
 int32_t m25517 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25517 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1408_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1408_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1408_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1408_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.InteropServices.GCHandleType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1408_0_0_0;
static ParameterInfo t20_m25517_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25517_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25517_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25517, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25517_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25517_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25518_MI;
struct t20;
 void m25518 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25518 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.InteropServices.GCHandleType>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1408_0_0_0;
static ParameterInfo t20_m25518_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25518_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25518_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25518, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25518_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25518_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25519_MI;
struct t20;
extern MethodInfo m25520_MI;
struct t20;
 void m25520 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25520 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1408_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.InteropServices.GCHandleType>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1408_0_0_0;
static ParameterInfo t20_m25520_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1408_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25520_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25520_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25520, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25520_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25520_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.InteropServices.GCHandleType>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1408_1_0_0;
static ParameterInfo t20_m25519_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1408_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25519_GM;
extern void* RuntimeInvoker_t21_t44_t4924 (MethodInfo* method, void* obj, void** args);
MethodInfo m25519_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4924, t20_m25519_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25519_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3415.h"
extern TypeInfo t3415_TI;
#include "t3415MD.h"
extern MethodInfo m18945_MI;
extern MethodInfo m25521_MI;
struct t20;
 t29* m25521 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25521 (t20 * __this, MethodInfo* method){
	{
		t3415  L_0 = {0};
		m18945(&L_0, __this, &m18945_MI);
		t3415  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3415_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.InteropServices.GCHandleType>()
extern TypeInfo t20_TI;
extern TypeInfo t4925_TI;
extern Il2CppType t4925_0_0_0;
extern Il2CppGenericMethod m25521_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25521_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25521, &t20_TI, &t4925_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25521_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1409.h"
extern MethodInfo m25522_MI;
struct t20;
extern MethodInfo m25523_MI;
struct t20;
#define m25523(__this, p0, method) (t1409 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.InteropServices.InterfaceTypeAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1409_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25523_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1409_0_0_0;
extern Il2CppType t1409_0_0_0;
extern Il2CppGenericMethod m25523_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25523_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1409_0_0_0, RuntimeInvoker_t29_t44, t20_m25523_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25523_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.InteropServices.InterfaceTypeAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1409_1_0_2;
extern Il2CppType t1409_1_0_0;
static ParameterInfo t20_m25522_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1409_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25522_GM;
extern void* RuntimeInvoker_t21_t44_t4926 (MethodInfo* method, void* obj, void** args);
MethodInfo m25522_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4926, t20_m25522_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25522_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25524_MI;
struct t20;
#define m25524(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.InteropServices.InterfaceTypeAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1409_0_0_0;
extern Il2CppType t1409_0_0_0;
static ParameterInfo t20_m25524_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1409_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25524_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25524_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25524_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25524_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25525_MI;
struct t20;
#define m25525(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.InteropServices.InterfaceTypeAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1409_0_0_0;
static ParameterInfo t20_m25525_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1409_0_0_0},
};
extern TypeInfo t1409_TI;
static Il2CppRGCTXData m25525_RGCTXData[1] = 
{
	&t1409_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25525_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25525_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25525_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25525_RGCTXData, NULL, &m25525_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25526_MI;
struct t20;
#define m25526(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.InteropServices.InterfaceTypeAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3671_0_0_0;
extern Il2CppType t3671_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25526_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3671_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25526_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25526_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25526_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25526_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25527_MI;
struct t20;
#define m25527(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.InteropServices.InterfaceTypeAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1409_0_0_0;
static ParameterInfo t20_m25527_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1409_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25527_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25527_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25527_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25527_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25528_MI;
struct t20;
#define m25528(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.InteropServices.InterfaceTypeAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1409_0_0_0;
static ParameterInfo t20_m25528_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1409_0_0_0},
};
extern TypeInfo t1409_TI;
static Il2CppRGCTXData m25528_RGCTXData[1] = 
{
	&t1409_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25528_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25528_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25528_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25528_RGCTXData, NULL, &m25528_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25529_MI;
struct t20;
#define m25529(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.InteropServices.InterfaceTypeAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1409_0_0_0;
static ParameterInfo t20_m25529_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1409_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25529_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25529_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25529_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25529_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25530_MI;
struct t20;
extern MethodInfo m25531_MI;
struct t20;
#define m25531(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.InteropServices.InterfaceTypeAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1409_0_0_0;
static ParameterInfo t20_m25531_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1409_0_0_0},
};
extern TypeInfo t1409_TI;
static Il2CppRGCTXData m25531_RGCTXData[1] = 
{
	&t1409_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25531_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25531_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25531_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25531_RGCTXData, NULL, &m25531_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.InteropServices.InterfaceTypeAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1409_1_0_0;
static ParameterInfo t20_m25530_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1409_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25530_GM;
extern void* RuntimeInvoker_t21_t44_t4926 (MethodInfo* method, void* obj, void** args);
MethodInfo m25530_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4926, t20_m25530_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25530_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3416.h"
extern TypeInfo t3416_TI;
#include "t3416MD.h"
extern MethodInfo m18950_MI;
extern MethodInfo m25532_MI;
struct t20;
#define m25532(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.InteropServices.InterfaceTypeAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4927_TI;
extern TypeInfo t3416_TI;
static Il2CppRGCTXData m25532_RGCTXData[2] = 
{
	&t3416_TI/* Class Usage */,
	&m18950_MI/* Method Usage */,
};
extern Il2CppType t4927_0_0_0;
extern Il2CppGenericMethod m25532_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25532_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4927_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25532_RGCTXData, NULL, &m25532_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1412.h"
extern MethodInfo m25533_MI;
struct t20;
extern MethodInfo m25534_MI;
struct t20;
#define m25534(__this, p0, method) (t1412 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.InteropServices.PreserveSigAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1412_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25534_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1412_0_0_0;
extern Il2CppType t1412_0_0_0;
extern Il2CppGenericMethod m25534_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25534_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1412_0_0_0, RuntimeInvoker_t29_t44, t20_m25534_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25534_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.InteropServices.PreserveSigAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1412_1_0_2;
extern Il2CppType t1412_1_0_0;
static ParameterInfo t20_m25533_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1412_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25533_GM;
extern void* RuntimeInvoker_t21_t44_t4928 (MethodInfo* method, void* obj, void** args);
MethodInfo m25533_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4928, t20_m25533_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25533_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25535_MI;
struct t20;
#define m25535(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.InteropServices.PreserveSigAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1412_0_0_0;
extern Il2CppType t1412_0_0_0;
static ParameterInfo t20_m25535_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1412_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25535_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25535_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25535_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25535_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25536_MI;
struct t20;
#define m25536(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.InteropServices.PreserveSigAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1412_0_0_0;
static ParameterInfo t20_m25536_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1412_0_0_0},
};
extern TypeInfo t1412_TI;
static Il2CppRGCTXData m25536_RGCTXData[1] = 
{
	&t1412_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25536_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25536_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25536_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25536_RGCTXData, NULL, &m25536_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25537_MI;
struct t20;
#define m25537(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.InteropServices.PreserveSigAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3672_0_0_0;
extern Il2CppType t3672_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25537_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3672_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25537_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25537_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25537_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25537_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25538_MI;
struct t20;
#define m25538(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.InteropServices.PreserveSigAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1412_0_0_0;
static ParameterInfo t20_m25538_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1412_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25538_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25538_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25538_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25538_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25539_MI;
struct t20;
#define m25539(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.InteropServices.PreserveSigAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1412_0_0_0;
static ParameterInfo t20_m25539_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1412_0_0_0},
};
extern TypeInfo t1412_TI;
static Il2CppRGCTXData m25539_RGCTXData[1] = 
{
	&t1412_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25539_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25539_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25539_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25539_RGCTXData, NULL, &m25539_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25540_MI;
struct t20;
#define m25540(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.InteropServices.PreserveSigAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1412_0_0_0;
static ParameterInfo t20_m25540_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1412_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25540_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25540_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25540_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25540_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25541_MI;
struct t20;
extern MethodInfo m25542_MI;
struct t20;
#define m25542(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.InteropServices.PreserveSigAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1412_0_0_0;
static ParameterInfo t20_m25542_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1412_0_0_0},
};
extern TypeInfo t1412_TI;
static Il2CppRGCTXData m25542_RGCTXData[1] = 
{
	&t1412_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25542_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25542_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25542_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25542_RGCTXData, NULL, &m25542_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.InteropServices.PreserveSigAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1412_1_0_0;
static ParameterInfo t20_m25541_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1412_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25541_GM;
extern void* RuntimeInvoker_t21_t44_t4928 (MethodInfo* method, void* obj, void** args);
MethodInfo m25541_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4928, t20_m25541_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25541_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3417.h"
extern TypeInfo t3417_TI;
#include "t3417MD.h"
extern MethodInfo m18955_MI;
extern MethodInfo m25543_MI;
struct t20;
#define m25543(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.InteropServices.PreserveSigAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4929_TI;
extern TypeInfo t3417_TI;
static Il2CppRGCTXData m25543_RGCTXData[2] = 
{
	&t3417_TI/* Class Usage */,
	&m18955_MI/* Method Usage */,
};
extern Il2CppType t4929_0_0_0;
extern Il2CppGenericMethod m25543_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25543_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4929_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25543_RGCTXData, NULL, &m25543_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1413.h"
extern MethodInfo m25544_MI;
struct t20;
extern MethodInfo m25545_MI;
struct t20;
#define m25545(__this, p0, method) (t1413 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.InteropServices.TypeLibImportClassAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1413_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25545_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1413_0_0_0;
extern Il2CppType t1413_0_0_0;
extern Il2CppGenericMethod m25545_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25545_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1413_0_0_0, RuntimeInvoker_t29_t44, t20_m25545_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25545_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.InteropServices.TypeLibImportClassAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1413_1_0_2;
extern Il2CppType t1413_1_0_0;
static ParameterInfo t20_m25544_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1413_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25544_GM;
extern void* RuntimeInvoker_t21_t44_t4930 (MethodInfo* method, void* obj, void** args);
MethodInfo m25544_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4930, t20_m25544_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25544_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25546_MI;
struct t20;
#define m25546(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.InteropServices.TypeLibImportClassAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1413_0_0_0;
extern Il2CppType t1413_0_0_0;
static ParameterInfo t20_m25546_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1413_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25546_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25546_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25546_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25546_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25547_MI;
struct t20;
#define m25547(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.InteropServices.TypeLibImportClassAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1413_0_0_0;
static ParameterInfo t20_m25547_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1413_0_0_0},
};
extern TypeInfo t1413_TI;
static Il2CppRGCTXData m25547_RGCTXData[1] = 
{
	&t1413_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25547_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25547_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25547_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25547_RGCTXData, NULL, &m25547_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25548_MI;
struct t20;
#define m25548(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.InteropServices.TypeLibImportClassAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3673_0_0_0;
extern Il2CppType t3673_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25548_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3673_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25548_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25548_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25548_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25548_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25549_MI;
struct t20;
#define m25549(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.InteropServices.TypeLibImportClassAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1413_0_0_0;
static ParameterInfo t20_m25549_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1413_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25549_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25549_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25549_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25549_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25550_MI;
struct t20;
#define m25550(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.InteropServices.TypeLibImportClassAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1413_0_0_0;
static ParameterInfo t20_m25550_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1413_0_0_0},
};
extern TypeInfo t1413_TI;
static Il2CppRGCTXData m25550_RGCTXData[1] = 
{
	&t1413_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25550_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25550_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25550_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25550_RGCTXData, NULL, &m25550_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25551_MI;
struct t20;
#define m25551(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.InteropServices.TypeLibImportClassAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1413_0_0_0;
static ParameterInfo t20_m25551_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1413_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25551_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25551_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25551_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25551_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25552_MI;
struct t20;
extern MethodInfo m25553_MI;
struct t20;
#define m25553(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.InteropServices.TypeLibImportClassAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1413_0_0_0;
static ParameterInfo t20_m25553_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1413_0_0_0},
};
extern TypeInfo t1413_TI;
static Il2CppRGCTXData m25553_RGCTXData[1] = 
{
	&t1413_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25553_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25553_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25553_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25553_RGCTXData, NULL, &m25553_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.InteropServices.TypeLibImportClassAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1413_1_0_0;
static ParameterInfo t20_m25552_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1413_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25552_GM;
extern void* RuntimeInvoker_t21_t44_t4930 (MethodInfo* method, void* obj, void** args);
MethodInfo m25552_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4930, t20_m25552_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25552_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3418.h"
extern TypeInfo t3418_TI;
#include "t3418MD.h"
extern MethodInfo m18960_MI;
extern MethodInfo m25554_MI;
struct t20;
#define m25554(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.InteropServices.TypeLibImportClassAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4931_TI;
extern TypeInfo t3418_TI;
static Il2CppRGCTXData m25554_RGCTXData[2] = 
{
	&t3418_TI/* Class Usage */,
	&m18960_MI/* Method Usage */,
};
extern Il2CppType t4931_0_0_0;
extern Il2CppGenericMethod m25554_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25554_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4931_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25554_RGCTXData, NULL, &m25554_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1414.h"
extern MethodInfo m25555_MI;
struct t20;
extern MethodInfo m25556_MI;
struct t20;
#define m25556(__this, p0, method) (t1414 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.InteropServices.TypeLibVersionAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1414_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25556_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1414_0_0_0;
extern Il2CppType t1414_0_0_0;
extern Il2CppGenericMethod m25556_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25556_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1414_0_0_0, RuntimeInvoker_t29_t44, t20_m25556_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25556_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.InteropServices.TypeLibVersionAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1414_1_0_2;
extern Il2CppType t1414_1_0_0;
static ParameterInfo t20_m25555_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1414_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25555_GM;
extern void* RuntimeInvoker_t21_t44_t4932 (MethodInfo* method, void* obj, void** args);
MethodInfo m25555_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4932, t20_m25555_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25555_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25557_MI;
struct t20;
#define m25557(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.InteropServices.TypeLibVersionAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1414_0_0_0;
extern Il2CppType t1414_0_0_0;
static ParameterInfo t20_m25557_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1414_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25557_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25557_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25557_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25557_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25558_MI;
struct t20;
#define m25558(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.InteropServices.TypeLibVersionAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1414_0_0_0;
static ParameterInfo t20_m25558_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1414_0_0_0},
};
extern TypeInfo t1414_TI;
static Il2CppRGCTXData m25558_RGCTXData[1] = 
{
	&t1414_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25558_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25558_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25558_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25558_RGCTXData, NULL, &m25558_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25559_MI;
struct t20;
#define m25559(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.InteropServices.TypeLibVersionAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3674_0_0_0;
extern Il2CppType t3674_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25559_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3674_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25559_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25559_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25559_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25559_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25560_MI;
struct t20;
#define m25560(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.InteropServices.TypeLibVersionAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1414_0_0_0;
static ParameterInfo t20_m25560_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1414_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25560_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25560_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25560_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25560_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25561_MI;
struct t20;
#define m25561(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.InteropServices.TypeLibVersionAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1414_0_0_0;
static ParameterInfo t20_m25561_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1414_0_0_0},
};
extern TypeInfo t1414_TI;
static Il2CppRGCTXData m25561_RGCTXData[1] = 
{
	&t1414_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25561_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25561_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25561_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25561_RGCTXData, NULL, &m25561_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25562_MI;
struct t20;
#define m25562(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.InteropServices.TypeLibVersionAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1414_0_0_0;
static ParameterInfo t20_m25562_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1414_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25562_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25562_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25562_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25562_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25563_MI;
struct t20;
extern MethodInfo m25564_MI;
struct t20;
#define m25564(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.InteropServices.TypeLibVersionAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1414_0_0_0;
static ParameterInfo t20_m25564_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1414_0_0_0},
};
extern TypeInfo t1414_TI;
static Il2CppRGCTXData m25564_RGCTXData[1] = 
{
	&t1414_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25564_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25564_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25564_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25564_RGCTXData, NULL, &m25564_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.InteropServices.TypeLibVersionAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1414_1_0_0;
static ParameterInfo t20_m25563_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1414_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25563_GM;
extern void* RuntimeInvoker_t21_t44_t4932 (MethodInfo* method, void* obj, void** args);
MethodInfo m25563_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4932, t20_m25563_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25563_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3419.h"
extern TypeInfo t3419_TI;
#include "t3419MD.h"
extern MethodInfo m18965_MI;
extern MethodInfo m25565_MI;
struct t20;
#define m25565(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.InteropServices.TypeLibVersionAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4933_TI;
extern TypeInfo t3419_TI;
static Il2CppRGCTXData m25565_RGCTXData[2] = 
{
	&t3419_TI/* Class Usage */,
	&m18965_MI/* Method Usage */,
};
extern Il2CppType t4933_0_0_0;
extern Il2CppGenericMethod m25565_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25565_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4933_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25565_RGCTXData, NULL, &m25565_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1156.h"
extern MethodInfo m25566_MI;
struct t20;
extern MethodInfo m25567_MI;
struct t20;
 int32_t m25567 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25567 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.InteropServices.UnmanagedType>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1156_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25567_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1156_0_0_0;
extern Il2CppType t1156_0_0_0;
extern Il2CppGenericMethod m25567_GM;
extern void* RuntimeInvoker_t1156_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25567_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25567, &t20_TI, &t1156_0_0_0, RuntimeInvoker_t1156_t44, t20_m25567_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25567_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.InteropServices.UnmanagedType>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1156_1_0_2;
extern Il2CppType t1156_1_0_0;
static ParameterInfo t20_m25566_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1156_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25566_GM;
extern void* RuntimeInvoker_t21_t44_t4934 (MethodInfo* method, void* obj, void** args);
MethodInfo m25566_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4934, t20_m25566_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25566_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25568_MI;
struct t20;
 void m25568 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25568 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.InteropServices.UnmanagedType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1156_0_0_0;
extern Il2CppType t1156_0_0_0;
static ParameterInfo t20_m25568_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25568_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25568_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25568, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25568_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25568_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25569_MI;
struct t20;
 bool m25569 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25569 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1156_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1156_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1156_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1156_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.InteropServices.UnmanagedType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1156_0_0_0;
static ParameterInfo t20_m25569_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1156_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25569_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25569_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25569, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25569_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25569_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25570_MI;
struct t20;
 void m25570 (t20 * __this, t3675* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25570 (t20 * __this, t3675* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.InteropServices.UnmanagedType>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3675_0_0_0;
extern Il2CppType t3675_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25570_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3675_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25570_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25570_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25570, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25570_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25570_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25571_MI;
struct t20;
 bool m25571 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25571 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.InteropServices.UnmanagedType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1156_0_0_0;
static ParameterInfo t20_m25571_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1156_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25571_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25571_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25571, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25571_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25571_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25572_MI;
struct t20;
 int32_t m25572 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25572 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1156_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1156_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1156_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1156_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.InteropServices.UnmanagedType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1156_0_0_0;
static ParameterInfo t20_m25572_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1156_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25572_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25572_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25572, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25572_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25572_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25573_MI;
struct t20;
 void m25573 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25573 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.InteropServices.UnmanagedType>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1156_0_0_0;
static ParameterInfo t20_m25573_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25573_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25573_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25573, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25573_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25573_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25574_MI;
struct t20;
extern MethodInfo m25575_MI;
struct t20;
 void m25575 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25575 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1156_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.InteropServices.UnmanagedType>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1156_0_0_0;
static ParameterInfo t20_m25575_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1156_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25575_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25575_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25575, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25575_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25575_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.InteropServices.UnmanagedType>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1156_1_0_0;
static ParameterInfo t20_m25574_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1156_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25574_GM;
extern void* RuntimeInvoker_t21_t44_t4934 (MethodInfo* method, void* obj, void** args);
MethodInfo m25574_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4934, t20_m25574_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25574_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3420.h"
extern TypeInfo t3420_TI;
#include "t3420MD.h"
extern MethodInfo m18970_MI;
extern MethodInfo m25576_MI;
struct t20;
 t29* m25576 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25576 (t20 * __this, MethodInfo* method){
	{
		t3420  L_0 = {0};
		m18970(&L_0, __this, &m18970_MI);
		t3420  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3420_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.InteropServices.UnmanagedType>()
extern TypeInfo t20_TI;
extern TypeInfo t4935_TI;
extern Il2CppType t4935_0_0_0;
extern Il2CppGenericMethod m25576_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25576_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25576, &t20_TI, &t4935_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25576_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1422.h"
extern MethodInfo m25577_MI;
struct t20;
extern MethodInfo m25578_MI;
struct t20;
#define m25578(__this, p0, method) (t1422 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Remoting.Activation.UrlAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1422_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25578_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1422_0_0_0;
extern Il2CppType t1422_0_0_0;
extern Il2CppGenericMethod m25578_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25578_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1422_0_0_0, RuntimeInvoker_t29_t44, t20_m25578_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25578_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Remoting.Activation.UrlAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1422_1_0_2;
extern Il2CppType t1422_1_0_0;
static ParameterInfo t20_m25577_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1422_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25577_GM;
extern void* RuntimeInvoker_t21_t44_t4936 (MethodInfo* method, void* obj, void** args);
MethodInfo m25577_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4936, t20_m25577_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25577_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25579_MI;
struct t20;
#define m25579(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Remoting.Activation.UrlAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1422_0_0_0;
extern Il2CppType t1422_0_0_0;
static ParameterInfo t20_m25579_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1422_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25579_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25579_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25579_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25579_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25580_MI;
struct t20;
#define m25580(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Remoting.Activation.UrlAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1422_0_0_0;
static ParameterInfo t20_m25580_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1422_0_0_0},
};
extern TypeInfo t1422_TI;
static Il2CppRGCTXData m25580_RGCTXData[1] = 
{
	&t1422_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25580_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25580_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25580_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25580_RGCTXData, NULL, &m25580_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25581_MI;
struct t20;
#define m25581(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Remoting.Activation.UrlAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3676_0_0_0;
extern Il2CppType t3676_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25581_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3676_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25581_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25581_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25581_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25581_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25582_MI;
struct t20;
#define m25582(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Remoting.Activation.UrlAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1422_0_0_0;
static ParameterInfo t20_m25582_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1422_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25582_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25582_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25582_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25582_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25583_MI;
struct t20;
#define m25583(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Remoting.Activation.UrlAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1422_0_0_0;
static ParameterInfo t20_m25583_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1422_0_0_0},
};
extern TypeInfo t1422_TI;
static Il2CppRGCTXData m25583_RGCTXData[1] = 
{
	&t1422_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25583_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25583_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25583_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25583_RGCTXData, NULL, &m25583_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25584_MI;
struct t20;
#define m25584(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Remoting.Activation.UrlAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1422_0_0_0;
static ParameterInfo t20_m25584_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1422_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25584_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25584_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25584_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25584_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25585_MI;
struct t20;
extern MethodInfo m25586_MI;
struct t20;
#define m25586(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Remoting.Activation.UrlAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1422_0_0_0;
static ParameterInfo t20_m25586_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1422_0_0_0},
};
extern TypeInfo t1422_TI;
static Il2CppRGCTXData m25586_RGCTXData[1] = 
{
	&t1422_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25586_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25586_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25586_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25586_RGCTXData, NULL, &m25586_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Remoting.Activation.UrlAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1422_1_0_0;
static ParameterInfo t20_m25585_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1422_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25585_GM;
extern void* RuntimeInvoker_t21_t44_t4936 (MethodInfo* method, void* obj, void** args);
MethodInfo m25585_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4936, t20_m25585_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25585_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3421.h"
extern TypeInfo t3421_TI;
#include "t3421MD.h"
extern MethodInfo m18975_MI;
extern MethodInfo m25587_MI;
struct t20;
#define m25587(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Remoting.Activation.UrlAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4937_TI;
extern TypeInfo t3421_TI;
static Il2CppRGCTXData m25587_RGCTXData[2] = 
{
	&t3421_TI/* Class Usage */,
	&m18975_MI/* Method Usage */,
};
extern Il2CppType t4937_0_0_0;
extern Il2CppGenericMethod m25587_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25587_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4937_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25587_RGCTXData, NULL, &m25587_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1423.h"
extern MethodInfo m25588_MI;
struct t20;
extern MethodInfo m25589_MI;
struct t20;
#define m25589(__this, p0, method) (t1423 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.ContextAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1423_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25589_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1423_0_0_0;
extern Il2CppType t1423_0_0_0;
extern Il2CppGenericMethod m25589_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25589_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1423_0_0_0, RuntimeInvoker_t29_t44, t20_m25589_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25589_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Remoting.Contexts.ContextAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1423_1_0_2;
extern Il2CppType t1423_1_0_0;
static ParameterInfo t20_m25588_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1423_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25588_GM;
extern void* RuntimeInvoker_t21_t44_t4938 (MethodInfo* method, void* obj, void** args);
MethodInfo m25588_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4938, t20_m25588_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25588_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25590_MI;
struct t20;
#define m25590(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Remoting.Contexts.ContextAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1423_0_0_0;
extern Il2CppType t1423_0_0_0;
static ParameterInfo t20_m25590_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1423_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25590_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25590_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25590_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25590_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25591_MI;
struct t20;
#define m25591(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Remoting.Contexts.ContextAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1423_0_0_0;
static ParameterInfo t20_m25591_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1423_0_0_0},
};
extern TypeInfo t1423_TI;
static Il2CppRGCTXData m25591_RGCTXData[1] = 
{
	&t1423_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25591_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25591_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25591_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25591_RGCTXData, NULL, &m25591_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25592_MI;
struct t20;
#define m25592(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Remoting.Contexts.ContextAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3677_0_0_0;
extern Il2CppType t3677_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25592_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3677_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25592_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25592_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25592_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25592_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25593_MI;
struct t20;
#define m25593(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Remoting.Contexts.ContextAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1423_0_0_0;
static ParameterInfo t20_m25593_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1423_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25593_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25593_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25593_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25593_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25594_MI;
struct t20;
#define m25594(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Remoting.Contexts.ContextAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1423_0_0_0;
static ParameterInfo t20_m25594_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1423_0_0_0},
};
extern TypeInfo t1423_TI;
static Il2CppRGCTXData m25594_RGCTXData[1] = 
{
	&t1423_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25594_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25594_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25594_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25594_RGCTXData, NULL, &m25594_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25595_MI;
struct t20;
#define m25595(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Remoting.Contexts.ContextAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1423_0_0_0;
static ParameterInfo t20_m25595_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1423_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25595_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25595_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25595_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25595_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25596_MI;
struct t20;
extern MethodInfo m25597_MI;
struct t20;
#define m25597(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Remoting.Contexts.ContextAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1423_0_0_0;
static ParameterInfo t20_m25597_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1423_0_0_0},
};
extern TypeInfo t1423_TI;
static Il2CppRGCTXData m25597_RGCTXData[1] = 
{
	&t1423_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25597_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25597_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25597_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25597_RGCTXData, NULL, &m25597_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Remoting.Contexts.ContextAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1423_1_0_0;
static ParameterInfo t20_m25596_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1423_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25596_GM;
extern void* RuntimeInvoker_t21_t44_t4938 (MethodInfo* method, void* obj, void** args);
MethodInfo m25596_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4938, t20_m25596_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25596_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3422.h"
extern TypeInfo t3422_TI;
#include "t3422MD.h"
extern MethodInfo m18980_MI;
extern MethodInfo m25598_MI;
struct t20;
#define m25598(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Remoting.Contexts.ContextAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4939_TI;
extern TypeInfo t3422_TI;
static Il2CppRGCTXData m25598_RGCTXData[2] = 
{
	&t3422_TI/* Class Usage */,
	&m18980_MI/* Method Usage */,
};
extern Il2CppType t4939_0_0_0;
extern Il2CppGenericMethod m25598_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25598_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4939_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25598_RGCTXData, NULL, &m25598_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25599_MI;
struct t20;
extern MethodInfo m25600_MI;
struct t20;
#define m25600(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.IContextAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t2040_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25600_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t2040_0_0_0;
extern Il2CppType t2040_0_0_0;
extern Il2CppGenericMethod m25600_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25600_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t2040_0_0_0, RuntimeInvoker_t29_t44, t20_m25600_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25600_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Remoting.Contexts.IContextAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2040_1_0_2;
extern Il2CppType t2040_1_0_0;
static ParameterInfo t20_m25599_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2040_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25599_GM;
extern void* RuntimeInvoker_t21_t44_t4940 (MethodInfo* method, void* obj, void** args);
MethodInfo m25599_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4940, t20_m25599_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25599_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25601_MI;
struct t20;
#define m25601(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Remoting.Contexts.IContextAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2040_0_0_0;
extern Il2CppType t2040_0_0_0;
static ParameterInfo t20_m25601_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t2040_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25601_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25601_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25601_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25601_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25602_MI;
struct t20;
#define m25602(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Remoting.Contexts.IContextAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2040_0_0_0;
static ParameterInfo t20_m25602_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t2040_0_0_0},
};
extern TypeInfo t2040_TI;
static Il2CppRGCTXData m25602_RGCTXData[1] = 
{
	&t2040_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25602_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25602_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25602_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25602_RGCTXData, NULL, &m25602_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25603_MI;
struct t20;
#define m25603(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Remoting.Contexts.IContextAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t1474_0_0_0;
extern Il2CppType t1474_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25603_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t1474_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25603_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25603_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25603_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25603_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25604_MI;
struct t20;
#define m25604(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Remoting.Contexts.IContextAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2040_0_0_0;
static ParameterInfo t20_m25604_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t2040_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25604_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25604_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25604_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25604_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25605_MI;
struct t20;
#define m25605(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Remoting.Contexts.IContextAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2040_0_0_0;
static ParameterInfo t20_m25605_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t2040_0_0_0},
};
extern TypeInfo t2040_TI;
static Il2CppRGCTXData m25605_RGCTXData[1] = 
{
	&t2040_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25605_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25605_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25605_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25605_RGCTXData, NULL, &m25605_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25606_MI;
struct t20;
#define m25606(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Remoting.Contexts.IContextAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2040_0_0_0;
static ParameterInfo t20_m25606_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t2040_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25606_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25606_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25606_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25606_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25607_MI;
struct t20;
extern MethodInfo m25608_MI;
struct t20;
#define m25608(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Remoting.Contexts.IContextAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2040_0_0_0;
static ParameterInfo t20_m25608_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t2040_0_0_0},
};
extern TypeInfo t2040_TI;
static Il2CppRGCTXData m25608_RGCTXData[1] = 
{
	&t2040_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25608_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25608_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25608_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25608_RGCTXData, NULL, &m25608_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Remoting.Contexts.IContextAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2040_1_0_0;
static ParameterInfo t20_m25607_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2040_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25607_GM;
extern void* RuntimeInvoker_t21_t44_t4940 (MethodInfo* method, void* obj, void** args);
MethodInfo m25607_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4940, t20_m25607_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25607_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3423.h"
extern TypeInfo t3423_TI;
#include "t3423MD.h"
extern MethodInfo m18985_MI;
extern MethodInfo m25609_MI;
struct t20;
#define m25609(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Remoting.Contexts.IContextAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4941_TI;
extern TypeInfo t3423_TI;
static Il2CppRGCTXData m25609_RGCTXData[2] = 
{
	&t3423_TI/* Class Usage */,
	&m18985_MI/* Method Usage */,
};
extern Il2CppType t4941_0_0_0;
extern Il2CppGenericMethod m25609_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25609_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4941_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25609_RGCTXData, NULL, &m25609_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25610_MI;
struct t20;
extern MethodInfo m25611_MI;
struct t20;
#define m25611(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.IContextProperty>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1433_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25611_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1433_0_0_0;
extern Il2CppType t1433_0_0_0;
extern Il2CppGenericMethod m25611_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25611_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1433_0_0_0, RuntimeInvoker_t29_t44, t20_m25611_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25611_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Remoting.Contexts.IContextProperty>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1433_1_0_2;
extern Il2CppType t1433_1_0_0;
static ParameterInfo t20_m25610_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1433_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25610_GM;
extern void* RuntimeInvoker_t21_t44_t4942 (MethodInfo* method, void* obj, void** args);
MethodInfo m25610_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4942, t20_m25610_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25610_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25612_MI;
struct t20;
#define m25612(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Remoting.Contexts.IContextProperty>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1433_0_0_0;
extern Il2CppType t1433_0_0_0;
static ParameterInfo t20_m25612_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1433_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25612_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25612_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25612_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25612_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25613_MI;
struct t20;
#define m25613(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Remoting.Contexts.IContextProperty>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1433_0_0_0;
static ParameterInfo t20_m25613_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1433_0_0_0},
};
extern TypeInfo t1433_TI;
static Il2CppRGCTXData m25613_RGCTXData[1] = 
{
	&t1433_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25613_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25613_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25613_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25613_RGCTXData, NULL, &m25613_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25614_MI;
struct t20;
#define m25614(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Remoting.Contexts.IContextProperty>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3678_0_0_0;
extern Il2CppType t3678_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25614_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3678_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25614_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25614_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25614_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25614_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25615_MI;
struct t20;
#define m25615(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Remoting.Contexts.IContextProperty>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1433_0_0_0;
static ParameterInfo t20_m25615_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1433_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25615_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25615_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25615_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25615_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25616_MI;
struct t20;
#define m25616(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Remoting.Contexts.IContextProperty>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1433_0_0_0;
static ParameterInfo t20_m25616_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1433_0_0_0},
};
extern TypeInfo t1433_TI;
static Il2CppRGCTXData m25616_RGCTXData[1] = 
{
	&t1433_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25616_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25616_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25616_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25616_RGCTXData, NULL, &m25616_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25617_MI;
struct t20;
#define m25617(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Remoting.Contexts.IContextProperty>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1433_0_0_0;
static ParameterInfo t20_m25617_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1433_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25617_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25617_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25617_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25617_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25618_MI;
struct t20;
extern MethodInfo m25619_MI;
struct t20;
#define m25619(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Remoting.Contexts.IContextProperty>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1433_0_0_0;
static ParameterInfo t20_m25619_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1433_0_0_0},
};
extern TypeInfo t1433_TI;
static Il2CppRGCTXData m25619_RGCTXData[1] = 
{
	&t1433_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25619_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25619_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25619_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25619_RGCTXData, NULL, &m25619_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Remoting.Contexts.IContextProperty>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1433_1_0_0;
static ParameterInfo t20_m25618_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1433_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25618_GM;
extern void* RuntimeInvoker_t21_t44_t4942 (MethodInfo* method, void* obj, void** args);
MethodInfo m25618_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4942, t20_m25618_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25618_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3424.h"
extern TypeInfo t3424_TI;
#include "t3424MD.h"
extern MethodInfo m18990_MI;
extern MethodInfo m25620_MI;
struct t20;
#define m25620(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Remoting.Contexts.IContextProperty>()
extern TypeInfo t20_TI;
extern TypeInfo t4943_TI;
extern TypeInfo t3424_TI;
static Il2CppRGCTXData m25620_RGCTXData[2] = 
{
	&t3424_TI/* Class Usage */,
	&m18990_MI/* Method Usage */,
};
extern Il2CppType t4943_0_0_0;
extern Il2CppGenericMethod m25620_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25620_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4943_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25620_RGCTXData, NULL, &m25620_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1434.h"
extern MethodInfo m25621_MI;
struct t20;
extern MethodInfo m25622_MI;
struct t20;
#define m25622(__this, p0, method) (t1434 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.SynchronizationAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1434_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25622_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1434_0_0_0;
extern Il2CppType t1434_0_0_0;
extern Il2CppGenericMethod m25622_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25622_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1434_0_0_0, RuntimeInvoker_t29_t44, t20_m25622_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25622_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Remoting.Contexts.SynchronizationAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1434_1_0_2;
extern Il2CppType t1434_1_0_0;
static ParameterInfo t20_m25621_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1434_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25621_GM;
extern void* RuntimeInvoker_t21_t44_t4944 (MethodInfo* method, void* obj, void** args);
MethodInfo m25621_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4944, t20_m25621_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25621_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25623_MI;
struct t20;
#define m25623(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Remoting.Contexts.SynchronizationAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1434_0_0_0;
extern Il2CppType t1434_0_0_0;
static ParameterInfo t20_m25623_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1434_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25623_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25623_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25623_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25623_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25624_MI;
struct t20;
#define m25624(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Remoting.Contexts.SynchronizationAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1434_0_0_0;
static ParameterInfo t20_m25624_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1434_0_0_0},
};
extern TypeInfo t1434_TI;
static Il2CppRGCTXData m25624_RGCTXData[1] = 
{
	&t1434_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25624_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25624_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25624_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25624_RGCTXData, NULL, &m25624_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25625_MI;
struct t20;
#define m25625(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Remoting.Contexts.SynchronizationAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3679_0_0_0;
extern Il2CppType t3679_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25625_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3679_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25625_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25625_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25625_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25625_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25626_MI;
struct t20;
#define m25626(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Remoting.Contexts.SynchronizationAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1434_0_0_0;
static ParameterInfo t20_m25626_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1434_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25626_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25626_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25626_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25626_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25627_MI;
struct t20;
#define m25627(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Remoting.Contexts.SynchronizationAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1434_0_0_0;
static ParameterInfo t20_m25627_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1434_0_0_0},
};
extern TypeInfo t1434_TI;
static Il2CppRGCTXData m25627_RGCTXData[1] = 
{
	&t1434_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25627_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25627_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25627_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25627_RGCTXData, NULL, &m25627_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25628_MI;
struct t20;
#define m25628(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Remoting.Contexts.SynchronizationAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1434_0_0_0;
static ParameterInfo t20_m25628_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1434_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25628_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25628_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25628_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25628_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25629_MI;
struct t20;
extern MethodInfo m25630_MI;
struct t20;
#define m25630(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Remoting.Contexts.SynchronizationAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1434_0_0_0;
static ParameterInfo t20_m25630_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1434_0_0_0},
};
extern TypeInfo t1434_TI;
static Il2CppRGCTXData m25630_RGCTXData[1] = 
{
	&t1434_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25630_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25630_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25630_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25630_RGCTXData, NULL, &m25630_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Remoting.Contexts.SynchronizationAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1434_1_0_0;
static ParameterInfo t20_m25629_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1434_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25629_GM;
extern void* RuntimeInvoker_t21_t44_t4944 (MethodInfo* method, void* obj, void** args);
MethodInfo m25629_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4944, t20_m25629_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25629_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3425.h"
extern TypeInfo t3425_TI;
#include "t3425MD.h"
extern MethodInfo m18995_MI;
extern MethodInfo m25631_MI;
struct t20;
#define m25631(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Remoting.Contexts.SynchronizationAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4945_TI;
extern TypeInfo t3425_TI;
static Il2CppRGCTXData m25631_RGCTXData[2] = 
{
	&t3425_TI/* Class Usage */,
	&m18995_MI/* Method Usage */,
};
extern Il2CppType t4945_0_0_0;
extern Il2CppGenericMethod m25631_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25631_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4945_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25631_RGCTXData, NULL, &m25631_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25632_MI;
struct t20;
extern MethodInfo m25633_MI;
struct t20;
#define m25633(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.IContributeClientContextSink>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t2044_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25633_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t2044_0_0_0;
extern Il2CppType t2044_0_0_0;
extern Il2CppGenericMethod m25633_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25633_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t2044_0_0_0, RuntimeInvoker_t29_t44, t20_m25633_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25633_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Remoting.Contexts.IContributeClientContextSink>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2044_1_0_2;
extern Il2CppType t2044_1_0_0;
static ParameterInfo t20_m25632_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2044_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25632_GM;
extern void* RuntimeInvoker_t21_t44_t4946 (MethodInfo* method, void* obj, void** args);
MethodInfo m25632_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4946, t20_m25632_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25632_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25634_MI;
struct t20;
#define m25634(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Remoting.Contexts.IContributeClientContextSink>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2044_0_0_0;
extern Il2CppType t2044_0_0_0;
static ParameterInfo t20_m25634_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t2044_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25634_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25634_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25634_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25634_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25635_MI;
struct t20;
#define m25635(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Remoting.Contexts.IContributeClientContextSink>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2044_0_0_0;
static ParameterInfo t20_m25635_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t2044_0_0_0},
};
extern TypeInfo t2044_TI;
static Il2CppRGCTXData m25635_RGCTXData[1] = 
{
	&t2044_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25635_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25635_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25635_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25635_RGCTXData, NULL, &m25635_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25636_MI;
struct t20;
#define m25636(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Remoting.Contexts.IContributeClientContextSink>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3680_0_0_0;
extern Il2CppType t3680_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25636_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3680_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25636_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25636_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25636_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25636_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25637_MI;
struct t20;
#define m25637(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Remoting.Contexts.IContributeClientContextSink>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2044_0_0_0;
static ParameterInfo t20_m25637_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t2044_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25637_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25637_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25637_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25637_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25638_MI;
struct t20;
#define m25638(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Remoting.Contexts.IContributeClientContextSink>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2044_0_0_0;
static ParameterInfo t20_m25638_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t2044_0_0_0},
};
extern TypeInfo t2044_TI;
static Il2CppRGCTXData m25638_RGCTXData[1] = 
{
	&t2044_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25638_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25638_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25638_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25638_RGCTXData, NULL, &m25638_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25639_MI;
struct t20;
#define m25639(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Remoting.Contexts.IContributeClientContextSink>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2044_0_0_0;
static ParameterInfo t20_m25639_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t2044_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25639_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25639_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25639_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25639_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25640_MI;
struct t20;
extern MethodInfo m25641_MI;
struct t20;
#define m25641(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Remoting.Contexts.IContributeClientContextSink>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2044_0_0_0;
static ParameterInfo t20_m25641_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t2044_0_0_0},
};
extern TypeInfo t2044_TI;
static Il2CppRGCTXData m25641_RGCTXData[1] = 
{
	&t2044_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25641_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25641_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25641_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25641_RGCTXData, NULL, &m25641_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Remoting.Contexts.IContributeClientContextSink>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2044_1_0_0;
static ParameterInfo t20_m25640_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2044_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25640_GM;
extern void* RuntimeInvoker_t21_t44_t4946 (MethodInfo* method, void* obj, void** args);
MethodInfo m25640_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4946, t20_m25640_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25640_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3426.h"
extern TypeInfo t3426_TI;
#include "t3426MD.h"
extern MethodInfo m19000_MI;
extern MethodInfo m25642_MI;
struct t20;
#define m25642(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Remoting.Contexts.IContributeClientContextSink>()
extern TypeInfo t20_TI;
extern TypeInfo t4947_TI;
extern TypeInfo t3426_TI;
static Il2CppRGCTXData m25642_RGCTXData[2] = 
{
	&t3426_TI/* Class Usage */,
	&m19000_MI/* Method Usage */,
};
extern Il2CppType t4947_0_0_0;
extern Il2CppGenericMethod m25642_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25642_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4947_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25642_RGCTXData, NULL, &m25642_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25643_MI;
struct t20;
extern MethodInfo m25644_MI;
struct t20;
#define m25644(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Remoting.Contexts.IContributeServerContextSink>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t2045_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25644_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t2045_0_0_0;
extern Il2CppType t2045_0_0_0;
extern Il2CppGenericMethod m25644_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25644_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t2045_0_0_0, RuntimeInvoker_t29_t44, t20_m25644_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25644_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Remoting.Contexts.IContributeServerContextSink>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2045_1_0_2;
extern Il2CppType t2045_1_0_0;
static ParameterInfo t20_m25643_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2045_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25643_GM;
extern void* RuntimeInvoker_t21_t44_t4948 (MethodInfo* method, void* obj, void** args);
MethodInfo m25643_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4948, t20_m25643_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25643_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25645_MI;
struct t20;
#define m25645(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Remoting.Contexts.IContributeServerContextSink>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2045_0_0_0;
extern Il2CppType t2045_0_0_0;
static ParameterInfo t20_m25645_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t2045_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25645_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25645_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25645_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25645_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25646_MI;
struct t20;
#define m25646(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Remoting.Contexts.IContributeServerContextSink>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2045_0_0_0;
static ParameterInfo t20_m25646_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t2045_0_0_0},
};
extern TypeInfo t2045_TI;
static Il2CppRGCTXData m25646_RGCTXData[1] = 
{
	&t2045_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25646_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25646_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25646_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25646_RGCTXData, NULL, &m25646_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25647_MI;
struct t20;
#define m25647(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Remoting.Contexts.IContributeServerContextSink>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3681_0_0_0;
extern Il2CppType t3681_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25647_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3681_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25647_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25647_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25647_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25647_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25648_MI;
struct t20;
#define m25648(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Remoting.Contexts.IContributeServerContextSink>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2045_0_0_0;
static ParameterInfo t20_m25648_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t2045_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25648_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25648_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25648_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25648_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25649_MI;
struct t20;
#define m25649(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Remoting.Contexts.IContributeServerContextSink>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2045_0_0_0;
static ParameterInfo t20_m25649_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t2045_0_0_0},
};
extern TypeInfo t2045_TI;
static Il2CppRGCTXData m25649_RGCTXData[1] = 
{
	&t2045_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25649_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25649_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25649_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25649_RGCTXData, NULL, &m25649_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25650_MI;
struct t20;
#define m25650(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Remoting.Contexts.IContributeServerContextSink>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2045_0_0_0;
static ParameterInfo t20_m25650_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t2045_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25650_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25650_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25650_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25650_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25651_MI;
struct t20;
extern MethodInfo m25652_MI;
struct t20;
#define m25652(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Remoting.Contexts.IContributeServerContextSink>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2045_0_0_0;
static ParameterInfo t20_m25652_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t2045_0_0_0},
};
extern TypeInfo t2045_TI;
static Il2CppRGCTXData m25652_RGCTXData[1] = 
{
	&t2045_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25652_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25652_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25652_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25652_RGCTXData, NULL, &m25652_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Remoting.Contexts.IContributeServerContextSink>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2045_1_0_0;
static ParameterInfo t20_m25651_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2045_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25651_GM;
extern void* RuntimeInvoker_t21_t44_t4948 (MethodInfo* method, void* obj, void** args);
MethodInfo m25651_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4948, t20_m25651_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25651_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3427.h"
extern TypeInfo t3427_TI;
#include "t3427MD.h"
extern MethodInfo m19005_MI;
extern MethodInfo m25653_MI;
struct t20;
#define m25653(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Remoting.Contexts.IContributeServerContextSink>()
extern TypeInfo t20_TI;
extern TypeInfo t4949_TI;
extern TypeInfo t3427_TI;
static Il2CppRGCTXData m25653_RGCTXData[2] = 
{
	&t3427_TI/* Class Usage */,
	&m19005_MI/* Method Usage */,
};
extern Il2CppType t4949_0_0_0;
extern Il2CppGenericMethod m25653_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25653_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4949_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25653_RGCTXData, NULL, &m25653_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1437.h"
extern MethodInfo m25654_MI;
struct t20;
extern MethodInfo m25655_MI;
struct t20;
 uint8_t m25655 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m25655 (t20 * __this, int32_t p0, MethodInfo* method){
	uint8_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Remoting.Messaging.ArgInfoType>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1437_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25655_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1437_0_0_0;
extern Il2CppType t1437_0_0_0;
extern Il2CppGenericMethod m25655_GM;
extern void* RuntimeInvoker_t1437_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25655_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25655, &t20_TI, &t1437_0_0_0, RuntimeInvoker_t1437_t44, t20_m25655_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25655_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Remoting.Messaging.ArgInfoType>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1437_1_0_2;
extern Il2CppType t1437_1_0_0;
static ParameterInfo t20_m25654_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1437_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25654_GM;
extern void* RuntimeInvoker_t21_t44_t4950 (MethodInfo* method, void* obj, void** args);
MethodInfo m25654_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4950, t20_m25654_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25654_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25656_MI;
struct t20;
 void m25656 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25656 (t20 * __this, uint8_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Remoting.Messaging.ArgInfoType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1437_0_0_0;
extern Il2CppType t1437_0_0_0;
static ParameterInfo t20_m25656_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25656_GM;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25656_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25656, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t20_m25656_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25656_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25657_MI;
struct t20;
 bool m25657 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25657 (t20 * __this, uint8_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1437_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		uint8_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1437_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		uint8_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1437_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1437_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Remoting.Messaging.ArgInfoType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1437_0_0_0;
static ParameterInfo t20_m25657_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25657_GM;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25657_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25657, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t20_m25657_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25657_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25658_MI;
struct t20;
 void m25658 (t20 * __this, t3682* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25658 (t20 * __this, t3682* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Remoting.Messaging.ArgInfoType>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3682_0_0_0;
extern Il2CppType t3682_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25658_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3682_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25658_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25658_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25658, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25658_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25658_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25659_MI;
struct t20;
 bool m25659 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25659 (t20 * __this, uint8_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Remoting.Messaging.ArgInfoType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1437_0_0_0;
static ParameterInfo t20_m25659_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25659_GM;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25659_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25659, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t20_m25659_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25659_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25660_MI;
struct t20;
 int32_t m25660 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25660 (t20 * __this, uint8_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1437_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		uint8_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1437_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		uint8_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1437_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1437_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Remoting.Messaging.ArgInfoType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1437_0_0_0;
static ParameterInfo t20_m25660_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25660_GM;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25660_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25660, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t20_m25660_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25660_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25661_MI;
struct t20;
 void m25661 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25661 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Remoting.Messaging.ArgInfoType>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1437_0_0_0;
static ParameterInfo t20_m25661_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25661_GM;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25661_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25661, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t20_m25661_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25661_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25662_MI;
struct t20;
extern MethodInfo m25663_MI;
struct t20;
 void m25663 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25663 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		uint8_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1437_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Remoting.Messaging.ArgInfoType>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1437_0_0_0;
static ParameterInfo t20_m25663_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1437_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25663_GM;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25663_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25663, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t20_m25663_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25663_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Remoting.Messaging.ArgInfoType>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1437_1_0_0;
static ParameterInfo t20_m25662_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1437_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25662_GM;
extern void* RuntimeInvoker_t21_t44_t4950 (MethodInfo* method, void* obj, void** args);
MethodInfo m25662_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4950, t20_m25662_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25662_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3428.h"
extern TypeInfo t3428_TI;
#include "t3428MD.h"
extern MethodInfo m19010_MI;
extern MethodInfo m25664_MI;
struct t20;
 t29* m25664 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25664 (t20 * __this, MethodInfo* method){
	{
		t3428  L_0 = {0};
		m19010(&L_0, __this, &m19010_MI);
		t3428  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3428_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Remoting.Messaging.ArgInfoType>()
extern TypeInfo t20_TI;
extern TypeInfo t4951_TI;
extern Il2CppType t4951_0_0_0;
extern Il2CppGenericMethod m25664_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25664_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25664, &t20_TI, &t4951_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25664_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1448.h"
extern MethodInfo m25665_MI;
struct t20;
extern MethodInfo m25666_MI;
struct t20;
#define m25666(__this, p0, method) (t1448 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Remoting.Messaging.Header>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1448_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25666_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1448_0_0_0;
extern Il2CppType t1448_0_0_0;
extern Il2CppGenericMethod m25666_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25666_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1448_0_0_0, RuntimeInvoker_t29_t44, t20_m25666_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25666_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Remoting.Messaging.Header>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1448_1_0_2;
extern Il2CppType t1448_1_0_0;
static ParameterInfo t20_m25665_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1448_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25665_GM;
extern void* RuntimeInvoker_t21_t44_t4952 (MethodInfo* method, void* obj, void** args);
MethodInfo m25665_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4952, t20_m25665_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25665_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25667_MI;
struct t20;
#define m25667(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Remoting.Messaging.Header>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1448_0_0_0;
extern Il2CppType t1448_0_0_0;
static ParameterInfo t20_m25667_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1448_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25667_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25667_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25667_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25667_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25668_MI;
struct t20;
#define m25668(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Remoting.Messaging.Header>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1448_0_0_0;
static ParameterInfo t20_m25668_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1448_0_0_0},
};
extern TypeInfo t1448_TI;
static Il2CppRGCTXData m25668_RGCTXData[1] = 
{
	&t1448_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25668_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25668_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25668_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25668_RGCTXData, NULL, &m25668_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25669_MI;
struct t20;
#define m25669(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Remoting.Messaging.Header>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t1451_0_0_0;
extern Il2CppType t1451_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25669_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t1451_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25669_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25669_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25669_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25669_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25670_MI;
struct t20;
#define m25670(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Remoting.Messaging.Header>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1448_0_0_0;
static ParameterInfo t20_m25670_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1448_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25670_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25670_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25670_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25670_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25671_MI;
struct t20;
#define m25671(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Remoting.Messaging.Header>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1448_0_0_0;
static ParameterInfo t20_m25671_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1448_0_0_0},
};
extern TypeInfo t1448_TI;
static Il2CppRGCTXData m25671_RGCTXData[1] = 
{
	&t1448_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25671_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25671_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25671_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25671_RGCTXData, NULL, &m25671_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25672_MI;
struct t20;
#define m25672(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Remoting.Messaging.Header>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1448_0_0_0;
static ParameterInfo t20_m25672_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1448_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25672_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25672_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25672_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25672_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25673_MI;
struct t20;
extern MethodInfo m25674_MI;
struct t20;
#define m25674(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Remoting.Messaging.Header>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1448_0_0_0;
static ParameterInfo t20_m25674_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1448_0_0_0},
};
extern TypeInfo t1448_TI;
static Il2CppRGCTXData m25674_RGCTXData[1] = 
{
	&t1448_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25674_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25674_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25674_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25674_RGCTXData, NULL, &m25674_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Remoting.Messaging.Header>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1448_1_0_0;
static ParameterInfo t20_m25673_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1448_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25673_GM;
extern void* RuntimeInvoker_t21_t44_t4952 (MethodInfo* method, void* obj, void** args);
MethodInfo m25673_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4952, t20_m25673_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25673_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3429.h"
extern TypeInfo t3429_TI;
#include "t3429MD.h"
extern MethodInfo m19015_MI;
extern MethodInfo m25675_MI;
struct t20;
#define m25675(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Remoting.Messaging.Header>()
extern TypeInfo t20_TI;
extern TypeInfo t4953_TI;
extern TypeInfo t3429_TI;
static Il2CppRGCTXData m25675_RGCTXData[2] = 
{
	&t3429_TI/* Class Usage */,
	&m19015_MI/* Method Usage */,
};
extern Il2CppType t4953_0_0_0;
extern Il2CppGenericMethod m25675_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25675_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4953_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25675_RGCTXData, NULL, &m25675_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1464.h"
extern MethodInfo m25676_MI;
struct t20;
extern MethodInfo m25677_MI;
struct t20;
#define m25677(__this, p0, method) (t1464 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Remoting.Proxies.ProxyAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1464_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25677_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1464_0_0_0;
extern Il2CppType t1464_0_0_0;
extern Il2CppGenericMethod m25677_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25677_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1464_0_0_0, RuntimeInvoker_t29_t44, t20_m25677_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25677_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Remoting.Proxies.ProxyAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1464_1_0_2;
extern Il2CppType t1464_1_0_0;
static ParameterInfo t20_m25676_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1464_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25676_GM;
extern void* RuntimeInvoker_t21_t44_t4954 (MethodInfo* method, void* obj, void** args);
MethodInfo m25676_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4954, t20_m25676_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25676_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25678_MI;
struct t20;
#define m25678(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Remoting.Proxies.ProxyAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1464_0_0_0;
extern Il2CppType t1464_0_0_0;
static ParameterInfo t20_m25678_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1464_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25678_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25678_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25678_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25678_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25679_MI;
struct t20;
#define m25679(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Remoting.Proxies.ProxyAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1464_0_0_0;
static ParameterInfo t20_m25679_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1464_0_0_0},
};
extern TypeInfo t1464_TI;
static Il2CppRGCTXData m25679_RGCTXData[1] = 
{
	&t1464_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25679_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25679_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25679_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25679_RGCTXData, NULL, &m25679_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25680_MI;
struct t20;
#define m25680(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Remoting.Proxies.ProxyAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3683_0_0_0;
extern Il2CppType t3683_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25680_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3683_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25680_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25680_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25680_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25680_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25681_MI;
struct t20;
#define m25681(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Remoting.Proxies.ProxyAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1464_0_0_0;
static ParameterInfo t20_m25681_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1464_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25681_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25681_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25681_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25681_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25682_MI;
struct t20;
#define m25682(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Remoting.Proxies.ProxyAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1464_0_0_0;
static ParameterInfo t20_m25682_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1464_0_0_0},
};
extern TypeInfo t1464_TI;
static Il2CppRGCTXData m25682_RGCTXData[1] = 
{
	&t1464_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25682_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25682_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25682_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25682_RGCTXData, NULL, &m25682_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25683_MI;
struct t20;
#define m25683(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Remoting.Proxies.ProxyAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1464_0_0_0;
static ParameterInfo t20_m25683_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1464_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25683_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25683_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25683_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25683_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25684_MI;
struct t20;
extern MethodInfo m25685_MI;
struct t20;
#define m25685(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Remoting.Proxies.ProxyAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1464_0_0_0;
static ParameterInfo t20_m25685_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1464_0_0_0},
};
extern TypeInfo t1464_TI;
static Il2CppRGCTXData m25685_RGCTXData[1] = 
{
	&t1464_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25685_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25685_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25685_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25685_RGCTXData, NULL, &m25685_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Remoting.Proxies.ProxyAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1464_1_0_0;
static ParameterInfo t20_m25684_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1464_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25684_GM;
extern void* RuntimeInvoker_t21_t44_t4954 (MethodInfo* method, void* obj, void** args);
MethodInfo m25684_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4954, t20_m25684_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25684_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3430.h"
extern TypeInfo t3430_TI;
#include "t3430MD.h"
extern MethodInfo m19020_MI;
extern MethodInfo m25686_MI;
struct t20;
#define m25686(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Remoting.Proxies.ProxyAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4955_TI;
extern TypeInfo t3430_TI;
static Il2CppRGCTXData m25686_RGCTXData[2] = 
{
	&t3430_TI/* Class Usage */,
	&m19020_MI/* Method Usage */,
};
extern Il2CppType t4955_0_0_0;
extern Il2CppGenericMethod m25686_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25686_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4955_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25686_RGCTXData, NULL, &m25686_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25687_MI;
struct t20;
extern MethodInfo m25688_MI;
struct t20;
#define m25688(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Remoting.Services.ITrackingHandler>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t2051_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25688_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t2051_0_0_0;
extern Il2CppType t2051_0_0_0;
extern Il2CppGenericMethod m25688_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25688_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t2051_0_0_0, RuntimeInvoker_t29_t44, t20_m25688_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25688_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Remoting.Services.ITrackingHandler>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2051_1_0_2;
extern Il2CppType t2051_1_0_0;
static ParameterInfo t20_m25687_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2051_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25687_GM;
extern void* RuntimeInvoker_t21_t44_t4956 (MethodInfo* method, void* obj, void** args);
MethodInfo m25687_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4956, t20_m25687_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25687_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25689_MI;
struct t20;
#define m25689(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Remoting.Services.ITrackingHandler>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2051_0_0_0;
extern Il2CppType t2051_0_0_0;
static ParameterInfo t20_m25689_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t2051_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25689_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25689_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25689_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25689_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25690_MI;
struct t20;
#define m25690(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Remoting.Services.ITrackingHandler>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2051_0_0_0;
static ParameterInfo t20_m25690_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t2051_0_0_0},
};
extern TypeInfo t2051_TI;
static Il2CppRGCTXData m25690_RGCTXData[1] = 
{
	&t2051_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25690_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25690_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25690_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25690_RGCTXData, NULL, &m25690_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25691_MI;
struct t20;
#define m25691(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Remoting.Services.ITrackingHandler>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t2052_0_0_0;
extern Il2CppType t2052_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25691_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t2052_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25691_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25691_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25691_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25691_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25692_MI;
struct t20;
#define m25692(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Remoting.Services.ITrackingHandler>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2051_0_0_0;
static ParameterInfo t20_m25692_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t2051_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25692_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25692_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25692_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25692_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25693_MI;
struct t20;
#define m25693(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Remoting.Services.ITrackingHandler>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2051_0_0_0;
static ParameterInfo t20_m25693_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t2051_0_0_0},
};
extern TypeInfo t2051_TI;
static Il2CppRGCTXData m25693_RGCTXData[1] = 
{
	&t2051_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25693_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25693_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25693_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25693_RGCTXData, NULL, &m25693_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25694_MI;
struct t20;
#define m25694(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Remoting.Services.ITrackingHandler>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2051_0_0_0;
static ParameterInfo t20_m25694_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t2051_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25694_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25694_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25694_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25694_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25695_MI;
struct t20;
extern MethodInfo m25696_MI;
struct t20;
#define m25696(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Remoting.Services.ITrackingHandler>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2051_0_0_0;
static ParameterInfo t20_m25696_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t2051_0_0_0},
};
extern TypeInfo t2051_TI;
static Il2CppRGCTXData m25696_RGCTXData[1] = 
{
	&t2051_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25696_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25696_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25696_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25696_RGCTXData, NULL, &m25696_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Remoting.Services.ITrackingHandler>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2051_1_0_0;
static ParameterInfo t20_m25695_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2051_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25695_GM;
extern void* RuntimeInvoker_t21_t44_t4956 (MethodInfo* method, void* obj, void** args);
MethodInfo m25695_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4956, t20_m25695_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25695_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3431.h"
extern TypeInfo t3431_TI;
#include "t3431MD.h"
extern MethodInfo m19025_MI;
extern MethodInfo m25697_MI;
struct t20;
#define m25697(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Remoting.Services.ITrackingHandler>()
extern TypeInfo t20_TI;
extern TypeInfo t4957_TI;
extern TypeInfo t3431_TI;
static Il2CppRGCTXData m25697_RGCTXData[2] = 
{
	&t3431_TI/* Class Usage */,
	&m19025_MI/* Method Usage */,
};
extern Il2CppType t4957_0_0_0;
extern Il2CppGenericMethod m25697_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25697_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4957_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25697_RGCTXData, NULL, &m25697_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1484.h"
extern MethodInfo m25698_MI;
struct t20;
extern MethodInfo m25699_MI;
struct t20;
 int32_t m25699 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25699 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Remoting.WellKnownObjectMode>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1484_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25699_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1484_0_0_0;
extern Il2CppType t1484_0_0_0;
extern Il2CppGenericMethod m25699_GM;
extern void* RuntimeInvoker_t1484_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25699_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25699, &t20_TI, &t1484_0_0_0, RuntimeInvoker_t1484_t44, t20_m25699_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25699_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Remoting.WellKnownObjectMode>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1484_1_0_2;
extern Il2CppType t1484_1_0_0;
static ParameterInfo t20_m25698_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1484_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25698_GM;
extern void* RuntimeInvoker_t21_t44_t4958 (MethodInfo* method, void* obj, void** args);
MethodInfo m25698_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4958, t20_m25698_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25698_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25700_MI;
struct t20;
 void m25700 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25700 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Remoting.WellKnownObjectMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1484_0_0_0;
extern Il2CppType t1484_0_0_0;
static ParameterInfo t20_m25700_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25700_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25700_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25700, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25700_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25700_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25701_MI;
struct t20;
 bool m25701 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25701 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1484_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1484_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1484_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1484_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Remoting.WellKnownObjectMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1484_0_0_0;
static ParameterInfo t20_m25701_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25701_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25701_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25701, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25701_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25701_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25702_MI;
struct t20;
 void m25702 (t20 * __this, t3684* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25702 (t20 * __this, t3684* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Remoting.WellKnownObjectMode>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3684_0_0_0;
extern Il2CppType t3684_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25702_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3684_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25702_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25702_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25702, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25702_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25702_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25703_MI;
struct t20;
 bool m25703 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25703 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Remoting.WellKnownObjectMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1484_0_0_0;
static ParameterInfo t20_m25703_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25703_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25703_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25703, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25703_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25703_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25704_MI;
struct t20;
 int32_t m25704 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25704 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1484_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1484_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1484_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1484_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Remoting.WellKnownObjectMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1484_0_0_0;
static ParameterInfo t20_m25704_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25704_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25704_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25704, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25704_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25704_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25705_MI;
struct t20;
 void m25705 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25705 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Remoting.WellKnownObjectMode>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1484_0_0_0;
static ParameterInfo t20_m25705_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25705_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25705_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25705, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25705_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25705_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25706_MI;
struct t20;
extern MethodInfo m25707_MI;
struct t20;
 void m25707 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25707 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1484_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Remoting.WellKnownObjectMode>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1484_0_0_0;
static ParameterInfo t20_m25707_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1484_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25707_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25707_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25707, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25707_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25707_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Remoting.WellKnownObjectMode>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1484_1_0_0;
static ParameterInfo t20_m25706_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1484_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25706_GM;
extern void* RuntimeInvoker_t21_t44_t4958 (MethodInfo* method, void* obj, void** args);
MethodInfo m25706_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4958, t20_m25706_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25706_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3432.h"
extern TypeInfo t3432_TI;
#include "t3432MD.h"
extern MethodInfo m19030_MI;
extern MethodInfo m25708_MI;
struct t20;
 t29* m25708 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25708 (t20 * __this, MethodInfo* method){
	{
		t3432  L_0 = {0};
		m19030(&L_0, __this, &m19030_MI);
		t3432  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3432_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Remoting.WellKnownObjectMode>()
extern TypeInfo t20_TI;
extern TypeInfo t4959_TI;
extern Il2CppType t4959_0_0_0;
extern Il2CppGenericMethod m25708_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25708_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25708, &t20_TI, &t4959_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25708_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1490.h"
extern MethodInfo m25709_MI;
struct t20;
extern MethodInfo m25710_MI;
struct t20;
 uint8_t m25710 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m25710 (t20 * __this, int32_t p0, MethodInfo* method){
	uint8_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.BinaryElement>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1490_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25710_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1490_0_0_0;
extern Il2CppType t1490_0_0_0;
extern Il2CppGenericMethod m25710_GM;
extern void* RuntimeInvoker_t1490_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25710_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25710, &t20_TI, &t1490_0_0_0, RuntimeInvoker_t1490_t44, t20_m25710_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25710_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.BinaryElement>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1490_1_0_2;
extern Il2CppType t1490_1_0_0;
static ParameterInfo t20_m25709_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1490_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25709_GM;
extern void* RuntimeInvoker_t21_t44_t4960 (MethodInfo* method, void* obj, void** args);
MethodInfo m25709_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4960, t20_m25709_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25709_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25711_MI;
struct t20;
 void m25711 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25711 (t20 * __this, uint8_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.BinaryElement>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1490_0_0_0;
extern Il2CppType t1490_0_0_0;
static ParameterInfo t20_m25711_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1490_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25711_GM;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25711_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25711, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t20_m25711_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25711_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25712_MI;
struct t20;
 bool m25712 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25712 (t20 * __this, uint8_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1490_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		uint8_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1490_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		uint8_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1490_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1490_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.BinaryElement>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1490_0_0_0;
static ParameterInfo t20_m25712_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1490_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25712_GM;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25712_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25712, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t20_m25712_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25712_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25713_MI;
struct t20;
 void m25713 (t20 * __this, t3685* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25713 (t20 * __this, t3685* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.BinaryElement>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3685_0_0_0;
extern Il2CppType t3685_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25713_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3685_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25713_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25713_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25713, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25713_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25713_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25714_MI;
struct t20;
 bool m25714 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25714 (t20 * __this, uint8_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.BinaryElement>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1490_0_0_0;
static ParameterInfo t20_m25714_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1490_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25714_GM;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25714_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25714, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t20_m25714_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25714_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25715_MI;
struct t20;
 int32_t m25715 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25715 (t20 * __this, uint8_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1490_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		uint8_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1490_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		uint8_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1490_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1490_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.BinaryElement>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1490_0_0_0;
static ParameterInfo t20_m25715_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1490_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25715_GM;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25715_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25715, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t20_m25715_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25715_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25716_MI;
struct t20;
 void m25716 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25716 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.BinaryElement>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1490_0_0_0;
static ParameterInfo t20_m25716_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1490_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25716_GM;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25716_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25716, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t20_m25716_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25716_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25717_MI;
struct t20;
extern MethodInfo m25718_MI;
struct t20;
 void m25718 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25718 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		uint8_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1490_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.BinaryElement>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1490_0_0_0;
static ParameterInfo t20_m25718_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1490_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25718_GM;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25718_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25718, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t20_m25718_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25718_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.BinaryElement>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1490_1_0_0;
static ParameterInfo t20_m25717_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1490_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25717_GM;
extern void* RuntimeInvoker_t21_t44_t4960 (MethodInfo* method, void* obj, void** args);
MethodInfo m25717_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4960, t20_m25717_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25717_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3433.h"
extern TypeInfo t3433_TI;
#include "t3433MD.h"
extern MethodInfo m19035_MI;
extern MethodInfo m25719_MI;
struct t20;
 t29* m25719 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25719 (t20 * __this, MethodInfo* method){
	{
		t3433  L_0 = {0};
		m19035(&L_0, __this, &m19035_MI);
		t3433  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3433_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.BinaryElement>()
extern TypeInfo t20_TI;
extern TypeInfo t4961_TI;
extern Il2CppType t4961_0_0_0;
extern Il2CppGenericMethod m25719_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25719_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25719, &t20_TI, &t4961_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25719_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1491.h"
extern MethodInfo m25720_MI;
struct t20;
extern MethodInfo m25721_MI;
struct t20;
 uint8_t m25721 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m25721 (t20 * __this, int32_t p0, MethodInfo* method){
	uint8_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1491_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25721_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1491_0_0_0;
extern Il2CppType t1491_0_0_0;
extern Il2CppGenericMethod m25721_GM;
extern void* RuntimeInvoker_t1491_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25721_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25721, &t20_TI, &t1491_0_0_0, RuntimeInvoker_t1491_t44, t20_m25721_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25721_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1491_1_0_2;
extern Il2CppType t1491_1_0_0;
static ParameterInfo t20_m25720_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1491_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25720_GM;
extern void* RuntimeInvoker_t21_t44_t4962 (MethodInfo* method, void* obj, void** args);
MethodInfo m25720_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4962, t20_m25720_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25720_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25722_MI;
struct t20;
 void m25722 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25722 (t20 * __this, uint8_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1491_0_0_0;
extern Il2CppType t1491_0_0_0;
static ParameterInfo t20_m25722_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25722_GM;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25722_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25722, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t20_m25722_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25722_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25723_MI;
struct t20;
 bool m25723 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25723 (t20 * __this, uint8_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1491_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		uint8_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1491_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		uint8_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1491_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1491_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1491_0_0_0;
static ParameterInfo t20_m25723_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25723_GM;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25723_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25723, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t20_m25723_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25723_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25724_MI;
struct t20;
 void m25724 (t20 * __this, t2056* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25724 (t20 * __this, t2056* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t2056_0_0_0;
extern Il2CppType t2056_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25724_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t2056_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25724_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25724_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25724, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25724_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25724_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25725_MI;
struct t20;
 bool m25725 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25725 (t20 * __this, uint8_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1491_0_0_0;
static ParameterInfo t20_m25725_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25725_GM;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25725_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25725, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t20_m25725_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25725_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25726_MI;
struct t20;
 int32_t m25726 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25726 (t20 * __this, uint8_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1491_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		uint8_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1491_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		uint8_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1491_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1491_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.TypeTag>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1491_0_0_0;
static ParameterInfo t20_m25726_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25726_GM;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25726_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25726, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t20_m25726_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25726_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25727_MI;
struct t20;
 void m25727 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25727 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1491_0_0_0;
static ParameterInfo t20_m25727_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25727_GM;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25727_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25727, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t20_m25727_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25727_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25728_MI;
struct t20;
extern MethodInfo m25729_MI;
struct t20;
 void m25729 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25729 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		uint8_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1491_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1491_0_0_0;
static ParameterInfo t20_m25729_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1491_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25729_GM;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25729_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25729, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t20_m25729_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25729_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1491_1_0_0;
static ParameterInfo t20_m25728_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1491_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25728_GM;
extern void* RuntimeInvoker_t21_t44_t4962 (MethodInfo* method, void* obj, void** args);
MethodInfo m25728_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4962, t20_m25728_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25728_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3434.h"
extern TypeInfo t3434_TI;
#include "t3434MD.h"
extern MethodInfo m19040_MI;
extern MethodInfo m25730_MI;
struct t20;
 t29* m25730 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25730 (t20 * __this, MethodInfo* method){
	{
		t3434  L_0 = {0};
		m19040(&L_0, __this, &m19040_MI);
		t3434  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3434_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.TypeTag>()
extern TypeInfo t20_TI;
extern TypeInfo t4963_TI;
extern Il2CppType t4963_0_0_0;
extern Il2CppGenericMethod m25730_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25730_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25730, &t20_TI, &t4963_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25730_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1492.h"
extern MethodInfo m25731_MI;
struct t20;
extern MethodInfo m25732_MI;
struct t20;
 int32_t m25732 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25732 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.MethodFlags>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1492_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25732_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1492_0_0_0;
extern Il2CppType t1492_0_0_0;
extern Il2CppGenericMethod m25732_GM;
extern void* RuntimeInvoker_t1492_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25732_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25732, &t20_TI, &t1492_0_0_0, RuntimeInvoker_t1492_t44, t20_m25732_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25732_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.MethodFlags>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1492_1_0_2;
extern Il2CppType t1492_1_0_0;
static ParameterInfo t20_m25731_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1492_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25731_GM;
extern void* RuntimeInvoker_t21_t44_t4964 (MethodInfo* method, void* obj, void** args);
MethodInfo m25731_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4964, t20_m25731_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25731_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25733_MI;
struct t20;
 void m25733 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25733 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.MethodFlags>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1492_0_0_0;
extern Il2CppType t1492_0_0_0;
static ParameterInfo t20_m25733_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1492_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25733_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25733_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25733, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25733_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25733_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25734_MI;
struct t20;
 bool m25734 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25734 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1492_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1492_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1492_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1492_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.MethodFlags>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1492_0_0_0;
static ParameterInfo t20_m25734_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1492_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25734_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25734_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25734, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25734_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25734_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25735_MI;
struct t20;
 void m25735 (t20 * __this, t3686* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25735 (t20 * __this, t3686* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.MethodFlags>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3686_0_0_0;
extern Il2CppType t3686_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25735_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3686_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25735_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25735_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25735, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25735_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25735_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25736_MI;
struct t20;
 bool m25736 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25736 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.MethodFlags>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1492_0_0_0;
static ParameterInfo t20_m25736_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1492_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25736_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25736_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25736, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25736_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25736_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25737_MI;
struct t20;
 int32_t m25737 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25737 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1492_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1492_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1492_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1492_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.MethodFlags>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1492_0_0_0;
static ParameterInfo t20_m25737_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1492_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25737_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25737_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25737, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25737_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25737_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25738_MI;
struct t20;
 void m25738 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25738 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.MethodFlags>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1492_0_0_0;
static ParameterInfo t20_m25738_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1492_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25738_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25738_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25738, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25738_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25738_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25739_MI;
struct t20;
extern MethodInfo m25740_MI;
struct t20;
 void m25740 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25740 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1492_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.MethodFlags>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1492_0_0_0;
static ParameterInfo t20_m25740_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1492_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25740_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25740_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25740, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25740_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25740_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.MethodFlags>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1492_1_0_0;
static ParameterInfo t20_m25739_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1492_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25739_GM;
extern void* RuntimeInvoker_t21_t44_t4964 (MethodInfo* method, void* obj, void** args);
MethodInfo m25739_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4964, t20_m25739_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25739_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3435.h"
extern TypeInfo t3435_TI;
#include "t3435MD.h"
extern MethodInfo m19045_MI;
extern MethodInfo m25741_MI;
struct t20;
 t29* m25741 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25741 (t20 * __this, MethodInfo* method){
	{
		t3435  L_0 = {0};
		m19045(&L_0, __this, &m19045_MI);
		t3435  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3435_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.MethodFlags>()
extern TypeInfo t20_TI;
extern TypeInfo t4965_TI;
extern Il2CppType t4965_0_0_0;
extern Il2CppGenericMethod m25741_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25741_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25741, &t20_TI, &t4965_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25741_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1493.h"
extern MethodInfo m25742_MI;
struct t20;
extern MethodInfo m25743_MI;
struct t20;
 uint8_t m25743 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m25743 (t20 * __this, int32_t p0, MethodInfo* method){
	uint8_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1493_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25743_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1493_0_0_0;
extern Il2CppType t1493_0_0_0;
extern Il2CppGenericMethod m25743_GM;
extern void* RuntimeInvoker_t1493_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25743_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25743, &t20_TI, &t1493_0_0_0, RuntimeInvoker_t1493_t44, t20_m25743_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25743_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1493_1_0_2;
extern Il2CppType t1493_1_0_0;
static ParameterInfo t20_m25742_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1493_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25742_GM;
extern void* RuntimeInvoker_t21_t44_t4966 (MethodInfo* method, void* obj, void** args);
MethodInfo m25742_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4966, t20_m25742_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25742_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25744_MI;
struct t20;
 void m25744 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25744 (t20 * __this, uint8_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1493_0_0_0;
extern Il2CppType t1493_0_0_0;
static ParameterInfo t20_m25744_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1493_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25744_GM;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25744_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25744, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t20_m25744_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25744_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25745_MI;
struct t20;
 bool m25745 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25745 (t20 * __this, uint8_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1493_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		uint8_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1493_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		uint8_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1493_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1493_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1493_0_0_0;
static ParameterInfo t20_m25745_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1493_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25745_GM;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25745_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25745, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t20_m25745_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25745_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25746_MI;
struct t20;
 void m25746 (t20 * __this, t3687* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25746 (t20 * __this, t3687* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3687_0_0_0;
extern Il2CppType t3687_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25746_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3687_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25746_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25746_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25746, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25746_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25746_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25747_MI;
struct t20;
 bool m25747 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25747 (t20 * __this, uint8_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1493_0_0_0;
static ParameterInfo t20_m25747_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1493_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25747_GM;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25747_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25747, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t20_m25747_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25747_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25748_MI;
struct t20;
 int32_t m25748 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25748 (t20 * __this, uint8_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1493_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		uint8_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1493_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		uint8_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1493_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1493_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1493_0_0_0;
static ParameterInfo t20_m25748_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1493_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25748_GM;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25748_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25748, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t20_m25748_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25748_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25749_MI;
struct t20;
 void m25749 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25749 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1493_0_0_0;
static ParameterInfo t20_m25749_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1493_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25749_GM;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25749_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25749, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t20_m25749_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25749_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25750_MI;
struct t20;
extern MethodInfo m25751_MI;
struct t20;
 void m25751 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25751 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		uint8_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1493_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1493_0_0_0;
static ParameterInfo t20_m25751_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1493_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25751_GM;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25751_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25751, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t20_m25751_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25751_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1493_1_0_0;
static ParameterInfo t20_m25750_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1493_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25750_GM;
extern void* RuntimeInvoker_t21_t44_t4966 (MethodInfo* method, void* obj, void** args);
MethodInfo m25750_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4966, t20_m25750_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25750_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3436.h"
extern TypeInfo t3436_TI;
#include "t3436MD.h"
extern MethodInfo m19050_MI;
extern MethodInfo m25752_MI;
struct t20;
 t29* m25752 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25752 (t20 * __this, MethodInfo* method){
	{
		t3436  L_0 = {0};
		m19050(&L_0, __this, &m19050_MI);
		t3436  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3436_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag>()
extern TypeInfo t20_TI;
extern TypeInfo t4967_TI;
extern Il2CppType t4967_0_0_0;
extern Il2CppGenericMethod m25752_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25752_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25752, &t20_TI, &t4967_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25752_GM};
#ifndef _MSC_VER
#else
#endif

#include "t465.h"
extern MethodInfo m25753_MI;
struct t20;
extern MethodInfo m25754_MI;
struct t20;
 t465  m25754 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m25754 (t20 * __this, int32_t p0, MethodInfo* method){
	t465  V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t465_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25754_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t465_0_0_0;
extern Il2CppType t465_0_0_0;
extern Il2CppGenericMethod m25754_GM;
extern void* RuntimeInvoker_t465_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25754_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25754, &t20_TI, &t465_0_0_0, RuntimeInvoker_t465_t44, t20_m25754_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25754_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.DateTime>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t465_1_0_2;
extern Il2CppType t465_1_0_0;
static ParameterInfo t20_m25753_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t465_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25753_GM;
extern void* RuntimeInvoker_t21_t44_t2072 (MethodInfo* method, void* obj, void** args);
MethodInfo m25753_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2072, t20_m25753_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25753_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25755_MI;
struct t20;
 void m25755 (t20 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25755 (t20 * __this, t465  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.DateTime>(T)
extern TypeInfo t20_TI;
extern Il2CppType t465_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t20_m25755_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25755_GM;
extern void* RuntimeInvoker_t21_t465 (MethodInfo* method, void* obj, void** args);
MethodInfo m25755_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25755, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t465, t20_m25755_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25755_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25756_MI;
struct t20;
 bool m25756 (t20 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25756 (t20 * __this, t465  p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t465  V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		t465  L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t465_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		t465  L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t465_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		t465  L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t465_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t465_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime>(T)
extern TypeInfo t20_TI;
extern Il2CppType t465_0_0_0;
static ParameterInfo t20_m25756_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25756_GM;
extern void* RuntimeInvoker_t40_t465 (MethodInfo* method, void* obj, void** args);
MethodInfo m25756_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25756, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t465, t20_m25756_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25756_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25757_MI;
struct t20;
 void m25757 (t20 * __this, t2053* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25757 (t20 * __this, t2053* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t2053_0_0_0;
extern Il2CppType t2053_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25757_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t2053_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25757_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25757_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25757, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25757_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25757_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25758_MI;
struct t20;
 bool m25758 (t20 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25758 (t20 * __this, t465  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime>(T)
extern TypeInfo t20_TI;
extern Il2CppType t465_0_0_0;
static ParameterInfo t20_m25758_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25758_GM;
extern void* RuntimeInvoker_t40_t465 (MethodInfo* method, void* obj, void** args);
MethodInfo m25758_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25758, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t465, t20_m25758_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25758_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25759_MI;
struct t20;
 int32_t m25759 (t20 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25759 (t20 * __this, t465  p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t465  V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		t465  L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t465_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		t465  L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t465_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		t465  L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t465_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t465_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.DateTime>(T)
extern TypeInfo t20_TI;
extern Il2CppType t465_0_0_0;
static ParameterInfo t20_m25759_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25759_GM;
extern void* RuntimeInvoker_t44_t465 (MethodInfo* method, void* obj, void** args);
MethodInfo m25759_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25759, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t465, t20_m25759_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25759_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25760_MI;
struct t20;
 void m25760 (t20 * __this, int32_t p0, t465  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25760 (t20 * __this, int32_t p0, t465  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.DateTime>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t20_m25760_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25760_GM;
extern void* RuntimeInvoker_t21_t44_t465 (MethodInfo* method, void* obj, void** args);
MethodInfo m25760_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25760, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t465, t20_m25760_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25760_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25761_MI;
struct t20;
extern MethodInfo m25762_MI;
struct t20;
 void m25762 (t20 * __this, int32_t p0, t465  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25762 (t20 * __this, int32_t p0, t465  p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		t465  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t465_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.DateTime>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t465_0_0_0;
static ParameterInfo t20_m25762_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t465_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25762_GM;
extern void* RuntimeInvoker_t21_t44_t465 (MethodInfo* method, void* obj, void** args);
MethodInfo m25762_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25762, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t465, t20_m25762_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25762_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.DateTime>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t465_1_0_0;
static ParameterInfo t20_m25761_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t465_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25761_GM;
extern void* RuntimeInvoker_t21_t44_t2072 (MethodInfo* method, void* obj, void** args);
MethodInfo m25761_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2072, t20_m25761_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25761_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3437.h"
extern TypeInfo t3437_TI;
#include "t3437MD.h"
extern MethodInfo m19055_MI;
extern MethodInfo m25763_MI;
struct t20;
 t29* m25763 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25763 (t20 * __this, MethodInfo* method){
	{
		t3437  L_0 = {0};
		m19055(&L_0, __this, &m19055_MI);
		t3437  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3437_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime>()
extern TypeInfo t20_TI;
extern TypeInfo t4968_TI;
extern Il2CppType t4968_0_0_0;
extern Il2CppGenericMethod m25763_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25763_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25763, &t20_TI, &t4968_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25763_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25764_MI;
struct t20;
extern MethodInfo m25765_MI;
struct t20;
#define m25765(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.IComparable`1<System.DateTime>>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t2074_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25765_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t2074_0_0_0;
extern Il2CppType t2074_0_0_0;
extern Il2CppGenericMethod m25765_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25765_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t2074_0_0_0, RuntimeInvoker_t29_t44, t20_m25765_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25765_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.IComparable`1<System.DateTime>>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2074_1_0_2;
extern Il2CppType t2074_1_0_0;
static ParameterInfo t20_m25764_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2074_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25764_GM;
extern void* RuntimeInvoker_t21_t44_t4969 (MethodInfo* method, void* obj, void** args);
MethodInfo m25764_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4969, t20_m25764_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25764_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25766_MI;
struct t20;
#define m25766(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.IComparable`1<System.DateTime>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2074_0_0_0;
extern Il2CppType t2074_0_0_0;
static ParameterInfo t20_m25766_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t2074_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25766_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25766_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25766_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25766_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25767_MI;
struct t20;
#define m25767(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.IComparable`1<System.DateTime>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2074_0_0_0;
static ParameterInfo t20_m25767_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t2074_0_0_0},
};
extern TypeInfo t2074_TI;
static Il2CppRGCTXData m25767_RGCTXData[1] = 
{
	&t2074_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25767_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25767_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25767_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25767_RGCTXData, NULL, &m25767_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25768_MI;
struct t20;
#define m25768(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.IComparable`1<System.DateTime>>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3688_0_0_0;
extern Il2CppType t3688_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25768_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3688_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25768_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25768_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25768_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25768_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25769_MI;
struct t20;
#define m25769(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.IComparable`1<System.DateTime>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2074_0_0_0;
static ParameterInfo t20_m25769_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t2074_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25769_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25769_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25769_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25769_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25770_MI;
struct t20;
#define m25770(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.IComparable`1<System.DateTime>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2074_0_0_0;
static ParameterInfo t20_m25770_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t2074_0_0_0},
};
extern TypeInfo t2074_TI;
static Il2CppRGCTXData m25770_RGCTXData[1] = 
{
	&t2074_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25770_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25770_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25770_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25770_RGCTXData, NULL, &m25770_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25771_MI;
struct t20;
#define m25771(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.IComparable`1<System.DateTime>>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2074_0_0_0;
static ParameterInfo t20_m25771_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t2074_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25771_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25771_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25771_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25771_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25772_MI;
struct t20;
extern MethodInfo m25773_MI;
struct t20;
#define m25773(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.IComparable`1<System.DateTime>>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2074_0_0_0;
static ParameterInfo t20_m25773_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t2074_0_0_0},
};
extern TypeInfo t2074_TI;
static Il2CppRGCTXData m25773_RGCTXData[1] = 
{
	&t2074_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25773_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25773_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25773_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25773_RGCTXData, NULL, &m25773_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.IComparable`1<System.DateTime>>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2074_1_0_0;
static ParameterInfo t20_m25772_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2074_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25772_GM;
extern void* RuntimeInvoker_t21_t44_t4969 (MethodInfo* method, void* obj, void** args);
MethodInfo m25772_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4969, t20_m25772_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25772_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3438.h"
extern TypeInfo t3438_TI;
#include "t3438MD.h"
extern MethodInfo m19060_MI;
extern MethodInfo m25774_MI;
struct t20;
#define m25774(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.IComparable`1<System.DateTime>>()
extern TypeInfo t20_TI;
extern TypeInfo t4970_TI;
extern TypeInfo t3438_TI;
static Il2CppRGCTXData m25774_RGCTXData[2] = 
{
	&t3438_TI/* Class Usage */,
	&m19060_MI/* Method Usage */,
};
extern Il2CppType t4970_0_0_0;
extern Il2CppGenericMethod m25774_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25774_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4970_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25774_RGCTXData, NULL, &m25774_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25775_MI;
struct t20;
extern MethodInfo m25776_MI;
struct t20;
#define m25776(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.IEquatable`1<System.DateTime>>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t2075_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25776_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t2075_0_0_0;
extern Il2CppType t2075_0_0_0;
extern Il2CppGenericMethod m25776_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25776_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t2075_0_0_0, RuntimeInvoker_t29_t44, t20_m25776_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25776_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.IEquatable`1<System.DateTime>>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2075_1_0_2;
extern Il2CppType t2075_1_0_0;
static ParameterInfo t20_m25775_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2075_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25775_GM;
extern void* RuntimeInvoker_t21_t44_t4971 (MethodInfo* method, void* obj, void** args);
MethodInfo m25775_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4971, t20_m25775_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25775_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25777_MI;
struct t20;
#define m25777(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.IEquatable`1<System.DateTime>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2075_0_0_0;
extern Il2CppType t2075_0_0_0;
static ParameterInfo t20_m25777_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t2075_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25777_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25777_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25777_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25777_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25778_MI;
struct t20;
#define m25778(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.IEquatable`1<System.DateTime>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2075_0_0_0;
static ParameterInfo t20_m25778_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t2075_0_0_0},
};
extern TypeInfo t2075_TI;
static Il2CppRGCTXData m25778_RGCTXData[1] = 
{
	&t2075_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25778_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25778_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25778_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25778_RGCTXData, NULL, &m25778_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25779_MI;
struct t20;
#define m25779(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.IEquatable`1<System.DateTime>>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3689_0_0_0;
extern Il2CppType t3689_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25779_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3689_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25779_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25779_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25779_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25779_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25780_MI;
struct t20;
#define m25780(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.IEquatable`1<System.DateTime>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2075_0_0_0;
static ParameterInfo t20_m25780_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t2075_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25780_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25780_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25780_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25780_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25781_MI;
struct t20;
#define m25781(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.IEquatable`1<System.DateTime>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2075_0_0_0;
static ParameterInfo t20_m25781_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t2075_0_0_0},
};
extern TypeInfo t2075_TI;
static Il2CppRGCTXData m25781_RGCTXData[1] = 
{
	&t2075_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25781_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25781_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25781_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25781_RGCTXData, NULL, &m25781_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25782_MI;
struct t20;
#define m25782(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.IEquatable`1<System.DateTime>>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2075_0_0_0;
static ParameterInfo t20_m25782_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t2075_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25782_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25782_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25782_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25782_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25783_MI;
struct t20;
extern MethodInfo m25784_MI;
struct t20;
#define m25784(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.IEquatable`1<System.DateTime>>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2075_0_0_0;
static ParameterInfo t20_m25784_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t2075_0_0_0},
};
extern TypeInfo t2075_TI;
static Il2CppRGCTXData m25784_RGCTXData[1] = 
{
	&t2075_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25784_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25784_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25784_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25784_RGCTXData, NULL, &m25784_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.IEquatable`1<System.DateTime>>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2075_1_0_0;
static ParameterInfo t20_m25783_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2075_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25783_GM;
extern void* RuntimeInvoker_t21_t44_t4971 (MethodInfo* method, void* obj, void** args);
MethodInfo m25783_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4971, t20_m25783_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25783_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3439.h"
extern TypeInfo t3439_TI;
#include "t3439MD.h"
extern MethodInfo m19065_MI;
extern MethodInfo m25785_MI;
struct t20;
#define m25785(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.IEquatable`1<System.DateTime>>()
extern TypeInfo t20_TI;
extern TypeInfo t4972_TI;
extern TypeInfo t3439_TI;
static Il2CppRGCTXData m25785_RGCTXData[2] = 
{
	&t3439_TI/* Class Usage */,
	&m19065_MI/* Method Usage */,
};
extern Il2CppType t4972_0_0_0;
extern Il2CppGenericMethod m25785_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25785_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4972_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25785_RGCTXData, NULL, &m25785_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1126.h"
extern MethodInfo m25786_MI;
struct t20;
extern MethodInfo m25787_MI;
struct t20;
 t1126  m25787 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m25787 (t20 * __this, int32_t p0, MethodInfo* method){
	t1126  V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1126_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25787_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1126_0_0_0;
extern Il2CppType t1126_0_0_0;
extern Il2CppGenericMethod m25787_GM;
extern void* RuntimeInvoker_t1126_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25787_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25787, &t20_TI, &t1126_0_0_0, RuntimeInvoker_t1126_t44, t20_m25787_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25787_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Decimal>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1126_1_0_2;
extern Il2CppType t1126_1_0_0;
static ParameterInfo t20_m25786_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1126_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25786_GM;
extern void* RuntimeInvoker_t21_t44_t1756 (MethodInfo* method, void* obj, void** args);
MethodInfo m25786_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1756, t20_m25786_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25786_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25788_MI;
struct t20;
 void m25788 (t20 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25788 (t20 * __this, t1126  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Decimal>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1126_0_0_0;
extern Il2CppType t1126_0_0_0;
static ParameterInfo t20_m25788_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25788_GM;
extern void* RuntimeInvoker_t21_t1126 (MethodInfo* method, void* obj, void** args);
MethodInfo m25788_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25788, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t1126, t20_m25788_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25788_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25789_MI;
struct t20;
 bool m25789 (t20 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25789 (t20 * __this, t1126  p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t1126  V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		t1126  L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1126_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		t1126  L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1126_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		t1126  L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1126_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1126_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Decimal>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1126_0_0_0;
static ParameterInfo t20_m25789_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25789_GM;
extern void* RuntimeInvoker_t40_t1126 (MethodInfo* method, void* obj, void** args);
MethodInfo m25789_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25789, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t1126, t20_m25789_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25789_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25790_MI;
struct t20;
 void m25790 (t20 * __this, t2054* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25790 (t20 * __this, t2054* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Decimal>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t2054_0_0_0;
extern Il2CppType t2054_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25790_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t2054_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25790_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25790_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25790, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25790_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25790_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25791_MI;
struct t20;
 bool m25791 (t20 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25791 (t20 * __this, t1126  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Decimal>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1126_0_0_0;
static ParameterInfo t20_m25791_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25791_GM;
extern void* RuntimeInvoker_t40_t1126 (MethodInfo* method, void* obj, void** args);
MethodInfo m25791_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25791, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t1126, t20_m25791_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25791_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25792_MI;
struct t20;
 int32_t m25792 (t20 * __this, t1126  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25792 (t20 * __this, t1126  p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t1126  V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		t1126  L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1126_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		t1126  L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1126_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		t1126  L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1126_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1126_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Decimal>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1126_0_0_0;
static ParameterInfo t20_m25792_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25792_GM;
extern void* RuntimeInvoker_t44_t1126 (MethodInfo* method, void* obj, void** args);
MethodInfo m25792_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25792, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t1126, t20_m25792_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25792_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25793_MI;
struct t20;
 void m25793 (t20 * __this, int32_t p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25793 (t20 * __this, int32_t p0, t1126  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Decimal>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1126_0_0_0;
static ParameterInfo t20_m25793_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25793_GM;
extern void* RuntimeInvoker_t21_t44_t1126 (MethodInfo* method, void* obj, void** args);
MethodInfo m25793_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25793, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1126, t20_m25793_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25793_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25794_MI;
struct t20;
extern MethodInfo m25795_MI;
struct t20;
 void m25795 (t20 * __this, int32_t p0, t1126  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25795 (t20 * __this, int32_t p0, t1126  p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		t1126  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1126_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Decimal>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1126_0_0_0;
static ParameterInfo t20_m25795_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1126_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25795_GM;
extern void* RuntimeInvoker_t21_t44_t1126 (MethodInfo* method, void* obj, void** args);
MethodInfo m25795_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25795, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1126, t20_m25795_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25795_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Decimal>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1126_1_0_0;
static ParameterInfo t20_m25794_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1126_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25794_GM;
extern void* RuntimeInvoker_t21_t44_t1756 (MethodInfo* method, void* obj, void** args);
MethodInfo m25794_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t1756, t20_m25794_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25794_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3440.h"
extern TypeInfo t3440_TI;
#include "t3440MD.h"
extern MethodInfo m19070_MI;
extern MethodInfo m25796_MI;
struct t20;
 t29* m25796 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25796 (t20 * __this, MethodInfo* method){
	{
		t3440  L_0 = {0};
		m19070(&L_0, __this, &m19070_MI);
		t3440  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3440_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Decimal>()
extern TypeInfo t20_TI;
extern TypeInfo t4973_TI;
extern Il2CppType t4973_0_0_0;
extern Il2CppGenericMethod m25796_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25796_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25796, &t20_TI, &t4973_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25796_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25797_MI;
struct t20;
extern MethodInfo m25798_MI;
struct t20;
#define m25798(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.IComparable`1<System.Decimal>>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1757_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25798_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1757_0_0_0;
extern Il2CppType t1757_0_0_0;
extern Il2CppGenericMethod m25798_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25798_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1757_0_0_0, RuntimeInvoker_t29_t44, t20_m25798_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25798_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.IComparable`1<System.Decimal>>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1757_1_0_2;
extern Il2CppType t1757_1_0_0;
static ParameterInfo t20_m25797_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1757_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25797_GM;
extern void* RuntimeInvoker_t21_t44_t4974 (MethodInfo* method, void* obj, void** args);
MethodInfo m25797_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4974, t20_m25797_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25797_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25799_MI;
struct t20;
#define m25799(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.IComparable`1<System.Decimal>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1757_0_0_0;
extern Il2CppType t1757_0_0_0;
static ParameterInfo t20_m25799_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1757_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25799_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25799_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25799_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25799_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25800_MI;
struct t20;
#define m25800(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.IComparable`1<System.Decimal>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1757_0_0_0;
static ParameterInfo t20_m25800_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1757_0_0_0},
};
extern TypeInfo t1757_TI;
static Il2CppRGCTXData m25800_RGCTXData[1] = 
{
	&t1757_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25800_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25800_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25800_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25800_RGCTXData, NULL, &m25800_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25801_MI;
struct t20;
#define m25801(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.IComparable`1<System.Decimal>>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3690_0_0_0;
extern Il2CppType t3690_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25801_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3690_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25801_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25801_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25801_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25801_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25802_MI;
struct t20;
#define m25802(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.IComparable`1<System.Decimal>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1757_0_0_0;
static ParameterInfo t20_m25802_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1757_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25802_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25802_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25802_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25802_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25803_MI;
struct t20;
#define m25803(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.IComparable`1<System.Decimal>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1757_0_0_0;
static ParameterInfo t20_m25803_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1757_0_0_0},
};
extern TypeInfo t1757_TI;
static Il2CppRGCTXData m25803_RGCTXData[1] = 
{
	&t1757_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25803_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25803_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25803_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25803_RGCTXData, NULL, &m25803_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25804_MI;
struct t20;
#define m25804(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.IComparable`1<System.Decimal>>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1757_0_0_0;
static ParameterInfo t20_m25804_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1757_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25804_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25804_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25804_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25804_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25805_MI;
struct t20;
extern MethodInfo m25806_MI;
struct t20;
#define m25806(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.IComparable`1<System.Decimal>>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1757_0_0_0;
static ParameterInfo t20_m25806_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1757_0_0_0},
};
extern TypeInfo t1757_TI;
static Il2CppRGCTXData m25806_RGCTXData[1] = 
{
	&t1757_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25806_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25806_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25806_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25806_RGCTXData, NULL, &m25806_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.IComparable`1<System.Decimal>>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1757_1_0_0;
static ParameterInfo t20_m25805_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1757_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25805_GM;
extern void* RuntimeInvoker_t21_t44_t4974 (MethodInfo* method, void* obj, void** args);
MethodInfo m25805_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4974, t20_m25805_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25805_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3441.h"
extern TypeInfo t3441_TI;
#include "t3441MD.h"
extern MethodInfo m19075_MI;
extern MethodInfo m25807_MI;
struct t20;
#define m25807(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.IComparable`1<System.Decimal>>()
extern TypeInfo t20_TI;
extern TypeInfo t4975_TI;
extern TypeInfo t3441_TI;
static Il2CppRGCTXData m25807_RGCTXData[2] = 
{
	&t3441_TI/* Class Usage */,
	&m19075_MI/* Method Usage */,
};
extern Il2CppType t4975_0_0_0;
extern Il2CppGenericMethod m25807_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25807_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4975_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25807_RGCTXData, NULL, &m25807_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25808_MI;
struct t20;
extern MethodInfo m25809_MI;
struct t20;
#define m25809(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.IEquatable`1<System.Decimal>>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1758_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25809_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1758_0_0_0;
extern Il2CppType t1758_0_0_0;
extern Il2CppGenericMethod m25809_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25809_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1758_0_0_0, RuntimeInvoker_t29_t44, t20_m25809_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25809_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.IEquatable`1<System.Decimal>>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1758_1_0_2;
extern Il2CppType t1758_1_0_0;
static ParameterInfo t20_m25808_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1758_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25808_GM;
extern void* RuntimeInvoker_t21_t44_t4976 (MethodInfo* method, void* obj, void** args);
MethodInfo m25808_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4976, t20_m25808_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25808_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25810_MI;
struct t20;
#define m25810(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.IEquatable`1<System.Decimal>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1758_0_0_0;
extern Il2CppType t1758_0_0_0;
static ParameterInfo t20_m25810_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1758_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25810_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25810_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25810_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25810_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25811_MI;
struct t20;
#define m25811(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.IEquatable`1<System.Decimal>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1758_0_0_0;
static ParameterInfo t20_m25811_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1758_0_0_0},
};
extern TypeInfo t1758_TI;
static Il2CppRGCTXData m25811_RGCTXData[1] = 
{
	&t1758_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25811_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25811_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25811_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25811_RGCTXData, NULL, &m25811_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25812_MI;
struct t20;
#define m25812(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.IEquatable`1<System.Decimal>>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3691_0_0_0;
extern Il2CppType t3691_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25812_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3691_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25812_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25812_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25812_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25812_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25813_MI;
struct t20;
#define m25813(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.IEquatable`1<System.Decimal>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1758_0_0_0;
static ParameterInfo t20_m25813_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1758_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25813_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25813_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25813_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25813_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25814_MI;
struct t20;
#define m25814(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.IEquatable`1<System.Decimal>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1758_0_0_0;
static ParameterInfo t20_m25814_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1758_0_0_0},
};
extern TypeInfo t1758_TI;
static Il2CppRGCTXData m25814_RGCTXData[1] = 
{
	&t1758_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25814_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25814_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25814_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25814_RGCTXData, NULL, &m25814_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25815_MI;
struct t20;
#define m25815(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.IEquatable`1<System.Decimal>>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1758_0_0_0;
static ParameterInfo t20_m25815_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1758_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25815_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25815_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25815_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25815_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25816_MI;
struct t20;
extern MethodInfo m25817_MI;
struct t20;
#define m25817(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.IEquatable`1<System.Decimal>>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1758_0_0_0;
static ParameterInfo t20_m25817_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1758_0_0_0},
};
extern TypeInfo t1758_TI;
static Il2CppRGCTXData m25817_RGCTXData[1] = 
{
	&t1758_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25817_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25817_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25817_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25817_RGCTXData, NULL, &m25817_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.IEquatable`1<System.Decimal>>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1758_1_0_0;
static ParameterInfo t20_m25816_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1758_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25816_GM;
extern void* RuntimeInvoker_t21_t44_t4976 (MethodInfo* method, void* obj, void** args);
MethodInfo m25816_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4976, t20_m25816_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25816_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3442.h"
extern TypeInfo t3442_TI;
#include "t3442MD.h"
extern MethodInfo m19080_MI;
extern MethodInfo m25818_MI;
struct t20;
#define m25818(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.IEquatable`1<System.Decimal>>()
extern TypeInfo t20_TI;
extern TypeInfo t4977_TI;
extern TypeInfo t3442_TI;
static Il2CppRGCTXData m25818_RGCTXData[2] = 
{
	&t3442_TI/* Class Usage */,
	&m19080_MI/* Method Usage */,
};
extern Il2CppType t4977_0_0_0;
extern Il2CppGenericMethod m25818_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25818_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4977_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25818_RGCTXData, NULL, &m25818_GM};
#ifndef _MSC_VER
#else
#endif

#include "t816.h"
extern MethodInfo m25819_MI;
struct t20;
extern MethodInfo m25820_MI;
struct t20;
 t816  m25820 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m25820 (t20 * __this, int32_t p0, MethodInfo* method){
	t816  V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t816_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25820_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t816_0_0_0;
extern Il2CppType t816_0_0_0;
extern Il2CppGenericMethod m25820_GM;
extern void* RuntimeInvoker_t816_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25820_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25820, &t20_TI, &t816_0_0_0, RuntimeInvoker_t816_t44, t20_m25820_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25820_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t816_1_0_2;
extern Il2CppType t816_1_0_0;
static ParameterInfo t20_m25819_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t816_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25819_GM;
extern void* RuntimeInvoker_t21_t44_t4978 (MethodInfo* method, void* obj, void** args);
MethodInfo m25819_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4978, t20_m25819_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25819_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25821_MI;
struct t20;
 void m25821 (t20 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25821 (t20 * __this, t816  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.TimeSpan>(T)
extern TypeInfo t20_TI;
extern Il2CppType t816_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t20_m25821_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25821_GM;
extern void* RuntimeInvoker_t21_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m25821_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25821, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t816, t20_m25821_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25821_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25822_MI;
struct t20;
 bool m25822 (t20 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25822 (t20 * __this, t816  p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t816  V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		t816  L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t816_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		t816  L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t816_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		t816  L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t816_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t816_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.TimeSpan>(T)
extern TypeInfo t20_TI;
extern Il2CppType t816_0_0_0;
static ParameterInfo t20_m25822_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25822_GM;
extern void* RuntimeInvoker_t40_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m25822_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25822, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t816, t20_m25822_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25822_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25823_MI;
struct t20;
 void m25823 (t20 * __this, t2055* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25823 (t20 * __this, t2055* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.TimeSpan>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t2055_0_0_0;
extern Il2CppType t2055_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25823_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t2055_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25823_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25823_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25823, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25823_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25823_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25824_MI;
struct t20;
 bool m25824 (t20 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25824 (t20 * __this, t816  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.TimeSpan>(T)
extern TypeInfo t20_TI;
extern Il2CppType t816_0_0_0;
static ParameterInfo t20_m25824_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25824_GM;
extern void* RuntimeInvoker_t40_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m25824_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25824, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t816, t20_m25824_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25824_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25825_MI;
struct t20;
 int32_t m25825 (t20 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25825 (t20 * __this, t816  p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	t816  V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		t816  L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t816_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		t816  L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t816_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		t816  L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t816_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t816_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.TimeSpan>(T)
extern TypeInfo t20_TI;
extern Il2CppType t816_0_0_0;
static ParameterInfo t20_m25825_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25825_GM;
extern void* RuntimeInvoker_t44_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m25825_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25825, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t816, t20_m25825_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25825_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25826_MI;
struct t20;
 void m25826 (t20 * __this, int32_t p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25826 (t20 * __this, int32_t p0, t816  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.TimeSpan>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t20_m25826_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25826_GM;
extern void* RuntimeInvoker_t21_t44_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m25826_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25826, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t816, t20_m25826_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25826_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25827_MI;
struct t20;
extern MethodInfo m25828_MI;
struct t20;
 void m25828 (t20 * __this, int32_t p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25828 (t20 * __this, int32_t p0, t816  p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		t816  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t816_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.TimeSpan>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t816_0_0_0;
static ParameterInfo t20_m25828_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t816_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25828_GM;
extern void* RuntimeInvoker_t21_t44_t816 (MethodInfo* method, void* obj, void** args);
MethodInfo m25828_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25828, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t816, t20_m25828_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25828_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.TimeSpan>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t816_1_0_0;
static ParameterInfo t20_m25827_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t816_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25827_GM;
extern void* RuntimeInvoker_t21_t44_t4978 (MethodInfo* method, void* obj, void** args);
MethodInfo m25827_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4978, t20_m25827_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25827_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3443.h"
extern TypeInfo t3443_TI;
#include "t3443MD.h"
extern MethodInfo m19085_MI;
extern MethodInfo m25829_MI;
struct t20;
 t29* m25829 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25829 (t20 * __this, MethodInfo* method){
	{
		t3443  L_0 = {0};
		m19085(&L_0, __this, &m19085_MI);
		t3443  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3443_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TimeSpan>()
extern TypeInfo t20_TI;
extern TypeInfo t4979_TI;
extern Il2CppType t4979_0_0_0;
extern Il2CppGenericMethod m25829_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25829_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25829, &t20_TI, &t4979_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25829_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25830_MI;
struct t20;
extern MethodInfo m25831_MI;
struct t20;
#define m25831(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.IComparable`1<System.TimeSpan>>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t2099_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25831_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t2099_0_0_0;
extern Il2CppType t2099_0_0_0;
extern Il2CppGenericMethod m25831_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25831_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t2099_0_0_0, RuntimeInvoker_t29_t44, t20_m25831_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25831_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.IComparable`1<System.TimeSpan>>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2099_1_0_2;
extern Il2CppType t2099_1_0_0;
static ParameterInfo t20_m25830_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2099_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25830_GM;
extern void* RuntimeInvoker_t21_t44_t4980 (MethodInfo* method, void* obj, void** args);
MethodInfo m25830_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4980, t20_m25830_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25830_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25832_MI;
struct t20;
#define m25832(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.IComparable`1<System.TimeSpan>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2099_0_0_0;
extern Il2CppType t2099_0_0_0;
static ParameterInfo t20_m25832_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t2099_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25832_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25832_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25832_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25832_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25833_MI;
struct t20;
#define m25833(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.IComparable`1<System.TimeSpan>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2099_0_0_0;
static ParameterInfo t20_m25833_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t2099_0_0_0},
};
extern TypeInfo t2099_TI;
static Il2CppRGCTXData m25833_RGCTXData[1] = 
{
	&t2099_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25833_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25833_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25833_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25833_RGCTXData, NULL, &m25833_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25834_MI;
struct t20;
#define m25834(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.IComparable`1<System.TimeSpan>>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3692_0_0_0;
extern Il2CppType t3692_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25834_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3692_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25834_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25834_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25834_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25834_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25835_MI;
struct t20;
#define m25835(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.IComparable`1<System.TimeSpan>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2099_0_0_0;
static ParameterInfo t20_m25835_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t2099_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25835_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25835_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25835_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25835_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25836_MI;
struct t20;
#define m25836(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.IComparable`1<System.TimeSpan>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2099_0_0_0;
static ParameterInfo t20_m25836_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t2099_0_0_0},
};
extern TypeInfo t2099_TI;
static Il2CppRGCTXData m25836_RGCTXData[1] = 
{
	&t2099_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25836_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25836_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25836_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25836_RGCTXData, NULL, &m25836_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25837_MI;
struct t20;
#define m25837(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.IComparable`1<System.TimeSpan>>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2099_0_0_0;
static ParameterInfo t20_m25837_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t2099_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25837_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25837_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25837_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25837_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25838_MI;
struct t20;
extern MethodInfo m25839_MI;
struct t20;
#define m25839(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.IComparable`1<System.TimeSpan>>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2099_0_0_0;
static ParameterInfo t20_m25839_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t2099_0_0_0},
};
extern TypeInfo t2099_TI;
static Il2CppRGCTXData m25839_RGCTXData[1] = 
{
	&t2099_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25839_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25839_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25839_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25839_RGCTXData, NULL, &m25839_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.IComparable`1<System.TimeSpan>>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2099_1_0_0;
static ParameterInfo t20_m25838_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2099_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25838_GM;
extern void* RuntimeInvoker_t21_t44_t4980 (MethodInfo* method, void* obj, void** args);
MethodInfo m25838_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4980, t20_m25838_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25838_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3444.h"
extern TypeInfo t3444_TI;
#include "t3444MD.h"
extern MethodInfo m19090_MI;
extern MethodInfo m25840_MI;
struct t20;
#define m25840(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.IComparable`1<System.TimeSpan>>()
extern TypeInfo t20_TI;
extern TypeInfo t4981_TI;
extern TypeInfo t3444_TI;
static Il2CppRGCTXData m25840_RGCTXData[2] = 
{
	&t3444_TI/* Class Usage */,
	&m19090_MI/* Method Usage */,
};
extern Il2CppType t4981_0_0_0;
extern Il2CppGenericMethod m25840_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25840_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4981_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25840_RGCTXData, NULL, &m25840_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25841_MI;
struct t20;
extern MethodInfo m25842_MI;
struct t20;
#define m25842(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.IEquatable`1<System.TimeSpan>>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t2100_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25842_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t2100_0_0_0;
extern Il2CppType t2100_0_0_0;
extern Il2CppGenericMethod m25842_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25842_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t2100_0_0_0, RuntimeInvoker_t29_t44, t20_m25842_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25842_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.IEquatable`1<System.TimeSpan>>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2100_1_0_2;
extern Il2CppType t2100_1_0_0;
static ParameterInfo t20_m25841_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2100_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25841_GM;
extern void* RuntimeInvoker_t21_t44_t4982 (MethodInfo* method, void* obj, void** args);
MethodInfo m25841_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4982, t20_m25841_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25841_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25843_MI;
struct t20;
#define m25843(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.IEquatable`1<System.TimeSpan>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2100_0_0_0;
extern Il2CppType t2100_0_0_0;
static ParameterInfo t20_m25843_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t2100_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25843_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25843_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25843_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25843_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25844_MI;
struct t20;
#define m25844(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.IEquatable`1<System.TimeSpan>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2100_0_0_0;
static ParameterInfo t20_m25844_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t2100_0_0_0},
};
extern TypeInfo t2100_TI;
static Il2CppRGCTXData m25844_RGCTXData[1] = 
{
	&t2100_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25844_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25844_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25844_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25844_RGCTXData, NULL, &m25844_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25845_MI;
struct t20;
#define m25845(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.IEquatable`1<System.TimeSpan>>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3693_0_0_0;
extern Il2CppType t3693_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25845_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3693_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25845_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25845_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25845_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25845_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25846_MI;
struct t20;
#define m25846(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.IEquatable`1<System.TimeSpan>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2100_0_0_0;
static ParameterInfo t20_m25846_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t2100_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25846_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25846_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25846_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25846_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25847_MI;
struct t20;
#define m25847(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.IEquatable`1<System.TimeSpan>>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2100_0_0_0;
static ParameterInfo t20_m25847_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t2100_0_0_0},
};
extern TypeInfo t2100_TI;
static Il2CppRGCTXData m25847_RGCTXData[1] = 
{
	&t2100_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25847_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25847_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25847_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25847_RGCTXData, NULL, &m25847_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25848_MI;
struct t20;
#define m25848(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.IEquatable`1<System.TimeSpan>>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2100_0_0_0;
static ParameterInfo t20_m25848_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t2100_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25848_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25848_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25848_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25848_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25849_MI;
struct t20;
extern MethodInfo m25850_MI;
struct t20;
#define m25850(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.IEquatable`1<System.TimeSpan>>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2100_0_0_0;
static ParameterInfo t20_m25850_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t2100_0_0_0},
};
extern TypeInfo t2100_TI;
static Il2CppRGCTXData m25850_RGCTXData[1] = 
{
	&t2100_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25850_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25850_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25850_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25850_RGCTXData, NULL, &m25850_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.IEquatable`1<System.TimeSpan>>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2100_1_0_0;
static ParameterInfo t20_m25849_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2100_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25849_GM;
extern void* RuntimeInvoker_t21_t44_t4982 (MethodInfo* method, void* obj, void** args);
MethodInfo m25849_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4982, t20_m25849_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25849_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3445.h"
extern TypeInfo t3445_TI;
#include "t3445MD.h"
extern MethodInfo m19095_MI;
extern MethodInfo m25851_MI;
struct t20;
#define m25851(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.IEquatable`1<System.TimeSpan>>()
extern TypeInfo t20_TI;
extern TypeInfo t4983_TI;
extern TypeInfo t3445_TI;
static Il2CppRGCTXData m25851_RGCTXData[2] = 
{
	&t3445_TI/* Class Usage */,
	&m19095_MI/* Method Usage */,
};
extern Il2CppType t4983_0_0_0;
extern Il2CppGenericMethod m25851_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25851_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4983_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25851_RGCTXData, NULL, &m25851_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1495.h"
extern MethodInfo m25852_MI;
struct t20;
extern MethodInfo m25853_MI;
struct t20;
 int32_t m25853 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25853 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1495_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25853_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1495_0_0_0;
extern Il2CppType t1495_0_0_0;
extern Il2CppGenericMethod m25853_GM;
extern void* RuntimeInvoker_t1495_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25853_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25853, &t20_TI, &t1495_0_0_0, RuntimeInvoker_t1495_t44, t20_m25853_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25853_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1495_1_0_2;
extern Il2CppType t1495_1_0_0;
static ParameterInfo t20_m25852_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1495_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25852_GM;
extern void* RuntimeInvoker_t21_t44_t4984 (MethodInfo* method, void* obj, void** args);
MethodInfo m25852_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4984, t20_m25852_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25852_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25854_MI;
struct t20;
 void m25854 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25854 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1495_0_0_0;
extern Il2CppType t1495_0_0_0;
static ParameterInfo t20_m25854_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25854_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25854_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25854, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25854_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25854_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25855_MI;
struct t20;
 bool m25855 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25855 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1495_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1495_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1495_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1495_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1495_0_0_0;
static ParameterInfo t20_m25855_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25855_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25855_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25855, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25855_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25855_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25856_MI;
struct t20;
 void m25856 (t20 * __this, t3694* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25856 (t20 * __this, t3694* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3694_0_0_0;
extern Il2CppType t3694_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25856_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3694_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25856_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25856_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25856, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25856_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25856_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25857_MI;
struct t20;
 bool m25857 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25857 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1495_0_0_0;
static ParameterInfo t20_m25857_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25857_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25857_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25857, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25857_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25857_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25858_MI;
struct t20;
 int32_t m25858 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25858 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1495_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1495_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1495_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1495_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1495_0_0_0;
static ParameterInfo t20_m25858_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25858_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25858_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25858, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25858_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25858_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25859_MI;
struct t20;
 void m25859 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25859 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1495_0_0_0;
static ParameterInfo t20_m25859_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25859_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25859_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25859, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25859_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25859_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25860_MI;
struct t20;
extern MethodInfo m25861_MI;
struct t20;
 void m25861 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25861 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1495_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1495_0_0_0;
static ParameterInfo t20_m25861_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1495_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25861_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25861_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25861, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25861_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25861_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1495_1_0_0;
static ParameterInfo t20_m25860_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1495_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25860_GM;
extern void* RuntimeInvoker_t21_t44_t4984 (MethodInfo* method, void* obj, void** args);
MethodInfo m25860_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4984, t20_m25860_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25860_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3446.h"
extern TypeInfo t3446_TI;
#include "t3446MD.h"
extern MethodInfo m19100_MI;
extern MethodInfo m25862_MI;
struct t20;
 t29* m25862 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25862 (t20 * __this, MethodInfo* method){
	{
		t3446  L_0 = {0};
		m19100(&L_0, __this, &m19100_MI);
		t3446  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3446_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.FormatterAssemblyStyle>()
extern TypeInfo t20_TI;
extern TypeInfo t4985_TI;
extern Il2CppType t4985_0_0_0;
extern Il2CppGenericMethod m25862_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25862_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25862, &t20_TI, &t4985_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25862_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1496.h"
extern MethodInfo m25863_MI;
struct t20;
extern MethodInfo m25864_MI;
struct t20;
 int32_t m25864 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25864 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.FormatterTypeStyle>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1496_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25864_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1496_0_0_0;
extern Il2CppType t1496_0_0_0;
extern Il2CppGenericMethod m25864_GM;
extern void* RuntimeInvoker_t1496_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25864_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25864, &t20_TI, &t1496_0_0_0, RuntimeInvoker_t1496_t44, t20_m25864_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25864_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.FormatterTypeStyle>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1496_1_0_2;
extern Il2CppType t1496_1_0_0;
static ParameterInfo t20_m25863_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1496_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25863_GM;
extern void* RuntimeInvoker_t21_t44_t4986 (MethodInfo* method, void* obj, void** args);
MethodInfo m25863_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4986, t20_m25863_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25863_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25865_MI;
struct t20;
 void m25865 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25865 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.FormatterTypeStyle>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1496_0_0_0;
extern Il2CppType t1496_0_0_0;
static ParameterInfo t20_m25865_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1496_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25865_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25865_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25865, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25865_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25865_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25866_MI;
struct t20;
 bool m25866 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25866 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1496_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1496_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1496_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1496_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.FormatterTypeStyle>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1496_0_0_0;
static ParameterInfo t20_m25866_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1496_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25866_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25866_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25866, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25866_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25866_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25867_MI;
struct t20;
 void m25867 (t20 * __this, t3695* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25867 (t20 * __this, t3695* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.FormatterTypeStyle>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3695_0_0_0;
extern Il2CppType t3695_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25867_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3695_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25867_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25867_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25867, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25867_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25867_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25868_MI;
struct t20;
 bool m25868 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25868 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.FormatterTypeStyle>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1496_0_0_0;
static ParameterInfo t20_m25868_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1496_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25868_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25868_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25868, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25868_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25868_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25869_MI;
struct t20;
 int32_t m25869 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25869 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1496_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1496_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1496_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1496_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.FormatterTypeStyle>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1496_0_0_0;
static ParameterInfo t20_m25869_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1496_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25869_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25869_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25869, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25869_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25869_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25870_MI;
struct t20;
 void m25870 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25870 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.FormatterTypeStyle>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1496_0_0_0;
static ParameterInfo t20_m25870_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1496_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25870_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25870_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25870, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25870_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25870_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25871_MI;
struct t20;
extern MethodInfo m25872_MI;
struct t20;
 void m25872 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25872 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1496_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.FormatterTypeStyle>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1496_0_0_0;
static ParameterInfo t20_m25872_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1496_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25872_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25872_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25872, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25872_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25872_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.FormatterTypeStyle>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1496_1_0_0;
static ParameterInfo t20_m25871_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1496_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25871_GM;
extern void* RuntimeInvoker_t21_t44_t4986 (MethodInfo* method, void* obj, void** args);
MethodInfo m25871_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4986, t20_m25871_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25871_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3447.h"
extern TypeInfo t3447_TI;
#include "t3447MD.h"
extern MethodInfo m19105_MI;
extern MethodInfo m25873_MI;
struct t20;
 t29* m25873 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25873 (t20 * __this, MethodInfo* method){
	{
		t3447  L_0 = {0};
		m19105(&L_0, __this, &m19105_MI);
		t3447  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3447_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.FormatterTypeStyle>()
extern TypeInfo t20_TI;
extern TypeInfo t4987_TI;
extern Il2CppType t4987_0_0_0;
extern Il2CppGenericMethod m25873_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25873_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25873, &t20_TI, &t4987_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25873_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1497.h"
extern MethodInfo m25874_MI;
struct t20;
extern MethodInfo m25875_MI;
struct t20;
 int32_t m25875 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25875 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.TypeFilterLevel>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1497_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25875_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1497_0_0_0;
extern Il2CppType t1497_0_0_0;
extern Il2CppGenericMethod m25875_GM;
extern void* RuntimeInvoker_t1497_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25875_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25875, &t20_TI, &t1497_0_0_0, RuntimeInvoker_t1497_t44, t20_m25875_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25875_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.TypeFilterLevel>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1497_1_0_2;
extern Il2CppType t1497_1_0_0;
static ParameterInfo t20_m25874_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1497_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25874_GM;
extern void* RuntimeInvoker_t21_t44_t4988 (MethodInfo* method, void* obj, void** args);
MethodInfo m25874_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4988, t20_m25874_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25874_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25876_MI;
struct t20;
 void m25876 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25876 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.Formatters.TypeFilterLevel>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1497_0_0_0;
extern Il2CppType t1497_0_0_0;
static ParameterInfo t20_m25876_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1497_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25876_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25876_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25876, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25876_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25876_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25877_MI;
struct t20;
 bool m25877 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25877 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1497_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1497_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1497_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1497_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.Formatters.TypeFilterLevel>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1497_0_0_0;
static ParameterInfo t20_m25877_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1497_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25877_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25877_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25877, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25877_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25877_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25878_MI;
struct t20;
 void m25878 (t20 * __this, t3696* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25878 (t20 * __this, t3696* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.Formatters.TypeFilterLevel>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3696_0_0_0;
extern Il2CppType t3696_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25878_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3696_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25878_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25878_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25878, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25878_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25878_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25879_MI;
struct t20;
 bool m25879 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25879 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.Formatters.TypeFilterLevel>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1497_0_0_0;
static ParameterInfo t20_m25879_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1497_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25879_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25879_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25879, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25879_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25879_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25880_MI;
struct t20;
 int32_t m25880 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25880 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1497_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1497_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1497_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1497_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.Formatters.TypeFilterLevel>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1497_0_0_0;
static ParameterInfo t20_m25880_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1497_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25880_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25880_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25880, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25880_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25880_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25881_MI;
struct t20;
 void m25881 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25881 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.Formatters.TypeFilterLevel>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1497_0_0_0;
static ParameterInfo t20_m25881_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1497_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25881_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25881_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25881, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25881_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25881_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25882_MI;
struct t20;
extern MethodInfo m25883_MI;
struct t20;
 void m25883 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25883 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1497_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.Formatters.TypeFilterLevel>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1497_0_0_0;
static ParameterInfo t20_m25883_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1497_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25883_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25883_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25883, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25883_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25883_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.Formatters.TypeFilterLevel>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1497_1_0_0;
static ParameterInfo t20_m25882_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1497_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25882_GM;
extern void* RuntimeInvoker_t21_t44_t4988 (MethodInfo* method, void* obj, void** args);
MethodInfo m25882_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4988, t20_m25882_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25882_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3448.h"
extern TypeInfo t3448_TI;
#include "t3448MD.h"
extern MethodInfo m19110_MI;
extern MethodInfo m25884_MI;
struct t20;
 t29* m25884 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25884 (t20 * __this, MethodInfo* method){
	{
		t3448  L_0 = {0};
		m19110(&L_0, __this, &m19110_MI);
		t3448  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3448_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.Formatters.TypeFilterLevel>()
extern TypeInfo t20_TI;
extern TypeInfo t4989_TI;
extern Il2CppType t4989_0_0_0;
extern Il2CppGenericMethod m25884_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25884_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25884, &t20_TI, &t4989_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25884_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1513.h"
extern MethodInfo m25885_MI;
struct t20;
extern MethodInfo m25886_MI;
struct t20;
 uint8_t m25886 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m25886 (t20 * __this, int32_t p0, MethodInfo* method){
	uint8_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.ObjectRecordStatus>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1513_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25886_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1513_0_0_0;
extern Il2CppType t1513_0_0_0;
extern Il2CppGenericMethod m25886_GM;
extern void* RuntimeInvoker_t1513_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25886_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25886, &t20_TI, &t1513_0_0_0, RuntimeInvoker_t1513_t44, t20_m25886_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25886_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.ObjectRecordStatus>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1513_1_0_2;
extern Il2CppType t1513_1_0_0;
static ParameterInfo t20_m25885_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1513_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25885_GM;
extern void* RuntimeInvoker_t21_t44_t4990 (MethodInfo* method, void* obj, void** args);
MethodInfo m25885_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4990, t20_m25885_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25885_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25887_MI;
struct t20;
 void m25887 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25887 (t20 * __this, uint8_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.ObjectRecordStatus>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1513_0_0_0;
extern Il2CppType t1513_0_0_0;
static ParameterInfo t20_m25887_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1513_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25887_GM;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25887_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25887, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t20_m25887_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25887_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25888_MI;
struct t20;
 bool m25888 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25888 (t20 * __this, uint8_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1513_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		uint8_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1513_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		uint8_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1513_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1513_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.ObjectRecordStatus>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1513_0_0_0;
static ParameterInfo t20_m25888_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1513_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25888_GM;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25888_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25888, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t20_m25888_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25888_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25889_MI;
struct t20;
 void m25889 (t20 * __this, t3697* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25889 (t20 * __this, t3697* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.ObjectRecordStatus>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3697_0_0_0;
extern Il2CppType t3697_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25889_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3697_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25889_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25889_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25889, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25889_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25889_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25890_MI;
struct t20;
 bool m25890 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25890 (t20 * __this, uint8_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.ObjectRecordStatus>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1513_0_0_0;
static ParameterInfo t20_m25890_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1513_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25890_GM;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25890_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25890, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t20_m25890_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25890_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25891_MI;
struct t20;
 int32_t m25891 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25891 (t20 * __this, uint8_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1513_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		uint8_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1513_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		uint8_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1513_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1513_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.ObjectRecordStatus>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1513_0_0_0;
static ParameterInfo t20_m25891_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1513_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25891_GM;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25891_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25891, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t20_m25891_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25891_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25892_MI;
struct t20;
 void m25892 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25892 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.ObjectRecordStatus>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1513_0_0_0;
static ParameterInfo t20_m25892_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1513_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25892_GM;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25892_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25892, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t20_m25892_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25892_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25893_MI;
struct t20;
extern MethodInfo m25894_MI;
struct t20;
 void m25894 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25894 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		uint8_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1513_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.ObjectRecordStatus>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1513_0_0_0;
static ParameterInfo t20_m25894_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1513_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25894_GM;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m25894_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25894, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t20_m25894_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25894_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.ObjectRecordStatus>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1513_1_0_0;
static ParameterInfo t20_m25893_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1513_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25893_GM;
extern void* RuntimeInvoker_t21_t44_t4990 (MethodInfo* method, void* obj, void** args);
MethodInfo m25893_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4990, t20_m25893_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25893_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3449.h"
extern TypeInfo t3449_TI;
#include "t3449MD.h"
extern MethodInfo m19115_MI;
extern MethodInfo m25895_MI;
struct t20;
 t29* m25895 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25895 (t20 * __this, MethodInfo* method){
	{
		t3449  L_0 = {0};
		m19115(&L_0, __this, &m19115_MI);
		t3449  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3449_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.ObjectRecordStatus>()
extern TypeInfo t20_TI;
extern TypeInfo t4991_TI;
extern Il2CppType t4991_0_0_0;
extern Il2CppGenericMethod m25895_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25895_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25895, &t20_TI, &t4991_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25895_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1514.h"
extern MethodInfo m25896_MI;
struct t20;
extern MethodInfo m25897_MI;
struct t20;
#define m25897(__this, p0, method) (t1514 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.OnDeserializedAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1514_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25897_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1514_0_0_0;
extern Il2CppType t1514_0_0_0;
extern Il2CppGenericMethod m25897_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25897_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1514_0_0_0, RuntimeInvoker_t29_t44, t20_m25897_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25897_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.OnDeserializedAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1514_1_0_2;
extern Il2CppType t1514_1_0_0;
static ParameterInfo t20_m25896_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1514_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25896_GM;
extern void* RuntimeInvoker_t21_t44_t4992 (MethodInfo* method, void* obj, void** args);
MethodInfo m25896_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4992, t20_m25896_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25896_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25898_MI;
struct t20;
#define m25898(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.OnDeserializedAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1514_0_0_0;
extern Il2CppType t1514_0_0_0;
static ParameterInfo t20_m25898_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1514_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25898_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25898_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25898_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25898_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25899_MI;
struct t20;
#define m25899(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.OnDeserializedAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1514_0_0_0;
static ParameterInfo t20_m25899_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1514_0_0_0},
};
extern TypeInfo t1514_TI;
static Il2CppRGCTXData m25899_RGCTXData[1] = 
{
	&t1514_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25899_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25899_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25899_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25899_RGCTXData, NULL, &m25899_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25900_MI;
struct t20;
#define m25900(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.OnDeserializedAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3698_0_0_0;
extern Il2CppType t3698_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25900_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3698_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25900_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25900_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25900_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25900_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25901_MI;
struct t20;
#define m25901(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.OnDeserializedAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1514_0_0_0;
static ParameterInfo t20_m25901_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1514_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25901_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25901_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25901_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25901_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25902_MI;
struct t20;
#define m25902(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.OnDeserializedAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1514_0_0_0;
static ParameterInfo t20_m25902_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1514_0_0_0},
};
extern TypeInfo t1514_TI;
static Il2CppRGCTXData m25902_RGCTXData[1] = 
{
	&t1514_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25902_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25902_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25902_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25902_RGCTXData, NULL, &m25902_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25903_MI;
struct t20;
#define m25903(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.OnDeserializedAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1514_0_0_0;
static ParameterInfo t20_m25903_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1514_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25903_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25903_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25903_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25903_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25904_MI;
struct t20;
extern MethodInfo m25905_MI;
struct t20;
#define m25905(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.OnDeserializedAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1514_0_0_0;
static ParameterInfo t20_m25905_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1514_0_0_0},
};
extern TypeInfo t1514_TI;
static Il2CppRGCTXData m25905_RGCTXData[1] = 
{
	&t1514_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25905_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25905_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25905_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25905_RGCTXData, NULL, &m25905_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.OnDeserializedAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1514_1_0_0;
static ParameterInfo t20_m25904_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1514_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25904_GM;
extern void* RuntimeInvoker_t21_t44_t4992 (MethodInfo* method, void* obj, void** args);
MethodInfo m25904_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4992, t20_m25904_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25904_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3450.h"
extern TypeInfo t3450_TI;
#include "t3450MD.h"
extern MethodInfo m19120_MI;
extern MethodInfo m25906_MI;
struct t20;
#define m25906(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.OnDeserializedAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4993_TI;
extern TypeInfo t3450_TI;
static Il2CppRGCTXData m25906_RGCTXData[2] = 
{
	&t3450_TI/* Class Usage */,
	&m19120_MI/* Method Usage */,
};
extern Il2CppType t4993_0_0_0;
extern Il2CppGenericMethod m25906_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25906_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4993_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25906_RGCTXData, NULL, &m25906_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1515.h"
extern MethodInfo m25907_MI;
struct t20;
extern MethodInfo m25908_MI;
struct t20;
#define m25908(__this, p0, method) (t1515 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.OnDeserializingAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1515_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25908_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1515_0_0_0;
extern Il2CppType t1515_0_0_0;
extern Il2CppGenericMethod m25908_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25908_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1515_0_0_0, RuntimeInvoker_t29_t44, t20_m25908_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25908_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.OnDeserializingAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1515_1_0_2;
extern Il2CppType t1515_1_0_0;
static ParameterInfo t20_m25907_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1515_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25907_GM;
extern void* RuntimeInvoker_t21_t44_t4994 (MethodInfo* method, void* obj, void** args);
MethodInfo m25907_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4994, t20_m25907_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25907_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25909_MI;
struct t20;
#define m25909(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.OnDeserializingAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1515_0_0_0;
extern Il2CppType t1515_0_0_0;
static ParameterInfo t20_m25909_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1515_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25909_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25909_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25909_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25909_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25910_MI;
struct t20;
#define m25910(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.OnDeserializingAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1515_0_0_0;
static ParameterInfo t20_m25910_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1515_0_0_0},
};
extern TypeInfo t1515_TI;
static Il2CppRGCTXData m25910_RGCTXData[1] = 
{
	&t1515_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25910_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25910_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25910_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25910_RGCTXData, NULL, &m25910_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25911_MI;
struct t20;
#define m25911(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.OnDeserializingAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3699_0_0_0;
extern Il2CppType t3699_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25911_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3699_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25911_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25911_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25911_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25911_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25912_MI;
struct t20;
#define m25912(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.OnDeserializingAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1515_0_0_0;
static ParameterInfo t20_m25912_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1515_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25912_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25912_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25912_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25912_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25913_MI;
struct t20;
#define m25913(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.OnDeserializingAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1515_0_0_0;
static ParameterInfo t20_m25913_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1515_0_0_0},
};
extern TypeInfo t1515_TI;
static Il2CppRGCTXData m25913_RGCTXData[1] = 
{
	&t1515_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25913_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25913_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25913_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25913_RGCTXData, NULL, &m25913_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25914_MI;
struct t20;
#define m25914(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.OnDeserializingAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1515_0_0_0;
static ParameterInfo t20_m25914_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1515_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25914_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25914_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25914_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25914_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25915_MI;
struct t20;
extern MethodInfo m25916_MI;
struct t20;
#define m25916(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.OnDeserializingAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1515_0_0_0;
static ParameterInfo t20_m25916_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1515_0_0_0},
};
extern TypeInfo t1515_TI;
static Il2CppRGCTXData m25916_RGCTXData[1] = 
{
	&t1515_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25916_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25916_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25916_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25916_RGCTXData, NULL, &m25916_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.OnDeserializingAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1515_1_0_0;
static ParameterInfo t20_m25915_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1515_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25915_GM;
extern void* RuntimeInvoker_t21_t44_t4994 (MethodInfo* method, void* obj, void** args);
MethodInfo m25915_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4994, t20_m25915_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25915_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3451.h"
extern TypeInfo t3451_TI;
#include "t3451MD.h"
extern MethodInfo m19125_MI;
extern MethodInfo m25917_MI;
struct t20;
#define m25917(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.OnDeserializingAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4995_TI;
extern TypeInfo t3451_TI;
static Il2CppRGCTXData m25917_RGCTXData[2] = 
{
	&t3451_TI/* Class Usage */,
	&m19125_MI/* Method Usage */,
};
extern Il2CppType t4995_0_0_0;
extern Il2CppGenericMethod m25917_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25917_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4995_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25917_RGCTXData, NULL, &m25917_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1516.h"
extern MethodInfo m25918_MI;
struct t20;
extern MethodInfo m25919_MI;
struct t20;
#define m25919(__this, p0, method) (t1516 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.OnSerializedAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1516_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25919_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1516_0_0_0;
extern Il2CppType t1516_0_0_0;
extern Il2CppGenericMethod m25919_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25919_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1516_0_0_0, RuntimeInvoker_t29_t44, t20_m25919_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25919_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.OnSerializedAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1516_1_0_2;
extern Il2CppType t1516_1_0_0;
static ParameterInfo t20_m25918_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1516_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25918_GM;
extern void* RuntimeInvoker_t21_t44_t4996 (MethodInfo* method, void* obj, void** args);
MethodInfo m25918_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4996, t20_m25918_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25918_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25920_MI;
struct t20;
#define m25920(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.OnSerializedAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1516_0_0_0;
extern Il2CppType t1516_0_0_0;
static ParameterInfo t20_m25920_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1516_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25920_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25920_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25920_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25920_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25921_MI;
struct t20;
#define m25921(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.OnSerializedAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1516_0_0_0;
static ParameterInfo t20_m25921_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1516_0_0_0},
};
extern TypeInfo t1516_TI;
static Il2CppRGCTXData m25921_RGCTXData[1] = 
{
	&t1516_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25921_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25921_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25921_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25921_RGCTXData, NULL, &m25921_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25922_MI;
struct t20;
#define m25922(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.OnSerializedAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3700_0_0_0;
extern Il2CppType t3700_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25922_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3700_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25922_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25922_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25922_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25922_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25923_MI;
struct t20;
#define m25923(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.OnSerializedAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1516_0_0_0;
static ParameterInfo t20_m25923_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1516_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25923_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25923_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25923_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25923_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25924_MI;
struct t20;
#define m25924(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.OnSerializedAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1516_0_0_0;
static ParameterInfo t20_m25924_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1516_0_0_0},
};
extern TypeInfo t1516_TI;
static Il2CppRGCTXData m25924_RGCTXData[1] = 
{
	&t1516_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25924_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25924_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25924_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25924_RGCTXData, NULL, &m25924_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25925_MI;
struct t20;
#define m25925(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.OnSerializedAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1516_0_0_0;
static ParameterInfo t20_m25925_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1516_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25925_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25925_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25925_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25925_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25926_MI;
struct t20;
extern MethodInfo m25927_MI;
struct t20;
#define m25927(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.OnSerializedAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1516_0_0_0;
static ParameterInfo t20_m25927_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1516_0_0_0},
};
extern TypeInfo t1516_TI;
static Il2CppRGCTXData m25927_RGCTXData[1] = 
{
	&t1516_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25927_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25927_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25927_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25927_RGCTXData, NULL, &m25927_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.OnSerializedAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1516_1_0_0;
static ParameterInfo t20_m25926_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1516_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25926_GM;
extern void* RuntimeInvoker_t21_t44_t4996 (MethodInfo* method, void* obj, void** args);
MethodInfo m25926_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4996, t20_m25926_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25926_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3452.h"
extern TypeInfo t3452_TI;
#include "t3452MD.h"
extern MethodInfo m19130_MI;
extern MethodInfo m25928_MI;
struct t20;
#define m25928(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.OnSerializedAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4997_TI;
extern TypeInfo t3452_TI;
static Il2CppRGCTXData m25928_RGCTXData[2] = 
{
	&t3452_TI/* Class Usage */,
	&m19130_MI/* Method Usage */,
};
extern Il2CppType t4997_0_0_0;
extern Il2CppGenericMethod m25928_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25928_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4997_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25928_RGCTXData, NULL, &m25928_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1517.h"
extern MethodInfo m25929_MI;
struct t20;
extern MethodInfo m25930_MI;
struct t20;
#define m25930(__this, p0, method) (t1517 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.OnSerializingAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1517_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25930_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1517_0_0_0;
extern Il2CppType t1517_0_0_0;
extern Il2CppGenericMethod m25930_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25930_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1517_0_0_0, RuntimeInvoker_t29_t44, t20_m25930_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25930_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.OnSerializingAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1517_1_0_2;
extern Il2CppType t1517_1_0_0;
static ParameterInfo t20_m25929_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1517_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25929_GM;
extern void* RuntimeInvoker_t21_t44_t4998 (MethodInfo* method, void* obj, void** args);
MethodInfo m25929_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4998, t20_m25929_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25929_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25931_MI;
struct t20;
#define m25931(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.OnSerializingAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1517_0_0_0;
extern Il2CppType t1517_0_0_0;
static ParameterInfo t20_m25931_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1517_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25931_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25931_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25931_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25931_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25932_MI;
struct t20;
#define m25932(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.OnSerializingAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1517_0_0_0;
static ParameterInfo t20_m25932_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1517_0_0_0},
};
extern TypeInfo t1517_TI;
static Il2CppRGCTXData m25932_RGCTXData[1] = 
{
	&t1517_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25932_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25932_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25932_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25932_RGCTXData, NULL, &m25932_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25933_MI;
struct t20;
#define m25933(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.OnSerializingAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3701_0_0_0;
extern Il2CppType t3701_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25933_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3701_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25933_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25933_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25933_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25933_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25934_MI;
struct t20;
#define m25934(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.OnSerializingAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1517_0_0_0;
static ParameterInfo t20_m25934_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1517_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25934_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25934_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25934_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25934_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25935_MI;
struct t20;
#define m25935(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.OnSerializingAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1517_0_0_0;
static ParameterInfo t20_m25935_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1517_0_0_0},
};
extern TypeInfo t1517_TI;
static Il2CppRGCTXData m25935_RGCTXData[1] = 
{
	&t1517_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25935_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25935_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m25935_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25935_RGCTXData, NULL, &m25935_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25936_MI;
struct t20;
#define m25936(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.OnSerializingAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1517_0_0_0;
static ParameterInfo t20_m25936_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1517_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25936_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25936_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25936_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25936_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25937_MI;
struct t20;
extern MethodInfo m25938_MI;
struct t20;
#define m25938(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.OnSerializingAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1517_0_0_0;
static ParameterInfo t20_m25938_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1517_0_0_0},
};
extern TypeInfo t1517_TI;
static Il2CppRGCTXData m25938_RGCTXData[1] = 
{
	&t1517_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25938_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25938_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m25938_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m25938_RGCTXData, NULL, &m25938_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.OnSerializingAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1517_1_0_0;
static ParameterInfo t20_m25937_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1517_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25937_GM;
extern void* RuntimeInvoker_t21_t44_t4998 (MethodInfo* method, void* obj, void** args);
MethodInfo m25937_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t4998, t20_m25937_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25937_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3453.h"
extern TypeInfo t3453_TI;
#include "t3453MD.h"
extern MethodInfo m19135_MI;
extern MethodInfo m25939_MI;
struct t20;
#define m25939(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.OnSerializingAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t4999_TI;
extern TypeInfo t3453_TI;
static Il2CppRGCTXData m25939_RGCTXData[2] = 
{
	&t3453_TI/* Class Usage */,
	&m19135_MI/* Method Usage */,
};
extern Il2CppType t4999_0_0_0;
extern Il2CppGenericMethod m25939_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25939_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t4999_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m25939_RGCTXData, NULL, &m25939_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1523.h"
extern MethodInfo m25940_MI;
struct t20;
extern MethodInfo m25941_MI;
struct t20;
 int32_t m25941 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25941 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Runtime.Serialization.StreamingContextStates>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1523_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25941_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1523_0_0_0;
extern Il2CppType t1523_0_0_0;
extern Il2CppGenericMethod m25941_GM;
extern void* RuntimeInvoker_t1523_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25941_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25941, &t20_TI, &t1523_0_0_0, RuntimeInvoker_t1523_t44, t20_m25941_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25941_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.StreamingContextStates>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1523_1_0_2;
extern Il2CppType t1523_1_0_0;
static ParameterInfo t20_m25940_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1523_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25940_GM;
extern void* RuntimeInvoker_t21_t44_t5000 (MethodInfo* method, void* obj, void** args);
MethodInfo m25940_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5000, t20_m25940_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25940_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25942_MI;
struct t20;
 void m25942 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25942 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Runtime.Serialization.StreamingContextStates>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1523_0_0_0;
extern Il2CppType t1523_0_0_0;
static ParameterInfo t20_m25942_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1523_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25942_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25942_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25942, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25942_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25942_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25943_MI;
struct t20;
 bool m25943 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25943 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1523_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1523_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1523_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1523_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Runtime.Serialization.StreamingContextStates>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1523_0_0_0;
static ParameterInfo t20_m25943_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1523_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25943_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25943_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25943, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25943_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25943_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25944_MI;
struct t20;
 void m25944 (t20 * __this, t3702* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25944 (t20 * __this, t3702* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Runtime.Serialization.StreamingContextStates>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3702_0_0_0;
extern Il2CppType t3702_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25944_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3702_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25944_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25944_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25944, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25944_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25944_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25945_MI;
struct t20;
 bool m25945 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25945 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Runtime.Serialization.StreamingContextStates>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1523_0_0_0;
static ParameterInfo t20_m25945_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1523_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25945_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25945_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25945, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25945_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25945_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25946_MI;
struct t20;
 int32_t m25946 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25946 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1523_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1523_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1523_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1523_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Runtime.Serialization.StreamingContextStates>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1523_0_0_0;
static ParameterInfo t20_m25946_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1523_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25946_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25946_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25946, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25946_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25946_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25947_MI;
struct t20;
 void m25947 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25947 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Runtime.Serialization.StreamingContextStates>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1523_0_0_0;
static ParameterInfo t20_m25947_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1523_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25947_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25947_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25947, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25947_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25947_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25948_MI;
struct t20;
extern MethodInfo m25949_MI;
struct t20;
 void m25949 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25949 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1523_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Runtime.Serialization.StreamingContextStates>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1523_0_0_0;
static ParameterInfo t20_m25949_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1523_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25949_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25949_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25949, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25949_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25949_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Runtime.Serialization.StreamingContextStates>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1523_1_0_0;
static ParameterInfo t20_m25948_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1523_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25948_GM;
extern void* RuntimeInvoker_t21_t44_t5000 (MethodInfo* method, void* obj, void** args);
MethodInfo m25948_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5000, t20_m25948_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25948_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3454.h"
extern TypeInfo t3454_TI;
#include "t3454MD.h"
extern MethodInfo m19140_MI;
extern MethodInfo m25950_MI;
struct t20;
 t29* m25950 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25950 (t20 * __this, MethodInfo* method){
	{
		t3454  L_0 = {0};
		m19140(&L_0, __this, &m19140_MI);
		t3454  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3454_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Runtime.Serialization.StreamingContextStates>()
extern TypeInfo t20_TI;
extern TypeInfo t5001_TI;
extern Il2CppType t5001_0_0_0;
extern Il2CppGenericMethod m25950_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25950_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25950, &t20_TI, &t5001_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25950_GM};
#ifndef _MSC_VER
#else
#endif

#include "t795.h"
extern MethodInfo m25951_MI;
struct t20;
extern MethodInfo m25952_MI;
struct t20;
 int32_t m25952 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25952 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t795_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25952_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t795_0_0_0;
extern Il2CppType t795_0_0_0;
extern Il2CppGenericMethod m25952_GM;
extern void* RuntimeInvoker_t795_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25952_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25952, &t20_TI, &t795_0_0_0, RuntimeInvoker_t795_t44, t20_m25952_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25952_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t795_1_0_2;
extern Il2CppType t795_1_0_0;
static ParameterInfo t20_m25951_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t795_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25951_GM;
extern void* RuntimeInvoker_t21_t44_t5002 (MethodInfo* method, void* obj, void** args);
MethodInfo m25951_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5002, t20_m25951_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25951_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25953_MI;
struct t20;
 void m25953 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25953 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>(T)
extern TypeInfo t20_TI;
extern Il2CppType t795_0_0_0;
extern Il2CppType t795_0_0_0;
static ParameterInfo t20_m25953_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t795_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25953_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25953_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25953, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25953_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25953_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25954_MI;
struct t20;
 bool m25954 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25954 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t795_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t795_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t795_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t795_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>(T)
extern TypeInfo t20_TI;
extern Il2CppType t795_0_0_0;
static ParameterInfo t20_m25954_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t795_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25954_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25954_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25954, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25954_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25954_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25955_MI;
struct t20;
 void m25955 (t20 * __this, t3703* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25955 (t20 * __this, t3703* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3703_0_0_0;
extern Il2CppType t3703_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25955_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3703_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25955_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25955_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25955, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25955_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25955_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25956_MI;
struct t20;
 bool m25956 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25956 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>(T)
extern TypeInfo t20_TI;
extern Il2CppType t795_0_0_0;
static ParameterInfo t20_m25956_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t795_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25956_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25956_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25956, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25956_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25956_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25957_MI;
struct t20;
 int32_t m25957 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25957 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t795_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t795_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t795_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t795_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>(T)
extern TypeInfo t20_TI;
extern Il2CppType t795_0_0_0;
static ParameterInfo t20_m25957_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t795_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25957_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25957_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25957, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25957_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25957_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25958_MI;
struct t20;
 void m25958 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25958 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t795_0_0_0;
static ParameterInfo t20_m25958_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t795_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25958_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25958_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25958, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25958_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25958_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25959_MI;
struct t20;
extern MethodInfo m25960_MI;
struct t20;
 void m25960 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25960 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t795_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t795_0_0_0;
static ParameterInfo t20_m25960_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t795_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25960_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25960_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25960, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25960_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25960_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t795_1_0_0;
static ParameterInfo t20_m25959_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t795_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25959_GM;
extern void* RuntimeInvoker_t21_t44_t5002 (MethodInfo* method, void* obj, void** args);
MethodInfo m25959_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5002, t20_m25959_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25959_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3455.h"
extern TypeInfo t3455_TI;
#include "t3455MD.h"
extern MethodInfo m19145_MI;
extern MethodInfo m25961_MI;
struct t20;
 t29* m25961 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25961 (t20 * __this, MethodInfo* method){
	{
		t3455  L_0 = {0};
		m19145(&L_0, __this, &m19145_MI);
		t3455  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3455_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Security.Cryptography.X509Certificates.X509KeyStorageFlags>()
extern TypeInfo t20_TI;
extern TypeInfo t5003_TI;
extern Il2CppType t5003_0_0_0;
extern Il2CppGenericMethod m25961_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25961_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25961, &t20_TI, &t5003_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25961_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1023.h"
extern MethodInfo m25962_MI;
struct t20;
extern MethodInfo m25963_MI;
struct t20;
 int32_t m25963 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25963 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Security.Cryptography.CipherMode>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1023_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25963_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1023_0_0_0;
extern Il2CppType t1023_0_0_0;
extern Il2CppGenericMethod m25963_GM;
extern void* RuntimeInvoker_t1023_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25963_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25963, &t20_TI, &t1023_0_0_0, RuntimeInvoker_t1023_t44, t20_m25963_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25963_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Security.Cryptography.CipherMode>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1023_1_0_2;
extern Il2CppType t1023_1_0_0;
static ParameterInfo t20_m25962_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1023_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25962_GM;
extern void* RuntimeInvoker_t21_t44_t5004 (MethodInfo* method, void* obj, void** args);
MethodInfo m25962_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5004, t20_m25962_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25962_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25964_MI;
struct t20;
 void m25964 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25964 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Security.Cryptography.CipherMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1023_0_0_0;
extern Il2CppType t1023_0_0_0;
static ParameterInfo t20_m25964_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1023_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25964_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25964_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25964, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25964_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25964_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25965_MI;
struct t20;
 bool m25965 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25965 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1023_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1023_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1023_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1023_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Security.Cryptography.CipherMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1023_0_0_0;
static ParameterInfo t20_m25965_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1023_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25965_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25965_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25965, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25965_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25965_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25966_MI;
struct t20;
 void m25966 (t20 * __this, t3704* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25966 (t20 * __this, t3704* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Security.Cryptography.CipherMode>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3704_0_0_0;
extern Il2CppType t3704_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25966_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3704_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25966_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25966_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25966, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25966_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25966_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25967_MI;
struct t20;
 bool m25967 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25967 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Security.Cryptography.CipherMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1023_0_0_0;
static ParameterInfo t20_m25967_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1023_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25967_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25967_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25967, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25967_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25967_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25968_MI;
struct t20;
 int32_t m25968 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25968 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1023_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1023_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1023_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1023_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Security.Cryptography.CipherMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1023_0_0_0;
static ParameterInfo t20_m25968_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1023_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25968_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25968_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25968, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25968_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25968_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25969_MI;
struct t20;
 void m25969 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25969 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Security.Cryptography.CipherMode>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1023_0_0_0;
static ParameterInfo t20_m25969_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1023_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25969_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25969_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25969, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25969_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25969_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25970_MI;
struct t20;
extern MethodInfo m25971_MI;
struct t20;
 void m25971 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25971 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1023_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Security.Cryptography.CipherMode>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1023_0_0_0;
static ParameterInfo t20_m25971_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1023_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25971_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25971_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25971, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25971_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25971_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Security.Cryptography.CipherMode>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1023_1_0_0;
static ParameterInfo t20_m25970_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1023_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25970_GM;
extern void* RuntimeInvoker_t21_t44_t5004 (MethodInfo* method, void* obj, void** args);
MethodInfo m25970_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5004, t20_m25970_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25970_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3456.h"
extern TypeInfo t3456_TI;
#include "t3456MD.h"
extern MethodInfo m19150_MI;
extern MethodInfo m25972_MI;
struct t20;
 t29* m25972 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25972 (t20 * __this, MethodInfo* method){
	{
		t3456  L_0 = {0};
		m19150(&L_0, __this, &m19150_MI);
		t3456  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3456_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Security.Cryptography.CipherMode>()
extern TypeInfo t20_TI;
extern TypeInfo t5005_TI;
extern Il2CppType t5005_0_0_0;
extern Il2CppGenericMethod m25972_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25972_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25972, &t20_TI, &t5005_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25972_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1099.h"
extern MethodInfo m25973_MI;
struct t20;
extern MethodInfo m25974_MI;
struct t20;
 int32_t m25974 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25974 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Security.Cryptography.CspProviderFlags>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1099_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25974_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1099_0_0_0;
extern Il2CppType t1099_0_0_0;
extern Il2CppGenericMethod m25974_GM;
extern void* RuntimeInvoker_t1099_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25974_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25974, &t20_TI, &t1099_0_0_0, RuntimeInvoker_t1099_t44, t20_m25974_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25974_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Security.Cryptography.CspProviderFlags>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1099_1_0_2;
extern Il2CppType t1099_1_0_0;
static ParameterInfo t20_m25973_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1099_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25973_GM;
extern void* RuntimeInvoker_t21_t44_t5006 (MethodInfo* method, void* obj, void** args);
MethodInfo m25973_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5006, t20_m25973_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25973_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25975_MI;
struct t20;
 void m25975 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25975 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Security.Cryptography.CspProviderFlags>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1099_0_0_0;
extern Il2CppType t1099_0_0_0;
static ParameterInfo t20_m25975_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1099_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25975_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25975_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25975, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25975_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25975_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25976_MI;
struct t20;
 bool m25976 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25976 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1099_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1099_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1099_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1099_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Security.Cryptography.CspProviderFlags>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1099_0_0_0;
static ParameterInfo t20_m25976_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1099_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25976_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25976_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25976, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25976_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25976_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25977_MI;
struct t20;
 void m25977 (t20 * __this, t3705* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25977 (t20 * __this, t3705* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Security.Cryptography.CspProviderFlags>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3705_0_0_0;
extern Il2CppType t3705_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25977_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3705_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25977_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25977_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25977, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25977_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25977_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25978_MI;
struct t20;
 bool m25978 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25978 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Security.Cryptography.CspProviderFlags>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1099_0_0_0;
static ParameterInfo t20_m25978_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1099_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25978_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25978_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25978, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25978_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25978_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25979_MI;
struct t20;
 int32_t m25979 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25979 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1099_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1099_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1099_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1099_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Security.Cryptography.CspProviderFlags>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1099_0_0_0;
static ParameterInfo t20_m25979_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1099_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25979_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25979_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25979, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25979_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25979_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25980_MI;
struct t20;
 void m25980 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25980 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Security.Cryptography.CspProviderFlags>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1099_0_0_0;
static ParameterInfo t20_m25980_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1099_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25980_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25980_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25980, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25980_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25980_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25981_MI;
struct t20;
extern MethodInfo m25982_MI;
struct t20;
 void m25982 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25982 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1099_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Security.Cryptography.CspProviderFlags>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1099_0_0_0;
static ParameterInfo t20_m25982_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1099_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25982_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25982_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25982, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25982_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25982_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Security.Cryptography.CspProviderFlags>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1099_1_0_0;
static ParameterInfo t20_m25981_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1099_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25981_GM;
extern void* RuntimeInvoker_t21_t44_t5006 (MethodInfo* method, void* obj, void** args);
MethodInfo m25981_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5006, t20_m25981_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25981_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3457.h"
extern TypeInfo t3457_TI;
#include "t3457MD.h"
extern MethodInfo m19155_MI;
extern MethodInfo m25983_MI;
struct t20;
 t29* m25983 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25983 (t20 * __this, MethodInfo* method){
	{
		t3457  L_0 = {0};
		m19155(&L_0, __this, &m19155_MI);
		t3457  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3457_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Security.Cryptography.CspProviderFlags>()
extern TypeInfo t20_TI;
extern TypeInfo t5007_TI;
extern Il2CppType t5007_0_0_0;
extern Il2CppGenericMethod m25983_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25983_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25983, &t20_TI, &t5007_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25983_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1117.h"
extern MethodInfo m25984_MI;
struct t20;
extern MethodInfo m25985_MI;
struct t20;
 int32_t m25985 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25985 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Security.Cryptography.PaddingMode>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1117_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25985_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1117_0_0_0;
extern Il2CppType t1117_0_0_0;
extern Il2CppGenericMethod m25985_GM;
extern void* RuntimeInvoker_t1117_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25985_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m25985, &t20_TI, &t1117_0_0_0, RuntimeInvoker_t1117_t44, t20_m25985_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25985_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Security.Cryptography.PaddingMode>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1117_1_0_2;
extern Il2CppType t1117_1_0_0;
static ParameterInfo t20_m25984_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1117_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25984_GM;
extern void* RuntimeInvoker_t21_t44_t5008 (MethodInfo* method, void* obj, void** args);
MethodInfo m25984_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5008, t20_m25984_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25984_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25986_MI;
struct t20;
 void m25986 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25986 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Security.Cryptography.PaddingMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1117_0_0_0;
extern Il2CppType t1117_0_0_0;
static ParameterInfo t20_m25986_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1117_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25986_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25986_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m25986, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m25986_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25986_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25987_MI;
struct t20;
 bool m25987 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25987 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1117_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1117_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1117_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1117_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Security.Cryptography.PaddingMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1117_0_0_0;
static ParameterInfo t20_m25987_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1117_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25987_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25987_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m25987, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25987_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25987_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25988_MI;
struct t20;
 void m25988 (t20 * __this, t3706* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25988 (t20 * __this, t3706* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Security.Cryptography.PaddingMode>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3706_0_0_0;
extern Il2CppType t3706_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25988_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3706_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25988_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25988_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m25988, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25988_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25988_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25989_MI;
struct t20;
 bool m25989 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m25989 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Security.Cryptography.PaddingMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1117_0_0_0;
static ParameterInfo t20_m25989_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1117_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25989_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25989_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m25989, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m25989_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25989_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25990_MI;
struct t20;
 int32_t m25990 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m25990 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1117_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1117_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1117_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1117_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Security.Cryptography.PaddingMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1117_0_0_0;
static ParameterInfo t20_m25990_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1117_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m25990_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25990_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m25990, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m25990_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25990_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25991_MI;
struct t20;
 void m25991 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25991 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Security.Cryptography.PaddingMode>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1117_0_0_0;
static ParameterInfo t20_m25991_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1117_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25991_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25991_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m25991, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25991_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25991_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25992_MI;
struct t20;
extern MethodInfo m25993_MI;
struct t20;
 void m25993 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m25993 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1117_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Security.Cryptography.PaddingMode>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1117_0_0_0;
static ParameterInfo t20_m25993_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1117_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25993_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25993_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m25993, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m25993_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25993_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Security.Cryptography.PaddingMode>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1117_1_0_0;
static ParameterInfo t20_m25992_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1117_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25992_GM;
extern void* RuntimeInvoker_t21_t44_t5008 (MethodInfo* method, void* obj, void** args);
MethodInfo m25992_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5008, t20_m25992_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25992_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3458.h"
extern TypeInfo t3458_TI;
#include "t3458MD.h"
extern MethodInfo m19160_MI;
extern MethodInfo m25994_MI;
struct t20;
 t29* m25994 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m25994 (t20 * __this, MethodInfo* method){
	{
		t3458  L_0 = {0};
		m19160(&L_0, __this, &m19160_MI);
		t3458  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3458_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Security.Cryptography.PaddingMode>()
extern TypeInfo t20_TI;
extern TypeInfo t5009_TI;
extern Il2CppType t5009_0_0_0;
extern Il2CppGenericMethod m25994_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25994_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m25994, &t20_TI, &t5009_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m25994_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1567.h"
extern MethodInfo m25995_MI;
struct t20;
extern MethodInfo m25996_MI;
struct t20;
#define m25996(__this, p0, method) (t1567 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Security.Policy.StrongName>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1567_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25996_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1567_0_0_0;
extern Il2CppType t1567_0_0_0;
extern Il2CppGenericMethod m25996_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25996_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1567_0_0_0, RuntimeInvoker_t29_t44, t20_m25996_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25996_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Security.Policy.StrongName>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_1_0_2;
extern Il2CppType t1567_1_0_0;
static ParameterInfo t20_m25995_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1567_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25995_GM;
extern void* RuntimeInvoker_t21_t44_t5010 (MethodInfo* method, void* obj, void** args);
MethodInfo m25995_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5010, t20_m25995_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m25995_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25997_MI;
struct t20;
#define m25997(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Security.Policy.StrongName>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1567_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t20_m25997_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25997_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25997_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m25997_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m25997_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25998_MI;
struct t20;
#define m25998(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Security.Policy.StrongName>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t20_m25998_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern TypeInfo t1567_TI;
static Il2CppRGCTXData m25998_RGCTXData[1] = 
{
	&t1567_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m25998_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m25998_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m25998_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m25998_RGCTXData, NULL, &m25998_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m25999_MI;
struct t20;
#define m25999(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Security.Policy.StrongName>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m25999_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m25999_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m25999_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m25999_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m25999_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26000_MI;
struct t20;
#define m26000(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Security.Policy.StrongName>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t20_m26000_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26000_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26000_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26000_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26000_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26001_MI;
struct t20;
#define m26001(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Security.Policy.StrongName>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t20_m26001_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern TypeInfo t1567_TI;
static Il2CppRGCTXData m26001_RGCTXData[1] = 
{
	&t1567_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26001_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26001_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m26001_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26001_RGCTXData, NULL, &m26001_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26002_MI;
struct t20;
#define m26002(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Security.Policy.StrongName>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t20_m26002_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26002_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26002_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26002_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26002_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26003_MI;
struct t20;
extern MethodInfo m26004_MI;
struct t20;
#define m26004(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Security.Policy.StrongName>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_0_0_0;
static ParameterInfo t20_m26004_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1567_0_0_0},
};
extern TypeInfo t1567_TI;
static Il2CppRGCTXData m26004_RGCTXData[1] = 
{
	&t1567_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26004_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26004_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26004_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m26004_RGCTXData, NULL, &m26004_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Security.Policy.StrongName>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1567_1_0_0;
static ParameterInfo t20_m26003_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1567_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26003_GM;
extern void* RuntimeInvoker_t21_t44_t5010 (MethodInfo* method, void* obj, void** args);
MethodInfo m26003_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5010, t20_m26003_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26003_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3459.h"
extern TypeInfo t3459_TI;
#include "t3459MD.h"
extern MethodInfo m19165_MI;
extern MethodInfo m26005_MI;
struct t20;
#define m26005(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Security.Policy.StrongName>()
extern TypeInfo t20_TI;
extern TypeInfo t3463_TI;
extern TypeInfo t3459_TI;
static Il2CppRGCTXData m26005_RGCTXData[2] = 
{
	&t3459_TI/* Class Usage */,
	&m19165_MI/* Method Usage */,
};
extern Il2CppType t3463_0_0_0;
extern Il2CppGenericMethod m26005_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26005_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t3463_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m26005_RGCTXData, NULL, &m26005_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26006_MI;
struct t20;
extern MethodInfo m26007_MI;
struct t20;
#define m26007(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Security.Policy.IBuiltInEvidence>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t2063_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26007_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t2063_0_0_0;
extern Il2CppType t2063_0_0_0;
extern Il2CppGenericMethod m26007_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26007_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t2063_0_0_0, RuntimeInvoker_t29_t44, t20_m26007_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26007_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Security.Policy.IBuiltInEvidence>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2063_1_0_2;
extern Il2CppType t2063_1_0_0;
static ParameterInfo t20_m26006_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2063_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26006_GM;
extern void* RuntimeInvoker_t21_t44_t5011 (MethodInfo* method, void* obj, void** args);
MethodInfo m26006_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5011, t20_m26006_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26006_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26008_MI;
struct t20;
#define m26008(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Security.Policy.IBuiltInEvidence>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2063_0_0_0;
extern Il2CppType t2063_0_0_0;
static ParameterInfo t20_m26008_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t2063_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26008_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26008_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m26008_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26008_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26009_MI;
struct t20;
#define m26009(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Security.Policy.IBuiltInEvidence>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2063_0_0_0;
static ParameterInfo t20_m26009_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t2063_0_0_0},
};
extern TypeInfo t2063_TI;
static Il2CppRGCTXData m26009_RGCTXData[1] = 
{
	&t2063_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26009_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26009_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26009_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26009_RGCTXData, NULL, &m26009_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26010_MI;
struct t20;
#define m26010(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Security.Policy.IBuiltInEvidence>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3707_0_0_0;
extern Il2CppType t3707_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26010_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3707_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26010_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26010_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26010_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26010_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26011_MI;
struct t20;
#define m26011(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Security.Policy.IBuiltInEvidence>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2063_0_0_0;
static ParameterInfo t20_m26011_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t2063_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26011_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26011_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26011_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26011_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26012_MI;
struct t20;
#define m26012(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Security.Policy.IBuiltInEvidence>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2063_0_0_0;
static ParameterInfo t20_m26012_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t2063_0_0_0},
};
extern TypeInfo t2063_TI;
static Il2CppRGCTXData m26012_RGCTXData[1] = 
{
	&t2063_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26012_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26012_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m26012_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26012_RGCTXData, NULL, &m26012_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26013_MI;
struct t20;
#define m26013(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Security.Policy.IBuiltInEvidence>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2063_0_0_0;
static ParameterInfo t20_m26013_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t2063_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26013_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26013_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26013_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26013_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26014_MI;
struct t20;
extern MethodInfo m26015_MI;
struct t20;
#define m26015(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Security.Policy.IBuiltInEvidence>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2063_0_0_0;
static ParameterInfo t20_m26015_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t2063_0_0_0},
};
extern TypeInfo t2063_TI;
static Il2CppRGCTXData m26015_RGCTXData[1] = 
{
	&t2063_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26015_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26015_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26015_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m26015_RGCTXData, NULL, &m26015_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Security.Policy.IBuiltInEvidence>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2063_1_0_0;
static ParameterInfo t20_m26014_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2063_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26014_GM;
extern void* RuntimeInvoker_t21_t44_t5011 (MethodInfo* method, void* obj, void** args);
MethodInfo m26014_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5011, t20_m26014_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26014_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3460.h"
extern TypeInfo t3460_TI;
#include "t3460MD.h"
extern MethodInfo m19170_MI;
extern MethodInfo m26016_MI;
struct t20;
#define m26016(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Security.Policy.IBuiltInEvidence>()
extern TypeInfo t20_TI;
extern TypeInfo t5012_TI;
extern TypeInfo t3460_TI;
static Il2CppRGCTXData m26016_RGCTXData[2] = 
{
	&t3460_TI/* Class Usage */,
	&m19170_MI/* Method Usage */,
};
extern Il2CppType t5012_0_0_0;
extern Il2CppGenericMethod m26016_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26016_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t5012_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m26016_RGCTXData, NULL, &m26016_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26017_MI;
struct t20;
extern MethodInfo m26018_MI;
struct t20;
#define m26018(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Security.Policy.IIdentityPermissionFactory>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t2064_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26018_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t2064_0_0_0;
extern Il2CppType t2064_0_0_0;
extern Il2CppGenericMethod m26018_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26018_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t2064_0_0_0, RuntimeInvoker_t29_t44, t20_m26018_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26018_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Security.Policy.IIdentityPermissionFactory>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2064_1_0_2;
extern Il2CppType t2064_1_0_0;
static ParameterInfo t20_m26017_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2064_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26017_GM;
extern void* RuntimeInvoker_t21_t44_t5013 (MethodInfo* method, void* obj, void** args);
MethodInfo m26017_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5013, t20_m26017_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26017_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26019_MI;
struct t20;
#define m26019(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Security.Policy.IIdentityPermissionFactory>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2064_0_0_0;
extern Il2CppType t2064_0_0_0;
static ParameterInfo t20_m26019_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t2064_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26019_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26019_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m26019_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26019_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26020_MI;
struct t20;
#define m26020(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Security.Policy.IIdentityPermissionFactory>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2064_0_0_0;
static ParameterInfo t20_m26020_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t2064_0_0_0},
};
extern TypeInfo t2064_TI;
static Il2CppRGCTXData m26020_RGCTXData[1] = 
{
	&t2064_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26020_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26020_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26020_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26020_RGCTXData, NULL, &m26020_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26021_MI;
struct t20;
#define m26021(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Security.Policy.IIdentityPermissionFactory>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3708_0_0_0;
extern Il2CppType t3708_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26021_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3708_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26021_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26021_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26021_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26021_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26022_MI;
struct t20;
#define m26022(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Security.Policy.IIdentityPermissionFactory>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2064_0_0_0;
static ParameterInfo t20_m26022_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t2064_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26022_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26022_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26022_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26022_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26023_MI;
struct t20;
#define m26023(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Security.Policy.IIdentityPermissionFactory>(T)
extern TypeInfo t20_TI;
extern Il2CppType t2064_0_0_0;
static ParameterInfo t20_m26023_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t2064_0_0_0},
};
extern TypeInfo t2064_TI;
static Il2CppRGCTXData m26023_RGCTXData[1] = 
{
	&t2064_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26023_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26023_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m26023_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26023_RGCTXData, NULL, &m26023_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26024_MI;
struct t20;
#define m26024(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Security.Policy.IIdentityPermissionFactory>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2064_0_0_0;
static ParameterInfo t20_m26024_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t2064_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26024_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26024_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26024_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26024_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26025_MI;
struct t20;
extern MethodInfo m26026_MI;
struct t20;
#define m26026(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Security.Policy.IIdentityPermissionFactory>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2064_0_0_0;
static ParameterInfo t20_m26026_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t2064_0_0_0},
};
extern TypeInfo t2064_TI;
static Il2CppRGCTXData m26026_RGCTXData[1] = 
{
	&t2064_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26026_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26026_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26026_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m26026_RGCTXData, NULL, &m26026_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Security.Policy.IIdentityPermissionFactory>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2064_1_0_0;
static ParameterInfo t20_m26025_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2064_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26025_GM;
extern void* RuntimeInvoker_t21_t44_t5013 (MethodInfo* method, void* obj, void** args);
MethodInfo m26025_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5013, t20_m26025_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26025_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3461.h"
extern TypeInfo t3461_TI;
#include "t3461MD.h"
extern MethodInfo m19175_MI;
extern MethodInfo m26027_MI;
struct t20;
#define m26027(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Security.Policy.IIdentityPermissionFactory>()
extern TypeInfo t20_TI;
extern TypeInfo t5014_TI;
extern TypeInfo t3461_TI;
static Il2CppRGCTXData m26027_RGCTXData[2] = 
{
	&t3461_TI/* Class Usage */,
	&m19175_MI/* Method Usage */,
};
extern Il2CppType t5014_0_0_0;
extern Il2CppGenericMethod m26027_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26027_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t5014_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m26027_RGCTXData, NULL, &m26027_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26028_MI;
struct t20;
struct t20;
 void m19787_gshared (t29 * __this, t316** p0, int32_t p1, int32_t p2, MethodInfo* method);
#define m19787(__this, p0, p1, p2, method) (void)m19787_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, (int32_t)p2, method)
#define m26028(__this, p0, p1, p2, method) (void)m19787_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, (int32_t)p2, method)
extern MethodInfo m26029_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m26029(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::Resize<System.Security.Policy.StrongName>(T[]&,System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3462_1_0_0;
extern Il2CppType t3462_1_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26029_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3462_1_0_0},
	{"newSize", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
static Il2CppRGCTXData m26029_RGCTXData[1] = 
{
	&m26028_MI/* Method Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26029_GM;
extern void* RuntimeInvoker_t21_t5015_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26029_MI = 
{
	"Resize", (methodPointerType)&m19788_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t5015_t44, t20_m26029_ParameterInfos, NULL, 150, 0, 255, 2, false, true, 0, m26029_RGCTXData, NULL, &m26029_GM};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t3462_TI;
#include "t602MD.h"
extern MethodInfo m4198_MI;
extern MethodInfo m9285_MI;
extern MethodInfo m5951_MI;
// Metadata Definition System.Void System.Array::Resize<System.Security.Policy.StrongName>(T[]&,System.Int32,System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3462_1_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26028_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3462_1_0_0},
	{"length", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"newSize", 2, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern TypeInfo t3462_TI;
static Il2CppRGCTXData m26028_RGCTXData[1] = 
{
	&t3462_TI/* Array Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26028_GM;
extern void* RuntimeInvoker_t21_t5015_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26028_MI = 
{
	"Resize", (methodPointerType)&m19787_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t5015_t44_t44, t20_m26028_ParameterInfos, NULL, 147, 0, 255, 3, false, true, 0, m26028_RGCTXData, NULL, &m26028_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3471.h"
extern TypeInfo t3471_TI;
#include "t3471MD.h"
extern MethodInfo m5924_MI;
extern MethodInfo m19304_MI;
extern MethodInfo m26030_MI;
extern MethodInfo m26031_MI;
struct t20;
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m26031(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
// Metadata Definition System.Int32 System.Array::IndexOf<System.Security.Policy.StrongName>(T[],T,System.Int32,System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t1567_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26031_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"startIndex", 2, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 3, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
static Il2CppRGCTXData m26031_RGCTXData[2] = 
{
	&m19304_MI/* Method Usage */,
	&m26030_MI/* Method Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26031_GM;
extern void* RuntimeInvoker_t44_t29_t29_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26031_MI = 
{
	"IndexOf", (methodPointerType)&m10120_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29_t44_t44, t20_m26031_ParameterInfos, NULL, 150, 0, 255, 4, false, true, 0, m26031_RGCTXData, NULL, &m26031_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26032_MI;
struct t20;
#include "t1138.h"
#include "t295.h"
#include "t601.h"
#include "t194.h"
#include "t914.h"
struct t20;
 void m19856_gshared (t29 * __this, t316* p0, t316* p1, int32_t p2, int32_t p3, t29* p4, MethodInfo* method);
#define m19856(__this, p0, p1, p2, p3, p4, method) (void)m19856_gshared((t29 *)__this, (t316*)p0, (t316*)p1, (int32_t)p2, (int32_t)p3, (t29*)p4, method)
#define m26032(__this, p0, p1, p2, p3, p4, method) (void)m19856_gshared((t29 *)__this, (t316*)p0, (t316*)p1, (int32_t)p2, (int32_t)p3, (t29*)p4, method)
extern MethodInfo m26033_MI;
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m26033(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
// Metadata Definition System.Void System.Array::Sort<System.Security.Policy.StrongName>(T[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo t20_TI;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t5016_0_0_0;
extern Il2CppType t5016_0_0_0;
static ParameterInfo t20_m26033_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"length", 2, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"comparer", 3, 134217728, &EmptyCustomAttributesCache, &t5016_0_0_0},
};
static Il2CppRGCTXData m26033_RGCTXData[1] = 
{
	&m26032_MI/* Method Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26033_GM;
extern void* RuntimeInvoker_t21_t29_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26033_MI = 
{
	"Sort", (methodPointerType)&m19857_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t44_t29, t20_m26033_ParameterInfos, NULL, 150, 0, 255, 4, false, true, 0, m26033_RGCTXData, NULL, &m26033_GM};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t1140_TI;
extern TypeInfo t601_TI;
extern TypeInfo t841_TI;
extern TypeInfo t200_TI;
extern TypeInfo t194_TI;
extern TypeInfo t914_TI;
extern TypeInfo t295_TI;
#include "t914MD.h"
extern MethodInfo m8852_MI;
extern MethodInfo m26034_MI;
extern MethodInfo m5976_MI;
extern MethodInfo m5977_MI;
extern MethodInfo m5978_MI;
extern MethodInfo m26035_MI;
extern MethodInfo m9281_MI;
struct t20;
#include "t35.h"
struct t20;
 t1138 * m19858_gshared (t29 * __this, t316* p0, MethodInfo* method);
#define m19858(__this, p0, method) (t1138 *)m19858_gshared((t29 *)__this, (t316*)p0, method)
#define m26034(__this, p0, method) (t1138 *)m19858_gshared((t29 *)__this, (t316*)p0, method)
struct t20;
struct t20;
 void m19859_gshared (t29 * __this, t316* p0, t316* p1, int32_t p2, int32_t p3, t29* p4, MethodInfo* method);
#define m19859(__this, p0, p1, p2, p3, p4, method) (void)m19859_gshared((t29 *)__this, (t316*)p0, (t316*)p1, (int32_t)p2, (int32_t)p3, (t29*)p4, method)
#define m26035(__this, p0, p1, p2, p3, p4, method) (void)m19859_gshared((t29 *)__this, (t316*)p0, (t316*)p1, (int32_t)p2, (int32_t)p3, (t29*)p4, method)
// Metadata Definition System.Void System.Array::Sort<System.Security.Policy.StrongName,System.Security.Policy.StrongName>(TKey[],TValue[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<TKey>)
extern TypeInfo t20_TI;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t5016_0_0_0;
static ParameterInfo t20_m26032_ParameterInfos[] = 
{
	{"keys", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"items", 1, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"index", 2, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"length", 3, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"comparer", 4, 134217728, &EmptyCustomAttributesCache, &t5016_0_0_0},
};
extern TypeInfo t3462_TI;
static Il2CppRGCTXData m26032_RGCTXData[3] = 
{
	&m26034_MI/* Method Usage */,
	&t3462_TI/* Class Usage */,
	&m26035_MI/* Method Usage */,
};
extern Il2CppGenericInst GenInst_t1567_0_0_0_t1567_0_0_0;
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26032_GM;
extern void* RuntimeInvoker_t21_t29_t29_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26032_MI = 
{
	"Sort", (methodPointerType)&m19856_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t44_t44_t29, t20_m26032_ParameterInfos, NULL, 150, 0, 255, 5, false, true, 0, m26032_RGCTXData, NULL, &m26032_GM};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t1138_TI;
#include "t1138MD.h"
extern MethodInfo m5971_MI;
extern MethodInfo m5890_MI;
extern MethodInfo m5974_MI;
extern MethodInfo m5973_MI;
// Metadata Definition System.Array/Swapper System.Array::get_swapper<System.Security.Policy.StrongName>(T[])
extern TypeInfo t20_TI;
extern Il2CppType t3462_0_0_0;
static ParameterInfo t20_m26034_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
};
extern Il2CppType t1138_0_0_0;
extern Il2CppGenericMethod m26034_GM;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26034_MI = 
{
	"get_swapper", (methodPointerType)&m19858_gshared, &t20_TI, &t1138_0_0_0, RuntimeInvoker_t29_t29, t20_m26034_ParameterInfos, NULL, 145, 0, 255, 1, false, true, 0, NULL, NULL, &m26034_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26036_MI;
extern MethodInfo m26037_MI;
struct t20;
#include "t42.h"
#include "t43.h"
struct t20;
 int32_t m19959_gshared (t29 * __this, t29 * p0, t29 * p1, t29* p2, MethodInfo* method);
#define m19959(__this, p0, p1, p2, method) (int32_t)m19959_gshared((t29 *)__this, (t29 *)p0, (t29 *)p1, (t29*)p2, method)
#define m26036(__this, p0, p1, p2, method) (int32_t)m19959_gshared((t29 *)__this, (t29 *)p0, (t29 *)p1, (t29*)p2, method)
struct t20;
struct t20;
 void m19960_gshared (t29 * __this, t316* p0, t316* p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m19960(__this, p0, p1, p2, p3, method) (void)m19960_gshared((t29 *)__this, (t316*)p0, (t316*)p1, (int32_t)p2, (int32_t)p3, method)
#define m26037(__this, p0, p1, p2, p3, method) (void)m19960_gshared((t29 *)__this, (t316*)p0, (t316*)p1, (int32_t)p2, (int32_t)p3, method)
// Metadata Definition System.Void System.Array::qsort<System.Security.Policy.StrongName,System.Security.Policy.StrongName>(K[],V[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<K>)
extern TypeInfo t20_TI;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t5016_0_0_0;
static ParameterInfo t20_m26035_ParameterInfos[] = 
{
	{"keys", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"items", 1, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"low0", 2, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"high0", 3, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"comparer", 4, 134217728, &EmptyCustomAttributesCache, &t5016_0_0_0},
};
static Il2CppRGCTXData m26035_RGCTXData[3] = 
{
	&m26036_MI/* Method Usage */,
	&m26037_MI/* Method Usage */,
	&m26035_MI/* Method Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26035_GM;
extern void* RuntimeInvoker_t21_t29_t29_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26035_MI = 
{
	"qsort", (methodPointerType)&m19859_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t44_t44_t29, t20_m26035_ParameterInfos, NULL, 145, 0, 255, 5, false, true, 0, m26035_RGCTXData, NULL, &m26035_GM};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t5016_TI;
extern TypeInfo t5017_TI;
extern TypeInfo t290_TI;
extern TypeInfo t42_TI;
extern TypeInfo t7_TI;
#include "t42MD.h"
#include "t7MD.h"
extern MethodInfo m26038_MI;
extern MethodInfo m26039_MI;
extern MethodInfo m9672_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m1535_MI;
extern MethodInfo m3964_MI;
// Metadata Definition System.Int32 System.Array::compare<System.Security.Policy.StrongName>(T,T,System.Collections.Generic.IComparer`1<T>)
extern TypeInfo t20_TI;
extern Il2CppType t1567_0_0_0;
extern Il2CppType t1567_0_0_0;
extern Il2CppType t5016_0_0_0;
static ParameterInfo t20_m26036_ParameterInfos[] = 
{
	{"value1", 0, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"value2", 1, 134217728, &EmptyCustomAttributesCache, &t1567_0_0_0},
	{"comparer", 2, 134217728, &EmptyCustomAttributesCache, &t5016_0_0_0},
};
extern TypeInfo t1567_TI;
extern TypeInfo t5017_TI;
static Il2CppRGCTXData m26036_RGCTXData[5] = 
{
	&m26038_MI/* Method Usage */,
	&t1567_TI/* Class Usage */,
	&t5017_TI/* Class Usage */,
	&m26039_MI/* Method Usage */,
	&t1567_0_0_0/* Type Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26036_GM;
extern void* RuntimeInvoker_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26036_MI = 
{
	"compare", (methodPointerType)&m19959_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29_t29, t20_m26036_ParameterInfos, NULL, 145, 0, 255, 3, false, true, 0, m26036_RGCTXData, NULL, &m26036_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::swap<System.Security.Policy.StrongName,System.Security.Policy.StrongName>(K[],V[],System.Int32,System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26037_ParameterInfos[] = 
{
	{"keys", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"items", 1, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"i", 2, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"j", 3, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26037_GM;
extern void* RuntimeInvoker_t21_t29_t29_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26037_MI = 
{
	"swap", (methodPointerType)&m19960_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t44_t44, t20_m26037_ParameterInfos, NULL, 145, 0, 255, 4, false, true, 0, NULL, NULL, &m26037_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3468.h"
extern MethodInfo m26040_MI;
struct t20;
struct t20;
#include "t2181.h"
 void m19963_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t2181 * p3, MethodInfo* method);
#define m19963(__this, p0, p1, p2, p3, method) (void)m19963_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t2181 *)p3, method)
#define m26040(__this, p0, p1, p2, p3, method) (void)m19963_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t2181 *)p3, method)
extern MethodInfo m26041_MI;
struct t20;
struct t20;
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m26041(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
// Metadata Definition System.Void System.Array::Sort<System.Security.Policy.StrongName>(T[],System.Int32,System.Comparison`1<T>)
extern TypeInfo t20_TI;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t3468_0_0_0;
extern Il2CppType t3468_0_0_0;
static ParameterInfo t20_m26041_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"length", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"comparison", 2, 134217728, &EmptyCustomAttributesCache, &t3468_0_0_0},
};
static Il2CppRGCTXData m26041_RGCTXData[1] = 
{
	&m26040_MI/* Method Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26041_GM;
extern void* RuntimeInvoker_t21_t29_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26041_MI = 
{
	"Sort", (methodPointerType)&m19964_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t29, t20_m26041_ParameterInfos, NULL, 147, 0, 255, 3, false, true, 0, m26041_RGCTXData, NULL, &m26041_GM};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t3468_TI;
#include "t3468MD.h"
extern MethodInfo m19319_MI;
extern MethodInfo m26042_MI;
struct t20;
struct t20;
 void m19965_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, MethodInfo* method);
#define m19965(__this, p0, p1, p2, method) (void)m19965_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, method)
#define m26042(__this, p0, p1, p2, method) (void)m19965_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, method)
// Metadata Definition System.Void System.Array::qsort<System.Security.Policy.StrongName>(T[],System.Int32,System.Int32,System.Comparison`1<T>)
extern TypeInfo t20_TI;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t3468_0_0_0;
static ParameterInfo t20_m26040_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"low0", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"high0", 2, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"comparison", 3, 134217728, &EmptyCustomAttributesCache, &t3468_0_0_0},
};
static Il2CppRGCTXData m26040_RGCTXData[3] = 
{
	&m19319_MI/* Method Usage */,
	&m26042_MI/* Method Usage */,
	&m26040_MI/* Method Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26040_GM;
extern void* RuntimeInvoker_t21_t29_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26040_MI = 
{
	"qsort", (methodPointerType)&m19963_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t44_t29, t20_m26040_ParameterInfos, NULL, 145, 0, 255, 4, false, true, 0, m26040_RGCTXData, NULL, &m26040_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::swap<System.Security.Policy.StrongName>(T[],System.Int32,System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3462_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26042_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3462_0_0_0},
	{"i", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"j", 2, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26042_GM;
extern void* RuntimeInvoker_t21_t29_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26042_MI = 
{
	"swap", (methodPointerType)&m19965_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44_t44, t20_m26042_ParameterInfos, NULL, 145, 0, 255, 3, false, true, 0, NULL, NULL, &m26042_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1568.h"
extern MethodInfo m26043_MI;
struct t20;
extern MethodInfo m26044_MI;
struct t20;
 int32_t m26044 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26044 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Security.Principal.PrincipalPolicy>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1568_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26044_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1568_0_0_0;
extern Il2CppType t1568_0_0_0;
extern Il2CppGenericMethod m26044_GM;
extern void* RuntimeInvoker_t1568_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26044_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26044, &t20_TI, &t1568_0_0_0, RuntimeInvoker_t1568_t44, t20_m26044_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26044_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Security.Principal.PrincipalPolicy>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1568_1_0_2;
extern Il2CppType t1568_1_0_0;
static ParameterInfo t20_m26043_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1568_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26043_GM;
extern void* RuntimeInvoker_t21_t44_t5018 (MethodInfo* method, void* obj, void** args);
MethodInfo m26043_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5018, t20_m26043_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26043_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26045_MI;
struct t20;
 void m26045 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26045 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Security.Principal.PrincipalPolicy>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1568_0_0_0;
extern Il2CppType t1568_0_0_0;
static ParameterInfo t20_m26045_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1568_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26045_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26045_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26045, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26045_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26045_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26046_MI;
struct t20;
 bool m26046 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26046 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1568_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1568_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1568_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1568_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Security.Principal.PrincipalPolicy>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1568_0_0_0;
static ParameterInfo t20_m26046_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1568_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26046_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26046_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26046, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26046_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26046_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26047_MI;
struct t20;
 void m26047 (t20 * __this, t3709* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26047 (t20 * __this, t3709* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Security.Principal.PrincipalPolicy>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3709_0_0_0;
extern Il2CppType t3709_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26047_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3709_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26047_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26047_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26047, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26047_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26047_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26048_MI;
struct t20;
 bool m26048 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26048 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Security.Principal.PrincipalPolicy>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1568_0_0_0;
static ParameterInfo t20_m26048_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1568_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26048_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26048_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26048, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26048_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26048_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26049_MI;
struct t20;
 int32_t m26049 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26049 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1568_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1568_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1568_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1568_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Security.Principal.PrincipalPolicy>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1568_0_0_0;
static ParameterInfo t20_m26049_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1568_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26049_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26049_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26049, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26049_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26049_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26050_MI;
struct t20;
 void m26050 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26050 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Security.Principal.PrincipalPolicy>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1568_0_0_0;
static ParameterInfo t20_m26050_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1568_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26050_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26050_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26050, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26050_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26050_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26051_MI;
struct t20;
extern MethodInfo m26052_MI;
struct t20;
 void m26052 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26052 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1568_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Security.Principal.PrincipalPolicy>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1568_0_0_0;
static ParameterInfo t20_m26052_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1568_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26052_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26052_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26052, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26052_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26052_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Security.Principal.PrincipalPolicy>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1568_1_0_0;
static ParameterInfo t20_m26051_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1568_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26051_GM;
extern void* RuntimeInvoker_t21_t44_t5018 (MethodInfo* method, void* obj, void** args);
MethodInfo m26051_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5018, t20_m26051_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26051_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3475.h"
extern TypeInfo t3475_TI;
#include "t3475MD.h"
extern MethodInfo m19322_MI;
extern MethodInfo m26053_MI;
struct t20;
 t29* m26053 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26053 (t20 * __this, MethodInfo* method){
	{
		t3475  L_0 = {0};
		m19322(&L_0, __this, &m19322_MI);
		t3475  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3475_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Security.Principal.PrincipalPolicy>()
extern TypeInfo t20_TI;
extern TypeInfo t5019_TI;
extern Il2CppType t5019_0_0_0;
extern Il2CppGenericMethod m26053_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26053_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26053, &t20_TI, &t5019_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26053_GM};
#ifndef _MSC_VER
#else
#endif

#include "t615.h"
extern MethodInfo m26054_MI;
struct t20;
extern MethodInfo m26055_MI;
struct t20;
#define m26055(__this, p0, method) (t615 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Security.SecuritySafeCriticalAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t615_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26055_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t615_0_0_0;
extern Il2CppType t615_0_0_0;
extern Il2CppGenericMethod m26055_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26055_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t615_0_0_0, RuntimeInvoker_t29_t44, t20_m26055_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26055_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Security.SecuritySafeCriticalAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t615_1_0_2;
extern Il2CppType t615_1_0_0;
static ParameterInfo t20_m26054_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t615_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26054_GM;
extern void* RuntimeInvoker_t21_t44_t5020 (MethodInfo* method, void* obj, void** args);
MethodInfo m26054_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5020, t20_m26054_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26054_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26056_MI;
struct t20;
#define m26056(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Security.SecuritySafeCriticalAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t615_0_0_0;
extern Il2CppType t615_0_0_0;
static ParameterInfo t20_m26056_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t615_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26056_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26056_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m26056_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26056_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26057_MI;
struct t20;
#define m26057(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Security.SecuritySafeCriticalAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t615_0_0_0;
static ParameterInfo t20_m26057_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t615_0_0_0},
};
extern TypeInfo t615_TI;
static Il2CppRGCTXData m26057_RGCTXData[1] = 
{
	&t615_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26057_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26057_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26057_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26057_RGCTXData, NULL, &m26057_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26058_MI;
struct t20;
#define m26058(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Security.SecuritySafeCriticalAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3710_0_0_0;
extern Il2CppType t3710_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26058_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3710_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26058_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26058_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26058_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26058_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26059_MI;
struct t20;
#define m26059(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Security.SecuritySafeCriticalAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t615_0_0_0;
static ParameterInfo t20_m26059_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t615_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26059_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26059_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26059_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26059_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26060_MI;
struct t20;
#define m26060(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Security.SecuritySafeCriticalAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t615_0_0_0;
static ParameterInfo t20_m26060_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t615_0_0_0},
};
extern TypeInfo t615_TI;
static Il2CppRGCTXData m26060_RGCTXData[1] = 
{
	&t615_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26060_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26060_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m26060_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26060_RGCTXData, NULL, &m26060_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26061_MI;
struct t20;
#define m26061(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Security.SecuritySafeCriticalAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t615_0_0_0;
static ParameterInfo t20_m26061_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t615_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26061_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26061_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26061_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26061_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26062_MI;
struct t20;
extern MethodInfo m26063_MI;
struct t20;
#define m26063(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Security.SecuritySafeCriticalAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t615_0_0_0;
static ParameterInfo t20_m26063_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t615_0_0_0},
};
extern TypeInfo t615_TI;
static Il2CppRGCTXData m26063_RGCTXData[1] = 
{
	&t615_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26063_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26063_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26063_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m26063_RGCTXData, NULL, &m26063_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Security.SecuritySafeCriticalAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t615_1_0_0;
static ParameterInfo t20_m26062_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t615_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26062_GM;
extern void* RuntimeInvoker_t21_t44_t5020 (MethodInfo* method, void* obj, void** args);
MethodInfo m26062_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5020, t20_m26062_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26062_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3476.h"
extern TypeInfo t3476_TI;
#include "t3476MD.h"
extern MethodInfo m19327_MI;
extern MethodInfo m26064_MI;
struct t20;
#define m26064(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Security.SecuritySafeCriticalAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t5021_TI;
extern TypeInfo t3476_TI;
static Il2CppRGCTXData m26064_RGCTXData[2] = 
{
	&t3476_TI/* Class Usage */,
	&m19327_MI/* Method Usage */,
};
extern Il2CppType t5021_0_0_0;
extern Il2CppGenericMethod m26064_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26064_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t5021_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m26064_RGCTXData, NULL, &m26064_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1572.h"
extern MethodInfo m26065_MI;
struct t20;
extern MethodInfo m26066_MI;
struct t20;
#define m26066(__this, p0, method) (t1572 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Security.SuppressUnmanagedCodeSecurityAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1572_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26066_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1572_0_0_0;
extern Il2CppType t1572_0_0_0;
extern Il2CppGenericMethod m26066_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26066_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1572_0_0_0, RuntimeInvoker_t29_t44, t20_m26066_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26066_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Security.SuppressUnmanagedCodeSecurityAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1572_1_0_2;
extern Il2CppType t1572_1_0_0;
static ParameterInfo t20_m26065_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1572_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26065_GM;
extern void* RuntimeInvoker_t21_t44_t5022 (MethodInfo* method, void* obj, void** args);
MethodInfo m26065_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5022, t20_m26065_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26065_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26067_MI;
struct t20;
#define m26067(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Security.SuppressUnmanagedCodeSecurityAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1572_0_0_0;
extern Il2CppType t1572_0_0_0;
static ParameterInfo t20_m26067_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1572_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26067_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26067_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m26067_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26067_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26068_MI;
struct t20;
#define m26068(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Security.SuppressUnmanagedCodeSecurityAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1572_0_0_0;
static ParameterInfo t20_m26068_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1572_0_0_0},
};
extern TypeInfo t1572_TI;
static Il2CppRGCTXData m26068_RGCTXData[1] = 
{
	&t1572_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26068_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26068_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26068_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26068_RGCTXData, NULL, &m26068_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26069_MI;
struct t20;
#define m26069(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Security.SuppressUnmanagedCodeSecurityAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3711_0_0_0;
extern Il2CppType t3711_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26069_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3711_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26069_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26069_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26069_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26069_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26070_MI;
struct t20;
#define m26070(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Security.SuppressUnmanagedCodeSecurityAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1572_0_0_0;
static ParameterInfo t20_m26070_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1572_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26070_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26070_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26070_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26070_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26071_MI;
struct t20;
#define m26071(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Security.SuppressUnmanagedCodeSecurityAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1572_0_0_0;
static ParameterInfo t20_m26071_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1572_0_0_0},
};
extern TypeInfo t1572_TI;
static Il2CppRGCTXData m26071_RGCTXData[1] = 
{
	&t1572_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26071_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26071_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m26071_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26071_RGCTXData, NULL, &m26071_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26072_MI;
struct t20;
#define m26072(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Security.SuppressUnmanagedCodeSecurityAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1572_0_0_0;
static ParameterInfo t20_m26072_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1572_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26072_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26072_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26072_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26072_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26073_MI;
struct t20;
extern MethodInfo m26074_MI;
struct t20;
#define m26074(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Security.SuppressUnmanagedCodeSecurityAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1572_0_0_0;
static ParameterInfo t20_m26074_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1572_0_0_0},
};
extern TypeInfo t1572_TI;
static Il2CppRGCTXData m26074_RGCTXData[1] = 
{
	&t1572_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26074_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26074_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26074_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m26074_RGCTXData, NULL, &m26074_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Security.SuppressUnmanagedCodeSecurityAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1572_1_0_0;
static ParameterInfo t20_m26073_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1572_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26073_GM;
extern void* RuntimeInvoker_t21_t44_t5022 (MethodInfo* method, void* obj, void** args);
MethodInfo m26073_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5022, t20_m26073_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26073_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3477.h"
extern TypeInfo t3477_TI;
#include "t3477MD.h"
extern MethodInfo m19332_MI;
extern MethodInfo m26075_MI;
struct t20;
#define m26075(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Security.SuppressUnmanagedCodeSecurityAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t5023_TI;
extern TypeInfo t3477_TI;
static Il2CppRGCTXData m26075_RGCTXData[2] = 
{
	&t3477_TI/* Class Usage */,
	&m19332_MI/* Method Usage */,
};
extern Il2CppType t5023_0_0_0;
extern Il2CppGenericMethod m26075_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26075_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t5023_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m26075_RGCTXData, NULL, &m26075_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1573.h"
extern MethodInfo m26076_MI;
struct t20;
extern MethodInfo m26077_MI;
struct t20;
#define m26077(__this, p0, method) (t1573 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.Security.UnverifiableCodeAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1573_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26077_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1573_0_0_0;
extern Il2CppType t1573_0_0_0;
extern Il2CppGenericMethod m26077_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26077_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1573_0_0_0, RuntimeInvoker_t29_t44, t20_m26077_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26077_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Security.UnverifiableCodeAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1573_1_0_2;
extern Il2CppType t1573_1_0_0;
static ParameterInfo t20_m26076_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1573_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26076_GM;
extern void* RuntimeInvoker_t21_t44_t5024 (MethodInfo* method, void* obj, void** args);
MethodInfo m26076_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5024, t20_m26076_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26076_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26078_MI;
struct t20;
#define m26078(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Security.UnverifiableCodeAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1573_0_0_0;
extern Il2CppType t1573_0_0_0;
static ParameterInfo t20_m26078_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1573_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26078_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26078_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m26078_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26078_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26079_MI;
struct t20;
#define m26079(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Security.UnverifiableCodeAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1573_0_0_0;
static ParameterInfo t20_m26079_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1573_0_0_0},
};
extern TypeInfo t1573_TI;
static Il2CppRGCTXData m26079_RGCTXData[1] = 
{
	&t1573_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26079_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26079_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26079_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26079_RGCTXData, NULL, &m26079_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26080_MI;
struct t20;
#define m26080(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Security.UnverifiableCodeAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3712_0_0_0;
extern Il2CppType t3712_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26080_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3712_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26080_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26080_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26080_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26080_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26081_MI;
struct t20;
#define m26081(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Security.UnverifiableCodeAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1573_0_0_0;
static ParameterInfo t20_m26081_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1573_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26081_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26081_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26081_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26081_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26082_MI;
struct t20;
#define m26082(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Security.UnverifiableCodeAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1573_0_0_0;
static ParameterInfo t20_m26082_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1573_0_0_0},
};
extern TypeInfo t1573_TI;
static Il2CppRGCTXData m26082_RGCTXData[1] = 
{
	&t1573_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26082_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26082_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m26082_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26082_RGCTXData, NULL, &m26082_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26083_MI;
struct t20;
#define m26083(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Security.UnverifiableCodeAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1573_0_0_0;
static ParameterInfo t20_m26083_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1573_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26083_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26083_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26083_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26083_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26084_MI;
struct t20;
extern MethodInfo m26085_MI;
struct t20;
#define m26085(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Security.UnverifiableCodeAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1573_0_0_0;
static ParameterInfo t20_m26085_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1573_0_0_0},
};
extern TypeInfo t1573_TI;
static Il2CppRGCTXData m26085_RGCTXData[1] = 
{
	&t1573_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26085_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26085_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26085_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m26085_RGCTXData, NULL, &m26085_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Security.UnverifiableCodeAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1573_1_0_0;
static ParameterInfo t20_m26084_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1573_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26084_GM;
extern void* RuntimeInvoker_t21_t44_t5024 (MethodInfo* method, void* obj, void** args);
MethodInfo m26084_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5024, t20_m26084_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26084_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3478.h"
extern TypeInfo t3478_TI;
#include "t3478MD.h"
extern MethodInfo m19337_MI;
extern MethodInfo m26086_MI;
struct t20;
#define m26086(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Security.UnverifiableCodeAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t5025_TI;
extern TypeInfo t3478_TI;
static Il2CppRGCTXData m26086_RGCTXData[2] = 
{
	&t3478_TI/* Class Usage */,
	&m19337_MI/* Method Usage */,
};
extern Il2CppType t5025_0_0_0;
extern Il2CppGenericMethod m26086_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26086_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t5025_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m26086_RGCTXData, NULL, &m26086_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1600.h"
extern MethodInfo m26087_MI;
struct t20;
extern MethodInfo m26088_MI;
struct t20;
 int32_t m26088 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26088 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Threading.EventResetMode>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1600_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26088_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1600_0_0_0;
extern Il2CppType t1600_0_0_0;
extern Il2CppGenericMethod m26088_GM;
extern void* RuntimeInvoker_t1600_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26088_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26088, &t20_TI, &t1600_0_0_0, RuntimeInvoker_t1600_t44, t20_m26088_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26088_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Threading.EventResetMode>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1600_1_0_2;
extern Il2CppType t1600_1_0_0;
static ParameterInfo t20_m26087_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1600_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26087_GM;
extern void* RuntimeInvoker_t21_t44_t5026 (MethodInfo* method, void* obj, void** args);
MethodInfo m26087_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5026, t20_m26087_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26087_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26089_MI;
struct t20;
 void m26089 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26089 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Threading.EventResetMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1600_0_0_0;
extern Il2CppType t1600_0_0_0;
static ParameterInfo t20_m26089_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1600_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26089_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26089_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26089, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26089_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26089_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26090_MI;
struct t20;
 bool m26090 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26090 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1600_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1600_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1600_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1600_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Threading.EventResetMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1600_0_0_0;
static ParameterInfo t20_m26090_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1600_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26090_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26090_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26090, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26090_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26090_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26091_MI;
struct t20;
 void m26091 (t20 * __this, t3713* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26091 (t20 * __this, t3713* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Threading.EventResetMode>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3713_0_0_0;
extern Il2CppType t3713_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26091_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3713_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26091_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26091_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26091, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26091_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26091_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26092_MI;
struct t20;
 bool m26092 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26092 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Threading.EventResetMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1600_0_0_0;
static ParameterInfo t20_m26092_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1600_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26092_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26092_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26092, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26092_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26092_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26093_MI;
struct t20;
 int32_t m26093 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26093 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1600_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1600_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1600_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1600_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Threading.EventResetMode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1600_0_0_0;
static ParameterInfo t20_m26093_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1600_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26093_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26093_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26093, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26093_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26093_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26094_MI;
struct t20;
 void m26094 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26094 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Threading.EventResetMode>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1600_0_0_0;
static ParameterInfo t20_m26094_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1600_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26094_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26094_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26094, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26094_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26094_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26095_MI;
struct t20;
extern MethodInfo m26096_MI;
struct t20;
 void m26096 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26096 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1600_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Threading.EventResetMode>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1600_0_0_0;
static ParameterInfo t20_m26096_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1600_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26096_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26096_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26096, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26096_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26096_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Threading.EventResetMode>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1600_1_0_0;
static ParameterInfo t20_m26095_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1600_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26095_GM;
extern void* RuntimeInvoker_t21_t44_t5026 (MethodInfo* method, void* obj, void** args);
MethodInfo m26095_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5026, t20_m26095_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26095_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3479.h"
extern TypeInfo t3479_TI;
#include "t3479MD.h"
extern MethodInfo m19342_MI;
extern MethodInfo m26097_MI;
struct t20;
 t29* m26097 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26097 (t20 * __this, MethodInfo* method){
	{
		t3479  L_0 = {0};
		m19342(&L_0, __this, &m19342_MI);
		t3479  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3479_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Threading.EventResetMode>()
extern TypeInfo t20_TI;
extern TypeInfo t5027_TI;
extern Il2CppType t5027_0_0_0;
extern Il2CppGenericMethod m26097_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26097_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26097, &t20_TI, &t5027_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26097_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1605.h"
extern MethodInfo m26098_MI;
struct t20;
extern MethodInfo m26099_MI;
struct t20;
 int32_t m26099 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26099 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Threading.ThreadState>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1605_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26099_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1605_0_0_0;
extern Il2CppType t1605_0_0_0;
extern Il2CppGenericMethod m26099_GM;
extern void* RuntimeInvoker_t1605_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26099_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26099, &t20_TI, &t1605_0_0_0, RuntimeInvoker_t1605_t44, t20_m26099_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26099_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Threading.ThreadState>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1605_1_0_2;
extern Il2CppType t1605_1_0_0;
static ParameterInfo t20_m26098_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1605_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26098_GM;
extern void* RuntimeInvoker_t21_t44_t5028 (MethodInfo* method, void* obj, void** args);
MethodInfo m26098_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5028, t20_m26098_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26098_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26100_MI;
struct t20;
 void m26100 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26100 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Threading.ThreadState>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1605_0_0_0;
extern Il2CppType t1605_0_0_0;
static ParameterInfo t20_m26100_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1605_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26100_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26100_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26100, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26100_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26100_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26101_MI;
struct t20;
 bool m26101 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26101 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1605_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1605_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1605_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1605_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Threading.ThreadState>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1605_0_0_0;
static ParameterInfo t20_m26101_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1605_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26101_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26101_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26101, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26101_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26101_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26102_MI;
struct t20;
 void m26102 (t20 * __this, t3714* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26102 (t20 * __this, t3714* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Threading.ThreadState>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3714_0_0_0;
extern Il2CppType t3714_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26102_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3714_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26102_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26102_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26102, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26102_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26102_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26103_MI;
struct t20;
 bool m26103 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26103 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Threading.ThreadState>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1605_0_0_0;
static ParameterInfo t20_m26103_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1605_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26103_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26103_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26103, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26103_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26103_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26104_MI;
struct t20;
 int32_t m26104 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26104 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1605_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1605_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1605_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1605_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Threading.ThreadState>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1605_0_0_0;
static ParameterInfo t20_m26104_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1605_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26104_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26104_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26104, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26104_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26104_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26105_MI;
struct t20;
 void m26105 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26105 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Threading.ThreadState>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1605_0_0_0;
static ParameterInfo t20_m26105_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1605_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26105_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26105_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26105, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26105_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26105_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26106_MI;
struct t20;
extern MethodInfo m26107_MI;
struct t20;
 void m26107 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26107 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1605_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Threading.ThreadState>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1605_0_0_0;
static ParameterInfo t20_m26107_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1605_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26107_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26107_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26107, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26107_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26107_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Threading.ThreadState>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1605_1_0_0;
static ParameterInfo t20_m26106_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1605_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26106_GM;
extern void* RuntimeInvoker_t21_t44_t5028 (MethodInfo* method, void* obj, void** args);
MethodInfo m26106_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5028, t20_m26106_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26106_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3480.h"
extern TypeInfo t3480_TI;
#include "t3480MD.h"
extern MethodInfo m19347_MI;
extern MethodInfo m26108_MI;
struct t20;
 t29* m26108 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26108 (t20 * __this, MethodInfo* method){
	{
		t3480  L_0 = {0};
		m19347(&L_0, __this, &m19347_MI);
		t3480  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3480_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Threading.ThreadState>()
extern TypeInfo t20_TI;
extern TypeInfo t5029_TI;
extern Il2CppType t5029_0_0_0;
extern Il2CppGenericMethod m26108_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26108_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26108, &t20_TI, &t5029_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26108_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1129.h"
extern MethodInfo m26109_MI;
struct t20;
extern MethodInfo m26110_MI;
struct t20;
 int32_t m26110 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26110 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.AttributeTargets>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1129_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26110_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1129_0_0_0;
extern Il2CppType t1129_0_0_0;
extern Il2CppGenericMethod m26110_GM;
extern void* RuntimeInvoker_t1129_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26110_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26110, &t20_TI, &t1129_0_0_0, RuntimeInvoker_t1129_t44, t20_m26110_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26110_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.AttributeTargets>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1129_1_0_2;
extern Il2CppType t1129_1_0_0;
static ParameterInfo t20_m26109_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1129_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26109_GM;
extern void* RuntimeInvoker_t21_t44_t5030 (MethodInfo* method, void* obj, void** args);
MethodInfo m26109_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5030, t20_m26109_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26109_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26111_MI;
struct t20;
 void m26111 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26111 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.AttributeTargets>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1129_0_0_0;
extern Il2CppType t1129_0_0_0;
static ParameterInfo t20_m26111_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1129_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26111_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26111_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26111, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26111_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26111_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26112_MI;
struct t20;
 bool m26112 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26112 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1129_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1129_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1129_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1129_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.AttributeTargets>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1129_0_0_0;
static ParameterInfo t20_m26112_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1129_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26112_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26112_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26112, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26112_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26112_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26113_MI;
struct t20;
 void m26113 (t20 * __this, t3715* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26113 (t20 * __this, t3715* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.AttributeTargets>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3715_0_0_0;
extern Il2CppType t3715_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26113_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3715_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26113_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26113_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26113, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26113_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26113_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26114_MI;
struct t20;
 bool m26114 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26114 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.AttributeTargets>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1129_0_0_0;
static ParameterInfo t20_m26114_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1129_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26114_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26114_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26114, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26114_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26114_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26115_MI;
struct t20;
 int32_t m26115 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26115 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1129_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1129_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1129_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1129_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.AttributeTargets>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1129_0_0_0;
static ParameterInfo t20_m26115_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1129_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26115_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26115_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26115, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26115_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26115_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26116_MI;
struct t20;
 void m26116 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26116 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.AttributeTargets>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1129_0_0_0;
static ParameterInfo t20_m26116_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1129_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26116_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26116_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26116, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26116_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26116_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26117_MI;
struct t20;
extern MethodInfo m26118_MI;
struct t20;
 void m26118 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26118 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1129_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.AttributeTargets>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1129_0_0_0;
static ParameterInfo t20_m26118_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1129_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26118_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26118_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26118, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26118_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26118_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.AttributeTargets>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1129_1_0_0;
static ParameterInfo t20_m26117_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1129_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26117_GM;
extern void* RuntimeInvoker_t21_t44_t5030 (MethodInfo* method, void* obj, void** args);
MethodInfo m26117_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5030, t20_m26117_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26117_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3481.h"
extern TypeInfo t3481_TI;
#include "t3481MD.h"
extern MethodInfo m19352_MI;
extern MethodInfo m26119_MI;
struct t20;
 t29* m26119 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26119 (t20 * __this, MethodInfo* method){
	{
		t3481  L_0 = {0};
		m19352(&L_0, __this, &m19352_MI);
		t3481  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3481_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.AttributeTargets>()
extern TypeInfo t20_TI;
extern TypeInfo t5031_TI;
extern Il2CppType t5031_0_0_0;
extern Il2CppGenericMethod m26119_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26119_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26119, &t20_TI, &t5031_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26119_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1627.h"
extern MethodInfo m26120_MI;
struct t20;
extern MethodInfo m26121_MI;
struct t20;
 int32_t m26121 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26121 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.DateTime/Which>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1627_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26121_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1627_0_0_0;
extern Il2CppType t1627_0_0_0;
extern Il2CppGenericMethod m26121_GM;
extern void* RuntimeInvoker_t1627_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26121_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26121, &t20_TI, &t1627_0_0_0, RuntimeInvoker_t1627_t44, t20_m26121_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26121_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.DateTime/Which>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1627_1_0_2;
extern Il2CppType t1627_1_0_0;
static ParameterInfo t20_m26120_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1627_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26120_GM;
extern void* RuntimeInvoker_t21_t44_t5032 (MethodInfo* method, void* obj, void** args);
MethodInfo m26120_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5032, t20_m26120_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26120_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26122_MI;
struct t20;
 void m26122 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26122 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.DateTime/Which>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1627_0_0_0;
extern Il2CppType t1627_0_0_0;
static ParameterInfo t20_m26122_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1627_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26122_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26122_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26122, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26122_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26122_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26123_MI;
struct t20;
 bool m26123 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26123 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1627_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1627_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1627_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1627_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTime/Which>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1627_0_0_0;
static ParameterInfo t20_m26123_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1627_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26123_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26123_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26123, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26123_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26123_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26124_MI;
struct t20;
 void m26124 (t20 * __this, t3716* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26124 (t20 * __this, t3716* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTime/Which>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3716_0_0_0;
extern Il2CppType t3716_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26124_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3716_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26124_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26124_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26124, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26124_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26124_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26125_MI;
struct t20;
 bool m26125 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26125 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTime/Which>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1627_0_0_0;
static ParameterInfo t20_m26125_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1627_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26125_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26125_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26125, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26125_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26125_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26126_MI;
struct t20;
 int32_t m26126 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26126 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1627_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1627_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1627_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1627_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.DateTime/Which>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1627_0_0_0;
static ParameterInfo t20_m26126_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1627_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26126_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26126_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26126, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26126_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26126_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26127_MI;
struct t20;
 void m26127 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26127 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.DateTime/Which>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1627_0_0_0;
static ParameterInfo t20_m26127_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1627_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26127_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26127_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26127, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26127_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26127_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26128_MI;
struct t20;
extern MethodInfo m26129_MI;
struct t20;
 void m26129 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26129 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1627_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.DateTime/Which>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1627_0_0_0;
static ParameterInfo t20_m26129_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1627_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26129_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26129_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26129, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26129_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26129_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.DateTime/Which>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1627_1_0_0;
static ParameterInfo t20_m26128_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1627_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26128_GM;
extern void* RuntimeInvoker_t21_t44_t5032 (MethodInfo* method, void* obj, void** args);
MethodInfo m26128_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5032, t20_m26128_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26128_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3486.h"
extern TypeInfo t3486_TI;
#include "t3486MD.h"
extern MethodInfo m19374_MI;
extern MethodInfo m26130_MI;
struct t20;
 t29* m26130 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26130 (t20 * __this, MethodInfo* method){
	{
		t3486  L_0 = {0};
		m19374(&L_0, __this, &m19374_MI);
		t3486  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3486_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTime/Which>()
extern TypeInfo t20_TI;
extern TypeInfo t5033_TI;
extern Il2CppType t5033_0_0_0;
extern Il2CppGenericMethod m26130_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26130_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26130, &t20_TI, &t5033_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26130_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1628.h"
extern MethodInfo m26131_MI;
struct t20;
extern MethodInfo m26132_MI;
struct t20;
 int32_t m26132 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26132 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.DateTimeKind>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1628_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26132_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1628_0_0_0;
extern Il2CppType t1628_0_0_0;
extern Il2CppGenericMethod m26132_GM;
extern void* RuntimeInvoker_t1628_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26132_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26132, &t20_TI, &t1628_0_0_0, RuntimeInvoker_t1628_t44, t20_m26132_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26132_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.DateTimeKind>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1628_1_0_2;
extern Il2CppType t1628_1_0_0;
static ParameterInfo t20_m26131_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1628_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26131_GM;
extern void* RuntimeInvoker_t21_t44_t5034 (MethodInfo* method, void* obj, void** args);
MethodInfo m26131_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5034, t20_m26131_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26131_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26133_MI;
struct t20;
 void m26133 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26133 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.DateTimeKind>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1628_0_0_0;
extern Il2CppType t1628_0_0_0;
static ParameterInfo t20_m26133_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1628_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26133_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26133_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26133, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26133_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26133_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26134_MI;
struct t20;
 bool m26134 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26134 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1628_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1628_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1628_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1628_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.DateTimeKind>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1628_0_0_0;
static ParameterInfo t20_m26134_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1628_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26134_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26134_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26134, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26134_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26134_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26135_MI;
struct t20;
 void m26135 (t20 * __this, t3717* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26135 (t20 * __this, t3717* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.DateTimeKind>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3717_0_0_0;
extern Il2CppType t3717_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26135_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3717_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26135_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26135_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26135, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26135_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26135_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26136_MI;
struct t20;
 bool m26136 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26136 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.DateTimeKind>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1628_0_0_0;
static ParameterInfo t20_m26136_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1628_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26136_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26136_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26136, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26136_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26136_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26137_MI;
struct t20;
 int32_t m26137 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26137 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1628_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1628_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1628_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1628_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.DateTimeKind>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1628_0_0_0;
static ParameterInfo t20_m26137_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1628_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26137_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26137_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26137, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26137_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26137_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26138_MI;
struct t20;
 void m26138 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26138 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.DateTimeKind>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1628_0_0_0;
static ParameterInfo t20_m26138_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1628_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26138_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26138_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26138, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26138_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26138_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26139_MI;
struct t20;
extern MethodInfo m26140_MI;
struct t20;
 void m26140 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26140 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1628_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.DateTimeKind>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1628_0_0_0;
static ParameterInfo t20_m26140_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1628_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26140_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26140_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26140, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26140_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26140_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.DateTimeKind>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1628_1_0_0;
static ParameterInfo t20_m26139_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1628_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26139_GM;
extern void* RuntimeInvoker_t21_t44_t5034 (MethodInfo* method, void* obj, void** args);
MethodInfo m26139_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5034, t20_m26139_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26139_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3487.h"
extern TypeInfo t3487_TI;
#include "t3487MD.h"
extern MethodInfo m19379_MI;
extern MethodInfo m26141_MI;
struct t20;
 t29* m26141 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26141 (t20 * __this, MethodInfo* method){
	{
		t3487  L_0 = {0};
		m19379(&L_0, __this, &m19379_MI);
		t3487  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3487_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DateTimeKind>()
extern TypeInfo t20_TI;
extern TypeInfo t5035_TI;
extern Il2CppType t5035_0_0_0;
extern Il2CppGenericMethod m26141_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26141_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26141, &t20_TI, &t5035_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26141_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1292.h"
extern MethodInfo m26142_MI;
struct t20;
extern MethodInfo m26143_MI;
struct t20;
 int32_t m26143 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26143 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.DayOfWeek>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1292_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26143_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1292_0_0_0;
extern Il2CppType t1292_0_0_0;
extern Il2CppGenericMethod m26143_GM;
extern void* RuntimeInvoker_t1292_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26143_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26143, &t20_TI, &t1292_0_0_0, RuntimeInvoker_t1292_t44, t20_m26143_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26143_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.DayOfWeek>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1292_1_0_2;
extern Il2CppType t1292_1_0_0;
static ParameterInfo t20_m26142_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1292_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26142_GM;
extern void* RuntimeInvoker_t21_t44_t5036 (MethodInfo* method, void* obj, void** args);
MethodInfo m26142_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5036, t20_m26142_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26142_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26144_MI;
struct t20;
 void m26144 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26144 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.DayOfWeek>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1292_0_0_0;
extern Il2CppType t1292_0_0_0;
static ParameterInfo t20_m26144_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1292_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26144_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26144_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26144, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26144_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26144_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26145_MI;
struct t20;
 bool m26145 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26145 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1292_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1292_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1292_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1292_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.DayOfWeek>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1292_0_0_0;
static ParameterInfo t20_m26145_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1292_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26145_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26145_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26145, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26145_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26145_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26146_MI;
struct t20;
 void m26146 (t20 * __this, t3718* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26146 (t20 * __this, t3718* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.DayOfWeek>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3718_0_0_0;
extern Il2CppType t3718_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26146_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3718_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26146_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26146_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26146, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26146_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26146_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26147_MI;
struct t20;
 bool m26147 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26147 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.DayOfWeek>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1292_0_0_0;
static ParameterInfo t20_m26147_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1292_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26147_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26147_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26147, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26147_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26147_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26148_MI;
struct t20;
 int32_t m26148 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26148 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1292_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1292_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1292_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1292_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.DayOfWeek>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1292_0_0_0;
static ParameterInfo t20_m26148_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1292_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26148_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26148_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26148, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26148_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26148_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26149_MI;
struct t20;
 void m26149 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26149 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.DayOfWeek>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1292_0_0_0;
static ParameterInfo t20_m26149_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1292_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26149_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26149_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26149, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26149_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26149_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26150_MI;
struct t20;
extern MethodInfo m26151_MI;
struct t20;
 void m26151 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26151 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1292_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.DayOfWeek>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1292_0_0_0;
static ParameterInfo t20_m26151_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1292_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26151_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26151_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26151, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26151_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26151_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.DayOfWeek>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1292_1_0_0;
static ParameterInfo t20_m26150_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1292_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26150_GM;
extern void* RuntimeInvoker_t21_t44_t5036 (MethodInfo* method, void* obj, void** args);
MethodInfo m26150_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5036, t20_m26150_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26150_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3492.h"
extern TypeInfo t3492_TI;
#include "t3492MD.h"
extern MethodInfo m19405_MI;
extern MethodInfo m26152_MI;
struct t20;
 t29* m26152 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26152 (t20 * __this, MethodInfo* method){
	{
		t3492  L_0 = {0};
		m19405(&L_0, __this, &m19405_MI);
		t3492  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3492_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.DayOfWeek>()
extern TypeInfo t20_TI;
extern TypeInfo t5037_TI;
extern Il2CppType t5037_0_0_0;
extern Il2CppGenericMethod m26152_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26152_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26152, &t20_TI, &t5037_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26152_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1112.h"
extern MethodInfo m26153_MI;
struct t20;
extern MethodInfo m26154_MI;
struct t20;
 int32_t m26154 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26154 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.Environment/SpecialFolder>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1112_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26154_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1112_0_0_0;
extern Il2CppType t1112_0_0_0;
extern Il2CppGenericMethod m26154_GM;
extern void* RuntimeInvoker_t1112_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26154_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26154, &t20_TI, &t1112_0_0_0, RuntimeInvoker_t1112_t44, t20_m26154_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26154_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.Environment/SpecialFolder>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1112_1_0_2;
extern Il2CppType t1112_1_0_0;
static ParameterInfo t20_m26153_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1112_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26153_GM;
extern void* RuntimeInvoker_t21_t44_t5038 (MethodInfo* method, void* obj, void** args);
MethodInfo m26153_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5038, t20_m26153_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26153_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26155_MI;
struct t20;
 void m26155 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26155 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.Environment/SpecialFolder>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1112_0_0_0;
extern Il2CppType t1112_0_0_0;
static ParameterInfo t20_m26155_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1112_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26155_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26155_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26155, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26155_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26155_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26156_MI;
struct t20;
 bool m26156 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26156 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1112_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1112_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1112_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1112_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.Environment/SpecialFolder>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1112_0_0_0;
static ParameterInfo t20_m26156_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1112_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26156_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26156_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26156, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26156_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26156_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26157_MI;
struct t20;
 void m26157 (t20 * __this, t3719* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26157 (t20 * __this, t3719* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.Environment/SpecialFolder>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3719_0_0_0;
extern Il2CppType t3719_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26157_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3719_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26157_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26157_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26157, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26157_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26157_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26158_MI;
struct t20;
 bool m26158 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26158 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.Environment/SpecialFolder>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1112_0_0_0;
static ParameterInfo t20_m26158_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1112_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26158_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26158_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26158, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26158_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26158_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26159_MI;
struct t20;
 int32_t m26159 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26159 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1112_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1112_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1112_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1112_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.Environment/SpecialFolder>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1112_0_0_0;
static ParameterInfo t20_m26159_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1112_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26159_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26159_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26159, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26159_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26159_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26160_MI;
struct t20;
 void m26160 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26160 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.Environment/SpecialFolder>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1112_0_0_0;
static ParameterInfo t20_m26160_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1112_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26160_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26160_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26160, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26160_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26160_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26161_MI;
struct t20;
extern MethodInfo m26162_MI;
struct t20;
 void m26162 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26162 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1112_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.Environment/SpecialFolder>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1112_0_0_0;
static ParameterInfo t20_m26162_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1112_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26162_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26162_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26162, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26162_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26162_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.Environment/SpecialFolder>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1112_1_0_0;
static ParameterInfo t20_m26161_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1112_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26161_GM;
extern void* RuntimeInvoker_t21_t44_t5038 (MethodInfo* method, void* obj, void** args);
MethodInfo m26161_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5038, t20_m26161_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26161_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3493.h"
extern TypeInfo t3493_TI;
#include "t3493MD.h"
extern MethodInfo m19410_MI;
extern MethodInfo m26163_MI;
struct t20;
 t29* m26163 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26163 (t20 * __this, MethodInfo* method){
	{
		t3493  L_0 = {0};
		m19410(&L_0, __this, &m19410_MI);
		t3493  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3493_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.Environment/SpecialFolder>()
extern TypeInfo t20_TI;
extern TypeInfo t5039_TI;
extern Il2CppType t5039_0_0_0;
extern Il2CppGenericMethod m26163_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26163_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26163, &t20_TI, &t5039_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26163_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1620.h"
extern MethodInfo m26164_MI;
struct t20;
extern MethodInfo m26165_MI;
struct t20;
 int32_t m26165 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26165 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.LoaderOptimization>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1620_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26165_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1620_0_0_0;
extern Il2CppType t1620_0_0_0;
extern Il2CppGenericMethod m26165_GM;
extern void* RuntimeInvoker_t1620_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26165_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26165, &t20_TI, &t1620_0_0_0, RuntimeInvoker_t1620_t44, t20_m26165_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26165_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.LoaderOptimization>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1620_1_0_2;
extern Il2CppType t1620_1_0_0;
static ParameterInfo t20_m26164_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1620_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26164_GM;
extern void* RuntimeInvoker_t21_t44_t5040 (MethodInfo* method, void* obj, void** args);
MethodInfo m26164_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5040, t20_m26164_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26164_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26166_MI;
struct t20;
 void m26166 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26166 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.LoaderOptimization>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1620_0_0_0;
extern Il2CppType t1620_0_0_0;
static ParameterInfo t20_m26166_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1620_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26166_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26166_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26166, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26166_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26166_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26167_MI;
struct t20;
 bool m26167 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26167 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1620_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1620_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1620_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1620_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.LoaderOptimization>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1620_0_0_0;
static ParameterInfo t20_m26167_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1620_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26167_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26167_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26167, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26167_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26167_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26168_MI;
struct t20;
 void m26168 (t20 * __this, t3720* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26168 (t20 * __this, t3720* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.LoaderOptimization>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3720_0_0_0;
extern Il2CppType t3720_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26168_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3720_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26168_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26168_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26168, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26168_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26168_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26169_MI;
struct t20;
 bool m26169 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26169 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.LoaderOptimization>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1620_0_0_0;
static ParameterInfo t20_m26169_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1620_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26169_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26169_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26169, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26169_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26169_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26170_MI;
struct t20;
 int32_t m26170 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26170 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1620_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1620_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1620_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1620_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.LoaderOptimization>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1620_0_0_0;
static ParameterInfo t20_m26170_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1620_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26170_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26170_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26170, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26170_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26170_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26171_MI;
struct t20;
 void m26171 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26171 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.LoaderOptimization>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1620_0_0_0;
static ParameterInfo t20_m26171_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1620_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26171_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26171_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26171, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26171_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26171_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26172_MI;
struct t20;
extern MethodInfo m26173_MI;
struct t20;
 void m26173 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26173 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1620_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.LoaderOptimization>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1620_0_0_0;
static ParameterInfo t20_m26173_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1620_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26173_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26173_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26173, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26173_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26173_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.LoaderOptimization>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1620_1_0_0;
static ParameterInfo t20_m26172_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1620_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26172_GM;
extern void* RuntimeInvoker_t21_t44_t5040 (MethodInfo* method, void* obj, void** args);
MethodInfo m26172_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5040, t20_m26172_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26172_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3498.h"
extern TypeInfo t3498_TI;
#include "t3498MD.h"
extern MethodInfo m19432_MI;
extern MethodInfo m26174_MI;
struct t20;
 t29* m26174 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26174 (t20 * __this, MethodInfo* method){
	{
		t3498  L_0 = {0};
		m19432(&L_0, __this, &m19432_MI);
		t3498  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3498_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.LoaderOptimization>()
extern TypeInfo t20_TI;
extern TypeInfo t5041_TI;
extern Il2CppType t5041_0_0_0;
extern Il2CppGenericMethod m26174_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26174_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26174, &t20_TI, &t5041_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26174_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1662.h"
extern MethodInfo m26175_MI;
struct t20;
extern MethodInfo m26176_MI;
struct t20;
#define m26176(__this, p0, method) (t1662 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.NonSerializedAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1662_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26176_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1662_0_0_0;
extern Il2CppType t1662_0_0_0;
extern Il2CppGenericMethod m26176_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26176_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1662_0_0_0, RuntimeInvoker_t29_t44, t20_m26176_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26176_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.NonSerializedAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1662_1_0_2;
extern Il2CppType t1662_1_0_0;
static ParameterInfo t20_m26175_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1662_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26175_GM;
extern void* RuntimeInvoker_t21_t44_t5042 (MethodInfo* method, void* obj, void** args);
MethodInfo m26175_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5042, t20_m26175_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26175_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26177_MI;
struct t20;
#define m26177(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.NonSerializedAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1662_0_0_0;
extern Il2CppType t1662_0_0_0;
static ParameterInfo t20_m26177_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1662_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26177_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26177_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m26177_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26177_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26178_MI;
struct t20;
#define m26178(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.NonSerializedAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1662_0_0_0;
static ParameterInfo t20_m26178_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1662_0_0_0},
};
extern TypeInfo t1662_TI;
static Il2CppRGCTXData m26178_RGCTXData[1] = 
{
	&t1662_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26178_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26178_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26178_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26178_RGCTXData, NULL, &m26178_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26179_MI;
struct t20;
#define m26179(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.NonSerializedAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3721_0_0_0;
extern Il2CppType t3721_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26179_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3721_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26179_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26179_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26179_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26179_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26180_MI;
struct t20;
#define m26180(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.NonSerializedAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1662_0_0_0;
static ParameterInfo t20_m26180_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1662_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26180_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26180_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26180_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26180_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26181_MI;
struct t20;
#define m26181(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.NonSerializedAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1662_0_0_0;
static ParameterInfo t20_m26181_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1662_0_0_0},
};
extern TypeInfo t1662_TI;
static Il2CppRGCTXData m26181_RGCTXData[1] = 
{
	&t1662_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26181_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26181_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m26181_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26181_RGCTXData, NULL, &m26181_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26182_MI;
struct t20;
#define m26182(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.NonSerializedAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1662_0_0_0;
static ParameterInfo t20_m26182_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1662_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26182_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26182_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26182_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26182_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26183_MI;
struct t20;
extern MethodInfo m26184_MI;
struct t20;
#define m26184(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.NonSerializedAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1662_0_0_0;
static ParameterInfo t20_m26184_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1662_0_0_0},
};
extern TypeInfo t1662_TI;
static Il2CppRGCTXData m26184_RGCTXData[1] = 
{
	&t1662_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26184_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26184_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26184_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m26184_RGCTXData, NULL, &m26184_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.NonSerializedAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1662_1_0_0;
static ParameterInfo t20_m26183_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1662_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26183_GM;
extern void* RuntimeInvoker_t21_t44_t5042 (MethodInfo* method, void* obj, void** args);
MethodInfo m26183_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5042, t20_m26183_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26183_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3499.h"
extern TypeInfo t3499_TI;
#include "t3499MD.h"
extern MethodInfo m19437_MI;
extern MethodInfo m26185_MI;
struct t20;
#define m26185(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.NonSerializedAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t5043_TI;
extern TypeInfo t3499_TI;
static Il2CppRGCTXData m26185_RGCTXData[2] = 
{
	&t3499_TI/* Class Usage */,
	&m19437_MI/* Method Usage */,
};
extern Il2CppType t5043_0_0_0;
extern Il2CppGenericMethod m26185_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26185_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t5043_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m26185_RGCTXData, NULL, &m26185_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1644.h"
extern MethodInfo m26186_MI;
struct t20;
extern MethodInfo m26187_MI;
struct t20;
 int32_t m26187 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26187 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.PlatformID>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1644_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26187_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1644_0_0_0;
extern Il2CppType t1644_0_0_0;
extern Il2CppGenericMethod m26187_GM;
extern void* RuntimeInvoker_t1644_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26187_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26187, &t20_TI, &t1644_0_0_0, RuntimeInvoker_t1644_t44, t20_m26187_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26187_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.PlatformID>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1644_1_0_2;
extern Il2CppType t1644_1_0_0;
static ParameterInfo t20_m26186_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1644_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26186_GM;
extern void* RuntimeInvoker_t21_t44_t5044 (MethodInfo* method, void* obj, void** args);
MethodInfo m26186_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5044, t20_m26186_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26186_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26188_MI;
struct t20;
 void m26188 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26188 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.PlatformID>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1644_0_0_0;
extern Il2CppType t1644_0_0_0;
static ParameterInfo t20_m26188_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1644_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26188_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26188_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26188, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26188_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26188_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26189_MI;
struct t20;
 bool m26189 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26189 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1644_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1644_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1644_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1644_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.PlatformID>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1644_0_0_0;
static ParameterInfo t20_m26189_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1644_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26189_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26189_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26189, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26189_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26189_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26190_MI;
struct t20;
 void m26190 (t20 * __this, t3722* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26190 (t20 * __this, t3722* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.PlatformID>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3722_0_0_0;
extern Il2CppType t3722_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26190_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3722_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26190_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26190_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26190, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26190_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26190_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26191_MI;
struct t20;
 bool m26191 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26191 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.PlatformID>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1644_0_0_0;
static ParameterInfo t20_m26191_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1644_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26191_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26191_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26191, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26191_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26191_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26192_MI;
struct t20;
 int32_t m26192 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26192 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1644_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1644_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1644_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1644_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.PlatformID>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1644_0_0_0;
static ParameterInfo t20_m26192_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1644_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26192_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26192_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26192, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26192_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26192_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26193_MI;
struct t20;
 void m26193 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26193 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.PlatformID>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1644_0_0_0;
static ParameterInfo t20_m26193_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1644_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26193_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26193_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26193, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26193_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26193_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26194_MI;
struct t20;
extern MethodInfo m26195_MI;
struct t20;
 void m26195 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26195 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1644_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.PlatformID>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1644_0_0_0;
static ParameterInfo t20_m26195_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1644_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26195_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26195_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26195, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26195_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26195_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.PlatformID>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1644_1_0_0;
static ParameterInfo t20_m26194_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1644_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26194_GM;
extern void* RuntimeInvoker_t21_t44_t5044 (MethodInfo* method, void* obj, void** args);
MethodInfo m26194_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5044, t20_m26194_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26194_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3500.h"
extern TypeInfo t3500_TI;
#include "t3500MD.h"
extern MethodInfo m19442_MI;
extern MethodInfo m26196_MI;
struct t20;
 t29* m26196 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26196 (t20 * __this, MethodInfo* method){
	{
		t3500  L_0 = {0};
		m19442(&L_0, __this, &m19442_MI);
		t3500  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3500_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.PlatformID>()
extern TypeInfo t20_TI;
extern TypeInfo t5045_TI;
extern Il2CppType t5045_0_0_0;
extern Il2CppGenericMethod m26196_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26196_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26196, &t20_TI, &t5045_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26196_GM};
#ifndef _MSC_VER
#else
#endif

#include "t947.h"
extern MethodInfo m26197_MI;
struct t20;
extern MethodInfo m26198_MI;
struct t20;
 int32_t m26198 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26198 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.StringComparison>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t947_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26198_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t947_0_0_0;
extern Il2CppType t947_0_0_0;
extern Il2CppGenericMethod m26198_GM;
extern void* RuntimeInvoker_t947_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26198_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26198, &t20_TI, &t947_0_0_0, RuntimeInvoker_t947_t44, t20_m26198_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26198_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.StringComparison>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t947_1_0_2;
extern Il2CppType t947_1_0_0;
static ParameterInfo t20_m26197_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t947_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26197_GM;
extern void* RuntimeInvoker_t21_t44_t5046 (MethodInfo* method, void* obj, void** args);
MethodInfo m26197_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5046, t20_m26197_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26197_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26199_MI;
struct t20;
 void m26199 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26199 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.StringComparison>(T)
extern TypeInfo t20_TI;
extern Il2CppType t947_0_0_0;
extern Il2CppType t947_0_0_0;
static ParameterInfo t20_m26199_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t947_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26199_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26199_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26199, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26199_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26199_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26200_MI;
struct t20;
 bool m26200 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26200 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t947_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t947_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t947_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t947_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.StringComparison>(T)
extern TypeInfo t20_TI;
extern Il2CppType t947_0_0_0;
static ParameterInfo t20_m26200_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t947_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26200_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26200_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26200, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26200_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26200_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26201_MI;
struct t20;
 void m26201 (t20 * __this, t3723* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26201 (t20 * __this, t3723* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.StringComparison>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3723_0_0_0;
extern Il2CppType t3723_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26201_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3723_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26201_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26201_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26201, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26201_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26201_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26202_MI;
struct t20;
 bool m26202 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26202 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.StringComparison>(T)
extern TypeInfo t20_TI;
extern Il2CppType t947_0_0_0;
static ParameterInfo t20_m26202_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t947_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26202_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26202_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26202, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26202_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26202_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26203_MI;
struct t20;
 int32_t m26203 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26203 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t947_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t947_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t947_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t947_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.StringComparison>(T)
extern TypeInfo t20_TI;
extern Il2CppType t947_0_0_0;
static ParameterInfo t20_m26203_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t947_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26203_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26203_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26203, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26203_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26203_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26204_MI;
struct t20;
 void m26204 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26204 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.StringComparison>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t947_0_0_0;
static ParameterInfo t20_m26204_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t947_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26204_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26204_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26204, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26204_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26204_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26205_MI;
struct t20;
extern MethodInfo m26206_MI;
struct t20;
 void m26206 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26206 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t947_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.StringComparison>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t947_0_0_0;
static ParameterInfo t20_m26206_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t947_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26206_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26206_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26206, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26206_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26206_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.StringComparison>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t947_1_0_0;
static ParameterInfo t20_m26205_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t947_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26205_GM;
extern void* RuntimeInvoker_t21_t44_t5046 (MethodInfo* method, void* obj, void** args);
MethodInfo m26205_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5046, t20_m26205_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26205_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3501.h"
extern TypeInfo t3501_TI;
#include "t3501MD.h"
extern MethodInfo m19447_MI;
extern MethodInfo m26207_MI;
struct t20;
 t29* m26207 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26207 (t20 * __this, MethodInfo* method){
	{
		t3501  L_0 = {0};
		m19447(&L_0, __this, &m19447_MI);
		t3501  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3501_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.StringComparison>()
extern TypeInfo t20_TI;
extern TypeInfo t5047_TI;
extern Il2CppType t5047_0_0_0;
extern Il2CppGenericMethod m26207_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26207_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26207, &t20_TI, &t5047_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26207_GM};
#ifndef _MSC_VER
#else
#endif

#include "t939.h"
extern MethodInfo m26208_MI;
struct t20;
extern MethodInfo m26209_MI;
struct t20;
 int32_t m26209 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26209 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.StringSplitOptions>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t939_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26209_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t939_0_0_0;
extern Il2CppType t939_0_0_0;
extern Il2CppGenericMethod m26209_GM;
extern void* RuntimeInvoker_t939_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26209_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26209, &t20_TI, &t939_0_0_0, RuntimeInvoker_t939_t44, t20_m26209_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26209_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.StringSplitOptions>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t939_1_0_2;
extern Il2CppType t939_1_0_0;
static ParameterInfo t20_m26208_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t939_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26208_GM;
extern void* RuntimeInvoker_t21_t44_t5048 (MethodInfo* method, void* obj, void** args);
MethodInfo m26208_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5048, t20_m26208_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26208_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26210_MI;
struct t20;
 void m26210 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26210 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.StringSplitOptions>(T)
extern TypeInfo t20_TI;
extern Il2CppType t939_0_0_0;
extern Il2CppType t939_0_0_0;
static ParameterInfo t20_m26210_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t939_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26210_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26210_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26210, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26210_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26210_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26211_MI;
struct t20;
 bool m26211 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26211 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t939_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t939_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t939_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t939_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.StringSplitOptions>(T)
extern TypeInfo t20_TI;
extern Il2CppType t939_0_0_0;
static ParameterInfo t20_m26211_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t939_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26211_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26211_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26211, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26211_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26211_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26212_MI;
struct t20;
 void m26212 (t20 * __this, t3724* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26212 (t20 * __this, t3724* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.StringSplitOptions>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3724_0_0_0;
extern Il2CppType t3724_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26212_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3724_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26212_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26212_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26212, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26212_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26212_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26213_MI;
struct t20;
 bool m26213 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26213 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.StringSplitOptions>(T)
extern TypeInfo t20_TI;
extern Il2CppType t939_0_0_0;
static ParameterInfo t20_m26213_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t939_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26213_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26213_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26213, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26213_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26213_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26214_MI;
struct t20;
 int32_t m26214 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26214 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t939_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t939_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t939_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t939_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.StringSplitOptions>(T)
extern TypeInfo t20_TI;
extern Il2CppType t939_0_0_0;
static ParameterInfo t20_m26214_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t939_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26214_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26214_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26214, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26214_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26214_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26215_MI;
struct t20;
 void m26215 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26215 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.StringSplitOptions>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t939_0_0_0;
static ParameterInfo t20_m26215_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t939_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26215_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26215_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26215, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26215_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26215_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26216_MI;
struct t20;
extern MethodInfo m26217_MI;
struct t20;
 void m26217 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26217 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t939_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.StringSplitOptions>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t939_0_0_0;
static ParameterInfo t20_m26217_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t939_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26217_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26217_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26217, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26217_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26217_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.StringSplitOptions>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t939_1_0_0;
static ParameterInfo t20_m26216_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t939_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26216_GM;
extern void* RuntimeInvoker_t21_t44_t5048 (MethodInfo* method, void* obj, void** args);
MethodInfo m26216_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5048, t20_m26216_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26216_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3502.h"
extern TypeInfo t3502_TI;
#include "t3502MD.h"
extern MethodInfo m19452_MI;
extern MethodInfo m26218_MI;
struct t20;
 t29* m26218 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26218 (t20 * __this, MethodInfo* method){
	{
		t3502  L_0 = {0};
		m19452(&L_0, __this, &m19452_MI);
		t3502  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3502_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.StringSplitOptions>()
extern TypeInfo t20_TI;
extern TypeInfo t5049_TI;
extern Il2CppType t5049_0_0_0;
extern Il2CppGenericMethod m26218_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26218_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26218, &t20_TI, &t5049_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26218_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1671.h"
extern MethodInfo m26219_MI;
struct t20;
extern MethodInfo m26220_MI;
struct t20;
#define m26220(__this, p0, method) (t1671 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
// Metadata Definition T System.Array::InternalArray__get_Item<System.ThreadStaticAttribute>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1671_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26220_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1671_0_0_0;
extern Il2CppType t1671_0_0_0;
extern Il2CppGenericMethod m26220_GM;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26220_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m19490_gshared, &t20_TI, &t1671_0_0_0, RuntimeInvoker_t29_t44, t20_m26220_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26220_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.ThreadStaticAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1671_1_0_2;
extern Il2CppType t1671_1_0_0;
static ParameterInfo t20_m26219_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1671_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26219_GM;
extern void* RuntimeInvoker_t21_t44_t5050 (MethodInfo* method, void* obj, void** args);
MethodInfo m26219_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5050, t20_m26219_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26219_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26221_MI;
struct t20;
#define m26221(__this, p0, method) (void)m19494_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.ThreadStaticAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1671_0_0_0;
extern Il2CppType t1671_0_0_0;
static ParameterInfo t20_m26221_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1671_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26221_GM;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26221_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m19494_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t20_m26221_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26221_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26222_MI;
struct t20;
#define m26222(__this, p0, method) (bool)m19496_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.ThreadStaticAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1671_0_0_0;
static ParameterInfo t20_m26222_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1671_0_0_0},
};
extern TypeInfo t1671_TI;
static Il2CppRGCTXData m26222_RGCTXData[1] = 
{
	&t1671_TI/* Class Usage */,
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26222_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26222_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m19496_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26222_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26222_RGCTXData, NULL, &m26222_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26223_MI;
struct t20;
#define m26223(__this, p0, p1, method) (void)m19498_gshared((t20 *)__this, (t316*)p0, (int32_t)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.ThreadStaticAttribute>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3725_0_0_0;
extern Il2CppType t3725_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26223_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3725_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26223_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26223_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m19498_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26223_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26223_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26224_MI;
struct t20;
#define m26224(__this, p0, method) (bool)m19499_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.ThreadStaticAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1671_0_0_0;
static ParameterInfo t20_m26224_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1671_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26224_GM;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26224_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m19499_gshared, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t20_m26224_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26224_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26225_MI;
struct t20;
#define m26225(__this, p0, method) (int32_t)m19500_gshared((t20 *)__this, (t29 *)p0, method)
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.ThreadStaticAttribute>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1671_0_0_0;
static ParameterInfo t20_m26225_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1671_0_0_0},
};
extern TypeInfo t1671_TI;
static Il2CppRGCTXData m26225_RGCTXData[1] = 
{
	&t1671_TI/* Class Usage */,
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26225_GM;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26225_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m19500_gshared, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t20_m26225_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, m26225_RGCTXData, NULL, &m26225_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26226_MI;
struct t20;
#define m26226(__this, p0, p1, method) (void)m19501_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.ThreadStaticAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1671_0_0_0;
static ParameterInfo t20_m26226_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1671_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26226_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26226_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m19501_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26226_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26226_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26227_MI;
struct t20;
extern MethodInfo m26228_MI;
struct t20;
#define m26228(__this, p0, p1, method) (void)m19503_gshared((t20 *)__this, (int32_t)p0, (t29 *)p1, method)
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.ThreadStaticAttribute>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1671_0_0_0;
static ParameterInfo t20_m26228_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1671_0_0_0},
};
extern TypeInfo t1671_TI;
static Il2CppRGCTXData m26228_RGCTXData[1] = 
{
	&t1671_TI/* Class Usage */,
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26228_GM;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26228_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m19503_gshared, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t20_m26228_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, m26228_RGCTXData, NULL, &m26228_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.ThreadStaticAttribute>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1671_1_0_0;
static ParameterInfo t20_m26227_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1671_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26227_GM;
extern void* RuntimeInvoker_t21_t44_t5050 (MethodInfo* method, void* obj, void** args);
MethodInfo m26227_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5050, t20_m26227_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26227_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3503.h"
extern TypeInfo t3503_TI;
#include "t3503MD.h"
extern MethodInfo m19457_MI;
extern MethodInfo m26229_MI;
struct t20;
#define m26229(__this, method) (t29*)m19504_gshared((t20 *)__this, method)
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.ThreadStaticAttribute>()
extern TypeInfo t20_TI;
extern TypeInfo t5051_TI;
extern TypeInfo t3503_TI;
static Il2CppRGCTXData m26229_RGCTXData[2] = 
{
	&t3503_TI/* Class Usage */,
	&m19457_MI/* Method Usage */,
};
extern Il2CppType t5051_0_0_0;
extern Il2CppGenericMethod m26229_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26229_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m19504_gshared, &t20_TI, &t5051_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, m26229_RGCTXData, NULL, &m26229_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1127.h"
extern MethodInfo m26230_MI;
struct t20;
extern MethodInfo m26231_MI;
struct t20;
 int32_t m26231 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26231 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.TypeCode>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1127_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26231_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1127_0_0_0;
extern Il2CppType t1127_0_0_0;
extern Il2CppGenericMethod m26231_GM;
extern void* RuntimeInvoker_t1127_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26231_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26231, &t20_TI, &t1127_0_0_0, RuntimeInvoker_t1127_t44, t20_m26231_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26231_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.TypeCode>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1127_1_0_2;
extern Il2CppType t1127_1_0_0;
static ParameterInfo t20_m26230_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1127_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26230_GM;
extern void* RuntimeInvoker_t21_t44_t5052 (MethodInfo* method, void* obj, void** args);
MethodInfo m26230_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5052, t20_m26230_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26230_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26232_MI;
struct t20;
 void m26232 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26232 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.TypeCode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1127_0_0_0;
extern Il2CppType t1127_0_0_0;
static ParameterInfo t20_m26232_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1127_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26232_GM;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26232_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26232, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t20_m26232_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26232_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26233_MI;
struct t20;
 bool m26233 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26233 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1127_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1127_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		int32_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1127_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1127_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.TypeCode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1127_0_0_0;
static ParameterInfo t20_m26233_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1127_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26233_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26233_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26233, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26233_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26233_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26234_MI;
struct t20;
 void m26234 (t20 * __this, t3726* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26234 (t20 * __this, t3726* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.TypeCode>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3726_0_0_0;
extern Il2CppType t3726_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26234_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3726_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26234_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26234_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26234, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26234_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26234_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26235_MI;
struct t20;
 bool m26235 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26235 (t20 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.TypeCode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1127_0_0_0;
static ParameterInfo t20_m26235_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1127_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26235_GM;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26235_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26235, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t20_m26235_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26235_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26236_MI;
struct t20;
 int32_t m26236 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26236 (t20 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		int32_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1127_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		int32_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1127_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		int32_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1127_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1127_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.TypeCode>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1127_0_0_0;
static ParameterInfo t20_m26236_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1127_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26236_GM;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26236_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26236, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t20_m26236_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26236_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26237_MI;
struct t20;
 void m26237 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26237 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.TypeCode>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1127_0_0_0;
static ParameterInfo t20_m26237_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1127_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26237_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26237_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26237, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26237_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26237_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26238_MI;
struct t20;
extern MethodInfo m26239_MI;
struct t20;
 void m26239 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26239 (t20 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1127_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.TypeCode>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1127_0_0_0;
static ParameterInfo t20_m26239_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1127_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26239_GM;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26239_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26239, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t20_m26239_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26239_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.TypeCode>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1127_1_0_0;
static ParameterInfo t20_m26238_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1127_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26238_GM;
extern void* RuntimeInvoker_t21_t44_t5052 (MethodInfo* method, void* obj, void** args);
MethodInfo m26238_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5052, t20_m26238_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26238_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3508.h"
extern TypeInfo t3508_TI;
#include "t3508MD.h"
extern MethodInfo m19479_MI;
extern MethodInfo m26240_MI;
struct t20;
 t29* m26240 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26240 (t20 * __this, MethodInfo* method){
	{
		t3508  L_0 = {0};
		m19479(&L_0, __this, &m19479_MI);
		t3508  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3508_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.TypeCode>()
extern TypeInfo t20_TI;
extern TypeInfo t5053_TI;
extern Il2CppType t5053_0_0_0;
extern Il2CppGenericMethod m26240_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26240_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26240, &t20_TI, &t5053_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26240_GM};
#ifndef _MSC_VER
#else
#endif

#include "t1677.h"
extern MethodInfo m26241_MI;
struct t20;
extern MethodInfo m26242_MI;
struct t20;
 uint8_t m26242 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m26242 (t20 * __this, int32_t p0, MethodInfo* method){
	uint8_t V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		ArrayGetGenericValueImpl (__this, p0, (&V_0));
		return V_0;
	}
}
// Metadata Definition T System.Array::InternalArray__get_Item<System.UnitySerializationHolder/UnityType>(System.Int32)
extern TypeInfo t20_TI;
extern TypeInfo t1677_TI;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26242_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppGenericInst GenInst_t1677_0_0_0;
extern Il2CppType t1677_0_0_0;
extern Il2CppGenericMethod m26242_GM;
extern void* RuntimeInvoker_t1677_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26242_MI = 
{
	"InternalArray__get_Item", (methodPointerType)&m26242, &t20_TI, &t1677_0_0_0, RuntimeInvoker_t1677_t44, t20_m26242_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26242_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::GetGenericValueImpl<System.UnitySerializationHolder/UnityType>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1677_1_0_2;
extern Il2CppType t1677_1_0_0;
static ParameterInfo t20_m26241_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1677_1_0_2},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26241_GM;
extern void* RuntimeInvoker_t21_t44_t5054 (MethodInfo* method, void* obj, void** args);
MethodInfo m26241_MI = 
{
	"GetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5054, t20_m26241_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26241_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26243_MI;
struct t20;
 void m26243 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26243 (t20 * __this, uint8_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_Add<System.UnitySerializationHolder/UnityType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1677_0_0_0;
extern Il2CppType t1677_0_0_0;
static ParameterInfo t20_m26243_ParameterInfos[] = 
{
	{"item", 0, 134218871, &EmptyCustomAttributesCache, &t1677_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26243_GM;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m26243_MI = 
{
	"InternalArray__ICollection_Add", (methodPointerType)&m26243, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t20_m26243_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26243_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26244_MI;
struct t20;
 bool m26244 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26244 (t20 * __this, uint8_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_005c;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1677_TI), &L_4);
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		uint8_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1677_TI), &L_6);
		if (L_7)
		{
			goto IL_003f;
		}
	}
	{
		return 1;
	}

IL_003f:
	{
		return 0;
	}

IL_0041:
	{
		uint8_t L_8 = V_2;
		t29 * L_9 = Box(InitializedTypeInfo(&t1677_TI), &L_8);
		bool L_10 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1677_TI), &(*(&p0))), L_9);
		if (!L_10)
		{
			goto IL_0058;
		}
	}
	{
		return 1;
	}

IL_0058:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_005c:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		return 0;
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Contains<System.UnitySerializationHolder/UnityType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1677_0_0_0;
static ParameterInfo t20_m26244_ParameterInfos[] = 
{
	{"item", 0, 134218873, &EmptyCustomAttributesCache, &t1677_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26244_GM;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m26244_MI = 
{
	"InternalArray__ICollection_Contains", (methodPointerType)&m26244, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t20_m26244_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26244_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26245_MI;
struct t20;
 void m26245 (t20 * __this, t3727* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26245 (t20 * __this, t3727* p0, int32_t p1, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral197, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		int32_t L_1 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0027;
		}
	}
	{
		t7* L_2 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_3 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_3, L_2, &m9495_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0027:
	{
		int32_t L_4 = m5911(__this, 0, &m5911_MI);
		int32_t L_5 = m5913(p0, 0, &m5913_MI);
		int32_t L_6 = m5911(p0, 0, &m5911_MI);
		if ((((int32_t)((int32_t)(p1+L_4))) <= ((int32_t)((int32_t)(L_5+L_6)))))
		{
			goto IL_004c;
		}
	}
	{
		t305 * L_7 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_7, (t7*) &_stringLiteral993, &m1935_MI);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_004c:
	{
		int32_t L_8 = m3977(p0, &m3977_MI);
		if ((((int32_t)L_8) <= ((int32_t)1)))
		{
			goto IL_0065;
		}
	}
	{
		t7* L_9 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_10 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_10, L_9, &m9495_MI);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_0065:
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_007e;
		}
	}
	{
		t7* L_11 = m6079(NULL, (t7*) &_stringLiteral994, &m6079_MI);
		t915 * L_12 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3968(L_12, (t7*) &_stringLiteral199, L_11, &m3968_MI);
		il2cpp_codegen_raise_exception(L_12);
	}

IL_007e:
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		int32_t L_14 = m5911(__this, 0, &m5911_MI);
		m5952(NULL, __this, L_13, (t20 *)(t20 *)p0, p1, L_14, &m5952_MI);
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__ICollection_CopyTo<System.UnitySerializationHolder/UnityType>(T[],System.Int32)
extern TypeInfo t20_TI;
extern Il2CppType t3727_0_0_0;
extern Il2CppType t3727_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t20_m26245_ParameterInfos[] = 
{
	{"array", 0, 134218874, &EmptyCustomAttributesCache, &t3727_0_0_0},
	{"index", 1, 134218875, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26245_GM;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
MethodInfo m26245_MI = 
{
	"InternalArray__ICollection_CopyTo", (methodPointerType)&m26245, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t20_m26245_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26245_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26246_MI;
struct t20;
 bool m26246 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m26246 (t20 * __this, uint8_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Boolean System.Array::InternalArray__ICollection_Remove<System.UnitySerializationHolder/UnityType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1677_0_0_0;
static ParameterInfo t20_m26246_ParameterInfos[] = 
{
	{"item", 0, 134218872, &EmptyCustomAttributesCache, &t1677_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern Il2CppGenericMethod m26246_GM;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m26246_MI = 
{
	"InternalArray__ICollection_Remove", (methodPointerType)&m26246, &t20_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t20_m26246_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26246_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26247_MI;
struct t20;
 int32_t m26247 (t20 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m26247 (t20 * __this, uint8_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = {0};
	{
		int32_t L_0 = m3977(__this, &m3977_MI);
		if ((((int32_t)L_0) <= ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		t7* L_1 = m6079(NULL, (t7*) &_stringLiteral992, &m6079_MI);
		t1667 * L_2 = (t1667 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1667_TI));
		m9495(L_2, L_1, &m9495_MI);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0019:
	{
		int32_t L_3 = m3969(__this, &m3969_MI);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0074;
	}

IL_0024:
	{
		ArrayGetGenericValueImpl (__this, V_1, (&V_2));
		uint8_t L_4 = p0;
		t29 * L_5 = Box(InitializedTypeInfo(&t1677_TI), &L_4);
		if (L_5)
		{
			goto IL_0051;
		}
	}
	{
		uint8_t L_6 = V_2;
		t29 * L_7 = Box(InitializedTypeInfo(&t1677_TI), &L_6);
		if (L_7)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_8 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_8));
	}

IL_0047:
	{
		int32_t L_9 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_9-1));
	}

IL_0051:
	{
		uint8_t L_10 = p0;
		t29 * L_11 = Box(InitializedTypeInfo(&t1677_TI), &L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t1677_TI), &(*(&V_2))), L_11);
		if (!L_12)
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_13 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(V_1+L_13));
	}

IL_0070:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0074:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_14 = m5913(__this, 0, &m5913_MI);
		return ((int32_t)(L_14-1));
	}
}
// Metadata Definition System.Int32 System.Array::InternalArray__IndexOf<System.UnitySerializationHolder/UnityType>(T)
extern TypeInfo t20_TI;
extern Il2CppType t1677_0_0_0;
static ParameterInfo t20_m26247_ParameterInfos[] = 
{
	{"item", 0, 134218879, &EmptyCustomAttributesCache, &t1677_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern Il2CppGenericMethod m26247_GM;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m26247_MI = 
{
	"InternalArray__IndexOf", (methodPointerType)&m26247, &t20_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t20_m26247_ParameterInfos, NULL, 131, 0, 255, 1, false, true, 0, NULL, NULL, &m26247_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26248_MI;
struct t20;
 void m26248 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26248 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m3988(L_0, (t7*) &_stringLiteral218, &m3988_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition System.Void System.Array::InternalArray__Insert<System.UnitySerializationHolder/UnityType>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1677_0_0_0;
static ParameterInfo t20_m26248_ParameterInfos[] = 
{
	{"index", 0, 134218876, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218877, &EmptyCustomAttributesCache, &t1677_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26248_GM;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m26248_MI = 
{
	"InternalArray__Insert", (methodPointerType)&m26248, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t20_m26248_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26248_GM};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m26249_MI;
struct t20;
extern MethodInfo m26250_MI;
struct t20;
 void m26250 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m26250 (t20 * __this, int32_t p0, uint8_t p1, MethodInfo* method){
	t316* V_0 = {0};
	{
		int32_t L_0 = m3969(__this, &m3969_MI);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		V_0 = ((t316*)IsInst(__this, InitializedTypeInfo(&t316_TI)));
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		uint8_t L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t1677_TI), &L_2);
		ArrayElementTypeCheck (V_0, L_3);
		*((t29 **)(t29 **)SZArrayLdElema(V_0, p0)) = (t29 *)L_3;
		return;
	}

IL_0028:
	{
		ArraySetGenericValueImpl (__this, p0, (&p1));
		return;
	}
}
// Metadata Definition System.Void System.Array::InternalArray__set_Item<System.UnitySerializationHolder/UnityType>(System.Int32,T)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1677_0_0_0;
static ParameterInfo t20_m26250_ParameterInfos[] = 
{
	{"index", 0, 134218881, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134218882, &EmptyCustomAttributesCache, &t1677_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26250_GM;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
MethodInfo m26250_MI = 
{
	"InternalArray__set_Item", (methodPointerType)&m26250, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t20_m26250_ParameterInfos, NULL, 131, 0, 255, 2, false, true, 0, NULL, NULL, &m26250_GM};
#ifndef _MSC_VER
#else
#endif

// Metadata Definition System.Void System.Array::SetGenericValueImpl<System.UnitySerializationHolder/UnityType>(System.Int32,T&)
extern TypeInfo t20_TI;
extern Il2CppType t44_0_0_0;
extern Il2CppType t1677_1_0_0;
static ParameterInfo t20_m26249_ParameterInfos[] = 
{
	{"pos", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1677_1_0_0},
};
extern Il2CppType t21_0_0_0;
extern Il2CppGenericMethod m26249_GM;
extern void* RuntimeInvoker_t21_t44_t5054 (MethodInfo* method, void* obj, void** args);
MethodInfo m26249_MI = 
{
	"SetGenericValueImpl", NULL, &t20_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t5054, t20_m26249_ParameterInfos, NULL, 131, 4096, 255, 2, false, true, 0, NULL, NULL, &m26249_GM};
#ifndef _MSC_VER
#else
#endif

#include "t3509.h"
extern TypeInfo t3509_TI;
#include "t3509MD.h"
extern MethodInfo m19484_MI;
extern MethodInfo m26251_MI;
struct t20;
 t29* m26251 (t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m26251 (t20 * __this, MethodInfo* method){
	{
		t3509  L_0 = {0};
		m19484(&L_0, __this, &m19484_MI);
		t3509  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t3509_TI), &L_1);
		return (t29*)L_2;
	}
}
// Metadata Definition System.Collections.Generic.IEnumerator`1<T> System.Array::InternalArray__IEnumerable_GetEnumerator<System.UnitySerializationHolder/UnityType>()
extern TypeInfo t20_TI;
extern TypeInfo t5055_TI;
extern Il2CppType t5055_0_0_0;
extern Il2CppGenericMethod m26251_GM;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
MethodInfo m26251_MI = 
{
	"InternalArray__IEnumerable_GetEnumerator", (methodPointerType)&m26251, &t20_TI, &t5055_0_0_0, RuntimeInvoker_t29, NULL, NULL, 131, 0, 255, 0, false, true, 0, NULL, NULL, &m26251_GM};
