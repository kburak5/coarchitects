﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t858;
struct t29;
struct t20;
struct t136;

 void m4276 (t858 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m6661 (t858 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6662 (t858 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6663 (t858 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4256 (t858 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4277 (t858 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4255 (t858 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6664 (t858 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6665 (t858 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6666 (t858 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6667 (t858 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6668 (t858 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
