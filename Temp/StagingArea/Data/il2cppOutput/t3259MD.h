﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3259;
struct t29;
struct t20;
#include "t1037.h"

 void m18091 (t3259 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18092 (t3259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18093 (t3259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18094 (t3259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m18095 (t3259 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
