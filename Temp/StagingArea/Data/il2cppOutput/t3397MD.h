﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3397;
struct t29;
struct t20;
#include "t1148.h"

 void m18855 (t3397 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18856 (t3397 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18857 (t3397 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18858 (t3397 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18859 (t3397 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
