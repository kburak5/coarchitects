﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1598;
struct t781;
struct t200;

 void m8749 (t1598 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8750 (t1598 * __this, t781* p0, int32_t p1, int32_t p2, t200* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
