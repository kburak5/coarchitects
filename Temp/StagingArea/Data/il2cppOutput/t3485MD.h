﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3485;
#include "t465.h"

 void m19371 (t3485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19372 (t3485 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19373 (t3485 * __this, t465  p0, t465  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
