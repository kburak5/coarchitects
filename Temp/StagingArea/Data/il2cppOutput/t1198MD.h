﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1198;
struct t1196;
struct t29;

 void m6198 (t1198 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6199 (t1198 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6200 (t1198 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1196 * m6201 (t1198 * __this, int32_t p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6202 (t1198 * __this, t1196 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
