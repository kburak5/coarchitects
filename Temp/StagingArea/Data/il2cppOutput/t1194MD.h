﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1194;
struct t1195;
struct t1196;
#include "t1197.h"

 void m6194 (t1194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6195 (t1194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1195 * m6196 (t1194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6197 (t1194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
