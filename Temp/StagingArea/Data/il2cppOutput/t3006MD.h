﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3006;
struct t29;
struct t20;

 void m16434 (t3006 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16435 (t3006 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16436 (t3006 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16437 (t3006 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m16438 (t3006 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
