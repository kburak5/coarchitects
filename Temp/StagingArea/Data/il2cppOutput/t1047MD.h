﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1047;
struct t29;
struct t777;
struct t745;
struct t7;
struct t66;
struct t67;
#include "t35.h"

 void m5096 (t1047 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t777 * m5097 (t1047 * __this, t745 * p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5098 (t1047 * __this, t745 * p0, t7* p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t777 * m5099 (t1047 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
