﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1208;
struct t976;
struct t1207;
struct t781;
#include "t936.h"

 void m6299 (t1208 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6300 (t1208 * __this, t1207 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6301 (t1208 * __this, t1207 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6302 (t1208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6303 (t1208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6304 (t1208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6305 (t1208 * __this, t781* p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6306 (t1208 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t976 * m6307 (t1208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6308 (t1208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6309 (t1208 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6310 (t1208 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t936  m6311 (t1208 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6312 (t1208 * __this, t936  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6313 (t1208 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6314 (t1208 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6315 (t1208 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
