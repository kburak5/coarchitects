﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3486;
struct t29;
struct t20;
#include "t1627.h"

 void m19374 (t3486 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19375 (t3486 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19376 (t3486 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19377 (t3486 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19378 (t3486 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
