﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t261;

 void m1104 (t261 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1105 (t261 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1106 (t261 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1107 (t261 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1108 (t261 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1109 (t261 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1110 (t261 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1111 (t261 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1112 (t261 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
