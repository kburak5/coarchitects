﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1692;
struct t1692_marshaled;

void t1692_marshal(const t1692& unmarshaled, t1692_marshaled& marshaled);
void t1692_marshal_back(const t1692_marshaled& marshaled, t1692& unmarshaled);
void t1692_marshal_cleanup(t1692_marshaled& marshaled);
