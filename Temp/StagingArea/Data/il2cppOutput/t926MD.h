﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t926;
struct t29;
struct t726;
struct t136;
struct t722;
struct t20;

 void m6745 (t926 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4020 (t926 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6746 (t926 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6747 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6748 (t926 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4024 (t926 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6749 (t926 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6750 (t926 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6751 (t926 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6752 (t926 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6753 (t926 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6754 (t926 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6755 (t926 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4022 (t926 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6756 (t926 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6757 (t926 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6758 (t926 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6759 (t926 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6760 (t926 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6761 (t926 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4021 (t926 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4023 (t926 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6762 (t926 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6763 (t926 * __this, t29 * p0, t29 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6764 (t926 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6765 (t926 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6766 (t926 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
