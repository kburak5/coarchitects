﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1375;
struct t42;
struct t7;
struct t316;
struct t29;
struct t1144;
struct t631;
struct t633;
struct t733;
#include "t1347.h"
#include "t927.h"
#include "t630.h"
#include "t735.h"

 void m7589 (t1375 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7590 (t1375 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t927  m7591 (t1375 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7592 (t1375 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7593 (t1375 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7594 (t1375 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7595 (t1375 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7596 (t1375 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7597 (t1375 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7598 (t1375 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m7599 (t1375 * __this, t42 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7600 (t1375 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7601 (t1375 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7602 (t1375 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7603 (t1375 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7604 (t29 * __this, t1144 * p0, t29 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7605 (t1375 * __this, t29 * p0, t29 * p1, int32_t p2, t631 * p3, t633 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7606 (t1375 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7607 (t1375 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
