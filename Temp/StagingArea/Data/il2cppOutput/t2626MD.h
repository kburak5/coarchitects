﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2626;
struct t29;
struct t20;
#include "t215.h"

 void m14127 (t2626 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14128 (t2626 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14129 (t2626 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m14130 (t2626 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m14131 (t2626 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
