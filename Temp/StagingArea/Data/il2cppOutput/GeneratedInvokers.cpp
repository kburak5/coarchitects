﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
struct t29;
#include "t29.h"
#include "t21.h"
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

struct t29;
#include "t40.h"
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(&t40_TI, &ret);
}

struct t29;
#include "t194.h"
extern TypeInfo t194_TI;
void* RuntimeInvoker_t194 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(&t194_TI, &ret);
}

struct t29;
#include "t297.h"
extern TypeInfo t297_TI;
void* RuntimeInvoker_t297 (MethodInfo* method, void* obj, void** args){
	typedef int8_t (*Func)(void* obj, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(&t297_TI, &ret);
}

struct t29;
#include "t348.h"
extern TypeInfo t348_TI;
void* RuntimeInvoker_t348 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(&t348_TI, &ret);
}

struct t29;
#include "t372.h"
extern TypeInfo t372_TI;
void* RuntimeInvoker_t372 (MethodInfo* method, void* obj, void** args){
	typedef int16_t (*Func)(void* obj, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(&t372_TI, &ret);
}

struct t29;
#include "t626.h"
extern TypeInfo t626_TI;
void* RuntimeInvoker_t626 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(&t626_TI, &ret);
}

struct t29;
#include "t44.h"
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t44_TI, &ret);
}

struct t29;
#include "t344.h"
extern TypeInfo t344_TI;
void* RuntimeInvoker_t344 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(&t344_TI, &ret);
}

struct t29;
#include "t919.h"
extern TypeInfo t919_TI;
void* RuntimeInvoker_t919 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(&t919_TI, &ret);
}

struct t29;
#include "t1089.h"
extern TypeInfo t1089_TI;
void* RuntimeInvoker_t1089 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(&t1089_TI, &ret);
}

struct t29;
#include "t22.h"
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(&t22_TI, &ret);
}

struct t29;
#include "t601.h"
extern TypeInfo t601_TI;
void* RuntimeInvoker_t601 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(&t601_TI, &ret);
}

struct t29;
#include "t17.h"
extern TypeInfo t17_TI;
void* RuntimeInvoker_t17 (MethodInfo* method, void* obj, void** args){
	typedef t17  (*Func)(void* obj, MethodInfo* method);
	t17  ret = ((Func)method->method)(obj, method);
	return Box(&t17_TI, &ret);
}

struct t29;
#include "t106.h"
extern TypeInfo t106_TI;
void* RuntimeInvoker_t106 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t106_TI, &ret);
}

struct t29;
#include "t57.h"
extern TypeInfo t57_TI;
void* RuntimeInvoker_t57 (MethodInfo* method, void* obj, void** args){
	typedef t57  (*Func)(void* obj, MethodInfo* method);
	t57  ret = ((Func)method->method)(obj, method);
	return Box(&t57_TI, &ret);
}

struct t29;
#include "t23.h"
extern TypeInfo t23_TI;
void* RuntimeInvoker_t23 (MethodInfo* method, void* obj, void** args){
	typedef t23  (*Func)(void* obj, MethodInfo* method);
	t23  ret = ((Func)method->method)(obj, method);
	return Box(&t23_TI, &ret);
}

struct t29;
#include "t111.h"
extern TypeInfo t111_TI;
void* RuntimeInvoker_t111 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t111_TI, &ret);
}

struct t29;
#include "t120.h"
extern TypeInfo t120_TI;
void* RuntimeInvoker_t120 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t120_TI, &ret);
}

struct t29;
#include "t126.h"
extern TypeInfo t126_TI;
void* RuntimeInvoker_t126 (MethodInfo* method, void* obj, void** args){
	typedef t126  (*Func)(void* obj, MethodInfo* method);
	t126  ret = ((Func)method->method)(obj, method);
	return Box(&t126_TI, &ret);
}

struct t29;
#include "t132.h"
extern TypeInfo t132_TI;
void* RuntimeInvoker_t132 (MethodInfo* method, void* obj, void** args){
	typedef t132  (*Func)(void* obj, MethodInfo* method);
	t132  ret = ((Func)method->method)(obj, method);
	return Box(&t132_TI, &ret);
}

struct t29;
#include "t128.h"
extern TypeInfo t128_TI;
void* RuntimeInvoker_t128 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t128_TI, &ret);
}

struct t29;
#include "t146.h"
extern TypeInfo t146_TI;
void* RuntimeInvoker_t146 (MethodInfo* method, void* obj, void** args){
	typedef t146  (*Func)(void* obj, MethodInfo* method);
	t146  ret = ((Func)method->method)(obj, method);
	return Box(&t146_TI, &ret);
}

struct t29;
#include "t149.h"
extern TypeInfo t149_TI;
void* RuntimeInvoker_t149 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t149_TI, &ret);
}

struct t29;
#include "t150.h"
extern TypeInfo t150_TI;
void* RuntimeInvoker_t150 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t150_TI, &ret);
}

struct t29;
#include "t151.h"
extern TypeInfo t151_TI;
void* RuntimeInvoker_t151 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t151_TI, &ret);
}

struct t29;
#include "t152.h"
extern TypeInfo t152_TI;
void* RuntimeInvoker_t152 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t152_TI, &ret);
}

struct t29;
#include "t164.h"
extern TypeInfo t164_TI;
void* RuntimeInvoker_t164 (MethodInfo* method, void* obj, void** args){
	typedef t164  (*Func)(void* obj, MethodInfo* method);
	t164  ret = ((Func)method->method)(obj, method);
	return Box(&t164_TI, &ret);
}

struct t29;
#include "t165.h"
extern TypeInfo t165_TI;
void* RuntimeInvoker_t165 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t165_TI, &ret);
}

struct t29;
#include "t172.h"
extern TypeInfo t172_TI;
void* RuntimeInvoker_t172 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t172_TI, &ret);
}

struct t29;
#include "t173.h"
extern TypeInfo t173_TI;
void* RuntimeInvoker_t173 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t173_TI, &ret);
}

struct t29;
#include "t185.h"
extern TypeInfo t185_TI;
void* RuntimeInvoker_t185 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t185_TI, &ret);
}

struct t29;
#include "t188.h"
extern TypeInfo t188_TI;
void* RuntimeInvoker_t188 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t188_TI, &ret);
}

struct t29;
#include "t186.h"
extern TypeInfo t186_TI;
void* RuntimeInvoker_t186 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t186_TI, &ret);
}

struct t29;
#include "t205.h"
extern TypeInfo t205_TI;
void* RuntimeInvoker_t205 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t205_TI, &ret);
}

struct t29;
#include "t187.h"
extern TypeInfo t187_TI;
void* RuntimeInvoker_t187 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t187_TI, &ret);
}

struct t29;
#include "t209.h"
extern TypeInfo t209_TI;
void* RuntimeInvoker_t209 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t209_TI, &ret);
}

struct t29;
#include "t210.h"
extern TypeInfo t210_TI;
void* RuntimeInvoker_t210 (MethodInfo* method, void* obj, void** args){
	typedef t210  (*Func)(void* obj, MethodInfo* method);
	t210  ret = ((Func)method->method)(obj, method);
	return Box(&t210_TI, &ret);
}

struct t29;
#include "t212.h"
extern TypeInfo t212_TI;
void* RuntimeInvoker_t212 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t212_TI, &ret);
}

struct t29;
#include "t215.h"
extern TypeInfo t215_TI;
void* RuntimeInvoker_t215 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t215_TI, &ret);
}

struct t29;
#include "t219.h"
extern TypeInfo t219_TI;
void* RuntimeInvoker_t219 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t219_TI, &ret);
}

struct t29;
#include "t224.h"
extern TypeInfo t224_TI;
void* RuntimeInvoker_t224 (MethodInfo* method, void* obj, void** args){
	typedef t224  (*Func)(void* obj, MethodInfo* method);
	t224  ret = ((Func)method->method)(obj, method);
	return Box(&t224_TI, &ret);
}

struct t29;
#include "t225.h"
extern TypeInfo t225_TI;
void* RuntimeInvoker_t225 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t225_TI, &ret);
}

struct t29;
#include "t228.h"
extern TypeInfo t228_TI;
void* RuntimeInvoker_t228 (MethodInfo* method, void* obj, void** args){
	typedef t228  (*Func)(void* obj, MethodInfo* method);
	t228  ret = ((Func)method->method)(obj, method);
	return Box(&t228_TI, &ret);
}

struct t29;
#include "t207.h"
extern TypeInfo t207_TI;
void* RuntimeInvoker_t207 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t207_TI, &ret);
}

struct t29;
#include "t231.h"
extern TypeInfo t231_TI;
void* RuntimeInvoker_t231 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t231_TI, &ret);
}

struct t29;
#include "t233.h"
extern TypeInfo t233_TI;
void* RuntimeInvoker_t233 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t233_TI, &ret);
}

struct t29;
#include "t247.h"
extern TypeInfo t247_TI;
void* RuntimeInvoker_t247 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t247_TI, &ret);
}

struct t29;
#include "t249.h"
extern TypeInfo t249_TI;
void* RuntimeInvoker_t249 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t249_TI, &ret);
}

struct t29;
#include "t250.h"
extern TypeInfo t250_TI;
void* RuntimeInvoker_t250 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t250_TI, &ret);
}

struct t29;
#include "t251.h"
extern TypeInfo t251_TI;
void* RuntimeInvoker_t251 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t251_TI, &ret);
}

struct t29;
#include "t253.h"
extern TypeInfo t253_TI;
void* RuntimeInvoker_t253 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t253_TI, &ret);
}

struct t29;
#include "t255.h"
extern TypeInfo t255_TI;
void* RuntimeInvoker_t255 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t255_TI, &ret);
}

struct t29;
#include "t256.h"
extern TypeInfo t256_TI;
void* RuntimeInvoker_t256 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t256_TI, &ret);
}

struct t29;
#include "t257.h"
extern TypeInfo t257_TI;
void* RuntimeInvoker_t257 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t257_TI, &ret);
}

struct t29;
#include "t384.h"
extern TypeInfo t384_TI;
void* RuntimeInvoker_t384 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t384_TI, &ret);
}

struct t29;
#include "t382.h"
extern TypeInfo t382_TI;
void* RuntimeInvoker_t382 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t382_TI, &ret);
}

struct t29;
#include "t383.h"
extern TypeInfo t383_TI;
void* RuntimeInvoker_t383 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t383_TI, &ret);
}

struct t29;
#include "t396.h"
extern TypeInfo t396_TI;
void* RuntimeInvoker_t396 (MethodInfo* method, void* obj, void** args){
	typedef t396  (*Func)(void* obj, MethodInfo* method);
	t396  ret = ((Func)method->method)(obj, method);
	return Box(&t396_TI, &ret);
}

struct t29;
#include "t183.h"
extern TypeInfo t183_TI;
void* RuntimeInvoker_t183 (MethodInfo* method, void* obj, void** args){
	typedef t183  (*Func)(void* obj, MethodInfo* method);
	t183  ret = ((Func)method->method)(obj, method);
	return Box(&t183_TI, &ret);
}

struct t29;
#include "t376.h"
extern TypeInfo t376_TI;
void* RuntimeInvoker_t376 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t376_TI, &ret);
}

struct t29;
#include "t498.h"
extern TypeInfo t498_TI;
void* RuntimeInvoker_t498 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t498_TI, &ret);
}

struct t29;
#include "t503.h"
extern TypeInfo t503_TI;
void* RuntimeInvoker_t503 (MethodInfo* method, void* obj, void** args){
	typedef t503  (*Func)(void* obj, MethodInfo* method);
	t503  ret = ((Func)method->method)(obj, method);
	return Box(&t503_TI, &ret);
}

struct t29;
#include "t319.h"
extern TypeInfo t319_TI;
void* RuntimeInvoker_t319 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t319_TI, &ret);
}

struct t29;
#include "t362.h"
extern TypeInfo t362_TI;
void* RuntimeInvoker_t362 (MethodInfo* method, void* obj, void** args){
	typedef t362  (*Func)(void* obj, MethodInfo* method);
	t362  ret = ((Func)method->method)(obj, method);
	return Box(&t362_TI, &ret);
}

struct t29;
#include "t445.h"
extern TypeInfo t445_TI;
void* RuntimeInvoker_t445 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t445_TI, &ret);
}

struct t29;
#include "t515.h"
extern TypeInfo t515_TI;
void* RuntimeInvoker_t515 (MethodInfo* method, void* obj, void** args){
	typedef t515  (*Func)(void* obj, MethodInfo* method);
	t515  ret = ((Func)method->method)(obj, method);
	return Box(&t515_TI, &ret);
}

struct t29;
#include "t516.h"
extern TypeInfo t516_TI;
void* RuntimeInvoker_t516 (MethodInfo* method, void* obj, void** args){
	typedef t516  (*Func)(void* obj, MethodInfo* method);
	t516  ret = ((Func)method->method)(obj, method);
	return Box(&t516_TI, &ret);
}

struct t29;
#include "t361.h"
extern TypeInfo t361_TI;
void* RuntimeInvoker_t361 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t361_TI, &ret);
}

struct t29;
#include "t554.h"
extern TypeInfo t554_TI;
void* RuntimeInvoker_t554 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t554_TI, &ret);
}

struct t29;
#include "t725.h"
extern TypeInfo t725_TI;
void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args){
	typedef t725  (*Func)(void* obj, MethodInfo* method);
	t725  ret = ((Func)method->method)(obj, method);
	return Box(&t725_TI, &ret);
}

struct t29;
#include "t737.h"
extern TypeInfo t737_TI;
void* RuntimeInvoker_t737 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t737_TI, &ret);
}

struct t29;
#include "t742.h"
extern TypeInfo t742_TI;
void* RuntimeInvoker_t742 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t742_TI, &ret);
}

struct t29;
#include "t465.h"
extern TypeInfo t465_TI;
void* RuntimeInvoker_t465 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, method);
	return Box(&t465_TI, &ret);
}

struct t29;
#include "t766.h"
extern TypeInfo t766_TI;
void* RuntimeInvoker_t766 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t766_TI, &ret);
}

struct t29;
#include "t811.h"
extern TypeInfo t811_TI;
void* RuntimeInvoker_t811 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t811_TI, &ret);
}

struct t29;
#include "t814.h"
extern TypeInfo t814_TI;
void* RuntimeInvoker_t814 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t814_TI, &ret);
}

struct t29;
#include "t815.h"
extern TypeInfo t815_TI;
void* RuntimeInvoker_t815 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t815_TI, &ret);
}

struct t29;
#include "t817.h"
extern TypeInfo t817_TI;
void* RuntimeInvoker_t817 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t817_TI, &ret);
}

struct t29;
#include "t821.h"
extern TypeInfo t821_TI;
void* RuntimeInvoker_t821 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t821_TI, &ret);
}

struct t29;
#include "t842.h"
extern TypeInfo t842_TI;
void* RuntimeInvoker_t842 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t842_TI, &ret);
}

struct t29;
#include "t866.h"
extern TypeInfo t866_TI;
void* RuntimeInvoker_t866 (MethodInfo* method, void* obj, void** args){
	typedef t866  (*Func)(void* obj, MethodInfo* method);
	t866  ret = ((Func)method->method)(obj, method);
	return Box(&t866_TI, &ret);
}

struct t29;
#include "t849.h"
extern TypeInfo t849_TI;
void* RuntimeInvoker_t849 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(&t849_TI, &ret);
}

struct t29;
#include "t845.h"
extern TypeInfo t845_TI;
void* RuntimeInvoker_t845 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(&t845_TI, &ret);
}

struct t29;
#include "t977.h"
extern TypeInfo t977_TI;
void* RuntimeInvoker_t977 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t977_TI, &ret);
}

struct t29;
#include "t1003.h"
extern TypeInfo t1003_TI;
void* RuntimeInvoker_t1003 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1003_TI, &ret);
}

struct t29;
#include "t1015.h"
extern TypeInfo t1015_TI;
void* RuntimeInvoker_t1015 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(&t1015_TI, &ret);
}

struct t29;
#include "t1016.h"
extern TypeInfo t1016_TI;
void* RuntimeInvoker_t1016 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(&t1016_TI, &ret);
}

struct t29;
#include "t1018.h"
extern TypeInfo t1018_TI;
void* RuntimeInvoker_t1018 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1018_TI, &ret);
}

struct t29;
#include "t1021.h"
extern TypeInfo t1021_TI;
void* RuntimeInvoker_t1021 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1021_TI, &ret);
}

struct t29;
#include "t1022.h"
extern TypeInfo t1022_TI;
void* RuntimeInvoker_t1022 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1022_TI, &ret);
}

struct t29;
#include "t1023.h"
extern TypeInfo t1023_TI;
void* RuntimeInvoker_t1023 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1023_TI, &ret);
}

struct t29;
#include "t1026.h"
extern TypeInfo t1026_TI;
void* RuntimeInvoker_t1026 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1026_TI, &ret);
}

struct t29;
#include "t1043.h"
extern TypeInfo t1043_TI;
void* RuntimeInvoker_t1043 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1043_TI, &ret);
}

struct t29;
#include "t1037.h"
extern TypeInfo t1037_TI;
void* RuntimeInvoker_t1037 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(&t1037_TI, &ret);
}

struct t29;
#include "t1044.h"
extern TypeInfo t1044_TI;
void* RuntimeInvoker_t1044 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1044_TI, &ret);
}

struct t29;
#include "t934.h"
extern TypeInfo t934_TI;
void* RuntimeInvoker_t934 (MethodInfo* method, void* obj, void** args){
	typedef t934  (*Func)(void* obj, MethodInfo* method);
	t934  ret = ((Func)method->method)(obj, method);
	return Box(&t934_TI, &ret);
}

struct t29;
#include "t1024.h"
extern TypeInfo t1024_TI;
void* RuntimeInvoker_t1024 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(&t1024_TI, &ret);
}

struct t29;
#include "t1127.h"
extern TypeInfo t1127_TI;
void* RuntimeInvoker_t1127 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1127_TI, &ret);
}

struct t29;
#include "t1148.h"
extern TypeInfo t1148_TI;
void* RuntimeInvoker_t1148 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1148_TI, &ret);
}

struct t29;
#include "t1149.h"
extern TypeInfo t1149_TI;
void* RuntimeInvoker_t1149 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1149_TI, &ret);
}

struct t29;
#include "t43.h"
extern TypeInfo t43_TI;
void* RuntimeInvoker_t43 (MethodInfo* method, void* obj, void** args){
	typedef t43  (*Func)(void* obj, MethodInfo* method);
	t43  ret = ((Func)method->method)(obj, method);
	return Box(&t43_TI, &ret);
}

struct t29;
#include "t1197.h"
extern TypeInfo t1197_TI;
void* RuntimeInvoker_t1197 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1197_TI, &ret);
}

struct t29;
#include "t816.h"
extern TypeInfo t816_TI;
void* RuntimeInvoker_t816 (MethodInfo* method, void* obj, void** args){
	typedef t816  (*Func)(void* obj, MethodInfo* method);
	t816  ret = ((Func)method->method)(obj, method);
	return Box(&t816_TI, &ret);
}

struct t29;
#include "t1126.h"
extern TypeInfo t1126_TI;
void* RuntimeInvoker_t1126 (MethodInfo* method, void* obj, void** args){
	typedef t1126  (*Func)(void* obj, MethodInfo* method);
	t1126  ret = ((Func)method->method)(obj, method);
	return Box(&t1126_TI, &ret);
}

struct t29;
#include "t1150.h"
extern TypeInfo t1150_TI;
void* RuntimeInvoker_t1150 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1150_TI, &ret);
}

struct t29;
#include "t1343.h"
extern TypeInfo t1343_TI;
void* RuntimeInvoker_t1343 (MethodInfo* method, void* obj, void** args){
	typedef t1343  (*Func)(void* obj, MethodInfo* method);
	t1343  ret = ((Func)method->method)(obj, method);
	return Box(&t1343_TI, &ret);
}

struct t29;
#include "t1342.h"
extern TypeInfo t1342_TI;
void* RuntimeInvoker_t1342 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1342_TI, &ret);
}

struct t29;
#include "t1347.h"
extern TypeInfo t1347_TI;
void* RuntimeInvoker_t1347 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1347_TI, &ret);
}

struct t29;
#include "t927.h"
extern TypeInfo t927_TI;
void* RuntimeInvoker_t927 (MethodInfo* method, void* obj, void** args){
	typedef t927  (*Func)(void* obj, MethodInfo* method);
	t927  ret = ((Func)method->method)(obj, method);
	return Box(&t927_TI, &ret);
}

struct t29;
#include "t1362.h"
extern TypeInfo t1362_TI;
void* RuntimeInvoker_t1362 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1362_TI, &ret);
}

struct t29;
#include "t1367.h"
extern TypeInfo t1367_TI;
void* RuntimeInvoker_t1367 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1367_TI, &ret);
}

struct t29;
#include "t1382.h"
extern TypeInfo t1382_TI;
void* RuntimeInvoker_t1382 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1382_TI, &ret);
}

struct t29;
#include "t1353.h"
extern TypeInfo t1353_TI;
void* RuntimeInvoker_t1353 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1353_TI, &ret);
}

struct t29;
#include "t735.h"
extern TypeInfo t735_TI;
void* RuntimeInvoker_t735 (MethodInfo* method, void* obj, void** args){
	typedef t735  (*Func)(void* obj, MethodInfo* method);
	t735  ret = ((Func)method->method)(obj, method);
	return Box(&t735_TI, &ret);
}

struct t29;
#include "t1497.h"
extern TypeInfo t1497_TI;
void* RuntimeInvoker_t1497 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1497_TI, &ret);
}

struct t29;
#include "t1520.h"
extern TypeInfo t1520_TI;
void* RuntimeInvoker_t1520 (MethodInfo* method, void* obj, void** args){
	typedef t1520  (*Func)(void* obj, MethodInfo* method);
	t1520  ret = ((Func)method->method)(obj, method);
	return Box(&t1520_TI, &ret);
}

struct t29;
#include "t1523.h"
extern TypeInfo t1523_TI;
void* RuntimeInvoker_t1523 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1523_TI, &ret);
}

struct t29;
#include "t1099.h"
extern TypeInfo t1099_TI;
void* RuntimeInvoker_t1099 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1099_TI, &ret);
}

struct t29;
#include "t1117.h"
extern TypeInfo t1117_TI;
void* RuntimeInvoker_t1117 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1117_TI, &ret);
}

struct t29;
#include "t1292.h"
extern TypeInfo t1292_TI;
void* RuntimeInvoker_t1292 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1292_TI, &ret);
}

struct t29;
#include "t1628.h"
extern TypeInfo t1628_TI;
void* RuntimeInvoker_t1628 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1628_TI, &ret);
}

struct t29;
#include "t1644.h"
extern TypeInfo t1644_TI;
void* RuntimeInvoker_t1644 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1644_TI, &ret);
}

struct t29;
#include "t1648.h"
extern TypeInfo t1648_TI;
void* RuntimeInvoker_t1648 (MethodInfo* method, void* obj, void** args){
	typedef t1648  (*Func)(void* obj, MethodInfo* method);
	t1648  ret = ((Func)method->method)(obj, method);
	return Box(&t1648_TI, &ret);
}

struct t29;
#include "t48.h"
extern TypeInfo t48_TI;
void* RuntimeInvoker_t48 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t48_TI, &ret);
}

struct t29;
#include "t61.h"
extern TypeInfo t61_TI;
void* RuntimeInvoker_t61 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t61_TI, &ret);
}

struct t29;
#include "t112.h"
extern TypeInfo t112_TI;
void* RuntimeInvoker_t112 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t112_TI, &ret);
}

struct t29;
#include "t1248.h"
extern TypeInfo t1248_TI;
void* RuntimeInvoker_t1248 (MethodInfo* method, void* obj, void** args){
	typedef t1248  (*Func)(void* obj, MethodInfo* method);
	t1248  ret = ((Func)method->method)(obj, method);
	return Box(&t1248_TI, &ret);
}

struct t29;
#include "t330.h"
extern TypeInfo t330_TI;
void* RuntimeInvoker_t330 (MethodInfo* method, void* obj, void** args){
	typedef t330  (*Func)(void* obj, MethodInfo* method);
	t330  ret = ((Func)method->method)(obj, method);
	return Box(&t330_TI, &ret);
}

struct t29;
#include "t127.h"
extern TypeInfo t127_TI;
void* RuntimeInvoker_t127 (MethodInfo* method, void* obj, void** args){
	typedef t127  (*Func)(void* obj, MethodInfo* method);
	t127  ret = ((Func)method->method)(obj, method);
	return Box(&t127_TI, &ret);
}

struct t29;
#include "t140.h"
extern TypeInfo t140_TI;
void* RuntimeInvoker_t140 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t140_TI, &ret);
}

struct t29;
#include "t184.h"
extern TypeInfo t184_TI;
void* RuntimeInvoker_t184 (MethodInfo* method, void* obj, void** args){
	typedef t184  (*Func)(void* obj, MethodInfo* method);
	t184  ret = ((Func)method->method)(obj, method);
	return Box(&t184_TI, &ret);
}

struct t29;
#include "t174.h"
extern TypeInfo t174_TI;
void* RuntimeInvoker_t174 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t174_TI, &ret);
}

struct t29;
#include "t175.h"
extern TypeInfo t175_TI;
void* RuntimeInvoker_t175 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t175_TI, &ret);
}

struct t29;
#include "t176.h"
extern TypeInfo t176_TI;
void* RuntimeInvoker_t176 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t176_TI, &ret);
}

struct t29;
#include "t177.h"
extern TypeInfo t177_TI;
void* RuntimeInvoker_t177 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t177_TI, &ret);
}

struct t29;
#include "t178.h"
extern TypeInfo t178_TI;
void* RuntimeInvoker_t178 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t178_TI, &ret);
}

struct t29;
#include "t380.h"
extern TypeInfo t380_TI;
void* RuntimeInvoker_t380 (MethodInfo* method, void* obj, void** args){
	typedef t380  (*Func)(void* obj, MethodInfo* method);
	t380  ret = ((Func)method->method)(obj, method);
	return Box(&t380_TI, &ret);
}

struct t29;
#include "t381.h"
extern TypeInfo t381_TI;
void* RuntimeInvoker_t381 (MethodInfo* method, void* obj, void** args){
	typedef t381  (*Func)(void* obj, MethodInfo* method);
	t381  ret = ((Func)method->method)(obj, method);
	return Box(&t381_TI, &ret);
}

struct t29;
#include "t192.h"
extern TypeInfo t192_TI;
void* RuntimeInvoker_t192 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t192_TI, &ret);
}

struct t29;
#include "t239.h"
extern TypeInfo t239_TI;
void* RuntimeInvoker_t239 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t239_TI, &ret);
}

struct t29;
#include "t447.h"
extern TypeInfo t447_TI;
void* RuntimeInvoker_t447 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t447_TI, &ret);
}

struct t29;
#include "t477.h"
extern TypeInfo t477_TI;
void* RuntimeInvoker_t477 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t477_TI, &ret);
}

struct t29;
#include "t393.h"
extern TypeInfo t393_TI;
void* RuntimeInvoker_t393 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t393_TI, &ret);
}

struct t29;
#include "t413.h"
extern TypeInfo t413_TI;
void* RuntimeInvoker_t413 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t413_TI, &ret);
}

struct t29;
#include "t408.h"
extern TypeInfo t408_TI;
void* RuntimeInvoker_t408 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t408_TI, &ret);
}

struct t29;
#include "t386.h"
extern TypeInfo t386_TI;
void* RuntimeInvoker_t386 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t386_TI, &ret);
}

struct t29;
#include "t385.h"
extern TypeInfo t385_TI;
void* RuntimeInvoker_t385 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t385_TI, &ret);
}

struct t29;
#include "t512.h"
extern TypeInfo t512_TI;
void* RuntimeInvoker_t512 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t512_TI, &ret);
}

struct t29;
#include "t517.h"
extern TypeInfo t517_TI;
void* RuntimeInvoker_t517 (MethodInfo* method, void* obj, void** args){
	typedef t517  (*Func)(void* obj, MethodInfo* method);
	t517  ret = ((Func)method->method)(obj, method);
	return Box(&t517_TI, &ret);
}

struct t29;
#include "t632.h"
extern TypeInfo t632_TI;
void* RuntimeInvoker_t632 (MethodInfo* method, void* obj, void** args){
	typedef t632  (*Func)(void* obj, MethodInfo* method);
	t632  ret = ((Func)method->method)(obj, method);
	return Box(&t632_TI, &ret);
}

struct t29;
#include "t542.h"
extern TypeInfo t542_TI;
void* RuntimeInvoker_t542 (MethodInfo* method, void* obj, void** args){
	typedef t542  (*Func)(void* obj, MethodInfo* method);
	t542  ret = ((Func)method->method)(obj, method);
	return Box(&t542_TI, &ret);
}

struct t29;
#include "t552.h"
extern TypeInfo t552_TI;
void* RuntimeInvoker_t552 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t552_TI, &ret);
}

struct t29;
#include "t551.h"
extern TypeInfo t551_TI;
void* RuntimeInvoker_t551 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(&t551_TI, &ret);
}

struct t29;
#include "t564.h"
extern TypeInfo t564_TI;
void* RuntimeInvoker_t564 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t564_TI, &ret);
}

struct t29;
#include "t578.h"
extern TypeInfo t578_TI;
void* RuntimeInvoker_t578 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t578_TI, &ret);
}

struct t29;
#include "t740.h"
extern TypeInfo t740_TI;
void* RuntimeInvoker_t740 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t740_TI, &ret);
}

struct t29;
#include "t741.h"
extern TypeInfo t741_TI;
void* RuntimeInvoker_t741 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t741_TI, &ret);
}

struct t29;
#include "t751.h"
extern TypeInfo t751_TI;
void* RuntimeInvoker_t751 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t751_TI, &ret);
}

struct t29;
#include "t775.h"
extern TypeInfo t775_TI;
void* RuntimeInvoker_t775 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t775_TI, &ret);
}

struct t29;
#include "t784.h"
extern TypeInfo t784_TI;
void* RuntimeInvoker_t784 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t784_TI, &ret);
}

struct t29;
#include "t785.h"
extern TypeInfo t785_TI;
void* RuntimeInvoker_t785 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t785_TI, &ret);
}

struct t29;
#include "t787.h"
extern TypeInfo t787_TI;
void* RuntimeInvoker_t787 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t787_TI, &ret);
}

struct t29;
#include "t805.h"
extern TypeInfo t805_TI;
void* RuntimeInvoker_t805 (MethodInfo* method, void* obj, void** args){
	typedef t805  (*Func)(void* obj, MethodInfo* method);
	t805  ret = ((Func)method->method)(obj, method);
	return Box(&t805_TI, &ret);
}

struct t29;
#include "t798.h"
extern TypeInfo t798_TI;
void* RuntimeInvoker_t798 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t798_TI, &ret);
}

struct t29;
#include "t794.h"
extern TypeInfo t794_TI;
void* RuntimeInvoker_t794 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t794_TI, &ret);
}

struct t29;
#include "t825.h"
extern TypeInfo t825_TI;
void* RuntimeInvoker_t825 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t825_TI, &ret);
}

struct t29;
#include "t790.h"
extern TypeInfo t790_TI;
void* RuntimeInvoker_t790 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t790_TI, &ret);
}

struct t29;
#include "t843.h"
extern TypeInfo t843_TI;
void* RuntimeInvoker_t843 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(&t843_TI, &ret);
}

struct t29;
#include "t844.h"
extern TypeInfo t844_TI;
void* RuntimeInvoker_t844 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(&t844_TI, &ret);
}

struct t29;
#include "t859.h"
extern TypeInfo t859_TI;
void* RuntimeInvoker_t859 (MethodInfo* method, void* obj, void** args){
	typedef t859  (*Func)(void* obj, MethodInfo* method);
	t859  ret = ((Func)method->method)(obj, method);
	return Box(&t859_TI, &ret);
}

struct t29;
#include "t862.h"
extern TypeInfo t862_TI;
void* RuntimeInvoker_t862 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t862_TI, &ret);
}

struct t29;
#include "t895.h"
extern TypeInfo t895_TI;
void* RuntimeInvoker_t895 (MethodInfo* method, void* obj, void** args){
	typedef t895  (*Func)(void* obj, MethodInfo* method);
	t895  ret = ((Func)method->method)(obj, method);
	return Box(&t895_TI, &ret);
}

struct t29;
#include "t897.h"
extern TypeInfo t897_TI;
void* RuntimeInvoker_t897 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t897_TI, &ret);
}

struct t29;
#include "t899.h"
extern TypeInfo t899_TI;
void* RuntimeInvoker_t899 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t899_TI, &ret);
}

struct t29;
#include "t898.h"
extern TypeInfo t898_TI;
void* RuntimeInvoker_t898 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t898_TI, &ret);
}

struct t29;
#include "t970.h"
extern TypeInfo t970_TI;
void* RuntimeInvoker_t970 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t970_TI, &ret);
}

struct t29;
#include "t1007.h"
extern TypeInfo t1007_TI;
void* RuntimeInvoker_t1007 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1007_TI, &ret);
}

struct t29;
#include "t1009.h"
extern TypeInfo t1009_TI;
void* RuntimeInvoker_t1009 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1009_TI, &ret);
}

struct t29;
#include "t1067.h"
extern TypeInfo t1067_TI;
void* RuntimeInvoker_t1067 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1067_TI, &ret);
}

struct t29;
#include "t1173.h"
extern TypeInfo t1173_TI;
void* RuntimeInvoker_t1173 (MethodInfo* method, void* obj, void** args){
	typedef t1173  (*Func)(void* obj, MethodInfo* method);
	t1173  ret = ((Func)method->method)(obj, method);
	return Box(&t1173_TI, &ret);
}

struct t29;
#include "t1189.h"
extern TypeInfo t1189_TI;
void* RuntimeInvoker_t1189 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1189_TI, &ret);
}

struct t29;
#include "t1200.h"
extern TypeInfo t1200_TI;
void* RuntimeInvoker_t1200 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1200_TI, &ret);
}

struct t29;
#include "t1272.h"
extern TypeInfo t1272_TI;
void* RuntimeInvoker_t1272 (MethodInfo* method, void* obj, void** args){
	typedef t1272  (*Func)(void* obj, MethodInfo* method);
	t1272  ret = ((Func)method->method)(obj, method);
	return Box(&t1272_TI, &ret);
}

struct t29;
#include "t1274.h"
extern TypeInfo t1274_TI;
void* RuntimeInvoker_t1274 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1274_TI, &ret);
}

struct t29;
#include "t1279.h"
extern TypeInfo t1279_TI;
void* RuntimeInvoker_t1279 (MethodInfo* method, void* obj, void** args){
	typedef t1279  (*Func)(void* obj, MethodInfo* method);
	t1279  ret = ((Func)method->method)(obj, method);
	return Box(&t1279_TI, &ret);
}

struct t29;
#include "t1280.h"
extern TypeInfo t1280_TI;
void* RuntimeInvoker_t1280 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1280_TI, &ret);
}

struct t29;
#include "t1284.h"
extern TypeInfo t1284_TI;
void* RuntimeInvoker_t1284 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1284_TI, &ret);
}

struct t29;
#include "t1285.h"
extern TypeInfo t1285_TI;
void* RuntimeInvoker_t1285 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1285_TI, &ret);
}

struct t29;
#include "t1286.h"
extern TypeInfo t1286_TI;
void* RuntimeInvoker_t1286 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1286_TI, &ret);
}

struct t29;
#include "t1120.h"
extern TypeInfo t1120_TI;
void* RuntimeInvoker_t1120 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1120_TI, &ret);
}

struct t29;
#include "t1298.h"
extern TypeInfo t1298_TI;
void* RuntimeInvoker_t1298 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1298_TI, &ret);
}

struct t29;
#include "t1093.h"
extern TypeInfo t1093_TI;
void* RuntimeInvoker_t1093 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1093_TI, &ret);
}

struct t29;
#include "t1301.h"
extern TypeInfo t1301_TI;
void* RuntimeInvoker_t1301 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1301_TI, &ret);
}

struct t29;
#include "t923.h"
extern TypeInfo t923_TI;
void* RuntimeInvoker_t923 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t923_TI, &ret);
}

struct t29;
#include "t851.h"
extern TypeInfo t851_TI;
void* RuntimeInvoker_t851 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t851_TI, &ret);
}

struct t29;
#include "t1306.h"
extern TypeInfo t1306_TI;
void* RuntimeInvoker_t1306 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1306_TI, &ret);
}

struct t29;
#include "t1311.h"
extern TypeInfo t1311_TI;
void* RuntimeInvoker_t1311 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1311_TI, &ret);
}

struct t29;
#include "t1313.h"
extern TypeInfo t1313_TI;
void* RuntimeInvoker_t1313 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1313_TI, &ret);
}

struct t29;
#include "t1314.h"
extern TypeInfo t1314_TI;
void* RuntimeInvoker_t1314 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1314_TI, &ret);
}

struct t29;
#include "t1319.h"
extern TypeInfo t1319_TI;
void* RuntimeInvoker_t1319 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1319_TI, &ret);
}

struct t29;
#include "t1321.h"
extern TypeInfo t1321_TI;
void* RuntimeInvoker_t1321 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1321_TI, &ret);
}

struct t29;
#include "t1064.h"
extern TypeInfo t1064_TI;
void* RuntimeInvoker_t1064 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1064_TI, &ret);
}

struct t29;
#include "t630.h"
extern TypeInfo t630_TI;
void* RuntimeInvoker_t630 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t630_TI, &ret);
}

struct t29;
#include "t1370.h"
extern TypeInfo t1370_TI;
void* RuntimeInvoker_t1370 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1370_TI, &ret);
}

struct t29;
#include "t1384.h"
extern TypeInfo t1384_TI;
void* RuntimeInvoker_t1384 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1384_TI, &ret);
}

struct t29;
#include "t1363.h"
extern TypeInfo t1363_TI;
void* RuntimeInvoker_t1363 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1363_TI, &ret);
}

struct t29;
#include "t1392.h"
extern TypeInfo t1392_TI;
void* RuntimeInvoker_t1392 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1392_TI, &ret);
}

struct t29;
#include "t1394.h"
extern TypeInfo t1394_TI;
void* RuntimeInvoker_t1394 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1394_TI, &ret);
}

struct t29;
#include "t1397.h"
extern TypeInfo t1397_TI;
void* RuntimeInvoker_t1397 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1397_TI, &ret);
}

struct t29;
#include "t1398.h"
extern TypeInfo t1398_TI;
void* RuntimeInvoker_t1398 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1398_TI, &ret);
}

struct t29;
#include "t1153.h"
extern TypeInfo t1153_TI;
void* RuntimeInvoker_t1153 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1153_TI, &ret);
}

struct t29;
#include "t1154.h"
extern TypeInfo t1154_TI;
void* RuntimeInvoker_t1154 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1154_TI, &ret);
}

struct t29;
#include "t1403.h"
extern TypeInfo t1403_TI;
void* RuntimeInvoker_t1403 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1403_TI, &ret);
}

struct t29;
#include "t1405.h"
extern TypeInfo t1405_TI;
void* RuntimeInvoker_t1405 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1405_TI, &ret);
}

struct t29;
#include "t1408.h"
extern TypeInfo t1408_TI;
void* RuntimeInvoker_t1408 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1408_TI, &ret);
}

struct t29;
#include "t1156.h"
extern TypeInfo t1156_TI;
void* RuntimeInvoker_t1156 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1156_TI, &ret);
}

struct t29;
#include "t1437.h"
extern TypeInfo t1437_TI;
void* RuntimeInvoker_t1437 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(&t1437_TI, &ret);
}

struct t29;
#include "t1484.h"
extern TypeInfo t1484_TI;
void* RuntimeInvoker_t1484 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1484_TI, &ret);
}

struct t29;
#include "t1490.h"
extern TypeInfo t1490_TI;
void* RuntimeInvoker_t1490 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(&t1490_TI, &ret);
}

struct t29;
#include "t1491.h"
extern TypeInfo t1491_TI;
void* RuntimeInvoker_t1491 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(&t1491_TI, &ret);
}

struct t29;
#include "t1492.h"
extern TypeInfo t1492_TI;
void* RuntimeInvoker_t1492 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1492_TI, &ret);
}

struct t29;
#include "t1493.h"
extern TypeInfo t1493_TI;
void* RuntimeInvoker_t1493 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(&t1493_TI, &ret);
}

struct t29;
#include "t1495.h"
extern TypeInfo t1495_TI;
void* RuntimeInvoker_t1495 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1495_TI, &ret);
}

struct t29;
#include "t1496.h"
extern TypeInfo t1496_TI;
void* RuntimeInvoker_t1496 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1496_TI, &ret);
}

struct t29;
#include "t1513.h"
extern TypeInfo t1513_TI;
void* RuntimeInvoker_t1513 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(&t1513_TI, &ret);
}

struct t29;
#include "t795.h"
extern TypeInfo t795_TI;
void* RuntimeInvoker_t795 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t795_TI, &ret);
}

struct t29;
#include "t1568.h"
extern TypeInfo t1568_TI;
void* RuntimeInvoker_t1568 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1568_TI, &ret);
}

struct t29;
#include "t1600.h"
extern TypeInfo t1600_TI;
void* RuntimeInvoker_t1600 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1600_TI, &ret);
}

struct t29;
#include "t1605.h"
extern TypeInfo t1605_TI;
void* RuntimeInvoker_t1605 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1605_TI, &ret);
}

struct t29;
#include "t1129.h"
extern TypeInfo t1129_TI;
void* RuntimeInvoker_t1129 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1129_TI, &ret);
}

struct t29;
#include "t1627.h"
extern TypeInfo t1627_TI;
void* RuntimeInvoker_t1627 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1627_TI, &ret);
}

struct t29;
#include "t1112.h"
extern TypeInfo t1112_TI;
void* RuntimeInvoker_t1112 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1112_TI, &ret);
}

struct t29;
#include "t1620.h"
extern TypeInfo t1620_TI;
void* RuntimeInvoker_t1620 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t1620_TI, &ret);
}

struct t29;
#include "t947.h"
extern TypeInfo t947_TI;
void* RuntimeInvoker_t947 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t947_TI, &ret);
}

struct t29;
#include "t939.h"
extern TypeInfo t939_TI;
void* RuntimeInvoker_t939 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(&t939_TI, &ret);
}

struct t29;
#include "t1677.h"
extern TypeInfo t1677_TI;
void* RuntimeInvoker_t1677 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(&t1677_TI, &ret);
}

struct t29;
#include "t2179.h"
extern TypeInfo t2179_TI;
void* RuntimeInvoker_t2179 (MethodInfo* method, void* obj, void** args){
	typedef t2179  (*Func)(void* obj, MethodInfo* method);
	t2179  ret = ((Func)method->method)(obj, method);
	return Box(&t2179_TI, &ret);
}

struct t29;
#include "t2185.h"
extern TypeInfo t2185_TI;
void* RuntimeInvoker_t2185 (MethodInfo* method, void* obj, void** args){
	typedef t2185  (*Func)(void* obj, MethodInfo* method);
	t2185  ret = ((Func)method->method)(obj, method);
	return Box(&t2185_TI, &ret);
}

struct t29;
#include "t2221.h"
extern TypeInfo t2221_TI;
void* RuntimeInvoker_t2221 (MethodInfo* method, void* obj, void** args){
	typedef t2221  (*Func)(void* obj, MethodInfo* method);
	t2221  ret = ((Func)method->method)(obj, method);
	return Box(&t2221_TI, &ret);
}

struct t29;
#include "t2230.h"
extern TypeInfo t2230_TI;
void* RuntimeInvoker_t2230 (MethodInfo* method, void* obj, void** args){
	typedef t2230  (*Func)(void* obj, MethodInfo* method);
	t2230  ret = ((Func)method->method)(obj, method);
	return Box(&t2230_TI, &ret);
}

struct t29;
#include "t2233.h"
extern TypeInfo t2233_TI;
void* RuntimeInvoker_t2233 (MethodInfo* method, void* obj, void** args){
	typedef t2233  (*Func)(void* obj, MethodInfo* method);
	t2233  ret = ((Func)method->method)(obj, method);
	return Box(&t2233_TI, &ret);
}

struct t29;
#include "t2244.h"
extern TypeInfo t2244_TI;
void* RuntimeInvoker_t2244 (MethodInfo* method, void* obj, void** args){
	typedef t2244  (*Func)(void* obj, MethodInfo* method);
	t2244  ret = ((Func)method->method)(obj, method);
	return Box(&t2244_TI, &ret);
}

struct t29;
#include "t2257.h"
extern TypeInfo t2257_TI;
void* RuntimeInvoker_t2257 (MethodInfo* method, void* obj, void** args){
	typedef t2257  (*Func)(void* obj, MethodInfo* method);
	t2257  ret = ((Func)method->method)(obj, method);
	return Box(&t2257_TI, &ret);
}

struct t29;
#include "t2272.h"
extern TypeInfo t2272_TI;
void* RuntimeInvoker_t2272 (MethodInfo* method, void* obj, void** args){
	typedef t2272  (*Func)(void* obj, MethodInfo* method);
	t2272  ret = ((Func)method->method)(obj, method);
	return Box(&t2272_TI, &ret);
}

struct t29;
#include "t2306.h"
extern TypeInfo t2306_TI;
void* RuntimeInvoker_t2306 (MethodInfo* method, void* obj, void** args){
	typedef t2306  (*Func)(void* obj, MethodInfo* method);
	t2306  ret = ((Func)method->method)(obj, method);
	return Box(&t2306_TI, &ret);
}

struct t29;
#include "t2323.h"
extern TypeInfo t2323_TI;
void* RuntimeInvoker_t2323 (MethodInfo* method, void* obj, void** args){
	typedef t2323  (*Func)(void* obj, MethodInfo* method);
	t2323  ret = ((Func)method->method)(obj, method);
	return Box(&t2323_TI, &ret);
}

struct t29;
#include "t323.h"
extern TypeInfo t323_TI;
void* RuntimeInvoker_t323 (MethodInfo* method, void* obj, void** args){
	typedef t323  (*Func)(void* obj, MethodInfo* method);
	t323  ret = ((Func)method->method)(obj, method);
	return Box(&t323_TI, &ret);
}

struct t29;
#include "t322.h"
extern TypeInfo t322_TI;
void* RuntimeInvoker_t322 (MethodInfo* method, void* obj, void** args){
	typedef t322  (*Func)(void* obj, MethodInfo* method);
	t322  ret = ((Func)method->method)(obj, method);
	return Box(&t322_TI, &ret);
}

struct t29;
#include "t320.h"
extern TypeInfo t320_TI;
void* RuntimeInvoker_t320 (MethodInfo* method, void* obj, void** args){
	typedef t320  (*Func)(void* obj, MethodInfo* method);
	t320  ret = ((Func)method->method)(obj, method);
	return Box(&t320_TI, &ret);
}

struct t29;
#include "t2373.h"
extern TypeInfo t2373_TI;
void* RuntimeInvoker_t2373 (MethodInfo* method, void* obj, void** args){
	typedef t2373  (*Func)(void* obj, MethodInfo* method);
	t2373  ret = ((Func)method->method)(obj, method);
	return Box(&t2373_TI, &ret);
}

struct t29;
#include "t2422.h"
extern TypeInfo t2422_TI;
void* RuntimeInvoker_t2422 (MethodInfo* method, void* obj, void** args){
	typedef t2422  (*Func)(void* obj, MethodInfo* method);
	t2422  ret = ((Func)method->method)(obj, method);
	return Box(&t2422_TI, &ret);
}

struct t29;
#include "t2420.h"
extern TypeInfo t2420_TI;
void* RuntimeInvoker_t2420 (MethodInfo* method, void* obj, void** args){
	typedef t2420  (*Func)(void* obj, MethodInfo* method);
	t2420  ret = ((Func)method->method)(obj, method);
	return Box(&t2420_TI, &ret);
}

struct t29;
#include "t2425.h"
extern TypeInfo t2425_TI;
void* RuntimeInvoker_t2425 (MethodInfo* method, void* obj, void** args){
	typedef t2425  (*Func)(void* obj, MethodInfo* method);
	t2425  ret = ((Func)method->method)(obj, method);
	return Box(&t2425_TI, &ret);
}

struct t29;
#include "t2433.h"
extern TypeInfo t2433_TI;
void* RuntimeInvoker_t2433 (MethodInfo* method, void* obj, void** args){
	typedef t2433  (*Func)(void* obj, MethodInfo* method);
	t2433  ret = ((Func)method->method)(obj, method);
	return Box(&t2433_TI, &ret);
}

struct t29;
#include "t2446.h"
extern TypeInfo t2446_TI;
void* RuntimeInvoker_t2446 (MethodInfo* method, void* obj, void** args){
	typedef t2446  (*Func)(void* obj, MethodInfo* method);
	t2446  ret = ((Func)method->method)(obj, method);
	return Box(&t2446_TI, &ret);
}

struct t29;
#include "t2444.h"
extern TypeInfo t2444_TI;
void* RuntimeInvoker_t2444 (MethodInfo* method, void* obj, void** args){
	typedef t2444  (*Func)(void* obj, MethodInfo* method);
	t2444  ret = ((Func)method->method)(obj, method);
	return Box(&t2444_TI, &ret);
}

struct t29;
#include "t2448.h"
extern TypeInfo t2448_TI;
void* RuntimeInvoker_t2448 (MethodInfo* method, void* obj, void** args){
	typedef t2448  (*Func)(void* obj, MethodInfo* method);
	t2448  ret = ((Func)method->method)(obj, method);
	return Box(&t2448_TI, &ret);
}

struct t29;
#include "t2460.h"
extern TypeInfo t2460_TI;
void* RuntimeInvoker_t2460 (MethodInfo* method, void* obj, void** args){
	typedef t2460  (*Func)(void* obj, MethodInfo* method);
	t2460  ret = ((Func)method->method)(obj, method);
	return Box(&t2460_TI, &ret);
}

struct t29;
#include "t2467.h"
extern TypeInfo t2467_TI;
void* RuntimeInvoker_t2467 (MethodInfo* method, void* obj, void** args){
	typedef t2467  (*Func)(void* obj, MethodInfo* method);
	t2467  ret = ((Func)method->method)(obj, method);
	return Box(&t2467_TI, &ret);
}

struct t29;
#include "t2465.h"
extern TypeInfo t2465_TI;
void* RuntimeInvoker_t2465 (MethodInfo* method, void* obj, void** args){
	typedef t2465  (*Func)(void* obj, MethodInfo* method);
	t2465  ret = ((Func)method->method)(obj, method);
	return Box(&t2465_TI, &ret);
}

struct t29;
#include "t2469.h"
extern TypeInfo t2469_TI;
void* RuntimeInvoker_t2469 (MethodInfo* method, void* obj, void** args){
	typedef t2469  (*Func)(void* obj, MethodInfo* method);
	t2469  ret = ((Func)method->method)(obj, method);
	return Box(&t2469_TI, &ret);
}

struct t29;
#include "t2458.h"
extern TypeInfo t2458_TI;
void* RuntimeInvoker_t2458 (MethodInfo* method, void* obj, void** args){
	typedef t2458  (*Func)(void* obj, MethodInfo* method);
	t2458  ret = ((Func)method->method)(obj, method);
	return Box(&t2458_TI, &ret);
}

struct t29;
#include "t2480.h"
extern TypeInfo t2480_TI;
void* RuntimeInvoker_t2480 (MethodInfo* method, void* obj, void** args){
	typedef t2480  (*Func)(void* obj, MethodInfo* method);
	t2480  ret = ((Func)method->method)(obj, method);
	return Box(&t2480_TI, &ret);
}

struct t29;
#include "t2496.h"
extern TypeInfo t2496_TI;
void* RuntimeInvoker_t2496 (MethodInfo* method, void* obj, void** args){
	typedef t2496  (*Func)(void* obj, MethodInfo* method);
	t2496  ret = ((Func)method->method)(obj, method);
	return Box(&t2496_TI, &ret);
}

struct t29;
#include "t2511.h"
extern TypeInfo t2511_TI;
void* RuntimeInvoker_t2511 (MethodInfo* method, void* obj, void** args){
	typedef t2511  (*Func)(void* obj, MethodInfo* method);
	t2511  ret = ((Func)method->method)(obj, method);
	return Box(&t2511_TI, &ret);
}

struct t29;
#include "t2517.h"
extern TypeInfo t2517_TI;
void* RuntimeInvoker_t2517 (MethodInfo* method, void* obj, void** args){
	typedef t2517  (*Func)(void* obj, MethodInfo* method);
	t2517  ret = ((Func)method->method)(obj, method);
	return Box(&t2517_TI, &ret);
}

struct t29;
#include "t2533.h"
extern TypeInfo t2533_TI;
void* RuntimeInvoker_t2533 (MethodInfo* method, void* obj, void** args){
	typedef t2533  (*Func)(void* obj, MethodInfo* method);
	t2533  ret = ((Func)method->method)(obj, method);
	return Box(&t2533_TI, &ret);
}

struct t29;
#include "t2550.h"
extern TypeInfo t2550_TI;
void* RuntimeInvoker_t2550 (MethodInfo* method, void* obj, void** args){
	typedef t2550  (*Func)(void* obj, MethodInfo* method);
	t2550  ret = ((Func)method->method)(obj, method);
	return Box(&t2550_TI, &ret);
}

struct t29;
#include "t2564.h"
extern TypeInfo t2564_TI;
void* RuntimeInvoker_t2564 (MethodInfo* method, void* obj, void** args){
	typedef t2564  (*Func)(void* obj, MethodInfo* method);
	t2564  ret = ((Func)method->method)(obj, method);
	return Box(&t2564_TI, &ret);
}

struct t29;
#include "t2562.h"
extern TypeInfo t2562_TI;
void* RuntimeInvoker_t2562 (MethodInfo* method, void* obj, void** args){
	typedef t2562  (*Func)(void* obj, MethodInfo* method);
	t2562  ret = ((Func)method->method)(obj, method);
	return Box(&t2562_TI, &ret);
}

struct t29;
#include "t2571.h"
extern TypeInfo t2571_TI;
void* RuntimeInvoker_t2571 (MethodInfo* method, void* obj, void** args){
	typedef t2571  (*Func)(void* obj, MethodInfo* method);
	t2571  ret = ((Func)method->method)(obj, method);
	return Box(&t2571_TI, &ret);
}

struct t29;
#include "t2569.h"
extern TypeInfo t2569_TI;
void* RuntimeInvoker_t2569 (MethodInfo* method, void* obj, void** args){
	typedef t2569  (*Func)(void* obj, MethodInfo* method);
	t2569  ret = ((Func)method->method)(obj, method);
	return Box(&t2569_TI, &ret);
}

struct t29;
#include "t2573.h"
extern TypeInfo t2573_TI;
void* RuntimeInvoker_t2573 (MethodInfo* method, void* obj, void** args){
	typedef t2573  (*Func)(void* obj, MethodInfo* method);
	t2573  ret = ((Func)method->method)(obj, method);
	return Box(&t2573_TI, &ret);
}

struct t29;
#include "t2580.h"
extern TypeInfo t2580_TI;
void* RuntimeInvoker_t2580 (MethodInfo* method, void* obj, void** args){
	typedef t2580  (*Func)(void* obj, MethodInfo* method);
	t2580  ret = ((Func)method->method)(obj, method);
	return Box(&t2580_TI, &ret);
}

struct t29;
#include "t2645.h"
extern TypeInfo t2645_TI;
void* RuntimeInvoker_t2645 (MethodInfo* method, void* obj, void** args){
	typedef t2645  (*Func)(void* obj, MethodInfo* method);
	t2645  ret = ((Func)method->method)(obj, method);
	return Box(&t2645_TI, &ret);
}

struct t29;
#include "t2659.h"
extern TypeInfo t2659_TI;
void* RuntimeInvoker_t2659 (MethodInfo* method, void* obj, void** args){
	typedef t2659  (*Func)(void* obj, MethodInfo* method);
	t2659  ret = ((Func)method->method)(obj, method);
	return Box(&t2659_TI, &ret);
}

struct t29;
#include "t2682.h"
extern TypeInfo t2682_TI;
void* RuntimeInvoker_t2682 (MethodInfo* method, void* obj, void** args){
	typedef t2682  (*Func)(void* obj, MethodInfo* method);
	t2682  ret = ((Func)method->method)(obj, method);
	return Box(&t2682_TI, &ret);
}

struct t29;
#include "t2709.h"
extern TypeInfo t2709_TI;
void* RuntimeInvoker_t2709 (MethodInfo* method, void* obj, void** args){
	typedef t2709  (*Func)(void* obj, MethodInfo* method);
	t2709  ret = ((Func)method->method)(obj, method);
	return Box(&t2709_TI, &ret);
}

struct t29;
#include "t2750.h"
extern TypeInfo t2750_TI;
void* RuntimeInvoker_t2750 (MethodInfo* method, void* obj, void** args){
	typedef t2750  (*Func)(void* obj, MethodInfo* method);
	t2750  ret = ((Func)method->method)(obj, method);
	return Box(&t2750_TI, &ret);
}

struct t29;
#include "t2791.h"
extern TypeInfo t2791_TI;
void* RuntimeInvoker_t2791 (MethodInfo* method, void* obj, void** args){
	typedef t2791  (*Func)(void* obj, MethodInfo* method);
	t2791  ret = ((Func)method->method)(obj, method);
	return Box(&t2791_TI, &ret);
}

struct t29;
#include "t2796.h"
extern TypeInfo t2796_TI;
void* RuntimeInvoker_t2796 (MethodInfo* method, void* obj, void** args){
	typedef t2796  (*Func)(void* obj, MethodInfo* method);
	t2796  ret = ((Func)method->method)(obj, method);
	return Box(&t2796_TI, &ret);
}

struct t29;
#include "t2861.h"
extern TypeInfo t2861_TI;
void* RuntimeInvoker_t2861 (MethodInfo* method, void* obj, void** args){
	typedef t2861  (*Func)(void* obj, MethodInfo* method);
	t2861  ret = ((Func)method->method)(obj, method);
	return Box(&t2861_TI, &ret);
}

struct t29;
#include "t2859.h"
extern TypeInfo t2859_TI;
void* RuntimeInvoker_t2859 (MethodInfo* method, void* obj, void** args){
	typedef t2859  (*Func)(void* obj, MethodInfo* method);
	t2859  ret = ((Func)method->method)(obj, method);
	return Box(&t2859_TI, &ret);
}

struct t29;
#include "t2865.h"
extern TypeInfo t2865_TI;
void* RuntimeInvoker_t2865 (MethodInfo* method, void* obj, void** args){
	typedef t2865  (*Func)(void* obj, MethodInfo* method);
	t2865  ret = ((Func)method->method)(obj, method);
	return Box(&t2865_TI, &ret);
}

struct t29;
#include "t588.h"
extern TypeInfo t588_TI;
void* RuntimeInvoker_t588 (MethodInfo* method, void* obj, void** args){
	typedef t588  (*Func)(void* obj, MethodInfo* method);
	t588  ret = ((Func)method->method)(obj, method);
	return Box(&t588_TI, &ret);
}

struct t29;
#include "t2895.h"
extern TypeInfo t2895_TI;
void* RuntimeInvoker_t2895 (MethodInfo* method, void* obj, void** args){
	typedef t2895  (*Func)(void* obj, MethodInfo* method);
	t2895  ret = ((Func)method->method)(obj, method);
	return Box(&t2895_TI, &ret);
}

struct t29;
#include "t2893.h"
extern TypeInfo t2893_TI;
void* RuntimeInvoker_t2893 (MethodInfo* method, void* obj, void** args){
	typedef t2893  (*Func)(void* obj, MethodInfo* method);
	t2893  ret = ((Func)method->method)(obj, method);
	return Box(&t2893_TI, &ret);
}

struct t29;
#include "t593.h"
extern TypeInfo t593_TI;
void* RuntimeInvoker_t593 (MethodInfo* method, void* obj, void** args){
	typedef t593  (*Func)(void* obj, MethodInfo* method);
	t593  ret = ((Func)method->method)(obj, method);
	return Box(&t593_TI, &ret);
}

struct t29;
#include "t2913.h"
extern TypeInfo t2913_TI;
void* RuntimeInvoker_t2913 (MethodInfo* method, void* obj, void** args){
	typedef t2913  (*Func)(void* obj, MethodInfo* method);
	t2913  ret = ((Func)method->method)(obj, method);
	return Box(&t2913_TI, &ret);
}

struct t29;
#include "t2911.h"
extern TypeInfo t2911_TI;
void* RuntimeInvoker_t2911 (MethodInfo* method, void* obj, void** args){
	typedef t2911  (*Func)(void* obj, MethodInfo* method);
	t2911  ret = ((Func)method->method)(obj, method);
	return Box(&t2911_TI, &ret);
}

struct t29;
#include "t2915.h"
extern TypeInfo t2915_TI;
void* RuntimeInvoker_t2915 (MethodInfo* method, void* obj, void** args){
	typedef t2915  (*Func)(void* obj, MethodInfo* method);
	t2915  ret = ((Func)method->method)(obj, method);
	return Box(&t2915_TI, &ret);
}

struct t29;
#include "t2987.h"
extern TypeInfo t2987_TI;
void* RuntimeInvoker_t2987 (MethodInfo* method, void* obj, void** args){
	typedef t2987  (*Func)(void* obj, MethodInfo* method);
	t2987  ret = ((Func)method->method)(obj, method);
	return Box(&t2987_TI, &ret);
}

struct t29;
#include "t3035.h"
extern TypeInfo t3035_TI;
void* RuntimeInvoker_t3035 (MethodInfo* method, void* obj, void** args){
	typedef t3035  (*Func)(void* obj, MethodInfo* method);
	t3035  ret = ((Func)method->method)(obj, method);
	return Box(&t3035_TI, &ret);
}

struct t29;
#include "t3046.h"
extern TypeInfo t3046_TI;
void* RuntimeInvoker_t3046 (MethodInfo* method, void* obj, void** args){
	typedef t3046  (*Func)(void* obj, MethodInfo* method);
	t3046  ret = ((Func)method->method)(obj, method);
	return Box(&t3046_TI, &ret);
}

struct t29;
#include "t3068.h"
extern TypeInfo t3068_TI;
void* RuntimeInvoker_t3068 (MethodInfo* method, void* obj, void** args){
	typedef t3068  (*Func)(void* obj, MethodInfo* method);
	t3068  ret = ((Func)method->method)(obj, method);
	return Box(&t3068_TI, &ret);
}

struct t29;
#include "t3074.h"
extern TypeInfo t3074_TI;
void* RuntimeInvoker_t3074 (MethodInfo* method, void* obj, void** args){
	typedef t3074  (*Func)(void* obj, MethodInfo* method);
	t3074  ret = ((Func)method->method)(obj, method);
	return Box(&t3074_TI, &ret);
}

struct t29;
#include "t3108.h"
extern TypeInfo t3108_TI;
void* RuntimeInvoker_t3108 (MethodInfo* method, void* obj, void** args){
	typedef t3108  (*Func)(void* obj, MethodInfo* method);
	t3108  ret = ((Func)method->method)(obj, method);
	return Box(&t3108_TI, &ret);
}

struct t29;
#include "t3106.h"
extern TypeInfo t3106_TI;
void* RuntimeInvoker_t3106 (MethodInfo* method, void* obj, void** args){
	typedef t3106  (*Func)(void* obj, MethodInfo* method);
	t3106  ret = ((Func)method->method)(obj, method);
	return Box(&t3106_TI, &ret);
}

struct t29;
#include "t3113.h"
extern TypeInfo t3113_TI;
void* RuntimeInvoker_t3113 (MethodInfo* method, void* obj, void** args){
	typedef t3113  (*Func)(void* obj, MethodInfo* method);
	t3113  ret = ((Func)method->method)(obj, method);
	return Box(&t3113_TI, &ret);
}

struct t29;
#include "t661.h"
extern TypeInfo t661_TI;
void* RuntimeInvoker_t661 (MethodInfo* method, void* obj, void** args){
	typedef t661  (*Func)(void* obj, MethodInfo* method);
	t661  ret = ((Func)method->method)(obj, method);
	return Box(&t661_TI, &ret);
}

struct t29;
#include "t3152.h"
extern TypeInfo t3152_TI;
void* RuntimeInvoker_t3152 (MethodInfo* method, void* obj, void** args){
	typedef t3152  (*Func)(void* obj, MethodInfo* method);
	t3152  ret = ((Func)method->method)(obj, method);
	return Box(&t3152_TI, &ret);
}

struct t29;
#include "t3194.h"
extern TypeInfo t3194_TI;
void* RuntimeInvoker_t3194 (MethodInfo* method, void* obj, void** args){
	typedef t3194  (*Func)(void* obj, MethodInfo* method);
	t3194  ret = ((Func)method->method)(obj, method);
	return Box(&t3194_TI, &ret);
}

struct t29;
#include "t3192.h"
extern TypeInfo t3192_TI;
void* RuntimeInvoker_t3192 (MethodInfo* method, void* obj, void** args){
	typedef t3192  (*Func)(void* obj, MethodInfo* method);
	t3192  ret = ((Func)method->method)(obj, method);
	return Box(&t3192_TI, &ret);
}

struct t29;
#include "t3197.h"
extern TypeInfo t3197_TI;
void* RuntimeInvoker_t3197 (MethodInfo* method, void* obj, void** args){
	typedef t3197  (*Func)(void* obj, MethodInfo* method);
	t3197  ret = ((Func)method->method)(obj, method);
	return Box(&t3197_TI, &ret);
}

struct t29;
#include "t3282.h"
extern TypeInfo t3282_TI;
void* RuntimeInvoker_t3282 (MethodInfo* method, void* obj, void** args){
	typedef t3282  (*Func)(void* obj, MethodInfo* method);
	t3282  ret = ((Func)method->method)(obj, method);
	return Box(&t3282_TI, &ret);
}

struct t29;
#include "t3469.h"
extern TypeInfo t3469_TI;
void* RuntimeInvoker_t3469 (MethodInfo* method, void* obj, void** args){
	typedef t3469  (*Func)(void* obj, MethodInfo* method);
	t3469  ret = ((Func)method->method)(obj, method);
	return Box(&t3469_TI, &ret);
}

struct t29;
#include "t35.h"
extern TypeInfo t35_TI;
void* RuntimeInvoker_t35 (MethodInfo* method, void* obj, void** args){
	typedef t35 (*Func)(void* obj, MethodInfo* method);
	t35 ret = ((Func)method->method)(obj, method);
	return Box(&t35_TI, &ret);
}

struct t29;
#include "t1131.h"
extern TypeInfo t1131_TI;
void* RuntimeInvoker_t1131 (MethodInfo* method, void* obj, void** args){
	typedef t1131  (*Func)(void* obj, MethodInfo* method);
	t1131  ret = ((Func)method->method)(obj, method);
	return Box(&t1131_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int8_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint8_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int8_t p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t372 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int16_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t194_TI;
void* RuntimeInvoker_t194_t297 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t194_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, uint8_t p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t372 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int16_t p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t626 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint16_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t297_TI;
void* RuntimeInvoker_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef int8_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t297_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t194_TI;
void* RuntimeInvoker_t194_t372 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t194_TI, &ret);
}

struct t29;
extern TypeInfo t348_TI;
void* RuntimeInvoker_t348_t297 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t348_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t626 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, uint16_t p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t372_TI;
void* RuntimeInvoker_t372_t297 (MethodInfo* method, void* obj, void** args){
	typedef int16_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t372_TI, &ret);
}

struct t29;
extern TypeInfo t297_TI;
void* RuntimeInvoker_t297_t372 (MethodInfo* method, void* obj, void** args){
	typedef int8_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t297_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t919 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int64_t p1, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t348_TI;
void* RuntimeInvoker_t348_t372 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t348_TI, &ret);
}

struct t29;
extern TypeInfo t194_TI;
void* RuntimeInvoker_t194_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t194_TI, &ret);
}

struct t29;
extern TypeInfo t626_TI;
void* RuntimeInvoker_t626_t297 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t626_TI, &ret);
}

struct t29;
extern TypeInfo t372_TI;
void* RuntimeInvoker_t372_t372 (MethodInfo* method, void* obj, void** args){
	typedef int16_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t372_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t919 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int64_t p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t297_TI;
void* RuntimeInvoker_t297_t44 (MethodInfo* method, void* obj, void** args){
	typedef int8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t297_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, float p1, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t626_TI;
void* RuntimeInvoker_t626_t372 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t626_TI, &ret);
}

struct t29;
extern TypeInfo t348_TI;
void* RuntimeInvoker_t348_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t348_TI, &ret);
}

struct t29;
extern TypeInfo t194_TI;
void* RuntimeInvoker_t194_t919 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t194_TI, &ret);
}

struct t29;
extern TypeInfo t344_TI;
void* RuntimeInvoker_t344_t297 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, uint8_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t22 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, float p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t372 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t601 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, double p1, MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t372_TI;
void* RuntimeInvoker_t372_t44 (MethodInfo* method, void* obj, void** args){
	typedef int16_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t372_TI, &ret);
}

struct t29;
extern TypeInfo t919_TI;
void* RuntimeInvoker_t919_t297 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t297_TI;
void* RuntimeInvoker_t297_t919 (MethodInfo* method, void* obj, void** args){
	typedef int8_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t297_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t601 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, double p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t348_TI;
void* RuntimeInvoker_t348_t919 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t348_TI, &ret);
}

struct t29;
extern TypeInfo t194_TI;
void* RuntimeInvoker_t194_t22 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, float p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t194_TI, &ret);
}

struct t29;
extern TypeInfo t626_TI;
void* RuntimeInvoker_t626_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t626_TI, &ret);
}

struct t29;
extern TypeInfo t344_TI;
void* RuntimeInvoker_t344_t372 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t1089_TI;
void* RuntimeInvoker_t1089_t297 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, int8_t p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t626 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, uint16_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t372_TI;
void* RuntimeInvoker_t372_t919 (MethodInfo* method, void* obj, void** args){
	typedef int16_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t372_TI, &ret);
}

struct t29;
extern TypeInfo t919_TI;
void* RuntimeInvoker_t919_t372 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t297_TI;
void* RuntimeInvoker_t297_t22 (MethodInfo* method, void* obj, void** args){
	typedef int8_t (*Func)(void* obj, float p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t297_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t297 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, int8_t p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t390 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t* p1, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t400 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t132 * p1, MethodInfo* method);
	((Func)method->method)(obj, (t132 *)args[0], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t594 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t17 * p1, MethodInfo* method);
	((Func)method->method)(obj, (t17 *)args[0], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t589 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t164 * p1, MethodInfo* method);
	((Func)method->method)(obj, (t164 *)args[0], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
#include "t492.h"
void* RuntimeInvoker_t21_t605 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t492 * p1, MethodInfo* method);
	((Func)method->method)(obj, (t492 *)args[0], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t587 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t23 * p1, MethodInfo* method);
	((Func)method->method)(obj, (t23 *)args[0], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t596 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t362 * p1, MethodInfo* method);
	((Func)method->method)(obj, (t362 *)args[0], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t597 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t396 * p1, MethodInfo* method);
	((Func)method->method)(obj, (t396 *)args[0], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t7;
#include "t7.h"
void* RuntimeInvoker_t21_t639 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t7** p1, MethodInfo* method);
	((Func)method->method)(obj, (t7**)args[0], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t326 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, bool* p1, MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

struct t29;
extern TypeInfo t344_TI;
void* RuntimeInvoker_t344_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t348_TI;
void* RuntimeInvoker_t348_t22 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, float p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t348_TI, &ret);
}

struct t29;
extern TypeInfo t601_TI;
void* RuntimeInvoker_t601_t297 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, int8_t p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t601_TI, &ret);
}

struct t29;
extern TypeInfo t297_TI;
void* RuntimeInvoker_t297_t601 (MethodInfo* method, void* obj, void** args){
	typedef int8_t (*Func)(void* obj, double p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t297_TI, &ret);
}

struct t29;
extern TypeInfo t626_TI;
void* RuntimeInvoker_t626_t919 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t626_TI, &ret);
}

struct t29;
extern TypeInfo t1089_TI;
void* RuntimeInvoker_t1089_t372 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t17 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t17  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t17 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t57 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t57  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t57 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t23 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t23  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t23 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t126 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t126  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t126 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t132 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t132  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t132 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t164 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t164  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t164 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t210 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t210  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t210 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t146 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t146  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t146 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t228 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t228  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t228 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t465 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t465  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t465 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t224 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t224  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t224 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t362 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t362  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t362 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t866 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t866  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t866 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t934 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t934  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t934 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t919 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t919_TI;
void* RuntimeInvoker_t919_t44 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
#include "t936.h"
void* RuntimeInvoker_t21_t936 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t936  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t936 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t735 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t735  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t735 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t348_TI;
void* RuntimeInvoker_t348_t601 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, double p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t348_TI, &ret);
}

struct t29;
extern TypeInfo t372_TI;
void* RuntimeInvoker_t372_t22 (MethodInfo* method, void* obj, void** args){
	typedef int16_t (*Func)(void* obj, float p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t372_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t372 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, int16_t p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
#include "t1642.h"
void* RuntimeInvoker_t21_t1642 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t1642  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t1642 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t1248 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t1248  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t1248 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t725 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t725  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t725 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t330 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t330  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t330 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t127 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t127  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t127 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t184 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t184  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t184 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t380 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t380  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t380 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t381 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t381  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t381 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t517 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t517  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t517 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t632 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t632  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t632 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t542 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t542  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t542 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t805 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t805  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t805 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t859 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t859  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t859 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t895 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t895  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t895 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t1173 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t1173  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t1173 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t1272 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t1272  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t1272 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t1279 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t1279  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t1279 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t1126 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t1126  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t816 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t816  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t816 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
#include "t131.h"
void* RuntimeInvoker_t21_t131 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t131  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t131 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
#include "t265.h"
void* RuntimeInvoker_t40_t265 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t265  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t265 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t23 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t23  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t23 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t224 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t224  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t224 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
#include "t329.h"
void* RuntimeInvoker_t40_t329 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t329  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t329 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t542 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t542  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t542 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
#include "t238.h"
void* RuntimeInvoker_t40_t238 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t238  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t238 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t866 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t866  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t866 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t465 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t465  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t465 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t1126 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t1126  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t601_TI;
void* RuntimeInvoker_t601_t372 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, int16_t p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t601_TI, &ret);
}

struct t29;
extern TypeInfo t372_TI;
void* RuntimeInvoker_t372_t601 (MethodInfo* method, void* obj, void** args){
	typedef int16_t (*Func)(void* obj, double p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t372_TI, &ret);
}

struct t29;
extern TypeInfo t626_TI;
void* RuntimeInvoker_t626_t22 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, float p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t626_TI, &ret);
}

struct t29;
extern TypeInfo t344_TI;
void* RuntimeInvoker_t344_t919 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t1089_TI;
void* RuntimeInvoker_t1089_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
#include "t1629.h"
void* RuntimeInvoker_t40_t1629 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t1629  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t1629 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t1648 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t1648  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t1648 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t816 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t816  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t816 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t57 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t57  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t57 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t1248 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t1248  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t1248 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t725 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t725  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t725 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t330 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t330  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t330 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t127 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t127  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t127 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t184 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t184  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t184 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t17 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t17  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t17 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t380 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t380  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t380 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t381 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t381  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t381 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t517 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t517  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t517 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t632 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t632  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t632 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t805 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t805  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t805 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t859 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t859  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t859 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t895 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t895  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t895 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t1173 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t1173  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t1173 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t1272 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t1272  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t1272 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t1279 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t1279  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t1279 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, int32_t p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t22 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, float p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t919_TI;
void* RuntimeInvoker_t919_t919 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t626_TI;
void* RuntimeInvoker_t626_t601 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, double p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t626_TI, &ret);
}

struct t29;
extern TypeInfo t183_TI;
void* RuntimeInvoker_t183_t297 (MethodInfo* method, void* obj, void** args){
	typedef t183  (*Func)(void* obj, int8_t p1, MethodInfo* method);
	t183  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t183_TI, &ret);
}

struct t29;
extern TypeInfo t934_TI;
void* RuntimeInvoker_t934_t297 (MethodInfo* method, void* obj, void** args){
	typedef t934  (*Func)(void* obj, int8_t p1, MethodInfo* method);
	t934  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t934_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t601 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, double p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t297_TI;
void* RuntimeInvoker_t297_t1126 (MethodInfo* method, void* obj, void** args){
	typedef int8_t (*Func)(void* obj, t1126  p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return Box(&t297_TI, &ret);
}

struct t29;
extern TypeInfo t1126_TI;
void* RuntimeInvoker_t1126_t297 (MethodInfo* method, void* obj, void** args){
	typedef t1126  (*Func)(void* obj, int8_t p1, MethodInfo* method);
	t1126  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t1126_TI, &ret);
}

struct t29;
extern TypeInfo t936_TI;
void* RuntimeInvoker_t936_t297 (MethodInfo* method, void* obj, void** args){
	typedef t936  (*Func)(void* obj, int8_t p1, MethodInfo* method);
	t936  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t936_TI, &ret);
}

struct t29;
extern TypeInfo t1089_TI;
void* RuntimeInvoker_t1089_t919 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, int64_t p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t465_TI;
void* RuntimeInvoker_t465_t297 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, int8_t p1, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t601_TI;
void* RuntimeInvoker_t601_t44 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, int32_t p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t601_TI, &ret);
}

struct t29;
extern TypeInfo t344_TI;
void* RuntimeInvoker_t344_t22 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, float p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t348_TI;
void* RuntimeInvoker_t348_t1126 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, t1126  p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return Box(&t348_TI, &ret);
}

struct t29;
extern TypeInfo t919_TI;
void* RuntimeInvoker_t919_t22 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, float p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t919 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, int64_t p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t344_TI;
void* RuntimeInvoker_t344_t601 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, double p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t322 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t322  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t322 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t2420 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2420  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t2420 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t2444 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2444  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t2444 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t2465 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2465  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t2465 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t2458 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2458  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t2458 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t2569 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2569  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t2569 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t2562 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2562  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t2562 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t2859 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2859  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t2859 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t2893 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2893  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t2893 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t2911 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2911  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t2911 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t3106 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t3106  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t3106 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t3192 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t3192  p1, MethodInfo* method);
	((Func)method->method)(obj, *((t3192 *)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t1026_TI;
void* RuntimeInvoker_t1026_t372 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t1026_TI, &ret);
}

struct t29;
extern TypeInfo t851_TI;
void* RuntimeInvoker_t851_t372 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int16_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t851_TI, &ret);
}

struct t29;
extern TypeInfo t372_TI;
void* RuntimeInvoker_t372_t1126 (MethodInfo* method, void* obj, void** args){
	typedef int16_t (*Func)(void* obj, t1126  p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return Box(&t372_TI, &ret);
}

struct t29;
extern TypeInfo t1126_TI;
void* RuntimeInvoker_t1126_t372 (MethodInfo* method, void* obj, void** args){
	typedef t1126  (*Func)(void* obj, int16_t p1, MethodInfo* method);
	t1126  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t1126_TI, &ret);
}

struct t29;
extern TypeInfo t919_TI;
void* RuntimeInvoker_t919_t601 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, double p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t465_TI;
void* RuntimeInvoker_t465_t372 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, int16_t p1, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t601_TI;
void* RuntimeInvoker_t601_t919 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, int64_t p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t601_TI, &ret);
}

struct t29;
extern TypeInfo t1089_TI;
void* RuntimeInvoker_t1089_t22 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, float p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t322 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t322  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t322 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t2420 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t2420  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t2420 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t2444 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t2444  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t2444 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t2465 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t2465  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t2465 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t2458 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t2458  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t2458 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t2569 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t2569  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t2569 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t2562 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t2562  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t2562 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t2859 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t2859  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t2859 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t2893 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t2893  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t2893 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t2911 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t2911  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t2911 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t3106 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t3106  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t3106 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t3192 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t3192  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t3192 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
#include "t1631.h"
void* RuntimeInvoker_t40_t1631 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t1631  p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t1631 *)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, float p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t626_TI;
void* RuntimeInvoker_t626_t1126 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, t1126  p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return Box(&t626_TI, &ret);
}

struct t29;
extern TypeInfo t1089_TI;
void* RuntimeInvoker_t1089_t601 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, double p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t112_TI;
void* RuntimeInvoker_t112_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t112_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t17 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t17  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t17 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t17_TI;
void* RuntimeInvoker_t17_t44 (MethodInfo* method, void* obj, void** args){
	typedef t17  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t17  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t17_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t126 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t126  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t126 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t126_TI;
void* RuntimeInvoker_t126_t44 (MethodInfo* method, void* obj, void** args){
	typedef t126  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t126  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t126_TI, &ret);
}

struct t29;
extern TypeInfo t164_TI;
void* RuntimeInvoker_t164_t44 (MethodInfo* method, void* obj, void** args){
	typedef t164  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t164  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t164_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t35 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), method);
	return NULL;
}

struct t29;
extern TypeInfo t183_TI;
void* RuntimeInvoker_t183_t44 (MethodInfo* method, void* obj, void** args){
	typedef t183  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t183  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t183_TI, &ret);
}

struct t29;
#include "t119.h"
extern TypeInfo t119_TI;
void* RuntimeInvoker_t119_t44 (MethodInfo* method, void* obj, void** args){
	typedef t119  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t119  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t119_TI, &ret);
}

struct t29;
extern TypeInfo t821_TI;
void* RuntimeInvoker_t821_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t821_TI, &ret);
}

struct t29;
extern TypeInfo t866_TI;
void* RuntimeInvoker_t866_t44 (MethodInfo* method, void* obj, void** args){
	typedef t866  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t866  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t866_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1126 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1126  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t1126_TI;
void* RuntimeInvoker_t1126_t44 (MethodInfo* method, void* obj, void** args){
	typedef t1126  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t1126  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1126_TI, &ret);
}

struct t29;
extern TypeInfo t1189_TI;
void* RuntimeInvoker_t1189_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1189_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t465 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t465  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t465 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t1292_TI;
void* RuntimeInvoker_t1292_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1292_TI, &ret);
}

struct t29;
extern TypeInfo t465_TI;
void* RuntimeInvoker_t465_t44 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t601_TI;
void* RuntimeInvoker_t601_t22 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, float p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t601_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t601 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, double p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1629 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1629  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t1629 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1648 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1648  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t1648 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t816 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t816  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t816 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t48_TI;
void* RuntimeInvoker_t48_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t48_TI, &ret);
}

struct t29;
extern TypeInfo t57_TI;
void* RuntimeInvoker_t57_t44 (MethodInfo* method, void* obj, void** args){
	typedef t57  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t57  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t57_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t57 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t57  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t57 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t61_TI;
void* RuntimeInvoker_t61_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t61_TI, &ret);
}

struct t29;
extern TypeInfo t106_TI;
void* RuntimeInvoker_t106_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t106_TI, &ret);
}

struct t29;
extern TypeInfo t111_TI;
void* RuntimeInvoker_t111_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t111_TI, &ret);
}

struct t29;
extern TypeInfo t1248_TI;
void* RuntimeInvoker_t1248_t44 (MethodInfo* method, void* obj, void** args){
	typedef t1248  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t1248  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1248_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1248 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1248  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t1248 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t725_TI;
void* RuntimeInvoker_t725_t44 (MethodInfo* method, void* obj, void** args){
	typedef t725  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t725  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t725_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t725 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t725  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t725 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t120_TI;
void* RuntimeInvoker_t120_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t120_TI, &ret);
}

struct t29;
extern TypeInfo t330_TI;
void* RuntimeInvoker_t330_t44 (MethodInfo* method, void* obj, void** args){
	typedef t330  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t330  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t330_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t330 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t330  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t330 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t127_TI;
void* RuntimeInvoker_t127_t44 (MethodInfo* method, void* obj, void** args){
	typedef t127  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t127  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t127_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t127 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t127  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t127 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t128_TI;
void* RuntimeInvoker_t128_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t128_TI, &ret);
}

struct t29;
extern TypeInfo t140_TI;
void* RuntimeInvoker_t140_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t140_TI, &ret);
}

struct t29;
extern TypeInfo t184_TI;
void* RuntimeInvoker_t184_t44 (MethodInfo* method, void* obj, void** args){
	typedef t184  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t184  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t184_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t184 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t184  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t184 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t165_TI;
void* RuntimeInvoker_t165_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t165_TI, &ret);
}

struct t29;
extern TypeInfo t172_TI;
void* RuntimeInvoker_t172_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t172_TI, &ret);
}

struct t29;
extern TypeInfo t173_TI;
void* RuntimeInvoker_t173_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t173_TI, &ret);
}

struct t29;
extern TypeInfo t174_TI;
void* RuntimeInvoker_t174_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t174_TI, &ret);
}

struct t29;
extern TypeInfo t175_TI;
void* RuntimeInvoker_t175_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t175_TI, &ret);
}

struct t29;
extern TypeInfo t176_TI;
void* RuntimeInvoker_t176_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t176_TI, &ret);
}

struct t29;
extern TypeInfo t177_TI;
void* RuntimeInvoker_t177_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t177_TI, &ret);
}

struct t29;
extern TypeInfo t178_TI;
void* RuntimeInvoker_t178_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t178_TI, &ret);
}

struct t29;
extern TypeInfo t185_TI;
void* RuntimeInvoker_t185_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t185_TI, &ret);
}

struct t29;
extern TypeInfo t380_TI;
void* RuntimeInvoker_t380_t44 (MethodInfo* method, void* obj, void** args){
	typedef t380  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t380  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t380_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t380 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t380  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t380 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t381_TI;
void* RuntimeInvoker_t381_t44 (MethodInfo* method, void* obj, void** args){
	typedef t381  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t381  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t381_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t381 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t381  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t381 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t186_TI;
void* RuntimeInvoker_t186_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t186_TI, &ret);
}

struct t29;
extern TypeInfo t187_TI;
void* RuntimeInvoker_t187_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t187_TI, &ret);
}

struct t29;
extern TypeInfo t188_TI;
void* RuntimeInvoker_t188_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t188_TI, &ret);
}

struct t29;
extern TypeInfo t192_TI;
void* RuntimeInvoker_t192_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t192_TI, &ret);
}

struct t29;
extern TypeInfo t209_TI;
void* RuntimeInvoker_t209_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t209_TI, &ret);
}

struct t29;
extern TypeInfo t212_TI;
void* RuntimeInvoker_t212_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t212_TI, &ret);
}

struct t29;
extern TypeInfo t215_TI;
void* RuntimeInvoker_t215_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t215_TI, &ret);
}

struct t29;
extern TypeInfo t23_TI;
void* RuntimeInvoker_t23_t44 (MethodInfo* method, void* obj, void** args){
	typedef t23  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t23  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t23_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t23 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t23  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t23 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t219_TI;
void* RuntimeInvoker_t219_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t219_TI, &ret);
}

struct t29;
extern TypeInfo t225_TI;
void* RuntimeInvoker_t225_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t225_TI, &ret);
}

struct t29;
extern TypeInfo t207_TI;
void* RuntimeInvoker_t207_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t207_TI, &ret);
}

struct t29;
extern TypeInfo t231_TI;
void* RuntimeInvoker_t231_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t231_TI, &ret);
}

struct t29;
extern TypeInfo t233_TI;
void* RuntimeInvoker_t233_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t233_TI, &ret);
}

struct t29;
extern TypeInfo t239_TI;
void* RuntimeInvoker_t239_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t239_TI, &ret);
}

struct t29;
extern TypeInfo t247_TI;
void* RuntimeInvoker_t247_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t247_TI, &ret);
}

struct t29;
extern TypeInfo t249_TI;
void* RuntimeInvoker_t249_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t249_TI, &ret);
}

struct t29;
extern TypeInfo t250_TI;
void* RuntimeInvoker_t250_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t250_TI, &ret);
}

struct t29;
extern TypeInfo t251_TI;
void* RuntimeInvoker_t251_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t251_TI, &ret);
}

struct t29;
extern TypeInfo t253_TI;
void* RuntimeInvoker_t253_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t253_TI, &ret);
}

struct t29;
extern TypeInfo t255_TI;
void* RuntimeInvoker_t255_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t255_TI, &ret);
}

struct t29;
extern TypeInfo t256_TI;
void* RuntimeInvoker_t256_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t256_TI, &ret);
}

struct t29;
extern TypeInfo t257_TI;
void* RuntimeInvoker_t257_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t257_TI, &ret);
}

struct t29;
extern TypeInfo t445_TI;
void* RuntimeInvoker_t445_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t445_TI, &ret);
}

struct t29;
extern TypeInfo t376_TI;
void* RuntimeInvoker_t376_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t376_TI, &ret);
}

struct t29;
extern TypeInfo t447_TI;
void* RuntimeInvoker_t447_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t447_TI, &ret);
}

struct t29;
extern TypeInfo t477_TI;
void* RuntimeInvoker_t477_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t477_TI, &ret);
}

struct t29;
extern TypeInfo t149_TI;
void* RuntimeInvoker_t149_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t149_TI, &ret);
}

struct t29;
extern TypeInfo t205_TI;
void* RuntimeInvoker_t205_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t205_TI, &ret);
}

struct t29;
extern TypeInfo t383_TI;
void* RuntimeInvoker_t383_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t383_TI, &ret);
}

struct t29;
extern TypeInfo t384_TI;
void* RuntimeInvoker_t384_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t384_TI, &ret);
}

struct t29;
extern TypeInfo t382_TI;
void* RuntimeInvoker_t382_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t382_TI, &ret);
}

struct t29;
extern TypeInfo t393_TI;
void* RuntimeInvoker_t393_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t393_TI, &ret);
}

struct t29;
extern TypeInfo t413_TI;
void* RuntimeInvoker_t413_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t413_TI, &ret);
}

struct t29;
extern TypeInfo t408_TI;
void* RuntimeInvoker_t408_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t408_TI, &ret);
}

struct t29;
extern TypeInfo t319_TI;
void* RuntimeInvoker_t319_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t319_TI, &ret);
}

struct t29;
extern TypeInfo t386_TI;
void* RuntimeInvoker_t386_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t386_TI, &ret);
}

struct t29;
extern TypeInfo t385_TI;
void* RuntimeInvoker_t385_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t385_TI, &ret);
}

struct t29;
extern TypeInfo t512_TI;
void* RuntimeInvoker_t512_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t512_TI, &ret);
}

struct t29;
extern TypeInfo t517_TI;
void* RuntimeInvoker_t517_t44 (MethodInfo* method, void* obj, void** args){
	typedef t517  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t517  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t517_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t517 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t517  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t517 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t150_TI;
void* RuntimeInvoker_t150_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t150_TI, &ret);
}

struct t29;
extern TypeInfo t151_TI;
void* RuntimeInvoker_t151_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t151_TI, &ret);
}

struct t29;
extern TypeInfo t152_TI;
void* RuntimeInvoker_t152_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t152_TI, &ret);
}

struct t29;
extern TypeInfo t361_TI;
void* RuntimeInvoker_t361_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t361_TI, &ret);
}

struct t29;
extern TypeInfo t632_TI;
void* RuntimeInvoker_t632_t44 (MethodInfo* method, void* obj, void** args){
	typedef t632  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t632  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t632_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t632 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t632  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t632 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t498_TI;
void* RuntimeInvoker_t498_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t498_TI, &ret);
}

struct t29;
extern TypeInfo t542_TI;
void* RuntimeInvoker_t542_t44 (MethodInfo* method, void* obj, void** args){
	typedef t542  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t542  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t542_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t542 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t542  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t542 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t552_TI;
void* RuntimeInvoker_t552_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t552_TI, &ret);
}

struct t29;
extern TypeInfo t551_TI;
void* RuntimeInvoker_t551_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t551_TI, &ret);
}

struct t29;
extern TypeInfo t554_TI;
void* RuntimeInvoker_t554_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t554_TI, &ret);
}

struct t29;
extern TypeInfo t564_TI;
void* RuntimeInvoker_t564_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t564_TI, &ret);
}

struct t29;
extern TypeInfo t578_TI;
void* RuntimeInvoker_t578_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t578_TI, &ret);
}

struct t29;
extern TypeInfo t737_TI;
void* RuntimeInvoker_t737_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t737_TI, &ret);
}

struct t29;
extern TypeInfo t740_TI;
void* RuntimeInvoker_t740_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t740_TI, &ret);
}

struct t29;
extern TypeInfo t741_TI;
void* RuntimeInvoker_t741_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t741_TI, &ret);
}

struct t29;
extern TypeInfo t742_TI;
void* RuntimeInvoker_t742_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t742_TI, &ret);
}

struct t29;
extern TypeInfo t751_TI;
void* RuntimeInvoker_t751_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t751_TI, &ret);
}

struct t29;
extern TypeInfo t766_TI;
void* RuntimeInvoker_t766_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t766_TI, &ret);
}

struct t29;
extern TypeInfo t775_TI;
void* RuntimeInvoker_t775_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t775_TI, &ret);
}

struct t29;
extern TypeInfo t784_TI;
void* RuntimeInvoker_t784_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t784_TI, &ret);
}

struct t29;
extern TypeInfo t785_TI;
void* RuntimeInvoker_t785_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t785_TI, &ret);
}

struct t29;
extern TypeInfo t787_TI;
void* RuntimeInvoker_t787_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t787_TI, &ret);
}

struct t29;
extern TypeInfo t805_TI;
void* RuntimeInvoker_t805_t44 (MethodInfo* method, void* obj, void** args){
	typedef t805  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t805  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t805_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t805 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t805  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t805 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t811_TI;
void* RuntimeInvoker_t811_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t811_TI, &ret);
}

struct t29;
extern TypeInfo t798_TI;
void* RuntimeInvoker_t798_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t798_TI, &ret);
}

struct t29;
extern TypeInfo t794_TI;
void* RuntimeInvoker_t794_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t794_TI, &ret);
}

struct t29;
extern TypeInfo t814_TI;
void* RuntimeInvoker_t814_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t814_TI, &ret);
}

struct t29;
extern TypeInfo t815_TI;
void* RuntimeInvoker_t815_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t815_TI, &ret);
}

struct t29;
extern TypeInfo t825_TI;
void* RuntimeInvoker_t825_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t825_TI, &ret);
}

struct t29;
extern TypeInfo t817_TI;
void* RuntimeInvoker_t817_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t817_TI, &ret);
}

struct t29;
extern TypeInfo t790_TI;
void* RuntimeInvoker_t790_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t790_TI, &ret);
}

struct t29;
extern TypeInfo t842_TI;
void* RuntimeInvoker_t842_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t842_TI, &ret);
}

struct t29;
extern TypeInfo t843_TI;
void* RuntimeInvoker_t843_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t843_TI, &ret);
}

struct t29;
extern TypeInfo t844_TI;
void* RuntimeInvoker_t844_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t844_TI, &ret);
}

struct t29;
extern TypeInfo t845_TI;
void* RuntimeInvoker_t845_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t845_TI, &ret);
}

struct t29;
extern TypeInfo t849_TI;
void* RuntimeInvoker_t849_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t849_TI, &ret);
}

struct t29;
extern TypeInfo t859_TI;
void* RuntimeInvoker_t859_t44 (MethodInfo* method, void* obj, void** args){
	typedef t859  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t859  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t859_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t859 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t859  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t859 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t862_TI;
void* RuntimeInvoker_t862_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t862_TI, &ret);
}

struct t29;
extern TypeInfo t895_TI;
void* RuntimeInvoker_t895_t44 (MethodInfo* method, void* obj, void** args){
	typedef t895  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t895  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t895_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t895 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t895  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t895 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t897_TI;
void* RuntimeInvoker_t897_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t897_TI, &ret);
}

struct t29;
extern TypeInfo t899_TI;
void* RuntimeInvoker_t899_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t899_TI, &ret);
}

struct t29;
extern TypeInfo t898_TI;
void* RuntimeInvoker_t898_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t898_TI, &ret);
}

struct t29;
extern TypeInfo t970_TI;
void* RuntimeInvoker_t970_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t970_TI, &ret);
}

struct t29;
extern TypeInfo t977_TI;
void* RuntimeInvoker_t977_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t977_TI, &ret);
}

struct t29;
extern TypeInfo t1003_TI;
void* RuntimeInvoker_t1003_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1003_TI, &ret);
}

struct t29;
extern TypeInfo t1007_TI;
void* RuntimeInvoker_t1007_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1007_TI, &ret);
}

struct t29;
extern TypeInfo t1009_TI;
void* RuntimeInvoker_t1009_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1009_TI, &ret);
}

struct t29;
extern TypeInfo t1015_TI;
void* RuntimeInvoker_t1015_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1015_TI, &ret);
}

struct t29;
extern TypeInfo t1016_TI;
void* RuntimeInvoker_t1016_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1016_TI, &ret);
}

struct t29;
extern TypeInfo t1018_TI;
void* RuntimeInvoker_t1018_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1018_TI, &ret);
}

struct t29;
extern TypeInfo t1024_TI;
void* RuntimeInvoker_t1024_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1024_TI, &ret);
}

struct t29;
extern TypeInfo t1022_TI;
void* RuntimeInvoker_t1022_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1022_TI, &ret);
}

struct t29;
extern TypeInfo t1044_TI;
void* RuntimeInvoker_t1044_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1044_TI, &ret);
}

struct t29;
extern TypeInfo t1021_TI;
void* RuntimeInvoker_t1021_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1021_TI, &ret);
}

struct t29;
extern TypeInfo t1043_TI;
void* RuntimeInvoker_t1043_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1043_TI, &ret);
}

struct t29;
extern TypeInfo t1026_TI;
void* RuntimeInvoker_t1026_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1026_TI, &ret);
}

struct t29;
extern TypeInfo t1067_TI;
void* RuntimeInvoker_t1067_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1067_TI, &ret);
}

struct t29;
extern TypeInfo t1037_TI;
void* RuntimeInvoker_t1037_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1037_TI, &ret);
}

struct t29;
extern TypeInfo t1173_TI;
void* RuntimeInvoker_t1173_t44 (MethodInfo* method, void* obj, void** args){
	typedef t1173  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t1173  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1173_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1173 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1173  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t1173 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t1197_TI;
void* RuntimeInvoker_t1197_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1197_TI, &ret);
}

struct t29;
extern TypeInfo t1200_TI;
void* RuntimeInvoker_t1200_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1200_TI, &ret);
}

struct t29;
extern TypeInfo t1272_TI;
void* RuntimeInvoker_t1272_t44 (MethodInfo* method, void* obj, void** args){
	typedef t1272  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t1272  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1272_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1272 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1272  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t1272 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t1274_TI;
void* RuntimeInvoker_t1274_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1274_TI, &ret);
}

struct t29;
extern TypeInfo t1279_TI;
void* RuntimeInvoker_t1279_t44 (MethodInfo* method, void* obj, void** args){
	typedef t1279  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t1279  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1279_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1279 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1279  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t1279 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t1280_TI;
void* RuntimeInvoker_t1280_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1280_TI, &ret);
}

struct t29;
extern TypeInfo t1284_TI;
void* RuntimeInvoker_t1284_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1284_TI, &ret);
}

struct t29;
extern TypeInfo t1285_TI;
void* RuntimeInvoker_t1285_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1285_TI, &ret);
}

struct t29;
extern TypeInfo t1286_TI;
void* RuntimeInvoker_t1286_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1286_TI, &ret);
}

struct t29;
extern TypeInfo t1120_TI;
void* RuntimeInvoker_t1120_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1120_TI, &ret);
}

struct t29;
extern TypeInfo t1298_TI;
void* RuntimeInvoker_t1298_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1298_TI, &ret);
}

struct t29;
extern TypeInfo t1093_TI;
void* RuntimeInvoker_t1093_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1093_TI, &ret);
}

struct t29;
extern TypeInfo t1301_TI;
void* RuntimeInvoker_t1301_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1301_TI, &ret);
}

struct t29;
extern TypeInfo t923_TI;
void* RuntimeInvoker_t923_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t923_TI, &ret);
}

struct t29;
extern TypeInfo t851_TI;
void* RuntimeInvoker_t851_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t851_TI, &ret);
}

struct t29;
extern TypeInfo t1306_TI;
void* RuntimeInvoker_t1306_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1306_TI, &ret);
}

struct t29;
extern TypeInfo t1311_TI;
void* RuntimeInvoker_t1311_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1311_TI, &ret);
}

struct t29;
extern TypeInfo t1313_TI;
void* RuntimeInvoker_t1313_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1313_TI, &ret);
}

struct t29;
extern TypeInfo t1314_TI;
void* RuntimeInvoker_t1314_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1314_TI, &ret);
}

struct t29;
extern TypeInfo t1319_TI;
void* RuntimeInvoker_t1319_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1319_TI, &ret);
}

struct t29;
extern TypeInfo t1321_TI;
void* RuntimeInvoker_t1321_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1321_TI, &ret);
}

struct t29;
extern TypeInfo t1064_TI;
void* RuntimeInvoker_t1064_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1064_TI, &ret);
}

struct t29;
extern TypeInfo t1362_TI;
void* RuntimeInvoker_t1362_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1362_TI, &ret);
}

struct t29;
extern TypeInfo t630_TI;
void* RuntimeInvoker_t630_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t630_TI, &ret);
}

struct t29;
extern TypeInfo t1150_TI;
void* RuntimeInvoker_t1150_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1150_TI, &ret);
}

struct t29;
extern TypeInfo t1367_TI;
void* RuntimeInvoker_t1367_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1367_TI, &ret);
}

struct t29;
extern TypeInfo t1347_TI;
void* RuntimeInvoker_t1347_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1347_TI, &ret);
}

struct t29;
extern TypeInfo t1149_TI;
void* RuntimeInvoker_t1149_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1149_TI, &ret);
}

struct t29;
extern TypeInfo t1342_TI;
void* RuntimeInvoker_t1342_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1342_TI, &ret);
}

struct t29;
extern TypeInfo t1370_TI;
void* RuntimeInvoker_t1370_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1370_TI, &ret);
}

struct t29;
extern TypeInfo t1384_TI;
void* RuntimeInvoker_t1384_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1384_TI, &ret);
}

struct t29;
extern TypeInfo t1353_TI;
void* RuntimeInvoker_t1353_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1353_TI, &ret);
}

struct t29;
extern TypeInfo t1363_TI;
void* RuntimeInvoker_t1363_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1363_TI, &ret);
}

struct t29;
extern TypeInfo t1382_TI;
void* RuntimeInvoker_t1382_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1382_TI, &ret);
}

struct t29;
extern TypeInfo t1148_TI;
void* RuntimeInvoker_t1148_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1148_TI, &ret);
}

struct t29;
extern TypeInfo t1392_TI;
void* RuntimeInvoker_t1392_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1392_TI, &ret);
}

struct t29;
extern TypeInfo t1394_TI;
void* RuntimeInvoker_t1394_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1394_TI, &ret);
}

struct t29;
extern TypeInfo t1397_TI;
void* RuntimeInvoker_t1397_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1397_TI, &ret);
}

struct t29;
extern TypeInfo t1398_TI;
void* RuntimeInvoker_t1398_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1398_TI, &ret);
}

struct t29;
extern TypeInfo t1153_TI;
void* RuntimeInvoker_t1153_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1153_TI, &ret);
}

struct t29;
extern TypeInfo t1154_TI;
void* RuntimeInvoker_t1154_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1154_TI, &ret);
}

struct t29;
extern TypeInfo t1403_TI;
void* RuntimeInvoker_t1403_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1403_TI, &ret);
}

struct t29;
extern TypeInfo t1405_TI;
void* RuntimeInvoker_t1405_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1405_TI, &ret);
}

struct t29;
extern TypeInfo t1408_TI;
void* RuntimeInvoker_t1408_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1408_TI, &ret);
}

struct t29;
extern TypeInfo t1156_TI;
void* RuntimeInvoker_t1156_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1156_TI, &ret);
}

struct t29;
extern TypeInfo t1437_TI;
void* RuntimeInvoker_t1437_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1437_TI, &ret);
}

struct t29;
extern TypeInfo t1484_TI;
void* RuntimeInvoker_t1484_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1484_TI, &ret);
}

struct t29;
extern TypeInfo t1490_TI;
void* RuntimeInvoker_t1490_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1490_TI, &ret);
}

struct t29;
extern TypeInfo t1491_TI;
void* RuntimeInvoker_t1491_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1491_TI, &ret);
}

struct t29;
extern TypeInfo t1492_TI;
void* RuntimeInvoker_t1492_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1492_TI, &ret);
}

struct t29;
extern TypeInfo t1493_TI;
void* RuntimeInvoker_t1493_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1493_TI, &ret);
}

struct t29;
extern TypeInfo t816_TI;
void* RuntimeInvoker_t816_t44 (MethodInfo* method, void* obj, void** args){
	typedef t816  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t816  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t816_TI, &ret);
}

struct t29;
extern TypeInfo t1495_TI;
void* RuntimeInvoker_t1495_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1495_TI, &ret);
}

struct t29;
extern TypeInfo t1496_TI;
void* RuntimeInvoker_t1496_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1496_TI, &ret);
}

struct t29;
extern TypeInfo t1497_TI;
void* RuntimeInvoker_t1497_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1497_TI, &ret);
}

struct t29;
extern TypeInfo t1513_TI;
void* RuntimeInvoker_t1513_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1513_TI, &ret);
}

struct t29;
extern TypeInfo t1523_TI;
void* RuntimeInvoker_t1523_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1523_TI, &ret);
}

struct t29;
extern TypeInfo t795_TI;
void* RuntimeInvoker_t795_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t795_TI, &ret);
}

struct t29;
extern TypeInfo t1023_TI;
void* RuntimeInvoker_t1023_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1023_TI, &ret);
}

struct t29;
extern TypeInfo t1099_TI;
void* RuntimeInvoker_t1099_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1099_TI, &ret);
}

struct t29;
extern TypeInfo t1117_TI;
void* RuntimeInvoker_t1117_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1117_TI, &ret);
}

struct t29;
extern TypeInfo t1568_TI;
void* RuntimeInvoker_t1568_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1568_TI, &ret);
}

struct t29;
extern TypeInfo t1600_TI;
void* RuntimeInvoker_t1600_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1600_TI, &ret);
}

struct t29;
extern TypeInfo t1605_TI;
void* RuntimeInvoker_t1605_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1605_TI, &ret);
}

struct t29;
extern TypeInfo t1129_TI;
void* RuntimeInvoker_t1129_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1129_TI, &ret);
}

struct t29;
extern TypeInfo t1627_TI;
void* RuntimeInvoker_t1627_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1627_TI, &ret);
}

struct t29;
extern TypeInfo t1628_TI;
void* RuntimeInvoker_t1628_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1628_TI, &ret);
}

struct t29;
extern TypeInfo t1112_TI;
void* RuntimeInvoker_t1112_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1112_TI, &ret);
}

struct t29;
extern TypeInfo t1620_TI;
void* RuntimeInvoker_t1620_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1620_TI, &ret);
}

struct t29;
extern TypeInfo t1644_TI;
void* RuntimeInvoker_t1644_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1644_TI, &ret);
}

struct t29;
extern TypeInfo t947_TI;
void* RuntimeInvoker_t947_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t947_TI, &ret);
}

struct t29;
extern TypeInfo t939_TI;
void* RuntimeInvoker_t939_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t939_TI, &ret);
}

struct t29;
extern TypeInfo t1127_TI;
void* RuntimeInvoker_t1127_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1127_TI, &ret);
}

struct t29;
extern TypeInfo t1677_TI;
void* RuntimeInvoker_t1677_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1677_TI, &ret);
}

struct t29;
extern TypeInfo t344_TI;
void* RuntimeInvoker_t344_t1126 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, t1126  p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t35 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t35 p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t35*)args[0]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t601_TI;
void* RuntimeInvoker_t601_t601 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, double p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t601_TI, &ret);
}

struct t29;
extern TypeInfo t919_TI;
void* RuntimeInvoker_t919_t1126 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, t1126  p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t1126_TI;
void* RuntimeInvoker_t1126_t919 (MethodInfo* method, void* obj, void** args){
	typedef t1126  (*Func)(void* obj, int64_t p1, MethodInfo* method);
	t1126  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t1126_TI, &ret);
}

struct t29;
extern TypeInfo t465_TI;
void* RuntimeInvoker_t465_t919 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, int64_t p1, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t1089_TI;
void* RuntimeInvoker_t1089_t1126 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, t1126  p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], method);
	return NULL;
}

struct t29;
extern TypeInfo t132_TI;
void* RuntimeInvoker_t132_t22 (MethodInfo* method, void* obj, void** args){
	typedef t132  (*Func)(void* obj, float p1, MethodInfo* method);
	t132  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t132_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t17 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t17  p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((t17 *)args[0]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t23 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t23  p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((t23 *)args[0]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t183 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t183  p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((t183 *)args[0]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t23_TI;
void* RuntimeInvoker_t23_t22 (MethodInfo* method, void* obj, void** args){
	typedef t23  (*Func)(void* obj, float p1, MethodInfo* method);
	t23  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t23_TI, &ret);
}

struct t29;
extern TypeInfo t601_TI;
void* RuntimeInvoker_t601_t1756 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, t1126 * p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, (t1126 *)args[0], method);
	return Box(&t601_TI, &ret);
}

struct t29;
extern TypeInfo t1126_TI;
void* RuntimeInvoker_t1126_t22 (MethodInfo* method, void* obj, void** args){
	typedef t1126  (*Func)(void* obj, float p1, MethodInfo* method);
	t1126  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t1126_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t1126 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t1126  p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t465_TI;
void* RuntimeInvoker_t465_t22 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, float p1, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t322_TI;
void* RuntimeInvoker_t322_t44 (MethodInfo* method, void* obj, void** args){
	typedef t322  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t322  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t322_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t322 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t322  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t322 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t2420_TI;
void* RuntimeInvoker_t2420_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2420  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t2420  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t2420_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t2420 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t2420  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t2420 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t2444_TI;
void* RuntimeInvoker_t2444_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2444  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t2444  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t2444_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t2444 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t2444  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t2444 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t2465_TI;
void* RuntimeInvoker_t2465_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2465  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t2465  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t2465_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t2465 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t2465  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t2465 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t2458_TI;
void* RuntimeInvoker_t2458_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2458  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t2458  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t2458_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t2458 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t2458  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t2458 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t2569_TI;
void* RuntimeInvoker_t2569_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2569  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t2569  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t2569_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t2569 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t2569  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t2569 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t2562_TI;
void* RuntimeInvoker_t2562_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2562  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t2562  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t2562_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t2562 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t2562  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t2562 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t2859_TI;
void* RuntimeInvoker_t2859_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2859  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t2859  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t2859_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t2859 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t2859  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t2859 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t2893_TI;
void* RuntimeInvoker_t2893_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2893  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t2893  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t2893_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t2893 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t2893  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t2893 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t2911_TI;
void* RuntimeInvoker_t2911_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2911  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t2911  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t2911_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t2911 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t2911  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t2911 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t3106_TI;
void* RuntimeInvoker_t3106_t44 (MethodInfo* method, void* obj, void** args){
	typedef t3106  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t3106  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t3106_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t3106 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t3106  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t3106 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t3192_TI;
void* RuntimeInvoker_t3192_t44 (MethodInfo* method, void* obj, void** args){
	typedef t3192  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t3192  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t3192_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t3192 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t3192  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t3192 *)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t601_TI;
void* RuntimeInvoker_t601_t866 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, t866  p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((t866 *)args[0]), method);
	return Box(&t601_TI, &ret);
}

struct t29;
extern TypeInfo t1126_TI;
void* RuntimeInvoker_t1126_t601 (MethodInfo* method, void* obj, void** args){
	typedef t1126  (*Func)(void* obj, double p1, MethodInfo* method);
	t1126  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t1126_TI, &ret);
}

struct t29;
extern TypeInfo t601_TI;
void* RuntimeInvoker_t601_t1126 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, t1126  p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return Box(&t601_TI, &ret);
}

struct t29;
extern TypeInfo t465_TI;
void* RuntimeInvoker_t465_t601 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, double p1, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t816_TI;
void* RuntimeInvoker_t816_t601 (MethodInfo* method, void* obj, void** args){
	typedef t816  (*Func)(void* obj, double p1, MethodInfo* method);
	t816  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(&t816_TI, &ret);
}

struct t29;
extern TypeInfo t194_TI;
struct t29;
void* RuntimeInvoker_t194_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t194_TI, &ret);
}

struct t29;
extern TypeInfo t35_TI;
void* RuntimeInvoker_t35_t44 (MethodInfo* method, void* obj, void** args){
	typedef t35 (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t35 ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t35_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int8_t p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

struct t29;
extern TypeInfo t297_TI;
struct t29;
void* RuntimeInvoker_t297_t29 (MethodInfo* method, void* obj, void** args){
	typedef int8_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t297_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t35 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t35 p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t35*)args[0]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t362_TI;
void* RuntimeInvoker_t362_t596 (MethodInfo* method, void* obj, void** args){
	typedef t362  (*Func)(void* obj, t362 * p1, MethodInfo* method);
	t362  ret = ((Func)method->method)(obj, (t362 *)args[0], method);
	return Box(&t362_TI, &ret);
}

struct t29;
extern TypeInfo t396_TI;
void* RuntimeInvoker_t396_t597 (MethodInfo* method, void* obj, void** args){
	typedef t396  (*Func)(void* obj, t396 * p1, MethodInfo* method);
	t396  ret = ((Func)method->method)(obj, (t396 *)args[0], method);
	return Box(&t396_TI, &ret);
}

struct t29;
extern TypeInfo t936_TI;
void* RuntimeInvoker_t936_t326 (MethodInfo* method, void* obj, void** args){
	typedef t936  (*Func)(void* obj, bool* p1, MethodInfo* method);
	t936  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(&t936_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t348 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, uint8_t p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

struct t29;
extern TypeInfo t348_TI;
struct t29;
void* RuntimeInvoker_t348_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t348_TI, &ret);
}

struct t29;
extern TypeInfo t344_TI;
void* RuntimeInvoker_t344_t35 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, t35 p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((t35*)args[0]), method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t1131_TI;
void* RuntimeInvoker_t1131_t44 (MethodInfo* method, void* obj, void** args){
	typedef t1131  (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t1131  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(&t1131_TI, &ret);
}

struct t29;
extern TypeInfo t17_TI;
void* RuntimeInvoker_t17_t17 (MethodInfo* method, void* obj, void** args){
	typedef t17  (*Func)(void* obj, t17  p1, MethodInfo* method);
	t17  ret = ((Func)method->method)(obj, *((t17 *)args[0]), method);
	return Box(&t17_TI, &ret);
}

struct t29;
extern TypeInfo t238_TI;
void* RuntimeInvoker_t238_t17 (MethodInfo* method, void* obj, void** args){
	typedef t238  (*Func)(void* obj, t17  p1, MethodInfo* method);
	t238  ret = ((Func)method->method)(obj, *((t17 *)args[0]), method);
	return Box(&t238_TI, &ret);
}

struct t29;
extern TypeInfo t164_TI;
void* RuntimeInvoker_t164_t164 (MethodInfo* method, void* obj, void** args){
	typedef t164  (*Func)(void* obj, t164  p1, MethodInfo* method);
	t164  ret = ((Func)method->method)(obj, *((t164 *)args[0]), method);
	return Box(&t164_TI, &ret);
}

struct t29;
extern TypeInfo t17_TI;
void* RuntimeInvoker_t17_t23 (MethodInfo* method, void* obj, void** args){
	typedef t17  (*Func)(void* obj, t23  p1, MethodInfo* method);
	t17  ret = ((Func)method->method)(obj, *((t23 *)args[0]), method);
	return Box(&t17_TI, &ret);
}

struct t29;
extern TypeInfo t23_TI;
void* RuntimeInvoker_t23_t17 (MethodInfo* method, void* obj, void** args){
	typedef t23  (*Func)(void* obj, t17  p1, MethodInfo* method);
	t23  ret = ((Func)method->method)(obj, *((t17 *)args[0]), method);
	return Box(&t23_TI, &ret);
}

struct t29;
extern TypeInfo t23_TI;
void* RuntimeInvoker_t23_t23 (MethodInfo* method, void* obj, void** args){
	typedef t23  (*Func)(void* obj, t23  p1, MethodInfo* method);
	t23  ret = ((Func)method->method)(obj, *((t23 *)args[0]), method);
	return Box(&t23_TI, &ret);
}

struct t29;
extern TypeInfo t183_TI;
void* RuntimeInvoker_t183_t132 (MethodInfo* method, void* obj, void** args){
	typedef t183  (*Func)(void* obj, t132  p1, MethodInfo* method);
	t183  ret = ((Func)method->method)(obj, *((t132 *)args[0]), method);
	return Box(&t183_TI, &ret);
}

struct t29;
#include "t287.h"
extern TypeInfo t287_TI;
void* RuntimeInvoker_t287_t132 (MethodInfo* method, void* obj, void** args){
	typedef t287  (*Func)(void* obj, t132  p1, MethodInfo* method);
	t287  ret = ((Func)method->method)(obj, *((t132 *)args[0]), method);
	return Box(&t287_TI, &ret);
}

struct t29;
extern TypeInfo t132_TI;
void* RuntimeInvoker_t132_t287 (MethodInfo* method, void* obj, void** args){
	typedef t132  (*Func)(void* obj, t287  p1, MethodInfo* method);
	t132  ret = ((Func)method->method)(obj, *((t287 *)args[0]), method);
	return Box(&t132_TI, &ret);
}

struct t29;
extern TypeInfo t362_TI;
void* RuntimeInvoker_t362_t362 (MethodInfo* method, void* obj, void** args){
	typedef t362  (*Func)(void* obj, t362  p1, MethodInfo* method);
	t362  ret = ((Func)method->method)(obj, *((t362 *)args[0]), method);
	return Box(&t362_TI, &ret);
}

struct t29;
extern TypeInfo t396_TI;
void* RuntimeInvoker_t396_t396 (MethodInfo* method, void* obj, void** args){
	typedef t396  (*Func)(void* obj, t396  p1, MethodInfo* method);
	t396  ret = ((Func)method->method)(obj, *((t396 *)args[0]), method);
	return Box(&t396_TI, &ret);
}

struct t29;
extern TypeInfo t396_TI;
void* RuntimeInvoker_t396_t23 (MethodInfo* method, void* obj, void** args){
	typedef t396  (*Func)(void* obj, t23  p1, MethodInfo* method);
	t396  ret = ((Func)method->method)(obj, *((t23 *)args[0]), method);
	return Box(&t396_TI, &ret);
}

struct t29;
extern TypeInfo t329_TI;
void* RuntimeInvoker_t329_t23 (MethodInfo* method, void* obj, void** args){
	typedef t329  (*Func)(void* obj, t23  p1, MethodInfo* method);
	t329  ret = ((Func)method->method)(obj, *((t23 *)args[0]), method);
	return Box(&t329_TI, &ret);
}

struct t29;
extern TypeInfo t238_TI;
void* RuntimeInvoker_t238_t238 (MethodInfo* method, void* obj, void** args){
	typedef t238  (*Func)(void* obj, t238  p1, MethodInfo* method);
	t238  ret = ((Func)method->method)(obj, *((t238 *)args[0]), method);
	return Box(&t238_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t372 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int16_t p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

struct t29;
extern TypeInfo t372_TI;
struct t29;
void* RuntimeInvoker_t372_t29 (MethodInfo* method, void* obj, void** args){
	typedef int16_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t372_TI, &ret);
}

struct t29;
extern TypeInfo t1126_TI;
void* RuntimeInvoker_t1126_t1126 (MethodInfo* method, void* obj, void** args){
	typedef t1126  (*Func)(void* obj, t1126  p1, MethodInfo* method);
	t1126  ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return Box(&t1126_TI, &ret);
}

struct t29;
extern TypeInfo t35_TI;
void* RuntimeInvoker_t35_t919 (MethodInfo* method, void* obj, void** args){
	typedef t35 (*Func)(void* obj, int64_t p1, MethodInfo* method);
	t35 ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t35_TI, &ret);
}

struct t29;
extern TypeInfo t1292_TI;
void* RuntimeInvoker_t1292_t465 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t465  p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t465 *)args[0]), method);
	return Box(&t1292_TI, &ret);
}

struct t29;
extern TypeInfo t465_TI;
void* RuntimeInvoker_t465_t816 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, t816  p1, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, *((t816 *)args[0]), method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t816_TI;
void* RuntimeInvoker_t816_t816 (MethodInfo* method, void* obj, void** args){
	typedef t816  (*Func)(void* obj, t816  p1, MethodInfo* method);
	t816  ret = ((Func)method->method)(obj, *((t816 *)args[0]), method);
	return Box(&t816_TI, &ret);
}

struct t29;
extern TypeInfo t816_TI;
void* RuntimeInvoker_t816_t465 (MethodInfo* method, void* obj, void** args){
	typedef t816  (*Func)(void* obj, t465  p1, MethodInfo* method);
	t816  ret = ((Func)method->method)(obj, *((t465 *)args[0]), method);
	return Box(&t816_TI, &ret);
}

struct t29;
extern TypeInfo t465_TI;
void* RuntimeInvoker_t465_t465 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, t465  p1, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, *((t465 *)args[0]), method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t626_TI;
struct t29;
void* RuntimeInvoker_t626_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t626_TI, &ret);
}

struct t29;
extern TypeInfo t1089_TI;
void* RuntimeInvoker_t1089_t35 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, t35 p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((t35*)args[0]), method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t1131_TI;
void* RuntimeInvoker_t1131_t919 (MethodInfo* method, void* obj, void** args){
	typedef t1131  (*Func)(void* obj, int64_t p1, MethodInfo* method);
	t1131  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(&t1131_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t35 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t35 p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((t35*)args[0]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t344_TI;
struct t29;
void* RuntimeInvoker_t344_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t344_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t919 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int64_t p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

struct t29;
extern TypeInfo t919_TI;
struct t29;
void* RuntimeInvoker_t919_t29 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t1089_TI;
struct t29;
void* RuntimeInvoker_t1089_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
struct t29;
void* RuntimeInvoker_t22_t29 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t29 * p1, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t22_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t22 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, float p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

struct t29;
extern TypeInfo t601_TI;
struct t29;
void* RuntimeInvoker_t601_t29 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, t29 * p1, MethodInfo* method);
	double ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t601_TI, &ret);
}

struct t29;
#include "t1380.h"
extern TypeInfo t1380_TI;
void* RuntimeInvoker_t1380_t35 (MethodInfo* method, void* obj, void** args){
	typedef t1380  (*Func)(void* obj, t35 p1, MethodInfo* method);
	t1380  ret = ((Func)method->method)(obj, *((t35*)args[0]), method);
	return Box(&t1380_TI, &ret);
}

struct t29;
extern TypeInfo t1342_TI;
void* RuntimeInvoker_t1342_t35 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t35 p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t35*)args[0]), method);
	return Box(&t1342_TI, &ret);
}

struct t29;
extern TypeInfo t1150_TI;
void* RuntimeInvoker_t1150_t35 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t35 p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t35*)args[0]), method);
	return Box(&t1150_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t601 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, double p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t957 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t* p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t2004 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t* p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t390 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t* p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

struct t29;
extern TypeInfo t57_TI;
struct t29;
void* RuntimeInvoker_t57_t29 (MethodInfo* method, void* obj, void** args){
	typedef t57  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t57  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t57_TI, &ret);
}

struct t29;
extern TypeInfo t192_TI;
struct t29;
void* RuntimeInvoker_t192_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t192_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t23 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t23  p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t23 *)args[0]), method);
	return ret;
}

struct t29;
extern TypeInfo t17_TI;
struct t29;
void* RuntimeInvoker_t17_t29 (MethodInfo* method, void* obj, void** args){
	typedef t17  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t17  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t17_TI, &ret);
}

struct t29;
extern TypeInfo t183_TI;
struct t29;
void* RuntimeInvoker_t183_t29 (MethodInfo* method, void* obj, void** args){
	typedef t183  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t183  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t183_TI, &ret);
}

struct t29;
extern TypeInfo t790_TI;
struct t29;
void* RuntimeInvoker_t790_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t790_TI, &ret);
}

struct t29;
extern TypeInfo t811_TI;
struct t29;
void* RuntimeInvoker_t811_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t811_TI, &ret);
}

struct t29;
extern TypeInfo t849_TI;
struct t29;
void* RuntimeInvoker_t849_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t849_TI, &ret);
}

struct t29;
extern TypeInfo t897_TI;
struct t29;
void* RuntimeInvoker_t897_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t897_TI, &ret);
}

struct t29;
extern TypeInfo t465_TI;
struct t29;
void* RuntimeInvoker_t465_t29 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t1126_TI;
struct t29;
void* RuntimeInvoker_t1126_t29 (MethodInfo* method, void* obj, void** args){
	typedef t1126  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t1126  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t1126_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t1126 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t1126  p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t1126 *)args[0]), method);
	return ret;
}

struct t29;
extern TypeInfo t1127_TI;
struct t29;
void* RuntimeInvoker_t1127_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t1127_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t43 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t43  p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t43 *)args[0]), method);
	return ret;
}

struct t29;
extern TypeInfo t43_TI;
struct t29;
void* RuntimeInvoker_t43_t29 (MethodInfo* method, void* obj, void** args){
	typedef t43  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t43  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t43_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t927 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t927  p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t927 *)args[0]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t735 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t735  p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t735 *)args[0]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t1343 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t1343  p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t1343 *)args[0]), method);
	return ret;
}

struct t29;
#include "t1373.h"
extern TypeInfo t1373_TI;
struct t29;
void* RuntimeInvoker_t1373_t29 (MethodInfo* method, void* obj, void** args){
	typedef t1373  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t1373  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t1373_TI, &ret);
}

struct t29;
extern TypeInfo t1148_TI;
struct t29;
void* RuntimeInvoker_t1148_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t1148_TI, &ret);
}

struct t29;
extern TypeInfo t725_TI;
struct t29;
void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args){
	typedef t725  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t725  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t725_TI, &ret);
}

struct t29;
extern TypeInfo t184_TI;
struct t29;
void* RuntimeInvoker_t184_t29 (MethodInfo* method, void* obj, void** args){
	typedef t184  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t184  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t184_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t131 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t131  p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t131 *)args[0]), method);
	return ret;
}

struct t29;
extern TypeInfo t381_TI;
struct t29;
void* RuntimeInvoker_t381_t29 (MethodInfo* method, void* obj, void** args){
	typedef t381  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t381  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t381_TI, &ret);
}

struct t29;
extern TypeInfo t380_TI;
struct t29;
void* RuntimeInvoker_t380_t29 (MethodInfo* method, void* obj, void** args){
	typedef t380  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t380  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t380_TI, &ret);
}

struct t29;
extern TypeInfo t552_TI;
struct t29;
void* RuntimeInvoker_t552_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t552_TI, &ret);
}

struct t29;
extern TypeInfo t322_TI;
struct t29;
void* RuntimeInvoker_t322_t29 (MethodInfo* method, void* obj, void** args){
	typedef t322  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t322  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t322_TI, &ret);
}

struct t29;
extern TypeInfo t2420_TI;
struct t29;
void* RuntimeInvoker_t2420_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2420  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t2420  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t2420_TI, &ret);
}

struct t29;
extern TypeInfo t2444_TI;
struct t29;
void* RuntimeInvoker_t2444_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2444  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t2444  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t2444_TI, &ret);
}

struct t29;
extern TypeInfo t2465_TI;
struct t29;
void* RuntimeInvoker_t2465_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2465  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t2465  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t2465_TI, &ret);
}

struct t29;
extern TypeInfo t2458_TI;
struct t29;
void* RuntimeInvoker_t2458_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2458  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t2458  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t2458_TI, &ret);
}

struct t29;
extern TypeInfo t2569_TI;
struct t29;
void* RuntimeInvoker_t2569_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2569  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t2569  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t2569_TI, &ret);
}

struct t29;
extern TypeInfo t2562_TI;
struct t29;
void* RuntimeInvoker_t2562_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2562  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t2562  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t2562_TI, &ret);
}

struct t29;
extern TypeInfo t2859_TI;
struct t29;
void* RuntimeInvoker_t2859_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2859  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t2859  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t2859_TI, &ret);
}

struct t29;
extern TypeInfo t2893_TI;
struct t29;
void* RuntimeInvoker_t2893_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2893  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t2893  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t2893_TI, &ret);
}

struct t29;
extern TypeInfo t2911_TI;
struct t29;
void* RuntimeInvoker_t2911_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2911  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t2911  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t2911_TI, &ret);
}

struct t29;
extern TypeInfo t3106_TI;
struct t29;
void* RuntimeInvoker_t3106_t29 (MethodInfo* method, void* obj, void** args){
	typedef t3106  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t3106  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t3106_TI, &ret);
}

struct t29;
extern TypeInfo t3192_TI;
struct t29;
void* RuntimeInvoker_t3192_t29 (MethodInfo* method, void* obj, void** args){
	typedef t3192  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t3192  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t3192_TI, &ret);
}

struct t29;
extern TypeInfo t35_TI;
struct t29;
void* RuntimeInvoker_t35_t29 (MethodInfo* method, void* obj, void** args){
	typedef t35 (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t35 ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t35_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t35 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t35 p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t35*)args[0]), method);
	return ret;
}

struct t29;
extern TypeInfo t1131_TI;
struct t29;
void* RuntimeInvoker_t1131_t29 (MethodInfo* method, void* obj, void** args){
	typedef t1131  (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t1131  ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return Box(&t1131_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t348_t348 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t626_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t372_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t372_t372 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t297_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t626_t372 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t626_t626 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t372_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t372 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t44_t372 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t372_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t626 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint16_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t22_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, float p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t348_TI;
void* RuntimeInvoker_t348_t372_t372 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(&t348_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t626_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t919_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t919 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, int8_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t372_t372 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, float p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, float p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t626_TI;
void* RuntimeInvoker_t626_t626_t626 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(&t626_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t957_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t348_TI;
void* RuntimeInvoker_t348_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(&t348_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t919_t919 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t326_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, bool* p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t132_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t132  p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((t132 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t372_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t297_t816 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int8_t p1, t816  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((t816 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t601 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, double p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t326_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, bool* p1, int8_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t961_t372 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, uint16_t* p1, int16_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint16_t*)args[0], *((int16_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, float p1, float p2, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t589 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t164 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t164 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t1756_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t1126 * p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t1126 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t344_TI;
void* RuntimeInvoker_t344_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t292;
#include "t292.h"
void* RuntimeInvoker_t21_t2065_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t292 ** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t292 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t44_t2022 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1;
#include "t1.h"
void* RuntimeInvoker_t21_t44_t3941 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t4;
#include "t4.h"
void* RuntimeInvoker_t21_t44_t3943 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t4 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t4 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t317;
#include "t317.h"
void* RuntimeInvoker_t21_t44_t3945 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t317 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t317 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t28;
#include "t28.h"
void* RuntimeInvoker_t21_t44_t3947 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t28 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t28 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t41;
#include "t41.h"
void* RuntimeInvoker_t21_t44_t3948 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t41 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t41 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t5;
#include "t5.h"
void* RuntimeInvoker_t21_t44_t3950 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t5 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t5 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t30;
void* RuntimeInvoker_t21_t44_t3952 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t31;
void* RuntimeInvoker_t21_t44_t3954 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t32;
void* RuntimeInvoker_t21_t44_t3955 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t8;
#include "t8.h"
void* RuntimeInvoker_t21_t44_t3957 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t8 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t8 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t10;
#include "t10.h"
void* RuntimeInvoker_t21_t44_t3959 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t10 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t10 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t11;
#include "t11.h"
void* RuntimeInvoker_t21_t44_t3961 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t11 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t11 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t12;
#include "t12.h"
void* RuntimeInvoker_t21_t44_t3963 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t12 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t12 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t13;
#include "t13.h"
void* RuntimeInvoker_t21_t44_t3965 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t13 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t13 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t14;
#include "t14.h"
void* RuntimeInvoker_t21_t44_t3967 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t14 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t14 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t18;
#include "t18.h"
void* RuntimeInvoker_t21_t44_t3969 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t18 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t18 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t19;
#include "t19.h"
void* RuntimeInvoker_t21_t44_t3971 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t19 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t19 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t3973 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t49;
#include "t49.h"
void* RuntimeInvoker_t21_t44_t3975 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t49 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t49 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t288;
void* RuntimeInvoker_t21_t44_t3977 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t289;
void* RuntimeInvoker_t21_t44_t3979 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t290;
void* RuntimeInvoker_t21_t44_t3981 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t110;
#include "t110.h"
void* RuntimeInvoker_t21_t44_t3983 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t110 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t110 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t50;
#include "t50.h"
void* RuntimeInvoker_t21_t44_t3985 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t50 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t50 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t55;
#include "t55.h"
void* RuntimeInvoker_t21_t44_t3987 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t55 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t55 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t316;
#include "mscorlib_ArrayTypes.h"
void* RuntimeInvoker_t21_t1704_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t316** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t316**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t42;
#include "t42.h"
void* RuntimeInvoker_t21_t44_t3989 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t42 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t42 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1913;
void* RuntimeInvoker_t21_t44_t3990 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1914;
void* RuntimeInvoker_t21_t44_t3992 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t296;
#include "t296.h"
void* RuntimeInvoker_t21_t44_t3994 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t296 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t296 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1657;
void* RuntimeInvoker_t21_t44_t3996 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1915;
void* RuntimeInvoker_t21_t44_t3998 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t390 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1706;
void* RuntimeInvoker_t21_t44_t4001 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1707;
void* RuntimeInvoker_t21_t44_t4003 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1753 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, double* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (double*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1754;
void* RuntimeInvoker_t21_t44_t4006 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1755;
void* RuntimeInvoker_t21_t44_t4008 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t961 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint16_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint16_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1739;
void* RuntimeInvoker_t21_t44_t4010 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1740;
void* RuntimeInvoker_t21_t44_t4012 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t52;
#include "t52.h"
void* RuntimeInvoker_t21_t44_t4015 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t52 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t52 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2172;
#include "UnityEngine.UI_ArrayTypes.h"
void* RuntimeInvoker_t21_t4016_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2172** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2172**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2214;
void* RuntimeInvoker_t21_t4019_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2214** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2214**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t105;
#include "t105.h"
void* RuntimeInvoker_t21_t44_t4022 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t105 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t105 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t603;
void* RuntimeInvoker_t21_t44_t4023 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t674;
void* RuntimeInvoker_t21_t44_t4025 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t868;
void* RuntimeInvoker_t21_t44_t4027 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2231;
void* RuntimeInvoker_t21_t4029_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2231** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2231**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2238;
#include "UnityEngine_ArrayTypes.h"
void* RuntimeInvoker_t21_t4030_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2238** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2238**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4033 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t57 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t57 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2251;
void* RuntimeInvoker_t21_t4034_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2251** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2251**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t109;
#include "t109.h"
void* RuntimeInvoker_t21_t44_t4037 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t109 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t109 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2265;
void* RuntimeInvoker_t21_t4038_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2265** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2265**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t62;
#include "t62.h"
void* RuntimeInvoker_t21_t44_t4041 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t62 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t62 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t89;
void* RuntimeInvoker_t21_t44_t4043 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t90;
void* RuntimeInvoker_t21_t44_t4045 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t91;
void* RuntimeInvoker_t21_t44_t4047 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t93;
void* RuntimeInvoker_t21_t44_t4049 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t92;
void* RuntimeInvoker_t21_t44_t4051 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t94;
void* RuntimeInvoker_t21_t44_t4053 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t95;
void* RuntimeInvoker_t21_t44_t4055 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t96;
void* RuntimeInvoker_t21_t44_t4057 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t97;
void* RuntimeInvoker_t21_t44_t4059 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t98;
void* RuntimeInvoker_t21_t44_t4061 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t99;
void* RuntimeInvoker_t21_t44_t4063 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t100;
void* RuntimeInvoker_t21_t44_t4065 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t101;
void* RuntimeInvoker_t21_t44_t4067 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t102;
void* RuntimeInvoker_t21_t44_t4069 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t103;
void* RuntimeInvoker_t21_t44_t4071 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t60;
#include "t60.h"
void* RuntimeInvoker_t21_t44_t4073 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t60 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t60 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2299;
void* RuntimeInvoker_t21_t4074_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2299** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2299**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4077 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t25;
#include "t25.h"
void* RuntimeInvoker_t21_t44_t4079 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t25 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t25 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2317;
void* RuntimeInvoker_t21_t4080_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2317** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2317**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4083 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4085 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4087 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t117;
#include "t117.h"
void* RuntimeInvoker_t21_t44_t4089 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t117 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t117 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t7;
void* RuntimeInvoker_t21_t44_t639 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t7** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t7**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t373;
void* RuntimeInvoker_t21_t44_t4091 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1745;
void* RuntimeInvoker_t21_t44_t4093 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1746;
void* RuntimeInvoker_t21_t44_t4095 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4097 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t322 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t322 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4098 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1248 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1248 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t6;
#include "t6.h"
void* RuntimeInvoker_t21_t44_t325 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t6 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t6 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t53;
#include "t53.h"
void* RuntimeInvoker_t21_t44_t4100 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t53 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t53 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4102 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t725 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t725 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t113;
#include "t113.h"
void* RuntimeInvoker_t21_t44_t4104 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t113 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t113 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2366;
void* RuntimeInvoker_t21_t4105_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2366** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2366**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t121;
#include "t121.h"
void* RuntimeInvoker_t21_t44_t4108 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t121 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t121 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4110 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t122;
#include "t122.h"
void* RuntimeInvoker_t21_t44_t4112 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t122 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t122 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t123;
#include "t123.h"
void* RuntimeInvoker_t21_t44_t4114 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t123 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t123 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t124;
#include "t124.h"
void* RuntimeInvoker_t21_t44_t4116 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t124 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t124 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t625 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t330 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t330 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t624 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t127 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t127 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4120 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t9;
#include "t9.h"
void* RuntimeInvoker_t21_t44_t4122 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t9 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t9 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t139;
#include "t139.h"
void* RuntimeInvoker_t21_t44_t4124 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t139 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t139 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4125 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4127 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2420 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t2420 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t145;
void* RuntimeInvoker_t21_t44_t4128 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2413;
void* RuntimeInvoker_t21_t4129_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2413** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2413**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4132 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2444 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t2444 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4133 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2465 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t2465 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t15;
#include "t15.h"
void* RuntimeInvoker_t21_t44_t4134 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t15 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t15 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t271;
void* RuntimeInvoker_t21_t44_t417 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t182;
#include "t182.h"
void* RuntimeInvoker_t21_t44_t4136 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t182 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t182 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t369;
void* RuntimeInvoker_t21_t44_t4138 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t155;
#include "t155.h"
void* RuntimeInvoker_t21_t44_t4140 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t155 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t155 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2473;
void* RuntimeInvoker_t21_t4141_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2473** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2473**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4144 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2458 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t2458 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t148;
#include "t148.h"
void* RuntimeInvoker_t21_t44_t4145 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t148 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t148 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t350;
#include "t350.h"
void* RuntimeInvoker_t21_t44_t4147 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t350 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t350 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4148 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t184 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t184 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t201;
void* RuntimeInvoker_t21_t4149_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t201** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t201**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t163;
#include "t163.h"
void* RuntimeInvoker_t21_t44_t4152 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t163 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t163 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2509;
void* RuntimeInvoker_t21_t4153_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2509** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2509**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t3;
#include "t3.h"
void* RuntimeInvoker_t21_t44_t4154 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t3 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t3 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2526;
void* RuntimeInvoker_t21_t4155_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2526** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2526**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t166;
#include "t166.h"
void* RuntimeInvoker_t21_t44_t4158 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t166 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t166 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2545;
void* RuntimeInvoker_t21_t4160_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2545** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2545**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4163 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4165 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2569 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t2569 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4166 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2562 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t2562 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t366;
#include "t366.h"
void* RuntimeInvoker_t21_t44_t4167 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t366 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t366 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t179;
#include "t179.h"
void* RuntimeInvoker_t21_t44_t4168 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t179 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t179 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t357;
void* RuntimeInvoker_t21_t44_t4170 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t301;
void* RuntimeInvoker_t21_t44_t4172 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t594 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t17 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t17 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4175 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4176 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4180 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4182 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4184 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4186 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4188 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t197;
#include "t197.h"
void* RuntimeInvoker_t21_t44_t4190 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t197 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t197 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4195 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4201 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t380 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t380 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4202 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t381 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t381 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4198 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4200 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4196 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4206 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4208 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t211;
#include "t211.h"
void* RuntimeInvoker_t21_t44_t4210 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t211 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t211 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t217;
#include "t217.h"
void* RuntimeInvoker_t21_t44_t4212 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t217 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t217 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4215 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4217 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t222;
#include "t222.h"
void* RuntimeInvoker_t21_t44_t4219 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t222 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t222 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t587 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t23 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t23 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4222 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2638;
void* RuntimeInvoker_t21_t4224_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2638** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2638**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t352;
#include "t352.h"
void* RuntimeInvoker_t21_t44_t4232 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t352 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t352 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2652;
void* RuntimeInvoker_t21_t4233_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2652** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2652**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4228 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4237 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t234;
#include "t234.h"
void* RuntimeInvoker_t21_t44_t4239 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t234 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t234 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4241 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4243 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t235;
#include "t235.h"
void* RuntimeInvoker_t21_t44_t4245 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t235 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t235 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2675;
void* RuntimeInvoker_t21_t4246_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2675** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2675**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t39;
#include "t39.h"
void* RuntimeInvoker_t21_t44_t4249 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t39 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t39 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4250 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t242;
#include "t242.h"
void* RuntimeInvoker_t21_t44_t4252 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t242 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t242 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2704;
void* RuntimeInvoker_t21_t4254_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2704** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2704**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t248;
#include "t248.h"
void* RuntimeInvoker_t21_t44_t4257 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t248 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t248 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t409;
void* RuntimeInvoker_t21_t44_t4259 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t410;
void* RuntimeInvoker_t21_t44_t4261 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4263 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t252;
#include "t252.h"
void* RuntimeInvoker_t21_t44_t4265 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t252 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t252 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4267 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4269 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4271 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t254;
#include "t254.h"
void* RuntimeInvoker_t21_t44_t4273 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t254 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t254 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4275 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t258;
#include "t258.h"
void* RuntimeInvoker_t21_t44_t4277 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t258 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t258 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t259;
#include "t259.h"
void* RuntimeInvoker_t21_t44_t4279 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t259 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t259 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t411;
void* RuntimeInvoker_t21_t44_t4281 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t4283_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t4284_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t4285_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t390_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2;
#include "t2.h"
void* RuntimeInvoker_t21_t44_t4214 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t2 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2743;
void* RuntimeInvoker_t21_t4286_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2743** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2743**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4283 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4284 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4285 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t260;
#include "t260.h"
void* RuntimeInvoker_t21_t44_t4292 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t260 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t260 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t261;
#include "t261.h"
void* RuntimeInvoker_t21_t44_t4294 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t261 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t261 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t262;
#include "t262.h"
void* RuntimeInvoker_t21_t44_t4296 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t262 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t262 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t412;
void* RuntimeInvoker_t21_t44_t4298 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t4301_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t272;
#include "t272.h"
void* RuntimeInvoker_t21_t44_t4302 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t272 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t272 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t273;
#include "t273.h"
void* RuntimeInvoker_t21_t44_t4304 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t273 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t273 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t355;
void* RuntimeInvoker_t21_t44_t4306 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t370;
void* RuntimeInvoker_t21_t44_t4308 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t354;
void* RuntimeInvoker_t21_t44_t4310 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t278;
#include "t278.h"
void* RuntimeInvoker_t21_t44_t4312 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t278 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t278 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2789;
void* RuntimeInvoker_t21_t4313_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2789** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2789**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t268;
#include "t268.h"
void* RuntimeInvoker_t21_t44_t4314 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t268 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t268 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2794;
void* RuntimeInvoker_t21_t4315_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2794** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2794**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t283;
#include "t283.h"
void* RuntimeInvoker_t21_t44_t4316 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t283 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t283 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t356;
void* RuntimeInvoker_t21_t44_t4318 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t284;
#include "t284.h"
void* RuntimeInvoker_t21_t44_t4320 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t284 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t284 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t285;
#include "t285.h"
void* RuntimeInvoker_t21_t44_t4322 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t285 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t285 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t286;
#include "t286.h"
void* RuntimeInvoker_t21_t44_t4324 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t286 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t286 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t442;
#include "t442.h"
void* RuntimeInvoker_t21_t44_t4326 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t442 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t442 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4328 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4330 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4332 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t450;
#include "t450.h"
void* RuntimeInvoker_t21_t44_t4334 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t450 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t450 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t334;
#include "t334.h"
void* RuntimeInvoker_t21_t44_t4336 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t334 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t334 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t162;
#include "t162.h"
void* RuntimeInvoker_t21_t44_t4338 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t162 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t162 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t157;
#include "t157.h"
void* RuntimeInvoker_t21_t44_t4340 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t157 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t157 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t453;
#include "t453.h"
void* RuntimeInvoker_t21_t44_t4342 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t453 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t453 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t454;
#include "t454.h"
void* RuntimeInvoker_t21_t44_t4344 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t454 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t454 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t455;
#include "t455.h"
void* RuntimeInvoker_t21_t44_t4346 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t455 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t455 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t456;
#include "t456.h"
void* RuntimeInvoker_t21_t44_t4348 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t456 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t456 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t468;
#include "t468.h"
void* RuntimeInvoker_t21_t44_t4350 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t468 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t468 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4352 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2859 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t2859 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t469;
#include "t469.h"
void* RuntimeInvoker_t21_t44_t4353 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t469 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t469 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t474;
#include "t474.h"
void* RuntimeInvoker_t21_t44_t4354 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t474 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t474 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2871;
void* RuntimeInvoker_t21_t4355_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2871** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2871**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4358 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t463;
#include "t463.h"
void* RuntimeInvoker_t21_t44_t4360 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t463 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t463 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t466;
#include "t466.h"
void* RuntimeInvoker_t21_t44_t4362 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t466 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t466 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4363 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2893 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t2893 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4364 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4199 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4367 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2911 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t2911 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4368 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4370 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4372 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4374 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4376 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4378 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t489;
#include "t489.h"
void* RuntimeInvoker_t21_t44_t4380 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t489 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t489 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t490;
#include "t490.h"
void* RuntimeInvoker_t21_t44_t4382 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t490 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t490 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t604;
void* RuntimeInvoker_t21_t44_t4384 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t300;
#include "t300.h"
void* RuntimeInvoker_t21_t44_t4386 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t300 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t300 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t491;
#include "t491.h"
void* RuntimeInvoker_t21_t44_t4388 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t491 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t491 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t156;
#include "t156.h"
void* RuntimeInvoker_t21_t44_t4390 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t156 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t156 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t180;
#include "t180.h"
void* RuntimeInvoker_t21_t44_t4174 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t180 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t180 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t331;
#include "t331.h"
void* RuntimeInvoker_t21_t44_t4393 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t331 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t331 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t24;
#include "t24.h"
void* RuntimeInvoker_t21_t44_t4395 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t24 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t24 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t500;
#include "t500.h"
void* RuntimeInvoker_t21_t44_t4397 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t500 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t500 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4399 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t35* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t35*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t374;
void* RuntimeInvoker_t21_t44_t4401 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4403 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4405 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4407 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t16;
#include "t16.h"
void* RuntimeInvoker_t21_t44_t4409 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t16 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t16 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t336;
#include "t336.h"
void* RuntimeInvoker_t21_t44_t4411 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t336 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t336 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t507;
#include "t507.h"
void* RuntimeInvoker_t21_t44_t4413 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t507 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t507 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2980;
void* RuntimeInvoker_t21_t4414_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2980** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t2980**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t332;
#include "t332.h"
void* RuntimeInvoker_t21_t44_t4417 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t332 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t332 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t511;
#include "t511.h"
void* RuntimeInvoker_t21_t44_t4419 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t511 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t511 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t600 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, float* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (float*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1751;
void* RuntimeInvoker_t21_t44_t4422 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1752;
void* RuntimeInvoker_t21_t44_t4424 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4426 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4428 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t517 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t517 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t229;
#include "t229.h"
void* RuntimeInvoker_t21_t44_t4430 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t229 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t229 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t398;
#include "t398.h"
void* RuntimeInvoker_t21_t44_t4432 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t398 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t398 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t525;
#include "t525.h"
void* RuntimeInvoker_t21_t44_t4434 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t525 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t525 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4301 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4437 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4439 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t530;
void* RuntimeInvoker_t21_t4441_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t530** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t530**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t531;
void* RuntimeInvoker_t21_t4444_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t531** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t531**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4447 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t159;
#include "t159.h"
void* RuntimeInvoker_t21_t44_t4449 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t159 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t159 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t532;
#include "t532.h"
void* RuntimeInvoker_t21_t44_t4451 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t532 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t532 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t359;
#include "t359.h"
void* RuntimeInvoker_t21_t44_t4453 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t359 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t359 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t360;
#include "t360.h"
void* RuntimeInvoker_t21_t44_t4455 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t360 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t360 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t318;
#include "t318.h"
void* RuntimeInvoker_t21_t44_t4457 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t318 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t318 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t537;
void* RuntimeInvoker_t21_t4459_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t537** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t537**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t298;
#include "t298.h"
void* RuntimeInvoker_t21_t44_t4462 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t298 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t298 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4464 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t632 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t632 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t539;
#include "t539.h"
void* RuntimeInvoker_t21_t44_t4466 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t539 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t539 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t540;
#include "t540.h"
void* RuntimeInvoker_t21_t44_t4468 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t540 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t540 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4470 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4472 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t542 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t542 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t545;
#include "t545.h"
void* RuntimeInvoker_t21_t44_t4474 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t545 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t545 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t399;
#include "t399.h"
void* RuntimeInvoker_t21_t44_t4476 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t399 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t399 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t394;
#include "t394.h"
void* RuntimeInvoker_t21_t44_t4478 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t394 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t394 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t349;
#include "t349.h"
void* RuntimeInvoker_t21_t44_t4480 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t349 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t349 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t405;
#include "t405.h"
void* RuntimeInvoker_t21_t44_t4482 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t405 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t405 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t397;
#include "t397.h"
void* RuntimeInvoker_t21_t44_t4484 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t397 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t397 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t637;
#include "t637.h"
void* RuntimeInvoker_t21_t44_t4486 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t637 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t637 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2037;
void* RuntimeInvoker_t21_t44_t4488 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t549;
#include "t549.h"
void* RuntimeInvoker_t21_t44_t4490 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t549 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t549 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t550;
#include "t550.h"
void* RuntimeInvoker_t21_t44_t4492 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t550 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t550 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4494 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t3106 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t3106 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t204;
#include "t204.h"
void* RuntimeInvoker_t21_t44_t4495 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t204 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t204 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4497 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4498 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4500 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4502 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t565;
#include "t565.h"
void* RuntimeInvoker_t21_t44_t4504 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t565 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t565 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t3132;
void* RuntimeInvoker_t21_t4505_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t3132** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t3132**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t556;
#include "t556.h"
void* RuntimeInvoker_t21_t44_t4508 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t556 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t556 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t3146;
void* RuntimeInvoker_t21_t4509_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t3146** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t3146**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t326 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, bool* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (bool*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1759;
void* RuntimeInvoker_t21_t44_t4512 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1760;
void* RuntimeInvoker_t21_t44_t4514 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t575;
#include "t575.h"
void* RuntimeInvoker_t21_t44_t4516 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t575 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t575 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t576;
#include "t576.h"
void* RuntimeInvoker_t21_t44_t4518 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t576 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t576 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t577;
#include "t577.h"
void* RuntimeInvoker_t21_t44_t4520 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t577 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t577 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t299;
#include "t299.h"
void* RuntimeInvoker_t21_t44_t4522 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t299 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t299 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4524 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t579;
#include "t579.h"
void* RuntimeInvoker_t21_t44_t4526 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t579 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t579 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t686;
#include "t686.h"
void* RuntimeInvoker_t21_t44_t4528 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t686 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t686 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t715;
#include "t715.h"
void* RuntimeInvoker_t21_t44_t4530 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t715 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t715 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t606;
#include "t606.h"
void* RuntimeInvoker_t21_t44_t4532 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t606 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t606 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4534 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t739;
#include "t739.h"
void* RuntimeInvoker_t21_t44_t4536 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t739 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t739 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4538 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4540 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4542 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4544 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1729 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint16_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint16_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1730;
void* RuntimeInvoker_t21_t44_t4547 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1731;
void* RuntimeInvoker_t21_t44_t4549 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4551 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4553 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t3192 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t3192 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4554 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1091 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1721;
void* RuntimeInvoker_t21_t44_t4557 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1722;
void* RuntimeInvoker_t21_t44_t4559 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4561 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4563 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4565 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t745;
#include "t745.h"
void* RuntimeInvoker_t21_t44_t4567 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t745 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t745 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t918;
void* RuntimeInvoker_t21_t44_t4569 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4571 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t805 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t805 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4573 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4575 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4577 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4579 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4581 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4583 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4585 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4587 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4589 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t830;
#include "t830.h"
void* RuntimeInvoker_t21_t44_t4591 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t830 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t830 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t833;
#include "t833.h"
void* RuntimeInvoker_t21_t44_t4593 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t833 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t833 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t957 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4596 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint16_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint16_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4598 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint16_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint16_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4600 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint16_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint16_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4602 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint16_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint16_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4604 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t859 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t859 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4606 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4608 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t895 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t895 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4610 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4612 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4614 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1715 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1716;
void* RuntimeInvoker_t21_t44_t4617 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1717;
void* RuntimeInvoker_t21_t44_t4619 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t972;
#include "t972.h"
void* RuntimeInvoker_t21_t44_t4621 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t972 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t972 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4623 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4625 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1100;
#include "t1100.h"
void* RuntimeInvoker_t21_t44_t4627 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1100 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1100 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4629 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4631 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4633 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4635 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4637 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4639 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t781;
void* RuntimeInvoker_t21_t44_t1092 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t781** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t781**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4642 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4644 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4646 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4648 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4650 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4652 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4654 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4656 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1128;
#include "t1128.h"
void* RuntimeInvoker_t21_t44_t4658 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1128 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1128 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t629;
#include "t629.h"
void* RuntimeInvoker_t21_t44_t4660 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t629 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t629 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t437;
#include "t437.h"
void* RuntimeInvoker_t21_t44_t4662 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t437 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t437 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1712 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int64_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int64_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1713;
void* RuntimeInvoker_t21_t44_t4665 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1714;
void* RuntimeInvoker_t21_t44_t4667 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t707;
#include "t707.h"
void* RuntimeInvoker_t21_t44_t4669 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t707 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t707 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1718 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint64_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint64_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1719;
void* RuntimeInvoker_t21_t44_t4672 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1720;
void* RuntimeInvoker_t21_t44_t4674 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1723 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1724;
void* RuntimeInvoker_t21_t44_t4677 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1725;
void* RuntimeInvoker_t21_t44_t4679 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1726 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int16_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int16_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1727;
void* RuntimeInvoker_t21_t44_t4682 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1728;
void* RuntimeInvoker_t21_t44_t4684 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t446;
void* RuntimeInvoker_t21_t2102_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t446** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t446**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4686 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1131 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1131 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t353;
#include "t353.h"
void* RuntimeInvoker_t21_t44_t4688 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t353 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t353 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t291;
#include "t291.h"
void* RuntimeInvoker_t21_t44_t4690 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t291 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t291 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t557;
#include "t557.h"
void* RuntimeInvoker_t21_t44_t4692 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t557 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t557 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2015;
void* RuntimeInvoker_t21_t44_t4694 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t636;
#include "t636.h"
void* RuntimeInvoker_t21_t44_t2002 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t636 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t636 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2010;
void* RuntimeInvoker_t21_t44_t4697 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t660;
#include "t660.h"
void* RuntimeInvoker_t21_t44_t4699 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t660 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t660 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2009;
void* RuntimeInvoker_t21_t44_t4701 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1660;
#include "t1660.h"
void* RuntimeInvoker_t21_t44_t4703 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1660 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1660 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t391;
#include "t391.h"
void* RuntimeInvoker_t21_t44_t4705 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t391 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t391 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1151;
#include "t1151.h"
void* RuntimeInvoker_t21_t44_t4707 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1151 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1151 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t327;
#include "t327.h"
void* RuntimeInvoker_t21_t44_t4709 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t327 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t327 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1152;
#include "t1152.h"
void* RuntimeInvoker_t21_t44_t4711 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1152 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1152 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1155;
#include "t1155.h"
void* RuntimeInvoker_t21_t44_t4713 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1155 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1155 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1157;
#include "t1157.h"
void* RuntimeInvoker_t21_t44_t4715 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1157 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1157 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t436;
#include "t436.h"
void* RuntimeInvoker_t21_t44_t4717 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t436 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t436 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1158;
#include "t1158.h"
void* RuntimeInvoker_t21_t44_t4719 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1158 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1158 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1159;
#include "t1159.h"
void* RuntimeInvoker_t21_t44_t4721 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1159 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1159 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t38;
#include "t38.h"
void* RuntimeInvoker_t21_t44_t4723 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t38 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t38 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t685;
#include "t685.h"
void* RuntimeInvoker_t21_t44_t4725 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t685 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t685 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t46;
#include "t46.h"
void* RuntimeInvoker_t21_t44_t4727 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t46 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t46 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t342;
#include "t342.h"
void* RuntimeInvoker_t21_t44_t4729 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t342 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t342 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t425;
#include "t425.h"
void* RuntimeInvoker_t21_t44_t4731 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t425 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t425 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1160;
#include "t1160.h"
void* RuntimeInvoker_t21_t44_t4733 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1160 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1160 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1161;
#include "t1161.h"
void* RuntimeInvoker_t21_t44_t4735 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1161 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1161 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1168;
#include "t1168.h"
void* RuntimeInvoker_t21_t44_t4737 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1168 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1168 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1169;
#include "t1169.h"
void* RuntimeInvoker_t21_t44_t4739 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1169 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1169 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4741 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1173 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1173 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1176;
#include "t1176.h"
void* RuntimeInvoker_t21_t44_t4743 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1176 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1176 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1177;
#include "t1177.h"
void* RuntimeInvoker_t21_t44_t1922 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1177 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1177 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1179;
#include "t1179.h"
void* RuntimeInvoker_t21_t44_t4746 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1179 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1179 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4748 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4750 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1196;
#include "t1196.h"
void* RuntimeInvoker_t21_t44_t4752 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1196 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1196 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4754 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4756 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1272 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1272 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4758 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4760 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1279 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1279 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4762 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4764 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4766 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t708;
#include "t708.h"
void* RuntimeInvoker_t21_t44_t4768 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t708 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t708 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4770 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1287;
#include "t1287.h"
void* RuntimeInvoker_t21_t44_t4772 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1287 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1287 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1288;
#include "t1288.h"
void* RuntimeInvoker_t21_t44_t4774 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1288 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1288 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1289;
#include "t1289.h"
void* RuntimeInvoker_t21_t44_t4776 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1289 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1289 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t635;
#include "t635.h"
void* RuntimeInvoker_t21_t44_t4778 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t635 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t635 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4780 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1291;
#include "t1291.h"
void* RuntimeInvoker_t21_t44_t4782 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1291 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1291 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4784 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4786 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4788 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4790 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4792 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4794 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4796 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4798 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4800 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4802 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t2004 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4805 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1351;
#include "t1351.h"
void* RuntimeInvoker_t21_t44_t4807 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1351 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1351 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2016;
void* RuntimeInvoker_t21_t44_t4809 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1142;
#include "t1142.h"
void* RuntimeInvoker_t21_t44_t2020 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1142 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1142 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2017;
void* RuntimeInvoker_t21_t44_t4812 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1352;
#include "t1352.h"
void* RuntimeInvoker_t21_t44_t4814 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1352 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1352 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2018;
void* RuntimeInvoker_t21_t44_t4816 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1348;
#include "t1348.h"
void* RuntimeInvoker_t21_t44_t4818 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1348 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1348 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1349;
#include "t1349.h"
void* RuntimeInvoker_t21_t44_t4820 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1349 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1349 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2014;
void* RuntimeInvoker_t21_t44_t4822 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1339;
#include "t1339.h"
void* RuntimeInvoker_t21_t44_t4824 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1339 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1339 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2008;
void* RuntimeInvoker_t21_t44_t4826 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1345;
#include "t1345.h"
void* RuntimeInvoker_t21_t44_t4828 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1345 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1345 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2012;
void* RuntimeInvoker_t21_t44_t4830 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1144;
#include "t1144.h"
void* RuntimeInvoker_t21_t44_t4832 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1144 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1144 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2013;
void* RuntimeInvoker_t21_t44_t4834 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t432;
#include "t432.h"
void* RuntimeInvoker_t21_t44_t4836 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t432 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t432 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t431;
#include "t431.h"
void* RuntimeInvoker_t21_t44_t4838 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t431 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t431 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t434;
#include "t434.h"
void* RuntimeInvoker_t21_t44_t4840 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t434 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t434 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t712;
#include "t712.h"
void* RuntimeInvoker_t21_t44_t4842 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t712 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t712 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t711;
#include "t711.h"
void* RuntimeInvoker_t21_t44_t4844 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t711 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t711 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t430;
#include "t430.h"
void* RuntimeInvoker_t21_t44_t4846 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t430 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t430 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t435;
#include "t435.h"
void* RuntimeInvoker_t21_t44_t4848 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t435 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t435 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t705;
#include "t705.h"
void* RuntimeInvoker_t21_t44_t4850 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t705 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t705 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t710;
#include "t710.h"
void* RuntimeInvoker_t21_t44_t4852 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t710 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t710 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4854 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t433;
#include "t433.h"
void* RuntimeInvoker_t21_t44_t4856 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t433 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t433 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t429;
#include "t429.h"
void* RuntimeInvoker_t21_t44_t4858 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t429 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t429 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t438;
#include "t438.h"
void* RuntimeInvoker_t21_t44_t4860 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t438 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t438 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1146;
#include "t1146.h"
void* RuntimeInvoker_t21_t44_t4862 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1146 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1146 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2036;
void* RuntimeInvoker_t21_t44_t4864 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4866 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4868 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4870 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4872 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4874 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4876 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4878 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4880 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4882 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4884 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4886 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4888 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t706;
#include "t706.h"
void* RuntimeInvoker_t21_t44_t4890 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t706 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t706 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t704;
#include "t704.h"
void* RuntimeInvoker_t21_t44_t4892 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t704 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t704 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4894 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t709;
#include "t709.h"
void* RuntimeInvoker_t21_t44_t4896 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t709 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t709 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1393;
#include "t1393.h"
void* RuntimeInvoker_t21_t44_t4898 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1393 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1393 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4900 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1396;
#include "t1396.h"
void* RuntimeInvoker_t21_t44_t4902 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1396 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1396 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4904 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4906 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1400;
#include "t1400.h"
void* RuntimeInvoker_t21_t44_t4908 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1400 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1400 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4910 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4912 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1402;
#include "t1402.h"
void* RuntimeInvoker_t21_t44_t4914 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1402 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1402 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4916 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1404;
#include "t1404.h"
void* RuntimeInvoker_t21_t44_t4918 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1404 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1404 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4920 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1406;
#include "t1406.h"
void* RuntimeInvoker_t21_t44_t4922 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1406 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1406 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4924 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1409;
#include "t1409.h"
void* RuntimeInvoker_t21_t44_t4926 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1409 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1409 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1412;
#include "t1412.h"
void* RuntimeInvoker_t21_t44_t4928 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1412 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1412 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1413;
#include "t1413.h"
void* RuntimeInvoker_t21_t44_t4930 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1413 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1413 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1414;
#include "t1414.h"
void* RuntimeInvoker_t21_t44_t4932 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1414 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1414 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4934 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1422;
#include "t1422.h"
void* RuntimeInvoker_t21_t44_t4936 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1422 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1422 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1423;
#include "t1423.h"
void* RuntimeInvoker_t21_t44_t4938 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1423 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1423 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2040;
void* RuntimeInvoker_t21_t44_t4940 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1433;
void* RuntimeInvoker_t21_t44_t4942 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1434;
#include "t1434.h"
void* RuntimeInvoker_t21_t44_t4944 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1434 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1434 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2044;
void* RuntimeInvoker_t21_t44_t4946 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2045;
void* RuntimeInvoker_t21_t44_t4948 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4950 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1448;
#include "t1448.h"
void* RuntimeInvoker_t21_t44_t4952 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1448 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1448 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1464;
#include "t1464.h"
void* RuntimeInvoker_t21_t44_t4954 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1464 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1464 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2051;
void* RuntimeInvoker_t21_t44_t4956 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4958 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4960 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4962 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4964 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4966 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t2072 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t465 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t465 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2074;
void* RuntimeInvoker_t21_t44_t4969 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2075;
void* RuntimeInvoker_t21_t44_t4971 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1756 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1126 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1126 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1757;
void* RuntimeInvoker_t21_t44_t4974 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1758;
void* RuntimeInvoker_t21_t44_t4976 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4978 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t816 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t816 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2099;
void* RuntimeInvoker_t21_t44_t4980 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2100;
void* RuntimeInvoker_t21_t44_t4982 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29**)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4984 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4986 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4988 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t4990 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1514;
#include "t1514.h"
void* RuntimeInvoker_t21_t44_t4992 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1514 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1514 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1515;
#include "t1515.h"
void* RuntimeInvoker_t21_t44_t4994 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1515 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1515 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1516;
#include "t1516.h"
void* RuntimeInvoker_t21_t44_t4996 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1516 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1516 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1517;
#include "t1517.h"
void* RuntimeInvoker_t21_t44_t4998 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1517 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1517 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5000 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5002 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5004 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5006 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5008 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1567;
#include "t1567.h"
void* RuntimeInvoker_t21_t44_t5010 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1567 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1567 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2063;
void* RuntimeInvoker_t21_t44_t5011 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2064;
void* RuntimeInvoker_t21_t44_t5013 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t3462;
void* RuntimeInvoker_t21_t5015_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t3462** p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t3462**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5018 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t615;
#include "t615.h"
void* RuntimeInvoker_t21_t44_t5020 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t615 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t615 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1572;
#include "t1572.h"
void* RuntimeInvoker_t21_t44_t5022 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1572 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1572 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1573;
#include "t1573.h"
void* RuntimeInvoker_t21_t44_t5024 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1573 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1573 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5026 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5028 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5030 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5032 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5034 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5036 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5038 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5040 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1662;
#include "t1662.h"
void* RuntimeInvoker_t21_t44_t5042 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1662 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1662 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5044 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5046 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5048 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t1671;
#include "t1671.h"
void* RuntimeInvoker_t21_t44_t5050 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1671 ** p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t1671 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5052 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t5054 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, uint8_t* p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (uint8_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t164 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t164  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t164 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t183 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t183  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t183 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, float p1, float p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t542 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t542  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t542 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t390_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t57 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t57  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t57 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1248 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1248  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t1248 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t725 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t725  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t725 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t330 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t330  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t330 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t127 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t127  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t127 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t184 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t184  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t184 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4175_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4176_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t17 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t17  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t17 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4195_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4196_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4198_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4199_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4200_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t380 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t380  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t380 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t381 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t381  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t381 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4215_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t23 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t23  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t23 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4228_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4241_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4263_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4275_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t517 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t517  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t517 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t632 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t632  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t632 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t805 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t805  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t805 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t859 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t859  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t859 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t895 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t895  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t895 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1173 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1173  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t1173 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1272 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1272  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t1272 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1279 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1279  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t1279 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t465 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t465  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t465 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t1126 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t1126  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t1126 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t816 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t816  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t816 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t6;
void* RuntimeInvoker_t40_t44_t325 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, t6 ** p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t6 **)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t469;
void* RuntimeInvoker_t40_t44_t4353 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, t469 ** p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t469 **)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t919_TI;
void* RuntimeInvoker_t919_t919_t44 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t919_t816 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int64_t p1, t816  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((t816 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t919_t919 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t35_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t1089_TI;
void* RuntimeInvoker_t1089_t919_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t600_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, float* p1, float p2, MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t132_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t132  p1, float p2, MethodInfo* method);
	((Func)method->method)(obj, *((t132 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t322 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t322  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t322 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t2420 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2420  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t2420 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t2444 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2444  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t2444 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t2465 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2465  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t2465 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t2458 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2458  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t2458 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t2569 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2569  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t2569 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t2562 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2562  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t2562 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t600_t22 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, float* p1, float p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t2859 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2859  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t2859 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t2893 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2893  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t2893 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t2911 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t2911  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t2911 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t3106 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t3106  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t3106 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t3192 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t3192  p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t3192 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, float p1, int32_t p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t44_t22 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, int32_t p1, float p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1756_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1126 * p1, int32_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t1126 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int8_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t400_t605 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t132 * p1, t492 * p2, MethodInfo* method);
	((Func)method->method)(obj, (t132 *)args[0], (t492 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t390_t390 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t297_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int8_t p1, t29 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (t29 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t35 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t35 p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((t35*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t597_t597 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t396 * p1, t396 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t396 *)args[0], (t396 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t598_t587 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t224 * p1, t23 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t224 *)args[0], (t23 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t132_t605 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t132  p1, t492 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((t132 *)args[0]), (t492 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int8_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t348_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint8_t p1, t29 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (t29 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t348 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, uint8_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t594_t17 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t17 * p1, t17  p2, MethodInfo* method);
	((Func)method->method)(obj, (t17 *)args[0], *((t17 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t400_t132 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t132 * p1, t132  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t132 *)args[0], *((t132 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t396_t597 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t396  p1, t396 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t396 *)args[0]), (t396 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t23_t23 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t23  p1, t23  p2, MethodInfo* method);
	((Func)method->method)(obj, *((t23 *)args[0]), *((t23 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t329_t600 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t329  p1, float* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t329 *)args[0]), (float*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t348_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, uint8_t p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t372 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int16_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t465_t816 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t465  p1, t816  p2, MethodInfo* method);
	((Func)method->method)(obj, *((t465 *)args[0]), *((t816 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4227_t210 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t210 * p1, t210  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t210 *)args[0], *((t210 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4229_t146 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t146 * p1, t146  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t146 *)args[0], *((t146 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t4230_t228 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t228 * p1, t228  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t228 *)args[0], *((t228 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, float p1, float p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
#include "t451.h"
void* RuntimeInvoker_t40_t451_t451 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t451  p1, t451  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t451 *)args[0]), *((t451 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t17_t17 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t17  p1, t17  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t17 *)args[0]), *((t17 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t23_t23 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t23  p1, t23  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t23 *)args[0]), *((t23 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t362_t362 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t362  p1, t362  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t362 *)args[0]), *((t362 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t164_t164 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t164  p1, t164  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t164 *)args[0]), *((t164 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t396_t396 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t396  p1, t396  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t396 *)args[0]), *((t396 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t224_t23 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t224  p1, t23  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t224 *)args[0]), *((t23 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t224_t224 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t224  p1, t224  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t224 *)args[0]), *((t224 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t183_t183 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t183  p1, t183  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t183 *)args[0]), *((t183 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t492_t492 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t492  p1, t492  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t492 *)args[0]), *((t492 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t542_t542 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t542  p1, t542  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t542 *)args[0]), *((t542 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t132_t132 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t132  p1, t132  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t132 *)args[0]), *((t132 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t1126_t1126 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t1126  p1, t1126  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t1126 *)args[0]), *((t1126 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t465_t465 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t465  p1, t465  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t465 *)args[0]), *((t465 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int8_t p1, int8_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t816_t816 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t816  p1, t816  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t816 *)args[0]), *((t816 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t57_t57 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t57  p1, t57  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t57 *)args[0]), *((t57 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t184_t184 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t184  p1, t184  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t184 *)args[0]), *((t184 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t381_t381 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t381  p1, t381  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t381 *)args[0]), *((t381 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t380_t380 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t380  p1, t380  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t380 *)args[0]), *((t380 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t1629_t1629 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t1629  p1, t1629  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t1629 *)args[0]), *((t1629 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t1648_t1648 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t1648  p1, t1648  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t1648 *)args[0]), *((t1648 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t194_TI;
struct t29;
void* RuntimeInvoker_t194_t372_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int16_t p1, t29 * p2, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (t29 *)args[1], method);
	return Box(&t194_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t372_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int16_t p1, int8_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t919 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int64_t p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t601_TI;
void* RuntimeInvoker_t601_t601_t601 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, double p1, double p2, MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(&t601_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, int8_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t372_t372 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int16_t p1, int16_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1756_t1718 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1126 * p1, uint64_t* p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t1126 *)args[0], (uint64_t*)args[1], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1756_t1712 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1126 * p1, int64_t* p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t1126 *)args[0], (int64_t*)args[1], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1756_t1756 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1126 * p1, t1126 * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t1126 *)args[0], (t1126 *)args[1], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t816_TI;
void* RuntimeInvoker_t816_t601_t919 (MethodInfo* method, void* obj, void** args){
	typedef t816  (*Func)(void* obj, double p1, int64_t p2, MethodInfo* method);
	t816  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(&t816_TI, &ret);
}

struct t29;
extern TypeInfo t106_TI;
void* RuntimeInvoker_t106_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, float p1, float p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(&t106_TI, &ret);
}

struct t29;
extern TypeInfo t132_TI;
void* RuntimeInvoker_t132_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef t132  (*Func)(void* obj, float p1, float p2, MethodInfo* method);
	t132  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(&t132_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, float p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((float*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t35_t2026 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, t1380 * p2, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), (t1380 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t57_t57 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t57  p1, t57  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t57 *)args[0]), *((t57 *)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t127_t127 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t127  p1, t127  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t127 *)args[0]), *((t127 *)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t44_t372 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, int16_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t372_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int16_t p1, int32_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1126_t1126 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1126  p1, t1126  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t1126 *)args[0]), *((t1126 *)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t372_TI;
struct t29;
void* RuntimeInvoker_t372_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef int16_t (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t372_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t35_t2004 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t35 p1, int32_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t35*)args[0]), (int32_t*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t465_t465 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t465  p1, t465  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t465 *)args[0]), *((t465 *)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t465_TI;
void* RuntimeInvoker_t465_t465_t44 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, t465  p1, int32_t p2, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, *((t465 *)args[0]), *((int32_t*)args[1]), method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t816_t816 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t816  p1, t816  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t816 *)args[0]), *((t816 *)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t184_t184 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t184  p1, t184  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t184 *)args[0]), *((t184 *)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t381_t381 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t381  p1, t381  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t381 *)args[0]), *((t381 *)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t380_t380 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t380  p1, t380  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t380 *)args[0]), *((t380 *)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1629_t1629 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1629  p1, t1629  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t1629 *)args[0]), *((t1629 *)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1648_t1648 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1648  p1, t1648  p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t1648 *)args[0]), *((t1648 *)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, t29 * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t598_t587 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t224 * p1, t23 * p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (t224 *)args[0], (t23 *)args[1], method);
	return Box(&t22_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, int32_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t594 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t17 * p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t17 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t400 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t132 * p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t132 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t900;
#include "t900.h"
void* RuntimeInvoker_t21_t29_t962 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t900 ** p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t900 **)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t344_TI;
struct t29;
void* RuntimeInvoker_t344_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t316;
struct t29;
void* RuntimeInvoker_t21_t1704_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t316** p1, t29 * p2, MethodInfo* method);
	((Func)method->method)(obj, (t316**)args[0], (t29 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t2025 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t1373 * p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t1373 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t326 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, bool* p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (bool*)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t2084 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t1642 * p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t1642 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t263;
#include "t263.h"
struct t29;
void* RuntimeInvoker_t21_t4300_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t263 ** p1, t29 * p2, MethodInfo* method);
	((Func)method->method)(obj, (t263 **)args[0], (t29 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t2022_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 ** p1, t29 * p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 **)args[0], (t29 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t17 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t17  p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((t17 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t17_TI;
void* RuntimeInvoker_t17_t17_t22 (MethodInfo* method, void* obj, void** args){
	typedef t17  (*Func)(void* obj, t17  p1, float p2, MethodInfo* method);
	t17  ret = ((Func)method->method)(obj, *((t17 *)args[0]), *((float*)args[1]), method);
	return Box(&t17_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t23_t23 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t23  p1, t23  p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((t23 *)args[0]), *((t23 *)args[1]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t23_TI;
void* RuntimeInvoker_t23_t23_t22 (MethodInfo* method, void* obj, void** args){
	typedef t23  (*Func)(void* obj, t23  p1, float p2, MethodInfo* method);
	t23  ret = ((Func)method->method)(obj, *((t23 *)args[0]), *((float*)args[1]), method);
	return Box(&t23_TI, &ret);
}

struct t29;
extern TypeInfo t132_TI;
void* RuntimeInvoker_t132_t132_t22 (MethodInfo* method, void* obj, void** args){
	typedef t132  (*Func)(void* obj, t132  p1, float p2, MethodInfo* method);
	t132  ret = ((Func)method->method)(obj, *((t132 *)args[0]), *((float*)args[1]), method);
	return Box(&t132_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t362_t362 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t362  p1, t362  p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((t362 *)args[0]), *((t362 *)args[1]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t224_t23 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t224  p1, t23  p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((t224 *)args[0]), *((t23 *)args[1]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t183_t183 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t183  p1, t183  p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((t183 *)args[0]), *((t183 *)args[1]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t183_TI;
void* RuntimeInvoker_t183_t183_t22 (MethodInfo* method, void* obj, void** args){
	typedef t183  (*Func)(void* obj, t183  p1, float p2, MethodInfo* method);
	t183  ret = ((Func)method->method)(obj, *((t183 *)args[0]), *((float*)args[1]), method);
	return Box(&t183_TI, &ret);
}

struct t29;
extern TypeInfo t492_TI;
void* RuntimeInvoker_t492_t492_t22 (MethodInfo* method, void* obj, void** args){
	typedef t492  (*Func)(void* obj, t492  p1, float p2, MethodInfo* method);
	t492  ret = ((Func)method->method)(obj, *((t492 *)args[0]), *((float*)args[1]), method);
	return Box(&t492_TI, &ret);
}

struct t29;
extern TypeInfo t492_TI;
void* RuntimeInvoker_t492_t22_t492 (MethodInfo* method, void* obj, void** args){
	typedef t492  (*Func)(void* obj, float p1, t492  p2, MethodInfo* method);
	t492  ret = ((Func)method->method)(obj, *((float*)args[0]), *((t492 *)args[1]), method);
	return Box(&t492_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t735 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t735  p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((t735 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t763;
#include "t763.h"
void* RuntimeInvoker_t40_t29_t924 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t763 ** p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t763 **)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t390 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t765;
#include "t765.h"
void* RuntimeInvoker_t40_t29_t925 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t765 ** p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t765 **)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t1712 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int64_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (int64_t*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t1715 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, uint32_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (uint32_t*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t1718 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, uint64_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (uint64_t*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t1091 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, uint8_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (uint8_t*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t1723 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int8_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (int8_t*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t1726 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int16_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (int16_t*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t1729 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, uint16_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (uint16_t*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t1753 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, double* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (double*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t927 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t927  p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((t927 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t2004 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t465 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t465  p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((t465 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t919_TI;
struct t29;
void* RuntimeInvoker_t919_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t1126 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t1126  p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((t1126 *)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t180;
struct t29;
void* RuntimeInvoker_t40_t4174_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t180 ** p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t180 **)args[0], (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t2022_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 ** p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 **)args[0], (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t15;
struct t29;
void* RuntimeInvoker_t40_t4134_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t15 ** p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t15 **)args[0], (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t155;
struct t29;
void* RuntimeInvoker_t40_t4140_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t155 ** p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t155 **)args[0], (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t189;
#include "t189.h"
struct t29;
void* RuntimeInvoker_t40_t4192_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t189 ** p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t189 **)args[0], (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t191;
#include "t191.h"
struct t29;
void* RuntimeInvoker_t40_t4193_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t191 ** p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t191 **)args[0], (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t193;
#include "t193.h"
struct t29;
void* RuntimeInvoker_t40_t4194_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t193 ** p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t193 **)args[0], (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t2;
struct t29;
void* RuntimeInvoker_t40_t4214_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t2 ** p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t2 **)args[0], (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t137;
#include "t137.h"
struct t29;
void* RuntimeInvoker_t40_t4231_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t137 ** p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t137 **)args[0], (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t350;
void* RuntimeInvoker_t40_t29_t4147 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t350 ** p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t350 **)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t2022 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 ** p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 **)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t366;
void* RuntimeInvoker_t40_t29_t4167 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t366 ** p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t366 **)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t466;
void* RuntimeInvoker_t40_t29_t4362 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t466 ** p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t466 **)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t4497 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t326 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, bool* p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (bool*)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t17_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t17  p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t17 *)args[0]), (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t29_t238 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t238  p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((t238 *)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t1089_TI;
struct t29;
void* RuntimeInvoker_t1089_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t465_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t465  p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t465 *)args[0]), (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
struct t29;
void* RuntimeInvoker_t22_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, int32_t p1, t29 * p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
struct t29;
void* RuntimeInvoker_t22_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t919_t919 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int64_t p1, int64_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

struct t29;
extern TypeInfo t23_TI;
void* RuntimeInvoker_t23_t598_t587 (MethodInfo* method, void* obj, void** args){
	typedef t23  (*Func)(void* obj, t224 * p1, t23 * p2, MethodInfo* method);
	t23  ret = ((Func)method->method)(obj, (t224 *)args[0], (t23 *)args[1], method);
	return Box(&t23_TI, &ret);
}

struct t29;
extern TypeInfo t601_TI;
struct t29;
void* RuntimeInvoker_t601_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	double ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t601_TI, &ret);
}

struct t29;
extern TypeInfo t725_TI;
struct t29;
void* RuntimeInvoker_t725_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef t725  (*Func)(void* obj, t29 * p1, int8_t p2, MethodInfo* method);
	t725  ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), method);
	return Box(&t725_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t35_t35 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t35 p1, t35 p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t35*)args[0]), *((t35*)args[1]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t919_TI;
void* RuntimeInvoker_t919_t35_t2004 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, t35 p1, int32_t* p2, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((t35*)args[0]), (int32_t*)args[1], method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t183_TI;
void* RuntimeInvoker_t183_t183_t164 (MethodInfo* method, void* obj, void** args){
	typedef t183  (*Func)(void* obj, t183  p1, t164  p2, MethodInfo* method);
	t183  ret = ((Func)method->method)(obj, *((t183 *)args[0]), *((t164 *)args[1]), method);
	return Box(&t183_TI, &ret);
}

struct t29;
extern TypeInfo t17_TI;
void* RuntimeInvoker_t17_t17_t164 (MethodInfo* method, void* obj, void** args){
	typedef t17  (*Func)(void* obj, t17  p1, t164  p2, MethodInfo* method);
	t17  ret = ((Func)method->method)(obj, *((t17 *)args[0]), *((t164 *)args[1]), method);
	return Box(&t17_TI, &ret);
}

struct t29;
extern TypeInfo t17_TI;
void* RuntimeInvoker_t17_t17_t17 (MethodInfo* method, void* obj, void** args){
	typedef t17  (*Func)(void* obj, t17  p1, t17  p2, MethodInfo* method);
	t17  ret = ((Func)method->method)(obj, *((t17 *)args[0]), *((t17 *)args[1]), method);
	return Box(&t17_TI, &ret);
}

struct t29;
extern TypeInfo t23_TI;
void* RuntimeInvoker_t23_t23_t23 (MethodInfo* method, void* obj, void** args){
	typedef t23  (*Func)(void* obj, t23  p1, t23  p2, MethodInfo* method);
	t23  ret = ((Func)method->method)(obj, *((t23 *)args[0]), *((t23 *)args[1]), method);
	return Box(&t23_TI, &ret);
}

struct t29;
extern TypeInfo t23_TI;
void* RuntimeInvoker_t23_t362_t23 (MethodInfo* method, void* obj, void** args){
	typedef t23  (*Func)(void* obj, t362  p1, t23  p2, MethodInfo* method);
	t23  ret = ((Func)method->method)(obj, *((t362 *)args[0]), *((t23 *)args[1]), method);
	return Box(&t23_TI, &ret);
}

struct t29;
extern TypeInfo t396_TI;
void* RuntimeInvoker_t396_t396_t396 (MethodInfo* method, void* obj, void** args){
	typedef t396  (*Func)(void* obj, t396  p1, t396  p2, MethodInfo* method);
	t396  ret = ((Func)method->method)(obj, *((t396 *)args[0]), *((t396 *)args[1]), method);
	return Box(&t396_TI, &ret);
}

struct t29;
extern TypeInfo t183_TI;
void* RuntimeInvoker_t183_t396_t183 (MethodInfo* method, void* obj, void** args){
	typedef t183  (*Func)(void* obj, t396  p1, t183  p2, MethodInfo* method);
	t183  ret = ((Func)method->method)(obj, *((t396 *)args[0]), *((t183 *)args[1]), method);
	return Box(&t183_TI, &ret);
}

struct t29;
extern TypeInfo t183_TI;
void* RuntimeInvoker_t183_t183_t183 (MethodInfo* method, void* obj, void** args){
	typedef t183  (*Func)(void* obj, t183  p1, t183  p2, MethodInfo* method);
	t183  ret = ((Func)method->method)(obj, *((t183 *)args[0]), *((t183 *)args[1]), method);
	return Box(&t183_TI, &ret);
}

struct t29;
extern TypeInfo t492_TI;
void* RuntimeInvoker_t492_t492_t492 (MethodInfo* method, void* obj, void** args){
	typedef t492  (*Func)(void* obj, t492  p1, t492  p2, MethodInfo* method);
	t492  ret = ((Func)method->method)(obj, *((t492 *)args[0]), *((t492 *)args[1]), method);
	return Box(&t492_TI, &ret);
}

struct t29;
extern TypeInfo t1126_TI;
void* RuntimeInvoker_t1126_t1126_t1126 (MethodInfo* method, void* obj, void** args){
	typedef t1126  (*Func)(void* obj, t1126  p1, t1126  p2, MethodInfo* method);
	t1126  ret = ((Func)method->method)(obj, *((t1126 *)args[0]), *((t1126 *)args[1]), method);
	return Box(&t1126_TI, &ret);
}

struct t29;
extern TypeInfo t465_TI;
void* RuntimeInvoker_t465_t465_t816 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, t465  p1, t816  p2, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, *((t465 *)args[0]), *((t816 *)args[1]), method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t816_TI;
void* RuntimeInvoker_t816_t816_t816 (MethodInfo* method, void* obj, void** args){
	typedef t816  (*Func)(void* obj, t816  p1, t816  p2, MethodInfo* method);
	t816  ret = ((Func)method->method)(obj, *((t816 *)args[0]), *((t816 *)args[1]), method);
	return Box(&t816_TI, &ret);
}

struct t29;
extern TypeInfo t816_TI;
void* RuntimeInvoker_t816_t465_t816 (MethodInfo* method, void* obj, void** args){
	typedef t816  (*Func)(void* obj, t465  p1, t816  p2, MethodInfo* method);
	t816  ret = ((Func)method->method)(obj, *((t465 *)args[0]), *((t816 *)args[1]), method);
	return Box(&t816_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
struct t29;
void* RuntimeInvoker_t22_t29_t22 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t29 * p1, float p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (t29 *)args[0], *((float*)args[1]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t390 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t* p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t316;
void* RuntimeInvoker_t44_t29_t1704 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t316** p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t316**)args[1], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t35 p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((t35*)args[1]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t17_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t17  p1, t29 * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t17 *)args[0]), (t29 *)args[1], method);
	return Box(&t44_TI, &ret);
}

struct t29;
#include "t1407.h"
extern TypeInfo t1407_TI;
struct t29;
void* RuntimeInvoker_t1407_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t1407  (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	t1407  ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t1407_TI, &ret);
}

struct t29;
extern TypeInfo t725_TI;
struct t29;
void* RuntimeInvoker_t725_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef t725  (*Func)(void* obj, int32_t p1, t29 * p2, MethodInfo* method);
	t725  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], method);
	return Box(&t725_TI, &ret);
}

struct t29;
extern TypeInfo t725_TI;
struct t29;
void* RuntimeInvoker_t725_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t725  (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	t725  ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t725_TI, &ret);
}

struct t29;
extern TypeInfo t552_TI;
struct t29;
void* RuntimeInvoker_t552_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t552_TI, &ret);
}

struct t29;
extern TypeInfo t3192_TI;
struct t29;
void* RuntimeInvoker_t3192_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef t3192  (*Func)(void* obj, t29 * p1, int8_t p2, MethodInfo* method);
	t3192  ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), method);
	return Box(&t3192_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return NULL;
}

struct t29;
extern TypeInfo t22_TI;
struct t29;
void* RuntimeInvoker_t22_t29_t238 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t29 * p1, t238  p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (t29 *)args[0], *((t238 *)args[1]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t1319_TI;
void* RuntimeInvoker_t1319_t35_t2004 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t35 p1, int32_t* p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t35*)args[0]), (int32_t*)args[1], method);
	return Box(&t1319_TI, &ret);
}

struct t29;
extern TypeInfo t322_TI;
struct t29;
void* RuntimeInvoker_t322_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef t322  (*Func)(void* obj, int32_t p1, t29 * p2, MethodInfo* method);
	t322  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], method);
	return Box(&t322_TI, &ret);
}

struct t29;
extern TypeInfo t2420_TI;
struct t29;
void* RuntimeInvoker_t2420_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2420  (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	t2420  ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t2420_TI, &ret);
}

struct t29;
extern TypeInfo t2444_TI;
struct t29;
void* RuntimeInvoker_t2444_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2444  (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	t2444  ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t2444_TI, &ret);
}

struct t29;
extern TypeInfo t2569_TI;
struct t29;
void* RuntimeInvoker_t2569_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2569  (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	t2569  ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t2569_TI, &ret);
}

struct t29;
extern TypeInfo t2859_TI;
struct t29;
void* RuntimeInvoker_t2859_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2859  (*Func)(void* obj, int32_t p1, t29 * p2, MethodInfo* method);
	t2859  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], method);
	return Box(&t2859_TI, &ret);
}

struct t29;
extern TypeInfo t2911_TI;
struct t29;
void* RuntimeInvoker_t2911_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t2911  (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	t2911  ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t2911_TI, &ret);
}

struct t29;
extern TypeInfo t3106_TI;
struct t29;
void* RuntimeInvoker_t3106_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t3106  (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	t3106  ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return Box(&t3106_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t194_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t194_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t194_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int8_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

struct t29;
extern TypeInfo t297_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t297_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef int8_t (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t297_TI, &ret);
}

struct t29;
extern TypeInfo t164_TI;
struct t29;
void* RuntimeInvoker_t164_t29_t589 (MethodInfo* method, void* obj, void** args){
	typedef t164  (*Func)(void* obj, t29 * p1, t164 * p2, MethodInfo* method);
	t164  ret = ((Func)method->method)(obj, (t29 *)args[0], (t164 *)args[1], method);
	return Box(&t164_TI, &ret);
}

struct t29;
extern TypeInfo t23_TI;
struct t29;
void* RuntimeInvoker_t23_t29_t587 (MethodInfo* method, void* obj, void** args){
	typedef t23  (*Func)(void* obj, t29 * p1, t23 * p2, MethodInfo* method);
	t23  ret = ((Func)method->method)(obj, (t29 *)args[0], (t23 *)args[1], method);
	return Box(&t23_TI, &ret);
}

struct t29;
extern TypeInfo t329_TI;
struct t29;
void* RuntimeInvoker_t329_t29_t587 (MethodInfo* method, void* obj, void** args){
	typedef t329  (*Func)(void* obj, t29 * p1, t23 * p2, MethodInfo* method);
	t329  ret = ((Func)method->method)(obj, (t29 *)args[0], (t23 *)args[1], method);
	return Box(&t329_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t348_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, uint8_t p1, t29 * p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (t29 *)args[1], method);
	return ret;
}

struct t29;
extern TypeInfo t348_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t348_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t348_TI, &ret);
}

struct t29;
extern TypeInfo t1306_TI;
struct t29;
void* RuntimeInvoker_t1306_t29_t2004 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t* p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], method);
	return Box(&t1306_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t348 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, uint8_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

struct t29;
extern TypeInfo t23_TI;
struct t29;
void* RuntimeInvoker_t23_t29_t17 (MethodInfo* method, void* obj, void** args){
	typedef t23  (*Func)(void* obj, t29 * p1, t17  p2, MethodInfo* method);
	t23  ret = ((Func)method->method)(obj, (t29 *)args[0], *((t17 *)args[1]), method);
	return Box(&t23_TI, &ret);
}

struct t29;
extern TypeInfo t329_TI;
struct t29;
void* RuntimeInvoker_t329_t29_t17 (MethodInfo* method, void* obj, void** args){
	typedef t329  (*Func)(void* obj, t29 * p1, t17  p2, MethodInfo* method);
	t329  ret = ((Func)method->method)(obj, (t29 *)args[0], *((t17 *)args[1]), method);
	return Box(&t329_TI, &ret);
}

struct t29;
extern TypeInfo t372_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t372_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef int16_t (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t372_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t372 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int16_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

struct t29;
extern TypeInfo t626_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t626_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t626_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, t29 * p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], method);
	return ret;
}

struct t29;
extern TypeInfo t344_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t344_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t919_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t919_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t919_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t919 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int64_t p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t919_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int64_t p1, t29 * p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (t29 *)args[1], method);
	return ret;
}

struct t29;
extern TypeInfo t1089_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t1089_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t22_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t22_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t22_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, float p1, t29 * p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((float*)args[0]), (t29 *)args[1], method);
	return ret;
}

struct t29;
extern TypeInfo t601_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t601_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	double ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t601_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t601_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, double p1, t29 * p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((double*)args[0]), (t29 *)args[1], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t587 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t23 * p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t23 *)args[1], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t723;
#include "t723.h"
void* RuntimeInvoker_t29_t29_t916 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t723 ** p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t723 **)args[1], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t390 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t* p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], method);
	return ret;
}

struct t29;
extern TypeInfo t164_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t164_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t164  (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	t164  ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t164_TI, &ret);
}

struct t29;
extern TypeInfo t970_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t970_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t970_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t936 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t936  p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((t936 *)args[1]), method);
	return ret;
}

struct t29;
extern TypeInfo t1126_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t1126_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t1126  (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	t1126  ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t1126_TI, &ret);
}

struct t29;
extern TypeInfo t1200_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t1200_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t1200_TI, &ret);
}

struct t29;
extern TypeInfo t465_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t465_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t725_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t725_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t725  (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	t725  ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t725_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t35_t35 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t35 p1, t35 p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t35*)args[0]), *((t35*)args[1]), method);
	return ret;
}

struct t29;
extern TypeInfo t35_TI;
struct t29;
void* RuntimeInvoker_t35_t35_t29 (MethodInfo* method, void* obj, void** args){
	typedef t35 (*Func)(void* obj, t35 p1, t29 * p2, MethodInfo* method);
	t35 ret = ((Func)method->method)(obj, *((t35*)args[0]), (t29 *)args[1], method);
	return Box(&t35_TI, &ret);
}

struct t29;
extern TypeInfo t2458_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t2458_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2458  (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	t2458  ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t2458_TI, &ret);
}

struct t29;
extern TypeInfo t2465_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t2465_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2465  (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	t2465  ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t2465_TI, &ret);
}

struct t29;
extern TypeInfo t2562_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t2562_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2562  (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	t2562  ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t2562_TI, &ret);
}

struct t29;
extern TypeInfo t2893_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t2893_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t2893  (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	t2893  ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return Box(&t2893_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t35_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t35 p1, t29 * p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t35*)args[0]), (t29 *)args[1], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t297_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t626_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t372_t372_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t372_t372_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t22_t22_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, float p1, float p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t44_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t919_t44_t919 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t6;
void* RuntimeInvoker_t40_t44_t325_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, t6 ** p2, int8_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t6 **)args[1], *((int8_t*)args[2]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t372_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t295;
#include "t295.h"
void* RuntimeInvoker_t40_t44_t297_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, t295 ** p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (t295 **)args[2], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, float p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t344_TI;
void* RuntimeInvoker_t344_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t316;
void* RuntimeInvoker_t21_t1704_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t316** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t316**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2172;
void* RuntimeInvoker_t21_t4016_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2172** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2172**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2214;
void* RuntimeInvoker_t21_t4019_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2214** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2214**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2231;
void* RuntimeInvoker_t21_t4029_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2231** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2231**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2238;
void* RuntimeInvoker_t21_t4030_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2238** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2238**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2251;
void* RuntimeInvoker_t21_t4034_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2251** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2251**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2265;
void* RuntimeInvoker_t21_t4038_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2265** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2265**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2299;
void* RuntimeInvoker_t21_t4074_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2299** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2299**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2317;
void* RuntimeInvoker_t21_t4080_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2317** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2317**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2366;
void* RuntimeInvoker_t21_t4105_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2366** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2366**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2413;
void* RuntimeInvoker_t21_t4129_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2413** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2413**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2473;
void* RuntimeInvoker_t21_t4141_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2473** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2473**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t201;
void* RuntimeInvoker_t21_t4149_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t201** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t201**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2509;
void* RuntimeInvoker_t21_t4153_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2509** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2509**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2526;
void* RuntimeInvoker_t21_t4155_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2526** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2526**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2545;
void* RuntimeInvoker_t21_t4160_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2545** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2545**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2638;
void* RuntimeInvoker_t21_t4224_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2638** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2638**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2652;
void* RuntimeInvoker_t21_t4233_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2652** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2652**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2675;
void* RuntimeInvoker_t21_t4246_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2675** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2675**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2704;
void* RuntimeInvoker_t21_t4254_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2704** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2704**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2743;
void* RuntimeInvoker_t21_t4286_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2743** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2743**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2789;
void* RuntimeInvoker_t21_t4313_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2789** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2789**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2794;
void* RuntimeInvoker_t21_t4315_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2794** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2794**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2871;
void* RuntimeInvoker_t21_t4355_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2871** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2871**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t2980;
void* RuntimeInvoker_t21_t4414_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t2980** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t2980**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t530;
void* RuntimeInvoker_t21_t4441_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t530** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t530**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t531;
void* RuntimeInvoker_t21_t4444_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t531** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t531**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t537;
void* RuntimeInvoker_t21_t4459_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t537** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t537**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t3132;
void* RuntimeInvoker_t21_t4505_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t3132** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t3132**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t3146;
void* RuntimeInvoker_t21_t4509_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t3146** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t3146**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t446;
void* RuntimeInvoker_t21_t2102_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t446** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t446**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t3462;
void* RuntimeInvoker_t21_t5015_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t3462** p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t3462**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t44_t390_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int8_t p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t22_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, float p1, float p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t781;
void* RuntimeInvoker_t21_t297_t1092_t390 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int8_t p1, t781** p2, int32_t* p3, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (t781**)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t297_t297_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, t29 * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (t29 *)args[2], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
void* RuntimeInvoker_t40_t297_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int8_t p1, t29 * p2, int8_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (t29 *)args[1], *((int8_t*)args[2]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t35_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t35 p1, int32_t p2, int8_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t35*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t348_t348_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, t29 * p3, MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (t29 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t348_t348 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, uint8_t p2, uint8_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t390_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t297_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t35_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t297_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, t29 * p3, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (t29 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t390_t390 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t390_t390_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t1089_TI;
void* RuntimeInvoker_t1089_t919_t919_t919 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t390_t390_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t1139;
struct t446;
void* RuntimeInvoker_t40_t44_t2101_t2102 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, t1139** p2, t446** p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t1139**)args[1], (t446**)args[2], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t372 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int16_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t44_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, t29 * p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (t29 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t194_TI;
struct t29;
void* RuntimeInvoker_t194_t29_t44_t372 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, t29 * p1, int32_t p2, int16_t p3, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(&t194_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t44_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 * p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t23_t132_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t23  p1, t132  p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, *((t23 *)args[0]), *((t132 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t919 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int64_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t919_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int64_t p1, t29 * p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t22_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, float p1, float p2, float p3, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int8_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t587_t400_t605 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t23 * p1, t132 * p2, t492 * p3, MethodInfo* method);
	((Func)method->method)(obj, (t23 *)args[0], (t132 *)args[1], (t492 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t919_t919 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int64_t p2, int64_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t44_t22_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, float p2, t29 * p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (t29 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t919_t29_t919 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int64_t p1, t29 * p2, int64_t p3, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (t29 *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t599_t598_t600 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t329 * p1, t224 * p2, float* p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t329 *)args[0], (t224 *)args[1], (float*)args[2], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t372 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int16_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t601_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, double p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t23_t132_t605 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t23  p1, t132  p2, t492 * p3, MethodInfo* method);
	((Func)method->method)(obj, *((t23 *)args[0]), *((t132 *)args[1]), (t492 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t23_t362_t23 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t23  p1, t362  p2, t23  p3, MethodInfo* method);
	((Func)method->method)(obj, *((t23 *)args[0]), *((t362 *)args[1]), *((t23 *)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t465_t465_t816 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t465  p1, t465  p2, t816  p3, MethodInfo* method);
	((Func)method->method)(obj, *((t465 *)args[0]), *((t465 *)args[1]), *((t816 *)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t35_t919_t2004 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t35 p1, int64_t p2, int32_t* p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t35*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, t29 * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (t29 *)args[2], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t106_TI;
void* RuntimeInvoker_t106_t22_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, float p1, float p2, float p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(&t106_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t390_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t* p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
#include "t1381.h"
void* RuntimeInvoker_t21_t29_t2027_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t1381 * p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t1381 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t515_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t515  p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((t515 *)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t1756_t1756_t1756 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1126 * p1, t1126 * p2, t1126 * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t1126 *)args[0], (t1126 *)args[1], (t1126 *)args[2], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t35_t390_t390 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, int32_t* p2, int32_t* p3, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t35_t607_t607 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, t503 * p2, t503 * p3, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), (t503 *)args[1], (t503 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t811_TI;
struct t29;
void* RuntimeInvoker_t811_t29_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int8_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(&t811_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t919_t919_t919 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t390_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t* p2, int32_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t7;
struct t7;
void* RuntimeInvoker_t21_t29_t639_t639 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t7** p2, t7** p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t7**)args[1], (t7**)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int8_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t781;
struct t781;
void* RuntimeInvoker_t21_t29_t1092_t1092 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t781** p2, t781** p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t781**)args[1], (t781**)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t297_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int8_t p2, t29 * p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), (t29 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t1712_t2022 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int64_t* p2, t29 ** p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (int64_t*)args[1], (t29 **)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t348_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, uint8_t p2, t29 * p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((uint8_t*)args[1]), (t29 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, int8_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int8_t*)args[2]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
#include "t1318.h"
void* RuntimeInvoker_t40_t29_t2005_t2004 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t1318 * p2, int32_t* p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t1318 *)args[1], (int32_t*)args[2], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t23_TI;
void* RuntimeInvoker_t23_t23_t23_t22 (MethodInfo* method, void* obj, void** args){
	typedef t23  (*Func)(void* obj, t23  p1, t23  p2, float p3, MethodInfo* method);
	t23  ret = ((Func)method->method)(obj, *((t23 *)args[0]), *((t23 *)args[1]), *((float*)args[2]), method);
	return Box(&t23_TI, &ret);
}

struct t29;
extern TypeInfo t132_TI;
void* RuntimeInvoker_t132_t132_t132_t22 (MethodInfo* method, void* obj, void** args){
	typedef t132  (*Func)(void* obj, t132  p1, t132  p2, float p3, MethodInfo* method);
	t132  ret = ((Func)method->method)(obj, *((t132 *)args[0]), *((t132 *)args[1]), *((float*)args[2]), method);
	return Box(&t132_TI, &ret);
}

struct t29;
extern TypeInfo t194_TI;
struct t29;
void* RuntimeInvoker_t194_t29_t390_t961 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, t29 * p1, int32_t* p2, uint16_t* p3, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(&t194_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t22_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, float p1, float p2, float p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int8_t p2, int8_t p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 * p2, t29 * p3, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t396_TI;
void* RuntimeInvoker_t396_t587_t596_t587 (MethodInfo* method, void* obj, void** args){
	typedef t396  (*Func)(void* obj, t23 * p1, t362 * p2, t23 * p3, MethodInfo* method);
	t396  ret = ((Func)method->method)(obj, (t23 *)args[0], (t362 *)args[1], (t23 *)args[2], method);
	return Box(&t396_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t329_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t329  p1, float p2, int32_t p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t329 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t919 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int64_t p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t396_TI;
void* RuntimeInvoker_t396_t23_t362_t23 (MethodInfo* method, void* obj, void** args){
	typedef t396  (*Func)(void* obj, t23  p1, t362  p2, t23  p3, MethodInfo* method);
	t396  ret = ((Func)method->method)(obj, *((t23 *)args[0]), *((t362 *)args[1]), *((t23 *)args[2]), method);
	return Box(&t396_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t44_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, t29 * p2, int8_t p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct t29;
extern TypeInfo t297_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t297_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef int8_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], method);
	return Box(&t297_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, int8_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int8_t*)args[2]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, int8_t p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t35_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t35 p1, int32_t p2, int32_t p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t35*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t35_t29_t594 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, t29 * p2, t17 * p3, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), (t29 *)args[1], (t17 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t348_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t348_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint8_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], method);
	return Box(&t348_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, float p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((float*)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t17_TI;
struct t29;
void* RuntimeInvoker_t17_t164_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t17  (*Func)(void* obj, t164  p1, t29 * p2, int32_t p3, MethodInfo* method);
	t17  ret = ((Func)method->method)(obj, *((t164 *)args[0]), (t29 *)args[1], *((int32_t*)args[2]), method);
	return Box(&t17_TI, &ret);
}

struct t29;
extern TypeInfo t372_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t372_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef int16_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], method);
	return Box(&t372_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t57_t57_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t57  p1, t57  p2, t29 * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t57 *)args[0]), *((t57 *)args[1]), (t29 *)args[2], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t184_t184_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t184  p1, t184  p2, t29 * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t184 *)args[0]), *((t184 *)args[1]), (t29 *)args[2], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t381_t381_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t381  p1, t381  p2, t29 * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t381 *)args[0]), *((t381 *)args[1]), (t29 *)args[2], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t380_t380_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t380  p1, t380  p2, t29 * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t380 *)args[0]), *((t380 *)args[1]), (t29 *)args[2], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t626_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t626_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], method);
	return Box(&t626_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t44_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, t29 * p2, int32_t p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct t29;
extern TypeInfo t35_TI;
struct t29;
void* RuntimeInvoker_t35_t297_t29_t326 (MethodInfo* method, void* obj, void** args){
	typedef t35 (*Func)(void* obj, int8_t p1, t29 * p2, bool* p3, MethodInfo* method);
	t35 ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (t29 *)args[1], (bool*)args[2], method);
	return Box(&t35_TI, &ret);
}

struct t29;
extern TypeInfo t344_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t344_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint32_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], method);
	return Box(&t344_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t316;
struct t29;
void* RuntimeInvoker_t21_t29_t1704_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t316** p2, t29 * p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t316**)args[1], (t29 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t594_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t17 * p2, t29 * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t17 *)args[1], (t29 *)args[2], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t316;
void* RuntimeInvoker_t40_t29_t29_t1704 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, t316** p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t316**)args[2], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t390_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, t29 * p2, t29 * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (t29 *)args[1], (t29 *)args[2], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t919_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t919_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t735 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t735  p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((t735 *)args[2]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t17_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t17  p2, t29 * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((t17 *)args[1]), (t29 *)args[2], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t1089_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t1089_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef uint64_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], method);
	return Box(&t1089_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t735 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, t735  p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((t735 *)args[2]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
struct t29;
void* RuntimeInvoker_t22_t35_t29_t22 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t35 p1, t29 * p2, float p3, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((t35*)args[0]), (t29 *)args[1], *((float*)args[2]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t295;
void* RuntimeInvoker_t29_t29_t297_t1705 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int8_t p2, t295 ** p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), (t295 **)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t119_t326_t326 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t119  p1, bool* p2, bool* p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t119 *)args[0]), (bool*)args[1], (bool*)args[2], method);
	return ret;
}

struct t29;
extern TypeInfo t811_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t811_t29_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, int8_t p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int8_t*)args[2]), method);
	return Box(&t811_TI, &ret);
}

struct t29;
extern TypeInfo t601_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t601_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef double (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, MethodInfo* method);
	double ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], method);
	return Box(&t601_TI, &ret);
}

struct t29;
extern TypeInfo t22_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t22_t29_t29_t22 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t29 * p1, t29 * p2, float p3, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((float*)args[2]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t1126_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t1126_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef t1126  (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, MethodInfo* method);
	t1126  ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], method);
	return Box(&t1126_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t35_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t35 p2, t29 * p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((t35*)args[1]), (t29 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t465_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t465_t29_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t35_TI;
struct t29;
struct t1142;
void* RuntimeInvoker_t35_t29_t390_t2020 (MethodInfo* method, void* obj, void** args){
	typedef t35 (*Func)(void* obj, t29 * p1, int32_t* p2, t1142 ** p3, MethodInfo* method);
	t35 ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], (t1142 **)args[2], method);
	return Box(&t35_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], method);
	return Box(&t40_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, int8_t p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t297_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int8_t p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t297_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int8_t p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t348_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, uint8_t p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t1458;
void* RuntimeInvoker_t29_t29_t735_t2050 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t735  p2, t29 ** p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((t735 *)args[1]), (t29 **)args[2], method);
	return ret;
}

struct t29;
extern TypeInfo t17_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t17_t17_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t17  (*Func)(void* obj, t17  p1, t29 * p2, t29 * p3, MethodInfo* method);
	t17  ret = ((Func)method->method)(obj, *((t17 *)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return Box(&t17_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t372_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int16_t p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int16_t*)args[1]), (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t919_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int64_t p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int64_t*)args[1]), (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t22_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, float p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((float*)args[1]), (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t22_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, float p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((float*)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t601_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, double p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((double*)args[1]), (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t7;
void* RuntimeInvoker_t29_t29_t29_t639 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t7** p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t7**)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t195;
#include "t195.h"
void* RuntimeInvoker_t29_t29_t29_t1762 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t195 ** p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t195 **)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t295;
void* RuntimeInvoker_t29_t29_t29_t1705 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t295 ** p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t295 **)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t2022 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t29 ** p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 **)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t866_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t866  p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t866 *)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t735_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t735  p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t735 *)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t465_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t465  p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t465 *)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t1126_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t1126  p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((t1126 *)args[1]), (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t57_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t57  p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t57 *)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t132_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t132  p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t132 *)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t184_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t184  p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t184 *)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t17_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t17  p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t17 *)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t381_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t381  p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t381 *)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t380_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t380  p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t380 *)args[0]), (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t297_t297_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t372_t297_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t297_t297_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t44_t297_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t44_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t44_t44_t297_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t844_TI;
void* RuntimeInvoker_t844_t297_t297_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(&t844_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t132_t22_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t132  p1, float p2, int8_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((t132 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t22_t22_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int8_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t44_t390_t390_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t22_t22_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t35_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t44_t44_t297_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, t29 * p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (t29 *)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t44_t44_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, t29 * p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (t29 *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t17_t17_t22_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t17  p1, t17  p2, float p3, int8_t p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t17 *)args[0]), *((t17 *)args[1]), *((float*)args[2]), *((int8_t*)args[3]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44_t626 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, uint16_t p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t44_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 * p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t44_t44_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, t29 * p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (t29 *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t329_t624_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t329  p1, t127 * p2, float p3, int32_t p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t329 *)args[0]), (t127 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
void* RuntimeInvoker_t44_t44_t44_t390_t390 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t390_t390_t390_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t297_t29_t44_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int8_t p1, t29 * p2, int32_t p3, t295 ** p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), (t295 **)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t919_t919_t919 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int64_t p2, int64_t p3, int64_t p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, t29 * p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, float p3, float p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t390_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t* p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t961_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, uint16_t* p3, int8_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t396_TI;
void* RuntimeInvoker_t396_t22_t22_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef t396  (*Func)(void* obj, float p1, float p2, float p3, float p4, MethodInfo* method);
	t396  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(&t396_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int8_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t1451;
void* RuntimeInvoker_t21_t29_t297_t2022_t2058 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int8_t p2, t29 ** p3, t1451** p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), (t29 **)args[2], (t1451**)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t29_t297_t390_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int8_t p2, int32_t* p3, t295 ** p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (t295 **)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t29_t297_t1712_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int8_t p2, int64_t* p3, t295 ** p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (t295 **)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t29_t297_t1715_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int8_t p2, uint32_t* p3, t295 ** p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (t295 **)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t29_t297_t1723_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int8_t p2, int8_t* p3, t295 ** p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (t295 **)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t29_t297_t1726_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int8_t p2, int16_t* p3, t295 ** p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (t295 **)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t297_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int8_t p2, int8_t p3, int8_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t1756_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t1126 * p1, t29 * p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t1126 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t390 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t* p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t919_TI;
void* RuntimeInvoker_t919_t35_t919_t44_t2004 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, t35 p1, int64_t p2, int32_t p3, int32_t* p4, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((t35*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t390_t390 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t* p3, int32_t* p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t297_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int8_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t35_t44_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, int32_t p2, t29 * p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), *((int32_t*)args[1]), (t29 *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t35_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, t29 * p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t57_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t57  p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((t57 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t184_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t184  p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((t184 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t381_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t381  p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((t381 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t380_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t380  p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((t380 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t330_TI;
void* RuntimeInvoker_t330_t17_t17_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef t330  (*Func)(void* obj, t17  p1, t17  p2, float p3, int32_t p4, MethodInfo* method);
	t330  ret = ((Func)method->method)(obj, *((t17 *)args[0]), *((t17 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t330_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, int8_t p3, int8_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t44_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, t29 * p3, t29 * p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, int32_t p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t35_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t35 p1, t29 * p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t35*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t35_TI;
struct t29;
void* RuntimeInvoker_t35_t297_t297_t29_t326 (MethodInfo* method, void* obj, void** args){
	typedef t35 (*Func)(void* obj, int8_t p1, int8_t p2, t29 * p3, bool* p4, MethodInfo* method);
	t35 ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (t29 *)args[2], (bool*)args[3], method);
	return Box(&t35_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t348_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, uint8_t p1, t29 * p2, int32_t p3, int32_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t44_t22_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, float p2, t29 * p3, t29 * p4, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t733;
#include "t733.h"
void* RuntimeInvoker_t21_t29_t1712_t2022_t2059 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int64_t* p2, t29 ** p3, t733 ** p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (int64_t*)args[1], (t29 **)args[2], (t733 **)args[3], method);
	return NULL;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t587_t587_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t23 * p1, t23 * p2, float p3, int32_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t23 *)args[0], (t23 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int32_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int32_t*)args[3]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t23_t23_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t23  p1, t23  p2, float p3, int32_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t23 *)args[0]), *((t23 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t17_t17_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t17  p1, t17  p2, float p3, int32_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t17 *)args[0]), *((t17 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t44_t29_t390 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int32_t* p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (int32_t*)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t44_t29_t1712 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int64_t* p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (int64_t*)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t44_t29_t1715 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, uint32_t* p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (uint32_t*)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t44_t29_t1091 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, uint8_t* p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (uint8_t*)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t44_t29_t1729 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, uint16_t* p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (uint16_t*)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t594_t29_t29_t594 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t17 * p1, t29 * p2, t29 * p3, t17 * p4, MethodInfo* method);
	((Func)method->method)(obj, (t17 *)args[0], (t29 *)args[1], (t29 *)args[2], (t17 *)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t297_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int8_t p2, t29 * p3, t29 * p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t390_t29_t29_t326 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t* p1, t29 * p2, t29 * p3, bool* p4, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (t29 *)args[1], (t29 *)args[2], (bool*)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t1183;
struct t1184;
void* RuntimeInvoker_t21_t29_t29_t1918_t1919 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t1183** p3, t1184** p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t1183**)args[2], (t1184**)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t297_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int8_t p3, t29 * p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int8_t*)args[2]), (t29 *)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t1712_t2022 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int64_t* p3, t29 ** p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (int64_t*)args[2], (t29 **)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int8_t p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t17_t29_t29_t594 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t17  p1, t29 * p2, t29 * p3, t17 * p4, MethodInfo* method);
	((Func)method->method)(obj, *((t17 *)args[0]), (t29 *)args[1], (t29 *)args[2], (t17 *)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t17_t29_t587 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t17  p2, t29 * p3, t23 * p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((t17 *)args[1]), (t29 *)args[2], (t23 *)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t17_t29_t594 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t17  p2, t29 * p3, t17 * p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((t17 *)args[1]), (t29 *)args[2], (t17 *)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t599_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t329 * p2, float p3, int32_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t329 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t297_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int8_t p2, t29 * p3, int8_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), (t29 *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, int8_t p3, int8_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int32_t p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, t29 * p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int32_t p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int32_t*)args[3]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t372_t29_t326_t326 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int16_t p1, t29 * p2, bool* p3, bool* p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (t29 *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

struct t29;
extern TypeInfo t22_TI;
struct t29;
struct t29;
struct t271;
void* RuntimeInvoker_t22_t29_t29_t22_t417 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, t29 * p1, t29 * p2, float p3, t29 ** p4, MethodInfo* method);
	float ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((float*)args[2]), (t29 **)args[3], method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t29_t297_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, int8_t p3, t29 * p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int8_t*)args[2]), (t29 *)args[3], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t29_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int8_t p4, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int8_t*)args[3]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t297_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, int8_t p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (t29 *)args[3], method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t22_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, float p2, t29 * p3, t29 * p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((float*)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return NULL;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, int32_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t44_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, int32_t p2, t29 * p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t17_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t17  p3, t29 * p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((t17 *)args[2]), (t29 *)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t465_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t465_t29_t29_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t465  (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int32_t p4, MethodInfo* method);
	t465  ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int32_t*)args[3]), method);
	return Box(&t465_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, t29 * p4, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, t29 * p4, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], method);
	return Box(&t40_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int8_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t297_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int8_t p2, t29 * p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t348_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, uint8_t p1, t29 * p2, t29 * p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t57_t57_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t57  p1, t57  p2, t29 * p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t57 *)args[0]), *((t57 *)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t127_t127_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t127  p1, t127  p2, t29 * p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t127 *)args[0]), *((t127 *)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t184_t184_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t184  p1, t184  p2, t29 * p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t184 *)args[0]), *((t184 *)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t381_t381_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t381  p1, t381  p2, t29 * p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t381 *)args[0]), *((t381 *)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t380_t380_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t380  p1, t380  p2, t29 * p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t380 *)args[0]), *((t380 *)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int32_t p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, t29 * p2, t29 * p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t465_t1631_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t465  p1, t1631  p2, t29 * p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((t465 *)args[0]), *((t1631 *)args[1]), (t29 *)args[2], (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t735_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t735  p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((t735 *)args[2]), (t29 *)args[3], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, t29 * p4, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t372_t372_t297_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t44_t44_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t297_t297_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t132_t22_t297_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t132  p1, float p2, int8_t p3, int8_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((t132 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t372_t29_t297_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int16_t p1, t29 * p2, int8_t p3, int8_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (t29 *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t919_TI;
void* RuntimeInvoker_t919_t44_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(&t919_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t35_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44_t297_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t22_t22_t297_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, float p2, float p3, int8_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((float*)args[1]), *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t587_t587_t624_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t23 * p1, t23 * p2, t127 * p3, float p4, int32_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t23 *)args[0], (t23 *)args[1], (t127 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t390_t29_t297_t297_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, t29 * p2, int8_t p3, int8_t p4, t295 ** p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (t29 *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (t295 **)args[4], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t1451;
void* RuntimeInvoker_t21_t348_t29_t297_t2022_t2058 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint8_t p1, t29 * p2, int8_t p3, t29 ** p4, t1451** p5, MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (t29 *)args[1], *((int8_t*)args[2]), (t29 **)args[3], (t1451**)args[4], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
void* RuntimeInvoker_t40_t23_t23_t624_t22_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t23  p1, t23  p2, t127 * p3, float p4, int32_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((t23 *)args[0]), *((t23 *)args[1]), (t127 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t961_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t297_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t390_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t22_t22_t22_t22_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, t29 * p5, MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (t29 *)args[4], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int8_t p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int32_t p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t326_t297_t390_t390 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t22_t297_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, float p3, int8_t p4, int32_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t390_t29_t390_t297_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t* p1, t29 * p2, int32_t* p3, int8_t p4, t295 ** p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (t29 *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (t295 **)args[4], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t44_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int32_t p4, int32_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t733;
void* RuntimeInvoker_t21_t348_t29_t1712_t2022_t2059 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint8_t p1, t29 * p2, int64_t* p3, t29 ** p4, t733 ** p5, MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (t29 *)args[1], (int64_t*)args[2], (t29 **)args[3], (t733 **)args[4], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t44_t29_t1756_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, t1126 * p4, int8_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (t1126 *)args[3], *((int8_t*)args[4]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t44_t29_t297_t390 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int8_t p4, int32_t* p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t919_t29_t919_t919 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int64_t p2, t29 * p3, int64_t p4, int64_t p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int64_t*)args[1]), (t29 *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int32_t p4, int32_t p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t29_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, int32_t p4, int32_t p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t p4, t29 * p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (t29 *)args[4], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t44_t44_t29_t390 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t* p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], (int32_t*)args[4], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t35_t29_t44_t44_t2004 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t35 p1, t29 * p2, int32_t p3, int32_t p4, int32_t* p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((t35*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t35_t589_t29_t44_t594 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, t164 * p2, t29 * p3, int32_t p4, t17 * p5, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), (t164 *)args[1], (t29 *)args[2], *((int32_t*)args[3]), (t17 *)args[4], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t781;
void* RuntimeInvoker_t21_t29_t390_t1091_t390_t1092 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t* p2, uint8_t* p3, int32_t* p4, t781** p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (t781**)args[4], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t35_t164_t29_t44_t594 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, t164  p2, t29 * p3, int32_t p4, t17 * p5, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), *((t164 *)args[1]), (t29 *)args[2], *((int32_t*)args[3]), (t17 *)args[4], method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t29_t44_t44_t326 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, int32_t p4, bool* p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t297_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int8_t p3, int32_t p4, t29 * p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (t29 *)args[4], method);
	return NULL;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t372_t29_t326_t326_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int16_t p1, t29 * p2, bool* p3, bool* p4, int8_t p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (t29 *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
struct t733;
void* RuntimeInvoker_t21_t29_t29_t919_t2022_t2059 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int64_t p3, t29 ** p4, t733 ** p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int64_t*)args[2]), (t29 **)args[3], (t733 **)args[4], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t44_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, int32_t p4, t29 * p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (t29 *)args[4], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t390_t29_t29_t326_t326 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t* p1, t29 * p2, t29 * p3, bool* p4, bool* p5, MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (t29 *)args[1], (t29 *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, t29 * p5, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], (t29 *)args[4], method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t44_t44_t2004 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, int32_t p4, int32_t* p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t29_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, t29 * p4, int8_t p5, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], *((int8_t*)args[4]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t29_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int8_t p4, int8_t p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, t29 * p4, t29 * p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), (t29 *)args[3], (t29 *)args[4], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t29_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, t29 * p4, int32_t p5, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t348_t29_t297_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, uint8_t p1, t29 * p2, int8_t p3, t29 * p4, t29 * p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (t29 *)args[1], *((int8_t*)args[2]), (t29 *)args[3], (t29 *)args[4], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t44_t29_t29_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, t29 * p2, t29 * p3, t29 * p4, int8_t p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t372_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, int16_t p3, t29 * p4, t29 * p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), (t29 *)args[3], (t29 *)args[4], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, t29 * p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], (t29 *)args[4], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t44_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, t29 * p2, int32_t p3, t29 * p4, t29 * p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), (t29 *)args[3], (t29 *)args[4], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, t29 * p4, t29 * p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), (t29 *)args[3], (t29 *)args[4], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, t29 * p4, t29 * p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (t29 *)args[3], (t29 *)args[4], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t44_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, t29 * p2, t29 * p3, t29 * p4, t29 * p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], (t29 *)args[4], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, t29 * p4, t29 * p5, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], (t29 *)args[4], method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t35_t44_t297_t44_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t35 p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, MethodInfo* method);
	((Func)method->method)(obj, *((t35*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t733;
void* RuntimeInvoker_t21_t29_t297_t297_t1712_t2022_t2059 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int8_t p2, int8_t p3, int64_t* p4, t29 ** p5, t733 ** p6, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (t29 **)args[4], (t733 **)args[5], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t287_t44_t44_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t287  p2, int32_t p3, int32_t p4, float p5, float p6, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((t287 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return NULL;
}

struct t29;
extern TypeInfo t22_TI;
void* RuntimeInvoker_t22_t22_t22_t600_t22_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, float p6, MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(&t22_TI, &ret);
}

struct t29;
extern TypeInfo t396_TI;
void* RuntimeInvoker_t396_t22_t22_t22_t22_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef t396  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, MethodInfo* method);
	t396  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(&t396_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t44_t44_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, int32_t p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, int8_t p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
#include "t1186.h"
void* RuntimeInvoker_t40_t29_t29_t44_t44_t297_t1921 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, int32_t p4, int8_t p5, t1186 * p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (t1186 *)args[5], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t330_TI;
void* RuntimeInvoker_t330_t17_t17_t22_t44_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef t330  (*Func)(void* obj, t17  p1, t17  p2, float p3, int32_t p4, float p5, float p6, MethodInfo* method);
	t330  ret = ((Func)method->method)(obj, *((t17 *)args[0]), *((t17 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(&t330_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, int32_t p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t2091_t2092_t2093_t2093_t2094_t2092 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t297_t297_t297_t297_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, t29 * p6, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (t29 *)args[5], method);
	return ret;
}

struct t29;
extern TypeInfo t35_TI;
struct t29;
void* RuntimeInvoker_t35_t29_t44_t44_t44_t44_t2004 (MethodInfo* method, void* obj, void** args){
	typedef t35 (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, MethodInfo* method);
	t35 ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(&t35_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t29_t44_t961_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int32_t p4, uint16_t* p5, int8_t p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t29_t44_t29_t297_t390_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int8_t p4, int32_t* p5, t295 ** p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (t295 **)args[5], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t29_t44_t29_t297_t1712_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int8_t p4, int64_t* p5, t295 ** p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (t295 **)args[5], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t29_t44_t29_t297_t1715_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int8_t p4, uint32_t* p5, t295 ** p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (t295 **)args[5], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t29_t44_t29_t297_t1718_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int8_t p4, uint64_t* p5, t295 ** p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (t295 **)args[5], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t29_t44_t29_t297_t1753_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int8_t p4, double* p5, t295 ** p6, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int8_t*)args[3]), (double*)args[4], (t295 **)args[5], method);
	return Box(&t40_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
void* RuntimeInvoker_t29_t594_t594_t22_t44_t22_t22 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t17 * p1, t17 * p2, float p3, int32_t p4, float p5, float p6, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t17 *)args[0], (t17 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return ret;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t1576;
#include "t1576.h"
void* RuntimeInvoker_t44_t29_t44_t44_t29_t44_t2068 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, t1576 ** p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), (t1576 **)args[5], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t44_t390 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, int32_t* p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t7;
void* RuntimeInvoker_t21_t29_t390_t390_t390_t326_t639 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, t7** p6, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (t7**)args[5], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t1174;
#include "t1174.h"
struct t1174;
void* RuntimeInvoker_t21_t29_t1920_t1736_t1736_t1920_t1736 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t1174 ** p2, uint8_t** p3, uint8_t** p4, t1174 ** p5, uint8_t** p6, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t1174 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (t1174 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t184_t17_t17_t17_t17 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t184  p2, t17  p3, t17  p4, t17  p5, t17  p6, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((t184 *)args[1]), *((t17 *)args[2]), *((t17 *)args[3]), *((t17 *)args[4]), *((t17 *)args[5]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t1576;
struct t781;
struct t29;
void* RuntimeInvoker_t44_t29_t2068_t1092_t29_t919_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t1576 ** p2, t781** p3, t29 * p4, int64_t p5, int32_t p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t1576 **)args[1], (t781**)args[2], (t29 *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t29_t29_t297_t390 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, t29 * p4, int8_t p5, int32_t* p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (t29 *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t29_t44_t44_t29_t1921 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, int32_t p4, t29 * p5, t1186 * p6, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (t29 *)args[4], (t1186 *)args[5], method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t29_t44_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int32_t p4, int8_t p5, int8_t p6, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t919_t29_t29_t919_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int64_t p1, t29 * p2, t29 * p3, int64_t p4, t29 * p5, t29 * p6, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (t29 *)args[1], (t29 *)args[2], *((int64_t*)args[3]), (t29 *)args[4], (t29 *)args[5], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t919_t29_t919_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int64_t p2, t29 * p3, int64_t p4, t29 * p5, t29 * p6, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int64_t*)args[1]), (t29 *)args[2], *((int64_t*)args[3]), (t29 *)args[4], (t29 *)args[5], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t29_t29_t44_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, t29 * p4, int32_t p5, t29 * p6, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], *((int32_t*)args[4]), (t29 *)args[5], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t44_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, t29 * p4, t29 * p5, t29 * p6, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), (t29 *)args[3], (t29 *)args[4], (t29 *)args[5], method);
	return NULL;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t348_t29_t297_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, uint8_t p1, t29 * p2, int8_t p3, t29 * p4, t29 * p5, t29 * p6, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (t29 *)args[1], *((int8_t*)args[2]), (t29 *)args[3], (t29 *)args[4], (t29 *)args[5], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int32_t p4, t29 * p5, t29 * p6, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int32_t*)args[3]), (t29 *)args[4], (t29 *)args[5], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t29_t44_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int32_t p4, t29 * p5, t29 * p6, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int32_t*)args[3]), (t29 *)args[4], (t29 *)args[5], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, t29 * p4, t29 * p5, t29 * p6, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (t29 *)args[3], (t29 *)args[4], (t29 *)args[5], method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, t29 * p4, t29 * p5, t29 * p6, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], (t29 *)args[4], (t29 *)args[5], method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t44_t44_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44_t44_t44_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t297_t44_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44_t44_t44_t297_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t1715_t44_t1715_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t44_t297_t297_t390 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t594_t594_t22_t44_t22_t22_t625 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t17 * p1, t17 * p2, float p3, int32_t p4, float p5, float p6, t330 * p7, MethodInfo* method);
	((Func)method->method)(obj, (t17 *)args[0], (t17 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (t330 *)args[6], method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t44_t29_t44_t44_t29_t44_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, t29 * p2, int32_t p3, int32_t p4, t29 * p5, int32_t p6, int8_t p7, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (t29 *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t17_t17_t22_t44_t22_t22_t625 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t17  p1, t17  p2, float p3, int32_t p4, float p5, float p6, t330 * p7, MethodInfo* method);
	((Func)method->method)(obj, *((t17 *)args[0]), *((t17 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (t330 *)args[6], method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, int32_t p6, int32_t p7, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t297_t297_t297_t297_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, t29 * p7, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (t29 *)args[6], method);
	return ret;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t44_t961_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, uint16_t* p6, int8_t p7, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t29_t44_t44_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t390_t44_t44_t29_t297_t1921 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t* p2, int32_t p3, int32_t p4, t29 * p5, int8_t p6, t1186 * p7, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (t29 *)args[4], *((int8_t*)args[5]), (t1186 *)args[6], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t44_t44_t29_t297_t390_t390 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int8_t p5, int32_t* p6, int32_t* p7, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t1736_t1736_t1737_t1738_t1738_t1738_t1738 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t29_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t29_t29_t44_t2072_t2073_t297_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, int32_t p3, t465 * p4, t1629 * p5, int8_t p6, t295 ** p7, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int32_t*)args[2]), (t465 *)args[3], (t1629 *)args[4], *((int8_t*)args[5]), (t295 **)args[6], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t29_t44_t44_t297_t29 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int32_t p4, int32_t p5, int8_t p6, t29 * p7, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (t29 *)args[6], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t1575;
#include "t1575.h"
struct t200;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t44_t2066_t2067 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, t1575 ** p6, t200** p7, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), (t1575 **)args[5], (t200**)args[6], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t29_t44_t22_t22_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int32_t p4, float p5, float p6, t29 * p7, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (t29 *)args[6], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t44_t29_t29_t29_t29_t29_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 * p2, t29 * p3, t29 * p4, t29 * p5, t29 * p6, int8_t p7, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], (t29 *)args[4], (t29 *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t919_t919_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, t29 * p3, t29 * p4, t29 * p5, t29 * p6, t29 * p7, MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (t29 *)args[2], (t29 *)args[3], (t29 *)args[4], (t29 *)args[5], (t29 *)args[6], method);
	return NULL;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t316;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t44_t29_t1704_t29_t29_t29_t2022 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int32_t p1, t29 * p2, t316** p3, t29 * p4, t29 * p5, t29 * p6, t29 ** p7, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], (t316**)args[2], (t29 *)args[3], (t29 *)args[4], (t29 *)args[5], (t29 **)args[6], method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, t29 * p4, t29 * p5, t29 * p6, t29 * p7, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], (t29 *)args[3], (t29 *)args[4], (t29 *)args[5], (t29 *)args[6], method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t297_t297_t297_t297_t297_t297_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t636;
struct t7;
void* RuntimeInvoker_t40_t44_t297_t2002_t390_t390_t639_t390_t390 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, t636 ** p3, int32_t* p4, int32_t* p5, t7** p6, int32_t* p7, int32_t* p8, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (t636 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (t7**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t372_t44_t297_t1921 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int16_t p5, int32_t p6, int8_t p7, t1186 * p8, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (t1186 *)args[7], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t44_t29_t44_t297_t1921 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t p4, t29 * p5, int32_t p6, int8_t p7, t1186 * p8, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (t29 *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (t1186 *)args[7], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t390_t44_t44_t44_t29_t297_t1921 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, t29 * p6, int8_t p7, t1186 * p8, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (t29 *)args[5], *((int8_t*)args[6]), (t1186 *)args[7], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t1576;
struct t781;
void* RuntimeInvoker_t44_t29_t44_t44_t44_t29_t2068_t1092_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t p4, t29 * p5, t1576 ** p6, t781** p7, int8_t p8, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (t29 *)args[4], (t1576 **)args[5], (t781**)args[6], *((int8_t*)args[7]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t29_t44_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44_t29_t44_t44_t29_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, int32_t p6, t29 * p7, int32_t p8, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (t29 *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t1576;
struct t781;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t2068_t1092_t29_t919_t44_t29_t390 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t1576 ** p2, t781** p3, t29 * p4, int64_t p5, int32_t p6, t29 * p7, int32_t* p8, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t1576 **)args[1], (t781**)args[2], (t29 *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (t29 *)args[6], (int32_t*)args[7], method);
	return NULL;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t29_t390_t326_t326_t390_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t44_t29_t297_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int8_t p5, t29 * p6, t29 * p7, t29 * p8, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int8_t*)args[4]), (t29 *)args[5], (t29 *)args[6], (t29 *)args[7], method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t29_t919_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, t29 * p2, int64_t p3, t29 * p4, t29 * p5, t29 * p6, t29 * p7, t29 * p8, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((int64_t*)args[2]), (t29 *)args[3], (t29 *)args[4], (t29 *)args[5], (t29 *)args[6], (t29 *)args[7], method);
	return NULL;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t29_t29_t44_t29_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, t29 * p4, t29 * p5, t29 * p6, t29 * p7, t29 * p8, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], (t29 *)args[3], (t29 *)args[4], (t29 *)args[5], (t29 *)args[6], (t29 *)args[7], method);
	return ret;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t44_t297_t390_t326_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t44_t44_t326_t326_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t1576;
struct t781;
void* RuntimeInvoker_t44_t29_t44_t44_t44_t44_t29_t2068_t1092_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, t29 * p6, t1576 ** p7, t781** p8, int8_t p9, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (t29 *)args[5], (t1576 **)args[6], (t781**)args[7], *((int8_t*)args[8]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t29_t44_t44_t29_t44_t44_t29_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, int32_t p6, t29 * p7, int32_t p8, int32_t p9, MethodInfo* method);
	((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (t29 *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t1177;
void* RuntimeInvoker_t40_t29_t390_t44_t44_t29_t297_t44_t1922_t1921 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t* p2, int32_t p3, int32_t p4, t29 * p5, int8_t p6, int32_t p7, t1177 ** p8, t1186 * p9, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (t29 *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (t1177 **)args[7], (t1186 *)args[8], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t29;
struct t295;
void* RuntimeInvoker_t40_t29_t29_t29_t44_t2072_t297_t326_t297_t1705 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int32_t p4, t465 * p5, int8_t p6, bool* p7, int8_t p8, t295 ** p9, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int32_t*)args[3]), (t465 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (t295 **)args[8], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t1177;
void* RuntimeInvoker_t40_t29_t390_t44_t44_t44_t29_t297_t44_t1922_t1921 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, t29 * p6, int8_t p7, int32_t p8, t1177 ** p9, t1186 * p10, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (t29 *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (t1177 **)args[8], (t1186 *)args[9], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t29;
struct t1576;
struct t781;
void* RuntimeInvoker_t44_t29_t44_t29_t44_t1715_t1715_t29_t2068_t1092_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, t29 * p3, int32_t p4, uint32_t* p5, uint32_t* p6, t29 * p7, t1576 ** p8, t781** p9, int8_t p10, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), (t29 *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (t29 *)args[6], (t1576 **)args[7], (t781**)args[8], *((int8_t*)args[9]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
void* RuntimeInvoker_t21_t44_t372_t372_t297_t297_t297_t297_t297_t297_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t44_t44_t326_t326_t297_t297_t1921 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, t1186 * p11, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (t1186 *)args[10], method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t44_TI;
struct t29;
struct t29;
struct t29;
struct t1576;
struct t781;
void* RuntimeInvoker_t44_t29_t44_t44_t29_t44_t1715_t1715_t29_t2068_t1092_t297 (MethodInfo* method, void* obj, void** args){
	typedef int32_t (*Func)(void* obj, t29 * p1, int32_t p2, int32_t p3, t29 * p4, int32_t p5, uint32_t* p6, uint32_t* p7, t29 * p8, t1576 ** p9, t781** p10, int8_t p11, MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (t29 *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (t29 *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (t29 *)args[7], (t1576 **)args[8], (t781**)args[9], *((int8_t*)args[10]), method);
	return Box(&t44_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t29_t297_t2072_t2073_t29_t44_t297_t326_t326 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, int8_t p4, t465 * p5, t1629 * p6, t29 * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], *((int8_t*)args[3]), (t465 *)args[4], (t1629 *)args[5], (t29 *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
void* RuntimeInvoker_t21_t372_t29_t44_t44_t44_t297_t297_t297_t297_t372_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int16_t p1, t29 * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

struct t29;
struct t29;
extern TypeInfo t29_TI;
struct t29;
void* RuntimeInvoker_t29_t372_t29_t44_t44_t44_t297_t297_t297_t297_t372_t297_t297 (MethodInfo* method, void* obj, void** args){
	typedef t29 * (*Func)(void* obj, int16_t p1, t29 * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, MethodInfo* method);
	t29 * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (t29 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

struct t29;
extern TypeInfo t21_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t21_t44_t29_t29_t44_t44_t44_t44_t44_t44_t44_t44_t44 (MethodInfo* method, void* obj, void** args){
	typedef void (*Func)(void* obj, int32_t p1, t29 * p2, t29 * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (t29 *)args[1], (t29 *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t132_t44_t22_t44_t297_t297_t44_t44_t44_t44_t297_t44_t17_t17_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, t132  p3, int32_t p4, float p5, int32_t p6, int8_t p7, int8_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, int8_t p13, int32_t p14, t17  p15, t17  p16, int8_t p17, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((t132 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int8_t*)args[12]), *((int32_t*)args[13]), *((t17 *)args[14]), *((t17 *)args[15]), *((int8_t*)args[16]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t132_t44_t22_t44_t297_t297_t44_t44_t44_t44_t297_t44_t22_t22_t22_t22_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, t132  p3, int32_t p4, float p5, int32_t p6, int8_t p7, int8_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, int8_t p13, int32_t p14, float p15, float p16, float p17, float p18, int8_t p19, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], *((t132 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int8_t*)args[12]), *((int32_t*)args[13]), *((float*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((int8_t*)args[18]), method);
	return Box(&t40_TI, &ret);
}

struct t29;
extern TypeInfo t40_TI;
struct t29;
struct t29;
struct t29;
void* RuntimeInvoker_t40_t29_t29_t29_t400_t44_t22_t44_t297_t297_t44_t44_t44_t44_t297_t44_t22_t22_t22_t22_t297 (MethodInfo* method, void* obj, void** args){
	typedef bool (*Func)(void* obj, t29 * p1, t29 * p2, t29 * p3, t132 * p4, int32_t p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (t29 *)args[0], (t29 *)args[1], (t29 *)args[2], (t132 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(&t40_TI, &ret);
}

