﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t465;
struct t1094;
struct t29;
struct t42;
struct t7;
struct t295;
struct t446;
struct t1296;
#include "t1292.h"
#include "t465.h"
#include "t1628.h"
#include "t816.h"
#include "t1126.h"
#include "t1627.h"
#include "t1093.h"
#include "t1629.h"

 void m9114 (t465 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9115 (t465 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9116 (t465 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9117 (t465 * __this, bool p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9118 (t465 * __this, int64_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9119 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9120 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m9121 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9122 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9123 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m9124 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m9125 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m9126 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9127 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9128 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m9129 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m9130 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9131 (t465 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m9132 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m9133 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m9134 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9135 (t29 * __this, int32_t p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9136 (t465 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9137 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9138 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9139 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9140 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9141 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9142 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m9143 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m2844 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m5222 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9144 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5170 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9145 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9146 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9147 (t465 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9148 (t465 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m4016 (t465 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5219 (t465 * __this, double p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9149 (t29 * __this, t465  p0, t465  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9150 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9151 (t465 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9152 (t465 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9153 (t29 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9154 (t29 * __this, t465  p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9155 (t29 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9156 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9157 (t465 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9158 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9159 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9160 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9161 (t29 * __this, t7* p0, t29 * p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9162 (t29 * __this, t7* p0, t29 * p1, int32_t p2, t465 * p3, t1629 * p4, bool p5, t295 ** p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t446* m9163 (t29 * __this, t1296 * p0, bool p1, t295 ** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9164 (t29 * __this, t7* p0, int32_t p1, int32_t p2, int32_t p3, bool p4, bool p5, int32_t* p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m9165 (t29 * __this, t7* p0, int32_t p1, t446* p2, t446* p3, bool p4, int32_t* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9166 (t29 * __this, t7* p0, int32_t p1, int32_t p2, t7* p3, int32_t* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9167 (t29 * __this, t7* p0, int32_t p1, int32_t p2, t1296 * p3, bool p4, int32_t* p5, int32_t* p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9168 (t29 * __this, t7* p0, int32_t p1, t1296 * p2, bool p3, int32_t* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9169 (t29 * __this, t7* p0, int32_t p1, t1296 * p2, bool p3, int32_t* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9170 (t29 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9171 (t29 * __this, t7* p0, t7* p1, t7* p2, bool p3, t465 * p4, t1629 * p5, t1296 * p6, int32_t p7, bool p8, bool* p9, bool* p10, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m5110 (t29 * __this, t7* p0, t7* p1, t29 * p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9172 (t29 * __this, t7* p0, t446* p1, t29 * p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9173 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9174 (t29 * __this, t7* p0, t446* p1, t1296 * p2, int32_t p3, t465 * p4, bool p5, bool* p6, bool p7, t295 ** p8, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9175 (t465 * __this, t816  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9176 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9177 (t465 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m9178 (t465 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m4080 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9179 (t465 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9180 (t29 * __this, t465  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9181 (t29 * __this, t465  p0, t465  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4129 (t29 * __this, t465  p0, t465  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4017 (t29 * __this, t465  p0, t465  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9182 (t29 * __this, t465  p0, t465  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4128 (t29 * __this, t465  p0, t465  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4127 (t29 * __this, t465  p0, t465  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m9183 (t29 * __this, t465  p0, t816  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
