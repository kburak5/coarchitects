﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3480;
struct t29;
struct t20;
#include "t1605.h"

 void m19347 (t3480 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19348 (t3480 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19349 (t3480 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19350 (t3480 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19351 (t3480 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
