﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1338;
struct t7;
struct t760;
struct t733;
struct t781;
struct t29;
#include "t1362.h"
#include "t735.h"

 void m7478 (t1338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7479 (t1338 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7480 (t1338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7481 (t1338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7482 (t1338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t760 * m7483 (t1338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7484 (t1338 * __this, t760 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7485 (t1338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7486 (t1338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m7487 (t1338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m7488 (t1338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7489 (t1338 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7490 (t1338 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7491 (t1338 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7492 (t1338 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
