﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3322;
struct t29;
struct t20;
#include "t1189.h"

 void m18492 (t3322 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18493 (t3322 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18494 (t3322 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18495 (t3322 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18496 (t3322 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
