﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t116;
struct t29;
struct t113;
struct t2367;
struct t20;
struct t136;
struct t2368;
struct t2369;
struct t2370;
struct t2366;
struct t2371;
struct t2372;
#include "t2373.h"

#include "t294MD.h"
#define m1405(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m12185(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m12186(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m12187(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m12188(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m12189(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m12190(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m12191(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m12192(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m12193(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m12194(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m12195(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m12196(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m12197(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m12198(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m12199(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m12200(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m12201(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m1408(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m12202(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m12203(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m12204(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m12205(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m12206(__this, method) (t2370 *)m10583_gshared((t294 *)__this, method)
#define m12207(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m12208(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m12209(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m12210(__this, p0, method) (t113 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m12211(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m12212(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2373  m12213 (t116 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m12214(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m12215(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m12216(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m12217(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m12218(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m12219(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m12220(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m12221(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m12222(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m12223(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m12224(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m12225(__this, method) (t2366*)m10619_gshared((t294 *)__this, method)
#define m12226(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m12227(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m12228(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1407(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1406(__this, p0, method) (t113 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m12229(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
