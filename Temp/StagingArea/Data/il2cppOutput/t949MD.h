﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t949;
struct t809;
struct t7;
struct t793;
struct t136;

 void m4549 (t949 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4550 (t949 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4551 (t949 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4552 (t949 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t809 * m4162 (t949 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
