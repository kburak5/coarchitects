﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3405;
struct t29;
struct t20;
#include "t1397.h"

 void m18895 (t3405 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18896 (t3405 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18897 (t3405 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18898 (t3405 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18899 (t3405 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
