﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t287;
struct t7;
#include "t287.h"
#include "t132.h"

 void m1537 (t287 * __this, uint8_t p0, uint8_t p1, uint8_t p2, uint8_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2336 (t287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t287  m1582 (t29 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m1538 (t29 * __this, t287  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
