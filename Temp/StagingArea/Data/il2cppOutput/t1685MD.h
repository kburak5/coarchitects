﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1685;
struct t1685_marshaled;

void t1685_marshal(const t1685& unmarshaled, t1685_marshaled& marshaled);
void t1685_marshal_back(const t1685_marshaled& marshaled, t1685& unmarshaled);
void t1685_marshal_cleanup(t1685_marshaled& marshaled);
