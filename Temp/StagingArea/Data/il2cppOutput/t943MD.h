﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t943;
struct t1577;
struct t1584;
struct t7;
struct t29;
struct t200;
struct t781;
struct t1305;
struct t316;

 void m8612 (t943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8613 (t943 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8614 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8615 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8616 (t943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1577 * m8617 (t943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8618 (t943 * __this, t1577 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1584 * m8619 (t943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8620 (t943 * __this, t1584 * p0, t1577 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8621 (t943 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8622 (t943 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8623 (t943 * __this, t200* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5158 (t943 * __this, t7* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m5199 (t943 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8624 (t943 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4280 (t943 * __this, t200* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t200* m8625 (t943 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1305 * m8626 (t943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8627 (t29 * __this, t7* p0, t316* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m8628 (t29 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m8629 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8630 (t943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8631 (t943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8632 (t943 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4092 (t943 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m4196 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m5157 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8633 (t29 * __this, int32_t* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m8634 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m8635 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m5166 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m4091 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m8636 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m8637 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m8638 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m8639 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t943 * m8640 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8641 (t943 * __this, uint16_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8642 (t943 * __this, uint16_t* p0, int32_t p1, uint8_t* p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
