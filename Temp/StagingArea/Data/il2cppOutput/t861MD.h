﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t861;

 void m3657 (t861 * __this, t861 * p0, int32_t p1, int32_t p2, bool p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3658 (t861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3659 (t861 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3660 (t861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3661 (t861 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3662 (t861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3663 (t861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3664 (t861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3665 (t861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t861 * m3666 (t861 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
