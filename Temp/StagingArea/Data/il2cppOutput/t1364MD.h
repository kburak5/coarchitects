﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1364;
struct t636;
struct t1365;
struct t316;
struct t634;
struct t633;
struct t446;
struct t29;
struct t42;
struct t537;
struct t638;
struct t1146;
struct t1366;
#include "t630.h"

 void m7493 (t1364 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m7494 (t1364 * __this, int32_t p0, t1365* p1, t316** p2, t634* p3, t633 * p4, t446* p5, t29 ** p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7495 (t1364 * __this, t446* p0, t316** p1, t636 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7496 (t29 * __this, t42 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7497 (t1364 * __this, t29 * p0, t42 * p1, t633 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7498 (t1364 * __this, t316** p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7499 (t29 * __this, t42 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7500 (t29 * __this, t537* p0, t638* p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m7501 (t1364 * __this, int32_t p0, t1365* p1, t537* p2, t634* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m7502 (t1364 * __this, int32_t p0, t1365* p1, t537* p2, t634* p3, bool p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t636 * m7503 (t1364 * __this, t636 * p0, t636 * p1, t537* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7504 (t1364 * __this, t42 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1146 * m7505 (t1364 * __this, int32_t p0, t1366* p1, t42 * p2, t537* p3, t634* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7506 (t29 * __this, t537* p0, t638* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7507 (t29 * __this, t42 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
