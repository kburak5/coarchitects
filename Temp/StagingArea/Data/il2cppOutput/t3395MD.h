﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3395;
struct t29;
struct t20;
#include "t1363.h"

 void m18845 (t3395 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18846 (t3395 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18847 (t3395 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18848 (t3395 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18849 (t3395 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
