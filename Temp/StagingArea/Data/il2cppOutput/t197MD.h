﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t197;
struct t202;
struct t7;
struct t15;
struct t155;
struct t189;
struct t191;
struct t193;
struct t136;
struct t6;
struct t204;
struct t204_marshaled;
struct t53;
struct t163;
struct t206;
struct t25;
#include "t132.h"
#include "t185.h"
#include "t188.h"
#include "t186.h"
#include "t205.h"
#include "t187.h"
#include "t17.h"
#include "t192.h"
#include "t140.h"
#include "t207.h"

 void m574 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m575 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t202 * m576 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m577 (t197 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m578 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m579 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m580 (t197 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m581 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m582 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m583 (t197 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t15 * m584 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m585 (t197 * __this, t15 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t155 * m586 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m587 (t197 * __this, t155 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t132  m588 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m589 (t197 * __this, t132  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t189 * m590 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m591 (t197 * __this, t189 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t191 * m592 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m593 (t197 * __this, t191 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t193 * m594 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m595 (t197 * __this, t193 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m596 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m597 (t197 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m598 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m599 (t197 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m600 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m601 (t197 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m602 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m603 (t197 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m604 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m605 (t197 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m606 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m607 (t197 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m608 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m609 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m610 (t197 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m611 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m612 (t197 * __this, int32_t* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m613 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m614 (t197 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m615 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m616 (t197 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m617 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m618 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m619 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m620 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m621 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m622 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m623 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m624 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m625 (t197 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m626 (t197 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m627 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m628 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m629 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m630 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m631 (t197 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m632 (t197 * __this, t17  p0, t202 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m633 (t197 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m634 (t197 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m635 (t197 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m636 (t197 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m637 (t197 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m638 (t197 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m639 (t197 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m640 (t197 * __this, t204 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m641 (t197 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m642 (t197 * __this, t204 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m643 (t197 * __this, t53 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m644 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m645 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m646 (t197 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m647 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m648 (t197 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m649 (t197 * __this, int32_t p0, t202 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m650 (t197 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m651 (t197 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m652 (t197 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m653 (t197 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m654 (t197 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m655 (t197 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m656 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m657 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m658 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m659 (t197 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m660 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m661 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m662 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m663 (t197 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m664 (t197 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m665 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m666 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m667 (t29 * __this, t202 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m668 (t29 * __this, t202 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m669 (t197 * __this, t202 * p0, int32_t p1, int32_t* p2, int32_t* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m670 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m671 (t197 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m672 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m673 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m674 (t197 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m675 (t197 * __this, t163 * p0, t17  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m676 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m677 (t197 * __this, int32_t p0, t202 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m678 (t197 * __this, t163 * p0, t17  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m679 (t197 * __this, t7* p0, int32_t p1, uint16_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m680 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m681 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m682 (t197 * __this, t53 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m683 (t197 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m684 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m685 (t197 * __this, t53 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m686 (t197 * __this, t53 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m687 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m688 (t197 * __this, t206* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m689 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m690 (t197 * __this, int32_t p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m691 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m692 (t197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
