﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t327;
struct t7;

 void m4287 (t327 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2838 (t327 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1438 (t327 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
