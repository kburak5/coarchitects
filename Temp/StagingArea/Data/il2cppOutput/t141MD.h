﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t141;
struct t145;
struct t25;

 void m379 (t141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m380 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t141 * m381 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m382 (t141 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m383 (t141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m384 (t29 * __this, t25 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m385 (t29 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m386 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m387 (t141 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m388 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m389 (t141 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m390 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m391 (t141 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m392 (t141 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m393 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m394 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m395 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m396 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
