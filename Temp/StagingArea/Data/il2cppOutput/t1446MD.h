﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1446;
struct t446;
struct t29;
struct t674;
struct t1453;
struct t136;
struct t721;
struct t7;
struct t20;
struct t722;

 void m7889 (t1446 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7890 (t1446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7891 (t1446 * __this, t446* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7892 (t1446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7893 (t1446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7894 (t1446 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7895 (t1446 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7896 (t1446 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7897 (t1446 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7898 (t1446 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7899 (t1446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7900 (t1446 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7901 (t1446 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7902 (t1446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7903 (t1446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7904 (t1446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7905 (t1446 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7906 (t1446 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
