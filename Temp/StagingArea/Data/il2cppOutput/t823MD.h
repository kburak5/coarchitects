﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t823;
struct t822;
struct t7;

 void m4561 (t823 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t822 * m4562 (t823 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t822 * m4187 (t823 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
