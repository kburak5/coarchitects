﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t367;
struct t7;
struct t295;

 void m2758 (t367 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2759 (t367 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2760 (t367 * __this, t7* p0, t295 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
