﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t830;
struct t7;

 void m3503 (t830 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3504 (t830 * __this, t7* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3505 (t830 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3506 (t830 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3507 (t830 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3508 (t830 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m3509 (t830 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
