﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1143;
struct t42;
struct t557;
#include "t1367.h"
#include "t1149.h"

 void m7521 (t1143 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m7522 (t1143 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7523 (t1143 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
