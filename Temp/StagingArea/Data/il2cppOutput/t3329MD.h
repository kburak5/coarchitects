﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3329;
struct t29;
struct t20;
#include "t1274.h"

 void m18517 (t3329 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18518 (t3329 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18519 (t3329 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18520 (t3329 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18521 (t3329 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
