﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1499;
struct t29;
struct t1304;
struct t1498;
struct t1483;
struct t1463;
#include "t1490.h"

 t29 * m8048 (t29 * __this, uint8_t p0, t1304 * p1, bool p2, t1498 * p3, t1483 * p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8049 (t29 * __this, uint8_t p0, t1304 * p1, bool p2, t1498 * p3, t29 * p4, t1483 * p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
