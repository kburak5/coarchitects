﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t944;
struct t7;
struct t731;
struct t945;
struct t781;
struct t997;
struct t984;
struct t993;
struct t793;
#include "t936.h"

 void m4477 (t944 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4093 (t944 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4094 (t944 * __this, t781* p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4478 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4479 (t944 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4480 (t944 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4481 (t944 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t731 * m4098 (t944 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t945 * m4095 (t944 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4482 (t944 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t997 * m4483 (t944 * __this, t7* p0, t781* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4484 (t944 * __this, t7* p0, t781* p1, int32_t p2, t781* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4485 (t944 * __this, t984 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t936  m4486 (t944 * __this, bool* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4487 (t944 * __this, t993 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4488 (t944 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4489 (t944 * __this, t781* p0, t781* p1, int32_t p2, t781* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4490 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
