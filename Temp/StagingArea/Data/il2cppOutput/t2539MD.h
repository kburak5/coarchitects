﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2539;
struct t29;
struct t3;

#include "t2195MD.h"
#define m13550(__this, method) (void)m10745_gshared((t2195 *)__this, method)
#define m13551(__this, method) (void)m10746_gshared((t29 *)__this, method)
#define m13552(__this, p0, p1, method) (int32_t)m10747_gshared((t2195 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m13553(__this, method) (t2539 *)m10748_gshared((t29 *)__this, method)
