﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t565;
struct t41;
struct t41_marshaled;
struct t7;
struct t555;
struct t556;
struct t566;
struct t557;
#include "t554.h"

 void m2796 (t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t41 * m2797 (t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m2798 (t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m2799 (t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t555 * m2800 (t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2801 (t565 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t556 * m2802 (t565 * __this, t566 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t556 * m2803 (t29 * __this, t41 * p0, t557 * p1, t555 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
