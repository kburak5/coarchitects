﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern TypeInfo t432_TI;
#include "t432.h"
#include "t432MD.h"
extern MethodInfo m2050_MI;
extern TypeInfo t434_TI;
#include "t434.h"
#include "t434MD.h"
extern MethodInfo m2052_MI;
extern TypeInfo t704_TI;
#include "t704.h"
#include "t704MD.h"
extern MethodInfo m3059_MI;
extern TypeInfo t705_TI;
#include "t705.h"
#include "t705MD.h"
extern MethodInfo m3060_MI;
extern TypeInfo t435_TI;
#include "t435.h"
#include "t435MD.h"
extern MethodInfo m2053_MI;
extern TypeInfo t706_TI;
#include "t706.h"
#include "t706MD.h"
extern MethodInfo m3061_MI;
extern TypeInfo t707_TI;
#include "t707.h"
#include "t707MD.h"
extern MethodInfo m3062_MI;
extern TypeInfo t46_TI;
#include "t46.h"
#include "t46MD.h"
extern MethodInfo m71_MI;
extern MethodInfo m72_MI;
extern TypeInfo t708_TI;
#include "t708.h"
#include "t708MD.h"
extern MethodInfo m3063_MI;
extern TypeInfo t709_TI;
#include "t709.h"
#include "t709MD.h"
extern MethodInfo m3064_MI;
extern TypeInfo t437_TI;
#include "t437.h"
#include "t437MD.h"
extern MethodInfo m2055_MI;
extern TypeInfo t710_TI;
#include "t710.h"
#include "t710MD.h"
extern MethodInfo m3065_MI;
extern TypeInfo t711_TI;
#include "t711.h"
#include "t711MD.h"
extern MethodInfo m3066_MI;
extern TypeInfo t433_TI;
#include "t433.h"
#include "t433MD.h"
extern MethodInfo m2051_MI;
extern TypeInfo t686_TI;
#include "t686.h"
#include "t686MD.h"
extern MethodInfo m3042_MI;
extern TypeInfo t712_TI;
#include "t712.h"
#include "t712MD.h"
extern MethodInfo m3067_MI;
extern TypeInfo t430_TI;
#include "t430.h"
#include "t430MD.h"
extern MethodInfo m2048_MI;
extern TypeInfo t429_TI;
#include "t429.h"
#include "t429MD.h"
extern MethodInfo m2047_MI;
void g_System_Core_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(t29 *) * cache->count, 0);
	{
		t432 * tmp;
		tmp = (t432 *)il2cpp_codegen_object_new (&t432_TI);
		m2050(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), &m2050_MI);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		t434 * tmp;
		tmp = (t434 *)il2cpp_codegen_object_new (&t434_TI);
		m2052(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), &m2052_MI);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		t704 * tmp;
		tmp = (t704 *)il2cpp_codegen_object_new (&t704_TI);
		m3059(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), &m3059_MI);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		t705 * tmp;
		tmp = (t705 *)il2cpp_codegen_object_new (&t705_TI);
		m3060(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), &m3060_MI);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		t435 * tmp;
		tmp = (t435 *)il2cpp_codegen_object_new (&t435_TI);
		m2053(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), &m2053_MI);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		t706 * tmp;
		tmp = (t706 *)il2cpp_codegen_object_new (&t706_TI);
		m3061(tmp, il2cpp_codegen_string_new_wrapper("en-US"), &m3061_MI);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		t707 * tmp;
		tmp = (t707 *)il2cpp_codegen_object_new (&t707_TI);
		m3062(tmp, true, &m3062_MI);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		t46 * tmp;
		tmp = (t46 *)il2cpp_codegen_object_new (&t46_TI);
		m71(tmp, &m71_MI);
		m72(tmp, true, &m72_MI);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		t708 * tmp;
		tmp = (t708 *)il2cpp_codegen_object_new (&t708_TI);
		m3063(tmp, 2, &m3063_MI);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		t709 * tmp;
		tmp = (t709 *)il2cpp_codegen_object_new (&t709_TI);
		m3064(tmp, 8, &m3064_MI);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		t437 * tmp;
		tmp = (t437 *)il2cpp_codegen_object_new (&t437_TI);
		m2055(tmp, false, &m2055_MI);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		t710 * tmp;
		tmp = (t710 *)il2cpp_codegen_object_new (&t710_TI);
		m3065(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), &m3065_MI);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		t711 * tmp;
		tmp = (t711 *)il2cpp_codegen_object_new (&t711_TI);
		m3066(tmp, true, &m3066_MI);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		t433 * tmp;
		tmp = (t433 *)il2cpp_codegen_object_new (&t433_TI);
		m2051(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), &m2051_MI);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		t686 * tmp;
		tmp = (t686 *)il2cpp_codegen_object_new (&t686_TI);
		m3042(tmp, &m3042_MI);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		t712 * tmp;
		tmp = (t712 *)il2cpp_codegen_object_new (&t712_TI);
		m3067(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), &m3067_MI);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		t430 * tmp;
		tmp = (t430 *)il2cpp_codegen_object_new (&t430_TI);
		m2048(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), &m2048_MI);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		t429 * tmp;
		tmp = (t429 *)il2cpp_codegen_object_new (&t429_TI);
		m2047(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), &m2047_MI);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache g_System_Core_Assembly__CustomAttributeCache = {
18,
NULL,
&g_System_Core_Assembly_CustomAttributesCacheGenerator
};
