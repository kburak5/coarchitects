﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1693;
struct t1693_marshaled;

void t1693_marshal(const t1693& unmarshaled, t1693_marshaled& marshaled);
void t1693_marshal_back(const t1693_marshaled& marshaled, t1693& unmarshaled);
void t1693_marshal_cleanup(t1693_marshaled& marshaled);
