﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1115;
struct t781;
struct t7;

 void m8455 (t1115 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8456 (t1115 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8457 (t1115 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8458 (t29 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1115 * m5202 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1115 * m8459 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
