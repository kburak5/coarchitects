﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2859;
struct t469;
struct t7;

 void m15539 (t2859 * __this, int32_t p0, t469 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15540 (t2859 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15541 (t2859 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t469 * m15542 (t2859 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15543 (t2859 * __this, t469 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m15544 (t2859 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
