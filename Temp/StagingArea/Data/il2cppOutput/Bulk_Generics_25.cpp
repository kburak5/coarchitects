﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t5942_TI;


#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Text.RegularExpressions.Interpreter/Mode>
extern Il2CppType t4607_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31016_GM;
MethodInfo m31016_MI = 
{
	"GetEnumerator", NULL, &t5942_TI, &t4607_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31016_GM};
static MethodInfo* t5942_MIs[] =
{
	&m31016_MI,
	NULL
};
extern TypeInfo t603_TI;
static TypeInfo* t5942_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5942_0_0_0;
extern Il2CppType t5942_1_0_0;
struct t5942;
extern Il2CppGenericClass t5942_GC;
TypeInfo t5942_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5942_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5942_TI, t5942_ITIs, NULL, &EmptyCustomAttributesCache, &t5942_TI, &t5942_0_0_0, &t5942_1_0_0, NULL, &t5942_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5941_TI;

#include "t862.h"
#include "t44.h"
#include "t21.h"


// Metadata Definition System.Collections.Generic.IList`1<System.Text.RegularExpressions.Interpreter/Mode>
extern MethodInfo m31017_MI;
extern MethodInfo m31018_MI;
static PropertyInfo t5941____Item_PropertyInfo = 
{
	&t5941_TI, "Item", &m31017_MI, &m31018_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5941_PIs[] =
{
	&t5941____Item_PropertyInfo,
	NULL
};
extern Il2CppType t862_0_0_0;
extern Il2CppType t862_0_0_0;
static ParameterInfo t5941_m31019_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t862_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31019_GM;
MethodInfo m31019_MI = 
{
	"IndexOf", NULL, &t5941_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5941_m31019_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31019_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t862_0_0_0;
static ParameterInfo t5941_m31020_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t862_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31020_GM;
MethodInfo m31020_MI = 
{
	"Insert", NULL, &t5941_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5941_m31020_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31020_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5941_m31021_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31021_GM;
MethodInfo m31021_MI = 
{
	"RemoveAt", NULL, &t5941_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5941_m31021_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31021_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5941_m31017_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t862_0_0_0;
extern void* RuntimeInvoker_t862_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31017_GM;
MethodInfo m31017_MI = 
{
	"get_Item", NULL, &t5941_TI, &t862_0_0_0, RuntimeInvoker_t862_t44, t5941_m31017_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31017_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t862_0_0_0;
static ParameterInfo t5941_m31018_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t862_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31018_GM;
MethodInfo m31018_MI = 
{
	"set_Item", NULL, &t5941_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5941_m31018_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31018_GM};
static MethodInfo* t5941_MIs[] =
{
	&m31019_MI,
	&m31020_MI,
	&m31021_MI,
	&m31017_MI,
	&m31018_MI,
	NULL
};
extern TypeInfo t5940_TI;
static TypeInfo* t5941_ITIs[] = 
{
	&t603_TI,
	&t5940_TI,
	&t5942_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5941_0_0_0;
extern Il2CppType t5941_1_0_0;
struct t5941;
extern Il2CppGenericClass t5941_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5941_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5941_MIs, t5941_PIs, NULL, NULL, NULL, NULL, NULL, &t5941_TI, t5941_ITIs, NULL, &t1908__CustomAttributeCache, &t5941_TI, &t5941_0_0_0, &t5941_1_0_0, NULL, &t5941_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4609_TI;

#include "t895.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Uri/UriScheme>
extern MethodInfo m31022_MI;
static PropertyInfo t4609____Current_PropertyInfo = 
{
	&t4609_TI, "Current", &m31022_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4609_PIs[] =
{
	&t4609____Current_PropertyInfo,
	NULL
};
extern Il2CppType t895_0_0_0;
extern void* RuntimeInvoker_t895 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31022_GM;
MethodInfo m31022_MI = 
{
	"get_Current", NULL, &t4609_TI, &t895_0_0_0, RuntimeInvoker_t895, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31022_GM};
static MethodInfo* t4609_MIs[] =
{
	&m31022_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4609_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4609_0_0_0;
extern Il2CppType t4609_1_0_0;
struct t4609;
extern Il2CppGenericClass t4609_GC;
TypeInfo t4609_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4609_MIs, t4609_PIs, NULL, NULL, NULL, NULL, NULL, &t4609_TI, t4609_ITIs, NULL, &EmptyCustomAttributesCache, &t4609_TI, &t4609_0_0_0, &t4609_1_0_0, NULL, &t4609_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3234.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3234_TI;
#include "t3234MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
#include "t40.h"
extern TypeInfo t895_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m17970_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m23674_MI;
struct t20;
#include "t915.h"
 t895  m23674 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17966_MI;
 void m17966 (t3234 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17967_MI;
 t29 * m17967 (t3234 * __this, MethodInfo* method){
	{
		t895  L_0 = m17970(__this, &m17970_MI);
		t895  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t895_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17968_MI;
 void m17968 (t3234 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17969_MI;
 bool m17969 (t3234 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t895  m17970 (t3234 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t895  L_8 = m23674(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23674_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Uri/UriScheme>
extern Il2CppType t20_0_0_1;
FieldInfo t3234_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3234_TI, offsetof(t3234, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3234_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3234_TI, offsetof(t3234, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3234_FIs[] =
{
	&t3234_f0_FieldInfo,
	&t3234_f1_FieldInfo,
	NULL
};
static PropertyInfo t3234____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3234_TI, "System.Collections.IEnumerator.Current", &m17967_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3234____Current_PropertyInfo = 
{
	&t3234_TI, "Current", &m17970_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3234_PIs[] =
{
	&t3234____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3234____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3234_m17966_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17966_GM;
MethodInfo m17966_MI = 
{
	".ctor", (methodPointerType)&m17966, &t3234_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3234_m17966_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17966_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17967_GM;
MethodInfo m17967_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17967, &t3234_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17967_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17968_GM;
MethodInfo m17968_MI = 
{
	"Dispose", (methodPointerType)&m17968, &t3234_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17968_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17969_GM;
MethodInfo m17969_MI = 
{
	"MoveNext", (methodPointerType)&m17969, &t3234_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17969_GM};
extern Il2CppType t895_0_0_0;
extern void* RuntimeInvoker_t895 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17970_GM;
MethodInfo m17970_MI = 
{
	"get_Current", (methodPointerType)&m17970, &t3234_TI, &t895_0_0_0, RuntimeInvoker_t895, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17970_GM};
static MethodInfo* t3234_MIs[] =
{
	&m17966_MI,
	&m17967_MI,
	&m17968_MI,
	&m17969_MI,
	&m17970_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t3234_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17967_MI,
	&m17969_MI,
	&m17968_MI,
	&m17970_MI,
};
static TypeInfo* t3234_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4609_TI,
};
static Il2CppInterfaceOffsetPair t3234_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4609_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3234_0_0_0;
extern Il2CppType t3234_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3234_GC;
extern TypeInfo t20_TI;
TypeInfo t3234_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3234_MIs, t3234_PIs, t3234_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3234_TI, t3234_ITIs, t3234_VT, &EmptyCustomAttributesCache, &t3234_TI, &t3234_0_0_0, &t3234_1_0_0, t3234_IOs, &t3234_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3234)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5943_TI;

#include "System_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Uri/UriScheme>
extern MethodInfo m31023_MI;
static PropertyInfo t5943____Count_PropertyInfo = 
{
	&t5943_TI, "Count", &m31023_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31024_MI;
static PropertyInfo t5943____IsReadOnly_PropertyInfo = 
{
	&t5943_TI, "IsReadOnly", &m31024_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5943_PIs[] =
{
	&t5943____Count_PropertyInfo,
	&t5943____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31023_GM;
MethodInfo m31023_MI = 
{
	"get_Count", NULL, &t5943_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31023_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31024_GM;
MethodInfo m31024_MI = 
{
	"get_IsReadOnly", NULL, &t5943_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31024_GM};
extern Il2CppType t895_0_0_0;
extern Il2CppType t895_0_0_0;
static ParameterInfo t5943_m31025_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t895_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t895 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31025_GM;
MethodInfo m31025_MI = 
{
	"Add", NULL, &t5943_TI, &t21_0_0_0, RuntimeInvoker_t21_t895, t5943_m31025_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31025_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31026_GM;
MethodInfo m31026_MI = 
{
	"Clear", NULL, &t5943_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31026_GM};
extern Il2CppType t895_0_0_0;
static ParameterInfo t5943_m31027_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t895_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t895 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31027_GM;
MethodInfo m31027_MI = 
{
	"Contains", NULL, &t5943_TI, &t40_0_0_0, RuntimeInvoker_t40_t895, t5943_m31027_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31027_GM};
extern Il2CppType t896_0_0_0;
extern Il2CppType t896_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5943_m31028_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t896_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31028_GM;
MethodInfo m31028_MI = 
{
	"CopyTo", NULL, &t5943_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5943_m31028_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31028_GM};
extern Il2CppType t895_0_0_0;
static ParameterInfo t5943_m31029_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t895_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t895 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31029_GM;
MethodInfo m31029_MI = 
{
	"Remove", NULL, &t5943_TI, &t40_0_0_0, RuntimeInvoker_t40_t895, t5943_m31029_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31029_GM};
static MethodInfo* t5943_MIs[] =
{
	&m31023_MI,
	&m31024_MI,
	&m31025_MI,
	&m31026_MI,
	&m31027_MI,
	&m31028_MI,
	&m31029_MI,
	NULL
};
extern TypeInfo t5945_TI;
static TypeInfo* t5943_ITIs[] = 
{
	&t603_TI,
	&t5945_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5943_0_0_0;
extern Il2CppType t5943_1_0_0;
struct t5943;
extern Il2CppGenericClass t5943_GC;
TypeInfo t5943_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5943_MIs, t5943_PIs, NULL, NULL, NULL, NULL, NULL, &t5943_TI, t5943_ITIs, NULL, &EmptyCustomAttributesCache, &t5943_TI, &t5943_0_0_0, &t5943_1_0_0, NULL, &t5943_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Uri/UriScheme>
extern Il2CppType t4609_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31030_GM;
MethodInfo m31030_MI = 
{
	"GetEnumerator", NULL, &t5945_TI, &t4609_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31030_GM};
static MethodInfo* t5945_MIs[] =
{
	&m31030_MI,
	NULL
};
static TypeInfo* t5945_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5945_0_0_0;
extern Il2CppType t5945_1_0_0;
struct t5945;
extern Il2CppGenericClass t5945_GC;
TypeInfo t5945_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5945_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5945_TI, t5945_ITIs, NULL, &EmptyCustomAttributesCache, &t5945_TI, &t5945_0_0_0, &t5945_1_0_0, NULL, &t5945_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5944_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Uri/UriScheme>
extern MethodInfo m31031_MI;
extern MethodInfo m31032_MI;
static PropertyInfo t5944____Item_PropertyInfo = 
{
	&t5944_TI, "Item", &m31031_MI, &m31032_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5944_PIs[] =
{
	&t5944____Item_PropertyInfo,
	NULL
};
extern Il2CppType t895_0_0_0;
static ParameterInfo t5944_m31033_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t895_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t895 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31033_GM;
MethodInfo m31033_MI = 
{
	"IndexOf", NULL, &t5944_TI, &t44_0_0_0, RuntimeInvoker_t44_t895, t5944_m31033_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31033_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t895_0_0_0;
static ParameterInfo t5944_m31034_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t895_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t895 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31034_GM;
MethodInfo m31034_MI = 
{
	"Insert", NULL, &t5944_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t895, t5944_m31034_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31034_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5944_m31035_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31035_GM;
MethodInfo m31035_MI = 
{
	"RemoveAt", NULL, &t5944_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5944_m31035_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31035_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5944_m31031_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t895_0_0_0;
extern void* RuntimeInvoker_t895_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31031_GM;
MethodInfo m31031_MI = 
{
	"get_Item", NULL, &t5944_TI, &t895_0_0_0, RuntimeInvoker_t895_t44, t5944_m31031_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31031_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t895_0_0_0;
static ParameterInfo t5944_m31032_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t895_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t895 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31032_GM;
MethodInfo m31032_MI = 
{
	"set_Item", NULL, &t5944_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t895, t5944_m31032_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31032_GM};
static MethodInfo* t5944_MIs[] =
{
	&m31033_MI,
	&m31034_MI,
	&m31035_MI,
	&m31031_MI,
	&m31032_MI,
	NULL
};
static TypeInfo* t5944_ITIs[] = 
{
	&t603_TI,
	&t5943_TI,
	&t5945_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5944_0_0_0;
extern Il2CppType t5944_1_0_0;
struct t5944;
extern Il2CppGenericClass t5944_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5944_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5944_MIs, t5944_PIs, NULL, NULL, NULL, NULL, NULL, &t5944_TI, t5944_ITIs, NULL, &t1908__CustomAttributeCache, &t5944_TI, &t5944_0_0_0, &t5944_1_0_0, NULL, &t5944_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4611_TI;

#include "t897.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.UriHostNameType>
extern MethodInfo m31036_MI;
static PropertyInfo t4611____Current_PropertyInfo = 
{
	&t4611_TI, "Current", &m31036_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4611_PIs[] =
{
	&t4611____Current_PropertyInfo,
	NULL
};
extern Il2CppType t897_0_0_0;
extern void* RuntimeInvoker_t897 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31036_GM;
MethodInfo m31036_MI = 
{
	"get_Current", NULL, &t4611_TI, &t897_0_0_0, RuntimeInvoker_t897, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31036_GM};
static MethodInfo* t4611_MIs[] =
{
	&m31036_MI,
	NULL
};
static TypeInfo* t4611_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4611_0_0_0;
extern Il2CppType t4611_1_0_0;
struct t4611;
extern Il2CppGenericClass t4611_GC;
TypeInfo t4611_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4611_MIs, t4611_PIs, NULL, NULL, NULL, NULL, NULL, &t4611_TI, t4611_ITIs, NULL, &EmptyCustomAttributesCache, &t4611_TI, &t4611_0_0_0, &t4611_1_0_0, NULL, &t4611_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3235.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3235_TI;
#include "t3235MD.h"

extern TypeInfo t897_TI;
extern MethodInfo m17975_MI;
extern MethodInfo m23685_MI;
struct t20;
 int32_t m23685 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17971_MI;
 void m17971 (t3235 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17972_MI;
 t29 * m17972 (t3235 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17975(__this, &m17975_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t897_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17973_MI;
 void m17973 (t3235 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17974_MI;
 bool m17974 (t3235 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17975 (t3235 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23685(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23685_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.UriHostNameType>
extern Il2CppType t20_0_0_1;
FieldInfo t3235_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3235_TI, offsetof(t3235, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3235_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3235_TI, offsetof(t3235, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3235_FIs[] =
{
	&t3235_f0_FieldInfo,
	&t3235_f1_FieldInfo,
	NULL
};
static PropertyInfo t3235____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3235_TI, "System.Collections.IEnumerator.Current", &m17972_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3235____Current_PropertyInfo = 
{
	&t3235_TI, "Current", &m17975_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3235_PIs[] =
{
	&t3235____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3235____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3235_m17971_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17971_GM;
MethodInfo m17971_MI = 
{
	".ctor", (methodPointerType)&m17971, &t3235_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3235_m17971_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17971_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17972_GM;
MethodInfo m17972_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17972, &t3235_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17972_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17973_GM;
MethodInfo m17973_MI = 
{
	"Dispose", (methodPointerType)&m17973, &t3235_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17973_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17974_GM;
MethodInfo m17974_MI = 
{
	"MoveNext", (methodPointerType)&m17974, &t3235_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17974_GM};
extern Il2CppType t897_0_0_0;
extern void* RuntimeInvoker_t897 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17975_GM;
MethodInfo m17975_MI = 
{
	"get_Current", (methodPointerType)&m17975, &t3235_TI, &t897_0_0_0, RuntimeInvoker_t897, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17975_GM};
static MethodInfo* t3235_MIs[] =
{
	&m17971_MI,
	&m17972_MI,
	&m17973_MI,
	&m17974_MI,
	&m17975_MI,
	NULL
};
static MethodInfo* t3235_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17972_MI,
	&m17974_MI,
	&m17973_MI,
	&m17975_MI,
};
static TypeInfo* t3235_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4611_TI,
};
static Il2CppInterfaceOffsetPair t3235_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4611_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3235_0_0_0;
extern Il2CppType t3235_1_0_0;
extern Il2CppGenericClass t3235_GC;
TypeInfo t3235_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3235_MIs, t3235_PIs, t3235_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3235_TI, t3235_ITIs, t3235_VT, &EmptyCustomAttributesCache, &t3235_TI, &t3235_0_0_0, &t3235_1_0_0, t3235_IOs, &t3235_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3235)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5946_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.UriHostNameType>
extern MethodInfo m31037_MI;
static PropertyInfo t5946____Count_PropertyInfo = 
{
	&t5946_TI, "Count", &m31037_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31038_MI;
static PropertyInfo t5946____IsReadOnly_PropertyInfo = 
{
	&t5946_TI, "IsReadOnly", &m31038_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5946_PIs[] =
{
	&t5946____Count_PropertyInfo,
	&t5946____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31037_GM;
MethodInfo m31037_MI = 
{
	"get_Count", NULL, &t5946_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31037_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31038_GM;
MethodInfo m31038_MI = 
{
	"get_IsReadOnly", NULL, &t5946_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31038_GM};
extern Il2CppType t897_0_0_0;
extern Il2CppType t897_0_0_0;
static ParameterInfo t5946_m31039_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t897_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31039_GM;
MethodInfo m31039_MI = 
{
	"Add", NULL, &t5946_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5946_m31039_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31039_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31040_GM;
MethodInfo m31040_MI = 
{
	"Clear", NULL, &t5946_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31040_GM};
extern Il2CppType t897_0_0_0;
static ParameterInfo t5946_m31041_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t897_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31041_GM;
MethodInfo m31041_MI = 
{
	"Contains", NULL, &t5946_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5946_m31041_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31041_GM};
extern Il2CppType t3923_0_0_0;
extern Il2CppType t3923_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5946_m31042_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3923_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31042_GM;
MethodInfo m31042_MI = 
{
	"CopyTo", NULL, &t5946_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5946_m31042_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31042_GM};
extern Il2CppType t897_0_0_0;
static ParameterInfo t5946_m31043_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t897_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31043_GM;
MethodInfo m31043_MI = 
{
	"Remove", NULL, &t5946_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5946_m31043_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31043_GM};
static MethodInfo* t5946_MIs[] =
{
	&m31037_MI,
	&m31038_MI,
	&m31039_MI,
	&m31040_MI,
	&m31041_MI,
	&m31042_MI,
	&m31043_MI,
	NULL
};
extern TypeInfo t5948_TI;
static TypeInfo* t5946_ITIs[] = 
{
	&t603_TI,
	&t5948_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5946_0_0_0;
extern Il2CppType t5946_1_0_0;
struct t5946;
extern Il2CppGenericClass t5946_GC;
TypeInfo t5946_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5946_MIs, t5946_PIs, NULL, NULL, NULL, NULL, NULL, &t5946_TI, t5946_ITIs, NULL, &EmptyCustomAttributesCache, &t5946_TI, &t5946_0_0_0, &t5946_1_0_0, NULL, &t5946_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.UriHostNameType>
extern Il2CppType t4611_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31044_GM;
MethodInfo m31044_MI = 
{
	"GetEnumerator", NULL, &t5948_TI, &t4611_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31044_GM};
static MethodInfo* t5948_MIs[] =
{
	&m31044_MI,
	NULL
};
static TypeInfo* t5948_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5948_0_0_0;
extern Il2CppType t5948_1_0_0;
struct t5948;
extern Il2CppGenericClass t5948_GC;
TypeInfo t5948_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5948_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5948_TI, t5948_ITIs, NULL, &EmptyCustomAttributesCache, &t5948_TI, &t5948_0_0_0, &t5948_1_0_0, NULL, &t5948_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5947_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.UriHostNameType>
extern MethodInfo m31045_MI;
extern MethodInfo m31046_MI;
static PropertyInfo t5947____Item_PropertyInfo = 
{
	&t5947_TI, "Item", &m31045_MI, &m31046_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5947_PIs[] =
{
	&t5947____Item_PropertyInfo,
	NULL
};
extern Il2CppType t897_0_0_0;
static ParameterInfo t5947_m31047_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t897_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31047_GM;
MethodInfo m31047_MI = 
{
	"IndexOf", NULL, &t5947_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5947_m31047_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31047_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t897_0_0_0;
static ParameterInfo t5947_m31048_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t897_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31048_GM;
MethodInfo m31048_MI = 
{
	"Insert", NULL, &t5947_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5947_m31048_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31048_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5947_m31049_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31049_GM;
MethodInfo m31049_MI = 
{
	"RemoveAt", NULL, &t5947_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5947_m31049_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31049_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5947_m31045_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t897_0_0_0;
extern void* RuntimeInvoker_t897_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31045_GM;
MethodInfo m31045_MI = 
{
	"get_Item", NULL, &t5947_TI, &t897_0_0_0, RuntimeInvoker_t897_t44, t5947_m31045_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31045_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t897_0_0_0;
static ParameterInfo t5947_m31046_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t897_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31046_GM;
MethodInfo m31046_MI = 
{
	"set_Item", NULL, &t5947_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5947_m31046_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31046_GM};
static MethodInfo* t5947_MIs[] =
{
	&m31047_MI,
	&m31048_MI,
	&m31049_MI,
	&m31045_MI,
	&m31046_MI,
	NULL
};
static TypeInfo* t5947_ITIs[] = 
{
	&t603_TI,
	&t5946_TI,
	&t5948_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5947_0_0_0;
extern Il2CppType t5947_1_0_0;
struct t5947;
extern Il2CppGenericClass t5947_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5947_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5947_MIs, t5947_PIs, NULL, NULL, NULL, NULL, NULL, &t5947_TI, t5947_ITIs, NULL, &t1908__CustomAttributeCache, &t5947_TI, &t5947_0_0_0, &t5947_1_0_0, NULL, &t5947_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4613_TI;

#include "t899.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.UriKind>
extern MethodInfo m31050_MI;
static PropertyInfo t4613____Current_PropertyInfo = 
{
	&t4613_TI, "Current", &m31050_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4613_PIs[] =
{
	&t4613____Current_PropertyInfo,
	NULL
};
extern Il2CppType t899_0_0_0;
extern void* RuntimeInvoker_t899 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31050_GM;
MethodInfo m31050_MI = 
{
	"get_Current", NULL, &t4613_TI, &t899_0_0_0, RuntimeInvoker_t899, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31050_GM};
static MethodInfo* t4613_MIs[] =
{
	&m31050_MI,
	NULL
};
static TypeInfo* t4613_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4613_0_0_0;
extern Il2CppType t4613_1_0_0;
struct t4613;
extern Il2CppGenericClass t4613_GC;
TypeInfo t4613_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4613_MIs, t4613_PIs, NULL, NULL, NULL, NULL, NULL, &t4613_TI, t4613_ITIs, NULL, &EmptyCustomAttributesCache, &t4613_TI, &t4613_0_0_0, &t4613_1_0_0, NULL, &t4613_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3236.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3236_TI;
#include "t3236MD.h"

extern TypeInfo t899_TI;
extern MethodInfo m17980_MI;
extern MethodInfo m23696_MI;
struct t20;
 int32_t m23696 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17976_MI;
 void m17976 (t3236 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17977_MI;
 t29 * m17977 (t3236 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17980(__this, &m17980_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t899_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17978_MI;
 void m17978 (t3236 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17979_MI;
 bool m17979 (t3236 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17980 (t3236 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23696(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23696_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.UriKind>
extern Il2CppType t20_0_0_1;
FieldInfo t3236_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3236_TI, offsetof(t3236, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3236_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3236_TI, offsetof(t3236, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3236_FIs[] =
{
	&t3236_f0_FieldInfo,
	&t3236_f1_FieldInfo,
	NULL
};
static PropertyInfo t3236____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3236_TI, "System.Collections.IEnumerator.Current", &m17977_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3236____Current_PropertyInfo = 
{
	&t3236_TI, "Current", &m17980_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3236_PIs[] =
{
	&t3236____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3236____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3236_m17976_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17976_GM;
MethodInfo m17976_MI = 
{
	".ctor", (methodPointerType)&m17976, &t3236_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3236_m17976_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17976_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17977_GM;
MethodInfo m17977_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17977, &t3236_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17977_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17978_GM;
MethodInfo m17978_MI = 
{
	"Dispose", (methodPointerType)&m17978, &t3236_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17978_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17979_GM;
MethodInfo m17979_MI = 
{
	"MoveNext", (methodPointerType)&m17979, &t3236_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17979_GM};
extern Il2CppType t899_0_0_0;
extern void* RuntimeInvoker_t899 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17980_GM;
MethodInfo m17980_MI = 
{
	"get_Current", (methodPointerType)&m17980, &t3236_TI, &t899_0_0_0, RuntimeInvoker_t899, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17980_GM};
static MethodInfo* t3236_MIs[] =
{
	&m17976_MI,
	&m17977_MI,
	&m17978_MI,
	&m17979_MI,
	&m17980_MI,
	NULL
};
static MethodInfo* t3236_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17977_MI,
	&m17979_MI,
	&m17978_MI,
	&m17980_MI,
};
static TypeInfo* t3236_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4613_TI,
};
static Il2CppInterfaceOffsetPair t3236_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4613_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3236_0_0_0;
extern Il2CppType t3236_1_0_0;
extern Il2CppGenericClass t3236_GC;
TypeInfo t3236_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3236_MIs, t3236_PIs, t3236_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3236_TI, t3236_ITIs, t3236_VT, &EmptyCustomAttributesCache, &t3236_TI, &t3236_0_0_0, &t3236_1_0_0, t3236_IOs, &t3236_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3236)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5949_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.UriKind>
extern MethodInfo m31051_MI;
static PropertyInfo t5949____Count_PropertyInfo = 
{
	&t5949_TI, "Count", &m31051_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31052_MI;
static PropertyInfo t5949____IsReadOnly_PropertyInfo = 
{
	&t5949_TI, "IsReadOnly", &m31052_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5949_PIs[] =
{
	&t5949____Count_PropertyInfo,
	&t5949____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31051_GM;
MethodInfo m31051_MI = 
{
	"get_Count", NULL, &t5949_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31051_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31052_GM;
MethodInfo m31052_MI = 
{
	"get_IsReadOnly", NULL, &t5949_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31052_GM};
extern Il2CppType t899_0_0_0;
extern Il2CppType t899_0_0_0;
static ParameterInfo t5949_m31053_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t899_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31053_GM;
MethodInfo m31053_MI = 
{
	"Add", NULL, &t5949_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5949_m31053_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31053_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31054_GM;
MethodInfo m31054_MI = 
{
	"Clear", NULL, &t5949_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31054_GM};
extern Il2CppType t899_0_0_0;
static ParameterInfo t5949_m31055_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t899_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31055_GM;
MethodInfo m31055_MI = 
{
	"Contains", NULL, &t5949_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5949_m31055_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31055_GM};
extern Il2CppType t3924_0_0_0;
extern Il2CppType t3924_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5949_m31056_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3924_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31056_GM;
MethodInfo m31056_MI = 
{
	"CopyTo", NULL, &t5949_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5949_m31056_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31056_GM};
extern Il2CppType t899_0_0_0;
static ParameterInfo t5949_m31057_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t899_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31057_GM;
MethodInfo m31057_MI = 
{
	"Remove", NULL, &t5949_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5949_m31057_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31057_GM};
static MethodInfo* t5949_MIs[] =
{
	&m31051_MI,
	&m31052_MI,
	&m31053_MI,
	&m31054_MI,
	&m31055_MI,
	&m31056_MI,
	&m31057_MI,
	NULL
};
extern TypeInfo t5951_TI;
static TypeInfo* t5949_ITIs[] = 
{
	&t603_TI,
	&t5951_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5949_0_0_0;
extern Il2CppType t5949_1_0_0;
struct t5949;
extern Il2CppGenericClass t5949_GC;
TypeInfo t5949_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5949_MIs, t5949_PIs, NULL, NULL, NULL, NULL, NULL, &t5949_TI, t5949_ITIs, NULL, &EmptyCustomAttributesCache, &t5949_TI, &t5949_0_0_0, &t5949_1_0_0, NULL, &t5949_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.UriKind>
extern Il2CppType t4613_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31058_GM;
MethodInfo m31058_MI = 
{
	"GetEnumerator", NULL, &t5951_TI, &t4613_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31058_GM};
static MethodInfo* t5951_MIs[] =
{
	&m31058_MI,
	NULL
};
static TypeInfo* t5951_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5951_0_0_0;
extern Il2CppType t5951_1_0_0;
struct t5951;
extern Il2CppGenericClass t5951_GC;
TypeInfo t5951_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5951_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5951_TI, t5951_ITIs, NULL, &EmptyCustomAttributesCache, &t5951_TI, &t5951_0_0_0, &t5951_1_0_0, NULL, &t5951_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5950_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.UriKind>
extern MethodInfo m31059_MI;
extern MethodInfo m31060_MI;
static PropertyInfo t5950____Item_PropertyInfo = 
{
	&t5950_TI, "Item", &m31059_MI, &m31060_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5950_PIs[] =
{
	&t5950____Item_PropertyInfo,
	NULL
};
extern Il2CppType t899_0_0_0;
static ParameterInfo t5950_m31061_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t899_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31061_GM;
MethodInfo m31061_MI = 
{
	"IndexOf", NULL, &t5950_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5950_m31061_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31061_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t899_0_0_0;
static ParameterInfo t5950_m31062_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t899_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31062_GM;
MethodInfo m31062_MI = 
{
	"Insert", NULL, &t5950_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5950_m31062_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31062_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5950_m31063_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31063_GM;
MethodInfo m31063_MI = 
{
	"RemoveAt", NULL, &t5950_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5950_m31063_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31063_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5950_m31059_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t899_0_0_0;
extern void* RuntimeInvoker_t899_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31059_GM;
MethodInfo m31059_MI = 
{
	"get_Item", NULL, &t5950_TI, &t899_0_0_0, RuntimeInvoker_t899_t44, t5950_m31059_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31059_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t899_0_0_0;
static ParameterInfo t5950_m31060_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t899_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31060_GM;
MethodInfo m31060_MI = 
{
	"set_Item", NULL, &t5950_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5950_m31060_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31060_GM};
static MethodInfo* t5950_MIs[] =
{
	&m31061_MI,
	&m31062_MI,
	&m31063_MI,
	&m31059_MI,
	&m31060_MI,
	NULL
};
static TypeInfo* t5950_ITIs[] = 
{
	&t603_TI,
	&t5949_TI,
	&t5951_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5950_0_0_0;
extern Il2CppType t5950_1_0_0;
struct t5950;
extern Il2CppGenericClass t5950_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5950_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5950_MIs, t5950_PIs, NULL, NULL, NULL, NULL, NULL, &t5950_TI, t5950_ITIs, NULL, &t1908__CustomAttributeCache, &t5950_TI, &t5950_0_0_0, &t5950_1_0_0, NULL, &t5950_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4615_TI;

#include "t898.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.UriPartial>
extern MethodInfo m31064_MI;
static PropertyInfo t4615____Current_PropertyInfo = 
{
	&t4615_TI, "Current", &m31064_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4615_PIs[] =
{
	&t4615____Current_PropertyInfo,
	NULL
};
extern Il2CppType t898_0_0_0;
extern void* RuntimeInvoker_t898 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31064_GM;
MethodInfo m31064_MI = 
{
	"get_Current", NULL, &t4615_TI, &t898_0_0_0, RuntimeInvoker_t898, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31064_GM};
static MethodInfo* t4615_MIs[] =
{
	&m31064_MI,
	NULL
};
static TypeInfo* t4615_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4615_0_0_0;
extern Il2CppType t4615_1_0_0;
struct t4615;
extern Il2CppGenericClass t4615_GC;
TypeInfo t4615_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4615_MIs, t4615_PIs, NULL, NULL, NULL, NULL, NULL, &t4615_TI, t4615_ITIs, NULL, &EmptyCustomAttributesCache, &t4615_TI, &t4615_0_0_0, &t4615_1_0_0, NULL, &t4615_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3237.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3237_TI;
#include "t3237MD.h"

extern TypeInfo t898_TI;
extern MethodInfo m17985_MI;
extern MethodInfo m23707_MI;
struct t20;
 int32_t m23707 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17981_MI;
 void m17981 (t3237 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17982_MI;
 t29 * m17982 (t3237 * __this, MethodInfo* method){
	{
		int32_t L_0 = m17985(__this, &m17985_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t898_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17983_MI;
 void m17983 (t3237 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17984_MI;
 bool m17984 (t3237 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m17985 (t3237 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23707(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23707_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.UriPartial>
extern Il2CppType t20_0_0_1;
FieldInfo t3237_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3237_TI, offsetof(t3237, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3237_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3237_TI, offsetof(t3237, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3237_FIs[] =
{
	&t3237_f0_FieldInfo,
	&t3237_f1_FieldInfo,
	NULL
};
static PropertyInfo t3237____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3237_TI, "System.Collections.IEnumerator.Current", &m17982_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3237____Current_PropertyInfo = 
{
	&t3237_TI, "Current", &m17985_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3237_PIs[] =
{
	&t3237____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3237____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3237_m17981_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17981_GM;
MethodInfo m17981_MI = 
{
	".ctor", (methodPointerType)&m17981, &t3237_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3237_m17981_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17981_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17982_GM;
MethodInfo m17982_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17982, &t3237_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17982_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17983_GM;
MethodInfo m17983_MI = 
{
	"Dispose", (methodPointerType)&m17983, &t3237_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17983_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17984_GM;
MethodInfo m17984_MI = 
{
	"MoveNext", (methodPointerType)&m17984, &t3237_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17984_GM};
extern Il2CppType t898_0_0_0;
extern void* RuntimeInvoker_t898 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17985_GM;
MethodInfo m17985_MI = 
{
	"get_Current", (methodPointerType)&m17985, &t3237_TI, &t898_0_0_0, RuntimeInvoker_t898, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17985_GM};
static MethodInfo* t3237_MIs[] =
{
	&m17981_MI,
	&m17982_MI,
	&m17983_MI,
	&m17984_MI,
	&m17985_MI,
	NULL
};
static MethodInfo* t3237_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17982_MI,
	&m17984_MI,
	&m17983_MI,
	&m17985_MI,
};
static TypeInfo* t3237_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4615_TI,
};
static Il2CppInterfaceOffsetPair t3237_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4615_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3237_0_0_0;
extern Il2CppType t3237_1_0_0;
extern Il2CppGenericClass t3237_GC;
TypeInfo t3237_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3237_MIs, t3237_PIs, t3237_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3237_TI, t3237_ITIs, t3237_VT, &EmptyCustomAttributesCache, &t3237_TI, &t3237_0_0_0, &t3237_1_0_0, t3237_IOs, &t3237_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3237)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5952_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.UriPartial>
extern MethodInfo m31065_MI;
static PropertyInfo t5952____Count_PropertyInfo = 
{
	&t5952_TI, "Count", &m31065_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31066_MI;
static PropertyInfo t5952____IsReadOnly_PropertyInfo = 
{
	&t5952_TI, "IsReadOnly", &m31066_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5952_PIs[] =
{
	&t5952____Count_PropertyInfo,
	&t5952____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31065_GM;
MethodInfo m31065_MI = 
{
	"get_Count", NULL, &t5952_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31065_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31066_GM;
MethodInfo m31066_MI = 
{
	"get_IsReadOnly", NULL, &t5952_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31066_GM};
extern Il2CppType t898_0_0_0;
extern Il2CppType t898_0_0_0;
static ParameterInfo t5952_m31067_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t898_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31067_GM;
MethodInfo m31067_MI = 
{
	"Add", NULL, &t5952_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5952_m31067_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31067_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31068_GM;
MethodInfo m31068_MI = 
{
	"Clear", NULL, &t5952_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31068_GM};
extern Il2CppType t898_0_0_0;
static ParameterInfo t5952_m31069_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t898_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31069_GM;
MethodInfo m31069_MI = 
{
	"Contains", NULL, &t5952_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5952_m31069_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31069_GM};
extern Il2CppType t3925_0_0_0;
extern Il2CppType t3925_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5952_m31070_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3925_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31070_GM;
MethodInfo m31070_MI = 
{
	"CopyTo", NULL, &t5952_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5952_m31070_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31070_GM};
extern Il2CppType t898_0_0_0;
static ParameterInfo t5952_m31071_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t898_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31071_GM;
MethodInfo m31071_MI = 
{
	"Remove", NULL, &t5952_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5952_m31071_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31071_GM};
static MethodInfo* t5952_MIs[] =
{
	&m31065_MI,
	&m31066_MI,
	&m31067_MI,
	&m31068_MI,
	&m31069_MI,
	&m31070_MI,
	&m31071_MI,
	NULL
};
extern TypeInfo t5954_TI;
static TypeInfo* t5952_ITIs[] = 
{
	&t603_TI,
	&t5954_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5952_0_0_0;
extern Il2CppType t5952_1_0_0;
struct t5952;
extern Il2CppGenericClass t5952_GC;
TypeInfo t5952_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5952_MIs, t5952_PIs, NULL, NULL, NULL, NULL, NULL, &t5952_TI, t5952_ITIs, NULL, &EmptyCustomAttributesCache, &t5952_TI, &t5952_0_0_0, &t5952_1_0_0, NULL, &t5952_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.UriPartial>
extern Il2CppType t4615_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31072_GM;
MethodInfo m31072_MI = 
{
	"GetEnumerator", NULL, &t5954_TI, &t4615_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31072_GM};
static MethodInfo* t5954_MIs[] =
{
	&m31072_MI,
	NULL
};
static TypeInfo* t5954_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5954_0_0_0;
extern Il2CppType t5954_1_0_0;
struct t5954;
extern Il2CppGenericClass t5954_GC;
TypeInfo t5954_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5954_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5954_TI, t5954_ITIs, NULL, &EmptyCustomAttributesCache, &t5954_TI, &t5954_0_0_0, &t5954_1_0_0, NULL, &t5954_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5953_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.UriPartial>
extern MethodInfo m31073_MI;
extern MethodInfo m31074_MI;
static PropertyInfo t5953____Item_PropertyInfo = 
{
	&t5953_TI, "Item", &m31073_MI, &m31074_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5953_PIs[] =
{
	&t5953____Item_PropertyInfo,
	NULL
};
extern Il2CppType t898_0_0_0;
static ParameterInfo t5953_m31075_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t898_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31075_GM;
MethodInfo m31075_MI = 
{
	"IndexOf", NULL, &t5953_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5953_m31075_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31075_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t898_0_0_0;
static ParameterInfo t5953_m31076_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t898_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31076_GM;
MethodInfo m31076_MI = 
{
	"Insert", NULL, &t5953_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5953_m31076_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31076_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5953_m31077_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31077_GM;
MethodInfo m31077_MI = 
{
	"RemoveAt", NULL, &t5953_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5953_m31077_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31077_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5953_m31073_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t898_0_0_0;
extern void* RuntimeInvoker_t898_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31073_GM;
MethodInfo m31073_MI = 
{
	"get_Item", NULL, &t5953_TI, &t898_0_0_0, RuntimeInvoker_t898_t44, t5953_m31073_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31073_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t898_0_0_0;
static ParameterInfo t5953_m31074_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t898_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31074_GM;
MethodInfo m31074_MI = 
{
	"set_Item", NULL, &t5953_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5953_m31074_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31074_GM};
static MethodInfo* t5953_MIs[] =
{
	&m31075_MI,
	&m31076_MI,
	&m31077_MI,
	&m31073_MI,
	&m31074_MI,
	NULL
};
static TypeInfo* t5953_ITIs[] = 
{
	&t603_TI,
	&t5952_TI,
	&t5954_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5953_0_0_0;
extern Il2CppType t5953_1_0_0;
struct t5953;
extern Il2CppGenericClass t5953_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5953_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5953_MIs, t5953_PIs, NULL, NULL, NULL, NULL, NULL, &t5953_TI, t5953_ITIs, NULL, &t1908__CustomAttributeCache, &t5953_TI, &t5953_0_0_0, &t5953_1_0_0, NULL, &t5953_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4616_TI;

#include "t344.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.UInt32>
extern MethodInfo m31078_MI;
static PropertyInfo t4616____Current_PropertyInfo = 
{
	&t4616_TI, "Current", &m31078_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4616_PIs[] =
{
	&t4616____Current_PropertyInfo,
	NULL
};
extern Il2CppType t344_0_0_0;
extern void* RuntimeInvoker_t344 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31078_GM;
MethodInfo m31078_MI = 
{
	"get_Current", NULL, &t4616_TI, &t344_0_0_0, RuntimeInvoker_t344, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31078_GM};
static MethodInfo* t4616_MIs[] =
{
	&m31078_MI,
	NULL
};
static TypeInfo* t4616_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4616_0_0_0;
extern Il2CppType t4616_1_0_0;
struct t4616;
extern Il2CppGenericClass t4616_GC;
TypeInfo t4616_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4616_MIs, t4616_PIs, NULL, NULL, NULL, NULL, NULL, &t4616_TI, t4616_ITIs, NULL, &EmptyCustomAttributesCache, &t4616_TI, &t4616_0_0_0, &t4616_1_0_0, NULL, &t4616_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3238.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3238_TI;
#include "t3238MD.h"

extern TypeInfo t344_TI;
extern MethodInfo m17990_MI;
extern MethodInfo m23718_MI;
struct t20;
 uint32_t m23718 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m17986_MI;
 void m17986 (t3238 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17987_MI;
 t29 * m17987 (t3238 * __this, MethodInfo* method){
	{
		uint32_t L_0 = m17990(__this, &m17990_MI);
		uint32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t344_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m17988_MI;
 void m17988 (t3238 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m17989_MI;
 bool m17989 (t3238 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint32_t m17990 (t3238 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint32_t L_8 = m23718(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23718_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.UInt32>
extern Il2CppType t20_0_0_1;
FieldInfo t3238_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3238_TI, offsetof(t3238, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3238_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3238_TI, offsetof(t3238, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3238_FIs[] =
{
	&t3238_f0_FieldInfo,
	&t3238_f1_FieldInfo,
	NULL
};
static PropertyInfo t3238____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3238_TI, "System.Collections.IEnumerator.Current", &m17987_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3238____Current_PropertyInfo = 
{
	&t3238_TI, "Current", &m17990_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3238_PIs[] =
{
	&t3238____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3238____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3238_m17986_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17986_GM;
MethodInfo m17986_MI = 
{
	".ctor", (methodPointerType)&m17986, &t3238_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3238_m17986_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17986_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17987_GM;
MethodInfo m17987_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m17987, &t3238_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17987_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17988_GM;
MethodInfo m17988_MI = 
{
	"Dispose", (methodPointerType)&m17988, &t3238_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17988_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17989_GM;
MethodInfo m17989_MI = 
{
	"MoveNext", (methodPointerType)&m17989, &t3238_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17989_GM};
extern Il2CppType t344_0_0_0;
extern void* RuntimeInvoker_t344 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17990_GM;
MethodInfo m17990_MI = 
{
	"get_Current", (methodPointerType)&m17990, &t3238_TI, &t344_0_0_0, RuntimeInvoker_t344, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17990_GM};
static MethodInfo* t3238_MIs[] =
{
	&m17986_MI,
	&m17987_MI,
	&m17988_MI,
	&m17989_MI,
	&m17990_MI,
	NULL
};
static MethodInfo* t3238_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17987_MI,
	&m17989_MI,
	&m17988_MI,
	&m17990_MI,
};
static TypeInfo* t3238_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4616_TI,
};
static Il2CppInterfaceOffsetPair t3238_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4616_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3238_0_0_0;
extern Il2CppType t3238_1_0_0;
extern Il2CppGenericClass t3238_GC;
TypeInfo t3238_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3238_MIs, t3238_PIs, t3238_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3238_TI, t3238_ITIs, t3238_VT, &EmptyCustomAttributesCache, &t3238_TI, &t3238_0_0_0, &t3238_1_0_0, t3238_IOs, &t3238_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3238)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5955_TI;

#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.UInt32>
extern MethodInfo m31079_MI;
static PropertyInfo t5955____Count_PropertyInfo = 
{
	&t5955_TI, "Count", &m31079_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31080_MI;
static PropertyInfo t5955____IsReadOnly_PropertyInfo = 
{
	&t5955_TI, "IsReadOnly", &m31080_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5955_PIs[] =
{
	&t5955____Count_PropertyInfo,
	&t5955____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31079_GM;
MethodInfo m31079_MI = 
{
	"get_Count", NULL, &t5955_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31079_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31080_GM;
MethodInfo m31080_MI = 
{
	"get_IsReadOnly", NULL, &t5955_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31080_GM};
extern Il2CppType t344_0_0_0;
extern Il2CppType t344_0_0_0;
static ParameterInfo t5955_m31081_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t344_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31081_GM;
MethodInfo m31081_MI = 
{
	"Add", NULL, &t5955_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5955_m31081_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31081_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31082_GM;
MethodInfo m31082_MI = 
{
	"Clear", NULL, &t5955_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31082_GM};
extern Il2CppType t344_0_0_0;
static ParameterInfo t5955_m31083_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t344_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31083_GM;
MethodInfo m31083_MI = 
{
	"Contains", NULL, &t5955_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5955_m31083_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31083_GM};
extern Il2CppType t975_0_0_0;
extern Il2CppType t975_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5955_m31084_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t975_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31084_GM;
MethodInfo m31084_MI = 
{
	"CopyTo", NULL, &t5955_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5955_m31084_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31084_GM};
extern Il2CppType t344_0_0_0;
static ParameterInfo t5955_m31085_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t344_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31085_GM;
MethodInfo m31085_MI = 
{
	"Remove", NULL, &t5955_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5955_m31085_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31085_GM};
static MethodInfo* t5955_MIs[] =
{
	&m31079_MI,
	&m31080_MI,
	&m31081_MI,
	&m31082_MI,
	&m31083_MI,
	&m31084_MI,
	&m31085_MI,
	NULL
};
extern TypeInfo t5957_TI;
static TypeInfo* t5955_ITIs[] = 
{
	&t603_TI,
	&t5957_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5955_0_0_0;
extern Il2CppType t5955_1_0_0;
struct t5955;
extern Il2CppGenericClass t5955_GC;
TypeInfo t5955_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5955_MIs, t5955_PIs, NULL, NULL, NULL, NULL, NULL, &t5955_TI, t5955_ITIs, NULL, &EmptyCustomAttributesCache, &t5955_TI, &t5955_0_0_0, &t5955_1_0_0, NULL, &t5955_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.UInt32>
extern Il2CppType t4616_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31086_GM;
MethodInfo m31086_MI = 
{
	"GetEnumerator", NULL, &t5957_TI, &t4616_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31086_GM};
static MethodInfo* t5957_MIs[] =
{
	&m31086_MI,
	NULL
};
static TypeInfo* t5957_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5957_0_0_0;
extern Il2CppType t5957_1_0_0;
struct t5957;
extern Il2CppGenericClass t5957_GC;
TypeInfo t5957_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5957_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5957_TI, t5957_ITIs, NULL, &EmptyCustomAttributesCache, &t5957_TI, &t5957_0_0_0, &t5957_1_0_0, NULL, &t5957_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5956_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.UInt32>
extern MethodInfo m31087_MI;
extern MethodInfo m31088_MI;
static PropertyInfo t5956____Item_PropertyInfo = 
{
	&t5956_TI, "Item", &m31087_MI, &m31088_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5956_PIs[] =
{
	&t5956____Item_PropertyInfo,
	NULL
};
extern Il2CppType t344_0_0_0;
static ParameterInfo t5956_m31089_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t344_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31089_GM;
MethodInfo m31089_MI = 
{
	"IndexOf", NULL, &t5956_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5956_m31089_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31089_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t344_0_0_0;
static ParameterInfo t5956_m31090_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t344_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31090_GM;
MethodInfo m31090_MI = 
{
	"Insert", NULL, &t5956_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5956_m31090_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31090_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5956_m31091_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31091_GM;
MethodInfo m31091_MI = 
{
	"RemoveAt", NULL, &t5956_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5956_m31091_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31091_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5956_m31087_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t344_0_0_0;
extern void* RuntimeInvoker_t344_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31087_GM;
MethodInfo m31087_MI = 
{
	"get_Item", NULL, &t5956_TI, &t344_0_0_0, RuntimeInvoker_t344_t44, t5956_m31087_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31087_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t344_0_0_0;
static ParameterInfo t5956_m31088_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t344_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31088_GM;
MethodInfo m31088_MI = 
{
	"set_Item", NULL, &t5956_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5956_m31088_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31088_GM};
static MethodInfo* t5956_MIs[] =
{
	&m31089_MI,
	&m31090_MI,
	&m31091_MI,
	&m31087_MI,
	&m31088_MI,
	NULL
};
static TypeInfo* t5956_ITIs[] = 
{
	&t603_TI,
	&t5955_TI,
	&t5957_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5956_0_0_0;
extern Il2CppType t5956_1_0_0;
struct t5956;
extern Il2CppGenericClass t5956_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5956_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5956_MIs, t5956_PIs, NULL, NULL, NULL, NULL, NULL, &t5956_TI, t5956_ITIs, NULL, &t1908__CustomAttributeCache, &t5956_TI, &t5956_0_0_0, &t5956_1_0_0, NULL, &t5956_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5958_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.UInt32>>
extern MethodInfo m31092_MI;
static PropertyInfo t5958____Count_PropertyInfo = 
{
	&t5958_TI, "Count", &m31092_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31093_MI;
static PropertyInfo t5958____IsReadOnly_PropertyInfo = 
{
	&t5958_TI, "IsReadOnly", &m31093_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5958_PIs[] =
{
	&t5958____Count_PropertyInfo,
	&t5958____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31092_GM;
MethodInfo m31092_MI = 
{
	"get_Count", NULL, &t5958_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31092_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31093_GM;
MethodInfo m31093_MI = 
{
	"get_IsReadOnly", NULL, &t5958_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31093_GM};
extern Il2CppType t1716_0_0_0;
extern Il2CppType t1716_0_0_0;
static ParameterInfo t5958_m31094_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1716_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31094_GM;
MethodInfo m31094_MI = 
{
	"Add", NULL, &t5958_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5958_m31094_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31094_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31095_GM;
MethodInfo m31095_MI = 
{
	"Clear", NULL, &t5958_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31095_GM};
extern Il2CppType t1716_0_0_0;
static ParameterInfo t5958_m31096_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1716_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31096_GM;
MethodInfo m31096_MI = 
{
	"Contains", NULL, &t5958_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5958_m31096_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31096_GM};
extern Il2CppType t3556_0_0_0;
extern Il2CppType t3556_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5958_m31097_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3556_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31097_GM;
MethodInfo m31097_MI = 
{
	"CopyTo", NULL, &t5958_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5958_m31097_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31097_GM};
extern Il2CppType t1716_0_0_0;
static ParameterInfo t5958_m31098_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1716_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31098_GM;
MethodInfo m31098_MI = 
{
	"Remove", NULL, &t5958_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5958_m31098_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31098_GM};
static MethodInfo* t5958_MIs[] =
{
	&m31092_MI,
	&m31093_MI,
	&m31094_MI,
	&m31095_MI,
	&m31096_MI,
	&m31097_MI,
	&m31098_MI,
	NULL
};
extern TypeInfo t5960_TI;
static TypeInfo* t5958_ITIs[] = 
{
	&t603_TI,
	&t5960_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5958_0_0_0;
extern Il2CppType t5958_1_0_0;
struct t5958;
extern Il2CppGenericClass t5958_GC;
TypeInfo t5958_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5958_MIs, t5958_PIs, NULL, NULL, NULL, NULL, NULL, &t5958_TI, t5958_ITIs, NULL, &EmptyCustomAttributesCache, &t5958_TI, &t5958_0_0_0, &t5958_1_0_0, NULL, &t5958_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.UInt32>>
extern Il2CppType t4618_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31099_GM;
MethodInfo m31099_MI = 
{
	"GetEnumerator", NULL, &t5960_TI, &t4618_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31099_GM};
static MethodInfo* t5960_MIs[] =
{
	&m31099_MI,
	NULL
};
static TypeInfo* t5960_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5960_0_0_0;
extern Il2CppType t5960_1_0_0;
struct t5960;
extern Il2CppGenericClass t5960_GC;
TypeInfo t5960_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5960_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5960_TI, t5960_ITIs, NULL, &EmptyCustomAttributesCache, &t5960_TI, &t5960_0_0_0, &t5960_1_0_0, NULL, &t5960_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4618_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.UInt32>>
extern MethodInfo m31100_MI;
static PropertyInfo t4618____Current_PropertyInfo = 
{
	&t4618_TI, "Current", &m31100_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4618_PIs[] =
{
	&t4618____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1716_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31100_GM;
MethodInfo m31100_MI = 
{
	"get_Current", NULL, &t4618_TI, &t1716_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31100_GM};
static MethodInfo* t4618_MIs[] =
{
	&m31100_MI,
	NULL
};
static TypeInfo* t4618_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4618_0_0_0;
extern Il2CppType t4618_1_0_0;
struct t4618;
extern Il2CppGenericClass t4618_GC;
TypeInfo t4618_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4618_MIs, t4618_PIs, NULL, NULL, NULL, NULL, NULL, &t4618_TI, t4618_ITIs, NULL, &EmptyCustomAttributesCache, &t4618_TI, &t4618_0_0_0, &t4618_1_0_0, NULL, &t4618_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1716_TI;



// Metadata Definition System.IComparable`1<System.UInt32>
extern Il2CppType t344_0_0_0;
static ParameterInfo t1716_m31101_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t344_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31101_GM;
MethodInfo m31101_MI = 
{
	"CompareTo", NULL, &t1716_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t1716_m31101_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31101_GM};
static MethodInfo* t1716_MIs[] =
{
	&m31101_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1716_1_0_0;
struct t1716;
extern Il2CppGenericClass t1716_GC;
TypeInfo t1716_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t1716_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1716_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1716_TI, &t1716_0_0_0, &t1716_1_0_0, NULL, &t1716_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3239.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3239_TI;
#include "t3239MD.h"

extern MethodInfo m17995_MI;
extern MethodInfo m23729_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m23729(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.UInt32>>
extern Il2CppType t20_0_0_1;
FieldInfo t3239_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3239_TI, offsetof(t3239, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3239_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3239_TI, offsetof(t3239, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3239_FIs[] =
{
	&t3239_f0_FieldInfo,
	&t3239_f1_FieldInfo,
	NULL
};
extern MethodInfo m17992_MI;
static PropertyInfo t3239____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3239_TI, "System.Collections.IEnumerator.Current", &m17992_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3239____Current_PropertyInfo = 
{
	&t3239_TI, "Current", &m17995_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3239_PIs[] =
{
	&t3239____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3239____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3239_m17991_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17991_GM;
MethodInfo m17991_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3239_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3239_m17991_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17991_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17992_GM;
MethodInfo m17992_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3239_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17992_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17993_GM;
MethodInfo m17993_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3239_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17993_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17994_GM;
MethodInfo m17994_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3239_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17994_GM};
extern Il2CppType t1716_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17995_GM;
MethodInfo m17995_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3239_TI, &t1716_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17995_GM};
static MethodInfo* t3239_MIs[] =
{
	&m17991_MI,
	&m17992_MI,
	&m17993_MI,
	&m17994_MI,
	&m17995_MI,
	NULL
};
extern MethodInfo m17994_MI;
extern MethodInfo m17993_MI;
static MethodInfo* t3239_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17992_MI,
	&m17994_MI,
	&m17993_MI,
	&m17995_MI,
};
static TypeInfo* t3239_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4618_TI,
};
static Il2CppInterfaceOffsetPair t3239_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4618_TI, 7},
};
extern TypeInfo t1716_TI;
static Il2CppRGCTXData t3239_RGCTXData[3] = 
{
	&m17995_MI/* Method Usage */,
	&t1716_TI/* Class Usage */,
	&m23729_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3239_0_0_0;
extern Il2CppType t3239_1_0_0;
extern Il2CppGenericClass t3239_GC;
TypeInfo t3239_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3239_MIs, t3239_PIs, t3239_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3239_TI, t3239_ITIs, t3239_VT, &EmptyCustomAttributesCache, &t3239_TI, &t3239_0_0_0, &t3239_1_0_0, t3239_IOs, &t3239_GC, NULL, NULL, NULL, t3239_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3239)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5959_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.UInt32>>
extern MethodInfo m31102_MI;
extern MethodInfo m31103_MI;
static PropertyInfo t5959____Item_PropertyInfo = 
{
	&t5959_TI, "Item", &m31102_MI, &m31103_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5959_PIs[] =
{
	&t5959____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1716_0_0_0;
static ParameterInfo t5959_m31104_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1716_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31104_GM;
MethodInfo m31104_MI = 
{
	"IndexOf", NULL, &t5959_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5959_m31104_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31104_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1716_0_0_0;
static ParameterInfo t5959_m31105_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1716_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31105_GM;
MethodInfo m31105_MI = 
{
	"Insert", NULL, &t5959_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5959_m31105_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31105_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5959_m31106_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31106_GM;
MethodInfo m31106_MI = 
{
	"RemoveAt", NULL, &t5959_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5959_m31106_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31106_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5959_m31102_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1716_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31102_GM;
MethodInfo m31102_MI = 
{
	"get_Item", NULL, &t5959_TI, &t1716_0_0_0, RuntimeInvoker_t29_t44, t5959_m31102_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31102_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1716_0_0_0;
static ParameterInfo t5959_m31103_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1716_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31103_GM;
MethodInfo m31103_MI = 
{
	"set_Item", NULL, &t5959_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5959_m31103_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31103_GM};
static MethodInfo* t5959_MIs[] =
{
	&m31104_MI,
	&m31105_MI,
	&m31106_MI,
	&m31102_MI,
	&m31103_MI,
	NULL
};
static TypeInfo* t5959_ITIs[] = 
{
	&t603_TI,
	&t5958_TI,
	&t5960_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5959_0_0_0;
extern Il2CppType t5959_1_0_0;
struct t5959;
extern Il2CppGenericClass t5959_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5959_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5959_MIs, t5959_PIs, NULL, NULL, NULL, NULL, NULL, &t5959_TI, t5959_ITIs, NULL, &t1908__CustomAttributeCache, &t5959_TI, &t5959_0_0_0, &t5959_1_0_0, NULL, &t5959_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5961_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.UInt32>>
extern MethodInfo m31107_MI;
static PropertyInfo t5961____Count_PropertyInfo = 
{
	&t5961_TI, "Count", &m31107_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31108_MI;
static PropertyInfo t5961____IsReadOnly_PropertyInfo = 
{
	&t5961_TI, "IsReadOnly", &m31108_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5961_PIs[] =
{
	&t5961____Count_PropertyInfo,
	&t5961____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31107_GM;
MethodInfo m31107_MI = 
{
	"get_Count", NULL, &t5961_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31107_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31108_GM;
MethodInfo m31108_MI = 
{
	"get_IsReadOnly", NULL, &t5961_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31108_GM};
extern Il2CppType t1717_0_0_0;
extern Il2CppType t1717_0_0_0;
static ParameterInfo t5961_m31109_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1717_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31109_GM;
MethodInfo m31109_MI = 
{
	"Add", NULL, &t5961_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5961_m31109_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31109_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31110_GM;
MethodInfo m31110_MI = 
{
	"Clear", NULL, &t5961_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31110_GM};
extern Il2CppType t1717_0_0_0;
static ParameterInfo t5961_m31111_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1717_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31111_GM;
MethodInfo m31111_MI = 
{
	"Contains", NULL, &t5961_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5961_m31111_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31111_GM};
extern Il2CppType t3557_0_0_0;
extern Il2CppType t3557_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5961_m31112_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3557_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31112_GM;
MethodInfo m31112_MI = 
{
	"CopyTo", NULL, &t5961_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5961_m31112_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31112_GM};
extern Il2CppType t1717_0_0_0;
static ParameterInfo t5961_m31113_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1717_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31113_GM;
MethodInfo m31113_MI = 
{
	"Remove", NULL, &t5961_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5961_m31113_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31113_GM};
static MethodInfo* t5961_MIs[] =
{
	&m31107_MI,
	&m31108_MI,
	&m31109_MI,
	&m31110_MI,
	&m31111_MI,
	&m31112_MI,
	&m31113_MI,
	NULL
};
extern TypeInfo t5963_TI;
static TypeInfo* t5961_ITIs[] = 
{
	&t603_TI,
	&t5963_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5961_0_0_0;
extern Il2CppType t5961_1_0_0;
struct t5961;
extern Il2CppGenericClass t5961_GC;
TypeInfo t5961_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5961_MIs, t5961_PIs, NULL, NULL, NULL, NULL, NULL, &t5961_TI, t5961_ITIs, NULL, &EmptyCustomAttributesCache, &t5961_TI, &t5961_0_0_0, &t5961_1_0_0, NULL, &t5961_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.UInt32>>
extern Il2CppType t4620_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31114_GM;
MethodInfo m31114_MI = 
{
	"GetEnumerator", NULL, &t5963_TI, &t4620_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31114_GM};
static MethodInfo* t5963_MIs[] =
{
	&m31114_MI,
	NULL
};
static TypeInfo* t5963_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5963_0_0_0;
extern Il2CppType t5963_1_0_0;
struct t5963;
extern Il2CppGenericClass t5963_GC;
TypeInfo t5963_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5963_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5963_TI, t5963_ITIs, NULL, &EmptyCustomAttributesCache, &t5963_TI, &t5963_0_0_0, &t5963_1_0_0, NULL, &t5963_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4620_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.UInt32>>
extern MethodInfo m31115_MI;
static PropertyInfo t4620____Current_PropertyInfo = 
{
	&t4620_TI, "Current", &m31115_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4620_PIs[] =
{
	&t4620____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1717_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31115_GM;
MethodInfo m31115_MI = 
{
	"get_Current", NULL, &t4620_TI, &t1717_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31115_GM};
static MethodInfo* t4620_MIs[] =
{
	&m31115_MI,
	NULL
};
static TypeInfo* t4620_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4620_0_0_0;
extern Il2CppType t4620_1_0_0;
struct t4620;
extern Il2CppGenericClass t4620_GC;
TypeInfo t4620_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4620_MIs, t4620_PIs, NULL, NULL, NULL, NULL, NULL, &t4620_TI, t4620_ITIs, NULL, &EmptyCustomAttributesCache, &t4620_TI, &t4620_0_0_0, &t4620_1_0_0, NULL, &t4620_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t1717_TI;



// Metadata Definition System.IEquatable`1<System.UInt32>
extern Il2CppType t344_0_0_0;
static ParameterInfo t1717_m31116_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t344_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31116_GM;
MethodInfo m31116_MI = 
{
	"Equals", NULL, &t1717_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t1717_m31116_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31116_GM};
static MethodInfo* t1717_MIs[] =
{
	&m31116_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t1717_1_0_0;
struct t1717;
extern Il2CppGenericClass t1717_GC;
TypeInfo t1717_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t1717_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t1717_TI, NULL, NULL, &EmptyCustomAttributesCache, &t1717_TI, &t1717_0_0_0, &t1717_1_0_0, NULL, &t1717_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#include "t3240.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3240_TI;
#include "t3240MD.h"

extern MethodInfo m18000_MI;
extern MethodInfo m23740_MI;
struct t20;
#define m23740(__this, p0, method) (t29*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.UInt32>>
extern Il2CppType t20_0_0_1;
FieldInfo t3240_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3240_TI, offsetof(t3240, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3240_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3240_TI, offsetof(t3240, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3240_FIs[] =
{
	&t3240_f0_FieldInfo,
	&t3240_f1_FieldInfo,
	NULL
};
extern MethodInfo m17997_MI;
static PropertyInfo t3240____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3240_TI, "System.Collections.IEnumerator.Current", &m17997_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3240____Current_PropertyInfo = 
{
	&t3240_TI, "Current", &m18000_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3240_PIs[] =
{
	&t3240____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3240____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3240_m17996_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17996_GM;
MethodInfo m17996_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3240_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3240_m17996_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m17996_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17997_GM;
MethodInfo m17997_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3240_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17997_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17998_GM;
MethodInfo m17998_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3240_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17998_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m17999_GM;
MethodInfo m17999_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3240_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m17999_GM};
extern Il2CppType t1717_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18000_GM;
MethodInfo m18000_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3240_TI, &t1717_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18000_GM};
static MethodInfo* t3240_MIs[] =
{
	&m17996_MI,
	&m17997_MI,
	&m17998_MI,
	&m17999_MI,
	&m18000_MI,
	NULL
};
extern MethodInfo m17999_MI;
extern MethodInfo m17998_MI;
static MethodInfo* t3240_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m17997_MI,
	&m17999_MI,
	&m17998_MI,
	&m18000_MI,
};
static TypeInfo* t3240_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4620_TI,
};
static Il2CppInterfaceOffsetPair t3240_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4620_TI, 7},
};
extern TypeInfo t1717_TI;
static Il2CppRGCTXData t3240_RGCTXData[3] = 
{
	&m18000_MI/* Method Usage */,
	&t1717_TI/* Class Usage */,
	&m23740_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3240_0_0_0;
extern Il2CppType t3240_1_0_0;
extern Il2CppGenericClass t3240_GC;
TypeInfo t3240_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3240_MIs, t3240_PIs, t3240_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3240_TI, t3240_ITIs, t3240_VT, &EmptyCustomAttributesCache, &t3240_TI, &t3240_0_0_0, &t3240_1_0_0, t3240_IOs, &t3240_GC, NULL, NULL, NULL, t3240_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3240)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5962_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.UInt32>>
extern MethodInfo m31117_MI;
extern MethodInfo m31118_MI;
static PropertyInfo t5962____Item_PropertyInfo = 
{
	&t5962_TI, "Item", &m31117_MI, &m31118_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5962_PIs[] =
{
	&t5962____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1717_0_0_0;
static ParameterInfo t5962_m31119_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1717_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31119_GM;
MethodInfo m31119_MI = 
{
	"IndexOf", NULL, &t5962_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5962_m31119_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31119_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1717_0_0_0;
static ParameterInfo t5962_m31120_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1717_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31120_GM;
MethodInfo m31120_MI = 
{
	"Insert", NULL, &t5962_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5962_m31120_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31120_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5962_m31121_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31121_GM;
MethodInfo m31121_MI = 
{
	"RemoveAt", NULL, &t5962_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5962_m31121_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31121_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5962_m31117_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1717_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31117_GM;
MethodInfo m31117_MI = 
{
	"get_Item", NULL, &t5962_TI, &t1717_0_0_0, RuntimeInvoker_t29_t44, t5962_m31117_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31117_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1717_0_0_0;
static ParameterInfo t5962_m31118_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1717_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31118_GM;
MethodInfo m31118_MI = 
{
	"set_Item", NULL, &t5962_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5962_m31118_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31118_GM};
static MethodInfo* t5962_MIs[] =
{
	&m31119_MI,
	&m31120_MI,
	&m31121_MI,
	&m31117_MI,
	&m31118_MI,
	NULL
};
static TypeInfo* t5962_ITIs[] = 
{
	&t603_TI,
	&t5961_TI,
	&t5963_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5962_0_0_0;
extern Il2CppType t5962_1_0_0;
struct t5962;
extern Il2CppGenericClass t5962_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5962_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5962_MIs, t5962_PIs, NULL, NULL, NULL, NULL, NULL, &t5962_TI, t5962_ITIs, NULL, &t1908__CustomAttributeCache, &t5962_TI, &t5962_0_0_0, &t5962_1_0_0, NULL, &t5962_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4622_TI;

#include "t972.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Math.BigInteger>
extern MethodInfo m31122_MI;
static PropertyInfo t4622____Current_PropertyInfo = 
{
	&t4622_TI, "Current", &m31122_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4622_PIs[] =
{
	&t4622____Current_PropertyInfo,
	NULL
};
extern Il2CppType t972_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31122_GM;
MethodInfo m31122_MI = 
{
	"get_Current", NULL, &t4622_TI, &t972_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31122_GM};
static MethodInfo* t4622_MIs[] =
{
	&m31122_MI,
	NULL
};
static TypeInfo* t4622_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4622_0_0_0;
extern Il2CppType t4622_1_0_0;
struct t4622;
extern Il2CppGenericClass t4622_GC;
TypeInfo t4622_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4622_MIs, t4622_PIs, NULL, NULL, NULL, NULL, NULL, &t4622_TI, t4622_ITIs, NULL, &EmptyCustomAttributesCache, &t4622_TI, &t4622_0_0_0, &t4622_1_0_0, NULL, &t4622_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3241.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3241_TI;
#include "t3241MD.h"

extern TypeInfo t972_TI;
extern MethodInfo m18005_MI;
extern MethodInfo m23751_MI;
struct t20;
#define m23751(__this, p0, method) (t972 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<Mono.Math.BigInteger>
extern Il2CppType t20_0_0_1;
FieldInfo t3241_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3241_TI, offsetof(t3241, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3241_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3241_TI, offsetof(t3241, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3241_FIs[] =
{
	&t3241_f0_FieldInfo,
	&t3241_f1_FieldInfo,
	NULL
};
extern MethodInfo m18002_MI;
static PropertyInfo t3241____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3241_TI, "System.Collections.IEnumerator.Current", &m18002_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3241____Current_PropertyInfo = 
{
	&t3241_TI, "Current", &m18005_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3241_PIs[] =
{
	&t3241____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3241____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3241_m18001_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18001_GM;
MethodInfo m18001_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3241_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3241_m18001_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18001_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18002_GM;
MethodInfo m18002_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3241_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18002_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18003_GM;
MethodInfo m18003_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3241_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18003_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18004_GM;
MethodInfo m18004_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3241_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18004_GM};
extern Il2CppType t972_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18005_GM;
MethodInfo m18005_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3241_TI, &t972_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18005_GM};
static MethodInfo* t3241_MIs[] =
{
	&m18001_MI,
	&m18002_MI,
	&m18003_MI,
	&m18004_MI,
	&m18005_MI,
	NULL
};
extern MethodInfo m18004_MI;
extern MethodInfo m18003_MI;
static MethodInfo* t3241_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18002_MI,
	&m18004_MI,
	&m18003_MI,
	&m18005_MI,
};
static TypeInfo* t3241_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4622_TI,
};
static Il2CppInterfaceOffsetPair t3241_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4622_TI, 7},
};
extern TypeInfo t972_TI;
static Il2CppRGCTXData t3241_RGCTXData[3] = 
{
	&m18005_MI/* Method Usage */,
	&t972_TI/* Class Usage */,
	&m23751_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3241_0_0_0;
extern Il2CppType t3241_1_0_0;
extern Il2CppGenericClass t3241_GC;
TypeInfo t3241_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3241_MIs, t3241_PIs, t3241_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3241_TI, t3241_ITIs, t3241_VT, &EmptyCustomAttributesCache, &t3241_TI, &t3241_0_0_0, &t3241_1_0_0, t3241_IOs, &t3241_GC, NULL, NULL, NULL, t3241_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3241)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5964_TI;

#include "Mono.Security_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Math.BigInteger>
extern MethodInfo m31123_MI;
static PropertyInfo t5964____Count_PropertyInfo = 
{
	&t5964_TI, "Count", &m31123_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31124_MI;
static PropertyInfo t5964____IsReadOnly_PropertyInfo = 
{
	&t5964_TI, "IsReadOnly", &m31124_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5964_PIs[] =
{
	&t5964____Count_PropertyInfo,
	&t5964____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31123_GM;
MethodInfo m31123_MI = 
{
	"get_Count", NULL, &t5964_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31123_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31124_GM;
MethodInfo m31124_MI = 
{
	"get_IsReadOnly", NULL, &t5964_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31124_GM};
extern Il2CppType t972_0_0_0;
extern Il2CppType t972_0_0_0;
static ParameterInfo t5964_m31125_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t972_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31125_GM;
MethodInfo m31125_MI = 
{
	"Add", NULL, &t5964_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5964_m31125_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31125_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31126_GM;
MethodInfo m31126_MI = 
{
	"Clear", NULL, &t5964_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31126_GM};
extern Il2CppType t972_0_0_0;
static ParameterInfo t5964_m31127_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t972_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31127_GM;
MethodInfo m31127_MI = 
{
	"Contains", NULL, &t5964_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5964_m31127_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31127_GM};
extern Il2CppType t974_0_0_0;
extern Il2CppType t974_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5964_m31128_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t974_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31128_GM;
MethodInfo m31128_MI = 
{
	"CopyTo", NULL, &t5964_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5964_m31128_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31128_GM};
extern Il2CppType t972_0_0_0;
static ParameterInfo t5964_m31129_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t972_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31129_GM;
MethodInfo m31129_MI = 
{
	"Remove", NULL, &t5964_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5964_m31129_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31129_GM};
static MethodInfo* t5964_MIs[] =
{
	&m31123_MI,
	&m31124_MI,
	&m31125_MI,
	&m31126_MI,
	&m31127_MI,
	&m31128_MI,
	&m31129_MI,
	NULL
};
extern TypeInfo t5966_TI;
static TypeInfo* t5964_ITIs[] = 
{
	&t603_TI,
	&t5966_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5964_0_0_0;
extern Il2CppType t5964_1_0_0;
struct t5964;
extern Il2CppGenericClass t5964_GC;
TypeInfo t5964_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5964_MIs, t5964_PIs, NULL, NULL, NULL, NULL, NULL, &t5964_TI, t5964_ITIs, NULL, &EmptyCustomAttributesCache, &t5964_TI, &t5964_0_0_0, &t5964_1_0_0, NULL, &t5964_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger>
extern Il2CppType t4622_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31130_GM;
MethodInfo m31130_MI = 
{
	"GetEnumerator", NULL, &t5966_TI, &t4622_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31130_GM};
static MethodInfo* t5966_MIs[] =
{
	&m31130_MI,
	NULL
};
static TypeInfo* t5966_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5966_0_0_0;
extern Il2CppType t5966_1_0_0;
struct t5966;
extern Il2CppGenericClass t5966_GC;
TypeInfo t5966_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5966_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5966_TI, t5966_ITIs, NULL, &EmptyCustomAttributesCache, &t5966_TI, &t5966_0_0_0, &t5966_1_0_0, NULL, &t5966_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5965_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Math.BigInteger>
extern MethodInfo m31131_MI;
extern MethodInfo m31132_MI;
static PropertyInfo t5965____Item_PropertyInfo = 
{
	&t5965_TI, "Item", &m31131_MI, &m31132_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5965_PIs[] =
{
	&t5965____Item_PropertyInfo,
	NULL
};
extern Il2CppType t972_0_0_0;
static ParameterInfo t5965_m31133_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t972_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31133_GM;
MethodInfo m31133_MI = 
{
	"IndexOf", NULL, &t5965_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5965_m31133_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31133_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t972_0_0_0;
static ParameterInfo t5965_m31134_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t972_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31134_GM;
MethodInfo m31134_MI = 
{
	"Insert", NULL, &t5965_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5965_m31134_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31134_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5965_m31135_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31135_GM;
MethodInfo m31135_MI = 
{
	"RemoveAt", NULL, &t5965_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5965_m31135_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31135_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5965_m31131_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t972_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31131_GM;
MethodInfo m31131_MI = 
{
	"get_Item", NULL, &t5965_TI, &t972_0_0_0, RuntimeInvoker_t29_t44, t5965_m31131_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31131_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t972_0_0_0;
static ParameterInfo t5965_m31132_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t972_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31132_GM;
MethodInfo m31132_MI = 
{
	"set_Item", NULL, &t5965_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5965_m31132_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31132_GM};
static MethodInfo* t5965_MIs[] =
{
	&m31133_MI,
	&m31134_MI,
	&m31135_MI,
	&m31131_MI,
	&m31132_MI,
	NULL
};
static TypeInfo* t5965_ITIs[] = 
{
	&t603_TI,
	&t5964_TI,
	&t5966_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5965_0_0_0;
extern Il2CppType t5965_1_0_0;
struct t5965;
extern Il2CppGenericClass t5965_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5965_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5965_MIs, t5965_PIs, NULL, NULL, NULL, NULL, NULL, &t5965_TI, t5965_ITIs, NULL, &t1908__CustomAttributeCache, &t5965_TI, &t5965_0_0_0, &t5965_1_0_0, NULL, &t5965_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4624_TI;

#include "t970.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Math.BigInteger/Sign>
extern MethodInfo m31136_MI;
static PropertyInfo t4624____Current_PropertyInfo = 
{
	&t4624_TI, "Current", &m31136_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4624_PIs[] =
{
	&t4624____Current_PropertyInfo,
	NULL
};
extern Il2CppType t970_0_0_0;
extern void* RuntimeInvoker_t970 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31136_GM;
MethodInfo m31136_MI = 
{
	"get_Current", NULL, &t4624_TI, &t970_0_0_0, RuntimeInvoker_t970, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31136_GM};
static MethodInfo* t4624_MIs[] =
{
	&m31136_MI,
	NULL
};
static TypeInfo* t4624_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4624_0_0_0;
extern Il2CppType t4624_1_0_0;
struct t4624;
extern Il2CppGenericClass t4624_GC;
TypeInfo t4624_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4624_MIs, t4624_PIs, NULL, NULL, NULL, NULL, NULL, &t4624_TI, t4624_ITIs, NULL, &EmptyCustomAttributesCache, &t4624_TI, &t4624_0_0_0, &t4624_1_0_0, NULL, &t4624_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3242.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3242_TI;
#include "t3242MD.h"

extern TypeInfo t970_TI;
extern MethodInfo m18010_MI;
extern MethodInfo m23762_MI;
struct t20;
 int32_t m23762 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18006_MI;
 void m18006 (t3242 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18007_MI;
 t29 * m18007 (t3242 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18010(__this, &m18010_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t970_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18008_MI;
 void m18008 (t3242 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18009_MI;
 bool m18009 (t3242 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18010 (t3242 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23762(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23762_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Math.BigInteger/Sign>
extern Il2CppType t20_0_0_1;
FieldInfo t3242_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3242_TI, offsetof(t3242, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3242_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3242_TI, offsetof(t3242, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3242_FIs[] =
{
	&t3242_f0_FieldInfo,
	&t3242_f1_FieldInfo,
	NULL
};
static PropertyInfo t3242____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3242_TI, "System.Collections.IEnumerator.Current", &m18007_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3242____Current_PropertyInfo = 
{
	&t3242_TI, "Current", &m18010_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3242_PIs[] =
{
	&t3242____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3242____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3242_m18006_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18006_GM;
MethodInfo m18006_MI = 
{
	".ctor", (methodPointerType)&m18006, &t3242_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3242_m18006_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18006_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18007_GM;
MethodInfo m18007_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18007, &t3242_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18007_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18008_GM;
MethodInfo m18008_MI = 
{
	"Dispose", (methodPointerType)&m18008, &t3242_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18008_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18009_GM;
MethodInfo m18009_MI = 
{
	"MoveNext", (methodPointerType)&m18009, &t3242_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18009_GM};
extern Il2CppType t970_0_0_0;
extern void* RuntimeInvoker_t970 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18010_GM;
MethodInfo m18010_MI = 
{
	"get_Current", (methodPointerType)&m18010, &t3242_TI, &t970_0_0_0, RuntimeInvoker_t970, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18010_GM};
static MethodInfo* t3242_MIs[] =
{
	&m18006_MI,
	&m18007_MI,
	&m18008_MI,
	&m18009_MI,
	&m18010_MI,
	NULL
};
static MethodInfo* t3242_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18007_MI,
	&m18009_MI,
	&m18008_MI,
	&m18010_MI,
};
static TypeInfo* t3242_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4624_TI,
};
static Il2CppInterfaceOffsetPair t3242_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4624_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3242_0_0_0;
extern Il2CppType t3242_1_0_0;
extern Il2CppGenericClass t3242_GC;
TypeInfo t3242_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3242_MIs, t3242_PIs, t3242_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3242_TI, t3242_ITIs, t3242_VT, &EmptyCustomAttributesCache, &t3242_TI, &t3242_0_0_0, &t3242_1_0_0, t3242_IOs, &t3242_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3242)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5967_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Math.BigInteger/Sign>
extern MethodInfo m31137_MI;
static PropertyInfo t5967____Count_PropertyInfo = 
{
	&t5967_TI, "Count", &m31137_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31138_MI;
static PropertyInfo t5967____IsReadOnly_PropertyInfo = 
{
	&t5967_TI, "IsReadOnly", &m31138_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5967_PIs[] =
{
	&t5967____Count_PropertyInfo,
	&t5967____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31137_GM;
MethodInfo m31137_MI = 
{
	"get_Count", NULL, &t5967_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31137_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31138_GM;
MethodInfo m31138_MI = 
{
	"get_IsReadOnly", NULL, &t5967_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31138_GM};
extern Il2CppType t970_0_0_0;
extern Il2CppType t970_0_0_0;
static ParameterInfo t5967_m31139_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t970_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31139_GM;
MethodInfo m31139_MI = 
{
	"Add", NULL, &t5967_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5967_m31139_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31139_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31140_GM;
MethodInfo m31140_MI = 
{
	"Clear", NULL, &t5967_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31140_GM};
extern Il2CppType t970_0_0_0;
static ParameterInfo t5967_m31141_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t970_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31141_GM;
MethodInfo m31141_MI = 
{
	"Contains", NULL, &t5967_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5967_m31141_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31141_GM};
extern Il2CppType t3926_0_0_0;
extern Il2CppType t3926_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5967_m31142_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3926_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31142_GM;
MethodInfo m31142_MI = 
{
	"CopyTo", NULL, &t5967_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5967_m31142_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31142_GM};
extern Il2CppType t970_0_0_0;
static ParameterInfo t5967_m31143_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t970_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31143_GM;
MethodInfo m31143_MI = 
{
	"Remove", NULL, &t5967_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5967_m31143_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31143_GM};
static MethodInfo* t5967_MIs[] =
{
	&m31137_MI,
	&m31138_MI,
	&m31139_MI,
	&m31140_MI,
	&m31141_MI,
	&m31142_MI,
	&m31143_MI,
	NULL
};
extern TypeInfo t5969_TI;
static TypeInfo* t5967_ITIs[] = 
{
	&t603_TI,
	&t5969_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5967_0_0_0;
extern Il2CppType t5967_1_0_0;
struct t5967;
extern Il2CppGenericClass t5967_GC;
TypeInfo t5967_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5967_MIs, t5967_PIs, NULL, NULL, NULL, NULL, NULL, &t5967_TI, t5967_ITIs, NULL, &EmptyCustomAttributesCache, &t5967_TI, &t5967_0_0_0, &t5967_1_0_0, NULL, &t5967_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger/Sign>
extern Il2CppType t4624_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31144_GM;
MethodInfo m31144_MI = 
{
	"GetEnumerator", NULL, &t5969_TI, &t4624_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31144_GM};
static MethodInfo* t5969_MIs[] =
{
	&m31144_MI,
	NULL
};
static TypeInfo* t5969_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5969_0_0_0;
extern Il2CppType t5969_1_0_0;
struct t5969;
extern Il2CppGenericClass t5969_GC;
TypeInfo t5969_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5969_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5969_TI, t5969_ITIs, NULL, &EmptyCustomAttributesCache, &t5969_TI, &t5969_0_0_0, &t5969_1_0_0, NULL, &t5969_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5968_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>
extern MethodInfo m31145_MI;
extern MethodInfo m31146_MI;
static PropertyInfo t5968____Item_PropertyInfo = 
{
	&t5968_TI, "Item", &m31145_MI, &m31146_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5968_PIs[] =
{
	&t5968____Item_PropertyInfo,
	NULL
};
extern Il2CppType t970_0_0_0;
static ParameterInfo t5968_m31147_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t970_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31147_GM;
MethodInfo m31147_MI = 
{
	"IndexOf", NULL, &t5968_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5968_m31147_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31147_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t970_0_0_0;
static ParameterInfo t5968_m31148_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t970_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31148_GM;
MethodInfo m31148_MI = 
{
	"Insert", NULL, &t5968_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5968_m31148_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31148_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5968_m31149_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31149_GM;
MethodInfo m31149_MI = 
{
	"RemoveAt", NULL, &t5968_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5968_m31149_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31149_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5968_m31145_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t970_0_0_0;
extern void* RuntimeInvoker_t970_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31145_GM;
MethodInfo m31145_MI = 
{
	"get_Item", NULL, &t5968_TI, &t970_0_0_0, RuntimeInvoker_t970_t44, t5968_m31145_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31145_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t970_0_0_0;
static ParameterInfo t5968_m31146_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t970_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31146_GM;
MethodInfo m31146_MI = 
{
	"set_Item", NULL, &t5968_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5968_m31146_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31146_GM};
static MethodInfo* t5968_MIs[] =
{
	&m31147_MI,
	&m31148_MI,
	&m31149_MI,
	&m31145_MI,
	&m31146_MI,
	NULL
};
static TypeInfo* t5968_ITIs[] = 
{
	&t603_TI,
	&t5967_TI,
	&t5969_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5968_0_0_0;
extern Il2CppType t5968_1_0_0;
struct t5968;
extern Il2CppGenericClass t5968_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5968_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5968_MIs, t5968_PIs, NULL, NULL, NULL, NULL, NULL, &t5968_TI, t5968_ITIs, NULL, &t1908__CustomAttributeCache, &t5968_TI, &t5968_0_0_0, &t5968_1_0_0, NULL, &t5968_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4626_TI;

#include "t977.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
extern MethodInfo m31150_MI;
static PropertyInfo t4626____Current_PropertyInfo = 
{
	&t4626_TI, "Current", &m31150_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4626_PIs[] =
{
	&t4626____Current_PropertyInfo,
	NULL
};
extern Il2CppType t977_0_0_0;
extern void* RuntimeInvoker_t977 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31150_GM;
MethodInfo m31150_MI = 
{
	"get_Current", NULL, &t4626_TI, &t977_0_0_0, RuntimeInvoker_t977, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31150_GM};
static MethodInfo* t4626_MIs[] =
{
	&m31150_MI,
	NULL
};
static TypeInfo* t4626_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4626_0_0_0;
extern Il2CppType t4626_1_0_0;
struct t4626;
extern Il2CppGenericClass t4626_GC;
TypeInfo t4626_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4626_MIs, t4626_PIs, NULL, NULL, NULL, NULL, NULL, &t4626_TI, t4626_ITIs, NULL, &EmptyCustomAttributesCache, &t4626_TI, &t4626_0_0_0, &t4626_1_0_0, NULL, &t4626_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3243.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3243_TI;
#include "t3243MD.h"

extern TypeInfo t977_TI;
extern MethodInfo m18015_MI;
extern MethodInfo m23773_MI;
struct t20;
 int32_t m23773 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18011_MI;
 void m18011 (t3243 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18012_MI;
 t29 * m18012 (t3243 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18015(__this, &m18015_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t977_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18013_MI;
 void m18013 (t3243 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18014_MI;
 bool m18014 (t3243 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18015 (t3243 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23773(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23773_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
extern Il2CppType t20_0_0_1;
FieldInfo t3243_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3243_TI, offsetof(t3243, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3243_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3243_TI, offsetof(t3243, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3243_FIs[] =
{
	&t3243_f0_FieldInfo,
	&t3243_f1_FieldInfo,
	NULL
};
static PropertyInfo t3243____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3243_TI, "System.Collections.IEnumerator.Current", &m18012_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3243____Current_PropertyInfo = 
{
	&t3243_TI, "Current", &m18015_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3243_PIs[] =
{
	&t3243____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3243____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3243_m18011_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18011_GM;
MethodInfo m18011_MI = 
{
	".ctor", (methodPointerType)&m18011, &t3243_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3243_m18011_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18011_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18012_GM;
MethodInfo m18012_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18012, &t3243_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18012_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18013_GM;
MethodInfo m18013_MI = 
{
	"Dispose", (methodPointerType)&m18013, &t3243_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18013_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18014_GM;
MethodInfo m18014_MI = 
{
	"MoveNext", (methodPointerType)&m18014, &t3243_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18014_GM};
extern Il2CppType t977_0_0_0;
extern void* RuntimeInvoker_t977 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18015_GM;
MethodInfo m18015_MI = 
{
	"get_Current", (methodPointerType)&m18015, &t3243_TI, &t977_0_0_0, RuntimeInvoker_t977, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18015_GM};
static MethodInfo* t3243_MIs[] =
{
	&m18011_MI,
	&m18012_MI,
	&m18013_MI,
	&m18014_MI,
	&m18015_MI,
	NULL
};
static MethodInfo* t3243_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18012_MI,
	&m18014_MI,
	&m18013_MI,
	&m18015_MI,
};
static TypeInfo* t3243_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4626_TI,
};
static Il2CppInterfaceOffsetPair t3243_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4626_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3243_0_0_0;
extern Il2CppType t3243_1_0_0;
extern Il2CppGenericClass t3243_GC;
TypeInfo t3243_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3243_MIs, t3243_PIs, t3243_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3243_TI, t3243_ITIs, t3243_VT, &EmptyCustomAttributesCache, &t3243_TI, &t3243_0_0_0, &t3243_1_0_0, t3243_IOs, &t3243_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3243)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5970_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>
extern MethodInfo m31151_MI;
static PropertyInfo t5970____Count_PropertyInfo = 
{
	&t5970_TI, "Count", &m31151_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31152_MI;
static PropertyInfo t5970____IsReadOnly_PropertyInfo = 
{
	&t5970_TI, "IsReadOnly", &m31152_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5970_PIs[] =
{
	&t5970____Count_PropertyInfo,
	&t5970____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31151_GM;
MethodInfo m31151_MI = 
{
	"get_Count", NULL, &t5970_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31151_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31152_GM;
MethodInfo m31152_MI = 
{
	"get_IsReadOnly", NULL, &t5970_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31152_GM};
extern Il2CppType t977_0_0_0;
extern Il2CppType t977_0_0_0;
static ParameterInfo t5970_m31153_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t977_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31153_GM;
MethodInfo m31153_MI = 
{
	"Add", NULL, &t5970_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5970_m31153_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31153_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31154_GM;
MethodInfo m31154_MI = 
{
	"Clear", NULL, &t5970_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31154_GM};
extern Il2CppType t977_0_0_0;
static ParameterInfo t5970_m31155_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t977_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31155_GM;
MethodInfo m31155_MI = 
{
	"Contains", NULL, &t5970_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5970_m31155_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31155_GM};
extern Il2CppType t3927_0_0_0;
extern Il2CppType t3927_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5970_m31156_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3927_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31156_GM;
MethodInfo m31156_MI = 
{
	"CopyTo", NULL, &t5970_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5970_m31156_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31156_GM};
extern Il2CppType t977_0_0_0;
static ParameterInfo t5970_m31157_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t977_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31157_GM;
MethodInfo m31157_MI = 
{
	"Remove", NULL, &t5970_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5970_m31157_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31157_GM};
static MethodInfo* t5970_MIs[] =
{
	&m31151_MI,
	&m31152_MI,
	&m31153_MI,
	&m31154_MI,
	&m31155_MI,
	&m31156_MI,
	&m31157_MI,
	NULL
};
extern TypeInfo t5972_TI;
static TypeInfo* t5970_ITIs[] = 
{
	&t603_TI,
	&t5972_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5970_0_0_0;
extern Il2CppType t5970_1_0_0;
struct t5970;
extern Il2CppGenericClass t5970_GC;
TypeInfo t5970_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5970_MIs, t5970_PIs, NULL, NULL, NULL, NULL, NULL, &t5970_TI, t5970_ITIs, NULL, &EmptyCustomAttributesCache, &t5970_TI, &t5970_0_0_0, &t5970_1_0_0, NULL, &t5970_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Math.Prime.ConfidenceFactor>
extern Il2CppType t4626_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31158_GM;
MethodInfo m31158_MI = 
{
	"GetEnumerator", NULL, &t5972_TI, &t4626_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31158_GM};
static MethodInfo* t5972_MIs[] =
{
	&m31158_MI,
	NULL
};
static TypeInfo* t5972_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5972_0_0_0;
extern Il2CppType t5972_1_0_0;
struct t5972;
extern Il2CppGenericClass t5972_GC;
TypeInfo t5972_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5972_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5972_TI, t5972_ITIs, NULL, &EmptyCustomAttributesCache, &t5972_TI, &t5972_0_0_0, &t5972_1_0_0, NULL, &t5972_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5971_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>
extern MethodInfo m31159_MI;
extern MethodInfo m31160_MI;
static PropertyInfo t5971____Item_PropertyInfo = 
{
	&t5971_TI, "Item", &m31159_MI, &m31160_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5971_PIs[] =
{
	&t5971____Item_PropertyInfo,
	NULL
};
extern Il2CppType t977_0_0_0;
static ParameterInfo t5971_m31161_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t977_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31161_GM;
MethodInfo m31161_MI = 
{
	"IndexOf", NULL, &t5971_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5971_m31161_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31161_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t977_0_0_0;
static ParameterInfo t5971_m31162_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t977_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31162_GM;
MethodInfo m31162_MI = 
{
	"Insert", NULL, &t5971_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5971_m31162_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31162_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5971_m31163_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31163_GM;
MethodInfo m31163_MI = 
{
	"RemoveAt", NULL, &t5971_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5971_m31163_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31163_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5971_m31159_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t977_0_0_0;
extern void* RuntimeInvoker_t977_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31159_GM;
MethodInfo m31159_MI = 
{
	"get_Item", NULL, &t5971_TI, &t977_0_0_0, RuntimeInvoker_t977_t44, t5971_m31159_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31159_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t977_0_0_0;
static ParameterInfo t5971_m31160_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t977_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31160_GM;
MethodInfo m31160_MI = 
{
	"set_Item", NULL, &t5971_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5971_m31160_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31160_GM};
static MethodInfo* t5971_MIs[] =
{
	&m31161_MI,
	&m31162_MI,
	&m31163_MI,
	&m31159_MI,
	&m31160_MI,
	NULL
};
static TypeInfo* t5971_ITIs[] = 
{
	&t603_TI,
	&t5970_TI,
	&t5972_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5971_0_0_0;
extern Il2CppType t5971_1_0_0;
struct t5971;
extern Il2CppGenericClass t5971_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5971_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5971_MIs, t5971_PIs, NULL, NULL, NULL, NULL, NULL, &t5971_TI, t5971_ITIs, NULL, &t1908__CustomAttributeCache, &t5971_TI, &t5971_0_0_0, &t5971_1_0_0, NULL, &t5971_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4628_TI;

#include "t1100.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.KeySizes>
extern MethodInfo m31164_MI;
static PropertyInfo t4628____Current_PropertyInfo = 
{
	&t4628_TI, "Current", &m31164_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4628_PIs[] =
{
	&t4628____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1100_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31164_GM;
MethodInfo m31164_MI = 
{
	"get_Current", NULL, &t4628_TI, &t1100_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31164_GM};
static MethodInfo* t4628_MIs[] =
{
	&m31164_MI,
	NULL
};
static TypeInfo* t4628_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4628_0_0_0;
extern Il2CppType t4628_1_0_0;
struct t4628;
extern Il2CppGenericClass t4628_GC;
TypeInfo t4628_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4628_MIs, t4628_PIs, NULL, NULL, NULL, NULL, NULL, &t4628_TI, t4628_ITIs, NULL, &EmptyCustomAttributesCache, &t4628_TI, &t4628_0_0_0, &t4628_1_0_0, NULL, &t4628_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3244.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3244_TI;
#include "t3244MD.h"

extern TypeInfo t1100_TI;
extern MethodInfo m18020_MI;
extern MethodInfo m23784_MI;
struct t20;
#define m23784(__this, p0, method) (t1100 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>
extern Il2CppType t20_0_0_1;
FieldInfo t3244_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3244_TI, offsetof(t3244, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3244_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3244_TI, offsetof(t3244, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3244_FIs[] =
{
	&t3244_f0_FieldInfo,
	&t3244_f1_FieldInfo,
	NULL
};
extern MethodInfo m18017_MI;
static PropertyInfo t3244____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3244_TI, "System.Collections.IEnumerator.Current", &m18017_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3244____Current_PropertyInfo = 
{
	&t3244_TI, "Current", &m18020_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3244_PIs[] =
{
	&t3244____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3244____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3244_m18016_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18016_GM;
MethodInfo m18016_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3244_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3244_m18016_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18016_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18017_GM;
MethodInfo m18017_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3244_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18017_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18018_GM;
MethodInfo m18018_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3244_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18018_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18019_GM;
MethodInfo m18019_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3244_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18019_GM};
extern Il2CppType t1100_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18020_GM;
MethodInfo m18020_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3244_TI, &t1100_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18020_GM};
static MethodInfo* t3244_MIs[] =
{
	&m18016_MI,
	&m18017_MI,
	&m18018_MI,
	&m18019_MI,
	&m18020_MI,
	NULL
};
extern MethodInfo m18019_MI;
extern MethodInfo m18018_MI;
static MethodInfo* t3244_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18017_MI,
	&m18019_MI,
	&m18018_MI,
	&m18020_MI,
};
static TypeInfo* t3244_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4628_TI,
};
static Il2CppInterfaceOffsetPair t3244_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4628_TI, 7},
};
extern TypeInfo t1100_TI;
static Il2CppRGCTXData t3244_RGCTXData[3] = 
{
	&m18020_MI/* Method Usage */,
	&t1100_TI/* Class Usage */,
	&m23784_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3244_0_0_0;
extern Il2CppType t3244_1_0_0;
extern Il2CppGenericClass t3244_GC;
TypeInfo t3244_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3244_MIs, t3244_PIs, t3244_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3244_TI, t3244_ITIs, t3244_VT, &EmptyCustomAttributesCache, &t3244_TI, &t3244_0_0_0, &t3244_1_0_0, t3244_IOs, &t3244_GC, NULL, NULL, NULL, t3244_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3244)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5973_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>
extern MethodInfo m31165_MI;
static PropertyInfo t5973____Count_PropertyInfo = 
{
	&t5973_TI, "Count", &m31165_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31166_MI;
static PropertyInfo t5973____IsReadOnly_PropertyInfo = 
{
	&t5973_TI, "IsReadOnly", &m31166_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5973_PIs[] =
{
	&t5973____Count_PropertyInfo,
	&t5973____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31165_GM;
MethodInfo m31165_MI = 
{
	"get_Count", NULL, &t5973_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31165_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31166_GM;
MethodInfo m31166_MI = 
{
	"get_IsReadOnly", NULL, &t5973_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31166_GM};
extern Il2CppType t1100_0_0_0;
extern Il2CppType t1100_0_0_0;
static ParameterInfo t5973_m31167_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1100_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31167_GM;
MethodInfo m31167_MI = 
{
	"Add", NULL, &t5973_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5973_m31167_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31167_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31168_GM;
MethodInfo m31168_MI = 
{
	"Clear", NULL, &t5973_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31168_GM};
extern Il2CppType t1100_0_0_0;
static ParameterInfo t5973_m31169_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1100_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31169_GM;
MethodInfo m31169_MI = 
{
	"Contains", NULL, &t5973_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5973_m31169_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31169_GM};
extern Il2CppType t996_0_0_0;
extern Il2CppType t996_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5973_m31170_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t996_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31170_GM;
MethodInfo m31170_MI = 
{
	"CopyTo", NULL, &t5973_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5973_m31170_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31170_GM};
extern Il2CppType t1100_0_0_0;
static ParameterInfo t5973_m31171_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1100_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31171_GM;
MethodInfo m31171_MI = 
{
	"Remove", NULL, &t5973_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5973_m31171_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31171_GM};
static MethodInfo* t5973_MIs[] =
{
	&m31165_MI,
	&m31166_MI,
	&m31167_MI,
	&m31168_MI,
	&m31169_MI,
	&m31170_MI,
	&m31171_MI,
	NULL
};
extern TypeInfo t5975_TI;
static TypeInfo* t5973_ITIs[] = 
{
	&t603_TI,
	&t5975_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5973_0_0_0;
extern Il2CppType t5973_1_0_0;
struct t5973;
extern Il2CppGenericClass t5973_GC;
TypeInfo t5973_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5973_MIs, t5973_PIs, NULL, NULL, NULL, NULL, NULL, &t5973_TI, t5973_ITIs, NULL, &EmptyCustomAttributesCache, &t5973_TI, &t5973_0_0_0, &t5973_1_0_0, NULL, &t5973_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.KeySizes>
extern Il2CppType t4628_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31172_GM;
MethodInfo m31172_MI = 
{
	"GetEnumerator", NULL, &t5975_TI, &t4628_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31172_GM};
static MethodInfo* t5975_MIs[] =
{
	&m31172_MI,
	NULL
};
static TypeInfo* t5975_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5975_0_0_0;
extern Il2CppType t5975_1_0_0;
struct t5975;
extern Il2CppGenericClass t5975_GC;
TypeInfo t5975_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5975_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5975_TI, t5975_ITIs, NULL, &EmptyCustomAttributesCache, &t5975_TI, &t5975_0_0_0, &t5975_1_0_0, NULL, &t5975_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5974_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>
extern MethodInfo m31173_MI;
extern MethodInfo m31174_MI;
static PropertyInfo t5974____Item_PropertyInfo = 
{
	&t5974_TI, "Item", &m31173_MI, &m31174_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5974_PIs[] =
{
	&t5974____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1100_0_0_0;
static ParameterInfo t5974_m31175_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1100_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31175_GM;
MethodInfo m31175_MI = 
{
	"IndexOf", NULL, &t5974_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5974_m31175_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31175_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1100_0_0_0;
static ParameterInfo t5974_m31176_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1100_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31176_GM;
MethodInfo m31176_MI = 
{
	"Insert", NULL, &t5974_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5974_m31176_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31176_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5974_m31177_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31177_GM;
MethodInfo m31177_MI = 
{
	"RemoveAt", NULL, &t5974_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5974_m31177_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31177_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5974_m31173_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1100_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31173_GM;
MethodInfo m31173_MI = 
{
	"get_Item", NULL, &t5974_TI, &t1100_0_0_0, RuntimeInvoker_t29_t44, t5974_m31173_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31173_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1100_0_0_0;
static ParameterInfo t5974_m31174_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1100_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31174_GM;
MethodInfo m31174_MI = 
{
	"set_Item", NULL, &t5974_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5974_m31174_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31174_GM};
static MethodInfo* t5974_MIs[] =
{
	&m31175_MI,
	&m31176_MI,
	&m31177_MI,
	&m31173_MI,
	&m31174_MI,
	NULL
};
static TypeInfo* t5974_ITIs[] = 
{
	&t603_TI,
	&t5973_TI,
	&t5975_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5974_0_0_0;
extern Il2CppType t5974_1_0_0;
struct t5974;
extern Il2CppGenericClass t5974_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5974_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5974_MIs, t5974_PIs, NULL, NULL, NULL, NULL, NULL, &t5974_TI, t5974_ITIs, NULL, &t1908__CustomAttributeCache, &t5974_TI, &t5974_0_0_0, &t5974_1_0_0, NULL, &t5974_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4630_TI;

#include "t1003.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>
extern MethodInfo m31178_MI;
static PropertyInfo t4630____Current_PropertyInfo = 
{
	&t4630_TI, "Current", &m31178_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4630_PIs[] =
{
	&t4630____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1003_0_0_0;
extern void* RuntimeInvoker_t1003 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31178_GM;
MethodInfo m31178_MI = 
{
	"get_Current", NULL, &t4630_TI, &t1003_0_0_0, RuntimeInvoker_t1003, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31178_GM};
static MethodInfo* t4630_MIs[] =
{
	&m31178_MI,
	NULL
};
static TypeInfo* t4630_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4630_0_0_0;
extern Il2CppType t4630_1_0_0;
struct t4630;
extern Il2CppGenericClass t4630_GC;
TypeInfo t4630_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4630_MIs, t4630_PIs, NULL, NULL, NULL, NULL, NULL, &t4630_TI, t4630_ITIs, NULL, &EmptyCustomAttributesCache, &t4630_TI, &t4630_0_0_0, &t4630_1_0_0, NULL, &t4630_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3245.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3245_TI;
#include "t3245MD.h"

extern TypeInfo t1003_TI;
extern MethodInfo m18025_MI;
extern MethodInfo m23795_MI;
struct t20;
 int32_t m23795 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18021_MI;
 void m18021 (t3245 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18022_MI;
 t29 * m18022 (t3245 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18025(__this, &m18025_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1003_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18023_MI;
 void m18023 (t3245 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18024_MI;
 bool m18024 (t3245 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18025 (t3245 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23795(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23795_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3245_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3245_TI, offsetof(t3245, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3245_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3245_TI, offsetof(t3245, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3245_FIs[] =
{
	&t3245_f0_FieldInfo,
	&t3245_f1_FieldInfo,
	NULL
};
static PropertyInfo t3245____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3245_TI, "System.Collections.IEnumerator.Current", &m18022_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3245____Current_PropertyInfo = 
{
	&t3245_TI, "Current", &m18025_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3245_PIs[] =
{
	&t3245____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3245____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3245_m18021_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18021_GM;
MethodInfo m18021_MI = 
{
	".ctor", (methodPointerType)&m18021, &t3245_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3245_m18021_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18021_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18022_GM;
MethodInfo m18022_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18022, &t3245_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18022_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18023_GM;
MethodInfo m18023_MI = 
{
	"Dispose", (methodPointerType)&m18023, &t3245_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18023_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18024_GM;
MethodInfo m18024_MI = 
{
	"MoveNext", (methodPointerType)&m18024, &t3245_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18024_GM};
extern Il2CppType t1003_0_0_0;
extern void* RuntimeInvoker_t1003 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18025_GM;
MethodInfo m18025_MI = 
{
	"get_Current", (methodPointerType)&m18025, &t3245_TI, &t1003_0_0_0, RuntimeInvoker_t1003, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18025_GM};
static MethodInfo* t3245_MIs[] =
{
	&m18021_MI,
	&m18022_MI,
	&m18023_MI,
	&m18024_MI,
	&m18025_MI,
	NULL
};
static MethodInfo* t3245_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18022_MI,
	&m18024_MI,
	&m18023_MI,
	&m18025_MI,
};
static TypeInfo* t3245_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4630_TI,
};
static Il2CppInterfaceOffsetPair t3245_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4630_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3245_0_0_0;
extern Il2CppType t3245_1_0_0;
extern Il2CppGenericClass t3245_GC;
TypeInfo t3245_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3245_MIs, t3245_PIs, t3245_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3245_TI, t3245_ITIs, t3245_VT, &EmptyCustomAttributesCache, &t3245_TI, &t3245_0_0_0, &t3245_1_0_0, t3245_IOs, &t3245_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3245)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5976_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>
extern MethodInfo m31179_MI;
static PropertyInfo t5976____Count_PropertyInfo = 
{
	&t5976_TI, "Count", &m31179_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31180_MI;
static PropertyInfo t5976____IsReadOnly_PropertyInfo = 
{
	&t5976_TI, "IsReadOnly", &m31180_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5976_PIs[] =
{
	&t5976____Count_PropertyInfo,
	&t5976____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31179_GM;
MethodInfo m31179_MI = 
{
	"get_Count", NULL, &t5976_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31179_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31180_GM;
MethodInfo m31180_MI = 
{
	"get_IsReadOnly", NULL, &t5976_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31180_GM};
extern Il2CppType t1003_0_0_0;
extern Il2CppType t1003_0_0_0;
static ParameterInfo t5976_m31181_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1003_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31181_GM;
MethodInfo m31181_MI = 
{
	"Add", NULL, &t5976_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5976_m31181_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31181_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31182_GM;
MethodInfo m31182_MI = 
{
	"Clear", NULL, &t5976_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31182_GM};
extern Il2CppType t1003_0_0_0;
static ParameterInfo t5976_m31183_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1003_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31183_GM;
MethodInfo m31183_MI = 
{
	"Contains", NULL, &t5976_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5976_m31183_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31183_GM};
extern Il2CppType t3928_0_0_0;
extern Il2CppType t3928_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5976_m31184_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3928_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31184_GM;
MethodInfo m31184_MI = 
{
	"CopyTo", NULL, &t5976_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5976_m31184_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31184_GM};
extern Il2CppType t1003_0_0_0;
static ParameterInfo t5976_m31185_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1003_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31185_GM;
MethodInfo m31185_MI = 
{
	"Remove", NULL, &t5976_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5976_m31185_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31185_GM};
static MethodInfo* t5976_MIs[] =
{
	&m31179_MI,
	&m31180_MI,
	&m31181_MI,
	&m31182_MI,
	&m31183_MI,
	&m31184_MI,
	&m31185_MI,
	NULL
};
extern TypeInfo t5978_TI;
static TypeInfo* t5976_ITIs[] = 
{
	&t603_TI,
	&t5978_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5976_0_0_0;
extern Il2CppType t5976_1_0_0;
struct t5976;
extern Il2CppGenericClass t5976_GC;
TypeInfo t5976_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5976_MIs, t5976_PIs, NULL, NULL, NULL, NULL, NULL, &t5976_TI, t5976_ITIs, NULL, &EmptyCustomAttributesCache, &t5976_TI, &t5976_0_0_0, &t5976_1_0_0, NULL, &t5976_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.X509.X509ChainStatusFlags>
extern Il2CppType t4630_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31186_GM;
MethodInfo m31186_MI = 
{
	"GetEnumerator", NULL, &t5978_TI, &t4630_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31186_GM};
static MethodInfo* t5978_MIs[] =
{
	&m31186_MI,
	NULL
};
static TypeInfo* t5978_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5978_0_0_0;
extern Il2CppType t5978_1_0_0;
struct t5978;
extern Il2CppGenericClass t5978_GC;
TypeInfo t5978_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5978_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5978_TI, t5978_ITIs, NULL, &EmptyCustomAttributesCache, &t5978_TI, &t5978_0_0_0, &t5978_1_0_0, NULL, &t5978_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5977_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>
extern MethodInfo m31187_MI;
extern MethodInfo m31188_MI;
static PropertyInfo t5977____Item_PropertyInfo = 
{
	&t5977_TI, "Item", &m31187_MI, &m31188_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5977_PIs[] =
{
	&t5977____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1003_0_0_0;
static ParameterInfo t5977_m31189_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1003_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31189_GM;
MethodInfo m31189_MI = 
{
	"IndexOf", NULL, &t5977_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5977_m31189_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31189_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1003_0_0_0;
static ParameterInfo t5977_m31190_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1003_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31190_GM;
MethodInfo m31190_MI = 
{
	"Insert", NULL, &t5977_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5977_m31190_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31190_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5977_m31191_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31191_GM;
MethodInfo m31191_MI = 
{
	"RemoveAt", NULL, &t5977_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5977_m31191_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31191_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5977_m31187_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1003_0_0_0;
extern void* RuntimeInvoker_t1003_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31187_GM;
MethodInfo m31187_MI = 
{
	"get_Item", NULL, &t5977_TI, &t1003_0_0_0, RuntimeInvoker_t1003_t44, t5977_m31187_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31187_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1003_0_0_0;
static ParameterInfo t5977_m31188_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1003_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31188_GM;
MethodInfo m31188_MI = 
{
	"set_Item", NULL, &t5977_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5977_m31188_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31188_GM};
static MethodInfo* t5977_MIs[] =
{
	&m31189_MI,
	&m31190_MI,
	&m31191_MI,
	&m31187_MI,
	&m31188_MI,
	NULL
};
static TypeInfo* t5977_ITIs[] = 
{
	&t603_TI,
	&t5976_TI,
	&t5978_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5977_0_0_0;
extern Il2CppType t5977_1_0_0;
struct t5977;
extern Il2CppGenericClass t5977_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5977_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5977_MIs, t5977_PIs, NULL, NULL, NULL, NULL, NULL, &t5977_TI, t5977_ITIs, NULL, &t1908__CustomAttributeCache, &t5977_TI, &t5977_0_0_0, &t5977_1_0_0, NULL, &t5977_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4632_TI;

#include "t1007.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>
extern MethodInfo m31192_MI;
static PropertyInfo t4632____Current_PropertyInfo = 
{
	&t4632_TI, "Current", &m31192_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4632_PIs[] =
{
	&t4632____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1007_0_0_0;
extern void* RuntimeInvoker_t1007 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31192_GM;
MethodInfo m31192_MI = 
{
	"get_Current", NULL, &t4632_TI, &t1007_0_0_0, RuntimeInvoker_t1007, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31192_GM};
static MethodInfo* t4632_MIs[] =
{
	&m31192_MI,
	NULL
};
static TypeInfo* t4632_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4632_0_0_0;
extern Il2CppType t4632_1_0_0;
struct t4632;
extern Il2CppGenericClass t4632_GC;
TypeInfo t4632_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4632_MIs, t4632_PIs, NULL, NULL, NULL, NULL, NULL, &t4632_TI, t4632_ITIs, NULL, &EmptyCustomAttributesCache, &t4632_TI, &t4632_0_0_0, &t4632_1_0_0, NULL, &t4632_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3246.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3246_TI;
#include "t3246MD.h"

extern TypeInfo t1007_TI;
extern MethodInfo m18030_MI;
extern MethodInfo m23806_MI;
struct t20;
 int32_t m23806 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18026_MI;
 void m18026 (t3246 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18027_MI;
 t29 * m18027 (t3246 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18030(__this, &m18030_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1007_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18028_MI;
 void m18028 (t3246 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18029_MI;
 bool m18029 (t3246 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18030 (t3246 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23806(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23806_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>
extern Il2CppType t20_0_0_1;
FieldInfo t3246_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3246_TI, offsetof(t3246, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3246_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3246_TI, offsetof(t3246, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3246_FIs[] =
{
	&t3246_f0_FieldInfo,
	&t3246_f1_FieldInfo,
	NULL
};
static PropertyInfo t3246____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3246_TI, "System.Collections.IEnumerator.Current", &m18027_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3246____Current_PropertyInfo = 
{
	&t3246_TI, "Current", &m18030_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3246_PIs[] =
{
	&t3246____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3246____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3246_m18026_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18026_GM;
MethodInfo m18026_MI = 
{
	".ctor", (methodPointerType)&m18026, &t3246_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3246_m18026_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18026_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18027_GM;
MethodInfo m18027_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18027, &t3246_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18027_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18028_GM;
MethodInfo m18028_MI = 
{
	"Dispose", (methodPointerType)&m18028, &t3246_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18028_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18029_GM;
MethodInfo m18029_MI = 
{
	"MoveNext", (methodPointerType)&m18029, &t3246_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18029_GM};
extern Il2CppType t1007_0_0_0;
extern void* RuntimeInvoker_t1007 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18030_GM;
MethodInfo m18030_MI = 
{
	"get_Current", (methodPointerType)&m18030, &t3246_TI, &t1007_0_0_0, RuntimeInvoker_t1007, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18030_GM};
static MethodInfo* t3246_MIs[] =
{
	&m18026_MI,
	&m18027_MI,
	&m18028_MI,
	&m18029_MI,
	&m18030_MI,
	NULL
};
static MethodInfo* t3246_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18027_MI,
	&m18029_MI,
	&m18028_MI,
	&m18030_MI,
};
static TypeInfo* t3246_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4632_TI,
};
static Il2CppInterfaceOffsetPair t3246_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4632_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3246_0_0_0;
extern Il2CppType t3246_1_0_0;
extern Il2CppGenericClass t3246_GC;
TypeInfo t3246_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3246_MIs, t3246_PIs, t3246_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3246_TI, t3246_ITIs, t3246_VT, &EmptyCustomAttributesCache, &t3246_TI, &t3246_0_0_0, &t3246_1_0_0, t3246_IOs, &t3246_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3246)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5979_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>
extern MethodInfo m31193_MI;
static PropertyInfo t5979____Count_PropertyInfo = 
{
	&t5979_TI, "Count", &m31193_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31194_MI;
static PropertyInfo t5979____IsReadOnly_PropertyInfo = 
{
	&t5979_TI, "IsReadOnly", &m31194_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5979_PIs[] =
{
	&t5979____Count_PropertyInfo,
	&t5979____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31193_GM;
MethodInfo m31193_MI = 
{
	"get_Count", NULL, &t5979_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31193_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31194_GM;
MethodInfo m31194_MI = 
{
	"get_IsReadOnly", NULL, &t5979_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31194_GM};
extern Il2CppType t1007_0_0_0;
extern Il2CppType t1007_0_0_0;
static ParameterInfo t5979_m31195_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1007_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31195_GM;
MethodInfo m31195_MI = 
{
	"Add", NULL, &t5979_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5979_m31195_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31195_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31196_GM;
MethodInfo m31196_MI = 
{
	"Clear", NULL, &t5979_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31196_GM};
extern Il2CppType t1007_0_0_0;
static ParameterInfo t5979_m31197_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1007_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31197_GM;
MethodInfo m31197_MI = 
{
	"Contains", NULL, &t5979_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5979_m31197_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31197_GM};
extern Il2CppType t3929_0_0_0;
extern Il2CppType t3929_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5979_m31198_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3929_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31198_GM;
MethodInfo m31198_MI = 
{
	"CopyTo", NULL, &t5979_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5979_m31198_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31198_GM};
extern Il2CppType t1007_0_0_0;
static ParameterInfo t5979_m31199_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1007_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31199_GM;
MethodInfo m31199_MI = 
{
	"Remove", NULL, &t5979_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5979_m31199_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31199_GM};
static MethodInfo* t5979_MIs[] =
{
	&m31193_MI,
	&m31194_MI,
	&m31195_MI,
	&m31196_MI,
	&m31197_MI,
	&m31198_MI,
	&m31199_MI,
	NULL
};
extern TypeInfo t5981_TI;
static TypeInfo* t5979_ITIs[] = 
{
	&t603_TI,
	&t5981_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5979_0_0_0;
extern Il2CppType t5979_1_0_0;
struct t5979;
extern Il2CppGenericClass t5979_GC;
TypeInfo t5979_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5979_MIs, t5979_PIs, NULL, NULL, NULL, NULL, NULL, &t5979_TI, t5979_ITIs, NULL, &EmptyCustomAttributesCache, &t5979_TI, &t5979_0_0_0, &t5979_1_0_0, NULL, &t5979_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.KeyUsages>
extern Il2CppType t4632_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31200_GM;
MethodInfo m31200_MI = 
{
	"GetEnumerator", NULL, &t5981_TI, &t4632_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31200_GM};
static MethodInfo* t5981_MIs[] =
{
	&m31200_MI,
	NULL
};
static TypeInfo* t5981_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5981_0_0_0;
extern Il2CppType t5981_1_0_0;
struct t5981;
extern Il2CppGenericClass t5981_GC;
TypeInfo t5981_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5981_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5981_TI, t5981_ITIs, NULL, &EmptyCustomAttributesCache, &t5981_TI, &t5981_0_0_0, &t5981_1_0_0, NULL, &t5981_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5980_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>
extern MethodInfo m31201_MI;
extern MethodInfo m31202_MI;
static PropertyInfo t5980____Item_PropertyInfo = 
{
	&t5980_TI, "Item", &m31201_MI, &m31202_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5980_PIs[] =
{
	&t5980____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1007_0_0_0;
static ParameterInfo t5980_m31203_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1007_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31203_GM;
MethodInfo m31203_MI = 
{
	"IndexOf", NULL, &t5980_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5980_m31203_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31203_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1007_0_0_0;
static ParameterInfo t5980_m31204_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1007_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31204_GM;
MethodInfo m31204_MI = 
{
	"Insert", NULL, &t5980_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5980_m31204_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31204_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5980_m31205_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31205_GM;
MethodInfo m31205_MI = 
{
	"RemoveAt", NULL, &t5980_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5980_m31205_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31205_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5980_m31201_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1007_0_0_0;
extern void* RuntimeInvoker_t1007_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31201_GM;
MethodInfo m31201_MI = 
{
	"get_Item", NULL, &t5980_TI, &t1007_0_0_0, RuntimeInvoker_t1007_t44, t5980_m31201_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31201_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1007_0_0_0;
static ParameterInfo t5980_m31202_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1007_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31202_GM;
MethodInfo m31202_MI = 
{
	"set_Item", NULL, &t5980_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5980_m31202_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31202_GM};
static MethodInfo* t5980_MIs[] =
{
	&m31203_MI,
	&m31204_MI,
	&m31205_MI,
	&m31201_MI,
	&m31202_MI,
	NULL
};
static TypeInfo* t5980_ITIs[] = 
{
	&t603_TI,
	&t5979_TI,
	&t5981_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5980_0_0_0;
extern Il2CppType t5980_1_0_0;
struct t5980;
extern Il2CppGenericClass t5980_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5980_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5980_MIs, t5980_PIs, NULL, NULL, NULL, NULL, NULL, &t5980_TI, t5980_ITIs, NULL, &t1908__CustomAttributeCache, &t5980_TI, &t5980_0_0_0, &t5980_1_0_0, NULL, &t5980_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4634_TI;

#include "t1009.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern MethodInfo m31206_MI;
static PropertyInfo t4634____Current_PropertyInfo = 
{
	&t4634_TI, "Current", &m31206_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4634_PIs[] =
{
	&t4634____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1009_0_0_0;
extern void* RuntimeInvoker_t1009 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31206_GM;
MethodInfo m31206_MI = 
{
	"get_Current", NULL, &t4634_TI, &t1009_0_0_0, RuntimeInvoker_t1009, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31206_GM};
static MethodInfo* t4634_MIs[] =
{
	&m31206_MI,
	NULL
};
static TypeInfo* t4634_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4634_0_0_0;
extern Il2CppType t4634_1_0_0;
struct t4634;
extern Il2CppGenericClass t4634_GC;
TypeInfo t4634_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4634_MIs, t4634_PIs, NULL, NULL, NULL, NULL, NULL, &t4634_TI, t4634_ITIs, NULL, &EmptyCustomAttributesCache, &t4634_TI, &t4634_0_0_0, &t4634_1_0_0, NULL, &t4634_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3247.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3247_TI;
#include "t3247MD.h"

extern TypeInfo t1009_TI;
extern MethodInfo m18035_MI;
extern MethodInfo m23817_MI;
struct t20;
 int32_t m23817 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18031_MI;
 void m18031 (t3247 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18032_MI;
 t29 * m18032 (t3247 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18035(__this, &m18035_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1009_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18033_MI;
 void m18033 (t3247 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18034_MI;
 bool m18034 (t3247 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18035 (t3247 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23817(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23817_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern Il2CppType t20_0_0_1;
FieldInfo t3247_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3247_TI, offsetof(t3247, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3247_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3247_TI, offsetof(t3247, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3247_FIs[] =
{
	&t3247_f0_FieldInfo,
	&t3247_f1_FieldInfo,
	NULL
};
static PropertyInfo t3247____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3247_TI, "System.Collections.IEnumerator.Current", &m18032_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3247____Current_PropertyInfo = 
{
	&t3247_TI, "Current", &m18035_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3247_PIs[] =
{
	&t3247____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3247____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3247_m18031_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18031_GM;
MethodInfo m18031_MI = 
{
	".ctor", (methodPointerType)&m18031, &t3247_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3247_m18031_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18031_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18032_GM;
MethodInfo m18032_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18032, &t3247_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18032_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18033_GM;
MethodInfo m18033_MI = 
{
	"Dispose", (methodPointerType)&m18033, &t3247_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18033_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18034_GM;
MethodInfo m18034_MI = 
{
	"MoveNext", (methodPointerType)&m18034, &t3247_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18034_GM};
extern Il2CppType t1009_0_0_0;
extern void* RuntimeInvoker_t1009 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18035_GM;
MethodInfo m18035_MI = 
{
	"get_Current", (methodPointerType)&m18035, &t3247_TI, &t1009_0_0_0, RuntimeInvoker_t1009, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18035_GM};
static MethodInfo* t3247_MIs[] =
{
	&m18031_MI,
	&m18032_MI,
	&m18033_MI,
	&m18034_MI,
	&m18035_MI,
	NULL
};
static MethodInfo* t3247_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18032_MI,
	&m18034_MI,
	&m18033_MI,
	&m18035_MI,
};
static TypeInfo* t3247_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4634_TI,
};
static Il2CppInterfaceOffsetPair t3247_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4634_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3247_0_0_0;
extern Il2CppType t3247_1_0_0;
extern Il2CppGenericClass t3247_GC;
TypeInfo t3247_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3247_MIs, t3247_PIs, t3247_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3247_TI, t3247_ITIs, t3247_VT, &EmptyCustomAttributesCache, &t3247_TI, &t3247_0_0_0, &t3247_1_0_0, t3247_IOs, &t3247_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3247)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5982_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern MethodInfo m31207_MI;
static PropertyInfo t5982____Count_PropertyInfo = 
{
	&t5982_TI, "Count", &m31207_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31208_MI;
static PropertyInfo t5982____IsReadOnly_PropertyInfo = 
{
	&t5982_TI, "IsReadOnly", &m31208_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5982_PIs[] =
{
	&t5982____Count_PropertyInfo,
	&t5982____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31207_GM;
MethodInfo m31207_MI = 
{
	"get_Count", NULL, &t5982_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31207_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31208_GM;
MethodInfo m31208_MI = 
{
	"get_IsReadOnly", NULL, &t5982_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31208_GM};
extern Il2CppType t1009_0_0_0;
extern Il2CppType t1009_0_0_0;
static ParameterInfo t5982_m31209_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1009_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31209_GM;
MethodInfo m31209_MI = 
{
	"Add", NULL, &t5982_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5982_m31209_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31209_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31210_GM;
MethodInfo m31210_MI = 
{
	"Clear", NULL, &t5982_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31210_GM};
extern Il2CppType t1009_0_0_0;
static ParameterInfo t5982_m31211_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1009_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31211_GM;
MethodInfo m31211_MI = 
{
	"Contains", NULL, &t5982_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5982_m31211_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31211_GM};
extern Il2CppType t3930_0_0_0;
extern Il2CppType t3930_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5982_m31212_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3930_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31212_GM;
MethodInfo m31212_MI = 
{
	"CopyTo", NULL, &t5982_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5982_m31212_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31212_GM};
extern Il2CppType t1009_0_0_0;
static ParameterInfo t5982_m31213_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1009_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31213_GM;
MethodInfo m31213_MI = 
{
	"Remove", NULL, &t5982_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5982_m31213_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31213_GM};
static MethodInfo* t5982_MIs[] =
{
	&m31207_MI,
	&m31208_MI,
	&m31209_MI,
	&m31210_MI,
	&m31211_MI,
	&m31212_MI,
	&m31213_MI,
	NULL
};
extern TypeInfo t5984_TI;
static TypeInfo* t5982_ITIs[] = 
{
	&t603_TI,
	&t5984_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5982_0_0_0;
extern Il2CppType t5982_1_0_0;
struct t5982;
extern Il2CppGenericClass t5982_GC;
TypeInfo t5982_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5982_MIs, t5982_PIs, NULL, NULL, NULL, NULL, NULL, &t5982_TI, t5982_ITIs, NULL, &EmptyCustomAttributesCache, &t5982_TI, &t5982_0_0_0, &t5982_1_0_0, NULL, &t5982_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern Il2CppType t4634_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31214_GM;
MethodInfo m31214_MI = 
{
	"GetEnumerator", NULL, &t5984_TI, &t4634_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31214_GM};
static MethodInfo* t5984_MIs[] =
{
	&m31214_MI,
	NULL
};
static TypeInfo* t5984_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5984_0_0_0;
extern Il2CppType t5984_1_0_0;
struct t5984;
extern Il2CppGenericClass t5984_GC;
TypeInfo t5984_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5984_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5984_TI, t5984_ITIs, NULL, &EmptyCustomAttributesCache, &t5984_TI, &t5984_0_0_0, &t5984_1_0_0, NULL, &t5984_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5983_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern MethodInfo m31215_MI;
extern MethodInfo m31216_MI;
static PropertyInfo t5983____Item_PropertyInfo = 
{
	&t5983_TI, "Item", &m31215_MI, &m31216_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5983_PIs[] =
{
	&t5983____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1009_0_0_0;
static ParameterInfo t5983_m31217_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1009_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31217_GM;
MethodInfo m31217_MI = 
{
	"IndexOf", NULL, &t5983_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5983_m31217_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31217_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1009_0_0_0;
static ParameterInfo t5983_m31218_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1009_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31218_GM;
MethodInfo m31218_MI = 
{
	"Insert", NULL, &t5983_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5983_m31218_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31218_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5983_m31219_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31219_GM;
MethodInfo m31219_MI = 
{
	"RemoveAt", NULL, &t5983_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5983_m31219_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31219_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5983_m31215_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1009_0_0_0;
extern void* RuntimeInvoker_t1009_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31215_GM;
MethodInfo m31215_MI = 
{
	"get_Item", NULL, &t5983_TI, &t1009_0_0_0, RuntimeInvoker_t1009_t44, t5983_m31215_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31215_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1009_0_0_0;
static ParameterInfo t5983_m31216_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1009_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31216_GM;
MethodInfo m31216_MI = 
{
	"set_Item", NULL, &t5983_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5983_m31216_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31216_GM};
static MethodInfo* t5983_MIs[] =
{
	&m31217_MI,
	&m31218_MI,
	&m31219_MI,
	&m31215_MI,
	&m31216_MI,
	NULL
};
static TypeInfo* t5983_ITIs[] = 
{
	&t603_TI,
	&t5982_TI,
	&t5984_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5983_0_0_0;
extern Il2CppType t5983_1_0_0;
struct t5983;
extern Il2CppGenericClass t5983_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5983_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5983_MIs, t5983_PIs, NULL, NULL, NULL, NULL, NULL, &t5983_TI, t5983_ITIs, NULL, &t1908__CustomAttributeCache, &t5983_TI, &t5983_0_0_0, &t5983_1_0_0, NULL, &t5983_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4636_TI;

#include "t1015.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>
extern MethodInfo m31220_MI;
static PropertyInfo t4636____Current_PropertyInfo = 
{
	&t4636_TI, "Current", &m31220_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4636_PIs[] =
{
	&t4636____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1015_0_0_0;
extern void* RuntimeInvoker_t1015 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31220_GM;
MethodInfo m31220_MI = 
{
	"get_Current", NULL, &t4636_TI, &t1015_0_0_0, RuntimeInvoker_t1015, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31220_GM};
static MethodInfo* t4636_MIs[] =
{
	&m31220_MI,
	NULL
};
static TypeInfo* t4636_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4636_0_0_0;
extern Il2CppType t4636_1_0_0;
struct t4636;
extern Il2CppGenericClass t4636_GC;
TypeInfo t4636_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4636_MIs, t4636_PIs, NULL, NULL, NULL, NULL, NULL, &t4636_TI, t4636_ITIs, NULL, &EmptyCustomAttributesCache, &t4636_TI, &t4636_0_0_0, &t4636_1_0_0, NULL, &t4636_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3248.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3248_TI;
#include "t3248MD.h"

extern TypeInfo t1015_TI;
extern MethodInfo m18040_MI;
extern MethodInfo m23828_MI;
struct t20;
 uint8_t m23828 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18036_MI;
 void m18036 (t3248 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18037_MI;
 t29 * m18037 (t3248 * __this, MethodInfo* method){
	{
		uint8_t L_0 = m18040(__this, &m18040_MI);
		uint8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1015_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18038_MI;
 void m18038 (t3248 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18039_MI;
 bool m18039 (t3248 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint8_t m18040 (t3248 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint8_t L_8 = m23828(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23828_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>
extern Il2CppType t20_0_0_1;
FieldInfo t3248_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3248_TI, offsetof(t3248, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3248_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3248_TI, offsetof(t3248, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3248_FIs[] =
{
	&t3248_f0_FieldInfo,
	&t3248_f1_FieldInfo,
	NULL
};
static PropertyInfo t3248____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3248_TI, "System.Collections.IEnumerator.Current", &m18037_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3248____Current_PropertyInfo = 
{
	&t3248_TI, "Current", &m18040_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3248_PIs[] =
{
	&t3248____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3248____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3248_m18036_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18036_GM;
MethodInfo m18036_MI = 
{
	".ctor", (methodPointerType)&m18036, &t3248_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3248_m18036_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18036_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18037_GM;
MethodInfo m18037_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18037, &t3248_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18037_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18038_GM;
MethodInfo m18038_MI = 
{
	"Dispose", (methodPointerType)&m18038, &t3248_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18038_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18039_GM;
MethodInfo m18039_MI = 
{
	"MoveNext", (methodPointerType)&m18039, &t3248_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18039_GM};
extern Il2CppType t1015_0_0_0;
extern void* RuntimeInvoker_t1015 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18040_GM;
MethodInfo m18040_MI = 
{
	"get_Current", (methodPointerType)&m18040, &t3248_TI, &t1015_0_0_0, RuntimeInvoker_t1015, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18040_GM};
static MethodInfo* t3248_MIs[] =
{
	&m18036_MI,
	&m18037_MI,
	&m18038_MI,
	&m18039_MI,
	&m18040_MI,
	NULL
};
static MethodInfo* t3248_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18037_MI,
	&m18039_MI,
	&m18038_MI,
	&m18040_MI,
};
static TypeInfo* t3248_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4636_TI,
};
static Il2CppInterfaceOffsetPair t3248_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4636_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3248_0_0_0;
extern Il2CppType t3248_1_0_0;
extern Il2CppGenericClass t3248_GC;
TypeInfo t3248_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3248_MIs, t3248_PIs, t3248_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3248_TI, t3248_ITIs, t3248_VT, &EmptyCustomAttributesCache, &t3248_TI, &t3248_0_0_0, &t3248_1_0_0, t3248_IOs, &t3248_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3248)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5985_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>
extern MethodInfo m31221_MI;
static PropertyInfo t5985____Count_PropertyInfo = 
{
	&t5985_TI, "Count", &m31221_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31222_MI;
static PropertyInfo t5985____IsReadOnly_PropertyInfo = 
{
	&t5985_TI, "IsReadOnly", &m31222_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5985_PIs[] =
{
	&t5985____Count_PropertyInfo,
	&t5985____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31221_GM;
MethodInfo m31221_MI = 
{
	"get_Count", NULL, &t5985_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31221_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31222_GM;
MethodInfo m31222_MI = 
{
	"get_IsReadOnly", NULL, &t5985_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31222_GM};
extern Il2CppType t1015_0_0_0;
extern Il2CppType t1015_0_0_0;
static ParameterInfo t5985_m31223_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1015_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31223_GM;
MethodInfo m31223_MI = 
{
	"Add", NULL, &t5985_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t5985_m31223_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31223_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31224_GM;
MethodInfo m31224_MI = 
{
	"Clear", NULL, &t5985_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31224_GM};
extern Il2CppType t1015_0_0_0;
static ParameterInfo t5985_m31225_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1015_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31225_GM;
MethodInfo m31225_MI = 
{
	"Contains", NULL, &t5985_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t5985_m31225_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31225_GM};
extern Il2CppType t3931_0_0_0;
extern Il2CppType t3931_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5985_m31226_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3931_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31226_GM;
MethodInfo m31226_MI = 
{
	"CopyTo", NULL, &t5985_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5985_m31226_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31226_GM};
extern Il2CppType t1015_0_0_0;
static ParameterInfo t5985_m31227_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1015_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31227_GM;
MethodInfo m31227_MI = 
{
	"Remove", NULL, &t5985_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t5985_m31227_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31227_GM};
static MethodInfo* t5985_MIs[] =
{
	&m31221_MI,
	&m31222_MI,
	&m31223_MI,
	&m31224_MI,
	&m31225_MI,
	&m31226_MI,
	&m31227_MI,
	NULL
};
extern TypeInfo t5987_TI;
static TypeInfo* t5985_ITIs[] = 
{
	&t603_TI,
	&t5987_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5985_0_0_0;
extern Il2CppType t5985_1_0_0;
struct t5985;
extern Il2CppGenericClass t5985_GC;
TypeInfo t5985_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5985_MIs, t5985_PIs, NULL, NULL, NULL, NULL, NULL, &t5985_TI, t5985_ITIs, NULL, &EmptyCustomAttributesCache, &t5985_TI, &t5985_0_0_0, &t5985_1_0_0, NULL, &t5985_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertLevel>
extern Il2CppType t4636_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31228_GM;
MethodInfo m31228_MI = 
{
	"GetEnumerator", NULL, &t5987_TI, &t4636_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31228_GM};
static MethodInfo* t5987_MIs[] =
{
	&m31228_MI,
	NULL
};
static TypeInfo* t5987_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5987_0_0_0;
extern Il2CppType t5987_1_0_0;
struct t5987;
extern Il2CppGenericClass t5987_GC;
TypeInfo t5987_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5987_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5987_TI, t5987_ITIs, NULL, &EmptyCustomAttributesCache, &t5987_TI, &t5987_0_0_0, &t5987_1_0_0, NULL, &t5987_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5986_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>
extern MethodInfo m31229_MI;
extern MethodInfo m31230_MI;
static PropertyInfo t5986____Item_PropertyInfo = 
{
	&t5986_TI, "Item", &m31229_MI, &m31230_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5986_PIs[] =
{
	&t5986____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1015_0_0_0;
static ParameterInfo t5986_m31231_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1015_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31231_GM;
MethodInfo m31231_MI = 
{
	"IndexOf", NULL, &t5986_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t5986_m31231_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31231_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1015_0_0_0;
static ParameterInfo t5986_m31232_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1015_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31232_GM;
MethodInfo m31232_MI = 
{
	"Insert", NULL, &t5986_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t5986_m31232_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31232_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5986_m31233_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31233_GM;
MethodInfo m31233_MI = 
{
	"RemoveAt", NULL, &t5986_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5986_m31233_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31233_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5986_m31229_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1015_0_0_0;
extern void* RuntimeInvoker_t1015_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31229_GM;
MethodInfo m31229_MI = 
{
	"get_Item", NULL, &t5986_TI, &t1015_0_0_0, RuntimeInvoker_t1015_t44, t5986_m31229_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31229_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1015_0_0_0;
static ParameterInfo t5986_m31230_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1015_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31230_GM;
MethodInfo m31230_MI = 
{
	"set_Item", NULL, &t5986_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t5986_m31230_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31230_GM};
static MethodInfo* t5986_MIs[] =
{
	&m31231_MI,
	&m31232_MI,
	&m31233_MI,
	&m31229_MI,
	&m31230_MI,
	NULL
};
static TypeInfo* t5986_ITIs[] = 
{
	&t603_TI,
	&t5985_TI,
	&t5987_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5986_0_0_0;
extern Il2CppType t5986_1_0_0;
struct t5986;
extern Il2CppGenericClass t5986_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5986_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5986_MIs, t5986_PIs, NULL, NULL, NULL, NULL, NULL, &t5986_TI, t5986_ITIs, NULL, &t1908__CustomAttributeCache, &t5986_TI, &t5986_0_0_0, &t5986_1_0_0, NULL, &t5986_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4638_TI;

#include "t1016.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>
extern MethodInfo m31234_MI;
static PropertyInfo t4638____Current_PropertyInfo = 
{
	&t4638_TI, "Current", &m31234_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4638_PIs[] =
{
	&t4638____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1016_0_0_0;
extern void* RuntimeInvoker_t1016 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31234_GM;
MethodInfo m31234_MI = 
{
	"get_Current", NULL, &t4638_TI, &t1016_0_0_0, RuntimeInvoker_t1016, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31234_GM};
static MethodInfo* t4638_MIs[] =
{
	&m31234_MI,
	NULL
};
static TypeInfo* t4638_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4638_0_0_0;
extern Il2CppType t4638_1_0_0;
struct t4638;
extern Il2CppGenericClass t4638_GC;
TypeInfo t4638_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4638_MIs, t4638_PIs, NULL, NULL, NULL, NULL, NULL, &t4638_TI, t4638_ITIs, NULL, &EmptyCustomAttributesCache, &t4638_TI, &t4638_0_0_0, &t4638_1_0_0, NULL, &t4638_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3249.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3249_TI;
#include "t3249MD.h"

extern TypeInfo t1016_TI;
extern MethodInfo m18045_MI;
extern MethodInfo m23839_MI;
struct t20;
 uint8_t m23839 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18041_MI;
 void m18041 (t3249 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18042_MI;
 t29 * m18042 (t3249 * __this, MethodInfo* method){
	{
		uint8_t L_0 = m18045(__this, &m18045_MI);
		uint8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1016_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18043_MI;
 void m18043 (t3249 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18044_MI;
 bool m18044 (t3249 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint8_t m18045 (t3249 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint8_t L_8 = m23839(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23839_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>
extern Il2CppType t20_0_0_1;
FieldInfo t3249_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3249_TI, offsetof(t3249, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3249_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3249_TI, offsetof(t3249, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3249_FIs[] =
{
	&t3249_f0_FieldInfo,
	&t3249_f1_FieldInfo,
	NULL
};
static PropertyInfo t3249____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3249_TI, "System.Collections.IEnumerator.Current", &m18042_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3249____Current_PropertyInfo = 
{
	&t3249_TI, "Current", &m18045_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3249_PIs[] =
{
	&t3249____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3249____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3249_m18041_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18041_GM;
MethodInfo m18041_MI = 
{
	".ctor", (methodPointerType)&m18041, &t3249_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3249_m18041_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18041_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18042_GM;
MethodInfo m18042_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18042, &t3249_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18042_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18043_GM;
MethodInfo m18043_MI = 
{
	"Dispose", (methodPointerType)&m18043, &t3249_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18043_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18044_GM;
MethodInfo m18044_MI = 
{
	"MoveNext", (methodPointerType)&m18044, &t3249_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18044_GM};
extern Il2CppType t1016_0_0_0;
extern void* RuntimeInvoker_t1016 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18045_GM;
MethodInfo m18045_MI = 
{
	"get_Current", (methodPointerType)&m18045, &t3249_TI, &t1016_0_0_0, RuntimeInvoker_t1016, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18045_GM};
static MethodInfo* t3249_MIs[] =
{
	&m18041_MI,
	&m18042_MI,
	&m18043_MI,
	&m18044_MI,
	&m18045_MI,
	NULL
};
static MethodInfo* t3249_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18042_MI,
	&m18044_MI,
	&m18043_MI,
	&m18045_MI,
};
static TypeInfo* t3249_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4638_TI,
};
static Il2CppInterfaceOffsetPair t3249_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4638_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3249_0_0_0;
extern Il2CppType t3249_1_0_0;
extern Il2CppGenericClass t3249_GC;
TypeInfo t3249_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3249_MIs, t3249_PIs, t3249_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3249_TI, t3249_ITIs, t3249_VT, &EmptyCustomAttributesCache, &t3249_TI, &t3249_0_0_0, &t3249_1_0_0, t3249_IOs, &t3249_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3249)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5988_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>
extern MethodInfo m31235_MI;
static PropertyInfo t5988____Count_PropertyInfo = 
{
	&t5988_TI, "Count", &m31235_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31236_MI;
static PropertyInfo t5988____IsReadOnly_PropertyInfo = 
{
	&t5988_TI, "IsReadOnly", &m31236_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5988_PIs[] =
{
	&t5988____Count_PropertyInfo,
	&t5988____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31235_GM;
MethodInfo m31235_MI = 
{
	"get_Count", NULL, &t5988_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31235_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31236_GM;
MethodInfo m31236_MI = 
{
	"get_IsReadOnly", NULL, &t5988_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31236_GM};
extern Il2CppType t1016_0_0_0;
extern Il2CppType t1016_0_0_0;
static ParameterInfo t5988_m31237_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1016_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31237_GM;
MethodInfo m31237_MI = 
{
	"Add", NULL, &t5988_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t5988_m31237_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31237_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31238_GM;
MethodInfo m31238_MI = 
{
	"Clear", NULL, &t5988_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31238_GM};
extern Il2CppType t1016_0_0_0;
static ParameterInfo t5988_m31239_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1016_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31239_GM;
MethodInfo m31239_MI = 
{
	"Contains", NULL, &t5988_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t5988_m31239_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31239_GM};
extern Il2CppType t3932_0_0_0;
extern Il2CppType t3932_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5988_m31240_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3932_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31240_GM;
MethodInfo m31240_MI = 
{
	"CopyTo", NULL, &t5988_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5988_m31240_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31240_GM};
extern Il2CppType t1016_0_0_0;
static ParameterInfo t5988_m31241_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1016_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31241_GM;
MethodInfo m31241_MI = 
{
	"Remove", NULL, &t5988_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t5988_m31241_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31241_GM};
static MethodInfo* t5988_MIs[] =
{
	&m31235_MI,
	&m31236_MI,
	&m31237_MI,
	&m31238_MI,
	&m31239_MI,
	&m31240_MI,
	&m31241_MI,
	NULL
};
extern TypeInfo t5990_TI;
static TypeInfo* t5988_ITIs[] = 
{
	&t603_TI,
	&t5990_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5988_0_0_0;
extern Il2CppType t5988_1_0_0;
struct t5988;
extern Il2CppGenericClass t5988_GC;
TypeInfo t5988_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5988_MIs, t5988_PIs, NULL, NULL, NULL, NULL, NULL, &t5988_TI, t5988_ITIs, NULL, &EmptyCustomAttributesCache, &t5988_TI, &t5988_0_0_0, &t5988_1_0_0, NULL, &t5988_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertDescription>
extern Il2CppType t4638_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31242_GM;
MethodInfo m31242_MI = 
{
	"GetEnumerator", NULL, &t5990_TI, &t4638_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31242_GM};
static MethodInfo* t5990_MIs[] =
{
	&m31242_MI,
	NULL
};
static TypeInfo* t5990_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5990_0_0_0;
extern Il2CppType t5990_1_0_0;
struct t5990;
extern Il2CppGenericClass t5990_GC;
TypeInfo t5990_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5990_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5990_TI, t5990_ITIs, NULL, &EmptyCustomAttributesCache, &t5990_TI, &t5990_0_0_0, &t5990_1_0_0, NULL, &t5990_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5989_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>
extern MethodInfo m31243_MI;
extern MethodInfo m31244_MI;
static PropertyInfo t5989____Item_PropertyInfo = 
{
	&t5989_TI, "Item", &m31243_MI, &m31244_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5989_PIs[] =
{
	&t5989____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1016_0_0_0;
static ParameterInfo t5989_m31245_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1016_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31245_GM;
MethodInfo m31245_MI = 
{
	"IndexOf", NULL, &t5989_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t5989_m31245_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31245_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1016_0_0_0;
static ParameterInfo t5989_m31246_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1016_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31246_GM;
MethodInfo m31246_MI = 
{
	"Insert", NULL, &t5989_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t5989_m31246_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31246_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5989_m31247_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31247_GM;
MethodInfo m31247_MI = 
{
	"RemoveAt", NULL, &t5989_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5989_m31247_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31247_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5989_m31243_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1016_0_0_0;
extern void* RuntimeInvoker_t1016_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31243_GM;
MethodInfo m31243_MI = 
{
	"get_Item", NULL, &t5989_TI, &t1016_0_0_0, RuntimeInvoker_t1016_t44, t5989_m31243_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31243_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1016_0_0_0;
static ParameterInfo t5989_m31244_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1016_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31244_GM;
MethodInfo m31244_MI = 
{
	"set_Item", NULL, &t5989_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t5989_m31244_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31244_GM};
static MethodInfo* t5989_MIs[] =
{
	&m31245_MI,
	&m31246_MI,
	&m31247_MI,
	&m31243_MI,
	&m31244_MI,
	NULL
};
static TypeInfo* t5989_ITIs[] = 
{
	&t603_TI,
	&t5988_TI,
	&t5990_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5989_0_0_0;
extern Il2CppType t5989_1_0_0;
struct t5989;
extern Il2CppGenericClass t5989_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5989_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5989_MIs, t5989_PIs, NULL, NULL, NULL, NULL, NULL, &t5989_TI, t5989_ITIs, NULL, &t1908__CustomAttributeCache, &t5989_TI, &t5989_0_0_0, &t5989_1_0_0, NULL, &t5989_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4640_TI;

#include "t1018.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern MethodInfo m31248_MI;
static PropertyInfo t4640____Current_PropertyInfo = 
{
	&t4640_TI, "Current", &m31248_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4640_PIs[] =
{
	&t4640____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1018_0_0_0;
extern void* RuntimeInvoker_t1018 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31248_GM;
MethodInfo m31248_MI = 
{
	"get_Current", NULL, &t4640_TI, &t1018_0_0_0, RuntimeInvoker_t1018, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31248_GM};
static MethodInfo* t4640_MIs[] =
{
	&m31248_MI,
	NULL
};
static TypeInfo* t4640_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4640_0_0_0;
extern Il2CppType t4640_1_0_0;
struct t4640;
extern Il2CppGenericClass t4640_GC;
TypeInfo t4640_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4640_MIs, t4640_PIs, NULL, NULL, NULL, NULL, NULL, &t4640_TI, t4640_ITIs, NULL, &EmptyCustomAttributesCache, &t4640_TI, &t4640_0_0_0, &t4640_1_0_0, NULL, &t4640_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3250.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3250_TI;
#include "t3250MD.h"

extern TypeInfo t1018_TI;
extern MethodInfo m18050_MI;
extern MethodInfo m23850_MI;
struct t20;
 int32_t m23850 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18046_MI;
 void m18046 (t3250 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18047_MI;
 t29 * m18047 (t3250 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18050(__this, &m18050_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1018_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18048_MI;
 void m18048 (t3250 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18049_MI;
 bool m18049 (t3250 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18050 (t3250 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m23850(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23850_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern Il2CppType t20_0_0_1;
FieldInfo t3250_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3250_TI, offsetof(t3250, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3250_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3250_TI, offsetof(t3250, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3250_FIs[] =
{
	&t3250_f0_FieldInfo,
	&t3250_f1_FieldInfo,
	NULL
};
static PropertyInfo t3250____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3250_TI, "System.Collections.IEnumerator.Current", &m18047_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3250____Current_PropertyInfo = 
{
	&t3250_TI, "Current", &m18050_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3250_PIs[] =
{
	&t3250____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3250____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3250_m18046_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18046_GM;
MethodInfo m18046_MI = 
{
	".ctor", (methodPointerType)&m18046, &t3250_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3250_m18046_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18046_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18047_GM;
MethodInfo m18047_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18047, &t3250_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18047_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18048_GM;
MethodInfo m18048_MI = 
{
	"Dispose", (methodPointerType)&m18048, &t3250_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18048_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18049_GM;
MethodInfo m18049_MI = 
{
	"MoveNext", (methodPointerType)&m18049, &t3250_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18049_GM};
extern Il2CppType t1018_0_0_0;
extern void* RuntimeInvoker_t1018 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18050_GM;
MethodInfo m18050_MI = 
{
	"get_Current", (methodPointerType)&m18050, &t3250_TI, &t1018_0_0_0, RuntimeInvoker_t1018, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18050_GM};
static MethodInfo* t3250_MIs[] =
{
	&m18046_MI,
	&m18047_MI,
	&m18048_MI,
	&m18049_MI,
	&m18050_MI,
	NULL
};
static MethodInfo* t3250_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18047_MI,
	&m18049_MI,
	&m18048_MI,
	&m18050_MI,
};
static TypeInfo* t3250_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4640_TI,
};
static Il2CppInterfaceOffsetPair t3250_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4640_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3250_0_0_0;
extern Il2CppType t3250_1_0_0;
extern Il2CppGenericClass t3250_GC;
TypeInfo t3250_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3250_MIs, t3250_PIs, t3250_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3250_TI, t3250_ITIs, t3250_VT, &EmptyCustomAttributesCache, &t3250_TI, &t3250_0_0_0, &t3250_1_0_0, t3250_IOs, &t3250_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3250)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5991_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern MethodInfo m31249_MI;
static PropertyInfo t5991____Count_PropertyInfo = 
{
	&t5991_TI, "Count", &m31249_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31250_MI;
static PropertyInfo t5991____IsReadOnly_PropertyInfo = 
{
	&t5991_TI, "IsReadOnly", &m31250_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5991_PIs[] =
{
	&t5991____Count_PropertyInfo,
	&t5991____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31249_GM;
MethodInfo m31249_MI = 
{
	"get_Count", NULL, &t5991_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31249_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31250_GM;
MethodInfo m31250_MI = 
{
	"get_IsReadOnly", NULL, &t5991_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31250_GM};
extern Il2CppType t1018_0_0_0;
extern Il2CppType t1018_0_0_0;
static ParameterInfo t5991_m31251_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1018_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31251_GM;
MethodInfo m31251_MI = 
{
	"Add", NULL, &t5991_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5991_m31251_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31251_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31252_GM;
MethodInfo m31252_MI = 
{
	"Clear", NULL, &t5991_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31252_GM};
extern Il2CppType t1018_0_0_0;
static ParameterInfo t5991_m31253_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1018_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31253_GM;
MethodInfo m31253_MI = 
{
	"Contains", NULL, &t5991_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5991_m31253_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31253_GM};
extern Il2CppType t3933_0_0_0;
extern Il2CppType t3933_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5991_m31254_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3933_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31254_GM;
MethodInfo m31254_MI = 
{
	"CopyTo", NULL, &t5991_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5991_m31254_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31254_GM};
extern Il2CppType t1018_0_0_0;
static ParameterInfo t5991_m31255_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1018_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31255_GM;
MethodInfo m31255_MI = 
{
	"Remove", NULL, &t5991_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5991_m31255_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31255_GM};
static MethodInfo* t5991_MIs[] =
{
	&m31249_MI,
	&m31250_MI,
	&m31251_MI,
	&m31252_MI,
	&m31253_MI,
	&m31254_MI,
	&m31255_MI,
	NULL
};
extern TypeInfo t5993_TI;
static TypeInfo* t5991_ITIs[] = 
{
	&t603_TI,
	&t5993_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5991_0_0_0;
extern Il2CppType t5991_1_0_0;
struct t5991;
extern Il2CppGenericClass t5991_GC;
TypeInfo t5991_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5991_MIs, t5991_PIs, NULL, NULL, NULL, NULL, NULL, &t5991_TI, t5991_ITIs, NULL, &EmptyCustomAttributesCache, &t5991_TI, &t5991_0_0_0, &t5991_1_0_0, NULL, &t5991_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern Il2CppType t4640_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31256_GM;
MethodInfo m31256_MI = 
{
	"GetEnumerator", NULL, &t5993_TI, &t4640_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31256_GM};
static MethodInfo* t5993_MIs[] =
{
	&m31256_MI,
	NULL
};
static TypeInfo* t5993_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5993_0_0_0;
extern Il2CppType t5993_1_0_0;
struct t5993;
extern Il2CppGenericClass t5993_GC;
TypeInfo t5993_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5993_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5993_TI, t5993_ITIs, NULL, &EmptyCustomAttributesCache, &t5993_TI, &t5993_0_0_0, &t5993_1_0_0, NULL, &t5993_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5992_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern MethodInfo m31257_MI;
extern MethodInfo m31258_MI;
static PropertyInfo t5992____Item_PropertyInfo = 
{
	&t5992_TI, "Item", &m31257_MI, &m31258_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5992_PIs[] =
{
	&t5992____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1018_0_0_0;
static ParameterInfo t5992_m31259_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1018_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31259_GM;
MethodInfo m31259_MI = 
{
	"IndexOf", NULL, &t5992_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5992_m31259_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31259_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1018_0_0_0;
static ParameterInfo t5992_m31260_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1018_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31260_GM;
MethodInfo m31260_MI = 
{
	"Insert", NULL, &t5992_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5992_m31260_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31260_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5992_m31261_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31261_GM;
MethodInfo m31261_MI = 
{
	"RemoveAt", NULL, &t5992_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5992_m31261_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31261_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5992_m31257_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1018_0_0_0;
extern void* RuntimeInvoker_t1018_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31257_GM;
MethodInfo m31257_MI = 
{
	"get_Item", NULL, &t5992_TI, &t1018_0_0_0, RuntimeInvoker_t1018_t44, t5992_m31257_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31257_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1018_0_0_0;
static ParameterInfo t5992_m31258_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1018_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31258_GM;
MethodInfo m31258_MI = 
{
	"set_Item", NULL, &t5992_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5992_m31258_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31258_GM};
static MethodInfo* t5992_MIs[] =
{
	&m31259_MI,
	&m31260_MI,
	&m31261_MI,
	&m31257_MI,
	&m31258_MI,
	NULL
};
static TypeInfo* t5992_ITIs[] = 
{
	&t603_TI,
	&t5991_TI,
	&t5993_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5992_0_0_0;
extern Il2CppType t5992_1_0_0;
struct t5992;
extern Il2CppGenericClass t5992_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5992_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5992_MIs, t5992_PIs, NULL, NULL, NULL, NULL, NULL, &t5992_TI, t5992_ITIs, NULL, &t1908__CustomAttributeCache, &t5992_TI, &t5992_0_0_0, &t5992_1_0_0, NULL, &t5992_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4641_TI;

#include "t348.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Byte[]>
extern MethodInfo m31262_MI;
static PropertyInfo t4641____Current_PropertyInfo = 
{
	&t4641_TI, "Current", &m31262_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4641_PIs[] =
{
	&t4641____Current_PropertyInfo,
	NULL
};
extern Il2CppType t781_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31262_GM;
MethodInfo m31262_MI = 
{
	"get_Current", NULL, &t4641_TI, &t781_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31262_GM};
static MethodInfo* t4641_MIs[] =
{
	&m31262_MI,
	NULL
};
static TypeInfo* t4641_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4641_0_0_0;
extern Il2CppType t4641_1_0_0;
struct t4641;
extern Il2CppGenericClass t4641_GC;
TypeInfo t4641_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4641_MIs, t4641_PIs, NULL, NULL, NULL, NULL, NULL, &t4641_TI, t4641_ITIs, NULL, &EmptyCustomAttributesCache, &t4641_TI, &t4641_0_0_0, &t4641_1_0_0, NULL, &t4641_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3251.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3251_TI;
#include "t3251MD.h"

extern TypeInfo t781_TI;
extern TypeInfo t348_TI;
extern MethodInfo m18055_MI;
extern MethodInfo m23861_MI;
struct t20;
#define m23861(__this, p0, method) (t781*)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Byte[]>
extern Il2CppType t20_0_0_1;
FieldInfo t3251_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3251_TI, offsetof(t3251, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3251_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3251_TI, offsetof(t3251, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3251_FIs[] =
{
	&t3251_f0_FieldInfo,
	&t3251_f1_FieldInfo,
	NULL
};
extern MethodInfo m18052_MI;
static PropertyInfo t3251____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3251_TI, "System.Collections.IEnumerator.Current", &m18052_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3251____Current_PropertyInfo = 
{
	&t3251_TI, "Current", &m18055_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3251_PIs[] =
{
	&t3251____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3251____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3251_m18051_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18051_GM;
MethodInfo m18051_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3251_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3251_m18051_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18051_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18052_GM;
MethodInfo m18052_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3251_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18052_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18053_GM;
MethodInfo m18053_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3251_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18053_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18054_GM;
MethodInfo m18054_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3251_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18054_GM};
extern Il2CppType t781_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18055_GM;
MethodInfo m18055_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3251_TI, &t781_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18055_GM};
static MethodInfo* t3251_MIs[] =
{
	&m18051_MI,
	&m18052_MI,
	&m18053_MI,
	&m18054_MI,
	&m18055_MI,
	NULL
};
extern MethodInfo m18054_MI;
extern MethodInfo m18053_MI;
static MethodInfo* t3251_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18052_MI,
	&m18054_MI,
	&m18053_MI,
	&m18055_MI,
};
static TypeInfo* t3251_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4641_TI,
};
static Il2CppInterfaceOffsetPair t3251_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4641_TI, 7},
};
extern TypeInfo t781_TI;
static Il2CppRGCTXData t3251_RGCTXData[3] = 
{
	&m18055_MI/* Method Usage */,
	&t781_TI/* Class Usage */,
	&m23861_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3251_0_0_0;
extern Il2CppType t3251_1_0_0;
extern Il2CppGenericClass t3251_GC;
TypeInfo t3251_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3251_MIs, t3251_PIs, t3251_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3251_TI, t3251_ITIs, t3251_VT, &EmptyCustomAttributesCache, &t3251_TI, &t3251_0_0_0, &t3251_1_0_0, t3251_IOs, &t3251_GC, NULL, NULL, NULL, t3251_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3251)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5994_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Byte[]>
extern MethodInfo m31263_MI;
static PropertyInfo t5994____Count_PropertyInfo = 
{
	&t5994_TI, "Count", &m31263_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31264_MI;
static PropertyInfo t5994____IsReadOnly_PropertyInfo = 
{
	&t5994_TI, "IsReadOnly", &m31264_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5994_PIs[] =
{
	&t5994____Count_PropertyInfo,
	&t5994____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31263_GM;
MethodInfo m31263_MI = 
{
	"get_Count", NULL, &t5994_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31263_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31264_GM;
MethodInfo m31264_MI = 
{
	"get_IsReadOnly", NULL, &t5994_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31264_GM};
extern Il2CppType t781_0_0_0;
extern Il2CppType t781_0_0_0;
static ParameterInfo t5994_m31265_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t781_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31265_GM;
MethodInfo m31265_MI = 
{
	"Add", NULL, &t5994_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5994_m31265_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31265_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31266_GM;
MethodInfo m31266_MI = 
{
	"Clear", NULL, &t5994_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31266_GM};
extern Il2CppType t781_0_0_0;
static ParameterInfo t5994_m31267_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t781_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31267_GM;
MethodInfo m31267_MI = 
{
	"Contains", NULL, &t5994_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5994_m31267_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31267_GM};
extern Il2CppType t1118_0_0_0;
extern Il2CppType t1118_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5994_m31268_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1118_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31268_GM;
MethodInfo m31268_MI = 
{
	"CopyTo", NULL, &t5994_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5994_m31268_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31268_GM};
extern Il2CppType t781_0_0_0;
static ParameterInfo t5994_m31269_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t781_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31269_GM;
MethodInfo m31269_MI = 
{
	"Remove", NULL, &t5994_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5994_m31269_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31269_GM};
static MethodInfo* t5994_MIs[] =
{
	&m31263_MI,
	&m31264_MI,
	&m31265_MI,
	&m31266_MI,
	&m31267_MI,
	&m31268_MI,
	&m31269_MI,
	NULL
};
extern TypeInfo t5996_TI;
static TypeInfo* t5994_ITIs[] = 
{
	&t603_TI,
	&t5996_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5994_0_0_0;
extern Il2CppType t5994_1_0_0;
struct t5994;
extern Il2CppGenericClass t5994_GC;
TypeInfo t5994_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5994_MIs, t5994_PIs, NULL, NULL, NULL, NULL, NULL, &t5994_TI, t5994_ITIs, NULL, &EmptyCustomAttributesCache, &t5994_TI, &t5994_0_0_0, &t5994_1_0_0, NULL, &t5994_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Byte[]>
extern Il2CppType t4641_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31270_GM;
MethodInfo m31270_MI = 
{
	"GetEnumerator", NULL, &t5996_TI, &t4641_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31270_GM};
static MethodInfo* t5996_MIs[] =
{
	&m31270_MI,
	NULL
};
static TypeInfo* t5996_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5996_0_0_0;
extern Il2CppType t5996_1_0_0;
struct t5996;
extern Il2CppGenericClass t5996_GC;
TypeInfo t5996_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5996_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5996_TI, t5996_ITIs, NULL, &EmptyCustomAttributesCache, &t5996_TI, &t5996_0_0_0, &t5996_1_0_0, NULL, &t5996_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5995_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Byte[]>
extern MethodInfo m31271_MI;
extern MethodInfo m31272_MI;
static PropertyInfo t5995____Item_PropertyInfo = 
{
	&t5995_TI, "Item", &m31271_MI, &m31272_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5995_PIs[] =
{
	&t5995____Item_PropertyInfo,
	NULL
};
extern Il2CppType t781_0_0_0;
static ParameterInfo t5995_m31273_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t781_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31273_GM;
MethodInfo m31273_MI = 
{
	"IndexOf", NULL, &t5995_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5995_m31273_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31273_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t781_0_0_0;
static ParameterInfo t5995_m31274_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t781_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31274_GM;
MethodInfo m31274_MI = 
{
	"Insert", NULL, &t5995_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5995_m31274_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31274_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5995_m31275_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31275_GM;
MethodInfo m31275_MI = 
{
	"RemoveAt", NULL, &t5995_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5995_m31275_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31275_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5995_m31271_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t781_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31271_GM;
MethodInfo m31271_MI = 
{
	"get_Item", NULL, &t5995_TI, &t781_0_0_0, RuntimeInvoker_t29_t44, t5995_m31271_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31271_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t781_0_0_0;
static ParameterInfo t5995_m31272_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t781_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31272_GM;
MethodInfo m31272_MI = 
{
	"set_Item", NULL, &t5995_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5995_m31272_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31272_GM};
static MethodInfo* t5995_MIs[] =
{
	&m31273_MI,
	&m31274_MI,
	&m31275_MI,
	&m31271_MI,
	&m31272_MI,
	NULL
};
static TypeInfo* t5995_ITIs[] = 
{
	&t603_TI,
	&t5994_TI,
	&t5996_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5995_0_0_0;
extern Il2CppType t5995_1_0_0;
struct t5995;
extern Il2CppGenericClass t5995_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5995_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5995_MIs, t5995_PIs, NULL, NULL, NULL, NULL, NULL, &t5995_TI, t5995_ITIs, NULL, &t1908__CustomAttributeCache, &t5995_TI, &t5995_0_0_0, &t5995_1_0_0, NULL, &t5995_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4643_TI;

#include "t1024.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ContentType>
extern MethodInfo m31276_MI;
static PropertyInfo t4643____Current_PropertyInfo = 
{
	&t4643_TI, "Current", &m31276_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4643_PIs[] =
{
	&t4643____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1024_0_0_0;
extern void* RuntimeInvoker_t1024 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31276_GM;
MethodInfo m31276_MI = 
{
	"get_Current", NULL, &t4643_TI, &t1024_0_0_0, RuntimeInvoker_t1024, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31276_GM};
static MethodInfo* t4643_MIs[] =
{
	&m31276_MI,
	NULL
};
static TypeInfo* t4643_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4643_0_0_0;
extern Il2CppType t4643_1_0_0;
struct t4643;
extern Il2CppGenericClass t4643_GC;
TypeInfo t4643_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4643_MIs, t4643_PIs, NULL, NULL, NULL, NULL, NULL, &t4643_TI, t4643_ITIs, NULL, &EmptyCustomAttributesCache, &t4643_TI, &t4643_0_0_0, &t4643_1_0_0, NULL, &t4643_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3252.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3252_TI;
#include "t3252MD.h"

extern TypeInfo t1024_TI;
extern MethodInfo m18060_MI;
extern MethodInfo m23872_MI;
struct t20;
 uint8_t m23872 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18056_MI;
 void m18056 (t3252 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18057_MI;
 t29 * m18057 (t3252 * __this, MethodInfo* method){
	{
		uint8_t L_0 = m18060(__this, &m18060_MI);
		uint8_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1024_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18058_MI;
 void m18058 (t3252 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18059_MI;
 bool m18059 (t3252 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 uint8_t m18060 (t3252 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		uint8_t L_8 = m23872(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m23872_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>
extern Il2CppType t20_0_0_1;
FieldInfo t3252_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3252_TI, offsetof(t3252, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3252_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3252_TI, offsetof(t3252, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3252_FIs[] =
{
	&t3252_f0_FieldInfo,
	&t3252_f1_FieldInfo,
	NULL
};
static PropertyInfo t3252____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3252_TI, "System.Collections.IEnumerator.Current", &m18057_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3252____Current_PropertyInfo = 
{
	&t3252_TI, "Current", &m18060_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3252_PIs[] =
{
	&t3252____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3252____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3252_m18056_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18056_GM;
MethodInfo m18056_MI = 
{
	".ctor", (methodPointerType)&m18056, &t3252_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3252_m18056_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18056_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18057_GM;
MethodInfo m18057_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18057, &t3252_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18057_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18058_GM;
MethodInfo m18058_MI = 
{
	"Dispose", (methodPointerType)&m18058, &t3252_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18058_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18059_GM;
MethodInfo m18059_MI = 
{
	"MoveNext", (methodPointerType)&m18059, &t3252_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18059_GM};
extern Il2CppType t1024_0_0_0;
extern void* RuntimeInvoker_t1024 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18060_GM;
MethodInfo m18060_MI = 
{
	"get_Current", (methodPointerType)&m18060, &t3252_TI, &t1024_0_0_0, RuntimeInvoker_t1024, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18060_GM};
static MethodInfo* t3252_MIs[] =
{
	&m18056_MI,
	&m18057_MI,
	&m18058_MI,
	&m18059_MI,
	&m18060_MI,
	NULL
};
static MethodInfo* t3252_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18057_MI,
	&m18059_MI,
	&m18058_MI,
	&m18060_MI,
};
static TypeInfo* t3252_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4643_TI,
};
static Il2CppInterfaceOffsetPair t3252_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4643_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3252_0_0_0;
extern Il2CppType t3252_1_0_0;
extern Il2CppGenericClass t3252_GC;
TypeInfo t3252_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3252_MIs, t3252_PIs, t3252_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3252_TI, t3252_ITIs, t3252_VT, &EmptyCustomAttributesCache, &t3252_TI, &t3252_0_0_0, &t3252_1_0_0, t3252_IOs, &t3252_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3252)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5997_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>
extern MethodInfo m31277_MI;
static PropertyInfo t5997____Count_PropertyInfo = 
{
	&t5997_TI, "Count", &m31277_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m31278_MI;
static PropertyInfo t5997____IsReadOnly_PropertyInfo = 
{
	&t5997_TI, "IsReadOnly", &m31278_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5997_PIs[] =
{
	&t5997____Count_PropertyInfo,
	&t5997____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31277_GM;
MethodInfo m31277_MI = 
{
	"get_Count", NULL, &t5997_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31277_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31278_GM;
MethodInfo m31278_MI = 
{
	"get_IsReadOnly", NULL, &t5997_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31278_GM};
extern Il2CppType t1024_0_0_0;
extern Il2CppType t1024_0_0_0;
static ParameterInfo t5997_m31279_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1024_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31279_GM;
MethodInfo m31279_MI = 
{
	"Add", NULL, &t5997_TI, &t21_0_0_0, RuntimeInvoker_t21_t348, t5997_m31279_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31279_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31280_GM;
MethodInfo m31280_MI = 
{
	"Clear", NULL, &t5997_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31280_GM};
extern Il2CppType t1024_0_0_0;
static ParameterInfo t5997_m31281_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1024_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31281_GM;
MethodInfo m31281_MI = 
{
	"Contains", NULL, &t5997_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t5997_m31281_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31281_GM};
extern Il2CppType t3934_0_0_0;
extern Il2CppType t3934_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5997_m31282_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3934_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31282_GM;
MethodInfo m31282_MI = 
{
	"CopyTo", NULL, &t5997_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5997_m31282_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31282_GM};
extern Il2CppType t1024_0_0_0;
static ParameterInfo t5997_m31283_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1024_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31283_GM;
MethodInfo m31283_MI = 
{
	"Remove", NULL, &t5997_TI, &t40_0_0_0, RuntimeInvoker_t40_t348, t5997_m31283_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31283_GM};
static MethodInfo* t5997_MIs[] =
{
	&m31277_MI,
	&m31278_MI,
	&m31279_MI,
	&m31280_MI,
	&m31281_MI,
	&m31282_MI,
	&m31283_MI,
	NULL
};
extern TypeInfo t5999_TI;
static TypeInfo* t5997_ITIs[] = 
{
	&t603_TI,
	&t5999_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5997_0_0_0;
extern Il2CppType t5997_1_0_0;
struct t5997;
extern Il2CppGenericClass t5997_GC;
TypeInfo t5997_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5997_MIs, t5997_PIs, NULL, NULL, NULL, NULL, NULL, &t5997_TI, t5997_ITIs, NULL, &EmptyCustomAttributesCache, &t5997_TI, &t5997_0_0_0, &t5997_1_0_0, NULL, &t5997_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ContentType>
extern Il2CppType t4643_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31284_GM;
MethodInfo m31284_MI = 
{
	"GetEnumerator", NULL, &t5999_TI, &t4643_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31284_GM};
static MethodInfo* t5999_MIs[] =
{
	&m31284_MI,
	NULL
};
static TypeInfo* t5999_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5999_0_0_0;
extern Il2CppType t5999_1_0_0;
struct t5999;
extern Il2CppGenericClass t5999_GC;
TypeInfo t5999_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5999_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5999_TI, t5999_ITIs, NULL, &EmptyCustomAttributesCache, &t5999_TI, &t5999_0_0_0, &t5999_1_0_0, NULL, &t5999_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5998_TI;



// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>
extern MethodInfo m31285_MI;
extern MethodInfo m31286_MI;
static PropertyInfo t5998____Item_PropertyInfo = 
{
	&t5998_TI, "Item", &m31285_MI, &m31286_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5998_PIs[] =
{
	&t5998____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1024_0_0_0;
static ParameterInfo t5998_m31287_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1024_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31287_GM;
MethodInfo m31287_MI = 
{
	"IndexOf", NULL, &t5998_TI, &t44_0_0_0, RuntimeInvoker_t44_t348, t5998_m31287_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31287_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1024_0_0_0;
static ParameterInfo t5998_m31288_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1024_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31288_GM;
MethodInfo m31288_MI = 
{
	"Insert", NULL, &t5998_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t5998_m31288_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31288_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5998_m31289_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31289_GM;
MethodInfo m31289_MI = 
{
	"RemoveAt", NULL, &t5998_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5998_m31289_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31289_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5998_m31285_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1024_0_0_0;
extern void* RuntimeInvoker_t1024_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31285_GM;
MethodInfo m31285_MI = 
{
	"get_Item", NULL, &t5998_TI, &t1024_0_0_0, RuntimeInvoker_t1024_t44, t5998_m31285_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m31285_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1024_0_0_0;
static ParameterInfo t5998_m31286_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1024_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t348 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31286_GM;
MethodInfo m31286_MI = 
{
	"set_Item", NULL, &t5998_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t348, t5998_m31286_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m31286_GM};
static MethodInfo* t5998_MIs[] =
{
	&m31287_MI,
	&m31288_MI,
	&m31289_MI,
	&m31285_MI,
	&m31286_MI,
	NULL
};
static TypeInfo* t5998_ITIs[] = 
{
	&t603_TI,
	&t5997_TI,
	&t5999_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5998_0_0_0;
extern Il2CppType t5998_1_0_0;
struct t5998;
extern Il2CppGenericClass t5998_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5998_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5998_MIs, t5998_PIs, NULL, NULL, NULL, NULL, NULL, &t5998_TI, t5998_ITIs, NULL, &t1908__CustomAttributeCache, &t5998_TI, &t5998_0_0_0, &t5998_1_0_0, NULL, &t5998_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4645_TI;

#include "t1022.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern MethodInfo m31290_MI;
static PropertyInfo t4645____Current_PropertyInfo = 
{
	&t4645_TI, "Current", &m31290_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4645_PIs[] =
{
	&t4645____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1022_0_0_0;
extern void* RuntimeInvoker_t1022 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m31290_GM;
MethodInfo m31290_MI = 
{
	"get_Current", NULL, &t4645_TI, &t1022_0_0_0, RuntimeInvoker_t1022, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m31290_GM};
static MethodInfo* t4645_MIs[] =
{
	&m31290_MI,
	NULL
};
static TypeInfo* t4645_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4645_0_0_0;
extern Il2CppType t4645_1_0_0;
struct t4645;
extern Il2CppGenericClass t4645_GC;
TypeInfo t4645_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4645_MIs, t4645_PIs, NULL, NULL, NULL, NULL, NULL, &t4645_TI, t4645_ITIs, NULL, &EmptyCustomAttributesCache, &t4645_TI, &t4645_0_0_0, &t4645_1_0_0, NULL, &t4645_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
