﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t874;
struct t875;

 void m3800 (t874 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t875 * m3801 (t874 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3802 (t874 * __this, t875 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t875 * m3803 (t874 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3804 (t874 * __this, t875 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3805 (t874 * __this, int32_t* p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
