﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t553;
struct t29;
struct t204;
struct t204_marshaled;
struct t3104;
struct t3102;
struct t733;
struct t3105;
struct t20;
struct t136;
struct t3107;
struct t722;
#include "t552.h"
#include "t735.h"
#include "t3106.h"
#include "t3108.h"
#include "t725.h"

 void m17115 (t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17116 (t553 * __this, t29* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17117 (t553 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17118 (t553 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17119 (t553 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17120 (t553 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17121 (t553 * __this, t29 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17122 (t553 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17123 (t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17124 (t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17125 (t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17126 (t553 * __this, t3106  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17127 (t553 * __this, t3106  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17128 (t553 * __this, t3105* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17129 (t553 * __this, t3106  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17130 (t553 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17131 (t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m17132 (t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17133 (t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17134 (t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17135 (t553 * __this, t204 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17136 (t553 * __this, t204 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17137 (t553 * __this, int32_t p0, t29* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17138 (t553 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17139 (t553 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3106  m17140 (t29 * __this, t204 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17141 (t29 * __this, t204 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17142 (t553 * __this, t3105* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17143 (t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17144 (t553 * __this, t204 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17145 (t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17146 (t553 * __this, t204 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17147 (t553 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17148 (t553 * __this, t733 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17149 (t553 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17150 (t553 * __this, t204 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17151 (t553 * __this, t204 * p0, int32_t* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3104 * m17152 (t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t204 * m17153 (t553 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17154 (t553 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17155 (t553 * __this, t3106  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3108  m17156 (t553 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m17157 (t29 * __this, t204 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
