﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t395;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m1841 (t395 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14115 (t395 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m14116 (t395 * __this, float p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m14117 (t395 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
