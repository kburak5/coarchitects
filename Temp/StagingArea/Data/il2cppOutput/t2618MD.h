﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2618;
struct t29;
struct t557;
struct t2619;
struct t316;

#include "t2119MD.h"
#define m14091(__this, p0, p1, method) (void)m10321_gshared((t2119 *)__this, (t29 *)p0, (t557 *)p1, method)
#define m14092(__this, p0, method) (void)m10322_gshared((t2119 *)__this, (t2120 *)p0, method)
#define m14093(__this, p0, method) (void)m10323_gshared((t2119 *)__this, (t316*)p0, method)
#define m14094(__this, p0, p1, method) (bool)m10324_gshared((t2119 *)__this, (t29 *)p0, (t557 *)p1, method)
