﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1002;
struct t945;
struct t780;
#include "t1003.h"

 void m4521 (t1002 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4522 (t1002 * __this, t945 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4523 (t1002 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t945 * m4524 (t1002 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4525 (t1002 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4526 (t1002 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t780 * m4527 (t1002 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t780 * m4528 (t1002 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4529 (t1002 * __this, t780 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4530 (t1002 * __this, t780 * p0, t780 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
