﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t629;
#include "t1129.h"

 void m2915 (t629 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5329 (t629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2917 (t629 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5330 (t629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2916 (t629 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
