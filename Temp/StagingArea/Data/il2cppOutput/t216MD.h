﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t216;
struct t29;

 void m728 (t216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m729 (t216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m730 (t216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m731 (t216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m732 (t216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m733 (t216 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
