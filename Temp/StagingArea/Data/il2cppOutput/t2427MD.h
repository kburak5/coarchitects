﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2427;
struct t29;
struct t66;
struct t67;
#include "t35.h"
#include "t2420.h"

 void m12575 (t2427 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2420  m12576 (t2427 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12577 (t2427 * __this, t29 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2420  m12578 (t2427 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
