﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t966;
struct t967;
struct t7;
struct t29;
struct t42;
struct t316;

 t29 * m4291 (t29 * __this, t7* p0, t29 * p1, t7** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4292 (t29 * __this, t42 * p0, t7* p1, t316* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
