﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1519;
struct t42;
struct t731;
struct t29;
#include "t735.h"

 void m8130 (t1519 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8131 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8132 (t1519 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t731 * m8133 (t29 * __this, t42 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8134 (t29 * __this, t731 * p0, t29 * p1, t735  p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8135 (t1519 * __this, t29 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8136 (t1519 * __this, t29 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1519 * m8137 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
