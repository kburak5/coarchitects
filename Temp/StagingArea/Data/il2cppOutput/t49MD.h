﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t49;
struct t29;
struct t1094;
struct t42;
struct t20;
struct t7;
struct t719;
struct t446;
#include "t465.h"
#include "t1126.h"
#include "t1127.h"

 void m5862 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5863 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1252 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m1253 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m1254 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t465  m1255 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1126  m1256 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 double m1257 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int16_t m1258 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1259 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m1260 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int8_t m1261 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1262 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m1264 (t29 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m1265 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m1266 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m1267 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1269 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5864 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5865 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5866 (t29 * __this, t29 * p0, t20 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5867 (t29 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5239 (t29 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m5868 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m5869 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5870 (t29 * __this, t719 * p0, t446* p1, t7* p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint64_t m5871 (t29 * __this, t29 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4220 (t29 * __this, t42 * p0, t7* p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5872 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1268 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1250 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1263 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5873 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m1251 (t29 * __this, t7* p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5874 (t29 * __this, t42 * p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5875 (t29 * __this, t42 * p0, int16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5876 (t29 * __this, t42 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5877 (t29 * __this, t42 * p0, int64_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5878 (t29 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5879 (t29 * __this, t42 * p0, int8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5880 (t29 * __this, t42 * p0, uint16_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5881 (t29 * __this, t42 * p0, uint32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m5882 (t29 * __this, t42 * p0, uint64_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1248 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5883 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1249 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5884 (t29 * __this, t42 * p0, t29 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5885 (t29 * __this, t42 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m5886 (t29 * __this, t42 * p0, t29 * p1, t7* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
