﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3210;
struct t29;
struct t20;
#include "t787.h"

 void m17850 (t3210 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17851 (t3210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17852 (t3210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17853 (t3210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17854 (t3210 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
