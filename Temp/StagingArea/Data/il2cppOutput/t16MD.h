﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t16;
struct t25;
struct t7;
struct t28;
struct t42;
struct t20;
struct t29;
#include "t445.h"

 void m1777 (t16 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t28 * m2553 (t16 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t20 * m2554 (t16 * __this, t42 * p0, bool p1, bool p2, bool p3, bool p4, t29 * p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m1363 (t16 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1781 (t16 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1782 (t16 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1393 (t16 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2555 (t16 * __this, t7* p0, t29 * p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t28 * m2556 (t16 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t28 * m2557 (t16 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2558 (t29 * __this, t16 * p0, t7* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
