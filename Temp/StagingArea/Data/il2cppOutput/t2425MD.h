﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2425;
struct t29;
struct t2415;

 void m12550 (t2425 * __this, t2415 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m12551 (t2425 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m12552 (t2425 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12553 (t2425 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12554 (t2425 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
