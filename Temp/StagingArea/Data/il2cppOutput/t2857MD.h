﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2857;
struct t29;
struct t472;
struct t469;
struct t2864;
struct t20;
struct t136;
struct t2855;
#include "t2865.h"

 void m15555 (t2857 * __this, t472 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15556 (t2857 * __this, t469 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15557 (t2857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15558 (t2857 * __this, t469 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15559 (t2857 * __this, t469 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29* m15560 (t2857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15561 (t2857 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15562 (t2857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15563 (t2857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m15564 (t2857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m15565 (t2857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m15566 (t2857 * __this, t2855* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2865  m15567 (t2857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m15568 (t2857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
