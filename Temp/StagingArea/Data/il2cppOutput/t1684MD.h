﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1684;
struct t1684_marshaled;

void t1684_marshal(const t1684& unmarshaled, t1684_marshaled& marshaled);
void t1684_marshal_back(const t1684_marshaled& marshaled, t1684& unmarshaled);
void t1684_marshal_cleanup(t1684_marshaled& marshaled);
