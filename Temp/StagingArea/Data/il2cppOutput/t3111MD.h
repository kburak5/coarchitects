﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3111;
struct t29;
struct t20;
#include "t552.h"

 void m17174 (t3111 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17175 (t3111 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17176 (t3111 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17177 (t3111 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17178 (t3111 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
