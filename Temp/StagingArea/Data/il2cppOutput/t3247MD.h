﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3247;
struct t29;
struct t20;
#include "t1009.h"

 void m18031 (t3247 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18032 (t3247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18033 (t3247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18034 (t3247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m18035 (t3247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
