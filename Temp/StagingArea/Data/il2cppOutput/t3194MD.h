﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3194;
struct t29;
struct t7;
struct t770;
#include "t725.h"
#include "t3192.h"

 void m17779 (t3194 * __this, t770 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17780 (t3194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m17781 (t3194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17782 (t3194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17783 (t3194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17784 (t3194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3192  m17785 (t3194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m17786 (t3194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17787 (t3194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17788 (t3194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17789 (t3194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17790 (t3194 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
