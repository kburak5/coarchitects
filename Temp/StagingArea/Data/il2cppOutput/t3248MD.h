﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3248;
struct t29;
struct t20;
#include "t1015.h"

 void m18036 (t3248 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m18037 (t3248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m18038 (t3248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m18039 (t3248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m18040 (t3248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
