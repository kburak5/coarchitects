﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t569;
struct t556;
struct t29;
struct t557;
struct t316;

 void m2806 (t569 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2807 (t569 * __this, t556 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2808 (t569 * __this, t556 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2809 (t569 * __this, t29 * p0, t557 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2810 (t569 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2811 (t569 * __this, t316* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
