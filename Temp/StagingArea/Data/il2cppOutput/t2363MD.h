﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2363;

 void m12174 (t2363 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m12175 (t2363 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m12176 (t2363 * __this, int32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
