﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1673;
struct t29;
struct t1139;
struct t446;
struct t1299;
#include "t816.h"
#include "t465.h"

 void m9566 (t1673 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9567 (t1673 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9568 (t1673 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m9569 (t29 * __this, int32_t p0, t1139** p1, t446** p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1299 * m9570 (t1673 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t816  m9571 (t1673 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9572 (t1673 * __this, t1299 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1299 * m9573 (t1673 * __this, t1139* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
