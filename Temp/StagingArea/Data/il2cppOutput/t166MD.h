﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t166;
struct t3;
struct t24;
struct t6;
struct t56;
struct t167;
struct t155;
#include "t165.h"
#include "t17.h"

 void m488 (t166 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m489 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m490 (t166 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m491 (t166 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m492 (t166 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m493 (t166 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m494 (t166 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m495 (t166 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3 * m496 (t166 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m497 (t166 * __this, t6 * p0, t56 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t24 * m498 (t166 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m499 (t29 * __this, t3 * p0, t24 * p1, t17  p2, t167 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m500 (t29 * __this, t155 * p0, t155 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
