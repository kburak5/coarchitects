﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3238;
struct t29;
struct t20;

 void m17986 (t3238 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17987 (t3238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17988 (t3238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17989 (t3238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m17990 (t3238 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
