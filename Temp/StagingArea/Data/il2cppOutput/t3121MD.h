﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3121;
struct t29;
struct t20;
#include "t551.h"

 void m17244 (t3121 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17245 (t3121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17246 (t3121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17247 (t3121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint8_t m17248 (t3121 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
