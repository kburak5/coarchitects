﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t4139_TI;


#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IMaskable>
extern MethodInfo m27746_MI;
static PropertyInfo t4139____Current_PropertyInfo = 
{
	&t4139_TI, "Current", &m27746_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4139_PIs[] =
{
	&t4139____Current_PropertyInfo,
	NULL
};
extern Il2CppType t369_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27746_GM;
MethodInfo m27746_MI = 
{
	"get_Current", NULL, &t4139_TI, &t369_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27746_GM};
static MethodInfo* t4139_MIs[] =
{
	&m27746_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4139_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4139_0_0_0;
extern Il2CppType t4139_1_0_0;
struct t4139;
extern Il2CppGenericClass t4139_GC;
TypeInfo t4139_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4139_MIs, t4139_PIs, NULL, NULL, NULL, NULL, NULL, &t4139_TI, t4139_ITIs, NULL, &EmptyCustomAttributesCache, &t4139_TI, &t4139_0_0_0, &t4139_1_0_0, NULL, &t4139_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2484.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2484_TI;
#include "t2484MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t40.h"
extern TypeInfo t369_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m13046_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m20830_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m20830(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.IMaskable>
extern Il2CppType t20_0_0_1;
FieldInfo t2484_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2484_TI, offsetof(t2484, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2484_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2484_TI, offsetof(t2484, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2484_FIs[] =
{
	&t2484_f0_FieldInfo,
	&t2484_f1_FieldInfo,
	NULL
};
extern MethodInfo m13043_MI;
static PropertyInfo t2484____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2484_TI, "System.Collections.IEnumerator.Current", &m13043_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2484____Current_PropertyInfo = 
{
	&t2484_TI, "Current", &m13046_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2484_PIs[] =
{
	&t2484____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2484____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2484_m13042_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13042_GM;
MethodInfo m13042_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2484_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2484_m13042_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13042_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13043_GM;
MethodInfo m13043_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2484_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13043_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13044_GM;
MethodInfo m13044_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2484_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13044_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13045_GM;
MethodInfo m13045_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2484_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13045_GM};
extern Il2CppType t369_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13046_GM;
MethodInfo m13046_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2484_TI, &t369_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13046_GM};
static MethodInfo* t2484_MIs[] =
{
	&m13042_MI,
	&m13043_MI,
	&m13044_MI,
	&m13045_MI,
	&m13046_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m13045_MI;
extern MethodInfo m13044_MI;
static MethodInfo* t2484_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13043_MI,
	&m13045_MI,
	&m13044_MI,
	&m13046_MI,
};
static TypeInfo* t2484_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4139_TI,
};
static Il2CppInterfaceOffsetPair t2484_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4139_TI, 7},
};
extern TypeInfo t369_TI;
static Il2CppRGCTXData t2484_RGCTXData[3] = 
{
	&m13046_MI/* Method Usage */,
	&t369_TI/* Class Usage */,
	&m20830_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2484_0_0_0;
extern Il2CppType t2484_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2484_GC;
extern TypeInfo t20_TI;
TypeInfo t2484_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2484_MIs, t2484_PIs, t2484_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2484_TI, t2484_ITIs, t2484_VT, &EmptyCustomAttributesCache, &t2484_TI, &t2484_0_0_0, &t2484_1_0_0, t2484_IOs, &t2484_GC, NULL, NULL, NULL, t2484_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2484)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5311_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.IMaskable>
extern MethodInfo m27747_MI;
extern MethodInfo m27748_MI;
static PropertyInfo t5311____Item_PropertyInfo = 
{
	&t5311_TI, "Item", &m27747_MI, &m27748_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5311_PIs[] =
{
	&t5311____Item_PropertyInfo,
	NULL
};
extern Il2CppType t369_0_0_0;
extern Il2CppType t369_0_0_0;
static ParameterInfo t5311_m27749_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t369_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27749_GM;
MethodInfo m27749_MI = 
{
	"IndexOf", NULL, &t5311_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5311_m27749_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27749_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t369_0_0_0;
static ParameterInfo t5311_m27750_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t369_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27750_GM;
MethodInfo m27750_MI = 
{
	"Insert", NULL, &t5311_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5311_m27750_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27750_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5311_m27751_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27751_GM;
MethodInfo m27751_MI = 
{
	"RemoveAt", NULL, &t5311_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5311_m27751_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27751_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5311_m27747_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t369_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27747_GM;
MethodInfo m27747_MI = 
{
	"get_Item", NULL, &t5311_TI, &t369_0_0_0, RuntimeInvoker_t29_t44, t5311_m27747_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27747_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t369_0_0_0;
static ParameterInfo t5311_m27748_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t369_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27748_GM;
MethodInfo m27748_MI = 
{
	"set_Item", NULL, &t5311_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5311_m27748_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27748_GM};
static MethodInfo* t5311_MIs[] =
{
	&m27749_MI,
	&m27750_MI,
	&m27751_MI,
	&m27747_MI,
	&m27748_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5310_TI;
extern TypeInfo t5312_TI;
static TypeInfo* t5311_ITIs[] = 
{
	&t603_TI,
	&t5310_TI,
	&t5312_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5311_0_0_0;
extern Il2CppType t5311_1_0_0;
struct t5311;
extern Il2CppGenericClass t5311_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5311_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5311_MIs, t5311_PIs, NULL, NULL, NULL, NULL, NULL, &t5311_TI, t5311_ITIs, NULL, &t1908__CustomAttributeCache, &t5311_TI, &t5311_0_0_0, &t5311_1_0_0, NULL, &t5311_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t363_TI;

#include "t155.h"
#include "UnityEngine.UI_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Graphic>
extern MethodInfo m1630_MI;
static PropertyInfo t363____Count_PropertyInfo = 
{
	&t363_TI, "Count", &m1630_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27752_MI;
static PropertyInfo t363____IsReadOnly_PropertyInfo = 
{
	&t363_TI, "IsReadOnly", &m27752_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t363_PIs[] =
{
	&t363____Count_PropertyInfo,
	&t363____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1630_GM;
MethodInfo m1630_MI = 
{
	"get_Count", NULL, &t363_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1630_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27752_GM;
MethodInfo m27752_MI = 
{
	"get_IsReadOnly", NULL, &t363_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27752_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t363_m27753_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27753_GM;
MethodInfo m27753_MI = 
{
	"Add", NULL, &t363_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t363_m27753_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27753_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27754_GM;
MethodInfo m27754_MI = 
{
	"Clear", NULL, &t363_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27754_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t363_m27755_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27755_GM;
MethodInfo m27755_MI = 
{
	"Contains", NULL, &t363_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t363_m27755_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27755_GM};
extern Il2CppType t2545_0_0_0;
extern Il2CppType t2545_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t363_m27756_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2545_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27756_GM;
MethodInfo m27756_MI = 
{
	"CopyTo", NULL, &t363_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t363_m27756_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27756_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t363_m27757_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27757_GM;
MethodInfo m27757_MI = 
{
	"Remove", NULL, &t363_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t363_m27757_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27757_GM};
static MethodInfo* t363_MIs[] =
{
	&m1630_MI,
	&m27752_MI,
	&m27753_MI,
	&m27754_MI,
	&m27755_MI,
	&m27756_MI,
	&m27757_MI,
	NULL
};
extern TypeInfo t2547_TI;
static TypeInfo* t363_ITIs[] = 
{
	&t603_TI,
	&t2547_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t363_0_0_0;
extern Il2CppType t363_1_0_0;
struct t363;
extern Il2CppGenericClass t363_GC;
TypeInfo t363_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t363_MIs, t363_PIs, NULL, NULL, NULL, NULL, NULL, &t363_TI, t363_ITIs, NULL, &EmptyCustomAttributesCache, &t363_TI, &t363_0_0_0, &t363_1_0_0, NULL, &t363_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Graphic>
extern Il2CppType t2546_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27758_GM;
MethodInfo m27758_MI = 
{
	"GetEnumerator", NULL, &t2547_TI, &t2546_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27758_GM};
static MethodInfo* t2547_MIs[] =
{
	&m27758_MI,
	NULL
};
static TypeInfo* t2547_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2547_0_0_0;
extern Il2CppType t2547_1_0_0;
struct t2547;
extern Il2CppGenericClass t2547_GC;
TypeInfo t2547_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2547_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2547_TI, t2547_ITIs, NULL, &EmptyCustomAttributesCache, &t2547_TI, &t2547_0_0_0, &t2547_1_0_0, NULL, &t2547_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2546_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Graphic>
extern MethodInfo m27759_MI;
static PropertyInfo t2546____Current_PropertyInfo = 
{
	&t2546_TI, "Current", &m27759_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2546_PIs[] =
{
	&t2546____Current_PropertyInfo,
	NULL
};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27759_GM;
MethodInfo m27759_MI = 
{
	"get_Current", NULL, &t2546_TI, &t155_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27759_GM};
static MethodInfo* t2546_MIs[] =
{
	&m27759_MI,
	NULL
};
static TypeInfo* t2546_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2546_0_0_0;
extern Il2CppType t2546_1_0_0;
struct t2546;
extern Il2CppGenericClass t2546_GC;
TypeInfo t2546_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2546_MIs, t2546_PIs, NULL, NULL, NULL, NULL, NULL, &t2546_TI, t2546_ITIs, NULL, &EmptyCustomAttributesCache, &t2546_TI, &t2546_0_0_0, &t2546_1_0_0, NULL, &t2546_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2485.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2485_TI;
#include "t2485MD.h"

extern TypeInfo t155_TI;
extern MethodInfo m13051_MI;
extern MethodInfo m20841_MI;
struct t20;
#define m20841(__this, p0, method) (t155 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Graphic>
extern Il2CppType t20_0_0_1;
FieldInfo t2485_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2485_TI, offsetof(t2485, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2485_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2485_TI, offsetof(t2485, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2485_FIs[] =
{
	&t2485_f0_FieldInfo,
	&t2485_f1_FieldInfo,
	NULL
};
extern MethodInfo m13048_MI;
static PropertyInfo t2485____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2485_TI, "System.Collections.IEnumerator.Current", &m13048_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2485____Current_PropertyInfo = 
{
	&t2485_TI, "Current", &m13051_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2485_PIs[] =
{
	&t2485____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2485____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2485_m13047_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13047_GM;
MethodInfo m13047_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2485_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2485_m13047_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13047_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13048_GM;
MethodInfo m13048_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2485_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13048_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13049_GM;
MethodInfo m13049_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2485_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13049_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13050_GM;
MethodInfo m13050_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2485_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13050_GM};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13051_GM;
MethodInfo m13051_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2485_TI, &t155_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13051_GM};
static MethodInfo* t2485_MIs[] =
{
	&m13047_MI,
	&m13048_MI,
	&m13049_MI,
	&m13050_MI,
	&m13051_MI,
	NULL
};
extern MethodInfo m13050_MI;
extern MethodInfo m13049_MI;
static MethodInfo* t2485_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13048_MI,
	&m13050_MI,
	&m13049_MI,
	&m13051_MI,
};
static TypeInfo* t2485_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2546_TI,
};
static Il2CppInterfaceOffsetPair t2485_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2546_TI, 7},
};
extern TypeInfo t155_TI;
static Il2CppRGCTXData t2485_RGCTXData[3] = 
{
	&m13051_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
	&m20841_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2485_0_0_0;
extern Il2CppType t2485_1_0_0;
extern Il2CppGenericClass t2485_GC;
TypeInfo t2485_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2485_MIs, t2485_PIs, t2485_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2485_TI, t2485_ITIs, t2485_VT, &EmptyCustomAttributesCache, &t2485_TI, &t2485_0_0_0, &t2485_1_0_0, t2485_IOs, &t2485_GC, NULL, NULL, NULL, t2485_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2485)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t171_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Graphic>
extern MethodInfo m1627_MI;
extern MethodInfo m27760_MI;
static PropertyInfo t171____Item_PropertyInfo = 
{
	&t171_TI, "Item", &m1627_MI, &m27760_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t171_PIs[] =
{
	&t171____Item_PropertyInfo,
	NULL
};
extern Il2CppType t155_0_0_0;
static ParameterInfo t171_m27761_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27761_GM;
MethodInfo m27761_MI = 
{
	"IndexOf", NULL, &t171_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t171_m27761_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27761_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t171_m27762_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27762_GM;
MethodInfo m27762_MI = 
{
	"Insert", NULL, &t171_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t171_m27762_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27762_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t171_m27763_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27763_GM;
MethodInfo m27763_MI = 
{
	"RemoveAt", NULL, &t171_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t171_m27763_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27763_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t171_m1627_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t155_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1627_GM;
MethodInfo m1627_MI = 
{
	"get_Item", NULL, &t171_TI, &t155_0_0_0, RuntimeInvoker_t29_t44, t171_m1627_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1627_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t171_m27760_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27760_GM;
MethodInfo m27760_MI = 
{
	"set_Item", NULL, &t171_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t171_m27760_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27760_GM};
static MethodInfo* t171_MIs[] =
{
	&m27761_MI,
	&m27762_MI,
	&m27763_MI,
	&m1627_MI,
	&m27760_MI,
	NULL
};
static TypeInfo* t171_ITIs[] = 
{
	&t603_TI,
	&t363_TI,
	&t2547_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t171_0_0_0;
extern Il2CppType t171_1_0_0;
struct t171;
extern Il2CppGenericClass t171_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t171_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t171_MIs, t171_PIs, NULL, NULL, NULL, NULL, NULL, &t171_TI, t171_ITIs, NULL, &t1908__CustomAttributeCache, &t171_TI, &t171_0_0_0, &t171_1_0_0, NULL, &t171_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2480.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2480_TI;
#include "t2480MD.h"

#include "t15.h"
#include "t350.h"
#include "t42.h"
#include "t1101.h"
extern TypeInfo t15_TI;
extern TypeInfo t350_TI;
extern TypeInfo t42_TI;
extern TypeInfo t7_TI;
extern TypeInfo t1101_TI;
#include "t29MD.h"
#include "t42MD.h"
#include "t1101MD.h"
extern MethodInfo m13055_MI;
extern MethodInfo m3974_MI;
extern MethodInfo m1430_MI;
extern MethodInfo m2998_MI;
extern MethodInfo m5150_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>
extern Il2CppType t350_0_0_1;
FieldInfo t2480_f0_FieldInfo = 
{
	"l", &t350_0_0_1, &t2480_TI, offsetof(t2480, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2480_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2480_TI, offsetof(t2480, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2480_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2480_TI, offsetof(t2480, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t15_0_0_1;
FieldInfo t2480_f3_FieldInfo = 
{
	"current", &t15_0_0_1, &t2480_TI, offsetof(t2480, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2480_FIs[] =
{
	&t2480_f0_FieldInfo,
	&t2480_f1_FieldInfo,
	&t2480_f2_FieldInfo,
	&t2480_f3_FieldInfo,
	NULL
};
extern MethodInfo m13053_MI;
static PropertyInfo t2480____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2480_TI, "System.Collections.IEnumerator.Current", &m13053_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13057_MI;
static PropertyInfo t2480____Current_PropertyInfo = 
{
	&t2480_TI, "Current", &m13057_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2480_PIs[] =
{
	&t2480____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2480____Current_PropertyInfo,
	NULL
};
extern Il2CppType t350_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t2480_m13052_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13052_GM;
MethodInfo m13052_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2480_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2480_m13052_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13052_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13053_GM;
MethodInfo m13053_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2480_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13053_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13054_GM;
MethodInfo m13054_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2480_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13054_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13055_GM;
MethodInfo m13055_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2480_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13055_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13056_GM;
MethodInfo m13056_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2480_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13056_GM};
extern Il2CppType t15_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13057_GM;
MethodInfo m13057_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2480_TI, &t15_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13057_GM};
static MethodInfo* t2480_MIs[] =
{
	&m13052_MI,
	&m13053_MI,
	&m13054_MI,
	&m13055_MI,
	&m13056_MI,
	&m13057_MI,
	NULL
};
extern MethodInfo m13056_MI;
extern MethodInfo m13054_MI;
static MethodInfo* t2480_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13053_MI,
	&m13056_MI,
	&m13054_MI,
	&m13057_MI,
};
extern TypeInfo t2474_TI;
static TypeInfo* t2480_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2474_TI,
};
static Il2CppInterfaceOffsetPair t2480_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2474_TI, 7},
};
extern TypeInfo t15_TI;
extern TypeInfo t2480_TI;
static Il2CppRGCTXData t2480_RGCTXData[3] = 
{
	&m13055_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
	&t2480_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2480_0_0_0;
extern Il2CppType t2480_1_0_0;
extern Il2CppGenericClass t2480_GC;
extern TypeInfo t1261_TI;
TypeInfo t2480_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2480_MIs, t2480_PIs, t2480_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2480_TI, t2480_ITIs, t2480_VT, &EmptyCustomAttributesCache, &t2480_TI, &t2480_0_0_0, &t2480_1_0_0, t2480_IOs, &t2480_GC, NULL, NULL, NULL, t2480_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2480)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#include "t2477.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2477_TI;
#include "t2477MD.h"

#include "t345.h"
#include "t338.h"
extern TypeInfo t44_TI;
extern TypeInfo t345_TI;
extern TypeInfo t2486_TI;
extern TypeInfo t2475_TI;
extern TypeInfo t338_TI;
extern TypeInfo t674_TI;
extern TypeInfo t21_TI;
extern TypeInfo t40_TI;
extern TypeInfo t2473_TI;
extern TypeInfo t2476_TI;
#include "t345MD.h"
#include "t338MD.h"
#include "t2487MD.h"
extern MethodInfo m13087_MI;
extern MethodInfo m1516_MI;
extern MethodInfo m27705_MI;
extern MethodInfo m27696_MI;
extern MethodInfo m1331_MI;
extern MethodInfo m2950_MI;
extern MethodInfo m3960_MI;
extern MethodInfo m4154_MI;
extern MethodInfo m13119_MI;
extern MethodInfo m27703_MI;
extern MethodInfo m27707_MI;
extern MethodInfo m27697_MI;
extern MethodInfo m27698_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.Text>
extern Il2CppType t2486_0_0_1;
FieldInfo t2477_f0_FieldInfo = 
{
	"list", &t2486_0_0_1, &t2477_TI, offsetof(t2477, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2477_FIs[] =
{
	&t2477_f0_FieldInfo,
	NULL
};
extern MethodInfo m13064_MI;
extern MethodInfo m13065_MI;
static PropertyInfo t2477____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2477_TI, "System.Collections.Generic.IList<T>.Item", &m13064_MI, &m13065_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13066_MI;
static PropertyInfo t2477____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2477_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m13066_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13076_MI;
static PropertyInfo t2477____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2477_TI, "System.Collections.ICollection.IsSynchronized", &m13076_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13077_MI;
static PropertyInfo t2477____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2477_TI, "System.Collections.ICollection.SyncRoot", &m13077_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13078_MI;
static PropertyInfo t2477____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2477_TI, "System.Collections.IList.IsFixedSize", &m13078_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13079_MI;
static PropertyInfo t2477____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2477_TI, "System.Collections.IList.IsReadOnly", &m13079_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13080_MI;
extern MethodInfo m13081_MI;
static PropertyInfo t2477____System_Collections_IList_Item_PropertyInfo = 
{
	&t2477_TI, "System.Collections.IList.Item", &m13080_MI, &m13081_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13086_MI;
static PropertyInfo t2477____Count_PropertyInfo = 
{
	&t2477_TI, "Count", &m13086_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2477____Item_PropertyInfo = 
{
	&t2477_TI, "Item", &m13087_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2477_PIs[] =
{
	&t2477____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2477____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2477____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2477____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2477____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2477____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2477____System_Collections_IList_Item_PropertyInfo,
	&t2477____Count_PropertyInfo,
	&t2477____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2486_0_0_0;
extern Il2CppType t2486_0_0_0;
static ParameterInfo t2477_m13058_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2486_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13058_GM;
MethodInfo m13058_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2477_m13058_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13058_GM};
extern Il2CppType t15_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2477_m13059_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13059_GM;
MethodInfo m13059_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2477_m13059_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13059_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13060_GM;
MethodInfo m13060_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13060_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2477_m13061_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13061_GM;
MethodInfo m13061_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2477_m13061_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13061_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2477_m13062_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13062_GM;
MethodInfo m13062_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2477_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2477_m13062_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13062_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2477_m13063_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13063_GM;
MethodInfo m13063_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2477_m13063_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13063_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2477_m13064_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t15_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13064_GM;
MethodInfo m13064_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2477_TI, &t15_0_0_0, RuntimeInvoker_t29_t44, t2477_m13064_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13064_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2477_m13065_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13065_GM;
MethodInfo m13065_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2477_m13065_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13065_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13066_GM;
MethodInfo m13066_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2477_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13066_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2477_m13067_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13067_GM;
MethodInfo m13067_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2477_m13067_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13067_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13068_GM;
MethodInfo m13068_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2477_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13068_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2477_m13069_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13069_GM;
MethodInfo m13069_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2477_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2477_m13069_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13069_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13070_GM;
MethodInfo m13070_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13070_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2477_m13071_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13071_GM;
MethodInfo m13071_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2477_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2477_m13071_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13071_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2477_m13072_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13072_GM;
MethodInfo m13072_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2477_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2477_m13072_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13072_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2477_m13073_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13073_GM;
MethodInfo m13073_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2477_m13073_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13073_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2477_m13074_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13074_GM;
MethodInfo m13074_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2477_m13074_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13074_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2477_m13075_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13075_GM;
MethodInfo m13075_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2477_m13075_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13075_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13076_GM;
MethodInfo m13076_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2477_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13076_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13077_GM;
MethodInfo m13077_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2477_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13077_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13078_GM;
MethodInfo m13078_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2477_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13078_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13079_GM;
MethodInfo m13079_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2477_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13079_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2477_m13080_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13080_GM;
MethodInfo m13080_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2477_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2477_m13080_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13080_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2477_m13081_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13081_GM;
MethodInfo m13081_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2477_m13081_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13081_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2477_m13082_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13082_GM;
MethodInfo m13082_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2477_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2477_m13082_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13082_GM};
extern Il2CppType t2473_0_0_0;
extern Il2CppType t2473_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2477_m13083_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2473_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13083_GM;
MethodInfo m13083_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2477_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2477_m13083_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13083_GM};
extern Il2CppType t2474_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13084_GM;
MethodInfo m13084_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2477_TI, &t2474_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13084_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2477_m13085_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13085_GM;
MethodInfo m13085_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2477_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2477_m13085_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13085_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13086_GM;
MethodInfo m13086_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2477_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13086_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2477_m13087_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t15_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13087_GM;
MethodInfo m13087_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2477_TI, &t15_0_0_0, RuntimeInvoker_t29_t44, t2477_m13087_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13087_GM};
static MethodInfo* t2477_MIs[] =
{
	&m13058_MI,
	&m13059_MI,
	&m13060_MI,
	&m13061_MI,
	&m13062_MI,
	&m13063_MI,
	&m13064_MI,
	&m13065_MI,
	&m13066_MI,
	&m13067_MI,
	&m13068_MI,
	&m13069_MI,
	&m13070_MI,
	&m13071_MI,
	&m13072_MI,
	&m13073_MI,
	&m13074_MI,
	&m13075_MI,
	&m13076_MI,
	&m13077_MI,
	&m13078_MI,
	&m13079_MI,
	&m13080_MI,
	&m13081_MI,
	&m13082_MI,
	&m13083_MI,
	&m13084_MI,
	&m13085_MI,
	&m13086_MI,
	&m13087_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m13068_MI;
extern MethodInfo m13067_MI;
extern MethodInfo m13069_MI;
extern MethodInfo m13070_MI;
extern MethodInfo m13071_MI;
extern MethodInfo m13072_MI;
extern MethodInfo m13073_MI;
extern MethodInfo m13074_MI;
extern MethodInfo m13075_MI;
extern MethodInfo m13059_MI;
extern MethodInfo m13060_MI;
extern MethodInfo m13082_MI;
extern MethodInfo m13083_MI;
extern MethodInfo m13062_MI;
extern MethodInfo m13085_MI;
extern MethodInfo m13061_MI;
extern MethodInfo m13063_MI;
extern MethodInfo m13084_MI;
static MethodInfo* t2477_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13068_MI,
	&m13086_MI,
	&m13076_MI,
	&m13077_MI,
	&m13067_MI,
	&m13078_MI,
	&m13079_MI,
	&m13080_MI,
	&m13081_MI,
	&m13069_MI,
	&m13070_MI,
	&m13071_MI,
	&m13072_MI,
	&m13073_MI,
	&m13074_MI,
	&m13075_MI,
	&m13086_MI,
	&m13066_MI,
	&m13059_MI,
	&m13060_MI,
	&m13082_MI,
	&m13083_MI,
	&m13062_MI,
	&m13085_MI,
	&m13061_MI,
	&m13063_MI,
	&m13064_MI,
	&m13065_MI,
	&m13084_MI,
	&m13087_MI,
};
extern TypeInfo t868_TI;
static TypeInfo* t2477_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2475_TI,
	&t2486_TI,
	&t2476_TI,
};
static Il2CppInterfaceOffsetPair t2477_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2475_TI, 20},
	{ &t2486_TI, 27},
	{ &t2476_TI, 32},
};
extern TypeInfo t15_TI;
static Il2CppRGCTXData t2477_RGCTXData[9] = 
{
	&m13087_MI/* Method Usage */,
	&m13119_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
	&m27703_MI/* Method Usage */,
	&m27707_MI/* Method Usage */,
	&m27705_MI/* Method Usage */,
	&m27697_MI/* Method Usage */,
	&m27698_MI/* Method Usage */,
	&m27696_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2477_0_0_0;
extern Il2CppType t2477_1_0_0;
extern TypeInfo t29_TI;
struct t2477;
extern Il2CppGenericClass t2477_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2477_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2477_MIs, t2477_PIs, t2477_FIs, NULL, &t29_TI, NULL, NULL, &t2477_TI, t2477_ITIs, t2477_VT, &t1263__CustomAttributeCache, &t2477_TI, &t2477_0_0_0, &t2477_1_0_0, t2477_IOs, &t2477_GC, NULL, NULL, NULL, t2477_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2477), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2487.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2487_TI;

#include "t43.h"
#include "t305.h"
extern TypeInfo t305_TI;
#include "t350MD.h"
#include "t305MD.h"
extern MethodInfo m27700_MI;
extern MethodInfo m13122_MI;
extern MethodInfo m13123_MI;
extern MethodInfo m13120_MI;
extern MethodInfo m13118_MI;
extern MethodInfo m1543_MI;
extern MethodInfo m9816_MI;
extern MethodInfo m13111_MI;
extern MethodInfo m13121_MI;
extern MethodInfo m13109_MI;
extern MethodInfo m13114_MI;
extern MethodInfo m13105_MI;
extern MethodInfo m27702_MI;
extern MethodInfo m27708_MI;
extern MethodInfo m27709_MI;
extern MethodInfo m27706_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m6004_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m9815_MI;
extern MethodInfo m9817_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.UI.Text>
extern Il2CppType t2486_0_0_1;
FieldInfo t2487_f0_FieldInfo = 
{
	"list", &t2486_0_0_1, &t2487_TI, offsetof(t2487, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2487_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2487_TI, offsetof(t2487, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2487_FIs[] =
{
	&t2487_f0_FieldInfo,
	&t2487_f1_FieldInfo,
	NULL
};
extern MethodInfo m13089_MI;
static PropertyInfo t2487____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2487_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m13089_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13097_MI;
static PropertyInfo t2487____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2487_TI, "System.Collections.ICollection.IsSynchronized", &m13097_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13098_MI;
static PropertyInfo t2487____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2487_TI, "System.Collections.ICollection.SyncRoot", &m13098_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13099_MI;
static PropertyInfo t2487____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2487_TI, "System.Collections.IList.IsFixedSize", &m13099_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13100_MI;
static PropertyInfo t2487____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2487_TI, "System.Collections.IList.IsReadOnly", &m13100_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13101_MI;
extern MethodInfo m13102_MI;
static PropertyInfo t2487____System_Collections_IList_Item_PropertyInfo = 
{
	&t2487_TI, "System.Collections.IList.Item", &m13101_MI, &m13102_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13115_MI;
static PropertyInfo t2487____Count_PropertyInfo = 
{
	&t2487_TI, "Count", &m13115_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13116_MI;
extern MethodInfo m13117_MI;
static PropertyInfo t2487____Item_PropertyInfo = 
{
	&t2487_TI, "Item", &m13116_MI, &m13117_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2487_PIs[] =
{
	&t2487____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2487____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2487____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2487____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2487____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2487____System_Collections_IList_Item_PropertyInfo,
	&t2487____Count_PropertyInfo,
	&t2487____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13088_GM;
MethodInfo m13088_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13088_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13089_GM;
MethodInfo m13089_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2487_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13089_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2487_m13090_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13090_GM;
MethodInfo m13090_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2487_m13090_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13090_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13091_GM;
MethodInfo m13091_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2487_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13091_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2487_m13092_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13092_GM;
MethodInfo m13092_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2487_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2487_m13092_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13092_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2487_m13093_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13093_GM;
MethodInfo m13093_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2487_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2487_m13093_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13093_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2487_m13094_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13094_GM;
MethodInfo m13094_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2487_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2487_m13094_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13094_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2487_m13095_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13095_GM;
MethodInfo m13095_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2487_m13095_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13095_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2487_m13096_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13096_GM;
MethodInfo m13096_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2487_m13096_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13096_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13097_GM;
MethodInfo m13097_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2487_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13097_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13098_GM;
MethodInfo m13098_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2487_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13098_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13099_GM;
MethodInfo m13099_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2487_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13099_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13100_GM;
MethodInfo m13100_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2487_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13100_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2487_m13101_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13101_GM;
MethodInfo m13101_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2487_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2487_m13101_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13101_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2487_m13102_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13102_GM;
MethodInfo m13102_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2487_m13102_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13102_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2487_m13103_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13103_GM;
MethodInfo m13103_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2487_m13103_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13103_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13104_GM;
MethodInfo m13104_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13104_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13105_GM;
MethodInfo m13105_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13105_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2487_m13106_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13106_GM;
MethodInfo m13106_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2487_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2487_m13106_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13106_GM};
extern Il2CppType t2473_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2487_m13107_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2473_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13107_GM;
MethodInfo m13107_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2487_m13107_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13107_GM};
extern Il2CppType t2474_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13108_GM;
MethodInfo m13108_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2487_TI, &t2474_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13108_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2487_m13109_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13109_GM;
MethodInfo m13109_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2487_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2487_m13109_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13109_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2487_m13110_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13110_GM;
MethodInfo m13110_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2487_m13110_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13110_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2487_m13111_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13111_GM;
MethodInfo m13111_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2487_m13111_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13111_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2487_m13112_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13112_GM;
MethodInfo m13112_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2487_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2487_m13112_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13112_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2487_m13113_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13113_GM;
MethodInfo m13113_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2487_m13113_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13113_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2487_m13114_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13114_GM;
MethodInfo m13114_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2487_m13114_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13114_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13115_GM;
MethodInfo m13115_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2487_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13115_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2487_m13116_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t15_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13116_GM;
MethodInfo m13116_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2487_TI, &t15_0_0_0, RuntimeInvoker_t29_t44, t2487_m13116_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13116_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2487_m13117_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13117_GM;
MethodInfo m13117_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2487_m13117_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13117_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2487_m13118_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13118_GM;
MethodInfo m13118_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2487_m13118_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13118_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2487_m13119_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13119_GM;
MethodInfo m13119_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2487_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2487_m13119_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13119_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2487_m13120_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t15_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13120_GM;
MethodInfo m13120_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2487_TI, &t15_0_0_0, RuntimeInvoker_t29_t29, t2487_m13120_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13120_GM};
extern Il2CppType t2486_0_0_0;
static ParameterInfo t2487_m13121_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2486_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13121_GM;
MethodInfo m13121_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2487_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2487_m13121_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13121_GM};
extern Il2CppType t2486_0_0_0;
static ParameterInfo t2487_m13122_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2486_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13122_GM;
MethodInfo m13122_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2487_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2487_m13122_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13122_GM};
extern Il2CppType t2486_0_0_0;
static ParameterInfo t2487_m13123_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2486_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13123_GM;
MethodInfo m13123_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2487_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2487_m13123_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13123_GM};
static MethodInfo* t2487_MIs[] =
{
	&m13088_MI,
	&m13089_MI,
	&m13090_MI,
	&m13091_MI,
	&m13092_MI,
	&m13093_MI,
	&m13094_MI,
	&m13095_MI,
	&m13096_MI,
	&m13097_MI,
	&m13098_MI,
	&m13099_MI,
	&m13100_MI,
	&m13101_MI,
	&m13102_MI,
	&m13103_MI,
	&m13104_MI,
	&m13105_MI,
	&m13106_MI,
	&m13107_MI,
	&m13108_MI,
	&m13109_MI,
	&m13110_MI,
	&m13111_MI,
	&m13112_MI,
	&m13113_MI,
	&m13114_MI,
	&m13115_MI,
	&m13116_MI,
	&m13117_MI,
	&m13118_MI,
	&m13119_MI,
	&m13120_MI,
	&m13121_MI,
	&m13122_MI,
	&m13123_MI,
	NULL
};
extern MethodInfo m13091_MI;
extern MethodInfo m13090_MI;
extern MethodInfo m13092_MI;
extern MethodInfo m13104_MI;
extern MethodInfo m13093_MI;
extern MethodInfo m13094_MI;
extern MethodInfo m13095_MI;
extern MethodInfo m13096_MI;
extern MethodInfo m13113_MI;
extern MethodInfo m13103_MI;
extern MethodInfo m13106_MI;
extern MethodInfo m13107_MI;
extern MethodInfo m13112_MI;
extern MethodInfo m13110_MI;
extern MethodInfo m13108_MI;
static MethodInfo* t2487_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13091_MI,
	&m13115_MI,
	&m13097_MI,
	&m13098_MI,
	&m13090_MI,
	&m13099_MI,
	&m13100_MI,
	&m13101_MI,
	&m13102_MI,
	&m13092_MI,
	&m13104_MI,
	&m13093_MI,
	&m13094_MI,
	&m13095_MI,
	&m13096_MI,
	&m13113_MI,
	&m13115_MI,
	&m13089_MI,
	&m13103_MI,
	&m13104_MI,
	&m13106_MI,
	&m13107_MI,
	&m13112_MI,
	&m13109_MI,
	&m13110_MI,
	&m13113_MI,
	&m13116_MI,
	&m13117_MI,
	&m13108_MI,
	&m13105_MI,
	&m13111_MI,
	&m13114_MI,
	&m13118_MI,
};
static TypeInfo* t2487_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2475_TI,
	&t2486_TI,
	&t2476_TI,
};
static Il2CppInterfaceOffsetPair t2487_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2475_TI, 20},
	{ &t2486_TI, 27},
	{ &t2476_TI, 32},
};
extern TypeInfo t350_TI;
extern TypeInfo t15_TI;
static Il2CppRGCTXData t2487_RGCTXData[25] = 
{
	&t350_TI/* Class Usage */,
	&m1543_MI/* Method Usage */,
	&m27700_MI/* Method Usage */,
	&m27698_MI/* Method Usage */,
	&m27696_MI/* Method Usage */,
	&m13120_MI/* Method Usage */,
	&m13111_MI/* Method Usage */,
	&m13119_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
	&m27703_MI/* Method Usage */,
	&m27707_MI/* Method Usage */,
	&m13121_MI/* Method Usage */,
	&m13109_MI/* Method Usage */,
	&m13114_MI/* Method Usage */,
	&m13122_MI/* Method Usage */,
	&m13123_MI/* Method Usage */,
	&m27705_MI/* Method Usage */,
	&m13118_MI/* Method Usage */,
	&m13105_MI/* Method Usage */,
	&m27702_MI/* Method Usage */,
	&m27697_MI/* Method Usage */,
	&m27708_MI/* Method Usage */,
	&m27709_MI/* Method Usage */,
	&m27706_MI/* Method Usage */,
	&t15_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2487_0_0_0;
extern Il2CppType t2487_1_0_0;
struct t2487;
extern Il2CppGenericClass t2487_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2487_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2487_MIs, t2487_PIs, t2487_FIs, NULL, &t29_TI, NULL, NULL, &t2487_TI, t2487_ITIs, t2487_VT, &t1262__CustomAttributeCache, &t2487_TI, &t2487_0_0_0, &t2487_1_0_0, t2487_IOs, &t2487_GC, NULL, NULL, NULL, t2487_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2487), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#include "t2488.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2488_TI;
#include "t2488MD.h"

#include "t1257.h"
#include "mscorlib_ArrayTypes.h"
#include "t2489.h"
extern TypeInfo t6678_TI;
extern TypeInfo t1257_TI;
extern TypeInfo t537_TI;
extern TypeInfo t2489_TI;
#include "t931MD.h"
#include "t2489MD.h"
extern Il2CppType t6678_0_0_0;
extern Il2CppType t1257_0_0_0;
extern MethodInfo m2981_MI;
extern MethodInfo m2979_MI;
extern MethodInfo m8828_MI;
extern MethodInfo m13129_MI;
extern MethodInfo m27764_MI;
extern MethodInfo m20853_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Text>
extern Il2CppType t2488_0_0_49;
FieldInfo t2488_f0_FieldInfo = 
{
	"_default", &t2488_0_0_49, &t2488_TI, offsetof(t2488_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2488_FIs[] =
{
	&t2488_f0_FieldInfo,
	NULL
};
extern MethodInfo m13128_MI;
static PropertyInfo t2488____Default_PropertyInfo = 
{
	&t2488_TI, "Default", &m13128_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2488_PIs[] =
{
	&t2488____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13124_GM;
MethodInfo m13124_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2488_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13124_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13125_GM;
MethodInfo m13125_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2488_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13125_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2488_m13126_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13126_GM;
MethodInfo m13126_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2488_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2488_m13126_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13126_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2488_m13127_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13127_GM;
MethodInfo m13127_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2488_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2488_m13127_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13127_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2488_m27764_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27764_GM;
MethodInfo m27764_MI = 
{
	"GetHashCode", NULL, &t2488_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2488_m27764_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27764_GM};
extern Il2CppType t15_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2488_m20853_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20853_GM;
MethodInfo m20853_MI = 
{
	"Equals", NULL, &t2488_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2488_m20853_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20853_GM};
extern Il2CppType t2488_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13128_GM;
MethodInfo m13128_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2488_TI, &t2488_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13128_GM};
static MethodInfo* t2488_MIs[] =
{
	&m13124_MI,
	&m13125_MI,
	&m13126_MI,
	&m13127_MI,
	&m27764_MI,
	&m20853_MI,
	&m13128_MI,
	NULL
};
extern MethodInfo m13127_MI;
extern MethodInfo m13126_MI;
static MethodInfo* t2488_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m20853_MI,
	&m27764_MI,
	&m13127_MI,
	&m13126_MI,
	NULL,
	NULL,
};
extern TypeInfo t6679_TI;
extern TypeInfo t734_TI;
static TypeInfo* t2488_ITIs[] = 
{
	&t6679_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2488_IOs[] = 
{
	{ &t6679_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2488_TI;
extern TypeInfo t2488_TI;
extern TypeInfo t2489_TI;
extern TypeInfo t15_TI;
static Il2CppRGCTXData t2488_RGCTXData[9] = 
{
	&t6678_0_0_0/* Type Usage */,
	&t15_0_0_0/* Type Usage */,
	&t2488_TI/* Class Usage */,
	&t2488_TI/* Static Usage */,
	&t2489_TI/* Class Usage */,
	&m13129_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
	&m27764_MI/* Method Usage */,
	&m20853_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2488_0_0_0;
extern Il2CppType t2488_1_0_0;
struct t2488;
extern Il2CppGenericClass t2488_GC;
TypeInfo t2488_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2488_MIs, t2488_PIs, t2488_FIs, NULL, &t29_TI, NULL, NULL, &t2488_TI, t2488_ITIs, t2488_VT, &EmptyCustomAttributesCache, &t2488_TI, &t2488_0_0_0, &t2488_1_0_0, t2488_IOs, &t2488_GC, NULL, NULL, NULL, t2488_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2488), 0, -1, sizeof(t2488_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.UI.Text>
extern Il2CppType t15_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t6679_m27765_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27765_GM;
MethodInfo m27765_MI = 
{
	"Equals", NULL, &t6679_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6679_m27765_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27765_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t6679_m27766_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27766_GM;
MethodInfo m27766_MI = 
{
	"GetHashCode", NULL, &t6679_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6679_m27766_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27766_GM};
static MethodInfo* t6679_MIs[] =
{
	&m27765_MI,
	&m27766_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6679_0_0_0;
extern Il2CppType t6679_1_0_0;
struct t6679;
extern Il2CppGenericClass t6679_GC;
TypeInfo t6679_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6679_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6679_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6679_TI, &t6679_0_0_0, &t6679_1_0_0, NULL, &t6679_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.UI.Text>
extern Il2CppType t15_0_0_0;
static ParameterInfo t6678_m27767_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27767_GM;
MethodInfo m27767_MI = 
{
	"Equals", NULL, &t6678_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6678_m27767_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27767_GM};
static MethodInfo* t6678_MIs[] =
{
	&m27767_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6678_1_0_0;
struct t6678;
extern Il2CppGenericClass t6678_GC;
TypeInfo t6678_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6678_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6678_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6678_TI, &t6678_0_0_0, &t6678_1_0_0, NULL, &t6678_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13124_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UI.Text>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13129_GM;
MethodInfo m13129_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2489_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13129_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2489_m13130_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13130_GM;
MethodInfo m13130_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2489_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2489_m13130_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13130_GM};
extern Il2CppType t15_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2489_m13131_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13131_GM;
MethodInfo m13131_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2489_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2489_m13131_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13131_GM};
static MethodInfo* t2489_MIs[] =
{
	&m13129_MI,
	&m13130_MI,
	&m13131_MI,
	NULL
};
extern MethodInfo m13131_MI;
extern MethodInfo m13130_MI;
static MethodInfo* t2489_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13131_MI,
	&m13130_MI,
	&m13127_MI,
	&m13126_MI,
	&m13130_MI,
	&m13131_MI,
};
static Il2CppInterfaceOffsetPair t2489_IOs[] = 
{
	{ &t6679_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2488_TI;
extern TypeInfo t2488_TI;
extern TypeInfo t2489_TI;
extern TypeInfo t15_TI;
extern TypeInfo t15_TI;
static Il2CppRGCTXData t2489_RGCTXData[11] = 
{
	&t6678_0_0_0/* Type Usage */,
	&t15_0_0_0/* Type Usage */,
	&t2488_TI/* Class Usage */,
	&t2488_TI/* Static Usage */,
	&t2489_TI/* Class Usage */,
	&m13129_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
	&m27764_MI/* Method Usage */,
	&m20853_MI/* Method Usage */,
	&m13124_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2489_0_0_0;
extern Il2CppType t2489_1_0_0;
struct t2489;
extern Il2CppGenericClass t2489_GC;
extern TypeInfo t1256_TI;
TypeInfo t2489_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2489_MIs, NULL, NULL, NULL, &t2488_TI, NULL, &t1256_TI, &t2489_TI, NULL, t2489_VT, &EmptyCustomAttributesCache, &t2489_TI, &t2489_0_0_0, &t2489_1_0_0, t2489_IOs, &t2489_GC, NULL, NULL, NULL, t2489_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2489), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t2478.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2478_TI;
#include "t2478MD.h"

#include "t35.h"
#include "t67.h"


// Metadata Definition System.Predicate`1<UnityEngine.UI.Text>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2478_m13132_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13132_GM;
MethodInfo m13132_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2478_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2478_m13132_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13132_GM};
extern Il2CppType t15_0_0_0;
static ParameterInfo t2478_m13133_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13133_GM;
MethodInfo m13133_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2478_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2478_m13133_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13133_GM};
extern Il2CppType t15_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2478_m13134_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13134_GM;
MethodInfo m13134_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2478_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2478_m13134_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13134_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2478_m13135_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13135_GM;
MethodInfo m13135_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2478_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2478_m13135_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13135_GM};
static MethodInfo* t2478_MIs[] =
{
	&m13132_MI,
	&m13133_MI,
	&m13134_MI,
	&m13135_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m13133_MI;
extern MethodInfo m13134_MI;
extern MethodInfo m13135_MI;
static MethodInfo* t2478_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13133_MI,
	&m13134_MI,
	&m13135_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2478_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2478_0_0_0;
extern Il2CppType t2478_1_0_0;
extern TypeInfo t195_TI;
struct t2478;
extern Il2CppGenericClass t2478_GC;
TypeInfo t2478_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2478_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2478_TI, NULL, t2478_VT, &EmptyCustomAttributesCache, &t2478_TI, &t2478_0_0_0, &t2478_1_0_0, t2478_IOs, &t2478_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2478), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2490.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2490_TI;
#include "t2490MD.h"

#include "t1247.h"
#include "t2491.h"
extern TypeInfo t4143_TI;
extern TypeInfo t1247_TI;
extern TypeInfo t2491_TI;
#include "t2491MD.h"
extern Il2CppType t4143_0_0_0;
extern Il2CppType t1247_0_0_0;
extern MethodInfo m13140_MI;
extern MethodInfo m27768_MI;
extern MethodInfo m8852_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.UI.Text>
extern Il2CppType t2490_0_0_49;
FieldInfo t2490_f0_FieldInfo = 
{
	"_default", &t2490_0_0_49, &t2490_TI, offsetof(t2490_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2490_FIs[] =
{
	&t2490_f0_FieldInfo,
	NULL
};
extern MethodInfo m13139_MI;
static PropertyInfo t2490____Default_PropertyInfo = 
{
	&t2490_TI, "Default", &m13139_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2490_PIs[] =
{
	&t2490____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13136_GM;
MethodInfo m13136_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2490_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13136_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13137_GM;
MethodInfo m13137_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2490_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13137_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2490_m13138_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13138_GM;
MethodInfo m13138_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2490_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2490_m13138_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13138_GM};
extern Il2CppType t15_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2490_m27768_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27768_GM;
MethodInfo m27768_MI = 
{
	"Compare", NULL, &t2490_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2490_m27768_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27768_GM};
extern Il2CppType t2490_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13139_GM;
MethodInfo m13139_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2490_TI, &t2490_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13139_GM};
static MethodInfo* t2490_MIs[] =
{
	&m13136_MI,
	&m13137_MI,
	&m13138_MI,
	&m27768_MI,
	&m13139_MI,
	NULL
};
extern MethodInfo m13138_MI;
static MethodInfo* t2490_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27768_MI,
	&m13138_MI,
	NULL,
};
extern TypeInfo t4142_TI;
extern TypeInfo t726_TI;
static TypeInfo* t2490_ITIs[] = 
{
	&t4142_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2490_IOs[] = 
{
	{ &t4142_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2490_TI;
extern TypeInfo t2490_TI;
extern TypeInfo t2491_TI;
extern TypeInfo t15_TI;
static Il2CppRGCTXData t2490_RGCTXData[8] = 
{
	&t4143_0_0_0/* Type Usage */,
	&t15_0_0_0/* Type Usage */,
	&t2490_TI/* Class Usage */,
	&t2490_TI/* Static Usage */,
	&t2491_TI/* Class Usage */,
	&m13140_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
	&m27768_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2490_0_0_0;
extern Il2CppType t2490_1_0_0;
struct t2490;
extern Il2CppGenericClass t2490_GC;
TypeInfo t2490_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2490_MIs, t2490_PIs, t2490_FIs, NULL, &t29_TI, NULL, NULL, &t2490_TI, t2490_ITIs, t2490_VT, &EmptyCustomAttributesCache, &t2490_TI, &t2490_0_0_0, &t2490_1_0_0, t2490_IOs, &t2490_GC, NULL, NULL, NULL, t2490_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2490), 0, -1, sizeof(t2490_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.UI.Text>
extern Il2CppType t15_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t4142_m20861_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20861_GM;
MethodInfo m20861_MI = 
{
	"Compare", NULL, &t4142_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4142_m20861_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20861_GM};
static MethodInfo* t4142_MIs[] =
{
	&m20861_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4142_0_0_0;
extern Il2CppType t4142_1_0_0;
struct t4142;
extern Il2CppGenericClass t4142_GC;
TypeInfo t4142_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4142_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4142_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4142_TI, &t4142_0_0_0, &t4142_1_0_0, NULL, &t4142_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.UI.Text>
extern Il2CppType t15_0_0_0;
static ParameterInfo t4143_m20862_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20862_GM;
MethodInfo m20862_MI = 
{
	"CompareTo", NULL, &t4143_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t4143_m20862_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20862_GM};
static MethodInfo* t4143_MIs[] =
{
	&m20862_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4143_1_0_0;
struct t4143;
extern Il2CppGenericClass t4143_GC;
TypeInfo t4143_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4143_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4143_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4143_TI, &t4143_0_0_0, &t4143_1_0_0, NULL, &t4143_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern TypeInfo t290_TI;
extern MethodInfo m13136_MI;
extern MethodInfo m20862_MI;
extern MethodInfo m9672_MI;


// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UI.Text>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13140_GM;
MethodInfo m13140_MI = 
{
	".ctor", (methodPointerType)&m10749_gshared, &t2491_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13140_GM};
extern Il2CppType t15_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2491_m13141_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13141_GM;
MethodInfo m13141_MI = 
{
	"Compare", (methodPointerType)&m10750_gshared, &t2491_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2491_m13141_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13141_GM};
static MethodInfo* t2491_MIs[] =
{
	&m13140_MI,
	&m13141_MI,
	NULL
};
extern MethodInfo m13141_MI;
static MethodInfo* t2491_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13141_MI,
	&m13138_MI,
	&m13141_MI,
};
static Il2CppInterfaceOffsetPair t2491_IOs[] = 
{
	{ &t4142_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2490_TI;
extern TypeInfo t2490_TI;
extern TypeInfo t2491_TI;
extern TypeInfo t15_TI;
extern TypeInfo t15_TI;
extern TypeInfo t4143_TI;
static Il2CppRGCTXData t2491_RGCTXData[12] = 
{
	&t4143_0_0_0/* Type Usage */,
	&t15_0_0_0/* Type Usage */,
	&t2490_TI/* Class Usage */,
	&t2490_TI/* Static Usage */,
	&t2491_TI/* Class Usage */,
	&m13140_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
	&m27768_MI/* Method Usage */,
	&m13136_MI/* Method Usage */,
	&t15_TI/* Class Usage */,
	&t4143_TI/* Class Usage */,
	&m20862_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2491_0_0_0;
extern Il2CppType t2491_1_0_0;
struct t2491;
extern Il2CppGenericClass t2491_GC;
extern TypeInfo t1246_TI;
TypeInfo t2491_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2491_MIs, NULL, NULL, NULL, &t2490_TI, NULL, &t1246_TI, &t2491_TI, NULL, t2491_VT, &EmptyCustomAttributesCache, &t2491_TI, &t2491_0_0_0, &t2491_1_0_0, t2491_IOs, &t2491_GC, NULL, NULL, NULL, t2491_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2491), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#include "t2479.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2479_TI;
#include "t2479MD.h"



// Metadata Definition System.Comparison`1<UnityEngine.UI.Text>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2479_m13142_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13142_GM;
MethodInfo m13142_MI = 
{
	".ctor", (methodPointerType)&m10796_gshared, &t2479_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2479_m13142_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13142_GM};
extern Il2CppType t15_0_0_0;
extern Il2CppType t15_0_0_0;
static ParameterInfo t2479_m13143_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13143_GM;
MethodInfo m13143_MI = 
{
	"Invoke", (methodPointerType)&m10797_gshared, &t2479_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2479_m13143_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13143_GM};
extern Il2CppType t15_0_0_0;
extern Il2CppType t15_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2479_m13144_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t15_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13144_GM;
MethodInfo m13144_MI = 
{
	"BeginInvoke", (methodPointerType)&m10798_gshared, &t2479_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2479_m13144_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13144_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2479_m13145_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13145_GM;
MethodInfo m13145_MI = 
{
	"EndInvoke", (methodPointerType)&m10799_gshared, &t2479_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2479_m13145_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13145_GM};
static MethodInfo* t2479_MIs[] =
{
	&m13142_MI,
	&m13143_MI,
	&m13144_MI,
	&m13145_MI,
	NULL
};
extern MethodInfo m13143_MI;
extern MethodInfo m13144_MI;
extern MethodInfo m13145_MI;
static MethodInfo* t2479_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13143_MI,
	&m13144_MI,
	&m13145_MI,
};
static Il2CppInterfaceOffsetPair t2479_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2479_0_0_0;
extern Il2CppType t2479_1_0_0;
struct t2479;
extern Il2CppGenericClass t2479_GC;
TypeInfo t2479_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2479_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2479_TI, NULL, t2479_VT, &EmptyCustomAttributesCache, &t2479_TI, &t2479_0_0_0, &t2479_1_0_0, t2479_IOs, &t2479_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2479), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2492.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2492_TI;
#include "t2492MD.h"

#include "t2458.h"
extern TypeInfo t2458_TI;
extern MethodInfo m13150_MI;
extern MethodInfo m20867_MI;
struct t20;
 t2458  m20867 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m13146_MI;
 void m13146 (t2492 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13147_MI;
 t29 * m13147 (t2492 * __this, MethodInfo* method){
	{
		t2458  L_0 = m13150(__this, &m13150_MI);
		t2458  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2458_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13148_MI;
 void m13148 (t2492 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13149_MI;
 bool m13149 (t2492 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t2458  m13150 (t2492 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t2458  L_8 = m20867(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20867_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>>
extern Il2CppType t20_0_0_1;
FieldInfo t2492_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2492_TI, offsetof(t2492, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2492_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2492_TI, offsetof(t2492, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2492_FIs[] =
{
	&t2492_f0_FieldInfo,
	&t2492_f1_FieldInfo,
	NULL
};
static PropertyInfo t2492____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2492_TI, "System.Collections.IEnumerator.Current", &m13147_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2492____Current_PropertyInfo = 
{
	&t2492_TI, "Current", &m13150_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2492_PIs[] =
{
	&t2492____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2492____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2492_m13146_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13146_GM;
MethodInfo m13146_MI = 
{
	".ctor", (methodPointerType)&m13146, &t2492_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2492_m13146_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13146_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13147_GM;
MethodInfo m13147_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13147, &t2492_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13147_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13148_GM;
MethodInfo m13148_MI = 
{
	"Dispose", (methodPointerType)&m13148, &t2492_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13148_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13149_GM;
MethodInfo m13149_MI = 
{
	"MoveNext", (methodPointerType)&m13149, &t2492_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13149_GM};
extern Il2CppType t2458_0_0_0;
extern void* RuntimeInvoker_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13150_GM;
MethodInfo m13150_MI = 
{
	"get_Current", (methodPointerType)&m13150, &t2492_TI, &t2458_0_0_0, RuntimeInvoker_t2458, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13150_GM};
static MethodInfo* t2492_MIs[] =
{
	&m13146_MI,
	&m13147_MI,
	&m13148_MI,
	&m13149_MI,
	&m13150_MI,
	NULL
};
static MethodInfo* t2492_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13147_MI,
	&m13149_MI,
	&m13148_MI,
	&m13150_MI,
};
extern TypeInfo t2459_TI;
static TypeInfo* t2492_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2459_TI,
};
static Il2CppInterfaceOffsetPair t2492_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2459_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2492_0_0_0;
extern Il2CppType t2492_1_0_0;
extern Il2CppGenericClass t2492_GC;
TypeInfo t2492_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2492_MIs, t2492_PIs, t2492_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2492_TI, t2492_ITIs, t2492_VT, &EmptyCustomAttributesCache, &t2492_TI, &t2492_0_0_0, &t2492_1_0_0, t2492_IOs, &t2492_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2492)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5314_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>>
extern MethodInfo m27769_MI;
extern MethodInfo m27770_MI;
static PropertyInfo t5314____Item_PropertyInfo = 
{
	&t5314_TI, "Item", &m27769_MI, &m27770_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5314_PIs[] =
{
	&t5314____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2458_0_0_0;
extern Il2CppType t2458_0_0_0;
static ParameterInfo t5314_m27771_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2458_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27771_GM;
MethodInfo m27771_MI = 
{
	"IndexOf", NULL, &t5314_TI, &t44_0_0_0, RuntimeInvoker_t44_t2458, t5314_m27771_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27771_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2458_0_0_0;
static ParameterInfo t5314_m27772_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2458_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27772_GM;
MethodInfo m27772_MI = 
{
	"Insert", NULL, &t5314_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2458, t5314_m27772_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27772_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5314_m27773_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27773_GM;
MethodInfo m27773_MI = 
{
	"RemoveAt", NULL, &t5314_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5314_m27773_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27773_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5314_m27769_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2458_0_0_0;
extern void* RuntimeInvoker_t2458_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27769_GM;
MethodInfo m27769_MI = 
{
	"get_Item", NULL, &t5314_TI, &t2458_0_0_0, RuntimeInvoker_t2458_t44, t5314_m27769_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27769_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2458_0_0_0;
static ParameterInfo t5314_m27770_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2458_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27770_GM;
MethodInfo m27770_MI = 
{
	"set_Item", NULL, &t5314_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t2458, t5314_m27770_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27770_GM};
static MethodInfo* t5314_MIs[] =
{
	&m27771_MI,
	&m27772_MI,
	&m27773_MI,
	&m27769_MI,
	&m27770_MI,
	NULL
};
extern TypeInfo t5313_TI;
extern TypeInfo t5315_TI;
static TypeInfo* t5314_ITIs[] = 
{
	&t603_TI,
	&t5313_TI,
	&t5315_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5314_0_0_0;
extern Il2CppType t5314_1_0_0;
struct t5314;
extern Il2CppGenericClass t5314_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5314_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5314_MIs, t5314_PIs, NULL, NULL, NULL, NULL, NULL, &t5314_TI, t5314_ITIs, NULL, &t1908__CustomAttributeCache, &t5314_TI, &t5314_0_0_0, &t5314_1_0_0, NULL, &t5314_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6671_TI;

#include "t148.h"


// Metadata Definition System.Collections.Generic.IDictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t148_0_0_0;
extern Il2CppType t148_0_0_0;
static ParameterInfo t6671_m27774_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27774_GM;
MethodInfo m27774_MI = 
{
	"Remove", NULL, &t6671_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6671_m27774_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27774_GM};
static MethodInfo* t6671_MIs[] =
{
	&m27774_MI,
	NULL
};
static TypeInfo* t6671_ITIs[] = 
{
	&t603_TI,
	&t5313_TI,
	&t5315_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6671_0_0_0;
extern Il2CppType t6671_1_0_0;
struct t6671;
extern Il2CppGenericClass t6671_GC;
extern CustomAttributesCache t1975__CustomAttributeCache;
TypeInfo t6671_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IDictionary`2", "System.Collections.Generic", t6671_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6671_TI, t6671_ITIs, NULL, &t1975__CustomAttributeCache, &t6671_TI, &t6671_0_0_0, &t6671_1_0_0, NULL, &t6671_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2454_TI;



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.Font>
extern Il2CppType t148_0_0_0;
extern Il2CppType t148_0_0_0;
static ParameterInfo t2454_m27661_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27661_GM;
MethodInfo m27661_MI = 
{
	"Equals", NULL, &t2454_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2454_m27661_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27661_GM};
extern Il2CppType t148_0_0_0;
static ParameterInfo t2454_m27660_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27660_GM;
MethodInfo m27660_MI = 
{
	"GetHashCode", NULL, &t2454_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2454_m27660_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27660_GM};
static MethodInfo* t2454_MIs[] =
{
	&m27661_MI,
	&m27660_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2454_0_0_0;
extern Il2CppType t2454_1_0_0;
struct t2454;
extern Il2CppGenericClass t2454_GC;
TypeInfo t2454_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t2454_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2454_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2454_TI, &t2454_0_0_0, &t2454_1_0_0, NULL, &t2454_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4146_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Font>
extern MethodInfo m27775_MI;
static PropertyInfo t4146____Current_PropertyInfo = 
{
	&t4146_TI, "Current", &m27775_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4146_PIs[] =
{
	&t4146____Current_PropertyInfo,
	NULL
};
extern Il2CppType t148_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27775_GM;
MethodInfo m27775_MI = 
{
	"get_Current", NULL, &t4146_TI, &t148_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27775_GM};
static MethodInfo* t4146_MIs[] =
{
	&m27775_MI,
	NULL
};
static TypeInfo* t4146_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4146_0_0_0;
extern Il2CppType t4146_1_0_0;
struct t4146;
extern Il2CppGenericClass t4146_GC;
TypeInfo t4146_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4146_MIs, t4146_PIs, NULL, NULL, NULL, NULL, NULL, &t4146_TI, t4146_ITIs, NULL, &EmptyCustomAttributesCache, &t4146_TI, &t4146_0_0_0, &t4146_1_0_0, NULL, &t4146_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2493.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2493_TI;
#include "t2493MD.h"

extern TypeInfo t148_TI;
extern MethodInfo m13155_MI;
extern MethodInfo m20878_MI;
struct t20;
#define m20878(__this, p0, method) (t148 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Font>
extern Il2CppType t20_0_0_1;
FieldInfo t2493_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2493_TI, offsetof(t2493, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2493_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2493_TI, offsetof(t2493, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2493_FIs[] =
{
	&t2493_f0_FieldInfo,
	&t2493_f1_FieldInfo,
	NULL
};
extern MethodInfo m13152_MI;
static PropertyInfo t2493____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2493_TI, "System.Collections.IEnumerator.Current", &m13152_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2493____Current_PropertyInfo = 
{
	&t2493_TI, "Current", &m13155_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2493_PIs[] =
{
	&t2493____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2493____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2493_m13151_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13151_GM;
MethodInfo m13151_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2493_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2493_m13151_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13151_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13152_GM;
MethodInfo m13152_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2493_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13152_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13153_GM;
MethodInfo m13153_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2493_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13153_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13154_GM;
MethodInfo m13154_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2493_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13154_GM};
extern Il2CppType t148_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13155_GM;
MethodInfo m13155_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2493_TI, &t148_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13155_GM};
static MethodInfo* t2493_MIs[] =
{
	&m13151_MI,
	&m13152_MI,
	&m13153_MI,
	&m13154_MI,
	&m13155_MI,
	NULL
};
extern MethodInfo m13154_MI;
extern MethodInfo m13153_MI;
static MethodInfo* t2493_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13152_MI,
	&m13154_MI,
	&m13153_MI,
	&m13155_MI,
};
static TypeInfo* t2493_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4146_TI,
};
static Il2CppInterfaceOffsetPair t2493_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4146_TI, 7},
};
extern TypeInfo t148_TI;
static Il2CppRGCTXData t2493_RGCTXData[3] = 
{
	&m13155_MI/* Method Usage */,
	&t148_TI/* Class Usage */,
	&m20878_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2493_0_0_0;
extern Il2CppType t2493_1_0_0;
extern Il2CppGenericClass t2493_GC;
TypeInfo t2493_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2493_MIs, t2493_PIs, t2493_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2493_TI, t2493_ITIs, t2493_VT, &EmptyCustomAttributesCache, &t2493_TI, &t2493_0_0_0, &t2493_1_0_0, t2493_IOs, &t2493_GC, NULL, NULL, NULL, t2493_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2493)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5316_TI;

#include "UnityEngine_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Font>
extern MethodInfo m27776_MI;
static PropertyInfo t5316____Count_PropertyInfo = 
{
	&t5316_TI, "Count", &m27776_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27777_MI;
static PropertyInfo t5316____IsReadOnly_PropertyInfo = 
{
	&t5316_TI, "IsReadOnly", &m27777_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5316_PIs[] =
{
	&t5316____Count_PropertyInfo,
	&t5316____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27776_GM;
MethodInfo m27776_MI = 
{
	"get_Count", NULL, &t5316_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27776_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27777_GM;
MethodInfo m27777_MI = 
{
	"get_IsReadOnly", NULL, &t5316_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27777_GM};
extern Il2CppType t148_0_0_0;
static ParameterInfo t5316_m27778_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27778_GM;
MethodInfo m27778_MI = 
{
	"Add", NULL, &t5316_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5316_m27778_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27778_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27779_GM;
MethodInfo m27779_MI = 
{
	"Clear", NULL, &t5316_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27779_GM};
extern Il2CppType t148_0_0_0;
static ParameterInfo t5316_m27780_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27780_GM;
MethodInfo m27780_MI = 
{
	"Contains", NULL, &t5316_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5316_m27780_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27780_GM};
extern Il2CppType t2452_0_0_0;
extern Il2CppType t2452_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5316_m27781_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2452_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27781_GM;
MethodInfo m27781_MI = 
{
	"CopyTo", NULL, &t5316_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5316_m27781_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27781_GM};
extern Il2CppType t148_0_0_0;
static ParameterInfo t5316_m27782_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27782_GM;
MethodInfo m27782_MI = 
{
	"Remove", NULL, &t5316_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5316_m27782_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27782_GM};
static MethodInfo* t5316_MIs[] =
{
	&m27776_MI,
	&m27777_MI,
	&m27778_MI,
	&m27779_MI,
	&m27780_MI,
	&m27781_MI,
	&m27782_MI,
	NULL
};
extern TypeInfo t5318_TI;
static TypeInfo* t5316_ITIs[] = 
{
	&t603_TI,
	&t5318_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5316_0_0_0;
extern Il2CppType t5316_1_0_0;
struct t5316;
extern Il2CppGenericClass t5316_GC;
TypeInfo t5316_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5316_MIs, t5316_PIs, NULL, NULL, NULL, NULL, NULL, &t5316_TI, t5316_ITIs, NULL, &EmptyCustomAttributesCache, &t5316_TI, &t5316_0_0_0, &t5316_1_0_0, NULL, &t5316_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Font>
extern Il2CppType t4146_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27783_GM;
MethodInfo m27783_MI = 
{
	"GetEnumerator", NULL, &t5318_TI, &t4146_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27783_GM};
static MethodInfo* t5318_MIs[] =
{
	&m27783_MI,
	NULL
};
static TypeInfo* t5318_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5318_0_0_0;
extern Il2CppType t5318_1_0_0;
struct t5318;
extern Il2CppGenericClass t5318_GC;
TypeInfo t5318_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5318_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5318_TI, t5318_ITIs, NULL, &EmptyCustomAttributesCache, &t5318_TI, &t5318_0_0_0, &t5318_1_0_0, NULL, &t5318_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5317_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Font>
extern MethodInfo m27784_MI;
extern MethodInfo m27785_MI;
static PropertyInfo t5317____Item_PropertyInfo = 
{
	&t5317_TI, "Item", &m27784_MI, &m27785_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5317_PIs[] =
{
	&t5317____Item_PropertyInfo,
	NULL
};
extern Il2CppType t148_0_0_0;
static ParameterInfo t5317_m27786_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27786_GM;
MethodInfo m27786_MI = 
{
	"IndexOf", NULL, &t5317_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5317_m27786_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27786_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t148_0_0_0;
static ParameterInfo t5317_m27787_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27787_GM;
MethodInfo m27787_MI = 
{
	"Insert", NULL, &t5317_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5317_m27787_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27787_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5317_m27788_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27788_GM;
MethodInfo m27788_MI = 
{
	"RemoveAt", NULL, &t5317_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5317_m27788_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27788_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5317_m27784_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t148_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27784_GM;
MethodInfo m27784_MI = 
{
	"get_Item", NULL, &t5317_TI, &t148_0_0_0, RuntimeInvoker_t29_t44, t5317_m27784_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27784_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t148_0_0_0;
static ParameterInfo t5317_m27785_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27785_GM;
MethodInfo m27785_MI = 
{
	"set_Item", NULL, &t5317_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5317_m27785_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27785_GM};
static MethodInfo* t5317_MIs[] =
{
	&m27786_MI,
	&m27787_MI,
	&m27788_MI,
	&m27784_MI,
	&m27785_MI,
	NULL
};
static TypeInfo* t5317_ITIs[] = 
{
	&t603_TI,
	&t5316_TI,
	&t5318_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5317_0_0_0;
extern Il2CppType t5317_1_0_0;
struct t5317;
extern Il2CppGenericClass t5317_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5317_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5317_MIs, t5317_PIs, NULL, NULL, NULL, NULL, NULL, &t5317_TI, t5317_ITIs, NULL, &t1908__CustomAttributeCache, &t5317_TI, &t5317_0_0_0, &t5317_1_0_0, NULL, &t5317_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2495_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern MethodInfo m27789_MI;
static PropertyInfo t2495____Current_PropertyInfo = 
{
	&t2495_TI, "Current", &m27789_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2495_PIs[] =
{
	&t2495____Current_PropertyInfo,
	NULL
};
extern Il2CppType t350_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27789_GM;
MethodInfo m27789_MI = 
{
	"get_Current", NULL, &t2495_TI, &t350_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27789_GM};
static MethodInfo* t2495_MIs[] =
{
	&m27789_MI,
	NULL
};
static TypeInfo* t2495_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2495_0_0_0;
extern Il2CppType t2495_1_0_0;
struct t2495;
extern Il2CppGenericClass t2495_GC;
TypeInfo t2495_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2495_MIs, t2495_PIs, NULL, NULL, NULL, NULL, NULL, &t2495_TI, t2495_ITIs, NULL, &EmptyCustomAttributesCache, &t2495_TI, &t2495_0_0_0, &t2495_1_0_0, NULL, &t2495_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2494.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2494_TI;
#include "t2494MD.h"

extern MethodInfo m13160_MI;
extern MethodInfo m20889_MI;
struct t20;
#define m20889(__this, p0, method) (t350 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t20_0_0_1;
FieldInfo t2494_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2494_TI, offsetof(t2494, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2494_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2494_TI, offsetof(t2494, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2494_FIs[] =
{
	&t2494_f0_FieldInfo,
	&t2494_f1_FieldInfo,
	NULL
};
extern MethodInfo m13157_MI;
static PropertyInfo t2494____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2494_TI, "System.Collections.IEnumerator.Current", &m13157_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2494____Current_PropertyInfo = 
{
	&t2494_TI, "Current", &m13160_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2494_PIs[] =
{
	&t2494____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2494____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2494_m13156_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13156_GM;
MethodInfo m13156_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2494_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2494_m13156_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13156_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13157_GM;
MethodInfo m13157_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2494_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13157_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13158_GM;
MethodInfo m13158_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2494_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13158_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13159_GM;
MethodInfo m13159_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2494_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13159_GM};
extern Il2CppType t350_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13160_GM;
MethodInfo m13160_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2494_TI, &t350_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13160_GM};
static MethodInfo* t2494_MIs[] =
{
	&m13156_MI,
	&m13157_MI,
	&m13158_MI,
	&m13159_MI,
	&m13160_MI,
	NULL
};
extern MethodInfo m13159_MI;
extern MethodInfo m13158_MI;
static MethodInfo* t2494_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13157_MI,
	&m13159_MI,
	&m13158_MI,
	&m13160_MI,
};
static TypeInfo* t2494_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2495_TI,
};
static Il2CppInterfaceOffsetPair t2494_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2495_TI, 7},
};
extern TypeInfo t350_TI;
static Il2CppRGCTXData t2494_RGCTXData[3] = 
{
	&m13160_MI/* Method Usage */,
	&t350_TI/* Class Usage */,
	&m20889_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2494_0_0_0;
extern Il2CppType t2494_1_0_0;
extern Il2CppGenericClass t2494_GC;
TypeInfo t2494_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2494_MIs, t2494_PIs, t2494_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2494_TI, t2494_ITIs, t2494_VT, &EmptyCustomAttributesCache, &t2494_TI, &t2494_0_0_0, &t2494_1_0_0, t2494_IOs, &t2494_GC, NULL, NULL, NULL, t2494_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2494)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5319_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern MethodInfo m27790_MI;
static PropertyInfo t5319____Count_PropertyInfo = 
{
	&t5319_TI, "Count", &m27790_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27791_MI;
static PropertyInfo t5319____IsReadOnly_PropertyInfo = 
{
	&t5319_TI, "IsReadOnly", &m27791_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5319_PIs[] =
{
	&t5319____Count_PropertyInfo,
	&t5319____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27790_GM;
MethodInfo m27790_MI = 
{
	"get_Count", NULL, &t5319_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27790_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27791_GM;
MethodInfo m27791_MI = 
{
	"get_IsReadOnly", NULL, &t5319_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27791_GM};
extern Il2CppType t350_0_0_0;
static ParameterInfo t5319_m27792_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27792_GM;
MethodInfo m27792_MI = 
{
	"Add", NULL, &t5319_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5319_m27792_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27792_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27793_GM;
MethodInfo m27793_MI = 
{
	"Clear", NULL, &t5319_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27793_GM};
extern Il2CppType t350_0_0_0;
static ParameterInfo t5319_m27794_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27794_GM;
MethodInfo m27794_MI = 
{
	"Contains", NULL, &t5319_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5319_m27794_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27794_GM};
extern Il2CppType t2453_0_0_0;
extern Il2CppType t2453_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5319_m27795_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2453_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27795_GM;
MethodInfo m27795_MI = 
{
	"CopyTo", NULL, &t5319_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5319_m27795_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27795_GM};
extern Il2CppType t350_0_0_0;
static ParameterInfo t5319_m27796_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27796_GM;
MethodInfo m27796_MI = 
{
	"Remove", NULL, &t5319_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5319_m27796_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27796_GM};
static MethodInfo* t5319_MIs[] =
{
	&m27790_MI,
	&m27791_MI,
	&m27792_MI,
	&m27793_MI,
	&m27794_MI,
	&m27795_MI,
	&m27796_MI,
	NULL
};
extern TypeInfo t5321_TI;
static TypeInfo* t5319_ITIs[] = 
{
	&t603_TI,
	&t5321_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5319_0_0_0;
extern Il2CppType t5319_1_0_0;
struct t5319;
extern Il2CppGenericClass t5319_GC;
TypeInfo t5319_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5319_MIs, t5319_PIs, NULL, NULL, NULL, NULL, NULL, &t5319_TI, t5319_ITIs, NULL, &EmptyCustomAttributesCache, &t5319_TI, &t5319_0_0_0, &t5319_1_0_0, NULL, &t5319_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t2495_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27797_GM;
MethodInfo m27797_MI = 
{
	"GetEnumerator", NULL, &t5321_TI, &t2495_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27797_GM};
static MethodInfo* t5321_MIs[] =
{
	&m27797_MI,
	NULL
};
static TypeInfo* t5321_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5321_0_0_0;
extern Il2CppType t5321_1_0_0;
struct t5321;
extern Il2CppGenericClass t5321_GC;
TypeInfo t5321_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5321_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5321_TI, t5321_ITIs, NULL, &EmptyCustomAttributesCache, &t5321_TI, &t5321_0_0_0, &t5321_1_0_0, NULL, &t5321_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5320_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern MethodInfo m27798_MI;
extern MethodInfo m27799_MI;
static PropertyInfo t5320____Item_PropertyInfo = 
{
	&t5320_TI, "Item", &m27798_MI, &m27799_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5320_PIs[] =
{
	&t5320____Item_PropertyInfo,
	NULL
};
extern Il2CppType t350_0_0_0;
static ParameterInfo t5320_m27800_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27800_GM;
MethodInfo m27800_MI = 
{
	"IndexOf", NULL, &t5320_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5320_m27800_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27800_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t5320_m27801_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27801_GM;
MethodInfo m27801_MI = 
{
	"Insert", NULL, &t5320_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5320_m27801_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27801_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5320_m27802_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27802_GM;
MethodInfo m27802_MI = 
{
	"RemoveAt", NULL, &t5320_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5320_m27802_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27802_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5320_m27798_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t350_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27798_GM;
MethodInfo m27798_MI = 
{
	"get_Item", NULL, &t5320_TI, &t350_0_0_0, RuntimeInvoker_t29_t44, t5320_m27798_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27798_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t5320_m27799_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27799_GM;
MethodInfo m27799_MI = 
{
	"set_Item", NULL, &t5320_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5320_m27799_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27799_GM};
static MethodInfo* t5320_MIs[] =
{
	&m27800_MI,
	&m27801_MI,
	&m27802_MI,
	&m27798_MI,
	&m27799_MI,
	NULL
};
static TypeInfo* t5320_ITIs[] = 
{
	&t603_TI,
	&t5319_TI,
	&t5321_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5320_0_0_0;
extern Il2CppType t5320_1_0_0;
struct t5320;
extern Il2CppGenericClass t5320_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5320_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5320_MIs, t5320_PIs, NULL, NULL, NULL, NULL, NULL, &t5320_TI, t5320_ITIs, NULL, &t1908__CustomAttributeCache, &t5320_TI, &t5320_0_0_0, &t5320_1_0_0, NULL, &t5320_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2456.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2456_TI;
#include "t2456MD.h"

#include "t154.h"
#include "t2496.h"
#include "t2497.h"
extern TypeInfo t154_TI;
extern TypeInfo t2496_TI;
extern TypeInfo t2453_TI;
extern TypeInfo t2497_TI;
#include "t154MD.h"
#include "t2497MD.h"
#include "t2496MD.h"
extern MethodInfo m12868_MI;
extern MethodInfo m3988_MI;
extern MethodInfo m12892_MI;
extern MethodInfo m13173_MI;
extern MethodInfo m13172_MI;
extern MethodInfo m12878_MI;
extern MethodInfo m12881_MI;
extern MethodInfo m13192_MI;
extern MethodInfo m20900_MI;
extern MethodInfo m20901_MI;
extern MethodInfo m13175_MI;
struct t154;
#include "t295.h"
struct t2461;
#include "t2461.h"
#include "t2470.h"
 void m20791_gshared (t2461 * __this, t20 * p0, int32_t p1, t2470 * p2, MethodInfo* method);
#define m20791(__this, p0, p1, p2, method) (void)m20791_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, (t2470 *)p2, method)
#define m20900(__this, p0, p1, p2, method) (void)m20791_gshared((t2461 *)__this, (t20 *)p0, (int32_t)p1, (t2470 *)p2, method)
struct t154;
#include "t1248.h"
 void m20901 (t154 * __this, t2453* p0, int32_t p1, t2497 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


 t2496  m13173 (t2456 * __this, MethodInfo* method){
	{
		t154 * L_0 = (__this->f0);
		t2496  L_1 = {0};
		m13175(&L_1, L_0, &m13175_MI);
		return L_1;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t154_0_0_1;
FieldInfo t2456_f0_FieldInfo = 
{
	"dictionary", &t154_0_0_1, &t2456_TI, offsetof(t2456, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2456_FIs[] =
{
	&t2456_f0_FieldInfo,
	NULL
};
extern MethodInfo m13169_MI;
static PropertyInfo t2456____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo = 
{
	&t2456_TI, "System.Collections.Generic.ICollection<TValue>.IsReadOnly", &m13169_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13170_MI;
static PropertyInfo t2456____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2456_TI, "System.Collections.ICollection.IsSynchronized", &m13170_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13171_MI;
static PropertyInfo t2456____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2456_TI, "System.Collections.ICollection.SyncRoot", &m13171_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13174_MI;
static PropertyInfo t2456____Count_PropertyInfo = 
{
	&t2456_TI, "Count", &m13174_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2456_PIs[] =
{
	&t2456____System_Collections_Generic_ICollectionU3CTValueU3E_IsReadOnly_PropertyInfo,
	&t2456____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2456____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2456____Count_PropertyInfo,
	NULL
};
extern Il2CppType t154_0_0_0;
extern Il2CppType t154_0_0_0;
static ParameterInfo t2456_m13161_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t154_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13161_GM;
MethodInfo m13161_MI = 
{
	".ctor", (methodPointerType)&m12928_gshared, &t2456_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2456_m13161_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13161_GM};
extern Il2CppType t350_0_0_0;
static ParameterInfo t2456_m13162_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13162_GM;
MethodInfo m13162_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Add", (methodPointerType)&m12929_gshared, &t2456_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2456_m13162_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13162_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13163_GM;
MethodInfo m13163_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Clear", (methodPointerType)&m12930_gshared, &t2456_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 12, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13163_GM};
extern Il2CppType t350_0_0_0;
static ParameterInfo t2456_m13164_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13164_GM;
MethodInfo m13164_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Contains", (methodPointerType)&m12931_gshared, &t2456_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2456_m13164_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13164_GM};
extern Il2CppType t350_0_0_0;
static ParameterInfo t2456_m13165_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13165_GM;
MethodInfo m13165_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.Remove", (methodPointerType)&m12932_gshared, &t2456_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2456_m13165_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13165_GM};
extern Il2CppType t2495_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13166_GM;
MethodInfo m13166_MI = 
{
	"System.Collections.Generic.IEnumerable<TValue>.GetEnumerator", (methodPointerType)&m12933_gshared, &t2456_TI, &t2495_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 16, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13166_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2456_m13167_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13167_GM;
MethodInfo m13167_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m12934_gshared, &t2456_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2456_m13167_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13167_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13168_GM;
MethodInfo m13168_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m12935_gshared, &t2456_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13168_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13169_GM;
MethodInfo m13169_MI = 
{
	"System.Collections.Generic.ICollection<TValue>.get_IsReadOnly", (methodPointerType)&m12936_gshared, &t2456_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13169_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13170_GM;
MethodInfo m13170_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m12937_gshared, &t2456_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13170_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13171_GM;
MethodInfo m13171_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m12938_gshared, &t2456_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13171_GM};
extern Il2CppType t2453_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2456_m13172_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2453_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13172_GM;
MethodInfo m13172_MI = 
{
	"CopyTo", (methodPointerType)&m12939_gshared, &t2456_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2456_m13172_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 14, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13172_GM};
extern Il2CppType t2496_0_0_0;
extern void* RuntimeInvoker_t2496 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13173_GM;
MethodInfo m13173_MI = 
{
	"GetEnumerator", (methodPointerType)&m13173, &t2456_TI, &t2496_0_0_0, RuntimeInvoker_t2496, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13173_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13174_GM;
MethodInfo m13174_MI = 
{
	"get_Count", (methodPointerType)&m12941_gshared, &t2456_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13174_GM};
static MethodInfo* t2456_MIs[] =
{
	&m13161_MI,
	&m13162_MI,
	&m13163_MI,
	&m13164_MI,
	&m13165_MI,
	&m13166_MI,
	&m13167_MI,
	&m13168_MI,
	&m13169_MI,
	&m13170_MI,
	&m13171_MI,
	&m13172_MI,
	&m13173_MI,
	&m13174_MI,
	NULL
};
extern MethodInfo m13168_MI;
extern MethodInfo m13167_MI;
extern MethodInfo m13162_MI;
extern MethodInfo m13163_MI;
extern MethodInfo m13164_MI;
extern MethodInfo m13165_MI;
extern MethodInfo m13166_MI;
static MethodInfo* t2456_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13168_MI,
	&m13174_MI,
	&m13170_MI,
	&m13171_MI,
	&m13167_MI,
	&m13174_MI,
	&m13169_MI,
	&m13162_MI,
	&m13163_MI,
	&m13164_MI,
	&m13172_MI,
	&m13165_MI,
	&m13166_MI,
};
static TypeInfo* t2456_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t5319_TI,
	&t5321_TI,
};
static Il2CppInterfaceOffsetPair t2456_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t5319_TI, 9},
	{ &t5321_TI, 16},
};
extern TypeInfo t2496_TI;
extern TypeInfo t2453_TI;
extern TypeInfo t2497_TI;
static Il2CppRGCTXData t2456_RGCTXData[13] = 
{
	&m12892_MI/* Method Usage */,
	&m13173_MI/* Method Usage */,
	&t2496_TI/* Class Usage */,
	&t2453_TI/* Class Usage */,
	&m13172_MI/* Method Usage */,
	&m12878_MI/* Method Usage */,
	&m12881_MI/* Method Usage */,
	&t2497_TI/* Class Usage */,
	&m13192_MI/* Method Usage */,
	&m20900_MI/* Method Usage */,
	&m20901_MI/* Method Usage */,
	&m13175_MI/* Method Usage */,
	&m12868_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2456_0_0_0;
extern Il2CppType t2456_1_0_0;
struct t2456;
extern Il2CppGenericClass t2456_GC;
extern TypeInfo t1254_TI;
extern CustomAttributesCache t1252__CustomAttributeCache;
TypeInfo t2456_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ValueCollection", "", t2456_MIs, t2456_PIs, t2456_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2456_TI, t2456_ITIs, t2456_VT, &t1252__CustomAttributeCache, &t2456_TI, &t2456_0_0_0, &t2456_1_0_0, t2456_IOs, &t2456_GC, NULL, NULL, NULL, t2456_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2456), 0, -1, 0, 0, -1, 1057026, 0, false, false, false, false, true, false, false, false, false, false, false, false, 14, 4, 1, 0, 0, 17, 4, 4};
#ifndef _MSC_VER
#else
#endif

#include "t2460.h"
extern TypeInfo t2460_TI;
#include "t2460MD.h"
#include "t2458MD.h"
extern MethodInfo m13188_MI;
extern MethodInfo m12980_MI;
extern MethodInfo m12908_MI;
extern MethodInfo m13191_MI;
extern MethodInfo m13185_MI;


// Metadata Definition System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t2460_0_0_1;
FieldInfo t2496_f0_FieldInfo = 
{
	"host_enumerator", &t2460_0_0_1, &t2496_TI, offsetof(t2496, f0) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2496_FIs[] =
{
	&t2496_f0_FieldInfo,
	NULL
};
extern MethodInfo m13176_MI;
static PropertyInfo t2496____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2496_TI, "System.Collections.IEnumerator.Current", &m13176_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13179_MI;
static PropertyInfo t2496____Current_PropertyInfo = 
{
	&t2496_TI, "Current", &m13179_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2496_PIs[] =
{
	&t2496____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2496____Current_PropertyInfo,
	NULL
};
extern Il2CppType t154_0_0_0;
static ParameterInfo t2496_m13175_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t154_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13175_GM;
MethodInfo m13175_MI = 
{
	".ctor", (methodPointerType)&m12942_gshared, &t2496_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2496_m13175_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13175_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13176_GM;
MethodInfo m13176_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12943_gshared, &t2496_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13176_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13177_GM;
MethodInfo m13177_MI = 
{
	"Dispose", (methodPointerType)&m12944_gshared, &t2496_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13177_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13178_GM;
MethodInfo m13178_MI = 
{
	"MoveNext", (methodPointerType)&m12945_gshared, &t2496_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13178_GM};
extern Il2CppType t350_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13179_GM;
MethodInfo m13179_MI = 
{
	"get_Current", (methodPointerType)&m12946_gshared, &t2496_TI, &t350_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13179_GM};
static MethodInfo* t2496_MIs[] =
{
	&m13175_MI,
	&m13176_MI,
	&m13177_MI,
	&m13178_MI,
	&m13179_MI,
	NULL
};
extern MethodInfo m13178_MI;
extern MethodInfo m13177_MI;
static MethodInfo* t2496_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13176_MI,
	&m13178_MI,
	&m13177_MI,
	&m13179_MI,
};
static TypeInfo* t2496_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2495_TI,
};
static Il2CppInterfaceOffsetPair t2496_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2495_TI, 7},
};
extern TypeInfo t350_TI;
static Il2CppRGCTXData t2496_RGCTXData[6] = 
{
	&m12908_MI/* Method Usage */,
	&m13188_MI/* Method Usage */,
	&t350_TI/* Class Usage */,
	&m13191_MI/* Method Usage */,
	&m13185_MI/* Method Usage */,
	&m12980_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2496_0_0_0;
extern Il2CppType t2496_1_0_0;
extern Il2CppGenericClass t2496_GC;
extern TypeInfo t1252_TI;
TypeInfo t2496_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2496_MIs, t2496_PIs, t2496_FIs, NULL, &t110_TI, NULL, &t1252_TI, &t2496_TI, t2496_ITIs, t2496_VT, &EmptyCustomAttributesCache, &t2496_TI, &t2496_0_0_0, &t2496_1_0_0, t2496_IOs, &t2496_GC, NULL, NULL, NULL, t2496_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2496)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 1, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t725.h"
extern TypeInfo t725_TI;
extern TypeInfo t1248_TI;
#include "t725MD.h"
extern MethodInfo m13190_MI;
extern MethodInfo m12978_MI;
extern MethodInfo m3965_MI;
extern MethodInfo m13187_MI;
extern MethodInfo m13189_MI;
extern MethodInfo m12977_MI;


extern MethodInfo m13186_MI;
 t2458  m13186 (t2460 * __this, MethodInfo* method){
	{
		t2458  L_0 = (__this->f3);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t154_0_0_1;
FieldInfo t2460_f0_FieldInfo = 
{
	"dictionary", &t154_0_0_1, &t2460_TI, offsetof(t2460, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2460_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2460_TI, offsetof(t2460, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2460_f2_FieldInfo = 
{
	"stamp", &t44_0_0_1, &t2460_TI, offsetof(t2460, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t2458_0_0_3;
FieldInfo t2460_f3_FieldInfo = 
{
	"current", &t2458_0_0_3, &t2460_TI, offsetof(t2460, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2460_FIs[] =
{
	&t2460_f0_FieldInfo,
	&t2460_f1_FieldInfo,
	&t2460_f2_FieldInfo,
	&t2460_f3_FieldInfo,
	NULL
};
extern MethodInfo m13181_MI;
static PropertyInfo t2460____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2460_TI, "System.Collections.IEnumerator.Current", &m13181_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13182_MI;
static PropertyInfo t2460____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo = 
{
	&t2460_TI, "System.Collections.IDictionaryEnumerator.Entry", &m13182_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13183_MI;
static PropertyInfo t2460____System_Collections_IDictionaryEnumerator_Key_PropertyInfo = 
{
	&t2460_TI, "System.Collections.IDictionaryEnumerator.Key", &m13183_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13184_MI;
static PropertyInfo t2460____System_Collections_IDictionaryEnumerator_Value_PropertyInfo = 
{
	&t2460_TI, "System.Collections.IDictionaryEnumerator.Value", &m13184_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2460____Current_PropertyInfo = 
{
	&t2460_TI, "Current", &m13186_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2460____CurrentKey_PropertyInfo = 
{
	&t2460_TI, "CurrentKey", &m13187_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2460____CurrentValue_PropertyInfo = 
{
	&t2460_TI, "CurrentValue", &m13188_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2460_PIs[] =
{
	&t2460____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2460____System_Collections_IDictionaryEnumerator_Entry_PropertyInfo,
	&t2460____System_Collections_IDictionaryEnumerator_Key_PropertyInfo,
	&t2460____System_Collections_IDictionaryEnumerator_Value_PropertyInfo,
	&t2460____Current_PropertyInfo,
	&t2460____CurrentKey_PropertyInfo,
	&t2460____CurrentValue_PropertyInfo,
	NULL
};
extern Il2CppType t154_0_0_0;
static ParameterInfo t2460_m13180_ParameterInfos[] = 
{
	{"dictionary", 0, 134217728, &EmptyCustomAttributesCache, &t154_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13180_GM;
MethodInfo m13180_MI = 
{
	".ctor", (methodPointerType)&m12947_gshared, &t2460_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2460_m13180_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13180_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13181_GM;
MethodInfo m13181_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m12948_gshared, &t2460_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13181_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13182_GM;
MethodInfo m13182_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Entry", (methodPointerType)&m12949_gshared, &t2460_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2529, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13182_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13183_GM;
MethodInfo m13183_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Key", (methodPointerType)&m12950_gshared, &t2460_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13183_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13184_GM;
MethodInfo m13184_MI = 
{
	"System.Collections.IDictionaryEnumerator.get_Value", (methodPointerType)&m12951_gshared, &t2460_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13184_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13185_GM;
MethodInfo m13185_MI = 
{
	"MoveNext", (methodPointerType)&m12952_gshared, &t2460_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13185_GM};
extern Il2CppType t2458_0_0_0;
extern void* RuntimeInvoker_t2458 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13186_GM;
MethodInfo m13186_MI = 
{
	"get_Current", (methodPointerType)&m13186, &t2460_TI, &t2458_0_0_0, RuntimeInvoker_t2458, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13186_GM};
extern Il2CppType t148_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13187_GM;
MethodInfo m13187_MI = 
{
	"get_CurrentKey", (methodPointerType)&m12954_gshared, &t2460_TI, &t148_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13187_GM};
extern Il2CppType t350_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13188_GM;
MethodInfo m13188_MI = 
{
	"get_CurrentValue", (methodPointerType)&m12955_gshared, &t2460_TI, &t350_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2179, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13188_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13189_GM;
MethodInfo m13189_MI = 
{
	"VerifyState", (methodPointerType)&m12956_gshared, &t2460_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13189_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13190_GM;
MethodInfo m13190_MI = 
{
	"VerifyCurrent", (methodPointerType)&m12957_gshared, &t2460_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13190_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13191_GM;
MethodInfo m13191_MI = 
{
	"Dispose", (methodPointerType)&m12958_gshared, &t2460_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13191_GM};
static MethodInfo* t2460_MIs[] =
{
	&m13180_MI,
	&m13181_MI,
	&m13182_MI,
	&m13183_MI,
	&m13184_MI,
	&m13185_MI,
	&m13186_MI,
	&m13187_MI,
	&m13188_MI,
	&m13189_MI,
	&m13190_MI,
	&m13191_MI,
	NULL
};
static MethodInfo* t2460_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13181_MI,
	&m13185_MI,
	&m13191_MI,
	&m13186_MI,
	&m13182_MI,
	&m13183_MI,
	&m13184_MI,
};
extern TypeInfo t722_TI;
static TypeInfo* t2460_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2459_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2460_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2459_TI, 7},
	{ &t722_TI, 8},
};
extern TypeInfo t2458_TI;
extern TypeInfo t148_TI;
extern TypeInfo t350_TI;
static Il2CppRGCTXData t2460_RGCTXData[10] = 
{
	&m13190_MI/* Method Usage */,
	&t2458_TI/* Class Usage */,
	&m12978_MI/* Method Usage */,
	&t148_TI/* Class Usage */,
	&m12980_MI/* Method Usage */,
	&t350_TI/* Class Usage */,
	&m13187_MI/* Method Usage */,
	&m13188_MI/* Method Usage */,
	&m13189_MI/* Method Usage */,
	&m12977_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2460_0_0_0;
extern Il2CppType t2460_1_0_0;
extern Il2CppGenericClass t2460_GC;
TypeInfo t2460_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2460_MIs, t2460_PIs, t2460_FIs, NULL, &t110_TI, NULL, &t1254_TI, &t2460_TI, t2460_ITIs, t2460_VT, &EmptyCustomAttributesCache, &t2460_TI, &t2460_0_0_0, &t2460_1_0_0, t2460_IOs, &t2460_GC, NULL, NULL, NULL, t2460_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2460)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 12, 7, 4, 0, 0, 11, 4, 4};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2497_m13192_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13192_GM;
MethodInfo m13192_MI = 
{
	".ctor", (methodPointerType)&m12959_gshared, &t2497_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2497_m13192_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13192_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t2497_m13193_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t350_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13193_GM;
MethodInfo m13193_MI = 
{
	"Invoke", (methodPointerType)&m12960_gshared, &t2497_TI, &t350_0_0_0, RuntimeInvoker_t29_t29_t29, t2497_m13193_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13193_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2497_m13194_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13194_GM;
MethodInfo m13194_MI = 
{
	"BeginInvoke", (methodPointerType)&m12961_gshared, &t2497_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2497_m13194_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13194_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2497_m13195_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t350_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13195_GM;
MethodInfo m13195_MI = 
{
	"EndInvoke", (methodPointerType)&m12962_gshared, &t2497_TI, &t350_0_0_0, RuntimeInvoker_t29_t29, t2497_m13195_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13195_GM};
static MethodInfo* t2497_MIs[] =
{
	&m13192_MI,
	&m13193_MI,
	&m13194_MI,
	&m13195_MI,
	NULL
};
extern MethodInfo m13193_MI;
extern MethodInfo m13194_MI;
extern MethodInfo m13195_MI;
static MethodInfo* t2497_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13193_MI,
	&m13194_MI,
	&m13195_MI,
};
static Il2CppInterfaceOffsetPair t2497_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2497_0_0_0;
extern Il2CppType t2497_1_0_0;
struct t2497;
extern Il2CppGenericClass t2497_GC;
TypeInfo t2497_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2497_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2497_TI, NULL, t2497_VT, &EmptyCustomAttributesCache, &t2497_TI, &t2497_0_0_0, &t2497_1_0_0, t2497_IOs, &t2497_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2497), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2455.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2455_TI;
#include "t2455MD.h"



extern MethodInfo m13196_MI;
 void m13196 (t2455 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m13197_MI;
 t725  m13197 (t2455 * __this, t148 * p0, t350 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m13197((t2455 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 *, t29 * __this, t148 * p0, t350 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t148 * p0, t350 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t725  (*FunctionPointerType) (t29 * __this, t350 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m13198_MI;
 t29 * m13198 (t2455 * __this, t148 * p0, t350 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m13199_MI;
 t725  m13199 (t2455 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t725 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,System.Collections.DictionaryEntry>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2455_m13196_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13196_GM;
MethodInfo m13196_MI = 
{
	".ctor", (methodPointerType)&m13196, &t2455_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2455_m13196_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13196_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t2455_m13197_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13197_GM;
MethodInfo m13197_MI = 
{
	"Invoke", (methodPointerType)&m13197, &t2455_TI, &t725_0_0_0, RuntimeInvoker_t725_t29_t29, t2455_m13197_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13197_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2455_m13198_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13198_GM;
MethodInfo m13198_MI = 
{
	"BeginInvoke", (methodPointerType)&m13198, &t2455_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2455_m13198_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13198_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2455_m13199_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13199_GM;
MethodInfo m13199_MI = 
{
	"EndInvoke", (methodPointerType)&m13199, &t2455_TI, &t725_0_0_0, RuntimeInvoker_t725_t29, t2455_m13199_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13199_GM};
static MethodInfo* t2455_MIs[] =
{
	&m13196_MI,
	&m13197_MI,
	&m13198_MI,
	&m13199_MI,
	NULL
};
static MethodInfo* t2455_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13197_MI,
	&m13198_MI,
	&m13199_MI,
};
static Il2CppInterfaceOffsetPair t2455_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2455_0_0_0;
extern Il2CppType t2455_1_0_0;
struct t2455;
extern Il2CppGenericClass t2455_GC;
TypeInfo t2455_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2455_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2455_TI, NULL, t2455_VT, &EmptyCustomAttributesCache, &t2455_TI, &t2455_0_0_0, &t2455_1_0_0, t2455_IOs, &t2455_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2455), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2498.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2498_TI;
#include "t2498MD.h"



extern MethodInfo m13200_MI;
 void m13200 (t2498 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m13201_MI;
 t2458  m13201 (t2498 * __this, t148 * p0, t350 * p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m13201((t2498 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef t2458  (*FunctionPointerType) (t29 *, t29 * __this, t148 * p0, t350 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef t2458  (*FunctionPointerType) (t29 * __this, t148 * p0, t350 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef t2458  (*FunctionPointerType) (t29 * __this, t350 * p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(p0, p1,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m13202_MI;
 t29 * m13202 (t2498 * __this, t148 * p0, t350 * p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = p0;
	__d_args[1] = p1;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m13203_MI;
 t2458  m13203 (t2498 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(t2458 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2498_m13200_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13200_GM;
MethodInfo m13200_MI = 
{
	".ctor", (methodPointerType)&m13200, &t2498_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2498_m13200_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13200_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t2498_m13201_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t2458_0_0_0;
extern void* RuntimeInvoker_t2458_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13201_GM;
MethodInfo m13201_MI = 
{
	"Invoke", (methodPointerType)&m13201, &t2498_TI, &t2458_0_0_0, RuntimeInvoker_t2458_t29_t29, t2498_m13201_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13201_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t350_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2498_m13202_ParameterInfos[] = 
{
	{"key", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13202_GM;
MethodInfo m13202_MI = 
{
	"BeginInvoke", (methodPointerType)&m13202, &t2498_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29_t29, t2498_m13202_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13202_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2498_m13203_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t2458_0_0_0;
extern void* RuntimeInvoker_t2458_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13203_GM;
MethodInfo m13203_MI = 
{
	"EndInvoke", (methodPointerType)&m13203, &t2498_TI, &t2458_0_0_0, RuntimeInvoker_t2458_t29, t2498_m13203_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13203_GM};
static MethodInfo* t2498_MIs[] =
{
	&m13200_MI,
	&m13201_MI,
	&m13202_MI,
	&m13203_MI,
	NULL
};
static MethodInfo* t2498_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13201_MI,
	&m13202_MI,
	&m13203_MI,
};
static Il2CppInterfaceOffsetPair t2498_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2498_0_0_0;
extern Il2CppType t2498_1_0_0;
struct t2498;
extern Il2CppGenericClass t2498_GC;
TypeInfo t2498_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Transform`1", "", t2498_MIs, NULL, NULL, NULL, &t195_TI, NULL, &t1254_TI, &t2498_TI, NULL, t2498_VT, &EmptyCustomAttributesCache, &t2498_TI, &t2498_0_0_0, &t2498_1_0_0, t2498_IOs, &t2498_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2498), 0, -1, 0, 0, -1, 259, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2499.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2499_TI;
#include "t2499MD.h"

extern MethodInfo m10122_MI;
extern MethodInfo m13206_MI;


// Metadata Definition System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t2460_0_0_1;
FieldInfo t2499_f0_FieldInfo = 
{
	"host_enumerator", &t2460_0_0_1, &t2499_TI, offsetof(t2499, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2499_FIs[] =
{
	&t2499_f0_FieldInfo,
	NULL
};
static PropertyInfo t2499____Entry_PropertyInfo = 
{
	&t2499_TI, "Entry", &m13206_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13207_MI;
static PropertyInfo t2499____Key_PropertyInfo = 
{
	&t2499_TI, "Key", &m13207_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13208_MI;
static PropertyInfo t2499____Value_PropertyInfo = 
{
	&t2499_TI, "Value", &m13208_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13209_MI;
static PropertyInfo t2499____Current_PropertyInfo = 
{
	&t2499_TI, "Current", &m13209_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2499_PIs[] =
{
	&t2499____Entry_PropertyInfo,
	&t2499____Key_PropertyInfo,
	&t2499____Value_PropertyInfo,
	&t2499____Current_PropertyInfo,
	NULL
};
extern Il2CppType t154_0_0_0;
static ParameterInfo t2499_m13204_ParameterInfos[] = 
{
	{"host", 0, 134217728, &EmptyCustomAttributesCache, &t154_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13204_GM;
MethodInfo m13204_MI = 
{
	".ctor", (methodPointerType)&m12971_gshared, &t2499_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2499_m13204_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13204_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13205_GM;
MethodInfo m13205_MI = 
{
	"MoveNext", (methodPointerType)&m12972_gshared, &t2499_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13205_GM};
extern Il2CppType t725_0_0_0;
extern void* RuntimeInvoker_t725 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13206_GM;
MethodInfo m13206_MI = 
{
	"get_Entry", (methodPointerType)&m12973_gshared, &t2499_TI, &t725_0_0_0, RuntimeInvoker_t725, NULL, &EmptyCustomAttributesCache, 2534, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13206_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13207_GM;
MethodInfo m13207_MI = 
{
	"get_Key", (methodPointerType)&m12974_gshared, &t2499_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13207_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13208_GM;
MethodInfo m13208_MI = 
{
	"get_Value", (methodPointerType)&m12975_gshared, &t2499_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13208_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13209_GM;
MethodInfo m13209_MI = 
{
	"get_Current", (methodPointerType)&m12976_gshared, &t2499_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13209_GM};
static MethodInfo* t2499_MIs[] =
{
	&m13204_MI,
	&m13205_MI,
	&m13206_MI,
	&m13207_MI,
	&m13208_MI,
	&m13209_MI,
	NULL
};
extern MethodInfo m13205_MI;
static MethodInfo* t2499_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13209_MI,
	&m13205_MI,
	&m13206_MI,
	&m13207_MI,
	&m13208_MI,
};
static TypeInfo* t2499_ITIs[] = 
{
	&t136_TI,
	&t722_TI,
};
static Il2CppInterfaceOffsetPair t2499_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t722_TI, 6},
};
extern TypeInfo t2460_TI;
extern TypeInfo t148_TI;
extern TypeInfo t350_TI;
static Il2CppRGCTXData t2499_RGCTXData[9] = 
{
	&m12908_MI/* Method Usage */,
	&m13185_MI/* Method Usage */,
	&t2460_TI/* Class Usage */,
	&m13186_MI/* Method Usage */,
	&m12978_MI/* Method Usage */,
	&t148_TI/* Class Usage */,
	&m12980_MI/* Method Usage */,
	&t350_TI/* Class Usage */,
	&m13206_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2499_0_0_0;
extern Il2CppType t2499_1_0_0;
struct t2499;
extern Il2CppGenericClass t2499_GC;
TypeInfo t2499_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ShimEnumerator", "", t2499_MIs, t2499_PIs, t2499_FIs, NULL, &t29_TI, NULL, &t1254_TI, &t2499_TI, t2499_ITIs, t2499_VT, &EmptyCustomAttributesCache, &t2499_TI, &t2499_0_0_0, &t2499_1_0_0, t2499_IOs, &t2499_GC, NULL, NULL, NULL, t2499_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2499), 0, -1, 0, 0, -1, 1056771, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 4, 1, 0, 0, 9, 2, 2};
#include "t2500.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2500_TI;
#include "t2500MD.h"

#include "t2501.h"
extern TypeInfo t6680_TI;
extern TypeInfo t2501_TI;
#include "t2501MD.h"
extern Il2CppType t6680_0_0_0;
extern MethodInfo m13215_MI;
extern MethodInfo m27803_MI;
extern MethodInfo m27804_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.Font>
extern Il2CppType t2500_0_0_49;
FieldInfo t2500_f0_FieldInfo = 
{
	"_default", &t2500_0_0_49, &t2500_TI, offsetof(t2500_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2500_FIs[] =
{
	&t2500_f0_FieldInfo,
	NULL
};
extern MethodInfo m13214_MI;
static PropertyInfo t2500____Default_PropertyInfo = 
{
	&t2500_TI, "Default", &m13214_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2500_PIs[] =
{
	&t2500____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13210_GM;
MethodInfo m13210_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2500_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13210_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13211_GM;
MethodInfo m13211_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2500_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13211_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2500_m13212_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13212_GM;
MethodInfo m13212_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2500_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2500_m13212_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13212_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2500_m13213_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13213_GM;
MethodInfo m13213_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2500_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2500_m13213_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13213_GM};
extern Il2CppType t148_0_0_0;
static ParameterInfo t2500_m27803_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27803_GM;
MethodInfo m27803_MI = 
{
	"GetHashCode", NULL, &t2500_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2500_m27803_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27803_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t148_0_0_0;
static ParameterInfo t2500_m27804_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27804_GM;
MethodInfo m27804_MI = 
{
	"Equals", NULL, &t2500_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2500_m27804_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27804_GM};
extern Il2CppType t2500_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13214_GM;
MethodInfo m13214_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2500_TI, &t2500_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13214_GM};
static MethodInfo* t2500_MIs[] =
{
	&m13210_MI,
	&m13211_MI,
	&m13212_MI,
	&m13213_MI,
	&m27803_MI,
	&m27804_MI,
	&m13214_MI,
	NULL
};
extern MethodInfo m13213_MI;
extern MethodInfo m13212_MI;
static MethodInfo* t2500_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27804_MI,
	&m27803_MI,
	&m13213_MI,
	&m13212_MI,
	NULL,
	NULL,
};
static TypeInfo* t2500_ITIs[] = 
{
	&t2454_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2500_IOs[] = 
{
	{ &t2454_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2500_TI;
extern TypeInfo t2500_TI;
extern TypeInfo t2501_TI;
extern TypeInfo t148_TI;
static Il2CppRGCTXData t2500_RGCTXData[9] = 
{
	&t6680_0_0_0/* Type Usage */,
	&t148_0_0_0/* Type Usage */,
	&t2500_TI/* Class Usage */,
	&t2500_TI/* Static Usage */,
	&t2501_TI/* Class Usage */,
	&m13215_MI/* Method Usage */,
	&t148_TI/* Class Usage */,
	&m27803_MI/* Method Usage */,
	&m27804_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2500_0_0_0;
extern Il2CppType t2500_1_0_0;
struct t2500;
extern Il2CppGenericClass t2500_GC;
TypeInfo t2500_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2500_MIs, t2500_PIs, t2500_FIs, NULL, &t29_TI, NULL, NULL, &t2500_TI, t2500_ITIs, t2500_VT, &EmptyCustomAttributesCache, &t2500_TI, &t2500_0_0_0, &t2500_1_0_0, t2500_IOs, &t2500_GC, NULL, NULL, NULL, t2500_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2500), 0, -1, sizeof(t2500_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.Font>
extern Il2CppType t148_0_0_0;
static ParameterInfo t6680_m27805_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27805_GM;
MethodInfo m27805_MI = 
{
	"Equals", NULL, &t6680_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6680_m27805_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27805_GM};
static MethodInfo* t6680_MIs[] =
{
	&m27805_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6680_1_0_0;
struct t6680;
extern Il2CppGenericClass t6680_GC;
TypeInfo t6680_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6680_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6680_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6680_TI, &t6680_0_0_0, &t6680_1_0_0, NULL, &t6680_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13210_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Font>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13215_GM;
MethodInfo m13215_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2501_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13215_GM};
extern Il2CppType t148_0_0_0;
static ParameterInfo t2501_m13216_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13216_GM;
MethodInfo m13216_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2501_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2501_m13216_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13216_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t148_0_0_0;
static ParameterInfo t2501_m13217_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13217_GM;
MethodInfo m13217_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2501_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2501_m13217_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13217_GM};
static MethodInfo* t2501_MIs[] =
{
	&m13215_MI,
	&m13216_MI,
	&m13217_MI,
	NULL
};
extern MethodInfo m13217_MI;
extern MethodInfo m13216_MI;
static MethodInfo* t2501_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13217_MI,
	&m13216_MI,
	&m13213_MI,
	&m13212_MI,
	&m13216_MI,
	&m13217_MI,
};
static Il2CppInterfaceOffsetPair t2501_IOs[] = 
{
	{ &t2454_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2500_TI;
extern TypeInfo t2500_TI;
extern TypeInfo t2501_TI;
extern TypeInfo t148_TI;
extern TypeInfo t148_TI;
static Il2CppRGCTXData t2501_RGCTXData[11] = 
{
	&t6680_0_0_0/* Type Usage */,
	&t148_0_0_0/* Type Usage */,
	&t2500_TI/* Class Usage */,
	&t2500_TI/* Static Usage */,
	&t2501_TI/* Class Usage */,
	&m13215_MI/* Method Usage */,
	&t148_TI/* Class Usage */,
	&m27803_MI/* Method Usage */,
	&m27804_MI/* Method Usage */,
	&m13210_MI/* Method Usage */,
	&t148_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2501_0_0_0;
extern Il2CppType t2501_1_0_0;
struct t2501;
extern Il2CppGenericClass t2501_GC;
TypeInfo t2501_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2501_MIs, NULL, NULL, NULL, &t2500_TI, NULL, &t1256_TI, &t2501_TI, NULL, t2501_VT, &EmptyCustomAttributesCache, &t2501_TI, &t2501_0_0_0, &t2501_1_0_0, t2501_IOs, &t2501_GC, NULL, NULL, NULL, t2501_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2501), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6665_TI;



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t350_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t6665_m27662_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27662_GM;
MethodInfo m27662_MI = 
{
	"Equals", NULL, &t6665_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t6665_m27662_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27662_GM};
extern Il2CppType t350_0_0_0;
static ParameterInfo t6665_m27806_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27806_GM;
MethodInfo m27806_MI = 
{
	"GetHashCode", NULL, &t6665_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6665_m27806_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27806_GM};
static MethodInfo* t6665_MIs[] =
{
	&m27662_MI,
	&m27806_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6665_0_0_0;
extern Il2CppType t6665_1_0_0;
struct t6665;
extern Il2CppGenericClass t6665_GC;
TypeInfo t6665_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6665_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6665_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6665_TI, &t6665_0_0_0, &t6665_1_0_0, NULL, &t6665_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#include "t2502.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2502_TI;
#include "t2502MD.h"

#include "t2503.h"
extern TypeInfo t6681_TI;
extern TypeInfo t2503_TI;
#include "t2503MD.h"
extern Il2CppType t6681_0_0_0;
extern MethodInfo m13223_MI;
extern MethodInfo m27807_MI;
extern MethodInfo m27663_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t2502_0_0_49;
FieldInfo t2502_f0_FieldInfo = 
{
	"_default", &t2502_0_0_49, &t2502_TI, offsetof(t2502_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2502_FIs[] =
{
	&t2502_f0_FieldInfo,
	NULL
};
extern MethodInfo m13222_MI;
static PropertyInfo t2502____Default_PropertyInfo = 
{
	&t2502_TI, "Default", &m13222_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2502_PIs[] =
{
	&t2502____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13218_GM;
MethodInfo m13218_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2502_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13218_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13219_GM;
MethodInfo m13219_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2502_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13219_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2502_m13220_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13220_GM;
MethodInfo m13220_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2502_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2502_m13220_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13220_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2502_m13221_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13221_GM;
MethodInfo m13221_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2502_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2502_m13221_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13221_GM};
extern Il2CppType t350_0_0_0;
static ParameterInfo t2502_m27807_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27807_GM;
MethodInfo m27807_MI = 
{
	"GetHashCode", NULL, &t2502_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2502_m27807_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27807_GM};
extern Il2CppType t350_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t2502_m27663_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27663_GM;
MethodInfo m27663_MI = 
{
	"Equals", NULL, &t2502_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2502_m27663_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27663_GM};
extern Il2CppType t2502_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13222_GM;
MethodInfo m13222_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2502_TI, &t2502_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13222_GM};
static MethodInfo* t2502_MIs[] =
{
	&m13218_MI,
	&m13219_MI,
	&m13220_MI,
	&m13221_MI,
	&m27807_MI,
	&m27663_MI,
	&m13222_MI,
	NULL
};
extern MethodInfo m13221_MI;
extern MethodInfo m13220_MI;
static MethodInfo* t2502_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27663_MI,
	&m27807_MI,
	&m13221_MI,
	&m13220_MI,
	NULL,
	NULL,
};
static TypeInfo* t2502_ITIs[] = 
{
	&t6665_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2502_IOs[] = 
{
	{ &t6665_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2502_TI;
extern TypeInfo t2502_TI;
extern TypeInfo t2503_TI;
extern TypeInfo t350_TI;
static Il2CppRGCTXData t2502_RGCTXData[9] = 
{
	&t6681_0_0_0/* Type Usage */,
	&t350_0_0_0/* Type Usage */,
	&t2502_TI/* Class Usage */,
	&t2502_TI/* Static Usage */,
	&t2503_TI/* Class Usage */,
	&m13223_MI/* Method Usage */,
	&t350_TI/* Class Usage */,
	&m27807_MI/* Method Usage */,
	&m27663_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2502_0_0_0;
extern Il2CppType t2502_1_0_0;
struct t2502;
extern Il2CppGenericClass t2502_GC;
TypeInfo t2502_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2502_MIs, t2502_PIs, t2502_FIs, NULL, &t29_TI, NULL, NULL, &t2502_TI, t2502_ITIs, t2502_VT, &EmptyCustomAttributesCache, &t2502_TI, &t2502_0_0_0, &t2502_1_0_0, t2502_IOs, &t2502_GC, NULL, NULL, NULL, t2502_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2502), 0, -1, sizeof(t2502_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t350_0_0_0;
static ParameterInfo t6681_m27808_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27808_GM;
MethodInfo m27808_MI = 
{
	"Equals", NULL, &t6681_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6681_m27808_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27808_GM};
static MethodInfo* t6681_MIs[] =
{
	&m27808_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6681_1_0_0;
struct t6681;
extern Il2CppGenericClass t6681_GC;
TypeInfo t6681_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6681_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6681_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6681_TI, &t6681_0_0_0, &t6681_1_0_0, NULL, &t6681_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13218_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.List`1<UnityEngine.UI.Text>>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13223_GM;
MethodInfo m13223_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2503_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13223_GM};
extern Il2CppType t350_0_0_0;
static ParameterInfo t2503_m13224_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13224_GM;
MethodInfo m13224_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2503_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2503_m13224_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13224_GM};
extern Il2CppType t350_0_0_0;
extern Il2CppType t350_0_0_0;
static ParameterInfo t2503_m13225_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t350_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13225_GM;
MethodInfo m13225_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2503_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2503_m13225_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13225_GM};
static MethodInfo* t2503_MIs[] =
{
	&m13223_MI,
	&m13224_MI,
	&m13225_MI,
	NULL
};
extern MethodInfo m13225_MI;
extern MethodInfo m13224_MI;
static MethodInfo* t2503_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13225_MI,
	&m13224_MI,
	&m13221_MI,
	&m13220_MI,
	&m13224_MI,
	&m13225_MI,
};
static Il2CppInterfaceOffsetPair t2503_IOs[] = 
{
	{ &t6665_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2502_TI;
extern TypeInfo t2502_TI;
extern TypeInfo t2503_TI;
extern TypeInfo t350_TI;
extern TypeInfo t350_TI;
static Il2CppRGCTXData t2503_RGCTXData[11] = 
{
	&t6681_0_0_0/* Type Usage */,
	&t350_0_0_0/* Type Usage */,
	&t2502_TI/* Class Usage */,
	&t2502_TI/* Static Usage */,
	&t2503_TI/* Class Usage */,
	&m13223_MI/* Method Usage */,
	&t350_TI/* Class Usage */,
	&m27807_MI/* Method Usage */,
	&m27663_MI/* Method Usage */,
	&m13218_MI/* Method Usage */,
	&t350_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2503_0_0_0;
extern Il2CppType t2503_1_0_0;
struct t2503;
extern Il2CppGenericClass t2503_GC;
TypeInfo t2503_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2503_MIs, NULL, NULL, NULL, &t2502_TI, NULL, &t1256_TI, &t2503_TI, NULL, t2503_VT, &EmptyCustomAttributesCache, &t2503_TI, &t2503_0_0_0, &t2503_1_0_0, t2503_IOs, &t2503_GC, NULL, NULL, NULL, t2503_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2503), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#include "t351.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t351_TI;
#include "t351MD.h"



// Metadata Definition System.Action`1<UnityEngine.Font>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t351_m1545_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1545_GM;
MethodInfo m1545_MI = 
{
	".ctor", (methodPointerType)&m13226_gshared, &t351_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t351_m1545_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1545_GM};
extern Il2CppType t148_0_0_0;
static ParameterInfo t351_m2902_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2902_GM;
MethodInfo m2902_MI = 
{
	"Invoke", (methodPointerType)&m13227_gshared, &t351_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t351_m2902_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2902_GM};
extern Il2CppType t148_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t351_m13228_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t148_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13228_GM;
MethodInfo m13228_MI = 
{
	"BeginInvoke", (methodPointerType)&m13229_gshared, &t351_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t351_m13228_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13228_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t351_m13230_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13230_GM;
MethodInfo m13230_MI = 
{
	"EndInvoke", (methodPointerType)&m13231_gshared, &t351_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t351_m13230_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13230_GM};
static MethodInfo* t351_MIs[] =
{
	&m1545_MI,
	&m2902_MI,
	&m13228_MI,
	&m13230_MI,
	NULL
};
extern MethodInfo m2902_MI;
extern MethodInfo m13228_MI;
extern MethodInfo m13230_MI;
static MethodInfo* t351_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m2902_MI,
	&m13228_MI,
	&m13230_MI,
};
static Il2CppInterfaceOffsetPair t351_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t351_0_0_0;
extern Il2CppType t351_1_0_0;
struct t351;
extern Il2CppGenericClass t351_GC;
TypeInfo t351_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Action`1", "System", t351_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t351_TI, NULL, t351_VT, &EmptyCustomAttributesCache, &t351_TI, &t351_0_0_0, &t351_1_0_0, t351_IOs, &t351_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t351), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2504.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2504_TI;
#include "t2504MD.h"



extern MethodInfo m13226_MI;
 void m13226_gshared (t2504 * __this, t29 * p0, t35 p1, MethodInfo* method)
{
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m13227_MI;
 void m13227_gshared (t2504 * __this, t29 * p0, MethodInfo* method)
{
	if(__this->f9 != NULL)
	{
		m13227((t2504 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, t29 * p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	if (__this->f2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 * __this, t29 * p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	else
	{
		typedef void (*FunctionPointerType) (t29 * __this, MethodInfo* method);
		((FunctionPointerType)__this->f0)(p0,(MethodInfo*)(__this->f3.f0));
	}
}
extern MethodInfo m13229_MI;
 t29 * m13229_gshared (t2504 * __this, t29 * p0, t67 * p1, t29 * p2, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = p0;
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m13231_MI;
 void m13231_gshared (t2504 * __this, t29 * p0, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition System.Action`1<System.Object>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2504_m13226_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13226_GM;
MethodInfo m13226_MI = 
{
	".ctor", (methodPointerType)&m13226_gshared, &t2504_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2504_m13226_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13226_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2504_m13227_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13227_GM;
MethodInfo m13227_MI = 
{
	"Invoke", (methodPointerType)&m13227_gshared, &t2504_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2504_m13227_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13227_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2504_m13229_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13229_GM;
MethodInfo m13229_MI = 
{
	"BeginInvoke", (methodPointerType)&m13229_gshared, &t2504_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2504_m13229_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13229_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2504_m13231_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13231_GM;
MethodInfo m13231_MI = 
{
	"EndInvoke", (methodPointerType)&m13231_gshared, &t2504_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2504_m13231_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13231_GM};
static MethodInfo* t2504_MIs[] =
{
	&m13226_MI,
	&m13227_MI,
	&m13229_MI,
	&m13231_MI,
	NULL
};
static MethodInfo* t2504_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13227_MI,
	&m13229_MI,
	&m13231_MI,
};
static Il2CppInterfaceOffsetPair t2504_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2504_0_0_0;
extern Il2CppType t2504_1_0_0;
struct t2504;
extern Il2CppGenericClass t2504_GC;
TypeInfo t2504_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Action`1", "System", t2504_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2504_TI, NULL, t2504_VT, &EmptyCustomAttributesCache, &t2504_TI, &t2504_0_0_0, &t2504_1_0_0, t2504_IOs, &t2504_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2504), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2505.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2505_TI;
#include "t2505MD.h"

#include "t41.h"
#include "t557.h"
#include "t2506.h"
extern TypeInfo t316_TI;
extern TypeInfo t2506_TI;
#include "t2506MD.h"
extern MethodInfo m13234_MI;
extern MethodInfo m13236_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Graphic>
extern Il2CppType t316_0_0_33;
FieldInfo t2505_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2505_TI, offsetof(t2505, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2505_FIs[] =
{
	&t2505_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t155_0_0_0;
static ParameterInfo t2505_m13232_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13232_GM;
MethodInfo m13232_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2505_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2505_m13232_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13232_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2505_m13233_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13233_GM;
MethodInfo m13233_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2505_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2505_m13233_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13233_GM};
static MethodInfo* t2505_MIs[] =
{
	&m13232_MI,
	&m13233_MI,
	NULL
};
extern MethodInfo m13233_MI;
extern MethodInfo m13237_MI;
static MethodInfo* t2505_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13233_MI,
	&m13237_MI,
};
extern Il2CppType t2507_0_0_0;
extern TypeInfo t2507_TI;
extern MethodInfo m20906_MI;
extern TypeInfo t155_TI;
extern MethodInfo m13239_MI;
extern TypeInfo t155_TI;
static Il2CppRGCTXData t2505_RGCTXData[8] = 
{
	&t2507_0_0_0/* Type Usage */,
	&t2507_TI/* Class Usage */,
	&m20906_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
	&m13239_MI/* Method Usage */,
	&m13234_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
	&m13236_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2505_0_0_0;
extern Il2CppType t2505_1_0_0;
struct t2505;
extern Il2CppGenericClass t2505_GC;
TypeInfo t2505_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2505_MIs, NULL, t2505_FIs, NULL, &t2506_TI, NULL, NULL, &t2505_TI, NULL, t2505_VT, &EmptyCustomAttributesCache, &t2505_TI, &t2505_0_0_0, &t2505_1_0_0, NULL, &t2505_GC, NULL, NULL, NULL, t2505_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2505), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2507.h"
#include "t353.h"
extern TypeInfo t2507_TI;
#include "t556MD.h"
#include "t353MD.h"
#include "t2507MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m20906(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Graphic>
extern Il2CppType t2507_0_0_1;
FieldInfo t2506_f0_FieldInfo = 
{
	"Delegate", &t2507_0_0_1, &t2506_TI, offsetof(t2506, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2506_FIs[] =
{
	&t2506_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2506_m13234_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13234_GM;
MethodInfo m13234_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2506_m13234_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13234_GM};
extern Il2CppType t2507_0_0_0;
static ParameterInfo t2506_m13235_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2507_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13235_GM;
MethodInfo m13235_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2506_m13235_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13235_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2506_m13236_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13236_GM;
MethodInfo m13236_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2506_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2506_m13236_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13236_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2506_m13237_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13237_GM;
MethodInfo m13237_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2506_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2506_m13237_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13237_GM};
static MethodInfo* t2506_MIs[] =
{
	&m13234_MI,
	&m13235_MI,
	&m13236_MI,
	&m13237_MI,
	NULL
};
static MethodInfo* t2506_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13236_MI,
	&m13237_MI,
};
extern TypeInfo t2507_TI;
extern TypeInfo t155_TI;
static Il2CppRGCTXData t2506_RGCTXData[5] = 
{
	&t2507_0_0_0/* Type Usage */,
	&t2507_TI/* Class Usage */,
	&m20906_MI/* Method Usage */,
	&t155_TI/* Class Usage */,
	&m13239_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2506_0_0_0;
extern Il2CppType t2506_1_0_0;
extern TypeInfo t556_TI;
struct t2506;
extern Il2CppGenericClass t2506_GC;
TypeInfo t2506_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2506_MIs, NULL, t2506_FIs, NULL, &t556_TI, NULL, NULL, &t2506_TI, NULL, t2506_VT, &EmptyCustomAttributesCache, &t2506_TI, &t2506_0_0_0, &t2506_1_0_0, NULL, &t2506_GC, NULL, NULL, NULL, t2506_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2506), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Graphic>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2507_m13238_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13238_GM;
MethodInfo m13238_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2507_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2507_m13238_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13238_GM};
extern Il2CppType t155_0_0_0;
static ParameterInfo t2507_m13239_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13239_GM;
MethodInfo m13239_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2507_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2507_m13239_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13239_GM};
extern Il2CppType t155_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2507_m13240_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t155_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13240_GM;
MethodInfo m13240_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2507_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2507_m13240_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13240_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2507_m13241_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13241_GM;
MethodInfo m13241_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2507_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2507_m13241_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13241_GM};
static MethodInfo* t2507_MIs[] =
{
	&m13238_MI,
	&m13239_MI,
	&m13240_MI,
	&m13241_MI,
	NULL
};
extern MethodInfo m13240_MI;
extern MethodInfo m13241_MI;
static MethodInfo* t2507_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13239_MI,
	&m13240_MI,
	&m13241_MI,
};
static Il2CppInterfaceOffsetPair t2507_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2507_1_0_0;
struct t2507;
extern Il2CppGenericClass t2507_GC;
TypeInfo t2507_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2507_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2507_TI, NULL, t2507_VT, &EmptyCustomAttributesCache, &t2507_TI, &t2507_0_0_0, &t2507_1_0_0, t2507_IOs, &t2507_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2507), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t158.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t158_TI;
#include "t158MD.h"

#include "t2508.h"
#include "t161.h"
#include "t163.h"
extern TypeInfo t2508_TI;
extern TypeInfo t163_TI;
extern TypeInfo t161_TI;
#include "t2508MD.h"
#include "t161MD.h"
#include "t293MD.h"
extern MethodInfo m13242_MI;
extern MethodInfo m13245_MI;
extern MethodInfo m13255_MI;
extern MethodInfo m13246_MI;
extern MethodInfo m20946_MI;
extern MethodInfo m13243_MI;
extern MethodInfo m13253_MI;
extern MethodInfo m13405_MI;
extern MethodInfo m13252_MI;
extern MethodInfo m2868_MI;
extern MethodInfo m1296_MI;
extern MethodInfo m13254_MI;
struct t931;
#include "t931.h"
struct t931;
 t29 * m20008_gshared (t29 * __this, MethodInfo* method);
#define m20008(__this, method) (t29 *)m20008_gshared((t29 *)__this, method)
#define m20946(__this, method) (t163 *)m20008_gshared((t29 *)__this, method)


// Metadata Definition UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
extern Il2CppType t2508_0_0_33;
FieldInfo t158_f0_FieldInfo = 
{
	"m_Stack", &t2508_0_0_33, &t158_TI, offsetof(t158, f0), &EmptyCustomAttributesCache};
extern Il2CppType t161_0_0_33;
FieldInfo t158_f1_FieldInfo = 
{
	"m_ActionOnGet", &t161_0_0_33, &t158_TI, offsetof(t158, f1), &EmptyCustomAttributesCache};
extern Il2CppType t161_0_0_33;
FieldInfo t158_f2_FieldInfo = 
{
	"m_ActionOnRelease", &t161_0_0_33, &t158_TI, offsetof(t158, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
extern CustomAttributesCache t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
FieldInfo t158_f3_FieldInfo = 
{
	"<countAll>k__BackingField", &t44_0_0_1, &t158_TI, offsetof(t158, f3), &t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField};
static FieldInfo* t158_FIs[] =
{
	&t158_f0_FieldInfo,
	&t158_f1_FieldInfo,
	&t158_f2_FieldInfo,
	&t158_f3_FieldInfo,
	NULL
};
static PropertyInfo t158____countAll_PropertyInfo = 
{
	&t158_TI, "countAll", &m13242_MI, &m13243_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13244_MI;
static PropertyInfo t158____countActive_PropertyInfo = 
{
	&t158_TI, "countActive", &m13244_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t158____countInactive_PropertyInfo = 
{
	&t158_TI, "countInactive", &m13245_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t158_PIs[] =
{
	&t158____countAll_PropertyInfo,
	&t158____countActive_PropertyInfo,
	&t158____countInactive_PropertyInfo,
	NULL
};
extern Il2CppType t161_0_0_0;
extern Il2CppType t161_0_0_0;
extern Il2CppType t161_0_0_0;
static ParameterInfo t158_m1563_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134217728, &EmptyCustomAttributesCache, &t161_0_0_0},
	{"actionOnRelease", 1, 134217728, &EmptyCustomAttributesCache, &t161_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1563_GM;
MethodInfo m1563_MI = 
{
	".ctor", (methodPointerType)&m11051_gshared, &t158_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t158_m1563_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1563_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t282__CustomAttributeCache_m2037;
extern Il2CppGenericMethod m13242_GM;
MethodInfo m13242_MI = 
{
	"get_countAll", (methodPointerType)&m11053_gshared, &t158_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &t282__CustomAttributeCache_m2037, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13242_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t158_m13243_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t282__CustomAttributeCache_m2038;
extern Il2CppGenericMethod m13243_GM;
MethodInfo m13243_MI = 
{
	"set_countAll", (methodPointerType)&m11055_gshared, &t158_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t158_m13243_ParameterInfos, &t282__CustomAttributeCache_m2038, 2177, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13243_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13244_GM;
MethodInfo m13244_MI = 
{
	"get_countActive", (methodPointerType)&m11057_gshared, &t158_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13244_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13245_GM;
MethodInfo m13245_MI = 
{
	"get_countInactive", (methodPointerType)&m11059_gshared, &t158_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13245_GM};
extern Il2CppType t163_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1571_GM;
MethodInfo m1571_MI = 
{
	"Get", (methodPointerType)&m11061_gshared, &t158_TI, &t163_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1571_GM};
extern Il2CppType t163_0_0_0;
extern Il2CppType t163_0_0_0;
static ParameterInfo t158_m1577_ParameterInfos[] = 
{
	{"element", 0, 134217728, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1577_GM;
MethodInfo m1577_MI = 
{
	"Release", (methodPointerType)&m11063_gshared, &t158_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t158_m1577_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1577_GM};
static MethodInfo* t158_MIs[] =
{
	&m1563_MI,
	&m13242_MI,
	&m13243_MI,
	&m13244_MI,
	&m13245_MI,
	&m1571_MI,
	&m1577_MI,
	NULL
};
static MethodInfo* t158_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern TypeInfo t2508_TI;
extern TypeInfo t163_TI;
static Il2CppRGCTXData t158_RGCTXData[12] = 
{
	&t2508_TI/* Class Usage */,
	&m13246_MI/* Method Usage */,
	&m13242_MI/* Method Usage */,
	&m13245_MI/* Method Usage */,
	&m13255_MI/* Method Usage */,
	&t163_TI/* Class Usage */,
	&m20946_MI/* Method Usage */,
	&m13243_MI/* Method Usage */,
	&m13253_MI/* Method Usage */,
	&m13405_MI/* Method Usage */,
	&m13252_MI/* Method Usage */,
	&m13254_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t158_0_0_0;
extern Il2CppType t158_1_0_0;
struct t158;
extern Il2CppGenericClass t158_GC;
extern CustomAttributesCache t282__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
extern CustomAttributesCache t282__CustomAttributeCache_m2037;
extern CustomAttributesCache t282__CustomAttributeCache_m2038;
TypeInfo t158_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "ObjectPool`1", "UnityEngine.UI", t158_MIs, t158_PIs, t158_FIs, NULL, &t29_TI, NULL, NULL, &t158_TI, NULL, t158_VT, &EmptyCustomAttributesCache, &t158_TI, &t158_0_0_0, &t158_1_0_0, NULL, &t158_GC, NULL, NULL, NULL, t158_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t158), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 3, 4, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2511.h"
extern TypeInfo t1622_TI;
extern TypeInfo t2511_TI;
#include "t2511MD.h"
extern MethodInfo m4199_MI;
extern MethodInfo m5171_MI;
extern MethodInfo m13256_MI;
extern MethodInfo m20945_MI;
extern MethodInfo m13400_MI;
struct t20;
struct t20;
 void m19788_gshared (t29 * __this, t316** p0, int32_t p1, MethodInfo* method);
#define m19788(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
#define m20945(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)


 t2511  m13256 (t2508 * __this, MethodInfo* method){
	{
		t2511  L_0 = {0};
		m13400(&L_0, __this, &m13400_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
extern Il2CppType t44_0_0_32849;
FieldInfo t2508_f0_FieldInfo = 
{
	"INITIAL_SIZE", &t44_0_0_32849, &t2508_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2509_0_0_1;
FieldInfo t2508_f1_FieldInfo = 
{
	"_array", &t2509_0_0_1, &t2508_TI, offsetof(t2508, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2508_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t2508_TI, offsetof(t2508, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2508_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t2508_TI, offsetof(t2508, f3), &EmptyCustomAttributesCache};
static FieldInfo* t2508_FIs[] =
{
	&t2508_f0_FieldInfo,
	&t2508_f1_FieldInfo,
	&t2508_f2_FieldInfo,
	&t2508_f3_FieldInfo,
	NULL
};
static const int32_t t2508_f0_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry t2508_f0_DefaultValue = 
{
	&t2508_f0_FieldInfo, { (char*)&t2508_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t2508_FDVs[] = 
{
	&t2508_f0_DefaultValue,
	NULL
};
extern MethodInfo m13247_MI;
static PropertyInfo t2508____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2508_TI, "System.Collections.ICollection.IsSynchronized", &m13247_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13248_MI;
static PropertyInfo t2508____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2508_TI, "System.Collections.ICollection.SyncRoot", &m13248_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2508____Count_PropertyInfo = 
{
	&t2508_TI, "Count", &m13255_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2508_PIs[] =
{
	&t2508____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2508____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2508____Count_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13246_GM;
MethodInfo m13246_MI = 
{
	".ctor", (methodPointerType)&m11064_gshared, &t2508_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13246_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13247_GM;
MethodInfo m13247_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m11065_gshared, &t2508_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13247_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13248_GM;
MethodInfo m13248_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m11066_gshared, &t2508_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13248_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2508_m13249_ParameterInfos[] = 
{
	{"dest", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"idx", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13249_GM;
MethodInfo m13249_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m11067_gshared, &t2508_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2508_m13249_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13249_GM};
extern Il2CppType t2510_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13250_GM;
MethodInfo m13250_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m11068_gshared, &t2508_TI, &t2510_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13250_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13251_GM;
MethodInfo m13251_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m11069_gshared, &t2508_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13251_GM};
extern Il2CppType t163_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13252_GM;
MethodInfo m13252_MI = 
{
	"Peek", (methodPointerType)&m11070_gshared, &t2508_TI, &t163_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13252_GM};
extern Il2CppType t163_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13253_GM;
MethodInfo m13253_MI = 
{
	"Pop", (methodPointerType)&m11071_gshared, &t2508_TI, &t163_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13253_GM};
extern Il2CppType t163_0_0_0;
static ParameterInfo t2508_m13254_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13254_GM;
MethodInfo m13254_MI = 
{
	"Push", (methodPointerType)&m11072_gshared, &t2508_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2508_m13254_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13254_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13255_GM;
MethodInfo m13255_MI = 
{
	"get_Count", (methodPointerType)&m11073_gshared, &t2508_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13255_GM};
extern Il2CppType t2511_0_0_0;
extern void* RuntimeInvoker_t2511 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13256_GM;
MethodInfo m13256_MI = 
{
	"GetEnumerator", (methodPointerType)&m13256, &t2508_TI, &t2511_0_0_0, RuntimeInvoker_t2511, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13256_GM};
static MethodInfo* t2508_MIs[] =
{
	&m13246_MI,
	&m13247_MI,
	&m13248_MI,
	&m13249_MI,
	&m13250_MI,
	&m13251_MI,
	&m13252_MI,
	&m13253_MI,
	&m13254_MI,
	&m13255_MI,
	&m13256_MI,
	NULL
};
extern MethodInfo m13249_MI;
extern MethodInfo m13251_MI;
extern MethodInfo m13250_MI;
static MethodInfo* t2508_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13255_MI,
	&m13247_MI,
	&m13248_MI,
	&m13249_MI,
	&m13251_MI,
	&m13250_MI,
};
extern TypeInfo t5324_TI;
static TypeInfo* t2508_ITIs[] = 
{
	&t674_TI,
	&t603_TI,
	&t5324_TI,
};
static Il2CppInterfaceOffsetPair t2508_IOs[] = 
{
	{ &t674_TI, 4},
	{ &t603_TI, 8},
	{ &t5324_TI, 9},
};
extern TypeInfo t2511_TI;
static Il2CppRGCTXData t2508_RGCTXData[4] = 
{
	&m13256_MI/* Method Usage */,
	&t2511_TI/* Class Usage */,
	&m20945_MI/* Method Usage */,
	&m13400_MI/* Method Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t2508_0_0_0;
extern Il2CppType t2508_1_0_0;
struct t2508;
extern Il2CppGenericClass t2508_GC;
extern CustomAttributesCache t717__CustomAttributeCache;
TypeInfo t2508_TI = 
{
	&g_System_dll_Image, NULL, "Stack`1", "System.Collections.Generic", t2508_MIs, t2508_PIs, t2508_FIs, NULL, &t29_TI, NULL, NULL, &t2508_TI, t2508_ITIs, t2508_VT, &t717__CustomAttributeCache, &t2508_TI, &t2508_0_0_0, &t2508_1_0_0, t2508_IOs, &t2508_GC, NULL, t2508_FDVs, NULL, t2508_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2508), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 11, 3, 4, 0, 0, 10, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
extern Il2CppType t2510_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27809_GM;
MethodInfo m27809_MI = 
{
	"GetEnumerator", NULL, &t5324_TI, &t2510_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27809_GM};
static MethodInfo* t5324_MIs[] =
{
	&m27809_MI,
	NULL
};
static TypeInfo* t5324_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5324_0_0_0;
extern Il2CppType t5324_1_0_0;
struct t5324;
extern Il2CppGenericClass t5324_GC;
TypeInfo t5324_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5324_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5324_TI, t5324_ITIs, NULL, &EmptyCustomAttributesCache, &t5324_TI, &t5324_0_0_0, &t5324_1_0_0, NULL, &t5324_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2510_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
extern MethodInfo m27810_MI;
static PropertyInfo t2510____Current_PropertyInfo = 
{
	&t2510_TI, "Current", &m27810_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2510_PIs[] =
{
	&t2510____Current_PropertyInfo,
	NULL
};
extern Il2CppType t163_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27810_GM;
MethodInfo m27810_MI = 
{
	"get_Current", NULL, &t2510_TI, &t163_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27810_GM};
static MethodInfo* t2510_MIs[] =
{
	&m27810_MI,
	NULL
};
static TypeInfo* t2510_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2510_0_0_0;
extern Il2CppType t2510_1_0_0;
struct t2510;
extern Il2CppGenericClass t2510_GC;
TypeInfo t2510_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2510_MIs, t2510_PIs, NULL, NULL, NULL, NULL, NULL, &t2510_TI, t2510_ITIs, NULL, &EmptyCustomAttributesCache, &t2510_TI, &t2510_0_0_0, &t2510_1_0_0, NULL, &t2510_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
#include "t163MD.h"

#include "t184.h"
#include "t2517.h"
#include "t2514.h"
#include "t2515.h"
#include "t2522.h"
#include "t2516.h"
extern TypeInfo t184_TI;
extern TypeInfo t585_TI;
extern TypeInfo t1649_TI;
extern TypeInfo t915_TI;
extern TypeInfo t201_TI;
extern TypeInfo t2517_TI;
extern TypeInfo t404_TI;
extern TypeInfo t2513_TI;
extern TypeInfo t2512_TI;
extern TypeInfo t2514_TI;
extern TypeInfo t2515_TI;
extern TypeInfo t2522_TI;
#include "t915MD.h"
#include "t602MD.h"
#include "t2514MD.h"
#include "t2515MD.h"
#include "t2517MD.h"
#include "t2522MD.h"
extern MethodInfo m2044_MI;
extern MethodInfo m2045_MI;
extern MethodInfo m4198_MI;
extern MethodInfo m20919_MI;
extern MethodInfo m3975_MI;
extern MethodInfo m13286_MI;
extern MethodInfo m13283_MI;
extern MethodInfo m5952_MI;
extern MethodInfo m1584_MI;
extern MethodInfo m13278_MI;
extern MethodInfo m13284_MI;
extern MethodInfo m13287_MI;
extern MethodInfo m13289_MI;
extern MethodInfo m13273_MI;
extern MethodInfo m1599_MI;
extern MethodInfo m5139_MI;
extern MethodInfo m1600_MI;
extern MethodInfo m1926_MI;
extern MethodInfo m27811_MI;
extern MethodInfo m27812_MI;
extern MethodInfo m27813_MI;
extern MethodInfo m3972_MI;
extern MethodInfo m1428_MI;
extern MethodInfo m13288_MI;
extern MethodInfo m13274_MI;
extern MethodInfo m13275_MI;
extern MethodInfo m13307_MI;
extern MethodInfo m5112_MI;
extern MethodInfo m20921_MI;
extern MethodInfo m13281_MI;
extern MethodInfo m13282_MI;
extern MethodInfo m13382_MI;
extern MethodInfo m13301_MI;
extern MethodInfo m13285_MI;
extern MethodInfo m13291_MI;
extern MethodInfo m13388_MI;
extern MethodInfo m20923_MI;
extern MethodInfo m20931_MI;
extern MethodInfo m5951_MI;
struct t20;
 void m20919 (t29 * __this, t201** p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t20;
#include "t2520.h"
 int32_t m20921 (t29 * __this, t201* p0, t184  p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t20;
 void m20923 (t29 * __this, t201* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct t20;
 void m20931 (t29 * __this, t201* p0, int32_t p1, t2516 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m1735_MI;
 void m1735 (t163 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t163_TI));
		__this->f1 = (((t163_SFs*)InitializedTypeInfo(&t163_TI)->static_fields)->f4);
		return;
	}
}
extern MethodInfo m2903_MI;
 void m2903 (t163 * __this, int32_t p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if ((((int32_t)p0) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		t915 * L_0 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_0, (t7*) &_stringLiteral1163, &m3975_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0015:
	{
		__this->f1 = ((t201*)SZArrayNew(InitializedTypeInfo(&t201_TI), p0));
		return;
	}
}
extern MethodInfo m13257_MI;
 void m13257 (t29 * __this, MethodInfo* method){
	{
		((t163_SFs*)InitializedTypeInfo(&t163_TI)->static_fields)->f4 = ((t201*)SZArrayNew(InitializedTypeInfo(&t201_TI), 0));
		return;
	}
}
extern MethodInfo m13258_MI;
 t29* m13258 (t163 * __this, MethodInfo* method){
	{
		t2517  L_0 = m13283(__this, &m13283_MI);
		t2517  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2517_TI), &L_1);
		return (t29*)L_2;
	}
}
extern MethodInfo m13259_MI;
 void m13259 (t163 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t201* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_0, 0, p0, p1, L_1, &m5952_MI);
		return;
	}
}
extern MethodInfo m13260_MI;
 t29 * m13260 (t163 * __this, MethodInfo* method){
	{
		t2517  L_0 = m13283(__this, &m13283_MI);
		t2517  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t2517_TI), &L_1);
		return (t29 *)L_2;
	}
}
extern MethodInfo m13261_MI;
 int32_t m13261 (t163 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker1< t184  >::Invoke(&m1584_MI, __this, ((*(t184 *)((t184 *)UnBox (p0, InitializedTypeInfo(&t184_TI))))));
			int32_t L_0 = (__this->f2);
			V_0 = ((int32_t)(L_0-1));
			// IL_0015: leave.s IL_002a
			goto IL_002a;
		}

IL_0017:
		{
			// IL_0017: leave.s IL_001f
			goto IL_001f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0019;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_001c;
		throw e;
	}

IL_0019:
	{ // begin catch(System.NullReferenceException)
		// IL_001a: leave.s IL_001f
		goto IL_001f;
	} // end catch (depth: 1)

IL_001c:
	{ // begin catch(System.InvalidCastException)
		// IL_001d: leave.s IL_001f
		goto IL_001f;
	} // end catch (depth: 1)

IL_001f:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_002a:
	{
		return V_0;
	}
}
extern MethodInfo m13262_MI;
 bool m13262 (t163 * __this, t29 * p0, MethodInfo* method){
	bool V_0 = false;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			bool L_0 = (bool)VirtFuncInvoker1< bool, t184  >::Invoke(&m13278_MI, __this, ((*(t184 *)((t184 *)UnBox (p0, InitializedTypeInfo(&t184_TI))))));
			V_0 = L_0;
			// IL_000d: leave.s IL_0019
			goto IL_0019;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return 0;
	}

IL_0019:
	{
		return V_0;
	}
}
extern MethodInfo m13263_MI;
 int32_t m13263 (t163 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t184  >::Invoke(&m13284_MI, __this, ((*(t184 *)((t184 *)UnBox (p0, InitializedTypeInfo(&t184_TI))))));
			V_0 = L_0;
			// IL_000d: leave.s IL_0019
			goto IL_0019;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return (-1);
	}

IL_0019:
	{
		return V_0;
	}
}
extern MethodInfo m13264_MI;
 void m13264 (t163 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		m13286(__this, p0, &m13286_MI);
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker2< int32_t, t184  >::Invoke(&m13287_MI, __this, p0, ((*(t184 *)((t184 *)UnBox (p1, InitializedTypeInfo(&t184_TI))))));
			// IL_0014: leave.s IL_0029
			goto IL_0029;
		}

IL_0016:
		{
			// IL_0016: leave.s IL_001e
			goto IL_001e;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0018;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_001b;
		throw e;
	}

IL_0018:
	{ // begin catch(System.NullReferenceException)
		// IL_0019: leave.s IL_001e
		goto IL_001e;
	} // end catch (depth: 1)

IL_001b:
	{ // begin catch(System.InvalidCastException)
		// IL_001c: leave.s IL_001e
		goto IL_001e;
	} // end catch (depth: 1)

IL_001e:
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0029:
	{
		return;
	}
}
extern MethodInfo m13265_MI;
 void m13265 (t163 * __this, t29 * p0, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtFuncInvoker1< bool, t184  >::Invoke(&m13289_MI, __this, ((*(t184 *)((t184 *)UnBox (p0, InitializedTypeInfo(&t184_TI))))));
			// IL_000d: leave.s IL_0017
			goto IL_0017;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return;
	}
}
extern MethodInfo m13266_MI;
 bool m13266 (t163 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m13267_MI;
 bool m13267 (t163 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m13268_MI;
 t29 * m13268 (t163 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m13269_MI;
 bool m13269 (t163 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m13270_MI;
 bool m13270 (t163 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m13271_MI;
 t29 * m13271 (t163 * __this, int32_t p0, MethodInfo* method){
	{
		t184  L_0 = (t184 )VirtFuncInvoker1< t184 , int32_t >::Invoke(&m2044_MI, __this, p0);
		t184  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t184_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13272_MI;
 void m13272 (t163 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker2< int32_t, t184  >::Invoke(&m2045_MI, __this, p0, ((*(t184 *)((t184 *)UnBox (p1, InitializedTypeInfo(&t184_TI))))));
			// IL_000d: leave.s IL_0022
			goto IL_0022;
		}

IL_000f:
		{
			// IL_000f: leave.s IL_0017
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (t295 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&t585_TI, e.ex->object.klass))
			goto IL_0011;
		if(il2cpp_codegen_class_is_assignable_from (&t1649_TI, e.ex->object.klass))
			goto IL_0014;
		throw e;
	}

IL_0011:
	{ // begin catch(System.NullReferenceException)
		// IL_0012: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0014:
	{ // begin catch(System.InvalidCastException)
		// IL_0015: leave.s IL_0017
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		t305 * L_0 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_0, (t7*) &_stringLiteral401, &m1935_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0022:
	{
		return;
	}
}
 void m1584 (t163 * __this, t184  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f2);
		t201* L_1 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_0017;
		}
	}
	{
		m13273(__this, 1, &m13273_MI);
	}

IL_0017:
	{
		t201* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		int32_t L_4 = L_3;
		V_0 = L_4;
		__this->f2 = ((int32_t)(L_4+1));
		*((t184 *)(t184 *)SZArrayLdElema(L_2, V_0)) = (t184 )p0;
		int32_t L_5 = (__this->f3);
		__this->f3 = ((int32_t)(L_5+1));
		return;
	}
}
 void m13273 (t163 * __this, int32_t p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->f2);
		V_0 = ((int32_t)(L_0+p0));
		t201* L_1 = (__this->f1);
		if ((((int32_t)V_0) <= ((int32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = m1599(__this, &m1599_MI);
		int32_t L_3 = m5139(NULL, ((int32_t)((int32_t)L_2*(int32_t)2)), 4, &m5139_MI);
		int32_t L_4 = m5139(NULL, L_3, V_0, &m5139_MI);
		m1600(__this, L_4, &m1600_MI);
	}

IL_002e:
	{
		return;
	}
}
 void m13274 (t163 * __this, t29* p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1926_MI, p0);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		m13273(__this, V_0, &m13273_MI);
		t201* L_1 = (__this->f1);
		int32_t L_2 = (__this->f2);
		InterfaceActionInvoker2< t201*, int32_t >::Invoke(&m27811_MI, p0, L_1, L_2);
		int32_t L_3 = (__this->f2);
		__this->f2 = ((int32_t)(L_3+V_0));
		return;
	}
}
 void m13275 (t163 * __this, t29* p0, MethodInfo* method){
	t184  V_0 = {0};
	t29* V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	t295 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	t295 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		t29* L_0 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m27812_MI, p0);
		V_1 = L_0;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0017;
		}

IL_0009:
		{
			t184  L_1 = (t184 )InterfaceFuncInvoker0< t184  >::Invoke(&m27813_MI, V_1);
			V_0 = L_1;
			VirtActionInvoker1< t184  >::Invoke(&m1584_MI, __this, V_0);
		}

IL_0017:
		{
			bool L_2 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m3972_MI, V_1);
			if (L_2)
			{
				goto IL_0009;
			}
		}

IL_001f:
		{
			// IL_001f: leave.s IL_002c
			leaveInstructions[0] = 0x2C; // 1
			THROW_SENTINEL(IL_002c);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0021;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (t295 *)e.ex;
		goto IL_0021;
	}

IL_0021:
	{ // begin finally (depth: 1)
		{
			if (V_1)
			{
				goto IL_0025;
			}
		}

IL_0024:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x2C:
					goto IL_002c;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}

IL_0025:
		{
			InterfaceActionInvoker0::Invoke(&m1428_MI, V_1);
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x2C:
					goto IL_002c;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					t295 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_002c:
	{
		return;
	}
}
extern MethodInfo m13276_MI;
 void m13276 (t163 * __this, t29* p0, MethodInfo* method){
	t29* V_0 = {0};
	{
		m13288(__this, p0, &m13288_MI);
		V_0 = ((t29*)IsInst(p0, InitializedTypeInfo(&t404_TI)));
		if (!V_0)
		{
			goto IL_001a;
		}
	}
	{
		m13274(__this, V_0, &m13274_MI);
		goto IL_0021;
	}

IL_001a:
	{
		m13275(__this, p0, &m13275_MI);
	}

IL_0021:
	{
		int32_t L_0 = (__this->f3);
		__this->f3 = ((int32_t)(L_0+1));
		return;
	}
}
extern MethodInfo m13277_MI;
 t2514 * m13277 (t163 * __this, MethodInfo* method){
	{
		t2514 * L_0 = (t2514 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2514_TI));
		m13307(L_0, __this, &m13307_MI);
		return L_0;
	}
}
extern MethodInfo m1601_MI;
 void m1601 (t163 * __this, MethodInfo* method){
	{
		t201* L_0 = (__this->f1);
		t201* L_1 = (__this->f1);
		m5112(NULL, (t20 *)(t20 *)L_0, 0, (((int32_t)(((t20 *)L_1)->max_length))), &m5112_MI);
		__this->f2 = 0;
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
 bool m13278 (t163 * __this, t184  p0, MethodInfo* method){
	{
		t201* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		int32_t L_2 = m20921(NULL, L_0, p0, 0, L_1, &m20921_MI);
		return ((((int32_t)((((int32_t)L_2) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m13279_MI;
 void m13279 (t163 * __this, t201* p0, int32_t p1, MethodInfo* method){
	{
		t201* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_0, 0, (t20 *)(t20 *)p0, p1, L_1, &m5952_MI);
		return;
	}
}
extern MethodInfo m13280_MI;
 t184  m13280 (t163 * __this, t2515 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	t184  V_1 = {0};
	t184  G_B3_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t163_TI));
		m13281(NULL, p0, &m13281_MI);
		int32_t L_0 = (__this->f2);
		int32_t L_1 = m13282(__this, 0, L_0, p0, &m13282_MI);
		V_0 = L_1;
		if ((((int32_t)V_0) == ((int32_t)(-1))))
		{
			goto IL_0027;
		}
	}
	{
		t201* L_2 = (__this->f1);
		int32_t L_3 = V_0;
		G_B3_0 = (*(t184 *)(t184 *)SZArrayLdElema(L_2, L_3));
		goto IL_0030;
	}

IL_0027:
	{
		Initobj (&t184_TI, (&V_1));
		G_B3_0 = V_1;
	}

IL_0030:
	{
		return G_B3_0;
	}
}
 void m13281 (t29 * __this, t2515 * p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1033, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		return;
	}
}
 int32_t m13282 (t163 * __this, int32_t p0, int32_t p1, t2515 * p2, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = ((int32_t)(p0+p1));
		V_1 = p0;
		goto IL_0022;
	}

IL_0008:
	{
		t201* L_0 = (__this->f1);
		int32_t L_1 = V_1;
		bool L_2 = (bool)VirtFuncInvoker1< bool, t184  >::Invoke(&m13382_MI, p2, (*(t184 *)(t184 *)SZArrayLdElema(L_0, L_1)));
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return V_1;
	}

IL_001e:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0022:
	{
		if ((((int32_t)V_1) < ((int32_t)V_0)))
		{
			goto IL_0008;
		}
	}
	{
		return (-1);
	}
}
 t2517  m13283 (t163 * __this, MethodInfo* method){
	{
		t2517  L_0 = {0};
		m13301(&L_0, __this, &m13301_MI);
		return L_0;
	}
}
 int32_t m13284 (t163 * __this, t184  p0, MethodInfo* method){
	{
		t201* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		int32_t L_2 = m20921(NULL, L_0, p0, 0, L_1, &m20921_MI);
		return L_2;
	}
}
 void m13285 (t163 * __this, int32_t p0, int32_t p1, MethodInfo* method){
	{
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		p0 = ((int32_t)(p0-p1));
	}

IL_000b:
	{
		int32_t L_0 = (__this->f2);
		if ((((int32_t)p0) >= ((int32_t)L_0)))
		{
			goto IL_0031;
		}
	}
	{
		t201* L_1 = (__this->f1);
		t201* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		m5952(NULL, (t20 *)(t20 *)L_1, p0, (t20 *)(t20 *)L_2, ((int32_t)(p0+p1)), ((int32_t)(L_3-p0)), &m5952_MI);
	}

IL_0031:
	{
		int32_t L_4 = (__this->f2);
		__this->f2 = ((int32_t)(L_4+p1));
		if ((((int32_t)p1) >= ((int32_t)0)))
		{
			goto IL_0056;
		}
	}
	{
		t201* L_5 = (__this->f1);
		int32_t L_6 = (__this->f2);
		m5112(NULL, (t20 *)(t20 *)L_5, L_6, ((-p1)), &m5112_MI);
	}

IL_0056:
	{
		return;
	}
}
 void m13286 (t163 * __this, int32_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) <= ((uint32_t)L_0)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0018:
	{
		return;
	}
}
 void m13287 (t163 * __this, int32_t p0, t184  p1, MethodInfo* method){
	{
		m13286(__this, p0, &m13286_MI);
		int32_t L_0 = (__this->f2);
		t201* L_1 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)(((int32_t)(((t20 *)L_1)->max_length))))))
		{
			goto IL_001e;
		}
	}
	{
		m13273(__this, 1, &m13273_MI);
	}

IL_001e:
	{
		m13285(__this, p0, 1, &m13285_MI);
		t201* L_2 = (__this->f1);
		*((t184 *)(t184 *)SZArrayLdElema(L_2, p0)) = (t184 )p1;
		int32_t L_3 = (__this->f3);
		__this->f3 = ((int32_t)(L_3+1));
		return;
	}
}
 void m13288 (t163 * __this, t29* p0, MethodInfo* method){
	{
		if (p0)
		{
			goto IL_000e;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1177, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_000e:
	{
		return;
	}
}
 bool m13289 (t163 * __this, t184  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t184  >::Invoke(&m13284_MI, __this, p0);
		V_0 = L_0;
		if ((((int32_t)V_0) == ((int32_t)(-1))))
		{
			goto IL_0013;
		}
	}
	{
		VirtActionInvoker1< int32_t >::Invoke(&m13291_MI, __this, V_0);
	}

IL_0013:
	{
		return ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern MethodInfo m13290_MI;
 int32_t m13290 (t163 * __this, t2515 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t163_TI));
		m13281(NULL, p0, &m13281_MI);
		V_0 = 0;
		V_1 = 0;
		V_0 = 0;
		goto IL_0028;
	}

IL_000e:
	{
		t201* L_0 = (__this->f1);
		int32_t L_1 = V_0;
		bool L_2 = (bool)VirtFuncInvoker1< bool, t184  >::Invoke(&m13382_MI, p0, (*(t184 *)(t184 *)SZArrayLdElema(L_0, L_1)));
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		goto IL_0031;
	}

IL_0024:
	{
		V_0 = ((int32_t)(V_0+1));
	}

IL_0028:
	{
		int32_t L_3 = (__this->f2);
		if ((((int32_t)V_0) < ((int32_t)L_3)))
		{
			goto IL_000e;
		}
	}

IL_0031:
	{
		int32_t L_4 = (__this->f2);
		if ((((uint32_t)V_0) != ((uint32_t)L_4)))
		{
			goto IL_003c;
		}
	}
	{
		return 0;
	}

IL_003c:
	{
		int32_t L_5 = (__this->f3);
		__this->f3 = ((int32_t)(L_5+1));
		V_1 = ((int32_t)(V_0+1));
		goto IL_0084;
	}

IL_0050:
	{
		t201* L_6 = (__this->f1);
		int32_t L_7 = V_1;
		bool L_8 = (bool)VirtFuncInvoker1< bool, t184  >::Invoke(&m13382_MI, p0, (*(t184 *)(t184 *)SZArrayLdElema(L_6, L_7)));
		if (L_8)
		{
			goto IL_0080;
		}
	}
	{
		t201* L_9 = (__this->f1);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)(L_10+1));
		t201* L_11 = (__this->f1);
		int32_t L_12 = V_1;
		*((t184 *)(t184 *)SZArrayLdElema(L_9, L_10)) = (t184 )(*(t184 *)(t184 *)SZArrayLdElema(L_11, L_12));
	}

IL_0080:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_0084:
	{
		int32_t L_13 = (__this->f2);
		if ((((int32_t)V_1) < ((int32_t)L_13)))
		{
			goto IL_0050;
		}
	}
	{
		if ((((int32_t)((int32_t)(V_1-V_0))) <= ((int32_t)0)))
		{
			goto IL_00a2;
		}
	}
	{
		t201* L_14 = (__this->f1);
		m5112(NULL, (t20 *)(t20 *)L_14, V_0, ((int32_t)(V_1-V_0)), &m5112_MI);
	}

IL_00a2:
	{
		__this->f2 = V_0;
		return ((int32_t)(V_1-V_0));
	}
}
 void m13291 (t163 * __this, int32_t p0, MethodInfo* method){
	{
		if ((((int32_t)p0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0018:
	{
		m13285(__this, p0, (-1), &m13285_MI);
		t201* L_2 = (__this->f1);
		int32_t L_3 = (__this->f2);
		m5112(NULL, (t20 *)(t20 *)L_2, L_3, 1, &m5112_MI);
		int32_t L_4 = (__this->f3);
		__this->f3 = ((int32_t)(L_4+1));
		return;
	}
}
extern MethodInfo m13292_MI;
 void m13292 (t163 * __this, MethodInfo* method){
	{
		t201* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m5171(NULL, (t20 *)(t20 *)L_0, 0, L_1, &m5171_MI);
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
extern MethodInfo m13293_MI;
 void m13293 (t163 * __this, MethodInfo* method){
	{
		t201* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2522_TI));
		t2522 * L_2 = m13388(NULL, &m13388_MI);
		m20923(NULL, L_0, 0, L_1, L_2, &m20923_MI);
		int32_t L_3 = (__this->f3);
		__this->f3 = ((int32_t)(L_3+1));
		return;
	}
}
extern MethodInfo m13294_MI;
 void m13294 (t163 * __this, t2516 * p0, MethodInfo* method){
	{
		t201* L_0 = (__this->f1);
		int32_t L_1 = (__this->f2);
		m20931(NULL, L_0, L_1, p0, &m20931_MI);
		int32_t L_2 = (__this->f3);
		__this->f3 = ((int32_t)(L_2+1));
		return;
	}
}
extern MethodInfo m1787_MI;
 t201* m1787 (t163 * __this, MethodInfo* method){
	t201* V_0 = {0};
	{
		int32_t L_0 = (__this->f2);
		V_0 = ((t201*)SZArrayNew(InitializedTypeInfo(&t201_TI), L_0));
		t201* L_1 = (__this->f1);
		int32_t L_2 = (__this->f2);
		m5951(NULL, (t20 *)(t20 *)L_1, (t20 *)(t20 *)V_0, L_2, &m5951_MI);
		return V_0;
	}
}
extern MethodInfo m13295_MI;
 void m13295 (t163 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		m1600(__this, L_0, &m1600_MI);
		return;
	}
}
 int32_t m1599 (t163 * __this, MethodInfo* method){
	{
		t201* L_0 = (__this->f1);
		return (((int32_t)(((t20 *)L_0)->max_length)));
	}
}
 void m1600 (t163 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) >= ((uint32_t)L_0)))
		{
			goto IL_000f;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m4198(L_1, &m4198_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		t201** L_2 = &(__this->f1);
		m20919(NULL, L_2, p0, &m20919_MI);
		return;
	}
}
extern MethodInfo m1786_MI;
 int32_t m1786 (t163 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		return L_0;
	}
}
 t184  m2044 (t163 * __this, int32_t p0, MethodInfo* method){
	{
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) < ((uint32_t)L_0)))
		{
			goto IL_0014;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		t201* L_2 = (__this->f1);
		int32_t L_3 = p0;
		return (*(t184 *)(t184 *)SZArrayLdElema(L_2, L_3));
	}
}
 void m2045 (t163 * __this, int32_t p0, t184  p1, MethodInfo* method){
	{
		m13286(__this, p0, &m13286_MI);
		int32_t L_0 = (__this->f2);
		if ((((uint32_t)p0) != ((uint32_t)L_0)))
		{
			goto IL_001b;
		}
	}
	{
		t915 * L_1 = (t915 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t915_TI));
		m3975(L_1, (t7*) &_stringLiteral199, &m3975_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001b:
	{
		t201* L_2 = (__this->f1);
		*((t184 *)(t184 *)SZArrayLdElema(L_2, p0)) = (t184 )p1;
		return;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.UIVertex>
extern Il2CppType t44_0_0_32849;
FieldInfo t163_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t163_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t201_0_0_1;
FieldInfo t163_f1_FieldInfo = 
{
	"_items", &t201_0_0_1, &t163_TI, offsetof(t163, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t163_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t163_TI, offsetof(t163, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t163_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t163_TI, offsetof(t163, f3), &EmptyCustomAttributesCache};
extern Il2CppType t201_0_0_49;
FieldInfo t163_f4_FieldInfo = 
{
	"EmptyArray", &t201_0_0_49, &t163_TI, offsetof(t163_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t163_FIs[] =
{
	&t163_f0_FieldInfo,
	&t163_f1_FieldInfo,
	&t163_f2_FieldInfo,
	&t163_f3_FieldInfo,
	&t163_f4_FieldInfo,
	NULL
};
static const int32_t t163_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t163_f0_DefaultValue = 
{
	&t163_f0_FieldInfo, { (char*)&t163_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t163_FDVs[] = 
{
	&t163_f0_DefaultValue,
	NULL
};
static PropertyInfo t163____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t163_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m13266_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t163____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t163_TI, "System.Collections.ICollection.IsSynchronized", &m13267_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t163____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t163_TI, "System.Collections.ICollection.SyncRoot", &m13268_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t163____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t163_TI, "System.Collections.IList.IsFixedSize", &m13269_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t163____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t163_TI, "System.Collections.IList.IsReadOnly", &m13270_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t163____System_Collections_IList_Item_PropertyInfo = 
{
	&t163_TI, "System.Collections.IList.Item", &m13271_MI, &m13272_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t163____Capacity_PropertyInfo = 
{
	&t163_TI, "Capacity", &m1599_MI, &m1600_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t163____Count_PropertyInfo = 
{
	&t163_TI, "Count", &m1786_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t163____Item_PropertyInfo = 
{
	&t163_TI, "Item", &m2044_MI, &m2045_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t163_PIs[] =
{
	&t163____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t163____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t163____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t163____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t163____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t163____System_Collections_IList_Item_PropertyInfo,
	&t163____Capacity_PropertyInfo,
	&t163____Count_PropertyInfo,
	&t163____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1735_GM;
MethodInfo m1735_MI = 
{
	".ctor", (methodPointerType)&m1735, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1735_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t163_m2903_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2903_GM;
MethodInfo m2903_MI = 
{
	".ctor", (methodPointerType)&m2903, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t163_m2903_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2903_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13257_GM;
MethodInfo m13257_MI = 
{
	".cctor", (methodPointerType)&m13257, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13257_GM};
extern Il2CppType t2512_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13258_GM;
MethodInfo m13258_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m13258, &t163_TI, &t2512_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13258_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t163_m13259_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13259_GM;
MethodInfo m13259_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m13259, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t163_m13259_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13259_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13260_GM;
MethodInfo m13260_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m13260, &t163_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13260_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t163_m13261_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13261_GM;
MethodInfo m13261_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m13261, &t163_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t163_m13261_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13261_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t163_m13262_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13262_GM;
MethodInfo m13262_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m13262, &t163_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t163_m13262_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13262_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t163_m13263_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13263_GM;
MethodInfo m13263_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m13263, &t163_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t163_m13263_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13263_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t163_m13264_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13264_GM;
MethodInfo m13264_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m13264, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t163_m13264_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13264_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t163_m13265_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13265_GM;
MethodInfo m13265_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m13265, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t163_m13265_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13265_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13266_GM;
MethodInfo m13266_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m13266, &t163_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13266_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13267_GM;
MethodInfo m13267_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m13267, &t163_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13267_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13268_GM;
MethodInfo m13268_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m13268, &t163_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13268_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13269_GM;
MethodInfo m13269_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m13269, &t163_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13269_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13270_GM;
MethodInfo m13270_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m13270, &t163_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13270_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t163_m13271_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13271_GM;
MethodInfo m13271_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m13271, &t163_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t163_m13271_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13271_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t163_m13272_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13272_GM;
MethodInfo m13272_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m13272, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t163_m13272_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13272_GM};
extern Il2CppType t184_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t163_m1584_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1584_GM;
MethodInfo m1584_MI = 
{
	"Add", (methodPointerType)&m1584, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t184, t163_m1584_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1584_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t163_m13273_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13273_GM;
MethodInfo m13273_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m13273, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t163_m13273_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13273_GM};
extern Il2CppType t404_0_0_0;
extern Il2CppType t404_0_0_0;
static ParameterInfo t163_m13274_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t404_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13274_GM;
MethodInfo m13274_MI = 
{
	"AddCollection", (methodPointerType)&m13274, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t163_m13274_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13274_GM};
extern Il2CppType t2513_0_0_0;
extern Il2CppType t2513_0_0_0;
static ParameterInfo t163_m13275_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2513_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13275_GM;
MethodInfo m13275_MI = 
{
	"AddEnumerable", (methodPointerType)&m13275, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t163_m13275_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13275_GM};
extern Il2CppType t2513_0_0_0;
static ParameterInfo t163_m13276_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2513_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13276_GM;
MethodInfo m13276_MI = 
{
	"AddRange", (methodPointerType)&m13276, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t163_m13276_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13276_GM};
extern Il2CppType t2514_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13277_GM;
MethodInfo m13277_MI = 
{
	"AsReadOnly", (methodPointerType)&m13277, &t163_TI, &t2514_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13277_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1601_GM;
MethodInfo m1601_MI = 
{
	"Clear", (methodPointerType)&m1601, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1601_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t163_m13278_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13278_GM;
MethodInfo m13278_MI = 
{
	"Contains", (methodPointerType)&m13278, &t163_TI, &t40_0_0_0, RuntimeInvoker_t40_t184, t163_m13278_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13278_GM};
extern Il2CppType t201_0_0_0;
extern Il2CppType t201_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t163_m13279_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t201_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13279_GM;
MethodInfo m13279_MI = 
{
	"CopyTo", (methodPointerType)&m13279, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t163_m13279_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13279_GM};
extern Il2CppType t2515_0_0_0;
extern Il2CppType t2515_0_0_0;
static ParameterInfo t163_m13280_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2515_0_0_0},
};
extern Il2CppType t184_0_0_0;
extern void* RuntimeInvoker_t184_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13280_GM;
MethodInfo m13280_MI = 
{
	"Find", (methodPointerType)&m13280, &t163_TI, &t184_0_0_0, RuntimeInvoker_t184_t29, t163_m13280_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13280_GM};
extern Il2CppType t2515_0_0_0;
static ParameterInfo t163_m13281_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2515_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13281_GM;
MethodInfo m13281_MI = 
{
	"CheckMatch", (methodPointerType)&m13281, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t163_m13281_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13281_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2515_0_0_0;
static ParameterInfo t163_m13282_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2515_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13282_GM;
MethodInfo m13282_MI = 
{
	"GetIndex", (methodPointerType)&m13282, &t163_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t163_m13282_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13282_GM};
extern Il2CppType t2517_0_0_0;
extern void* RuntimeInvoker_t2517 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13283_GM;
MethodInfo m13283_MI = 
{
	"GetEnumerator", (methodPointerType)&m13283, &t163_TI, &t2517_0_0_0, RuntimeInvoker_t2517, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13283_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t163_m13284_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13284_GM;
MethodInfo m13284_MI = 
{
	"IndexOf", (methodPointerType)&m13284, &t163_TI, &t44_0_0_0, RuntimeInvoker_t44_t184, t163_m13284_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13284_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t163_m13285_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13285_GM;
MethodInfo m13285_MI = 
{
	"Shift", (methodPointerType)&m13285, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t163_m13285_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13285_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t163_m13286_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13286_GM;
MethodInfo m13286_MI = 
{
	"CheckIndex", (methodPointerType)&m13286, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t163_m13286_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13286_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t163_m13287_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13287_GM;
MethodInfo m13287_MI = 
{
	"Insert", (methodPointerType)&m13287, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t184, t163_m13287_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13287_GM};
extern Il2CppType t2513_0_0_0;
static ParameterInfo t163_m13288_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2513_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13288_GM;
MethodInfo m13288_MI = 
{
	"CheckCollection", (methodPointerType)&m13288, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t163_m13288_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13288_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t163_m13289_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13289_GM;
MethodInfo m13289_MI = 
{
	"Remove", (methodPointerType)&m13289, &t163_TI, &t40_0_0_0, RuntimeInvoker_t40_t184, t163_m13289_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13289_GM};
extern Il2CppType t2515_0_0_0;
static ParameterInfo t163_m13290_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2515_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13290_GM;
MethodInfo m13290_MI = 
{
	"RemoveAll", (methodPointerType)&m13290, &t163_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t163_m13290_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13290_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t163_m13291_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13291_GM;
MethodInfo m13291_MI = 
{
	"RemoveAt", (methodPointerType)&m13291, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t163_m13291_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13291_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13292_GM;
MethodInfo m13292_MI = 
{
	"Reverse", (methodPointerType)&m13292, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13292_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13293_GM;
MethodInfo m13293_MI = 
{
	"Sort", (methodPointerType)&m13293, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13293_GM};
extern Il2CppType t2516_0_0_0;
extern Il2CppType t2516_0_0_0;
static ParameterInfo t163_m13294_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2516_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13294_GM;
MethodInfo m13294_MI = 
{
	"Sort", (methodPointerType)&m13294, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t163_m13294_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13294_GM};
extern Il2CppType t201_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1787_GM;
MethodInfo m1787_MI = 
{
	"ToArray", (methodPointerType)&m1787, &t163_TI, &t201_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1787_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13295_GM;
MethodInfo m13295_MI = 
{
	"TrimExcess", (methodPointerType)&m13295, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13295_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1599_GM;
MethodInfo m1599_MI = 
{
	"get_Capacity", (methodPointerType)&m1599, &t163_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1599_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t163_m1600_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1600_GM;
MethodInfo m1600_MI = 
{
	"set_Capacity", (methodPointerType)&m1600, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t163_m1600_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1600_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1786_GM;
MethodInfo m1786_MI = 
{
	"get_Count", (methodPointerType)&m1786, &t163_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1786_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t163_m2044_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t184_0_0_0;
extern void* RuntimeInvoker_t184_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2044_GM;
MethodInfo m2044_MI = 
{
	"get_Item", (methodPointerType)&m2044, &t163_TI, &t184_0_0_0, RuntimeInvoker_t184_t44, t163_m2044_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m2044_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t163_m2045_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2045_GM;
MethodInfo m2045_MI = 
{
	"set_Item", (methodPointerType)&m2045, &t163_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t184, t163_m2045_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m2045_GM};
static MethodInfo* t163_MIs[] =
{
	&m1735_MI,
	&m2903_MI,
	&m13257_MI,
	&m13258_MI,
	&m13259_MI,
	&m13260_MI,
	&m13261_MI,
	&m13262_MI,
	&m13263_MI,
	&m13264_MI,
	&m13265_MI,
	&m13266_MI,
	&m13267_MI,
	&m13268_MI,
	&m13269_MI,
	&m13270_MI,
	&m13271_MI,
	&m13272_MI,
	&m1584_MI,
	&m13273_MI,
	&m13274_MI,
	&m13275_MI,
	&m13276_MI,
	&m13277_MI,
	&m1601_MI,
	&m13278_MI,
	&m13279_MI,
	&m13280_MI,
	&m13281_MI,
	&m13282_MI,
	&m13283_MI,
	&m13284_MI,
	&m13285_MI,
	&m13286_MI,
	&m13287_MI,
	&m13288_MI,
	&m13289_MI,
	&m13290_MI,
	&m13291_MI,
	&m13292_MI,
	&m13293_MI,
	&m13294_MI,
	&m1787_MI,
	&m13295_MI,
	&m1599_MI,
	&m1600_MI,
	&m1786_MI,
	&m2044_MI,
	&m2045_MI,
	NULL
};
static MethodInfo* t163_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13260_MI,
	&m1786_MI,
	&m13267_MI,
	&m13268_MI,
	&m13259_MI,
	&m13269_MI,
	&m13270_MI,
	&m13271_MI,
	&m13272_MI,
	&m13261_MI,
	&m1601_MI,
	&m13262_MI,
	&m13263_MI,
	&m13264_MI,
	&m13265_MI,
	&m13291_MI,
	&m1786_MI,
	&m13266_MI,
	&m1584_MI,
	&m1601_MI,
	&m13278_MI,
	&m13279_MI,
	&m13289_MI,
	&m13258_MI,
	&m13284_MI,
	&m13287_MI,
	&m13291_MI,
	&m2044_MI,
	&m2045_MI,
};
extern TypeInfo t403_TI;
static TypeInfo* t163_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t404_TI,
	&t2513_TI,
	&t403_TI,
};
static Il2CppInterfaceOffsetPair t163_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t404_TI, 20},
	{ &t2513_TI, 27},
	{ &t403_TI, 28},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t163_1_0_0;
struct t163;
extern Il2CppGenericClass t163_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t163_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t163_MIs, t163_PIs, t163_FIs, NULL, &t29_TI, NULL, NULL, &t163_TI, t163_ITIs, t163_VT, &t1261__CustomAttributeCache, &t163_TI, &t163_0_0_0, &t163_1_0_0, t163_IOs, &t163_GC, NULL, t163_FDVs, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t163), 0, -1, sizeof(t163_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>
static PropertyInfo t404____Count_PropertyInfo = 
{
	&t404_TI, "Count", &m1926_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27814_MI;
static PropertyInfo t404____IsReadOnly_PropertyInfo = 
{
	&t404_TI, "IsReadOnly", &m27814_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t404_PIs[] =
{
	&t404____Count_PropertyInfo,
	&t404____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1926_GM;
MethodInfo m1926_MI = 
{
	"get_Count", NULL, &t404_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1926_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27814_GM;
MethodInfo m27814_MI = 
{
	"get_IsReadOnly", NULL, &t404_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27814_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t404_m27815_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27815_GM;
MethodInfo m27815_MI = 
{
	"Add", NULL, &t404_TI, &t21_0_0_0, RuntimeInvoker_t21_t184, t404_m27815_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27815_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27816_GM;
MethodInfo m27816_MI = 
{
	"Clear", NULL, &t404_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27816_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t404_m27817_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27817_GM;
MethodInfo m27817_MI = 
{
	"Contains", NULL, &t404_TI, &t40_0_0_0, RuntimeInvoker_t40_t184, t404_m27817_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27817_GM};
extern Il2CppType t201_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t404_m27811_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t201_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27811_GM;
MethodInfo m27811_MI = 
{
	"CopyTo", NULL, &t404_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t404_m27811_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27811_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t404_m27818_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27818_GM;
MethodInfo m27818_MI = 
{
	"Remove", NULL, &t404_TI, &t40_0_0_0, RuntimeInvoker_t40_t184, t404_m27818_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27818_GM};
static MethodInfo* t404_MIs[] =
{
	&m1926_MI,
	&m27814_MI,
	&m27815_MI,
	&m27816_MI,
	&m27817_MI,
	&m27811_MI,
	&m27818_MI,
	NULL
};
static TypeInfo* t404_ITIs[] = 
{
	&t603_TI,
	&t2513_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t404_1_0_0;
struct t404;
extern Il2CppGenericClass t404_GC;
TypeInfo t404_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t404_MIs, t404_PIs, NULL, NULL, NULL, NULL, NULL, &t404_TI, t404_ITIs, NULL, &EmptyCustomAttributesCache, &t404_TI, &t404_0_0_0, &t404_1_0_0, NULL, &t404_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex>
extern Il2CppType t2512_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27812_GM;
MethodInfo m27812_MI = 
{
	"GetEnumerator", NULL, &t2513_TI, &t2512_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27812_GM};
static MethodInfo* t2513_MIs[] =
{
	&m27812_MI,
	NULL
};
static TypeInfo* t2513_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2513_1_0_0;
struct t2513;
extern Il2CppGenericClass t2513_GC;
TypeInfo t2513_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2513_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2513_TI, t2513_ITIs, NULL, &EmptyCustomAttributesCache, &t2513_TI, &t2513_0_0_0, &t2513_1_0_0, NULL, &t2513_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
static PropertyInfo t2512____Current_PropertyInfo = 
{
	&t2512_TI, "Current", &m27813_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2512_PIs[] =
{
	&t2512____Current_PropertyInfo,
	NULL
};
extern Il2CppType t184_0_0_0;
extern void* RuntimeInvoker_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27813_GM;
MethodInfo m27813_MI = 
{
	"get_Current", NULL, &t2512_TI, &t184_0_0_0, RuntimeInvoker_t184, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27813_GM};
static MethodInfo* t2512_MIs[] =
{
	&m27813_MI,
	NULL
};
static TypeInfo* t2512_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2512_0_0_0;
extern Il2CppType t2512_1_0_0;
struct t2512;
extern Il2CppGenericClass t2512_GC;
TypeInfo t2512_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2512_MIs, t2512_PIs, NULL, NULL, NULL, NULL, NULL, &t2512_TI, t2512_ITIs, NULL, &EmptyCustomAttributesCache, &t2512_TI, &t2512_0_0_0, &t2512_1_0_0, NULL, &t2512_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2518.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2518_TI;
#include "t2518MD.h"

extern MethodInfo m13300_MI;
extern MethodInfo m20908_MI;
struct t20;
 t184  m20908 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m13296_MI;
 void m13296 (t2518 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13297_MI;
 t29 * m13297 (t2518 * __this, MethodInfo* method){
	{
		t184  L_0 = m13300(__this, &m13300_MI);
		t184  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t184_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13298_MI;
 void m13298 (t2518 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13299_MI;
 bool m13299 (t2518 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t184  m13300 (t2518 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t184  L_8 = m20908(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m20908_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UIVertex>
extern Il2CppType t20_0_0_1;
FieldInfo t2518_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2518_TI, offsetof(t2518, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2518_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2518_TI, offsetof(t2518, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2518_FIs[] =
{
	&t2518_f0_FieldInfo,
	&t2518_f1_FieldInfo,
	NULL
};
static PropertyInfo t2518____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2518_TI, "System.Collections.IEnumerator.Current", &m13297_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2518____Current_PropertyInfo = 
{
	&t2518_TI, "Current", &m13300_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2518_PIs[] =
{
	&t2518____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2518____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2518_m13296_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13296_GM;
MethodInfo m13296_MI = 
{
	".ctor", (methodPointerType)&m13296, &t2518_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2518_m13296_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13296_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13297_GM;
MethodInfo m13297_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13297, &t2518_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13297_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13298_GM;
MethodInfo m13298_MI = 
{
	"Dispose", (methodPointerType)&m13298, &t2518_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13298_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13299_GM;
MethodInfo m13299_MI = 
{
	"MoveNext", (methodPointerType)&m13299, &t2518_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13299_GM};
extern Il2CppType t184_0_0_0;
extern void* RuntimeInvoker_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13300_GM;
MethodInfo m13300_MI = 
{
	"get_Current", (methodPointerType)&m13300, &t2518_TI, &t184_0_0_0, RuntimeInvoker_t184, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13300_GM};
static MethodInfo* t2518_MIs[] =
{
	&m13296_MI,
	&m13297_MI,
	&m13298_MI,
	&m13299_MI,
	&m13300_MI,
	NULL
};
static MethodInfo* t2518_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13297_MI,
	&m13299_MI,
	&m13298_MI,
	&m13300_MI,
};
static TypeInfo* t2518_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2512_TI,
};
static Il2CppInterfaceOffsetPair t2518_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2512_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2518_0_0_0;
extern Il2CppType t2518_1_0_0;
extern Il2CppGenericClass t2518_GC;
TypeInfo t2518_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2518_MIs, t2518_PIs, t2518_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2518_TI, t2518_ITIs, t2518_VT, &EmptyCustomAttributesCache, &t2518_TI, &t2518_0_0_0, &t2518_1_0_0, t2518_IOs, &t2518_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2518)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UIVertex>
extern MethodInfo m1924_MI;
extern MethodInfo m27819_MI;
static PropertyInfo t403____Item_PropertyInfo = 
{
	&t403_TI, "Item", &m1924_MI, &m27819_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t403_PIs[] =
{
	&t403____Item_PropertyInfo,
	NULL
};
extern Il2CppType t184_0_0_0;
static ParameterInfo t403_m27820_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27820_GM;
MethodInfo m27820_MI = 
{
	"IndexOf", NULL, &t403_TI, &t44_0_0_0, RuntimeInvoker_t44_t184, t403_m27820_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27820_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t403_m27821_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27821_GM;
MethodInfo m27821_MI = 
{
	"Insert", NULL, &t403_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t184, t403_m27821_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27821_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t403_m27822_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27822_GM;
MethodInfo m27822_MI = 
{
	"RemoveAt", NULL, &t403_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t403_m27822_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27822_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t403_m1924_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t184_0_0_0;
extern void* RuntimeInvoker_t184_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1924_GM;
MethodInfo m1924_MI = 
{
	"get_Item", NULL, &t403_TI, &t184_0_0_0, RuntimeInvoker_t184_t44, t403_m1924_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1924_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t403_m27819_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27819_GM;
MethodInfo m27819_MI = 
{
	"set_Item", NULL, &t403_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t184, t403_m27819_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27819_GM};
static MethodInfo* t403_MIs[] =
{
	&m27820_MI,
	&m27821_MI,
	&m27822_MI,
	&m1924_MI,
	&m27819_MI,
	NULL
};
static TypeInfo* t403_ITIs[] = 
{
	&t603_TI,
	&t404_TI,
	&t2513_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t403_0_0_0;
extern Il2CppType t403_1_0_0;
struct t403;
extern Il2CppGenericClass t403_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t403_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t403_MIs, t403_PIs, NULL, NULL, NULL, NULL, NULL, &t403_TI, t403_ITIs, NULL, &t1908__CustomAttributeCache, &t403_TI, &t403_0_0_0, &t403_1_0_0, NULL, &t403_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13304_MI;


 void m13301 (t2517 * __this, t163 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		int32_t L_0 = (p0->f3);
		__this->f2 = L_0;
		return;
	}
}
extern MethodInfo m13302_MI;
 t29 * m13302 (t2517 * __this, MethodInfo* method){
	{
		m13304(__this, &m13304_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3974(L_1, &m3974_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		t184  L_2 = (__this->f3);
		t184  L_3 = L_2;
		t29 * L_4 = Box(InitializedTypeInfo(&t184_TI), &L_3);
		return L_4;
	}
}
extern MethodInfo m13303_MI;
 void m13303 (t2517 * __this, MethodInfo* method){
	{
		__this->f0 = (t163 *)NULL;
		return;
	}
}
 void m13304 (t2517 * __this, MethodInfo* method){
	{
		t163 * L_0 = (__this->f0);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		t2517  L_1 = (*(t2517 *)__this);
		t29 * L_2 = Box(InitializedTypeInfo(&t2517_TI), &L_1);
		t42 * L_3 = m1430(L_2, &m1430_MI);
		t7* L_4 = (t7*)VirtFuncInvoker0< t7* >::Invoke(&m2998_MI, L_3);
		t1101 * L_5 = (t1101 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t1101_TI));
		m5150(L_5, L_4, &m5150_MI);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0023:
	{
		int32_t L_6 = (__this->f2);
		t163 * L_7 = (__this->f0);
		int32_t L_8 = (L_7->f3);
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0041;
		}
	}
	{
		t914 * L_9 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_9, (t7*) &_stringLiteral1178, &m3964_MI);
		il2cpp_codegen_raise_exception(L_9);
	}

IL_0041:
	{
		return;
	}
}
extern MethodInfo m13305_MI;
 bool m13305 (t2517 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	{
		m13304(__this, &m13304_MI);
		int32_t L_0 = (__this->f1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		int32_t L_1 = (__this->f1);
		t163 * L_2 = (__this->f0);
		int32_t L_3 = (L_2->f2);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_004d;
		}
	}
	{
		t163 * L_4 = (__this->f0);
		t201* L_5 = (L_4->f1);
		int32_t L_6 = (__this->f1);
		int32_t L_7 = L_6;
		V_0 = L_7;
		__this->f1 = ((int32_t)(L_7+1));
		int32_t L_8 = V_0;
		__this->f3 = (*(t184 *)(t184 *)SZArrayLdElema(L_5, L_8));
		return 1;
	}

IL_004d:
	{
		__this->f1 = (-1);
		return 0;
	}
}
extern MethodInfo m13306_MI;
 t184  m13306 (t2517 * __this, MethodInfo* method){
	{
		t184  L_0 = (__this->f3);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>
extern Il2CppType t163_0_0_1;
FieldInfo t2517_f0_FieldInfo = 
{
	"l", &t163_0_0_1, &t2517_TI, offsetof(t2517, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2517_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2517_TI, offsetof(t2517, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2517_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2517_TI, offsetof(t2517, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t184_0_0_1;
FieldInfo t2517_f3_FieldInfo = 
{
	"current", &t184_0_0_1, &t2517_TI, offsetof(t2517, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2517_FIs[] =
{
	&t2517_f0_FieldInfo,
	&t2517_f1_FieldInfo,
	&t2517_f2_FieldInfo,
	&t2517_f3_FieldInfo,
	NULL
};
static PropertyInfo t2517____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2517_TI, "System.Collections.IEnumerator.Current", &m13302_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2517____Current_PropertyInfo = 
{
	&t2517_TI, "Current", &m13306_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2517_PIs[] =
{
	&t2517____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2517____Current_PropertyInfo,
	NULL
};
extern Il2CppType t163_0_0_0;
static ParameterInfo t2517_m13301_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13301_GM;
MethodInfo m13301_MI = 
{
	".ctor", (methodPointerType)&m13301, &t2517_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2517_m13301_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13301_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13302_GM;
MethodInfo m13302_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13302, &t2517_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13302_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13303_GM;
MethodInfo m13303_MI = 
{
	"Dispose", (methodPointerType)&m13303, &t2517_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13303_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13304_GM;
MethodInfo m13304_MI = 
{
	"VerifyState", (methodPointerType)&m13304, &t2517_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13304_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13305_GM;
MethodInfo m13305_MI = 
{
	"MoveNext", (methodPointerType)&m13305, &t2517_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13305_GM};
extern Il2CppType t184_0_0_0;
extern void* RuntimeInvoker_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13306_GM;
MethodInfo m13306_MI = 
{
	"get_Current", (methodPointerType)&m13306, &t2517_TI, &t184_0_0_0, RuntimeInvoker_t184, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13306_GM};
static MethodInfo* t2517_MIs[] =
{
	&m13301_MI,
	&m13302_MI,
	&m13303_MI,
	&m13304_MI,
	&m13305_MI,
	&m13306_MI,
	NULL
};
static MethodInfo* t2517_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13302_MI,
	&m13305_MI,
	&m13303_MI,
	&m13306_MI,
};
static TypeInfo* t2517_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2512_TI,
};
static Il2CppInterfaceOffsetPair t2517_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2512_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2517_0_0_0;
extern Il2CppType t2517_1_0_0;
extern Il2CppGenericClass t2517_GC;
TypeInfo t2517_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2517_MIs, t2517_PIs, t2517_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2517_TI, t2517_ITIs, t2517_VT, &EmptyCustomAttributesCache, &t2517_TI, &t2517_0_0_0, &t2517_1_0_0, t2517_IOs, &t2517_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2517)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t2519MD.h"
extern MethodInfo m13336_MI;
extern MethodInfo m13368_MI;
extern MethodInfo m27817_MI;
extern MethodInfo m27820_MI;


 void m13307 (t2514 * __this, t29* p0, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		if (p0)
		{
			goto IL_0014;
		}
	}
	{
		t338 * L_0 = (t338 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t338_TI));
		m2950(L_0, (t7*) &_stringLiteral1179, &m2950_MI);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m13308_MI;
 void m13308 (t2514 * __this, t184  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13309_MI;
 void m13309 (t2514 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13310_MI;
 void m13310 (t2514 * __this, int32_t p0, t184  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13311_MI;
 bool m13311 (t2514 * __this, t184  p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13312_MI;
 void m13312 (t2514 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13313_MI;
 t184  m13313 (t2514 * __this, int32_t p0, MethodInfo* method){
	{
		t184  L_0 = (t184 )VirtFuncInvoker1< t184 , int32_t >::Invoke(&m13336_MI, __this, p0);
		return L_0;
	}
}
extern MethodInfo m13314_MI;
 void m13314 (t2514 * __this, int32_t p0, t184  p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13315_MI;
 bool m13315 (t2514 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m13316_MI;
 void m13316 (t2514 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t20 *, int32_t >::Invoke(&m3960_MI, ((t29 *)Castclass(L_0, InitializedTypeInfo(&t674_TI))), p0, p1);
		return;
	}
}
extern MethodInfo m13317_MI;
 t29 * m13317 (t2514 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m4154_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m13318_MI;
 int32_t m13318 (t2514 * __this, t29 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13319_MI;
 void m13319 (t2514 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13320_MI;
 bool m13320 (t2514 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m13368(NULL, p0, &m13368_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, t184  >::Invoke(&m27817_MI, L_1, ((*(t184 *)((t184 *)UnBox (p0, InitializedTypeInfo(&t184_TI))))));
		return L_2;
	}

IL_001a:
	{
		return 0;
	}
}
extern MethodInfo m13321_MI;
 int32_t m13321 (t2514 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m13368(NULL, p0, &m13368_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, t184  >::Invoke(&m27820_MI, L_1, ((*(t184 *)((t184 *)UnBox (p0, InitializedTypeInfo(&t184_TI))))));
		return L_2;
	}

IL_001a:
	{
		return (-1);
	}
}
extern MethodInfo m13322_MI;
 void m13322 (t2514 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13323_MI;
 void m13323 (t2514 * __this, t29 * p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13324_MI;
 void m13324 (t2514 * __this, int32_t p0, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13325_MI;
 bool m13325 (t2514 * __this, MethodInfo* method){
	{
		return 0;
	}
}
extern MethodInfo m13326_MI;
 t29 * m13326 (t2514 * __this, MethodInfo* method){
	{
		return __this;
	}
}
extern MethodInfo m13327_MI;
 bool m13327 (t2514 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m13328_MI;
 bool m13328 (t2514 * __this, MethodInfo* method){
	{
		return 1;
	}
}
extern MethodInfo m13329_MI;
 t29 * m13329 (t2514 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t184  L_1 = (t184 )InterfaceFuncInvoker1< t184 , int32_t >::Invoke(&m1924_MI, L_0, p0);
		t184  L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t184_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m13330_MI;
 void m13330 (t2514 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
extern MethodInfo m13331_MI;
 bool m13331 (t2514 * __this, t184  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker1< bool, t184  >::Invoke(&m27817_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m13332_MI;
 void m13332 (t2514 * __this, t201* p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t201*, int32_t >::Invoke(&m27811_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m13333_MI;
 t29* m13333 (t2514 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m27812_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m13334_MI;
 int32_t m13334 (t2514 * __this, t184  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker1< int32_t, t184  >::Invoke(&m27820_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m13335_MI;
 int32_t m13335 (t2514 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1926_MI, L_0);
		return L_1;
	}
}
 t184  m13336 (t2514 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t184  L_1 = (t184 )InterfaceFuncInvoker1< t184 , int32_t >::Invoke(&m1924_MI, L_0, p0);
		return L_1;
	}
}
// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>
extern Il2CppType t403_0_0_1;
FieldInfo t2514_f0_FieldInfo = 
{
	"list", &t403_0_0_1, &t2514_TI, offsetof(t2514, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2514_FIs[] =
{
	&t2514_f0_FieldInfo,
	NULL
};
static PropertyInfo t2514____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2514_TI, "System.Collections.Generic.IList<T>.Item", &m13313_MI, &m13314_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2514____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2514_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m13315_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2514____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2514_TI, "System.Collections.ICollection.IsSynchronized", &m13325_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2514____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2514_TI, "System.Collections.ICollection.SyncRoot", &m13326_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2514____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2514_TI, "System.Collections.IList.IsFixedSize", &m13327_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2514____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2514_TI, "System.Collections.IList.IsReadOnly", &m13328_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2514____System_Collections_IList_Item_PropertyInfo = 
{
	&t2514_TI, "System.Collections.IList.Item", &m13329_MI, &m13330_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2514____Count_PropertyInfo = 
{
	&t2514_TI, "Count", &m13335_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2514____Item_PropertyInfo = 
{
	&t2514_TI, "Item", &m13336_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2514_PIs[] =
{
	&t2514____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2514____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2514____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2514____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2514____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2514____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2514____System_Collections_IList_Item_PropertyInfo,
	&t2514____Count_PropertyInfo,
	&t2514____Item_PropertyInfo,
	NULL
};
extern Il2CppType t403_0_0_0;
static ParameterInfo t2514_m13307_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t403_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13307_GM;
MethodInfo m13307_MI = 
{
	".ctor", (methodPointerType)&m13307, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2514_m13307_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13307_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t2514_m13308_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13308_GM;
MethodInfo m13308_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m13308, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21_t184, t2514_m13308_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13308_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13309_GM;
MethodInfo m13309_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m13309, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13309_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t2514_m13310_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13310_GM;
MethodInfo m13310_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m13310, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t184, t2514_m13310_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13310_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t2514_m13311_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13311_GM;
MethodInfo m13311_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m13311, &t2514_TI, &t40_0_0_0, RuntimeInvoker_t40_t184, t2514_m13311_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13311_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2514_m13312_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13312_GM;
MethodInfo m13312_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m13312, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2514_m13312_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13312_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2514_m13313_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t184_0_0_0;
extern void* RuntimeInvoker_t184_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13313_GM;
MethodInfo m13313_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m13313, &t2514_TI, &t184_0_0_0, RuntimeInvoker_t184_t44, t2514_m13313_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13313_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t2514_m13314_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13314_GM;
MethodInfo m13314_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m13314, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t184, t2514_m13314_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13314_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13315_GM;
MethodInfo m13315_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m13315, &t2514_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13315_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2514_m13316_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13316_GM;
MethodInfo m13316_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m13316, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2514_m13316_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13316_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13317_GM;
MethodInfo m13317_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m13317, &t2514_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13317_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2514_m13318_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13318_GM;
MethodInfo m13318_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m13318, &t2514_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2514_m13318_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13318_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13319_GM;
MethodInfo m13319_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m13319, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13319_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2514_m13320_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13320_GM;
MethodInfo m13320_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m13320, &t2514_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2514_m13320_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13320_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2514_m13321_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13321_GM;
MethodInfo m13321_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m13321, &t2514_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2514_m13321_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13321_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2514_m13322_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13322_GM;
MethodInfo m13322_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m13322, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2514_m13322_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13322_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2514_m13323_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13323_GM;
MethodInfo m13323_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m13323, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2514_m13323_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13323_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2514_m13324_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13324_GM;
MethodInfo m13324_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m13324, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2514_m13324_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13324_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13325_GM;
MethodInfo m13325_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m13325, &t2514_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13325_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13326_GM;
MethodInfo m13326_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m13326, &t2514_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13326_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13327_GM;
MethodInfo m13327_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m13327, &t2514_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13327_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13328_GM;
MethodInfo m13328_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m13328, &t2514_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13328_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2514_m13329_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13329_GM;
MethodInfo m13329_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m13329, &t2514_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2514_m13329_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13329_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2514_m13330_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13330_GM;
MethodInfo m13330_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m13330, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2514_m13330_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13330_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t2514_m13331_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13331_GM;
MethodInfo m13331_MI = 
{
	"Contains", (methodPointerType)&m13331, &t2514_TI, &t40_0_0_0, RuntimeInvoker_t40_t184, t2514_m13331_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13331_GM};
extern Il2CppType t201_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2514_m13332_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t201_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13332_GM;
MethodInfo m13332_MI = 
{
	"CopyTo", (methodPointerType)&m13332, &t2514_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2514_m13332_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13332_GM};
extern Il2CppType t2512_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13333_GM;
MethodInfo m13333_MI = 
{
	"GetEnumerator", (methodPointerType)&m13333, &t2514_TI, &t2512_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13333_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t2514_m13334_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13334_GM;
MethodInfo m13334_MI = 
{
	"IndexOf", (methodPointerType)&m13334, &t2514_TI, &t44_0_0_0, RuntimeInvoker_t44_t184, t2514_m13334_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13334_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13335_GM;
MethodInfo m13335_MI = 
{
	"get_Count", (methodPointerType)&m13335, &t2514_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13335_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2514_m13336_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t184_0_0_0;
extern void* RuntimeInvoker_t184_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13336_GM;
MethodInfo m13336_MI = 
{
	"get_Item", (methodPointerType)&m13336, &t2514_TI, &t184_0_0_0, RuntimeInvoker_t184_t44, t2514_m13336_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13336_GM};
static MethodInfo* t2514_MIs[] =
{
	&m13307_MI,
	&m13308_MI,
	&m13309_MI,
	&m13310_MI,
	&m13311_MI,
	&m13312_MI,
	&m13313_MI,
	&m13314_MI,
	&m13315_MI,
	&m13316_MI,
	&m13317_MI,
	&m13318_MI,
	&m13319_MI,
	&m13320_MI,
	&m13321_MI,
	&m13322_MI,
	&m13323_MI,
	&m13324_MI,
	&m13325_MI,
	&m13326_MI,
	&m13327_MI,
	&m13328_MI,
	&m13329_MI,
	&m13330_MI,
	&m13331_MI,
	&m13332_MI,
	&m13333_MI,
	&m13334_MI,
	&m13335_MI,
	&m13336_MI,
	NULL
};
static MethodInfo* t2514_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13317_MI,
	&m13335_MI,
	&m13325_MI,
	&m13326_MI,
	&m13316_MI,
	&m13327_MI,
	&m13328_MI,
	&m13329_MI,
	&m13330_MI,
	&m13318_MI,
	&m13319_MI,
	&m13320_MI,
	&m13321_MI,
	&m13322_MI,
	&m13323_MI,
	&m13324_MI,
	&m13335_MI,
	&m13315_MI,
	&m13308_MI,
	&m13309_MI,
	&m13331_MI,
	&m13332_MI,
	&m13311_MI,
	&m13334_MI,
	&m13310_MI,
	&m13312_MI,
	&m13313_MI,
	&m13314_MI,
	&m13333_MI,
	&m13336_MI,
};
static TypeInfo* t2514_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t404_TI,
	&t403_TI,
	&t2513_TI,
};
static Il2CppInterfaceOffsetPair t2514_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t404_TI, 20},
	{ &t403_TI, 27},
	{ &t2513_TI, 32},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2514_0_0_0;
extern Il2CppType t2514_1_0_0;
struct t2514;
extern Il2CppGenericClass t2514_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2514_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2514_MIs, t2514_PIs, t2514_FIs, NULL, &t29_TI, NULL, NULL, &t2514_TI, t2514_ITIs, t2514_VT, &t1263__CustomAttributeCache, &t2514_TI, &t2514_0_0_0, &t2514_1_0_0, t2514_IOs, &t2514_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2514), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2519.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2519_TI;

extern MethodInfo m13371_MI;
extern MethodInfo m13372_MI;
extern MethodInfo m13369_MI;
extern MethodInfo m13367_MI;
extern MethodInfo m13360_MI;
extern MethodInfo m13370_MI;
extern MethodInfo m13358_MI;
extern MethodInfo m13363_MI;
extern MethodInfo m13354_MI;
extern MethodInfo m27816_MI;
extern MethodInfo m27821_MI;
extern MethodInfo m27822_MI;


extern MethodInfo m13337_MI;
 void m13337 (t2519 * __this, MethodInfo* method){
	t163 * V_0 = {0};
	t29 * V_1 = {0};
	{
		m1331(__this, &m1331_MI);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t163_TI));
		t163 * L_0 = (t163 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t163_TI));
		m1735(L_0, &m1735_MI);
		V_0 = L_0;
		V_1 = V_0;
		t29 * L_1 = (t29 *)InterfaceFuncInvoker0< t29 * >::Invoke(&m9816_MI, V_1);
		__this->f1 = L_1;
		__this->f0 = V_0;
		return;
	}
}
extern MethodInfo m13338_MI;
 bool m13338 (t2519 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m27814_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m13339_MI;
 void m13339 (t2519 * __this, t20 * p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t20 *, int32_t >::Invoke(&m3960_MI, ((t29 *)Castclass(L_0, InitializedTypeInfo(&t674_TI))), p0, p1);
		return;
	}
}
extern MethodInfo m13340_MI;
 t29 * m13340 (t2519 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m27812_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m13341_MI;
 int32_t m13341 (t2519 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1926_MI, L_0);
		V_0 = L_1;
		t184  L_2 = m13369(NULL, p0, &m13369_MI);
		VirtActionInvoker2< int32_t, t184  >::Invoke(&m13360_MI, __this, V_0, L_2);
		return V_0;
	}
}
extern MethodInfo m13342_MI;
 bool m13342 (t2519 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m13368(NULL, p0, &m13368_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, t184  >::Invoke(&m27817_MI, L_1, ((*(t184 *)((t184 *)UnBox (p0, InitializedTypeInfo(&t184_TI))))));
		return L_2;
	}

IL_001a:
	{
		return 0;
	}
}
extern MethodInfo m13343_MI;
 int32_t m13343 (t2519 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m13368(NULL, p0, &m13368_MI);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		t29* L_1 = (__this->f0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, t184  >::Invoke(&m27820_MI, L_1, ((*(t184 *)((t184 *)UnBox (p0, InitializedTypeInfo(&t184_TI))))));
		return L_2;
	}

IL_001a:
	{
		return (-1);
	}
}
extern MethodInfo m13344_MI;
 void m13344 (t2519 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t184  L_0 = m13369(NULL, p1, &m13369_MI);
		VirtActionInvoker2< int32_t, t184  >::Invoke(&m13360_MI, __this, p0, L_0);
		return;
	}
}
extern MethodInfo m13345_MI;
 void m13345 (t2519 * __this, t29 * p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		m13370(NULL, L_0, &m13370_MI);
		t184  L_1 = m13369(NULL, p0, &m13369_MI);
		int32_t L_2 = (int32_t)VirtFuncInvoker1< int32_t, t184  >::Invoke(&m13358_MI, __this, L_1);
		V_0 = L_2;
		VirtActionInvoker1< int32_t >::Invoke(&m13363_MI, __this, V_0);
		return;
	}
}
extern MethodInfo m13346_MI;
 bool m13346 (t2519 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = m13371(NULL, L_0, &m13371_MI);
		return L_1;
	}
}
extern MethodInfo m13347_MI;
 t29 * m13347 (t2519 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f1);
		return L_0;
	}
}
extern MethodInfo m13348_MI;
 bool m13348 (t2519 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = m13372(NULL, L_0, &m13372_MI);
		return L_1;
	}
}
extern MethodInfo m13349_MI;
 bool m13349 (t2519 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m27814_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m13350_MI;
 t29 * m13350 (t2519 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t184  L_1 = (t184 )InterfaceFuncInvoker1< t184 , int32_t >::Invoke(&m1924_MI, L_0, p0);
		t184  L_2 = L_1;
		t29 * L_3 = Box(InitializedTypeInfo(&t184_TI), &L_2);
		return L_3;
	}
}
extern MethodInfo m13351_MI;
 void m13351 (t2519 * __this, int32_t p0, t29 * p1, MethodInfo* method){
	{
		t184  L_0 = m13369(NULL, p1, &m13369_MI);
		VirtActionInvoker2< int32_t, t184  >::Invoke(&m13367_MI, __this, p0, L_0);
		return;
	}
}
extern MethodInfo m13352_MI;
 void m13352 (t2519 * __this, t184  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1926_MI, L_0);
		V_0 = L_1;
		VirtActionInvoker2< int32_t, t184  >::Invoke(&m13360_MI, __this, V_0, p0);
		return;
	}
}
extern MethodInfo m13353_MI;
 void m13353 (t2519 * __this, MethodInfo* method){
	{
		VirtActionInvoker0::Invoke(&m13354_MI, __this);
		return;
	}
}
 void m13354 (t2519 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker0::Invoke(&m27816_MI, L_0);
		return;
	}
}
extern MethodInfo m13355_MI;
 bool m13355 (t2519 * __this, t184  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		bool L_1 = (bool)InterfaceFuncInvoker1< bool, t184  >::Invoke(&m27817_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m13356_MI;
 void m13356 (t2519 * __this, t201* p0, int32_t p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< t201*, int32_t >::Invoke(&m27811_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m13357_MI;
 t29* m13357 (t2519 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t29* L_1 = (t29*)InterfaceFuncInvoker0< t29* >::Invoke(&m27812_MI, L_0);
		return L_1;
	}
}
 int32_t m13358 (t2519 * __this, t184  p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker1< int32_t, t184  >::Invoke(&m27820_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m13359_MI;
 void m13359 (t2519 * __this, int32_t p0, t184  p1, MethodInfo* method){
	{
		VirtActionInvoker2< int32_t, t184  >::Invoke(&m13360_MI, __this, p0, p1);
		return;
	}
}
 void m13360 (t2519 * __this, int32_t p0, t184  p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< int32_t, t184  >::Invoke(&m27821_MI, L_0, p0, p1);
		return;
	}
}
extern MethodInfo m13361_MI;
 bool m13361 (t2519 * __this, t184  p0, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t184  >::Invoke(&m13358_MI, __this, p0);
		V_0 = L_0;
		if ((((uint32_t)V_0) != ((uint32_t)(-1))))
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		VirtActionInvoker1< int32_t >::Invoke(&m13363_MI, __this, V_0);
		return 1;
	}
}
extern MethodInfo m13362_MI;
 void m13362 (t2519 * __this, int32_t p0, MethodInfo* method){
	{
		VirtActionInvoker1< int32_t >::Invoke(&m13363_MI, __this, p0);
		return;
	}
}
 void m13363 (t2519 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker1< int32_t >::Invoke(&m27822_MI, L_0, p0);
		return;
	}
}
extern MethodInfo m13364_MI;
 int32_t m13364 (t2519 * __this, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(&m1926_MI, L_0);
		return L_1;
	}
}
extern MethodInfo m13365_MI;
 t184  m13365 (t2519 * __this, int32_t p0, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		t184  L_1 = (t184 )InterfaceFuncInvoker1< t184 , int32_t >::Invoke(&m1924_MI, L_0, p0);
		return L_1;
	}
}
extern MethodInfo m13366_MI;
 void m13366 (t2519 * __this, int32_t p0, t184  p1, MethodInfo* method){
	{
		VirtActionInvoker2< int32_t, t184  >::Invoke(&m13367_MI, __this, p0, p1);
		return;
	}
}
 void m13367 (t2519 * __this, int32_t p0, t184  p1, MethodInfo* method){
	{
		t29* L_0 = (__this->f0);
		InterfaceActionInvoker2< int32_t, t184  >::Invoke(&m27819_MI, L_0, p0, p1);
		return;
	}
}
 bool m13368 (t29 * __this, t29 * p0, MethodInfo* method){
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		if (((t29 *)IsInst(p0, InitializedTypeInfo(&t184_TI))))
		{
			goto IL_0022;
		}
	}
	{
		if (p0)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_0 = m1554(NULL, LoadTypeToken(&t184_0_0_0), &m1554_MI);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&m6004_MI, L_0);
		G_B4_0 = ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0020;
	}

IL_001f:
	{
		G_B4_0 = 0;
	}

IL_0020:
	{
		G_B6_0 = G_B4_0;
		goto IL_0023;
	}

IL_0022:
	{
		G_B6_0 = 1;
	}

IL_0023:
	{
		return G_B6_0;
	}
}
 t184  m13369 (t29 * __this, t29 * p0, MethodInfo* method){
	{
		bool L_0 = m13368(NULL, p0, &m13368_MI);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		return ((*(t184 *)((t184 *)UnBox (p0, InitializedTypeInfo(&t184_TI)))));
	}

IL_000f:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_1, (t7*) &_stringLiteral1176, &m1935_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
 void m13370 (t29 * __this, t29* p0, MethodInfo* method){
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m27814_MI, p0);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		t345 * L_1 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_1, &m1516_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		return;
	}
}
 bool m13371 (t29 * __this, t29* p0, MethodInfo* method){
	t29 * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		V_0 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t674_TI)));
		if (!V_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m9815_MI, V_0);
		G_B3_0 = ((int32_t)(L_0));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
 bool m13372 (t29 * __this, t29* p0, MethodInfo* method){
	t29 * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		V_0 = ((t29 *)IsInst(p0, InitializedTypeInfo(&t868_TI)));
		if (!V_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_0 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m9817_MI, V_0);
		G_B3_0 = ((int32_t)(L_0));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>
extern Il2CppType t403_0_0_1;
FieldInfo t2519_f0_FieldInfo = 
{
	"list", &t403_0_0_1, &t2519_TI, offsetof(t2519, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2519_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2519_TI, offsetof(t2519, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2519_FIs[] =
{
	&t2519_f0_FieldInfo,
	&t2519_f1_FieldInfo,
	NULL
};
static PropertyInfo t2519____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2519_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m13338_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2519____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2519_TI, "System.Collections.ICollection.IsSynchronized", &m13346_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2519____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2519_TI, "System.Collections.ICollection.SyncRoot", &m13347_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2519____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2519_TI, "System.Collections.IList.IsFixedSize", &m13348_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2519____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2519_TI, "System.Collections.IList.IsReadOnly", &m13349_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2519____System_Collections_IList_Item_PropertyInfo = 
{
	&t2519_TI, "System.Collections.IList.Item", &m13350_MI, &m13351_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2519____Count_PropertyInfo = 
{
	&t2519_TI, "Count", &m13364_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2519____Item_PropertyInfo = 
{
	&t2519_TI, "Item", &m13365_MI, &m13366_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2519_PIs[] =
{
	&t2519____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2519____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2519____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2519____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2519____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2519____System_Collections_IList_Item_PropertyInfo,
	&t2519____Count_PropertyInfo,
	&t2519____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13337_GM;
MethodInfo m13337_MI = 
{
	".ctor", (methodPointerType)&m13337, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13337_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13338_GM;
MethodInfo m13338_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m13338, &t2519_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13338_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2519_m13339_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13339_GM;
MethodInfo m13339_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m13339, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2519_m13339_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13339_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13340_GM;
MethodInfo m13340_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m13340, &t2519_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13340_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2519_m13341_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13341_GM;
MethodInfo m13341_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m13341, &t2519_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2519_m13341_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13341_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2519_m13342_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13342_GM;
MethodInfo m13342_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m13342, &t2519_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2519_m13342_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13342_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2519_m13343_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13343_GM;
MethodInfo m13343_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m13343, &t2519_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2519_m13343_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13343_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2519_m13344_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13344_GM;
MethodInfo m13344_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m13344, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2519_m13344_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13344_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2519_m13345_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13345_GM;
MethodInfo m13345_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m13345, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2519_m13345_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13345_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13346_GM;
MethodInfo m13346_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m13346, &t2519_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13346_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13347_GM;
MethodInfo m13347_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m13347, &t2519_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13347_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13348_GM;
MethodInfo m13348_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m13348, &t2519_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13348_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13349_GM;
MethodInfo m13349_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m13349, &t2519_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13349_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2519_m13350_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13350_GM;
MethodInfo m13350_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m13350, &t2519_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2519_m13350_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13350_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2519_m13351_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13351_GM;
MethodInfo m13351_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m13351, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2519_m13351_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13351_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t2519_m13352_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13352_GM;
MethodInfo m13352_MI = 
{
	"Add", (methodPointerType)&m13352, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t184, t2519_m13352_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13352_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13353_GM;
MethodInfo m13353_MI = 
{
	"Clear", (methodPointerType)&m13353, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13353_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13354_GM;
MethodInfo m13354_MI = 
{
	"ClearItems", (methodPointerType)&m13354, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13354_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t2519_m13355_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13355_GM;
MethodInfo m13355_MI = 
{
	"Contains", (methodPointerType)&m13355, &t2519_TI, &t40_0_0_0, RuntimeInvoker_t40_t184, t2519_m13355_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13355_GM};
extern Il2CppType t201_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2519_m13356_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t201_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13356_GM;
MethodInfo m13356_MI = 
{
	"CopyTo", (methodPointerType)&m13356, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2519_m13356_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13356_GM};
extern Il2CppType t2512_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13357_GM;
MethodInfo m13357_MI = 
{
	"GetEnumerator", (methodPointerType)&m13357, &t2519_TI, &t2512_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13357_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t2519_m13358_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13358_GM;
MethodInfo m13358_MI = 
{
	"IndexOf", (methodPointerType)&m13358, &t2519_TI, &t44_0_0_0, RuntimeInvoker_t44_t184, t2519_m13358_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13358_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t2519_m13359_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13359_GM;
MethodInfo m13359_MI = 
{
	"Insert", (methodPointerType)&m13359, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t184, t2519_m13359_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13359_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t2519_m13360_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13360_GM;
MethodInfo m13360_MI = 
{
	"InsertItem", (methodPointerType)&m13360, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t184, t2519_m13360_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13360_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t2519_m13361_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13361_GM;
MethodInfo m13361_MI = 
{
	"Remove", (methodPointerType)&m13361, &t2519_TI, &t40_0_0_0, RuntimeInvoker_t40_t184, t2519_m13361_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13361_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2519_m13362_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13362_GM;
MethodInfo m13362_MI = 
{
	"RemoveAt", (methodPointerType)&m13362, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2519_m13362_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13362_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2519_m13363_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13363_GM;
MethodInfo m13363_MI = 
{
	"RemoveItem", (methodPointerType)&m13363, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2519_m13363_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13363_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13364_GM;
MethodInfo m13364_MI = 
{
	"get_Count", (methodPointerType)&m13364, &t2519_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13364_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2519_m13365_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t184_0_0_0;
extern void* RuntimeInvoker_t184_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13365_GM;
MethodInfo m13365_MI = 
{
	"get_Item", (methodPointerType)&m13365, &t2519_TI, &t184_0_0_0, RuntimeInvoker_t184_t44, t2519_m13365_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13365_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t2519_m13366_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13366_GM;
MethodInfo m13366_MI = 
{
	"set_Item", (methodPointerType)&m13366, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t184, t2519_m13366_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13366_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t2519_m13367_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13367_GM;
MethodInfo m13367_MI = 
{
	"SetItem", (methodPointerType)&m13367, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t184, t2519_m13367_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13367_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2519_m13368_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13368_GM;
MethodInfo m13368_MI = 
{
	"IsValidItem", (methodPointerType)&m13368, &t2519_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2519_m13368_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13368_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2519_m13369_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t184_0_0_0;
extern void* RuntimeInvoker_t184_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13369_GM;
MethodInfo m13369_MI = 
{
	"ConvertItem", (methodPointerType)&m13369, &t2519_TI, &t184_0_0_0, RuntimeInvoker_t184_t29, t2519_m13369_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13369_GM};
extern Il2CppType t403_0_0_0;
static ParameterInfo t2519_m13370_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t403_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13370_GM;
MethodInfo m13370_MI = 
{
	"CheckWritable", (methodPointerType)&m13370, &t2519_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2519_m13370_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13370_GM};
extern Il2CppType t403_0_0_0;
static ParameterInfo t2519_m13371_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t403_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13371_GM;
MethodInfo m13371_MI = 
{
	"IsSynchronized", (methodPointerType)&m13371, &t2519_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2519_m13371_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13371_GM};
extern Il2CppType t403_0_0_0;
static ParameterInfo t2519_m13372_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t403_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13372_GM;
MethodInfo m13372_MI = 
{
	"IsFixedSize", (methodPointerType)&m13372, &t2519_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2519_m13372_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13372_GM};
static MethodInfo* t2519_MIs[] =
{
	&m13337_MI,
	&m13338_MI,
	&m13339_MI,
	&m13340_MI,
	&m13341_MI,
	&m13342_MI,
	&m13343_MI,
	&m13344_MI,
	&m13345_MI,
	&m13346_MI,
	&m13347_MI,
	&m13348_MI,
	&m13349_MI,
	&m13350_MI,
	&m13351_MI,
	&m13352_MI,
	&m13353_MI,
	&m13354_MI,
	&m13355_MI,
	&m13356_MI,
	&m13357_MI,
	&m13358_MI,
	&m13359_MI,
	&m13360_MI,
	&m13361_MI,
	&m13362_MI,
	&m13363_MI,
	&m13364_MI,
	&m13365_MI,
	&m13366_MI,
	&m13367_MI,
	&m13368_MI,
	&m13369_MI,
	&m13370_MI,
	&m13371_MI,
	&m13372_MI,
	NULL
};
static MethodInfo* t2519_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13340_MI,
	&m13364_MI,
	&m13346_MI,
	&m13347_MI,
	&m13339_MI,
	&m13348_MI,
	&m13349_MI,
	&m13350_MI,
	&m13351_MI,
	&m13341_MI,
	&m13353_MI,
	&m13342_MI,
	&m13343_MI,
	&m13344_MI,
	&m13345_MI,
	&m13362_MI,
	&m13364_MI,
	&m13338_MI,
	&m13352_MI,
	&m13353_MI,
	&m13355_MI,
	&m13356_MI,
	&m13361_MI,
	&m13358_MI,
	&m13359_MI,
	&m13362_MI,
	&m13365_MI,
	&m13366_MI,
	&m13357_MI,
	&m13354_MI,
	&m13360_MI,
	&m13363_MI,
	&m13367_MI,
};
static TypeInfo* t2519_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t404_TI,
	&t403_TI,
	&t2513_TI,
};
static Il2CppInterfaceOffsetPair t2519_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t404_TI, 20},
	{ &t403_TI, 27},
	{ &t2513_TI, 32},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2519_0_0_0;
extern Il2CppType t2519_1_0_0;
struct t2519;
extern Il2CppGenericClass t2519_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2519_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2519_MIs, t2519_PIs, t2519_FIs, NULL, &t29_TI, NULL, NULL, &t2519_TI, t2519_ITIs, t2519_VT, &t1262__CustomAttributeCache, &t2519_TI, &t2519_0_0_0, &t2519_1_0_0, t2519_IOs, &t2519_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2519), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2520_TI;
#include "t2520MD.h"

#include "t2521.h"
extern TypeInfo t6682_TI;
extern TypeInfo t2521_TI;
#include "t2521MD.h"
extern Il2CppType t6682_0_0_0;
extern MethodInfo m13378_MI;
extern MethodInfo m27823_MI;
extern MethodInfo m20920_MI;


extern MethodInfo m13373_MI;
 void m13373 (t2520 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m13374_MI;
 void m13374 (t29 * __this, MethodInfo* method){
	t2521 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t2521 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t2521_TI));
	m13378(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m13378_MI);
	((t2520_SFs*)InitializedTypeInfo(&t2520_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m13375_MI;
 int32_t m13375 (t2520 * __this, t29 * p0, MethodInfo* method){
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker1< int32_t, t184  >::Invoke(&m27823_MI, __this, ((*(t184 *)((t184 *)UnBox (p0, InitializedTypeInfo(&t184_TI))))));
		return L_0;
	}
}
extern MethodInfo m13376_MI;
 bool m13376 (t2520 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	{
		bool L_0 = (bool)VirtFuncInvoker2< bool, t184 , t184  >::Invoke(&m20920_MI, __this, ((*(t184 *)((t184 *)UnBox (p0, InitializedTypeInfo(&t184_TI))))), ((*(t184 *)((t184 *)UnBox (p1, InitializedTypeInfo(&t184_TI))))));
		return L_0;
	}
}
extern MethodInfo m13377_MI;
 t2520 * m13377 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2520_TI));
		return (((t2520_SFs*)InitializedTypeInfo(&t2520_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.UIVertex>
extern Il2CppType t2520_0_0_49;
FieldInfo t2520_f0_FieldInfo = 
{
	"_default", &t2520_0_0_49, &t2520_TI, offsetof(t2520_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2520_FIs[] =
{
	&t2520_f0_FieldInfo,
	NULL
};
static PropertyInfo t2520____Default_PropertyInfo = 
{
	&t2520_TI, "Default", &m13377_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2520_PIs[] =
{
	&t2520____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13373_GM;
MethodInfo m13373_MI = 
{
	".ctor", (methodPointerType)&m13373, &t2520_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13373_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13374_GM;
MethodInfo m13374_MI = 
{
	".cctor", (methodPointerType)&m13374, &t2520_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13374_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2520_m13375_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13375_GM;
MethodInfo m13375_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m13375, &t2520_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2520_m13375_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13375_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2520_m13376_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13376_GM;
MethodInfo m13376_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m13376, &t2520_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2520_m13376_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13376_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t2520_m27823_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27823_GM;
MethodInfo m27823_MI = 
{
	"GetHashCode", NULL, &t2520_TI, &t44_0_0_0, RuntimeInvoker_t44_t184, t2520_m27823_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27823_GM};
extern Il2CppType t184_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t2520_m20920_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20920_GM;
MethodInfo m20920_MI = 
{
	"Equals", NULL, &t2520_TI, &t40_0_0_0, RuntimeInvoker_t40_t184_t184, t2520_m20920_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20920_GM};
extern Il2CppType t2520_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13377_GM;
MethodInfo m13377_MI = 
{
	"get_Default", (methodPointerType)&m13377, &t2520_TI, &t2520_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13377_GM};
static MethodInfo* t2520_MIs[] =
{
	&m13373_MI,
	&m13374_MI,
	&m13375_MI,
	&m13376_MI,
	&m27823_MI,
	&m20920_MI,
	&m13377_MI,
	NULL
};
static MethodInfo* t2520_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m20920_MI,
	&m27823_MI,
	&m13376_MI,
	&m13375_MI,
	NULL,
	NULL,
};
extern TypeInfo t6683_TI;
static TypeInfo* t2520_ITIs[] = 
{
	&t6683_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2520_IOs[] = 
{
	{ &t6683_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2520_0_0_0;
extern Il2CppType t2520_1_0_0;
struct t2520;
extern Il2CppGenericClass t2520_GC;
TypeInfo t2520_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2520_MIs, t2520_PIs, t2520_FIs, NULL, &t29_TI, NULL, NULL, &t2520_TI, t2520_ITIs, t2520_VT, &EmptyCustomAttributesCache, &t2520_TI, &t2520_0_0_0, &t2520_1_0_0, t2520_IOs, &t2520_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2520), 0, -1, sizeof(t2520_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.UIVertex>
extern Il2CppType t184_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t6683_m27824_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27824_GM;
MethodInfo m27824_MI = 
{
	"Equals", NULL, &t6683_TI, &t40_0_0_0, RuntimeInvoker_t40_t184_t184, t6683_m27824_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27824_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t6683_m27825_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27825_GM;
MethodInfo m27825_MI = 
{
	"GetHashCode", NULL, &t6683_TI, &t44_0_0_0, RuntimeInvoker_t44_t184, t6683_m27825_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27825_GM};
static MethodInfo* t6683_MIs[] =
{
	&m27824_MI,
	&m27825_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6683_0_0_0;
extern Il2CppType t6683_1_0_0;
struct t6683;
extern Il2CppGenericClass t6683_GC;
TypeInfo t6683_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t6683_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6683_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6683_TI, &t6683_0_0_0, &t6683_1_0_0, NULL, &t6683_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.UIVertex>
extern Il2CppType t184_0_0_0;
static ParameterInfo t6682_m27826_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27826_GM;
MethodInfo m27826_MI = 
{
	"Equals", NULL, &t6682_TI, &t40_0_0_0, RuntimeInvoker_t40_t184, t6682_m27826_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27826_GM};
static MethodInfo* t6682_MIs[] =
{
	&m27826_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6682_1_0_0;
struct t6682;
extern Il2CppGenericClass t6682_GC;
TypeInfo t6682_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6682_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6682_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6682_TI, &t6682_0_0_0, &t6682_1_0_0, NULL, &t6682_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



 void m13378 (t2521 * __this, MethodInfo* method){
	{
		m13373(__this, &m13373_MI);
		return;
	}
}
extern MethodInfo m13379_MI;
 int32_t m13379 (t2521 * __this, t184  p0, MethodInfo* method){
	{
		t184  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t184_TI), &L_0);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&m1322_MI, Box(InitializedTypeInfo(&t184_TI), &(*(&p0))));
		return L_2;
	}
}
extern MethodInfo m13380_MI;
 bool m13380 (t2521 * __this, t184  p0, t184  p1, MethodInfo* method){
	{
		t184  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t184_TI), &L_0);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		t184  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t184_TI), &L_2);
		return ((((t29 *)L_3) == ((t29 *)NULL))? 1 : 0);
	}

IL_0012:
	{
		t184  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t184_TI), &L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, t29 * >::Invoke(&m1321_MI, Box(InitializedTypeInfo(&t184_TI), &(*(&p0))), L_5);
		return L_6;
	}
}
// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13378_GM;
MethodInfo m13378_MI = 
{
	".ctor", (methodPointerType)&m13378, &t2521_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13378_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t2521_m13379_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13379_GM;
MethodInfo m13379_MI = 
{
	"GetHashCode", (methodPointerType)&m13379, &t2521_TI, &t44_0_0_0, RuntimeInvoker_t44_t184, t2521_m13379_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13379_GM};
extern Il2CppType t184_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t2521_m13380_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13380_GM;
MethodInfo m13380_MI = 
{
	"Equals", (methodPointerType)&m13380, &t2521_TI, &t40_0_0_0, RuntimeInvoker_t40_t184_t184, t2521_m13380_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13380_GM};
static MethodInfo* t2521_MIs[] =
{
	&m13378_MI,
	&m13379_MI,
	&m13380_MI,
	NULL
};
static MethodInfo* t2521_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13380_MI,
	&m13379_MI,
	&m13376_MI,
	&m13375_MI,
	&m13379_MI,
	&m13380_MI,
};
static Il2CppInterfaceOffsetPair t2521_IOs[] = 
{
	{ &t6683_TI, 4},
	{ &t734_TI, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2521_0_0_0;
extern Il2CppType t2521_1_0_0;
struct t2521;
extern Il2CppGenericClass t2521_GC;
TypeInfo t2521_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2521_MIs, NULL, NULL, NULL, &t2520_TI, NULL, &t1256_TI, &t2521_TI, NULL, t2521_VT, &EmptyCustomAttributesCache, &t2521_TI, &t2521_0_0_0, &t2521_1_0_0, t2521_IOs, &t2521_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2521), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



extern MethodInfo m13381_MI;
 void m13381 (t2515 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
 bool m13382 (t2515 * __this, t184  p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m13382((t2515 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (t29 *, t29 * __this, t184  p0, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	typedef bool (*FunctionPointerType) (t29 * __this, t184  p0, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m13383_MI;
 t29 * m13383 (t2515 * __this, t184  p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t184_TI), &p0);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m13384_MI;
 bool m13384 (t2515 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Predicate`1<UnityEngine.UIVertex>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2515_m13381_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13381_GM;
MethodInfo m13381_MI = 
{
	".ctor", (methodPointerType)&m13381, &t2515_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2515_m13381_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13381_GM};
extern Il2CppType t184_0_0_0;
static ParameterInfo t2515_m13382_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13382_GM;
MethodInfo m13382_MI = 
{
	"Invoke", (methodPointerType)&m13382, &t2515_TI, &t40_0_0_0, RuntimeInvoker_t40_t184, t2515_m13382_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13382_GM};
extern Il2CppType t184_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2515_m13383_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t184_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13383_GM;
MethodInfo m13383_MI = 
{
	"BeginInvoke", (methodPointerType)&m13383, &t2515_TI, &t66_0_0_0, RuntimeInvoker_t29_t184_t29_t29, t2515_m13383_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13383_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2515_m13384_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13384_GM;
MethodInfo m13384_MI = 
{
	"EndInvoke", (methodPointerType)&m13384, &t2515_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2515_m13384_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13384_GM};
static MethodInfo* t2515_MIs[] =
{
	&m13381_MI,
	&m13382_MI,
	&m13383_MI,
	&m13384_MI,
	NULL
};
static MethodInfo* t2515_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13382_MI,
	&m13383_MI,
	&m13384_MI,
};
static Il2CppInterfaceOffsetPair t2515_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2515_1_0_0;
struct t2515;
extern Il2CppGenericClass t2515_GC;
TypeInfo t2515_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2515_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2515_TI, NULL, t2515_VT, &EmptyCustomAttributesCache, &t2515_TI, &t2515_0_0_0, &t2515_1_0_0, t2515_IOs, &t2515_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2515), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t2523.h"
extern TypeInfo t4151_TI;
extern TypeInfo t2523_TI;
#include "t2523MD.h"
extern Il2CppType t4151_0_0_0;
extern MethodInfo m13389_MI;
extern MethodInfo m27827_MI;


extern MethodInfo m13385_MI;
 void m13385 (t2522 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m13386_MI;
 void m13386 (t29 * __this, MethodInfo* method){
	t2523 * L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer = (t2523 *)il2cpp_codegen_object_new(InitializedTypeInfo(&t2523_TI));
	m13389(L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer, &m13389_MI);
	((t2522_SFs*)InitializedTypeInfo(&t2522_TI)->static_fields)->f0 = L_2417_System_Collections_Generic_EqualityComparer_1_DefaultComparer;
}
extern MethodInfo m13387_MI;
 int32_t m13387 (t2522 * __this, t29 * p0, t29 * p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		if (p0)
		{
			goto IL_000b;
		}
	}
	{
		if (p1)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		if (p1)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		if (!((t29 *)IsInst(p0, InitializedTypeInfo(&t184_TI))))
		{
			goto IL_0033;
		}
	}
	{
		if (!((t29 *)IsInst(p1, InitializedTypeInfo(&t184_TI))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_0 = (int32_t)VirtFuncInvoker2< int32_t, t184 , t184  >::Invoke(&m27827_MI, __this, ((*(t184 *)((t184 *)UnBox (p0, InitializedTypeInfo(&t184_TI))))), ((*(t184 *)((t184 *)UnBox (p1, InitializedTypeInfo(&t184_TI))))));
		return L_0;
	}

IL_0033:
	{
		t305 * L_1 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m8852(L_1, &m8852_MI);
		il2cpp_codegen_raise_exception(L_1);
	}
}
 t2522 * m13388 (t29 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t2522_TI));
		return (((t2522_SFs*)InitializedTypeInfo(&t2522_TI)->static_fields)->f0);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.UIVertex>
extern Il2CppType t2522_0_0_49;
FieldInfo t2522_f0_FieldInfo = 
{
	"_default", &t2522_0_0_49, &t2522_TI, offsetof(t2522_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2522_FIs[] =
{
	&t2522_f0_FieldInfo,
	NULL
};
static PropertyInfo t2522____Default_PropertyInfo = 
{
	&t2522_TI, "Default", &m13388_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2522_PIs[] =
{
	&t2522____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13385_GM;
MethodInfo m13385_MI = 
{
	".ctor", (methodPointerType)&m13385, &t2522_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13385_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13386_GM;
MethodInfo m13386_MI = 
{
	".cctor", (methodPointerType)&m13386, &t2522_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13386_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2522_m13387_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13387_GM;
MethodInfo m13387_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m13387, &t2522_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2522_m13387_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13387_GM};
extern Il2CppType t184_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t2522_m27827_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t184_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27827_GM;
MethodInfo m27827_MI = 
{
	"Compare", NULL, &t2522_TI, &t44_0_0_0, RuntimeInvoker_t44_t184_t184, t2522_m27827_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27827_GM};
extern Il2CppType t2522_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13388_GM;
MethodInfo m13388_MI = 
{
	"get_Default", (methodPointerType)&m13388, &t2522_TI, &t2522_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13388_GM};
static MethodInfo* t2522_MIs[] =
{
	&m13385_MI,
	&m13386_MI,
	&m13387_MI,
	&m27827_MI,
	&m13388_MI,
	NULL
};
static MethodInfo* t2522_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27827_MI,
	&m13387_MI,
	NULL,
};
extern TypeInfo t4150_TI;
static TypeInfo* t2522_ITIs[] = 
{
	&t4150_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2522_IOs[] = 
{
	{ &t4150_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2522_0_0_0;
extern Il2CppType t2522_1_0_0;
struct t2522;
extern Il2CppGenericClass t2522_GC;
TypeInfo t2522_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2522_MIs, t2522_PIs, t2522_FIs, NULL, &t29_TI, NULL, NULL, &t2522_TI, t2522_ITIs, t2522_VT, &EmptyCustomAttributesCache, &t2522_TI, &t2522_0_0_0, &t2522_1_0_0, t2522_IOs, &t2522_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2522), 0, -1, sizeof(t2522_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.UIVertex>
extern Il2CppType t184_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t4150_m20928_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t184_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20928_GM;
MethodInfo m20928_MI = 
{
	"Compare", NULL, &t4150_TI, &t44_0_0_0, RuntimeInvoker_t44_t184_t184, t4150_m20928_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20928_GM};
static MethodInfo* t4150_MIs[] =
{
	&m20928_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4150_0_0_0;
extern Il2CppType t4150_1_0_0;
struct t4150;
extern Il2CppGenericClass t4150_GC;
TypeInfo t4150_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4150_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4150_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4150_TI, &t4150_0_0_0, &t4150_1_0_0, NULL, &t4150_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IComparable`1<UnityEngine.UIVertex>
extern Il2CppType t184_0_0_0;
static ParameterInfo t4151_m20929_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20929_GM;
MethodInfo m20929_MI = 
{
	"CompareTo", NULL, &t4151_TI, &t44_0_0_0, RuntimeInvoker_t44_t184, t4151_m20929_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m20929_GM};
static MethodInfo* t4151_MIs[] =
{
	&m20929_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4151_1_0_0;
struct t4151;
extern Il2CppGenericClass t4151_GC;
TypeInfo t4151_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparable`1", "System", t4151_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4151_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4151_TI, &t4151_0_0_0, &t4151_1_0_0, NULL, &t4151_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m20929_MI;


 void m13389 (t2523 * __this, MethodInfo* method){
	{
		m13385(__this, &m13385_MI);
		return;
	}
}
extern MethodInfo m13390_MI;
 int32_t m13390 (t2523 * __this, t184  p0, t184  p1, MethodInfo* method){
	int32_t G_B4_0 = 0;
	{
		t184  L_0 = p0;
		t29 * L_1 = Box(InitializedTypeInfo(&t184_TI), &L_0);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		t184  L_2 = p1;
		t29 * L_3 = Box(InitializedTypeInfo(&t184_TI), &L_2);
		if (L_3)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		t184  L_4 = p1;
		t29 * L_5 = Box(InitializedTypeInfo(&t184_TI), &L_4);
		if (L_5)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		t184  L_6 = p0;
		t29 * L_7 = Box(InitializedTypeInfo(&t184_TI), &L_6);
		if (!((t29*)IsInst(L_7, InitializedTypeInfo(&t4151_TI))))
		{
			goto IL_003e;
		}
	}
	{
		t184  L_8 = p0;
		t29 * L_9 = Box(InitializedTypeInfo(&t184_TI), &L_8);
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, t184  >::Invoke(&m20929_MI, ((t29*)Castclass(L_9, InitializedTypeInfo(&t4151_TI))), p1);
		return L_10;
	}

IL_003e:
	{
		t184  L_11 = p0;
		t29 * L_12 = Box(InitializedTypeInfo(&t184_TI), &L_11);
		if (!((t29 *)IsInst(L_12, InitializedTypeInfo(&t290_TI))))
		{
			goto IL_0062;
		}
	}
	{
		t184  L_13 = p0;
		t29 * L_14 = Box(InitializedTypeInfo(&t184_TI), &L_13);
		t184  L_15 = p1;
		t29 * L_16 = Box(InitializedTypeInfo(&t184_TI), &L_15);
		int32_t L_17 = (int32_t)InterfaceFuncInvoker1< int32_t, t29 * >::Invoke(&m9672_MI, ((t29 *)Castclass(L_14, InitializedTypeInfo(&t290_TI))), L_16);
		return L_17;
	}

IL_0062:
	{
		t305 * L_18 = (t305 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t305_TI));
		m1935(L_18, (t7*) &_stringLiteral1162, &m1935_MI);
		il2cpp_codegen_raise_exception(L_18);
	}
}
// Metadata Definition System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13389_GM;
MethodInfo m13389_MI = 
{
	".ctor", (methodPointerType)&m13389, &t2523_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13389_GM};
extern Il2CppType t184_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t2523_m13390_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t184_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13390_GM;
MethodInfo m13390_MI = 
{
	"Compare", (methodPointerType)&m13390, &t2523_TI, &t44_0_0_0, RuntimeInvoker_t44_t184_t184, t2523_m13390_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13390_GM};
static MethodInfo* t2523_MIs[] =
{
	&m13389_MI,
	&m13390_MI,
	NULL
};
static MethodInfo* t2523_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13390_MI,
	&m13387_MI,
	&m13390_MI,
};
static Il2CppInterfaceOffsetPair t2523_IOs[] = 
{
	{ &t4150_TI, 4},
	{ &t726_TI, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2523_0_0_0;
extern Il2CppType t2523_1_0_0;
struct t2523;
extern Il2CppGenericClass t2523_GC;
TypeInfo t2523_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2523_MIs, NULL, NULL, NULL, &t2522_TI, NULL, &t1246_TI, &t2523_TI, NULL, t2523_VT, &EmptyCustomAttributesCache, &t2523_TI, &t2523_0_0_0, &t2523_1_0_0, t2523_IOs, &t2523_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2523), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 7, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2516_TI;
#include "t2516MD.h"



extern MethodInfo m13391_MI;
 void m13391 (t2516 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m13392_MI;
 int32_t m13392 (t2516 * __this, t184  p0, t184  p1, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m13392((t2516 *)__this->f9,p0, p1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (t29 *, t29 * __this, t184  p0, t184  p1, MethodInfo* method);
		return ((FunctionPointerType)__this->f0)(NULL,__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
	}
	typedef int32_t (*FunctionPointerType) (t29 * __this, t184  p0, t184  p1, MethodInfo* method);
	return ((FunctionPointerType)__this->f0)(__this->f2,p0, p1,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m13393_MI;
 t29 * m13393 (t2516 * __this, t184  p0, t184  p1, t67 * p2, t29 * p3, MethodInfo* method){
	void *__d_args[3] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t184_TI), &p0);
	__d_args[1] = Box(InitializedTypeInfo(&t184_TI), &p1);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p2, (Il2CppObject*)p3);
}
extern MethodInfo m13394_MI;
 int32_t m13394 (t2516 * __this, t29 * p0, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Comparison`1<UnityEngine.UIVertex>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2516_m13391_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13391_GM;
MethodInfo m13391_MI = 
{
	".ctor", (methodPointerType)&m13391, &t2516_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2516_m13391_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13391_GM};
extern Il2CppType t184_0_0_0;
extern Il2CppType t184_0_0_0;
static ParameterInfo t2516_m13392_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t184_t184 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13392_GM;
MethodInfo m13392_MI = 
{
	"Invoke", (methodPointerType)&m13392, &t2516_TI, &t44_0_0_0, RuntimeInvoker_t44_t184_t184, t2516_m13392_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13392_GM};
extern Il2CppType t184_0_0_0;
extern Il2CppType t184_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2516_m13393_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t184_0_0_0},
	{"callback", 2, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 3, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t184_t184_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13393_GM;
MethodInfo m13393_MI = 
{
	"BeginInvoke", (methodPointerType)&m13393, &t2516_TI, &t66_0_0_0, RuntimeInvoker_t29_t184_t184_t29_t29, t2516_m13393_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 4, false, true, 0, NULL, (methodPointerType)NULL, &m13393_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2516_m13394_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13394_GM;
MethodInfo m13394_MI = 
{
	"EndInvoke", (methodPointerType)&m13394, &t2516_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2516_m13394_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13394_GM};
static MethodInfo* t2516_MIs[] =
{
	&m13391_MI,
	&m13392_MI,
	&m13393_MI,
	&m13394_MI,
	NULL
};
static MethodInfo* t2516_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13392_MI,
	&m13393_MI,
	&m13394_MI,
};
static Il2CppInterfaceOffsetPair t2516_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2516_1_0_0;
struct t2516;
extern Il2CppGenericClass t2516_GC;
TypeInfo t2516_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparison`1", "System", t2516_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2516_TI, NULL, t2516_VT, &EmptyCustomAttributesCache, &t2516_TI, &t2516_0_0_0, &t2516_1_0_0, t2516_IOs, &t2516_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2516), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t2524.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2524_TI;
#include "t2524MD.h"

extern MethodInfo m13399_MI;
extern MethodInfo m20934_MI;
struct t20;
#define m20934(__this, p0, method) (t163 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
extern Il2CppType t20_0_0_1;
FieldInfo t2524_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2524_TI, offsetof(t2524, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2524_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2524_TI, offsetof(t2524, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2524_FIs[] =
{
	&t2524_f0_FieldInfo,
	&t2524_f1_FieldInfo,
	NULL
};
extern MethodInfo m13396_MI;
static PropertyInfo t2524____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2524_TI, "System.Collections.IEnumerator.Current", &m13396_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2524____Current_PropertyInfo = 
{
	&t2524_TI, "Current", &m13399_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2524_PIs[] =
{
	&t2524____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2524____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2524_m13395_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13395_GM;
MethodInfo m13395_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2524_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2524_m13395_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13395_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13396_GM;
MethodInfo m13396_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2524_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13396_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13397_GM;
MethodInfo m13397_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2524_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13397_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13398_GM;
MethodInfo m13398_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2524_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13398_GM};
extern Il2CppType t163_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13399_GM;
MethodInfo m13399_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2524_TI, &t163_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13399_GM};
static MethodInfo* t2524_MIs[] =
{
	&m13395_MI,
	&m13396_MI,
	&m13397_MI,
	&m13398_MI,
	&m13399_MI,
	NULL
};
extern MethodInfo m13398_MI;
extern MethodInfo m13397_MI;
static MethodInfo* t2524_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13396_MI,
	&m13398_MI,
	&m13397_MI,
	&m13399_MI,
};
static TypeInfo* t2524_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2510_TI,
};
static Il2CppInterfaceOffsetPair t2524_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2510_TI, 7},
};
extern TypeInfo t163_TI;
static Il2CppRGCTXData t2524_RGCTXData[3] = 
{
	&m13399_MI/* Method Usage */,
	&t163_TI/* Class Usage */,
	&m20934_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2524_0_0_0;
extern Il2CppType t2524_1_0_0;
extern Il2CppGenericClass t2524_GC;
TypeInfo t2524_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2524_MIs, t2524_PIs, t2524_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2524_TI, t2524_ITIs, t2524_VT, &EmptyCustomAttributesCache, &t2524_TI, &t2524_0_0_0, &t2524_1_0_0, t2524_IOs, &t2524_GC, NULL, NULL, NULL, t2524_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2524)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5322_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
extern MethodInfo m27828_MI;
static PropertyInfo t5322____Count_PropertyInfo = 
{
	&t5322_TI, "Count", &m27828_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27829_MI;
static PropertyInfo t5322____IsReadOnly_PropertyInfo = 
{
	&t5322_TI, "IsReadOnly", &m27829_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5322_PIs[] =
{
	&t5322____Count_PropertyInfo,
	&t5322____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27828_GM;
MethodInfo m27828_MI = 
{
	"get_Count", NULL, &t5322_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27828_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27829_GM;
MethodInfo m27829_MI = 
{
	"get_IsReadOnly", NULL, &t5322_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27829_GM};
extern Il2CppType t163_0_0_0;
static ParameterInfo t5322_m27830_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27830_GM;
MethodInfo m27830_MI = 
{
	"Add", NULL, &t5322_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5322_m27830_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27830_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27831_GM;
MethodInfo m27831_MI = 
{
	"Clear", NULL, &t5322_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27831_GM};
extern Il2CppType t163_0_0_0;
static ParameterInfo t5322_m27832_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27832_GM;
MethodInfo m27832_MI = 
{
	"Contains", NULL, &t5322_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5322_m27832_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27832_GM};
extern Il2CppType t2509_0_0_0;
extern Il2CppType t2509_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5322_m27833_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2509_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27833_GM;
MethodInfo m27833_MI = 
{
	"CopyTo", NULL, &t5322_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5322_m27833_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27833_GM};
extern Il2CppType t163_0_0_0;
static ParameterInfo t5322_m27834_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27834_GM;
MethodInfo m27834_MI = 
{
	"Remove", NULL, &t5322_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5322_m27834_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27834_GM};
static MethodInfo* t5322_MIs[] =
{
	&m27828_MI,
	&m27829_MI,
	&m27830_MI,
	&m27831_MI,
	&m27832_MI,
	&m27833_MI,
	&m27834_MI,
	NULL
};
static TypeInfo* t5322_ITIs[] = 
{
	&t603_TI,
	&t5324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5322_0_0_0;
extern Il2CppType t5322_1_0_0;
struct t5322;
extern Il2CppGenericClass t5322_GC;
TypeInfo t5322_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5322_MIs, t5322_PIs, NULL, NULL, NULL, NULL, NULL, &t5322_TI, t5322_ITIs, NULL, &EmptyCustomAttributesCache, &t5322_TI, &t5322_0_0_0, &t5322_1_0_0, NULL, &t5322_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5323_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
extern MethodInfo m27835_MI;
extern MethodInfo m27836_MI;
static PropertyInfo t5323____Item_PropertyInfo = 
{
	&t5323_TI, "Item", &m27835_MI, &m27836_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5323_PIs[] =
{
	&t5323____Item_PropertyInfo,
	NULL
};
extern Il2CppType t163_0_0_0;
static ParameterInfo t5323_m27837_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27837_GM;
MethodInfo m27837_MI = 
{
	"IndexOf", NULL, &t5323_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5323_m27837_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27837_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t163_0_0_0;
static ParameterInfo t5323_m27838_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27838_GM;
MethodInfo m27838_MI = 
{
	"Insert", NULL, &t5323_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5323_m27838_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27838_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5323_m27839_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27839_GM;
MethodInfo m27839_MI = 
{
	"RemoveAt", NULL, &t5323_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5323_m27839_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27839_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5323_m27835_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t163_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27835_GM;
MethodInfo m27835_MI = 
{
	"get_Item", NULL, &t5323_TI, &t163_0_0_0, RuntimeInvoker_t29_t44, t5323_m27835_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27835_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t163_0_0_0;
static ParameterInfo t5323_m27836_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27836_GM;
MethodInfo m27836_MI = 
{
	"set_Item", NULL, &t5323_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5323_m27836_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27836_GM};
static MethodInfo* t5323_MIs[] =
{
	&m27837_MI,
	&m27838_MI,
	&m27839_MI,
	&m27835_MI,
	&m27836_MI,
	NULL
};
static TypeInfo* t5323_ITIs[] = 
{
	&t603_TI,
	&t5322_TI,
	&t5324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5323_0_0_0;
extern Il2CppType t5323_1_0_0;
struct t5323;
extern Il2CppGenericClass t5323_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5323_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5323_MIs, t5323_PIs, NULL, NULL, NULL, NULL, NULL, &t5323_TI, t5323_ITIs, NULL, &t1908__CustomAttributeCache, &t5323_TI, &t5323_0_0_0, &t5323_1_0_0, NULL, &t5323_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13404_MI;


// Metadata Definition System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
extern Il2CppType t2508_0_0_1;
FieldInfo t2511_f0_FieldInfo = 
{
	"parent", &t2508_0_0_1, &t2511_TI, offsetof(t2511, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2511_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2511_TI, offsetof(t2511, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2511_f2_FieldInfo = 
{
	"_version", &t44_0_0_1, &t2511_TI, offsetof(t2511, f2) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2511_FIs[] =
{
	&t2511_f0_FieldInfo,
	&t2511_f1_FieldInfo,
	&t2511_f2_FieldInfo,
	NULL
};
extern MethodInfo m13401_MI;
static PropertyInfo t2511____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2511_TI, "System.Collections.IEnumerator.Current", &m13401_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2511____Current_PropertyInfo = 
{
	&t2511_TI, "Current", &m13404_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2511_PIs[] =
{
	&t2511____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2511____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2508_0_0_0;
static ParameterInfo t2511_m13400_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &t2508_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13400_GM;
MethodInfo m13400_MI = 
{
	".ctor", (methodPointerType)&m11075_gshared, &t2511_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2511_m13400_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13400_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13401_GM;
MethodInfo m13401_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m11076_gshared, &t2511_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13401_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13402_GM;
MethodInfo m13402_MI = 
{
	"Dispose", (methodPointerType)&m11077_gshared, &t2511_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13402_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13403_GM;
MethodInfo m13403_MI = 
{
	"MoveNext", (methodPointerType)&m11078_gshared, &t2511_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13403_GM};
extern Il2CppType t163_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13404_GM;
MethodInfo m13404_MI = 
{
	"get_Current", (methodPointerType)&m11079_gshared, &t2511_TI, &t163_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13404_GM};
static MethodInfo* t2511_MIs[] =
{
	&m13400_MI,
	&m13401_MI,
	&m13402_MI,
	&m13403_MI,
	&m13404_MI,
	NULL
};
extern MethodInfo m13403_MI;
extern MethodInfo m13402_MI;
static MethodInfo* t2511_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13401_MI,
	&m13403_MI,
	&m13402_MI,
	&m13404_MI,
};
static TypeInfo* t2511_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2510_TI,
};
static Il2CppInterfaceOffsetPair t2511_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2510_TI, 7},
};
extern TypeInfo t163_TI;
static Il2CppRGCTXData t2511_RGCTXData[2] = 
{
	&m13404_MI/* Method Usage */,
	&t163_TI/* Class Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType t2511_0_0_0;
extern Il2CppType t2511_1_0_0;
extern Il2CppGenericClass t2511_GC;
extern TypeInfo t717_TI;
TypeInfo t2511_TI = 
{
	&g_System_dll_Image, NULL, "Enumerator", "", t2511_MIs, t2511_PIs, t2511_FIs, NULL, &t110_TI, NULL, &t717_TI, &t2511_TI, t2511_ITIs, t2511_VT, &EmptyCustomAttributesCache, &t2511_TI, &t2511_0_0_0, &t2511_1_0_0, t2511_IOs, &t2511_GC, NULL, NULL, NULL, t2511_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2511)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 3, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t161_m1562_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1562_GM;
MethodInfo m1562_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t161_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t161_m1562_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1562_GM};
extern Il2CppType t163_0_0_0;
static ParameterInfo t161_m13405_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t163_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13405_GM;
MethodInfo m13405_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t161_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t161_m13405_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13405_GM};
extern Il2CppType t163_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t161_m13406_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t163_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13406_GM;
MethodInfo m13406_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t161_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t161_m13406_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13406_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t161_m13407_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13407_GM;
MethodInfo m13407_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t161_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t161_m13407_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13407_GM};
static MethodInfo* t161_MIs[] =
{
	&m1562_MI,
	&m13405_MI,
	&m13406_MI,
	&m13407_MI,
	NULL
};
extern MethodInfo m13406_MI;
extern MethodInfo m13407_MI;
static MethodInfo* t161_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13405_MI,
	&m13406_MI,
	&m13407_MI,
};
static Il2CppInterfaceOffsetPair t161_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t161_1_0_0;
struct t161;
extern Il2CppGenericClass t161_GC;
TypeInfo t161_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t161_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t161_TI, NULL, t161_VT, &EmptyCustomAttributesCache, &t161_TI, &t161_0_0_0, &t161_1_0_0, t161_IOs, &t161_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t161), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t160.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t160_TI;
#include "t160MD.h"

#include "t131.h"
#include "t2525.h"
#include "t4.h"
#include "t16.h"
#include "t22.h"
#include "t203.h"
extern TypeInfo t2525_TI;
extern TypeInfo t131_TI;
extern TypeInfo t340_TI;
extern TypeInfo t22_TI;
#include "t2525MD.h"
#include "t41MD.h"
#include "t4MD.h"
#include "t28MD.h"
#include "t16MD.h"
extern MethodInfo m13409_MI;
extern MethodInfo m1297_MI;
extern MethodInfo m2505_MI;
extern MethodInfo m2539_MI;
extern MethodInfo m1392_MI;
extern MethodInfo m1393_MI;
extern MethodInfo m1492_MI;
extern MethodInfo m13408_MI;
extern MethodInfo m1518_MI;


extern MethodInfo m1560_MI;
 void m1560 (t160 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
 t29 * m13408 (t29 * __this, t131  p0, MethodInfo* method){
	t2525 * V_0 = {0};
	{
		t2525 * L_0 = (t2525 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2525_TI));
		m13409(L_0, &m13409_MI);
		V_0 = L_0;
		V_0->f0 = p0;
		V_0->f5 = p0;
		return V_0;
	}
}
extern MethodInfo m1561_MI;
 void m1561 (t160 * __this, t4 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		return;
	}
}
extern MethodInfo m1595_MI;
 void m1595 (t160 * __this, t131  p0, MethodInfo* method){
	{
		t4 * L_0 = (__this->f0);
		bool L_1 = m1297(NULL, L_0, (t41 *)NULL, &m1297_MI);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		m2505(NULL, (t7*) &_stringLiteral42, &m2505_MI);
		return;
	}

IL_001c:
	{
		t29 * L_2 = (__this->f1);
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		t4 * L_3 = (__this->f0);
		t29 * L_4 = (__this->f1);
		m2539(L_3, L_4, &m2539_MI);
		__this->f1 = (t29 *)NULL;
	}

IL_003f:
	{
		t4 * L_5 = (__this->f0);
		t16 * L_6 = m1392(L_5, &m1392_MI);
		bool L_7 = m1393(L_6, &m1393_MI);
		if (L_7)
		{
			goto IL_0067;
		}
	}
	{
		InterfaceActionInvoker1< float >::Invoke(&m1492_MI, Box(InitializedTypeInfo(&t131_TI), &(*(&p0))), (1.0f));
		return;
	}

IL_0067:
	{
		t29 * L_8 = m13408(NULL, p0, &m13408_MI);
		__this->f1 = L_8;
		t4 * L_9 = (__this->f0);
		t29 * L_10 = (__this->f1);
		m1518(L_9, L_10, &m1518_MI);
		return;
	}
}
// Metadata Definition UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
extern Il2CppType t4_0_0_4;
FieldInfo t160_f0_FieldInfo = 
{
	"m_CoroutineContainer", &t4_0_0_4, &t160_TI, offsetof(t160, f0), &EmptyCustomAttributesCache};
extern Il2CppType t136_0_0_4;
FieldInfo t160_f1_FieldInfo = 
{
	"m_Tween", &t136_0_0_4, &t160_TI, offsetof(t160, f1), &EmptyCustomAttributesCache};
static FieldInfo* t160_FIs[] =
{
	&t160_f0_FieldInfo,
	&t160_f1_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1560_GM;
MethodInfo m1560_MI = 
{
	".ctor", (methodPointerType)&m1560, &t160_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1560_GM};
extern Il2CppType t131_0_0_0;
extern Il2CppType t131_0_0_0;
static ParameterInfo t160_m13408_ParameterInfos[] = 
{
	{"tweenInfo", 0, 134217728, &EmptyCustomAttributesCache, &t131_0_0_0},
};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29_t131 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t135__CustomAttributeCache_m1509;
extern Il2CppGenericMethod m13408_GM;
MethodInfo m13408_MI = 
{
	"Start", (methodPointerType)&m13408, &t160_TI, &t136_0_0_0, RuntimeInvoker_t29_t131, t160_m13408_ParameterInfos, &t135__CustomAttributeCache_m1509, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13408_GM};
extern Il2CppType t4_0_0_0;
extern Il2CppType t4_0_0_0;
static ParameterInfo t160_m1561_ParameterInfos[] = 
{
	{"coroutineContainer", 0, 134217728, &EmptyCustomAttributesCache, &t4_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1561_GM;
MethodInfo m1561_MI = 
{
	"Init", (methodPointerType)&m1561, &t160_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t160_m1561_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1561_GM};
extern Il2CppType t131_0_0_0;
static ParameterInfo t160_m1595_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &t131_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t131 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1595_GM;
MethodInfo m1595_MI = 
{
	"StartTween", (methodPointerType)&m1595, &t160_TI, &t21_0_0_0, RuntimeInvoker_t21_t131, t160_m1595_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1595_GM};
static MethodInfo* t160_MIs[] =
{
	&m1560_MI,
	&m13408_MI,
	&m1561_MI,
	&m1595_MI,
	NULL
};
static MethodInfo* t160_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t160_0_0_0;
extern Il2CppType t160_1_0_0;
struct t160;
extern Il2CppGenericClass t160_GC;
extern CustomAttributesCache t135__CustomAttributeCache_m1509;
TypeInfo t160_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "TweenRunner`1", "UnityEngine.UI.CoroutineTween", t160_MIs, NULL, t160_FIs, NULL, &t29_TI, NULL, NULL, &t160_TI, NULL, t160_VT, &EmptyCustomAttributesCache, &t160_TI, &t160_0_0_0, &t160_1_0_0, NULL, &t160_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t160), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 2, 0, 0, 4, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t344.h"
extern TypeInfo t27_TI;
#include "t328MD.h"
#include "t27MD.h"
extern MethodInfo m1493_MI;
extern MethodInfo m1490_MI;
extern MethodInfo m1515_MI;
extern MethodInfo m2578_MI;
extern MethodInfo m1491_MI;
extern MethodInfo m1643_MI;


 void m13409 (t2525 * __this, MethodInfo* method){
	{
		m1331(__this, &m1331_MI);
		return;
	}
}
extern MethodInfo m13410_MI;
 t29 * m13410 (t2525 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m13411_MI;
 t29 * m13411 (t2525 * __this, MethodInfo* method){
	{
		t29 * L_0 = (__this->f4);
		return L_0;
	}
}
extern MethodInfo m13412_MI;
 bool m13412 (t2525 * __this, MethodInfo* method){
	uint32_t V_0 = 0;
	bool V_1 = false;
	float G_B7_0 = 0.0f;
	t2525 * G_B7_1 = {0};
	float G_B6_0 = 0.0f;
	t2525 * G_B6_1 = {0};
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	t2525 * G_B8_2 = {0};
	{
		int32_t L_0 = (__this->f3);
		V_0 = L_0;
		__this->f3 = (-1);
		if (V_0 == 0)
		{
			goto IL_0021;
		}
		if (V_0 == 1)
		{
			goto IL_00cb;
		}
	}
	{
		goto IL_0104;
	}

IL_0021:
	{
		t131 * L_1 = &(__this->f0);
		bool L_2 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m1493_MI, Box(InitializedTypeInfo(&t131_TI), &(*L_1)));
		if (L_2)
		{
			goto IL_003c;
		}
	}
	{
		goto IL_0104;
	}

IL_003c:
	{
		__this->f1 = (0.0f);
		goto IL_00cb;
	}

IL_004c:
	{
		float L_3 = (__this->f1);
		t131 * L_4 = &(__this->f0);
		bool L_5 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&m1490_MI, Box(InitializedTypeInfo(&t131_TI), &(*L_4)));
		G_B6_0 = L_3;
		G_B6_1 = ((t2525 *)(__this));
		if (!L_5)
		{
			G_B7_0 = L_3;
			G_B7_1 = ((t2525 *)(__this));
			goto IL_0073;
		}
	}
	{
		float L_6 = m1515(NULL, &m1515_MI);
		G_B8_0 = L_6;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((t2525 *)(G_B6_1));
		goto IL_0078;
	}

IL_0073:
	{
		float L_7 = m2578(NULL, &m2578_MI);
		G_B8_0 = L_7;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((t2525 *)(G_B7_1));
	}

IL_0078:
	{
		G_B8_2->f1 = ((float)(G_B8_1+G_B8_0));
		float L_8 = (__this->f1);
		t131 * L_9 = &(__this->f0);
		float L_10 = (float)InterfaceFuncInvoker0< float >::Invoke(&m1491_MI, Box(InitializedTypeInfo(&t131_TI), &(*L_9)));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t27_TI));
		float L_11 = m1643(NULL, ((float)((float)L_8/(float)L_10)), &m1643_MI);
		__this->f2 = L_11;
		t131 * L_12 = &(__this->f0);
		float L_13 = (__this->f2);
		InterfaceActionInvoker1< float >::Invoke(&m1492_MI, Box(InitializedTypeInfo(&t131_TI), &(*L_12)), L_13);
		__this->f4 = NULL;
		__this->f3 = 1;
		goto IL_0106;
	}

IL_00cb:
	{
		float L_14 = (__this->f1);
		t131 * L_15 = &(__this->f0);
		float L_16 = (float)InterfaceFuncInvoker0< float >::Invoke(&m1491_MI, Box(InitializedTypeInfo(&t131_TI), &(*L_15)));
		if ((((float)L_14) < ((float)L_16)))
		{
			goto IL_004c;
		}
	}
	{
		t131 * L_17 = &(__this->f0);
		InterfaceActionInvoker1< float >::Invoke(&m1492_MI, Box(InitializedTypeInfo(&t131_TI), &(*L_17)), (1.0f));
		__this->f3 = (-1);
	}

IL_0104:
	{
		return 0;
	}

IL_0106:
	{
		return 1;
	}
	// Dead block : IL_0108: ldloc.1
}
extern MethodInfo m13413_MI;
 void m13413 (t2525 * __this, MethodInfo* method){
	{
		__this->f3 = (-1);
		return;
	}
}
extern MethodInfo m13414_MI;
 void m13414 (t2525 * __this, MethodInfo* method){
	{
		t345 * L_0 = (t345 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t345_TI));
		m1516(L_0, &m1516_MI);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// Metadata Definition UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
extern Il2CppType t131_0_0_3;
FieldInfo t2525_f0_FieldInfo = 
{
	"tweenInfo", &t131_0_0_3, &t2525_TI, offsetof(t2525, f0), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_3;
FieldInfo t2525_f1_FieldInfo = 
{
	"<elapsedTime>__0", &t22_0_0_3, &t2525_TI, offsetof(t2525, f1), &EmptyCustomAttributesCache};
extern Il2CppType t22_0_0_3;
FieldInfo t2525_f2_FieldInfo = 
{
	"<percentage>__1", &t22_0_0_3, &t2525_TI, offsetof(t2525, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_3;
FieldInfo t2525_f3_FieldInfo = 
{
	"$PC", &t44_0_0_3, &t2525_TI, offsetof(t2525, f3), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_3;
FieldInfo t2525_f4_FieldInfo = 
{
	"$current", &t29_0_0_3, &t2525_TI, offsetof(t2525, f4), &EmptyCustomAttributesCache};
extern Il2CppType t131_0_0_3;
FieldInfo t2525_f5_FieldInfo = 
{
	"<$>tweenInfo", &t131_0_0_3, &t2525_TI, offsetof(t2525, f5), &EmptyCustomAttributesCache};
static FieldInfo* t2525_FIs[] =
{
	&t2525_f0_FieldInfo,
	&t2525_f1_FieldInfo,
	&t2525_f2_FieldInfo,
	&t2525_f3_FieldInfo,
	&t2525_f4_FieldInfo,
	&t2525_f5_FieldInfo,
	NULL
};
static PropertyInfo t2525____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo = 
{
	&t2525_TI, "System.Collections.Generic.IEnumerator<object>.Current", &m13410_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2525____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2525_TI, "System.Collections.IEnumerator.Current", &m13411_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2525_PIs[] =
{
	&t2525____System_Collections_Generic_IEnumeratorU3CobjectU3E_Current_PropertyInfo,
	&t2525____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13409_GM;
MethodInfo m13409_MI = 
{
	".ctor", (methodPointerType)&m13409, &t2525_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13409_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t134__CustomAttributeCache_m1502;
extern Il2CppGenericMethod m13410_GM;
MethodInfo m13410_MI = 
{
	"System.Collections.Generic.IEnumerator<object>.get_Current", (methodPointerType)&m13410, &t2525_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &t134__CustomAttributeCache_m1502, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13410_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t134__CustomAttributeCache_m1503;
extern Il2CppGenericMethod m13411_GM;
MethodInfo m13411_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13411, &t2525_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &t134__CustomAttributeCache_m1503, 2529, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13411_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13412_GM;
MethodInfo m13412_MI = 
{
	"MoveNext", (methodPointerType)&m13412, &t2525_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13412_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t134__CustomAttributeCache_m1505;
extern Il2CppGenericMethod m13413_GM;
MethodInfo m13413_MI = 
{
	"Dispose", (methodPointerType)&m13413, &t2525_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t134__CustomAttributeCache_m1505, 486, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13413_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache t134__CustomAttributeCache_m1506;
extern Il2CppGenericMethod m13414_GM;
MethodInfo m13414_MI = 
{
	"Reset", (methodPointerType)&m13414, &t2525_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &t134__CustomAttributeCache_m1506, 486, 0, 8, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13414_GM};
static MethodInfo* t2525_MIs[] =
{
	&m13409_MI,
	&m13410_MI,
	&m13411_MI,
	&m13412_MI,
	&m13413_MI,
	&m13414_MI,
	NULL
};
static MethodInfo* t2525_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13413_MI,
	&m13411_MI,
	&m13412_MI,
	&m13410_MI,
	&m13414_MI,
};
extern TypeInfo t346_TI;
static TypeInfo* t2525_ITIs[] = 
{
	&t324_TI,
	&t136_TI,
	&t346_TI,
};
static Il2CppInterfaceOffsetPair t2525_IOs[] = 
{
	{ &t324_TI, 4},
	{ &t136_TI, 5},
	{ &t346_TI, 7},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType t2525_0_0_0;
extern Il2CppType t2525_1_0_0;
struct t2525;
extern Il2CppGenericClass t2525_GC;
extern TypeInfo t135_TI;
extern CustomAttributesCache t134__CustomAttributeCache;
extern CustomAttributesCache t134__CustomAttributeCache_m1502;
extern CustomAttributesCache t134__CustomAttributeCache_m1503;
extern CustomAttributesCache t134__CustomAttributeCache_m1505;
extern CustomAttributesCache t134__CustomAttributeCache_m1506;
TypeInfo t2525_TI = 
{
	&g_UnityEngine_UI_dll_Image, NULL, "<Start>c__Iterator0", "", t2525_MIs, t2525_PIs, t2525_FIs, NULL, &t29_TI, NULL, &t135_TI, &t2525_TI, t2525_ITIs, t2525_VT, &t134__CustomAttributeCache, &t2525_TI, &t2525_0_0_0, &t2525_1_0_0, t2525_IOs, &t2525_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2525), 0, -1, 0, 0, -1, 1048835, 0, false, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 6, 0, 0, 9, 3, 3};
#include "t278.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t278_TI;
#include "t278MD.h"

#include "t3.h"
#include "t2533.h"
#include "t2530.h"
#include "t2531.h"
#include "t2539.h"
#include "t2532.h"
extern TypeInfo t3_TI;
extern TypeInfo t2526_TI;
extern TypeInfo t2533_TI;
extern TypeInfo t2528_TI;
extern TypeInfo t2529_TI;
extern TypeInfo t2527_TI;
extern TypeInfo t2530_TI;
extern TypeInfo t2531_TI;
extern TypeInfo t2539_TI;
#include "t2530MD.h"
#include "t2531MD.h"
#include "t2533MD.h"
#include "t2539MD.h"
extern MethodInfo m1567_MI;
extern MethodInfo m13460_MI;
extern MethodInfo m20959_MI;
extern MethodInfo m13447_MI;
extern MethodInfo m13444_MI;
extern MethodInfo m13433_MI;
extern MethodInfo m13439_MI;
extern MethodInfo m13445_MI;
extern MethodInfo m13448_MI;
extern MethodInfo m13450_MI;
extern MethodInfo m13434_MI;
extern MethodInfo m13458_MI;
extern MethodInfo m13459_MI;
extern MethodInfo m27840_MI;
extern MethodInfo m27841_MI;
extern MethodInfo m27842_MI;
extern MethodInfo m27843_MI;
extern MethodInfo m13449_MI;
extern MethodInfo m13435_MI;
extern MethodInfo m13436_MI;
extern MethodInfo m13472_MI;
extern MethodInfo m20961_MI;
extern MethodInfo m13442_MI;
extern MethodInfo m13443_MI;
extern MethodInfo m13547_MI;
extern MethodInfo m13466_MI;
extern MethodInfo m13446_MI;
extern MethodInfo m13452_MI;
extern MethodInfo m13553_MI;
extern MethodInfo m20963_MI;
extern MethodInfo m20971_MI;
struct t20;
#define m20959(__this, p0, p1, method) (void)m19788_gshared((t29 *)__this, (t316**)p0, (int32_t)p1, method)
struct t20;
#include "t2537.h"
struct t20;
#include "t2001.h"
 int32_t m10120_gshared (t29 * __this, t316* p0, t29 * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define m10120(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
#define m20961(__this, p0, p1, p2, p3, method) (int32_t)m10120_gshared((t29 *)__this, (t316*)p0, (t29 *)p1, (int32_t)p2, (int32_t)p3, method)
struct t20;
struct t20;
 void m19857_gshared (t29 * __this, t316* p0, int32_t p1, int32_t p2, t29* p3, MethodInfo* method);
#define m19857(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
#define m20963(__this, p0, p1, p2, p3, method) (void)m19857_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (int32_t)p2, (t29*)p3, method)
struct t20;
struct t20;
#include "t2181.h"
 void m19964_gshared (t29 * __this, t316* p0, int32_t p1, t2181 * p2, MethodInfo* method);
#define m19964(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)
#define m20971(__this, p0, p1, p2, method) (void)m19964_gshared((t29 *)__this, (t316*)p0, (int32_t)p1, (t2181 *)p2, method)


 t2533  m13444 (t278 * __this, MethodInfo* method){
	{
		t2533  L_0 = {0};
		m13466(&L_0, __this, &m13466_MI);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.Canvas>
extern Il2CppType t44_0_0_32849;
FieldInfo t278_f0_FieldInfo = 
{
	"DefaultCapacity", &t44_0_0_32849, &t278_TI, 0 /*field is const -> no data*/, &EmptyCustomAttributesCache};
extern Il2CppType t2526_0_0_1;
FieldInfo t278_f1_FieldInfo = 
{
	"_items", &t2526_0_0_1, &t278_TI, offsetof(t278, f1), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t278_f2_FieldInfo = 
{
	"_size", &t44_0_0_1, &t278_TI, offsetof(t278, f2), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t278_f3_FieldInfo = 
{
	"_version", &t44_0_0_1, &t278_TI, offsetof(t278, f3), &EmptyCustomAttributesCache};
extern Il2CppType t2526_0_0_49;
FieldInfo t278_f4_FieldInfo = 
{
	"EmptyArray", &t2526_0_0_49, &t278_TI, offsetof(t278_SFs, f4), &EmptyCustomAttributesCache};
static FieldInfo* t278_FIs[] =
{
	&t278_f0_FieldInfo,
	&t278_f1_FieldInfo,
	&t278_f2_FieldInfo,
	&t278_f3_FieldInfo,
	&t278_f4_FieldInfo,
	NULL
};
static const int32_t t278_f0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry t278_f0_DefaultValue = 
{
	&t278_f0_FieldInfo, { (char*)&t278_f0_DefaultValueData, &t44_0_0_0 }};
static Il2CppFieldDefaultValueEntry* t278_FDVs[] = 
{
	&t278_f0_DefaultValue,
	NULL
};
extern MethodInfo m13426_MI;
static PropertyInfo t278____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t278_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m13426_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13427_MI;
static PropertyInfo t278____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t278_TI, "System.Collections.ICollection.IsSynchronized", &m13427_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13428_MI;
static PropertyInfo t278____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t278_TI, "System.Collections.ICollection.SyncRoot", &m13428_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13429_MI;
static PropertyInfo t278____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t278_TI, "System.Collections.IList.IsFixedSize", &m13429_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13430_MI;
static PropertyInfo t278____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t278_TI, "System.Collections.IList.IsReadOnly", &m13430_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13431_MI;
extern MethodInfo m13432_MI;
static PropertyInfo t278____System_Collections_IList_Item_PropertyInfo = 
{
	&t278_TI, "System.Collections.IList.Item", &m13431_MI, &m13432_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo t278____Capacity_PropertyInfo = 
{
	&t278_TI, "Capacity", &m13458_MI, &m13459_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m1566_MI;
static PropertyInfo t278____Count_PropertyInfo = 
{
	&t278_TI, "Count", &m1566_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t278____Item_PropertyInfo = 
{
	&t278_TI, "Item", &m1567_MI, &m13460_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t278_PIs[] =
{
	&t278____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t278____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t278____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t278____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t278____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t278____System_Collections_IList_Item_PropertyInfo,
	&t278____Capacity_PropertyInfo,
	&t278____Count_PropertyInfo,
	&t278____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13415_GM;
MethodInfo m13415_MI = 
{
	".ctor", (methodPointerType)&m10537_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13415_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t278_m13416_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13416_GM;
MethodInfo m13416_MI = 
{
	".ctor", (methodPointerType)&m10539_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t278_m13416_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13416_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13417_GM;
MethodInfo m13417_MI = 
{
	".cctor", (methodPointerType)&m10541_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13417_GM};
extern Il2CppType t2527_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13418_GM;
MethodInfo m13418_MI = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator", (methodPointerType)&m10543_gshared, &t278_TI, &t2527_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 27, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13418_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t278_m13419_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13419_GM;
MethodInfo m13419_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10545_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t278_m13419_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13419_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13420_GM;
MethodInfo m13420_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10547_gshared, &t278_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13420_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t278_m13421_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13421_GM;
MethodInfo m13421_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10549_gshared, &t278_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t278_m13421_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13421_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t278_m13422_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13422_GM;
MethodInfo m13422_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10551_gshared, &t278_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t278_m13422_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13422_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t278_m13423_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13423_GM;
MethodInfo m13423_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10553_gshared, &t278_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t278_m13423_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13423_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t278_m13424_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13424_GM;
MethodInfo m13424_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10555_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t278_m13424_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13424_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t278_m13425_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13425_GM;
MethodInfo m13425_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10557_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t278_m13425_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13425_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13426_GM;
MethodInfo m13426_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10559_gshared, &t278_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13426_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13427_GM;
MethodInfo m13427_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10561_gshared, &t278_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13427_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13428_GM;
MethodInfo m13428_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10563_gshared, &t278_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13428_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13429_GM;
MethodInfo m13429_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10565_gshared, &t278_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13429_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13430_GM;
MethodInfo m13430_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10567_gshared, &t278_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13430_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t278_m13431_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13431_GM;
MethodInfo m13431_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10569_gshared, &t278_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t278_m13431_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13431_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t278_m13432_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13432_GM;
MethodInfo m13432_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10571_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t278_m13432_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13432_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t278_m13433_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13433_GM;
MethodInfo m13433_MI = 
{
	"Add", (methodPointerType)&m10573_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t278_m13433_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13433_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t278_m13434_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13434_GM;
MethodInfo m13434_MI = 
{
	"GrowIfNeeded", (methodPointerType)&m10575_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t278_m13434_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13434_GM};
extern Il2CppType t2528_0_0_0;
extern Il2CppType t2528_0_0_0;
static ParameterInfo t278_m13435_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2528_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13435_GM;
MethodInfo m13435_MI = 
{
	"AddCollection", (methodPointerType)&m10577_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t278_m13435_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13435_GM};
extern Il2CppType t2529_0_0_0;
extern Il2CppType t2529_0_0_0;
static ParameterInfo t278_m13436_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &t2529_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13436_GM;
MethodInfo m13436_MI = 
{
	"AddEnumerable", (methodPointerType)&m10579_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t278_m13436_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13436_GM};
extern Il2CppType t2529_0_0_0;
static ParameterInfo t278_m13437_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2529_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13437_GM;
MethodInfo m13437_MI = 
{
	"AddRange", (methodPointerType)&m10581_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t278_m13437_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13437_GM};
extern Il2CppType t2530_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13438_GM;
MethodInfo m13438_MI = 
{
	"AsReadOnly", (methodPointerType)&m10583_gshared, &t278_TI, &t2530_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13438_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m2030_GM;
MethodInfo m2030_MI = 
{
	"Clear", (methodPointerType)&m10585_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m2030_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t278_m13439_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13439_GM;
MethodInfo m13439_MI = 
{
	"Contains", (methodPointerType)&m10587_gshared, &t278_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t278_m13439_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13439_GM};
extern Il2CppType t2526_0_0_0;
extern Il2CppType t2526_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t278_m13440_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2526_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13440_GM;
MethodInfo m13440_MI = 
{
	"CopyTo", (methodPointerType)&m10589_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t278_m13440_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13440_GM};
extern Il2CppType t2531_0_0_0;
extern Il2CppType t2531_0_0_0;
static ParameterInfo t278_m13441_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2531_0_0_0},
};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13441_GM;
MethodInfo m13441_MI = 
{
	"Find", (methodPointerType)&m10591_gshared, &t278_TI, &t3_0_0_0, RuntimeInvoker_t29_t29, t278_m13441_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13441_GM};
extern Il2CppType t2531_0_0_0;
static ParameterInfo t278_m13442_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2531_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13442_GM;
MethodInfo m13442_MI = 
{
	"CheckMatch", (methodPointerType)&m10593_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t278_m13442_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13442_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2531_0_0_0;
static ParameterInfo t278_m13443_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &t2531_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13443_GM;
MethodInfo m13443_MI = 
{
	"GetIndex", (methodPointerType)&m10595_gshared, &t278_TI, &t44_0_0_0, RuntimeInvoker_t44_t44_t44_t29, t278_m13443_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13443_GM};
extern Il2CppType t2533_0_0_0;
extern void* RuntimeInvoker_t2533 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13444_GM;
MethodInfo m13444_MI = 
{
	"GetEnumerator", (methodPointerType)&m13444, &t278_TI, &t2533_0_0_0, RuntimeInvoker_t2533, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13444_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t278_m13445_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13445_GM;
MethodInfo m13445_MI = 
{
	"IndexOf", (methodPointerType)&m10598_gshared, &t278_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t278_m13445_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13445_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t278_m13446_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13446_GM;
MethodInfo m13446_MI = 
{
	"Shift", (methodPointerType)&m10600_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t278_m13446_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13446_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t278_m13447_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13447_GM;
MethodInfo m13447_MI = 
{
	"CheckIndex", (methodPointerType)&m10602_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t278_m13447_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13447_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t278_m13448_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13448_GM;
MethodInfo m13448_MI = 
{
	"Insert", (methodPointerType)&m10604_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t278_m13448_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13448_GM};
extern Il2CppType t2529_0_0_0;
static ParameterInfo t278_m13449_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &t2529_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13449_GM;
MethodInfo m13449_MI = 
{
	"CheckCollection", (methodPointerType)&m10606_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t278_m13449_ParameterInfos, &EmptyCustomAttributesCache, 129, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13449_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t278_m13450_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13450_GM;
MethodInfo m13450_MI = 
{
	"Remove", (methodPointerType)&m10608_gshared, &t278_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t278_m13450_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13450_GM};
extern Il2CppType t2531_0_0_0;
static ParameterInfo t278_m13451_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &t2531_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13451_GM;
MethodInfo m13451_MI = 
{
	"RemoveAll", (methodPointerType)&m10610_gshared, &t278_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t278_m13451_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13451_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t278_m13452_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13452_GM;
MethodInfo m13452_MI = 
{
	"RemoveAt", (methodPointerType)&m10611_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t278_m13452_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13452_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13453_GM;
MethodInfo m13453_MI = 
{
	"Reverse", (methodPointerType)&m10613_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13453_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13454_GM;
MethodInfo m13454_MI = 
{
	"Sort", (methodPointerType)&m10615_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13454_GM};
extern Il2CppType t2532_0_0_0;
extern Il2CppType t2532_0_0_0;
static ParameterInfo t278_m13455_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &t2532_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13455_GM;
MethodInfo m13455_MI = 
{
	"Sort", (methodPointerType)&m10617_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t278_m13455_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13455_GM};
extern Il2CppType t2526_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13456_GM;
MethodInfo m13456_MI = 
{
	"ToArray", (methodPointerType)&m10619_gshared, &t278_TI, &t2526_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13456_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13457_GM;
MethodInfo m13457_MI = 
{
	"TrimExcess", (methodPointerType)&m10621_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 134, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13457_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13458_GM;
MethodInfo m13458_MI = 
{
	"get_Capacity", (methodPointerType)&m10623_gshared, &t278_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2182, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13458_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t278_m13459_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13459_GM;
MethodInfo m13459_MI = 
{
	"set_Capacity", (methodPointerType)&m10625_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t278_m13459_ParameterInfos, &EmptyCustomAttributesCache, 2182, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13459_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1566_GM;
MethodInfo m1566_MI = 
{
	"get_Count", (methodPointerType)&m10626_gshared, &t278_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1566_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t278_m1567_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1567_GM;
MethodInfo m1567_MI = 
{
	"get_Item", (methodPointerType)&m10627_gshared, &t278_TI, &t3_0_0_0, RuntimeInvoker_t29_t44, t278_m1567_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1567_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t278_m13460_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13460_GM;
MethodInfo m13460_MI = 
{
	"set_Item", (methodPointerType)&m10629_gshared, &t278_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t278_m13460_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 32, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13460_GM};
static MethodInfo* t278_MIs[] =
{
	&m13415_MI,
	&m13416_MI,
	&m13417_MI,
	&m13418_MI,
	&m13419_MI,
	&m13420_MI,
	&m13421_MI,
	&m13422_MI,
	&m13423_MI,
	&m13424_MI,
	&m13425_MI,
	&m13426_MI,
	&m13427_MI,
	&m13428_MI,
	&m13429_MI,
	&m13430_MI,
	&m13431_MI,
	&m13432_MI,
	&m13433_MI,
	&m13434_MI,
	&m13435_MI,
	&m13436_MI,
	&m13437_MI,
	&m13438_MI,
	&m2030_MI,
	&m13439_MI,
	&m13440_MI,
	&m13441_MI,
	&m13442_MI,
	&m13443_MI,
	&m13444_MI,
	&m13445_MI,
	&m13446_MI,
	&m13447_MI,
	&m13448_MI,
	&m13449_MI,
	&m13450_MI,
	&m13451_MI,
	&m13452_MI,
	&m13453_MI,
	&m13454_MI,
	&m13455_MI,
	&m13456_MI,
	&m13457_MI,
	&m13458_MI,
	&m13459_MI,
	&m1566_MI,
	&m1567_MI,
	&m13460_MI,
	NULL
};
extern MethodInfo m13420_MI;
extern MethodInfo m13419_MI;
extern MethodInfo m13421_MI;
extern MethodInfo m2030_MI;
extern MethodInfo m13422_MI;
extern MethodInfo m13423_MI;
extern MethodInfo m13424_MI;
extern MethodInfo m13425_MI;
extern MethodInfo m13440_MI;
extern MethodInfo m13418_MI;
static MethodInfo* t278_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13420_MI,
	&m1566_MI,
	&m13427_MI,
	&m13428_MI,
	&m13419_MI,
	&m13429_MI,
	&m13430_MI,
	&m13431_MI,
	&m13432_MI,
	&m13421_MI,
	&m2030_MI,
	&m13422_MI,
	&m13423_MI,
	&m13424_MI,
	&m13425_MI,
	&m13452_MI,
	&m1566_MI,
	&m13426_MI,
	&m13433_MI,
	&m2030_MI,
	&m13439_MI,
	&m13440_MI,
	&m13450_MI,
	&m13418_MI,
	&m13445_MI,
	&m13448_MI,
	&m13452_MI,
	&m1567_MI,
	&m13460_MI,
};
extern TypeInfo t2535_TI;
static TypeInfo* t278_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2528_TI,
	&t2529_TI,
	&t2535_TI,
};
static Il2CppInterfaceOffsetPair t278_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2528_TI, 20},
	{ &t2529_TI, 27},
	{ &t2535_TI, 28},
};
extern TypeInfo t278_TI;
extern TypeInfo t2526_TI;
extern TypeInfo t2533_TI;
extern TypeInfo t3_TI;
extern TypeInfo t2528_TI;
extern TypeInfo t2530_TI;
static Il2CppRGCTXData t278_RGCTXData[37] = 
{
	&t278_TI/* Static Usage */,
	&t2526_TI/* Array Usage */,
	&m13444_MI/* Method Usage */,
	&t2533_TI/* Class Usage */,
	&t3_TI/* Class Usage */,
	&m13433_MI/* Method Usage */,
	&m13439_MI/* Method Usage */,
	&m13445_MI/* Method Usage */,
	&m13447_MI/* Method Usage */,
	&m13448_MI/* Method Usage */,
	&m13450_MI/* Method Usage */,
	&m1567_MI/* Method Usage */,
	&m13460_MI/* Method Usage */,
	&m13434_MI/* Method Usage */,
	&m13458_MI/* Method Usage */,
	&m13459_MI/* Method Usage */,
	&m27840_MI/* Method Usage */,
	&m27841_MI/* Method Usage */,
	&m27842_MI/* Method Usage */,
	&m27843_MI/* Method Usage */,
	&m13449_MI/* Method Usage */,
	&t2528_TI/* Class Usage */,
	&m13435_MI/* Method Usage */,
	&m13436_MI/* Method Usage */,
	&t2530_TI/* Class Usage */,
	&m13472_MI/* Method Usage */,
	&m20961_MI/* Method Usage */,
	&m13442_MI/* Method Usage */,
	&m13443_MI/* Method Usage */,
	&m13547_MI/* Method Usage */,
	&m13466_MI/* Method Usage */,
	&m13446_MI/* Method Usage */,
	&m13452_MI/* Method Usage */,
	&m13553_MI/* Method Usage */,
	&m20963_MI/* Method Usage */,
	&m20971_MI/* Method Usage */,
	&m20959_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t278_0_0_0;
extern Il2CppType t278_1_0_0;
struct t278;
extern Il2CppGenericClass t278_GC;
extern CustomAttributesCache t1261__CustomAttributeCache;
TypeInfo t278_TI = 
{
	&g_mscorlib_dll_Image, NULL, "List`1", "System.Collections.Generic", t278_MIs, t278_PIs, t278_FIs, NULL, &t29_TI, NULL, NULL, &t278_TI, t278_ITIs, t278_VT, &t1261__CustomAttributeCache, &t278_TI, &t278_0_0_0, &t278_1_0_0, t278_IOs, &t278_GC, NULL, t278_FDVs, NULL, t278_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t278), 0, -1, sizeof(t278_SFs), 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, true, false, false, 49, 9, 5, 0, 0, 33, 6, 6};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Canvas>
static PropertyInfo t2528____Count_PropertyInfo = 
{
	&t2528_TI, "Count", &m27840_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m27844_MI;
static PropertyInfo t2528____IsReadOnly_PropertyInfo = 
{
	&t2528_TI, "IsReadOnly", &m27844_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2528_PIs[] =
{
	&t2528____Count_PropertyInfo,
	&t2528____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27840_GM;
MethodInfo m27840_MI = 
{
	"get_Count", NULL, &t2528_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27840_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27844_GM;
MethodInfo m27844_MI = 
{
	"get_IsReadOnly", NULL, &t2528_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27844_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2528_m27845_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27845_GM;
MethodInfo m27845_MI = 
{
	"Add", NULL, &t2528_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2528_m27845_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27845_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27846_GM;
MethodInfo m27846_MI = 
{
	"Clear", NULL, &t2528_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27846_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2528_m27847_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27847_GM;
MethodInfo m27847_MI = 
{
	"Contains", NULL, &t2528_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2528_m27847_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27847_GM};
extern Il2CppType t2526_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2528_m27841_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2526_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27841_GM;
MethodInfo m27841_MI = 
{
	"CopyTo", NULL, &t2528_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2528_m27841_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27841_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2528_m27848_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27848_GM;
MethodInfo m27848_MI = 
{
	"Remove", NULL, &t2528_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2528_m27848_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27848_GM};
static MethodInfo* t2528_MIs[] =
{
	&m27840_MI,
	&m27844_MI,
	&m27845_MI,
	&m27846_MI,
	&m27847_MI,
	&m27841_MI,
	&m27848_MI,
	NULL
};
static TypeInfo* t2528_ITIs[] = 
{
	&t603_TI,
	&t2529_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2528_1_0_0;
struct t2528;
extern Il2CppGenericClass t2528_GC;
TypeInfo t2528_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t2528_MIs, t2528_PIs, NULL, NULL, NULL, NULL, NULL, &t2528_TI, t2528_ITIs, NULL, &EmptyCustomAttributesCache, &t2528_TI, &t2528_0_0_0, &t2528_1_0_0, NULL, &t2528_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Canvas>
extern Il2CppType t2527_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27842_GM;
MethodInfo m27842_MI = 
{
	"GetEnumerator", NULL, &t2529_TI, &t2527_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27842_GM};
static MethodInfo* t2529_MIs[] =
{
	&m27842_MI,
	NULL
};
static TypeInfo* t2529_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2529_1_0_0;
struct t2529;
extern Il2CppGenericClass t2529_GC;
TypeInfo t2529_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t2529_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2529_TI, t2529_ITIs, NULL, &EmptyCustomAttributesCache, &t2529_TI, &t2529_0_0_0, &t2529_1_0_0, NULL, &t2529_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Canvas>
static PropertyInfo t2527____Current_PropertyInfo = 
{
	&t2527_TI, "Current", &m27843_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2527_PIs[] =
{
	&t2527____Current_PropertyInfo,
	NULL
};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27843_GM;
MethodInfo m27843_MI = 
{
	"get_Current", NULL, &t2527_TI, &t3_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m27843_GM};
static MethodInfo* t2527_MIs[] =
{
	&m27843_MI,
	NULL
};
static TypeInfo* t2527_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2527_0_0_0;
extern Il2CppType t2527_1_0_0;
struct t2527;
extern Il2CppGenericClass t2527_GC;
TypeInfo t2527_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t2527_MIs, t2527_PIs, NULL, NULL, NULL, NULL, NULL, &t2527_TI, t2527_ITIs, NULL, &EmptyCustomAttributesCache, &t2527_TI, &t2527_0_0_0, &t2527_1_0_0, NULL, &t2527_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2534.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2534_TI;
#include "t2534MD.h"

extern MethodInfo m13465_MI;
extern MethodInfo m20948_MI;
struct t20;
#define m20948(__this, p0, method) (t3 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Canvas>
extern Il2CppType t20_0_0_1;
FieldInfo t2534_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2534_TI, offsetof(t2534, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2534_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2534_TI, offsetof(t2534, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2534_FIs[] =
{
	&t2534_f0_FieldInfo,
	&t2534_f1_FieldInfo,
	NULL
};
extern MethodInfo m13462_MI;
static PropertyInfo t2534____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2534_TI, "System.Collections.IEnumerator.Current", &m13462_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2534____Current_PropertyInfo = 
{
	&t2534_TI, "Current", &m13465_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2534_PIs[] =
{
	&t2534____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2534____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2534_m13461_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13461_GM;
MethodInfo m13461_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2534_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2534_m13461_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13461_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13462_GM;
MethodInfo m13462_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2534_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13462_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13463_GM;
MethodInfo m13463_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2534_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13463_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13464_GM;
MethodInfo m13464_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2534_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13464_GM};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13465_GM;
MethodInfo m13465_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2534_TI, &t3_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13465_GM};
static MethodInfo* t2534_MIs[] =
{
	&m13461_MI,
	&m13462_MI,
	&m13463_MI,
	&m13464_MI,
	&m13465_MI,
	NULL
};
extern MethodInfo m13464_MI;
extern MethodInfo m13463_MI;
static MethodInfo* t2534_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13462_MI,
	&m13464_MI,
	&m13463_MI,
	&m13465_MI,
};
static TypeInfo* t2534_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2527_TI,
};
static Il2CppInterfaceOffsetPair t2534_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2527_TI, 7},
};
extern TypeInfo t3_TI;
static Il2CppRGCTXData t2534_RGCTXData[3] = 
{
	&m13465_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m20948_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2534_0_0_0;
extern Il2CppType t2534_1_0_0;
extern Il2CppGenericClass t2534_GC;
TypeInfo t2534_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2534_MIs, t2534_PIs, t2534_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2534_TI, t2534_ITIs, t2534_VT, &EmptyCustomAttributesCache, &t2534_TI, &t2534_0_0_0, &t2534_1_0_0, t2534_IOs, &t2534_GC, NULL, NULL, NULL, t2534_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2534)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Canvas>
extern MethodInfo m27849_MI;
extern MethodInfo m27850_MI;
static PropertyInfo t2535____Item_PropertyInfo = 
{
	&t2535_TI, "Item", &m27849_MI, &m27850_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2535_PIs[] =
{
	&t2535____Item_PropertyInfo,
	NULL
};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2535_m27851_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27851_GM;
MethodInfo m27851_MI = 
{
	"IndexOf", NULL, &t2535_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2535_m27851_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27851_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2535_m27852_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27852_GM;
MethodInfo m27852_MI = 
{
	"Insert", NULL, &t2535_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2535_m27852_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27852_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2535_m27853_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27853_GM;
MethodInfo m27853_MI = 
{
	"RemoveAt", NULL, &t2535_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2535_m27853_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27853_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2535_m27849_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27849_GM;
MethodInfo m27849_MI = 
{
	"get_Item", NULL, &t2535_TI, &t3_0_0_0, RuntimeInvoker_t29_t44, t2535_m27849_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27849_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2535_m27850_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27850_GM;
MethodInfo m27850_MI = 
{
	"set_Item", NULL, &t2535_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2535_m27850_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27850_GM};
static MethodInfo* t2535_MIs[] =
{
	&m27851_MI,
	&m27852_MI,
	&m27853_MI,
	&m27849_MI,
	&m27850_MI,
	NULL
};
static TypeInfo* t2535_ITIs[] = 
{
	&t603_TI,
	&t2528_TI,
	&t2529_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2535_0_0_0;
extern Il2CppType t2535_1_0_0;
struct t2535;
extern Il2CppGenericClass t2535_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t2535_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t2535_MIs, t2535_PIs, NULL, NULL, NULL, NULL, NULL, &t2535_TI, t2535_ITIs, NULL, &t1908__CustomAttributeCache, &t2535_TI, &t2535_0_0_0, &t2535_1_0_0, NULL, &t2535_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13469_MI;


// Metadata Definition System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>
extern Il2CppType t278_0_0_1;
FieldInfo t2533_f0_FieldInfo = 
{
	"l", &t278_0_0_1, &t2533_TI, offsetof(t2533, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2533_f1_FieldInfo = 
{
	"next", &t44_0_0_1, &t2533_TI, offsetof(t2533, f1) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2533_f2_FieldInfo = 
{
	"ver", &t44_0_0_1, &t2533_TI, offsetof(t2533, f2) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t3_0_0_1;
FieldInfo t2533_f3_FieldInfo = 
{
	"current", &t3_0_0_1, &t2533_TI, offsetof(t2533, f3) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2533_FIs[] =
{
	&t2533_f0_FieldInfo,
	&t2533_f1_FieldInfo,
	&t2533_f2_FieldInfo,
	&t2533_f3_FieldInfo,
	NULL
};
extern MethodInfo m13467_MI;
static PropertyInfo t2533____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2533_TI, "System.Collections.IEnumerator.Current", &m13467_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13471_MI;
static PropertyInfo t2533____Current_PropertyInfo = 
{
	&t2533_TI, "Current", &m13471_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2533_PIs[] =
{
	&t2533____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2533____Current_PropertyInfo,
	NULL
};
extern Il2CppType t278_0_0_0;
static ParameterInfo t2533_m13466_ParameterInfos[] = 
{
	{"l", 0, 134217728, &EmptyCustomAttributesCache, &t278_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13466_GM;
MethodInfo m13466_MI = 
{
	".ctor", (methodPointerType)&m10631_gshared, &t2533_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2533_m13466_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13466_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13467_GM;
MethodInfo m13467_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10632_gshared, &t2533_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13467_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13468_GM;
MethodInfo m13468_MI = 
{
	"Dispose", (methodPointerType)&m10633_gshared, &t2533_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13468_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13469_GM;
MethodInfo m13469_MI = 
{
	"VerifyState", (methodPointerType)&m10634_gshared, &t2533_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 129, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13469_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13470_GM;
MethodInfo m13470_MI = 
{
	"MoveNext", (methodPointerType)&m10635_gshared, &t2533_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13470_GM};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13471_GM;
MethodInfo m13471_MI = 
{
	"get_Current", (methodPointerType)&m10636_gshared, &t2533_TI, &t3_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13471_GM};
static MethodInfo* t2533_MIs[] =
{
	&m13466_MI,
	&m13467_MI,
	&m13468_MI,
	&m13469_MI,
	&m13470_MI,
	&m13471_MI,
	NULL
};
extern MethodInfo m13470_MI;
extern MethodInfo m13468_MI;
static MethodInfo* t2533_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13467_MI,
	&m13470_MI,
	&m13468_MI,
	&m13471_MI,
};
static TypeInfo* t2533_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t2527_TI,
};
static Il2CppInterfaceOffsetPair t2533_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t2527_TI, 7},
};
extern TypeInfo t3_TI;
extern TypeInfo t2533_TI;
static Il2CppRGCTXData t2533_RGCTXData[3] = 
{
	&m13469_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&t2533_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2533_0_0_0;
extern Il2CppType t2533_1_0_0;
extern Il2CppGenericClass t2533_GC;
TypeInfo t2533_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Enumerator", "", t2533_MIs, t2533_PIs, t2533_FIs, NULL, &t110_TI, NULL, &t1261_TI, &t2533_TI, t2533_ITIs, t2533_VT, &EmptyCustomAttributesCache, &t2533_TI, &t2533_0_0_0, &t2533_1_0_0, t2533_IOs, &t2533_GC, NULL, NULL, NULL, t2533_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2533)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1057034, 0, true, false, false, false, true, false, false, false, false, false, false, false, 6, 2, 4, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif

#include "t2536MD.h"
extern MethodInfo m13501_MI;
extern MethodInfo m13533_MI;
extern MethodInfo m27847_MI;
extern MethodInfo m27851_MI;


// Metadata Definition System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Canvas>
extern Il2CppType t2535_0_0_1;
FieldInfo t2530_f0_FieldInfo = 
{
	"list", &t2535_0_0_1, &t2530_TI, offsetof(t2530, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2530_FIs[] =
{
	&t2530_f0_FieldInfo,
	NULL
};
extern MethodInfo m13478_MI;
extern MethodInfo m13479_MI;
static PropertyInfo t2530____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo = 
{
	&t2530_TI, "System.Collections.Generic.IList<T>.Item", &m13478_MI, &m13479_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13480_MI;
static PropertyInfo t2530____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2530_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m13480_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13490_MI;
static PropertyInfo t2530____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2530_TI, "System.Collections.ICollection.IsSynchronized", &m13490_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13491_MI;
static PropertyInfo t2530____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2530_TI, "System.Collections.ICollection.SyncRoot", &m13491_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13492_MI;
static PropertyInfo t2530____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2530_TI, "System.Collections.IList.IsFixedSize", &m13492_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13493_MI;
static PropertyInfo t2530____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2530_TI, "System.Collections.IList.IsReadOnly", &m13493_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13494_MI;
extern MethodInfo m13495_MI;
static PropertyInfo t2530____System_Collections_IList_Item_PropertyInfo = 
{
	&t2530_TI, "System.Collections.IList.Item", &m13494_MI, &m13495_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13500_MI;
static PropertyInfo t2530____Count_PropertyInfo = 
{
	&t2530_TI, "Count", &m13500_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2530____Item_PropertyInfo = 
{
	&t2530_TI, "Item", &m13501_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2530_PIs[] =
{
	&t2530____System_Collections_Generic_IListU3CTU3E_Item_PropertyInfo,
	&t2530____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2530____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2530____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2530____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2530____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2530____System_Collections_IList_Item_PropertyInfo,
	&t2530____Count_PropertyInfo,
	&t2530____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2535_0_0_0;
static ParameterInfo t2530_m13472_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2535_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13472_GM;
MethodInfo m13472_MI = 
{
	".ctor", (methodPointerType)&m10637_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2530_m13472_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13472_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2530_m13473_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13473_GM;
MethodInfo m13473_MI = 
{
	"System.Collections.Generic.ICollection<T>.Add", (methodPointerType)&m10638_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2530_m13473_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13473_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13474_GM;
MethodInfo m13474_MI = 
{
	"System.Collections.Generic.ICollection<T>.Clear", (methodPointerType)&m10639_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13474_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2530_m13475_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13475_GM;
MethodInfo m13475_MI = 
{
	"System.Collections.Generic.IList<T>.Insert", (methodPointerType)&m10640_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2530_m13475_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13475_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2530_m13476_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13476_GM;
MethodInfo m13476_MI = 
{
	"System.Collections.Generic.ICollection<T>.Remove", (methodPointerType)&m10641_gshared, &t2530_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2530_m13476_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13476_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2530_m13477_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13477_GM;
MethodInfo m13477_MI = 
{
	"System.Collections.Generic.IList<T>.RemoveAt", (methodPointerType)&m10642_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2530_m13477_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13477_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2530_m13478_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13478_GM;
MethodInfo m13478_MI = 
{
	"System.Collections.Generic.IList<T>.get_Item", (methodPointerType)&m10643_gshared, &t2530_TI, &t3_0_0_0, RuntimeInvoker_t29_t44, t2530_m13478_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13478_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2530_m13479_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13479_GM;
MethodInfo m13479_MI = 
{
	"System.Collections.Generic.IList<T>.set_Item", (methodPointerType)&m10644_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2530_m13479_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13479_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13480_GM;
MethodInfo m13480_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10645_gshared, &t2530_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13480_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2530_m13481_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13481_GM;
MethodInfo m13481_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10646_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2530_m13481_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13481_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13482_GM;
MethodInfo m13482_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10647_gshared, &t2530_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13482_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2530_m13483_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13483_GM;
MethodInfo m13483_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10648_gshared, &t2530_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2530_m13483_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13483_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13484_GM;
MethodInfo m13484_MI = 
{
	"System.Collections.IList.Clear", (methodPointerType)&m10649_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 481, 0, 14, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13484_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2530_m13485_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13485_GM;
MethodInfo m13485_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10650_gshared, &t2530_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2530_m13485_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13485_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2530_m13486_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13486_GM;
MethodInfo m13486_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10651_gshared, &t2530_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2530_m13486_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13486_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2530_m13487_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13487_GM;
MethodInfo m13487_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10652_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2530_m13487_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13487_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2530_m13488_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13488_GM;
MethodInfo m13488_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10653_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2530_m13488_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13488_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2530_m13489_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13489_GM;
MethodInfo m13489_MI = 
{
	"System.Collections.IList.RemoveAt", (methodPointerType)&m10654_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2530_m13489_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 19, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13489_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13490_GM;
MethodInfo m13490_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10655_gshared, &t2530_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13490_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13491_GM;
MethodInfo m13491_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10656_gshared, &t2530_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13491_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13492_GM;
MethodInfo m13492_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10657_gshared, &t2530_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13492_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13493_GM;
MethodInfo m13493_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10658_gshared, &t2530_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13493_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2530_m13494_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13494_GM;
MethodInfo m13494_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10659_gshared, &t2530_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2530_m13494_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13494_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2530_m13495_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13495_GM;
MethodInfo m13495_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10660_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2530_m13495_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13495_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2530_m13496_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13496_GM;
MethodInfo m13496_MI = 
{
	"Contains", (methodPointerType)&m10661_gshared, &t2530_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2530_m13496_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13496_GM};
extern Il2CppType t2526_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2530_m13497_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2526_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13497_GM;
MethodInfo m13497_MI = 
{
	"CopyTo", (methodPointerType)&m10662_gshared, &t2530_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2530_m13497_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13497_GM};
extern Il2CppType t2527_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13498_GM;
MethodInfo m13498_MI = 
{
	"GetEnumerator", (methodPointerType)&m10663_gshared, &t2530_TI, &t2527_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13498_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2530_m13499_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13499_GM;
MethodInfo m13499_MI = 
{
	"IndexOf", (methodPointerType)&m10664_gshared, &t2530_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2530_m13499_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13499_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13500_GM;
MethodInfo m13500_MI = 
{
	"get_Count", (methodPointerType)&m10665_gshared, &t2530_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13500_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2530_m13501_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13501_GM;
MethodInfo m13501_MI = 
{
	"get_Item", (methodPointerType)&m10666_gshared, &t2530_TI, &t3_0_0_0, RuntimeInvoker_t29_t44, t2530_m13501_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 33, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13501_GM};
static MethodInfo* t2530_MIs[] =
{
	&m13472_MI,
	&m13473_MI,
	&m13474_MI,
	&m13475_MI,
	&m13476_MI,
	&m13477_MI,
	&m13478_MI,
	&m13479_MI,
	&m13480_MI,
	&m13481_MI,
	&m13482_MI,
	&m13483_MI,
	&m13484_MI,
	&m13485_MI,
	&m13486_MI,
	&m13487_MI,
	&m13488_MI,
	&m13489_MI,
	&m13490_MI,
	&m13491_MI,
	&m13492_MI,
	&m13493_MI,
	&m13494_MI,
	&m13495_MI,
	&m13496_MI,
	&m13497_MI,
	&m13498_MI,
	&m13499_MI,
	&m13500_MI,
	&m13501_MI,
	NULL
};
extern MethodInfo m13482_MI;
extern MethodInfo m13481_MI;
extern MethodInfo m13483_MI;
extern MethodInfo m13484_MI;
extern MethodInfo m13485_MI;
extern MethodInfo m13486_MI;
extern MethodInfo m13487_MI;
extern MethodInfo m13488_MI;
extern MethodInfo m13489_MI;
extern MethodInfo m13473_MI;
extern MethodInfo m13474_MI;
extern MethodInfo m13496_MI;
extern MethodInfo m13497_MI;
extern MethodInfo m13476_MI;
extern MethodInfo m13499_MI;
extern MethodInfo m13475_MI;
extern MethodInfo m13477_MI;
extern MethodInfo m13498_MI;
static MethodInfo* t2530_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13482_MI,
	&m13500_MI,
	&m13490_MI,
	&m13491_MI,
	&m13481_MI,
	&m13492_MI,
	&m13493_MI,
	&m13494_MI,
	&m13495_MI,
	&m13483_MI,
	&m13484_MI,
	&m13485_MI,
	&m13486_MI,
	&m13487_MI,
	&m13488_MI,
	&m13489_MI,
	&m13500_MI,
	&m13480_MI,
	&m13473_MI,
	&m13474_MI,
	&m13496_MI,
	&m13497_MI,
	&m13476_MI,
	&m13499_MI,
	&m13475_MI,
	&m13477_MI,
	&m13478_MI,
	&m13479_MI,
	&m13498_MI,
	&m13501_MI,
};
static TypeInfo* t2530_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2528_TI,
	&t2535_TI,
	&t2529_TI,
};
static Il2CppInterfaceOffsetPair t2530_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2528_TI, 20},
	{ &t2535_TI, 27},
	{ &t2529_TI, 32},
};
extern TypeInfo t3_TI;
static Il2CppRGCTXData t2530_RGCTXData[9] = 
{
	&m13501_MI/* Method Usage */,
	&m13533_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m27847_MI/* Method Usage */,
	&m27851_MI/* Method Usage */,
	&m27849_MI/* Method Usage */,
	&m27841_MI/* Method Usage */,
	&m27842_MI/* Method Usage */,
	&m27840_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2530_0_0_0;
extern Il2CppType t2530_1_0_0;
struct t2530;
extern Il2CppGenericClass t2530_GC;
extern CustomAttributesCache t1263__CustomAttributeCache;
TypeInfo t2530_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ReadOnlyCollection`1", "System.Collections.ObjectModel", t2530_MIs, t2530_PIs, t2530_FIs, NULL, &t29_TI, NULL, NULL, &t2530_TI, t2530_ITIs, t2530_VT, &t1263__CustomAttributeCache, &t2530_TI, &t2530_0_0_0, &t2530_1_0_0, t2530_IOs, &t2530_GC, NULL, NULL, NULL, t2530_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2530), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 30, 9, 1, 0, 0, 34, 6, 6};
#include "t2536.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2536_TI;

extern MethodInfo m13536_MI;
extern MethodInfo m13537_MI;
extern MethodInfo m13534_MI;
extern MethodInfo m13532_MI;
extern MethodInfo m13415_MI;
extern MethodInfo m13525_MI;
extern MethodInfo m13535_MI;
extern MethodInfo m13523_MI;
extern MethodInfo m13528_MI;
extern MethodInfo m13519_MI;
extern MethodInfo m27846_MI;
extern MethodInfo m27852_MI;
extern MethodInfo m27853_MI;


// Metadata Definition System.Collections.ObjectModel.Collection`1<UnityEngine.Canvas>
extern Il2CppType t2535_0_0_1;
FieldInfo t2536_f0_FieldInfo = 
{
	"list", &t2535_0_0_1, &t2536_TI, offsetof(t2536, f0), &EmptyCustomAttributesCache};
extern Il2CppType t29_0_0_1;
FieldInfo t2536_f1_FieldInfo = 
{
	"syncRoot", &t29_0_0_1, &t2536_TI, offsetof(t2536, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2536_FIs[] =
{
	&t2536_f0_FieldInfo,
	&t2536_f1_FieldInfo,
	NULL
};
extern MethodInfo m13503_MI;
static PropertyInfo t2536____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&t2536_TI, "System.Collections.Generic.ICollection<T>.IsReadOnly", &m13503_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13511_MI;
static PropertyInfo t2536____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&t2536_TI, "System.Collections.ICollection.IsSynchronized", &m13511_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13512_MI;
static PropertyInfo t2536____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&t2536_TI, "System.Collections.ICollection.SyncRoot", &m13512_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13513_MI;
static PropertyInfo t2536____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&t2536_TI, "System.Collections.IList.IsFixedSize", &m13513_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13514_MI;
static PropertyInfo t2536____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&t2536_TI, "System.Collections.IList.IsReadOnly", &m13514_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13515_MI;
extern MethodInfo m13516_MI;
static PropertyInfo t2536____System_Collections_IList_Item_PropertyInfo = 
{
	&t2536_TI, "System.Collections.IList.Item", &m13515_MI, &m13516_MI, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13529_MI;
static PropertyInfo t2536____Count_PropertyInfo = 
{
	&t2536_TI, "Count", &m13529_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m13530_MI;
extern MethodInfo m13531_MI;
static PropertyInfo t2536____Item_PropertyInfo = 
{
	&t2536_TI, "Item", &m13530_MI, &m13531_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2536_PIs[] =
{
	&t2536____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&t2536____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&t2536____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&t2536____System_Collections_IList_IsFixedSize_PropertyInfo,
	&t2536____System_Collections_IList_IsReadOnly_PropertyInfo,
	&t2536____System_Collections_IList_Item_PropertyInfo,
	&t2536____Count_PropertyInfo,
	&t2536____Item_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13502_GM;
MethodInfo m13502_MI = 
{
	".ctor", (methodPointerType)&m10667_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13502_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13503_GM;
MethodInfo m13503_MI = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly", (methodPointerType)&m10668_gshared, &t2536_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 21, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13503_GM};
extern Il2CppType t20_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2536_m13504_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13504_GM;
MethodInfo m13504_MI = 
{
	"System.Collections.ICollection.CopyTo", (methodPointerType)&m10669_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2536_m13504_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 8, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13504_GM};
extern Il2CppType t136_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13505_GM;
MethodInfo m13505_MI = 
{
	"System.Collections.IEnumerable.GetEnumerator", (methodPointerType)&m10670_gshared, &t2536_TI, &t136_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 481, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13505_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2536_m13506_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13506_GM;
MethodInfo m13506_MI = 
{
	"System.Collections.IList.Add", (methodPointerType)&m10671_gshared, &t2536_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2536_m13506_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 13, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13506_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2536_m13507_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13507_GM;
MethodInfo m13507_MI = 
{
	"System.Collections.IList.Contains", (methodPointerType)&m10672_gshared, &t2536_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2536_m13507_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 15, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13507_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2536_m13508_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13508_GM;
MethodInfo m13508_MI = 
{
	"System.Collections.IList.IndexOf", (methodPointerType)&m10673_gshared, &t2536_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2536_m13508_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 16, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13508_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2536_m13509_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13509_GM;
MethodInfo m13509_MI = 
{
	"System.Collections.IList.Insert", (methodPointerType)&m10674_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2536_m13509_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 17, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13509_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2536_m13510_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13510_GM;
MethodInfo m13510_MI = 
{
	"System.Collections.IList.Remove", (methodPointerType)&m10675_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2536_m13510_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 18, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13510_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13511_GM;
MethodInfo m13511_MI = 
{
	"System.Collections.ICollection.get_IsSynchronized", (methodPointerType)&m10676_gshared, &t2536_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13511_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13512_GM;
MethodInfo m13512_MI = 
{
	"System.Collections.ICollection.get_SyncRoot", (methodPointerType)&m10677_gshared, &t2536_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13512_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13513_GM;
MethodInfo m13513_MI = 
{
	"System.Collections.IList.get_IsFixedSize", (methodPointerType)&m10678_gshared, &t2536_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 9, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13513_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13514_GM;
MethodInfo m13514_MI = 
{
	"System.Collections.IList.get_IsReadOnly", (methodPointerType)&m10679_gshared, &t2536_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 2529, 0, 10, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13514_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2536_m13515_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13515_GM;
MethodInfo m13515_MI = 
{
	"System.Collections.IList.get_Item", (methodPointerType)&m10680_gshared, &t2536_TI, &t29_0_0_0, RuntimeInvoker_t29_t44, t2536_m13515_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 11, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13515_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2536_m13516_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13516_GM;
MethodInfo m13516_MI = 
{
	"System.Collections.IList.set_Item", (methodPointerType)&m10681_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2536_m13516_ParameterInfos, &EmptyCustomAttributesCache, 2529, 0, 12, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13516_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2536_m13517_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13517_GM;
MethodInfo m13517_MI = 
{
	"Add", (methodPointerType)&m10682_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2536_m13517_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 22, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13517_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13518_GM;
MethodInfo m13518_MI = 
{
	"Clear", (methodPointerType)&m10683_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 23, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13518_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13519_GM;
MethodInfo m13519_MI = 
{
	"ClearItems", (methodPointerType)&m10684_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 452, 0, 33, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13519_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2536_m13520_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13520_GM;
MethodInfo m13520_MI = 
{
	"Contains", (methodPointerType)&m10685_gshared, &t2536_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2536_m13520_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 24, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13520_GM};
extern Il2CppType t2526_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t2536_m13521_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t2526_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13521_GM;
MethodInfo m13521_MI = 
{
	"CopyTo", (methodPointerType)&m10686_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t2536_m13521_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 25, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13521_GM};
extern Il2CppType t2527_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13522_GM;
MethodInfo m13522_MI = 
{
	"GetEnumerator", (methodPointerType)&m10687_gshared, &t2536_TI, &t2527_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 486, 0, 32, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13522_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2536_m13523_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13523_GM;
MethodInfo m13523_MI = 
{
	"IndexOf", (methodPointerType)&m10688_gshared, &t2536_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2536_m13523_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 27, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13523_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2536_m13524_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13524_GM;
MethodInfo m13524_MI = 
{
	"Insert", (methodPointerType)&m10689_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2536_m13524_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 28, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13524_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2536_m13525_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13525_GM;
MethodInfo m13525_MI = 
{
	"InsertItem", (methodPointerType)&m10690_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2536_m13525_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 34, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13525_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2536_m13526_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13526_GM;
MethodInfo m13526_MI = 
{
	"Remove", (methodPointerType)&m10691_gshared, &t2536_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2536_m13526_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 26, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13526_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2536_m13527_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13527_GM;
MethodInfo m13527_MI = 
{
	"RemoveAt", (methodPointerType)&m10692_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2536_m13527_ParameterInfos, &EmptyCustomAttributesCache, 486, 0, 29, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13527_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2536_m13528_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13528_GM;
MethodInfo m13528_MI = 
{
	"RemoveItem", (methodPointerType)&m10693_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t2536_m13528_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 35, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13528_GM};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13529_GM;
MethodInfo m13529_MI = 
{
	"get_Count", (methodPointerType)&m10694_gshared, &t2536_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 2534, 0, 20, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13529_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t2536_m13530_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13530_GM;
MethodInfo m13530_MI = 
{
	"get_Item", (methodPointerType)&m10695_gshared, &t2536_TI, &t3_0_0_0, RuntimeInvoker_t29_t44, t2536_m13530_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 30, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13530_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2536_m13531_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13531_GM;
MethodInfo m13531_MI = 
{
	"set_Item", (methodPointerType)&m10696_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2536_m13531_ParameterInfos, &EmptyCustomAttributesCache, 2534, 0, 31, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13531_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2536_m13532_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13532_GM;
MethodInfo m13532_MI = 
{
	"SetItem", (methodPointerType)&m10697_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t2536_m13532_ParameterInfos, &EmptyCustomAttributesCache, 452, 0, 36, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13532_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2536_m13533_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13533_GM;
MethodInfo m13533_MI = 
{
	"IsValidItem", (methodPointerType)&m10698_gshared, &t2536_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2536_m13533_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13533_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2536_m13534_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t3_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13534_GM;
MethodInfo m13534_MI = 
{
	"ConvertItem", (methodPointerType)&m10699_gshared, &t2536_TI, &t3_0_0_0, RuntimeInvoker_t29_t29, t2536_m13534_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13534_GM};
extern Il2CppType t2535_0_0_0;
static ParameterInfo t2536_m13535_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2535_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13535_GM;
MethodInfo m13535_MI = 
{
	"CheckWritable", (methodPointerType)&m10700_gshared, &t2536_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2536_m13535_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13535_GM};
extern Il2CppType t2535_0_0_0;
static ParameterInfo t2536_m13536_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2535_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13536_GM;
MethodInfo m13536_MI = 
{
	"IsSynchronized", (methodPointerType)&m10701_gshared, &t2536_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2536_m13536_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13536_GM};
extern Il2CppType t2535_0_0_0;
static ParameterInfo t2536_m13537_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &t2535_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13537_GM;
MethodInfo m13537_MI = 
{
	"IsFixedSize", (methodPointerType)&m10702_gshared, &t2536_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2536_m13537_ParameterInfos, &EmptyCustomAttributesCache, 147, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13537_GM};
static MethodInfo* t2536_MIs[] =
{
	&m13502_MI,
	&m13503_MI,
	&m13504_MI,
	&m13505_MI,
	&m13506_MI,
	&m13507_MI,
	&m13508_MI,
	&m13509_MI,
	&m13510_MI,
	&m13511_MI,
	&m13512_MI,
	&m13513_MI,
	&m13514_MI,
	&m13515_MI,
	&m13516_MI,
	&m13517_MI,
	&m13518_MI,
	&m13519_MI,
	&m13520_MI,
	&m13521_MI,
	&m13522_MI,
	&m13523_MI,
	&m13524_MI,
	&m13525_MI,
	&m13526_MI,
	&m13527_MI,
	&m13528_MI,
	&m13529_MI,
	&m13530_MI,
	&m13531_MI,
	&m13532_MI,
	&m13533_MI,
	&m13534_MI,
	&m13535_MI,
	&m13536_MI,
	&m13537_MI,
	NULL
};
extern MethodInfo m13505_MI;
extern MethodInfo m13504_MI;
extern MethodInfo m13506_MI;
extern MethodInfo m13518_MI;
extern MethodInfo m13507_MI;
extern MethodInfo m13508_MI;
extern MethodInfo m13509_MI;
extern MethodInfo m13510_MI;
extern MethodInfo m13527_MI;
extern MethodInfo m13517_MI;
extern MethodInfo m13520_MI;
extern MethodInfo m13521_MI;
extern MethodInfo m13526_MI;
extern MethodInfo m13524_MI;
extern MethodInfo m13522_MI;
static MethodInfo* t2536_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13505_MI,
	&m13529_MI,
	&m13511_MI,
	&m13512_MI,
	&m13504_MI,
	&m13513_MI,
	&m13514_MI,
	&m13515_MI,
	&m13516_MI,
	&m13506_MI,
	&m13518_MI,
	&m13507_MI,
	&m13508_MI,
	&m13509_MI,
	&m13510_MI,
	&m13527_MI,
	&m13529_MI,
	&m13503_MI,
	&m13517_MI,
	&m13518_MI,
	&m13520_MI,
	&m13521_MI,
	&m13526_MI,
	&m13523_MI,
	&m13524_MI,
	&m13527_MI,
	&m13530_MI,
	&m13531_MI,
	&m13522_MI,
	&m13519_MI,
	&m13525_MI,
	&m13528_MI,
	&m13532_MI,
};
static TypeInfo* t2536_ITIs[] = 
{
	&t603_TI,
	&t674_TI,
	&t868_TI,
	&t2528_TI,
	&t2535_TI,
	&t2529_TI,
};
static Il2CppInterfaceOffsetPair t2536_IOs[] = 
{
	{ &t603_TI, 4},
	{ &t674_TI, 5},
	{ &t868_TI, 9},
	{ &t2528_TI, 20},
	{ &t2535_TI, 27},
	{ &t2529_TI, 32},
};
extern TypeInfo t278_TI;
extern TypeInfo t3_TI;
static Il2CppRGCTXData t2536_RGCTXData[25] = 
{
	&t278_TI/* Class Usage */,
	&m13415_MI/* Method Usage */,
	&m27844_MI/* Method Usage */,
	&m27842_MI/* Method Usage */,
	&m27840_MI/* Method Usage */,
	&m13534_MI/* Method Usage */,
	&m13525_MI/* Method Usage */,
	&m13533_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m27847_MI/* Method Usage */,
	&m27851_MI/* Method Usage */,
	&m13535_MI/* Method Usage */,
	&m13523_MI/* Method Usage */,
	&m13528_MI/* Method Usage */,
	&m13536_MI/* Method Usage */,
	&m13537_MI/* Method Usage */,
	&m27849_MI/* Method Usage */,
	&m13532_MI/* Method Usage */,
	&m13519_MI/* Method Usage */,
	&m27846_MI/* Method Usage */,
	&m27841_MI/* Method Usage */,
	&m27852_MI/* Method Usage */,
	&m27853_MI/* Method Usage */,
	&m27850_MI/* Method Usage */,
	&t3_0_0_0/* Type Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2536_0_0_0;
extern Il2CppType t2536_1_0_0;
struct t2536;
extern Il2CppGenericClass t2536_GC;
extern CustomAttributesCache t1262__CustomAttributeCache;
TypeInfo t2536_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Collection`1", "System.Collections.ObjectModel", t2536_MIs, t2536_PIs, t2536_FIs, NULL, &t29_TI, NULL, NULL, &t2536_TI, t2536_ITIs, t2536_VT, &t1262__CustomAttributeCache, &t2536_TI, &t2536_0_0_0, &t2536_1_0_0, t2536_IOs, &t2536_GC, NULL, NULL, NULL, t2536_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2536), 0, -1, 0, 0, -1, 1056769, 0, false, false, false, false, true, false, false, false, false, false, false, false, 36, 8, 2, 0, 0, 37, 6, 6};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2537_TI;
#include "t2537MD.h"

#include "t2538.h"
extern TypeInfo t6684_TI;
extern TypeInfo t2538_TI;
#include "t2538MD.h"
extern Il2CppType t6684_0_0_0;
extern MethodInfo m13543_MI;
extern MethodInfo m27854_MI;
extern MethodInfo m20960_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1<UnityEngine.Canvas>
extern Il2CppType t2537_0_0_49;
FieldInfo t2537_f0_FieldInfo = 
{
	"_default", &t2537_0_0_49, &t2537_TI, offsetof(t2537_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2537_FIs[] =
{
	&t2537_f0_FieldInfo,
	NULL
};
extern MethodInfo m13542_MI;
static PropertyInfo t2537____Default_PropertyInfo = 
{
	&t2537_TI, "Default", &m13542_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2537_PIs[] =
{
	&t2537____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13538_GM;
MethodInfo m13538_MI = 
{
	".ctor", (methodPointerType)&m10703_gshared, &t2537_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13538_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13539_GM;
MethodInfo m13539_MI = 
{
	".cctor", (methodPointerType)&m10704_gshared, &t2537_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13539_GM};
extern Il2CppType t29_0_0_0;
static ParameterInfo t2537_m13540_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13540_GM;
MethodInfo m13540_MI = 
{
	"System.Collections.IEqualityComparer.GetHashCode", (methodPointerType)&m10705_gshared, &t2537_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2537_m13540_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 7, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13540_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2537_m13541_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13541_GM;
MethodInfo m13541_MI = 
{
	"System.Collections.IEqualityComparer.Equals", (methodPointerType)&m10706_gshared, &t2537_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2537_m13541_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13541_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2537_m27854_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27854_GM;
MethodInfo m27854_MI = 
{
	"GetHashCode", NULL, &t2537_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2537_m27854_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27854_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2537_m20960_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20960_GM;
MethodInfo m20960_MI = 
{
	"Equals", NULL, &t2537_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2537_m20960_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20960_GM};
extern Il2CppType t2537_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13542_GM;
MethodInfo m13542_MI = 
{
	"get_Default", (methodPointerType)&m10707_gshared, &t2537_TI, &t2537_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13542_GM};
static MethodInfo* t2537_MIs[] =
{
	&m13538_MI,
	&m13539_MI,
	&m13540_MI,
	&m13541_MI,
	&m27854_MI,
	&m20960_MI,
	&m13542_MI,
	NULL
};
extern MethodInfo m13541_MI;
extern MethodInfo m13540_MI;
static MethodInfo* t2537_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m20960_MI,
	&m27854_MI,
	&m13541_MI,
	&m13540_MI,
	NULL,
	NULL,
};
extern TypeInfo t2558_TI;
static TypeInfo* t2537_ITIs[] = 
{
	&t2558_TI,
	&t734_TI,
};
static Il2CppInterfaceOffsetPair t2537_IOs[] = 
{
	{ &t2558_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2537_TI;
extern TypeInfo t2537_TI;
extern TypeInfo t2538_TI;
extern TypeInfo t3_TI;
static Il2CppRGCTXData t2537_RGCTXData[9] = 
{
	&t6684_0_0_0/* Type Usage */,
	&t3_0_0_0/* Type Usage */,
	&t2537_TI/* Class Usage */,
	&t2537_TI/* Static Usage */,
	&t2538_TI/* Class Usage */,
	&m13543_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m27854_MI/* Method Usage */,
	&m20960_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2537_0_0_0;
extern Il2CppType t2537_1_0_0;
struct t2537;
extern Il2CppGenericClass t2537_GC;
TypeInfo t2537_TI = 
{
	&g_mscorlib_dll_Image, NULL, "EqualityComparer`1", "System.Collections.Generic", t2537_MIs, t2537_PIs, t2537_FIs, NULL, &t29_TI, NULL, NULL, &t2537_TI, t2537_ITIs, t2537_VT, &EmptyCustomAttributesCache, &t2537_TI, &t2537_0_0_0, &t2537_1_0_0, t2537_IOs, &t2537_GC, NULL, NULL, NULL, t2537_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2537), 0, -1, sizeof(t2537_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 7, 1, 1, 0, 0, 10, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEqualityComparer`1<UnityEngine.Canvas>
extern Il2CppType t3_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2558_m27855_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27855_GM;
MethodInfo m27855_MI = 
{
	"Equals", NULL, &t2558_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2558_m27855_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27855_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2558_m27856_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27856_GM;
MethodInfo m27856_MI = 
{
	"GetHashCode", NULL, &t2558_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2558_m27856_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27856_GM};
static MethodInfo* t2558_MIs[] =
{
	&m27855_MI,
	&m27856_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2558_0_0_0;
extern Il2CppType t2558_1_0_0;
struct t2558;
extern Il2CppGenericClass t2558_GC;
TypeInfo t2558_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEqualityComparer`1", "System.Collections.Generic", t2558_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t2558_TI, NULL, NULL, &EmptyCustomAttributesCache, &t2558_TI, &t2558_0_0_0, &t2558_1_0_0, NULL, &t2558_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 2, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.IEquatable`1<UnityEngine.Canvas>
extern Il2CppType t3_0_0_0;
static ParameterInfo t6684_m27857_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27857_GM;
MethodInfo m27857_MI = 
{
	"Equals", NULL, &t6684_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6684_m27857_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m27857_GM};
static MethodInfo* t6684_MIs[] =
{
	&m27857_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6684_1_0_0;
struct t6684;
extern Il2CppGenericClass t6684_GC;
TypeInfo t6684_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEquatable`1", "System", t6684_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6684_TI, NULL, NULL, &EmptyCustomAttributesCache, &t6684_TI, &t6684_0_0_0, &t6684_1_0_0, NULL, &t6684_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m13538_MI;


// Metadata Definition System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Canvas>
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13543_GM;
MethodInfo m13543_MI = 
{
	".ctor", (methodPointerType)&m10738_gshared, &t2538_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6278, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13543_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2538_m13544_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13544_GM;
MethodInfo m13544_MI = 
{
	"GetHashCode", (methodPointerType)&m10739_gshared, &t2538_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t2538_m13544_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 8, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13544_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2538_m13545_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13545_GM;
MethodInfo m13545_MI = 
{
	"Equals", (methodPointerType)&m10740_gshared, &t2538_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2538_m13545_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 9, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13545_GM};
static MethodInfo* t2538_MIs[] =
{
	&m13543_MI,
	&m13544_MI,
	&m13545_MI,
	NULL
};
extern MethodInfo m13545_MI;
extern MethodInfo m13544_MI;
static MethodInfo* t2538_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m13545_MI,
	&m13544_MI,
	&m13541_MI,
	&m13540_MI,
	&m13544_MI,
	&m13545_MI,
};
static Il2CppInterfaceOffsetPair t2538_IOs[] = 
{
	{ &t2558_TI, 4},
	{ &t734_TI, 6},
};
extern TypeInfo t2537_TI;
extern TypeInfo t2537_TI;
extern TypeInfo t2538_TI;
extern TypeInfo t3_TI;
extern TypeInfo t3_TI;
static Il2CppRGCTXData t2538_RGCTXData[11] = 
{
	&t6684_0_0_0/* Type Usage */,
	&t3_0_0_0/* Type Usage */,
	&t2537_TI/* Class Usage */,
	&t2537_TI/* Static Usage */,
	&t2538_TI/* Class Usage */,
	&m13543_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m27854_MI/* Method Usage */,
	&m20960_MI/* Method Usage */,
	&m13538_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2538_0_0_0;
extern Il2CppType t2538_1_0_0;
struct t2538;
extern Il2CppGenericClass t2538_GC;
TypeInfo t2538_TI = 
{
	&g_mscorlib_dll_Image, NULL, "DefaultComparer", "", t2538_MIs, NULL, NULL, NULL, &t2537_TI, NULL, &t1256_TI, &t2538_TI, NULL, t2538_VT, &EmptyCustomAttributesCache, &t2538_TI, &t2538_0_0_0, &t2538_1_0_0, t2538_IOs, &t2538_GC, NULL, NULL, NULL, t2538_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2538), 0, -1, 0, 0, -1, 1057027, 0, false, false, false, false, true, false, false, false, false, false, false, false, 3, 0, 0, 0, 0, 10, 0, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Predicate`1<UnityEngine.Canvas>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2531_m13546_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13546_GM;
MethodInfo m13546_MI = 
{
	".ctor", (methodPointerType)&m10741_gshared, &t2531_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2531_m13546_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13546_GM};
extern Il2CppType t3_0_0_0;
static ParameterInfo t2531_m13547_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13547_GM;
MethodInfo m13547_MI = 
{
	"Invoke", (methodPointerType)&m10742_gshared, &t2531_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2531_m13547_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13547_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2531_m13548_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13548_GM;
MethodInfo m13548_MI = 
{
	"BeginInvoke", (methodPointerType)&m10743_gshared, &t2531_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2531_m13548_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m13548_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2531_m13549_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13549_GM;
MethodInfo m13549_MI = 
{
	"EndInvoke", (methodPointerType)&m10744_gshared, &t2531_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t2531_m13549_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13549_GM};
static MethodInfo* t2531_MIs[] =
{
	&m13546_MI,
	&m13547_MI,
	&m13548_MI,
	&m13549_MI,
	NULL
};
extern MethodInfo m13548_MI;
extern MethodInfo m13549_MI;
static MethodInfo* t2531_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m13547_MI,
	&m13548_MI,
	&m13549_MI,
};
static Il2CppInterfaceOffsetPair t2531_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2531_1_0_0;
struct t2531;
extern Il2CppGenericClass t2531_GC;
TypeInfo t2531_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Predicate`1", "System", t2531_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2531_TI, NULL, t2531_VT, &EmptyCustomAttributesCache, &t2531_TI, &t2531_0_0_0, &t2531_1_0_0, t2531_IOs, &t2531_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2531), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

#include "t2540.h"
extern TypeInfo t4157_TI;
extern TypeInfo t2540_TI;
#include "t2540MD.h"
extern Il2CppType t4157_0_0_0;
extern MethodInfo m13554_MI;
extern MethodInfo m27858_MI;


// Metadata Definition System.Collections.Generic.Comparer`1<UnityEngine.Canvas>
extern Il2CppType t2539_0_0_49;
FieldInfo t2539_f0_FieldInfo = 
{
	"_default", &t2539_0_0_49, &t2539_TI, offsetof(t2539_SFs, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2539_FIs[] =
{
	&t2539_f0_FieldInfo,
	NULL
};
static PropertyInfo t2539____Default_PropertyInfo = 
{
	&t2539_TI, "Default", &m13553_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2539_PIs[] =
{
	&t2539____Default_PropertyInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13550_GM;
MethodInfo m13550_MI = 
{
	".ctor", (methodPointerType)&m10745_gshared, &t2539_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13550_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13551_GM;
MethodInfo m13551_MI = 
{
	".cctor", (methodPointerType)&m10746_gshared, &t2539_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6161, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13551_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2539_m13552_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13552_GM;
MethodInfo m13552_MI = 
{
	"System.Collections.IComparer.Compare", (methodPointerType)&m10747_gshared, &t2539_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2539_m13552_ParameterInfos, &EmptyCustomAttributesCache, 481, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m13552_GM};
extern Il2CppType t3_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t2539_m27858_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m27858_GM;
MethodInfo m27858_MI = 
{
	"Compare", NULL, &t2539_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t2539_m27858_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m27858_GM};
extern Il2CppType t2539_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13553_GM;
MethodInfo m13553_MI = 
{
	"get_Default", (methodPointerType)&m10748_gshared, &t2539_TI, &t2539_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2198, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13553_GM};
static MethodInfo* t2539_MIs[] =
{
	&m13550_MI,
	&m13551_MI,
	&m13552_MI,
	&m27858_MI,
	&m13553_MI,
	NULL
};
extern MethodInfo m13552_MI;
static MethodInfo* t2539_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m27858_MI,
	&m13552_MI,
	NULL,
};
extern TypeInfo t4156_TI;
static TypeInfo* t2539_ITIs[] = 
{
	&t4156_TI,
	&t726_TI,
};
static Il2CppInterfaceOffsetPair t2539_IOs[] = 
{
	{ &t4156_TI, 4},
	{ &t726_TI, 5},
};
extern TypeInfo t2539_TI;
extern TypeInfo t2539_TI;
extern TypeInfo t2540_TI;
extern TypeInfo t3_TI;
static Il2CppRGCTXData t2539_RGCTXData[8] = 
{
	&t4157_0_0_0/* Type Usage */,
	&t3_0_0_0/* Type Usage */,
	&t2539_TI/* Class Usage */,
	&t2539_TI/* Static Usage */,
	&t2540_TI/* Class Usage */,
	&m13554_MI/* Method Usage */,
	&t3_TI/* Class Usage */,
	&m27858_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2539_0_0_0;
extern Il2CppType t2539_1_0_0;
struct t2539;
extern Il2CppGenericClass t2539_GC;
TypeInfo t2539_TI = 
{
	&g_mscorlib_dll_Image, NULL, "Comparer`1", "System.Collections.Generic", t2539_MIs, t2539_PIs, t2539_FIs, NULL, &t29_TI, NULL, NULL, &t2539_TI, t2539_ITIs, t2539_VT, &EmptyCustomAttributesCache, &t2539_TI, &t2539_0_0_0, &t2539_1_0_0, t2539_IOs, &t2539_GC, NULL, NULL, NULL, t2539_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2539), 0, -1, sizeof(t2539_SFs), 0, -1, 8321, 0, false, false, false, false, true, false, false, false, false, true, false, false, 5, 1, 1, 0, 0, 7, 2, 2};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IComparer`1<UnityEngine.Canvas>
extern Il2CppType t3_0_0_0;
extern Il2CppType t3_0_0_0;
static ParameterInfo t4156_m20968_ParameterInfos[] = 
{
	{"x", 0, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
	{"y", 1, 134217728, &EmptyCustomAttributesCache, &t3_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m20968_GM;
MethodInfo m20968_MI = 
{
	"Compare", NULL, &t4156_TI, &t44_0_0_0, RuntimeInvoker_t44_t29_t29, t4156_m20968_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 2, false, true, 0, NULL, (methodPointerType)NULL, &m20968_GM};
static MethodInfo* t4156_MIs[] =
{
	&m20968_MI,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4156_0_0_0;
extern Il2CppType t4156_1_0_0;
struct t4156;
extern Il2CppGenericClass t4156_GC;
TypeInfo t4156_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IComparer`1", "System.Collections.Generic", t4156_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t4156_TI, NULL, NULL, &EmptyCustomAttributesCache, &t4156_TI, &t4156_0_0_0, &t4156_1_0_0, NULL, &t4156_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 0, 0};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
