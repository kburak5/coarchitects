﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2517;
struct t29;
struct t163;
#include "t184.h"

 void m13301 (t2517 * __this, t163 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13302 (t2517 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13303 (t2517 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13304 (t2517 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13305 (t2517 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t184  m13306 (t2517 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
