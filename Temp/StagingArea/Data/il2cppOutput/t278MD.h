﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t278;
struct t29;
struct t3;
struct t2527;
struct t20;
struct t136;
struct t2528;
struct t2529;
struct t2530;
struct t2526;
struct t2531;
struct t2532;
#include "t2533.h"

#include "t294MD.h"
#define m13415(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m13416(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m13417(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m13418(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m13419(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m13420(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m13421(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m13422(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m13423(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m13424(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m13425(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m13426(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m13427(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m13428(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m13429(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m13430(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m13431(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m13432(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m13433(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m13434(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m13435(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m13436(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m13437(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m13438(__this, method) (t2530 *)m10583_gshared((t294 *)__this, method)
#define m2030(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m13439(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m13440(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m13441(__this, p0, method) (t3 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m13442(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m13443(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t2533  m13444 (t278 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m13445(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m13446(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m13447(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m13448(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m13449(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m13450(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m13451(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m13452(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m13453(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m13454(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m13455(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m13456(__this, method) (t2526*)m10619_gshared((t294 *)__this, method)
#define m13457(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m13458(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m13459(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m1566(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m1567(__this, p0, method) (t3 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m13460(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
