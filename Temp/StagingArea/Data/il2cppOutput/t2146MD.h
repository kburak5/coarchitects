﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2146;
struct t41;
struct t41_marshaled;
struct t557;
struct t13;
struct t316;

#include "t2118MD.h"
#define m10432(__this, p0, p1, p2, method) (void)m10318_gshared((t2118 *)__this, (t41 *)p0, (t557 *)p1, (t29 *)p2, method)
#define m10433(__this, p0, method) (void)m10320_gshared((t2118 *)__this, (t316*)p0, method)
