﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1033;
struct t1020;
struct t1034;
struct t1036;
struct t1035;
struct t66;
struct t67;
struct t29;
struct t781;
struct t1017;
struct t1019;
struct t7;
#include "t1037.h"
#include "t1015.h"
#include "t1016.h"
#include "t1024.h"
#include "t1026.h"

 void m4792 (t1033 * __this, t1034 * p0, t1020 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4793 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1020 * m4794 (t1033 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4795 (t1033 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4796 (t1033 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1035 * m4797 (t1033 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4798 (t1033 * __this, t1034 * p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4799 (t1033 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4800 (t1033 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4801 (t1033 * __this, t1034 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4802 (t1033 * __this, int32_t p0, t1034 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4803 (t1033 * __this, t1034 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4804 (t1033 * __this, t1034 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4805 (t1033 * __this, uint8_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4806 (t1033 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4807 (t1033 * __this, uint8_t p0, uint8_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4808 (t1033 * __this, t1017 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4809 (t1033 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4810 (t1033 * __this, uint8_t p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4811 (t1033 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4812 (t1033 * __this, uint8_t p0, t781* p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4813 (t1033 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4814 (t1033 * __this, uint8_t p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4815 (t1033 * __this, uint8_t p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4816 (t1033 * __this, uint8_t p0, t781* p1, int32_t p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4817 (t1033 * __this, uint8_t p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4818 (t1033 * __this, uint8_t p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4819 (t1033 * __this, t781* p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4820 (t1033 * __this, int32_t p0, t781* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1019 * m4821 (t1033 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
