﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t5355_TI;

#include "t44.h"
#include "t40.h"
#include "t21.h"
#include "t173.h"
#include "UnityEngine.UI_ArrayTypes.h"

#include "t20.h"

// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>
extern MethodInfo m28020_MI;
static PropertyInfo t5355____Count_PropertyInfo = 
{
	&t5355_TI, "Count", &m28020_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28021_MI;
static PropertyInfo t5355____IsReadOnly_PropertyInfo = 
{
	&t5355_TI, "IsReadOnly", &m28021_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5355_PIs[] =
{
	&t5355____Count_PropertyInfo,
	&t5355____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28020_GM;
MethodInfo m28020_MI = 
{
	"get_Count", NULL, &t5355_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28020_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28021_GM;
MethodInfo m28021_MI = 
{
	"get_IsReadOnly", NULL, &t5355_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28021_GM};
extern Il2CppType t173_0_0_0;
extern Il2CppType t173_0_0_0;
static ParameterInfo t5355_m28022_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t173_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28022_GM;
MethodInfo m28022_MI = 
{
	"Add", NULL, &t5355_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5355_m28022_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28022_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28023_GM;
MethodInfo m28023_MI = 
{
	"Clear", NULL, &t5355_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28023_GM};
extern Il2CppType t173_0_0_0;
static ParameterInfo t5355_m28024_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t173_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28024_GM;
MethodInfo m28024_MI = 
{
	"Contains", NULL, &t5355_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5355_m28024_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28024_GM};
extern Il2CppType t3840_0_0_0;
extern Il2CppType t3840_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5355_m28025_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3840_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28025_GM;
MethodInfo m28025_MI = 
{
	"CopyTo", NULL, &t5355_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5355_m28025_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28025_GM};
extern Il2CppType t173_0_0_0;
static ParameterInfo t5355_m28026_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t173_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28026_GM;
MethodInfo m28026_MI = 
{
	"Remove", NULL, &t5355_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5355_m28026_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28026_GM};
static MethodInfo* t5355_MIs[] =
{
	&m28020_MI,
	&m28021_MI,
	&m28022_MI,
	&m28023_MI,
	&m28024_MI,
	&m28025_MI,
	&m28026_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t5357_TI;
static TypeInfo* t5355_ITIs[] = 
{
	&t603_TI,
	&t5357_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5355_0_0_0;
extern Il2CppType t5355_1_0_0;
struct t5355;
extern Il2CppGenericClass t5355_GC;
TypeInfo t5355_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5355_MIs, t5355_PIs, NULL, NULL, NULL, NULL, NULL, &t5355_TI, t5355_ITIs, NULL, &EmptyCustomAttributesCache, &t5355_TI, &t5355_0_0_0, &t5355_1_0_0, NULL, &t5355_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/FillMethod>
extern Il2CppType t4179_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28027_GM;
MethodInfo m28027_MI = 
{
	"GetEnumerator", NULL, &t5357_TI, &t4179_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28027_GM};
static MethodInfo* t5357_MIs[] =
{
	&m28027_MI,
	NULL
};
static TypeInfo* t5357_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5357_0_0_0;
extern Il2CppType t5357_1_0_0;
struct t5357;
extern Il2CppGenericClass t5357_GC;
TypeInfo t5357_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5357_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5357_TI, t5357_ITIs, NULL, &EmptyCustomAttributesCache, &t5357_TI, &t5357_0_0_0, &t5357_1_0_0, NULL, &t5357_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5356_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/FillMethod>
extern MethodInfo m28028_MI;
extern MethodInfo m28029_MI;
static PropertyInfo t5356____Item_PropertyInfo = 
{
	&t5356_TI, "Item", &m28028_MI, &m28029_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5356_PIs[] =
{
	&t5356____Item_PropertyInfo,
	NULL
};
extern Il2CppType t173_0_0_0;
static ParameterInfo t5356_m28030_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t173_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28030_GM;
MethodInfo m28030_MI = 
{
	"IndexOf", NULL, &t5356_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5356_m28030_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28030_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t173_0_0_0;
static ParameterInfo t5356_m28031_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t173_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28031_GM;
MethodInfo m28031_MI = 
{
	"Insert", NULL, &t5356_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5356_m28031_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28031_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5356_m28032_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28032_GM;
MethodInfo m28032_MI = 
{
	"RemoveAt", NULL, &t5356_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5356_m28032_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28032_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5356_m28028_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t173_0_0_0;
extern void* RuntimeInvoker_t173_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28028_GM;
MethodInfo m28028_MI = 
{
	"get_Item", NULL, &t5356_TI, &t173_0_0_0, RuntimeInvoker_t173_t44, t5356_m28028_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28028_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t173_0_0_0;
static ParameterInfo t5356_m28029_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t173_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28029_GM;
MethodInfo m28029_MI = 
{
	"set_Item", NULL, &t5356_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5356_m28029_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28029_GM};
static MethodInfo* t5356_MIs[] =
{
	&m28030_MI,
	&m28031_MI,
	&m28032_MI,
	&m28028_MI,
	&m28029_MI,
	NULL
};
static TypeInfo* t5356_ITIs[] = 
{
	&t603_TI,
	&t5355_TI,
	&t5357_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5356_0_0_0;
extern Il2CppType t5356_1_0_0;
struct t5356;
extern Il2CppGenericClass t5356_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5356_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5356_MIs, t5356_PIs, NULL, NULL, NULL, NULL, NULL, &t5356_TI, t5356_ITIs, NULL, &t1908__CustomAttributeCache, &t5356_TI, &t5356_0_0_0, &t5356_1_0_0, NULL, &t5356_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4181_TI;

#include "t174.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>
extern MethodInfo m28033_MI;
static PropertyInfo t4181____Current_PropertyInfo = 
{
	&t4181_TI, "Current", &m28033_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4181_PIs[] =
{
	&t4181____Current_PropertyInfo,
	NULL
};
extern Il2CppType t174_0_0_0;
extern void* RuntimeInvoker_t174 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28033_GM;
MethodInfo m28033_MI = 
{
	"get_Current", NULL, &t4181_TI, &t174_0_0_0, RuntimeInvoker_t174, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28033_GM};
static MethodInfo* t4181_MIs[] =
{
	&m28033_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4181_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4181_0_0_0;
extern Il2CppType t4181_1_0_0;
struct t4181;
extern Il2CppGenericClass t4181_GC;
TypeInfo t4181_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4181_MIs, t4181_PIs, NULL, NULL, NULL, NULL, NULL, &t4181_TI, t4181_ITIs, NULL, &EmptyCustomAttributesCache, &t4181_TI, &t4181_0_0_0, &t4181_1_0_0, NULL, &t4181_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2595.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2595_TI;
#include "t2595MD.h"

#include "t29.h"
#include "t7.h"
#include "t914.h"
extern TypeInfo t174_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m13990_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m21126_MI;
struct t20;
#include "t915.h"
 int32_t m21126 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m13986_MI;
 void m13986 (t2595 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13987_MI;
 t29 * m13987 (t2595 * __this, MethodInfo* method){
	{
		int32_t L_0 = m13990(__this, &m13990_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t174_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13988_MI;
 void m13988 (t2595 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13989_MI;
 bool m13989 (t2595 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m13990 (t2595 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21126(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21126_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>
extern Il2CppType t20_0_0_1;
FieldInfo t2595_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2595_TI, offsetof(t2595, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2595_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2595_TI, offsetof(t2595, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2595_FIs[] =
{
	&t2595_f0_FieldInfo,
	&t2595_f1_FieldInfo,
	NULL
};
static PropertyInfo t2595____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2595_TI, "System.Collections.IEnumerator.Current", &m13987_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2595____Current_PropertyInfo = 
{
	&t2595_TI, "Current", &m13990_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2595_PIs[] =
{
	&t2595____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2595____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t2595_m13986_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13986_GM;
MethodInfo m13986_MI = 
{
	".ctor", (methodPointerType)&m13986, &t2595_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2595_m13986_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13986_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13987_GM;
MethodInfo m13987_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13987, &t2595_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13987_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13988_GM;
MethodInfo m13988_MI = 
{
	"Dispose", (methodPointerType)&m13988, &t2595_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13988_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13989_GM;
MethodInfo m13989_MI = 
{
	"MoveNext", (methodPointerType)&m13989, &t2595_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13989_GM};
extern Il2CppType t174_0_0_0;
extern void* RuntimeInvoker_t174 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13990_GM;
MethodInfo m13990_MI = 
{
	"get_Current", (methodPointerType)&m13990, &t2595_TI, &t174_0_0_0, RuntimeInvoker_t174, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13990_GM};
static MethodInfo* t2595_MIs[] =
{
	&m13986_MI,
	&m13987_MI,
	&m13988_MI,
	&m13989_MI,
	&m13990_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
static MethodInfo* t2595_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13987_MI,
	&m13989_MI,
	&m13988_MI,
	&m13990_MI,
};
static TypeInfo* t2595_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4181_TI,
};
static Il2CppInterfaceOffsetPair t2595_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4181_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2595_0_0_0;
extern Il2CppType t2595_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t2595_GC;
extern TypeInfo t20_TI;
TypeInfo t2595_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2595_MIs, t2595_PIs, t2595_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2595_TI, t2595_ITIs, t2595_VT, &EmptyCustomAttributesCache, &t2595_TI, &t2595_0_0_0, &t2595_1_0_0, t2595_IOs, &t2595_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2595)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5358_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>
extern MethodInfo m28034_MI;
static PropertyInfo t5358____Count_PropertyInfo = 
{
	&t5358_TI, "Count", &m28034_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28035_MI;
static PropertyInfo t5358____IsReadOnly_PropertyInfo = 
{
	&t5358_TI, "IsReadOnly", &m28035_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5358_PIs[] =
{
	&t5358____Count_PropertyInfo,
	&t5358____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28034_GM;
MethodInfo m28034_MI = 
{
	"get_Count", NULL, &t5358_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28034_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28035_GM;
MethodInfo m28035_MI = 
{
	"get_IsReadOnly", NULL, &t5358_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28035_GM};
extern Il2CppType t174_0_0_0;
extern Il2CppType t174_0_0_0;
static ParameterInfo t5358_m28036_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t174_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28036_GM;
MethodInfo m28036_MI = 
{
	"Add", NULL, &t5358_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5358_m28036_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28036_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28037_GM;
MethodInfo m28037_MI = 
{
	"Clear", NULL, &t5358_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28037_GM};
extern Il2CppType t174_0_0_0;
static ParameterInfo t5358_m28038_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t174_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28038_GM;
MethodInfo m28038_MI = 
{
	"Contains", NULL, &t5358_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5358_m28038_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28038_GM};
extern Il2CppType t3841_0_0_0;
extern Il2CppType t3841_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5358_m28039_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3841_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28039_GM;
MethodInfo m28039_MI = 
{
	"CopyTo", NULL, &t5358_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5358_m28039_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28039_GM};
extern Il2CppType t174_0_0_0;
static ParameterInfo t5358_m28040_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t174_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28040_GM;
MethodInfo m28040_MI = 
{
	"Remove", NULL, &t5358_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5358_m28040_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28040_GM};
static MethodInfo* t5358_MIs[] =
{
	&m28034_MI,
	&m28035_MI,
	&m28036_MI,
	&m28037_MI,
	&m28038_MI,
	&m28039_MI,
	&m28040_MI,
	NULL
};
extern TypeInfo t5360_TI;
static TypeInfo* t5358_ITIs[] = 
{
	&t603_TI,
	&t5360_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5358_0_0_0;
extern Il2CppType t5358_1_0_0;
struct t5358;
extern Il2CppGenericClass t5358_GC;
TypeInfo t5358_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5358_MIs, t5358_PIs, NULL, NULL, NULL, NULL, NULL, &t5358_TI, t5358_ITIs, NULL, &EmptyCustomAttributesCache, &t5358_TI, &t5358_0_0_0, &t5358_1_0_0, NULL, &t5358_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/OriginHorizontal>
extern Il2CppType t4181_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28041_GM;
MethodInfo m28041_MI = 
{
	"GetEnumerator", NULL, &t5360_TI, &t4181_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28041_GM};
static MethodInfo* t5360_MIs[] =
{
	&m28041_MI,
	NULL
};
static TypeInfo* t5360_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5360_0_0_0;
extern Il2CppType t5360_1_0_0;
struct t5360;
extern Il2CppGenericClass t5360_GC;
TypeInfo t5360_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5360_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5360_TI, t5360_ITIs, NULL, &EmptyCustomAttributesCache, &t5360_TI, &t5360_0_0_0, &t5360_1_0_0, NULL, &t5360_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5359_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginHorizontal>
extern MethodInfo m28042_MI;
extern MethodInfo m28043_MI;
static PropertyInfo t5359____Item_PropertyInfo = 
{
	&t5359_TI, "Item", &m28042_MI, &m28043_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5359_PIs[] =
{
	&t5359____Item_PropertyInfo,
	NULL
};
extern Il2CppType t174_0_0_0;
static ParameterInfo t5359_m28044_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t174_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28044_GM;
MethodInfo m28044_MI = 
{
	"IndexOf", NULL, &t5359_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5359_m28044_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28044_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t174_0_0_0;
static ParameterInfo t5359_m28045_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t174_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28045_GM;
MethodInfo m28045_MI = 
{
	"Insert", NULL, &t5359_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5359_m28045_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28045_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5359_m28046_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28046_GM;
MethodInfo m28046_MI = 
{
	"RemoveAt", NULL, &t5359_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5359_m28046_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28046_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5359_m28042_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t174_0_0_0;
extern void* RuntimeInvoker_t174_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28042_GM;
MethodInfo m28042_MI = 
{
	"get_Item", NULL, &t5359_TI, &t174_0_0_0, RuntimeInvoker_t174_t44, t5359_m28042_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28042_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t174_0_0_0;
static ParameterInfo t5359_m28043_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t174_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28043_GM;
MethodInfo m28043_MI = 
{
	"set_Item", NULL, &t5359_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5359_m28043_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28043_GM};
static MethodInfo* t5359_MIs[] =
{
	&m28044_MI,
	&m28045_MI,
	&m28046_MI,
	&m28042_MI,
	&m28043_MI,
	NULL
};
static TypeInfo* t5359_ITIs[] = 
{
	&t603_TI,
	&t5358_TI,
	&t5360_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5359_0_0_0;
extern Il2CppType t5359_1_0_0;
struct t5359;
extern Il2CppGenericClass t5359_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5359_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5359_MIs, t5359_PIs, NULL, NULL, NULL, NULL, NULL, &t5359_TI, t5359_ITIs, NULL, &t1908__CustomAttributeCache, &t5359_TI, &t5359_0_0_0, &t5359_1_0_0, NULL, &t5359_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4183_TI;

#include "t175.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/OriginVertical>
extern MethodInfo m28047_MI;
static PropertyInfo t4183____Current_PropertyInfo = 
{
	&t4183_TI, "Current", &m28047_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4183_PIs[] =
{
	&t4183____Current_PropertyInfo,
	NULL
};
extern Il2CppType t175_0_0_0;
extern void* RuntimeInvoker_t175 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28047_GM;
MethodInfo m28047_MI = 
{
	"get_Current", NULL, &t4183_TI, &t175_0_0_0, RuntimeInvoker_t175, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28047_GM};
static MethodInfo* t4183_MIs[] =
{
	&m28047_MI,
	NULL
};
static TypeInfo* t4183_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4183_0_0_0;
extern Il2CppType t4183_1_0_0;
struct t4183;
extern Il2CppGenericClass t4183_GC;
TypeInfo t4183_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4183_MIs, t4183_PIs, NULL, NULL, NULL, NULL, NULL, &t4183_TI, t4183_ITIs, NULL, &EmptyCustomAttributesCache, &t4183_TI, &t4183_0_0_0, &t4183_1_0_0, NULL, &t4183_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2596.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2596_TI;
#include "t2596MD.h"

extern TypeInfo t175_TI;
extern MethodInfo m13995_MI;
extern MethodInfo m21137_MI;
struct t20;
 int32_t m21137 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m13991_MI;
 void m13991 (t2596 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13992_MI;
 t29 * m13992 (t2596 * __this, MethodInfo* method){
	{
		int32_t L_0 = m13995(__this, &m13995_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t175_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13993_MI;
 void m13993 (t2596 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13994_MI;
 bool m13994 (t2596 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m13995 (t2596 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21137(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21137_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>
extern Il2CppType t20_0_0_1;
FieldInfo t2596_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2596_TI, offsetof(t2596, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2596_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2596_TI, offsetof(t2596, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2596_FIs[] =
{
	&t2596_f0_FieldInfo,
	&t2596_f1_FieldInfo,
	NULL
};
static PropertyInfo t2596____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2596_TI, "System.Collections.IEnumerator.Current", &m13992_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2596____Current_PropertyInfo = 
{
	&t2596_TI, "Current", &m13995_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2596_PIs[] =
{
	&t2596____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2596____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2596_m13991_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13991_GM;
MethodInfo m13991_MI = 
{
	".ctor", (methodPointerType)&m13991, &t2596_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2596_m13991_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13991_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13992_GM;
MethodInfo m13992_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13992, &t2596_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13992_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13993_GM;
MethodInfo m13993_MI = 
{
	"Dispose", (methodPointerType)&m13993, &t2596_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13993_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13994_GM;
MethodInfo m13994_MI = 
{
	"MoveNext", (methodPointerType)&m13994, &t2596_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13994_GM};
extern Il2CppType t175_0_0_0;
extern void* RuntimeInvoker_t175 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13995_GM;
MethodInfo m13995_MI = 
{
	"get_Current", (methodPointerType)&m13995, &t2596_TI, &t175_0_0_0, RuntimeInvoker_t175, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13995_GM};
static MethodInfo* t2596_MIs[] =
{
	&m13991_MI,
	&m13992_MI,
	&m13993_MI,
	&m13994_MI,
	&m13995_MI,
	NULL
};
static MethodInfo* t2596_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13992_MI,
	&m13994_MI,
	&m13993_MI,
	&m13995_MI,
};
static TypeInfo* t2596_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4183_TI,
};
static Il2CppInterfaceOffsetPair t2596_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4183_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2596_0_0_0;
extern Il2CppType t2596_1_0_0;
extern Il2CppGenericClass t2596_GC;
TypeInfo t2596_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2596_MIs, t2596_PIs, t2596_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2596_TI, t2596_ITIs, t2596_VT, &EmptyCustomAttributesCache, &t2596_TI, &t2596_0_0_0, &t2596_1_0_0, t2596_IOs, &t2596_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2596)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5361_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>
extern MethodInfo m28048_MI;
static PropertyInfo t5361____Count_PropertyInfo = 
{
	&t5361_TI, "Count", &m28048_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28049_MI;
static PropertyInfo t5361____IsReadOnly_PropertyInfo = 
{
	&t5361_TI, "IsReadOnly", &m28049_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5361_PIs[] =
{
	&t5361____Count_PropertyInfo,
	&t5361____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28048_GM;
MethodInfo m28048_MI = 
{
	"get_Count", NULL, &t5361_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28048_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28049_GM;
MethodInfo m28049_MI = 
{
	"get_IsReadOnly", NULL, &t5361_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28049_GM};
extern Il2CppType t175_0_0_0;
extern Il2CppType t175_0_0_0;
static ParameterInfo t5361_m28050_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t175_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28050_GM;
MethodInfo m28050_MI = 
{
	"Add", NULL, &t5361_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5361_m28050_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28050_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28051_GM;
MethodInfo m28051_MI = 
{
	"Clear", NULL, &t5361_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28051_GM};
extern Il2CppType t175_0_0_0;
static ParameterInfo t5361_m28052_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t175_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28052_GM;
MethodInfo m28052_MI = 
{
	"Contains", NULL, &t5361_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5361_m28052_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28052_GM};
extern Il2CppType t3842_0_0_0;
extern Il2CppType t3842_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5361_m28053_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3842_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28053_GM;
MethodInfo m28053_MI = 
{
	"CopyTo", NULL, &t5361_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5361_m28053_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28053_GM};
extern Il2CppType t175_0_0_0;
static ParameterInfo t5361_m28054_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t175_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28054_GM;
MethodInfo m28054_MI = 
{
	"Remove", NULL, &t5361_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5361_m28054_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28054_GM};
static MethodInfo* t5361_MIs[] =
{
	&m28048_MI,
	&m28049_MI,
	&m28050_MI,
	&m28051_MI,
	&m28052_MI,
	&m28053_MI,
	&m28054_MI,
	NULL
};
extern TypeInfo t5363_TI;
static TypeInfo* t5361_ITIs[] = 
{
	&t603_TI,
	&t5363_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5361_0_0_0;
extern Il2CppType t5361_1_0_0;
struct t5361;
extern Il2CppGenericClass t5361_GC;
TypeInfo t5361_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5361_MIs, t5361_PIs, NULL, NULL, NULL, NULL, NULL, &t5361_TI, t5361_ITIs, NULL, &EmptyCustomAttributesCache, &t5361_TI, &t5361_0_0_0, &t5361_1_0_0, NULL, &t5361_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/OriginVertical>
extern Il2CppType t4183_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28055_GM;
MethodInfo m28055_MI = 
{
	"GetEnumerator", NULL, &t5363_TI, &t4183_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28055_GM};
static MethodInfo* t5363_MIs[] =
{
	&m28055_MI,
	NULL
};
static TypeInfo* t5363_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5363_0_0_0;
extern Il2CppType t5363_1_0_0;
struct t5363;
extern Il2CppGenericClass t5363_GC;
TypeInfo t5363_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5363_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5363_TI, t5363_ITIs, NULL, &EmptyCustomAttributesCache, &t5363_TI, &t5363_0_0_0, &t5363_1_0_0, NULL, &t5363_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5362_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginVertical>
extern MethodInfo m28056_MI;
extern MethodInfo m28057_MI;
static PropertyInfo t5362____Item_PropertyInfo = 
{
	&t5362_TI, "Item", &m28056_MI, &m28057_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5362_PIs[] =
{
	&t5362____Item_PropertyInfo,
	NULL
};
extern Il2CppType t175_0_0_0;
static ParameterInfo t5362_m28058_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t175_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28058_GM;
MethodInfo m28058_MI = 
{
	"IndexOf", NULL, &t5362_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5362_m28058_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28058_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t175_0_0_0;
static ParameterInfo t5362_m28059_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t175_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28059_GM;
MethodInfo m28059_MI = 
{
	"Insert", NULL, &t5362_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5362_m28059_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28059_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5362_m28060_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28060_GM;
MethodInfo m28060_MI = 
{
	"RemoveAt", NULL, &t5362_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5362_m28060_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28060_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5362_m28056_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t175_0_0_0;
extern void* RuntimeInvoker_t175_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28056_GM;
MethodInfo m28056_MI = 
{
	"get_Item", NULL, &t5362_TI, &t175_0_0_0, RuntimeInvoker_t175_t44, t5362_m28056_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28056_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t175_0_0_0;
static ParameterInfo t5362_m28057_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t175_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28057_GM;
MethodInfo m28057_MI = 
{
	"set_Item", NULL, &t5362_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5362_m28057_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28057_GM};
static MethodInfo* t5362_MIs[] =
{
	&m28058_MI,
	&m28059_MI,
	&m28060_MI,
	&m28056_MI,
	&m28057_MI,
	NULL
};
static TypeInfo* t5362_ITIs[] = 
{
	&t603_TI,
	&t5361_TI,
	&t5363_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5362_0_0_0;
extern Il2CppType t5362_1_0_0;
struct t5362;
extern Il2CppGenericClass t5362_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5362_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5362_MIs, t5362_PIs, NULL, NULL, NULL, NULL, NULL, &t5362_TI, t5362_ITIs, NULL, &t1908__CustomAttributeCache, &t5362_TI, &t5362_0_0_0, &t5362_1_0_0, NULL, &t5362_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4185_TI;

#include "t176.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Origin90>
extern MethodInfo m28061_MI;
static PropertyInfo t4185____Current_PropertyInfo = 
{
	&t4185_TI, "Current", &m28061_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4185_PIs[] =
{
	&t4185____Current_PropertyInfo,
	NULL
};
extern Il2CppType t176_0_0_0;
extern void* RuntimeInvoker_t176 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28061_GM;
MethodInfo m28061_MI = 
{
	"get_Current", NULL, &t4185_TI, &t176_0_0_0, RuntimeInvoker_t176, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28061_GM};
static MethodInfo* t4185_MIs[] =
{
	&m28061_MI,
	NULL
};
static TypeInfo* t4185_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4185_0_0_0;
extern Il2CppType t4185_1_0_0;
struct t4185;
extern Il2CppGenericClass t4185_GC;
TypeInfo t4185_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4185_MIs, t4185_PIs, NULL, NULL, NULL, NULL, NULL, &t4185_TI, t4185_ITIs, NULL, &EmptyCustomAttributesCache, &t4185_TI, &t4185_0_0_0, &t4185_1_0_0, NULL, &t4185_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2597.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2597_TI;
#include "t2597MD.h"

extern TypeInfo t176_TI;
extern MethodInfo m14000_MI;
extern MethodInfo m21148_MI;
struct t20;
 int32_t m21148 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m13996_MI;
 void m13996 (t2597 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13997_MI;
 t29 * m13997 (t2597 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14000(__this, &m14000_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t176_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m13998_MI;
 void m13998 (t2597 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m13999_MI;
 bool m13999 (t2597 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14000 (t2597 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21148(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21148_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>
extern Il2CppType t20_0_0_1;
FieldInfo t2597_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2597_TI, offsetof(t2597, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2597_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2597_TI, offsetof(t2597, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2597_FIs[] =
{
	&t2597_f0_FieldInfo,
	&t2597_f1_FieldInfo,
	NULL
};
static PropertyInfo t2597____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2597_TI, "System.Collections.IEnumerator.Current", &m13997_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2597____Current_PropertyInfo = 
{
	&t2597_TI, "Current", &m14000_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2597_PIs[] =
{
	&t2597____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2597____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2597_m13996_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13996_GM;
MethodInfo m13996_MI = 
{
	".ctor", (methodPointerType)&m13996, &t2597_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2597_m13996_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m13996_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13997_GM;
MethodInfo m13997_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m13997, &t2597_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13997_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13998_GM;
MethodInfo m13998_MI = 
{
	"Dispose", (methodPointerType)&m13998, &t2597_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13998_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m13999_GM;
MethodInfo m13999_MI = 
{
	"MoveNext", (methodPointerType)&m13999, &t2597_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m13999_GM};
extern Il2CppType t176_0_0_0;
extern void* RuntimeInvoker_t176 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14000_GM;
MethodInfo m14000_MI = 
{
	"get_Current", (methodPointerType)&m14000, &t2597_TI, &t176_0_0_0, RuntimeInvoker_t176, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14000_GM};
static MethodInfo* t2597_MIs[] =
{
	&m13996_MI,
	&m13997_MI,
	&m13998_MI,
	&m13999_MI,
	&m14000_MI,
	NULL
};
static MethodInfo* t2597_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m13997_MI,
	&m13999_MI,
	&m13998_MI,
	&m14000_MI,
};
static TypeInfo* t2597_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4185_TI,
};
static Il2CppInterfaceOffsetPair t2597_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4185_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2597_0_0_0;
extern Il2CppType t2597_1_0_0;
extern Il2CppGenericClass t2597_GC;
TypeInfo t2597_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2597_MIs, t2597_PIs, t2597_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2597_TI, t2597_ITIs, t2597_VT, &EmptyCustomAttributesCache, &t2597_TI, &t2597_0_0_0, &t2597_1_0_0, t2597_IOs, &t2597_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2597)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5364_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>
extern MethodInfo m28062_MI;
static PropertyInfo t5364____Count_PropertyInfo = 
{
	&t5364_TI, "Count", &m28062_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28063_MI;
static PropertyInfo t5364____IsReadOnly_PropertyInfo = 
{
	&t5364_TI, "IsReadOnly", &m28063_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5364_PIs[] =
{
	&t5364____Count_PropertyInfo,
	&t5364____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28062_GM;
MethodInfo m28062_MI = 
{
	"get_Count", NULL, &t5364_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28062_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28063_GM;
MethodInfo m28063_MI = 
{
	"get_IsReadOnly", NULL, &t5364_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28063_GM};
extern Il2CppType t176_0_0_0;
extern Il2CppType t176_0_0_0;
static ParameterInfo t5364_m28064_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t176_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28064_GM;
MethodInfo m28064_MI = 
{
	"Add", NULL, &t5364_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5364_m28064_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28064_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28065_GM;
MethodInfo m28065_MI = 
{
	"Clear", NULL, &t5364_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28065_GM};
extern Il2CppType t176_0_0_0;
static ParameterInfo t5364_m28066_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t176_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28066_GM;
MethodInfo m28066_MI = 
{
	"Contains", NULL, &t5364_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5364_m28066_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28066_GM};
extern Il2CppType t3843_0_0_0;
extern Il2CppType t3843_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5364_m28067_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3843_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28067_GM;
MethodInfo m28067_MI = 
{
	"CopyTo", NULL, &t5364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5364_m28067_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28067_GM};
extern Il2CppType t176_0_0_0;
static ParameterInfo t5364_m28068_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t176_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28068_GM;
MethodInfo m28068_MI = 
{
	"Remove", NULL, &t5364_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5364_m28068_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28068_GM};
static MethodInfo* t5364_MIs[] =
{
	&m28062_MI,
	&m28063_MI,
	&m28064_MI,
	&m28065_MI,
	&m28066_MI,
	&m28067_MI,
	&m28068_MI,
	NULL
};
extern TypeInfo t5366_TI;
static TypeInfo* t5364_ITIs[] = 
{
	&t603_TI,
	&t5366_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5364_0_0_0;
extern Il2CppType t5364_1_0_0;
struct t5364;
extern Il2CppGenericClass t5364_GC;
TypeInfo t5364_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5364_MIs, t5364_PIs, NULL, NULL, NULL, NULL, NULL, &t5364_TI, t5364_ITIs, NULL, &EmptyCustomAttributesCache, &t5364_TI, &t5364_0_0_0, &t5364_1_0_0, NULL, &t5364_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Origin90>
extern Il2CppType t4185_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28069_GM;
MethodInfo m28069_MI = 
{
	"GetEnumerator", NULL, &t5366_TI, &t4185_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28069_GM};
static MethodInfo* t5366_MIs[] =
{
	&m28069_MI,
	NULL
};
static TypeInfo* t5366_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5366_0_0_0;
extern Il2CppType t5366_1_0_0;
struct t5366;
extern Il2CppGenericClass t5366_GC;
TypeInfo t5366_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5366_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5366_TI, t5366_ITIs, NULL, &EmptyCustomAttributesCache, &t5366_TI, &t5366_0_0_0, &t5366_1_0_0, NULL, &t5366_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5365_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin90>
extern MethodInfo m28070_MI;
extern MethodInfo m28071_MI;
static PropertyInfo t5365____Item_PropertyInfo = 
{
	&t5365_TI, "Item", &m28070_MI, &m28071_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5365_PIs[] =
{
	&t5365____Item_PropertyInfo,
	NULL
};
extern Il2CppType t176_0_0_0;
static ParameterInfo t5365_m28072_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t176_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28072_GM;
MethodInfo m28072_MI = 
{
	"IndexOf", NULL, &t5365_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5365_m28072_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28072_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t176_0_0_0;
static ParameterInfo t5365_m28073_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t176_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28073_GM;
MethodInfo m28073_MI = 
{
	"Insert", NULL, &t5365_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5365_m28073_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28073_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5365_m28074_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28074_GM;
MethodInfo m28074_MI = 
{
	"RemoveAt", NULL, &t5365_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5365_m28074_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28074_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5365_m28070_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t176_0_0_0;
extern void* RuntimeInvoker_t176_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28070_GM;
MethodInfo m28070_MI = 
{
	"get_Item", NULL, &t5365_TI, &t176_0_0_0, RuntimeInvoker_t176_t44, t5365_m28070_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28070_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t176_0_0_0;
static ParameterInfo t5365_m28071_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t176_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28071_GM;
MethodInfo m28071_MI = 
{
	"set_Item", NULL, &t5365_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5365_m28071_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28071_GM};
static MethodInfo* t5365_MIs[] =
{
	&m28072_MI,
	&m28073_MI,
	&m28074_MI,
	&m28070_MI,
	&m28071_MI,
	NULL
};
static TypeInfo* t5365_ITIs[] = 
{
	&t603_TI,
	&t5364_TI,
	&t5366_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5365_0_0_0;
extern Il2CppType t5365_1_0_0;
struct t5365;
extern Il2CppGenericClass t5365_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5365_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5365_MIs, t5365_PIs, NULL, NULL, NULL, NULL, NULL, &t5365_TI, t5365_ITIs, NULL, &t1908__CustomAttributeCache, &t5365_TI, &t5365_0_0_0, &t5365_1_0_0, NULL, &t5365_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4187_TI;

#include "t177.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Origin180>
extern MethodInfo m28075_MI;
static PropertyInfo t4187____Current_PropertyInfo = 
{
	&t4187_TI, "Current", &m28075_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4187_PIs[] =
{
	&t4187____Current_PropertyInfo,
	NULL
};
extern Il2CppType t177_0_0_0;
extern void* RuntimeInvoker_t177 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28075_GM;
MethodInfo m28075_MI = 
{
	"get_Current", NULL, &t4187_TI, &t177_0_0_0, RuntimeInvoker_t177, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28075_GM};
static MethodInfo* t4187_MIs[] =
{
	&m28075_MI,
	NULL
};
static TypeInfo* t4187_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4187_0_0_0;
extern Il2CppType t4187_1_0_0;
struct t4187;
extern Il2CppGenericClass t4187_GC;
TypeInfo t4187_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4187_MIs, t4187_PIs, NULL, NULL, NULL, NULL, NULL, &t4187_TI, t4187_ITIs, NULL, &EmptyCustomAttributesCache, &t4187_TI, &t4187_0_0_0, &t4187_1_0_0, NULL, &t4187_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2598.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2598_TI;
#include "t2598MD.h"

extern TypeInfo t177_TI;
extern MethodInfo m14005_MI;
extern MethodInfo m21159_MI;
struct t20;
 int32_t m21159 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14001_MI;
 void m14001 (t2598 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14002_MI;
 t29 * m14002 (t2598 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14005(__this, &m14005_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t177_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14003_MI;
 void m14003 (t2598 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14004_MI;
 bool m14004 (t2598 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14005 (t2598 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21159(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21159_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>
extern Il2CppType t20_0_0_1;
FieldInfo t2598_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2598_TI, offsetof(t2598, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2598_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2598_TI, offsetof(t2598, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2598_FIs[] =
{
	&t2598_f0_FieldInfo,
	&t2598_f1_FieldInfo,
	NULL
};
static PropertyInfo t2598____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2598_TI, "System.Collections.IEnumerator.Current", &m14002_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2598____Current_PropertyInfo = 
{
	&t2598_TI, "Current", &m14005_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2598_PIs[] =
{
	&t2598____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2598____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2598_m14001_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14001_GM;
MethodInfo m14001_MI = 
{
	".ctor", (methodPointerType)&m14001, &t2598_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2598_m14001_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14001_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14002_GM;
MethodInfo m14002_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14002, &t2598_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14002_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14003_GM;
MethodInfo m14003_MI = 
{
	"Dispose", (methodPointerType)&m14003, &t2598_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14003_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14004_GM;
MethodInfo m14004_MI = 
{
	"MoveNext", (methodPointerType)&m14004, &t2598_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14004_GM};
extern Il2CppType t177_0_0_0;
extern void* RuntimeInvoker_t177 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14005_GM;
MethodInfo m14005_MI = 
{
	"get_Current", (methodPointerType)&m14005, &t2598_TI, &t177_0_0_0, RuntimeInvoker_t177, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14005_GM};
static MethodInfo* t2598_MIs[] =
{
	&m14001_MI,
	&m14002_MI,
	&m14003_MI,
	&m14004_MI,
	&m14005_MI,
	NULL
};
static MethodInfo* t2598_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14002_MI,
	&m14004_MI,
	&m14003_MI,
	&m14005_MI,
};
static TypeInfo* t2598_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4187_TI,
};
static Il2CppInterfaceOffsetPair t2598_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4187_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2598_0_0_0;
extern Il2CppType t2598_1_0_0;
extern Il2CppGenericClass t2598_GC;
TypeInfo t2598_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2598_MIs, t2598_PIs, t2598_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2598_TI, t2598_ITIs, t2598_VT, &EmptyCustomAttributesCache, &t2598_TI, &t2598_0_0_0, &t2598_1_0_0, t2598_IOs, &t2598_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2598)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5367_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>
extern MethodInfo m28076_MI;
static PropertyInfo t5367____Count_PropertyInfo = 
{
	&t5367_TI, "Count", &m28076_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28077_MI;
static PropertyInfo t5367____IsReadOnly_PropertyInfo = 
{
	&t5367_TI, "IsReadOnly", &m28077_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5367_PIs[] =
{
	&t5367____Count_PropertyInfo,
	&t5367____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28076_GM;
MethodInfo m28076_MI = 
{
	"get_Count", NULL, &t5367_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28076_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28077_GM;
MethodInfo m28077_MI = 
{
	"get_IsReadOnly", NULL, &t5367_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28077_GM};
extern Il2CppType t177_0_0_0;
extern Il2CppType t177_0_0_0;
static ParameterInfo t5367_m28078_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t177_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28078_GM;
MethodInfo m28078_MI = 
{
	"Add", NULL, &t5367_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5367_m28078_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28078_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28079_GM;
MethodInfo m28079_MI = 
{
	"Clear", NULL, &t5367_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28079_GM};
extern Il2CppType t177_0_0_0;
static ParameterInfo t5367_m28080_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t177_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28080_GM;
MethodInfo m28080_MI = 
{
	"Contains", NULL, &t5367_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5367_m28080_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28080_GM};
extern Il2CppType t3844_0_0_0;
extern Il2CppType t3844_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5367_m28081_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3844_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28081_GM;
MethodInfo m28081_MI = 
{
	"CopyTo", NULL, &t5367_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5367_m28081_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28081_GM};
extern Il2CppType t177_0_0_0;
static ParameterInfo t5367_m28082_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t177_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28082_GM;
MethodInfo m28082_MI = 
{
	"Remove", NULL, &t5367_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5367_m28082_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28082_GM};
static MethodInfo* t5367_MIs[] =
{
	&m28076_MI,
	&m28077_MI,
	&m28078_MI,
	&m28079_MI,
	&m28080_MI,
	&m28081_MI,
	&m28082_MI,
	NULL
};
extern TypeInfo t5369_TI;
static TypeInfo* t5367_ITIs[] = 
{
	&t603_TI,
	&t5369_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5367_0_0_0;
extern Il2CppType t5367_1_0_0;
struct t5367;
extern Il2CppGenericClass t5367_GC;
TypeInfo t5367_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5367_MIs, t5367_PIs, NULL, NULL, NULL, NULL, NULL, &t5367_TI, t5367_ITIs, NULL, &EmptyCustomAttributesCache, &t5367_TI, &t5367_0_0_0, &t5367_1_0_0, NULL, &t5367_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Origin180>
extern Il2CppType t4187_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28083_GM;
MethodInfo m28083_MI = 
{
	"GetEnumerator", NULL, &t5369_TI, &t4187_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28083_GM};
static MethodInfo* t5369_MIs[] =
{
	&m28083_MI,
	NULL
};
static TypeInfo* t5369_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5369_0_0_0;
extern Il2CppType t5369_1_0_0;
struct t5369;
extern Il2CppGenericClass t5369_GC;
TypeInfo t5369_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5369_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5369_TI, t5369_ITIs, NULL, &EmptyCustomAttributesCache, &t5369_TI, &t5369_0_0_0, &t5369_1_0_0, NULL, &t5369_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5368_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin180>
extern MethodInfo m28084_MI;
extern MethodInfo m28085_MI;
static PropertyInfo t5368____Item_PropertyInfo = 
{
	&t5368_TI, "Item", &m28084_MI, &m28085_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5368_PIs[] =
{
	&t5368____Item_PropertyInfo,
	NULL
};
extern Il2CppType t177_0_0_0;
static ParameterInfo t5368_m28086_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t177_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28086_GM;
MethodInfo m28086_MI = 
{
	"IndexOf", NULL, &t5368_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5368_m28086_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28086_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t177_0_0_0;
static ParameterInfo t5368_m28087_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t177_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28087_GM;
MethodInfo m28087_MI = 
{
	"Insert", NULL, &t5368_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5368_m28087_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28087_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5368_m28088_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28088_GM;
MethodInfo m28088_MI = 
{
	"RemoveAt", NULL, &t5368_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5368_m28088_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28088_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5368_m28084_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t177_0_0_0;
extern void* RuntimeInvoker_t177_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28084_GM;
MethodInfo m28084_MI = 
{
	"get_Item", NULL, &t5368_TI, &t177_0_0_0, RuntimeInvoker_t177_t44, t5368_m28084_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28084_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t177_0_0_0;
static ParameterInfo t5368_m28085_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t177_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28085_GM;
MethodInfo m28085_MI = 
{
	"set_Item", NULL, &t5368_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5368_m28085_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28085_GM};
static MethodInfo* t5368_MIs[] =
{
	&m28086_MI,
	&m28087_MI,
	&m28088_MI,
	&m28084_MI,
	&m28085_MI,
	NULL
};
static TypeInfo* t5368_ITIs[] = 
{
	&t603_TI,
	&t5367_TI,
	&t5369_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5368_0_0_0;
extern Il2CppType t5368_1_0_0;
struct t5368;
extern Il2CppGenericClass t5368_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5368_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5368_MIs, t5368_PIs, NULL, NULL, NULL, NULL, NULL, &t5368_TI, t5368_ITIs, NULL, &t1908__CustomAttributeCache, &t5368_TI, &t5368_0_0_0, &t5368_1_0_0, NULL, &t5368_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4189_TI;

#include "t178.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Origin360>
extern MethodInfo m28089_MI;
static PropertyInfo t4189____Current_PropertyInfo = 
{
	&t4189_TI, "Current", &m28089_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4189_PIs[] =
{
	&t4189____Current_PropertyInfo,
	NULL
};
extern Il2CppType t178_0_0_0;
extern void* RuntimeInvoker_t178 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28089_GM;
MethodInfo m28089_MI = 
{
	"get_Current", NULL, &t4189_TI, &t178_0_0_0, RuntimeInvoker_t178, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28089_GM};
static MethodInfo* t4189_MIs[] =
{
	&m28089_MI,
	NULL
};
static TypeInfo* t4189_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4189_0_0_0;
extern Il2CppType t4189_1_0_0;
struct t4189;
extern Il2CppGenericClass t4189_GC;
TypeInfo t4189_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4189_MIs, t4189_PIs, NULL, NULL, NULL, NULL, NULL, &t4189_TI, t4189_ITIs, NULL, &EmptyCustomAttributesCache, &t4189_TI, &t4189_0_0_0, &t4189_1_0_0, NULL, &t4189_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2599.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2599_TI;
#include "t2599MD.h"

extern TypeInfo t178_TI;
extern MethodInfo m14010_MI;
extern MethodInfo m21170_MI;
struct t20;
 int32_t m21170 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14006_MI;
 void m14006 (t2599 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14007_MI;
 t29 * m14007 (t2599 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14010(__this, &m14010_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t178_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14008_MI;
 void m14008 (t2599 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14009_MI;
 bool m14009 (t2599 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14010 (t2599 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21170(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21170_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>
extern Il2CppType t20_0_0_1;
FieldInfo t2599_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2599_TI, offsetof(t2599, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2599_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2599_TI, offsetof(t2599, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2599_FIs[] =
{
	&t2599_f0_FieldInfo,
	&t2599_f1_FieldInfo,
	NULL
};
static PropertyInfo t2599____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2599_TI, "System.Collections.IEnumerator.Current", &m14007_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2599____Current_PropertyInfo = 
{
	&t2599_TI, "Current", &m14010_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2599_PIs[] =
{
	&t2599____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2599____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2599_m14006_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14006_GM;
MethodInfo m14006_MI = 
{
	".ctor", (methodPointerType)&m14006, &t2599_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2599_m14006_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14006_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14007_GM;
MethodInfo m14007_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14007, &t2599_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14007_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14008_GM;
MethodInfo m14008_MI = 
{
	"Dispose", (methodPointerType)&m14008, &t2599_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14008_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14009_GM;
MethodInfo m14009_MI = 
{
	"MoveNext", (methodPointerType)&m14009, &t2599_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14009_GM};
extern Il2CppType t178_0_0_0;
extern void* RuntimeInvoker_t178 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14010_GM;
MethodInfo m14010_MI = 
{
	"get_Current", (methodPointerType)&m14010, &t2599_TI, &t178_0_0_0, RuntimeInvoker_t178, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14010_GM};
static MethodInfo* t2599_MIs[] =
{
	&m14006_MI,
	&m14007_MI,
	&m14008_MI,
	&m14009_MI,
	&m14010_MI,
	NULL
};
static MethodInfo* t2599_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14007_MI,
	&m14009_MI,
	&m14008_MI,
	&m14010_MI,
};
static TypeInfo* t2599_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4189_TI,
};
static Il2CppInterfaceOffsetPair t2599_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4189_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2599_0_0_0;
extern Il2CppType t2599_1_0_0;
extern Il2CppGenericClass t2599_GC;
TypeInfo t2599_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2599_MIs, t2599_PIs, t2599_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2599_TI, t2599_ITIs, t2599_VT, &EmptyCustomAttributesCache, &t2599_TI, &t2599_0_0_0, &t2599_1_0_0, t2599_IOs, &t2599_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2599)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5370_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>
extern MethodInfo m28090_MI;
static PropertyInfo t5370____Count_PropertyInfo = 
{
	&t5370_TI, "Count", &m28090_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28091_MI;
static PropertyInfo t5370____IsReadOnly_PropertyInfo = 
{
	&t5370_TI, "IsReadOnly", &m28091_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5370_PIs[] =
{
	&t5370____Count_PropertyInfo,
	&t5370____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28090_GM;
MethodInfo m28090_MI = 
{
	"get_Count", NULL, &t5370_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28090_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28091_GM;
MethodInfo m28091_MI = 
{
	"get_IsReadOnly", NULL, &t5370_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28091_GM};
extern Il2CppType t178_0_0_0;
extern Il2CppType t178_0_0_0;
static ParameterInfo t5370_m28092_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t178_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28092_GM;
MethodInfo m28092_MI = 
{
	"Add", NULL, &t5370_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5370_m28092_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28092_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28093_GM;
MethodInfo m28093_MI = 
{
	"Clear", NULL, &t5370_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28093_GM};
extern Il2CppType t178_0_0_0;
static ParameterInfo t5370_m28094_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t178_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28094_GM;
MethodInfo m28094_MI = 
{
	"Contains", NULL, &t5370_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5370_m28094_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28094_GM};
extern Il2CppType t3845_0_0_0;
extern Il2CppType t3845_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5370_m28095_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3845_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28095_GM;
MethodInfo m28095_MI = 
{
	"CopyTo", NULL, &t5370_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5370_m28095_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28095_GM};
extern Il2CppType t178_0_0_0;
static ParameterInfo t5370_m28096_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t178_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28096_GM;
MethodInfo m28096_MI = 
{
	"Remove", NULL, &t5370_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5370_m28096_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28096_GM};
static MethodInfo* t5370_MIs[] =
{
	&m28090_MI,
	&m28091_MI,
	&m28092_MI,
	&m28093_MI,
	&m28094_MI,
	&m28095_MI,
	&m28096_MI,
	NULL
};
extern TypeInfo t5372_TI;
static TypeInfo* t5370_ITIs[] = 
{
	&t603_TI,
	&t5372_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5370_0_0_0;
extern Il2CppType t5370_1_0_0;
struct t5370;
extern Il2CppGenericClass t5370_GC;
TypeInfo t5370_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5370_MIs, t5370_PIs, NULL, NULL, NULL, NULL, NULL, &t5370_TI, t5370_ITIs, NULL, &EmptyCustomAttributesCache, &t5370_TI, &t5370_0_0_0, &t5370_1_0_0, NULL, &t5370_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Origin360>
extern Il2CppType t4189_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28097_GM;
MethodInfo m28097_MI = 
{
	"GetEnumerator", NULL, &t5372_TI, &t4189_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28097_GM};
static MethodInfo* t5372_MIs[] =
{
	&m28097_MI,
	NULL
};
static TypeInfo* t5372_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5372_0_0_0;
extern Il2CppType t5372_1_0_0;
struct t5372;
extern Il2CppGenericClass t5372_GC;
TypeInfo t5372_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5372_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5372_TI, t5372_ITIs, NULL, &EmptyCustomAttributesCache, &t5372_TI, &t5372_0_0_0, &t5372_1_0_0, NULL, &t5372_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5371_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin360>
extern MethodInfo m28098_MI;
extern MethodInfo m28099_MI;
static PropertyInfo t5371____Item_PropertyInfo = 
{
	&t5371_TI, "Item", &m28098_MI, &m28099_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5371_PIs[] =
{
	&t5371____Item_PropertyInfo,
	NULL
};
extern Il2CppType t178_0_0_0;
static ParameterInfo t5371_m28100_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t178_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28100_GM;
MethodInfo m28100_MI = 
{
	"IndexOf", NULL, &t5371_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5371_m28100_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28100_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t178_0_0_0;
static ParameterInfo t5371_m28101_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t178_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28101_GM;
MethodInfo m28101_MI = 
{
	"Insert", NULL, &t5371_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5371_m28101_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28101_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5371_m28102_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28102_GM;
MethodInfo m28102_MI = 
{
	"RemoveAt", NULL, &t5371_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5371_m28102_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28102_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5371_m28098_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t178_0_0_0;
extern void* RuntimeInvoker_t178_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28098_GM;
MethodInfo m28098_MI = 
{
	"get_Item", NULL, &t5371_TI, &t178_0_0_0, RuntimeInvoker_t178_t44, t5371_m28098_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28098_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t178_0_0_0;
static ParameterInfo t5371_m28099_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t178_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28099_GM;
MethodInfo m28099_MI = 
{
	"set_Item", NULL, &t5371_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5371_m28099_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28099_GM};
static MethodInfo* t5371_MIs[] =
{
	&m28100_MI,
	&m28101_MI,
	&m28102_MI,
	&m28098_MI,
	&m28099_MI,
	NULL
};
static TypeInfo* t5371_ITIs[] = 
{
	&t603_TI,
	&t5370_TI,
	&t5372_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5371_0_0_0;
extern Il2CppType t5371_1_0_0;
struct t5371;
extern Il2CppGenericClass t5371_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5371_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5371_MIs, t5371_PIs, NULL, NULL, NULL, NULL, NULL, &t5371_TI, t5371_ITIs, NULL, &t1908__CustomAttributeCache, &t5371_TI, &t5371_0_0_0, &t5371_1_0_0, NULL, &t5371_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4191_TI;

#include "t197.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField>
extern MethodInfo m28103_MI;
static PropertyInfo t4191____Current_PropertyInfo = 
{
	&t4191_TI, "Current", &m28103_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4191_PIs[] =
{
	&t4191____Current_PropertyInfo,
	NULL
};
extern Il2CppType t197_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28103_GM;
MethodInfo m28103_MI = 
{
	"get_Current", NULL, &t4191_TI, &t197_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28103_GM};
static MethodInfo* t4191_MIs[] =
{
	&m28103_MI,
	NULL
};
static TypeInfo* t4191_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4191_0_0_0;
extern Il2CppType t4191_1_0_0;
struct t4191;
extern Il2CppGenericClass t4191_GC;
TypeInfo t4191_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4191_MIs, t4191_PIs, NULL, NULL, NULL, NULL, NULL, &t4191_TI, t4191_ITIs, NULL, &EmptyCustomAttributesCache, &t4191_TI, &t4191_0_0_0, &t4191_1_0_0, NULL, &t4191_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2600.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2600_TI;
#include "t2600MD.h"

extern TypeInfo t197_TI;
extern MethodInfo m14015_MI;
extern MethodInfo m21181_MI;
struct t20;
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m21181(__this, p0, method) (t197 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>
extern Il2CppType t20_0_0_1;
FieldInfo t2600_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2600_TI, offsetof(t2600, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2600_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2600_TI, offsetof(t2600, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2600_FIs[] =
{
	&t2600_f0_FieldInfo,
	&t2600_f1_FieldInfo,
	NULL
};
extern MethodInfo m14012_MI;
static PropertyInfo t2600____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2600_TI, "System.Collections.IEnumerator.Current", &m14012_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2600____Current_PropertyInfo = 
{
	&t2600_TI, "Current", &m14015_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2600_PIs[] =
{
	&t2600____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2600____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2600_m14011_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14011_GM;
MethodInfo m14011_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2600_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2600_m14011_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14011_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14012_GM;
MethodInfo m14012_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2600_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14012_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14013_GM;
MethodInfo m14013_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2600_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14013_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14014_GM;
MethodInfo m14014_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2600_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14014_GM};
extern Il2CppType t197_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14015_GM;
MethodInfo m14015_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2600_TI, &t197_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14015_GM};
static MethodInfo* t2600_MIs[] =
{
	&m14011_MI,
	&m14012_MI,
	&m14013_MI,
	&m14014_MI,
	&m14015_MI,
	NULL
};
extern MethodInfo m14014_MI;
extern MethodInfo m14013_MI;
static MethodInfo* t2600_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14012_MI,
	&m14014_MI,
	&m14013_MI,
	&m14015_MI,
};
static TypeInfo* t2600_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4191_TI,
};
static Il2CppInterfaceOffsetPair t2600_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4191_TI, 7},
};
extern TypeInfo t197_TI;
static Il2CppRGCTXData t2600_RGCTXData[3] = 
{
	&m14015_MI/* Method Usage */,
	&t197_TI/* Class Usage */,
	&m21181_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2600_0_0_0;
extern Il2CppType t2600_1_0_0;
extern Il2CppGenericClass t2600_GC;
TypeInfo t2600_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2600_MIs, t2600_PIs, t2600_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2600_TI, t2600_ITIs, t2600_VT, &EmptyCustomAttributesCache, &t2600_TI, &t2600_0_0_0, &t2600_1_0_0, t2600_IOs, &t2600_GC, NULL, NULL, NULL, t2600_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2600)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5373_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>
extern MethodInfo m28104_MI;
static PropertyInfo t5373____Count_PropertyInfo = 
{
	&t5373_TI, "Count", &m28104_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28105_MI;
static PropertyInfo t5373____IsReadOnly_PropertyInfo = 
{
	&t5373_TI, "IsReadOnly", &m28105_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5373_PIs[] =
{
	&t5373____Count_PropertyInfo,
	&t5373____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28104_GM;
MethodInfo m28104_MI = 
{
	"get_Count", NULL, &t5373_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28104_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28105_GM;
MethodInfo m28105_MI = 
{
	"get_IsReadOnly", NULL, &t5373_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28105_GM};
extern Il2CppType t197_0_0_0;
extern Il2CppType t197_0_0_0;
static ParameterInfo t5373_m28106_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t197_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28106_GM;
MethodInfo m28106_MI = 
{
	"Add", NULL, &t5373_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5373_m28106_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28106_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28107_GM;
MethodInfo m28107_MI = 
{
	"Clear", NULL, &t5373_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28107_GM};
extern Il2CppType t197_0_0_0;
static ParameterInfo t5373_m28108_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t197_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28108_GM;
MethodInfo m28108_MI = 
{
	"Contains", NULL, &t5373_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5373_m28108_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28108_GM};
extern Il2CppType t3846_0_0_0;
extern Il2CppType t3846_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5373_m28109_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3846_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28109_GM;
MethodInfo m28109_MI = 
{
	"CopyTo", NULL, &t5373_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5373_m28109_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28109_GM};
extern Il2CppType t197_0_0_0;
static ParameterInfo t5373_m28110_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t197_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28110_GM;
MethodInfo m28110_MI = 
{
	"Remove", NULL, &t5373_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5373_m28110_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28110_GM};
static MethodInfo* t5373_MIs[] =
{
	&m28104_MI,
	&m28105_MI,
	&m28106_MI,
	&m28107_MI,
	&m28108_MI,
	&m28109_MI,
	&m28110_MI,
	NULL
};
extern TypeInfo t5375_TI;
static TypeInfo* t5373_ITIs[] = 
{
	&t603_TI,
	&t5375_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5373_0_0_0;
extern Il2CppType t5373_1_0_0;
struct t5373;
extern Il2CppGenericClass t5373_GC;
TypeInfo t5373_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5373_MIs, t5373_PIs, NULL, NULL, NULL, NULL, NULL, &t5373_TI, t5373_ITIs, NULL, &EmptyCustomAttributesCache, &t5373_TI, &t5373_0_0_0, &t5373_1_0_0, NULL, &t5373_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField>
extern Il2CppType t4191_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28111_GM;
MethodInfo m28111_MI = 
{
	"GetEnumerator", NULL, &t5375_TI, &t4191_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28111_GM};
static MethodInfo* t5375_MIs[] =
{
	&m28111_MI,
	NULL
};
static TypeInfo* t5375_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5375_0_0_0;
extern Il2CppType t5375_1_0_0;
struct t5375;
extern Il2CppGenericClass t5375_GC;
TypeInfo t5375_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5375_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5375_TI, t5375_ITIs, NULL, &EmptyCustomAttributesCache, &t5375_TI, &t5375_0_0_0, &t5375_1_0_0, NULL, &t5375_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5374_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.InputField>
extern MethodInfo m28112_MI;
extern MethodInfo m28113_MI;
static PropertyInfo t5374____Item_PropertyInfo = 
{
	&t5374_TI, "Item", &m28112_MI, &m28113_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5374_PIs[] =
{
	&t5374____Item_PropertyInfo,
	NULL
};
extern Il2CppType t197_0_0_0;
static ParameterInfo t5374_m28114_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t197_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28114_GM;
MethodInfo m28114_MI = 
{
	"IndexOf", NULL, &t5374_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5374_m28114_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28114_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t197_0_0_0;
static ParameterInfo t5374_m28115_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t197_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28115_GM;
MethodInfo m28115_MI = 
{
	"Insert", NULL, &t5374_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5374_m28115_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28115_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5374_m28116_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28116_GM;
MethodInfo m28116_MI = 
{
	"RemoveAt", NULL, &t5374_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5374_m28116_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28116_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5374_m28112_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t197_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28112_GM;
MethodInfo m28112_MI = 
{
	"get_Item", NULL, &t5374_TI, &t197_0_0_0, RuntimeInvoker_t29_t44, t5374_m28112_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28112_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t197_0_0_0;
static ParameterInfo t5374_m28113_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t197_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28113_GM;
MethodInfo m28113_MI = 
{
	"set_Item", NULL, &t5374_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5374_m28113_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28113_GM};
static MethodInfo* t5374_MIs[] =
{
	&m28114_MI,
	&m28115_MI,
	&m28116_MI,
	&m28112_MI,
	&m28113_MI,
	NULL
};
static TypeInfo* t5374_ITIs[] = 
{
	&t603_TI,
	&t5373_TI,
	&t5375_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5374_0_0_0;
extern Il2CppType t5374_1_0_0;
struct t5374;
extern Il2CppGenericClass t5374_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5374_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5374_MIs, t5374_PIs, NULL, NULL, NULL, NULL, NULL, &t5374_TI, t5374_ITIs, NULL, &t1908__CustomAttributeCache, &t5374_TI, &t5374_0_0_0, &t5374_1_0_0, NULL, &t5374_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2601.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2601_TI;
#include "t2601MD.h"

#include "t41.h"
#include "t557.h"
#include "mscorlib_ArrayTypes.h"
#include "t2602.h"
extern TypeInfo t316_TI;
extern TypeInfo t29_TI;
extern TypeInfo t2602_TI;
extern TypeInfo t21_TI;
#include "t2602MD.h"
extern MethodInfo m14018_MI;
extern MethodInfo m14020_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.InputField>
extern Il2CppType t316_0_0_33;
FieldInfo t2601_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2601_TI, offsetof(t2601, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2601_FIs[] =
{
	&t2601_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t197_0_0_0;
static ParameterInfo t2601_m14016_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t197_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14016_GM;
MethodInfo m14016_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2601_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2601_m14016_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14016_GM};
extern Il2CppType t316_0_0_0;
extern Il2CppType t316_0_0_0;
static ParameterInfo t2601_m14017_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14017_GM;
MethodInfo m14017_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2601_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2601_m14017_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14017_GM};
static MethodInfo* t2601_MIs[] =
{
	&m14016_MI,
	&m14017_MI,
	NULL
};
extern MethodInfo m1321_MI;
extern MethodInfo m1322_MI;
extern MethodInfo m1332_MI;
extern MethodInfo m14017_MI;
extern MethodInfo m14021_MI;
static MethodInfo* t2601_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14017_MI,
	&m14021_MI,
};
extern Il2CppType t2603_0_0_0;
extern TypeInfo t2603_TI;
extern MethodInfo m21191_MI;
extern TypeInfo t197_TI;
extern MethodInfo m14023_MI;
extern TypeInfo t197_TI;
static Il2CppRGCTXData t2601_RGCTXData[8] = 
{
	&t2603_0_0_0/* Type Usage */,
	&t2603_TI/* Class Usage */,
	&m21191_MI/* Method Usage */,
	&t197_TI/* Class Usage */,
	&m14023_MI/* Method Usage */,
	&m14018_MI/* Method Usage */,
	&t197_TI/* Class Usage */,
	&m14020_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2601_0_0_0;
extern Il2CppType t2601_1_0_0;
struct t2601;
extern Il2CppGenericClass t2601_GC;
TypeInfo t2601_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2601_MIs, NULL, t2601_FIs, NULL, &t2602_TI, NULL, NULL, &t2601_TI, NULL, t2601_VT, &EmptyCustomAttributesCache, &t2601_TI, &t2601_0_0_0, &t2601_1_0_0, NULL, &t2601_GC, NULL, NULL, NULL, t2601_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2601), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2603.h"
#include "t42.h"
#include "t43.h"
#include "t353.h"
#include "t305.h"
extern TypeInfo t2603_TI;
extern TypeInfo t42_TI;
extern TypeInfo t305_TI;
#include "t556MD.h"
#include "t42MD.h"
#include "t353MD.h"
#include "t305MD.h"
#include "t2603MD.h"
extern MethodInfo m2790_MI;
extern MethodInfo m1554_MI;
extern MethodInfo m2957_MI;
extern MethodInfo m1597_MI;
extern MethodInfo m2789_MI;
extern MethodInfo m1935_MI;
extern MethodInfo m2791_MI;
extern MethodInfo m2953_MI;
extern MethodInfo m2951_MI;
struct t556;
#include "t556.h"
struct t556;
 void m19555_gshared (t29 * __this, t29 * p0, MethodInfo* method);
#define m19555(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)
#define m21191(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>
extern Il2CppType t2603_0_0_1;
FieldInfo t2602_f0_FieldInfo = 
{
	"Delegate", &t2603_0_0_1, &t2602_TI, offsetof(t2602, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2602_FIs[] =
{
	&t2602_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2602_m14018_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14018_GM;
MethodInfo m14018_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2602_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2602_m14018_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14018_GM};
extern Il2CppType t2603_0_0_0;
static ParameterInfo t2602_m14019_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2603_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14019_GM;
MethodInfo m14019_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2602_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2602_m14019_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14019_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2602_m14020_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14020_GM;
MethodInfo m14020_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2602_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2602_m14020_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14020_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2602_m14021_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14021_GM;
MethodInfo m14021_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2602_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2602_m14021_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14021_GM};
static MethodInfo* t2602_MIs[] =
{
	&m14018_MI,
	&m14019_MI,
	&m14020_MI,
	&m14021_MI,
	NULL
};
static MethodInfo* t2602_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14020_MI,
	&m14021_MI,
};
extern TypeInfo t2603_TI;
extern TypeInfo t197_TI;
static Il2CppRGCTXData t2602_RGCTXData[5] = 
{
	&t2603_0_0_0/* Type Usage */,
	&t2603_TI/* Class Usage */,
	&m21191_MI/* Method Usage */,
	&t197_TI/* Class Usage */,
	&m14023_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2602_0_0_0;
extern Il2CppType t2602_1_0_0;
extern TypeInfo t556_TI;
struct t2602;
extern Il2CppGenericClass t2602_GC;
TypeInfo t2602_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2602_MIs, NULL, t2602_FIs, NULL, &t556_TI, NULL, NULL, &t2602_TI, NULL, t2602_VT, &EmptyCustomAttributesCache, &t2602_TI, &t2602_0_0_0, &t2602_1_0_0, NULL, &t2602_GC, NULL, NULL, NULL, t2602_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2602), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t35.h"
#include "t67.h"


// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.InputField>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2603_m14022_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14022_GM;
MethodInfo m14022_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2603_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2603_m14022_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14022_GM};
extern Il2CppType t197_0_0_0;
static ParameterInfo t2603_m14023_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t197_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14023_GM;
MethodInfo m14023_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2603_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2603_m14023_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14023_GM};
extern Il2CppType t197_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2603_m14024_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t197_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14024_GM;
MethodInfo m14024_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2603_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2603_m14024_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14024_GM};
extern Il2CppType t66_0_0_0;
extern Il2CppType t66_0_0_0;
static ParameterInfo t2603_m14025_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14025_GM;
MethodInfo m14025_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2603_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2603_m14025_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14025_GM};
static MethodInfo* t2603_MIs[] =
{
	&m14022_MI,
	&m14023_MI,
	&m14024_MI,
	&m14025_MI,
	NULL
};
extern MethodInfo m1699_MI;
extern MethodInfo m1700_MI;
extern MethodInfo m1701_MI;
extern MethodInfo m1702_MI;
extern MethodInfo m1703_MI;
extern MethodInfo m1704_MI;
extern MethodInfo m1705_MI;
extern MethodInfo m14024_MI;
extern MethodInfo m14025_MI;
static MethodInfo* t2603_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14023_MI,
	&m14024_MI,
	&m14025_MI,
};
extern TypeInfo t373_TI;
extern TypeInfo t374_TI;
static Il2CppInterfaceOffsetPair t2603_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2603_1_0_0;
extern TypeInfo t195_TI;
struct t2603;
extern Il2CppGenericClass t2603_GC;
TypeInfo t2603_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2603_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2603_TI, NULL, t2603_VT, &EmptyCustomAttributesCache, &t2603_TI, &t2603_0_0_0, &t2603_1_0_0, t2603_IOs, &t2603_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2603), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4197_TI;

#include "t185.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/ContentType>
extern MethodInfo m28117_MI;
static PropertyInfo t4197____Current_PropertyInfo = 
{
	&t4197_TI, "Current", &m28117_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4197_PIs[] =
{
	&t4197____Current_PropertyInfo,
	NULL
};
extern Il2CppType t185_0_0_0;
extern void* RuntimeInvoker_t185 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28117_GM;
MethodInfo m28117_MI = 
{
	"get_Current", NULL, &t4197_TI, &t185_0_0_0, RuntimeInvoker_t185, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28117_GM};
static MethodInfo* t4197_MIs[] =
{
	&m28117_MI,
	NULL
};
static TypeInfo* t4197_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4197_0_0_0;
extern Il2CppType t4197_1_0_0;
struct t4197;
extern Il2CppGenericClass t4197_GC;
TypeInfo t4197_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4197_MIs, t4197_PIs, NULL, NULL, NULL, NULL, NULL, &t4197_TI, t4197_ITIs, NULL, &EmptyCustomAttributesCache, &t4197_TI, &t4197_0_0_0, &t4197_1_0_0, NULL, &t4197_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2604.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2604_TI;
#include "t2604MD.h"

extern TypeInfo t185_TI;
extern MethodInfo m14030_MI;
extern MethodInfo m21193_MI;
struct t20;
 int32_t m21193 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14026_MI;
 void m14026 (t2604 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14027_MI;
 t29 * m14027 (t2604 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14030(__this, &m14030_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t185_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14028_MI;
 void m14028 (t2604 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14029_MI;
 bool m14029 (t2604 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14030 (t2604 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21193(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21193_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>
extern Il2CppType t20_0_0_1;
FieldInfo t2604_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2604_TI, offsetof(t2604, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2604_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2604_TI, offsetof(t2604, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2604_FIs[] =
{
	&t2604_f0_FieldInfo,
	&t2604_f1_FieldInfo,
	NULL
};
static PropertyInfo t2604____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2604_TI, "System.Collections.IEnumerator.Current", &m14027_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2604____Current_PropertyInfo = 
{
	&t2604_TI, "Current", &m14030_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2604_PIs[] =
{
	&t2604____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2604____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2604_m14026_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14026_GM;
MethodInfo m14026_MI = 
{
	".ctor", (methodPointerType)&m14026, &t2604_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2604_m14026_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14026_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14027_GM;
MethodInfo m14027_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14027, &t2604_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14027_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14028_GM;
MethodInfo m14028_MI = 
{
	"Dispose", (methodPointerType)&m14028, &t2604_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14028_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14029_GM;
MethodInfo m14029_MI = 
{
	"MoveNext", (methodPointerType)&m14029, &t2604_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14029_GM};
extern Il2CppType t185_0_0_0;
extern void* RuntimeInvoker_t185 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14030_GM;
MethodInfo m14030_MI = 
{
	"get_Current", (methodPointerType)&m14030, &t2604_TI, &t185_0_0_0, RuntimeInvoker_t185, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14030_GM};
static MethodInfo* t2604_MIs[] =
{
	&m14026_MI,
	&m14027_MI,
	&m14028_MI,
	&m14029_MI,
	&m14030_MI,
	NULL
};
static MethodInfo* t2604_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14027_MI,
	&m14029_MI,
	&m14028_MI,
	&m14030_MI,
};
static TypeInfo* t2604_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4197_TI,
};
static Il2CppInterfaceOffsetPair t2604_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4197_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2604_0_0_0;
extern Il2CppType t2604_1_0_0;
extern Il2CppGenericClass t2604_GC;
TypeInfo t2604_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2604_MIs, t2604_PIs, t2604_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2604_TI, t2604_ITIs, t2604_VT, &EmptyCustomAttributesCache, &t2604_TI, &t2604_0_0_0, &t2604_1_0_0, t2604_IOs, &t2604_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2604)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5376_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>
extern MethodInfo m28118_MI;
static PropertyInfo t5376____Count_PropertyInfo = 
{
	&t5376_TI, "Count", &m28118_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28119_MI;
static PropertyInfo t5376____IsReadOnly_PropertyInfo = 
{
	&t5376_TI, "IsReadOnly", &m28119_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5376_PIs[] =
{
	&t5376____Count_PropertyInfo,
	&t5376____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28118_GM;
MethodInfo m28118_MI = 
{
	"get_Count", NULL, &t5376_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28118_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28119_GM;
MethodInfo m28119_MI = 
{
	"get_IsReadOnly", NULL, &t5376_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28119_GM};
extern Il2CppType t185_0_0_0;
extern Il2CppType t185_0_0_0;
static ParameterInfo t5376_m28120_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t185_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28120_GM;
MethodInfo m28120_MI = 
{
	"Add", NULL, &t5376_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5376_m28120_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28120_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28121_GM;
MethodInfo m28121_MI = 
{
	"Clear", NULL, &t5376_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28121_GM};
extern Il2CppType t185_0_0_0;
static ParameterInfo t5376_m28122_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t185_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28122_GM;
MethodInfo m28122_MI = 
{
	"Contains", NULL, &t5376_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5376_m28122_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28122_GM};
extern Il2CppType t206_0_0_0;
extern Il2CppType t206_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5376_m28123_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t206_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28123_GM;
MethodInfo m28123_MI = 
{
	"CopyTo", NULL, &t5376_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5376_m28123_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28123_GM};
extern Il2CppType t185_0_0_0;
static ParameterInfo t5376_m28124_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t185_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28124_GM;
MethodInfo m28124_MI = 
{
	"Remove", NULL, &t5376_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5376_m28124_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28124_GM};
static MethodInfo* t5376_MIs[] =
{
	&m28118_MI,
	&m28119_MI,
	&m28120_MI,
	&m28121_MI,
	&m28122_MI,
	&m28123_MI,
	&m28124_MI,
	NULL
};
extern TypeInfo t5378_TI;
static TypeInfo* t5376_ITIs[] = 
{
	&t603_TI,
	&t5378_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5376_0_0_0;
extern Il2CppType t5376_1_0_0;
struct t5376;
extern Il2CppGenericClass t5376_GC;
TypeInfo t5376_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5376_MIs, t5376_PIs, NULL, NULL, NULL, NULL, NULL, &t5376_TI, t5376_ITIs, NULL, &EmptyCustomAttributesCache, &t5376_TI, &t5376_0_0_0, &t5376_1_0_0, NULL, &t5376_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/ContentType>
extern Il2CppType t4197_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28125_GM;
MethodInfo m28125_MI = 
{
	"GetEnumerator", NULL, &t5378_TI, &t4197_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28125_GM};
static MethodInfo* t5378_MIs[] =
{
	&m28125_MI,
	NULL
};
static TypeInfo* t5378_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5378_0_0_0;
extern Il2CppType t5378_1_0_0;
struct t5378;
extern Il2CppGenericClass t5378_GC;
TypeInfo t5378_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5378_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5378_TI, t5378_ITIs, NULL, &EmptyCustomAttributesCache, &t5378_TI, &t5378_0_0_0, &t5378_1_0_0, NULL, &t5378_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5377_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.InputField/ContentType>
extern MethodInfo m28126_MI;
extern MethodInfo m28127_MI;
static PropertyInfo t5377____Item_PropertyInfo = 
{
	&t5377_TI, "Item", &m28126_MI, &m28127_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5377_PIs[] =
{
	&t5377____Item_PropertyInfo,
	NULL
};
extern Il2CppType t185_0_0_0;
static ParameterInfo t5377_m28128_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t185_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28128_GM;
MethodInfo m28128_MI = 
{
	"IndexOf", NULL, &t5377_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5377_m28128_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28128_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t185_0_0_0;
static ParameterInfo t5377_m28129_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t185_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28129_GM;
MethodInfo m28129_MI = 
{
	"Insert", NULL, &t5377_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5377_m28129_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28129_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5377_m28130_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28130_GM;
MethodInfo m28130_MI = 
{
	"RemoveAt", NULL, &t5377_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5377_m28130_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28130_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5377_m28126_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t185_0_0_0;
extern void* RuntimeInvoker_t185_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28126_GM;
MethodInfo m28126_MI = 
{
	"get_Item", NULL, &t5377_TI, &t185_0_0_0, RuntimeInvoker_t185_t44, t5377_m28126_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28126_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t185_0_0_0;
static ParameterInfo t5377_m28127_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t185_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28127_GM;
MethodInfo m28127_MI = 
{
	"set_Item", NULL, &t5377_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5377_m28127_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28127_GM};
static MethodInfo* t5377_MIs[] =
{
	&m28128_MI,
	&m28129_MI,
	&m28130_MI,
	&m28126_MI,
	&m28127_MI,
	NULL
};
static TypeInfo* t5377_ITIs[] = 
{
	&t603_TI,
	&t5376_TI,
	&t5378_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5377_0_0_0;
extern Il2CppType t5377_1_0_0;
struct t5377;
extern Il2CppGenericClass t5377_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5377_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5377_MIs, t5377_PIs, NULL, NULL, NULL, NULL, NULL, &t5377_TI, t5377_ITIs, NULL, &t1908__CustomAttributeCache, &t5377_TI, &t5377_0_0_0, &t5377_1_0_0, NULL, &t5377_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t387_TI;

#include "t380.h"


// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
extern MethodInfo m1749_MI;
extern MethodInfo m28131_MI;
static PropertyInfo t387____Item_PropertyInfo = 
{
	&t387_TI, "Item", &m1749_MI, &m28131_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t387_PIs[] =
{
	&t387____Item_PropertyInfo,
	NULL
};
extern Il2CppType t380_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t387_m28132_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28132_GM;
MethodInfo m28132_MI = 
{
	"IndexOf", NULL, &t387_TI, &t44_0_0_0, RuntimeInvoker_t44_t380, t387_m28132_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28132_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t387_m28133_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28133_GM;
MethodInfo m28133_MI = 
{
	"Insert", NULL, &t387_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t380, t387_m28133_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28133_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t387_m28134_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28134_GM;
MethodInfo m28134_MI = 
{
	"RemoveAt", NULL, &t387_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t387_m28134_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28134_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t387_m1749_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t380_0_0_0;
extern void* RuntimeInvoker_t380_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1749_GM;
MethodInfo m1749_MI = 
{
	"get_Item", NULL, &t387_TI, &t380_0_0_0, RuntimeInvoker_t380_t44, t387_m1749_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1749_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t380_0_0_0;
static ParameterInfo t387_m28131_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28131_GM;
MethodInfo m28131_MI = 
{
	"set_Item", NULL, &t387_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t380, t387_m28131_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28131_GM};
static MethodInfo* t387_MIs[] =
{
	&m28132_MI,
	&m28133_MI,
	&m28134_MI,
	&m1749_MI,
	&m28131_MI,
	NULL
};
extern TypeInfo t389_TI;
extern TypeInfo t3042_TI;
static TypeInfo* t387_ITIs[] = 
{
	&t603_TI,
	&t389_TI,
	&t3042_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t387_0_0_0;
extern Il2CppType t387_1_0_0;
struct t387;
extern Il2CppGenericClass t387_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t387_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t387_MIs, t387_PIs, NULL, NULL, NULL, NULL, NULL, &t387_TI, t387_ITIs, NULL, &t1908__CustomAttributeCache, &t387_TI, &t387_0_0_0, &t387_1_0_0, NULL, &t387_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif

#include "UnityEngine_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>
extern MethodInfo m1775_MI;
static PropertyInfo t389____Count_PropertyInfo = 
{
	&t389_TI, "Count", &m1775_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28135_MI;
static PropertyInfo t389____IsReadOnly_PropertyInfo = 
{
	&t389_TI, "IsReadOnly", &m28135_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t389_PIs[] =
{
	&t389____Count_PropertyInfo,
	&t389____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1775_GM;
MethodInfo m1775_MI = 
{
	"get_Count", NULL, &t389_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1775_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28135_GM;
MethodInfo m28135_MI = 
{
	"get_IsReadOnly", NULL, &t389_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28135_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t389_m28136_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28136_GM;
MethodInfo m28136_MI = 
{
	"Add", NULL, &t389_TI, &t21_0_0_0, RuntimeInvoker_t21_t380, t389_m28136_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28136_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28137_GM;
MethodInfo m28137_MI = 
{
	"Clear", NULL, &t389_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28137_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t389_m28138_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28138_GM;
MethodInfo m28138_MI = 
{
	"Contains", NULL, &t389_TI, &t40_0_0_0, RuntimeInvoker_t40_t380, t389_m28138_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28138_GM};
extern Il2CppType t531_0_0_0;
extern Il2CppType t531_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t389_m28139_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t531_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28139_GM;
MethodInfo m28139_MI = 
{
	"CopyTo", NULL, &t389_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t389_m28139_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28139_GM};
extern Il2CppType t380_0_0_0;
static ParameterInfo t389_m28140_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t380_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28140_GM;
MethodInfo m28140_MI = 
{
	"Remove", NULL, &t389_TI, &t40_0_0_0, RuntimeInvoker_t40_t380, t389_m28140_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28140_GM};
static MethodInfo* t389_MIs[] =
{
	&m1775_MI,
	&m28135_MI,
	&m28136_MI,
	&m28137_MI,
	&m28138_MI,
	&m28139_MI,
	&m28140_MI,
	NULL
};
static TypeInfo* t389_ITIs[] = 
{
	&t603_TI,
	&t3042_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t389_0_0_0;
extern Il2CppType t389_1_0_0;
struct t389;
extern Il2CppGenericClass t389_GC;
TypeInfo t389_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t389_MIs, t389_PIs, NULL, NULL, NULL, NULL, NULL, &t389_TI, t389_ITIs, NULL, &EmptyCustomAttributesCache, &t389_TI, &t389_0_0_0, &t389_1_0_0, NULL, &t389_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UILineInfo>
extern Il2CppType t3041_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28141_GM;
MethodInfo m28141_MI = 
{
	"GetEnumerator", NULL, &t3042_TI, &t3041_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28141_GM};
static MethodInfo* t3042_MIs[] =
{
	&m28141_MI,
	NULL
};
static TypeInfo* t3042_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3042_0_0_0;
extern Il2CppType t3042_1_0_0;
struct t3042;
extern Il2CppGenericClass t3042_GC;
TypeInfo t3042_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t3042_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t3042_TI, t3042_ITIs, NULL, &EmptyCustomAttributesCache, &t3042_TI, &t3042_0_0_0, &t3042_1_0_0, NULL, &t3042_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3041_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
extern MethodInfo m28142_MI;
static PropertyInfo t3041____Current_PropertyInfo = 
{
	&t3041_TI, "Current", &m28142_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3041_PIs[] =
{
	&t3041____Current_PropertyInfo,
	NULL
};
extern Il2CppType t380_0_0_0;
extern void* RuntimeInvoker_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28142_GM;
MethodInfo m28142_MI = 
{
	"get_Current", NULL, &t3041_TI, &t380_0_0_0, RuntimeInvoker_t380, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28142_GM};
static MethodInfo* t3041_MIs[] =
{
	&m28142_MI,
	NULL
};
static TypeInfo* t3041_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3041_0_0_0;
extern Il2CppType t3041_1_0_0;
struct t3041;
extern Il2CppGenericClass t3041_GC;
TypeInfo t3041_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3041_MIs, t3041_PIs, NULL, NULL, NULL, NULL, NULL, &t3041_TI, t3041_ITIs, NULL, &EmptyCustomAttributesCache, &t3041_TI, &t3041_0_0_0, &t3041_1_0_0, NULL, &t3041_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2605.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2605_TI;
#include "t2605MD.h"

extern TypeInfo t380_TI;
extern MethodInfo m14035_MI;
extern MethodInfo m21204_MI;
struct t20;
 t380  m21204 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14031_MI;
 void m14031 (t2605 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14032_MI;
 t29 * m14032 (t2605 * __this, MethodInfo* method){
	{
		t380  L_0 = m14035(__this, &m14035_MI);
		t380  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t380_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14033_MI;
 void m14033 (t2605 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14034_MI;
 bool m14034 (t2605 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t380  m14035 (t2605 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t380  L_8 = m21204(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21204_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t2605_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2605_TI, offsetof(t2605, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2605_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2605_TI, offsetof(t2605, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2605_FIs[] =
{
	&t2605_f0_FieldInfo,
	&t2605_f1_FieldInfo,
	NULL
};
static PropertyInfo t2605____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2605_TI, "System.Collections.IEnumerator.Current", &m14032_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2605____Current_PropertyInfo = 
{
	&t2605_TI, "Current", &m14035_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2605_PIs[] =
{
	&t2605____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2605____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2605_m14031_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14031_GM;
MethodInfo m14031_MI = 
{
	".ctor", (methodPointerType)&m14031, &t2605_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2605_m14031_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14031_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14032_GM;
MethodInfo m14032_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14032, &t2605_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14032_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14033_GM;
MethodInfo m14033_MI = 
{
	"Dispose", (methodPointerType)&m14033, &t2605_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14033_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14034_GM;
MethodInfo m14034_MI = 
{
	"MoveNext", (methodPointerType)&m14034, &t2605_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14034_GM};
extern Il2CppType t380_0_0_0;
extern void* RuntimeInvoker_t380 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14035_GM;
MethodInfo m14035_MI = 
{
	"get_Current", (methodPointerType)&m14035, &t2605_TI, &t380_0_0_0, RuntimeInvoker_t380, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14035_GM};
static MethodInfo* t2605_MIs[] =
{
	&m14031_MI,
	&m14032_MI,
	&m14033_MI,
	&m14034_MI,
	&m14035_MI,
	NULL
};
static MethodInfo* t2605_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14032_MI,
	&m14034_MI,
	&m14033_MI,
	&m14035_MI,
};
static TypeInfo* t2605_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3041_TI,
};
static Il2CppInterfaceOffsetPair t2605_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3041_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2605_0_0_0;
extern Il2CppType t2605_1_0_0;
extern Il2CppGenericClass t2605_GC;
TypeInfo t2605_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2605_MIs, t2605_PIs, t2605_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2605_TI, t2605_ITIs, t2605_VT, &EmptyCustomAttributesCache, &t2605_TI, &t2605_0_0_0, &t2605_1_0_0, t2605_IOs, &t2605_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2605)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t388_TI;

#include "t381.h"


// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
extern MethodInfo m1753_MI;
extern MethodInfo m28143_MI;
static PropertyInfo t388____Item_PropertyInfo = 
{
	&t388_TI, "Item", &m1753_MI, &m28143_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t388_PIs[] =
{
	&t388____Item_PropertyInfo,
	NULL
};
extern Il2CppType t381_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t388_m28144_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28144_GM;
MethodInfo m28144_MI = 
{
	"IndexOf", NULL, &t388_TI, &t44_0_0_0, RuntimeInvoker_t44_t381, t388_m28144_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28144_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t388_m28145_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28145_GM;
MethodInfo m28145_MI = 
{
	"Insert", NULL, &t388_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t381, t388_m28145_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28145_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t388_m28146_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28146_GM;
MethodInfo m28146_MI = 
{
	"RemoveAt", NULL, &t388_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t388_m28146_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28146_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t388_m1753_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t381_0_0_0;
extern void* RuntimeInvoker_t381_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1753_GM;
MethodInfo m1753_MI = 
{
	"get_Item", NULL, &t388_TI, &t381_0_0_0, RuntimeInvoker_t381_t44, t388_m1753_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1753_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t381_0_0_0;
static ParameterInfo t388_m28143_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28143_GM;
MethodInfo m28143_MI = 
{
	"set_Item", NULL, &t388_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t381, t388_m28143_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28143_GM};
static MethodInfo* t388_MIs[] =
{
	&m28144_MI,
	&m28145_MI,
	&m28146_MI,
	&m1753_MI,
	&m28143_MI,
	NULL
};
extern TypeInfo t3030_TI;
extern TypeInfo t3031_TI;
static TypeInfo* t388_ITIs[] = 
{
	&t603_TI,
	&t3030_TI,
	&t3031_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t388_0_0_0;
extern Il2CppType t388_1_0_0;
struct t388;
extern Il2CppGenericClass t388_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t388_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t388_MIs, t388_PIs, NULL, NULL, NULL, NULL, NULL, &t388_TI, t388_ITIs, NULL, &t1908__CustomAttributeCache, &t388_TI, &t388_0_0_0, &t388_1_0_0, NULL, &t388_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>
extern MethodInfo m28147_MI;
static PropertyInfo t3030____Count_PropertyInfo = 
{
	&t3030_TI, "Count", &m28147_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28148_MI;
static PropertyInfo t3030____IsReadOnly_PropertyInfo = 
{
	&t3030_TI, "IsReadOnly", &m28148_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3030_PIs[] =
{
	&t3030____Count_PropertyInfo,
	&t3030____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28147_GM;
MethodInfo m28147_MI = 
{
	"get_Count", NULL, &t3030_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28147_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28148_GM;
MethodInfo m28148_MI = 
{
	"get_IsReadOnly", NULL, &t3030_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28148_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3030_m28149_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28149_GM;
MethodInfo m28149_MI = 
{
	"Add", NULL, &t3030_TI, &t21_0_0_0, RuntimeInvoker_t21_t381, t3030_m28149_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28149_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28150_GM;
MethodInfo m28150_MI = 
{
	"Clear", NULL, &t3030_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28150_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3030_m28151_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28151_GM;
MethodInfo m28151_MI = 
{
	"Contains", NULL, &t3030_TI, &t40_0_0_0, RuntimeInvoker_t40_t381, t3030_m28151_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28151_GM};
extern Il2CppType t530_0_0_0;
extern Il2CppType t530_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t3030_m28152_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t530_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28152_GM;
MethodInfo m28152_MI = 
{
	"CopyTo", NULL, &t3030_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t3030_m28152_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28152_GM};
extern Il2CppType t381_0_0_0;
static ParameterInfo t3030_m28153_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t381_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28153_GM;
MethodInfo m28153_MI = 
{
	"Remove", NULL, &t3030_TI, &t40_0_0_0, RuntimeInvoker_t40_t381, t3030_m28153_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28153_GM};
static MethodInfo* t3030_MIs[] =
{
	&m28147_MI,
	&m28148_MI,
	&m28149_MI,
	&m28150_MI,
	&m28151_MI,
	&m28152_MI,
	&m28153_MI,
	NULL
};
static TypeInfo* t3030_ITIs[] = 
{
	&t603_TI,
	&t3031_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3030_0_0_0;
extern Il2CppType t3030_1_0_0;
struct t3030;
extern Il2CppGenericClass t3030_GC;
TypeInfo t3030_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t3030_MIs, t3030_PIs, NULL, NULL, NULL, NULL, NULL, &t3030_TI, t3030_ITIs, NULL, &EmptyCustomAttributesCache, &t3030_TI, &t3030_0_0_0, &t3030_1_0_0, NULL, &t3030_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>
extern Il2CppType t3029_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28154_GM;
MethodInfo m28154_MI = 
{
	"GetEnumerator", NULL, &t3031_TI, &t3029_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28154_GM};
static MethodInfo* t3031_MIs[] =
{
	&m28154_MI,
	NULL
};
static TypeInfo* t3031_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3031_0_0_0;
extern Il2CppType t3031_1_0_0;
struct t3031;
extern Il2CppGenericClass t3031_GC;
TypeInfo t3031_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t3031_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t3031_TI, t3031_ITIs, NULL, &EmptyCustomAttributesCache, &t3031_TI, &t3031_0_0_0, &t3031_1_0_0, NULL, &t3031_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3029_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
extern MethodInfo m28155_MI;
static PropertyInfo t3029____Current_PropertyInfo = 
{
	&t3029_TI, "Current", &m28155_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3029_PIs[] =
{
	&t3029____Current_PropertyInfo,
	NULL
};
extern Il2CppType t381_0_0_0;
extern void* RuntimeInvoker_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28155_GM;
MethodInfo m28155_MI = 
{
	"get_Current", NULL, &t3029_TI, &t381_0_0_0, RuntimeInvoker_t381, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28155_GM};
static MethodInfo* t3029_MIs[] =
{
	&m28155_MI,
	NULL
};
static TypeInfo* t3029_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3029_0_0_0;
extern Il2CppType t3029_1_0_0;
struct t3029;
extern Il2CppGenericClass t3029_GC;
TypeInfo t3029_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t3029_MIs, t3029_PIs, NULL, NULL, NULL, NULL, NULL, &t3029_TI, t3029_ITIs, NULL, &EmptyCustomAttributesCache, &t3029_TI, &t3029_0_0_0, &t3029_1_0_0, NULL, &t3029_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2606.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2606_TI;
#include "t2606MD.h"

extern TypeInfo t381_TI;
extern MethodInfo m14040_MI;
extern MethodInfo m21215_MI;
struct t20;
 t381  m21215 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14036_MI;
 void m14036 (t2606 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14037_MI;
 t29 * m14037 (t2606 * __this, MethodInfo* method){
	{
		t381  L_0 = m14040(__this, &m14040_MI);
		t381  L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t381_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14038_MI;
 void m14038 (t2606 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14039_MI;
 bool m14039 (t2606 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 t381  m14040 (t2606 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		t381  L_8 = m21215(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21215_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t2606_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2606_TI, offsetof(t2606, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2606_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2606_TI, offsetof(t2606, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2606_FIs[] =
{
	&t2606_f0_FieldInfo,
	&t2606_f1_FieldInfo,
	NULL
};
static PropertyInfo t2606____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2606_TI, "System.Collections.IEnumerator.Current", &m14037_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2606____Current_PropertyInfo = 
{
	&t2606_TI, "Current", &m14040_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2606_PIs[] =
{
	&t2606____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2606____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2606_m14036_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14036_GM;
MethodInfo m14036_MI = 
{
	".ctor", (methodPointerType)&m14036, &t2606_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2606_m14036_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14036_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14037_GM;
MethodInfo m14037_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14037, &t2606_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14037_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14038_GM;
MethodInfo m14038_MI = 
{
	"Dispose", (methodPointerType)&m14038, &t2606_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14038_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14039_GM;
MethodInfo m14039_MI = 
{
	"MoveNext", (methodPointerType)&m14039, &t2606_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14039_GM};
extern Il2CppType t381_0_0_0;
extern void* RuntimeInvoker_t381 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14040_GM;
MethodInfo m14040_MI = 
{
	"get_Current", (methodPointerType)&m14040, &t2606_TI, &t381_0_0_0, RuntimeInvoker_t381, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14040_GM};
static MethodInfo* t2606_MIs[] =
{
	&m14036_MI,
	&m14037_MI,
	&m14038_MI,
	&m14039_MI,
	&m14040_MI,
	NULL
};
static MethodInfo* t2606_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14037_MI,
	&m14039_MI,
	&m14038_MI,
	&m14040_MI,
};
static TypeInfo* t2606_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t3029_TI,
};
static Il2CppInterfaceOffsetPair t2606_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t3029_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2606_0_0_0;
extern Il2CppType t2606_1_0_0;
extern Il2CppGenericClass t2606_GC;
TypeInfo t2606_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2606_MIs, t2606_PIs, t2606_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2606_TI, t2606_ITIs, t2606_VT, &EmptyCustomAttributesCache, &t2606_TI, &t2606_0_0_0, &t2606_1_0_0, t2606_IOs, &t2606_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2606)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#include "t190.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t190_TI;
#include "t190MD.h"

#include "t2607.h"
#include "t371.h"
extern TypeInfo t537_TI;
extern TypeInfo t7_TI;
extern TypeInfo t371_TI;
#include "t566MD.h"
#include "t371MD.h"
extern Il2CppType t7_0_0_0;
extern MethodInfo m2812_MI;
extern MethodInfo m1696_MI;
extern MethodInfo m2817_MI;
extern MethodInfo m2818_MI;
extern MethodInfo m2820_MI;
extern MethodInfo m1697_MI;
extern MethodInfo m1698_MI;
extern MethodInfo m2819_MI;


// Metadata Definition UnityEngine.Events.UnityEvent`1<System.String>
extern Il2CppType t316_0_0_33;
FieldInfo t190_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t190_TI, offsetof(t190, f4), &EmptyCustomAttributesCache};
static FieldInfo* t190_FIs[] =
{
	&t190_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1693_GM;
MethodInfo m1693_MI = 
{
	".ctor", (methodPointerType)&m11776_gshared, &t190_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1693_GM};
extern Il2CppType t2607_0_0_0;
extern Il2CppType t2607_0_0_0;
static ParameterInfo t190_m14041_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t2607_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14041_GM;
MethodInfo m14041_MI = 
{
	"AddListener", (methodPointerType)&m11778_gshared, &t190_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t190_m14041_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14041_GM};
extern Il2CppType t2607_0_0_0;
static ParameterInfo t190_m14042_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t2607_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14042_GM;
MethodInfo m14042_MI = 
{
	"RemoveListener", (methodPointerType)&m11780_gshared, &t190_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t190_m14042_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14042_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t190_m1694_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1694_GM;
MethodInfo m1694_MI = 
{
	"FindMethod_Impl", (methodPointerType)&m11781_gshared, &t190_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29, t190_m1694_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1694_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t190_m1695_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1695_GM;
MethodInfo m1695_MI = 
{
	"GetDelegate", (methodPointerType)&m11782_gshared, &t190_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29, t190_m1695_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1695_GM};
extern Il2CppType t2607_0_0_0;
static ParameterInfo t190_m1696_ParameterInfos[] = 
{
	{"action", 0, 134217728, &EmptyCustomAttributesCache, &t2607_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1696_GM;
MethodInfo m1696_MI = 
{
	"GetDelegate", (methodPointerType)&m11783_gshared, &t190_TI, &t556_0_0_0, RuntimeInvoker_t29_t29, t190_m1696_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1696_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t190_m1769_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1769_GM;
MethodInfo m1769_MI = 
{
	"Invoke", (methodPointerType)&m11784_gshared, &t190_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t190_m1769_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1769_GM};
static MethodInfo* t190_MIs[] =
{
	&m1693_MI,
	&m14041_MI,
	&m14042_MI,
	&m1694_MI,
	&m1695_MI,
	&m1696_MI,
	&m1769_MI,
	NULL
};
extern MethodInfo m1323_MI;
extern MethodInfo m1324_MI;
extern MethodInfo m1325_MI;
extern MethodInfo m1694_MI;
extern MethodInfo m1695_MI;
static MethodInfo* t190_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m1694_MI,
	&m1695_MI,
};
extern TypeInfo t301_TI;
static Il2CppInterfaceOffsetPair t190_IOs[] = 
{
	{ &t301_TI, 4},
};
extern TypeInfo t371_TI;
extern TypeInfo t7_TI;
static Il2CppRGCTXData t190_RGCTXData[6] = 
{
	&m1696_MI/* Method Usage */,
	&t7_0_0_0/* Type Usage */,
	&t371_TI/* Class Usage */,
	&m1697_MI/* Method Usage */,
	&m1698_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t190_0_0_0;
extern Il2CppType t190_1_0_0;
extern TypeInfo t566_TI;
struct t190;
extern Il2CppGenericClass t190_GC;
TypeInfo t190_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`1", "UnityEngine.Events", t190_MIs, NULL, t190_FIs, NULL, &t566_TI, NULL, NULL, &t190_TI, NULL, t190_VT, &EmptyCustomAttributesCache, &t190_TI, &t190_0_0_0, &t190_1_0_0, t190_IOs, &t190_GC, NULL, NULL, NULL, t190_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t190), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 0, 1, 0, 0, 8, 0, 1};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2607_TI;
#include "t2607MD.h"



// Metadata Definition UnityEngine.Events.UnityAction`1<System.String>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2607_m14043_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14043_GM;
MethodInfo m14043_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2607_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2607_m14043_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14043_GM};
extern Il2CppType t7_0_0_0;
static ParameterInfo t2607_m14044_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14044_GM;
MethodInfo m14044_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2607_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2607_m14044_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14044_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2607_m14045_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14045_GM;
MethodInfo m14045_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2607_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2607_m14045_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14045_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2607_m14046_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14046_GM;
MethodInfo m14046_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2607_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2607_m14046_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14046_GM};
static MethodInfo* t2607_MIs[] =
{
	&m14043_MI,
	&m14044_MI,
	&m14045_MI,
	&m14046_MI,
	NULL
};
extern MethodInfo m14044_MI;
extern MethodInfo m14045_MI;
extern MethodInfo m14046_MI;
static MethodInfo* t2607_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14044_MI,
	&m14045_MI,
	&m14046_MI,
};
static Il2CppInterfaceOffsetPair t2607_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2607_1_0_0;
struct t2607;
extern Il2CppGenericClass t2607_GC;
TypeInfo t2607_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2607_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2607_TI, NULL, t2607_VT, &EmptyCustomAttributesCache, &t2607_TI, &t2607_0_0_0, &t2607_1_0_0, t2607_IOs, &t2607_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2607), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo m21225_MI;
struct t556;
#define m21225(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<System.String>
extern Il2CppType t2607_0_0_1;
FieldInfo t371_f0_FieldInfo = 
{
	"Delegate", &t2607_0_0_1, &t371_TI, offsetof(t371, f0), &EmptyCustomAttributesCache};
static FieldInfo* t371_FIs[] =
{
	&t371_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t371_m1697_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1697_GM;
MethodInfo m1697_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t371_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t371_m1697_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1697_GM};
extern Il2CppType t2607_0_0_0;
static ParameterInfo t371_m1698_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2607_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1698_GM;
MethodInfo m1698_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t371_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t371_m1698_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1698_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t371_m14047_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14047_GM;
MethodInfo m14047_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t371_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t371_m14047_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14047_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t371_m14048_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14048_GM;
MethodInfo m14048_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t371_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t371_m14048_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14048_GM};
static MethodInfo* t371_MIs[] =
{
	&m1697_MI,
	&m1698_MI,
	&m14047_MI,
	&m14048_MI,
	NULL
};
extern MethodInfo m14047_MI;
extern MethodInfo m14048_MI;
static MethodInfo* t371_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14047_MI,
	&m14048_MI,
};
extern TypeInfo t2607_TI;
extern TypeInfo t7_TI;
static Il2CppRGCTXData t371_RGCTXData[5] = 
{
	&t2607_0_0_0/* Type Usage */,
	&t2607_TI/* Class Usage */,
	&m21225_MI/* Method Usage */,
	&t7_TI/* Class Usage */,
	&m14044_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t371_0_0_0;
extern Il2CppType t371_1_0_0;
struct t371;
extern Il2CppGenericClass t371_GC;
TypeInfo t371_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t371_MIs, NULL, t371_FIs, NULL, &t556_TI, NULL, NULL, &t371_TI, NULL, t371_VT, &EmptyCustomAttributesCache, &t371_TI, &t371_0_0_0, &t371_1_0_0, NULL, &t371_GC, NULL, NULL, NULL, t371_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t371), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4203_TI;

#include "t186.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/InputType>
extern MethodInfo m28156_MI;
static PropertyInfo t4203____Current_PropertyInfo = 
{
	&t4203_TI, "Current", &m28156_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4203_PIs[] =
{
	&t4203____Current_PropertyInfo,
	NULL
};
extern Il2CppType t186_0_0_0;
extern void* RuntimeInvoker_t186 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28156_GM;
MethodInfo m28156_MI = 
{
	"get_Current", NULL, &t4203_TI, &t186_0_0_0, RuntimeInvoker_t186, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28156_GM};
static MethodInfo* t4203_MIs[] =
{
	&m28156_MI,
	NULL
};
static TypeInfo* t4203_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4203_0_0_0;
extern Il2CppType t4203_1_0_0;
struct t4203;
extern Il2CppGenericClass t4203_GC;
TypeInfo t4203_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4203_MIs, t4203_PIs, NULL, NULL, NULL, NULL, NULL, &t4203_TI, t4203_ITIs, NULL, &EmptyCustomAttributesCache, &t4203_TI, &t4203_0_0_0, &t4203_1_0_0, NULL, &t4203_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2608.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2608_TI;
#include "t2608MD.h"

extern TypeInfo t186_TI;
extern MethodInfo m14053_MI;
extern MethodInfo m21227_MI;
struct t20;
 int32_t m21227 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14049_MI;
 void m14049 (t2608 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14050_MI;
 t29 * m14050 (t2608 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14053(__this, &m14053_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t186_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14051_MI;
 void m14051 (t2608 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14052_MI;
 bool m14052 (t2608 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14053 (t2608 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21227(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21227_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>
extern Il2CppType t20_0_0_1;
FieldInfo t2608_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2608_TI, offsetof(t2608, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2608_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2608_TI, offsetof(t2608, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2608_FIs[] =
{
	&t2608_f0_FieldInfo,
	&t2608_f1_FieldInfo,
	NULL
};
static PropertyInfo t2608____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2608_TI, "System.Collections.IEnumerator.Current", &m14050_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2608____Current_PropertyInfo = 
{
	&t2608_TI, "Current", &m14053_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2608_PIs[] =
{
	&t2608____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2608____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2608_m14049_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14049_GM;
MethodInfo m14049_MI = 
{
	".ctor", (methodPointerType)&m14049, &t2608_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2608_m14049_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14049_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14050_GM;
MethodInfo m14050_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14050, &t2608_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14050_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14051_GM;
MethodInfo m14051_MI = 
{
	"Dispose", (methodPointerType)&m14051, &t2608_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14051_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14052_GM;
MethodInfo m14052_MI = 
{
	"MoveNext", (methodPointerType)&m14052, &t2608_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14052_GM};
extern Il2CppType t186_0_0_0;
extern void* RuntimeInvoker_t186 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14053_GM;
MethodInfo m14053_MI = 
{
	"get_Current", (methodPointerType)&m14053, &t2608_TI, &t186_0_0_0, RuntimeInvoker_t186, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14053_GM};
static MethodInfo* t2608_MIs[] =
{
	&m14049_MI,
	&m14050_MI,
	&m14051_MI,
	&m14052_MI,
	&m14053_MI,
	NULL
};
static MethodInfo* t2608_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14050_MI,
	&m14052_MI,
	&m14051_MI,
	&m14053_MI,
};
static TypeInfo* t2608_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4203_TI,
};
static Il2CppInterfaceOffsetPair t2608_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4203_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2608_0_0_0;
extern Il2CppType t2608_1_0_0;
extern Il2CppGenericClass t2608_GC;
TypeInfo t2608_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2608_MIs, t2608_PIs, t2608_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2608_TI, t2608_ITIs, t2608_VT, &EmptyCustomAttributesCache, &t2608_TI, &t2608_0_0_0, &t2608_1_0_0, t2608_IOs, &t2608_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2608)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5379_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>
extern MethodInfo m28157_MI;
static PropertyInfo t5379____Count_PropertyInfo = 
{
	&t5379_TI, "Count", &m28157_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28158_MI;
static PropertyInfo t5379____IsReadOnly_PropertyInfo = 
{
	&t5379_TI, "IsReadOnly", &m28158_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5379_PIs[] =
{
	&t5379____Count_PropertyInfo,
	&t5379____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28157_GM;
MethodInfo m28157_MI = 
{
	"get_Count", NULL, &t5379_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28157_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28158_GM;
MethodInfo m28158_MI = 
{
	"get_IsReadOnly", NULL, &t5379_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28158_GM};
extern Il2CppType t186_0_0_0;
extern Il2CppType t186_0_0_0;
static ParameterInfo t5379_m28159_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t186_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28159_GM;
MethodInfo m28159_MI = 
{
	"Add", NULL, &t5379_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5379_m28159_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28159_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28160_GM;
MethodInfo m28160_MI = 
{
	"Clear", NULL, &t5379_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28160_GM};
extern Il2CppType t186_0_0_0;
static ParameterInfo t5379_m28161_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t186_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28161_GM;
MethodInfo m28161_MI = 
{
	"Contains", NULL, &t5379_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5379_m28161_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28161_GM};
extern Il2CppType t3847_0_0_0;
extern Il2CppType t3847_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5379_m28162_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3847_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28162_GM;
MethodInfo m28162_MI = 
{
	"CopyTo", NULL, &t5379_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5379_m28162_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28162_GM};
extern Il2CppType t186_0_0_0;
static ParameterInfo t5379_m28163_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t186_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28163_GM;
MethodInfo m28163_MI = 
{
	"Remove", NULL, &t5379_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5379_m28163_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28163_GM};
static MethodInfo* t5379_MIs[] =
{
	&m28157_MI,
	&m28158_MI,
	&m28159_MI,
	&m28160_MI,
	&m28161_MI,
	&m28162_MI,
	&m28163_MI,
	NULL
};
extern TypeInfo t5381_TI;
static TypeInfo* t5379_ITIs[] = 
{
	&t603_TI,
	&t5381_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5379_0_0_0;
extern Il2CppType t5379_1_0_0;
struct t5379;
extern Il2CppGenericClass t5379_GC;
TypeInfo t5379_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5379_MIs, t5379_PIs, NULL, NULL, NULL, NULL, NULL, &t5379_TI, t5379_ITIs, NULL, &EmptyCustomAttributesCache, &t5379_TI, &t5379_0_0_0, &t5379_1_0_0, NULL, &t5379_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/InputType>
extern Il2CppType t4203_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28164_GM;
MethodInfo m28164_MI = 
{
	"GetEnumerator", NULL, &t5381_TI, &t4203_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28164_GM};
static MethodInfo* t5381_MIs[] =
{
	&m28164_MI,
	NULL
};
static TypeInfo* t5381_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5381_0_0_0;
extern Il2CppType t5381_1_0_0;
struct t5381;
extern Il2CppGenericClass t5381_GC;
TypeInfo t5381_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5381_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5381_TI, t5381_ITIs, NULL, &EmptyCustomAttributesCache, &t5381_TI, &t5381_0_0_0, &t5381_1_0_0, NULL, &t5381_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5380_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.InputField/InputType>
extern MethodInfo m28165_MI;
extern MethodInfo m28166_MI;
static PropertyInfo t5380____Item_PropertyInfo = 
{
	&t5380_TI, "Item", &m28165_MI, &m28166_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5380_PIs[] =
{
	&t5380____Item_PropertyInfo,
	NULL
};
extern Il2CppType t186_0_0_0;
static ParameterInfo t5380_m28167_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t186_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28167_GM;
MethodInfo m28167_MI = 
{
	"IndexOf", NULL, &t5380_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5380_m28167_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28167_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t186_0_0_0;
static ParameterInfo t5380_m28168_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t186_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28168_GM;
MethodInfo m28168_MI = 
{
	"Insert", NULL, &t5380_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5380_m28168_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28168_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5380_m28169_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28169_GM;
MethodInfo m28169_MI = 
{
	"RemoveAt", NULL, &t5380_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5380_m28169_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28169_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5380_m28165_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t186_0_0_0;
extern void* RuntimeInvoker_t186_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28165_GM;
MethodInfo m28165_MI = 
{
	"get_Item", NULL, &t5380_TI, &t186_0_0_0, RuntimeInvoker_t186_t44, t5380_m28165_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28165_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t186_0_0_0;
static ParameterInfo t5380_m28166_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t186_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28166_GM;
MethodInfo m28166_MI = 
{
	"set_Item", NULL, &t5380_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5380_m28166_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28166_GM};
static MethodInfo* t5380_MIs[] =
{
	&m28167_MI,
	&m28168_MI,
	&m28169_MI,
	&m28165_MI,
	&m28166_MI,
	NULL
};
static TypeInfo* t5380_ITIs[] = 
{
	&t603_TI,
	&t5379_TI,
	&t5381_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5380_0_0_0;
extern Il2CppType t5380_1_0_0;
struct t5380;
extern Il2CppGenericClass t5380_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5380_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5380_MIs, t5380_PIs, NULL, NULL, NULL, NULL, NULL, &t5380_TI, t5380_ITIs, NULL, &t1908__CustomAttributeCache, &t5380_TI, &t5380_0_0_0, &t5380_1_0_0, NULL, &t5380_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4204_TI;

#include "t187.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>
extern MethodInfo m28170_MI;
static PropertyInfo t4204____Current_PropertyInfo = 
{
	&t4204_TI, "Current", &m28170_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4204_PIs[] =
{
	&t4204____Current_PropertyInfo,
	NULL
};
extern Il2CppType t187_0_0_0;
extern void* RuntimeInvoker_t187 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28170_GM;
MethodInfo m28170_MI = 
{
	"get_Current", NULL, &t4204_TI, &t187_0_0_0, RuntimeInvoker_t187, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28170_GM};
static MethodInfo* t4204_MIs[] =
{
	&m28170_MI,
	NULL
};
static TypeInfo* t4204_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4204_0_0_0;
extern Il2CppType t4204_1_0_0;
struct t4204;
extern Il2CppGenericClass t4204_GC;
TypeInfo t4204_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4204_MIs, t4204_PIs, NULL, NULL, NULL, NULL, NULL, &t4204_TI, t4204_ITIs, NULL, &EmptyCustomAttributesCache, &t4204_TI, &t4204_0_0_0, &t4204_1_0_0, NULL, &t4204_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2609.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2609_TI;
#include "t2609MD.h"

extern TypeInfo t187_TI;
extern MethodInfo m14058_MI;
extern MethodInfo m21238_MI;
struct t20;
 int32_t m21238 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14054_MI;
 void m14054 (t2609 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14055_MI;
 t29 * m14055 (t2609 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14058(__this, &m14058_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t187_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14056_MI;
 void m14056 (t2609 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14057_MI;
 bool m14057 (t2609 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14058 (t2609 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21238(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21238_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>
extern Il2CppType t20_0_0_1;
FieldInfo t2609_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2609_TI, offsetof(t2609, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2609_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2609_TI, offsetof(t2609, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2609_FIs[] =
{
	&t2609_f0_FieldInfo,
	&t2609_f1_FieldInfo,
	NULL
};
static PropertyInfo t2609____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2609_TI, "System.Collections.IEnumerator.Current", &m14055_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2609____Current_PropertyInfo = 
{
	&t2609_TI, "Current", &m14058_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2609_PIs[] =
{
	&t2609____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2609____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2609_m14054_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14054_GM;
MethodInfo m14054_MI = 
{
	".ctor", (methodPointerType)&m14054, &t2609_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2609_m14054_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14054_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14055_GM;
MethodInfo m14055_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14055, &t2609_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14055_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14056_GM;
MethodInfo m14056_MI = 
{
	"Dispose", (methodPointerType)&m14056, &t2609_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14056_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14057_GM;
MethodInfo m14057_MI = 
{
	"MoveNext", (methodPointerType)&m14057, &t2609_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14057_GM};
extern Il2CppType t187_0_0_0;
extern void* RuntimeInvoker_t187 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14058_GM;
MethodInfo m14058_MI = 
{
	"get_Current", (methodPointerType)&m14058, &t2609_TI, &t187_0_0_0, RuntimeInvoker_t187, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14058_GM};
static MethodInfo* t2609_MIs[] =
{
	&m14054_MI,
	&m14055_MI,
	&m14056_MI,
	&m14057_MI,
	&m14058_MI,
	NULL
};
static MethodInfo* t2609_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14055_MI,
	&m14057_MI,
	&m14056_MI,
	&m14058_MI,
};
static TypeInfo* t2609_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4204_TI,
};
static Il2CppInterfaceOffsetPair t2609_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4204_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2609_0_0_0;
extern Il2CppType t2609_1_0_0;
extern Il2CppGenericClass t2609_GC;
TypeInfo t2609_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2609_MIs, t2609_PIs, t2609_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2609_TI, t2609_ITIs, t2609_VT, &EmptyCustomAttributesCache, &t2609_TI, &t2609_0_0_0, &t2609_1_0_0, t2609_IOs, &t2609_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2609)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5382_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>
extern MethodInfo m28171_MI;
static PropertyInfo t5382____Count_PropertyInfo = 
{
	&t5382_TI, "Count", &m28171_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28172_MI;
static PropertyInfo t5382____IsReadOnly_PropertyInfo = 
{
	&t5382_TI, "IsReadOnly", &m28172_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5382_PIs[] =
{
	&t5382____Count_PropertyInfo,
	&t5382____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28171_GM;
MethodInfo m28171_MI = 
{
	"get_Count", NULL, &t5382_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28171_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28172_GM;
MethodInfo m28172_MI = 
{
	"get_IsReadOnly", NULL, &t5382_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28172_GM};
extern Il2CppType t187_0_0_0;
extern Il2CppType t187_0_0_0;
static ParameterInfo t5382_m28173_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t187_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28173_GM;
MethodInfo m28173_MI = 
{
	"Add", NULL, &t5382_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5382_m28173_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28173_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28174_GM;
MethodInfo m28174_MI = 
{
	"Clear", NULL, &t5382_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28174_GM};
extern Il2CppType t187_0_0_0;
static ParameterInfo t5382_m28175_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t187_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28175_GM;
MethodInfo m28175_MI = 
{
	"Contains", NULL, &t5382_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5382_m28175_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28175_GM};
extern Il2CppType t3848_0_0_0;
extern Il2CppType t3848_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5382_m28176_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3848_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28176_GM;
MethodInfo m28176_MI = 
{
	"CopyTo", NULL, &t5382_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5382_m28176_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28176_GM};
extern Il2CppType t187_0_0_0;
static ParameterInfo t5382_m28177_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t187_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28177_GM;
MethodInfo m28177_MI = 
{
	"Remove", NULL, &t5382_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5382_m28177_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28177_GM};
static MethodInfo* t5382_MIs[] =
{
	&m28171_MI,
	&m28172_MI,
	&m28173_MI,
	&m28174_MI,
	&m28175_MI,
	&m28176_MI,
	&m28177_MI,
	NULL
};
extern TypeInfo t5384_TI;
static TypeInfo* t5382_ITIs[] = 
{
	&t603_TI,
	&t5384_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5382_0_0_0;
extern Il2CppType t5382_1_0_0;
struct t5382;
extern Il2CppGenericClass t5382_GC;
TypeInfo t5382_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5382_MIs, t5382_PIs, NULL, NULL, NULL, NULL, NULL, &t5382_TI, t5382_ITIs, NULL, &EmptyCustomAttributesCache, &t5382_TI, &t5382_0_0_0, &t5382_1_0_0, NULL, &t5382_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/CharacterValidation>
extern Il2CppType t4204_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28178_GM;
MethodInfo m28178_MI = 
{
	"GetEnumerator", NULL, &t5384_TI, &t4204_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28178_GM};
static MethodInfo* t5384_MIs[] =
{
	&m28178_MI,
	NULL
};
static TypeInfo* t5384_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5384_0_0_0;
extern Il2CppType t5384_1_0_0;
struct t5384;
extern Il2CppGenericClass t5384_GC;
TypeInfo t5384_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5384_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5384_TI, t5384_ITIs, NULL, &EmptyCustomAttributesCache, &t5384_TI, &t5384_0_0_0, &t5384_1_0_0, NULL, &t5384_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5383_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.InputField/CharacterValidation>
extern MethodInfo m28179_MI;
extern MethodInfo m28180_MI;
static PropertyInfo t5383____Item_PropertyInfo = 
{
	&t5383_TI, "Item", &m28179_MI, &m28180_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5383_PIs[] =
{
	&t5383____Item_PropertyInfo,
	NULL
};
extern Il2CppType t187_0_0_0;
static ParameterInfo t5383_m28181_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t187_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28181_GM;
MethodInfo m28181_MI = 
{
	"IndexOf", NULL, &t5383_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5383_m28181_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28181_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t187_0_0_0;
static ParameterInfo t5383_m28182_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t187_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28182_GM;
MethodInfo m28182_MI = 
{
	"Insert", NULL, &t5383_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5383_m28182_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28182_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5383_m28183_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28183_GM;
MethodInfo m28183_MI = 
{
	"RemoveAt", NULL, &t5383_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5383_m28183_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28183_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5383_m28179_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t187_0_0_0;
extern void* RuntimeInvoker_t187_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28179_GM;
MethodInfo m28179_MI = 
{
	"get_Item", NULL, &t5383_TI, &t187_0_0_0, RuntimeInvoker_t187_t44, t5383_m28179_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28179_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t187_0_0_0;
static ParameterInfo t5383_m28180_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t187_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28180_GM;
MethodInfo m28180_MI = 
{
	"set_Item", NULL, &t5383_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5383_m28180_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28180_GM};
static MethodInfo* t5383_MIs[] =
{
	&m28181_MI,
	&m28182_MI,
	&m28183_MI,
	&m28179_MI,
	&m28180_MI,
	NULL
};
static TypeInfo* t5383_ITIs[] = 
{
	&t603_TI,
	&t5382_TI,
	&t5384_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5383_0_0_0;
extern Il2CppType t5383_1_0_0;
struct t5383;
extern Il2CppGenericClass t5383_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5383_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5383_MIs, t5383_PIs, NULL, NULL, NULL, NULL, NULL, &t5383_TI, t5383_ITIs, NULL, &t1908__CustomAttributeCache, &t5383_TI, &t5383_0_0_0, &t5383_1_0_0, NULL, &t5383_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4205_TI;

#include "t188.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/LineType>
extern MethodInfo m28184_MI;
static PropertyInfo t4205____Current_PropertyInfo = 
{
	&t4205_TI, "Current", &m28184_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4205_PIs[] =
{
	&t4205____Current_PropertyInfo,
	NULL
};
extern Il2CppType t188_0_0_0;
extern void* RuntimeInvoker_t188 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28184_GM;
MethodInfo m28184_MI = 
{
	"get_Current", NULL, &t4205_TI, &t188_0_0_0, RuntimeInvoker_t188, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28184_GM};
static MethodInfo* t4205_MIs[] =
{
	&m28184_MI,
	NULL
};
static TypeInfo* t4205_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4205_0_0_0;
extern Il2CppType t4205_1_0_0;
struct t4205;
extern Il2CppGenericClass t4205_GC;
TypeInfo t4205_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4205_MIs, t4205_PIs, NULL, NULL, NULL, NULL, NULL, &t4205_TI, t4205_ITIs, NULL, &EmptyCustomAttributesCache, &t4205_TI, &t4205_0_0_0, &t4205_1_0_0, NULL, &t4205_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2610.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2610_TI;
#include "t2610MD.h"

extern TypeInfo t188_TI;
extern MethodInfo m14063_MI;
extern MethodInfo m21249_MI;
struct t20;
 int32_t m21249 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14059_MI;
 void m14059 (t2610 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14060_MI;
 t29 * m14060 (t2610 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14063(__this, &m14063_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t188_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14061_MI;
 void m14061 (t2610 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14062_MI;
 bool m14062 (t2610 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14063 (t2610 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21249(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21249_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>
extern Il2CppType t20_0_0_1;
FieldInfo t2610_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2610_TI, offsetof(t2610, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2610_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2610_TI, offsetof(t2610, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2610_FIs[] =
{
	&t2610_f0_FieldInfo,
	&t2610_f1_FieldInfo,
	NULL
};
static PropertyInfo t2610____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2610_TI, "System.Collections.IEnumerator.Current", &m14060_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2610____Current_PropertyInfo = 
{
	&t2610_TI, "Current", &m14063_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2610_PIs[] =
{
	&t2610____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2610____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2610_m14059_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14059_GM;
MethodInfo m14059_MI = 
{
	".ctor", (methodPointerType)&m14059, &t2610_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2610_m14059_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14059_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14060_GM;
MethodInfo m14060_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14060, &t2610_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14060_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14061_GM;
MethodInfo m14061_MI = 
{
	"Dispose", (methodPointerType)&m14061, &t2610_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14061_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14062_GM;
MethodInfo m14062_MI = 
{
	"MoveNext", (methodPointerType)&m14062, &t2610_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14062_GM};
extern Il2CppType t188_0_0_0;
extern void* RuntimeInvoker_t188 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14063_GM;
MethodInfo m14063_MI = 
{
	"get_Current", (methodPointerType)&m14063, &t2610_TI, &t188_0_0_0, RuntimeInvoker_t188, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14063_GM};
static MethodInfo* t2610_MIs[] =
{
	&m14059_MI,
	&m14060_MI,
	&m14061_MI,
	&m14062_MI,
	&m14063_MI,
	NULL
};
static MethodInfo* t2610_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14060_MI,
	&m14062_MI,
	&m14061_MI,
	&m14063_MI,
};
static TypeInfo* t2610_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4205_TI,
};
static Il2CppInterfaceOffsetPair t2610_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4205_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2610_0_0_0;
extern Il2CppType t2610_1_0_0;
extern Il2CppGenericClass t2610_GC;
TypeInfo t2610_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2610_MIs, t2610_PIs, t2610_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2610_TI, t2610_ITIs, t2610_VT, &EmptyCustomAttributesCache, &t2610_TI, &t2610_0_0_0, &t2610_1_0_0, t2610_IOs, &t2610_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2610)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5385_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>
extern MethodInfo m28185_MI;
static PropertyInfo t5385____Count_PropertyInfo = 
{
	&t5385_TI, "Count", &m28185_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28186_MI;
static PropertyInfo t5385____IsReadOnly_PropertyInfo = 
{
	&t5385_TI, "IsReadOnly", &m28186_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5385_PIs[] =
{
	&t5385____Count_PropertyInfo,
	&t5385____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28185_GM;
MethodInfo m28185_MI = 
{
	"get_Count", NULL, &t5385_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28185_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28186_GM;
MethodInfo m28186_MI = 
{
	"get_IsReadOnly", NULL, &t5385_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28186_GM};
extern Il2CppType t188_0_0_0;
extern Il2CppType t188_0_0_0;
static ParameterInfo t5385_m28187_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t188_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28187_GM;
MethodInfo m28187_MI = 
{
	"Add", NULL, &t5385_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5385_m28187_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28187_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28188_GM;
MethodInfo m28188_MI = 
{
	"Clear", NULL, &t5385_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28188_GM};
extern Il2CppType t188_0_0_0;
static ParameterInfo t5385_m28189_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t188_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28189_GM;
MethodInfo m28189_MI = 
{
	"Contains", NULL, &t5385_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5385_m28189_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28189_GM};
extern Il2CppType t3849_0_0_0;
extern Il2CppType t3849_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5385_m28190_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3849_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28190_GM;
MethodInfo m28190_MI = 
{
	"CopyTo", NULL, &t5385_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5385_m28190_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28190_GM};
extern Il2CppType t188_0_0_0;
static ParameterInfo t5385_m28191_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t188_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28191_GM;
MethodInfo m28191_MI = 
{
	"Remove", NULL, &t5385_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5385_m28191_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28191_GM};
static MethodInfo* t5385_MIs[] =
{
	&m28185_MI,
	&m28186_MI,
	&m28187_MI,
	&m28188_MI,
	&m28189_MI,
	&m28190_MI,
	&m28191_MI,
	NULL
};
extern TypeInfo t5387_TI;
static TypeInfo* t5385_ITIs[] = 
{
	&t603_TI,
	&t5387_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5385_0_0_0;
extern Il2CppType t5385_1_0_0;
struct t5385;
extern Il2CppGenericClass t5385_GC;
TypeInfo t5385_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5385_MIs, t5385_PIs, NULL, NULL, NULL, NULL, NULL, &t5385_TI, t5385_ITIs, NULL, &EmptyCustomAttributesCache, &t5385_TI, &t5385_0_0_0, &t5385_1_0_0, NULL, &t5385_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/LineType>
extern Il2CppType t4205_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28192_GM;
MethodInfo m28192_MI = 
{
	"GetEnumerator", NULL, &t5387_TI, &t4205_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28192_GM};
static MethodInfo* t5387_MIs[] =
{
	&m28192_MI,
	NULL
};
static TypeInfo* t5387_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5387_0_0_0;
extern Il2CppType t5387_1_0_0;
struct t5387;
extern Il2CppGenericClass t5387_GC;
TypeInfo t5387_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5387_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5387_TI, t5387_ITIs, NULL, &EmptyCustomAttributesCache, &t5387_TI, &t5387_0_0_0, &t5387_1_0_0, NULL, &t5387_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5386_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.InputField/LineType>
extern MethodInfo m28193_MI;
extern MethodInfo m28194_MI;
static PropertyInfo t5386____Item_PropertyInfo = 
{
	&t5386_TI, "Item", &m28193_MI, &m28194_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5386_PIs[] =
{
	&t5386____Item_PropertyInfo,
	NULL
};
extern Il2CppType t188_0_0_0;
static ParameterInfo t5386_m28195_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t188_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28195_GM;
MethodInfo m28195_MI = 
{
	"IndexOf", NULL, &t5386_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5386_m28195_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28195_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t188_0_0_0;
static ParameterInfo t5386_m28196_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t188_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28196_GM;
MethodInfo m28196_MI = 
{
	"Insert", NULL, &t5386_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5386_m28196_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28196_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5386_m28197_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28197_GM;
MethodInfo m28197_MI = 
{
	"RemoveAt", NULL, &t5386_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5386_m28197_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28197_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5386_m28193_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t188_0_0_0;
extern void* RuntimeInvoker_t188_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28193_GM;
MethodInfo m28193_MI = 
{
	"get_Item", NULL, &t5386_TI, &t188_0_0_0, RuntimeInvoker_t188_t44, t5386_m28193_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28193_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t188_0_0_0;
static ParameterInfo t5386_m28194_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t188_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28194_GM;
MethodInfo m28194_MI = 
{
	"set_Item", NULL, &t5386_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5386_m28194_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28194_GM};
static MethodInfo* t5386_MIs[] =
{
	&m28195_MI,
	&m28196_MI,
	&m28197_MI,
	&m28193_MI,
	&m28194_MI,
	NULL
};
static TypeInfo* t5386_ITIs[] = 
{
	&t603_TI,
	&t5385_TI,
	&t5387_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5386_0_0_0;
extern Il2CppType t5386_1_0_0;
struct t5386;
extern Il2CppGenericClass t5386_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5386_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5386_MIs, t5386_PIs, NULL, NULL, NULL, NULL, NULL, &t5386_TI, t5386_ITIs, NULL, &t1908__CustomAttributeCache, &t5386_TI, &t5386_0_0_0, &t5386_1_0_0, NULL, &t5386_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4207_TI;

#include "t192.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/EditState>
extern MethodInfo m28198_MI;
static PropertyInfo t4207____Current_PropertyInfo = 
{
	&t4207_TI, "Current", &m28198_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4207_PIs[] =
{
	&t4207____Current_PropertyInfo,
	NULL
};
extern Il2CppType t192_0_0_0;
extern void* RuntimeInvoker_t192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28198_GM;
MethodInfo m28198_MI = 
{
	"get_Current", NULL, &t4207_TI, &t192_0_0_0, RuntimeInvoker_t192, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28198_GM};
static MethodInfo* t4207_MIs[] =
{
	&m28198_MI,
	NULL
};
static TypeInfo* t4207_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4207_0_0_0;
extern Il2CppType t4207_1_0_0;
struct t4207;
extern Il2CppGenericClass t4207_GC;
TypeInfo t4207_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4207_MIs, t4207_PIs, NULL, NULL, NULL, NULL, NULL, &t4207_TI, t4207_ITIs, NULL, &EmptyCustomAttributesCache, &t4207_TI, &t4207_0_0_0, &t4207_1_0_0, NULL, &t4207_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2611.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2611_TI;
#include "t2611MD.h"

extern TypeInfo t192_TI;
extern MethodInfo m14068_MI;
extern MethodInfo m21260_MI;
struct t20;
 int32_t m21260 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14064_MI;
 void m14064 (t2611 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14065_MI;
 t29 * m14065 (t2611 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14068(__this, &m14068_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t192_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14066_MI;
 void m14066 (t2611 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14067_MI;
 bool m14067 (t2611 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14068 (t2611 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21260(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21260_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>
extern Il2CppType t20_0_0_1;
FieldInfo t2611_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2611_TI, offsetof(t2611, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2611_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2611_TI, offsetof(t2611, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2611_FIs[] =
{
	&t2611_f0_FieldInfo,
	&t2611_f1_FieldInfo,
	NULL
};
static PropertyInfo t2611____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2611_TI, "System.Collections.IEnumerator.Current", &m14065_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2611____Current_PropertyInfo = 
{
	&t2611_TI, "Current", &m14068_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2611_PIs[] =
{
	&t2611____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2611____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2611_m14064_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14064_GM;
MethodInfo m14064_MI = 
{
	".ctor", (methodPointerType)&m14064, &t2611_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2611_m14064_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14064_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14065_GM;
MethodInfo m14065_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14065, &t2611_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14065_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14066_GM;
MethodInfo m14066_MI = 
{
	"Dispose", (methodPointerType)&m14066, &t2611_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14066_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14067_GM;
MethodInfo m14067_MI = 
{
	"MoveNext", (methodPointerType)&m14067, &t2611_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14067_GM};
extern Il2CppType t192_0_0_0;
extern void* RuntimeInvoker_t192 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14068_GM;
MethodInfo m14068_MI = 
{
	"get_Current", (methodPointerType)&m14068, &t2611_TI, &t192_0_0_0, RuntimeInvoker_t192, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14068_GM};
static MethodInfo* t2611_MIs[] =
{
	&m14064_MI,
	&m14065_MI,
	&m14066_MI,
	&m14067_MI,
	&m14068_MI,
	NULL
};
static MethodInfo* t2611_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14065_MI,
	&m14067_MI,
	&m14066_MI,
	&m14068_MI,
};
static TypeInfo* t2611_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4207_TI,
};
static Il2CppInterfaceOffsetPair t2611_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4207_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2611_0_0_0;
extern Il2CppType t2611_1_0_0;
extern Il2CppGenericClass t2611_GC;
TypeInfo t2611_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2611_MIs, t2611_PIs, t2611_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2611_TI, t2611_ITIs, t2611_VT, &EmptyCustomAttributesCache, &t2611_TI, &t2611_0_0_0, &t2611_1_0_0, t2611_IOs, &t2611_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2611)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5388_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>
extern MethodInfo m28199_MI;
static PropertyInfo t5388____Count_PropertyInfo = 
{
	&t5388_TI, "Count", &m28199_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28200_MI;
static PropertyInfo t5388____IsReadOnly_PropertyInfo = 
{
	&t5388_TI, "IsReadOnly", &m28200_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5388_PIs[] =
{
	&t5388____Count_PropertyInfo,
	&t5388____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28199_GM;
MethodInfo m28199_MI = 
{
	"get_Count", NULL, &t5388_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28199_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28200_GM;
MethodInfo m28200_MI = 
{
	"get_IsReadOnly", NULL, &t5388_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28200_GM};
extern Il2CppType t192_0_0_0;
extern Il2CppType t192_0_0_0;
static ParameterInfo t5388_m28201_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t192_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28201_GM;
MethodInfo m28201_MI = 
{
	"Add", NULL, &t5388_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5388_m28201_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28201_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28202_GM;
MethodInfo m28202_MI = 
{
	"Clear", NULL, &t5388_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28202_GM};
extern Il2CppType t192_0_0_0;
static ParameterInfo t5388_m28203_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t192_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28203_GM;
MethodInfo m28203_MI = 
{
	"Contains", NULL, &t5388_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5388_m28203_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28203_GM};
extern Il2CppType t3850_0_0_0;
extern Il2CppType t3850_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5388_m28204_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3850_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28204_GM;
MethodInfo m28204_MI = 
{
	"CopyTo", NULL, &t5388_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5388_m28204_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28204_GM};
extern Il2CppType t192_0_0_0;
static ParameterInfo t5388_m28205_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t192_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28205_GM;
MethodInfo m28205_MI = 
{
	"Remove", NULL, &t5388_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5388_m28205_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28205_GM};
static MethodInfo* t5388_MIs[] =
{
	&m28199_MI,
	&m28200_MI,
	&m28201_MI,
	&m28202_MI,
	&m28203_MI,
	&m28204_MI,
	&m28205_MI,
	NULL
};
extern TypeInfo t5390_TI;
static TypeInfo* t5388_ITIs[] = 
{
	&t603_TI,
	&t5390_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5388_0_0_0;
extern Il2CppType t5388_1_0_0;
struct t5388;
extern Il2CppGenericClass t5388_GC;
TypeInfo t5388_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5388_MIs, t5388_PIs, NULL, NULL, NULL, NULL, NULL, &t5388_TI, t5388_ITIs, NULL, &EmptyCustomAttributesCache, &t5388_TI, &t5388_0_0_0, &t5388_1_0_0, NULL, &t5388_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/EditState>
extern Il2CppType t4207_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28206_GM;
MethodInfo m28206_MI = 
{
	"GetEnumerator", NULL, &t5390_TI, &t4207_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28206_GM};
static MethodInfo* t5390_MIs[] =
{
	&m28206_MI,
	NULL
};
static TypeInfo* t5390_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5390_0_0_0;
extern Il2CppType t5390_1_0_0;
struct t5390;
extern Il2CppGenericClass t5390_GC;
TypeInfo t5390_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5390_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5390_TI, t5390_ITIs, NULL, &EmptyCustomAttributesCache, &t5390_TI, &t5390_0_0_0, &t5390_1_0_0, NULL, &t5390_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5389_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.InputField/EditState>
extern MethodInfo m28207_MI;
extern MethodInfo m28208_MI;
static PropertyInfo t5389____Item_PropertyInfo = 
{
	&t5389_TI, "Item", &m28207_MI, &m28208_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5389_PIs[] =
{
	&t5389____Item_PropertyInfo,
	NULL
};
extern Il2CppType t192_0_0_0;
static ParameterInfo t5389_m28209_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t192_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28209_GM;
MethodInfo m28209_MI = 
{
	"IndexOf", NULL, &t5389_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5389_m28209_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28209_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t192_0_0_0;
static ParameterInfo t5389_m28210_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t192_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28210_GM;
MethodInfo m28210_MI = 
{
	"Insert", NULL, &t5389_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5389_m28210_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28210_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5389_m28211_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28211_GM;
MethodInfo m28211_MI = 
{
	"RemoveAt", NULL, &t5389_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5389_m28211_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28211_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5389_m28207_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t192_0_0_0;
extern void* RuntimeInvoker_t192_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28207_GM;
MethodInfo m28207_MI = 
{
	"get_Item", NULL, &t5389_TI, &t192_0_0_0, RuntimeInvoker_t192_t44, t5389_m28207_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28207_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t192_0_0_0;
static ParameterInfo t5389_m28208_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t192_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28208_GM;
MethodInfo m28208_MI = 
{
	"set_Item", NULL, &t5389_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5389_m28208_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28208_GM};
static MethodInfo* t5389_MIs[] =
{
	&m28209_MI,
	&m28210_MI,
	&m28211_MI,
	&m28207_MI,
	&m28208_MI,
	NULL
};
static TypeInfo* t5389_ITIs[] = 
{
	&t603_TI,
	&t5388_TI,
	&t5390_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5389_0_0_0;
extern Il2CppType t5389_1_0_0;
struct t5389;
extern Il2CppGenericClass t5389_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5389_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5389_MIs, t5389_PIs, NULL, NULL, NULL, NULL, NULL, &t5389_TI, t5389_ITIs, NULL, &t1908__CustomAttributeCache, &t5389_TI, &t5389_0_0_0, &t5389_1_0_0, NULL, &t5389_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2612.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2612_TI;
#include "t2612MD.h"

#include "t182.h"
#include "t2613.h"
extern TypeInfo t182_TI;
extern TypeInfo t2613_TI;
#include "t2613MD.h"
extern MethodInfo m14071_MI;
extern MethodInfo m14073_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.MaskableGraphic>
extern Il2CppType t316_0_0_33;
FieldInfo t2612_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2612_TI, offsetof(t2612, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2612_FIs[] =
{
	&t2612_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t182_0_0_0;
extern Il2CppType t182_0_0_0;
static ParameterInfo t2612_m14069_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t182_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14069_GM;
MethodInfo m14069_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2612_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2612_m14069_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14069_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2612_m14070_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14070_GM;
MethodInfo m14070_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2612_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2612_m14070_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14070_GM};
static MethodInfo* t2612_MIs[] =
{
	&m14069_MI,
	&m14070_MI,
	NULL
};
extern MethodInfo m14070_MI;
extern MethodInfo m14074_MI;
static MethodInfo* t2612_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14070_MI,
	&m14074_MI,
};
extern Il2CppType t2614_0_0_0;
extern TypeInfo t2614_TI;
extern MethodInfo m21270_MI;
extern TypeInfo t182_TI;
extern MethodInfo m14076_MI;
extern TypeInfo t182_TI;
static Il2CppRGCTXData t2612_RGCTXData[8] = 
{
	&t2614_0_0_0/* Type Usage */,
	&t2614_TI/* Class Usage */,
	&m21270_MI/* Method Usage */,
	&t182_TI/* Class Usage */,
	&m14076_MI/* Method Usage */,
	&m14071_MI/* Method Usage */,
	&t182_TI/* Class Usage */,
	&m14073_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2612_0_0_0;
extern Il2CppType t2612_1_0_0;
struct t2612;
extern Il2CppGenericClass t2612_GC;
TypeInfo t2612_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2612_MIs, NULL, t2612_FIs, NULL, &t2613_TI, NULL, NULL, &t2612_TI, NULL, t2612_VT, &EmptyCustomAttributesCache, &t2612_TI, &t2612_0_0_0, &t2612_1_0_0, NULL, &t2612_GC, NULL, NULL, NULL, t2612_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2612), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2614.h"
extern TypeInfo t2614_TI;
#include "t2614MD.h"
struct t556;
#define m21270(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>
extern Il2CppType t2614_0_0_1;
FieldInfo t2613_f0_FieldInfo = 
{
	"Delegate", &t2614_0_0_1, &t2613_TI, offsetof(t2613, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2613_FIs[] =
{
	&t2613_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2613_m14071_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14071_GM;
MethodInfo m14071_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2613_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2613_m14071_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14071_GM};
extern Il2CppType t2614_0_0_0;
static ParameterInfo t2613_m14072_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2614_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14072_GM;
MethodInfo m14072_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2613_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2613_m14072_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14072_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2613_m14073_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14073_GM;
MethodInfo m14073_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2613_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2613_m14073_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14073_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2613_m14074_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14074_GM;
MethodInfo m14074_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2613_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2613_m14074_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14074_GM};
static MethodInfo* t2613_MIs[] =
{
	&m14071_MI,
	&m14072_MI,
	&m14073_MI,
	&m14074_MI,
	NULL
};
static MethodInfo* t2613_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14073_MI,
	&m14074_MI,
};
extern TypeInfo t2614_TI;
extern TypeInfo t182_TI;
static Il2CppRGCTXData t2613_RGCTXData[5] = 
{
	&t2614_0_0_0/* Type Usage */,
	&t2614_TI/* Class Usage */,
	&m21270_MI/* Method Usage */,
	&t182_TI/* Class Usage */,
	&m14076_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2613_0_0_0;
extern Il2CppType t2613_1_0_0;
struct t2613;
extern Il2CppGenericClass t2613_GC;
TypeInfo t2613_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2613_MIs, NULL, t2613_FIs, NULL, &t556_TI, NULL, NULL, &t2613_TI, NULL, t2613_VT, &EmptyCustomAttributesCache, &t2613_TI, &t2613_0_0_0, &t2613_1_0_0, NULL, &t2613_GC, NULL, NULL, NULL, t2613_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2613), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.MaskableGraphic>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2614_m14075_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14075_GM;
MethodInfo m14075_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2614_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2614_m14075_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14075_GM};
extern Il2CppType t182_0_0_0;
static ParameterInfo t2614_m14076_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t182_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14076_GM;
MethodInfo m14076_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2614_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2614_m14076_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14076_GM};
extern Il2CppType t182_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2614_m14077_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t182_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14077_GM;
MethodInfo m14077_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2614_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2614_m14077_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14077_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2614_m14078_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14078_GM;
MethodInfo m14078_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2614_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2614_m14078_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14078_GM};
static MethodInfo* t2614_MIs[] =
{
	&m14075_MI,
	&m14076_MI,
	&m14077_MI,
	&m14078_MI,
	NULL
};
extern MethodInfo m14077_MI;
extern MethodInfo m14078_MI;
static MethodInfo* t2614_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14076_MI,
	&m14077_MI,
	&m14078_MI,
};
static Il2CppInterfaceOffsetPair t2614_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2614_1_0_0;
struct t2614;
extern Il2CppGenericClass t2614_GC;
TypeInfo t2614_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2614_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2614_TI, NULL, t2614_VT, &EmptyCustomAttributesCache, &t2614_TI, &t2614_0_0_0, &t2614_1_0_0, t2614_IOs, &t2614_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2614), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4209_TI;

#include "t209.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Navigation/Mode>
extern MethodInfo m28212_MI;
static PropertyInfo t4209____Current_PropertyInfo = 
{
	&t4209_TI, "Current", &m28212_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4209_PIs[] =
{
	&t4209____Current_PropertyInfo,
	NULL
};
extern Il2CppType t209_0_0_0;
extern void* RuntimeInvoker_t209 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28212_GM;
MethodInfo m28212_MI = 
{
	"get_Current", NULL, &t4209_TI, &t209_0_0_0, RuntimeInvoker_t209, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28212_GM};
static MethodInfo* t4209_MIs[] =
{
	&m28212_MI,
	NULL
};
static TypeInfo* t4209_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4209_0_0_0;
extern Il2CppType t4209_1_0_0;
struct t4209;
extern Il2CppGenericClass t4209_GC;
TypeInfo t4209_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4209_MIs, t4209_PIs, NULL, NULL, NULL, NULL, NULL, &t4209_TI, t4209_ITIs, NULL, &EmptyCustomAttributesCache, &t4209_TI, &t4209_0_0_0, &t4209_1_0_0, NULL, &t4209_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2615.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2615_TI;
#include "t2615MD.h"

extern TypeInfo t209_TI;
extern MethodInfo m14083_MI;
extern MethodInfo m21272_MI;
struct t20;
 int32_t m21272 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m14079_MI;
 void m14079 (t2615 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14080_MI;
 t29 * m14080 (t2615 * __this, MethodInfo* method){
	{
		int32_t L_0 = m14083(__this, &m14083_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t209_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m14081_MI;
 void m14081 (t2615 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m14082_MI;
 bool m14082 (t2615 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m14083 (t2615 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m21272(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m21272_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>
extern Il2CppType t20_0_0_1;
FieldInfo t2615_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2615_TI, offsetof(t2615, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2615_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2615_TI, offsetof(t2615, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2615_FIs[] =
{
	&t2615_f0_FieldInfo,
	&t2615_f1_FieldInfo,
	NULL
};
static PropertyInfo t2615____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2615_TI, "System.Collections.IEnumerator.Current", &m14080_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2615____Current_PropertyInfo = 
{
	&t2615_TI, "Current", &m14083_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2615_PIs[] =
{
	&t2615____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2615____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2615_m14079_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14079_GM;
MethodInfo m14079_MI = 
{
	".ctor", (methodPointerType)&m14079, &t2615_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2615_m14079_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14079_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14080_GM;
MethodInfo m14080_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m14080, &t2615_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14080_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14081_GM;
MethodInfo m14081_MI = 
{
	"Dispose", (methodPointerType)&m14081, &t2615_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14081_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14082_GM;
MethodInfo m14082_MI = 
{
	"MoveNext", (methodPointerType)&m14082, &t2615_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14082_GM};
extern Il2CppType t209_0_0_0;
extern void* RuntimeInvoker_t209 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14083_GM;
MethodInfo m14083_MI = 
{
	"get_Current", (methodPointerType)&m14083, &t2615_TI, &t209_0_0_0, RuntimeInvoker_t209, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14083_GM};
static MethodInfo* t2615_MIs[] =
{
	&m14079_MI,
	&m14080_MI,
	&m14081_MI,
	&m14082_MI,
	&m14083_MI,
	NULL
};
static MethodInfo* t2615_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14080_MI,
	&m14082_MI,
	&m14081_MI,
	&m14083_MI,
};
static TypeInfo* t2615_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4209_TI,
};
static Il2CppInterfaceOffsetPair t2615_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4209_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2615_0_0_0;
extern Il2CppType t2615_1_0_0;
extern Il2CppGenericClass t2615_GC;
TypeInfo t2615_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2615_MIs, t2615_PIs, t2615_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2615_TI, t2615_ITIs, t2615_VT, &EmptyCustomAttributesCache, &t2615_TI, &t2615_0_0_0, &t2615_1_0_0, t2615_IOs, &t2615_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2615)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5391_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>
extern MethodInfo m28213_MI;
static PropertyInfo t5391____Count_PropertyInfo = 
{
	&t5391_TI, "Count", &m28213_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28214_MI;
static PropertyInfo t5391____IsReadOnly_PropertyInfo = 
{
	&t5391_TI, "IsReadOnly", &m28214_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5391_PIs[] =
{
	&t5391____Count_PropertyInfo,
	&t5391____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28213_GM;
MethodInfo m28213_MI = 
{
	"get_Count", NULL, &t5391_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28213_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28214_GM;
MethodInfo m28214_MI = 
{
	"get_IsReadOnly", NULL, &t5391_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28214_GM};
extern Il2CppType t209_0_0_0;
extern Il2CppType t209_0_0_0;
static ParameterInfo t5391_m28215_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t209_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28215_GM;
MethodInfo m28215_MI = 
{
	"Add", NULL, &t5391_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5391_m28215_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28215_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28216_GM;
MethodInfo m28216_MI = 
{
	"Clear", NULL, &t5391_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28216_GM};
extern Il2CppType t209_0_0_0;
static ParameterInfo t5391_m28217_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t209_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28217_GM;
MethodInfo m28217_MI = 
{
	"Contains", NULL, &t5391_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5391_m28217_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28217_GM};
extern Il2CppType t3851_0_0_0;
extern Il2CppType t3851_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5391_m28218_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3851_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28218_GM;
MethodInfo m28218_MI = 
{
	"CopyTo", NULL, &t5391_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5391_m28218_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28218_GM};
extern Il2CppType t209_0_0_0;
static ParameterInfo t5391_m28219_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t209_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28219_GM;
MethodInfo m28219_MI = 
{
	"Remove", NULL, &t5391_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t5391_m28219_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28219_GM};
static MethodInfo* t5391_MIs[] =
{
	&m28213_MI,
	&m28214_MI,
	&m28215_MI,
	&m28216_MI,
	&m28217_MI,
	&m28218_MI,
	&m28219_MI,
	NULL
};
extern TypeInfo t5393_TI;
static TypeInfo* t5391_ITIs[] = 
{
	&t603_TI,
	&t5393_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5391_0_0_0;
extern Il2CppType t5391_1_0_0;
struct t5391;
extern Il2CppGenericClass t5391_GC;
TypeInfo t5391_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5391_MIs, t5391_PIs, NULL, NULL, NULL, NULL, NULL, &t5391_TI, t5391_ITIs, NULL, &EmptyCustomAttributesCache, &t5391_TI, &t5391_0_0_0, &t5391_1_0_0, NULL, &t5391_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Navigation/Mode>
extern Il2CppType t4209_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28220_GM;
MethodInfo m28220_MI = 
{
	"GetEnumerator", NULL, &t5393_TI, &t4209_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28220_GM};
static MethodInfo* t5393_MIs[] =
{
	&m28220_MI,
	NULL
};
static TypeInfo* t5393_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5393_0_0_0;
extern Il2CppType t5393_1_0_0;
struct t5393;
extern Il2CppGenericClass t5393_GC;
TypeInfo t5393_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5393_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5393_TI, t5393_ITIs, NULL, &EmptyCustomAttributesCache, &t5393_TI, &t5393_0_0_0, &t5393_1_0_0, NULL, &t5393_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5392_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Navigation/Mode>
extern MethodInfo m28221_MI;
extern MethodInfo m28222_MI;
static PropertyInfo t5392____Item_PropertyInfo = 
{
	&t5392_TI, "Item", &m28221_MI, &m28222_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5392_PIs[] =
{
	&t5392____Item_PropertyInfo,
	NULL
};
extern Il2CppType t209_0_0_0;
static ParameterInfo t5392_m28223_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t209_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28223_GM;
MethodInfo m28223_MI = 
{
	"IndexOf", NULL, &t5392_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t5392_m28223_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28223_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t209_0_0_0;
static ParameterInfo t5392_m28224_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t209_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28224_GM;
MethodInfo m28224_MI = 
{
	"Insert", NULL, &t5392_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5392_m28224_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28224_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5392_m28225_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28225_GM;
MethodInfo m28225_MI = 
{
	"RemoveAt", NULL, &t5392_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5392_m28225_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28225_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5392_m28221_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t209_0_0_0;
extern void* RuntimeInvoker_t209_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28221_GM;
MethodInfo m28221_MI = 
{
	"get_Item", NULL, &t5392_TI, &t209_0_0_0, RuntimeInvoker_t209_t44, t5392_m28221_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28221_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t209_0_0_0;
static ParameterInfo t5392_m28222_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t209_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28222_GM;
MethodInfo m28222_MI = 
{
	"set_Item", NULL, &t5392_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t5392_m28222_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28222_GM};
static MethodInfo* t5392_MIs[] =
{
	&m28223_MI,
	&m28224_MI,
	&m28225_MI,
	&m28221_MI,
	&m28222_MI,
	NULL
};
static TypeInfo* t5392_ITIs[] = 
{
	&t603_TI,
	&t5391_TI,
	&t5393_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5392_0_0_0;
extern Il2CppType t5392_1_0_0;
struct t5392;
extern Il2CppGenericClass t5392_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5392_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5392_MIs, t5392_PIs, NULL, NULL, NULL, NULL, NULL, &t5392_TI, t5392_ITIs, NULL, &t1908__CustomAttributeCache, &t5392_TI, &t5392_0_0_0, &t5392_1_0_0, NULL, &t5392_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4211_TI;

#include "t211.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.RawImage>
extern MethodInfo m28226_MI;
static PropertyInfo t4211____Current_PropertyInfo = 
{
	&t4211_TI, "Current", &m28226_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4211_PIs[] =
{
	&t4211____Current_PropertyInfo,
	NULL
};
extern Il2CppType t211_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28226_GM;
MethodInfo m28226_MI = 
{
	"get_Current", NULL, &t4211_TI, &t211_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28226_GM};
static MethodInfo* t4211_MIs[] =
{
	&m28226_MI,
	NULL
};
static TypeInfo* t4211_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4211_0_0_0;
extern Il2CppType t4211_1_0_0;
struct t4211;
extern Il2CppGenericClass t4211_GC;
TypeInfo t4211_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4211_MIs, t4211_PIs, NULL, NULL, NULL, NULL, NULL, &t4211_TI, t4211_ITIs, NULL, &EmptyCustomAttributesCache, &t4211_TI, &t4211_0_0_0, &t4211_1_0_0, NULL, &t4211_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2616.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2616_TI;
#include "t2616MD.h"

extern TypeInfo t211_TI;
extern MethodInfo m14088_MI;
extern MethodInfo m21283_MI;
struct t20;
#define m21283(__this, p0, method) (t211 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>
extern Il2CppType t20_0_0_1;
FieldInfo t2616_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2616_TI, offsetof(t2616, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2616_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2616_TI, offsetof(t2616, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2616_FIs[] =
{
	&t2616_f0_FieldInfo,
	&t2616_f1_FieldInfo,
	NULL
};
extern MethodInfo m14085_MI;
static PropertyInfo t2616____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2616_TI, "System.Collections.IEnumerator.Current", &m14085_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2616____Current_PropertyInfo = 
{
	&t2616_TI, "Current", &m14088_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2616_PIs[] =
{
	&t2616____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2616____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2616_m14084_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14084_GM;
MethodInfo m14084_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2616_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2616_m14084_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14084_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14085_GM;
MethodInfo m14085_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2616_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14085_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14086_GM;
MethodInfo m14086_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2616_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14086_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14087_GM;
MethodInfo m14087_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2616_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14087_GM};
extern Il2CppType t211_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14088_GM;
MethodInfo m14088_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2616_TI, &t211_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14088_GM};
static MethodInfo* t2616_MIs[] =
{
	&m14084_MI,
	&m14085_MI,
	&m14086_MI,
	&m14087_MI,
	&m14088_MI,
	NULL
};
extern MethodInfo m14087_MI;
extern MethodInfo m14086_MI;
static MethodInfo* t2616_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14085_MI,
	&m14087_MI,
	&m14086_MI,
	&m14088_MI,
};
static TypeInfo* t2616_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4211_TI,
};
static Il2CppInterfaceOffsetPair t2616_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4211_TI, 7},
};
extern TypeInfo t211_TI;
static Il2CppRGCTXData t2616_RGCTXData[3] = 
{
	&m14088_MI/* Method Usage */,
	&t211_TI/* Class Usage */,
	&m21283_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2616_0_0_0;
extern Il2CppType t2616_1_0_0;
extern Il2CppGenericClass t2616_GC;
TypeInfo t2616_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2616_MIs, t2616_PIs, t2616_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2616_TI, t2616_ITIs, t2616_VT, &EmptyCustomAttributesCache, &t2616_TI, &t2616_0_0_0, &t2616_1_0_0, t2616_IOs, &t2616_GC, NULL, NULL, NULL, t2616_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2616)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5394_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>
extern MethodInfo m28227_MI;
static PropertyInfo t5394____Count_PropertyInfo = 
{
	&t5394_TI, "Count", &m28227_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28228_MI;
static PropertyInfo t5394____IsReadOnly_PropertyInfo = 
{
	&t5394_TI, "IsReadOnly", &m28228_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5394_PIs[] =
{
	&t5394____Count_PropertyInfo,
	&t5394____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28227_GM;
MethodInfo m28227_MI = 
{
	"get_Count", NULL, &t5394_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28227_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28228_GM;
MethodInfo m28228_MI = 
{
	"get_IsReadOnly", NULL, &t5394_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28228_GM};
extern Il2CppType t211_0_0_0;
extern Il2CppType t211_0_0_0;
static ParameterInfo t5394_m28229_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t211_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28229_GM;
MethodInfo m28229_MI = 
{
	"Add", NULL, &t5394_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5394_m28229_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28229_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28230_GM;
MethodInfo m28230_MI = 
{
	"Clear", NULL, &t5394_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28230_GM};
extern Il2CppType t211_0_0_0;
static ParameterInfo t5394_m28231_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t211_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28231_GM;
MethodInfo m28231_MI = 
{
	"Contains", NULL, &t5394_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5394_m28231_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28231_GM};
extern Il2CppType t3852_0_0_0;
extern Il2CppType t3852_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5394_m28232_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3852_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28232_GM;
MethodInfo m28232_MI = 
{
	"CopyTo", NULL, &t5394_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5394_m28232_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28232_GM};
extern Il2CppType t211_0_0_0;
static ParameterInfo t5394_m28233_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t211_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28233_GM;
MethodInfo m28233_MI = 
{
	"Remove", NULL, &t5394_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5394_m28233_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28233_GM};
static MethodInfo* t5394_MIs[] =
{
	&m28227_MI,
	&m28228_MI,
	&m28229_MI,
	&m28230_MI,
	&m28231_MI,
	&m28232_MI,
	&m28233_MI,
	NULL
};
extern TypeInfo t5396_TI;
static TypeInfo* t5394_ITIs[] = 
{
	&t603_TI,
	&t5396_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5394_0_0_0;
extern Il2CppType t5394_1_0_0;
struct t5394;
extern Il2CppGenericClass t5394_GC;
TypeInfo t5394_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5394_MIs, t5394_PIs, NULL, NULL, NULL, NULL, NULL, &t5394_TI, t5394_ITIs, NULL, &EmptyCustomAttributesCache, &t5394_TI, &t5394_0_0_0, &t5394_1_0_0, NULL, &t5394_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.RawImage>
extern Il2CppType t4211_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28234_GM;
MethodInfo m28234_MI = 
{
	"GetEnumerator", NULL, &t5396_TI, &t4211_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28234_GM};
static MethodInfo* t5396_MIs[] =
{
	&m28234_MI,
	NULL
};
static TypeInfo* t5396_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5396_0_0_0;
extern Il2CppType t5396_1_0_0;
struct t5396;
extern Il2CppGenericClass t5396_GC;
TypeInfo t5396_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5396_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5396_TI, t5396_ITIs, NULL, &EmptyCustomAttributesCache, &t5396_TI, &t5396_0_0_0, &t5396_1_0_0, NULL, &t5396_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5395_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.RawImage>
extern MethodInfo m28235_MI;
extern MethodInfo m28236_MI;
static PropertyInfo t5395____Item_PropertyInfo = 
{
	&t5395_TI, "Item", &m28235_MI, &m28236_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5395_PIs[] =
{
	&t5395____Item_PropertyInfo,
	NULL
};
extern Il2CppType t211_0_0_0;
static ParameterInfo t5395_m28237_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t211_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28237_GM;
MethodInfo m28237_MI = 
{
	"IndexOf", NULL, &t5395_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5395_m28237_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28237_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t211_0_0_0;
static ParameterInfo t5395_m28238_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t211_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28238_GM;
MethodInfo m28238_MI = 
{
	"Insert", NULL, &t5395_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5395_m28238_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28238_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5395_m28239_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28239_GM;
MethodInfo m28239_MI = 
{
	"RemoveAt", NULL, &t5395_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5395_m28239_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28239_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5395_m28235_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t211_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28235_GM;
MethodInfo m28235_MI = 
{
	"get_Item", NULL, &t5395_TI, &t211_0_0_0, RuntimeInvoker_t29_t44, t5395_m28235_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28235_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t211_0_0_0;
static ParameterInfo t5395_m28236_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t211_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28236_GM;
MethodInfo m28236_MI = 
{
	"set_Item", NULL, &t5395_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5395_m28236_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28236_GM};
static MethodInfo* t5395_MIs[] =
{
	&m28237_MI,
	&m28238_MI,
	&m28239_MI,
	&m28235_MI,
	&m28236_MI,
	NULL
};
static TypeInfo* t5395_ITIs[] = 
{
	&t603_TI,
	&t5394_TI,
	&t5396_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5395_0_0_0;
extern Il2CppType t5395_1_0_0;
struct t5395;
extern Il2CppGenericClass t5395_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5395_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5395_MIs, t5395_PIs, NULL, NULL, NULL, NULL, NULL, &t5395_TI, t5395_ITIs, NULL, &t1908__CustomAttributeCache, &t5395_TI, &t5395_0_0_0, &t5395_1_0_0, NULL, &t5395_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2617.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2617_TI;
#include "t2617MD.h"

#include "t2618.h"
extern TypeInfo t2618_TI;
#include "t2618MD.h"
extern MethodInfo m14091_MI;
extern MethodInfo m14093_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.RawImage>
extern Il2CppType t316_0_0_33;
FieldInfo t2617_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2617_TI, offsetof(t2617, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2617_FIs[] =
{
	&t2617_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t211_0_0_0;
static ParameterInfo t2617_m14089_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t211_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14089_GM;
MethodInfo m14089_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2617_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2617_m14089_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14089_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2617_m14090_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14090_GM;
MethodInfo m14090_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2617_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2617_m14090_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14090_GM};
static MethodInfo* t2617_MIs[] =
{
	&m14089_MI,
	&m14090_MI,
	NULL
};
extern MethodInfo m14090_MI;
extern MethodInfo m14094_MI;
static MethodInfo* t2617_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14090_MI,
	&m14094_MI,
};
extern Il2CppType t2619_0_0_0;
extern TypeInfo t2619_TI;
extern MethodInfo m21293_MI;
extern TypeInfo t211_TI;
extern MethodInfo m14096_MI;
extern TypeInfo t211_TI;
static Il2CppRGCTXData t2617_RGCTXData[8] = 
{
	&t2619_0_0_0/* Type Usage */,
	&t2619_TI/* Class Usage */,
	&m21293_MI/* Method Usage */,
	&t211_TI/* Class Usage */,
	&m14096_MI/* Method Usage */,
	&m14091_MI/* Method Usage */,
	&t211_TI/* Class Usage */,
	&m14093_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2617_0_0_0;
extern Il2CppType t2617_1_0_0;
struct t2617;
extern Il2CppGenericClass t2617_GC;
TypeInfo t2617_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2617_MIs, NULL, t2617_FIs, NULL, &t2618_TI, NULL, NULL, &t2617_TI, NULL, t2617_VT, &EmptyCustomAttributesCache, &t2617_TI, &t2617_0_0_0, &t2617_1_0_0, NULL, &t2617_GC, NULL, NULL, NULL, t2617_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2617), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2619.h"
extern TypeInfo t2619_TI;
#include "t2619MD.h"
struct t556;
#define m21293(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>
extern Il2CppType t2619_0_0_1;
FieldInfo t2618_f0_FieldInfo = 
{
	"Delegate", &t2619_0_0_1, &t2618_TI, offsetof(t2618, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2618_FIs[] =
{
	&t2618_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2618_m14091_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14091_GM;
MethodInfo m14091_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2618_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2618_m14091_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14091_GM};
extern Il2CppType t2619_0_0_0;
static ParameterInfo t2618_m14092_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2619_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14092_GM;
MethodInfo m14092_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2618_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2618_m14092_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14092_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2618_m14093_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14093_GM;
MethodInfo m14093_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2618_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2618_m14093_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14093_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2618_m14094_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14094_GM;
MethodInfo m14094_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2618_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2618_m14094_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14094_GM};
static MethodInfo* t2618_MIs[] =
{
	&m14091_MI,
	&m14092_MI,
	&m14093_MI,
	&m14094_MI,
	NULL
};
static MethodInfo* t2618_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14093_MI,
	&m14094_MI,
};
extern TypeInfo t2619_TI;
extern TypeInfo t211_TI;
static Il2CppRGCTXData t2618_RGCTXData[5] = 
{
	&t2619_0_0_0/* Type Usage */,
	&t2619_TI/* Class Usage */,
	&m21293_MI/* Method Usage */,
	&t211_TI/* Class Usage */,
	&m14096_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2618_0_0_0;
extern Il2CppType t2618_1_0_0;
struct t2618;
extern Il2CppGenericClass t2618_GC;
TypeInfo t2618_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2618_MIs, NULL, t2618_FIs, NULL, &t556_TI, NULL, NULL, &t2618_TI, NULL, t2618_VT, &EmptyCustomAttributesCache, &t2618_TI, &t2618_0_0_0, &t2618_1_0_0, NULL, &t2618_GC, NULL, NULL, NULL, t2618_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2618), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.RawImage>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2619_m14095_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14095_GM;
MethodInfo m14095_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2619_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2619_m14095_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14095_GM};
extern Il2CppType t211_0_0_0;
static ParameterInfo t2619_m14096_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t211_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14096_GM;
MethodInfo m14096_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2619_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2619_m14096_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14096_GM};
extern Il2CppType t211_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2619_m14097_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t211_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14097_GM;
MethodInfo m14097_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2619_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2619_m14097_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14097_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2619_m14098_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14098_GM;
MethodInfo m14098_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2619_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2619_m14098_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14098_GM};
static MethodInfo* t2619_MIs[] =
{
	&m14095_MI,
	&m14096_MI,
	&m14097_MI,
	&m14098_MI,
	NULL
};
extern MethodInfo m14097_MI;
extern MethodInfo m14098_MI;
static MethodInfo* t2619_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14096_MI,
	&m14097_MI,
	&m14098_MI,
};
static Il2CppInterfaceOffsetPair t2619_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2619_1_0_0;
struct t2619;
extern Il2CppGenericClass t2619_GC;
TypeInfo t2619_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2619_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2619_TI, NULL, t2619_VT, &EmptyCustomAttributesCache, &t2619_TI, &t2619_0_0_0, &t2619_1_0_0, t2619_IOs, &t2619_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2619), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4213_TI;

#include "t217.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Scrollbar>
extern MethodInfo m28240_MI;
static PropertyInfo t4213____Current_PropertyInfo = 
{
	&t4213_TI, "Current", &m28240_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4213_PIs[] =
{
	&t4213____Current_PropertyInfo,
	NULL
};
extern Il2CppType t217_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28240_GM;
MethodInfo m28240_MI = 
{
	"get_Current", NULL, &t4213_TI, &t217_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28240_GM};
static MethodInfo* t4213_MIs[] =
{
	&m28240_MI,
	NULL
};
static TypeInfo* t4213_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4213_0_0_0;
extern Il2CppType t4213_1_0_0;
struct t4213;
extern Il2CppGenericClass t4213_GC;
TypeInfo t4213_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4213_MIs, t4213_PIs, NULL, NULL, NULL, NULL, NULL, &t4213_TI, t4213_ITIs, NULL, &EmptyCustomAttributesCache, &t4213_TI, &t4213_0_0_0, &t4213_1_0_0, NULL, &t4213_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t2620.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2620_TI;
#include "t2620MD.h"

extern TypeInfo t217_TI;
extern MethodInfo m14103_MI;
extern MethodInfo m21295_MI;
struct t20;
#define m21295(__this, p0, method) (t217 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>
extern Il2CppType t20_0_0_1;
FieldInfo t2620_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t2620_TI, offsetof(t2620, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t2620_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t2620_TI, offsetof(t2620, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t2620_FIs[] =
{
	&t2620_f0_FieldInfo,
	&t2620_f1_FieldInfo,
	NULL
};
extern MethodInfo m14100_MI;
static PropertyInfo t2620____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t2620_TI, "System.Collections.IEnumerator.Current", &m14100_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t2620____Current_PropertyInfo = 
{
	&t2620_TI, "Current", &m14103_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t2620_PIs[] =
{
	&t2620____System_Collections_IEnumerator_Current_PropertyInfo,
	&t2620____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t2620_m14099_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14099_GM;
MethodInfo m14099_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t2620_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2620_m14099_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14099_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14100_GM;
MethodInfo m14100_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t2620_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14100_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14101_GM;
MethodInfo m14101_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t2620_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14101_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14102_GM;
MethodInfo m14102_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t2620_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14102_GM};
extern Il2CppType t217_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14103_GM;
MethodInfo m14103_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t2620_TI, &t217_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m14103_GM};
static MethodInfo* t2620_MIs[] =
{
	&m14099_MI,
	&m14100_MI,
	&m14101_MI,
	&m14102_MI,
	&m14103_MI,
	NULL
};
extern MethodInfo m14102_MI;
extern MethodInfo m14101_MI;
static MethodInfo* t2620_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m14100_MI,
	&m14102_MI,
	&m14101_MI,
	&m14103_MI,
};
static TypeInfo* t2620_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4213_TI,
};
static Il2CppInterfaceOffsetPair t2620_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4213_TI, 7},
};
extern TypeInfo t217_TI;
static Il2CppRGCTXData t2620_RGCTXData[3] = 
{
	&m14103_MI/* Method Usage */,
	&t217_TI/* Class Usage */,
	&m21295_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t2620_0_0_0;
extern Il2CppType t2620_1_0_0;
extern Il2CppGenericClass t2620_GC;
TypeInfo t2620_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t2620_MIs, t2620_PIs, t2620_FIs, NULL, &t110_TI, NULL, &t20_TI, &t2620_TI, t2620_ITIs, t2620_VT, &EmptyCustomAttributesCache, &t2620_TI, &t2620_0_0_0, &t2620_1_0_0, t2620_IOs, &t2620_GC, NULL, NULL, NULL, t2620_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2620)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5397_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Scrollbar>
extern MethodInfo m28241_MI;
static PropertyInfo t5397____Count_PropertyInfo = 
{
	&t5397_TI, "Count", &m28241_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m28242_MI;
static PropertyInfo t5397____IsReadOnly_PropertyInfo = 
{
	&t5397_TI, "IsReadOnly", &m28242_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5397_PIs[] =
{
	&t5397____Count_PropertyInfo,
	&t5397____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28241_GM;
MethodInfo m28241_MI = 
{
	"get_Count", NULL, &t5397_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28241_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28242_GM;
MethodInfo m28242_MI = 
{
	"get_IsReadOnly", NULL, &t5397_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28242_GM};
extern Il2CppType t217_0_0_0;
extern Il2CppType t217_0_0_0;
static ParameterInfo t5397_m28243_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t217_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28243_GM;
MethodInfo m28243_MI = 
{
	"Add", NULL, &t5397_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t5397_m28243_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28243_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28244_GM;
MethodInfo m28244_MI = 
{
	"Clear", NULL, &t5397_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28244_GM};
extern Il2CppType t217_0_0_0;
static ParameterInfo t5397_m28245_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t217_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28245_GM;
MethodInfo m28245_MI = 
{
	"Contains", NULL, &t5397_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5397_m28245_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28245_GM};
extern Il2CppType t3853_0_0_0;
extern Il2CppType t3853_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t5397_m28246_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3853_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28246_GM;
MethodInfo m28246_MI = 
{
	"CopyTo", NULL, &t5397_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t5397_m28246_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28246_GM};
extern Il2CppType t217_0_0_0;
static ParameterInfo t5397_m28247_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t217_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28247_GM;
MethodInfo m28247_MI = 
{
	"Remove", NULL, &t5397_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t5397_m28247_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28247_GM};
static MethodInfo* t5397_MIs[] =
{
	&m28241_MI,
	&m28242_MI,
	&m28243_MI,
	&m28244_MI,
	&m28245_MI,
	&m28246_MI,
	&m28247_MI,
	NULL
};
extern TypeInfo t5399_TI;
static TypeInfo* t5397_ITIs[] = 
{
	&t603_TI,
	&t5399_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5397_0_0_0;
extern Il2CppType t5397_1_0_0;
struct t5397;
extern Il2CppGenericClass t5397_GC;
TypeInfo t5397_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t5397_MIs, t5397_PIs, NULL, NULL, NULL, NULL, NULL, &t5397_TI, t5397_ITIs, NULL, &EmptyCustomAttributesCache, &t5397_TI, &t5397_0_0_0, &t5397_1_0_0, NULL, &t5397_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Scrollbar>
extern Il2CppType t4213_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28248_GM;
MethodInfo m28248_MI = 
{
	"GetEnumerator", NULL, &t5399_TI, &t4213_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m28248_GM};
static MethodInfo* t5399_MIs[] =
{
	&m28248_MI,
	NULL
};
static TypeInfo* t5399_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5399_0_0_0;
extern Il2CppType t5399_1_0_0;
struct t5399;
extern Il2CppGenericClass t5399_GC;
TypeInfo t5399_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t5399_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t5399_TI, t5399_ITIs, NULL, &EmptyCustomAttributesCache, &t5399_TI, &t5399_0_0_0, &t5399_1_0_0, NULL, &t5399_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t5398_TI;



// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Scrollbar>
extern MethodInfo m28249_MI;
extern MethodInfo m28250_MI;
static PropertyInfo t5398____Item_PropertyInfo = 
{
	&t5398_TI, "Item", &m28249_MI, &m28250_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t5398_PIs[] =
{
	&t5398____Item_PropertyInfo,
	NULL
};
extern Il2CppType t217_0_0_0;
static ParameterInfo t5398_m28251_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t217_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28251_GM;
MethodInfo m28251_MI = 
{
	"IndexOf", NULL, &t5398_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t5398_m28251_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28251_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t217_0_0_0;
static ParameterInfo t5398_m28252_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t217_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28252_GM;
MethodInfo m28252_MI = 
{
	"Insert", NULL, &t5398_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5398_m28252_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28252_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5398_m28253_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28253_GM;
MethodInfo m28253_MI = 
{
	"RemoveAt", NULL, &t5398_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t5398_m28253_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28253_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t5398_m28249_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t217_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28249_GM;
MethodInfo m28249_MI = 
{
	"get_Item", NULL, &t5398_TI, &t217_0_0_0, RuntimeInvoker_t29_t44, t5398_m28249_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m28249_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t217_0_0_0;
static ParameterInfo t5398_m28250_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t217_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m28250_GM;
MethodInfo m28250_MI = 
{
	"set_Item", NULL, &t5398_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t5398_m28250_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m28250_GM};
static MethodInfo* t5398_MIs[] =
{
	&m28251_MI,
	&m28252_MI,
	&m28253_MI,
	&m28249_MI,
	&m28250_MI,
	NULL
};
static TypeInfo* t5398_ITIs[] = 
{
	&t603_TI,
	&t5397_TI,
	&t5399_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t5398_0_0_0;
extern Il2CppType t5398_1_0_0;
struct t5398;
extern Il2CppGenericClass t5398_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t5398_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t5398_MIs, t5398_PIs, NULL, NULL, NULL, NULL, NULL, &t5398_TI, t5398_ITIs, NULL, &t1908__CustomAttributeCache, &t5398_TI, &t5398_0_0_0, &t5398_1_0_0, NULL, &t5398_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#include "t2621.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t2621_TI;
#include "t2621MD.h"

#include "t2622.h"
extern TypeInfo t2622_TI;
#include "t2622MD.h"
extern MethodInfo m14106_MI;
extern MethodInfo m14108_MI;


// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Scrollbar>
extern Il2CppType t316_0_0_33;
FieldInfo t2621_f1_FieldInfo = 
{
	"m_Arg1", &t316_0_0_33, &t2621_TI, offsetof(t2621, f1), &EmptyCustomAttributesCache};
static FieldInfo* t2621_FIs[] =
{
	&t2621_f1_FieldInfo,
	NULL
};
extern Il2CppType t41_0_0_0;
extern Il2CppType t557_0_0_0;
extern Il2CppType t217_0_0_0;
static ParameterInfo t2621_m14104_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t41_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &t217_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14104_GM;
MethodInfo m14104_MI = 
{
	".ctor", (methodPointerType)&m10318_gshared, &t2621_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29_t29, t2621_m14104_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14104_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2621_m14105_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14105_GM;
MethodInfo m14105_MI = 
{
	"Invoke", (methodPointerType)&m10320_gshared, &t2621_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2621_m14105_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14105_GM};
static MethodInfo* t2621_MIs[] =
{
	&m14104_MI,
	&m14105_MI,
	NULL
};
extern MethodInfo m14105_MI;
extern MethodInfo m14109_MI;
static MethodInfo* t2621_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14105_MI,
	&m14109_MI,
};
extern Il2CppType t2623_0_0_0;
extern TypeInfo t2623_TI;
extern MethodInfo m21305_MI;
extern TypeInfo t217_TI;
extern MethodInfo m14111_MI;
extern TypeInfo t217_TI;
static Il2CppRGCTXData t2621_RGCTXData[8] = 
{
	&t2623_0_0_0/* Type Usage */,
	&t2623_TI/* Class Usage */,
	&m21305_MI/* Method Usage */,
	&t217_TI/* Class Usage */,
	&m14111_MI/* Method Usage */,
	&m14106_MI/* Method Usage */,
	&t217_TI/* Class Usage */,
	&m14108_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2621_0_0_0;
extern Il2CppType t2621_1_0_0;
struct t2621;
extern Il2CppGenericClass t2621_GC;
TypeInfo t2621_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "CachedInvokableCall`1", "UnityEngine.Events", t2621_MIs, NULL, t2621_FIs, NULL, &t2622_TI, NULL, NULL, &t2621_TI, NULL, t2621_VT, &EmptyCustomAttributesCache, &t2621_TI, &t2621_0_0_0, &t2621_1_0_0, NULL, &t2621_GC, NULL, NULL, NULL, t2621_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2621), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 2, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif

#include "t2623.h"
extern TypeInfo t2623_TI;
#include "t2623MD.h"
struct t556;
#define m21305(__this, p0, method) (void)m19555_gshared((t29 *)__this, (t29 *)p0, method)


// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Scrollbar>
extern Il2CppType t2623_0_0_1;
FieldInfo t2622_f0_FieldInfo = 
{
	"Delegate", &t2623_0_0_1, &t2622_TI, offsetof(t2622, f0), &EmptyCustomAttributesCache};
static FieldInfo* t2622_FIs[] =
{
	&t2622_f0_FieldInfo,
	NULL
};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2622_m14106_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14106_GM;
MethodInfo m14106_MI = 
{
	".ctor", (methodPointerType)&m10321_gshared, &t2622_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t29, t2622_m14106_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14106_GM};
extern Il2CppType t2623_0_0_0;
static ParameterInfo t2622_m14107_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &t2623_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14107_GM;
MethodInfo m14107_MI = 
{
	".ctor", (methodPointerType)&m10322_gshared, &t2622_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2622_m14107_ParameterInfos, &EmptyCustomAttributesCache, 6278, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14107_GM};
extern Il2CppType t316_0_0_0;
static ParameterInfo t2622_m14108_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &t316_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14108_GM;
MethodInfo m14108_MI = 
{
	"Invoke", (methodPointerType)&m10323_gshared, &t2622_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2622_m14108_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14108_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t2622_m14109_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14109_GM;
MethodInfo m14109_MI = 
{
	"Find", (methodPointerType)&m10324_gshared, &t2622_TI, &t40_0_0_0, RuntimeInvoker_t40_t29_t29, t2622_m14109_ParameterInfos, &EmptyCustomAttributesCache, 198, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14109_GM};
static MethodInfo* t2622_MIs[] =
{
	&m14106_MI,
	&m14107_MI,
	&m14108_MI,
	&m14109_MI,
	NULL
};
static MethodInfo* t2622_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1332_MI,
	&m14108_MI,
	&m14109_MI,
};
extern TypeInfo t2623_TI;
extern TypeInfo t217_TI;
static Il2CppRGCTXData t2622_RGCTXData[5] = 
{
	&t2623_0_0_0/* Type Usage */,
	&t2623_TI/* Class Usage */,
	&m21305_MI/* Method Usage */,
	&t217_TI/* Class Usage */,
	&m14111_MI/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2622_0_0_0;
extern Il2CppType t2622_1_0_0;
struct t2622;
extern Il2CppGenericClass t2622_GC;
TypeInfo t2622_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "InvokableCall`1", "UnityEngine.Events", t2622_MIs, NULL, t2622_FIs, NULL, &t556_TI, NULL, NULL, &t2622_TI, NULL, t2622_VT, &EmptyCustomAttributesCache, &t2622_TI, &t2622_0_0_0, &t2622_1_0_0, NULL, &t2622_GC, NULL, NULL, NULL, t2622_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2622), 0, -1, 0, 0, -1, 1048576, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 1, 0, 0, 6, 0, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Scrollbar>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t2623_m14110_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14110_GM;
MethodInfo m14110_MI = 
{
	".ctor", (methodPointerType)&m10325_gshared, &t2623_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t2623_m14110_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m14110_GM};
extern Il2CppType t217_0_0_0;
static ParameterInfo t2623_m14111_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t217_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14111_GM;
MethodInfo m14111_MI = 
{
	"Invoke", (methodPointerType)&m10326_gshared, &t2623_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2623_m14111_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14111_GM};
extern Il2CppType t217_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t2623_m14112_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t217_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14112_GM;
MethodInfo m14112_MI = 
{
	"BeginInvoke", (methodPointerType)&m10327_gshared, &t2623_TI, &t66_0_0_0, RuntimeInvoker_t29_t29_t29_t29, t2623_m14112_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14112_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t2623_m14113_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14113_GM;
MethodInfo m14113_MI = 
{
	"EndInvoke", (methodPointerType)&m10328_gshared, &t2623_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t2623_m14113_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14113_GM};
static MethodInfo* t2623_MIs[] =
{
	&m14110_MI,
	&m14111_MI,
	&m14112_MI,
	&m14113_MI,
	NULL
};
extern MethodInfo m14112_MI;
extern MethodInfo m14113_MI;
static MethodInfo* t2623_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14111_MI,
	&m14112_MI,
	&m14113_MI,
};
static Il2CppInterfaceOffsetPair t2623_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t2623_1_0_0;
struct t2623;
extern Il2CppGenericClass t2623_GC;
TypeInfo t2623_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t2623_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t2623_TI, NULL, t2623_VT, &EmptyCustomAttributesCache, &t2623_TI, &t2623_0_0_0, &t2623_1_0_0, t2623_IOs, &t2623_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t2623), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#include "t214.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t214_TI;
#include "t214MD.h"

#include "t395.h"
#include "t22.h"
#include "t2624.h"
extern TypeInfo t22_TI;
extern TypeInfo t2624_TI;
#include "t2624MD.h"
extern Il2CppType t22_0_0_0;
extern MethodInfo m14114_MI;
extern MethodInfo m14118_MI;
extern MethodInfo m14119_MI;


extern MethodInfo m1824_MI;
 void m1824 (t214 * __this, MethodInfo* method){
	{
		__this->f4 = ((t316*)SZArrayNew(InitializedTypeInfo(&t316_TI), 1));
		m2812(__this, &m2812_MI);
		return;
	}
}
extern MethodInfo m1843_MI;
 void m1843 (t214 * __this, t395 * p0, MethodInfo* method){
	{
		t556 * L_0 = m14114(NULL, p0, &m14114_MI);
		m2817(__this, L_0, &m2817_MI);
		return;
	}
}
extern MethodInfo m1842_MI;
 void m1842 (t214 * __this, t395 * p0, MethodInfo* method){
	{
		t29 * L_0 = m2953(p0, &m2953_MI);
		t557 * L_1 = m2951(p0, &m2951_MI);
		m2818(__this, L_0, L_1, &m2818_MI);
		return;
	}
}
extern MethodInfo m1825_MI;
 t557 * m1825 (t214 * __this, t7* p0, t29 * p1, MethodInfo* method){
	{
		t537* L_0 = ((t537*)SZArrayNew(InitializedTypeInfo(&t537_TI), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&t42_TI));
		t42 * L_1 = m1554(NULL, LoadTypeToken(&t22_0_0_0), &m1554_MI);
		ArrayElementTypeCheck (L_0, L_1);
		*((t42 **)(t42 **)SZArrayLdElema(L_0, 0)) = (t42 *)L_1;
		t557 * L_2 = m2820(NULL, p1, p0, L_0, &m2820_MI);
		return L_2;
	}
}
extern MethodInfo m1826_MI;
 t556 * m1826 (t214 * __this, t29 * p0, t557 * p1, MethodInfo* method){
	{
		t2624 * L_0 = (t2624 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2624_TI));
		m14118(L_0, p0, p1, &m14118_MI);
		return L_0;
	}
}
 t556 * m14114 (t29 * __this, t395 * p0, MethodInfo* method){
	{
		t2624 * L_0 = (t2624 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t2624_TI));
		m14119(L_0, p0, &m14119_MI);
		return L_0;
	}
}
extern MethodInfo m1832_MI;
 void m1832 (t214 * __this, float p0, MethodInfo* method){
	{
		t316* L_0 = (__this->f4);
		float L_1 = p0;
		t29 * L_2 = Box(InitializedTypeInfo(&t22_TI), &L_1);
		ArrayElementTypeCheck (L_0, L_2);
		*((t29 **)(t29 **)SZArrayLdElema(L_0, 0)) = (t29 *)L_2;
		t316* L_3 = (__this->f4);
		m2819(__this, L_3, &m2819_MI);
		return;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`1<System.Single>
extern Il2CppType t316_0_0_33;
FieldInfo t214_f4_FieldInfo = 
{
	"m_InvokeArray", &t316_0_0_33, &t214_TI, offsetof(t214, f4), &EmptyCustomAttributesCache};
static FieldInfo* t214_FIs[] =
{
	&t214_f4_FieldInfo,
	NULL
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1824_GM;
MethodInfo m1824_MI = 
{
	".ctor", (methodPointerType)&m1824, &t214_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 6276, 0, 255, 0, false, true, 0, NULL, (methodPointerType)NULL, &m1824_GM};
extern Il2CppType t395_0_0_0;
extern Il2CppType t395_0_0_0;
static ParameterInfo t214_m1843_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t395_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1843_GM;
MethodInfo m1843_MI = 
{
	"AddListener", (methodPointerType)&m1843, &t214_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t214_m1843_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1843_GM};
extern Il2CppType t395_0_0_0;
static ParameterInfo t214_m1842_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &t395_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1842_GM;
MethodInfo m1842_MI = 
{
	"RemoveListener", (methodPointerType)&m1842, &t214_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t214_m1842_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1842_GM};
extern Il2CppType t7_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t214_m1825_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &t7_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t557_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1825_GM;
MethodInfo m1825_MI = 
{
	"FindMethod_Impl", (methodPointerType)&m1825, &t214_TI, &t557_0_0_0, RuntimeInvoker_t29_t29_t29, t214_m1825_ParameterInfos, &EmptyCustomAttributesCache, 196, 0, 6, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1825_GM};
extern Il2CppType t29_0_0_0;
extern Il2CppType t557_0_0_0;
static ParameterInfo t214_m1826_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &t557_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1826_GM;
MethodInfo m1826_MI = 
{
	"GetDelegate", (methodPointerType)&m1826, &t214_TI, &t556_0_0_0, RuntimeInvoker_t29_t29_t29, t214_m1826_ParameterInfos, &EmptyCustomAttributesCache, 195, 0, 7, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1826_GM};
extern Il2CppType t395_0_0_0;
static ParameterInfo t214_m14114_ParameterInfos[] = 
{
	{"action", 0, 134217728, &EmptyCustomAttributesCache, &t395_0_0_0},
};
extern Il2CppType t556_0_0_0;
extern void* RuntimeInvoker_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14114_GM;
MethodInfo m14114_MI = 
{
	"GetDelegate", (methodPointerType)&m14114, &t214_TI, &t556_0_0_0, RuntimeInvoker_t29_t29, t214_m14114_ParameterInfos, &EmptyCustomAttributesCache, 145, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14114_GM};
extern Il2CppType t22_0_0_0;
static ParameterInfo t214_m1832_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1832_GM;
MethodInfo m1832_MI = 
{
	"Invoke", (methodPointerType)&m1832, &t214_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t214_m1832_ParameterInfos, &EmptyCustomAttributesCache, 134, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m1832_GM};
static MethodInfo* t214_MIs[] =
{
	&m1824_MI,
	&m1843_MI,
	&m1842_MI,
	&m1825_MI,
	&m1826_MI,
	&m14114_MI,
	&m1832_MI,
	NULL
};
static MethodInfo* t214_VT[] =
{
	&m1321_MI,
	&m46_MI,
	&m1322_MI,
	&m1323_MI,
	&m1324_MI,
	&m1325_MI,
	&m1825_MI,
	&m1826_MI,
};
static Il2CppInterfaceOffsetPair t214_IOs[] = 
{
	{ &t301_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t214_0_0_0;
extern Il2CppType t214_1_0_0;
struct t214;
extern Il2CppGenericClass t214_GC;
TypeInfo t214_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityEvent`1", "UnityEngine.Events", t214_MIs, NULL, t214_FIs, NULL, &t566_TI, NULL, NULL, &t214_TI, NULL, t214_VT, &EmptyCustomAttributesCache, &t214_TI, &t214_0_0_0, &t214_1_0_0, t214_IOs, &t214_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t214), 0, -1, 0, 0, -1, 1056897, 0, false, false, false, false, true, false, false, false, false, false, false, false, 7, 0, 1, 0, 0, 8, 0, 1};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t395_TI;
#include "t395MD.h"



extern MethodInfo m1841_MI;
 void m1841 (t395 * __this, t29 * p0, t35 p1, MethodInfo* method){
	__this->f0 = (methodPointerType)((MethodInfo*)p1.f0)->method;
	__this->f3 = p1;
	__this->f2 = p0;
}
extern MethodInfo m14115_MI;
 void m14115 (t395 * __this, float p0, MethodInfo* method){
	if(__this->f9 != NULL)
	{
		m14115((t395 *)__this->f9,p0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->f3.f0));
	if (__this->f2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (t29 *, t29 * __this, float p0, MethodInfo* method);
		((FunctionPointerType)__this->f0)(NULL,__this->f2,p0,(MethodInfo*)(__this->f3.f0));
	}
	typedef void (*FunctionPointerType) (t29 * __this, float p0, MethodInfo* method);
	((FunctionPointerType)__this->f0)(__this->f2,p0,(MethodInfo*)(__this->f3.f0));
}
extern MethodInfo m14116_MI;
 t29 * m14116 (t395 * __this, float p0, t67 * p1, t29 * p2, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&t22_TI), &p0);
	return (t29 *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)p1, (Il2CppObject*)p2);
}
extern MethodInfo m14117_MI;
 void m14117 (t395 * __this, t29 * p0, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) p0, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction`1<System.Single>
extern Il2CppType t29_0_0_0;
extern Il2CppType t35_0_0_0;
static ParameterInfo t395_m1841_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &t35_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t35 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m1841_GM;
MethodInfo m1841_MI = 
{
	".ctor", (methodPointerType)&m1841, &t395_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t35, t395_m1841_ParameterInfos, &EmptyCustomAttributesCache, 6278, 3, 255, 2, false, true, 0, NULL, (methodPointerType)NULL, &m1841_GM};
extern Il2CppType t22_0_0_0;
static ParameterInfo t395_m14115_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t22_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t22 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14115_GM;
MethodInfo m14115_MI = 
{
	"Invoke", (methodPointerType)&m14115, &t395_TI, &t21_0_0_0, RuntimeInvoker_t21_t22, t395_m14115_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 10, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14115_GM};
extern Il2CppType t22_0_0_0;
extern Il2CppType t67_0_0_0;
extern Il2CppType t29_0_0_0;
static ParameterInfo t395_m14116_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &t22_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &t67_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &t29_0_0_0},
};
extern Il2CppType t66_0_0_0;
extern void* RuntimeInvoker_t29_t22_t29_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14116_GM;
MethodInfo m14116_MI = 
{
	"BeginInvoke", (methodPointerType)&m14116, &t395_TI, &t66_0_0_0, RuntimeInvoker_t29_t22_t29_t29, t395_m14116_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 11, 3, false, true, 0, NULL, (methodPointerType)NULL, &m14116_GM};
extern Il2CppType t66_0_0_0;
static ParameterInfo t395_m14117_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &t66_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m14117_GM;
MethodInfo m14117_MI = 
{
	"EndInvoke", (methodPointerType)&m14117, &t395_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t395_m14117_ParameterInfos, &EmptyCustomAttributesCache, 454, 3, 12, 1, false, true, 0, NULL, (methodPointerType)NULL, &m14117_GM};
static MethodInfo* t395_MIs[] =
{
	&m1841_MI,
	&m14115_MI,
	&m14116_MI,
	&m14117_MI,
	NULL
};
static MethodInfo* t395_VT[] =
{
	&m1699_MI,
	&m46_MI,
	&m1700_MI,
	&m1332_MI,
	&m1701_MI,
	&m1702_MI,
	&m1701_MI,
	&m1703_MI,
	&m1704_MI,
	&m1705_MI,
	&m14115_MI,
	&m14116_MI,
	&m14117_MI,
};
static Il2CppInterfaceOffsetPair t395_IOs[] = 
{
	{ &t373_TI, 4},
	{ &t374_TI, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType t395_1_0_0;
struct t395;
extern Il2CppGenericClass t395_GC;
TypeInfo t395_TI = 
{
	&g_UnityEngine_dll_Image, NULL, "UnityAction`1", "UnityEngine.Events", t395_MIs, NULL, NULL, NULL, &t195_TI, NULL, NULL, &t395_TI, NULL, t395_VT, &EmptyCustomAttributesCache, &t395_TI, &t395_0_0_0, &t395_1_0_0, t395_IOs, &t395_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t395), 0, -1, 0, 0, -1, 257, 0, false, false, false, false, true, false, false, false, false, false, false, false, 4, 0, 0, 0, 0, 13, 0, 2};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
