﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1146;
struct t42;
struct t1145;
struct t557;
struct t638;
struct t29;
struct t316;
struct t631;
struct t633;
struct t537;
#include "t1382.h"
#include "t1149.h"
#include "t630.h"

 void m7711 (t1146 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7712 (t1146 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m7713 (t1146 * __this, t29 * p0, t316* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7714 (t1146 * __this, t29 * p0, t29 * p1, t316* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7715 (t1146 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m7716 (t1146 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
