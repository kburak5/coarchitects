﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t358;
struct t2;
struct t24;
struct t25;
struct t3;
#include "t17.h"
#include "t164.h"
#include "t23.h"
#include "t329.h"

 void m2724 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1628 (t29 * __this, t2 * p0, t17  p1, t24 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2725 (t29 * __this, t2 * p0, t17 * p1, t24 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1589 (t29 * __this, t17  p0, t25 * p1, t3 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2726 (t29 * __this, t17  p0, t25 * p1, t3 * p2, t17 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2727 (t29 * __this, t17 * p0, t25 * p1, t3 * p2, t17 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t164  m1590 (t29 * __this, t2 * p0, t3 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2728 (t29 * __this, t2 * p0, t17  p1, t24 * p2, t23 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1677 (t29 * __this, t2 * p0, t17  p1, t24 * p2, t17 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t329  m2729 (t29 * __this, t24 * p0, t17  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1836 (t29 * __this, t2 * p0, int32_t p1, bool p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1835 (t29 * __this, t2 * p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m2730 (t29 * __this, t17  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
