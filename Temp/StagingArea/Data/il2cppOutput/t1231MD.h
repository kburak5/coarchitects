﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1231;
struct t1219;
struct t7;
struct t781;

 void m6513 (t1231 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6514 (t1231 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6515 (t1231 * __this, t781* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6516 (t1231 * __this, t1219 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1219 * m6517 (t1231 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6518 (t1231 * __this, t1219 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6519 (t1231 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
