﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t198;
struct t29;

 void m568 (t198 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m569 (t198 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m570 (t198 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m571 (t198 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m572 (t198 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m573 (t198 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
