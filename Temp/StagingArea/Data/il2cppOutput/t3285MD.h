﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3285;
struct t29;
struct t7;

#include "t2195MD.h"
#define m18297(__this, method) (void)m10745_gshared((t2195 *)__this, method)
#define m18298(__this, method) (void)m10746_gshared((t29 *)__this, method)
#define m18299(__this, p0, p1, method) (int32_t)m10747_gshared((t2195 *)__this, (t29 *)p0, (t29 *)p1, method)
#define m18300(__this, method) (t3285 *)m10748_gshared((t29 *)__this, method)
