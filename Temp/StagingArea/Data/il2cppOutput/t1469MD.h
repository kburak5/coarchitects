﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1469;
struct t774;
struct t7;
struct t1466;
struct t42;

 void m7974 (t1469 * __this, t7* p0, t1466 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t774 * m7975 (t1469 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7976 (t1469 * __this, t774 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1466 * m7977 (t1469 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7978 (t1469 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
