﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1333;
struct t1034;
struct t943;
struct t200;
struct t7;

 void m7249 (t1333 * __this, t1034 * p0, t943 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7250 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7251 (t1333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7252 (t1333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7253 (t1333 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7254 (t1333 * __this, uint16_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7255 (t1333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7256 (t1333 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
