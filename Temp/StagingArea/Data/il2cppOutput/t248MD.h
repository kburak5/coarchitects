﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t248;
struct t2;
#include "t247.h"
#include "t17.h"

 void m1022 (t248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1023 (t248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1024 (t248 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1025 (t248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1026 (t248 * __this, float p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2 * m1027 (t248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1028 (t248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1029 (t248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1030 (t248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1031 (t248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 float m1032 (t248 * __this, float p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t17  m1033 (t248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1034 (t248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1035 (t248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1036 (t248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
