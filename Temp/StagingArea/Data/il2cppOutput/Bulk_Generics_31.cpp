﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo t4827_TI;


#include "t20.h"

// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>
extern MethodInfo m32639_MI;
static PropertyInfo t4827____Current_PropertyInfo = 
{
	&t4827_TI, "Current", &m32639_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4827_PIs[] =
{
	&t4827____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2008_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32639_GM;
MethodInfo m32639_MI = 
{
	"get_Current", NULL, &t4827_TI, &t2008_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32639_GM};
static MethodInfo* t4827_MIs[] =
{
	&m32639_MI,
	NULL
};
extern TypeInfo t136_TI;
extern TypeInfo t324_TI;
static TypeInfo* t4827_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4827_0_0_0;
extern Il2CppType t4827_1_0_0;
struct t4827;
extern Il2CppGenericClass t4827_GC;
TypeInfo t4827_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4827_MIs, t4827_PIs, NULL, NULL, NULL, NULL, NULL, &t4827_TI, t4827_ITIs, NULL, &EmptyCustomAttributesCache, &t4827_TI, &t4827_0_0_0, &t4827_1_0_0, NULL, &t4827_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3364.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3364_TI;
#include "t3364MD.h"

#include "t29.h"
#include "t44.h"
#include "t7.h"
#include "t914.h"
#include "t21.h"
#include "t40.h"
extern TypeInfo t2008_TI;
extern TypeInfo t914_TI;
#include "t914MD.h"
#include "t20MD.h"
extern MethodInfo m18696_MI;
extern MethodInfo m3964_MI;
extern MethodInfo m3969_MI;
extern MethodInfo m24971_MI;
struct t20;
#include "t915.h"
struct t20;
 t29 * m19490_gshared (t20 * __this, int32_t p0, MethodInfo* method);
#define m19490(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)
#define m24971(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>
extern Il2CppType t20_0_0_1;
FieldInfo t3364_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3364_TI, offsetof(t3364, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3364_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3364_TI, offsetof(t3364, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3364_FIs[] =
{
	&t3364_f0_FieldInfo,
	&t3364_f1_FieldInfo,
	NULL
};
extern MethodInfo m18693_MI;
static PropertyInfo t3364____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3364_TI, "System.Collections.IEnumerator.Current", &m18693_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3364____Current_PropertyInfo = 
{
	&t3364_TI, "Current", &m18696_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3364_PIs[] =
{
	&t3364____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3364____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
extern Il2CppType t20_0_0_0;
static ParameterInfo t3364_m18692_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18692_GM;
MethodInfo m18692_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3364_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3364_m18692_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18692_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18693_GM;
MethodInfo m18693_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3364_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18693_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18694_GM;
MethodInfo m18694_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3364_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18694_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18695_GM;
MethodInfo m18695_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3364_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18695_GM};
extern Il2CppType t2008_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18696_GM;
MethodInfo m18696_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3364_TI, &t2008_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18696_GM};
static MethodInfo* t3364_MIs[] =
{
	&m18692_MI,
	&m18693_MI,
	&m18694_MI,
	&m18695_MI,
	&m18696_MI,
	NULL
};
extern MethodInfo m1388_MI;
extern MethodInfo m46_MI;
extern MethodInfo m1389_MI;
extern MethodInfo m1500_MI;
extern MethodInfo m18695_MI;
extern MethodInfo m18694_MI;
static MethodInfo* t3364_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18693_MI,
	&m18695_MI,
	&m18694_MI,
	&m18696_MI,
};
static TypeInfo* t3364_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4827_TI,
};
static Il2CppInterfaceOffsetPair t3364_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4827_TI, 7},
};
extern TypeInfo t2008_TI;
static Il2CppRGCTXData t3364_RGCTXData[3] = 
{
	&m18696_MI/* Method Usage */,
	&t2008_TI/* Class Usage */,
	&m24971_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3364_0_0_0;
extern Il2CppType t3364_1_0_0;
extern TypeInfo t110_TI;
extern Il2CppGenericClass t3364_GC;
extern TypeInfo t20_TI;
TypeInfo t3364_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3364_MIs, t3364_PIs, t3364_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3364_TI, t3364_ITIs, t3364_VT, &EmptyCustomAttributesCache, &t3364_TI, &t3364_0_0_0, &t3364_1_0_0, t3364_IOs, &t3364_GC, NULL, NULL, NULL, t3364_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3364)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6286_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorBuilder>
extern MethodInfo m32640_MI;
extern MethodInfo m32641_MI;
static PropertyInfo t6286____Item_PropertyInfo = 
{
	&t6286_TI, "Item", &m32640_MI, &m32641_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6286_PIs[] =
{
	&t6286____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2008_0_0_0;
extern Il2CppType t2008_0_0_0;
static ParameterInfo t6286_m32642_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2008_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32642_GM;
MethodInfo m32642_MI = 
{
	"IndexOf", NULL, &t6286_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6286_m32642_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32642_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t44_0_0_0;
extern Il2CppType t2008_0_0_0;
static ParameterInfo t6286_m32643_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2008_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32643_GM;
MethodInfo m32643_MI = 
{
	"Insert", NULL, &t6286_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6286_m32643_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32643_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6286_m32644_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32644_GM;
MethodInfo m32644_MI = 
{
	"RemoveAt", NULL, &t6286_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6286_m32644_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32644_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6286_m32640_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2008_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32640_GM;
MethodInfo m32640_MI = 
{
	"get_Item", NULL, &t6286_TI, &t2008_0_0_0, RuntimeInvoker_t29_t44, t6286_m32640_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32640_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2008_0_0_0;
static ParameterInfo t6286_m32641_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2008_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32641_GM;
MethodInfo m32641_MI = 
{
	"set_Item", NULL, &t6286_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6286_m32641_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32641_GM};
static MethodInfo* t6286_MIs[] =
{
	&m32642_MI,
	&m32643_MI,
	&m32644_MI,
	&m32640_MI,
	&m32641_MI,
	NULL
};
extern TypeInfo t603_TI;
extern TypeInfo t6285_TI;
extern TypeInfo t6287_TI;
static TypeInfo* t6286_ITIs[] = 
{
	&t603_TI,
	&t6285_TI,
	&t6287_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6286_0_0_0;
extern Il2CppType t6286_1_0_0;
struct t6286;
extern Il2CppGenericClass t6286_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6286_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6286_MIs, t6286_PIs, NULL, NULL, NULL, NULL, NULL, &t6286_TI, t6286_ITIs, NULL, &t1908__CustomAttributeCache, &t6286_TI, &t6286_0_0_0, &t6286_1_0_0, NULL, &t6286_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4829_TI;

#include "t1345.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.FieldBuilder>
extern MethodInfo m32645_MI;
static PropertyInfo t4829____Current_PropertyInfo = 
{
	&t4829_TI, "Current", &m32645_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4829_PIs[] =
{
	&t4829____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1345_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32645_GM;
MethodInfo m32645_MI = 
{
	"get_Current", NULL, &t4829_TI, &t1345_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32645_GM};
static MethodInfo* t4829_MIs[] =
{
	&m32645_MI,
	NULL
};
static TypeInfo* t4829_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4829_0_0_0;
extern Il2CppType t4829_1_0_0;
struct t4829;
extern Il2CppGenericClass t4829_GC;
TypeInfo t4829_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4829_MIs, t4829_PIs, NULL, NULL, NULL, NULL, NULL, &t4829_TI, t4829_ITIs, NULL, &EmptyCustomAttributesCache, &t4829_TI, &t4829_0_0_0, &t4829_1_0_0, NULL, &t4829_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3365.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3365_TI;
#include "t3365MD.h"

extern TypeInfo t1345_TI;
extern MethodInfo m18701_MI;
extern MethodInfo m24982_MI;
struct t20;
#define m24982(__this, p0, method) (t1345 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>
extern Il2CppType t20_0_0_1;
FieldInfo t3365_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3365_TI, offsetof(t3365, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3365_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3365_TI, offsetof(t3365, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3365_FIs[] =
{
	&t3365_f0_FieldInfo,
	&t3365_f1_FieldInfo,
	NULL
};
extern MethodInfo m18698_MI;
static PropertyInfo t3365____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3365_TI, "System.Collections.IEnumerator.Current", &m18698_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3365____Current_PropertyInfo = 
{
	&t3365_TI, "Current", &m18701_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3365_PIs[] =
{
	&t3365____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3365____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3365_m18697_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18697_GM;
MethodInfo m18697_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3365_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3365_m18697_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18697_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18698_GM;
MethodInfo m18698_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3365_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18698_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18699_GM;
MethodInfo m18699_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3365_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18699_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18700_GM;
MethodInfo m18700_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3365_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18700_GM};
extern Il2CppType t1345_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18701_GM;
MethodInfo m18701_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3365_TI, &t1345_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18701_GM};
static MethodInfo* t3365_MIs[] =
{
	&m18697_MI,
	&m18698_MI,
	&m18699_MI,
	&m18700_MI,
	&m18701_MI,
	NULL
};
extern MethodInfo m18700_MI;
extern MethodInfo m18699_MI;
static MethodInfo* t3365_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18698_MI,
	&m18700_MI,
	&m18699_MI,
	&m18701_MI,
};
static TypeInfo* t3365_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4829_TI,
};
static Il2CppInterfaceOffsetPair t3365_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4829_TI, 7},
};
extern TypeInfo t1345_TI;
static Il2CppRGCTXData t3365_RGCTXData[3] = 
{
	&m18701_MI/* Method Usage */,
	&t1345_TI/* Class Usage */,
	&m24982_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3365_0_0_0;
extern Il2CppType t3365_1_0_0;
extern Il2CppGenericClass t3365_GC;
TypeInfo t3365_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3365_MIs, t3365_PIs, t3365_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3365_TI, t3365_ITIs, t3365_VT, &EmptyCustomAttributesCache, &t3365_TI, &t3365_0_0_0, &t3365_1_0_0, t3365_IOs, &t3365_GC, NULL, NULL, NULL, t3365_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3365)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6288_TI;

#include "mscorlib_ArrayTypes.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>
extern MethodInfo m32646_MI;
static PropertyInfo t6288____Count_PropertyInfo = 
{
	&t6288_TI, "Count", &m32646_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32647_MI;
static PropertyInfo t6288____IsReadOnly_PropertyInfo = 
{
	&t6288_TI, "IsReadOnly", &m32647_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6288_PIs[] =
{
	&t6288____Count_PropertyInfo,
	&t6288____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32646_GM;
MethodInfo m32646_MI = 
{
	"get_Count", NULL, &t6288_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32646_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32647_GM;
MethodInfo m32647_MI = 
{
	"get_IsReadOnly", NULL, &t6288_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32647_GM};
extern Il2CppType t1345_0_0_0;
extern Il2CppType t1345_0_0_0;
static ParameterInfo t6288_m32648_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1345_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32648_GM;
MethodInfo m32648_MI = 
{
	"Add", NULL, &t6288_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6288_m32648_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32648_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32649_GM;
MethodInfo m32649_MI = 
{
	"Clear", NULL, &t6288_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32649_GM};
extern Il2CppType t1345_0_0_0;
static ParameterInfo t6288_m32650_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1345_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32650_GM;
MethodInfo m32650_MI = 
{
	"Contains", NULL, &t6288_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6288_m32650_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32650_GM};
extern Il2CppType t1356_0_0_0;
extern Il2CppType t1356_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6288_m32651_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1356_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32651_GM;
MethodInfo m32651_MI = 
{
	"CopyTo", NULL, &t6288_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6288_m32651_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32651_GM};
extern Il2CppType t1345_0_0_0;
static ParameterInfo t6288_m32652_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1345_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32652_GM;
MethodInfo m32652_MI = 
{
	"Remove", NULL, &t6288_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6288_m32652_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32652_GM};
static MethodInfo* t6288_MIs[] =
{
	&m32646_MI,
	&m32647_MI,
	&m32648_MI,
	&m32649_MI,
	&m32650_MI,
	&m32651_MI,
	&m32652_MI,
	NULL
};
extern TypeInfo t6290_TI;
static TypeInfo* t6288_ITIs[] = 
{
	&t603_TI,
	&t6290_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6288_0_0_0;
extern Il2CppType t6288_1_0_0;
struct t6288;
extern Il2CppGenericClass t6288_GC;
TypeInfo t6288_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6288_MIs, t6288_PIs, NULL, NULL, NULL, NULL, NULL, &t6288_TI, t6288_ITIs, NULL, &EmptyCustomAttributesCache, &t6288_TI, &t6288_0_0_0, &t6288_1_0_0, NULL, &t6288_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.FieldBuilder>
extern Il2CppType t4829_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32653_GM;
MethodInfo m32653_MI = 
{
	"GetEnumerator", NULL, &t6290_TI, &t4829_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32653_GM};
static MethodInfo* t6290_MIs[] =
{
	&m32653_MI,
	NULL
};
static TypeInfo* t6290_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6290_0_0_0;
extern Il2CppType t6290_1_0_0;
struct t6290;
extern Il2CppGenericClass t6290_GC;
TypeInfo t6290_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6290_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6290_TI, t6290_ITIs, NULL, &EmptyCustomAttributesCache, &t6290_TI, &t6290_0_0_0, &t6290_1_0_0, NULL, &t6290_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6289_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.FieldBuilder>
extern MethodInfo m32654_MI;
extern MethodInfo m32655_MI;
static PropertyInfo t6289____Item_PropertyInfo = 
{
	&t6289_TI, "Item", &m32654_MI, &m32655_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6289_PIs[] =
{
	&t6289____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1345_0_0_0;
static ParameterInfo t6289_m32656_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1345_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32656_GM;
MethodInfo m32656_MI = 
{
	"IndexOf", NULL, &t6289_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6289_m32656_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32656_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1345_0_0_0;
static ParameterInfo t6289_m32657_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1345_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32657_GM;
MethodInfo m32657_MI = 
{
	"Insert", NULL, &t6289_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6289_m32657_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32657_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6289_m32658_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32658_GM;
MethodInfo m32658_MI = 
{
	"RemoveAt", NULL, &t6289_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6289_m32658_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32658_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6289_m32654_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1345_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32654_GM;
MethodInfo m32654_MI = 
{
	"get_Item", NULL, &t6289_TI, &t1345_0_0_0, RuntimeInvoker_t29_t44, t6289_m32654_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32654_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1345_0_0_0;
static ParameterInfo t6289_m32655_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1345_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32655_GM;
MethodInfo m32655_MI = 
{
	"set_Item", NULL, &t6289_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6289_m32655_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32655_GM};
static MethodInfo* t6289_MIs[] =
{
	&m32656_MI,
	&m32657_MI,
	&m32658_MI,
	&m32654_MI,
	&m32655_MI,
	NULL
};
static TypeInfo* t6289_ITIs[] = 
{
	&t603_TI,
	&t6288_TI,
	&t6290_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6289_0_0_0;
extern Il2CppType t6289_1_0_0;
struct t6289;
extern Il2CppGenericClass t6289_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6289_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6289_MIs, t6289_PIs, NULL, NULL, NULL, NULL, NULL, &t6289_TI, t6289_ITIs, NULL, &t1908__CustomAttributeCache, &t6289_TI, &t6289_0_0_0, &t6289_1_0_0, NULL, &t6289_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6291_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>
extern MethodInfo m32659_MI;
static PropertyInfo t6291____Count_PropertyInfo = 
{
	&t6291_TI, "Count", &m32659_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32660_MI;
static PropertyInfo t6291____IsReadOnly_PropertyInfo = 
{
	&t6291_TI, "IsReadOnly", &m32660_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6291_PIs[] =
{
	&t6291____Count_PropertyInfo,
	&t6291____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32659_GM;
MethodInfo m32659_MI = 
{
	"get_Count", NULL, &t6291_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32659_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32660_GM;
MethodInfo m32660_MI = 
{
	"get_IsReadOnly", NULL, &t6291_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32660_GM};
extern Il2CppType t2012_0_0_0;
extern Il2CppType t2012_0_0_0;
static ParameterInfo t6291_m32661_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2012_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32661_GM;
MethodInfo m32661_MI = 
{
	"Add", NULL, &t6291_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6291_m32661_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32661_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32662_GM;
MethodInfo m32662_MI = 
{
	"Clear", NULL, &t6291_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32662_GM};
extern Il2CppType t2012_0_0_0;
static ParameterInfo t6291_m32663_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2012_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32663_GM;
MethodInfo m32663_MI = 
{
	"Contains", NULL, &t6291_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6291_m32663_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32663_GM};
extern Il2CppType t3624_0_0_0;
extern Il2CppType t3624_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6291_m32664_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3624_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32664_GM;
MethodInfo m32664_MI = 
{
	"CopyTo", NULL, &t6291_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6291_m32664_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32664_GM};
extern Il2CppType t2012_0_0_0;
static ParameterInfo t6291_m32665_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2012_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32665_GM;
MethodInfo m32665_MI = 
{
	"Remove", NULL, &t6291_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6291_m32665_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32665_GM};
static MethodInfo* t6291_MIs[] =
{
	&m32659_MI,
	&m32660_MI,
	&m32661_MI,
	&m32662_MI,
	&m32663_MI,
	&m32664_MI,
	&m32665_MI,
	NULL
};
extern TypeInfo t6293_TI;
static TypeInfo* t6291_ITIs[] = 
{
	&t603_TI,
	&t6293_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6291_0_0_0;
extern Il2CppType t6291_1_0_0;
struct t6291;
extern Il2CppGenericClass t6291_GC;
TypeInfo t6291_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6291_MIs, t6291_PIs, NULL, NULL, NULL, NULL, NULL, &t6291_TI, t6291_ITIs, NULL, &EmptyCustomAttributesCache, &t6291_TI, &t6291_0_0_0, &t6291_1_0_0, NULL, &t6291_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._FieldBuilder>
extern Il2CppType t4831_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32666_GM;
MethodInfo m32666_MI = 
{
	"GetEnumerator", NULL, &t6293_TI, &t4831_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32666_GM};
static MethodInfo* t6293_MIs[] =
{
	&m32666_MI,
	NULL
};
static TypeInfo* t6293_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6293_0_0_0;
extern Il2CppType t6293_1_0_0;
struct t6293;
extern Il2CppGenericClass t6293_GC;
TypeInfo t6293_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6293_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6293_TI, t6293_ITIs, NULL, &EmptyCustomAttributesCache, &t6293_TI, &t6293_0_0_0, &t6293_1_0_0, NULL, &t6293_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4831_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._FieldBuilder>
extern MethodInfo m32667_MI;
static PropertyInfo t4831____Current_PropertyInfo = 
{
	&t4831_TI, "Current", &m32667_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4831_PIs[] =
{
	&t4831____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2012_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32667_GM;
MethodInfo m32667_MI = 
{
	"get_Current", NULL, &t4831_TI, &t2012_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32667_GM};
static MethodInfo* t4831_MIs[] =
{
	&m32667_MI,
	NULL
};
static TypeInfo* t4831_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4831_0_0_0;
extern Il2CppType t4831_1_0_0;
struct t4831;
extern Il2CppGenericClass t4831_GC;
TypeInfo t4831_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4831_MIs, t4831_PIs, NULL, NULL, NULL, NULL, NULL, &t4831_TI, t4831_ITIs, NULL, &EmptyCustomAttributesCache, &t4831_TI, &t4831_0_0_0, &t4831_1_0_0, NULL, &t4831_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3366.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3366_TI;
#include "t3366MD.h"

extern TypeInfo t2012_TI;
extern MethodInfo m18706_MI;
extern MethodInfo m24993_MI;
struct t20;
#define m24993(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>
extern Il2CppType t20_0_0_1;
FieldInfo t3366_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3366_TI, offsetof(t3366, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3366_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3366_TI, offsetof(t3366, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3366_FIs[] =
{
	&t3366_f0_FieldInfo,
	&t3366_f1_FieldInfo,
	NULL
};
extern MethodInfo m18703_MI;
static PropertyInfo t3366____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3366_TI, "System.Collections.IEnumerator.Current", &m18703_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3366____Current_PropertyInfo = 
{
	&t3366_TI, "Current", &m18706_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3366_PIs[] =
{
	&t3366____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3366____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3366_m18702_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18702_GM;
MethodInfo m18702_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3366_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3366_m18702_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18702_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18703_GM;
MethodInfo m18703_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3366_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18703_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18704_GM;
MethodInfo m18704_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3366_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18704_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18705_GM;
MethodInfo m18705_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3366_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18705_GM};
extern Il2CppType t2012_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18706_GM;
MethodInfo m18706_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3366_TI, &t2012_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18706_GM};
static MethodInfo* t3366_MIs[] =
{
	&m18702_MI,
	&m18703_MI,
	&m18704_MI,
	&m18705_MI,
	&m18706_MI,
	NULL
};
extern MethodInfo m18705_MI;
extern MethodInfo m18704_MI;
static MethodInfo* t3366_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18703_MI,
	&m18705_MI,
	&m18704_MI,
	&m18706_MI,
};
static TypeInfo* t3366_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4831_TI,
};
static Il2CppInterfaceOffsetPair t3366_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4831_TI, 7},
};
extern TypeInfo t2012_TI;
static Il2CppRGCTXData t3366_RGCTXData[3] = 
{
	&m18706_MI/* Method Usage */,
	&t2012_TI/* Class Usage */,
	&m24993_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3366_0_0_0;
extern Il2CppType t3366_1_0_0;
extern Il2CppGenericClass t3366_GC;
TypeInfo t3366_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3366_MIs, t3366_PIs, t3366_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3366_TI, t3366_ITIs, t3366_VT, &EmptyCustomAttributesCache, &t3366_TI, &t3366_0_0_0, &t3366_1_0_0, t3366_IOs, &t3366_GC, NULL, NULL, NULL, t3366_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3366)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6292_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldBuilder>
extern MethodInfo m32668_MI;
extern MethodInfo m32669_MI;
static PropertyInfo t6292____Item_PropertyInfo = 
{
	&t6292_TI, "Item", &m32668_MI, &m32669_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6292_PIs[] =
{
	&t6292____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2012_0_0_0;
static ParameterInfo t6292_m32670_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2012_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32670_GM;
MethodInfo m32670_MI = 
{
	"IndexOf", NULL, &t6292_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6292_m32670_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32670_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2012_0_0_0;
static ParameterInfo t6292_m32671_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2012_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32671_GM;
MethodInfo m32671_MI = 
{
	"Insert", NULL, &t6292_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6292_m32671_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32671_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6292_m32672_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32672_GM;
MethodInfo m32672_MI = 
{
	"RemoveAt", NULL, &t6292_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6292_m32672_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32672_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6292_m32668_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2012_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32668_GM;
MethodInfo m32668_MI = 
{
	"get_Item", NULL, &t6292_TI, &t2012_0_0_0, RuntimeInvoker_t29_t44, t6292_m32668_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32668_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2012_0_0_0;
static ParameterInfo t6292_m32669_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2012_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32669_GM;
MethodInfo m32669_MI = 
{
	"set_Item", NULL, &t6292_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6292_m32669_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32669_GM};
static MethodInfo* t6292_MIs[] =
{
	&m32670_MI,
	&m32671_MI,
	&m32672_MI,
	&m32668_MI,
	&m32669_MI,
	NULL
};
static TypeInfo* t6292_ITIs[] = 
{
	&t603_TI,
	&t6291_TI,
	&t6293_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6292_0_0_0;
extern Il2CppType t6292_1_0_0;
struct t6292;
extern Il2CppGenericClass t6292_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6292_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6292_MIs, t6292_PIs, NULL, NULL, NULL, NULL, NULL, &t6292_TI, t6292_ITIs, NULL, &t1908__CustomAttributeCache, &t6292_TI, &t6292_0_0_0, &t6292_1_0_0, NULL, &t6292_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6294_TI;

#include "t1144.h"


// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>
extern MethodInfo m32673_MI;
static PropertyInfo t6294____Count_PropertyInfo = 
{
	&t6294_TI, "Count", &m32673_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32674_MI;
static PropertyInfo t6294____IsReadOnly_PropertyInfo = 
{
	&t6294_TI, "IsReadOnly", &m32674_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6294_PIs[] =
{
	&t6294____Count_PropertyInfo,
	&t6294____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32673_GM;
MethodInfo m32673_MI = 
{
	"get_Count", NULL, &t6294_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32673_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32674_GM;
MethodInfo m32674_MI = 
{
	"get_IsReadOnly", NULL, &t6294_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32674_GM};
extern Il2CppType t1144_0_0_0;
extern Il2CppType t1144_0_0_0;
static ParameterInfo t6294_m32675_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1144_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32675_GM;
MethodInfo m32675_MI = 
{
	"Add", NULL, &t6294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6294_m32675_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32675_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32676_GM;
MethodInfo m32676_MI = 
{
	"Clear", NULL, &t6294_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32676_GM};
extern Il2CppType t1144_0_0_0;
static ParameterInfo t6294_m32677_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1144_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32677_GM;
MethodInfo m32677_MI = 
{
	"Contains", NULL, &t6294_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6294_m32677_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32677_GM};
extern Il2CppType t3625_0_0_0;
extern Il2CppType t3625_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6294_m32678_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3625_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32678_GM;
MethodInfo m32678_MI = 
{
	"CopyTo", NULL, &t6294_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6294_m32678_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32678_GM};
extern Il2CppType t1144_0_0_0;
static ParameterInfo t6294_m32679_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1144_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32679_GM;
MethodInfo m32679_MI = 
{
	"Remove", NULL, &t6294_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6294_m32679_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32679_GM};
static MethodInfo* t6294_MIs[] =
{
	&m32673_MI,
	&m32674_MI,
	&m32675_MI,
	&m32676_MI,
	&m32677_MI,
	&m32678_MI,
	&m32679_MI,
	NULL
};
extern TypeInfo t6296_TI;
static TypeInfo* t6294_ITIs[] = 
{
	&t603_TI,
	&t6296_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6294_0_0_0;
extern Il2CppType t6294_1_0_0;
struct t6294;
extern Il2CppGenericClass t6294_GC;
TypeInfo t6294_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6294_MIs, t6294_PIs, NULL, NULL, NULL, NULL, NULL, &t6294_TI, t6294_ITIs, NULL, &EmptyCustomAttributesCache, &t6294_TI, &t6294_0_0_0, &t6294_1_0_0, NULL, &t6294_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>
extern Il2CppType t4833_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32680_GM;
MethodInfo m32680_MI = 
{
	"GetEnumerator", NULL, &t6296_TI, &t4833_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32680_GM};
static MethodInfo* t6296_MIs[] =
{
	&m32680_MI,
	NULL
};
static TypeInfo* t6296_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6296_0_0_0;
extern Il2CppType t6296_1_0_0;
struct t6296;
extern Il2CppGenericClass t6296_GC;
TypeInfo t6296_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6296_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6296_TI, t6296_ITIs, NULL, &EmptyCustomAttributesCache, &t6296_TI, &t6296_0_0_0, &t6296_1_0_0, NULL, &t6296_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4833_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>
extern MethodInfo m32681_MI;
static PropertyInfo t4833____Current_PropertyInfo = 
{
	&t4833_TI, "Current", &m32681_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4833_PIs[] =
{
	&t4833____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1144_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32681_GM;
MethodInfo m32681_MI = 
{
	"get_Current", NULL, &t4833_TI, &t1144_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32681_GM};
static MethodInfo* t4833_MIs[] =
{
	&m32681_MI,
	NULL
};
static TypeInfo* t4833_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4833_0_0_0;
extern Il2CppType t4833_1_0_0;
struct t4833;
extern Il2CppGenericClass t4833_GC;
TypeInfo t4833_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4833_MIs, t4833_PIs, NULL, NULL, NULL, NULL, NULL, &t4833_TI, t4833_ITIs, NULL, &EmptyCustomAttributesCache, &t4833_TI, &t4833_0_0_0, &t4833_1_0_0, NULL, &t4833_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3367.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3367_TI;
#include "t3367MD.h"

extern TypeInfo t1144_TI;
extern MethodInfo m18711_MI;
extern MethodInfo m25004_MI;
struct t20;
#define m25004(__this, p0, method) (t1144 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3367_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3367_TI, offsetof(t3367, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3367_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3367_TI, offsetof(t3367, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3367_FIs[] =
{
	&t3367_f0_FieldInfo,
	&t3367_f1_FieldInfo,
	NULL
};
extern MethodInfo m18708_MI;
static PropertyInfo t3367____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3367_TI, "System.Collections.IEnumerator.Current", &m18708_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3367____Current_PropertyInfo = 
{
	&t3367_TI, "Current", &m18711_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3367_PIs[] =
{
	&t3367____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3367____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3367_m18707_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18707_GM;
MethodInfo m18707_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3367_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3367_m18707_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18707_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18708_GM;
MethodInfo m18708_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3367_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18708_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18709_GM;
MethodInfo m18709_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3367_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18709_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18710_GM;
MethodInfo m18710_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3367_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18710_GM};
extern Il2CppType t1144_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18711_GM;
MethodInfo m18711_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3367_TI, &t1144_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18711_GM};
static MethodInfo* t3367_MIs[] =
{
	&m18707_MI,
	&m18708_MI,
	&m18709_MI,
	&m18710_MI,
	&m18711_MI,
	NULL
};
extern MethodInfo m18710_MI;
extern MethodInfo m18709_MI;
static MethodInfo* t3367_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18708_MI,
	&m18710_MI,
	&m18709_MI,
	&m18711_MI,
};
static TypeInfo* t3367_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4833_TI,
};
static Il2CppInterfaceOffsetPair t3367_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4833_TI, 7},
};
extern TypeInfo t1144_TI;
static Il2CppRGCTXData t3367_RGCTXData[3] = 
{
	&m18711_MI/* Method Usage */,
	&t1144_TI/* Class Usage */,
	&m25004_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3367_0_0_0;
extern Il2CppType t3367_1_0_0;
extern Il2CppGenericClass t3367_GC;
TypeInfo t3367_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3367_MIs, t3367_PIs, t3367_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3367_TI, t3367_ITIs, t3367_VT, &EmptyCustomAttributesCache, &t3367_TI, &t3367_0_0_0, &t3367_1_0_0, t3367_IOs, &t3367_GC, NULL, NULL, NULL, t3367_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3367)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6295_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.FieldInfo>
extern MethodInfo m32682_MI;
extern MethodInfo m32683_MI;
static PropertyInfo t6295____Item_PropertyInfo = 
{
	&t6295_TI, "Item", &m32682_MI, &m32683_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6295_PIs[] =
{
	&t6295____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1144_0_0_0;
static ParameterInfo t6295_m32684_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1144_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32684_GM;
MethodInfo m32684_MI = 
{
	"IndexOf", NULL, &t6295_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6295_m32684_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32684_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1144_0_0_0;
static ParameterInfo t6295_m32685_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1144_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32685_GM;
MethodInfo m32685_MI = 
{
	"Insert", NULL, &t6295_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6295_m32685_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32685_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6295_m32686_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32686_GM;
MethodInfo m32686_MI = 
{
	"RemoveAt", NULL, &t6295_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6295_m32686_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32686_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6295_m32682_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1144_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32682_GM;
MethodInfo m32682_MI = 
{
	"get_Item", NULL, &t6295_TI, &t1144_0_0_0, RuntimeInvoker_t29_t44, t6295_m32682_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32682_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1144_0_0_0;
static ParameterInfo t6295_m32683_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1144_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32683_GM;
MethodInfo m32683_MI = 
{
	"set_Item", NULL, &t6295_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6295_m32683_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32683_GM};
static MethodInfo* t6295_MIs[] =
{
	&m32684_MI,
	&m32685_MI,
	&m32686_MI,
	&m32682_MI,
	&m32683_MI,
	NULL
};
static TypeInfo* t6295_ITIs[] = 
{
	&t603_TI,
	&t6294_TI,
	&t6296_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6295_0_0_0;
extern Il2CppType t6295_1_0_0;
struct t6295;
extern Il2CppGenericClass t6295_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6295_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6295_MIs, t6295_PIs, NULL, NULL, NULL, NULL, NULL, &t6295_TI, t6295_ITIs, NULL, &t1908__CustomAttributeCache, &t6295_TI, &t6295_0_0_0, &t6295_1_0_0, NULL, &t6295_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6297_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>
extern MethodInfo m32687_MI;
static PropertyInfo t6297____Count_PropertyInfo = 
{
	&t6297_TI, "Count", &m32687_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32688_MI;
static PropertyInfo t6297____IsReadOnly_PropertyInfo = 
{
	&t6297_TI, "IsReadOnly", &m32688_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6297_PIs[] =
{
	&t6297____Count_PropertyInfo,
	&t6297____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32687_GM;
MethodInfo m32687_MI = 
{
	"get_Count", NULL, &t6297_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32687_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32688_GM;
MethodInfo m32688_MI = 
{
	"get_IsReadOnly", NULL, &t6297_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32688_GM};
extern Il2CppType t2013_0_0_0;
extern Il2CppType t2013_0_0_0;
static ParameterInfo t6297_m32689_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2013_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32689_GM;
MethodInfo m32689_MI = 
{
	"Add", NULL, &t6297_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6297_m32689_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32689_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32690_GM;
MethodInfo m32690_MI = 
{
	"Clear", NULL, &t6297_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32690_GM};
extern Il2CppType t2013_0_0_0;
static ParameterInfo t6297_m32691_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2013_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32691_GM;
MethodInfo m32691_MI = 
{
	"Contains", NULL, &t6297_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6297_m32691_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32691_GM};
extern Il2CppType t3626_0_0_0;
extern Il2CppType t3626_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6297_m32692_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3626_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32692_GM;
MethodInfo m32692_MI = 
{
	"CopyTo", NULL, &t6297_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6297_m32692_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32692_GM};
extern Il2CppType t2013_0_0_0;
static ParameterInfo t6297_m32693_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2013_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32693_GM;
MethodInfo m32693_MI = 
{
	"Remove", NULL, &t6297_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6297_m32693_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32693_GM};
static MethodInfo* t6297_MIs[] =
{
	&m32687_MI,
	&m32688_MI,
	&m32689_MI,
	&m32690_MI,
	&m32691_MI,
	&m32692_MI,
	&m32693_MI,
	NULL
};
extern TypeInfo t6299_TI;
static TypeInfo* t6297_ITIs[] = 
{
	&t603_TI,
	&t6299_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6297_0_0_0;
extern Il2CppType t6297_1_0_0;
struct t6297;
extern Il2CppGenericClass t6297_GC;
TypeInfo t6297_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6297_MIs, t6297_PIs, NULL, NULL, NULL, NULL, NULL, &t6297_TI, t6297_ITIs, NULL, &EmptyCustomAttributesCache, &t6297_TI, &t6297_0_0_0, &t6297_1_0_0, NULL, &t6297_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._FieldInfo>
extern Il2CppType t4835_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32694_GM;
MethodInfo m32694_MI = 
{
	"GetEnumerator", NULL, &t6299_TI, &t4835_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32694_GM};
static MethodInfo* t6299_MIs[] =
{
	&m32694_MI,
	NULL
};
static TypeInfo* t6299_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6299_0_0_0;
extern Il2CppType t6299_1_0_0;
struct t6299;
extern Il2CppGenericClass t6299_GC;
TypeInfo t6299_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6299_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6299_TI, t6299_ITIs, NULL, &EmptyCustomAttributesCache, &t6299_TI, &t6299_0_0_0, &t6299_1_0_0, NULL, &t6299_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4835_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._FieldInfo>
extern MethodInfo m32695_MI;
static PropertyInfo t4835____Current_PropertyInfo = 
{
	&t4835_TI, "Current", &m32695_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4835_PIs[] =
{
	&t4835____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2013_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32695_GM;
MethodInfo m32695_MI = 
{
	"get_Current", NULL, &t4835_TI, &t2013_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32695_GM};
static MethodInfo* t4835_MIs[] =
{
	&m32695_MI,
	NULL
};
static TypeInfo* t4835_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4835_0_0_0;
extern Il2CppType t4835_1_0_0;
struct t4835;
extern Il2CppGenericClass t4835_GC;
TypeInfo t4835_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4835_MIs, t4835_PIs, NULL, NULL, NULL, NULL, NULL, &t4835_TI, t4835_ITIs, NULL, &EmptyCustomAttributesCache, &t4835_TI, &t4835_0_0_0, &t4835_1_0_0, NULL, &t4835_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3368.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3368_TI;
#include "t3368MD.h"

extern TypeInfo t2013_TI;
extern MethodInfo m18716_MI;
extern MethodInfo m25015_MI;
struct t20;
#define m25015(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3368_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3368_TI, offsetof(t3368, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3368_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3368_TI, offsetof(t3368, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3368_FIs[] =
{
	&t3368_f0_FieldInfo,
	&t3368_f1_FieldInfo,
	NULL
};
extern MethodInfo m18713_MI;
static PropertyInfo t3368____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3368_TI, "System.Collections.IEnumerator.Current", &m18713_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3368____Current_PropertyInfo = 
{
	&t3368_TI, "Current", &m18716_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3368_PIs[] =
{
	&t3368____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3368____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3368_m18712_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18712_GM;
MethodInfo m18712_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3368_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3368_m18712_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18712_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18713_GM;
MethodInfo m18713_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3368_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18713_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18714_GM;
MethodInfo m18714_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3368_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18714_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18715_GM;
MethodInfo m18715_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3368_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18715_GM};
extern Il2CppType t2013_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18716_GM;
MethodInfo m18716_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3368_TI, &t2013_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18716_GM};
static MethodInfo* t3368_MIs[] =
{
	&m18712_MI,
	&m18713_MI,
	&m18714_MI,
	&m18715_MI,
	&m18716_MI,
	NULL
};
extern MethodInfo m18715_MI;
extern MethodInfo m18714_MI;
static MethodInfo* t3368_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18713_MI,
	&m18715_MI,
	&m18714_MI,
	&m18716_MI,
};
static TypeInfo* t3368_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4835_TI,
};
static Il2CppInterfaceOffsetPair t3368_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4835_TI, 7},
};
extern TypeInfo t2013_TI;
static Il2CppRGCTXData t3368_RGCTXData[3] = 
{
	&m18716_MI/* Method Usage */,
	&t2013_TI/* Class Usage */,
	&m25015_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3368_0_0_0;
extern Il2CppType t3368_1_0_0;
extern Il2CppGenericClass t3368_GC;
TypeInfo t3368_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3368_MIs, t3368_PIs, t3368_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3368_TI, t3368_ITIs, t3368_VT, &EmptyCustomAttributesCache, &t3368_TI, &t3368_0_0_0, &t3368_1_0_0, t3368_IOs, &t3368_GC, NULL, NULL, NULL, t3368_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3368)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6298_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldInfo>
extern MethodInfo m32696_MI;
extern MethodInfo m32697_MI;
static PropertyInfo t6298____Item_PropertyInfo = 
{
	&t6298_TI, "Item", &m32696_MI, &m32697_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6298_PIs[] =
{
	&t6298____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2013_0_0_0;
static ParameterInfo t6298_m32698_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2013_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32698_GM;
MethodInfo m32698_MI = 
{
	"IndexOf", NULL, &t6298_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6298_m32698_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32698_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2013_0_0_0;
static ParameterInfo t6298_m32699_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2013_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32699_GM;
MethodInfo m32699_MI = 
{
	"Insert", NULL, &t6298_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6298_m32699_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32699_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6298_m32700_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32700_GM;
MethodInfo m32700_MI = 
{
	"RemoveAt", NULL, &t6298_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6298_m32700_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32700_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6298_m32696_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2013_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32696_GM;
MethodInfo m32696_MI = 
{
	"get_Item", NULL, &t6298_TI, &t2013_0_0_0, RuntimeInvoker_t29_t44, t6298_m32696_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32696_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2013_0_0_0;
static ParameterInfo t6298_m32697_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2013_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32697_GM;
MethodInfo m32697_MI = 
{
	"set_Item", NULL, &t6298_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6298_m32697_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32697_GM};
static MethodInfo* t6298_MIs[] =
{
	&m32698_MI,
	&m32699_MI,
	&m32700_MI,
	&m32696_MI,
	&m32697_MI,
	NULL
};
static TypeInfo* t6298_ITIs[] = 
{
	&t603_TI,
	&t6297_TI,
	&t6299_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6298_0_0_0;
extern Il2CppType t6298_1_0_0;
struct t6298;
extern Il2CppGenericClass t6298_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6298_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6298_MIs, t6298_PIs, NULL, NULL, NULL, NULL, NULL, &t6298_TI, t6298_ITIs, NULL, &t1908__CustomAttributeCache, &t6298_TI, &t6298_0_0_0, &t6298_1_0_0, NULL, &t6298_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4837_TI;

#include "t432.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyCompanyAttribute>
extern MethodInfo m32701_MI;
static PropertyInfo t4837____Current_PropertyInfo = 
{
	&t4837_TI, "Current", &m32701_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4837_PIs[] =
{
	&t4837____Current_PropertyInfo,
	NULL
};
extern Il2CppType t432_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32701_GM;
MethodInfo m32701_MI = 
{
	"get_Current", NULL, &t4837_TI, &t432_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32701_GM};
static MethodInfo* t4837_MIs[] =
{
	&m32701_MI,
	NULL
};
static TypeInfo* t4837_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4837_0_0_0;
extern Il2CppType t4837_1_0_0;
struct t4837;
extern Il2CppGenericClass t4837_GC;
TypeInfo t4837_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4837_MIs, t4837_PIs, NULL, NULL, NULL, NULL, NULL, &t4837_TI, t4837_ITIs, NULL, &EmptyCustomAttributesCache, &t4837_TI, &t4837_0_0_0, &t4837_1_0_0, NULL, &t4837_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3369.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3369_TI;
#include "t3369MD.h"

extern TypeInfo t432_TI;
extern MethodInfo m18721_MI;
extern MethodInfo m25026_MI;
struct t20;
#define m25026(__this, p0, method) (t432 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3369_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3369_TI, offsetof(t3369, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3369_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3369_TI, offsetof(t3369, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3369_FIs[] =
{
	&t3369_f0_FieldInfo,
	&t3369_f1_FieldInfo,
	NULL
};
extern MethodInfo m18718_MI;
static PropertyInfo t3369____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3369_TI, "System.Collections.IEnumerator.Current", &m18718_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3369____Current_PropertyInfo = 
{
	&t3369_TI, "Current", &m18721_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3369_PIs[] =
{
	&t3369____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3369____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3369_m18717_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18717_GM;
MethodInfo m18717_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3369_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3369_m18717_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18717_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18718_GM;
MethodInfo m18718_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3369_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18718_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18719_GM;
MethodInfo m18719_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3369_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18719_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18720_GM;
MethodInfo m18720_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3369_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18720_GM};
extern Il2CppType t432_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18721_GM;
MethodInfo m18721_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3369_TI, &t432_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18721_GM};
static MethodInfo* t3369_MIs[] =
{
	&m18717_MI,
	&m18718_MI,
	&m18719_MI,
	&m18720_MI,
	&m18721_MI,
	NULL
};
extern MethodInfo m18720_MI;
extern MethodInfo m18719_MI;
static MethodInfo* t3369_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18718_MI,
	&m18720_MI,
	&m18719_MI,
	&m18721_MI,
};
static TypeInfo* t3369_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4837_TI,
};
static Il2CppInterfaceOffsetPair t3369_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4837_TI, 7},
};
extern TypeInfo t432_TI;
static Il2CppRGCTXData t3369_RGCTXData[3] = 
{
	&m18721_MI/* Method Usage */,
	&t432_TI/* Class Usage */,
	&m25026_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3369_0_0_0;
extern Il2CppType t3369_1_0_0;
extern Il2CppGenericClass t3369_GC;
TypeInfo t3369_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3369_MIs, t3369_PIs, t3369_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3369_TI, t3369_ITIs, t3369_VT, &EmptyCustomAttributesCache, &t3369_TI, &t3369_0_0_0, &t3369_1_0_0, t3369_IOs, &t3369_GC, NULL, NULL, NULL, t3369_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3369)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6300_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>
extern MethodInfo m32702_MI;
static PropertyInfo t6300____Count_PropertyInfo = 
{
	&t6300_TI, "Count", &m32702_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32703_MI;
static PropertyInfo t6300____IsReadOnly_PropertyInfo = 
{
	&t6300_TI, "IsReadOnly", &m32703_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6300_PIs[] =
{
	&t6300____Count_PropertyInfo,
	&t6300____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32702_GM;
MethodInfo m32702_MI = 
{
	"get_Count", NULL, &t6300_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32702_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32703_GM;
MethodInfo m32703_MI = 
{
	"get_IsReadOnly", NULL, &t6300_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32703_GM};
extern Il2CppType t432_0_0_0;
extern Il2CppType t432_0_0_0;
static ParameterInfo t6300_m32704_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t432_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32704_GM;
MethodInfo m32704_MI = 
{
	"Add", NULL, &t6300_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6300_m32704_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32704_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32705_GM;
MethodInfo m32705_MI = 
{
	"Clear", NULL, &t6300_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32705_GM};
extern Il2CppType t432_0_0_0;
static ParameterInfo t6300_m32706_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t432_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32706_GM;
MethodInfo m32706_MI = 
{
	"Contains", NULL, &t6300_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6300_m32706_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32706_GM};
extern Il2CppType t3627_0_0_0;
extern Il2CppType t3627_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6300_m32707_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3627_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32707_GM;
MethodInfo m32707_MI = 
{
	"CopyTo", NULL, &t6300_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6300_m32707_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32707_GM};
extern Il2CppType t432_0_0_0;
static ParameterInfo t6300_m32708_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t432_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32708_GM;
MethodInfo m32708_MI = 
{
	"Remove", NULL, &t6300_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6300_m32708_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32708_GM};
static MethodInfo* t6300_MIs[] =
{
	&m32702_MI,
	&m32703_MI,
	&m32704_MI,
	&m32705_MI,
	&m32706_MI,
	&m32707_MI,
	&m32708_MI,
	NULL
};
extern TypeInfo t6302_TI;
static TypeInfo* t6300_ITIs[] = 
{
	&t603_TI,
	&t6302_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6300_0_0_0;
extern Il2CppType t6300_1_0_0;
struct t6300;
extern Il2CppGenericClass t6300_GC;
TypeInfo t6300_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6300_MIs, t6300_PIs, NULL, NULL, NULL, NULL, NULL, &t6300_TI, t6300_ITIs, NULL, &EmptyCustomAttributesCache, &t6300_TI, &t6300_0_0_0, &t6300_1_0_0, NULL, &t6300_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyCompanyAttribute>
extern Il2CppType t4837_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32709_GM;
MethodInfo m32709_MI = 
{
	"GetEnumerator", NULL, &t6302_TI, &t4837_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32709_GM};
static MethodInfo* t6302_MIs[] =
{
	&m32709_MI,
	NULL
};
static TypeInfo* t6302_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6302_0_0_0;
extern Il2CppType t6302_1_0_0;
struct t6302;
extern Il2CppGenericClass t6302_GC;
TypeInfo t6302_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6302_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6302_TI, t6302_ITIs, NULL, &EmptyCustomAttributesCache, &t6302_TI, &t6302_0_0_0, &t6302_1_0_0, NULL, &t6302_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6301_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyCompanyAttribute>
extern MethodInfo m32710_MI;
extern MethodInfo m32711_MI;
static PropertyInfo t6301____Item_PropertyInfo = 
{
	&t6301_TI, "Item", &m32710_MI, &m32711_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6301_PIs[] =
{
	&t6301____Item_PropertyInfo,
	NULL
};
extern Il2CppType t432_0_0_0;
static ParameterInfo t6301_m32712_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t432_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32712_GM;
MethodInfo m32712_MI = 
{
	"IndexOf", NULL, &t6301_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6301_m32712_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32712_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t432_0_0_0;
static ParameterInfo t6301_m32713_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t432_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32713_GM;
MethodInfo m32713_MI = 
{
	"Insert", NULL, &t6301_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6301_m32713_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32713_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6301_m32714_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32714_GM;
MethodInfo m32714_MI = 
{
	"RemoveAt", NULL, &t6301_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6301_m32714_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32714_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6301_m32710_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t432_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32710_GM;
MethodInfo m32710_MI = 
{
	"get_Item", NULL, &t6301_TI, &t432_0_0_0, RuntimeInvoker_t29_t44, t6301_m32710_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32710_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t432_0_0_0;
static ParameterInfo t6301_m32711_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t432_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32711_GM;
MethodInfo m32711_MI = 
{
	"set_Item", NULL, &t6301_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6301_m32711_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32711_GM};
static MethodInfo* t6301_MIs[] =
{
	&m32712_MI,
	&m32713_MI,
	&m32714_MI,
	&m32710_MI,
	&m32711_MI,
	NULL
};
static TypeInfo* t6301_ITIs[] = 
{
	&t603_TI,
	&t6300_TI,
	&t6302_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6301_0_0_0;
extern Il2CppType t6301_1_0_0;
struct t6301;
extern Il2CppGenericClass t6301_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6301_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6301_MIs, t6301_PIs, NULL, NULL, NULL, NULL, NULL, &t6301_TI, t6301_ITIs, NULL, &t1908__CustomAttributeCache, &t6301_TI, &t6301_0_0_0, &t6301_1_0_0, NULL, &t6301_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4839_TI;

#include "t431.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>
extern MethodInfo m32715_MI;
static PropertyInfo t4839____Current_PropertyInfo = 
{
	&t4839_TI, "Current", &m32715_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4839_PIs[] =
{
	&t4839____Current_PropertyInfo,
	NULL
};
extern Il2CppType t431_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32715_GM;
MethodInfo m32715_MI = 
{
	"get_Current", NULL, &t4839_TI, &t431_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32715_GM};
static MethodInfo* t4839_MIs[] =
{
	&m32715_MI,
	NULL
};
static TypeInfo* t4839_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4839_0_0_0;
extern Il2CppType t4839_1_0_0;
struct t4839;
extern Il2CppGenericClass t4839_GC;
TypeInfo t4839_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4839_MIs, t4839_PIs, NULL, NULL, NULL, NULL, NULL, &t4839_TI, t4839_ITIs, NULL, &EmptyCustomAttributesCache, &t4839_TI, &t4839_0_0_0, &t4839_1_0_0, NULL, &t4839_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3370.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3370_TI;
#include "t3370MD.h"

extern TypeInfo t431_TI;
extern MethodInfo m18726_MI;
extern MethodInfo m25037_MI;
struct t20;
#define m25037(__this, p0, method) (t431 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3370_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3370_TI, offsetof(t3370, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3370_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3370_TI, offsetof(t3370, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3370_FIs[] =
{
	&t3370_f0_FieldInfo,
	&t3370_f1_FieldInfo,
	NULL
};
extern MethodInfo m18723_MI;
static PropertyInfo t3370____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3370_TI, "System.Collections.IEnumerator.Current", &m18723_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3370____Current_PropertyInfo = 
{
	&t3370_TI, "Current", &m18726_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3370_PIs[] =
{
	&t3370____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3370____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3370_m18722_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18722_GM;
MethodInfo m18722_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3370_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3370_m18722_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18722_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18723_GM;
MethodInfo m18723_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3370_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18723_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18724_GM;
MethodInfo m18724_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3370_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18724_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18725_GM;
MethodInfo m18725_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3370_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18725_GM};
extern Il2CppType t431_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18726_GM;
MethodInfo m18726_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3370_TI, &t431_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18726_GM};
static MethodInfo* t3370_MIs[] =
{
	&m18722_MI,
	&m18723_MI,
	&m18724_MI,
	&m18725_MI,
	&m18726_MI,
	NULL
};
extern MethodInfo m18725_MI;
extern MethodInfo m18724_MI;
static MethodInfo* t3370_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18723_MI,
	&m18725_MI,
	&m18724_MI,
	&m18726_MI,
};
static TypeInfo* t3370_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4839_TI,
};
static Il2CppInterfaceOffsetPair t3370_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4839_TI, 7},
};
extern TypeInfo t431_TI;
static Il2CppRGCTXData t3370_RGCTXData[3] = 
{
	&m18726_MI/* Method Usage */,
	&t431_TI/* Class Usage */,
	&m25037_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3370_0_0_0;
extern Il2CppType t3370_1_0_0;
extern Il2CppGenericClass t3370_GC;
TypeInfo t3370_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3370_MIs, t3370_PIs, t3370_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3370_TI, t3370_ITIs, t3370_VT, &EmptyCustomAttributesCache, &t3370_TI, &t3370_0_0_0, &t3370_1_0_0, t3370_IOs, &t3370_GC, NULL, NULL, NULL, t3370_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3370)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6303_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>
extern MethodInfo m32716_MI;
static PropertyInfo t6303____Count_PropertyInfo = 
{
	&t6303_TI, "Count", &m32716_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32717_MI;
static PropertyInfo t6303____IsReadOnly_PropertyInfo = 
{
	&t6303_TI, "IsReadOnly", &m32717_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6303_PIs[] =
{
	&t6303____Count_PropertyInfo,
	&t6303____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32716_GM;
MethodInfo m32716_MI = 
{
	"get_Count", NULL, &t6303_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32716_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32717_GM;
MethodInfo m32717_MI = 
{
	"get_IsReadOnly", NULL, &t6303_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32717_GM};
extern Il2CppType t431_0_0_0;
extern Il2CppType t431_0_0_0;
static ParameterInfo t6303_m32718_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t431_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32718_GM;
MethodInfo m32718_MI = 
{
	"Add", NULL, &t6303_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6303_m32718_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32718_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32719_GM;
MethodInfo m32719_MI = 
{
	"Clear", NULL, &t6303_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32719_GM};
extern Il2CppType t431_0_0_0;
static ParameterInfo t6303_m32720_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t431_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32720_GM;
MethodInfo m32720_MI = 
{
	"Contains", NULL, &t6303_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6303_m32720_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32720_GM};
extern Il2CppType t3628_0_0_0;
extern Il2CppType t3628_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6303_m32721_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3628_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32721_GM;
MethodInfo m32721_MI = 
{
	"CopyTo", NULL, &t6303_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6303_m32721_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32721_GM};
extern Il2CppType t431_0_0_0;
static ParameterInfo t6303_m32722_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t431_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32722_GM;
MethodInfo m32722_MI = 
{
	"Remove", NULL, &t6303_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6303_m32722_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32722_GM};
static MethodInfo* t6303_MIs[] =
{
	&m32716_MI,
	&m32717_MI,
	&m32718_MI,
	&m32719_MI,
	&m32720_MI,
	&m32721_MI,
	&m32722_MI,
	NULL
};
extern TypeInfo t6305_TI;
static TypeInfo* t6303_ITIs[] = 
{
	&t603_TI,
	&t6305_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6303_0_0_0;
extern Il2CppType t6303_1_0_0;
struct t6303;
extern Il2CppGenericClass t6303_GC;
TypeInfo t6303_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6303_MIs, t6303_PIs, NULL, NULL, NULL, NULL, NULL, &t6303_TI, t6303_ITIs, NULL, &EmptyCustomAttributesCache, &t6303_TI, &t6303_0_0_0, &t6303_1_0_0, NULL, &t6303_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyConfigurationAttribute>
extern Il2CppType t4839_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32723_GM;
MethodInfo m32723_MI = 
{
	"GetEnumerator", NULL, &t6305_TI, &t4839_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32723_GM};
static MethodInfo* t6305_MIs[] =
{
	&m32723_MI,
	NULL
};
static TypeInfo* t6305_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6305_0_0_0;
extern Il2CppType t6305_1_0_0;
struct t6305;
extern Il2CppGenericClass t6305_GC;
TypeInfo t6305_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6305_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6305_TI, t6305_ITIs, NULL, &EmptyCustomAttributesCache, &t6305_TI, &t6305_0_0_0, &t6305_1_0_0, NULL, &t6305_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6304_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyConfigurationAttribute>
extern MethodInfo m32724_MI;
extern MethodInfo m32725_MI;
static PropertyInfo t6304____Item_PropertyInfo = 
{
	&t6304_TI, "Item", &m32724_MI, &m32725_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6304_PIs[] =
{
	&t6304____Item_PropertyInfo,
	NULL
};
extern Il2CppType t431_0_0_0;
static ParameterInfo t6304_m32726_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t431_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32726_GM;
MethodInfo m32726_MI = 
{
	"IndexOf", NULL, &t6304_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6304_m32726_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32726_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t431_0_0_0;
static ParameterInfo t6304_m32727_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t431_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32727_GM;
MethodInfo m32727_MI = 
{
	"Insert", NULL, &t6304_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6304_m32727_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32727_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6304_m32728_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32728_GM;
MethodInfo m32728_MI = 
{
	"RemoveAt", NULL, &t6304_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6304_m32728_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32728_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6304_m32724_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t431_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32724_GM;
MethodInfo m32724_MI = 
{
	"get_Item", NULL, &t6304_TI, &t431_0_0_0, RuntimeInvoker_t29_t44, t6304_m32724_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32724_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t431_0_0_0;
static ParameterInfo t6304_m32725_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t431_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32725_GM;
MethodInfo m32725_MI = 
{
	"set_Item", NULL, &t6304_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6304_m32725_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32725_GM};
static MethodInfo* t6304_MIs[] =
{
	&m32726_MI,
	&m32727_MI,
	&m32728_MI,
	&m32724_MI,
	&m32725_MI,
	NULL
};
static TypeInfo* t6304_ITIs[] = 
{
	&t603_TI,
	&t6303_TI,
	&t6305_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6304_0_0_0;
extern Il2CppType t6304_1_0_0;
struct t6304;
extern Il2CppGenericClass t6304_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6304_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6304_MIs, t6304_PIs, NULL, NULL, NULL, NULL, NULL, &t6304_TI, t6304_ITIs, NULL, &t1908__CustomAttributeCache, &t6304_TI, &t6304_0_0_0, &t6304_1_0_0, NULL, &t6304_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4841_TI;

#include "t434.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>
extern MethodInfo m32729_MI;
static PropertyInfo t4841____Current_PropertyInfo = 
{
	&t4841_TI, "Current", &m32729_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4841_PIs[] =
{
	&t4841____Current_PropertyInfo,
	NULL
};
extern Il2CppType t434_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32729_GM;
MethodInfo m32729_MI = 
{
	"get_Current", NULL, &t4841_TI, &t434_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32729_GM};
static MethodInfo* t4841_MIs[] =
{
	&m32729_MI,
	NULL
};
static TypeInfo* t4841_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4841_0_0_0;
extern Il2CppType t4841_1_0_0;
struct t4841;
extern Il2CppGenericClass t4841_GC;
TypeInfo t4841_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4841_MIs, t4841_PIs, NULL, NULL, NULL, NULL, NULL, &t4841_TI, t4841_ITIs, NULL, &EmptyCustomAttributesCache, &t4841_TI, &t4841_0_0_0, &t4841_1_0_0, NULL, &t4841_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3371.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3371_TI;
#include "t3371MD.h"

extern TypeInfo t434_TI;
extern MethodInfo m18731_MI;
extern MethodInfo m25048_MI;
struct t20;
#define m25048(__this, p0, method) (t434 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3371_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3371_TI, offsetof(t3371, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3371_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3371_TI, offsetof(t3371, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3371_FIs[] =
{
	&t3371_f0_FieldInfo,
	&t3371_f1_FieldInfo,
	NULL
};
extern MethodInfo m18728_MI;
static PropertyInfo t3371____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3371_TI, "System.Collections.IEnumerator.Current", &m18728_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3371____Current_PropertyInfo = 
{
	&t3371_TI, "Current", &m18731_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3371_PIs[] =
{
	&t3371____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3371____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3371_m18727_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18727_GM;
MethodInfo m18727_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3371_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3371_m18727_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18727_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18728_GM;
MethodInfo m18728_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3371_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18728_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18729_GM;
MethodInfo m18729_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3371_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18729_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18730_GM;
MethodInfo m18730_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3371_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18730_GM};
extern Il2CppType t434_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18731_GM;
MethodInfo m18731_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3371_TI, &t434_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18731_GM};
static MethodInfo* t3371_MIs[] =
{
	&m18727_MI,
	&m18728_MI,
	&m18729_MI,
	&m18730_MI,
	&m18731_MI,
	NULL
};
extern MethodInfo m18730_MI;
extern MethodInfo m18729_MI;
static MethodInfo* t3371_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18728_MI,
	&m18730_MI,
	&m18729_MI,
	&m18731_MI,
};
static TypeInfo* t3371_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4841_TI,
};
static Il2CppInterfaceOffsetPair t3371_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4841_TI, 7},
};
extern TypeInfo t434_TI;
static Il2CppRGCTXData t3371_RGCTXData[3] = 
{
	&m18731_MI/* Method Usage */,
	&t434_TI/* Class Usage */,
	&m25048_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3371_0_0_0;
extern Il2CppType t3371_1_0_0;
extern Il2CppGenericClass t3371_GC;
TypeInfo t3371_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3371_MIs, t3371_PIs, t3371_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3371_TI, t3371_ITIs, t3371_VT, &EmptyCustomAttributesCache, &t3371_TI, &t3371_0_0_0, &t3371_1_0_0, t3371_IOs, &t3371_GC, NULL, NULL, NULL, t3371_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3371)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6306_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>
extern MethodInfo m32730_MI;
static PropertyInfo t6306____Count_PropertyInfo = 
{
	&t6306_TI, "Count", &m32730_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32731_MI;
static PropertyInfo t6306____IsReadOnly_PropertyInfo = 
{
	&t6306_TI, "IsReadOnly", &m32731_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6306_PIs[] =
{
	&t6306____Count_PropertyInfo,
	&t6306____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32730_GM;
MethodInfo m32730_MI = 
{
	"get_Count", NULL, &t6306_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32730_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32731_GM;
MethodInfo m32731_MI = 
{
	"get_IsReadOnly", NULL, &t6306_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32731_GM};
extern Il2CppType t434_0_0_0;
extern Il2CppType t434_0_0_0;
static ParameterInfo t6306_m32732_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t434_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32732_GM;
MethodInfo m32732_MI = 
{
	"Add", NULL, &t6306_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6306_m32732_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32732_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32733_GM;
MethodInfo m32733_MI = 
{
	"Clear", NULL, &t6306_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32733_GM};
extern Il2CppType t434_0_0_0;
static ParameterInfo t6306_m32734_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t434_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32734_GM;
MethodInfo m32734_MI = 
{
	"Contains", NULL, &t6306_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6306_m32734_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32734_GM};
extern Il2CppType t3629_0_0_0;
extern Il2CppType t3629_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6306_m32735_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3629_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32735_GM;
MethodInfo m32735_MI = 
{
	"CopyTo", NULL, &t6306_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6306_m32735_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32735_GM};
extern Il2CppType t434_0_0_0;
static ParameterInfo t6306_m32736_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t434_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32736_GM;
MethodInfo m32736_MI = 
{
	"Remove", NULL, &t6306_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6306_m32736_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32736_GM};
static MethodInfo* t6306_MIs[] =
{
	&m32730_MI,
	&m32731_MI,
	&m32732_MI,
	&m32733_MI,
	&m32734_MI,
	&m32735_MI,
	&m32736_MI,
	NULL
};
extern TypeInfo t6308_TI;
static TypeInfo* t6306_ITIs[] = 
{
	&t603_TI,
	&t6308_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6306_0_0_0;
extern Il2CppType t6306_1_0_0;
struct t6306;
extern Il2CppGenericClass t6306_GC;
TypeInfo t6306_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6306_MIs, t6306_PIs, NULL, NULL, NULL, NULL, NULL, &t6306_TI, t6306_ITIs, NULL, &EmptyCustomAttributesCache, &t6306_TI, &t6306_0_0_0, &t6306_1_0_0, NULL, &t6306_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyCopyrightAttribute>
extern Il2CppType t4841_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32737_GM;
MethodInfo m32737_MI = 
{
	"GetEnumerator", NULL, &t6308_TI, &t4841_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32737_GM};
static MethodInfo* t6308_MIs[] =
{
	&m32737_MI,
	NULL
};
static TypeInfo* t6308_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6308_0_0_0;
extern Il2CppType t6308_1_0_0;
struct t6308;
extern Il2CppGenericClass t6308_GC;
TypeInfo t6308_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6308_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6308_TI, t6308_ITIs, NULL, &EmptyCustomAttributesCache, &t6308_TI, &t6308_0_0_0, &t6308_1_0_0, NULL, &t6308_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6307_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyCopyrightAttribute>
extern MethodInfo m32738_MI;
extern MethodInfo m32739_MI;
static PropertyInfo t6307____Item_PropertyInfo = 
{
	&t6307_TI, "Item", &m32738_MI, &m32739_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6307_PIs[] =
{
	&t6307____Item_PropertyInfo,
	NULL
};
extern Il2CppType t434_0_0_0;
static ParameterInfo t6307_m32740_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t434_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32740_GM;
MethodInfo m32740_MI = 
{
	"IndexOf", NULL, &t6307_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6307_m32740_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32740_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t434_0_0_0;
static ParameterInfo t6307_m32741_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t434_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32741_GM;
MethodInfo m32741_MI = 
{
	"Insert", NULL, &t6307_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6307_m32741_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32741_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6307_m32742_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32742_GM;
MethodInfo m32742_MI = 
{
	"RemoveAt", NULL, &t6307_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6307_m32742_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32742_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6307_m32738_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t434_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32738_GM;
MethodInfo m32738_MI = 
{
	"get_Item", NULL, &t6307_TI, &t434_0_0_0, RuntimeInvoker_t29_t44, t6307_m32738_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32738_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t434_0_0_0;
static ParameterInfo t6307_m32739_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t434_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32739_GM;
MethodInfo m32739_MI = 
{
	"set_Item", NULL, &t6307_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6307_m32739_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32739_GM};
static MethodInfo* t6307_MIs[] =
{
	&m32740_MI,
	&m32741_MI,
	&m32742_MI,
	&m32738_MI,
	&m32739_MI,
	NULL
};
static TypeInfo* t6307_ITIs[] = 
{
	&t603_TI,
	&t6306_TI,
	&t6308_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6307_0_0_0;
extern Il2CppType t6307_1_0_0;
struct t6307;
extern Il2CppGenericClass t6307_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6307_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6307_MIs, t6307_PIs, NULL, NULL, NULL, NULL, NULL, &t6307_TI, t6307_ITIs, NULL, &t1908__CustomAttributeCache, &t6307_TI, &t6307_0_0_0, &t6307_1_0_0, NULL, &t6307_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4843_TI;

#include "t712.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>
extern MethodInfo m32743_MI;
static PropertyInfo t4843____Current_PropertyInfo = 
{
	&t4843_TI, "Current", &m32743_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4843_PIs[] =
{
	&t4843____Current_PropertyInfo,
	NULL
};
extern Il2CppType t712_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32743_GM;
MethodInfo m32743_MI = 
{
	"get_Current", NULL, &t4843_TI, &t712_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32743_GM};
static MethodInfo* t4843_MIs[] =
{
	&m32743_MI,
	NULL
};
static TypeInfo* t4843_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4843_0_0_0;
extern Il2CppType t4843_1_0_0;
struct t4843;
extern Il2CppGenericClass t4843_GC;
TypeInfo t4843_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4843_MIs, t4843_PIs, NULL, NULL, NULL, NULL, NULL, &t4843_TI, t4843_ITIs, NULL, &EmptyCustomAttributesCache, &t4843_TI, &t4843_0_0_0, &t4843_1_0_0, NULL, &t4843_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3372.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3372_TI;
#include "t3372MD.h"

extern TypeInfo t712_TI;
extern MethodInfo m18736_MI;
extern MethodInfo m25059_MI;
struct t20;
#define m25059(__this, p0, method) (t712 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3372_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3372_TI, offsetof(t3372, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3372_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3372_TI, offsetof(t3372, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3372_FIs[] =
{
	&t3372_f0_FieldInfo,
	&t3372_f1_FieldInfo,
	NULL
};
extern MethodInfo m18733_MI;
static PropertyInfo t3372____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3372_TI, "System.Collections.IEnumerator.Current", &m18733_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3372____Current_PropertyInfo = 
{
	&t3372_TI, "Current", &m18736_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3372_PIs[] =
{
	&t3372____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3372____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3372_m18732_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18732_GM;
MethodInfo m18732_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3372_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3372_m18732_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18732_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18733_GM;
MethodInfo m18733_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3372_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18733_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18734_GM;
MethodInfo m18734_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3372_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18734_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18735_GM;
MethodInfo m18735_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3372_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18735_GM};
extern Il2CppType t712_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18736_GM;
MethodInfo m18736_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3372_TI, &t712_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18736_GM};
static MethodInfo* t3372_MIs[] =
{
	&m18732_MI,
	&m18733_MI,
	&m18734_MI,
	&m18735_MI,
	&m18736_MI,
	NULL
};
extern MethodInfo m18735_MI;
extern MethodInfo m18734_MI;
static MethodInfo* t3372_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18733_MI,
	&m18735_MI,
	&m18734_MI,
	&m18736_MI,
};
static TypeInfo* t3372_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4843_TI,
};
static Il2CppInterfaceOffsetPair t3372_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4843_TI, 7},
};
extern TypeInfo t712_TI;
static Il2CppRGCTXData t3372_RGCTXData[3] = 
{
	&m18736_MI/* Method Usage */,
	&t712_TI/* Class Usage */,
	&m25059_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3372_0_0_0;
extern Il2CppType t3372_1_0_0;
extern Il2CppGenericClass t3372_GC;
TypeInfo t3372_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3372_MIs, t3372_PIs, t3372_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3372_TI, t3372_ITIs, t3372_VT, &EmptyCustomAttributesCache, &t3372_TI, &t3372_0_0_0, &t3372_1_0_0, t3372_IOs, &t3372_GC, NULL, NULL, NULL, t3372_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3372)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6309_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>
extern MethodInfo m32744_MI;
static PropertyInfo t6309____Count_PropertyInfo = 
{
	&t6309_TI, "Count", &m32744_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32745_MI;
static PropertyInfo t6309____IsReadOnly_PropertyInfo = 
{
	&t6309_TI, "IsReadOnly", &m32745_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6309_PIs[] =
{
	&t6309____Count_PropertyInfo,
	&t6309____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32744_GM;
MethodInfo m32744_MI = 
{
	"get_Count", NULL, &t6309_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32744_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32745_GM;
MethodInfo m32745_MI = 
{
	"get_IsReadOnly", NULL, &t6309_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32745_GM};
extern Il2CppType t712_0_0_0;
extern Il2CppType t712_0_0_0;
static ParameterInfo t6309_m32746_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t712_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32746_GM;
MethodInfo m32746_MI = 
{
	"Add", NULL, &t6309_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6309_m32746_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32746_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32747_GM;
MethodInfo m32747_MI = 
{
	"Clear", NULL, &t6309_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32747_GM};
extern Il2CppType t712_0_0_0;
static ParameterInfo t6309_m32748_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t712_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32748_GM;
MethodInfo m32748_MI = 
{
	"Contains", NULL, &t6309_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6309_m32748_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32748_GM};
extern Il2CppType t3630_0_0_0;
extern Il2CppType t3630_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6309_m32749_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3630_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32749_GM;
MethodInfo m32749_MI = 
{
	"CopyTo", NULL, &t6309_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6309_m32749_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32749_GM};
extern Il2CppType t712_0_0_0;
static ParameterInfo t6309_m32750_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t712_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32750_GM;
MethodInfo m32750_MI = 
{
	"Remove", NULL, &t6309_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6309_m32750_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32750_GM};
static MethodInfo* t6309_MIs[] =
{
	&m32744_MI,
	&m32745_MI,
	&m32746_MI,
	&m32747_MI,
	&m32748_MI,
	&m32749_MI,
	&m32750_MI,
	NULL
};
extern TypeInfo t6311_TI;
static TypeInfo* t6309_ITIs[] = 
{
	&t603_TI,
	&t6311_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6309_0_0_0;
extern Il2CppType t6309_1_0_0;
struct t6309;
extern Il2CppGenericClass t6309_GC;
TypeInfo t6309_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6309_MIs, t6309_PIs, NULL, NULL, NULL, NULL, NULL, &t6309_TI, t6309_ITIs, NULL, &EmptyCustomAttributesCache, &t6309_TI, &t6309_0_0_0, &t6309_1_0_0, NULL, &t6309_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyDefaultAliasAttribute>
extern Il2CppType t4843_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32751_GM;
MethodInfo m32751_MI = 
{
	"GetEnumerator", NULL, &t6311_TI, &t4843_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32751_GM};
static MethodInfo* t6311_MIs[] =
{
	&m32751_MI,
	NULL
};
static TypeInfo* t6311_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6311_0_0_0;
extern Il2CppType t6311_1_0_0;
struct t6311;
extern Il2CppGenericClass t6311_GC;
TypeInfo t6311_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6311_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6311_TI, t6311_ITIs, NULL, &EmptyCustomAttributesCache, &t6311_TI, &t6311_0_0_0, &t6311_1_0_0, NULL, &t6311_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6310_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyDefaultAliasAttribute>
extern MethodInfo m32752_MI;
extern MethodInfo m32753_MI;
static PropertyInfo t6310____Item_PropertyInfo = 
{
	&t6310_TI, "Item", &m32752_MI, &m32753_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6310_PIs[] =
{
	&t6310____Item_PropertyInfo,
	NULL
};
extern Il2CppType t712_0_0_0;
static ParameterInfo t6310_m32754_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t712_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32754_GM;
MethodInfo m32754_MI = 
{
	"IndexOf", NULL, &t6310_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6310_m32754_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32754_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t712_0_0_0;
static ParameterInfo t6310_m32755_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t712_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32755_GM;
MethodInfo m32755_MI = 
{
	"Insert", NULL, &t6310_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6310_m32755_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32755_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6310_m32756_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32756_GM;
MethodInfo m32756_MI = 
{
	"RemoveAt", NULL, &t6310_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6310_m32756_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32756_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6310_m32752_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t712_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32752_GM;
MethodInfo m32752_MI = 
{
	"get_Item", NULL, &t6310_TI, &t712_0_0_0, RuntimeInvoker_t29_t44, t6310_m32752_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32752_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t712_0_0_0;
static ParameterInfo t6310_m32753_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t712_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32753_GM;
MethodInfo m32753_MI = 
{
	"set_Item", NULL, &t6310_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6310_m32753_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32753_GM};
static MethodInfo* t6310_MIs[] =
{
	&m32754_MI,
	&m32755_MI,
	&m32756_MI,
	&m32752_MI,
	&m32753_MI,
	NULL
};
static TypeInfo* t6310_ITIs[] = 
{
	&t603_TI,
	&t6309_TI,
	&t6311_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6310_0_0_0;
extern Il2CppType t6310_1_0_0;
struct t6310;
extern Il2CppGenericClass t6310_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6310_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6310_MIs, t6310_PIs, NULL, NULL, NULL, NULL, NULL, &t6310_TI, t6310_ITIs, NULL, &t1908__CustomAttributeCache, &t6310_TI, &t6310_0_0_0, &t6310_1_0_0, NULL, &t6310_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4845_TI;

#include "t711.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>
extern MethodInfo m32757_MI;
static PropertyInfo t4845____Current_PropertyInfo = 
{
	&t4845_TI, "Current", &m32757_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4845_PIs[] =
{
	&t4845____Current_PropertyInfo,
	NULL
};
extern Il2CppType t711_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32757_GM;
MethodInfo m32757_MI = 
{
	"get_Current", NULL, &t4845_TI, &t711_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32757_GM};
static MethodInfo* t4845_MIs[] =
{
	&m32757_MI,
	NULL
};
static TypeInfo* t4845_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4845_0_0_0;
extern Il2CppType t4845_1_0_0;
struct t4845;
extern Il2CppGenericClass t4845_GC;
TypeInfo t4845_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4845_MIs, t4845_PIs, NULL, NULL, NULL, NULL, NULL, &t4845_TI, t4845_ITIs, NULL, &EmptyCustomAttributesCache, &t4845_TI, &t4845_0_0_0, &t4845_1_0_0, NULL, &t4845_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3373.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3373_TI;
#include "t3373MD.h"

extern TypeInfo t711_TI;
extern MethodInfo m18741_MI;
extern MethodInfo m25070_MI;
struct t20;
#define m25070(__this, p0, method) (t711 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3373_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3373_TI, offsetof(t3373, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3373_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3373_TI, offsetof(t3373, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3373_FIs[] =
{
	&t3373_f0_FieldInfo,
	&t3373_f1_FieldInfo,
	NULL
};
extern MethodInfo m18738_MI;
static PropertyInfo t3373____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3373_TI, "System.Collections.IEnumerator.Current", &m18738_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3373____Current_PropertyInfo = 
{
	&t3373_TI, "Current", &m18741_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3373_PIs[] =
{
	&t3373____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3373____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3373_m18737_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18737_GM;
MethodInfo m18737_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3373_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3373_m18737_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18737_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18738_GM;
MethodInfo m18738_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3373_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18738_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18739_GM;
MethodInfo m18739_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3373_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18739_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18740_GM;
MethodInfo m18740_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3373_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18740_GM};
extern Il2CppType t711_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18741_GM;
MethodInfo m18741_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3373_TI, &t711_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18741_GM};
static MethodInfo* t3373_MIs[] =
{
	&m18737_MI,
	&m18738_MI,
	&m18739_MI,
	&m18740_MI,
	&m18741_MI,
	NULL
};
extern MethodInfo m18740_MI;
extern MethodInfo m18739_MI;
static MethodInfo* t3373_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18738_MI,
	&m18740_MI,
	&m18739_MI,
	&m18741_MI,
};
static TypeInfo* t3373_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4845_TI,
};
static Il2CppInterfaceOffsetPair t3373_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4845_TI, 7},
};
extern TypeInfo t711_TI;
static Il2CppRGCTXData t3373_RGCTXData[3] = 
{
	&m18741_MI/* Method Usage */,
	&t711_TI/* Class Usage */,
	&m25070_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3373_0_0_0;
extern Il2CppType t3373_1_0_0;
extern Il2CppGenericClass t3373_GC;
TypeInfo t3373_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3373_MIs, t3373_PIs, t3373_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3373_TI, t3373_ITIs, t3373_VT, &EmptyCustomAttributesCache, &t3373_TI, &t3373_0_0_0, &t3373_1_0_0, t3373_IOs, &t3373_GC, NULL, NULL, NULL, t3373_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3373)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6312_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>
extern MethodInfo m32758_MI;
static PropertyInfo t6312____Count_PropertyInfo = 
{
	&t6312_TI, "Count", &m32758_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32759_MI;
static PropertyInfo t6312____IsReadOnly_PropertyInfo = 
{
	&t6312_TI, "IsReadOnly", &m32759_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6312_PIs[] =
{
	&t6312____Count_PropertyInfo,
	&t6312____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32758_GM;
MethodInfo m32758_MI = 
{
	"get_Count", NULL, &t6312_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32758_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32759_GM;
MethodInfo m32759_MI = 
{
	"get_IsReadOnly", NULL, &t6312_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32759_GM};
extern Il2CppType t711_0_0_0;
extern Il2CppType t711_0_0_0;
static ParameterInfo t6312_m32760_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t711_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32760_GM;
MethodInfo m32760_MI = 
{
	"Add", NULL, &t6312_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6312_m32760_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32760_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32761_GM;
MethodInfo m32761_MI = 
{
	"Clear", NULL, &t6312_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32761_GM};
extern Il2CppType t711_0_0_0;
static ParameterInfo t6312_m32762_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t711_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32762_GM;
MethodInfo m32762_MI = 
{
	"Contains", NULL, &t6312_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6312_m32762_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32762_GM};
extern Il2CppType t3631_0_0_0;
extern Il2CppType t3631_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6312_m32763_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3631_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32763_GM;
MethodInfo m32763_MI = 
{
	"CopyTo", NULL, &t6312_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6312_m32763_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32763_GM};
extern Il2CppType t711_0_0_0;
static ParameterInfo t6312_m32764_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t711_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32764_GM;
MethodInfo m32764_MI = 
{
	"Remove", NULL, &t6312_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6312_m32764_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32764_GM};
static MethodInfo* t6312_MIs[] =
{
	&m32758_MI,
	&m32759_MI,
	&m32760_MI,
	&m32761_MI,
	&m32762_MI,
	&m32763_MI,
	&m32764_MI,
	NULL
};
extern TypeInfo t6314_TI;
static TypeInfo* t6312_ITIs[] = 
{
	&t603_TI,
	&t6314_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6312_0_0_0;
extern Il2CppType t6312_1_0_0;
struct t6312;
extern Il2CppGenericClass t6312_GC;
TypeInfo t6312_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6312_MIs, t6312_PIs, NULL, NULL, NULL, NULL, NULL, &t6312_TI, t6312_ITIs, NULL, &EmptyCustomAttributesCache, &t6312_TI, &t6312_0_0_0, &t6312_1_0_0, NULL, &t6312_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyDelaySignAttribute>
extern Il2CppType t4845_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32765_GM;
MethodInfo m32765_MI = 
{
	"GetEnumerator", NULL, &t6314_TI, &t4845_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32765_GM};
static MethodInfo* t6314_MIs[] =
{
	&m32765_MI,
	NULL
};
static TypeInfo* t6314_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6314_0_0_0;
extern Il2CppType t6314_1_0_0;
struct t6314;
extern Il2CppGenericClass t6314_GC;
TypeInfo t6314_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6314_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6314_TI, t6314_ITIs, NULL, &EmptyCustomAttributesCache, &t6314_TI, &t6314_0_0_0, &t6314_1_0_0, NULL, &t6314_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6313_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyDelaySignAttribute>
extern MethodInfo m32766_MI;
extern MethodInfo m32767_MI;
static PropertyInfo t6313____Item_PropertyInfo = 
{
	&t6313_TI, "Item", &m32766_MI, &m32767_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6313_PIs[] =
{
	&t6313____Item_PropertyInfo,
	NULL
};
extern Il2CppType t711_0_0_0;
static ParameterInfo t6313_m32768_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t711_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32768_GM;
MethodInfo m32768_MI = 
{
	"IndexOf", NULL, &t6313_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6313_m32768_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32768_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t711_0_0_0;
static ParameterInfo t6313_m32769_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t711_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32769_GM;
MethodInfo m32769_MI = 
{
	"Insert", NULL, &t6313_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6313_m32769_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32769_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6313_m32770_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32770_GM;
MethodInfo m32770_MI = 
{
	"RemoveAt", NULL, &t6313_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6313_m32770_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32770_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6313_m32766_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t711_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32766_GM;
MethodInfo m32766_MI = 
{
	"get_Item", NULL, &t6313_TI, &t711_0_0_0, RuntimeInvoker_t29_t44, t6313_m32766_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32766_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t711_0_0_0;
static ParameterInfo t6313_m32767_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t711_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32767_GM;
MethodInfo m32767_MI = 
{
	"set_Item", NULL, &t6313_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6313_m32767_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32767_GM};
static MethodInfo* t6313_MIs[] =
{
	&m32768_MI,
	&m32769_MI,
	&m32770_MI,
	&m32766_MI,
	&m32767_MI,
	NULL
};
static TypeInfo* t6313_ITIs[] = 
{
	&t603_TI,
	&t6312_TI,
	&t6314_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6313_0_0_0;
extern Il2CppType t6313_1_0_0;
struct t6313;
extern Il2CppGenericClass t6313_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6313_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6313_MIs, t6313_PIs, NULL, NULL, NULL, NULL, NULL, &t6313_TI, t6313_ITIs, NULL, &t1908__CustomAttributeCache, &t6313_TI, &t6313_0_0_0, &t6313_1_0_0, NULL, &t6313_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4847_TI;

#include "t430.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>
extern MethodInfo m32771_MI;
static PropertyInfo t4847____Current_PropertyInfo = 
{
	&t4847_TI, "Current", &m32771_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4847_PIs[] =
{
	&t4847____Current_PropertyInfo,
	NULL
};
extern Il2CppType t430_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32771_GM;
MethodInfo m32771_MI = 
{
	"get_Current", NULL, &t4847_TI, &t430_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32771_GM};
static MethodInfo* t4847_MIs[] =
{
	&m32771_MI,
	NULL
};
static TypeInfo* t4847_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4847_0_0_0;
extern Il2CppType t4847_1_0_0;
struct t4847;
extern Il2CppGenericClass t4847_GC;
TypeInfo t4847_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4847_MIs, t4847_PIs, NULL, NULL, NULL, NULL, NULL, &t4847_TI, t4847_ITIs, NULL, &EmptyCustomAttributesCache, &t4847_TI, &t4847_0_0_0, &t4847_1_0_0, NULL, &t4847_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3374.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3374_TI;
#include "t3374MD.h"

extern TypeInfo t430_TI;
extern MethodInfo m18746_MI;
extern MethodInfo m25081_MI;
struct t20;
#define m25081(__this, p0, method) (t430 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3374_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3374_TI, offsetof(t3374, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3374_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3374_TI, offsetof(t3374, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3374_FIs[] =
{
	&t3374_f0_FieldInfo,
	&t3374_f1_FieldInfo,
	NULL
};
extern MethodInfo m18743_MI;
static PropertyInfo t3374____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3374_TI, "System.Collections.IEnumerator.Current", &m18743_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3374____Current_PropertyInfo = 
{
	&t3374_TI, "Current", &m18746_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3374_PIs[] =
{
	&t3374____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3374____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3374_m18742_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18742_GM;
MethodInfo m18742_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3374_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3374_m18742_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18742_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18743_GM;
MethodInfo m18743_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3374_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18743_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18744_GM;
MethodInfo m18744_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3374_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18744_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18745_GM;
MethodInfo m18745_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3374_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18745_GM};
extern Il2CppType t430_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18746_GM;
MethodInfo m18746_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3374_TI, &t430_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18746_GM};
static MethodInfo* t3374_MIs[] =
{
	&m18742_MI,
	&m18743_MI,
	&m18744_MI,
	&m18745_MI,
	&m18746_MI,
	NULL
};
extern MethodInfo m18745_MI;
extern MethodInfo m18744_MI;
static MethodInfo* t3374_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18743_MI,
	&m18745_MI,
	&m18744_MI,
	&m18746_MI,
};
static TypeInfo* t3374_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4847_TI,
};
static Il2CppInterfaceOffsetPair t3374_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4847_TI, 7},
};
extern TypeInfo t430_TI;
static Il2CppRGCTXData t3374_RGCTXData[3] = 
{
	&m18746_MI/* Method Usage */,
	&t430_TI/* Class Usage */,
	&m25081_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3374_0_0_0;
extern Il2CppType t3374_1_0_0;
extern Il2CppGenericClass t3374_GC;
TypeInfo t3374_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3374_MIs, t3374_PIs, t3374_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3374_TI, t3374_ITIs, t3374_VT, &EmptyCustomAttributesCache, &t3374_TI, &t3374_0_0_0, &t3374_1_0_0, t3374_IOs, &t3374_GC, NULL, NULL, NULL, t3374_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3374)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6315_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>
extern MethodInfo m32772_MI;
static PropertyInfo t6315____Count_PropertyInfo = 
{
	&t6315_TI, "Count", &m32772_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32773_MI;
static PropertyInfo t6315____IsReadOnly_PropertyInfo = 
{
	&t6315_TI, "IsReadOnly", &m32773_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6315_PIs[] =
{
	&t6315____Count_PropertyInfo,
	&t6315____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32772_GM;
MethodInfo m32772_MI = 
{
	"get_Count", NULL, &t6315_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32772_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32773_GM;
MethodInfo m32773_MI = 
{
	"get_IsReadOnly", NULL, &t6315_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32773_GM};
extern Il2CppType t430_0_0_0;
extern Il2CppType t430_0_0_0;
static ParameterInfo t6315_m32774_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t430_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32774_GM;
MethodInfo m32774_MI = 
{
	"Add", NULL, &t6315_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6315_m32774_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32774_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32775_GM;
MethodInfo m32775_MI = 
{
	"Clear", NULL, &t6315_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32775_GM};
extern Il2CppType t430_0_0_0;
static ParameterInfo t6315_m32776_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t430_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32776_GM;
MethodInfo m32776_MI = 
{
	"Contains", NULL, &t6315_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6315_m32776_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32776_GM};
extern Il2CppType t3632_0_0_0;
extern Il2CppType t3632_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6315_m32777_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3632_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32777_GM;
MethodInfo m32777_MI = 
{
	"CopyTo", NULL, &t6315_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6315_m32777_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32777_GM};
extern Il2CppType t430_0_0_0;
static ParameterInfo t6315_m32778_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t430_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32778_GM;
MethodInfo m32778_MI = 
{
	"Remove", NULL, &t6315_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6315_m32778_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32778_GM};
static MethodInfo* t6315_MIs[] =
{
	&m32772_MI,
	&m32773_MI,
	&m32774_MI,
	&m32775_MI,
	&m32776_MI,
	&m32777_MI,
	&m32778_MI,
	NULL
};
extern TypeInfo t6317_TI;
static TypeInfo* t6315_ITIs[] = 
{
	&t603_TI,
	&t6317_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6315_0_0_0;
extern Il2CppType t6315_1_0_0;
struct t6315;
extern Il2CppGenericClass t6315_GC;
TypeInfo t6315_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6315_MIs, t6315_PIs, NULL, NULL, NULL, NULL, NULL, &t6315_TI, t6315_ITIs, NULL, &EmptyCustomAttributesCache, &t6315_TI, &t6315_0_0_0, &t6315_1_0_0, NULL, &t6315_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyDescriptionAttribute>
extern Il2CppType t4847_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32779_GM;
MethodInfo m32779_MI = 
{
	"GetEnumerator", NULL, &t6317_TI, &t4847_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32779_GM};
static MethodInfo* t6317_MIs[] =
{
	&m32779_MI,
	NULL
};
static TypeInfo* t6317_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6317_0_0_0;
extern Il2CppType t6317_1_0_0;
struct t6317;
extern Il2CppGenericClass t6317_GC;
TypeInfo t6317_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6317_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6317_TI, t6317_ITIs, NULL, &EmptyCustomAttributesCache, &t6317_TI, &t6317_0_0_0, &t6317_1_0_0, NULL, &t6317_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6316_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyDescriptionAttribute>
extern MethodInfo m32780_MI;
extern MethodInfo m32781_MI;
static PropertyInfo t6316____Item_PropertyInfo = 
{
	&t6316_TI, "Item", &m32780_MI, &m32781_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6316_PIs[] =
{
	&t6316____Item_PropertyInfo,
	NULL
};
extern Il2CppType t430_0_0_0;
static ParameterInfo t6316_m32782_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t430_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32782_GM;
MethodInfo m32782_MI = 
{
	"IndexOf", NULL, &t6316_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6316_m32782_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32782_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t430_0_0_0;
static ParameterInfo t6316_m32783_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t430_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32783_GM;
MethodInfo m32783_MI = 
{
	"Insert", NULL, &t6316_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6316_m32783_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32783_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6316_m32784_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32784_GM;
MethodInfo m32784_MI = 
{
	"RemoveAt", NULL, &t6316_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6316_m32784_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32784_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6316_m32780_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t430_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32780_GM;
MethodInfo m32780_MI = 
{
	"get_Item", NULL, &t6316_TI, &t430_0_0_0, RuntimeInvoker_t29_t44, t6316_m32780_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32780_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t430_0_0_0;
static ParameterInfo t6316_m32781_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t430_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32781_GM;
MethodInfo m32781_MI = 
{
	"set_Item", NULL, &t6316_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6316_m32781_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32781_GM};
static MethodInfo* t6316_MIs[] =
{
	&m32782_MI,
	&m32783_MI,
	&m32784_MI,
	&m32780_MI,
	&m32781_MI,
	NULL
};
static TypeInfo* t6316_ITIs[] = 
{
	&t603_TI,
	&t6315_TI,
	&t6317_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6316_0_0_0;
extern Il2CppType t6316_1_0_0;
struct t6316;
extern Il2CppGenericClass t6316_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6316_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6316_MIs, t6316_PIs, NULL, NULL, NULL, NULL, NULL, &t6316_TI, t6316_ITIs, NULL, &t1908__CustomAttributeCache, &t6316_TI, &t6316_0_0_0, &t6316_1_0_0, NULL, &t6316_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4849_TI;

#include "t435.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>
extern MethodInfo m32785_MI;
static PropertyInfo t4849____Current_PropertyInfo = 
{
	&t4849_TI, "Current", &m32785_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4849_PIs[] =
{
	&t4849____Current_PropertyInfo,
	NULL
};
extern Il2CppType t435_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32785_GM;
MethodInfo m32785_MI = 
{
	"get_Current", NULL, &t4849_TI, &t435_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32785_GM};
static MethodInfo* t4849_MIs[] =
{
	&m32785_MI,
	NULL
};
static TypeInfo* t4849_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4849_0_0_0;
extern Il2CppType t4849_1_0_0;
struct t4849;
extern Il2CppGenericClass t4849_GC;
TypeInfo t4849_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4849_MIs, t4849_PIs, NULL, NULL, NULL, NULL, NULL, &t4849_TI, t4849_ITIs, NULL, &EmptyCustomAttributesCache, &t4849_TI, &t4849_0_0_0, &t4849_1_0_0, NULL, &t4849_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3375.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3375_TI;
#include "t3375MD.h"

extern TypeInfo t435_TI;
extern MethodInfo m18751_MI;
extern MethodInfo m25092_MI;
struct t20;
#define m25092(__this, p0, method) (t435 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3375_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3375_TI, offsetof(t3375, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3375_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3375_TI, offsetof(t3375, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3375_FIs[] =
{
	&t3375_f0_FieldInfo,
	&t3375_f1_FieldInfo,
	NULL
};
extern MethodInfo m18748_MI;
static PropertyInfo t3375____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3375_TI, "System.Collections.IEnumerator.Current", &m18748_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3375____Current_PropertyInfo = 
{
	&t3375_TI, "Current", &m18751_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3375_PIs[] =
{
	&t3375____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3375____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3375_m18747_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18747_GM;
MethodInfo m18747_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3375_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3375_m18747_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18747_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18748_GM;
MethodInfo m18748_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3375_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18748_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18749_GM;
MethodInfo m18749_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3375_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18749_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18750_GM;
MethodInfo m18750_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3375_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18750_GM};
extern Il2CppType t435_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18751_GM;
MethodInfo m18751_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3375_TI, &t435_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18751_GM};
static MethodInfo* t3375_MIs[] =
{
	&m18747_MI,
	&m18748_MI,
	&m18749_MI,
	&m18750_MI,
	&m18751_MI,
	NULL
};
extern MethodInfo m18750_MI;
extern MethodInfo m18749_MI;
static MethodInfo* t3375_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18748_MI,
	&m18750_MI,
	&m18749_MI,
	&m18751_MI,
};
static TypeInfo* t3375_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4849_TI,
};
static Il2CppInterfaceOffsetPair t3375_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4849_TI, 7},
};
extern TypeInfo t435_TI;
static Il2CppRGCTXData t3375_RGCTXData[3] = 
{
	&m18751_MI/* Method Usage */,
	&t435_TI/* Class Usage */,
	&m25092_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3375_0_0_0;
extern Il2CppType t3375_1_0_0;
extern Il2CppGenericClass t3375_GC;
TypeInfo t3375_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3375_MIs, t3375_PIs, t3375_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3375_TI, t3375_ITIs, t3375_VT, &EmptyCustomAttributesCache, &t3375_TI, &t3375_0_0_0, &t3375_1_0_0, t3375_IOs, &t3375_GC, NULL, NULL, NULL, t3375_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3375)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6318_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>
extern MethodInfo m32786_MI;
static PropertyInfo t6318____Count_PropertyInfo = 
{
	&t6318_TI, "Count", &m32786_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32787_MI;
static PropertyInfo t6318____IsReadOnly_PropertyInfo = 
{
	&t6318_TI, "IsReadOnly", &m32787_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6318_PIs[] =
{
	&t6318____Count_PropertyInfo,
	&t6318____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32786_GM;
MethodInfo m32786_MI = 
{
	"get_Count", NULL, &t6318_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32786_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32787_GM;
MethodInfo m32787_MI = 
{
	"get_IsReadOnly", NULL, &t6318_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32787_GM};
extern Il2CppType t435_0_0_0;
extern Il2CppType t435_0_0_0;
static ParameterInfo t6318_m32788_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t435_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32788_GM;
MethodInfo m32788_MI = 
{
	"Add", NULL, &t6318_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6318_m32788_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32788_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32789_GM;
MethodInfo m32789_MI = 
{
	"Clear", NULL, &t6318_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32789_GM};
extern Il2CppType t435_0_0_0;
static ParameterInfo t6318_m32790_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t435_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32790_GM;
MethodInfo m32790_MI = 
{
	"Contains", NULL, &t6318_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6318_m32790_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32790_GM};
extern Il2CppType t3633_0_0_0;
extern Il2CppType t3633_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6318_m32791_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3633_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32791_GM;
MethodInfo m32791_MI = 
{
	"CopyTo", NULL, &t6318_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6318_m32791_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32791_GM};
extern Il2CppType t435_0_0_0;
static ParameterInfo t6318_m32792_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t435_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32792_GM;
MethodInfo m32792_MI = 
{
	"Remove", NULL, &t6318_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6318_m32792_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32792_GM};
static MethodInfo* t6318_MIs[] =
{
	&m32786_MI,
	&m32787_MI,
	&m32788_MI,
	&m32789_MI,
	&m32790_MI,
	&m32791_MI,
	&m32792_MI,
	NULL
};
extern TypeInfo t6320_TI;
static TypeInfo* t6318_ITIs[] = 
{
	&t603_TI,
	&t6320_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6318_0_0_0;
extern Il2CppType t6318_1_0_0;
struct t6318;
extern Il2CppGenericClass t6318_GC;
TypeInfo t6318_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6318_MIs, t6318_PIs, NULL, NULL, NULL, NULL, NULL, &t6318_TI, t6318_ITIs, NULL, &EmptyCustomAttributesCache, &t6318_TI, &t6318_0_0_0, &t6318_1_0_0, NULL, &t6318_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyFileVersionAttribute>
extern Il2CppType t4849_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32793_GM;
MethodInfo m32793_MI = 
{
	"GetEnumerator", NULL, &t6320_TI, &t4849_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32793_GM};
static MethodInfo* t6320_MIs[] =
{
	&m32793_MI,
	NULL
};
static TypeInfo* t6320_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6320_0_0_0;
extern Il2CppType t6320_1_0_0;
struct t6320;
extern Il2CppGenericClass t6320_GC;
TypeInfo t6320_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6320_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6320_TI, t6320_ITIs, NULL, &EmptyCustomAttributesCache, &t6320_TI, &t6320_0_0_0, &t6320_1_0_0, NULL, &t6320_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6319_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyFileVersionAttribute>
extern MethodInfo m32794_MI;
extern MethodInfo m32795_MI;
static PropertyInfo t6319____Item_PropertyInfo = 
{
	&t6319_TI, "Item", &m32794_MI, &m32795_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6319_PIs[] =
{
	&t6319____Item_PropertyInfo,
	NULL
};
extern Il2CppType t435_0_0_0;
static ParameterInfo t6319_m32796_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t435_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32796_GM;
MethodInfo m32796_MI = 
{
	"IndexOf", NULL, &t6319_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6319_m32796_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32796_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t435_0_0_0;
static ParameterInfo t6319_m32797_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t435_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32797_GM;
MethodInfo m32797_MI = 
{
	"Insert", NULL, &t6319_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6319_m32797_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32797_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6319_m32798_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32798_GM;
MethodInfo m32798_MI = 
{
	"RemoveAt", NULL, &t6319_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6319_m32798_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32798_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6319_m32794_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t435_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32794_GM;
MethodInfo m32794_MI = 
{
	"get_Item", NULL, &t6319_TI, &t435_0_0_0, RuntimeInvoker_t29_t44, t6319_m32794_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32794_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t435_0_0_0;
static ParameterInfo t6319_m32795_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t435_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32795_GM;
MethodInfo m32795_MI = 
{
	"set_Item", NULL, &t6319_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6319_m32795_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32795_GM};
static MethodInfo* t6319_MIs[] =
{
	&m32796_MI,
	&m32797_MI,
	&m32798_MI,
	&m32794_MI,
	&m32795_MI,
	NULL
};
static TypeInfo* t6319_ITIs[] = 
{
	&t603_TI,
	&t6318_TI,
	&t6320_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6319_0_0_0;
extern Il2CppType t6319_1_0_0;
struct t6319;
extern Il2CppGenericClass t6319_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6319_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6319_MIs, t6319_PIs, NULL, NULL, NULL, NULL, NULL, &t6319_TI, t6319_ITIs, NULL, &t1908__CustomAttributeCache, &t6319_TI, &t6319_0_0_0, &t6319_1_0_0, NULL, &t6319_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4851_TI;

#include "t705.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>
extern MethodInfo m32799_MI;
static PropertyInfo t4851____Current_PropertyInfo = 
{
	&t4851_TI, "Current", &m32799_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4851_PIs[] =
{
	&t4851____Current_PropertyInfo,
	NULL
};
extern Il2CppType t705_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32799_GM;
MethodInfo m32799_MI = 
{
	"get_Current", NULL, &t4851_TI, &t705_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32799_GM};
static MethodInfo* t4851_MIs[] =
{
	&m32799_MI,
	NULL
};
static TypeInfo* t4851_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4851_0_0_0;
extern Il2CppType t4851_1_0_0;
struct t4851;
extern Il2CppGenericClass t4851_GC;
TypeInfo t4851_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4851_MIs, t4851_PIs, NULL, NULL, NULL, NULL, NULL, &t4851_TI, t4851_ITIs, NULL, &EmptyCustomAttributesCache, &t4851_TI, &t4851_0_0_0, &t4851_1_0_0, NULL, &t4851_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3376.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3376_TI;
#include "t3376MD.h"

extern TypeInfo t705_TI;
extern MethodInfo m18756_MI;
extern MethodInfo m25103_MI;
struct t20;
#define m25103(__this, p0, method) (t705 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3376_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3376_TI, offsetof(t3376, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3376_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3376_TI, offsetof(t3376, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3376_FIs[] =
{
	&t3376_f0_FieldInfo,
	&t3376_f1_FieldInfo,
	NULL
};
extern MethodInfo m18753_MI;
static PropertyInfo t3376____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3376_TI, "System.Collections.IEnumerator.Current", &m18753_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3376____Current_PropertyInfo = 
{
	&t3376_TI, "Current", &m18756_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3376_PIs[] =
{
	&t3376____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3376____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3376_m18752_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18752_GM;
MethodInfo m18752_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3376_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3376_m18752_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18752_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18753_GM;
MethodInfo m18753_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3376_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18753_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18754_GM;
MethodInfo m18754_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3376_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18754_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18755_GM;
MethodInfo m18755_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3376_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18755_GM};
extern Il2CppType t705_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18756_GM;
MethodInfo m18756_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3376_TI, &t705_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18756_GM};
static MethodInfo* t3376_MIs[] =
{
	&m18752_MI,
	&m18753_MI,
	&m18754_MI,
	&m18755_MI,
	&m18756_MI,
	NULL
};
extern MethodInfo m18755_MI;
extern MethodInfo m18754_MI;
static MethodInfo* t3376_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18753_MI,
	&m18755_MI,
	&m18754_MI,
	&m18756_MI,
};
static TypeInfo* t3376_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4851_TI,
};
static Il2CppInterfaceOffsetPair t3376_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4851_TI, 7},
};
extern TypeInfo t705_TI;
static Il2CppRGCTXData t3376_RGCTXData[3] = 
{
	&m18756_MI/* Method Usage */,
	&t705_TI/* Class Usage */,
	&m25103_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3376_0_0_0;
extern Il2CppType t3376_1_0_0;
extern Il2CppGenericClass t3376_GC;
TypeInfo t3376_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3376_MIs, t3376_PIs, t3376_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3376_TI, t3376_ITIs, t3376_VT, &EmptyCustomAttributesCache, &t3376_TI, &t3376_0_0_0, &t3376_1_0_0, t3376_IOs, &t3376_GC, NULL, NULL, NULL, t3376_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3376)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6321_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>
extern MethodInfo m32800_MI;
static PropertyInfo t6321____Count_PropertyInfo = 
{
	&t6321_TI, "Count", &m32800_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32801_MI;
static PropertyInfo t6321____IsReadOnly_PropertyInfo = 
{
	&t6321_TI, "IsReadOnly", &m32801_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6321_PIs[] =
{
	&t6321____Count_PropertyInfo,
	&t6321____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32800_GM;
MethodInfo m32800_MI = 
{
	"get_Count", NULL, &t6321_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32800_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32801_GM;
MethodInfo m32801_MI = 
{
	"get_IsReadOnly", NULL, &t6321_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32801_GM};
extern Il2CppType t705_0_0_0;
extern Il2CppType t705_0_0_0;
static ParameterInfo t6321_m32802_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t705_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32802_GM;
MethodInfo m32802_MI = 
{
	"Add", NULL, &t6321_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6321_m32802_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32802_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32803_GM;
MethodInfo m32803_MI = 
{
	"Clear", NULL, &t6321_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32803_GM};
extern Il2CppType t705_0_0_0;
static ParameterInfo t6321_m32804_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t705_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32804_GM;
MethodInfo m32804_MI = 
{
	"Contains", NULL, &t6321_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6321_m32804_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32804_GM};
extern Il2CppType t3634_0_0_0;
extern Il2CppType t3634_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6321_m32805_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3634_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32805_GM;
MethodInfo m32805_MI = 
{
	"CopyTo", NULL, &t6321_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6321_m32805_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32805_GM};
extern Il2CppType t705_0_0_0;
static ParameterInfo t6321_m32806_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t705_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32806_GM;
MethodInfo m32806_MI = 
{
	"Remove", NULL, &t6321_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6321_m32806_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32806_GM};
static MethodInfo* t6321_MIs[] =
{
	&m32800_MI,
	&m32801_MI,
	&m32802_MI,
	&m32803_MI,
	&m32804_MI,
	&m32805_MI,
	&m32806_MI,
	NULL
};
extern TypeInfo t6323_TI;
static TypeInfo* t6321_ITIs[] = 
{
	&t603_TI,
	&t6323_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6321_0_0_0;
extern Il2CppType t6321_1_0_0;
struct t6321;
extern Il2CppGenericClass t6321_GC;
TypeInfo t6321_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6321_MIs, t6321_PIs, NULL, NULL, NULL, NULL, NULL, &t6321_TI, t6321_ITIs, NULL, &EmptyCustomAttributesCache, &t6321_TI, &t6321_0_0_0, &t6321_1_0_0, NULL, &t6321_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyInformationalVersionAttribute>
extern Il2CppType t4851_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32807_GM;
MethodInfo m32807_MI = 
{
	"GetEnumerator", NULL, &t6323_TI, &t4851_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32807_GM};
static MethodInfo* t6323_MIs[] =
{
	&m32807_MI,
	NULL
};
static TypeInfo* t6323_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6323_0_0_0;
extern Il2CppType t6323_1_0_0;
struct t6323;
extern Il2CppGenericClass t6323_GC;
TypeInfo t6323_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6323_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6323_TI, t6323_ITIs, NULL, &EmptyCustomAttributesCache, &t6323_TI, &t6323_0_0_0, &t6323_1_0_0, NULL, &t6323_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6322_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyInformationalVersionAttribute>
extern MethodInfo m32808_MI;
extern MethodInfo m32809_MI;
static PropertyInfo t6322____Item_PropertyInfo = 
{
	&t6322_TI, "Item", &m32808_MI, &m32809_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6322_PIs[] =
{
	&t6322____Item_PropertyInfo,
	NULL
};
extern Il2CppType t705_0_0_0;
static ParameterInfo t6322_m32810_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t705_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32810_GM;
MethodInfo m32810_MI = 
{
	"IndexOf", NULL, &t6322_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6322_m32810_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32810_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t705_0_0_0;
static ParameterInfo t6322_m32811_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t705_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32811_GM;
MethodInfo m32811_MI = 
{
	"Insert", NULL, &t6322_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6322_m32811_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32811_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6322_m32812_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32812_GM;
MethodInfo m32812_MI = 
{
	"RemoveAt", NULL, &t6322_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6322_m32812_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32812_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6322_m32808_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t705_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32808_GM;
MethodInfo m32808_MI = 
{
	"get_Item", NULL, &t6322_TI, &t705_0_0_0, RuntimeInvoker_t29_t44, t6322_m32808_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32808_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t705_0_0_0;
static ParameterInfo t6322_m32809_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t705_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32809_GM;
MethodInfo m32809_MI = 
{
	"set_Item", NULL, &t6322_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6322_m32809_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32809_GM};
static MethodInfo* t6322_MIs[] =
{
	&m32810_MI,
	&m32811_MI,
	&m32812_MI,
	&m32808_MI,
	&m32809_MI,
	NULL
};
static TypeInfo* t6322_ITIs[] = 
{
	&t603_TI,
	&t6321_TI,
	&t6323_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6322_0_0_0;
extern Il2CppType t6322_1_0_0;
struct t6322;
extern Il2CppGenericClass t6322_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6322_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6322_MIs, t6322_PIs, NULL, NULL, NULL, NULL, NULL, &t6322_TI, t6322_ITIs, NULL, &t1908__CustomAttributeCache, &t6322_TI, &t6322_0_0_0, &t6322_1_0_0, NULL, &t6322_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4853_TI;

#include "t710.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>
extern MethodInfo m32813_MI;
static PropertyInfo t4853____Current_PropertyInfo = 
{
	&t4853_TI, "Current", &m32813_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4853_PIs[] =
{
	&t4853____Current_PropertyInfo,
	NULL
};
extern Il2CppType t710_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32813_GM;
MethodInfo m32813_MI = 
{
	"get_Current", NULL, &t4853_TI, &t710_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32813_GM};
static MethodInfo* t4853_MIs[] =
{
	&m32813_MI,
	NULL
};
static TypeInfo* t4853_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4853_0_0_0;
extern Il2CppType t4853_1_0_0;
struct t4853;
extern Il2CppGenericClass t4853_GC;
TypeInfo t4853_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4853_MIs, t4853_PIs, NULL, NULL, NULL, NULL, NULL, &t4853_TI, t4853_ITIs, NULL, &EmptyCustomAttributesCache, &t4853_TI, &t4853_0_0_0, &t4853_1_0_0, NULL, &t4853_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3377.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3377_TI;
#include "t3377MD.h"

extern TypeInfo t710_TI;
extern MethodInfo m18761_MI;
extern MethodInfo m25114_MI;
struct t20;
#define m25114(__this, p0, method) (t710 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3377_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3377_TI, offsetof(t3377, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3377_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3377_TI, offsetof(t3377, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3377_FIs[] =
{
	&t3377_f0_FieldInfo,
	&t3377_f1_FieldInfo,
	NULL
};
extern MethodInfo m18758_MI;
static PropertyInfo t3377____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3377_TI, "System.Collections.IEnumerator.Current", &m18758_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3377____Current_PropertyInfo = 
{
	&t3377_TI, "Current", &m18761_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3377_PIs[] =
{
	&t3377____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3377____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3377_m18757_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18757_GM;
MethodInfo m18757_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3377_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3377_m18757_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18757_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18758_GM;
MethodInfo m18758_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3377_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18758_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18759_GM;
MethodInfo m18759_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3377_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18759_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18760_GM;
MethodInfo m18760_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3377_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18760_GM};
extern Il2CppType t710_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18761_GM;
MethodInfo m18761_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3377_TI, &t710_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18761_GM};
static MethodInfo* t3377_MIs[] =
{
	&m18757_MI,
	&m18758_MI,
	&m18759_MI,
	&m18760_MI,
	&m18761_MI,
	NULL
};
extern MethodInfo m18760_MI;
extern MethodInfo m18759_MI;
static MethodInfo* t3377_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18758_MI,
	&m18760_MI,
	&m18759_MI,
	&m18761_MI,
};
static TypeInfo* t3377_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4853_TI,
};
static Il2CppInterfaceOffsetPair t3377_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4853_TI, 7},
};
extern TypeInfo t710_TI;
static Il2CppRGCTXData t3377_RGCTXData[3] = 
{
	&m18761_MI/* Method Usage */,
	&t710_TI/* Class Usage */,
	&m25114_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3377_0_0_0;
extern Il2CppType t3377_1_0_0;
extern Il2CppGenericClass t3377_GC;
TypeInfo t3377_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3377_MIs, t3377_PIs, t3377_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3377_TI, t3377_ITIs, t3377_VT, &EmptyCustomAttributesCache, &t3377_TI, &t3377_0_0_0, &t3377_1_0_0, t3377_IOs, &t3377_GC, NULL, NULL, NULL, t3377_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3377)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6324_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>
extern MethodInfo m32814_MI;
static PropertyInfo t6324____Count_PropertyInfo = 
{
	&t6324_TI, "Count", &m32814_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32815_MI;
static PropertyInfo t6324____IsReadOnly_PropertyInfo = 
{
	&t6324_TI, "IsReadOnly", &m32815_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6324_PIs[] =
{
	&t6324____Count_PropertyInfo,
	&t6324____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32814_GM;
MethodInfo m32814_MI = 
{
	"get_Count", NULL, &t6324_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32814_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32815_GM;
MethodInfo m32815_MI = 
{
	"get_IsReadOnly", NULL, &t6324_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32815_GM};
extern Il2CppType t710_0_0_0;
extern Il2CppType t710_0_0_0;
static ParameterInfo t6324_m32816_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t710_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32816_GM;
MethodInfo m32816_MI = 
{
	"Add", NULL, &t6324_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6324_m32816_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32816_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32817_GM;
MethodInfo m32817_MI = 
{
	"Clear", NULL, &t6324_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32817_GM};
extern Il2CppType t710_0_0_0;
static ParameterInfo t6324_m32818_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t710_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32818_GM;
MethodInfo m32818_MI = 
{
	"Contains", NULL, &t6324_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6324_m32818_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32818_GM};
extern Il2CppType t3635_0_0_0;
extern Il2CppType t3635_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6324_m32819_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3635_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32819_GM;
MethodInfo m32819_MI = 
{
	"CopyTo", NULL, &t6324_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6324_m32819_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32819_GM};
extern Il2CppType t710_0_0_0;
static ParameterInfo t6324_m32820_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t710_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32820_GM;
MethodInfo m32820_MI = 
{
	"Remove", NULL, &t6324_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6324_m32820_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32820_GM};
static MethodInfo* t6324_MIs[] =
{
	&m32814_MI,
	&m32815_MI,
	&m32816_MI,
	&m32817_MI,
	&m32818_MI,
	&m32819_MI,
	&m32820_MI,
	NULL
};
extern TypeInfo t6326_TI;
static TypeInfo* t6324_ITIs[] = 
{
	&t603_TI,
	&t6326_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6324_0_0_0;
extern Il2CppType t6324_1_0_0;
struct t6324;
extern Il2CppGenericClass t6324_GC;
TypeInfo t6324_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6324_MIs, t6324_PIs, NULL, NULL, NULL, NULL, NULL, &t6324_TI, t6324_ITIs, NULL, &EmptyCustomAttributesCache, &t6324_TI, &t6324_0_0_0, &t6324_1_0_0, NULL, &t6324_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyKeyFileAttribute>
extern Il2CppType t4853_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32821_GM;
MethodInfo m32821_MI = 
{
	"GetEnumerator", NULL, &t6326_TI, &t4853_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32821_GM};
static MethodInfo* t6326_MIs[] =
{
	&m32821_MI,
	NULL
};
static TypeInfo* t6326_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6326_0_0_0;
extern Il2CppType t6326_1_0_0;
struct t6326;
extern Il2CppGenericClass t6326_GC;
TypeInfo t6326_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6326_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6326_TI, t6326_ITIs, NULL, &EmptyCustomAttributesCache, &t6326_TI, &t6326_0_0_0, &t6326_1_0_0, NULL, &t6326_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6325_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyKeyFileAttribute>
extern MethodInfo m32822_MI;
extern MethodInfo m32823_MI;
static PropertyInfo t6325____Item_PropertyInfo = 
{
	&t6325_TI, "Item", &m32822_MI, &m32823_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6325_PIs[] =
{
	&t6325____Item_PropertyInfo,
	NULL
};
extern Il2CppType t710_0_0_0;
static ParameterInfo t6325_m32824_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t710_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32824_GM;
MethodInfo m32824_MI = 
{
	"IndexOf", NULL, &t6325_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6325_m32824_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32824_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t710_0_0_0;
static ParameterInfo t6325_m32825_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t710_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32825_GM;
MethodInfo m32825_MI = 
{
	"Insert", NULL, &t6325_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6325_m32825_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32825_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6325_m32826_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32826_GM;
MethodInfo m32826_MI = 
{
	"RemoveAt", NULL, &t6325_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6325_m32826_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32826_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6325_m32822_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t710_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32822_GM;
MethodInfo m32822_MI = 
{
	"get_Item", NULL, &t6325_TI, &t710_0_0_0, RuntimeInvoker_t29_t44, t6325_m32822_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32822_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t710_0_0_0;
static ParameterInfo t6325_m32823_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t710_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32823_GM;
MethodInfo m32823_MI = 
{
	"set_Item", NULL, &t6325_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6325_m32823_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32823_GM};
static MethodInfo* t6325_MIs[] =
{
	&m32824_MI,
	&m32825_MI,
	&m32826_MI,
	&m32822_MI,
	&m32823_MI,
	NULL
};
static TypeInfo* t6325_ITIs[] = 
{
	&t603_TI,
	&t6324_TI,
	&t6326_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6325_0_0_0;
extern Il2CppType t6325_1_0_0;
struct t6325;
extern Il2CppGenericClass t6325_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6325_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6325_MIs, t6325_PIs, NULL, NULL, NULL, NULL, NULL, &t6325_TI, t6325_ITIs, NULL, &t1908__CustomAttributeCache, &t6325_TI, &t6325_0_0_0, &t6325_1_0_0, NULL, &t6325_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4855_TI;

#include "t1362.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyNameFlags>
extern MethodInfo m32827_MI;
static PropertyInfo t4855____Current_PropertyInfo = 
{
	&t4855_TI, "Current", &m32827_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4855_PIs[] =
{
	&t4855____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1362_0_0_0;
extern void* RuntimeInvoker_t1362 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32827_GM;
MethodInfo m32827_MI = 
{
	"get_Current", NULL, &t4855_TI, &t1362_0_0_0, RuntimeInvoker_t1362, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32827_GM};
static MethodInfo* t4855_MIs[] =
{
	&m32827_MI,
	NULL
};
static TypeInfo* t4855_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4855_0_0_0;
extern Il2CppType t4855_1_0_0;
struct t4855;
extern Il2CppGenericClass t4855_GC;
TypeInfo t4855_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4855_MIs, t4855_PIs, NULL, NULL, NULL, NULL, NULL, &t4855_TI, t4855_ITIs, NULL, &EmptyCustomAttributesCache, &t4855_TI, &t4855_0_0_0, &t4855_1_0_0, NULL, &t4855_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3378.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3378_TI;
#include "t3378MD.h"

extern TypeInfo t1362_TI;
extern MethodInfo m18766_MI;
extern MethodInfo m25125_MI;
struct t20;
 int32_t m25125 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18762_MI;
 void m18762 (t3378 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18763_MI;
 t29 * m18763 (t3378 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18766(__this, &m18766_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t1362_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18764_MI;
 void m18764 (t3378 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18765_MI;
 bool m18765 (t3378 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18766 (t3378 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25125(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25125_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3378_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3378_TI, offsetof(t3378, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3378_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3378_TI, offsetof(t3378, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3378_FIs[] =
{
	&t3378_f0_FieldInfo,
	&t3378_f1_FieldInfo,
	NULL
};
static PropertyInfo t3378____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3378_TI, "System.Collections.IEnumerator.Current", &m18763_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3378____Current_PropertyInfo = 
{
	&t3378_TI, "Current", &m18766_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3378_PIs[] =
{
	&t3378____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3378____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3378_m18762_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18762_GM;
MethodInfo m18762_MI = 
{
	".ctor", (methodPointerType)&m18762, &t3378_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3378_m18762_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18762_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18763_GM;
MethodInfo m18763_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18763, &t3378_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18763_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18764_GM;
MethodInfo m18764_MI = 
{
	"Dispose", (methodPointerType)&m18764, &t3378_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18764_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18765_GM;
MethodInfo m18765_MI = 
{
	"MoveNext", (methodPointerType)&m18765, &t3378_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18765_GM};
extern Il2CppType t1362_0_0_0;
extern void* RuntimeInvoker_t1362 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18766_GM;
MethodInfo m18766_MI = 
{
	"get_Current", (methodPointerType)&m18766, &t3378_TI, &t1362_0_0_0, RuntimeInvoker_t1362, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18766_GM};
static MethodInfo* t3378_MIs[] =
{
	&m18762_MI,
	&m18763_MI,
	&m18764_MI,
	&m18765_MI,
	&m18766_MI,
	NULL
};
static MethodInfo* t3378_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18763_MI,
	&m18765_MI,
	&m18764_MI,
	&m18766_MI,
};
static TypeInfo* t3378_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4855_TI,
};
static Il2CppInterfaceOffsetPair t3378_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4855_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3378_0_0_0;
extern Il2CppType t3378_1_0_0;
extern Il2CppGenericClass t3378_GC;
TypeInfo t3378_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3378_MIs, t3378_PIs, t3378_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3378_TI, t3378_ITIs, t3378_VT, &EmptyCustomAttributesCache, &t3378_TI, &t3378_0_0_0, &t3378_1_0_0, t3378_IOs, &t3378_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3378)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6327_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>
extern MethodInfo m32828_MI;
static PropertyInfo t6327____Count_PropertyInfo = 
{
	&t6327_TI, "Count", &m32828_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32829_MI;
static PropertyInfo t6327____IsReadOnly_PropertyInfo = 
{
	&t6327_TI, "IsReadOnly", &m32829_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6327_PIs[] =
{
	&t6327____Count_PropertyInfo,
	&t6327____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32828_GM;
MethodInfo m32828_MI = 
{
	"get_Count", NULL, &t6327_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32828_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32829_GM;
MethodInfo m32829_MI = 
{
	"get_IsReadOnly", NULL, &t6327_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32829_GM};
extern Il2CppType t1362_0_0_0;
extern Il2CppType t1362_0_0_0;
static ParameterInfo t6327_m32830_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1362_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32830_GM;
MethodInfo m32830_MI = 
{
	"Add", NULL, &t6327_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6327_m32830_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32830_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32831_GM;
MethodInfo m32831_MI = 
{
	"Clear", NULL, &t6327_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32831_GM};
extern Il2CppType t1362_0_0_0;
static ParameterInfo t6327_m32832_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1362_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32832_GM;
MethodInfo m32832_MI = 
{
	"Contains", NULL, &t6327_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6327_m32832_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32832_GM};
extern Il2CppType t3636_0_0_0;
extern Il2CppType t3636_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6327_m32833_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3636_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32833_GM;
MethodInfo m32833_MI = 
{
	"CopyTo", NULL, &t6327_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6327_m32833_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32833_GM};
extern Il2CppType t1362_0_0_0;
static ParameterInfo t6327_m32834_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1362_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32834_GM;
MethodInfo m32834_MI = 
{
	"Remove", NULL, &t6327_TI, &t40_0_0_0, RuntimeInvoker_t40_t44, t6327_m32834_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32834_GM};
static MethodInfo* t6327_MIs[] =
{
	&m32828_MI,
	&m32829_MI,
	&m32830_MI,
	&m32831_MI,
	&m32832_MI,
	&m32833_MI,
	&m32834_MI,
	NULL
};
extern TypeInfo t6329_TI;
static TypeInfo* t6327_ITIs[] = 
{
	&t603_TI,
	&t6329_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6327_0_0_0;
extern Il2CppType t6327_1_0_0;
struct t6327;
extern Il2CppGenericClass t6327_GC;
TypeInfo t6327_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6327_MIs, t6327_PIs, NULL, NULL, NULL, NULL, NULL, &t6327_TI, t6327_ITIs, NULL, &EmptyCustomAttributesCache, &t6327_TI, &t6327_0_0_0, &t6327_1_0_0, NULL, &t6327_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyNameFlags>
extern Il2CppType t4855_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32835_GM;
MethodInfo m32835_MI = 
{
	"GetEnumerator", NULL, &t6329_TI, &t4855_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32835_GM};
static MethodInfo* t6329_MIs[] =
{
	&m32835_MI,
	NULL
};
static TypeInfo* t6329_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6329_0_0_0;
extern Il2CppType t6329_1_0_0;
struct t6329;
extern Il2CppGenericClass t6329_GC;
TypeInfo t6329_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6329_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6329_TI, t6329_ITIs, NULL, &EmptyCustomAttributesCache, &t6329_TI, &t6329_0_0_0, &t6329_1_0_0, NULL, &t6329_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6328_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>
extern MethodInfo m32836_MI;
extern MethodInfo m32837_MI;
static PropertyInfo t6328____Item_PropertyInfo = 
{
	&t6328_TI, "Item", &m32836_MI, &m32837_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6328_PIs[] =
{
	&t6328____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1362_0_0_0;
static ParameterInfo t6328_m32838_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1362_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32838_GM;
MethodInfo m32838_MI = 
{
	"IndexOf", NULL, &t6328_TI, &t44_0_0_0, RuntimeInvoker_t44_t44, t6328_m32838_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32838_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1362_0_0_0;
static ParameterInfo t6328_m32839_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1362_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32839_GM;
MethodInfo m32839_MI = 
{
	"Insert", NULL, &t6328_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6328_m32839_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32839_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6328_m32840_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32840_GM;
MethodInfo m32840_MI = 
{
	"RemoveAt", NULL, &t6328_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6328_m32840_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32840_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6328_m32836_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1362_0_0_0;
extern void* RuntimeInvoker_t1362_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32836_GM;
MethodInfo m32836_MI = 
{
	"get_Item", NULL, &t6328_TI, &t1362_0_0_0, RuntimeInvoker_t1362_t44, t6328_m32836_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32836_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1362_0_0_0;
static ParameterInfo t6328_m32837_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1362_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32837_GM;
MethodInfo m32837_MI = 
{
	"set_Item", NULL, &t6328_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t44, t6328_m32837_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32837_GM};
static MethodInfo* t6328_MIs[] =
{
	&m32838_MI,
	&m32839_MI,
	&m32840_MI,
	&m32836_MI,
	&m32837_MI,
	NULL
};
static TypeInfo* t6328_ITIs[] = 
{
	&t603_TI,
	&t6327_TI,
	&t6329_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6328_0_0_0;
extern Il2CppType t6328_1_0_0;
struct t6328;
extern Il2CppGenericClass t6328_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6328_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6328_MIs, t6328_PIs, NULL, NULL, NULL, NULL, NULL, &t6328_TI, t6328_ITIs, NULL, &t1908__CustomAttributeCache, &t6328_TI, &t6328_0_0_0, &t6328_1_0_0, NULL, &t6328_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4857_TI;

#include "t433.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyProductAttribute>
extern MethodInfo m32841_MI;
static PropertyInfo t4857____Current_PropertyInfo = 
{
	&t4857_TI, "Current", &m32841_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4857_PIs[] =
{
	&t4857____Current_PropertyInfo,
	NULL
};
extern Il2CppType t433_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32841_GM;
MethodInfo m32841_MI = 
{
	"get_Current", NULL, &t4857_TI, &t433_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32841_GM};
static MethodInfo* t4857_MIs[] =
{
	&m32841_MI,
	NULL
};
static TypeInfo* t4857_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4857_0_0_0;
extern Il2CppType t4857_1_0_0;
struct t4857;
extern Il2CppGenericClass t4857_GC;
TypeInfo t4857_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4857_MIs, t4857_PIs, NULL, NULL, NULL, NULL, NULL, &t4857_TI, t4857_ITIs, NULL, &EmptyCustomAttributesCache, &t4857_TI, &t4857_0_0_0, &t4857_1_0_0, NULL, &t4857_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3379.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3379_TI;
#include "t3379MD.h"

extern TypeInfo t433_TI;
extern MethodInfo m18771_MI;
extern MethodInfo m25136_MI;
struct t20;
#define m25136(__this, p0, method) (t433 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3379_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3379_TI, offsetof(t3379, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3379_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3379_TI, offsetof(t3379, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3379_FIs[] =
{
	&t3379_f0_FieldInfo,
	&t3379_f1_FieldInfo,
	NULL
};
extern MethodInfo m18768_MI;
static PropertyInfo t3379____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3379_TI, "System.Collections.IEnumerator.Current", &m18768_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3379____Current_PropertyInfo = 
{
	&t3379_TI, "Current", &m18771_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3379_PIs[] =
{
	&t3379____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3379____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3379_m18767_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18767_GM;
MethodInfo m18767_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3379_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3379_m18767_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18767_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18768_GM;
MethodInfo m18768_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3379_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18768_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18769_GM;
MethodInfo m18769_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3379_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18769_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18770_GM;
MethodInfo m18770_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3379_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18770_GM};
extern Il2CppType t433_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18771_GM;
MethodInfo m18771_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3379_TI, &t433_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18771_GM};
static MethodInfo* t3379_MIs[] =
{
	&m18767_MI,
	&m18768_MI,
	&m18769_MI,
	&m18770_MI,
	&m18771_MI,
	NULL
};
extern MethodInfo m18770_MI;
extern MethodInfo m18769_MI;
static MethodInfo* t3379_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18768_MI,
	&m18770_MI,
	&m18769_MI,
	&m18771_MI,
};
static TypeInfo* t3379_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4857_TI,
};
static Il2CppInterfaceOffsetPair t3379_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4857_TI, 7},
};
extern TypeInfo t433_TI;
static Il2CppRGCTXData t3379_RGCTXData[3] = 
{
	&m18771_MI/* Method Usage */,
	&t433_TI/* Class Usage */,
	&m25136_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3379_0_0_0;
extern Il2CppType t3379_1_0_0;
extern Il2CppGenericClass t3379_GC;
TypeInfo t3379_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3379_MIs, t3379_PIs, t3379_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3379_TI, t3379_ITIs, t3379_VT, &EmptyCustomAttributesCache, &t3379_TI, &t3379_0_0_0, &t3379_1_0_0, t3379_IOs, &t3379_GC, NULL, NULL, NULL, t3379_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3379)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6330_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>
extern MethodInfo m32842_MI;
static PropertyInfo t6330____Count_PropertyInfo = 
{
	&t6330_TI, "Count", &m32842_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32843_MI;
static PropertyInfo t6330____IsReadOnly_PropertyInfo = 
{
	&t6330_TI, "IsReadOnly", &m32843_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6330_PIs[] =
{
	&t6330____Count_PropertyInfo,
	&t6330____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32842_GM;
MethodInfo m32842_MI = 
{
	"get_Count", NULL, &t6330_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32842_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32843_GM;
MethodInfo m32843_MI = 
{
	"get_IsReadOnly", NULL, &t6330_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32843_GM};
extern Il2CppType t433_0_0_0;
extern Il2CppType t433_0_0_0;
static ParameterInfo t6330_m32844_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t433_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32844_GM;
MethodInfo m32844_MI = 
{
	"Add", NULL, &t6330_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6330_m32844_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32844_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32845_GM;
MethodInfo m32845_MI = 
{
	"Clear", NULL, &t6330_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32845_GM};
extern Il2CppType t433_0_0_0;
static ParameterInfo t6330_m32846_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t433_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32846_GM;
MethodInfo m32846_MI = 
{
	"Contains", NULL, &t6330_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6330_m32846_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32846_GM};
extern Il2CppType t3637_0_0_0;
extern Il2CppType t3637_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6330_m32847_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3637_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32847_GM;
MethodInfo m32847_MI = 
{
	"CopyTo", NULL, &t6330_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6330_m32847_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32847_GM};
extern Il2CppType t433_0_0_0;
static ParameterInfo t6330_m32848_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t433_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32848_GM;
MethodInfo m32848_MI = 
{
	"Remove", NULL, &t6330_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6330_m32848_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32848_GM};
static MethodInfo* t6330_MIs[] =
{
	&m32842_MI,
	&m32843_MI,
	&m32844_MI,
	&m32845_MI,
	&m32846_MI,
	&m32847_MI,
	&m32848_MI,
	NULL
};
extern TypeInfo t6332_TI;
static TypeInfo* t6330_ITIs[] = 
{
	&t603_TI,
	&t6332_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6330_0_0_0;
extern Il2CppType t6330_1_0_0;
struct t6330;
extern Il2CppGenericClass t6330_GC;
TypeInfo t6330_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6330_MIs, t6330_PIs, NULL, NULL, NULL, NULL, NULL, &t6330_TI, t6330_ITIs, NULL, &EmptyCustomAttributesCache, &t6330_TI, &t6330_0_0_0, &t6330_1_0_0, NULL, &t6330_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyProductAttribute>
extern Il2CppType t4857_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32849_GM;
MethodInfo m32849_MI = 
{
	"GetEnumerator", NULL, &t6332_TI, &t4857_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32849_GM};
static MethodInfo* t6332_MIs[] =
{
	&m32849_MI,
	NULL
};
static TypeInfo* t6332_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6332_0_0_0;
extern Il2CppType t6332_1_0_0;
struct t6332;
extern Il2CppGenericClass t6332_GC;
TypeInfo t6332_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6332_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6332_TI, t6332_ITIs, NULL, &EmptyCustomAttributesCache, &t6332_TI, &t6332_0_0_0, &t6332_1_0_0, NULL, &t6332_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6331_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyProductAttribute>
extern MethodInfo m32850_MI;
extern MethodInfo m32851_MI;
static PropertyInfo t6331____Item_PropertyInfo = 
{
	&t6331_TI, "Item", &m32850_MI, &m32851_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6331_PIs[] =
{
	&t6331____Item_PropertyInfo,
	NULL
};
extern Il2CppType t433_0_0_0;
static ParameterInfo t6331_m32852_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t433_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32852_GM;
MethodInfo m32852_MI = 
{
	"IndexOf", NULL, &t6331_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6331_m32852_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32852_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t433_0_0_0;
static ParameterInfo t6331_m32853_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t433_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32853_GM;
MethodInfo m32853_MI = 
{
	"Insert", NULL, &t6331_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6331_m32853_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32853_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6331_m32854_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32854_GM;
MethodInfo m32854_MI = 
{
	"RemoveAt", NULL, &t6331_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6331_m32854_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32854_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6331_m32850_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t433_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32850_GM;
MethodInfo m32850_MI = 
{
	"get_Item", NULL, &t6331_TI, &t433_0_0_0, RuntimeInvoker_t29_t44, t6331_m32850_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32850_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t433_0_0_0;
static ParameterInfo t6331_m32851_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t433_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32851_GM;
MethodInfo m32851_MI = 
{
	"set_Item", NULL, &t6331_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6331_m32851_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32851_GM};
static MethodInfo* t6331_MIs[] =
{
	&m32852_MI,
	&m32853_MI,
	&m32854_MI,
	&m32850_MI,
	&m32851_MI,
	NULL
};
static TypeInfo* t6331_ITIs[] = 
{
	&t603_TI,
	&t6330_TI,
	&t6332_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6331_0_0_0;
extern Il2CppType t6331_1_0_0;
struct t6331;
extern Il2CppGenericClass t6331_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6331_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6331_MIs, t6331_PIs, NULL, NULL, NULL, NULL, NULL, &t6331_TI, t6331_ITIs, NULL, &t1908__CustomAttributeCache, &t6331_TI, &t6331_0_0_0, &t6331_1_0_0, NULL, &t6331_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4859_TI;

#include "t429.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyTitleAttribute>
extern MethodInfo m32855_MI;
static PropertyInfo t4859____Current_PropertyInfo = 
{
	&t4859_TI, "Current", &m32855_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4859_PIs[] =
{
	&t4859____Current_PropertyInfo,
	NULL
};
extern Il2CppType t429_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32855_GM;
MethodInfo m32855_MI = 
{
	"get_Current", NULL, &t4859_TI, &t429_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32855_GM};
static MethodInfo* t4859_MIs[] =
{
	&m32855_MI,
	NULL
};
static TypeInfo* t4859_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4859_0_0_0;
extern Il2CppType t4859_1_0_0;
struct t4859;
extern Il2CppGenericClass t4859_GC;
TypeInfo t4859_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4859_MIs, t4859_PIs, NULL, NULL, NULL, NULL, NULL, &t4859_TI, t4859_ITIs, NULL, &EmptyCustomAttributesCache, &t4859_TI, &t4859_0_0_0, &t4859_1_0_0, NULL, &t4859_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3380.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3380_TI;
#include "t3380MD.h"

extern TypeInfo t429_TI;
extern MethodInfo m18776_MI;
extern MethodInfo m25147_MI;
struct t20;
#define m25147(__this, p0, method) (t429 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyTitleAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3380_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3380_TI, offsetof(t3380, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3380_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3380_TI, offsetof(t3380, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3380_FIs[] =
{
	&t3380_f0_FieldInfo,
	&t3380_f1_FieldInfo,
	NULL
};
extern MethodInfo m18773_MI;
static PropertyInfo t3380____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3380_TI, "System.Collections.IEnumerator.Current", &m18773_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3380____Current_PropertyInfo = 
{
	&t3380_TI, "Current", &m18776_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3380_PIs[] =
{
	&t3380____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3380____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3380_m18772_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18772_GM;
MethodInfo m18772_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3380_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3380_m18772_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18772_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18773_GM;
MethodInfo m18773_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3380_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18773_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18774_GM;
MethodInfo m18774_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3380_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18774_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18775_GM;
MethodInfo m18775_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3380_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18775_GM};
extern Il2CppType t429_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18776_GM;
MethodInfo m18776_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3380_TI, &t429_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18776_GM};
static MethodInfo* t3380_MIs[] =
{
	&m18772_MI,
	&m18773_MI,
	&m18774_MI,
	&m18775_MI,
	&m18776_MI,
	NULL
};
extern MethodInfo m18775_MI;
extern MethodInfo m18774_MI;
static MethodInfo* t3380_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18773_MI,
	&m18775_MI,
	&m18774_MI,
	&m18776_MI,
};
static TypeInfo* t3380_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4859_TI,
};
static Il2CppInterfaceOffsetPair t3380_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4859_TI, 7},
};
extern TypeInfo t429_TI;
static Il2CppRGCTXData t3380_RGCTXData[3] = 
{
	&m18776_MI/* Method Usage */,
	&t429_TI/* Class Usage */,
	&m25147_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3380_0_0_0;
extern Il2CppType t3380_1_0_0;
extern Il2CppGenericClass t3380_GC;
TypeInfo t3380_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3380_MIs, t3380_PIs, t3380_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3380_TI, t3380_ITIs, t3380_VT, &EmptyCustomAttributesCache, &t3380_TI, &t3380_0_0_0, &t3380_1_0_0, t3380_IOs, &t3380_GC, NULL, NULL, NULL, t3380_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3380)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6333_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTitleAttribute>
extern MethodInfo m32856_MI;
static PropertyInfo t6333____Count_PropertyInfo = 
{
	&t6333_TI, "Count", &m32856_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32857_MI;
static PropertyInfo t6333____IsReadOnly_PropertyInfo = 
{
	&t6333_TI, "IsReadOnly", &m32857_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6333_PIs[] =
{
	&t6333____Count_PropertyInfo,
	&t6333____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32856_GM;
MethodInfo m32856_MI = 
{
	"get_Count", NULL, &t6333_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32856_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32857_GM;
MethodInfo m32857_MI = 
{
	"get_IsReadOnly", NULL, &t6333_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32857_GM};
extern Il2CppType t429_0_0_0;
extern Il2CppType t429_0_0_0;
static ParameterInfo t6333_m32858_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t429_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32858_GM;
MethodInfo m32858_MI = 
{
	"Add", NULL, &t6333_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6333_m32858_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32858_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32859_GM;
MethodInfo m32859_MI = 
{
	"Clear", NULL, &t6333_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32859_GM};
extern Il2CppType t429_0_0_0;
static ParameterInfo t6333_m32860_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t429_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32860_GM;
MethodInfo m32860_MI = 
{
	"Contains", NULL, &t6333_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6333_m32860_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32860_GM};
extern Il2CppType t3638_0_0_0;
extern Il2CppType t3638_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6333_m32861_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3638_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32861_GM;
MethodInfo m32861_MI = 
{
	"CopyTo", NULL, &t6333_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6333_m32861_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32861_GM};
extern Il2CppType t429_0_0_0;
static ParameterInfo t6333_m32862_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t429_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32862_GM;
MethodInfo m32862_MI = 
{
	"Remove", NULL, &t6333_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6333_m32862_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32862_GM};
static MethodInfo* t6333_MIs[] =
{
	&m32856_MI,
	&m32857_MI,
	&m32858_MI,
	&m32859_MI,
	&m32860_MI,
	&m32861_MI,
	&m32862_MI,
	NULL
};
extern TypeInfo t6335_TI;
static TypeInfo* t6333_ITIs[] = 
{
	&t603_TI,
	&t6335_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6333_0_0_0;
extern Il2CppType t6333_1_0_0;
struct t6333;
extern Il2CppGenericClass t6333_GC;
TypeInfo t6333_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6333_MIs, t6333_PIs, NULL, NULL, NULL, NULL, NULL, &t6333_TI, t6333_ITIs, NULL, &EmptyCustomAttributesCache, &t6333_TI, &t6333_0_0_0, &t6333_1_0_0, NULL, &t6333_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyTitleAttribute>
extern Il2CppType t4859_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32863_GM;
MethodInfo m32863_MI = 
{
	"GetEnumerator", NULL, &t6335_TI, &t4859_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32863_GM};
static MethodInfo* t6335_MIs[] =
{
	&m32863_MI,
	NULL
};
static TypeInfo* t6335_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6335_0_0_0;
extern Il2CppType t6335_1_0_0;
struct t6335;
extern Il2CppGenericClass t6335_GC;
TypeInfo t6335_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6335_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6335_TI, t6335_ITIs, NULL, &EmptyCustomAttributesCache, &t6335_TI, &t6335_0_0_0, &t6335_1_0_0, NULL, &t6335_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6334_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyTitleAttribute>
extern MethodInfo m32864_MI;
extern MethodInfo m32865_MI;
static PropertyInfo t6334____Item_PropertyInfo = 
{
	&t6334_TI, "Item", &m32864_MI, &m32865_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6334_PIs[] =
{
	&t6334____Item_PropertyInfo,
	NULL
};
extern Il2CppType t429_0_0_0;
static ParameterInfo t6334_m32866_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t429_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32866_GM;
MethodInfo m32866_MI = 
{
	"IndexOf", NULL, &t6334_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6334_m32866_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32866_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t429_0_0_0;
static ParameterInfo t6334_m32867_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t429_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32867_GM;
MethodInfo m32867_MI = 
{
	"Insert", NULL, &t6334_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6334_m32867_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32867_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6334_m32868_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32868_GM;
MethodInfo m32868_MI = 
{
	"RemoveAt", NULL, &t6334_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6334_m32868_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32868_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6334_m32864_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t429_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32864_GM;
MethodInfo m32864_MI = 
{
	"get_Item", NULL, &t6334_TI, &t429_0_0_0, RuntimeInvoker_t29_t44, t6334_m32864_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32864_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t429_0_0_0;
static ParameterInfo t6334_m32865_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t429_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32865_GM;
MethodInfo m32865_MI = 
{
	"set_Item", NULL, &t6334_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6334_m32865_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32865_GM};
static MethodInfo* t6334_MIs[] =
{
	&m32866_MI,
	&m32867_MI,
	&m32868_MI,
	&m32864_MI,
	&m32865_MI,
	NULL
};
static TypeInfo* t6334_ITIs[] = 
{
	&t603_TI,
	&t6333_TI,
	&t6335_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6334_0_0_0;
extern Il2CppType t6334_1_0_0;
struct t6334;
extern Il2CppGenericClass t6334_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6334_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6334_MIs, t6334_PIs, NULL, NULL, NULL, NULL, NULL, &t6334_TI, t6334_ITIs, NULL, &t1908__CustomAttributeCache, &t6334_TI, &t6334_0_0_0, &t6334_1_0_0, NULL, &t6334_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4861_TI;

#include "t438.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>
extern MethodInfo m32869_MI;
static PropertyInfo t4861____Current_PropertyInfo = 
{
	&t4861_TI, "Current", &m32869_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4861_PIs[] =
{
	&t4861____Current_PropertyInfo,
	NULL
};
extern Il2CppType t438_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32869_GM;
MethodInfo m32869_MI = 
{
	"get_Current", NULL, &t4861_TI, &t438_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32869_GM};
static MethodInfo* t4861_MIs[] =
{
	&m32869_MI,
	NULL
};
static TypeInfo* t4861_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4861_0_0_0;
extern Il2CppType t4861_1_0_0;
struct t4861;
extern Il2CppGenericClass t4861_GC;
TypeInfo t4861_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4861_MIs, t4861_PIs, NULL, NULL, NULL, NULL, NULL, &t4861_TI, t4861_ITIs, NULL, &EmptyCustomAttributesCache, &t4861_TI, &t4861_0_0_0, &t4861_1_0_0, NULL, &t4861_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3381.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3381_TI;
#include "t3381MD.h"

extern TypeInfo t438_TI;
extern MethodInfo m18781_MI;
extern MethodInfo m25158_MI;
struct t20;
#define m25158(__this, p0, method) (t438 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyTrademarkAttribute>
extern Il2CppType t20_0_0_1;
FieldInfo t3381_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3381_TI, offsetof(t3381, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3381_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3381_TI, offsetof(t3381, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3381_FIs[] =
{
	&t3381_f0_FieldInfo,
	&t3381_f1_FieldInfo,
	NULL
};
extern MethodInfo m18778_MI;
static PropertyInfo t3381____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3381_TI, "System.Collections.IEnumerator.Current", &m18778_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3381____Current_PropertyInfo = 
{
	&t3381_TI, "Current", &m18781_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3381_PIs[] =
{
	&t3381____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3381____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3381_m18777_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18777_GM;
MethodInfo m18777_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3381_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3381_m18777_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18777_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18778_GM;
MethodInfo m18778_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3381_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18778_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18779_GM;
MethodInfo m18779_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3381_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18779_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18780_GM;
MethodInfo m18780_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3381_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18780_GM};
extern Il2CppType t438_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18781_GM;
MethodInfo m18781_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3381_TI, &t438_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18781_GM};
static MethodInfo* t3381_MIs[] =
{
	&m18777_MI,
	&m18778_MI,
	&m18779_MI,
	&m18780_MI,
	&m18781_MI,
	NULL
};
extern MethodInfo m18780_MI;
extern MethodInfo m18779_MI;
static MethodInfo* t3381_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18778_MI,
	&m18780_MI,
	&m18779_MI,
	&m18781_MI,
};
static TypeInfo* t3381_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4861_TI,
};
static Il2CppInterfaceOffsetPair t3381_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4861_TI, 7},
};
extern TypeInfo t438_TI;
static Il2CppRGCTXData t3381_RGCTXData[3] = 
{
	&m18781_MI/* Method Usage */,
	&t438_TI/* Class Usage */,
	&m25158_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3381_0_0_0;
extern Il2CppType t3381_1_0_0;
extern Il2CppGenericClass t3381_GC;
TypeInfo t3381_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3381_MIs, t3381_PIs, t3381_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3381_TI, t3381_ITIs, t3381_VT, &EmptyCustomAttributesCache, &t3381_TI, &t3381_0_0_0, &t3381_1_0_0, t3381_IOs, &t3381_GC, NULL, NULL, NULL, t3381_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3381)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6336_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyTrademarkAttribute>
extern MethodInfo m32870_MI;
static PropertyInfo t6336____Count_PropertyInfo = 
{
	&t6336_TI, "Count", &m32870_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32871_MI;
static PropertyInfo t6336____IsReadOnly_PropertyInfo = 
{
	&t6336_TI, "IsReadOnly", &m32871_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6336_PIs[] =
{
	&t6336____Count_PropertyInfo,
	&t6336____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32870_GM;
MethodInfo m32870_MI = 
{
	"get_Count", NULL, &t6336_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32870_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32871_GM;
MethodInfo m32871_MI = 
{
	"get_IsReadOnly", NULL, &t6336_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32871_GM};
extern Il2CppType t438_0_0_0;
extern Il2CppType t438_0_0_0;
static ParameterInfo t6336_m32872_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t438_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32872_GM;
MethodInfo m32872_MI = 
{
	"Add", NULL, &t6336_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6336_m32872_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32872_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32873_GM;
MethodInfo m32873_MI = 
{
	"Clear", NULL, &t6336_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32873_GM};
extern Il2CppType t438_0_0_0;
static ParameterInfo t6336_m32874_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t438_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32874_GM;
MethodInfo m32874_MI = 
{
	"Contains", NULL, &t6336_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6336_m32874_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32874_GM};
extern Il2CppType t3639_0_0_0;
extern Il2CppType t3639_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6336_m32875_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3639_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32875_GM;
MethodInfo m32875_MI = 
{
	"CopyTo", NULL, &t6336_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6336_m32875_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32875_GM};
extern Il2CppType t438_0_0_0;
static ParameterInfo t6336_m32876_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t438_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32876_GM;
MethodInfo m32876_MI = 
{
	"Remove", NULL, &t6336_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6336_m32876_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32876_GM};
static MethodInfo* t6336_MIs[] =
{
	&m32870_MI,
	&m32871_MI,
	&m32872_MI,
	&m32873_MI,
	&m32874_MI,
	&m32875_MI,
	&m32876_MI,
	NULL
};
extern TypeInfo t6338_TI;
static TypeInfo* t6336_ITIs[] = 
{
	&t603_TI,
	&t6338_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6336_0_0_0;
extern Il2CppType t6336_1_0_0;
struct t6336;
extern Il2CppGenericClass t6336_GC;
TypeInfo t6336_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6336_MIs, t6336_PIs, NULL, NULL, NULL, NULL, NULL, &t6336_TI, t6336_ITIs, NULL, &EmptyCustomAttributesCache, &t6336_TI, &t6336_0_0_0, &t6336_1_0_0, NULL, &t6336_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyTrademarkAttribute>
extern Il2CppType t4861_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32877_GM;
MethodInfo m32877_MI = 
{
	"GetEnumerator", NULL, &t6338_TI, &t4861_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32877_GM};
static MethodInfo* t6338_MIs[] =
{
	&m32877_MI,
	NULL
};
static TypeInfo* t6338_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6338_0_0_0;
extern Il2CppType t6338_1_0_0;
struct t6338;
extern Il2CppGenericClass t6338_GC;
TypeInfo t6338_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6338_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6338_TI, t6338_ITIs, NULL, &EmptyCustomAttributesCache, &t6338_TI, &t6338_0_0_0, &t6338_1_0_0, NULL, &t6338_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6337_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyTrademarkAttribute>
extern MethodInfo m32878_MI;
extern MethodInfo m32879_MI;
static PropertyInfo t6337____Item_PropertyInfo = 
{
	&t6337_TI, "Item", &m32878_MI, &m32879_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6337_PIs[] =
{
	&t6337____Item_PropertyInfo,
	NULL
};
extern Il2CppType t438_0_0_0;
static ParameterInfo t6337_m32880_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t438_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32880_GM;
MethodInfo m32880_MI = 
{
	"IndexOf", NULL, &t6337_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6337_m32880_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32880_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t438_0_0_0;
static ParameterInfo t6337_m32881_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t438_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32881_GM;
MethodInfo m32881_MI = 
{
	"Insert", NULL, &t6337_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6337_m32881_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32881_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6337_m32882_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32882_GM;
MethodInfo m32882_MI = 
{
	"RemoveAt", NULL, &t6337_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6337_m32882_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32882_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6337_m32878_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t438_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32878_GM;
MethodInfo m32878_MI = 
{
	"get_Item", NULL, &t6337_TI, &t438_0_0_0, RuntimeInvoker_t29_t44, t6337_m32878_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32878_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t438_0_0_0;
static ParameterInfo t6337_m32879_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t438_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32879_GM;
MethodInfo m32879_MI = 
{
	"set_Item", NULL, &t6337_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6337_m32879_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32879_GM};
static MethodInfo* t6337_MIs[] =
{
	&m32880_MI,
	&m32881_MI,
	&m32882_MI,
	&m32878_MI,
	&m32879_MI,
	NULL
};
static TypeInfo* t6337_ITIs[] = 
{
	&t603_TI,
	&t6336_TI,
	&t6338_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6337_0_0_0;
extern Il2CppType t6337_1_0_0;
struct t6337;
extern Il2CppGenericClass t6337_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6337_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6337_MIs, t6337_PIs, NULL, NULL, NULL, NULL, NULL, &t6337_TI, t6337_ITIs, NULL, &t1908__CustomAttributeCache, &t6337_TI, &t6337_0_0_0, &t6337_1_0_0, NULL, &t6337_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4863_TI;

#include "t1146.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>
extern MethodInfo m32883_MI;
static PropertyInfo t4863____Current_PropertyInfo = 
{
	&t4863_TI, "Current", &m32883_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4863_PIs[] =
{
	&t4863____Current_PropertyInfo,
	NULL
};
extern Il2CppType t1146_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32883_GM;
MethodInfo m32883_MI = 
{
	"get_Current", NULL, &t4863_TI, &t1146_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32883_GM};
static MethodInfo* t4863_MIs[] =
{
	&m32883_MI,
	NULL
};
static TypeInfo* t4863_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4863_0_0_0;
extern Il2CppType t4863_1_0_0;
struct t4863;
extern Il2CppGenericClass t4863_GC;
TypeInfo t4863_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4863_MIs, t4863_PIs, NULL, NULL, NULL, NULL, NULL, &t4863_TI, t4863_ITIs, NULL, &EmptyCustomAttributesCache, &t4863_TI, &t4863_0_0_0, &t4863_1_0_0, NULL, &t4863_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3382.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3382_TI;
#include "t3382MD.h"

extern TypeInfo t1146_TI;
extern MethodInfo m18786_MI;
extern MethodInfo m25169_MI;
struct t20;
#define m25169(__this, p0, method) (t1146 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.PropertyInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3382_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3382_TI, offsetof(t3382, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3382_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3382_TI, offsetof(t3382, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3382_FIs[] =
{
	&t3382_f0_FieldInfo,
	&t3382_f1_FieldInfo,
	NULL
};
extern MethodInfo m18783_MI;
static PropertyInfo t3382____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3382_TI, "System.Collections.IEnumerator.Current", &m18783_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3382____Current_PropertyInfo = 
{
	&t3382_TI, "Current", &m18786_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3382_PIs[] =
{
	&t3382____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3382____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3382_m18782_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18782_GM;
MethodInfo m18782_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3382_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3382_m18782_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18782_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18783_GM;
MethodInfo m18783_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3382_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18783_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18784_GM;
MethodInfo m18784_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3382_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18784_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18785_GM;
MethodInfo m18785_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3382_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18785_GM};
extern Il2CppType t1146_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18786_GM;
MethodInfo m18786_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3382_TI, &t1146_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18786_GM};
static MethodInfo* t3382_MIs[] =
{
	&m18782_MI,
	&m18783_MI,
	&m18784_MI,
	&m18785_MI,
	&m18786_MI,
	NULL
};
extern MethodInfo m18785_MI;
extern MethodInfo m18784_MI;
static MethodInfo* t3382_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18783_MI,
	&m18785_MI,
	&m18784_MI,
	&m18786_MI,
};
static TypeInfo* t3382_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4863_TI,
};
static Il2CppInterfaceOffsetPair t3382_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4863_TI, 7},
};
extern TypeInfo t1146_TI;
static Il2CppRGCTXData t3382_RGCTXData[3] = 
{
	&m18786_MI/* Method Usage */,
	&t1146_TI/* Class Usage */,
	&m25169_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3382_0_0_0;
extern Il2CppType t3382_1_0_0;
extern Il2CppGenericClass t3382_GC;
TypeInfo t3382_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3382_MIs, t3382_PIs, t3382_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3382_TI, t3382_ITIs, t3382_VT, &EmptyCustomAttributesCache, &t3382_TI, &t3382_0_0_0, &t3382_1_0_0, t3382_IOs, &t3382_GC, NULL, NULL, NULL, t3382_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3382)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6339_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.PropertyInfo>
extern MethodInfo m32884_MI;
static PropertyInfo t6339____Count_PropertyInfo = 
{
	&t6339_TI, "Count", &m32884_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32885_MI;
static PropertyInfo t6339____IsReadOnly_PropertyInfo = 
{
	&t6339_TI, "IsReadOnly", &m32885_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6339_PIs[] =
{
	&t6339____Count_PropertyInfo,
	&t6339____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32884_GM;
MethodInfo m32884_MI = 
{
	"get_Count", NULL, &t6339_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32884_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32885_GM;
MethodInfo m32885_MI = 
{
	"get_IsReadOnly", NULL, &t6339_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32885_GM};
extern Il2CppType t1146_0_0_0;
extern Il2CppType t1146_0_0_0;
static ParameterInfo t6339_m32886_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1146_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32886_GM;
MethodInfo m32886_MI = 
{
	"Add", NULL, &t6339_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6339_m32886_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32886_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32887_GM;
MethodInfo m32887_MI = 
{
	"Clear", NULL, &t6339_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32887_GM};
extern Il2CppType t1146_0_0_0;
static ParameterInfo t6339_m32888_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1146_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32888_GM;
MethodInfo m32888_MI = 
{
	"Contains", NULL, &t6339_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6339_m32888_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32888_GM};
extern Il2CppType t1366_0_0_0;
extern Il2CppType t1366_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6339_m32889_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t1366_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32889_GM;
MethodInfo m32889_MI = 
{
	"CopyTo", NULL, &t6339_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6339_m32889_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32889_GM};
extern Il2CppType t1146_0_0_0;
static ParameterInfo t6339_m32890_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1146_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32890_GM;
MethodInfo m32890_MI = 
{
	"Remove", NULL, &t6339_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6339_m32890_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32890_GM};
static MethodInfo* t6339_MIs[] =
{
	&m32884_MI,
	&m32885_MI,
	&m32886_MI,
	&m32887_MI,
	&m32888_MI,
	&m32889_MI,
	&m32890_MI,
	NULL
};
extern TypeInfo t6341_TI;
static TypeInfo* t6339_ITIs[] = 
{
	&t603_TI,
	&t6341_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6339_0_0_0;
extern Il2CppType t6339_1_0_0;
struct t6339;
extern Il2CppGenericClass t6339_GC;
TypeInfo t6339_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6339_MIs, t6339_PIs, NULL, NULL, NULL, NULL, NULL, &t6339_TI, t6339_ITIs, NULL, &EmptyCustomAttributesCache, &t6339_TI, &t6339_0_0_0, &t6339_1_0_0, NULL, &t6339_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>
extern Il2CppType t4863_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32891_GM;
MethodInfo m32891_MI = 
{
	"GetEnumerator", NULL, &t6341_TI, &t4863_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32891_GM};
static MethodInfo* t6341_MIs[] =
{
	&m32891_MI,
	NULL
};
static TypeInfo* t6341_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6341_0_0_0;
extern Il2CppType t6341_1_0_0;
struct t6341;
extern Il2CppGenericClass t6341_GC;
TypeInfo t6341_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6341_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6341_TI, t6341_ITIs, NULL, &EmptyCustomAttributesCache, &t6341_TI, &t6341_0_0_0, &t6341_1_0_0, NULL, &t6341_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6340_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>
extern MethodInfo m32892_MI;
extern MethodInfo m32893_MI;
static PropertyInfo t6340____Item_PropertyInfo = 
{
	&t6340_TI, "Item", &m32892_MI, &m32893_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6340_PIs[] =
{
	&t6340____Item_PropertyInfo,
	NULL
};
extern Il2CppType t1146_0_0_0;
static ParameterInfo t6340_m32894_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t1146_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32894_GM;
MethodInfo m32894_MI = 
{
	"IndexOf", NULL, &t6340_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6340_m32894_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32894_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1146_0_0_0;
static ParameterInfo t6340_m32895_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t1146_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32895_GM;
MethodInfo m32895_MI = 
{
	"Insert", NULL, &t6340_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6340_m32895_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32895_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6340_m32896_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32896_GM;
MethodInfo m32896_MI = 
{
	"RemoveAt", NULL, &t6340_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6340_m32896_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32896_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6340_m32892_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t1146_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32892_GM;
MethodInfo m32892_MI = 
{
	"get_Item", NULL, &t6340_TI, &t1146_0_0_0, RuntimeInvoker_t29_t44, t6340_m32892_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32892_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t1146_0_0_0;
static ParameterInfo t6340_m32893_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t1146_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32893_GM;
MethodInfo m32893_MI = 
{
	"set_Item", NULL, &t6340_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6340_m32893_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32893_GM};
static MethodInfo* t6340_MIs[] =
{
	&m32894_MI,
	&m32895_MI,
	&m32896_MI,
	&m32892_MI,
	&m32893_MI,
	NULL
};
static TypeInfo* t6340_ITIs[] = 
{
	&t603_TI,
	&t6339_TI,
	&t6341_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6340_0_0_0;
extern Il2CppType t6340_1_0_0;
struct t6340;
extern Il2CppGenericClass t6340_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6340_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6340_MIs, t6340_PIs, NULL, NULL, NULL, NULL, NULL, &t6340_TI, t6340_ITIs, NULL, &t1908__CustomAttributeCache, &t6340_TI, &t6340_0_0_0, &t6340_1_0_0, NULL, &t6340_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6342_TI;



// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._PropertyInfo>
extern MethodInfo m32897_MI;
static PropertyInfo t6342____Count_PropertyInfo = 
{
	&t6342_TI, "Count", &m32897_MI, NULL, 0, &EmptyCustomAttributesCache};
extern MethodInfo m32898_MI;
static PropertyInfo t6342____IsReadOnly_PropertyInfo = 
{
	&t6342_TI, "IsReadOnly", &m32898_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6342_PIs[] =
{
	&t6342____Count_PropertyInfo,
	&t6342____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32897_GM;
MethodInfo m32897_MI = 
{
	"get_Count", NULL, &t6342_TI, &t44_0_0_0, RuntimeInvoker_t44, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32897_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32898_GM;
MethodInfo m32898_MI = 
{
	"get_IsReadOnly", NULL, &t6342_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 3526, 0, 1, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32898_GM};
extern Il2CppType t2036_0_0_0;
extern Il2CppType t2036_0_0_0;
static ParameterInfo t6342_m32899_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2036_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32899_GM;
MethodInfo m32899_MI = 
{
	"Add", NULL, &t6342_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t6342_m32899_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32899_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32900_GM;
MethodInfo m32900_MI = 
{
	"Clear", NULL, &t6342_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 1478, 0, 3, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32900_GM};
extern Il2CppType t2036_0_0_0;
static ParameterInfo t6342_m32901_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2036_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32901_GM;
MethodInfo m32901_MI = 
{
	"Contains", NULL, &t6342_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6342_m32901_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 4, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32901_GM};
extern Il2CppType t3640_0_0_0;
extern Il2CppType t3640_0_0_0;
extern Il2CppType t44_0_0_0;
static ParameterInfo t6342_m32902_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t3640_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32902_GM;
MethodInfo m32902_MI = 
{
	"CopyTo", NULL, &t6342_TI, &t21_0_0_0, RuntimeInvoker_t21_t29_t44, t6342_m32902_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 5, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32902_GM};
extern Il2CppType t2036_0_0_0;
static ParameterInfo t6342_m32903_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2036_0_0_0},
};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32903_GM;
MethodInfo m32903_MI = 
{
	"Remove", NULL, &t6342_TI, &t40_0_0_0, RuntimeInvoker_t40_t29, t6342_m32903_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 6, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32903_GM};
static MethodInfo* t6342_MIs[] =
{
	&m32897_MI,
	&m32898_MI,
	&m32899_MI,
	&m32900_MI,
	&m32901_MI,
	&m32902_MI,
	&m32903_MI,
	NULL
};
extern TypeInfo t6344_TI;
static TypeInfo* t6342_ITIs[] = 
{
	&t603_TI,
	&t6344_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6342_0_0_0;
extern Il2CppType t6342_1_0_0;
struct t6342;
extern Il2CppGenericClass t6342_GC;
TypeInfo t6342_TI = 
{
	&g_mscorlib_dll_Image, NULL, "ICollection`1", "System.Collections.Generic", t6342_MIs, t6342_PIs, NULL, NULL, NULL, NULL, NULL, &t6342_TI, t6342_ITIs, NULL, &EmptyCustomAttributesCache, &t6342_TI, &t6342_0_0_0, &t6342_1_0_0, NULL, &t6342_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 7, 2, 0, 0, 0, 0, 2, 0};
#ifndef _MSC_VER
#else
#endif



// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._PropertyInfo>
extern Il2CppType t4865_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32904_GM;
MethodInfo m32904_MI = 
{
	"GetEnumerator", NULL, &t6344_TI, &t4865_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 1478, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32904_GM};
static MethodInfo* t6344_MIs[] =
{
	&m32904_MI,
	NULL
};
static TypeInfo* t6344_ITIs[] = 
{
	&t603_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6344_0_0_0;
extern Il2CppType t6344_1_0_0;
struct t6344;
extern Il2CppGenericClass t6344_GC;
TypeInfo t6344_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerable`1", "System.Collections.Generic", t6344_MIs, NULL, NULL, NULL, NULL, NULL, NULL, &t6344_TI, t6344_ITIs, NULL, &EmptyCustomAttributesCache, &t6344_TI, &t6344_0_0_0, &t6344_1_0_0, NULL, &t6344_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 0, 0, 0, 0, 0, 1, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4865_TI;



// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._PropertyInfo>
extern MethodInfo m32905_MI;
static PropertyInfo t4865____Current_PropertyInfo = 
{
	&t4865_TI, "Current", &m32905_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4865_PIs[] =
{
	&t4865____Current_PropertyInfo,
	NULL
};
extern Il2CppType t2036_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32905_GM;
MethodInfo m32905_MI = 
{
	"get_Current", NULL, &t4865_TI, &t2036_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32905_GM};
static MethodInfo* t4865_MIs[] =
{
	&m32905_MI,
	NULL
};
static TypeInfo* t4865_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4865_0_0_0;
extern Il2CppType t4865_1_0_0;
struct t4865;
extern Il2CppGenericClass t4865_GC;
TypeInfo t4865_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4865_MIs, t4865_PIs, NULL, NULL, NULL, NULL, NULL, &t4865_TI, t4865_ITIs, NULL, &EmptyCustomAttributesCache, &t4865_TI, &t4865_0_0_0, &t4865_1_0_0, NULL, &t4865_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3383.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3383_TI;
#include "t3383MD.h"

extern TypeInfo t2036_TI;
extern MethodInfo m18791_MI;
extern MethodInfo m25180_MI;
struct t20;
#define m25180(__this, p0, method) (t29 *)m19490_gshared((t20 *)__this, (int32_t)p0, method)


// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._PropertyInfo>
extern Il2CppType t20_0_0_1;
FieldInfo t3383_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3383_TI, offsetof(t3383, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3383_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3383_TI, offsetof(t3383, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3383_FIs[] =
{
	&t3383_f0_FieldInfo,
	&t3383_f1_FieldInfo,
	NULL
};
extern MethodInfo m18788_MI;
static PropertyInfo t3383____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3383_TI, "System.Collections.IEnumerator.Current", &m18788_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3383____Current_PropertyInfo = 
{
	&t3383_TI, "Current", &m18791_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3383_PIs[] =
{
	&t3383____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3383____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3383_m18787_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18787_GM;
MethodInfo m18787_MI = 
{
	".ctor", (methodPointerType)&m10288_gshared, &t3383_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3383_m18787_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18787_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18788_GM;
MethodInfo m18788_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m10290_gshared, &t3383_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18788_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18789_GM;
MethodInfo m18789_MI = 
{
	"Dispose", (methodPointerType)&m10292_gshared, &t3383_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18789_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18790_GM;
MethodInfo m18790_MI = 
{
	"MoveNext", (methodPointerType)&m10294_gshared, &t3383_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18790_GM};
extern Il2CppType t2036_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18791_GM;
MethodInfo m18791_MI = 
{
	"get_Current", (methodPointerType)&m10296_gshared, &t3383_TI, &t2036_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18791_GM};
static MethodInfo* t3383_MIs[] =
{
	&m18787_MI,
	&m18788_MI,
	&m18789_MI,
	&m18790_MI,
	&m18791_MI,
	NULL
};
extern MethodInfo m18790_MI;
extern MethodInfo m18789_MI;
static MethodInfo* t3383_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18788_MI,
	&m18790_MI,
	&m18789_MI,
	&m18791_MI,
};
static TypeInfo* t3383_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4865_TI,
};
static Il2CppInterfaceOffsetPair t3383_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4865_TI, 7},
};
extern TypeInfo t2036_TI;
static Il2CppRGCTXData t3383_RGCTXData[3] = 
{
	&m18791_MI/* Method Usage */,
	&t2036_TI/* Class Usage */,
	&m25180_MI/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3383_0_0_0;
extern Il2CppType t3383_1_0_0;
extern Il2CppGenericClass t3383_GC;
TypeInfo t3383_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3383_MIs, t3383_PIs, t3383_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3383_TI, t3383_ITIs, t3383_VT, &EmptyCustomAttributesCache, &t3383_TI, &t3383_0_0_0, &t3383_1_0_0, t3383_IOs, &t3383_GC, NULL, NULL, NULL, t3383_RGCTXData, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3383)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t6343_TI;



// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._PropertyInfo>
extern MethodInfo m32906_MI;
extern MethodInfo m32907_MI;
static PropertyInfo t6343____Item_PropertyInfo = 
{
	&t6343_TI, "Item", &m32906_MI, &m32907_MI, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t6343_PIs[] =
{
	&t6343____Item_PropertyInfo,
	NULL
};
extern Il2CppType t2036_0_0_0;
static ParameterInfo t6343_m32908_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &t2036_0_0_0},
};
extern Il2CppType t44_0_0_0;
extern void* RuntimeInvoker_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32908_GM;
MethodInfo m32908_MI = 
{
	"IndexOf", NULL, &t6343_TI, &t44_0_0_0, RuntimeInvoker_t44_t29, t6343_m32908_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 0, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32908_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2036_0_0_0;
static ParameterInfo t6343_m32909_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &t2036_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32909_GM;
MethodInfo m32909_MI = 
{
	"Insert", NULL, &t6343_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6343_m32909_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 1, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32909_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6343_m32910_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32910_GM;
MethodInfo m32910_MI = 
{
	"RemoveAt", NULL, &t6343_TI, &t21_0_0_0, RuntimeInvoker_t21_t44, t6343_m32910_ParameterInfos, &EmptyCustomAttributesCache, 1478, 0, 2, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32910_GM};
extern Il2CppType t44_0_0_0;
static ParameterInfo t6343_m32906_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
};
extern Il2CppType t2036_0_0_0;
extern void* RuntimeInvoker_t29_t44 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32906_GM;
MethodInfo m32906_MI = 
{
	"get_Item", NULL, &t6343_TI, &t2036_0_0_0, RuntimeInvoker_t29_t44, t6343_m32906_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 3, 1, false, true, 0, NULL, (methodPointerType)NULL, &m32906_GM};
extern Il2CppType t44_0_0_0;
extern Il2CppType t2036_0_0_0;
static ParameterInfo t6343_m32907_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &t44_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &t2036_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t44_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32907_GM;
MethodInfo m32907_MI = 
{
	"set_Item", NULL, &t6343_TI, &t21_0_0_0, RuntimeInvoker_t21_t44_t29, t6343_m32907_ParameterInfos, &EmptyCustomAttributesCache, 3526, 0, 4, 2, false, true, 0, NULL, (methodPointerType)NULL, &m32907_GM};
static MethodInfo* t6343_MIs[] =
{
	&m32908_MI,
	&m32909_MI,
	&m32910_MI,
	&m32906_MI,
	&m32907_MI,
	NULL
};
static TypeInfo* t6343_ITIs[] = 
{
	&t603_TI,
	&t6342_TI,
	&t6344_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t6343_0_0_0;
extern Il2CppType t6343_1_0_0;
struct t6343;
extern Il2CppGenericClass t6343_GC;
extern CustomAttributesCache t1908__CustomAttributeCache;
TypeInfo t6343_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IList`1", "System.Collections.Generic", t6343_MIs, t6343_PIs, NULL, NULL, NULL, NULL, NULL, &t6343_TI, t6343_ITIs, NULL, &t1908__CustomAttributeCache, &t6343_TI, &t6343_0_0_0, &t6343_1_0_0, NULL, &t6343_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 5, 1, 0, 0, 0, 0, 3, 0};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t4867_TI;

#include "t630.h"


// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.BindingFlags>
extern MethodInfo m32911_MI;
static PropertyInfo t4867____Current_PropertyInfo = 
{
	&t4867_TI, "Current", &m32911_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t4867_PIs[] =
{
	&t4867____Current_PropertyInfo,
	NULL
};
extern Il2CppType t630_0_0_0;
extern void* RuntimeInvoker_t630 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m32911_GM;
MethodInfo m32911_MI = 
{
	"get_Current", NULL, &t4867_TI, &t630_0_0_0, RuntimeInvoker_t630, NULL, &EmptyCustomAttributesCache, 3526, 0, 0, 0, false, true, 0, NULL, (methodPointerType)NULL, &m32911_GM};
static MethodInfo* t4867_MIs[] =
{
	&m32911_MI,
	NULL
};
static TypeInfo* t4867_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t4867_0_0_0;
extern Il2CppType t4867_1_0_0;
struct t4867;
extern Il2CppGenericClass t4867_GC;
TypeInfo t4867_TI = 
{
	&g_mscorlib_dll_Image, NULL, "IEnumerator`1", "System.Collections.Generic", t4867_MIs, t4867_PIs, NULL, NULL, NULL, NULL, NULL, &t4867_TI, t4867_ITIs, NULL, &EmptyCustomAttributesCache, &t4867_TI, &t4867_0_0_0, &t4867_1_0_0, NULL, &t4867_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, 0, 0, -1, 0, 0, -1, 161, 0, false, true, false, false, true, false, false, false, false, false, false, false, 1, 1, 0, 0, 0, 0, 2, 0};
#include "t3384.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo t3384_TI;
#include "t3384MD.h"

extern TypeInfo t630_TI;
extern MethodInfo m18796_MI;
extern MethodInfo m25191_MI;
struct t20;
 int32_t m25191 (t20 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


extern MethodInfo m18792_MI;
 void m18792 (t3384 * __this, t20 * p0, MethodInfo* method){
	{
		__this->f0 = p0;
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18793_MI;
 t29 * m18793 (t3384 * __this, MethodInfo* method){
	{
		int32_t L_0 = m18796(__this, &m18796_MI);
		int32_t L_1 = L_0;
		t29 * L_2 = Box(InitializedTypeInfo(&t630_TI), &L_1);
		return L_2;
	}
}
extern MethodInfo m18794_MI;
 void m18794 (t3384 * __this, MethodInfo* method){
	{
		__this->f1 = ((int32_t)-2);
		return;
	}
}
extern MethodInfo m18795_MI;
 bool m18795 (t3384 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		t20 * L_1 = (__this->f0);
		int32_t L_2 = m3969(L_1, &m3969_MI);
		__this->f1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->f1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->f1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->f1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
 int32_t m18796 (t3384 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->f1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		t914 * L_1 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_1, (t7*) &_stringLiteral1036, &m3964_MI);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->f1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		t914 * L_3 = (t914 *)il2cpp_codegen_object_new (InitializedTypeInfo(&t914_TI));
		m3964(L_3, (t7*) &_stringLiteral1037, &m3964_MI);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		t20 * L_4 = (__this->f0);
		t20 * L_5 = (__this->f0);
		int32_t L_6 = m3969(L_5, &m3969_MI);
		int32_t L_7 = (__this->f1);
		int32_t L_8 = m25191(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), &m25191_MI);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.BindingFlags>
extern Il2CppType t20_0_0_1;
FieldInfo t3384_f0_FieldInfo = 
{
	"array", &t20_0_0_1, &t3384_TI, offsetof(t3384, f0) + sizeof(t29), &EmptyCustomAttributesCache};
extern Il2CppType t44_0_0_1;
FieldInfo t3384_f1_FieldInfo = 
{
	"idx", &t44_0_0_1, &t3384_TI, offsetof(t3384, f1) + sizeof(t29), &EmptyCustomAttributesCache};
static FieldInfo* t3384_FIs[] =
{
	&t3384_f0_FieldInfo,
	&t3384_f1_FieldInfo,
	NULL
};
static PropertyInfo t3384____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&t3384_TI, "System.Collections.IEnumerator.Current", &m18793_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo t3384____Current_PropertyInfo = 
{
	&t3384_TI, "Current", &m18796_MI, NULL, 0, &EmptyCustomAttributesCache};
static PropertyInfo* t3384_PIs[] =
{
	&t3384____System_Collections_IEnumerator_Current_PropertyInfo,
	&t3384____Current_PropertyInfo,
	NULL
};
extern Il2CppType t20_0_0_0;
static ParameterInfo t3384_m18792_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &t20_0_0_0},
};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18792_GM;
MethodInfo m18792_MI = 
{
	".ctor", (methodPointerType)&m18792, &t3384_TI, &t21_0_0_0, RuntimeInvoker_t21_t29, t3384_m18792_ParameterInfos, &EmptyCustomAttributesCache, 6275, 0, 255, 1, false, true, 0, NULL, (methodPointerType)NULL, &m18792_GM};
extern Il2CppType t29_0_0_0;
extern void* RuntimeInvoker_t29 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18793_GM;
MethodInfo m18793_MI = 
{
	"System.Collections.IEnumerator.get_Current", (methodPointerType)&m18793, &t3384_TI, &t29_0_0_0, RuntimeInvoker_t29, NULL, &EmptyCustomAttributesCache, 2529, 0, 4, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18793_GM};
extern Il2CppType t21_0_0_0;
extern void* RuntimeInvoker_t21 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18794_GM;
MethodInfo m18794_MI = 
{
	"Dispose", (methodPointerType)&m18794, &t3384_TI, &t21_0_0_0, RuntimeInvoker_t21, NULL, &EmptyCustomAttributesCache, 486, 0, 6, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18794_GM};
extern Il2CppType t40_0_0_0;
extern void* RuntimeInvoker_t40 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18795_GM;
MethodInfo m18795_MI = 
{
	"MoveNext", (methodPointerType)&m18795, &t3384_TI, &t40_0_0_0, RuntimeInvoker_t40, NULL, &EmptyCustomAttributesCache, 486, 0, 5, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18795_GM};
extern Il2CppType t630_0_0_0;
extern void* RuntimeInvoker_t630 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod m18796_GM;
MethodInfo m18796_MI = 
{
	"get_Current", (methodPointerType)&m18796, &t3384_TI, &t630_0_0_0, RuntimeInvoker_t630, NULL, &EmptyCustomAttributesCache, 2534, 0, 7, 0, false, true, 0, NULL, (methodPointerType)NULL, &m18796_GM};
static MethodInfo* t3384_MIs[] =
{
	&m18792_MI,
	&m18793_MI,
	&m18794_MI,
	&m18795_MI,
	&m18796_MI,
	NULL
};
static MethodInfo* t3384_VT[] =
{
	&m1388_MI,
	&m46_MI,
	&m1389_MI,
	&m1500_MI,
	&m18793_MI,
	&m18795_MI,
	&m18794_MI,
	&m18796_MI,
};
static TypeInfo* t3384_ITIs[] = 
{
	&t136_TI,
	&t324_TI,
	&t4867_TI,
};
static Il2CppInterfaceOffsetPair t3384_IOs[] = 
{
	{ &t136_TI, 4},
	{ &t324_TI, 6},
	{ &t4867_TI, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType t3384_0_0_0;
extern Il2CppType t3384_1_0_0;
extern Il2CppGenericClass t3384_GC;
TypeInfo t3384_TI = 
{
	&g_mscorlib_dll_Image, NULL, "InternalEnumerator`1", "", t3384_MIs, t3384_PIs, t3384_FIs, NULL, &t110_TI, NULL, &t20_TI, &t3384_TI, t3384_ITIs, t3384_VT, &EmptyCustomAttributesCache, &t3384_TI, &t3384_0_0_0, &t3384_1_0_0, t3384_IOs, &t3384_GC, NULL, NULL, NULL, NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, (methodPointerType)NULL, sizeof (t3384)+ sizeof (Il2CppObject), 0, -1, 0, 0, -1, 1048845, 0, true, false, false, false, true, false, false, false, false, false, false, false, 5, 2, 2, 0, 0, 8, 3, 3};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
