﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3108;
struct t29;
struct t204;
struct t204_marshaled;
struct t553;
#include "t725.h"
#include "t3106.h"
#include "t552.h"

 void m17198 (t3108 * __this, t553 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17199 (t3108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m17200 (t3108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17201 (t3108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17202 (t3108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17203 (t3108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t3106  m17204 (t3108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t204 * m17205 (t3108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17206 (t3108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17207 (t3108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17208 (t3108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17209 (t3108 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
