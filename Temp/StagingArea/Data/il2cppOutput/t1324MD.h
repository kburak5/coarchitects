﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1324;
struct t781;
#include "t1064.h"

 void m7135 (t1324 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7136 (t1324 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7137 (t1324 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7138 (t1324 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m7139 (t1324 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m7140 (t1324 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7141 (t1324 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7142 (t1324 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7143 (t1324 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7144 (t1324 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int64_t m7145 (t1324 * __this, int64_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7146 (t1324 * __this, int64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7147 (t1324 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7148 (t1324 * __this, uint8_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
