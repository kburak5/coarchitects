﻿#pragma once
#include <stdint.h>
#include "t448.h"
#include "t35.h"
struct t441  : public t448
{
	t35 f0;
};
// Native definition for marshalling of: UnityEngine.AsyncOperation
struct t441_marshaled
{
	t35 f0;
};
