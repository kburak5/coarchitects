﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1464;
struct t774;
struct t42;
struct t1465;
struct t1466;
struct t29;
struct t1425;
struct t1424;

 t774 * m7943 (t1464 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1465 * m7944 (t1464 * __this, t1466 * p0, t42 * p1, t29 * p2, t1425 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7945 (t1464 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m7946 (t1464 * __this, t1425 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
