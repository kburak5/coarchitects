﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2492;
struct t29;
struct t20;
#include "t2458.h"

 void m13146 (t2492 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m13147 (t2492 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m13148 (t2492 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m13149 (t2492 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t2458  m13150 (t2492 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
