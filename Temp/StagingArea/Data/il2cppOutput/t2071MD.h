﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t2071;
#include "t465.h"

 void m10256 (t2071 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19364 (t2071 * __this, t465  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19365 (t2071 * __this, t465  p0, t465  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
