﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3231;
struct t29;
struct t20;
#include "t849.h"

 void m17951 (t3231 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17952 (t3231 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17953 (t3231 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17954 (t3231 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint16_t m17955 (t3231 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
