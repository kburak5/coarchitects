﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1010;
struct t809;
struct t7;
#include "t1009.h"

 void m4584 (t1010 * __this, t809 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4585 (t1010 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4586 (t1010 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4587 (t1010 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
