﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t950;
struct t781;
struct t809;
struct t7;

 void m4164 (t950 * __this, t809 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4563 (t950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m4165 (t950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4564 (t950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
