﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t510;
struct t29;
struct t66;
struct t67;
#include "t35.h"

 void m2612 (t510 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2613 (t510 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m2614 (t510 * __this, int32_t p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2615 (t510 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
