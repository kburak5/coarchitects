﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1619;
struct t29;
struct t446;
struct t66;
struct t67;
#include "t35.h"

 void m9632 (t1619 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9633 (t1619 * __this, t446* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m9634 (t1619 * __this, t446* p0, t67 * p1, t29 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m9635 (t1619 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
