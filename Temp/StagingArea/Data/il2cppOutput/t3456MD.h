﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3456;
struct t29;
struct t20;
#include "t1023.h"

 void m19150 (t3456 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19151 (t3456 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19152 (t3456 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19153 (t3456 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19154 (t3456 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
