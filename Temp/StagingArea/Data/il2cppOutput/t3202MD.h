﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3202;

 void m17814 (t3202 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17815 (t3202 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17816 (t3202 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
