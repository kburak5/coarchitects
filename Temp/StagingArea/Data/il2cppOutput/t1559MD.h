﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1559;
struct t781;

 void m8445 (t1559 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8446 (t1559 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8447 (t1559 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8448 (t1559 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8449 (t1559 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8450 (t1559 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8451 (t1559 * __this, t781* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8452 (t29 * __this, t781* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8453 (t1559 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8454 (t29 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
