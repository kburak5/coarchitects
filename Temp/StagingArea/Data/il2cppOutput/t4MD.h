﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t4;
struct t203;
struct t203_marshaled;
struct t136;
struct t29;

 void m36 (t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t203 * m1518 (t4 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t203 * m2538 (t4 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2539 (t4 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1828 (t4 * __this, t203 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2540 (t4 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2541 (t4 * __this, t203 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m59 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
