﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3103;
struct t29;
struct t204;
struct t204_marshaled;
struct t66;
struct t67;
#include "t35.h"
#include "t725.h"
#include "t552.h"

 void m17214 (t3103 * __this, t29 * p0, t35 p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m17215 (t3103 * __this, t204 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17216 (t3103 * __this, t204 * p0, int32_t p1, t67 * p2, t29 * p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t725  m17217 (t3103 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
