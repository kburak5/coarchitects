﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t570;
struct t29;
struct t556;
struct t3147;
struct t20;
struct t136;
struct t3148;
struct t3149;
struct t3150;
struct t3146;
struct t662;
struct t3151;
#include "t3152.h"

#include "t294MD.h"
#define m2987(__this, method) (void)m10537_gshared((t294 *)__this, method)
#define m17436(__this, p0, method) (void)m10539_gshared((t294 *)__this, (int32_t)p0, method)
#define m17437(__this, method) (void)m10541_gshared((t29 *)__this, method)
#define m17438(__this, method) (t29*)m10543_gshared((t294 *)__this, method)
#define m17439(__this, p0, p1, method) (void)m10545_gshared((t294 *)__this, (t20 *)p0, (int32_t)p1, method)
#define m17440(__this, method) (t29 *)m10547_gshared((t294 *)__this, method)
#define m17441(__this, p0, method) (int32_t)m10549_gshared((t294 *)__this, (t29 *)p0, method)
#define m17442(__this, p0, method) (bool)m10551_gshared((t294 *)__this, (t29 *)p0, method)
#define m17443(__this, p0, method) (int32_t)m10553_gshared((t294 *)__this, (t29 *)p0, method)
#define m17444(__this, p0, p1, method) (void)m10555_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m17445(__this, p0, method) (void)m10557_gshared((t294 *)__this, (t29 *)p0, method)
#define m17446(__this, method) (bool)m10559_gshared((t294 *)__this, method)
#define m17447(__this, method) (bool)m10561_gshared((t294 *)__this, method)
#define m17448(__this, method) (t29 *)m10563_gshared((t294 *)__this, method)
#define m17449(__this, method) (bool)m10565_gshared((t294 *)__this, method)
#define m17450(__this, method) (bool)m10567_gshared((t294 *)__this, method)
#define m17451(__this, p0, method) (t29 *)m10569_gshared((t294 *)__this, (int32_t)p0, method)
#define m17452(__this, p0, p1, method) (void)m10571_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m2988(__this, p0, method) (void)m10573_gshared((t294 *)__this, (t29 *)p0, method)
#define m17453(__this, p0, method) (void)m10575_gshared((t294 *)__this, (int32_t)p0, method)
#define m17454(__this, p0, method) (void)m10577_gshared((t294 *)__this, (t29*)p0, method)
#define m17455(__this, p0, method) (void)m10579_gshared((t294 *)__this, (t29*)p0, method)
#define m2995(__this, p0, method) (void)m10581_gshared((t294 *)__this, (t29*)p0, method)
#define m17456(__this, method) (t3150 *)m10583_gshared((t294 *)__this, method)
#define m2994(__this, method) (void)m10585_gshared((t294 *)__this, method)
#define m2991(__this, p0, method) (bool)m10587_gshared((t294 *)__this, (t29 *)p0, method)
#define m17457(__this, p0, p1, method) (void)m10589_gshared((t294 *)__this, (t316*)p0, (int32_t)p1, method)
#define m17458(__this, p0, method) (t556 *)m10591_gshared((t294 *)__this, (t2180 *)p0, method)
#define m17459(__this, p0, method) (void)m10593_gshared((t29 *)__this, (t2180 *)p0, method)
#define m17460(__this, p0, p1, p2, method) (int32_t)m10595_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, (t2180 *)p2, method)
 t3152  m17461 (t570 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
#define m17462(__this, p0, method) (int32_t)m10598_gshared((t294 *)__this, (t29 *)p0, method)
#define m17463(__this, p0, p1, method) (void)m10600_gshared((t294 *)__this, (int32_t)p0, (int32_t)p1, method)
#define m17464(__this, p0, method) (void)m10602_gshared((t294 *)__this, (int32_t)p0, method)
#define m17465(__this, p0, p1, method) (void)m10604_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
#define m17466(__this, p0, method) (void)m10606_gshared((t294 *)__this, (t29*)p0, method)
#define m17467(__this, p0, method) (bool)m10608_gshared((t294 *)__this, (t29 *)p0, method)
#define m2993(__this, p0, method) (int32_t)m10610_gshared((t294 *)__this, (t2180 *)p0, method)
#define m17468(__this, p0, method) (void)m10611_gshared((t294 *)__this, (int32_t)p0, method)
#define m17469(__this, method) (void)m10613_gshared((t294 *)__this, method)
#define m17470(__this, method) (void)m10615_gshared((t294 *)__this, method)
#define m17471(__this, p0, method) (void)m10617_gshared((t294 *)__this, (t2181 *)p0, method)
#define m17472(__this, method) (t3146*)m10619_gshared((t294 *)__this, method)
#define m17473(__this, method) (void)m10621_gshared((t294 *)__this, method)
#define m17474(__this, method) (int32_t)m10623_gshared((t294 *)__this, method)
#define m17475(__this, p0, method) (void)m10625_gshared((t294 *)__this, (int32_t)p0, method)
#define m2990(__this, method) (int32_t)m10626_gshared((t294 *)__this, method)
#define m2989(__this, p0, method) (t556 *)m10627_gshared((t294 *)__this, (int32_t)p0, method)
#define m17476(__this, p0, p1, method) (void)m10629_gshared((t294 *)__this, (int32_t)p0, (t29 *)p1, method)
