﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1540;
struct t781;

 void m8295 (t1540 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8296 (t1540 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8297 (t1540 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8298 (t1540 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8299 (t1540 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8300 (t1540 * __this, t781* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8301 (t1540 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8302 (t1540 * __this, uint64_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m8303 (t1540 * __this, uint32_t p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m8304 (t1540 * __this, uint32_t p0, uint32_t p1, uint32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m8305 (t1540 * __this, uint32_t p0, uint32_t p1, uint32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m8306 (t1540 * __this, uint32_t p0, uint32_t p1, uint32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m8307 (t1540 * __this, uint32_t p0, uint32_t p1, uint32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 uint32_t m8308 (t1540 * __this, uint32_t p0, uint32_t p1, uint32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8309 (t1540 * __this, uint32_t* p0, uint32_t p1, uint32_t* p2, uint32_t p3, uint32_t p4, uint32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8310 (t1540 * __this, uint32_t* p0, uint32_t p1, uint32_t* p2, uint32_t p3, uint32_t p4, uint32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8311 (t1540 * __this, uint32_t* p0, uint32_t p1, uint32_t* p2, uint32_t p3, uint32_t p4, uint32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8312 (t1540 * __this, uint32_t* p0, uint32_t p1, uint32_t* p2, uint32_t p3, uint32_t p4, uint32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8313 (t1540 * __this, uint32_t* p0, uint32_t p1, uint32_t* p2, uint32_t p3, uint32_t p4, uint32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8314 (t1540 * __this, uint32_t* p0, uint32_t p1, uint32_t* p2, uint32_t p3, uint32_t p4, uint32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8315 (t1540 * __this, uint32_t* p0, uint32_t p1, uint32_t* p2, uint32_t p3, uint32_t p4, uint32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8316 (t1540 * __this, uint32_t* p0, uint32_t p1, uint32_t* p2, uint32_t p3, uint32_t p4, uint32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8317 (t1540 * __this, uint32_t* p0, uint32_t p1, uint32_t* p2, uint32_t p3, uint32_t p4, uint32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8318 (t1540 * __this, uint32_t* p0, uint32_t p1, uint32_t* p2, uint32_t p3, uint32_t p4, uint32_t p5, int32_t p6, MethodInfo* method) IL2CPP_METHOD_ATTR;
