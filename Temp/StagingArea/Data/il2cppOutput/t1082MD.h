﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1082;
struct t1082_marshaled;

void t1082_marshal(const t1082& unmarshaled, t1082_marshaled& marshaled);
void t1082_marshal_back(const t1082_marshaled& marshaled, t1082& unmarshaled);
void t1082_marshal_cleanup(t1082_marshaled& marshaled);
