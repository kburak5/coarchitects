﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1592;
struct t200;
struct t781;
struct t1305;
struct t29;
struct t7;

 void m8678 (t1592 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8679 (t1592 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8680 (t1592 * __this, bool p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8681 (t1592 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8682 (t1592 * __this, t200* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8683 (t1592 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8684 (t1592 * __this, t781* p0, int32_t p1, int32_t p2, t200* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8685 (t1592 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8686 (t1592 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1305 * m8687 (t1592 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m8688 (t1592 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m8689 (t1592 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8690 (t1592 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8691 (t1592 * __this, uint16_t* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8692 (t1592 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8693 (t1592 * __this, uint16_t* p0, int32_t p1, uint8_t* p2, int32_t p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8694 (t1592 * __this, t7* p0, int32_t p1, int32_t p2, t781* p3, int32_t p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m8695 (t1592 * __this, t781* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
