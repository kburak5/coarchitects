﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1328;
struct t7;
struct t200;

 void m7198 (t1328 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7199 (t1328 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7200 (t1328 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7201 (t1328 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m7202 (t1328 * __this, t200* p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7203 (t1328 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m7204 (t1328 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m7205 (t1328 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
