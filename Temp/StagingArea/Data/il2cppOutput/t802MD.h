﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t802;
struct t806;
struct t29;
struct t20;
struct t136;
struct t812;
struct t791;

 void m3389 (t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3390 (t802 * __this, t20 * p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3391 (t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m3392 (t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3393 (t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t806 * m3394 (t802 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m3395 (t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t812 * m3396 (t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3397 (t802 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m3398 (t802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3399 (t802 * __this, t791 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
