﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1051;
struct t1035;
struct t29;
struct t295;
struct t1050;
struct t67;

 void m4783 (t1051 * __this, t67 * p0, t29 * p1, t1035 * p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1035 * m4784 (t1051 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m4785 (t1051 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t295 * m4786 (t1051 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4787 (t1051 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1050 * m4788 (t1051 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4789 (t1051 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4790 (t1051 * __this, t295 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4791 (t1051 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
