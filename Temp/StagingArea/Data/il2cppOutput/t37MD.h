﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t37;
struct t7;
struct t441;
struct t441_marshaled;
#include "t376.h"
#include "t447.h"

 void m54 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t441 * m2477 (t29 * __this, t7* p0, int32_t p1, bool p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1818 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1821 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m1710 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m2478 (t29 * __this, t7* p0, t7* p1, int32_t p2, bool p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
