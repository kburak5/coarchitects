﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t953;
struct t7;

 void m8375 (t953 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t953 * m4194 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t953 * m8376 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
