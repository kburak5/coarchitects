﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3475;
struct t29;
struct t20;
#include "t1568.h"

 void m19322 (t3475 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m19323 (t3475 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m19324 (t3475 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m19325 (t3475 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m19326 (t3475 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
