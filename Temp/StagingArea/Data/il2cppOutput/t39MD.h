﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t39;
struct t242;
struct t6;
struct t53;
struct t25;
#include "t140.h"

 void m993 (t39 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t242 * m994 (t39 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m995 (t39 * __this, t242 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m996 (t39 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m997 (t39 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m998 (t39 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m999 (t39 * __this, t242 * p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m58 (t39 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1000 (t39 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1001 (t39 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1002 (t39 * __this, bool p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1003 (t39 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1004 (t39 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1005 (t39 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1006 (t39 * __this, t6 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m1007 (t39 * __this, t53 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m1008 (t39 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t25 * m1009 (t39 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
