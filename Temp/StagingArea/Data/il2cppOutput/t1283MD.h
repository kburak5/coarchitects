﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1283;
struct t29;
struct t580;

 void m6767 (t1283 * __this, t580 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m6768 (t1283 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6769 (t1283 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
