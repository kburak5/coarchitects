﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1192;
struct t7;
struct t781;
struct t29;
#include "t1120.h"

 void m6175 (t1192 * __this, int32_t p0, t7* p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m6176 (t1192 * __this, int32_t p0, t7* p1, t781* p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6177 (t29 * __this, t1192 * p0, t1192 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6178 (t1192 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t781* m6179 (t1192 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6180 (t1192 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6181 (t1192 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6182 (t1192 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
