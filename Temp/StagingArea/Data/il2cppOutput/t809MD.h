﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t809;
struct t7;
struct t793;
struct t29;
struct t292;

 void m4541 (t809 * __this, t793 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4542 (t809 * __this, t809 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4543 (t809 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4544 (t809 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4173 (t809 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4172 (t809 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t793 * m4182 (t809 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m4545 (t809 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m4546 (t809 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m4547 (t809 * __this, t292 * p0, int32_t p1, int32_t p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m4548 (t809 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
