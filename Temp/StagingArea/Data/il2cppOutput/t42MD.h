﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t42;
struct t929;
struct t7;
struct t1142;
struct t296;
struct t29;
struct t537;
struct t1143;
struct t1144;
struct t557;
struct t631;
struct t634;
struct t1145;
struct t1146;
struct t660;
struct t1147;
struct t316;
struct t633;
struct t446;
#include "t1148.h"
#include "t1149.h"
#include "t43.h"
#include "t35.h"
#include "t1127.h"
#include "t630.h"
#include "t1150.h"

 void m5984 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m5985 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5986 (t29 * __this, t296 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5987 (t29 * __this, t296 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5988 (t29 * __this, t296 * p0, t29 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m5989 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m5990 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5991 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5992 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5993 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5994 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5995 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5996 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5997 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5998 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m5999 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6000 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6001 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m3000 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6002 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6003 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6004 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6005 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m6006 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t43  m6007 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6008 (t42 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6009 (t42 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6010 (t42 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m6011 (t29 * __this, t35 p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m6012 (t29 * __this, t7* p0, bool p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m6013 (t29 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m2978 (t29 * __this, t7* p0, bool p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6014 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6015 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m1554 (t29 * __this, t43  p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t43  m6016 (t29 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6017 (t29 * __this, t42 * p0, t42 * p1, bool p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6018 (t29 * __this, t42 * p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6019 (t42 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m2981 (t42 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6020 (t42 * __this, t29 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m6021 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m6022 (t42 * __this, t7* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m6023 (t42 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m2999 (t42 * __this, t7* p0, int32_t p1, t631 * p2, t537* p3, t634* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t557 * m6024 (t42 * __this, t7* p0, int32_t p1, t631 * p2, int32_t p3, t537* p4, t634* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1146 * m6025 (t42 * __this, t7* p0, int32_t p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1146 * m6026 (t42 * __this, t7* p0, t42 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1146 * m6027 (t42 * __this, t7* p0, t42 * p1, t537* p2, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1146 * m6028 (t42 * __this, t7* p0, int32_t p1, t631 * p2, t42 * p3, t537* p4, t634* p5, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6029 (t29 * __this, t42 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6030 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6031 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6032 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t660 * m2980 (t42 * __this, t537* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t660 * m6033 (t42 * __this, int32_t p0, t631 * p1, t537* p2, t634* p3, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t660 * m6034 (t42 * __this, int32_t p0, t631 * p1, int32_t p2, t537* p3, t634* p4, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t7* m6035 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6036 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t537* m6037 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6038 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6039 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m6040 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m6041 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6042 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m6043 (t29 * __this, t42 * p0, t537* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t42 * m2979 (t42 * __this, t537* p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6044 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m6045 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t316* m6046 (t42 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
