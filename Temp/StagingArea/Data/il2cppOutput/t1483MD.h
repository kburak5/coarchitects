﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1483;
struct t1458;
struct t1494;
struct t29;
struct t1034;
struct t1498;
struct t1304;
#include "t1495.h"
#include "t735.h"
#include "t1497.h"

 void m8037 (t1483 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8038 (t1483 * __this, t29 * p0, t735  p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8039 (t29 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8040 (t1483 * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1494 * m8041 (t1483 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t735  m8042 (t1483 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8043 (t1483 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m8044 (t1483 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8045 (t1483 * __this, t1034 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m8046 (t1483 * __this, t1034 * p0, t1498 * p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8047 (t1483 * __this, t1304 * p0, bool* p1, MethodInfo* method) IL2CPP_METHOD_ATTR;
