﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3023;
struct t29;
struct t20;
#include "t150.h"

 void m16504 (t3023 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m16505 (t3023 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m16506 (t3023 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m16507 (t3023 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m16508 (t3023 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
