﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t1305;
struct t1577;
struct t1576;
struct t781;
struct t200;

 void m8547 (t1305 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m8548 (t1305 * __this, t1577 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t1576 * m8549 (t1305 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
