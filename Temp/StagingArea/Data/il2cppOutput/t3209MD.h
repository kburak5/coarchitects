﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

struct t3209;
struct t29;
struct t20;
#include "t785.h"

 void m17845 (t3209 * __this, t20 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
 t29 * m17846 (t3209 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 void m17847 (t3209 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 bool m17848 (t3209 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
 int32_t m17849 (t3209 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
